/*
Name 					- ActionNoteTrigger
Created By 				- Nitish Bansal
Created Date 			- 02/23/2016
Purpose 				- T-477246 

Modified                - T-477244 (02/24/2016)
*/
trigger ActionNoteTrigger on Action_Note__c (before insert, before update, before delete) {
	
	//Checking if trigger is active
	if(TriggerState.isActive(Constants.ACTIONNOTE_TRIGGER)){
		//Creating and initilizing trigger handler instance
		ActionNoteTriggerHandler handler = new ActionNoteTriggerHandler();
		
		if(trigger.isBefore){
			if(trigger.isInsert){
		        handler.beforeRecordInsert(Trigger.new);
		    } 	
			else if(trigger.isUpdate){
		        handler.beforeRecordUpdate(Trigger.oldMap, Trigger.newMap);
		    } 
		    //NB - 06/21- I-219767 - Start
		    else if(trigger.isDelete){
		    	handler.beforeRecordDelete(Trigger.oldMap);
		    }
		    //NB - 06/21- I-219767 - End
		} 
	}
	  
}