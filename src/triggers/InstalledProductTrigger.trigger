// (c) 2015 Appirio, Inc.
//
// Trigger Name: InstalledProductTrigger
//
// April 6 2016, Prakarsh Jain  Original 
//
trigger InstalledProductTrigger on InstalledProduct__c (after insert) {
  //Method to populate product description field on Installed product(Stryker Ipad Issues workbook)
  // Only run this trigger if the InstalledProductTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_INSTALLEDPRODUCTTRIGGER)) {
    Instruments_IPTriggerHandler.populateProductDescriptionOnInstalledProduct(trigger.new);
  } 
}