// (c) 2016 Appirio, Inc.
//
// Name: StrategicAccountTrigger
// Description: Trigger for Strategic Account Custom Object.
// 
// June 22 2016, Isha Shukla  Original (T-513125)
//
trigger StrategicAccountTrigger on Strategic_Account__c (before insert) {
	StrategicAccountTriggerHandler.populateUserOnStrategicAccount(Trigger.New);
}