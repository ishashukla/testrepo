// 
// (c) 2016 Appirio, Inc.
//
// Name : RMAShipmentOrderLineTrigger 
// 2 Nov 2016   Mohan Panneerselvam 
//
trigger RMAShipmentOrderLineTrigger on SVMXC__RMA_Shipment_Line__c (after update) {
    if(Trigger.isAfter){
        if( Trigger.isUpdate){
            COMM_RMAShipmentOrderLineTriggerHandler.processRMAItemsAfterUpdate(Trigger.oldMap, Trigger.newMap);
        }
    } 
}