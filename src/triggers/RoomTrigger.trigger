/*------------------------------------------------------------------------------------------------
/Purpose:   This purpose of this trigger is to autodelete Equipments, Matrix, Matrix Rows records 
/           on deleating a Room.
/Author:    Gagandeep Brar (gagandeep.brar@stryker.com)
/Date:      08/01/2015
/Requirements: 
/           CSI36191: SFDC Project Management PW to SOW Report
/Revision:
/           [08/01/2015, Gagan Brar] Initial release
/------------------------------------------------------------------------------------------------*/
trigger RoomTrigger on Room__c (before delete,after delete, after insert, after update) {
  Room__c[] RoomsToDelete = Trigger.old;
  if(Trigger.isdelete && Trigger.isbefore) {
    RoomTriggerHandler.deleteChildRecords(RoomsToDelete);
  }else if(Trigger.isdelete && Trigger.isAfter) {
    RoomTriggerHandler.onAfterDelete(Trigger.old);
  }else if(Trigger.isInsert && Trigger.isAfter) {
    RoomTriggerHandler.onAfterInsert(Trigger.new);
  }else if(Trigger.isUpdate && Trigger.isAfter) {
    RoomTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
  }
  
}