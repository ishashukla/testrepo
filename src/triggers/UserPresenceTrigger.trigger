/**================================================================      
 * Appirio, Inc
 * Name: UserPresenceTrigger
 * Description: Trigger on  Standard Object UserServicePresence(T-505263)
 * Created Date: 05/26/2016
 * Created By: Ashu Gupta (Appirio)
 ==================================================================*/
trigger UserPresenceTrigger on UserServicePresence (after insert) {

    if(Trigger.isAfter && Trigger.isInsert){
    	UserPresenceTriggerHandler.onAfterInsert(Trigger.new,Trigger.newMap);
    }
}