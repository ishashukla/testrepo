/*
Name            ProjectPlanTrigger
Purpose         T-490786 (To create project share records)
Created Date    04/26/2016
Created By      Nitish Bansal
*/
trigger ProjectPlanTrigger on Project_Plan__c (after insert, after update,before update,before insert) {
    if(TriggerState.isActive(Constants.PROJECT_TRIGGER)){
        if(Trigger.isAfter){
          if(Trigger.isInsert){
            ProjectPlanTriggerHandler.shareProjectRecords(Trigger.new,Trigger.oldMap);
            ProjectPlanTriggerHandler.afterInsert(Trigger.new);
          }else if(Trigger.isUpdate){
            ProjectPlanTriggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
            ProjectPlanTriggerHandler.shareProjectRecords(Trigger.new,Trigger.oldMap);
          }
        }
        if(Trigger.isBefore) {
          if(Trigger.isInsert) {
            ProjectPlanTriggerHandler.beforeInsert(Trigger.new);
          }else if(Trigger.isUpdate) {
            ProjectPlanTriggerHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
          }
        }
    }       
}