trigger RelatedContactTrigger on Related_Contact__c(after Insert, after update,after delete) { 
  if(trigger.isInsert && Trigger.isAfter) {
      OpptyContactTriggerHandler.afterInsert((List<Related_Contact__c >)trigger.new);
    }else if(trigger.isUpdate && Trigger.isAfter) {
      OpptyContactTriggerHandler.afterUpdate((List<Related_Contact__c>)trigger.new,(Map<ID, Related_Contact__c>) trigger.oldMap);
  }else if(trigger.isDelete && Trigger.isAfter) {
    OpptyContactTriggerHandler.afterDelete((List<Related_Contact__c>) trigger.old);
  } 
}