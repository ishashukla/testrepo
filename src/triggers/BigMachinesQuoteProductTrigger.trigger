// 
// (c) 2016 Appirio, Inc.
// Created this trigger to create a Opportunity product automatically when a Quote is created with line item and is related to T-483787
// 
//
// 18 Mar 2016    Mehul Jain      Added for T-483787

trigger BigMachinesQuoteProductTrigger on BigMachines__Quote_Product__c (after insert, after update) {
    if(Trigger.isAfter && Trigger.isInsert) {
        //Operation for trigger after Insert           
        BigMachinesQuoteProductTriggerHandler.afterInsert(Trigger.new);
        system.debug('in after insert trigger ');
    }

}