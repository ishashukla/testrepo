trigger SPS_AccountTrigger on Account (after insert, after update, before update) {
  
 // Only run this trigger if the SPS_AccountTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_SPS_AccountTrigger)) {
		  // Only run this trigger if the SPS_AccountTrigger Custom Setting is 'true' or the current user is not dataload.usernt@stryker.com
		  if(!UserInfo.getUserName().startsWith('dataload.usernt@stryker.com')) {
		    //Populate Strategic account and Negotiated Contract checkbox based on parent account specified on a account
		    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
		      SPS_AccountTriggerHandler.PopulateValuesToChildAccounts(trigger.oldMap, Trigger.new);
		      system.debug('==>isAfter==>'+trigger.new[0].ParentId);
		    }
		  }
		  
		  if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
		    //Method to add AccountTeamMember when Account is inserted Billing state/Shipping State is inserted/updated(T-486819)
		    Instruments_AccountTriggerHandler.insertFlexRepToAccountTeam(Trigger.oldMap, Trigger.new);
		    Instruments_AccountTriggerHandler.shareInstallBaseRecords(Trigger.new, Trigger.oldMap);
		  }
		  if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
		  	Instruments_AccountTriggerHandler.populateGPONames( Trigger.new);
		  }
  }
}