/*************************************************************************************************
Created By:    Sunil Gupta
Date:          Feb 06, 2014
Description  : Contact Trigger
**************************************************************************************************/
trigger Endo_ContactTrigger on Contact (before insert, before update, after insert, after update) {
  
   // Only run this trigger if the Endo_ContactTrigger Custom Setting is 'true'
   //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_ENDO_CONTACTTRIGGER)) {
  
          //----------------------------------------------------------------------------------------------
          //  Update Mailing Address on Contact from respective Account
          //---------------------------------------------------------------------------------------------- 
          if(Trigger.isBefore && (Trigger.isInsert || trigger.isUpdate)){
            Endo_ContactTriggerHandler.UpdateMailingAddress(trigger.new, trigger.oldMap);
            
          } 
          
          //----------------------------------------------------------------------------------------------
          //  Insert record in contact account junction object.
          //---------------------------------------------------------------------------------------------- 
          if(Trigger.isAfter && (Trigger.isInsert || trigger.isUpdate)){
            Endo_ContactTriggerHandler.InsertContactAccountAssociation(trigger.new, trigger.oldMap);
            
          }
          
          //----------------------------------------------------------------------------------------------
          //  Populate Sales Support Agent on Contact
          //---------------------------------------------------------------------------------------------- 
          if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
            Endo_ContactTriggerHandler.populateSalesSupportAgent(trigger.new, trigger.oldMap);    
          }
          //----------------------------------------------------------------------------------------------
          //  Update the Employee Contact and Product Junction Table if Product line value has been changed
          //---------------------------------------------------------------------------------------------- 
          //if(Trigger.isAfter && trigger.isUpdate){
          //  Endo_ContactTriggerHandler.UpdateEmployeeContactProductJunction(trigger.oldMap, trigger.new);
          //}
   
  } //end Story# S-415285
   
}