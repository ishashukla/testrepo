/*************************************************************************************************
Created By:    Megha Agarwal (Appirio JDC)
Date:          Oct 5th , 2015
Description  : Case Trigger - before update to prevent case closure if any open activity is exist

Feb.03, 2016; sunil; Add method for insertCaseAssetJunctionRecord

March.17, 2016; sunil; Add method for sendCasesToTrackwise
**************************************************************************************************/
trigger Medical_CaseTrigger on Case (before update, after insert, after update) {
  
 // Only run this trigger if the Medical_CaseTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_MEDICAL_CASETRIGGER)) {

          //----------------------------------------------------------------------------------------------
          //  Insert CaseAssetJunction Record When Case is created
          //----------------------------------------------------------------------------------------------
          if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            Medical_CaseTriggerHandler.insertCaseAssetJunctionRecord(Trigger.new, Trigger.oldMap);
          }
        
          //----------------------------------------------------------------------------------------------
          //  Send Eligible Case to Trackwise
          //----------------------------------------------------------------------------------------------
          if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            Medical_CaseTriggerHandler.sendCasesToTrackwise(Trigger.new, Trigger.oldMap);
          }
        
        
          //----------------------------------------------------------------------------------------------
          //  check NoActivity Associated before closing a Case
          //----------------------------------------------------------------------------------------------
          if(Trigger.isBefore && Trigger.isUpdate){
            // Commented out, Compiler Error - NW
            //Medical_CaseTriggerHandler.checkNoActivityAssociated(Trigger.newMap , Trigger.oldMap);
          }
        
        
          //----------------------------------------------------------------------------------------------
          //  Validate that before closing the Case, RQA record is associated when type is Potential Product Case
          //----------------------------------------------------------------------------------------------
          if(Trigger.isBefore && Trigger.isUpdate){
            // Commented out, Compiler Error - NW
            //Medical_CaseTriggerHandler.validateRQAExistOnPotentialProductCases(Trigger.newMap , Trigger.oldMap);
          }

          /*START ACARSON C-00173805 07.15.2016
          //Added method call for validate asset for checking a medical case has an asset before closing */
          //----------------------------------------------------------------------------------------------
          //  In this method we will check if Junction object is not having any reocrd and Asset Lookup is blank than throw an error.
          //  We can not do this using Validation rule as Asset Lookup is removed and copied to Junction object as soon as Case is Saved.
          //----------------------------------------------------------------------------------------------
          if(Trigger.isBefore && Trigger.isUpdate){
            // Commented out, Compiler Error - NW
            //Medical_CaseTriggerHandler.validateAssetExist(Trigger.newMap , Trigger.oldMap);
          }
          //END ACARSON C-00173805 07.15.2016
          
  }

}