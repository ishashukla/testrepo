/**================================================================      
* Appirio, Inc
* Name: ProductStockTrigger
* Description: ProductStokeTrigger for T-539940
* Created Date: 22 - Sept - 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update

==================================================================*/
trigger ProductStockTrigger on  SVMXC__Product_Stock__c (after update) {
    //Checking if thrigger is active
    if(TriggerState.isActive(Constants.SVMXC_Product_Stock_Trigger)){
        if(Trigger.isAfter && Trigger.isUpdate){
            ProductStockTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}