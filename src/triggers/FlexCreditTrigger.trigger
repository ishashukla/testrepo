/*************************************************************************************************
Created By:    Sunil Gupta
Date:          Jun 9, 2014
Description  : Flex Credit Trigger

// Revision History:
// 2015-08-25   Sunil;  Update opportunity winning bank field when credit status is approved
**************************************************************************************************/
trigger FlexCreditTrigger on Flex_Credit__c(after insert, after update, before insert, before update) {
  
  // Only run this trigger if the FlexCreditTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_FLEXCREDITTRIGGER)) {
    
	  //----------------------------------------------------------------------------------------------
	  //  Update opportunity stage when credit status is declined
	  //----------------------------------------------------------------------------------------------
	  if(Trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
	    FF_FlexCreditTriggerHandler.updateOpportunityStage(trigger.oldMap, trigger.new);
	    // Update Current Time when Status is changed
	    FF_FlexCreditTriggerHandler.updateSatusChangeTime(trigger.oldMap, trigger.new);
	
	    // Update opportunity winning bank field when credit status is approved
	    FF_FlexCreditTriggerHandler.updateOpportunityWinningBank(trigger.oldMap, trigger.new);
	  }
	
	  //----------------------------------------------------------------------------------------------
	  //  Before Insert, Before Update Set Business Hrs
	  //----------------------------------------------------------------------------------------------
	  if(Trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
	    FF_FlexCreditTriggerHandler.updateBusinessHours(trigger.oldMap, trigger.new);
	  }

 } //end Story# S-415285
}