// 
// (c) 2016 Appirio, Inc.
//
// Name : NV_OrderItemTrigger 
// Added after delete and undelete event for S-389737 by Jyoti
// 20th April 2016     Kirti Agarwal      Modified(T-492006)
//
trigger NV_OrderItemTrigger on OrderItem (before insert, before update, after insert, after update, after delete, after undelete) {
    
 // Only run this trigger if the NV_OrderItemTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
 if(TriggerState.isActive(Constants.TRIGGER_NV_ORDERITEMTRIGGER)) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            NV_OrderItemTriggerHandler.processOrderItemsBeforeInsertUpdate(Trigger.oldMap, Trigger.new, Trigger.isUpdate);
        }
    }

    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            NV_OrderItemTriggerHandler.processOrderItemsAfterInsertUpdate(Trigger.oldMap, Trigger.newMap);
        }

        //Added for CM Release April 14th
        //Start - Added by Jyoti for Story S-389737
        if(Trigger.isDelete){
            NV_OrderItemTriggerHandler.processOrderItemsAfterDelete(Trigger.old,Trigger.oldMap);
        }
        if(Trigger.isUnDelete){
            NV_OrderItemTriggerHandler.processOrderItemsAfterUndelete(Trigger.new);
        }
        //End - S-389737        
        
    } 
    
    //T-492006 - Added by Kirti - used to populate Order's field 
    if(Trigger.isBefore && Trigger.isUpdate) {
     COMM_OrderItemTriggerHandler orderItemObj = new COMM_OrderItemTriggerHandler();
     //Added oldMap parameter to below method - KM 10/4/16 - I-237348
     orderItemObj.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
    if(Trigger.isAfter &&  Trigger.isInsert ) {
      COMM_OrderItemTriggerHandler orderItemObj = new COMM_OrderItemTriggerHandler();
      orderItemObj.afterInsert(Trigger.New);
    }

    // Added by Mehul Jain for T-492016
    if( Trigger.isAfter && Trigger.isUpdate) {
      COMM_OrderItemTriggerHandler orderItemObj = new COMM_OrderItemTriggerHandler();
      orderItemObj.afterUpdate(Trigger.New, Trigger.oldMap);
    } 
   
    // Added by Mehul Jain for T-492016
    if( Trigger.isDelete) {
      system.debug('in delete' + Trigger.old);
      COMM_OrderItemTriggerHandler orderItemObj = new COMM_OrderItemTriggerHandler();
      orderItemObj.afterDelete(Trigger.old);
    }    
   
    if(Trigger.isBefore && Trigger.isInsert) {
      COMM_OrderItemTriggerHandler.beforeInsert(Trigger.new);
    } 
    /* Populate Pricebook Entry Id during data load - NB - 05/19 */
    if(Trigger.isBefore && Trigger.isInsert) {
      system.debug('in before trigger');
      COMM_OrderItemTriggerHandler orderItemObj = new COMM_OrderItemTriggerHandler();
      orderItemObj.populatePriceBookValue(Trigger.new);
    } 
  } 
}