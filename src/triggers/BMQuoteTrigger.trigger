//
// (c) 2016 Appirio, Inc.
//
//  Migrated From COMM Src
//
// This trigger controls the events on BigMachines_Quote__c and modifies data if required.
// 
//  13 April 2016,        Meghna Vijay

trigger BMQuoteTrigger on BigMachines__Quote__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  
  BMQuoteTriggerHandler inst = new BMQuoteTriggerHandler(trigger.isAfter, trigger.isBefore, trigger.isDelete, trigger.isInsert, trigger.isUnDelete, trigger.isUpdate,
                    trigger.new, trigger.newMap, trigger.old, trigger.oldMap);

    /* Run the controller. */
    inst.run();
    
    //NB - 06/27 - I-224056 - Start
    BMQuoteTriggerHandler handler = new BMQuoteTriggerHandler();
    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter){
        handler.setQuoteOrderNumberOpp(trigger.new, trigger.oldMap);
    }
    //NB - 06/27 - I-224056 - end
}