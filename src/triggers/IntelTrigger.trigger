/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 22, 2013
 Purpose       :  Trigger on Intel__c records
 Reference     :  Task T-214509
 Modifications :  Task T-217899    Dec 12, 2013    Sonal Shrivastava
*******************************************************************************/
trigger IntelTrigger on Intel__c (before insert, after insert) {
 
 // Only run this trigger if the IntelTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_INTELTRIGGER)) {
  
	  if(trigger.isBefore && trigger.isInsert){
	  	IntelTriggerHandler.onBeforeInsert(trigger.new);
	  }
	  else if(trigger.isAfter && trigger.isInsert){
	    IntelTriggerHandler.onAfterInsert(trigger.new);
	  }
  }
}