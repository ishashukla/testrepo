/*************************************************************************************************
Created By:    Megha Ag (Appirio)
Date:          Nov 28, 2015
Description  : Trigger on Order Product Object
**************************************************************************************************/
trigger OrderProductsTrigger on OrderItem (after insert , after update) {
    if(Trigger.isInsert && Trigger.isAfter){
        OrderProductsTriggerHandler.afterInsert(Trigger.new);
  }
  else if(Trigger.isUpdate && Trigger.isAfter){
    OrderProductsTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
  }
}