/******************************************************************************************************************
Created By:    Sunil Gupta
Date :          Jan 27, 2014
Description  : Case Trigger
Modified By  : Kajal Jalan , Date : May 05,2016, Description  : Case Trigger - T-500173(Total Hours by Case Status)
*******************************************************************************************************************/
trigger Endo_CaseTrigger on Case (after insert, before insert, before update, after update) {
  // Only run this trigger if the Endo_CaseTrigger Custom Setting is 'true' or the current user is not dataload.usernt@stryker.com
  if(TriggerState.isActive('Endo_CaseTrigger') || (!UserInfo.getUserName().startsWith('dataload.usernt@stryker.com'))) {
    //----------------------------------------------------------------------------------------------
    // Update Sales Rep Email Field on Case
    //---------------------------------------------------------------------------------------------- 
    //if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){ 
    //  Endo_CaseTriggerHandler.populateSalesRepEmailFieldOnCase(Trigger.new, Trigger.oldMap);
    //} 
    System.debug('Trigger Ran!!!');
    //----------------------------------------------------------------------------------------------
    // Notify If Tech Support case is already Exists in previous 7 days.
    //---------------------------------------------------------------------------------------------- 
    if(Trigger.isAfter && Trigger.isInsert){
        // Commented out, Compiler Error - NW
        //Endo_CaseTriggerHandler.notifySalesRepIfCaseAlreadyExists(Trigger.new, Trigger.newMap);
        //Endo_CaseTriggerHandler.TrackHistory(trigger.new, null);
    }
    
    //----------------------------------------------------------------------------------------------
    // T-242025 Auto-populate "Sales Rep" lookup for Case type :Sales Order Entry
    //---------------------------------------------------------------------------------------------- 
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
      // Don't change the order of calling below functions, we are using the Sales Rep in top35 method.
      Endo_CaseTriggerHandler.populateSalesRep(Trigger.new, Trigger.oldMap);
      Endo_CaseTriggerHandler.populateSaleSupportAgent(Trigger.new, Trigger.oldMap);
      if(Trigger.isInsert){
          Endo_CaseTriggerHandler.populateAssignedStateForEmailToCase(Trigger.new);
      }
      //----------------------------------------------------------------------------------------------
      // Start : T-500173 Total Hours by Case Status (INSTRUMENT)
      //----------------------------------------------------------------------------------------------
        // Commented out, Compiler Error - NW
        //Intr_CaseTriggerHandler.populateLastStatusChange(Trigger.new, Trigger.oldMap);
      //----------------------------------------------------------------------------------------------
      // Stop : T-500173 Total Hours by Case Status (INSTRUMENT)
      //----------------------------------------------------------------------------------------------
    }
    
    
    //----------------------------------------------------------------------------------------------
    // T-237859 Update Mood Value on Related Account.
    //---------------------------------------------------------------------------------------------- 
    if(Trigger.isAfter && Trigger.isUpdate){
      Endo_CaseTriggerHandler.updateAccountMoodValue(Trigger.new, Trigger.oldMap);
      //Endo_CaseTriggerHandler.TrackHistory(trigger.new, trigger.oldMap);    
    }
  
    //----------------------------------------------------------------------------------------------
    //  T-253588 Add Sales Support Agent to Support Case
    //---------------------------------------------------------------------------------------------- 
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
      Endo_CaseTriggerHandler.TrackHistory(trigger.new, trigger.oldMap);
      //Endo_CaseTriggerHandler.populateSalesSupportAgent(Trigger.new, Trigger.oldMap);    
    }  
  }
}