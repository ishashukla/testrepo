/**================================================================      
* Appirio, Inc
* Name: COMM_cycleCount_Trigger 
* Description: Create Trigger on Cycle count to call approval process from SFM T-542521
* Created Date: 3 - Oct - 2016
* Created By: Gaurav Sinha (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
trigger COMM_cycleCount_Trigger on Cycle_Count__c (After update) {

    If(trigger.isafter && trigger.isupdate){
        COMM_cycleCount_TriggerManager.afterUpdate(trigger.new,trigger.oldmap);
    }
}