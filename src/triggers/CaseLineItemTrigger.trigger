/*************************************************************************************************
// Created By:    Rahul Khanchandani
// Date:          13/04/2016
// Description  : CaseLineItemTrigger
//
// 21st April 2016     Kirti Agarwal      (I-214565)
// Populate Price field based on List Price from the related Product object (Part field) based on the COMM Price book List Price.
//
**************************************************************************************************/

trigger CaseLineItemTrigger on Case_Line_Item__c (before insert, before update, after insert, after update, after delete, after undelete) {
 
   if(Trigger.isAfter && Trigger.isInsert) {
        CaseLineItemTriggerHandler.afterInsert(Trigger.new);
    } 
    else if(Trigger.isAfter && Trigger.isUpdate) {
        CaseLineItemTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    } 
    else if(Trigger.isAfter && Trigger.isDelete) {
        CaseLineItemTriggerHandler.afterDelete(Trigger.old);
    }
    else if(Trigger.isAfter && Trigger.isUndelete) {
        CaseLineItemTriggerHandler.afterUndelete(Trigger.new);
    }
    
    //I-214565 - Added By Kirti - Populate Price field based on List Price from the related Product object 
    //based on the COMM Price book List Price
    if(Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)) {
      COMM_CaseLineItemTriggerHandler.beforeInsertAndUpdate(Trigger.new, Trigger.oldMap);
    }
}