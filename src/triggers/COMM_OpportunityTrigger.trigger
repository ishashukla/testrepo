// (c) 2016 Appirio, Inc.
// Class Name: COMM_OpportunityTrigger - Trigger for opportunity
// 03/09/2016, Deepti Maheshwari (T-458596)
// 04/11/2016, Noopur- Commented the code here and merged it to the trigger OpportunityTrigger
//
trigger COMM_OpportunityTrigger on Opportunity (before insert, before update) {
  //Checking if trigger is active
  if(TriggerState.isActive(Constants.OPPTY_TRIGGER)) {
  /*  if(trigger.isInsert) {
      COMM_OpportunityTriggerHandler.beforeInsert((List<Opportunity>)trigger.new);
    }else {
      COMM_OpportunityTriggerHandler.beforeUpdate((List<Opportunity>)trigger.new,
                                        (Map<ID, Opportunity>) trigger.oldMap);
    }*/
  }
}