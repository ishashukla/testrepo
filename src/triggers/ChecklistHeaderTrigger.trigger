/**================================================================      
* Appirio, Inc
* Name: ChecklistHeaderTrigger
* Description: T-534013
* Created Date: 09 Sep 2016
* Created By: Kanika Mathur (Appirio)
*
* Date Modified      Modified By      Description of the update
* 12 Sep 2016        Kanika Mathur    T-534015
==================================================================*/
trigger ChecklistHeaderTrigger on Checklist_Header__c (after insert, before update, 
                                                      after update, before insert) {
  
  if(TriggerState.isActive('ChecklistHeaderTrigger')){
  	if(Trigger.isAfter) {
  		if(Trigger.isUpdate) {
  			COMM_ChecklistHeaderTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
  		}
  		if(Trigger.isInsert) {
  		  COMM_ChecklistHeaderTriggerHandler.onAfterInsert(Trigger.new);
  		}
  	}
  	
  	if(Trigger.isBefore) {
      if(Trigger.isUpdate) {
        COMM_ChecklistHeaderTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
      }
      if(Trigger.isInsert) {
  		  COMM_ChecklistHeaderTriggerHandler.onBeforeInsert(Trigger.new);
  		}
  	}
  }
}