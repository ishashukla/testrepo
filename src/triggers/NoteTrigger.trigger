/**================================================================   
* Appirio, Inc
* Name: NotesTrigger
* Description: Notes Trigger
* Created Date: Oct 5, 2016
* Created By: Shreerath Nair
* 
* Date Modified    Modified By             Description of the update
* Oct 5, 2016      Shreerath Nair          Trigger Creation T-542918
==================================================================*/
trigger NoteTrigger on Notes__c (after insert, before insert, before update, after update,before delete) {

     //Checking if trigger is active
    if(NoteTriggerHandler.IsActive()) {
        NoteTriggerHandler handler = new NoteTriggerHandler();
        if(trigger.isBefore){
            if(trigger.isInsert){ 
                handler.beforeInsert(Trigger.new);
            } 
            else if(trigger.isUpdate){
                handler.beforeUpdate(Trigger.new, Trigger.oldMap);
            }
            else if(Trigger.isDelete){
                handler.beforeDelete(Trigger.old, Trigger.oldMap);
            }
        } 
        else if(trigger.isAfter){
            if(trigger.isInsert){
                handler.afterInsert(Trigger.new);
            } 
            else if(trigger.isUpdate){
                handler.afterUpdate(Trigger.new, Trigger.oldMap);
            }
            //else if(Trigger.isDelete && Trigger.isAfter){
            //    handler.afterDelete(Trigger.old, Trigger.oldMap);
            //}            
        }  
    }

}