/*************************************************************************************************
Created By:    Sunil Gupta
Date:          June 02, 2014
Description  : OpportunityLineItem  Trigger
**************************************************************************************************/
trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert, before update) {
  
 // Only run this trigger if the OpportunityLineItemTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_OPPORTUNITYLINEITEMTRIGGER)) {
    
		  //----------------------------------------------------------------------------------------------
		  //  Copy Location Account field from parent Opportunity T-281345
		  //----------------------------------------------------------------------------------------------
		  if(Trigger.isBefore && trigger.isInsert){
		  	System.debug('@@@abc');
		    FF_OpportunityLineItemTriggerHandler.copyLocationAccount(trigger.new);
		    System.debug('@@@xyz');
		  }
		
		  //----------------------------------------------------------------------------------------------
		  //  Populate PSR Contact
		  //----------------------------------------------------------------------------------------------
		  if(Trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
		  	System.debug('@@@abc');
		    FF_OpportunityLineItemTriggerHandler.populatePSRContact(trigger.oldMap, trigger.new);
		    System.debug('@@@xyz');
		  }
		  
  } //end Story# S-415285
  
}