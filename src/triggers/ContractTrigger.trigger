// (c) 2015 Appirio, Inc.
//
// Trigger Name: ContractTrigger
//
// February 25 2016, Prakarsh Jain  Original (T-479823)
//
trigger ContractTrigger on Contract (after insert,after update) {
  //trigger to update Reminder Date field on Contract Object
   // Only run this trigger if the SPS_AccountTrigger Custom Setting is 'true'
   //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_CONTRACTTRIGGER)) {
  	  if(Trigger.isAfter){
	    Instruments_ContractTriggerHandler.updateReminderDateField(trigger.oldmap, trigger.new); 
	  }
  } 
}