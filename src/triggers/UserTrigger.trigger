/*
Created by Najma Ateeq for Story #S-408891
Purpose - Trigger on User object

* 22 Nov   2016      Nitish Bansal     I-244792 - Empower prod sync
*/
trigger UserTrigger on User (after insert, after update, before insert, before update) {

      // Only run this trigger if the UserTrigger Custom Setting is 'true'
      //Added By Harendra for Story# S-415285
      if(TriggerState.isActive(Constants.TRIGGER_USERTRIGGER)) {
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
            UserTriggerHandler.beforeInsertUpdateHandler(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
        }

        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            UserTriggerHandler.afterInsertUpdateHandler(Trigger.new, Trigger.oldMap, Trigger.isUpdate);
            
            /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - Start ***/
            //Code moved from Endo_UserTrigger to this current trigger
        // Don't change the order of calling below functions, we are using the Sales Rep in top35 method.
           // Endo_UserTriggerHandler.alignSalesSupportAgentOnCases(Trigger.new, Trigger.oldMap);
           // Endo_UserTriggerHandler.changeAGWithStatus(Trigger.new, Trigger.oldMap); // I-236931 | Pratibha(Endo)
            UserTriggerHandler.onAfterChanges(Trigger.new, Trigger.oldMap); // I-236931 | Pratibha(Endo)
            
            Instruments_UserTriggerHandler.addToFlexChatterGroup(Trigger.new, Trigger.oldMap);
            //Method to restrict duplication of Division value for a user
            //Prakarsh Jain    29 March 2016     (I-208788)
            Instruments_UserTriggerHandler.restrictDuplicateDivision(Trigger.new, Trigger.oldMap);
            
            Intr_UserTriggerHandler.addProcareRep(Trigger.new, Trigger.oldMap);
            /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - End ***/
        }
    //end Story# S-415285
        if(Trigger.isBefore && Trigger.isInsert) {
          COMM_UserTriggerHandler.beforeInsert(Trigger.new);
        }
        if(Trigger.isBefore && Trigger.isUpdate) {
          COMM_UserTriggerHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
        }
      } 
      
  /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - Start ***/
  //Code moved from Endo_UserTrigger to this current trigger
  if(Trigger.isBefore && Trigger.isUpdate){
       //Method to restrict users to edit user records if user is of Intruments Profile
       //Prakarsh Jain    26 August 2016     (I-227715)
       //Commented the code as per I-241940. Method no longer needed. Functionality being controlled by validation rule. 
       //Instruments_UserTriggerHandler.restrictUsersToEditRecords(Trigger.new, Trigger.newMap, Trigger.oldMap);   
  } 
  /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - End ***/
  

}