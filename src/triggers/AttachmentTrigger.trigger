// (c) 2016 Appirio, Inc.
//
// Description - Trigger for Attachment (created for the Comm functionality)
//
// 02/26/2016   Nitish Bansal Reference : T-459651
// 03/29/2016   Deepti Maheshwari Reference : T-485167
// 03/31/2016   Deepti Maheshwari Updated class name (Prefixed with COMM_)
// 04/12/2016   Noopur renamed the trigger (removed COMM_)
//
trigger AttachmentTrigger on Attachment (after insert, before delete) {
    //Checking if trigger is active
    if(TriggerState.isActive(Constants.ATTACHMENT_TRIGGER)){
        //Creating and initilizing trigger handler instance     
        if(trigger.isBefore){
            if(trigger.isDelete){
            COMM_AttachmentTriggerHandler.beforeRecordDelete(Trigger.old);
          }     
        } else if(trigger.isAfter){
            if(trigger.isInsert){
            COMM_AttachmentTriggerHandler.afterRecordInsert(Trigger.new);
          }     
        }  
    }
}