//
// (c) 2015 Appirio, Inc.
//
// Trigger Name: Flex_LeadTrigger
// On SObject: Lead
// Description: Trigger to Set Lead Owner based on Region
// 3rd July 2015    Ravindra Shekhawat   Original (Task # S-326050)
//
trigger Flex_LeadTrigger on Lead (after insert, after update) {
 
 // Only run this trigger if the Flex_LeadTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_FLEX_LEADTRIGGER)) {
	  //  Call After Update
	  if(Trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
	    Flex_LeadTriggerHandler.assignLeadsToRFM(trigger.new, trigger.oldMap);
	  }
  } //end Story# S-415285
}