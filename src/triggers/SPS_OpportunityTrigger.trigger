/***************************************************************************
Created By: Gagan Brar
Date: June 27, 2014
Description:
****************************************************************************/

trigger SPS_OpportunityTrigger on Opportunity (after insert, after update, before update) {
    //----------------------------------------------------------------------------------------------
    //  Populate SPS Program and Program Products objects.
    //---------------------------------------------------------------------------------------------- 
    
 // Only run this trigger if the SPS_AccountTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_SPS_OPPORTUNITYTRIGGER)) {
    
      if(trigger.isAfter){
            if (trigger.isUpdate){
                SPS_OpportunityTriggerHandler.populateSPSprograms(trigger.oldMap, trigger.newMap, 'isUpdate');
                System.debug('===trigger.newMap==='+trigger.newMap);
            }
            if (trigger.isInsert){
                SPS_OpportunityTriggerHandler.populateSPSprograms(trigger.oldMap, trigger.newMap, 'isInsert');
                System.debug('===trigger.newMap==='+trigger.newMap);
            }
            System.debug('===SPS_OpportunityTrigger===');
        }
    } //end Story# S-415285
}