/*************************************************************************************************
Created By   :  Rahul Aeran
Date         :  05/03/2016
Description  :  OpportunityTeamMemberTrigger 
Purpose      :  I-208731, created to give access to opportunity team member for cases
**************************************************************************************************/
trigger OpportunityTeamMemberTrigger on OpportunityTeamMember (after insert, after update,after delete) {
    //Checking if trigger is active
    if(TriggerState.isActive(Constants.OPPORTUNITY_TEAM_MEMBER_TRIGGER)) {
      if(trigger.isBefore) {
          
      }else if(trigger.isAfter) {
          if(trigger.isInsert) {
            OpportunityTeamMemberTriggerHandler.onAfterInsert(Trigger.new);
          } else if(trigger.isUpdate){
            OpportunityTeamMemberTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap); 
          } else if(trigger.isDelete){
            OpportunityTeamMemberTriggerHandler.onAfterDelete(Trigger.old);
          }
      }  
    }
}