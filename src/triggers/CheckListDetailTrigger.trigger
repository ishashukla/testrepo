/**================================================================      
* Appirio, Inc
* Name: CheckListDetailTrigger
* Description: S-440222
* Created Date: 09 Sep 2016
* Created By: Shubham Dhupar (Appirio)
==================================================================*/
trigger CheckListDetailTrigger on CheckList_Detail__c (after update) {
  if(TriggerState.isActive('CheckListDetailTrigger')){
    if(Trigger.isAfter && Trigger.isUpdate) {
      COMM_CheckListDetailTriggerHandler.afterUpdate(trigger.new);
    } 
  }
}