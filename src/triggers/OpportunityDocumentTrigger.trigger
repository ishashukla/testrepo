// (c) 2016 Appirio, Inc.
// Name - OpportunityDocumentTrigger
// Description - Trigger for Opportunity Document 
//
// 02/25/2016   Nitish Bansal Reference : T-459652
// 03/30/2016   Deepti Maheshwari Reference : T-487784
// 04/12/2016   Noopur renamed the trigger (removed COMM_)
// 18th April 2016  Kirti Agarwal  : I-212882 :  populate the PE user on Oppty Document
// 20th April 2016  Rahul Aeran    : I-213997 - Added the before update function to validate document upload for closed opportunity 
// 21th April 2016  Kirti Agarwal  : T-496412 - populate the PE user on Oppty Document
//
trigger OpportunityDocumentTrigger on Document__c(before insert, before delete,before update, after insert,
  after delete, after update) {
  //Checking if trigger is active
  if (TriggerState.isActive(Constants.OPPTY_DOCUMENT_TRIGGER)) {
  
    
    if (trigger.isBefore) {
      if (trigger.isDelete) {
        COMM_OpptyDocumentTriggerHandler.beforeRecordDelete(Trigger.old);
      }
      
      //T-496412 - populate the PE user on Oppty Document
      //get it from the Oppty Team where the role = Project Engineer. 
      if (trigger.isInsert) {
        COMM_OpptyDocumentTriggerHandler.beforeRecordInsert(Trigger.new);
      }
      if(trigger.isUpdate){
          COMM_OpptyDocumentTriggerHandler.beforeRecordUpdate(Trigger.new,Trigger.oldMap); 
      }
    }
    
    // 03/30/2016   Deepti Maheshwari Reference : T-487784
    if (trigger.isAfter) {
      if (trigger.isInsert) {
        COMM_OpptyDocumentTriggerHandler.afterInsert(Trigger.new);
      }

      if (trigger.isUpdate) {
        COMM_OpptyDocumentTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
      }

      if (trigger.isDelete) {
        COMM_OpptyDocumentTriggerHandler.afterDelete(Trigger.old);
      }
    }
  }
}