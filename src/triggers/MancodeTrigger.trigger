/**================================================================      
* Appirio, Inc
* Name: MancodeTriggerHandler 
* Description: Trigger for Mancode Object
* Created Date: 18 July,2016
* Created By: Prakarsh Jain (Appirio)
* 
* Date Modified      Modified By      Description of the update
* 18 July 2016        Prakarsh Jain         Class creation.
* 24 Aug  2016       Isha Shukla          Modified (T-525938) Added a call to method updateOppSharing
*October 23,2016	 Sahil Batra		T-549184 - Added code to populate Mancode record Name - populateMancodeName
==================================================================*/
trigger MancodeTrigger on Mancode__c (after insert, after update) {
//Method to restrict user to create mancode record if business unit and sales rep are same and manager is different for newly created records.
//  MancodeTriggerHandler.restrictDuplicateMancodeRecord(trigger.new, trigger.oldMap);


//Method to update Opportunity BU Manager field and Share Opportunity on insert and update of Mancode records.
	if(Trigger.isAfter){
		MancodeTriggerHandler.updateOppSharing(trigger.new, trigger.oldMap); 
	}
/*	if(Trigger.isBefore){
		MancodeTriggerHandler.populateMancodeName(trigger.new, trigger.oldMap);
	} */
}