trigger InstallBaseTrigger on Install_Base__c (before delete, before update,after insert) {
  //Method to restrict users to delete or edit records if they are not the owner
  //Prakarsh Jain 13 April 2016 - UAT Issues sheet shared by Devin
  // Only run this trigger if the InstallBaseTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_INSTALLBASETRIGGER)) {
	  if(Trigger.isBefore && Trigger.isUpdate){
	    Instruments_InstallBaseTriggerHandler.restrictEditDeleteOfRecords(Trigger.new, Trigger.oldMap, false);
	  }
	  if(Trigger.isBefore && Trigger.isDelete){
	    Instruments_InstallBaseTriggerHandler.restrictEditDeleteOfRecords(Trigger.new, Trigger.oldMap, true);
	  }
	  if(Trigger.isAfter && Trigger.isInsert) {
	        Instruments_InstallBaseTriggerHandler.shareInstallBaseRecords(Trigger.new,Trigger.oldMap);
	  }
  }//end Story# S-415285
}