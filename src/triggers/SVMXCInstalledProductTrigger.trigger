// (c) 2016 Appirio, Inc.
//
// Trigger Name: SVMXCInstalledProductTrigger - Trigge for SVMXC__Installed_Product__c Custom Object
//
// 04/28/2016, Shubham Dhupar 	 (T-490747)
// 05/09/2016, Rahul Aeran       (T-501084) update

trigger SVMXCInstalledProductTrigger  on SVMXC__Installed_Product__c  (after insert,after update,before insert, before update) {
  if(TriggerState.isActive('SVMXCInstalledProductTrigger')) {
    if(Trigger.isAfter && Trigger.isInsert) {
      COMM_InstalledProductTriggerHandler.AfterInsert(Trigger.new); 
      }
    if(Trigger.isAfter && Trigger.isUpdate) {
        COMM_InstalledProductTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap); 
      }
    if(Trigger.isBefore && Trigger.isInsert) {
      COMM_InstalledProductTriggerHandler.onBeforeInsert(Trigger.new); 
    }
    if(Trigger.isBefore && Trigger.isUpdate) {
      COMM_InstalledProductTriggerHandler.onBeforeUpdate(Trigger.new,Trigger.oldMap); 
    }  
  }
}