/*************************************************************************************************
Created By:    Sunil Gupta
Date:          May 28, 2014
Description  : Opportunity Trigger
Sahil Batra     Feb 19,2016 Modified (T-477799) - Added call to method updatePricebook to populate Pricebook Id
Sahil Batra     Mar 07,2016 Modified (T-483180,T-482629,T-482640) - Added call to method updateOpportunityTeamMembers,populateParentOpportunityQuoteFields
Isha Shukla     Jul 19,2016 Modified (T-520730) - Added call to method setInstrumentsOwnerAsOppTeamMember
Shreerath Nair: Aug 12,2016 Modified (I-229838) - Updated function CalculateOpportunityNumber calling
Prakarsh Jain   Sept 19, 2016 Modified(T-538449) - Added call to method addASRUserToOppTeam()
Shreerath Nair  Nov 02,2016 Modified (T-552902) - Added call of method restrictUpdateOfOpportunityAmount

* 22 Nov   2016      Nitish Bansal     I-244792 - Empower prod sync
**************************************************************************************************/
trigger OpportunityTrigger on Opportunity(after update, before update, before insert, after insert,before delete) {
 // Only run this trigger if the SPS_AccountTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_OPPORTUNITYTRIGGER)) {
   system.debug('Executing trigger : ' + trigger.isInsert + ':::' + trigger.isUpdate);
   //I-229838 (SN) Skip other functions when Update query is fired Inside CalculateOpporunityNumber Function
  if (FF_OpportunityTriggerHandler.skipOppTrigger) {
      return;
  }
  //----------------------------------------------------------------------------------------------
  //  Update Flex Credit Record When Opportunity Amount exceeds.
  //----------------------------------------------------------------------------------------------
  if(Trigger.isBefore && trigger.isUpdate){
    FF_OpportunityTriggerHandler.updateFlexCreditRecord(trigger.oldMap, trigger.newMap);
    FF_OpportunityTriggerHandler.updatePendingPODuration(trigger.oldMap, trigger.newMap);
  }

  //----------------------------------------------------------------------------------------------
  //  Populate Flex Contract and Contract Line Item objects.
  //----------------------------------------------------------------------------------------------
  if(Trigger.isAfter && trigger.isUpdate){
    FF_OpportunityTriggerHandler.populateFlexContracts(trigger.oldMap, trigger.newMap);
  }

  //----------------------------------------------------------------------------------------------
  //  Insert Flex Credit Record.
  //----------------------------------------------------------------------------------------------
  if(Trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
    FF_OpportunityTriggerHandler.insertFlexCreditRecord(trigger.oldMap, trigger.newMap);
    
  }

  //----------------------------------------------------------------------------------------------
  //  Insert Flex Notes Record.
  //----------------------------------------------------------------------------------------------
  if(Trigger.isAfter && trigger.isInsert){
    FF_OpportunityTriggerHandler.insertFlexNotesRecord(trigger.newMap);
    Instruments_OpportunityTriggerHandler.shareOpportunityWithMultiRepManager(trigger.new);
    Instruments_OpportunityTriggerHandler.setInstrumentsOwnerAsOppTeamMember(trigger.new);
    //Prakarsh Jain   19 Sep, 2016  Modified (T-538449) - Method adds the ASR User to Opportunity Team Member with Read/Write Access on Opportunity.
    Instruments_OpportunityTriggerHandler.addASRUserToOppTeam(trigger.new);
  }

  //----------------------------------------------------------------------------------------------
  //  Validate Opportunity stage chnages 
  //----------------------------------------------------------------------------------------------
  if(Trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){
    FF_OpportunityTriggerHandler.validateOppStage(trigger.oldMap, trigger.newMap, trigger.new);
    Instruments_OpportunityTriggerHandler.populateandValidateBusinessUnitField(trigger.oldMap,trigger.new);
    FF_OpportunityTriggerHandler.updateFR_email(trigger.new, trigger.oldMap, Trigger.isInsert); // Added for story #S-402777
  }
  
  
  //----------------------------------------------------------------------------------------------
  //  Calculate Opportunity Number on Opportunity
  //----------------------------------------------------------------------------------------------
  if(Trigger.isBefore && trigger.isInsert){
    //FF_OpportunityTriggerHandler.calculateOpportunityNumber(trigger.new);
    Instruments_OpportunityTriggerHandler.resetSubmittedForForecastButton(trigger.new);
    Instruments_OpportunityTriggerHandler.populateParentOpportunityQuoteFields(trigger.new);
  }


  //----------------------------------------------------------------------------------------------
  //  Calculate Schedule Number on Opportunity,  Prod Sync Mehul Jain
  //----------------------------------------------------------------------------------------------
  if(Trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){
    FF_OpportunityTriggerHandler.calculateScheduleNumber(trigger.oldMap, trigger.new);
      
    //Prakarsh Jain   June 16, 2016   T-512007 Changes start
    //Prakarsh Jain   July 20, 2016   I-226475 Changes start
    Instruments_OpportunityTriggerHandler.setOppForecastCategoryName(trigger.oldMap,trigger.new);
    //I-226475 Changes End
    //T-512007  Changes End    
  }

  //----------------------------------------------------------------------------------------------
  //  Validate Opportunity stage chnages
  //----------------------------------------------------------------------------------------------
  if(Trigger.isBefore && trigger.isUpdate){
    FF_OpportunityTriggerHandler.validateIfCloseDateExceeds(trigger.oldMap, trigger.newMap);
      COMM_OpportunityTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap);
    //Prakarsh Jain   June 16, 2016   T-512007 Changes start
    Instruments_OpportunityTriggerHandler.setOppForecastCategoryName(trigger.oldMap,trigger.new);
    //T-512007  Changes End
  }
  
  //---------------------------------------------------------------------------------------------
  // Merged from the COMM_OpportunityTrigger
  //---------------------------------------------------------------------------------------------
   
    if(trigger.isInsert && Trigger.isBefore) {
        system.debug('Calling before insert');
      COMM_OpportunityTriggerHandler.beforeInsert((List<Opportunity>)trigger.new);
    }else if(trigger.isUpdate && Trigger.isBefore) {
        system.debug('Calling before update');
      //COMM_OpportunityTriggerHandler.beforeUpdate((List<Opportunity>)trigger.new,
      //                                  (Map<ID, Opportunity>) trigger.oldMap);
      
    }else if(trigger.isUpdate && Trigger.isAfter) {
      COMM_OpportunityTriggerHandler.AfterUpdate((List<Opportunity>)trigger.new,
                                        (Map<ID, Opportunity>) trigger.oldMap); 
    }
   
  
   // MJ - Empower  main release
  
  //----------------------------------------------------------------------------------------------
  //  Calculate Total Forecast Amount and update Monthly forecast records for Owner
  //----------------------------------------------------------------------------------------------
   if(Trigger.isAfter){
        Instruments_OpportunityTriggerHandler.updateMonthlyOpportunityForecast(trigger.oldMap,trigger.new);
        // Changes as part of T-477799 start
        Instruments_OpportunityTriggerHandler.updatePricebook(trigger.oldMap,trigger.new);
        // Changes as part of T-477799 End
        Instruments_OpportunityTriggerHandler.updateOpportunityTeamMembers(trigger.oldMap,trigger.new);
        if(trigger.isUpdate){
          Instruments_OpportunityTriggerHandler.keepOldOpportunityTeamMember(trigger.new, trigger.oldMap);
          Instruments_OpportunityTriggerHandler.updateSharingonOpportunity(trigger.oldMap,trigger.new);
        }
   }

   if(Trigger.isBefore && trigger.isDelete){
      Instruments_OpportunityTriggerHandler.updateMonthlyForecastOnDelete(trigger.oldMap);
   }


  //----------------------------------------------------------------------------------------------
  //T-522295(SN)  Create Scheduled Revenue for Closed Won Opportunity 
  //----------------------------------------------------------------------------------------------
   if(Trigger.isBefore && trigger.isUpdate){
    Intr_OpportunityTriggerHandler.handleOpportunityForScheduledRevenue(trigger.oldMap, trigger.new);
    //T-542917(SN) - function to restrict the updation of Opportunity Amount  if Oracle Quote is Associated with it.
    Instruments_OpportunityTriggerHandler.restrictUpdateOfOpportunityAmount(trigger.new,Trigger.oldMap);
  }
   
   
   //I-229838 (SN) Updated the CalculateOpportunity Number funciton
   if(Trigger.isAfter && Trigger.isInsert){
    //function to calculate the Opportunity Number of the newly created opportunity.
    FF_OpportunityTriggerHandler.calculateOpportunityNumber(trigger.new,0,Integer.ValueOf(Label.Intr_Max_Retry));
    COMM_OpportunityTriggerHandler.afterInsert(trigger.new);  //SD 9/26: Ref: I-236394
   }
   

 } 
}