/*************************************************************************************************
Created By:    Prakarsh Jain
Date:          Sept 28, 2015
Description  : Contact Trigger
**************************************************************************************************/
trigger ContactTrigger on Contact (before delete, before update, before insert, after insert, after update) {
  
  // Only run this trigger if the ContactTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_CONTACTTRIGGER)) {
	    if(Trigger.isBefore && Trigger.isDelete){
	        ContactTriggerHandler.preventDeleteOfMedEmployee(Trigger.old);
	    }
	    
	    //Added By Harendra for story# S-415286 at 13-6-2016
	    if(Trigger.isBefore && Trigger.isInsert ){
        NV_ContactTriggerHandler.updateParentAccount(trigger.new, trigger.oldMap);
      }
	    
	    if(Trigger.isBefore && (Trigger.isInsert || trigger.isUpdate)){
        Endo_ContactTriggerHandler.UpdateMailingAddress(trigger.new, trigger.oldMap);
        Endo_ContactTriggerHandler.populateSalesSupportAgent(trigger.new, trigger.oldMap);
      } //S-415286 - end 
      
	    //----------------------------------------------------------------------------------------------
	  //  Insert record in contact account junction object.
	  //---------------------------------------------------------------------------------------------- 
	  
	  if(Trigger.isAfter && (Trigger.isInsert || trigger.isUpdate)){
	    ContactTriggerHandler.InsertContactAccountAssociation(trigger.new, trigger.oldMap);
	    //Added By Harendra for story# S-415286 at 13-6-2016
	    Endo_ContactTriggerHandler.InsertContactAccountAssociation(trigger.new, trigger.oldMap);
	    //S-415286 - end 
	    }
  } //end Story# S-415285
}