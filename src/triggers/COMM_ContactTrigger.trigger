// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_ContactTriggerHandler - Trigger for Contact
//
// 02/26/2016, Deepti Maheshwari (T-477658)
// 04/11/2016, Noopur - Commented the code and merged it into the Endo_ContactTrigger
//
trigger COMM_ContactTrigger on Contact (after insert, after update, before update) {     
	if(TriggerState.isActive(Constants.COMM_CONTACTTRIGGER)) {
        if(trigger.isInsert && trigger.isBefore) {
          COMM_ContactTriggerHandler.beforeInsert((List<Contact>)trigger.new);
        }
        if(trigger.isInsert && trigger.isAfter) {
          COMM_ContactTriggerHandler.afterInsert((List<Contact>)trigger.new);
        }
        if(trigger.isUpdate && trigger.isAfter) {
          COMM_ContactTriggerHandler.afterUpdate((List<Contact>)trigger.new, 
                                              (Map<Id, Contact>)trigger.oldMap);
        }
        if(trigger.isUpdate && trigger.isBefore) {
          COMM_ContactTriggerHandler.beforeUpdate((List<Contact>)trigger.new, 
                                              (Map<Id, Contact>)trigger.oldMap);
        }
	}
}