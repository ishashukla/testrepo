/*
Name                    - ServiceContractLineItemTrigger
Updated By              - Rahul Aeran
Updated Date            - 06/15/2016
Purpose                 - T-510094

*/
trigger ServiceContractLineItemTrigger on SVMXC__Service_Contract_Products__c (after insert) {
  //Checking if trigger is active
  if(TriggerState.isActive(Constants.SERVICE_CONTRACT_LINEITEM_TRIGGER)){
    if(Trigger.isAfter && Trigger.isInsert){
      ServiceContractLineItemTriggerHandler.onAfterInsert(Trigger.new); 
    }
  }
}