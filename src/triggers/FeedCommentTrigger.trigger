/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Dec 12, 2013
 Purpose       :  Trigger on Chatter Post Reply records
 Reference     :  Task T-217899
*******************************************************************************/
trigger FeedCommentTrigger on FeedComment (after insert) {
  
 // Only run this trigger if the FeedCommentTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_FEEDCOMMENTTRIGGER)) {
	  if(trigger.isAfter && trigger.isInsert){
	    FeedCommentTriggerHandler.onAfterInsert(trigger.new);
	  }
  } //end Story# S-415285
}