//
// 2016 Appirio Inc.
//
// Created By       :     Mehul Jain
// Created Date     :     05/25/2016
// Description      :     COMM_EquipmentTrigger on Equipment__c Custom Object

trigger COMM_EquipmentTrigger on Equipment__c (before update) {
  
  if(trigger.isBefore && trigger.isUpdate ){
    COMM_EquipmentTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
  }
 
}