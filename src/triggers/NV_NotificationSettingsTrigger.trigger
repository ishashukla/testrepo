trigger NV_NotificationSettingsTrigger on Notification_Settings__c (before insert, before update) {
  
 // Only run this trigger if the NV_NotificationSettingsTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
 if(TriggerState.isActive(Constants.TRIGGER_NV_NOTIFICATIONSETTINGSTRIGGER)) {
		if(Trigger.isBefore){
			if(Trigger.isInsert || Trigger.isUpdate){
				NV_NotificationSettingsTriggerHandler.validateUser(Trigger.oldMap, Trigger.new);
			}
		}
 }	//end Story# S-415285
}