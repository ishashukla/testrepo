// 
// (c) 2016 Appirio, Inc.
//
// Name : PricingContractMemberTrigger 
// 
// 3rd May 2016     Original     T-499313
// Used to populate Account.Primary_Contract field 
//
trigger PricingContractMemberTrigger on Pricing_Contract_Account__c (after insert, after update) {
  
  COMM_PricingContractMemberTriggerHandler handlerObj = new COMM_PricingContractMemberTriggerHandler();
  if (Trigger.isAfter) {
    handlerObj.afterInsertAndUpdate(Trigger.new, Trigger.oldMap);
  }
}