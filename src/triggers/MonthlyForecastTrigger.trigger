trigger MonthlyForecastTrigger on Monthly_Forecast__c (after update) {
	Map<String,Boolean> customSettingMap = new Map<String,Boolean>();
	List<TriggerRunSetting__c> triggerRunList = TriggerRunSetting__c.getall().values();
	for(TriggerRunSetting__c triggerValue : triggerRunList){
	  if(!customSettingMap.containsKey(triggerValue.Name)){
	    customSettingMap.put(triggerValue.Name, triggerValue.Trigger_Run__c);
	  }
	}
	
  if(customSettingMap.size()>0 && customSettingMap.containsKey('MonthlyForecastTrigger') && customSettingMap.get('MonthlyForecastTrigger')){
	  MonthlyForecastTriggerHandler.updateOpportunityOnUnlock(trigger.oldMAp,trigger.new);
	  if(Constants.runUpdateTrigger){
	      MonthlyForecastTriggerHandler.updateManagerForecastMonthlyRecord(trigger.oldMAp,trigger.new);
	  }
	}
}