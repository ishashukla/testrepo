/**================================================================      
* Appirio, Inc
* Name: AddressTrigger
* Description: T-482844
* Created Date: 11 Apr 2016
* Created By: Rahul Aeran (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/

trigger AddressTrigger on Address__c (after update, after insert) {
    //Checking if trigger is active
    if(TriggerState.isActive(Constants.ADDRESS_TRIGGER)){
        if(trigger.isAfter){
            if(trigger.isInsert){
                AddressTriggerHandler.onAfterInsert(Trigger.new);
            }
            else if(trigger.isUpdate){
                AddressTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap);
            }
        }
    } 
}