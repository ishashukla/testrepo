trigger CheckListItemTrigger on Checklist_item__c (before Insert, after update ) {
  if(Trigger.isBefore && Trigger.isInsert) {
    COMM_CheckListItemTriggerHandler.beforeInsert(trigger.new);
  } 
  if(Trigger.isAfter && Trigger.isUpdate) {
    COMM_CheckListItemTriggerHandler.afterupdate(trigger.new,trigger.oldMap);
  } 
}