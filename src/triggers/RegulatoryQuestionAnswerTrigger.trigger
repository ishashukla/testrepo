/*************************************************************************************************
Created By:    Sunil Gupta
Date:          March 10, 2016
Description  : Trigger on Regulatory Question Answer

// Revision History:

**************************************************************************************************/


/*************************************************************************************************

* This method is validating that only one RQA record can be associated with one Case.

* Scope of RQA object is limited to Sherlock and no more functionality in trigger so avoided to use trigger handler.

**************************************************************************************************/
trigger RegulatoryQuestionAnswerTrigger on Regulatory_Question_Answer__c (before insert) {
  
 // Only run this trigger if the SPS_AccountTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_REGULATORYQUESTIONANSWERTRIGGER)) {
  
	  Set<Id> caseIds = new Set<Id>();
	  for(Regulatory_Question_Answer__c obj :Trigger.New){
	    caseIds.add(obj.Case__c);
	  }
	  
	  Set<Id> existingCaseIds = new Set<Id>();
	  for(Regulatory_Question_Answer__c objEx :[SELECT Case__c FROM Regulatory_Question_Answer__c WHERE Case__c IN :caseIds]){
	    existingCaseIds.add(objEx.Case__c); 
	  }
	  
	  for(Regulatory_Question_Answer__c obj :Trigger.New){
	    if(existingCaseIds.contains(obj.Case__c)){
	      // add error
	      obj.addError('Regulatory form already exist on this Case record.');
	    } 
	  }
	  if(Trigger.isinsert && Trigger.isbefore)
	  {
	      RegulatoryQuestionAnswerTriggerHandler.OnBeforeInsert(Trigger.New);
	  }
	  
  } //end Story# S-415285
}