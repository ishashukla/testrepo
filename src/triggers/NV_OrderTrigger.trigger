/*************************************************************************************************
Created By:    Durgesh Dhoot
Date:          Aug 26, 2015
Description  : Order Trigger

// 7th April 2016     Kirti Agarwal      Original(T-489842)
// 09th May 2016     Rahul Aeran T-501084, update installed base project plan if the order project plan is updated
// 10 Aug,2016    Sahil Batra T-525742 , Added call to method createCaseOrderJunctionRecord
* 16th September 2016    Nitish Bansal      Update (I-235018) // Timeout error resolution and added query execution time calculation logic
**************************************************************************************************/
trigger NV_OrderTrigger on Order (before insert, before update, after insert, after update, after delete) {
  Static Datetime qryStart, qryEnd; 
  // Prod Sync Mehul Jain  
  if(TriggerState.isActive(Constants.TRIGGER_NV_ORDERTRIGGER)) {

     //T-492006 - Added by Kirti     
    if(Trigger.isAfter && Trigger.isInsert) {
      COMM_OrderTriggerHandler.afterInsertOrder(Trigger.new,Trigger.oldMap);
      //Wrote new method as above method had some logic involved too
      COMM_OrderTriggerHandler.onAfterInsert(Trigger.new);
      COMM_OrderTriggerHandler.afterInsertAndUpdateAndDelete(Trigger.new,Trigger.oldMap, false);
      COMM_OrderTriggerHandler.createCaseOrderJunctionRecord(Trigger.new);
      NV_OrderTriggerHandler.processOrdersAfterInsert(Trigger.new);
      system.debug('**** Gagan ***** OrderTriggerHandler after insert');
      OrderTriggerHandler.onAfterInsert(Trigger.new);
    }
    //T-501084 - Added by Rahul Aeran
    if(Trigger.isAfter && Trigger.isUpdate) {
      qryStart = datetime.now();
      COMM_OrderTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap);
      COMM_OrderTriggerHandler.afterInsertAndUpdateAndDelete(Trigger.new,Trigger.oldMap, false);
      COMM_OrderTriggerHandler.afterInsertOrder(Trigger.new,Trigger.oldMap);
      qryEnd = datetime.now();
      system.debug('COMM_OrderTriggerHandler after update execution time ' +  (qryEnd.getTime() - qryStart.getTime()) /1000);
      qryStart = datetime.now();
      NV_OrderTriggerHandler.processOrdersAfterUpdate(Trigger.oldMap, Trigger.newMap);
      qryEnd = datetime.now();
      system.debug('NV_OrderTriggerHandler after update execution time ' +  (qryEnd.getTime() - qryStart.getTime()) /1000);
      if(OrderTriggerHandler.isTriggerInProgress == null ||  !OrderTriggerHandler.isTriggerInProgress) {
        OrderTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
      }
    }
   
    //T-492006 - Added by Kirti
    if(Trigger.isBefore && Trigger.isInsert) {
      COMM_OrderTriggerHandler.beforeInsertOrder(Trigger.new);
      COMM_OrderTriggerHandler.beforeUpdateOrder(Trigger.new, Trigger.oldMap);
      //Used to populate email fields on comm order records
      COMM_OrderTriggerHandler.beforeInsertAndUpdate(Trigger.new, Trigger.oldMap);
      NV_OrderTriggerHandler.processOrdersBeforeInsertUpdate(null, Trigger.new);
      //I-244441: Added before insert trigger to update salesrep details
      OrderTriggerHandler.onBeforeInsert(Trigger.new);
    }
   
    //T-492006 - Added by Kirti
    if(Trigger.isbefore && Trigger.isUpdate) {
      qryStart = datetime.now();
      COMM_OrderTriggerHandler.beforeUpdateOrder(Trigger.new, Trigger.oldMap);
      //Used to populate email fields on comm order records
      COMM_OrderTriggerHandler.beforeInsertAndUpdate(Trigger.new, Trigger.oldMap);
      qryEnd = datetime.now();
      system.debug('COMM_OrderTriggerHandler before update execution time ' +  (qryEnd.getTime() - qryStart.getTime()) /1000);
      qryStart = datetime.now();
      NV_OrderTriggerHandler.processOrdersBeforeInsertUpdate(Trigger.oldMap, Trigger.new);
      qryEnd = datetime.now();
      system.debug('NV_OrderTriggerHandler before update execution time ' +  (qryEnd.getTime() - qryStart.getTime()) /1000);
      //I-244441: Added before update trigger to update salesrep details
      OrderTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
    }

    
    if(Trigger.isAfter && Trigger.isDelete){
      NV_OrderTriggerHandler.processOrdersAfterDelete(Trigger.old);
      COMM_OrderTriggerHandler.afterInsertAndUpdateAndDelete(Trigger.old,null, true);
    }
  }
}