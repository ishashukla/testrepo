/*
Name                    - TrialTrigger 
Updated By              - Rahul Aeran
Updated Date            - 05/31/2016
Purpose                 - T-506835, Set Opportunity to Null if Self relationship is
populated on the inserted/updated object
*/
trigger TrialTrigger on Trial__c (before update, before insert, after insert, before delete, after delete) {
   //Checking if trigger is active
  if(TriggerState.isActive(Constants.TRIAL_TRIGGER)){
    if(trigger.isBefore && trigger.isUpdate){
        COMM_TrialTriggerHandler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
      } else if(trigger.isBefore && trigger.isInsert){
        COMM_TrialTriggerHandler.onBeforeInsert(Trigger.new);
    }
    else if(trigger.isBefore && trigger.isInsert) {
      //TrialTriggerHandler.onAfterDelete(Trigger.old);
    }
    if(trigger.isAfter) {
      if(trigger.isInsert) {
        COMM_TrialTriggerHandler.onAfterInsert(Trigger.new);
      }
      if(trigger.isDelete) {
        COMM_TrialTriggerHandler.onAfterDelete(Trigger.old);
      } 
      
    }
  }
}