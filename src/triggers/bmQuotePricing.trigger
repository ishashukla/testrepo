/*************************************************************************************************
Created By:    Deepti Maheshwari
Date:          Feb 27, 2014
Description  : bmQuotePricing (Migrated from COMM Source)
**************************************************************************************************/
trigger bmQuotePricing on BigMachines__Quote__c (after insert, after update, after delete) {
    if(TriggerState.isActive(Constants.BMQUOTE_TRIGGER)) {
    if(trigger.isInsert || trigger.isUpdate){
       bmQuotePricingTriggerHandler.afterInsertOrUpdate();
    }    
    if(trigger.isDelete) {
       bmQuotePricingTriggerHandler.afterDelete();
    }
    }
}