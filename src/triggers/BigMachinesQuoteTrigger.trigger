// 
// (c) 2016 Appirio, Inc.
// Created this trigger to calculate Roll-Up Summary values on Opportunity and is related to T-426533
// 
//
// 22 Feb 2016    Meghna Vijay       Added for T-426533
// 3 March 2016   Deepti Maheshwari  Updated for T-473984
// 4 March 2016   Deepti Maheshwari  Updated for T-459649
// 9 march 2016   Meghna Vijay       Updated for PR-004449

trigger BigMachinesQuoteTrigger on BigMachines__Quote__c(after delete, after insert, after undelete, after update,
    before insert) {
    //Checking if trigger is active
    if(TriggerState.isActive('BigMachinesQuoteTrigger')) {
        //Checking trigger for After Operation
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
            //Operation for trigger Insert
                BigMachinesQuoteTriggerHandler.afterInsert(Trigger.new);
            }
            if(Trigger.isUpdate) {
            //Operation for trigger Update
                BigMachinesQuoteTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
            }  
            
            if(Trigger.isDelete) {
            //Operation for trigger Delete
                BigMachinesQuoteTriggerHandler.afterDelete(Trigger.old);
            }

            if(Trigger.isUnDelete) {
            //Operation for trigger UnDelete
                BigMachinesQuoteTriggerHandler.afterUnDelete(Trigger.new);
            }
        }
    } else if(Trigger.isBefore && Trigger.isInsert) {
        //Operation for trigger before Insert           
        BigMachinesQuoteTriggerHandler.beforeInsert(Trigger.new);
    }
}