/*************************************************************************************************
Created By:    Jitendra Kothari
Date:          March 20, 2014
Description  : PricingLineItem Trigger
**************************************************************************************************/
trigger Endo_PricingLineItemTrigger on Pricing_Contract_Line_Item__c (before insert, before update) {
  
  // Only run this trigger if the Endo_PricingLineItemTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_ENDO_PRICINGLINEITEMTRIGGER)) {
		if(Trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){ 
	    	Endo_PricingLineItemHandler.populateContractPrice(trigger.new, trigger.oldMap);
		}
  } //end Story# S-415285
}