/*************************************************************************************************
Created By:    Brandon Jones
Date:          Nov 03, 2015
Description  : Contact Trigger
**************************************************************************************************/
trigger NV_ContactTrigger on Contact (before insert) {
  
  // Only run this trigger if the NV_ContactTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_NV_CONTACTTRIGGER)) {
  
    //----------------------------------------------------------------------------------------------
    //  If NV_Contact, ensure that Contacts created on Child account flow up to the highest
    //  Account in the hierarchy.
    //---------------------------------------------------------------------------------------------- 
    if(Trigger.isBefore && Trigger.isInsert ){
        NV_ContactTriggerHandler.updateParentAccount(trigger.new, trigger.oldMap);
    }
  } //end Story# S-415285 
}