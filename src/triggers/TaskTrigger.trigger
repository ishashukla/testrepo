// (c) 2016 Appirio, Inc.
//
// Class Name: TashTrigger - Trigger on Task
//
// 03/25/2016, Deepti Maheshwari (T-487405)
//
// Modified By                  Modified Date             Purpose
// Meghna Vijay(I-215112)       May 4, 2016               To insert values in Task fields when COMM Flex Contract
//                                                        Expiration Work flow rule activates.
//Pankaj Kuamr 10/6/2016 I-238439


trigger TaskTrigger on Task (before insert, after insert,before delete, before update, after update) {
  //  Set Task Owner to RFM
  if(TriggerState.isActive(Constants.TRIGGER_TASKTRIGGER)) {
    if(Trigger.isBefore && Trigger.isInsert){
      FF_TaskTriggerHandler.setTaskOwner(Trigger.new);
    COMM_TaskTriggerHandler.beforeInsert(Trigger.new);


    //Kajal Jalan(T-508997) 06 June 2016
    //Method to update call reason field from call disposition field
    Intr_TaskTriggerHandler.UpdateCallReason(Trigger.new,null);
    }
    
    //Update for T-487405 starts
    if(Trigger.isAfter){
      if(Trigger.isInsert){
        COMM_TaskTriggerHandler.afterInsert((List<Task>)trigger.new);
      }
      if(Trigger.isUpdate){
        COMM_TaskTriggerHandler.afterUpdate((List<Task>)trigger.new, 
                                (Map<Id, Task>)trigger.oldMap);
      }
    }
    //Update for T-487405 ends  
    
    
    // MJ -Empower main release
    //Prakarsh Jain(S-388510) 17 Feb 2015
    //Method to post Chatter on Account/Contact/Opportunity if a task is created from any of the three object
    if(Trigger.isAfter && Trigger.isInsert){
      Instruments_TaskTriggerHandler.onInsert(Trigger.new);
    }
    //Method to restrict users to delete or edit records if they are not the owner
    //Prakarsh Jain 13 April 2016 - UAT Issues sheet shared by Devin
    if(Trigger.isBefore && Trigger.isUpdate){
      Instruments_TaskTriggerHandler.onUpdateDelete(Trigger.new, Trigger.oldMap, false);

       //Kajal Jalan(T-508997) 06 June 2016
       //Method to update call reason field from call disposition field
      Intr_TaskTriggerHandler.UpdateCallReason(Trigger.new,Trigger.oldMap);
 
    }
    if(Trigger.isBefore && Trigger.isDelete){
      Instruments_TaskTriggerHandler.onUpdateDelete(Trigger.new, Trigger.oldMap, true);
    }

   //I-227752 (SN) Aug 5,2016
   if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        //Check if Attachment exist on Task
        Intr_TaskTriggerHandler.checkAttachmentOnTask(Trigger.newMap);
    }
  }  
}