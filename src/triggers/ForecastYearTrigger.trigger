// 
// 
//
// Base Trail Survey - Main page's handler
//
// 13 Jan 2016     Tom Muse      Added
// 
// 
//
trigger ForecastYearTrigger on Forecast_Year__c (Before Insert,After Insert, After Update) {
    if( Trigger.isAfter && Trigger.isInsert ) {
        ForecastYearTrigger_Handler.afterInsert(Trigger.NewMap);
    }
  /*  if(Trigger.isBefore){
    	ForecastYearTrigger_Handler.updateManagerforForecastYear(Trigger.new);
    } */

}