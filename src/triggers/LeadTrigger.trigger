trigger LeadTrigger on Lead (before delete) {
  
 // Only run this trigger if the SPS_AccountTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
 if(TriggerState.isActive(Constants.TRIGGER_LEADTRIGGER)) {
    LeadTriggerHandler.preventDelete(Trigger.Old);
 } //end Story# S-415285
}