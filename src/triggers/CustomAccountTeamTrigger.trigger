// 
// (c) 2016 Appirio, Inc.
//
// Name : CustomAccountTeamTrigger  
// Used to assign permission for Intl Entity Account by inserting custom account team member
//
// 7th March 2016     Kirti Agarwal      Original(T-477520)
//
//Modified Date       Modified By       Purpose
//10th May 2016       Meghna Vijay      Added afterInsertUpdate method to populate project user lookups
//                                      when a record of custom account team is inserted(T-500924)    
// 02nd Jun 2016      Rahul Aeran       Updated to create sharing records for installer assignment RT (T-501829)
// 07 June 2016       Meghna Vijay      Removed extra After Insert and after Update and added the methods called through
//                                      it to onAfterInsert and onAfterUpdate method.(PR-005122)
// 14 June 2016       Shubham Dhupar    Used to populate User value when it matches with Sales Rep Id
trigger CustomAccountTeamTrigger on Custom_Account_Team__c (after insert, after update,after delete,before Insert, before update) {
  
    // Prod Sync Mehul Jain  
    if(TriggerState.isActive(Constants.TRIGGER_CUSTOMACCOUNTTEAMTRIGGER)) {
    
      if(Trigger.isAfter && Trigger.isDelete){
        CustomAccountTeamTriggerHandler.onAfterDelete(trigger.old); // Rahul Aeran T-501829
        
      }else if(Trigger.isAfter && Trigger.isUpdate){
        CustomAccountTeamTriggerHandler.onAfterUpdate(trigger.new, Trigger.oldMap); // Rahul Aeran T-501829
      }else if(Trigger.isAfter && Trigger.isInsert){
        CustomAccountTeamTriggerHandler.onAfterInsert(trigger.new); // Rahul Aeran T-501829
      }
      if(Trigger.isBefore && Trigger.isInsert){
        CustomAccountTeamTriggerHandler.beforeInsert(trigger.new); // Shubham Dhupar I-222184
      }
      if(Trigger.isBefore && Trigger.isUpdate){
        CustomAccountTeamTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap); // Shubham Dhupar I-222184
      }
    
    }  
}