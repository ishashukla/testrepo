// (c) 2015 Appirio, Inc.
//
// Trigger Name: BaseProductTrigger
//
// April 6 2016, Prakarsh Jain  Original 
// Tom Muse commented out this until development has completed 6/22/2016
// July 2 2016,  Isha Shukla   Modified (T-516161)
// July 7  2016, Isha Shukla    Modified(I-225271)
trigger BaseProductTrigger on Base_Product__c (after insert, before insert,after update,after delete,before delete) {
  //Method to populate product description field on Base product(Stryker Ipad Issues workbook)
  //Instruments_BaseProductTriggerHandler.populateProductDescriptionOnBaseProduct(trigger.new);
  
  if(trigger.isBefore && trigger.isInsert) {
  	Instruments_BaseProductTriggerHandler.populateQuoteOnBaseTrailAndOppBaseTrailToBaseProductBaseTrail(Trigger.New);
  }
  if(trigger.isAfter && (trigger.isUpdate || Trigger.isInsert)) {
  	Instruments_BaseProductTriggerHandler.rollUpMonthlyTotal(trigger.oldMap,trigger.new);
  }
  if(trigger.isBefore && trigger.isDelete) {
  	Instruments_BaseProductTriggerHandler.baseProductOnBeforeDelete(trigger.old);
  }
}