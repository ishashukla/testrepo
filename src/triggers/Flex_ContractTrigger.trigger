// (c) 2015 Appirio, Inc.
//
// Trigger Name: Flex_ContractTrigger
//
// March 02 2016, Prakarsh Jain  Original (T-481264)
//
trigger Flex_ContractTrigger on Flex_Contract__c (after delete, after insert, after update) {
  //Method to update checkboxes on Account as per the value of Master Agreement Type field on Flex Contract
  // Only run this trigger if the Flex_ContractTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
     if(TriggerState.isActive(Constants.TRIGGER_FLEX_CONTRACTTRIGGER)) {
          if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            FlexContractTriggerHandler.updateCheckBoxes(trigger.oldmap, trigger.new, false); 
          }
          // Added by Nishank for Story S-422734
          if(Trigger.isBefore && Trigger.isInsert){
            FlexContractTriggerHandler.PopulateTaxFields(trigger.new); 
          }
          // End of changes added by Nishank for Story S-422734
          //Method to update checkboxes on Account as per the value of Master Agreement Type field on Flex Contract
          if(Trigger.isAfter && Trigger.isDelete){
            FlexContractTriggerHandler.updateCheckBoxes(trigger.oldmap, trigger.old, true);
          }
   } //end Story# S-415285
}