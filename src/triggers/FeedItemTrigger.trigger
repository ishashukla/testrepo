/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Dec 12, 2013
 Purpose       :  Trigger on Feed Item records
 Reference     :  Task T-241113
*******************************************************************************/
trigger FeedItemTrigger on FeedItem (after delete) {
	
	// Only run this trigger if the FeedItemTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_FEEDITEMTRIGGER)) {
		if(trigger.isAfter && trigger.isDelete){
	    FeedItemTriggerHandler.onAfterDelete(trigger.old);
	  }
  } //end Story# S-415285
}