/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  Jan 27, 2014
 Purpose       :  Trigger on Order__c records
 Reference     :  Task T-237810
 
 Modified: Added conditino for Update
 06 Oct 2016   Parul Gupta   T-542805 (Endo is using Stardard Order now)
*******************************************************************************/

trigger Endo_OrderTrigger on Order__c (after insert, after update) {
    //System.debug('@@@@ ' + Trigger.new[0].Name);
    //System.debug('@@@isUpdate' + Trigger.isUpdate);
    //System.debug('@@@isTriggerInProgress' + Endo_OrderTriggerHandler.isTriggerInProgress);
    //Only run this trigger if the Endo_OrderTrigger Custom Setting is 'true'
    //Added By Harendra for Story# S-415285
    
    
    // 06 Oct 2016 T-542805 - Commented out since Endo is using Stardard Order now
    /*if(TriggerState.isActive(Constants.TRIGGER_ENDO_ORDERTRIGGER)) {
    
              if(Trigger.isAfter && Trigger.isInsert) {
                    Endo_OrderTriggerHandler.onAfterInsert(Trigger.new);
              }
               
              if(Trigger.isAfter && Trigger.isUpdate && (Endo_OrderTriggerHandler.isTriggerInProgress != true)) {
                System.debug('@@@After Trigger');
                Endo_OrderTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
              }
   }*/
}