trigger NV_FollowTrigger on NV_Follow__c (before insert,
                                            after insert,
                                            after delete) {
  
	 // Only run this trigger if the NV_FollowTrigger Custom Setting is 'true'
	 //Added By Harendra for Story# S-415285
	 if(TriggerState.isActive(Constants.TRIGGER_NV_FOLLOWTRIGGER)) {
		    if(Trigger.isBefore){
		        if(Trigger.isInsert){
		            NV_FollowTriggerHandler.processFollowBeforeInsert(Trigger.oldMap, Trigger.new);
		        }
		    }
		
		    if(Trigger.isAfter){
		        if(Trigger.isInsert){
		            NV_FollowTriggerHandler.processFollowAfterInsert(Trigger.oldMap, Trigger.new);
		        }
		        
		        //Uncommented by Jyoti for Story S-389745
		        else if(Trigger.isDelete){
		            NV_FollowTriggerHandler.processFollowAfterDelete(Trigger.old);
		        }
		        
		    }
	    
	 } //end Story# S-415285

}