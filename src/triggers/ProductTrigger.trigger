/**================================================================      
* Appirio, Inc
* Name: ProductTrigger
* Description: T-522767 (To create Price book entries on the COMM GIM+ products)
* Created Date: 07/27/2016
* Created By: Nitish Bansal (Appirio)
*
* 1st September 2016     Nitish Bansal      Update (I-233001)  //To create PBE on COMM products on update of records.
* 12th September 2016    Nitish Bansal      Update //To resolve the non-sequential bulk API load errors.
==================================================================*/

trigger ProductTrigger on Product2 (after insert, after update) {
//Checking if trigger is active
  if(TriggerState.isActive('ProductTrigger')){
    if(trigger.isAfter){
        if(trigger.isInsert || trigger.isUpdate){
            COMM_ProductTriggerHandler.onAfterInsertUpdate(Trigger.new);
        } 
    }    
  } 
}