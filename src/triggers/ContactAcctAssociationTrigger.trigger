/*************************************************************************************************
Created By:    Megha Agarwal
Date:          October 15, 2015
Description:   Contact Acct Association Trigger
11 May 2016    Partibha Chhimpa(Appirio)  Changes in onInsert = > afterInsert
**************************************************************************************************/
trigger ContactAcctAssociationTrigger on Contact_Acct_Association__c (before delete, after delete, after insert) {
  
  // Only run this trigger if the ContactAcctAssociationTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_CONTACTACCTASSOCIATIONTRIGGER)) {
      if(Trigger.isBefore && Trigger.isDelete){
        // Prevent users to delete Contact Account Association Junction object entry if Primary flag is true
        ContactAcctAssociationTriggerHandler.beforeDelete(Trigger.old);
      }
        if(Trigger.isAfter && Trigger.isInsert) {
            ContactAcctAssociationTriggerHandler.afterInsert(Trigger.New);
        }
        /*if(Trigger.isAfter && Trigger.isDelete) {
            ContactAcctAssociationTriggerHandler.onDelete(Trigger.Old);
        }*/
  }
}