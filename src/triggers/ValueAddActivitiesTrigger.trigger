/**================================================================      
 * Appirio, Inc
 * Name: ValueAddActivitiesTrigger
 * Description: Trigger for Value_Add_Activities__c object(T-538445)
 * Created Date: [09/20/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 * Oct 12, 2016       Shreerath Nair   Implemented new Trigger framework(T-542899)
 ==================================================================*/
trigger ValueAddActivitiesTrigger on Value_Add_Activities__c (before insert,before update,after insert, after update) {

    if(ValueAddActivitiesTriggerHandler.isActive()){
    
        ValueAddActivitiesTriggerHandler handler = new ValueAddActivitiesTriggerHandler();
    
        if(Trigger.isBefore){
        
            if(trigger.isInsert){             
                handler.beforeInsert(Trigger.new);               
            } 
            else if(trigger.isUpdate){
                handler.beforeUpdate(Trigger.new, Trigger.oldMap);              
            }
            else if(Trigger.isDelete){              
                 handler.beforeDelete(Trigger.old, Trigger.oldMap);
            }
            
        }    
        else if(trigger.isAfter){
            
            if(trigger.isInsert){
                
                handler.afterInsert(Trigger.new);
                //Call to Method which shares the Value Add Activity record with the ASR User
                //ValueAddActivitiesTriggerHandler.shareASRUser(Trigger.new, Trigger.oldMap);
            }
            else if(Trigger.isUpdate){
                
                handler.afterUpdate(Trigger.new, Trigger.oldMap);
                //Call to Method which shares the Value Add Activity record with the ASR User
                //ValueAddActivitiesTriggerHandler.shareASRUser(Trigger.new, Trigger.oldMap);    
            }
            else if(Trigger.isDelete){            
           
            }
            
        }      
  
    }
    /*if(trigger.isBefore && trigger.isInsert){
    //Method to populate Business unit field on Value Add Activities record if the sales rep user belongs to a single business unit in the mancode record
    ValueAddActivitiesTriggerHandler.updateBusinessUnitValue(Trigger.new);
    
  }*/
}