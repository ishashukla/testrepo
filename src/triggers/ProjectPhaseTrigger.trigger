// 
// (c) 2016 Appirio, Inc.
//
// Name : ProjectPhaseTrigger 
// Rolling up fields which was converted as part of MD to Lookup
//
// 02nd Jun 2016      Rahul Aeran      Created (T-501829) 
//MOdified By : Pankaj Kumar  T-520411

trigger ProjectPhaseTrigger on Project_Phase__c (after insert,after update, after delete,before update,before insert) {
    if(TriggerState.isActive(Constants.PHASE_TRIGGER)){
        if(Trigger.isAfter && Trigger.isDelete){
            ProjectPhaseTriggerHandler.onAfterDelete(trigger.old); // Rahul Aeran T-501829
        }
        else if(Trigger.isAfter && Trigger.isUpdate){
            ProjectPhaseTriggerHandler.onAfterUpdate(trigger.new, Trigger.oldMap); // Rahul Aeran T-501829
        }
        else if(Trigger.isAfter && Trigger.isInsert){
            ProjectPhaseTriggerHandler.onAfterInsert(trigger.new); // Rahul Aeran T-501829
        }
        else if(Trigger.isBefore && Trigger.isInsert){
            ProjectPhaseTriggerHandler.onBeforeInsert(trigger.new); // Pankaj Kumar  T-520411
        }
        else if(Trigger.isBefore && Trigger.isUpdate){
            ProjectPhaseTriggerHandler.onBeforeUpdate(trigger.new,Trigger.oldMap); // Pankaj Kumar  T-520411
        }
    }
}