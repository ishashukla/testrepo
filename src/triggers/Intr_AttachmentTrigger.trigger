trigger Intr_AttachmentTrigger on Attachment (after delete) {
	
	//I-227752 (SN) Aug 5,2016
	if(Trigger.isAfter &&  Trigger.isDelete){
		//Check if Attachment exist on Task
		Intr_AttachmentTriggerHandler.checkAttachmentOnTask(Trigger.Old);
	
	}
    
}