/**================================================================      
* Appirio, Inc
* Name: RMAShipmentOrderTrigger
* Description: Parts Order
* Created Date: July 08, 2016
* Created By: Jagdeep Juneja
* 
* Date Modified    Modified By             Description of the update
* 07/29/2016       Kanika Mathur           updated logic to populate No of RMAs on Case T-522190
==================================================================*/
trigger RMAShipmentOrderTrigger on SVMXC__RMA_Shipment_Order__c (before insert, before update, after insert, after update, after delete){
    RmaShipmentOrderTriggerHandler handler = new RmaShipmentOrderTriggerHandler();
    
    if(handler.isActive()){
        if(Trigger.isBefore && Trigger.isUpdate){
            handler.beforeUpdate(Trigger.New,Trigger.oldMap); 
        }
        
        if(Trigger.isBefore && Trigger.isInsert){
            handler.beforeInsert(Trigger.New);
        }
        
        if(Trigger.isAfter && Trigger.isInsert) {
            System.debug('<<<<<<<<<'+Trigger.new);
            handler.afterInsert(Trigger.new);
        }
        
        if(Trigger.isAfter && Trigger.isUpdate) {
            handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }

        if(Trigger.isAfter && Trigger.isDelete) {
            handler.afterDelete(Trigger.new, Trigger.oldMap);
        }
    }
}