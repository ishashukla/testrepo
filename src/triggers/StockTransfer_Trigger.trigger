/**================================================================      
* Appirio, Inc
* Name: StockTransfer_Trigger
* Description: Trigger on SVMXC__Stock_Transfer__c
* Created Date: 22-Aug-2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/
trigger StockTransfer_Trigger on SVMXC__Stock_Transfer__c (before insert, before update) {
    if(trigger.isUpdate && trigger.isbefore)
    {
        StockTransferTriggerHandler.OnBeforeUpdate(trigger.new);
    }
    if(trigger.isbefore && trigger.isInsert)
    {
        StockTransferTriggerHandler.OnBeforeInsert(trigger.new);
    }
}