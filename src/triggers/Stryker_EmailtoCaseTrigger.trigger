/*************************************************************************************************
Created By:    Gagandeep Brar (Stryket IS)
Date:          April 4th , 2016
Description  : Email to Case Trigger - after insert that updates case contact on a case with devision
                specific contact. For Example:
            If there is exactly one MEDICAL contact (employee or customer), a match should be made
            If there is a duplicate contact but a different type (FLEX, ENDO, etc), they should be 
            ignored because the case is being created from outlook by a MEDICAL user

April.04.2016; Gagan; Trigger created with logic for Endo and Medical
**************************************************************************************************/
trigger Stryker_EmailtoCaseTrigger on EmailMessage (after insert) {
  
 // Only run this trigger if the Stryker_EmailtoCaseTrigger Custom Setting is 'true'
 //Added By Harendra for Story# S-415285
    if(TriggerState.isActive(Constants.TRIGGER_STRYKER_EMAILTOCASETRIGGER)) {
    
      //----------------------------------------------------------------------------------------------
      //  Update Contact on new cases created vai EmailtoCase
      //----------------------------------------------------------------------------------------------
      if(Trigger.isAfter && Trigger.isInsert){
        Stryker_EmailtoCaseTriggerHandler.onAfterInsert(Trigger.new);
      }
  
  } //end Story# S-415285
    
}