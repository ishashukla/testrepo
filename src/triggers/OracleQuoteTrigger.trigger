/*************************************************************************************************
Created By:    Sahil Batra
Description  : OracleQuote Trigger
Sahil Batra     Mar 07,2016 Modified (T-483180) - Added call to method notifyonQuoteClose
Prakarsh Jain August 18,2016  Modified(T-531095) - Added call to method updateSharingOnOpportunityRecord
Shreerath Nair October 5,2016 Modified(T-542918) - Added call of method restrictDeletionOfQuote
Shreerath Nair oct  19,2016  Modified(T-549050) - Added call of function setCPQQuoteCreatedOnInsert & setCPQQuoteCreatedOnDelete
**************************************************************************************************/
trigger OracleQuoteTrigger on BigMachines__Quote__c (before insert,before update,after insert, after update,before delete) {
    
    if(Trigger.isBefore){
    	Constants.oracleQuoteUpdated = true;
    }
    
    if(Trigger.isAfter){
        OracleQuoteTriggerHandler.updateOpportunityStage(trigger.oldMap,trigger.new);
		
		if(Trigger.isInsert){
            //T-549050(SN)  function to update the CPQ_Quote_Created field of Opportunity if Quote is associated with it.
            OracleQuoteTriggerHandler.setCPQQuoteCreatedOnInsert(Trigger.newMap);
        }
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        OracleQuoteTriggerHandler.notifyonQuoteClose(trigger.oldMap,trigger.new);
        OracleQuoteTriggerHandler.updateOnPrimaryQuoteChange(trigger.oldMap,trigger.new);
        //Method to add users present in the approver's field on Oracle Quote to Opportunity Share record and Oracle Quote Share record when Big Machine's quote status is changed to Quoting (Pending Approval)
        OracleQuoteTriggerHandler.updateSharingOnOpportunityRecord(trigger.new, trigger.oldMap);
    }
    if(Trigger.isBefore && Trigger.isDelete) {
        OracleQuoteTriggerHandler.OnPrimaryQuoteBeforeDelete(trigger.old);
        //T-542918(SN) - method to restrict deletion of Quote if its primary or related to closed Opportunity.
        OracleQuoteTriggerHandler.restrictDeletionOfQuote(trigger.old);
		//T-549050(SN) function to check if no quote is associated with opportunity then set its CPQ_Quote_Created__c to false
        OracleQuoteTriggerHandler.setCPQQuoteCreatedOnDelete(trigger.oldMap);
    }
}