/**================================================================      
* Appirio, Inc
* Name: [COMM_FSITrigger]
* Description: [Trigger for Field Investigation object]
* Created Date: [22-Sept-2016]
* Created By: [Gaurav Sinha] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 22  Sept 2016       Gaurav Sinha    T-539147
==================================================================*/
trigger COMM_FSITrigger on Field_Service_Investigation__c (before update,after insert,after update) {
    
    if(Trigger.isupdate && trigger.isbefore){
        COMM_FSITriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
    }
    if(Trigger.isInsert && Trigger.isAfter){
        COMM_FSITriggerHandler.onAfterInsert(trigger.new);
    }
    if(Trigger.isupdate && Trigger.isAfter){
        COMM_FSITriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
    }
}