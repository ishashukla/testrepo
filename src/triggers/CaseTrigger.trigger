/**================================================================      
* Appirio, Inc
* Name: CaseTrigger
* Description: 
* Created Date: Jan 27, 2014
* Created By: Sunil Gupta
* 
* Date Modified    Modified By             Description of the update
* 02/28/2016       Nitish Bansal           T-459650                          
* 04/13/2016       Rahul Khanchandani      T-489156                                 
* 07/07/2016       Jagdeep Juneja          T-517828                           
==================================================================*/
trigger CaseTrigger on Case (after insert, before insert, before update, after update) {
    //Checking if trigger is active
    if(CaseTriggerHandler.IsActive()) {
        CaseTriggerHandler handler = new CaseTriggerHandler();
        if(trigger.isBefore){
            if(trigger.isInsert){ 
                handler.beforeInsert(Trigger.new);
            } 
            else if(trigger.isUpdate){
                handler.beforeUpdate(Trigger.new, Trigger.oldMap);
            }
            /*else if(Trigger.isDelete){
                handler.beforeDelete(Trigger.old, Trigger.oldMap);
            }*/
        } 
        else if(trigger.isAfter){
            if(trigger.isInsert){
                handler.afterInsert(Trigger.new);
                //handler.afterInsertAsync(Trigger.newMap.keySet());
            } 
            else if(trigger.isUpdate){
                handler.afterUpdate(Trigger.new, Trigger.oldMap);
                //handler.afterUpdateAsync(Trigger.newMap.keySet());
            }
            /*else if(Trigger.isDelete){
                handler.afterDelete(Trigger.old, Trigger.oldMap);
                handler.afterDeleteAsync(Trigger.oldMap.keySet());
            }*/
        }
    }
}