/**================================================================      
* Appirio, Inc
* Name: QuoteItemTigger
* Description: WorkDetailsTrigger for T-534091
* Created Date: 13 - Sept - 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update

==================================================================*/
trigger WorkDetailsTrigger on SVMXC__Service_Order_Line__c (after insert,after update,before insert) {
    //Checking if thrigger is active
    if(TriggerState.isActive(Constants.SVMXC_Service_Order_Line_Trigger))
    {
        if(Trigger.isAfter && Trigger.isInsert)
        {
          WorkDetailsTiggerHandler.onAfterInsert(Trigger.new); 
        }
        
        if(Trigger.isAfter && Trigger.isUpdate)
        {
          WorkDetailsTiggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap); 
        }
        
        if(Trigger.isbefore && trigger.isinsert){
            WorkDetailsTiggerHandler.onbeforeInsert(Trigger.new); 
        }
    }
}