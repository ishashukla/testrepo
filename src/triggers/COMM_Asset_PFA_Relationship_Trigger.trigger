// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_Asset_PFA_Relationship_Trigger - Creating WO from PFA Relationship
//
// 08/09/2016, Gaurav Sinha      (T-507420) Trigger for auto-assigning tech/WO creation
trigger COMM_Asset_PFA_Relationship_Trigger on Asset_PFA_Relationship__c (after insert,before insert) {

    if(Trigger.isafter && trigger.isinsert){
        COMM_AssetPFARelTriggerHandler.afterInsert(Trigger.new);
    }
    if(trigger.isbefore && trigger.isinsert){
        COMM_AssetPFARelTriggerHandler.beforeInsert(trigger.new);
    }
}