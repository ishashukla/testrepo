/**================================================================      
* Appirio, Inc
* Name: [WorkOrder_Trigger]
* Description: [Trigger for Work Order object]
* Created Date: [20-Jul-2016]
* Created By: [Pankaj Kumar] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 11 Aug 2016        Varun Vasishtha        T-505396
* 26 Aug 2016        Varun Vasishtha        T-530002
==================================================================*/
trigger WorkOrderTrigger on SVMXC__Service_Order__c (before update, before insert, after insert, after update){
    // Handle Before Update event and calling the Handler method.
    if(trigger.isUpdate && trigger.isbefore){
        WorkOrderTriggerHandler.onBeforeUpdate(Trigger.New, Trigger.OldMap);
    }
    // Handle Before Insert event and calling the Handler method.
    if(trigger.isInsert && trigger.isbefore){
        WorkOrderTriggerHandler.onBeforeInsert(Trigger.New);
    }
        
    if (Trigger.isAfter && Trigger.isInsert) {
        WorkOrderTriggerHandler.onAfterInsert(Trigger.New);
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
        WorkOrderTriggerHandler.onAfterUpdate(Trigger.New, Trigger.OldMap);        
    }
}