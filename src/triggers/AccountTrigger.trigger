/**================================================================      
* Appirio, Inc
* Name: AccountTrigger
* Description: T-459647
* Created By :  Nitish Bansal
* Created Date : 26 Feb 2016
*
* Date Modified      Modified By      Description of the update
*                    Kirti Agarwal    T-477949 (Used to insert account team member for parent account)
*                    Depti Maheshwari T-473984(Updated for trigger control)
*                    Mehul Jain       I-216712 (used to update standard type field based on values from custom type field)
* 12 August 2016     Kanika Mathur    T-523264 - To update Account Owner Name field
* 24 August 2016     Kanika Mathur    T-529374 - To update COMM_Sales_Rep__c field
* 17 November 2016   Shubham Dhupar   T-555497 - Added before Delete
==================================================================*/

trigger AccountTrigger on Account (after insert, before update, after update, after delete, before insert,before Delete) {

  //Checking if trigger is active
  if(TriggerState.isActive(Constants.ACCOUNT_TRIGGER)){
    if(trigger.isBefore){
	    if(trigger.isUpdate){
            AccountTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
	    }
	    if(trigger.isInsert) {
	        AccountTriggerHandler.onBeforeInsert(Trigger.new);
	    }
	    if(trigger.isDelete) {
	      AccountTriggerHandler.onBeforeDelete(Trigger.old,Trigger.oldMap);
	    }
	  } else if(trigger.isAfter){
	        if(trigger.isInsert){
	            AccountTriggerHandler.onAfterInsert(Trigger.new, Trigger.oldMap);
	        }
	        if(trigger.isUpdate) {
                AccountTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
	        }
	        if(trigger.isDelete){
               AccountTriggerHandler.onAfterDelete(Trigger.old); 
	        }
        }   
   } 
}