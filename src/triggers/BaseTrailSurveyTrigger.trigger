trigger BaseTrailSurveyTrigger on Base_Trail_Survey__c (after insert, after update,after delete,before delete) {
	if(trigger.isInsert || trigger.isUpdate){
	//	BaseTrailSurveyTriggerHandler.updateMonthlyForecast(trigger.oldMap,trigger.new);
		BaseTrailSurveyTriggerHandler.updateTrailingImpact(trigger.oldMap,trigger.new);
	}
	if(trigger.isAfter && trigger.isDelete){
		BaseTrailSurveyTriggerHandler.updateTrailingImpactbeforeDelete(trigger.oldMap);
	} 
	if(trigger.isBefore && trigger.isDelete) {
		BaseTrailSurveyTriggerHandler.deleteBaseProducts(trigger.old);
	}
}