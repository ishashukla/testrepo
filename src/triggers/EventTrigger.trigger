trigger EventTrigger on Event (before delete, before update) {
  //Method to restrict users to delete or edit records if they are not the owner
  //Prakarsh Jain 13 April 2016 - UAT Issues sheet shared by Devin
  // Only run this trigger if the EventTrigger Custom Setting is 'true'
  //Added By Harendra for Story# S-415285
  if(TriggerState.isActive(Constants.TRIGGER_EVENTTRIGGER)) {
	  if(Trigger.isBefore && Trigger.isUpdate){
	    Instruments_EventTriggerHandler.restrictEditDeleteOfRecords(Trigger.new, Trigger.oldMap, false);
	  }
	  if(Trigger.isBefore && Trigger.isDelete){
	    Instruments_EventTriggerHandler.restrictEditDeleteOfRecords(Trigger.new, Trigger.oldMap, true);
	  }
  } //end Story# S-415285
}