/**================================================================      
 * Appirio, Inc
 * Name: ViewAsRelationshipTrigger
 * Description: Trigger for ASR_Relationship__c object(T-538445), added call to methods shareASRUserRecords() and deleteASRUserRecords
 * Created Date: [Sept 26, 2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 * Oct 12, 2016       Shreerath Nair   Implemented new Trigger framework(T-542899)
 ==================================================================*/
trigger ViewAsRelationshipTrigger on ASR_Relationship__c (before insert,after insert,before update,after update, before delete) {

  if(ViewAsRelationshipTriggerHandler.isActive()){
  
       ViewAsRelationshipTriggerHandler handler = new ViewAsRelationshipTriggerHandler();
  
       if(Trigger.isBefore){
        
           if(trigger.isInsert){             
                handler.beforeInsert(Trigger.new);               
           } 
           else if(trigger.isUpdate){
                handler.beforeUpdate(Trigger.new, Trigger.oldMap);              
           }
           else if(Trigger.isDelete){              
                 handler.beforeDelete(Trigger.old, Trigger.oldMap);
           }
            
       }    
       else if(trigger.isAfter){
            
           if(trigger.isInsert){               
               handler.afterInsert(Trigger.new);
           }
           else if(Trigger.isUpdate){               
               handler.afterUpdate(Trigger.new, Trigger.oldMap); 
           }
           else if(Trigger.isDelete){                       
           }
      }
  }    
        
  
}