({
    navigateToSObject : function(recordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },

    changeFollowRecord : function(component, recordId, toFollowRecord, type){
        var followString = 'followed';
        if(!toFollowRecord) followString = 'un'+followString;

        var action = component.get("c.changeFollowRecord");
        action.setParams({ recordIdS : recordId,
                              toFollow: toFollowRecord});
        action.setCallback(this, function(response) {
          // Display toast message to indicate load status
          var state = response.getState();
          var toastEvent = $A.get("e.force:showToast");
          if (state === 'SUCCESS'){
              var orderDetailInfo = component.get("v.orderDetailInfo");
              if(type == 'Account'){
                  orderDetailInfo.AccountFollowed = !orderDetailInfo.AccountFollowed;
                  if(orderDetailInfo.AccountFollowed == true){
                        orderDetailInfo.OrderFollowed = true;
                  }
              }
              else{
                  orderDetailInfo.OrderFollowed = !orderDetailInfo.OrderFollowed;
              }
              component.set("v.orderDetailInfo",orderDetailInfo );
              toastEvent.setParams({
                  "title": "Success!",
                  "message": "" + type + " has been successfully "+followString+"!"
              });
          }
          else {
              toastEvent.setParams({
                  "title": "Error!",
                  "message": "An error occurred following this "+type+". If this continues, contact an administrator for help."
              });
          }
          toastEvent.fire();
        });
        $A.enqueueAction(action);

    },

    openDialog : function(component){
      var dialog = component.find('dialog');
      var evt = dialog.get("e.toggle");
      evt.setParams({ isVisible : true });
      evt.fire();
    },
    closeDialog : function(component){
      var dialog = component.find('dialog');
      var evt = dialog.get("e.toggle");
      evt.setParams({ isVisible : false });
      evt.fire();
    }

})