({
    afterRender : function(component, event, helper){
        /*if(!component.get("v.afterRender")){
            var orderItem = component.get("v.orderDetailInfo");
            if(orderItem != null){
                var holdDescription = orderItem.HoldType;
                if(typeof holdDescription === "string"){
                    component.set("v.holdReason", holdDescription.split(";"));
                    component.set("v.afterRender", true);
                }
            }
        }*/
    },
    togglePanel : function(component, event, helper) {
		    //console.log(event.target);
        //console.log($(event.target).parents(".panel"));
        $(event.target).parents(".panel").toggleClass("collapsed").toggleClass("open").toggleClass("selected");
        event.stopPropagation();
	  },
    navigateToOrderDetail : function(component,event,helper){
        var orderId = component.get("v.orderDetailInfo").OrderId;
        helper.navigateToSObject(orderId);
    },
    navigateToAccountDetail : function(component,event,helper){
        var accountId =  component.get("v.orderDetailInfo").AccountId;
        helper.navigateToSObject(accountId);
    },
    followCustomer : function(component, event, helper){
        var orderDetailInfo = component.get("v.orderDetailInfo");
        if(orderDetailInfo.AccountFollowed){
            component.set("v.confirmMessage","Unfollowing an Account will NOT unfollow any related Orders. Do you wish to proceed?");
        }
        else{
            component.set("v.confirmMessage","Following an Account will also follow all of its Open Orders. Do you wish to proceed?");
        }
        //Show Confirm Dialog
        helper.openDialog(component);
    },
    followOrder : function(component, event, helper){
        var orderDetailInfo = component.get("v.orderDetailInfo");
        //Update Follow Order
        helper.changeFollowRecord(component, orderDetailInfo.OrderId, !orderDetailInfo.OrderFollowed, 'Order');
    },
    onConfirm : function(component, event, helper){
      helper.closeDialog(component);
      var orderDetailInfo = component.get("v.orderDetailInfo");
      //Update Follow Account
      helper.changeFollowRecord(component, orderDetailInfo.AccountId, !orderDetailInfo.AccountFollowed, 'Account');
    },
    showFedEx : function(component, event, helper){
        var text = event.source.elements[0].innerText;
        var trackingNumber = text.replace('Tracking #', '');
        //var trackingNumber = component.get("v.orderItemWithTrackingDetails.FedExTrackingNumber");
        var navToSObjEvt = $A.get("e.force:navigateToURL");
        navToSObjEvt.setParams({
            url: "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=" + trackingNumber + "&cntry_code=us"
        });
        navToSObjEvt.fire();
    }
    
})