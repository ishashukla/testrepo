({
	doInit : function(component, event, helper) {             
    	helper.getCurrentUserSettings(component); 
        
    },
    doSave : function(component, event, helper){
        var settingData = component.get("v.settings");
        settingData.Entered__c = component.find("Entered").get("v.activeLabel") == "On" ? true : false;
        settingData.Booked__c = component.find("Booked").get("v.activeLabel") == "On" ? true : false;
        settingData.Shipped__c = component.find("Shipped").get("v.activeLabel") == "On" ? true : false;
        settingData.Cancelled__c = component.find("Cancelled").get("v.activeLabel") == "On" ? true : false;
        settingData.Awaiting_Shipping__c = component.find("Awaiting_Shipping").get("v.activeLabel") == "On" ? true : false;
        settingData.Backordered__c = component.find("Backordered").get("v.activeLabel") == "On" ? true : false;
        settingData.On_Hold__c = component.find("OnHold").get("v.activeLabel") == "On" ? true : false;
        settingData.Delivered__c = component.find("Delivered").get("v.activeLabel") == "On" ? true : false;
        settingData.Closed__c = component.find("Closed").get("v.activeLabel") == "On" ? true : false;
        
        settingData.Awaiting_PO__c = component.find("Awaiting_PO").get("v.activeLabel") == "On" ? true : false;
        //settingData.Credit_Applied__c = component.find("Credit_Applied").get("v.activeLabel") == "On" ? true : false;
        
        settingData.Awaiting_Return__c = component.find("Awaiting_Return").get("v.activeLabel") == "On" ? true : false;
        settingData.Returned__c = component.find("Returned").get("v.activeLabel") == "On" ? true : false;
        
        
        //Added by Jyoti for S-389739
        settingData.Bill_Closed__c = component.find("Bill_Closed").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Bill_Closed__c = component.find("AllBill_Closed").get("v.activeLabel") == "On" ? true : false;
        //End S-389739
        
        //Added for S-397645
        settingData.All_Entered__c = component.find("AllEntered").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Booked__c = component.find("AllBooked").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Shipped__c = component.find("AllShipped").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Cancelled__c = component.find("AllCancelled").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Awaiting_Shipping__c = component.find("AllAwaiting_Shipping").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Backordered__c = component.find("AllBackordered").get("v.activeLabel") == "On" ? true : false;
        settingData.All_On_Hold__c = component.find("AllOnHold").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Delivered__c = component.find("AllDelivered").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Closed__c = component.find("AllClosed").get("v.activeLabel") == "On" ? true : false;
        
        settingData.All_Awaiting_PO__c = component.find("AllAwaiting_PO").get("v.activeLabel") == "On" ? true : false;
        //settingData.Credit_Applied__c = component.find("Credit_Applied").get("v.activeLabel") == "On" ? true : false;
        
        settingData.All_Awaiting_Return__c = component.find("AllAwaiting_Return").get("v.activeLabel") == "On" ? true : false;
        settingData.All_Returned__c = component.find("AllReturned").get("v.activeLabel") == "On" ? true : false;        
        //End S-397645
        helper.setCurrentUserSettings(component, settingData, function(response){
            var state = response.getState();
            // Display toast message to indicate load status
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Settings have been updated successfully."
                });
            }
            else {
                toastEvent.setParams({
                    "title": "Error!",
                    "message": " Something has gone wrong."
                });
            }
            toastEvent.fire();
        });
    },
    doGet : function(component, event, helper){
    	helper.getCurrentUserSettings(component);         
	},
        
    waiting : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();
    },
    doneWaiting : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();
    },
    
   //Added for S-397645  
    btnSwitch : function(component, event, helper){
        if(event.getSource){
            //Framework Component Event
            var target = event.getSource();
            filter = target.get("v.label");
        } else{
            var target = event.target.value;
        }       
       component.set("v.activeTab", target);       
    },
    //end S-397645
})