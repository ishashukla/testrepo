({
	getCurrentUserSettings: function(component) {
        //Call the getExpenses Apex class method
        var action = component.get("c.getCurrentUserSettings");
        var self = this;
        action.setCallback(this, function(response) {
        	var state = response.getState();
        	if (state === "SUCCESS") {
                        console.log(response.getReturnValue());
                        var settingData = response.getReturnValue();
                	component.set("v.settings", settingData);
                        component.find("Entered").set("v.activeLabel", settingData.Entered__c?"On":"Off");
                        component.find("Booked").set("v.activeLabel", settingData.Booked__c?"On":"Off");
                        component.find("Shipped").set("v.activeLabel", settingData.Shipped__c?"On":"Off");
                        component.find("Cancelled").set("v.activeLabel", settingData.Cancelled__c?"On":"Off");
                        component.find("Awaiting_Shipping").set("v.activeLabel", settingData.Awaiting_Shipping__c?"On":"Off");
                        component.find("Backordered").set("v.activeLabel", settingData.Backordered__c?"On":"Off");
                        component.find("OnHold").set("v.activeLabel", settingData.On_Hold__c?"On":"Off");
                		component.find("Delivered").set("v.activeLabel", settingData.Delivered__c?"On":"Off");
                        component.find("Closed").set("v.activeLabel", settingData.Closed__c?"On":"Off");
                        
                        component.find("Awaiting_PO").set("v.activeLabel", settingData.Awaiting_PO__c?"On":"Off");
                        //component.find("Credit_Applied").set("v.activeLabel", settingData.Credit_Applied__c?"On":"Off");
                        
                        component.find("Awaiting_Return").set("v.activeLabel", settingData.Awaiting_Return__c?"On":"Off");
                        component.find("Returned").set("v.activeLabel", settingData.Returned__c?"On":"Off");
                		
                        
                        //Added by Jyoti for S-389739
                		component.find("Bill_Closed").set("v.activeLabel", settingData.Bill_Closed__c?"On":"Off");                        
                		component.find("AllBill_Closed").set("v.activeLabel", settingData.All_Bill_Closed__c?"On":"Off");
                		//End S-389739
                
                		//Added for S-397645
        	        	component.find("AllEntered").set("v.activeLabel", settingData.All_Entered__c?"On":"Off");
                        component.find("AllBooked").set("v.activeLabel", settingData.All_Booked__c?"On":"Off");
                        component.find("AllShipped").set("v.activeLabel", settingData.All_Shipped__c?"On":"Off");
                        component.find("AllCancelled").set("v.activeLabel", settingData.All_Cancelled__c?"On":"Off");
                        component.find("AllAwaiting_Shipping").set("v.activeLabel", settingData.All_Awaiting_Shipping__c?"On":"Off");
                        component.find("AllBackordered").set("v.activeLabel", settingData.All_Backordered__c?"On":"Off");
                        component.find("AllOnHold").set("v.activeLabel", settingData.All_On_Hold__c?"On":"Off");
                		component.find("AllDelivered").set("v.activeLabel", settingData.All_Delivered__c?"On":"Off");
                        component.find("AllClosed").set("v.activeLabel", settingData.All_Closed__c?"On":"Off");
                        
                        component.find("AllAwaiting_PO").set("v.activeLabel", settingData.All_Awaiting_PO__c?"On":"Off");
                        //component.find("AllCredit_Applied").set("v.activeLabel", settingData.Credit_Applied__c?"On":"Off");
                        
                        component.find("AllAwaiting_Return").set("v.activeLabel", settingData.All_Awaiting_Return__c?"On":"Off");
                        component.find("AllReturned").set("v.activeLabel", settingData.All_Returned__c?"On":"Off");                        
                		//End S-397645
        	}
        });
        $A.enqueueAction(action);
    },
    
    setCurrentUserSettings: function(component, settings, callback) {
        //Call the getExpenses Apex class method
        var action = component.get("c.setCurrentUserSettings");
        action.setParams({
        	"settings": settings
        });
        if (callback) {
        	action.setCallback(this, callback);
        }
		$A.enqueueAction(action);
    },
   
    
})