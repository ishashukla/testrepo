({
		getFollowUsers: function(component) {
        var action = component.get("c.getCurrentUserFollowUsers");
        var self = this;
        action.setCallback(this, function(response) {
        	var state = response.getState();
        	if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var followUsers = response.getReturnValue();
        				component.set("v.followRecords", response.getReturnValue());
        	}
        });
        $A.enqueueAction(action);
    },

    setFollowUsers: function(component, followUsers, callback) {

        var action = component.get("c.setCurrentUserFollowUsersJSON");
        action.setParams({
        	"followUsersString": JSON.stringify(followUsers)
        });

        if (callback) {
        	action.setCallback(this, callback);
        }

				$A.enqueueAction(action);
    },
})