({
		doInit : function(component, event, helper) {
    	helper.getFollowUsers(component);
    },
    doSave : function(component, event, helper){
			  var followUsers = component.get("v.followRecords");
				var renderedUsers = component.find("User");
				for ( followUser in followUsers) {
						followUsers[followUser].IsFollowed = renderedUsers[followUser].get("v.activeLabel") == "On" ? true : false;
				}
        helper.setFollowUsers(component, followUsers, function(response){
            var state = response.getState();
            // Display toast message to indicate load status
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " Records have been updated successfully."
                });
            }
            else {
                toastEvent.setParams({
                    "title": "Error!",
                    "message": " Something has gone wrong."
                });
            }
            toastEvent.fire();
            helper.getFollowUsers(component);
        });
    },
    doGet : function(component, event, helper){
    	helper.getFollowUsers(component);
		},
    waiting : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();
    },
    doneWaiting : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();
    },
})