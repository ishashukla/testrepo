({
    setSearchOption : function(component, event, helper) {
		var sObj = component.get("v.product");
        var navToSObjEvt = $A.get("e.c:nv_SearchOptionSelected");
        navToSObjEvt.setParams({
            option: sObj
        });
        navToSObjEvt.fire();
	}
})