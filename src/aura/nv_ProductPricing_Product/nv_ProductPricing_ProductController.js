({
    toggleActive : function(component, event, helper) {
        var product = component.get("v.product");
        product.isActive = !product.isActive;
        component.set("v.product", product);
        var cmpEvent = component.getEvent("productSelected");
        cmpEvent.fire();
	}
})