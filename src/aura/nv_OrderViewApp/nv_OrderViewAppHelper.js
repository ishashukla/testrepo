({
	createRecordComponent : function(cmp, recordId) {
        var componentConfig = {
            "componentDef": "markup://c:nv_OrderView",
            "attributes": {
                "values": { orderId:recordId}
            }
        };

        $A.componentService.newComponentAsync(
            this,
            function(newComponent){
                var divComponent = cmp.find("ElementHere");
                var divBody = divComponent.get("v.body");
                divBody.push(newComponent);
                divComponent.set("v.body", divBody);
            },
            componentConfig
        );
    }
})