({
	doInit : function(component, event, helper) {
        $(".order-footer").remove().appendTo('body');
        $(".order-footer").css("visibility", "hidden");
        component.set("v.isInitialized", true);

        helper.retrieveRecent(component);
        helper.retrieveProducts(component);
        helper.retrieveFollowableUsers(component);
        helper.displaySVG(component);                    

        // in isBigDataMode version, we do not cache products locally
        var action = component.get("c.isBigDataMode");
        action.setCallback(this, function(response) {
            console.log('isBigDataMode Callback ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var isBigDataMode = response.getReturnValue();
                component.set("v.isBigDataMode", isBigDataMode);
            }
        });
        $A.enqueueAction(action);
  	},
    searchTextChanged : function(component, event, helper){
        //console.log("Search Text Changed");
        //console.log("Text Value: " + component.get("v.searchTerm") );
      	var searchTerm = component.get("v.searchTerm").toLowerCase();
        var accounts = component.get("v.recentAccounts");
        var atleastOneVisible = false;
        for(var i =0; i < accounts.length; i++){
            var account = accounts[i];
            //Check Account Name and Account number to determine if we should display.
            if(account.Name.toLowerCase().indexOf(searchTerm) > -1 || account.AccountNumber.toLowerCase().indexOf(searchTerm) > -1 || (!$A.util.isEmpty(account.Nickname__c) && account.Nickname__c.toLowerCase().indexOf(searchTerm) > -1)){
                account.isVisible = true;
                atleastOneVisible = true;
            } else{
                account.isVisible = false;
            }
        }
        component.set("v.atleastOneVisible", atleastOneVisible);
        component.set("v.recentAccounts", accounts);
    },
    //S-398540 Connor Flynn 03/25/2016 Add function to clear search field when "x" is clicked
    clearSearchBox : function(component, event, helper){
        if(component.get("v.searchTerm") != null){
            component.set("v.searchTerm", "");
            if(component.find("searchBoxText") != null){
                //component.find("searchBoxText").focus();
            }
        }
        if(component.get("v.productSearchTerm") != null){
            component.set("v.productSearchTerm", "");
            if(component.find("additionalSearchBoxText") != null){
                //component.find("additionalSearchBoxText").focus();
            }
        }
        if (component.get("v.isBigDataMode")) {
            component.set("v.alreadyRetrievedProducts", false);
        }
    },
    ////S-398540 03/25/2016 Connor Flynn End
    getResults : function(component, event, helper) {
		component.set("v.activePage", 1);
        var option = event.getParams("option").option;
        $('.scroller').css("transform", "");
        component.set("v.activeOption", option);
        component.set("v.ordersPage", 0);
        var selectedProducts = component.get("v.selectedProducts");
		if(!$A.util.isEmpty(selectedProducts)){
            $(".order-footer").css("visibility", "visible");
        } else{
            $(".order-footer").css("visibility", "hidden");
        }
        //$(".order-footer").css("visibility", "visible");
	},
    clearResults : function(component, event, helper) {
		component.set("v.activePage", 0);
        component.set("v.activeOption", null);
        component.set("v.selectedProducts", []);
        component.set("v.errorCount", 0);
        component.set("v.resetResults", true);
        component.set("v.searchTerm", "");
        component.set("v.productSearchTerm", "");
        component.set("v.alreadyRetrievedProducts", false);

        
        var productLines = component.get("v.products");
        for(var l = 0; l < productLines.length; l++){
            var productLine = productLines[l];
            for(var s = 0; s < productLine.segments.length; s++){
                var segment = productLine.segments[s];

                if (component.get("v.isBigDataMode")) {
                    segment.productsList = [];
                } else {
                    for(var p = 0; p < segment.productsList.length; p++){
                        var product = segment.productsList[p];
                        product.isVisible = true;
                        product.isActive = false;
                    }                    
                }
                segment.isVisible = true;
                segment.isActive = false;
                segment.allProductsActive = false;
            }
            productLine.isVisible = true;
            productLine.isActive = false;
        }
        component.set("v.products", productLines);
        
        $(".order-footer").css("visibility", "hidden");
	},
    productSelected : function(component, event, helper){
        var productLines = component.get("v.products");
        var selectedProducts = [];
        var selectedProductNumbers = [];
        var activeSegments = [];
        var activeIndexes = [];
        for(var l = 0; l < productLines.length; l++){
            var productLine = productLines[l];
            var numActive = 0;
            for(var s = 0; s < productLine.segments.length; s++){
                var segment = productLine.segments[s];
                if(segment.allProductsActive){
                    activeSegments.push("" + l + ";" + s);
                }
                for(var p = 0; p < segment.productsList.length; p++){
                    var product = segment.productsList[p];
                    if(product.isActive){
                        selectedProducts.push(product);
                        selectedProductNumbers.push(product.Product.Catalog_Number__c);
                        hasProduct = true;
                        activeIndexes.push("" + l + ";" + s + ";" + p);
                    }
                }
            }
        }
        console.log(activeIndexes);
        console.log(activeSegments);
        component.set("v.activeIndexes", activeIndexes);
        component.set("v.activeSegments", activeSegments);
        component.set("v.selectedProducts", selectedProducts);
        component.set("v.selectedProductNumbers", selectedProductNumbers);
        var currentPage = component.get("v.activePage");
        if(currentPage == 1 && selectedProducts.length > 0){
            $(".order-footer").css("visibility", "visible");
        } else{
            $(".order-footer").css("visibility", "hidden");
        }
    },
    uncheckAll : function(component, event, helper){
        component.set("v.selectedProducts", []);
        component.set("v.productsLoading", true);
        setTimeout(function(){
            $(".order-footer").css("visibility", "hidden");
            helper.uncheckAll(component);
        }, 20);
    },
    getPrices : function(component, event, helper){
        component.set("v.activePage", 2);
        component.set("v.retrievedPrices", []);
        component.set("v.errorCount", 0);
        $(".order-footer").css("visibility", "hidden");
        $('.scroller').css("transform", "");
        helper.retrievePricing(component);
    },
    backAccount: function(component, event, helper){
        component.set("v.activePage", 0);
        component.set("v.activeOption", null);
        $(".order-footer").css("visibility", "hidden");
    },
    backSelect : function(component, event, helper){
        component.set("v.activePage", 1);
        component.set("v.errorCount", 0);
        component.set("v.successCount", 0);
        if(component.get("v.selectedProducts").length > 0){
            $(".order-footer").css("visibility", "visible");
        } else{
            $(".order-footer").css("visibility", "hidden");
        }
    },
    productSearchTextChanged : function(component, event, helper){
        if (component.get("v.isBigDataMode")) {
            helper.productSearchTextChangedServer(component, event, helper);
        } else {
            helper.productSearchTextChangedLocal(component, event, helper);
        }
    },
    sortActiveTab : function(component, event, helper){
        console.log('Inside Controller Method: sortActiveTab');
        console.log('Sort By: ' + event.source.get('v.label'));
        var retrievedPrices = component.get("v.retrievedPrices");
        if(!$A.util.isEmpty(retrievedPrices)){
            component.set("v.retrievedPrices", helper.sortResults(event.source.get("v.label"), retrievedPrices, component));
        }
    }
});