({
	retrieveRecent : function(component){
        var action = component.get("c.getMyCustomerAccounts");
        action.setCallback(this, function(response) {
            console.log('Inside Callback ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log(response.getReturnValue());
                var returnedAccounts = response.getReturnValue();
                for(var i = 0; i < returnedAccounts.length; i++){
                    returnedAccounts[i].isVisible = true;
                }

                if (returnedAccounts.length > 0) {
                    component.set("v.atleastOneVisible", true);
                }
                //debugger;
                component.set("v.recentLoading", false);
                component.set("v.recentAccounts", returnedAccounts);
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    retrieveProducts : function(component){
        var action = component.get("c.getAllProducts");
        action.setCallback(this, function(response) {
            console.log('Inside Callback Products' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var returnedProducts = response.getReturnValue();

                //Pre-set all products to be set to not active.
                for(var l = 0; l < returnedProducts.length; l++){
                    var productLine = returnedProducts[l];
                    productLine.isActive = false;
                    productLine.isVisible = true;
                    productLine.products = [];

                    for(var s = 0; s < productLine.segments.length; s++){
                        var segment = productLine.segments[s];
                        segment.isActive = false;
                        segment.isVisible = true;
                        segment.allProductsActive = false;

                        for(var p = 0; p < segment.productsList.length; p++){
                            segment.productsList[p].isActive = false;
                            segment.productsList[p].isVisible = true;
                            productLine.products.push(segment.productsList[p]);
                        }
                    }
                }

                if (returnedProducts.length > 0) {
                    component.set("v.atleastOneProduct", true);
                }

                //debugger;
                setTimeout(function(){
                    component.set("v.products", returnedProducts);
                }, 20);
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    retrievePricing : function(component){
        component.set("v.retrievedPrices", []);
        component.set("v.retrievingPrices", true);
        var action = component.get("c.retrievePricing");
        console.log(component.get("v.activeOption.AccountNumber"));
        console.log(component.get("v.selectedProductNumbers").join(","));
        action.setParams({customerId : component.get("v.activeOption.AccountNumber"), productIdsCSV : component.get("v.selectedProductNumbers").join(",")});
        action.setCallback(this, function(response) {
            console.log('retrievePricing Callback ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var retrievedPrices = response.getReturnValue();
                var selectedProducts = component.get("v.selectedProducts");
                console.log("Selected Products", selectedProducts);
                console.log("Retrieved Prices", retrievedPrices);
                if(retrievedPrices == null){
                    retrievedPrices = [];
                    component.set("v.nullResult", true);
                    component.set("v.errorCount", selectedProducts.length);
                    component.set("v.successCount", 0);
                    $(".order-footer").css("visibility", "visible");
                } else{
                    component.set("v.nullResult", false);
                    var errorCount = 0;
                    var successCount = 0;
                    for(var r = 0; r < retrievedPrices.length; r++){
                        var price = retrievedPrices[r];
                        if(price.ResolvedPrice.Status == "E"){
                            errorCount++;
                        }
                    }
                    console.log("Error Count: " + errorCount);
                    component.set("v.errorCount", errorCount);
                    component.set("v.successCount", successCount);
                    $(".order-footer").css("visibility", "visible");
                }
                for(var p = 0; p < selectedProducts.length; p++){
                    var product = selectedProducts[p];
                    for(var r = 0; r < retrievedPrices.length; r++){
                        var price = retrievedPrices[r];
                        if(price.CatalogNumber == product.Product.Catalog_Number__c){
                            product.ResolvedPrice = price.ResolvedPrice;
                            break;
                        }
                    }
                }
                component.set("v.retrievedPrices", selectedProducts);
                component.set("v.retrievingPrices", false);
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    retrieveFollowableUsers : function(component){
        var action = component.get("c.getFollowableUsers");
        action.setCallback(this, function(response) {
            console.log('Inside Callback Followable: ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //debugger;
                //component.set("v.recentLoading", false);


                component.set("v.followableUsers", response.getReturnValue());
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    uncheckAll: function(component){
        var returnedProducts = component.get("v.products");
        //Pre-set all products to be set to not active.
        for(var l = 0; l < returnedProducts.length; l++){
            var productLine = returnedProducts[l];
            for(var s = 0; s < productLine.segments.length; s++){
                var segment = productLine.segments[s];
                segment.allProductsActive = false;
                for(var p = 0; p < segment.productsList.length; p++){
                    segment.productsList[p].isActive = false;
                }
            }
        }
        component.set("v.products", returnedProducts);
        component.set("v.productsLoading", false);
    },
    productMatches: function(searchTerm, product){
        if(!$A.util.isEmpty(product.Product.Name) && product.Product.Name.toLowerCase().indexOf(searchTerm) > -1){
            return true;
        }
        if(!$A.util.isEmpty(product.Product.Catalog_Number__c) && product.Product.Catalog_Number__c.toLowerCase().indexOf(searchTerm) > -1){
            return true;
        }
        if(!$A.util.isEmpty(product.Product.GTIN__c) && product.Product.GTIN__c.toLowerCase().indexOf(searchTerm) > -1){
            return true;
        }
        if(!$A.util.isEmpty(product.Product.Long_Description__c) && product.Product.Long_Description__c.toLowerCase().indexOf(searchTerm) > -1){
            return true;
        }
        if(!$A.util.isEmpty(product.Product.Long_Description__c) && product.Product.Long_Description__c.toLowerCase().indexOf(searchTerm) > -1){
            return true;
        }
		return false;
    },
    displaySVG: function(component){
        var svg = component.find("svg_sort");
        var value = svg.getElement().innerHTML;
        value = $("<div/>").html(value).text();
        svg.getElement().innerHTML = value;
    },
    sortResults: function(sortString, list, component){
        console.log(list);
        switch(sortString){
                case "Item Number Alphabetically":
                    list.sort(function(a, b){
                        //console.log("Order: ", a, b);
                        return a.Product.Catalog_Number__c.localeCompare(b.Product.Catalog_Number__c);
                    });
                    break;
                case "Item Number Reverse Alphabetically":
                    list.sort(function(a, b){
                        //console.log("Orders: ", a, b);
                        return b.Product.Catalog_Number__c.localeCompare(a.Product.Catalog_Number__c);
                    });
                    break;
                case "Largest Price First":
                    list.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aAmt = ($A.util.isUndefinedOrNull (a.ResolvedPrice) || a.ResolvedPrice.Status == "E") ? a.ListPrice : a.ResolvedPrice.ResolvedPrice;
                        var bAmt = ($A.util.isUndefinedOrNull (b.ResolvedPrice) || b.ResolvedPrice.Status == "E") ? b.ListPrice : b.ResolvedPrice.ResolvedPrice;
                        if(parseFloat(aAmt) > parseFloat(bAmt)){
                            return -1;
                        } else if(parseFloat(aAmt) < parseFloat(bAmt)){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "Smallest Price First":
                    list.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aAmt = ($A.util.isUndefinedOrNull (a.ResolvedPrice) || a.ResolvedPrice.Status == "E") ? a.ListPrice : a.ResolvedPrice.ResolvedPrice;
                        var bAmt = ($A.util.isUndefinedOrNull (b.ResolvedPrice) || b.ResolvedPrice.Status == "E") ? b.ListPrice : b.ResolvedPrice.ResolvedPrice;
                        if(parseFloat(aAmt) > parseFloat(bAmt)){
                            return 1;
                        } else if(parseFloat(aAmt) < parseFloat(bAmt)){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                default:
                    break;
            }
        return list;
    },
    productSearchTextChangedServer : function(component, event, helper){
        // make all product lines not visible or active
        component.set("v.productsLoading", true);
        var originalProducts = component.get("v.products");
        for (var i = 0; i < originalProducts.length; i++) {
            var productLine = originalProducts[i];
            productLine.isActive = false;
            productLine.isVisible = false;
            productLine.products = [];

            for (var s = 0; s < productLine.segments.length; s++) {
                var segment = productLine.segments[s];
                segment.isActive = false;
                segment.isVisible = false;
                segment.allProductsActive = false;

                for(var p = 0; p < segment.productsList.length; p++) {
                    segment.productsList[p].isActive = false;
                    segment.productsList[p].isVisible = false;
                    productLine.products.push(segment.productsList[p]);
                }                    
            }
        }

        var searchText = component.get("v.productSearchTerm").toLowerCase();
        var action = component.get("c.searchForProducts");
        action.setParams({searchText : searchText});
        action.setCallback(this, function(response) {
            console.log('productSearchTextChangedServer Callback ' + response.getState());

            // if we are performing a search, do not make them load products separately
            // but if the search text is blank, we simply return the segments
            if (!$A.util.isEmpty(searchText)) {
                component.set("v.alreadyRetrievedProducts", true);
                component.set("v.alreadyRetrievedProductsForSegment", true);
            }
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());

                var returnedProducts = response.getReturnValue();

                //Pre-set all products to be set to not active.
                for(var l = 0; l < returnedProducts.length; l++){
                    var productLine = returnedProducts[l];
                    productLine.isActive = false;
                    productLine.isVisible = true;
                    productLine.products = [];

                    for(var s = 0; s < productLine.segments.length; s++){
                        var segment = productLine.segments[s];
                        segment.isActive = false;
                        segment.isVisible = true;
                        segment.allProductsActive = false;

                        for(var p = 0; p < segment.productsList.length; p++){
                            segment.productsList[p].isActive = false;
                            segment.productsList[p].isVisible = true;
                            productLine.products.push(segment.productsList[p]);
                        }
                    }
                }

                if (returnedProducts.length > 0) {
                    component.set("v.atleastOneProduct", true);
                }

                //debugger;
                setTimeout(function(){
                    component.set("v.products", returnedProducts);
                    console.log(returnedProducts);
                    component.set("v.productsLoading", false);
                }, 20);
            }
        });
        $A.enqueueAction(action);
    },
    productSearchTextChangedLocal : function(component, event, helper){
        var searchTerm = component.get("v.productSearchTerm").toLowerCase();
        var productLines = component.get("v.products");
        var selectedProducts = [];
        var atleastOneProduct = false;
        var shouldReset = component.get("v.resetResults");
        if(searchTerm == ""){
            atleastOneProduct = true;
            for(var l = 0; l < productLines.length; l++){
                var productLine = productLines[l];
                for(var s = 0; s < productLine.segments.length; s++){
                    var segment = productLine.segments[s];
                    for(var p = 0; p < segment.productsList.length; p++){
                        var product = segment.productsList[p];
                        product.isVisible = true;
                        if(shouldReset) product.isActive = false;
                    }
                    segment.isVisible = true;
                    segment.isActive = false;
                    if(shouldReset) segment.allProductsActive = false;
                }
                productLine.isVisible = true;
                productLine.isActive = false;
            }
            if(shouldReset){component.set("v.resetResults", false);}
        } else{
            for(var l = 0; l < productLines.length; l++){
                var productLine = productLines[l];
                var hasVisibleSegment = false;
                var matchesSegment = false;
                var matchesPLProduct = false;
                var matchesProductLine = (!$A.util.isEmpty(productLine.ProductLineName) && productLine.ProductLineName.toLowerCase().indexOf(searchTerm) > -1);
                if(matchesProductLine){
                    atleastOneProduct = true;
                }
                console.log("Matches ProductLine: " + matchesProductLine);
                for(var s = 0; s < productLine.segments.length; s++){
                    var segment = productLine.segments[s];
                    var hasVisibleProduct = false;
                    var segmentMatches = (!$A.util.isEmpty(segment.SegmentName) && segment.SegmentName.toLowerCase().indexOf(searchTerm) > -1);
                    if(segmentMatches){
                       matchesSegment = true;
                       atleastOneProduct = true;
                    }
                    var matchesSegmentProduct = false;
                    for(var p = 0; p < segment.productsList.length; p++){
                        var product = segment.productsList[p];
                        var productMatches = helper.productMatches(searchTerm, product);
                        if( productMatches || segmentMatches || matchesProductLine){
                            if(productMatches){
                                matchesSegmentProduct = true;
                                matchesPLProduct = true;
                                atleastOneProduct = true;
                            }
                            hasVisibleProduct = true;
                            hasVisibleSegment = true;
                            product.isVisible = true;
                        } else{
                            product.isVisible = false;
                        }
                    }
                    segment.isVisible = hasVisibleProduct;
                    segment.isActive = matchesSegmentProduct;
                }
                productLine.isVisible = hasVisibleSegment;
                productLine.isActive = matchesPLProduct || matchesSegment;
            }
        }
        component.set("v.productsLoading", true);
        setTimeout(function(){
            component.set("v.products", productLines);
            component.set("v.atleastOneProduct", atleastOneProduct);
            component.set("v.productsLoading", false);
        }, 20);
    }
})