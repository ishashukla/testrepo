({
	getFollowedAccounts : function(component) {
		//Call the getFollowedAccounts Apex class method
        var action = component.get("c.getFollowedAccounts");
        var self = this;
        action.setCallback(this, function(response) {
        	var state = response.getState();
        	if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var accounts = response.getReturnValue();
                if(!$A.util.isEmpty(accounts)){
                    component.set("v.followedAccounts", accounts);
                }     
                component.set("v.accountsLoading", false);
        	}
        });
        $A.enqueueAction(action);
	},
    getFollowedOrders : function(component){
        //Call the getFollowedOrders Apex class method
        var action = component.get("c.getFollowedOrders");
        var self = this;
        action.setCallback(this, function(response) {
        	var state = response.getState();
        	if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var orders = response.getReturnValue();
                if(!$A.util.isEmpty(orders)){
                    component.set("v.followedOrders", orders);
                }  
                component.set("v.ordersLoading", false);
        	}
        });
        $A.enqueueAction(action);
    },
    unfollowAllRecords : function(component, recordType){
        //Call the getFollowedOrders Apex class method
        var action = component.get("c.unfollowAll");
        action.setParams({
        	"recordType": recordType
        });
        //console.log("Action", action);
        component.set("v.accountsLoading", true);
        component.set("v.ordersLoading", true);
        var self = this;
        action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(response);
        	var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                toastEvent.setParams({
                    "title": "Success!",
                    "message": " All " + recordType + " records have been unfollowed."
                });
                if(recordType == "Account"){
                    component.set("v.followedAccounts", []);
                } else{
                    component.set("v.followedOrders", []);
                }
            }
            else {
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "An error occurred while unfollowing."
                });
            }
            toastEvent.fire();
            component.set("v.accountsLoading", false);
            component.set("v.ordersLoading", false);
        });
        $A.enqueueAction(action);
    },
    openDialog : function(component){
      var dialog = component.find('dialog');
      var evt = dialog.get("e.toggle");
      evt.setParams({ isVisible : true });
      evt.fire();
    },
    closeDialog : function(component){
      var dialog = component.find('dialog');
      var evt = dialog.get("e.toggle");
      evt.setParams({ isVisible : false });
      evt.fire();
    }
})