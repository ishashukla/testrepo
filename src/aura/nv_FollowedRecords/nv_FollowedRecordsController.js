({
    init : function(component, event, helper){
        helper.getFollowedAccounts(component);
        helper.getFollowedOrders(component);
    },
	unfollowAllAccounts : function(component, event, helper) {
        //if(!$A.util.isEmpty(component.get("v.followedAccounts"))){
            component.set("v.unfollowRecordType", "Account");
            component.set("v.confirmMessage","Performing this action will unfollow all Accounts. This means that you will no longer automatically follow new Orders related to these Accounts.");
            //Show Confirm Dialog
            helper.openDialog(component);
        //}
	},
    unfollowAllOrders : function(component, event, helper) {
        //if(!$A.util.isEmpty(component.get("v.followedOrders"))){
            component.set("v.unfollowRecordType", "Order");
            component.set("v.confirmMessage","Performing this action will unfollow all Orders. If your notification settings are set such that you receive only for 'Those I Follow', you will no longer receive notifications for these Orders.");
            //Show Confirm Dialog
            helper.openDialog(component);
        //}
	},
    onConfirm : function(component, event, helper){
        helper.closeDialog(component);
        helper.unfollowAllRecords(component, component.get("v.unfollowRecordType"));
    }
})