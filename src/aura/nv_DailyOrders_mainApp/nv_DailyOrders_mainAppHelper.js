({
	displaySVG : function(component) {
        console.log('Inside Helper Method: displaySVG' + component);
        var svg = component.find("svg_filter");
        value = svg.getElement().innerHTML;
        value = $("<div/>").html(value).text();
        svg.getElement().innerHTML = value; 

        svg = component.find("svg_sort");
        value = svg.getElement().innerHTML;
        value = $("<div/>").html(value).text();
        svg.getElement().innerHTML = value
	}, // S-408121 -Praful -  Code update starts
    getFilterOptionList : function(activeSection, tabIndex){    
        console.log('Inside Helper Method: getFilterOptionList' + activeSection);
        console.log('tabIndev==' + tabIndex); 
        var list = [];
        if (tabIndex == 3)
        {
            switch(activeSection)
            {
                    case "sales":
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Booked", isEnabled: false});
                        list.push({label:"Backordered", isEnabled: false});
                        list.push({label:"On Hold", isEnabled: false});
                        list.push({label:"Awaiting Shipping", isEnabled: false});
                        break;
                    case "bo":
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Awaiting PO", isEnabled: false});
                        break;
                    case "rma":
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Awaiting Return", isEnabled: false});
                        break;
                    case "credit":
                        //Connor Flynn S-397644 Removing filter options as they no longer apply
                        //list.push({label:"Open", isEnabled: false});
                        //list.push({label:"Closed", isEnabled: false});
                        //Connor Flynn S-397644 End
                        break;
                    default:
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Booked", isEnabled: false});
                        list.push({label:"Backordered", isEnabled: false});
                        list.push({label:"On Hold", isEnabled: false});
                        list.push({label:"Awaiting Shipping", isEnabled: false});             
                        break;
                }
        }
        else
        {
                switch(activeSection){
                    case "sales":
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Booked", isEnabled: false});
                        list.push({label:"Backordered", isEnabled: false});
                        list.push({label:"On Hold", isEnabled: false});
                        list.push({label:"Cancelled", isEnabled: false});
                        list.push({label:"Expedited", isEnabled: false});
                        list.push({label:"Awaiting Shipping", isEnabled: false});
                        list.push({label:"Partial Ship", isEnabled: false});
                        list.push({label:"Shipped", isEnabled: false});
                        list.push({label:"Closed", isEnabled: false});
                        list.push({label:"Delivered", isEnabled: false});
                        break;
                    case "bo":
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Awaiting PO", isEnabled: false});
                        list.push({label:"Closed", isEnabled: false});
                        list.push({label:"Past Due", isEnabled: false});
                        //list.push({label:"Closed", isEnabled: false});
                        break;
                    case "rma":
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Awaiting Return", isEnabled: false});
                        list.push({label:"Closed", isEnabled: false});
                        break;
                    case "credit":
                        //Connor Flynn S-397644 Removing filter options as they no longer apply
                        //list.push({label:"Open", isEnabled: false});
                        //list.push({label:"Closed", isEnabled: false});
                        //Connor Flynn S-397644 End
                        break;
                    default:
                        list.push({label:"Entered", isEnabled: false});
                        list.push({label:"Booked", isEnabled: false});
                        list.push({label:"Backordered", isEnabled: false});
                        list.push({label:"On Hold", isEnabled: false});
                        list.push({label:"Cancelled", isEnabled: false});
                        list.push({label:"Expedited", isEnabled: false});
                        list.push({label:"Awaiting Shipping", isEnabled: false});
                        list.push({label:"Partial Ship", isEnabled: false});
                        list.push({label:"Shipped", isEnabled: false});
                        list.push({label:"Closed", isEnabled: false});
                        list.push({label:"Delivered", isEnabled: false});               
                        break;
                }
        }  
        // S-408121 -Praful -  Code update  -Ends
        return list;
    },
    getSortOptionList : function(activeSort) {
        console.log('Inside Helper Method: getSortOptionList');
        console.log('Active Sort: ' + activeSort);

        var allSortOptions = document.getElementsByClassName('actionMenuItemElement');
        for (var i = 0; i < allSortOptions.length; i++) {
            var item = allSortOptions[i];
            item.style.display = 'none';
        }

        switch(activeSort) {
			case "all":
                var defaultSortOptions = document.getElementsByClassName('actionMenuItemDefault');
                for (var i = 0; i < defaultSortOptions.length; i++) {
                    var item = defaultSortOptions[i];
                    item.style.display = '';
                }
                break;
            case "credit":
                var creditSortOptions = document.getElementsByClassName('actionMenuItemCredit');
                for (var i = 0; i < creditSortOptions.length; i++) {
                    var item = creditSortOptions[i];
                    item.style.display = '';
                }
                break;
            default:
				var defaultSortOptions = document.getElementsByClassName('actionMenuItemDefault');
                for (var i = 0; i < defaultSortOptions.length; i++) {
                    var item = defaultSortOptions[i];
                    item.style.display = '';
                }
        }
	},
    getActiveOrders : function(component){
        var action = component.get("c.getActiveOrders");
                    console.log('xxxxx' + action);
        action.setCallback(this, function(response) {
            console.log('Inside Callback Open' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var ret = response.getReturnValue();
                if(ret && ret.length){
                    component.set("v.activeOrders", ret);
                } else{
                    component.set("v.activeOrders", []);
                }
				if(!$A.util.isUndefinedOrNull(component.get("v.todayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.yesterdayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.twoDayOldOrders"))){
                    component.set("v.tabsLoaded", true);
                }                 
                //debugger;
                //component.set("v.recentLoading", false);
                //component.set("v.recentAccounts", response.getReturnValue());
            }
            else{
                console.log(response.getReturnValue());
                
                component.set("v.activeOrders", []);
				if(!$A.util.isUndefinedOrNull(component.get("v.todayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.yesterdayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.twoDayOldOrders"))){
                    component.set("v.tabsLoaded", true);
                }  
            }
            if(component.get("v.tabsLoaded")){
                setTimeout(function(){
                    $(".order-footer").remove().appendTo('body');
                }, 100);
            }
        });
        $A.enqueueAction(action);
    },
    getTodayOrders : function(component){
        var action = component.get("c.getTodayOrders");
        action.setCallback(this, function(response) {
            console.log('Inside Callback Today' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var ret = response.getReturnValue();
                if(ret && ret.length){
                    component.set("v.todayOrders", ret);
                } else{
                    component.set("v.todayOrders", []);
                }
                if(!$A.util.isUndefinedOrNull(component.get("v.activeOrders")) && !$A.util.isUndefinedOrNull(component.get("v.yesterdayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.twoDayOldOrders"))){
                    component.set("v.tabsLoaded", true);
                }                
                //debugger;
                //component.set("v.recentLoading", false);
                //component.set("v.recentAccounts", response.getReturnValue());
            }
            else{
                console.log(response.getReturnValue());
                
                component.set("v.todayOrders", []);
                if(!$A.util.isUndefinedOrNull(component.get("v.activeOrders")) && !$A.util.isUndefinedOrNull(component.get("v.yesterdayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.twoDayOldOrders"))){
                    component.set("v.tabsLoaded", true);
                }   
            }
            if(component.get("v.tabsLoaded")){
                setTimeout(function(){
                    $(".order-footer").remove().appendTo('body');
                }, 100);
            }
        });
        $A.enqueueAction(action);
    },
    getYesterdayOrders : function(component){
        var action = component.get("c.getYesterdayOrders");
        action.setCallback(this, function(response) {
            console.log('Inside Callback Yesterday' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var ret = response.getReturnValue();
                if(ret && ret.length){
                    component.set("v.yesterdayOrders", ret);
                } else{
                    component.set("v.yesterdayOrders", []);
                }
                if(!$A.util.isUndefinedOrNull(component.get("v.activeOrders")) && !$A.util.isUndefinedOrNull(component.get("v.todayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.twoDayOldOrders"))){
                    component.set("v.tabsLoaded", true);
                } 
                //debugger;
                //component.set("v.recentLoading", false);
                //component.set("v.recentAccounts", response.getReturnValue());
            }
            else{
                console.log(response.getReturnValue());
                
                component.set("v.yesterdayOrders", []);
                if(!$A.util.isUndefinedOrNull(component.get("v.activeOrders")) && !$A.util.isUndefinedOrNull(component.get("v.todayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.twoDayOldOrders"))){
                    component.set("v.tabsLoaded", true);
                } 
            }
            if(component.get("v.tabsLoaded")){
                setTimeout(function(){
                    $(".order-footer").remove().appendTo('body');
                }, 100);
            }
        });
        $A.enqueueAction(action);
    },
    getTwoDayOldOrders : function(component){
        var action = component.get("c.getTwoDayOldOrders");
        action.setCallback(this, function(response) {
            console.log('Inside Callback TwoDayOld' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var ret = response.getReturnValue();
                if(ret && ret.length){
                    component.set("v.twoDayOldOrders", ret);
                } else{
                    component.set("v.twoDayOldOrders", []);
                }
                if(!$A.util.isUndefinedOrNull(component.get("v.activeOrders")) && !$A.util.isUndefinedOrNull(component.get("v.todayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.yesterdayOrders"))){
                    component.set("v.tabsLoaded", true);
                } 
                //debugger;
                //component.set("v.recentLoading", false);
                //component.set("v.recentAccounts", response.getReturnValue());
            }
            else{
                console.log(response.getReturnValue());
                component.set("v.twoDayOldOrders", []);
                if(!$A.util.isUndefinedOrNull(component.get("v.activeOrders")) && !$A.util.isUndefinedOrNull(component.get("v.todayOrders")) && !$A.util.isUndefinedOrNull(component.get("v.yesterdayOrders"))){
                    component.set("v.tabsLoaded", true);
                } 
            }
            if(component.get("v.tabsLoaded")){
                setTimeout(function(){
                    $(".order-footer").remove().appendTo('body');
                }, 100);
            }
        });
        $A.enqueueAction(action);
    },
})