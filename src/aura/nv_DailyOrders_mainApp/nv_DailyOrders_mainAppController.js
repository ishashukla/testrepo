({
    init : function(component, event, helper){
        var today = new Date();
        //var today = new Date(2015,9,14);
        var dayAbbreviations = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
        var day = today.getDay();
        if(day == 0 || day == 6 || day == 1){
            component.set("v.todayString", dayAbbreviations[1]);
            component.set("v.yesterdayString", dayAbbreviations[5]);
            component.set("v.twoDaysString", dayAbbreviations[4]);
        }
        else if(day == 2){
            component.set("v.todayString", dayAbbreviations[2]);
            component.set("v.yesterdayString", dayAbbreviations[1]);
            component.set("v.twoDaysString", dayAbbreviations[5]);
        }
        else{
           component.set("v.todayString", dayAbbreviations[day--]);
           component.set("v.yesterdayString", dayAbbreviations[day--]);
           component.set("v.twoDaysString", dayAbbreviations[day]);
        }
        /*
        if(day == 0 || day == 6){ day = 1;}
        component.set("v.todayString", dayAbbreviations[day]);
        day--;
        if(day == 0 || day == 6){ day = 1;}
        component.set("v.yesterdayString", dayAbbreviations[day]);
        day--;
        if(day == 0 || day == 6){ day = 1;}
        component.set("v.twoDaysString", dayAbbreviations[day]);
		*/
        // S-408121 - Code update Praful -start
         var tabIndex = event.getParams("tabIndex").tabIndex;
        var filterList = helper.getFilterOptionList("all", tabIndex);
        // S-408121 - Code update Praful -Ends
        component.set("v.filterList", filterList);
        helper.getActiveOrders(component);
		helper.getTodayOrders(component);
        helper.getYesterdayOrders(component);
        helper.getTwoDayOldOrders(component);
        
        //Trigger Update
        var appEvent = $A.get("e.c:nv_DailyOrdersInitComplete");
        appEvent.fire();
    },
	doInit : function(component, event, helper) {
        component.set("v.isInitialized", true);
        if(component.get("v.isDoneRendering")){
            helper.displaySVG(component);
        }
        //alert("ORIGINAL HASH: " + location.hash);
        //component.set("v.originalHash", location.hash.substr(1));
        //console.log("RecordTypeId: ", component.get("m.contactRecordTypeId"));
  	},

    onSelectOrder : function(component, event, helper){
        var select = component.find("orderSelect").getElement();
        var value = select.value;
        helper.fireSelectOrder(value);
    },
    updateRender : function(component, event, helper){
        //We hide the footer when not showing results.
        /*var loc = event.getParam("token");
        if(loc == component.get("v.originalHash") || loc.indexOf("/") == -1){
            $(".order-footer").css("visibility", "visible");
            //alert("FOOTER SHOWN");
        }*/
    },
    displaySVG : function(component, event, helper){
        //BJ - This is a very hacky solution. But, using after-render causes the back/reload functionality to break.
        if(!component.get("v.isDoneRendering") && component.get("v.isInitialized")){
            component.set("v.isDoneRendering", true);
            helper.displaySVG(component);
        } else{
            component.set("v.isDoneRendering", true);
        }
    },
    updateTotal : function(component, event, helper){
        var tabIndex = event.getParams("tabIndex").tabIndex;
        var changeFilters = event.getParams("changeFilters").changeFilters;
        var filterString = "all";
        //console.log("Change Filters: " + changeFilters);
        component.set("v.currentTab", tabIndex);
        switch(tabIndex){
            case 0:
                component.set("v.rollupBookedTotal", component.find("twoDaysTab").get("v.rollupBooking"));
                component.set("v.rollupShippedTotal", component.find("twoDaysTab").get("v.rollupShipping"));
                if(changeFilters){
                    filterString = component.find("twoDaysTab").get("v.activeFilter");
                }
                break;
            case 1:
                component.set("v.rollupBookedTotal", component.find("yesterdayTab").get("v.rollupBooking"));
                component.set("v.rollupShippedTotal", component.find("yesterdayTab").get("v.rollupShipping"));
                if(changeFilters){
                    filterString = component.find("yesterdayTab").get("v.activeFilter");
                }
                break;
            case 2:
                component.set("v.rollupBookedTotal", component.find("todayTab").get("v.rollupBooking"));
                component.set("v.rollupShippedTotal", component.find("todayTab").get("v.rollupShipping"));
                if(changeFilters){
                    filterString = component.find("todayTab").get("v.activeFilter");
                }
                break;
            case 3:
                //Added by Harendra for story S-412528
                component.set("v.rollupOpenTotal", component.find("activeTab").get("v.rollupOpen"));
                component.set("v.rollupBookedTotal", component.find("activeTab").get("v.rollupBooking"));
                component.set("v.rollupShippedTotal", component.find("activeTab").get("v.rollupShipping"));
                if(changeFilters){
                    filterString = component.find("activeTab").get("v.activeFilter");
                }
                break;
            default:
                //Added by Harendra for story S-412528
                component.set("v.rollupOpenTotal",0);
                component.set("v.rollupBookedTotal", 0);
                component.set("v.rollupShippedTotal", 0);
        }
        if(changeFilters){
            // S-408121 - Code update Praful
            var filterList = helper.getFilterOptionList(filterString, tabIndex);
            component.set("v.filterList", filterList);
            var appEvent = $A.get("e.c:nv_DailyOrders_filterSelected");
            appEvent.setParams({"filters" : filterList, "tab" : tabIndex});
            appEvent.fire();
        }
    },
    updateFilters: function(component, event, helper){
        // S-408121 - Code update Praful
        var filterList = helper.getFilterOptionList(event.getParams("filterString").filterString, event.getParams("tabIndex").tabIndex);
        component.set("v.filterList", filterList);
        console.log('filterList', filterList);
        var appEvent = $A.get("e.c:nv_DailyOrders_filterSelected");
        appEvent.setParams({"filters" : filterList});
        appEvent.fire();
    },
    sortActiveTab: function(component, event, helper){
        //console.log("Display Sort Options");
        //console.log("Event: ", event);
        console.log("Event Source: ", event.source.get("v.label"));
        var sortString = "dateAsc";
        switch(event.source.get("v.label")){
            case "Most Recent First":
                sortString = "dateAsc";
                break;
            case "Oldest First":
                sortString = "dateDesc";
                break;
            case "Alphabetically":
                sortString = "alphAsc";
                break;
            case "Reverse Alphabetically":
                sortString = "alphDesc";
                break;
            case "Largest First":
                sortString = "amntDesc";
                break;
            case "Smallest First":
                sortString = "amntAsc";
                break;
        }
        
        var appEvent = $A.get("e.c:nv_DailyOrders_changeSort");
        appEvent.setParams({"sortString" : sortString, "tab" : component.get("v.currentTab")});
        appEvent.fire();
    },
    filterActiveTab: function(component, event, helper){
        //console.log("Filter Event: ", event);
        setTimeout(function(){
            //console.log("ACTIVEFilters: ", component.get("v.filterList"), new Date());
            var appEvent = $A.get("e.c:nv_DailyOrders_filterSelected");            
            appEvent.setParams({"filters" : component.get("v.filterList"), "tab" : component.get("v.currentTab")});
            appEvent.fire();
        }, 11);
        //Start - Added by Harendra for story S-412528
        var filterValue = component.get("v.filterList");
        
		//Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - Start
		var isSelected = false;
        for(var i = 0; i < filterValue.length; i++){
            
            var filterVal = filterValue[i];
            if(filterVal.isEnabled == true) {
                
                isSelected = true;
            }
        }
        
        //Code commented
        /**if(filterValue != ''){**/
        if(isSelected){
		//Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - End
            component.set("v.IsFilterSelected",true);
        }
        else{
            component.set("v.IsFilterSelected",false);
        }
        //End - Added by Harendra for story S-412528
    },
    openModal: function(component, event, helper){
        component.set("v.displayingModal", true);
        var orderId = event.getParams("orderId").orderId;

        $A.createComponent(
            "c:nv_OrderView",
            {
                "aura:Id": "findableAuraId",
                "orderId" : orderId,
                "redirectView" : false,
                /*"contactRecordTypeId" : component.get("m.contactRecordTypeId")*/
            },
            function(orderView){
                //Add the new button to the body array
                if (component.isValid()) {
                    component.set("v.modalComponent", orderView);
                    $('.scroller').css("transform", "");
                    /*var action = $A.get("e.ui:updateSize");
                    action.fire(); */

                }
            }
        );


        $(".order-footer").css("visibility", "hidden");
    },
    closeModal: function(component, event, helper){
        component.set("v.displayingModal", false);
        $(".order-footer").css("visibility", "visible");
    }
})