({
	showFedEx : function(component, event, helper){
        var trackingNumber = component.get("v.trackingNumber");
        var navToSObjEvt = $A.get("e.force:navigateToURL");
        navToSObjEvt.setParams({
            url: "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=" + trackingNumber + "&cntry_code=us"
        });
        navToSObjEvt.fire();
    }
})