({
	getEmailMessage : function(order) {
		var message = "";
        
        /*
        	MESSAGE TEMPLATE:
            Account Name
            Account Number
            PO Number
            Order Shipped
                FedEx Tracking Number 1 w/ Link
                FedEx Status
                Product Description and Quantitiy
                FedEx Tracking Number 2 w/ Link
                FedEx Status
                Product Description and Quantitiy
            Order Not Shipped
                Product Description and Quantitiy
        */
        
        /* lineBrk is "\r\n" for Android
         * lineBrk is <br/>  for iPhone
         * By jyotirmaya 6/7/2016,152280
         * Code started by jyotirmaya                
        */
        var lineBrk = "";
        // Check for Iphone
        var isMobile = {
                   iOS: function() {
                   return navigator.userAgent.match(/iPhone|iPad|iPod/i);
               }};        
        if(isMobile){
           lineBrk = "<br/>";
        }        
        // Check for Android
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
        if(isAndroid) {         
          // Redirect to Android-site?
          	lineBrk = "\r\n";
        }
        /* Code Ended By Jyotirmaya */ 
		
        message += order.AccountName + lineBrk;  
        message += "Account Number: " + order.AccountNumber + lineBrk;
        message += "PO Number: " + order.PONumber + lineBrk + lineBrk;
        
        if(order.ShippedOrderItems){
            if(order.ShippedOrderItems.length > 0){
                message += "Order Shipped:" + lineBrk;
            }
            for(var s = 0; s < order.ShippedOrderItems.length; s++){
                var shipment = order.ShippedOrderItems[s];
                message += "FedEx Tracking Number: " + shipment.FedExTrackingNumber;
                message += " ( https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=" + shipment.FedExTrackingNumber + " )\n";
                if(shipment.OrderItems.length > 0){
                    if(!$A.util.isEmpty(shipment.OrderItems[0].TrackingStatus)){
                        message += "Status: " + shipment.OrderItems[0].TrackingStatus + lineBrk;
                    }
                    message += "Products: " + lineBrk;
                    for(var i = 0; i < shipment.OrderItems.length; i++){
                        message += shipment.OrderItems[i].Description + " x " + shipment.OrderItems[i].Quantity + lineBrk;
                    }
                }
                message += lineBrk;
            }
            message += lineBrk;
        }
        if(order.NotShippedOrderItems && order.NotShippedOrderItems.length > 0){
            if(order.NotShippedOrderItems.length == 1 && (!$A.util.isEmpty(order.NotShippedOrderItems[0].OrderItems) && order.NotShippedOrderItems[0].OrderItems.length > 0)){
                message += "Order Not Shipped:" + lineBrk;
            }
            for(var s = 0; s < order.NotShippedOrderItems.length; s++){
                var shipment = order.NotShippedOrderItems[s];
                if(shipment.OrderItems.length > 0){
                    for(var i = 0; i < shipment.OrderItems.length; i++){
                        message += shipment.OrderItems[i].Description + " x " + shipment.OrderItems[i].Quantity + lineBrk;
                    }
                }
                message +=lineBrk;
            }
        }
        message = encodeURIComponent(message);
        console.log(message);
        return message;
	}
})