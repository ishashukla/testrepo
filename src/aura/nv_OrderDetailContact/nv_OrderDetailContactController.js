({
	updatePrimary : function(component, event, helper) {
		var checkbox = event.getSource();
        var isPrimary = checkbox.get("v.value");
        var contact = component.get("v.contact");
        var action = component.get("c.setPrimary");
        action.setParams({cId: contact.ContactId, isPrimary: isPrimary});
        $A.enqueueAction(action);
	},
    makeInvisible : function(component, event, helper){
        var contact = component.get("v.contact");
        var action = component.get("c.removeContact");
        action.setParams({cId: contact.ContactId});
        action.setCallback(this, function(a){
            var removeEvent = $A.get("e.c:nv_OrderDetailContactRemoved");
            removeEvent.setParams({contactId : contact.ContactId}).fire();
        })
        $A.enqueueAction(action);
    },
    editContact: function(component, event, helper){
     	var contact = component.get("v.contact");
        var sObjId = contact.ContactId;
        var navToSObjEvt = $A.get("e.force:editRecord");
        navToSObjEvt.setParams({
            recordId: sObjId
        });
        navToSObjEvt.fire();
    },
    emailContact: function(component, event, helper){
        var email = "mailto:" + component.get("v.contact.Email");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": email
        });
        urlEvent.fire();
    },
    emailContactDetails: function(component, event, helper){
        var email = "mailto:" + component.get("v.contact.Email") + "?subject=Your%20Order%20Details&body=";
        var orderDetails = component.get("v.order");
        //console.log(orderDetails);
        email += helper.getEmailMessage(orderDetails);
        /*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": email
        });
        urlEvent.fire();*/
		window.location = email;
    },
    callOffice: function(component, event, helper){
        var mobileNumber = component.get("v.contact.OfficeNumber");
        mobileNumber = mobileNumber.replace(/\(/g, '');
        mobileNumber = mobileNumber.replace(/\)/g, '');
        mobileNumber = mobileNumber.replace(/-/g, '');
        mobileNumber = mobileNumber.replace(/\s/g, '');
        var officeNumber = "tel:" + mobileNumber;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": officeNumber
        });
        urlEvent.fire();
    },
    callMobile: function(component, event, helper){
        var mobileNumber = component.get("v.contact.MobileNumber");
        mobileNumber = mobileNumber.replace(/\(/g, '');
        mobileNumber = mobileNumber.replace(/\)/g, '');
        mobileNumber = mobileNumber.replace(/-/g, '');
        mobileNumber = mobileNumber.replace(/\s/g, '');
        var officeNumber = "tel:" + mobileNumber;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": officeNumber
        });
        urlEvent.fire();
    },
    textContact: function(component, event, helper){
        var mobileNumber = "sms:" + component.get("v.contact.MobileNumber");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": mobileNumber
        });
        urlEvent.fire();
    }
})