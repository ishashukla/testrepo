({
	unfollowRecord : function(component) {
		//Call the getFollowedOrders Apex class method
        var action = component.get("c.unfollowRecord");
        var object = component.get("v.object");
        //console.log("Action", action);
        action.setParams({
        	"theRecordId": object.theNVFollowRecord.Id
        });
        var self = this;
        action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(response);
            component.set("v.isDisplayed", false);
        	var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been unfollowed."
                });
            }
            else {
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "An error occurred while unfollowing."
                });
            }
            toastEvent.fire();
        });
        $A.enqueueAction(action);
	}
})