({
	toggleActive : function(component, event, helper) {
        var productLine = component.get("v.productLine");
        productLine.isActive = !productLine.isActive;
        component.set("v.productLine", productLine);
	},
    uncheckAll : function(component, event, helper){
        var productLine = component.get("v.productLine");
        
        for(var s = 0; s < productLine.segments.length; s++){
            var segment = productLine.segments[s];
            if(!segment.isVisible){
                segment.allProductsActive = false;
                for(var p = 0; p < segment.productsList.length; p++){
                    segment.productsList[p].isActive = false;
                }
            }
        }
        component.set("v.productLine", productLine);
    }
})