({
    doInit : function(component, event, helper) {
        //TODO: Sort Primary Contacts to top, and iterate through Contacts.
    },
    togglePanel : function(component, event, helper) {
        //$(event.target).parents(".panel").toggleClass("collapsed").toggleClass("open").toggleClass("selected");
        if(component.get("v.panelExpanded")){
            component.set("v.panelClass", "panel collapsed");
            component.set("v.panelExpanded", false);
        } else{
            component.set("v.panelClass", "panel selected");
            component.set("v.panelExpanded", true);
        }   
    },
    removeContact : function(component, event, helper){
        var contacts = component.get("v.contacts");
        var cId = event.getParam("contactId");
        var contactList = [];
        if(contacts){
            for(c in contacts){
                if(contacts[c].ContactId != cId){
                    contactList.push(contacts[c]);
                }
            }
            component.set("v.contacts", contactList);
        }
    },
    createContact: function(component, event, helper){
        var createRecordEvent = $A.get("e.force:createRecord");
        var recordType = component.get("v.contactRecordTypeId");
        if(!$A.util.isEmpty(recordType)){
            console.log("NOT EMPTY!");
            createRecordEvent.setParams({"entityApiName":"Contact", "recordTypeId" : recordType});
        } else{
            createRecordEvent.setParams({"entityApiName":"Contact"});
        }
        createRecordEvent.fire();
    }
})