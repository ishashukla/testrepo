({
	init : function(component, event, helper) {
        try {
            var action = component.get("c.getSalesTotalsAndUsersForToday");
            action.setCallback(this, function(response) {
                if(response.getState() === "SUCCESS") {
                    var responseData = response.getReturnValue();
                    console.log("RESPONSE DATA: ", responseData);
                    var regionalData = {};
                    regionalData.ShippedTotal = responseData.RegionalShippedTotal;
                    regionalData.BookedTotal = responseData.RegionalBookedTotal;
                    regionalData.OpenTotal = responseData.RegionalOpenTotal;
                    regionalData.PendingPOTotal = responseData.RegionalPendingPOTotal;
                    regionalData.CreditTotal = responseData.RegionalCreditTotal;
    
                    regionalData.users = responseData.users;//[];
                    /*var theUserDataMap = responseData.userWrapperMap;
                    for(var key in theUserDataMap) {
                        var object = theUserDataMap[key];
    
                        var userData = {};
                        userData.UserId = object.UserId;
                        userData.ShippedTotal = object.ShippedTotal;
                        userData.BookedTotal = object.BookedTotal;
                        userData.OpenTotal = object.OpenTotal;
                        userData.PendingPOTotal = object.PendingPOTotal;
                        userData.CreditTotal = object.CreditTotal;
    
                        userData.FirstName = object.User.FirstName;
                        userData.LastName = object.User.LastName;
    
                        regionalData.users.push(userData);
                    }*/
                    component.set("v.regionalData", regionalData);
                }
                else if(response.getState() === "ERROR") {
                    $A.log("Errors", response.getError());
                }
            });
            $A.enqueueAction(action);
        }
        catch(e) {
			console.log('Exception Message: ' + e.message);
			alert('An internal error occurred while processing your request. Please contact system administrator.');
        }
	}
})