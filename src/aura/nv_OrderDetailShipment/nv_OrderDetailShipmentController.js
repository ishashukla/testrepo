({
   doInit : function(component, event, helper) {
        //Remove red color if shipped.
        var shipped = component.get('v.shipped');
        if(shipped){
            component.set('v.panelClass', 'panel collapsed');
        }
    },
    togglePanel : function(component, event, helper) {
        var shipped = component.get('v.shipped');
        var orderRecordType = component.get('v.orderRecordType');
        if(component.get("v.panelExpanded")){
            if(shipped){
                component.set("v.panelClass", "panel collapsed");
            } else{
                component.set("v.panelClass", "panel collapsed danger");
            }
            component.set("v.panelExpanded", false);
        } else{
            if(shipped || orderRecordType == 'NV_Bill_Only'){
                component.set("v.panelClass", "panel selected");
            } else{
                component.set("v.panelClass", "panel selected danger");
            }
            component.set("v.panelExpanded", true);
        }
	},
    showFedEx : function(component, event, helper){
        var trackingNumber = component.get("v.orderItemWithTrackingDetails.FedExTrackedNumber");
        var navToSObjEvt = $A.get("e.force:navigateToURL");
        navToSObjEvt.setParams({
            url: "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=" + trackingNumber + "&cntry_code=us"
        });
        navToSObjEvt.fire();
    }
})