({
    doInit: function(component, evt, helper) {
        
        var actionUrl = component.get("c.getStrykerPulseiUrl");
        actionUrl.setCallback(this, function(a) {
            console.log('get back from server:', a.getReturnValue());
            if(a.getReturnValue != null) {
                var oURL = component.find("optURL");
                oURL.set("v.value", a.getReturnValue());
				oURL.set("v.label", 'Stryker Pulse');
			}
        });
        
        $A.enqueueAction(actionUrl);
    },
    
})