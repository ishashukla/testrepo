({
	toggleDialog : function(component, event, helper) {
			var isVisible = event.getParam("isVisible");
      component.set("v.isVisible", isVisible);
	},
	closeDialog : function(component, event, helper) {
		  component.set("v.isVisible", false);
			//$A.util.removeClass(component.find("_sldx_dialog").getElement(), "hideEl");
	}
})