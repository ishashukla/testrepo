({
    init : function(component, event, helper){
        var orderItem = component.get("v.orderItem");
        var headerHold = component.get("v.headerHold");
        var holdDescription = orderItem.HoldDescription;
        if(!$A.util.isEmpty(headerHold)){
            holdDescription = headerHold + (typeof holdDescription === "string" ? ";" + holdDescription : "");
        }
        if(typeof holdDescription === "string"){
            component.set("v.holdReason", holdDescription.split(";"));
            orderItem.OnHold = true;
            component.set("v.orderItem", orderItem);
        }
		
		var statusClass = "default";
        if(orderItem.Status == "Shipped" || orderItem.Status == "Closed" || orderItem.Status == "Delivered"){
            statusClass = "shipped";
        } else if(orderItem.Status == "Partial Ship"){
            statusClass = "partial";
        } else if(orderItem.Status == "Partial Shipping" || orderItem.Status == "Backordered"){
            statusClass = "hold";
        }
        if(orderItem.OnHold){
            statusClass = "hold";
        }
        var uniqueTrackingNumbers = [];
        var trackedNumber = component.get("v.trackedNumber");
        if(!$A.util.isEmpty(trackedNumber) && !$A.util.isEmpty(orderItem.FedExTrackingNumber)){
            var trackingNumberString = orderItem.FedExTrackingNumber;
            var uniqueTracking = {};
            if(trackingNumberString.indexOf(";")){
                var trackingArray = trackingNumberString.split(";");
                for(var i = 0; i < trackingArray.length; i++){
                    var number = trackingArray[i];
                    if(number != trackedNumber && !uniqueTracking[number]){
                        uniqueTracking[number] = true;
                        uniqueTrackingNumbers.push(number);
                    }
                }
            }
        }
        component.set("v.uniqueTrackingNumbers", uniqueTrackingNumbers);
		component.set("v.itemStatusClass", statusClass);
    },
    navigateToOrderItemDetail : function(component,event,helper){
        var orderItemId = component.get("v.orderItem").OrderItemId;
        helper.navigateToSObject(orderItemId);
    }, 
})