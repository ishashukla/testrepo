({
	getOrderDetails : function(component) {
		console.log('[nvOrderViewController] Inside getOrderDetails');
		var self = this;		
      	var action = component.get("c.getOrderDetailByOrderId");
		action.setParams({ orderId : component.get("v.orderId") });
      	action.setCallback(this, function(response) {
	        console.log('Inside Callback ' + response.getState());
		    var state = response.getState();
		    if (state === "SUCCESS") {
				console.log(response.getReturnValue());
	            //debugger;
				component.set("v.orderDetail", response.getReturnValue());
		    }
			else{
				console.log(response.getReturnValue());
			}
			/*setTimeout(function(){
			  var act = $A.get("e.ui:updateSize"); 
			    act.fire();
			}, 10);*/
        });
        $A.enqueueAction(action);
	}
})