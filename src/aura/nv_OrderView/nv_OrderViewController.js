({
    doInit : function(component, event, helper){
        console.log('[nvOrderViewController] Calling doInit');
        //debugger;
        if(!component.get("v.redirectView")){
        } else{
            $("html").css("height", "100%").css("width", "100%").css("overflow", "auto");
            $("body").css("height", "100%").css("width", "100%").css("overflow", "auto");
        }
      	helper.getOrderDetails(component);
    },
	togglePanel : function(component, event, helper) {
		    //console.log(event.target);
        //console.log($(event.target).parents(".panel"));
        //$(event.target).parents(".panel").toggleClass("collapsed").toggleClass("open").toggleClass("selected");
		if(component.get("v.panelExpanded")){
            component.set("v.panelClass", "panel collapsed");
            component.set("v.panelExpanded", false);
        } else{
            component.set("v.panelClass", "panel selected");
            component.set("v.panelExpanded", true);
        }   
        event.stopPropagation();
  },
  waiting : function (component, event, helper) {
      var spinner = component.find('spinner');
      var evt = spinner.get("e.toggle");
      evt.setParams({ isVisible : true });
      evt.fire();
  },
  doneWaiting : function (component, event, helper) {
      var spinner = component.find('spinner');
      var evt = spinner.get("e.toggle");
      evt.setParams({ isVisible : false });
      evt.fire();
  }
})