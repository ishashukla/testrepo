({
	checkAllProducts : function(segment, component) {
        segment.allProductsActive = !segment.allProductsActive;
        for(var p = 0; p < segment.productsList.length; p++){
            segment.productsList[p].isActive = segment.allProductsActive;
        }
        component.set("v.segment", segment);
	},
	retrieveProducts : function(component, helper) {
		var segment = component.get("v.segment");
		var productLine = component.get("v.productLineName");
        console.log('Loading products for segment '+segment.SegmentName+', productLine '+productLine+', active: '+segment.allProductsActive);
        component.set("v.productsLoading", true);
        
        var action = component.get("c.getProductsForSegment");
        action.setParams({segment : segment.SegmentName, productLine: productLine});
        action.setCallback(this, function(response) {
            console.log('Inside Callback ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnedProducts = response.getReturnValue();

                for (var i = 0; i < returnedProducts.length; i++) {
                	returnedProducts[i].isVisible = true;
                	returnedProducts[i].isActive = segment.allProductsActive;
                }

                component.set("v.segment.productsList", returnedProducts);
                component.set("v.productsLoading", false);
                component.set("v.alreadyRetrievedProductsForSegment", true);

                var cmpEvent = component.getEvent("segmentSelected");
                cmpEvent.fire();
                event.stopPropagation();
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
	}
})