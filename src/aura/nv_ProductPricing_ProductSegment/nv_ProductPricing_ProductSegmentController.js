({
    toggleActive : function(component, event, helper) {
        var segment = component.get("v.segment");
        segment.isActive = !segment.isActive;
        component.set("v.segment", segment);

        if (segment.isActive && !component.get("v.alreadyRetrievedProductsForSegment") && !component.get("v.alreadyRetrievedProducts") && component.get("v.isBigDataMode")) {
            helper.retrieveProducts(component, helper);
        }
	},
    toggleCheckAll : function(component, event, helper){
        var segment = component.get("v.segment");

        helper.checkAllProducts(segment, component);        

        if (!component.get("v.alreadyRetrievedProductsForSegment") && !component.get("v.alreadyRetrievedProducts") && component.get("v.isBigDataMode")) {
            helper.retrieveProducts(component, helper);
        } else {
            var cmpEvent = component.getEvent("segmentSelected");
            cmpEvent.fire();
            event.stopPropagation();
        }

    },
    uncheckAll : function(component, event, helper){
        console.log("WE RECEIVED THE EVENT!");
        var segment = component.get("v.segment");
        segment.allProductsActive = true; //Ensures that it will go through the uncheck process.
        helper.checkAllProducts(segment, component);
    },
    productSelected : function(component, event, helper){
        console.log("PRODUCT SELECTED CALLED");
        var segment = component.get("v.segment");
        var selectedCount = 0;
        for(var p = 0; p < segment.productsList.length; p++){
            if(segment.productsList[p].isActive){
                selectedCount++;
            }
        }
        segment.allProductsActive = (selectedCount == segment.productsList.length);
        component.set("v.segment", segment);
    }
})