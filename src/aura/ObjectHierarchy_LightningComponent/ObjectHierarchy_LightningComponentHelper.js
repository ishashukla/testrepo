({
	toggleLoading : function(component) {
		var loadingSpinner = component.find("loadingSpinner");
		$A.util.toggleClass(loadingSpinner, "hidden");	
	},
    
	showMessage : function(component, message, isError) {
		var msgPanel = component.find("msgPanel");
		$A.util.removeClass(msgPanel, "hidden");
        component.set('v.messageText', message);
        var messageHead = component.find("messageHead");
		if(isError){
            component.set('v.messageType', 'ERROR:');
			$A.util.addClass(messageHead, 'slds-theme--error');
		}
		else{
            component.set('v.messageType', 'WARNING:');
			$A.util.addClass(messageHead, 'slds-theme--warning'); 
		}        
	}, 
    
    navigateToRecord : function(recordId) {
        var event = $A.get("e.force:navigateToSObject");
        if(event){
            event.setParams({"recordId": recordId});
            event.fire();
		}
    },
    
    getHierarchyData : function(component, helper) {
		var action = component.get("c.getObjectHierarchyData");
		action.setParams({ 'recordId' : component.get('v.recordId'),
                          'nameFieldAPIName': component.get('v.titleField'),
                          'parentFieldAPIName': component.get('v.parentField'),
                          'fieldsToDisplay': component.get('v.fieldsToDisplay'),
                          'hierarchyLevel':component.get('v.hierarchy')});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
                var result = response.getReturnValue();
				if(result){
                    if(result.error){
						helper.showMessage(component, result.error, true);   
                    }
                    else{
                        helper.generateTree(component, helper, result);
                        if(result.warning){
                            helper.showMessage(component, result.warning, false);  
                        }    
                    }    
                }
                else{
                    helper.showMessage(component, 'Getting error while retriving data. Contact your System Administrator.', true);
                }
			}
			else {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
                        helper.showMessage(component, errors[0].message ,true);
					}
				} else {
					console.log("Unknown error");
				}
			}
            helper.toggleLoading(component);
		});
        action.setExclusive();
		$A.enqueueAction(action);
	},

    generateTree : function(component, helper, treeData){
        var currentNodeId = component.get('v.recordId');
        var hierarchyTree = component.find('hierarchyTree');
        $(hierarchyTree.elements[0]).jstree({
            'plugins': ["grid"],
            'grid': {
                'columns':treeData.columns,
                'resizable':true
            },
            'core': {
                'data': treeData.data,
                'worker' : false,
                'themes': {
                    'name':'proton',
                    "icons":false,
                    'responsive':true
                }
            }
        })
        .on("activate_node.jstree", function(e,data){
            var currentNodeIdTemp = component.get('v.recordId');
            $('div[id^="jsgrid_"]').removeClass("dataCellActive");
            if(currentNodeIdTemp != data.node.id){
				helper.toggleLoading(component);
                $('div[id^="jsgrid_'+data.node.id+'_"]').addClass("dataCellActive");
				helper.navigateToRecord(data.node.id);
            }
            else{
                $('a[id^="'+data.node.id+'_anchor"]').removeClass('jstree-clicked');
            }
        })
        .on("hover_node.jstree",function(e,data){
            $('div[id^="jsgrid_'+data.node.id+'_"]').addClass("dataCellHover");
        })
        .on("dehover_node.jstree",function(e,data){
            $('div[id^="jsgrid_'+data.node.id+'_"]').removeClass("dataCellHover");
        })
        .on("open_node.jstree",function(e,data){
            var currentNodeIdTemp = component.get('v.recordId');
            $('a[id^="'+currentNodeIdTemp+'_anchor"]').addClass("boldText");
        })
        .on('ready.jstree', function (event, data) {
            var currentNodeIdTemp = component.get('v.recordId');
            data.instance._open_to(currentNodeIdTemp);
            $('a[id^="'+currentNodeIdTemp+'_anchor"]').addClass("boldText");           
        });         
    }
})