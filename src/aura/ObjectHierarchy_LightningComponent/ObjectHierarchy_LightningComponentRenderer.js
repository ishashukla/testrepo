({
    afterRender : function(component, helper) {
        helper.toggleLoading(component);
		return this.superAfterRender();
    }
})