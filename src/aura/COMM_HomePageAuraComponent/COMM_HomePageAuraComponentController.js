({
    doInit: function(component, evt, helper) {
        var action = component.get("c.getTotalSales");
        action.setCallback(this, function(a) {
            if(a.getReturnValue() !=null && a.getReturnValue()[0] != null && a.getReturnValue()[0] != ''){ //NB - 11/23 - I-245147
                component.set("v.quota", a.getReturnValue()[0]);
            } else {
                component.set("v.quota", 0);
            }
            if(a.getReturnValue() !=null && a.getReturnValue()[1] != null && a.getReturnValue()[1] != ''){//NB - 11/23 - I-245147
                component.set("v.ordersYTD", a.getReturnValue()[1]);
            } else {
                component.set("v.ordersYTD", 0);
            }
            if(a.getReturnValue() !=null && a.getReturnValue()[2] != null && a.getReturnValue()[2] != ''){//NB - 11/23 - I-245147
                component.set("v.quotaPTQ", a.getReturnValue()[2]);
            } else {
                component.set("v.quotaPTQ", 0);
            }
            if(a.getReturnValue() !=null && a.getReturnValue()[3] != null && a.getReturnValue()[3] != ''){//NB - 11/23 - I-245147
                component.set("v.show", a.getReturnValue()[3]);
            } else {
                component.set("v.show", 0);
            }
        });
        
        //NB - 11/10- I-243430 - Show Stryker Pulse URL Start
        var actionUrl = component.get("c.getStrykerPulseiUrl");
        actionUrl.setCallback(this, function(b) {
            console.log('get back from server:', b.getReturnValue());
            if(b.getReturnValue() != null) {//NB - 11/23 - I-245147
                var oURL = component.find("oURL");
                oURL.set("v.value", b.getReturnValue());
				oURL.set("v.label", 'Stryker Pulse');
			}
        });
        $A.enqueueAction(action);
        $A.enqueueAction(actionUrl);
        //NB - 11/10- I-243430 - Show Stryker Pulse URL End
    },
    
})