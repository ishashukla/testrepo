({
	toggleActive : function(component, event, helper) {
        var displayDivider = component.get("v.displayDivider");
        if(displayDivider){
            component.set("v.displayDivider", false);
            component.set("v.class", "parent");
        } else{
            component.set("v.displayDivider", true);
            component.set("v.class", "parent active");
        }
		//$A.util.toggleClass(component, 'active');
	}
})