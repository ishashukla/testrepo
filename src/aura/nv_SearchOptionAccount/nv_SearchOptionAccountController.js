({
    init : function(component, event, helper){
		var account = component.get("v.account");
        if(account.AccountNumber && !$A.util.isEmpty(account.AccountNumber)){
            account.AccountNumber.replace("NV-", "");
        }
    },
	setSearchOption : function(component, event, helper) {
		var sObj = component.get("v.account");
        var navToSObjEvt = $A.get("e.c:nv_SearchOptionSelected");
        navToSObjEvt.setParams({
            option: sObj
        });
        navToSObjEvt.fire();
	}
})