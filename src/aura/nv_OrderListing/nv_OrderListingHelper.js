({
	buildFromList : function(component, list, filterList) { 
        var overallBookedTotal = 0.0;
        var overallShippedTotal = 0.0;
        var overallOpenTotal = 0.0; //Added by Harendra for story S-412528
        var users = list;
        for(var u = 0; u < users.length; u++){
            var user = users[u];
            var items = user.filteredOrders;
            var openTotal = 0.0; //Added by Harendra for story S-412528
            var bookedTotal = 0.0;
            var shippingTotal = 0.0;
            // Added by Nishank
            var activeFilters = [];
            //var filterList = [];
			//Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - Start
            var enabledFilters = '';
            //Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - End
            
            if (filterList !=null && filterList!= 'undefined' && filterList.length>0){
            for(i = 0; i < filterList.length; i++){
                if(filterList[i].isEnabled){
                    activeFilters.push(filterList[i]);
                    
                    //Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - Start
            		enabledFilters += filterList[i].label + '#';
                    //Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - End
                }
            }
           }
            //End of changes by Nishank
            for (var i = 0; i <items.length; i++){
                
                item = items[i];
                console.log('item :: ', item);
                //For Status Class
                item.statusClass = "default";
                if(item.OrderData.Status == "Shipped" || item.OrderData.Status == "Closed" || item.OrderData.Status == "Delivered"){
                    item.statusClass = "shipped";
                } else if(item.OrderData.Status == "Partial Ship"){
                    item.statusClass = "partial";
                } else if(item.OrderData.Status == "Partial Shipping" || item.OrderData.Status == "Backordered"){
                    item.statusClass = "hold";
                }
    
                if(item.OnHold){
                    item.statusClass = "hold";
                }
                
                //For Record Type
                item.orderType = "Sales";
                if(item.OrderData.RecordType && item.OrderData.RecordType.DeveloperName){
                    if(item.OrderData.RecordType.DeveloperName == "NV_Bill_Only"){
                        item.orderType = "Bill Only";
                    } else if(item.OrderData.RecordType.DeveloperName == "NV_RMA"){
                        item.orderType = "RMA";
                    } else if(item.OrderData.RecordType.DeveloperName == "NV_Credit"){
                        item.orderType = "Credit";
                    }
                }
                shippingTotal += item.ShippedTotal;

				//Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - Start
                var orderItems = item.orderItemWrap;
                
                for(var k = 0; k < orderItems.length; k++) {
                    
                    if(enabledFilters.indexOf(orderItems[k].status) > -1 
                       || (orderItems[k].oldHold != '' 
                           && enabledFilters.indexOf(orderItems[k].oldHold) > -1)) {
                        
                        openTotal += orderItems[k].totalPrice;
                    }
                }
                //Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - End
                
                if(item.orderType != "RMA"){
                    bookedTotal += item.BookedTotal;
                    //openTotal += item.TotalOpen; //Added by Harendra for story S-412528
                    console.log(activeFilters);
                       if (activeFilters!= 'undefined' && activeFilters.length > 0){
                           
                     for(j = 0; j < activeFilters.length; j++){
                    var filter = activeFilters[j];
                         console.log('filter.isEnabled :: '+ filter.isEnabled);
                         console.log(activeFilters[j]);
                         console.log(item.OrderLineStatusTotalMap);
                         console.log('openTotal ::',filter.label);
                         console.log('openTotal ::',item.OrderLineStatusTotalMap);
                    if(filter.isEnabled && item.OrderLineStatusTotalMap[filter.label]!= undefined && !(Number.isNaN(item.OrderLineStatusTotalMap[filter.label]))){
                        console.log('filter.label :: ' + filter.label);
                        console.log('filter.value :: ' + item.OrderLineStatusTotalMap[filter.label]);
                        //Code commented - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - Start                
                        //openTotal += item.OrderLineStatusTotalMap[filter.label]; //Added by Harendra for story S-412528
						//Code commented - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - End
                    } 
                         console.log('openTotal ::',openTotal);
                  }
                 }
                } else {
                    bookedTotal -= item.BookedTotal;
                    //openTotal += item.TotalOpen; //Added by Harendra for story S-412528
                    //Added by Harendra for story S-412528
                    if (activeFilters!= 'undefined' && activeFilters.length > 0){
                        for(j = 0; j < activeFilters.length; j++){
                    var filter = activeFilters[j];
                    if(filter.isEnabled && item.OrderLineStatusTotalMap[filter.label] != 'undefined' && !(Number.isNaN(item.OrderLineStatusTotalMap[filter.label]))){
                    //Code commented - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - Start                
                    //openTotal += item.OrderLineStatusTotalMap[filter.label]; //Added by Harendra for story S-412528
					//Code commented - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #:00173911 - End
                    } 
                  }
                }
                }
                //console.log("Order: ", item);
                console.log("Order ITEM TOTAL: ", openTotal);
                //items[i] = item;
            }
             console.log("Order ITEM TOTAL 22: ", openTotal);
            if (Number.isNaN(openTotal)) {
                openTotal = 0;
            }
            overallOpenTotal += openTotal; //Added by Harendra for story S-412528 
			overallBookedTotal += bookedTotal;
            overallShippedTotal += shippingTotal;
            
           	var userBookedInt = Math.round(bookedTotal);
            var userShippedInt = Math.round(shippingTotal);
            user.BookedTotal = userBookedInt;
            user.ShippedTotal = userShippedInt;
            
            var pendingPayments = user.filteredPendingInvoices;
            for (var i = 0; i <pendingPayments.length; i++){
                item = pendingPayments[i];
                
                //For Status Class
                item.statusClass = "default";
                if(item.OrderData.Status == "Shipped" || item.OrderData.Status == "Closed" || item.OrderData.Status == "Delivered"){
                    item.statusClass = "shipped";
                } else if(item.OrderData.Status == "Partial Ship"){
                    item.statusClass = "partial";
                } else if(item.OrderData.Status == "Partial Shipping" || item.OrderData.Status == "Backordered"){
                    item.statusClass = "hold";
                }
    
                if(item.OnHold){
                    item.statusClass = "hold";
                }
            }
            //users[u] = user;
        }
        var openTotalInt = Math.round(overallOpenTotal); //Added by Harendra for story S-412528
        var bookedTotalInt = Math.round(overallBookedTotal);
        var shippingTotalInt = Math.round(overallShippedTotal);
        //console.log("Booked Total: " + bookedTotalInt);
        //console.log("Shipped Total: " + shippingTotalInt);
        //console.log("Filtered List: " + component.get("v.activeFilter"));
		//debugger;
		component.set("v.rollupOpen",openTotalInt); //Added by Harendra for story S-412528
        component.set("v.rollupBooking", bookedTotalInt);
        component.set("v.rollupShipping", shippingTotalInt);
        //component.set("v.filteredOrders", users);
    },
    /*setActiveFilter: function(target){
        $(target).siblings().removeClass('slds-is-selected');
        $(target).addClass('slds-is-selected');
    },*/
    getFilteredList: function(target, list, filterList){
        //var usersList = [];
        for(var u = 0; u < list.length; u++) {
            var unfilteredOrders = list[u].Orders.slice();
            var filteredList = [];

            var unfilteredPendingOrders = list[u].pendingInvoices.slice();
			var filteredPendingOrdersList = [];

            if(target == "all") {
                filteredList = unfilteredOrders;
                filteredPendingOrdersList = unfilteredPendingOrders;
            }
            else {
                console.log('Total Orders: ' + unfilteredOrders.length);
                for(var i = 0; i <unfilteredOrders.length; i++) {
                    item = unfilteredOrders[i];
                    switch(target) {
                        case "bo":
                            if(item.OrderData.RecordType.DeveloperName == "NV_Bill_Only") {
                                filteredList.push(item);
                            }
                            break;
                        case "rma":
                            if(item.OrderData.RecordType.DeveloperName == "NV_RMA") {
                                filteredList.push(item);
                            }
                            break;
                        case "credit":
                            if(item.OrderData.RecordType.DeveloperName == "NV_Credit") {
								filteredList.push(item);
                            }
                            break;
                        default:
                            if(item.OrderData.RecordType.DeveloperName == "NV_Standard" && item.OrderData.Order_Total__c != 0) {
                                filteredList.push(item);
                            }
                            break;
                    }
                }

                console.log('Total Pending Orders: ' + unfilteredPendingOrders.length);
                for(var i = 0; i < unfilteredPendingOrders.length; i++) {
                    item = unfilteredPendingOrders[i];
                    switch (target) {
                        case "bo":
                            if(item.OrderData.RecordType.DeveloperName == "NV_Bill_Only") {
                                filteredPendingOrdersList.push(item);
                            }
                            break;
                        case "rma":
                            if(item.OrderData.RecordType.DeveloperName == "NV_RMA") {
                                filteredPendingOrdersList.push(item);
                            }
                            break;
                        case "credit":
                            if(item.OrderData.RecordType.DeveloperName == "NV_Credit") {
								filteredPendingOrdersList.push(item);
                            }
                            break;
                        default:
                            if(item.OrderData.RecordType.DeveloperName == "NV_Standard" && item.Order_Total__c != 0) {
                                filteredPendingOrdersList.push(item);
                            }
                            break;
                    }
                }
            }

            var activeFilters = [];
            for(i = 0; i < filterList.length; i++){
                if(filterList[i].isEnabled){
                    activeFilters.push(filterList[i]);
                }
            }
            if(activeFilters.length == 0){
                list[u].filteredOrders = filteredList;
                list[u].filteredPendingInvoices = filteredPendingOrdersList;
                continue;
            }

            // Filter on active filters as well.
            var refinedList = [];
            for(i = 0; i < filteredList.length; i++) {
                var order = filteredList[i];
                for(j = 0; j < activeFilters.length; j++){
                    var filter = activeFilters[j];
                    //console.log("Filter vs Order: ", filter.label, order.OrderData.PartialBackOrder__c, (filter.label == order.Status), filter);
					//Added backordered condition for Story S-389737 by Jyoti
                    if(filter.label == order.OrderData.Status && filter.label != "Backordered"){						
                        if(filter.isEnabled ){
                            refinedList.push(order);
                        }						
                        break;
                    }
					//Start - Added by Jyoti for Story S-389737
					else if(filter.label == "Backordered" && filter.label == order.OrderData.PartialBackOrder__c){
                        if(filter.isEnabled){
                            refinedList.push(order);                            
                        }
                        break;
                    }
					//End - Story S-389737
					else if(filter.label == "On Hold"){
                        if(filter.isEnabled && order.OnHold){
                            refinedList.push(order);
                            break;
                        }
                    }
                }
            }    
            // console.log("Returning Filtered List: ", refinedList)
            list[u].filteredOrders = refinedList;

            refinedList = [];
            for(i = 0; i < filteredPendingOrdersList.length; i++) {
                var order = filteredPendingOrdersList[i];
                for(j = 0; j < activeFilters.length; j++){
                    var filter = activeFilters[j];
                    //console.log("Filter vs Order: ", filter.label, order.OrderData.PartialBackOrder__c, (filter.label == order.Status), filter);
					//Added backordered condition for Story S-389737 by Jyoti
                    if(filter.label == order.OrderData.Status && filter.label != "Backordered"){						
                        if(filter.isEnabled ){
                            refinedList.push(order);
                        }						
                        break;
                    }
					//Start - Added by Jyoti for Story S-389737
					else if(filter.label == "Backordered" && filter.label == order.OrderData.PartialBackOrder__c){
                        if(filter.isEnabled){
                            refinedList.push(order);                            
                        }
                        break;
                    }
					//End - Story S-389737
                        else if(filter.label == "On Hold"){
                        if(filter.isEnabled && order.OnHold){
                            refinedList.push(order);
                            break;
                        }
                    } else if(filter.label == "Past Due"){
                        if(filter.isEnabled && order.pastDue){
                            refinedList.push(order);
                            break;
                        }
                    }
                }
            }    
            // console.log("Returning Filtered List: ", refinedList)
            list[u].filteredPendingInvoices = refinedList;
        }
        return list;
    },
    getSortedList: function(sortString, list, tab){
        for(var u = 0; u < list.length; u++){
            var unsortedList = list[u].filteredOrders;
            var unsortedPendingInvoices = list[u].filteredPendingInvoices;
            console.log("Filtered Orders: ", unsortedList);
            switch(sortString){
                case "creditDateASC":
                    unsortedList.sort(function(a, b){
                        var aDate = new Date(a.OrderData.Invoice_Date__c);
                        var bDate = new Date(b.OrderData.Invoice_Date__c);
                        if(aDate > bDate){
                            return -1;
                        } else if(aDate < bDate){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        var aDate = new Date(a.OrderData.Invoice_Date__c);
                        var bDate = new Date(b.OrderData.Invoice_Date__c);
                        if(aDate > bDate){
                            return -1;
                        } else if(aDate < bDate){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "creditDateDESC":
                    unsortedList.sort(function(a, b){
                        var aDate = new Date(a.OrderData.Invoice_Date__c);
                        var bDate = new Date(b.OrderData.Invoice_Date__c);
                        if(aDate > bDate){
                            return 1;
                        } else if(aDate < bDate){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        var aDate = new Date(a.OrderData.Invoice_Date__c);
                        var bDate = new Date(b.OrderData.Invoice_Date__c);
                        if(aDate > bDate){
                            return 1;
                        } else if(aDate < bDate){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "amountASC":
                    unsortedList.sort(function(a, b){
                        var aAmt = a.OrderData.TotalAmount;
                        var bAmt = b.OrderData.TotalAmount;
                        if(!$A.util.isEmpty(a.OrderData.RecordType) && a.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            aAmt = a.OrderData.Invoice_Subtotal__c;
                        }
                        if(!$A.util.isEmpty(b.OrderData.RecordType) && b.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            bAmt = b.OrderData.Invoice_Subtotal__c;
                        }
                        if(aAmt > bAmt){
                            return 1;
                        } else if(aAmt < bAmt){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        var aAmt = a.OrderData.TotalAmount;
                        var bAmt = b.OrderData.TotalAmount;
                        if(aAmt > bAmt){
                            return 1;
                        } else if(aAmt < bAmt){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "amountDESC":
                    unsortedList.sort(function(a, b){
                        var aAmt = a.OrderData.TotalAmount;
                        var bAmt = b.OrderData.TotalAmount;
                        if(!$A.util.isEmpty(a.OrderData.RecordType) && a.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            aAmt = a.OrderData.Invoice_Subtotal__c;
                        }
                        if(!$A.util.isEmpty(b.OrderData.RecordType) && b.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            bAmt = b.OrderData.Invoice_Subtotal__c;
                        }
                        if(aAmt > bAmt){
                            return -1;
                        } else if(aAmt < bAmt){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        var aAmt = a.OrderData.TotalAmount;
                        var bAmt = b.OrderData.TotalAmount;
                        if(aAmt > bAmt){
                            return -1;
                        } else if(aAmt < bAmt){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "dateAsc":
                    unsortedList.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aDate = new Date(a.OrderData.Date_Ordered__c);
                        var bDate = new Date(b.OrderData.Date_Ordered__c);
                        if(aDate > bDate){
                            return -1;
                        } else if(aDate < bDate){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aDate = new Date(a.OrderData.Date_Ordered__c);
                        var bDate = new Date(b.OrderData.Date_Ordered__c);
                        if(aDate > bDate){
                            return -1;
                        } else if(aDate < bDate){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "dateDesc":
                    unsortedList.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aDate = new Date(a.OrderData.Date_Ordered__c);
                        var bDate = new Date(b.OrderData.Date_Ordered__c);
                        if(aDate > bDate){
                            return 1;
                        } else if(aDate < bDate){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aDate = new Date(a.OrderData.Date_Ordered__c);
                        var bDate = new Date(b.OrderData.Date_Ordered__c);
                        if(aDate > bDate){
                            return 1;
                        } else if(aDate < bDate){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "alphAsc":
                case "accountASC":
                    unsortedList.sort(function(a, b){
                        //console.log("Order: ", a, b);
                        return a.OrderData.Account.Name.localeCompare(b.OrderData.Account.Name);
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        //console.log("Order: ", a, b);
                        return a.OrderData.Account.Name.localeCompare(b.OrderData.Account.Name);
                    });
                    break;
                case "alphDesc":
                case "accountDESC":
                    unsortedList.sort(function(a, b){
                        //console.log("Orders: ", a, b);
                        return b.OrderData.Account.Name.localeCompare(a.OrderData.Account.Name);
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        //console.log("Orders: ", a, b);
                        return b.OrderData.Account.Name.localeCompare(a.OrderData.Account.Name);
                    });
                    break;
                case "amntDesc":
                    unsortedList.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aAmt = a.OrderData.Order_Total__c;
                        var bAmt = b.OrderData.Order_Total__c;
                        if(!$A.util.isEmpty(a.OrderData.RecordType) && a.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            aAmt = a.OrderData.Invoice_Subtotal__c;
                        }
                        if(!$A.util.isEmpty(b.OrderData.RecordType) && b.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            bAmt = b.OrderData.Invoice_Subtotal__c;
                        }
                        if(aAmt > bAmt){
                            return -1;
                        } else if(aAmt < bAmt){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aAmt = a.OrderData.Order_Total__c;
                        var bAmt = b.OrderData.Order_Total__c;
                        if(aAmt > bAmt){
                            return -1;
                        } else if(aAmt < bAmt){
                            return 1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                case "amntAsc":
                    unsortedList.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aAmt = a.OrderData.Order_Total__c;
                        var bAmt = b.OrderData.Order_Total__c;
                        if(!$A.util.isEmpty(a.OrderData.RecordType) && a.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            aAmt = a.OrderData.Invoice_Subtotal__c;
                        }
                        if(!$A.util.isEmpty(b.OrderData.RecordType) && b.OrderData.RecordType.DeveloperName == 'NV_Credit'){
                            bAmt = b.OrderData.Invoice_Subtotal__c;
                        }
                        if(aAmt > bAmt){
                            return 1;
                        } else if(aAmt < bAmt){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    unsortedPendingInvoices.sort(function(a, b){
                        //console.log("Order: ", b);
                        var aAmt = a.OrderData.Order_Total__c;
                        var bAmt = b.OrderData.Order_Total__c;
                        if(aAmt > bAmt){
                            return 1;
                        } else if(aAmt < bAmt){
                            return -1;
                        } else{
                            return 0;
                        }
                    });
                    break;
                default: 
                    break;
            }
        }
    	return list;
    }
})