({
    doInit : function(component, event, helper){
        var orders = component.get("v.orders");
        for(var i = 0; i < orders.length; i++){
            var user = orders[i];
            user.isActive = (orders.length == 1);
            user.filteredOrders = user.Orders.slice();
            user.filteredPendingInvoices = user.pendingInvoices.slice();
            orders[i] = user;
        }
        //var start = new Date().getTime();
        helper.buildFromList(component, component.get("v.orders"));
        component.set("v.filteredOrders", orders);
        //var end = new Date().getTime();
        //console.log("Build From List took: " + (end - start));
        if(component.get("v.tab") == 2){
            setTimeout(function(){
                var appEvent = $A.get("e.c:nv_DailyOrdersTabSelected");
                appEvent.setParams({"tabIndex" : component.get("v.tab")});
                appEvent.fire();
            }, 11);
        }
    },
    filterOrders : function(component, event, helper){
        if(event.getSource){
            //Framework Component Event
            var target = event.getSource();
            filter = target.get("v.label");
        } else{
            var target = event.target.value;
        }
        //console.log("Target:", target);
        //helper.setActiveFilter(event.target);
        component.set("v.activeFilter", target);
       	var list = helper.getFilteredList(target, component.get("v.orders"), component.get("v.filterList"));
        var sortString = component.get("v.activeSort");
        list = helper.getSortedList(sortString, list);
        helper.buildFromList(component, list);

        setTimeout(function(){
            var appEvent = $A.get("e.c:nv_DailyOrdersTabSelected");
            appEvent.setParams({"tabIndex" : component.get("v.tab"), "changeFilters" : true});
            appEvent.fire();
            /*appEvent = $A.get("e.c:nv_DailyOrders_changeFilterList");
            appEvent.setParams({"filterString" : target});
            appEvent.fire();*/
        }, 10);
    },
    sortOrders : function(component, event, helper){
        var sortString = event.getParams("sortString").sortString;
        var activeTab = event.getParams("tab").tab;
        //console.log("Sort: " + sortString);
        component.set("v.activeSort", sortString);
        var tab = component.get("v.tab");
        var list = component.get("v.filteredOrders");
        helper.getSortedList(sortString, list, tab);
        var timeoutDuration = activeTab == tab ? 5 : 50;
        setTimeout(function(){
            component.set("v.filteredOrders", list);
        }, timeoutDuration);
    },
    filterSelected: function(component, event, helper){
        var filters = event.getParams("filters").filters;
        var tab = event.getParams("tab").tab;
        //console.log("Filters: ", filters);
        component.set("v.filterList", filters);
        var target = component.get("v.activeFilter");
        var list = component.get("v.orders")
        helper.getFilteredList(target, list, filters);
        var sortString = component.get("v.activeSort");
        helper.getSortedList(sortString, list);
        //helper.buildFromList(component, list);
        helper.buildFromList(component, list, filters);
        if(component.get("v.tab") == tab){
            component.set("v.filteredOrders", list);
            //console.log("Calling Tab Selected");
            setTimeout(function(){
                var appEvent = $A.get("e.c:nv_DailyOrdersTabSelected");
                appEvent.setParams({"tabIndex" : component.get("v.tab")});
                appEvent.fire();
            }, 12);
        }
    },
    toggleActive: function(component, event, helper){
        console.log(event);
    }
})