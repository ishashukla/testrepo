({
    getTypeDisplay : function(type){
        switch(type){
            case "NV_Bill_Only":
                return "Bill Only";
            case "NV_RMA":
                return "RMA";
            case "NV_Credit":
                return "Credit";
            default:
                return "Sales";
        }
    },
})