({
    init : function(component, event, helper){
        /*
        var order = component.get("v.order");
        var type = "NV_Standard";
        if(order && order.OrderData && order.OrderData.RecordType && order.OrderData.RecordType.DeveloperName){
            type = order.OrderData.RecordType.DeveloperName;
        }
        component.set("v.orderType", helper.getTypeDisplay(type));
        */
        var order = component.get("v.order");
        if(order && order.OrderData && order.OrderData.RecordType && order.OrderData.RecordType.DeveloperName){
            if(order.OrderData.RecordType.DeveloperName == "NV_RMA"){
                if(order.OrderData.Order_Total__c && order.OrderData.Order_Total__c > 0){
                    order.OrderData.Order_Total__c *= -1;
                    component.set("v.order", order);
                }
            }
        }
    },
	navigateToSObject : function(component, event, helper) {
		var sObjId = component.get("v.order").OrderData.Id;
        $(".order-footer").css("visibility", "hidden");
        var navToSObjEvt = $A.get("e.c:nv_DailyOrders_orderSelected");
        navToSObjEvt.setParams({
            orderId: sObjId
        });
        navToSObjEvt.fire();
	}
})