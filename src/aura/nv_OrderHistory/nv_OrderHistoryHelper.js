({
    retrieveRecent : function(component){
        var action = component.get("c.getRecentOrderActivity");
        action.setCallback(this, function(response) {
            console.log('Inside Callback ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //debugger;
                component.set("v.recentLoading", false);
                component.set("v.recentAccounts", response.getReturnValue());
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    retrieveSearchOptions : function(searchTerm, page, component, callback){
        var action = component.get("c.getFilteredData");
        action.setParams({ searchString : searchTerm, offSetPage: page });
        action.setCallback(this, function(response) {
            console.log('Inside Callback ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //debugger;
                var responseArr = response.getReturnValue();
                var combinedArr = responseArr[0].concat(responseArr[1]);
                if(responseArr[0].length < 20 && responseArr[1].length < 20){
                    component.set("v.hasMoreOptions", false);
                } else{
                    component.set("v.hasMoreOptions", true);
                }
                combinedArr.sort(function(a, b){
                    if(a.Name && !$A.util.isEmpty(a.Name) && b.Name && !$A.util.isEmpty(b.Name)){
                        return a.Name.localeCompare(b.Name);
                    }
                    return 0;
                })
                if(typeof callback == "function"){
                    callback(combinedArr);
                }
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    retrieveOrders : function(accountId, productId, dateRange, statusFilter, orderBy, page, userIds, searchTerm, component, callback){
        console.log("userIds:", userIds);
        console.log("ProductId:", productId);
        var action = component.get("c.getFilteredOrders");
        if(searchTerm.length < 2){
            searchTerm = null;
        }
        action.setParams({ accountId : accountId, productId: productId, dateRange: dateRange, statusFilterString: statusFilter, orderByString: orderBy, offSetPage: page, ownerIds: userIds, searchTerm: searchTerm });
        action.setCallback(this, function(response) {
            console.log('Inside Callback ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //debugger;
                var orders = response.getReturnValue();
                for(var o in orders){
                    var item = orders[o];
                    item.statusClass = "default";
                    if(item.OrderData.Status == "Shipped" || item.OrderData.Status == "Closed" || item.OrderData.Status == "Delivered"){
                        item.statusClass = "shipped";
                    } else if(item.OrderData.Status == "Partial Ship"){
                        item.statusClass = "partial";
                    } else if(item.OrderData.Status == "Partial Shipping" || item.OrderData.Status == "Backordered"){
                        item.statusClass = "hold";
                    }
                    if(item.On_Hold__c){
                        item.statusClass = "hold";
                    }
                    orders[o] = item;
                }
                callback(orders);
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    retrieveFollowableUsers : function(component){
        var action = component.get("c.getFollowableUsers");
        action.setCallback(this, function(response) {
            console.log('Inside Callback Followable: ' + response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //debugger;
                //component.set("v.recentLoading", false);
                
                
                component.set("v.followableUsers", response.getReturnValue());
            }
            else{
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    displaySVG : function(component) {
        var svg = component.find("svg_filter");
        value = svg.getElement().innerHTML;
        value = $("<div/>").html(value).text();
        svg.getElement().innerHTML = value;
        
        svg = component.find("svg_range");
        value = svg.getElement().innerHTML;
        value = $("<div/>").html(value).text();
        svg.getElement().innerHTML = value;

        svg = component.find("svg_sort");
        value = svg.getElement().innerHTML;
        value = $("<div/>").html(value).text();
        svg.getElement().innerHTML = value;
        
	}, // S-408121 - Praful Gupta - Code update start
    getFilterOptionList : function(){
        var list = [];
        list.push({label:"Entered", isEnabled: false});
        list.push({label:"Booked", isEnabled: false});
        list.push({label:"Backordered", isEnabled: false});        
        list.push({label:"On Hold", isEnabled: false}); 
        list.push({label:"Cancelled", isEnabled: false});        
        list.push({label:"Expedited", isEnabled: false});        
        list.push({label:"Awaiting Shipping", isEnabled: false});
        list.push({label:"Partial Ship", isEnabled: false});       
        list.push({label:"Shipped", isEnabled: false});   
        list.push({label:"Awaiting PO", isEnabled: false});        
        list.push({label:"Awaiting Return", isEnabled: false});
        list.push({label:"Closed", isEnabled: false});      
        list.push({label:"Delivered", isEnabled: false});
        list.push({label:"Past Due", isEnabled: false});        
        return list; // S-408121 - Praful Gupta - Code update Ends
    }, 
    getThirtyBusinessDays: function(){
        var day = new Date().getDay();
        var mapping = [41,42,42,42,42,42,40]; //Sunday->Saturday, total days for 30 business days
        return mapping[day];
    },
    getFilterString: function(component){
        var filterList = component.get("v.filterList");
        var activeFilters = [];
        for(i = 0; i < filterList.length; i++){
            if(filterList[i].isEnabled){
                activeFilters.push(filterList[i].label);
            }
        }
        return activeFilters.join(";");
    },
    getUserIds: function(component){
        var users = component.get("v.followableUsers");
        var allUserIds = [];
        var activeUserIds = [];
        for(var u in users){
            var user = users[u];
            allUserIds.push(user.Id);
            if(user.isEnabled){
                activeUserIds.push(user.Id);
            }
        }
        if(activeUserIds.length > 0){
            return activeUserIds.join(";");
        } else if(allUserIds.length > 0){
            return allUserIds.join(";");
        } else{
            return null;
        }
    }
})