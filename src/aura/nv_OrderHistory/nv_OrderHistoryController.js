({
	doInit : function(component, event, helper) {
        $(".order-footer").remove().appendTo('body');
        $(".order-footer").css("visibility", "hidden");
        component.set("v.isInitialized", true);
        if(component.get("v.isDoneRendering")){
            helper.displaySVG(component);
        }

        setTimeout(function() {
			var filterList = helper.getFilterOptionList();
            console.log("FilterList: ", filterList);
            component.set("v.filterList", filterList);
        }, 20);

        helper.retrieveFollowableUsers(component);
        helper.retrieveRecent(component);
  	},
    displaySVG : function(component, event, helper){
        //BJ - This is a very hacky solution. But, using after-render causes the back/reload functionality to break.
        if(!component.get("v.isDoneRendering") && component.get("v.isInitialized")){
            component.set("v.isDoneRendering", true);
            helper.displaySVG(component);
        } else{
            component.set("v.isDoneRendering", true);
        }
    },
    searchTextChanged : function(component, event, helper){
        console.log("Search Text Changed");
        //console.log("Text Value: " + component.get("v.searchTerm") );
      	var searchTerm = component.get("v.searchTerm");
        if(!$A.util.isUndefined(searchTerm) && !$A.util.isEmpty(searchTerm) && searchTerm.length > 1){
            console.log("SEARCH FOR: " + searchTerm);
            component.set("v.resultLabel", "Search Results");
            component.set("v.optionPage", 0);
            component.set("v.gettingOptions", true);
            component.set("v.searchOptions", []);
            helper.retrieveSearchOptions(searchTerm, "0", component, function(combinedArr){
                component.set("v.searchOptions", combinedArr);
                component.set("v.gettingOptions", false);
            });
        } else{
            console.log("Not enough text, use recent results");
            component.set("v.resultLabel", "Recent Order Activity");
        }
    },
    //S-398540 03/25/2016 Connor Flynn Add function to clear search field when "x" is clicked
    clearSearchBox : function(component, event, helper){
        if(component.get("v.searchTerm") != null){
            component.set("v.searchTerm", "");
            if(component.find("searchBoxText") != null){
                //component.find("searchBoxText").focus();
            }
        }
        if(component.get("v.additionalSearchTerm") != null){
            component.set("v.additionalSearchTerm", "");
            if(component.find("additionalSearchBoxText") != null){
                //component.find("additionalSearchBoxText").focus();
            }
        }
    },
    //S-398540 03/25/2016 Connor Flynn End
    getMoreOptions : function(component, event, helper){
        var searchTerm = component.get("v.searchTerm");
        var page = component.get("v.optionPage");
        page++;
        component.set("v.optionPage", page);
        component.set("v.gettingOptions", true);
        helper.retrieveSearchOptions(searchTerm, ""+page, component, function(combinedArr){
                var options = component.get("v.searchOptions");
                component.set("v.gettingOptions", false);
            	component.set("v.searchOptions", options.concat(combinedArr));
            });
    },
    getResults : function(component, event, helper) {
		component.set("v.activePage", 1);
        var option = event.getParams("option").option;
        component.set("v.activeOption", option);
        component.set("v.gettingOrders", true);
        component.set("v.ordersPage", 0);

        //Prepare Request
        var accountId = null;
        var productId = null;
        if(!$A.util.isEmpty(option.AccountNumber)){
            accountId = option.Id;
        } else {
            productId = option.Id;
        }
        var dateRange = component.get("v.activeRange");
        var statusFilter = helper.getFilterString(component);
        var userIds = helper.getUserIds(component);
        var orderBy = component.get("v.activeSort");
        var searchTerm = component.get("v.additionalSearchTerm");
        var page = "0";
        var callback = function(result){
            if(result.length < 20){
                component.set("v.hasMoreOrders", false);
            } else{
                component.set("v.hasMoreOrders", true);
            }
            component.set("v.orderResults", result);
            component.set("v.gettingOrders", false);
            $('.scroller').css("transform", "");
        }
        console.log(userIds);
        //retrieveOrders : function(accountId, productId, dateRange, statusFilter, orderBy, page, userIds, searchTerm, component, callback){
        helper.retrieveOrders(accountId, productId, dateRange, statusFilter, orderBy, page, userIds, searchTerm, component, callback);

        $(".order-footer").css("visibility", "visible");
	},
    getMoreOrders : function(component, event, helper){
        var option = component.get("v.activeOption");
        component.set("v.gettingOrders", true);

        //Prepare Request
        var accountId = null;
        var productId = null;
        if(!$A.util.isEmpty(option.AccountNumber)){
            accountId = option.Id;
        } else {
            productId = option.Id;
        }
        var dateRange = component.get("v.activeRange");
        var statusFilter = helper.getFilterString(component);
        var userIds = helper.getUserIds(component);
        var orderBy = component.get("v.activeSort");
        var page = component.get("v.ordersPage");
        var searchTerm = component.get("v.additionalSearchTerm");
        page++;
        component.set("v.ordersPage", page);
        var callback = function(result){
            if(result.length < 20){
                component.set("v.hasMoreOrders", false);
            } else{
                component.set("v.hasMoreOrders", true);
            }
            var results = component.get("v.orderResults");
            component.set("v.orderResults", results.concat(result));
            component.set("v.gettingOrders", false);
        }
        helper.retrieveOrders(accountId, productId, dateRange, statusFilter, orderBy, ""+page, userIds, searchTerm, component, callback);
    },
    additionalSearchTextChanged : function(component, event, helper) {
		component.set("v.activePage", 1);
        component.set("v.gettingOrders", true);
        component.set("v.ordersPage", 0);

        //Prepare Request
        var accountId = null;
        var productId = null;
        var option = component.get("v.activeOption");
        if(!$A.util.isEmpty(option.AccountNumber)){
            accountId = option.Id;
        } else {
            productId = option.Id;
        }
        var dateRange = component.get("v.activeRange");
        var statusFilter = helper.getFilterString(component);
        var userIds = helper.getUserIds(component);
        var orderBy = component.get("v.activeSort");
        var searchTerm = component.get("v.additionalSearchTerm");
        console.log("SearchTerm: ", searchTerm);
        var page = "0";
        var callback = function(result){
            if(result.length < 20){
                component.set("v.hasMoreOrders", false);
            } else{
                component.set("v.hasMoreOrders", true);
            }
            component.set("v.orderResults", result);
            component.set("v.gettingOrders", false);
        }
        //retrieveOrders : function(accountId, productId, dateRange, statusFilter, orderBy, page, userIds, searchTerm, component, callback){
        helper.retrieveOrders(accountId, productId, dateRange, statusFilter, orderBy, page, userIds, searchTerm, component, callback);

        $(".order-footer").css("visibility", "visible");
	},
    clearResults : function(component, event, helper) {
		component.set("v.activePage", 0);
        component.set("v.activeOption", null);
        component.set("v.orderResults", []);
        component.set("v.additionalSearchTerm", "");
        $(".order-footer").css("visibility", "hidden");
	},
    sortActiveTab: function(component, event, helper){
        console.log("Display Sort Options");
        console.log("Component: ", component);
        console.log("Event: ", event);
        console.log("Event Source: ", event.source.get('v.label'));
        var currentSort = component.get("v.activeSort");
        switch(event.source.get('v.label')){
            case "Oldest First":
                component.set("v.activeSort", "EffectiveDate ASC");
                break;
            case "Alphabetically":
                component.set("v.activeSort", "Account.Name ASC");
                break;
            case "Reverse Alphabetically":
                component.set("v.activeSort", "Account.Name DESC");
                break;
            case "Largest First":
                component.set("v.activeSort", "Order_Total__c DESC");
                break;
            case "Smallest First":
                component.set("v.activeSort", "Order_Total__c ASC");
                break;
            case "Owner Alphabetically":
                component.set("v.activeSort", "Account.Owner.Name ASC");
                break;
            case "Owner Reverse Alphabetically":
                component.set("v.activeSort", "Account.Owner.Name DESC");
                break;
            default:
                component.set("v.activeSort", "EffectiveDate DESC");
        }
        if(currentSort != component.get("v.activeSort")){
            //Sorting Changed. Re-fetch
            var sObj = component.get("v.activeOption");
            var navToSObjEvt = $A.get("e.c:nv_SearchOptionSelected");
            navToSObjEvt.setParams({
                option: sObj
            });
            navToSObjEvt.fire();
        }
    },
    changeSearchRange: function(component, event, helper){
        console.log("Event Source: ", event.source.get('v.label'));
        var currentSort = component.get("v.activeRange");
        switch(event.source.get('v.label')){
            case "1 Week":
                component.set("v.activeRange", 7);
                break;
            case "2 Weeks":
                component.set("v.activeRange", 14);
                break;
            case "3 Weeks":
                component.set("v.activeRange", 21);
                break;
            case "30 Days":
                component.set("v.activeRange", helper.getThirtyBusinessDays());
                break;
        }
        if(currentSort != component.get("v.activeRange")){
            //Range Changed. Re-fetch
            var sObj = component.get("v.activeOption");
            var navToSObjEvt = $A.get("e.c:nv_SearchOptionSelected");
            navToSObjEvt.setParams({
                option: sObj
            });
            navToSObjEvt.fire();
        }
    },
    filterActiveTab: function(component, event, helper){
        console.log("Filter Event: ", event);
        setTimeout(function(){
            var sObj = component.get("v.activeOption");
            var navToSObjEvt = $A.get("e.c:nv_SearchOptionSelected");
            navToSObjEvt.setParams({
                option: sObj
            });
            navToSObjEvt.fire();
        }, 11);
    },
    waiting : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();
    },
    doneWaiting : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();
    },
    openModal: function(component, event, helper){
        component.set("v.activePage", 2);
        var orderId = event.getParams("orderId").orderId;

        $A.createComponent(
            "c:nv_OrderView",
            {
                "aura:Id": "findableAuraId",
                "orderId" : orderId,
                "redirectView" : false,
                "contactRecordTypeId" : component.get("v.contactRecordTypeId")
            },
            function(orderView){
                //Add the new button to the body array
                if (component.isValid()) {
                    component.set("v.modalComponent", orderView);
                    $('.scroller').css("transform", "");
                    /*var action = $A.get("e.ui:updateSize");
                    action.fire(); */

                }
            }
        );


        $(".order-footer").css("visibility", "hidden");
    },
    closeModal: function(component, event, helper){
        component.set("v.activePage", 1);
        $(".order-footer").css("visibility", "visible");
    },
    renderFooter: function(component, event, helper){
        console.log("RENDER FOOTER", component.get("v.followableUsers"));
        if(!$A.util.isEmpty(component.get("v.followableUsers"))){
            component.set("v.footerClass", "slds order-footer atm-view");
            setTimeout(function(){
                svg = component.find("svg_tm");
                value = svg.getElement().innerHTML;
                value = $("<div/>").html(value).text();
                svg.getElement().innerHTML = value;
            },11);
        }
    }
})