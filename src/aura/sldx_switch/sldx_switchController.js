({

    doInit:function (component, event, helper) {
        var intialIndex = component.get("v.intialIndex");
        var labels = component.get("v.labels");
        component.set("v.activeIndex", intialIndex);
        component.set('v.activeLabel', labels[intialIndex]); 
    },
    switchValue:function(component, event, helper){
        var selectedDiv = event.target;
        if(!$(selectedDiv).hasClass('active')){
            $(selectedDiv).siblings().removeClass('active');
            $(selectedDiv).addClass('active');
            component.set("v.activeIndex", selectedDiv.value);
            component.set('v.activeLabel', selectedDiv.innerText);
        }
    }

})