({
	toggleActive : function(component, event, helper) {
		var user = component.get("v.user");
        component.set("v.loading", true);
        setTimeout(function(){
            user.isActive = !user.isActive;
            component.set("v.user", user);
            component.set("v.loading", false);
        }, 20);
	}
})