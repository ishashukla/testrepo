<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values></values>
        </dashboardFilterOptions>
        <name>Opened Date</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Open Cases in Queues: Number and Age in Hours (Average) in Queue for Over 1 Hour</footer>
            <header>New and Open Support Cases in Queue for Over 1 Hour</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/Samples_Support_Cases_in_Queue</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All Cases: Number by Origin/Filtered Date</footer>
            <header>Cases by Origin</header>
            <legendPosition>Right</legendPosition>
            <report>C360_Call_Center/Samples_Support_Cases_by_Origin</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>BarStacked</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Customer Inquiry &amp; Request Cases, by Sub-Type and Owner</footer>
            <groupingColumn>OWNER</groupingColumn>
            <groupingColumn>Case.Sub_Type__c</groupingColumn>
            <header>Customer Inquiry &amp; Request Cases by Sub-Types &amp; Owner</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/Samples_Inquiry_and_Requests</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Samples Audit Support Cases by Owner &amp; Type within filtered date</footer>
            <header>Samples Audit Support Cases by Owner &amp; Type</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/SamplesAudit_byOwner</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Samples Audit Support Cases Total vs Closed within Filtered date</footer>
            <header>Samples Audit Support Cases Total vs Closed</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/SamplesAudit_TotalvsClosed</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Samples Audits Support Cases by Region &amp; Status within the filtered date</footer>
            <header>Samples Audits Support Cases by Region &amp; Status</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/SamplesAuditCasesByRegionVsStatus</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Open Cases: Number and Age in Hours (Average) by Owner/Filtered Creation Date</footer>
            <header>New and Open Support Cases by Owner</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/Samples_Open_Cases_Owner_Queue</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Phone Cases Closed When Created (Shown in Green): Number by Filtered Date</footer>
            <header>First Call Resolution</header>
            <legendPosition>Right</legendPosition>
            <report>C360_Call_Center/Samples_First_Call_Resolution</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Cases - Grouped by Record Types and Sub-Type within Filtered date</footer>
            <groupingColumn>RECORDTYPE</groupingColumn>
            <groupingColumn>Case.Sub_Type__c</groupingColumn>
            <header>Support Cases by Record Types and Sub-Types</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/Samples_Case_Types</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Samples Audit Support Cases by Type within Filtered date</footer>
            <header>Samples Audit Support Cases by Type</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/SamplesAudit_TotalVsType</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Samples Team Support Cases by Title</footer>
            <header>Samples Team Support Cases by Title</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/A0845_1_byTitle_SampleSupportTeam</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All Cases: Number and Age in Hours (Average) by Owner/Filtered Creation Date</footer>
            <header>New, Open and Closed Support Cases by Owner (Cumulative)</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/Samples_Support_Cases_by_Owner</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>BarStacked</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All Cases: Number of Cases by Owner (Max up to Last 8 Weeks)</footer>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <groupingColumn>OWNER</groupingColumn>
            <header>Support Cases by Owner and Week</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>8</maxValuesDisplayed>
            <report>C360_Call_Center/Samples_Support_Cases_by_Week</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y2</axisBinding>
                <column>AGE</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Customer Inquiry &amp; Requests Cases, by Sub-Type and Average Age in Hours</footer>
            <groupingColumn>Case.Sub_Type__c</groupingColumn>
            <header>Customer Inquiry &amp; Request Cases by Sub-Types</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/Samples_Inquiry_and_Requests</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Samples Audit Support Cases by Owner &amp; Status within Filtered date</footer>
            <header>Samples Audit Support Cases by Owner &amp; Status</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/SamplesAudit_ProgressByTeamMember</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATEONLY</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Samples Team Support Cases by Contact Record Type</footer>
            <header>Samples Team Support Cases by Contact Record Type</header>
            <legendPosition>Bottom</legendPosition>
            <report>C360_Call_Center/A0845_2_byContactRecordType_SamplesTeam</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
    </rightSection>
    <runningUser>sireesha.ganti@stryker.com</runningUser>
    <textColor>#000000</textColor>
    <title>Samples Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
