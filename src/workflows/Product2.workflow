<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Endo_CPT_Product_Notification</fullName>
        <description>Endo CPT Product Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Tech_Support</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_CPT_Product_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>CanOrderFlag</fullName>
        <description>Update Can_Order_Flag__c to true</description>
        <field>Can_Order_Flag__c</field>
        <literalValue>1</literalValue>
        <name>CanOrderFlag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CanOrderFlagToFalse</fullName>
        <description>Updates Can Order Flag to false</description>
        <field>Can_Order_Flag__c</field>
        <literalValue>0</literalValue>
        <name>CanOrderFlagToFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CanShipFlag</fullName>
        <description>Update Can_Ship_Flag__c to true</description>
        <field>Can_Ship_Flag__c</field>
        <literalValue>1</literalValue>
        <name>CanShipFlag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsServiceable</fullName>
        <description>Update Serviceable__c to true</description>
        <field>Serviceable__c</field>
        <literalValue>1</literalValue>
        <name>IsServiceable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ServiceableToFalse</fullName>
        <description>Updates Serviceable__c to false</description>
        <field>Serviceable__c</field>
        <literalValue>0</literalValue>
        <name>ServiceableToFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Endo CPT Product Notification</fullName>
        <actions>
            <name>Endo_CPT_Product_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Status__c</field>
            <operation>contains</operation>
            <value>CPT</value>
        </criteriaItems>
        <description>An email notification should be sent to the Tech Support Supervisor whenever a product with status CPT has arrived.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateProductForLCC300</fullName>
        <actions>
            <name>CanOrderFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CanShipFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ServiceableToFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Life_Cycle_Code__c</field>
            <operation>equals</operation>
            <value>LCC 300 - orderable</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Product Record Type</value>
        </criteriaItems>
        <description>This workflow rule updates the Product checkbox fields based on the value of Life cycle code field of product. (COMM BP : I-246780)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateProductForLCC501</fullName>
        <actions>
            <name>CanOrderFlagToFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CanShipFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IsServiceable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Life_Cycle_Code__c</field>
            <operation>equals</operation>
            <value>LCC 501 - serviceable</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Product Record Type</value>
        </criteriaItems>
        <description>This workflow rule updates the Product checkbox fields based on the value of Life cycle code field of product. (COMM BP : I-246780)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
