<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>How_To_Article_Review_Reminder</fullName>
        <description>How To Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Medical_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/How_To_Article_Review_Notification</template>
    </alerts>
    <alerts>
        <fullName>How_To_Rejection_Notification</fullName>
        <description>How To Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Article_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Instruments_How_To_Article_Review_Reminder</fullName>
        <description>Instruments How To Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Instrument_CCKM_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/How_To_Article_Review_Notification</template>
    </alerts>
    <knowledgePublishes>
        <fullName>Approve_Endo_How_To_Article</fullName>
        <action>Publish</action>
        <description>Approve Endo How To Article</description>
        <label>Approve Endo How To Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>How_To_Approve_Publish</fullName>
        <action>Publish</action>
        <label>How To: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>How To Article Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to Medical Knowledge Group users on Review Date in How To Article</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(BEGINS($UserRole.DeveloperName,&quot;Medical&quot;),!ISNULL(Review_Date__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>How_To_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>HowTo__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ICCKM How To Article Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to ICCKM Knowledge Group users on Review Date in How To Article</description>
        <formula>AND(BEGINS($UserRole.DeveloperName,&quot;Instruments&quot;),!ISNULL(Review_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Instruments_How_To_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>HowTo__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
