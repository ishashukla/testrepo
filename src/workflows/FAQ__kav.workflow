<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FAQ_Rejection_Notification</fullName>
        <description>FAQ Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Article_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>FAQ_Review_Reminder</fullName>
        <description>FAQ Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Medical_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/FAQ_Article_Review_Notification</template>
    </alerts>
    <alerts>
        <fullName>ICCKM_FAQ_Review_Reminder</fullName>
        <description>ICCKM FAQ Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Instrument_CCKM_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/FAQ_Article_Review_Notification</template>
    </alerts>
    <knowledgePublishes>
        <fullName>Approved_Endo_FAQ_Article</fullName>
        <action>Publish</action>
        <label>Approved Endo FAQ Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>FAQ_Approve_Publish</fullName>
        <action>Publish</action>
        <label>FAQ: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>FAQ Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to Medical Knowledge Group users  on Review Date in FAQ Article.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(BEGINS($UserRole.DeveloperName,&quot;Medical&quot;),!ISNULL(Review_Date__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FAQ_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>FAQ__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ICCKM FAQ Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to ICCKM Knowledge Group users  on Review Date in FAQ Article.</description>
        <formula>AND(BEGINS($UserRole.DeveloperName,&quot;Instruments&quot;),!ISNULL(Review_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ICCKM_FAQ_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>FAQ__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
