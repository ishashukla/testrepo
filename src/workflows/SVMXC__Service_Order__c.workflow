<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_Approve_WO</fullName>
        <description>COMM: Approve WO</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_WO_Approved_VF</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_notification_to_RAQA_when_Priority_Highest</fullName>
        <description>COMM: Email notification to RAQA when Priority Highest</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_RAQA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Email_notification_to_RAQA_when_Priority_Highest</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_notification_to_RAQA_when_Priority_QA</fullName>
        <description>COMM: Email notification to RAQA when Priority QA</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_RAQA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Email_notification_to_RAQA_when_Priority_Highest</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_notification_to_reopen_the_WO</fullName>
        <description>COMM: Email notification to reopen the WO</description>
        <protected>false</protected>
        <recipients>
            <recipient>System_Administrator_Users</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Email_notification_to_reopen_the_WO</template>
    </alerts>
    <alerts>
        <fullName>COMM_Notification_Installation_Work_Order_is_Assigned</fullName>
        <description>COMM: Notification Installation Work Order is Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Installation_Work_Order_Notification</template>
    </alerts>
    <alerts>
        <fullName>COMM_Notification_to_Scheduling_Manager_when_WO_is_submitted</fullName>
        <description>COMM: Notification to Scheduling Manager when WO is submitted</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Scheduling_Manager</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Notification_of_new_Installation_WO</template>
    </alerts>
    <alerts>
        <fullName>COMM_PFA_WO_Rejection_notification_to_RAQA</fullName>
        <description>COMM PFA WO Rejection notification to RAQA</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_RAQA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Stryker_Field_Service_Templates/Email_notification_to_RAQA_Group_when_WO_rejected</template>
    </alerts>
    <alerts>
        <fullName>COMM_WO_Not_ReSubmitted_Yet</fullName>
        <description>COMM WO: Not ReSubmitted Yet</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_WO_Resubmitted_VF</template>
    </alerts>
    <alerts>
        <fullName>COMM_WO_Rejected</fullName>
        <description>COMM: WO Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_WO_Rejected_VF</template>
    </alerts>
    <alerts>
        <fullName>COMM_Work_Order_is_scheduled_outside_of_SLA</fullName>
        <description>COMM : Work Order is scheduled outside of SLA</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Work_Order_is_scheduled_outside_of_SLA</template>
    </alerts>
    <alerts>
        <fullName>Discount_Approval_Notification</fullName>
        <description>Discount Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Discount_Approval_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Discount_Approved_Notfication</fullName>
        <description>Discount Approved Notfication</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Discount_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Manager_and_technician_that_WO_is_not_accepted</fullName>
        <description>Email to Manager that WO is not accepted</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Email_notification_to_Managers_when_WO_not_accepted</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Technician_that_WO_is_not_accepted</fullName>
        <description>Email to Technician that WO is not accepted</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Email_notification_to_Managers_when_WO_not_accepted</template>
    </alerts>
    <alerts>
        <fullName>Group_Alert_for_More_Than_3_Times_Reschedule</fullName>
        <description>Group Alert for More Than 3 Times Reschedule</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Tech_Support</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/WORescheduleHistory</template>
    </alerts>
    <alerts>
        <fullName>Installation_Approval_By_Scheduling_Mgr</fullName>
        <description>Installation Approval By Scheduling Mgr</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Scheduling_Manager</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Installation_Approval_By_Scheduling_Mgr</template>
    </alerts>
    <alerts>
        <fullName>Installation_rejection_By_Scheduling_Mgr</fullName>
        <description>Installation rejection By Scheduling Mgr</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Installation_Rejection_By_Scheduling_Mgr</template>
    </alerts>
    <alerts>
        <fullName>Manager_Alert_for_More_Than_3_Times_Reschedule</fullName>
        <description>Manager Alert for More Than 3 Times Reschedule</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/WORescheduleHistory</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Tech_support_queue_when_WO_Rejected</fullName>
        <description>COMM: Notification to Tech support queue when WO Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Email_notification_to_Queue_when_WO_rejected</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Tech_support_queue_when_WO_not_accepted</fullName>
        <description>COMM: Notification_to_Tech_support_queue_when_WO_not_accepted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Email_notification_to_Queue_when_WO_not_accepted</template>
    </alerts>
    <alerts>
        <fullName>Notify_Customer_On_Cancelled</fullName>
        <description>Notify Customer On Cancelled</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sales_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_WO_Cancelled_VF</template>
    </alerts>
    <alerts>
        <fullName>Notify_Customer_on_On_Complete</fullName>
        <description>Notify Customer on On Complete</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_WO_Complete_VF</template>
    </alerts>
    <alerts>
        <fullName>Notify_Customer_on_On_dispatch</fullName>
        <description>Notify Customer on On dispatch</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Event_Dispatched_VF</template>
    </alerts>
    <alerts>
        <fullName>Notify_FST_when_Manager_approves_the_submitted_Service_Quote</fullName>
        <description>Notify FST when Manager approves the submitted Service Quote</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/SVMX_Manager_has_Approved_your_Service_Quote</template>
    </alerts>
    <alerts>
        <fullName>Notify_Technician_for_PM_Workorders</fullName>
        <description>Notify Technician for PM Workorders</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Future_PM_WO_Notification</template>
    </alerts>
    <alerts>
        <fullName>SVMX_Estimated_Labor_on_Service_Quote_is_2500_Need_Mgmt_Approval</fullName>
        <description>SVMX - Estimated Labor on Service Quote is &gt;=2500 Need Mgmt Approval</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/SVMX_Manager_Approval_for_Labor_Estimate_2000</template>
    </alerts>
    <alerts>
        <fullName>SVMX_FST_Request_for_Manager_Approval_Estimate_5000</fullName>
        <description>SVMX FST request for Manager to Approve Estimate &gt; 5000</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/SVMX_FST_Approval_Request_for_Estimate_5000</template>
    </alerts>
    <alerts>
        <fullName>SVMX_Manager_Approval_for_Estimate_5000</fullName>
        <description>SVMX_Manager_Approval for Estimate &gt;5000</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/SVMX_Manager_Approval_for_Estimate_5000</template>
    </alerts>
    <alerts>
        <fullName>SVMX_Reopen_Work_Order_Request</fullName>
        <description>SVMX_Reopen_Work_Order_Request</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/SVMX_Reopen_Work_Order_Request</template>
    </alerts>
    <alerts>
        <fullName>Send_email_notification_to_Manager_of_Technician_requesting_Approval_of_Discount</fullName>
        <description>Send email notification to Manager of Technician requesting Approval of Discount</description>
        <protected>false</protected>
        <recipients>
            <field>MemberEmailManager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Discount_Approval_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Sent_when_a_High_Priority_Work_Order_is_created</fullName>
        <description>Sent when a Work Order is Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/High_Priority_Work_Order_Notification</template>
    </alerts>
    <alerts>
        <fullName>Work_Order_is_open_for_4_hours</fullName>
        <description>Work Order is open for 4 hours</description>
        <protected>false</protected>
        <recipients>
            <field>SVMXC__Member_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/High_Priority_Work_Order_Open_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_Approve_WO</fullName>
        <description>T-505944</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Closed</literalValue>
        <name>COMM : Approve WO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Reject_WO</fullName>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>COMM: Reject WO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Date_Time_of_Assignment</fullName>
        <description>Populate Date/Time of Assignment by Today()</description>
        <field>Date_Time_of_Assignment__c</field>
        <formula>NOW()</formula>
        <name>Populate Date/Time of Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Rep_Email_Field_Update</fullName>
        <description>Populate Sales Rep Email</description>
        <field>Sales_Rep_Email__c</field>
        <formula>Sales_Rep__r.Email</formula>
        <name>Sales Rep Email Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSubmitForApproval</fullName>
        <field>IsSubmitforApproval__c</field>
        <literalValue>1</literalValue>
        <name>SetSubmitForApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_On_to_Now</fullName>
        <field>SVMXC__Closed_On__c</field>
        <formula>NOW()</formula>
        <name>Set Closed On to Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Status_to_Approved</fullName>
        <description>It updates the field Order Status and set it to Approved - No Resources Allocated</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Approved - No Resources Allocated</literalValue>
        <name>Set Order Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MemberEmail</fullName>
        <description>COMM-Updates MemberEmail on Work Order as part of Schedule Work Order SFM</description>
        <field>SVMXC__Member_Email__c</field>
        <formula>SVMXC__Group_Member__r.SVMXC__Email__c</formula>
        <name>Update MemberEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MemberManagerEmail</fullName>
        <description>COMM-Updates MemberEmailManager on Work Order as part of Schedule Work Order SFM</description>
        <field>MemberEmailManager__c</field>
        <formula>SVMXC__Group_Member__r.SVMXC__Salesforce_User__r.Manager__r.Email</formula>
        <name>Update MemberManagerEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_to_Dispatched</fullName>
        <description>Will update Order Status to Dispatched</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Dispatched</literalValue>
        <name>Update Order Status to Dispatched</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_to_Open</fullName>
        <description>Updates Order Status to Open</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Update Order Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_to_Scheduled</fullName>
        <description>Will update Order status to Scheduled</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Scheduled</literalValue>
        <name>Update Order Status to Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Override_approved_Field</fullName>
        <field>Override_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Override approved Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner</fullName>
        <description>Update work order fields</description>
        <field>OwnerId</field>
        <lookupValue>Tech_Support_queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <description>Updated status to reassign</description>
        <field>Acceptance_Status__c</field>
        <literalValue>Reassign</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_WO_owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Tech_Support_queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update WO owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_WO_status</fullName>
        <field>Acceptance_Status__c</field>
        <literalValue>Reassign</literalValue>
        <name>Update WO status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ID_Update</fullName>
        <field>WO_ID__c</field>
        <formula>Id</formula>
        <name>WO ID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM %3A Notification to RAQA personnel when Priority is QA</fullName>
        <actions>
            <name>COMM_Email_notification_to_RAQA_when_Priority_QA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Priority__c</field>
            <operation>equals</operation>
            <value>QA</value>
        </criteriaItems>
        <description>notify the RAQA user if the Priority field is set to QA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM %3A Notification to RAQA personnel when Priority is high</fullName>
        <actions>
            <name>COMM_Email_notification_to_RAQA_when_Priority_Highest</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Priority__c</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <description>notify the RAQA user if the Priority field is set to the highest value, which is currently &apos;High&apos;.
Task T-523790</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM %3A Scheduling Unassigned work order to Manager</fullName>
        <active>true</active>
        <description>5 days from the Install Approval Date, If no resources have been assigned to the install, the system should create a task and a due date of TODAY and assign it to the Scheduling Manager.</description>
        <formula>ISBLANK( SVMXC__Group_Member__c ) &amp;&amp; !ISBLANK(SVMXC__Scheduled_Date_Time__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>No_Technician_is_assigned_on_the_Work_order_and_scheduled_date_is_after_5_days</name>
                <type>Task</type>
            </actions>
            <offsetFromField>SVMXC__Service_Order__c.SVMXC__Scheduled_Date_Time__c</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM %3A WO Rejected Escalation</fullName>
        <active>true</active>
        <description>Workflow to notify 3rd party management if Work Order is not resubmitted with in 24 hrs.</description>
        <formula>AND(ISPICKVAL(SVMXC__Order_Status__c ,&apos;Rejected&apos;), ISPICKVAL(SVMXC__Group_Member__r.SVMXC__Role__c ,&apos;Third Party Technician&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_WO_Not_ReSubmitted_Yet</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM %3A Work Order is scheduled outside of SLA</fullName>
        <actions>
            <name>COMM_Work_Order_is_scheduled_outside_of_SLA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Scheduled_Outside_SLA__c</field>
            <operation>contains</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Type__c</field>
            <operation>notEqual</operation>
            <value>Installation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM PFA WO Rejection notification to RAQA</fullName>
        <actions>
            <name>COMM_PFA_WO_Rejection_notification_to_RAQA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Type__c</field>
            <operation>equals</operation>
            <value>PFA</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Acceptance_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>RAQA Group is notified when PFA work order is rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A Email notification to System admin when WO is closed and FST needs additional access</fullName>
        <actions>
            <name>COMM_Email_notification_to_reopen_the_WO</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Revoke_Closed_WO__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>If work order is closed and FST needs additional access, FST can request that the WO be reopened.
T-523147</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A Notification to Scheduling Manager when WO is submitted</fullName>
        <actions>
            <name>COMM_Notification_to_Scheduling_Manager_when_WO_is_submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Type__c</field>
            <operation>equals</operation>
            <value>Installation</value>
        </criteriaItems>
        <description>Email template is sent to the scheduler and includes 
1. Link to the WO
2. Notes that were entered by the installer (related to why they rejected) 
3. Project phase
4. Account</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A Notification to Tech Support Queue when WO rejected</fullName>
        <actions>
            <name>Notification_to_Tech_support_queue_when_WO_Rejected</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Acceptance_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A Notify if WO not accepted by technician after 24 hrs</fullName>
        <active>true</active>
        <description>Notify if WO not accepted by technician after xx hrs</description>
        <formula>AND(NOT(ISNULL(SVMXC__Group_Member__c)),ISPICKVAL(Acceptance_Status__c,&apos;&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_to_Tech_support_queue_when_WO_not_accepted</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_WO_owner</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_WO_status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>SVMXC__Service_Order__c.Date_Time_of_Assignment__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM%3A Populate Date%2FTime of Assignment</fullName>
        <actions>
            <name>Populate_Date_Time_of_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If technician is assigned or changed it populate current date time. I-241962</description>
        <formula>(ISNEW() &amp;&amp;  SVMXC__Group_Member__c != null) || (ISCHANGED(SVMXC__Group_Member__c ) &amp;&amp; SVMXC__Group_Member__c != null)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM-Installation Work Order is assigned</fullName>
        <actions>
            <name>COMM_Notification_Installation_Work_Order_is_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Member_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Type__c</field>
            <operation>equals</operation>
            <value>Installation</value>
        </criteriaItems>
        <description>Sent when an Installation Work Order is assigned to a technician</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM-Update Member and Manager Email</fullName>
        <actions>
            <name>Update_MemberEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_MemberManagerEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() || ISCHANGED(SVMXC__Group_Member__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Discount Approved</fullName>
        <actions>
            <name>Discount_Approved_Notfication</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Discount_Approval__c</field>
            <operation>equals</operation>
            <value>Discount Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Discount Review</fullName>
        <actions>
            <name>Discount_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Discount_Approval__c</field>
            <operation>equals</operation>
            <value>Pending Approval</value>
        </criteriaItems>
        <description>Runs when Discount Approval value is set by Trigger</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>High Priority Work Order open past SLA</fullName>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Priority__c</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>Sent when a High Priority Work Order is open for more than 4 hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Work_Order_is_open_for_4_hours</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>SVMXC__Service_Order__c.CreatedDate</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>More Than 3 Times Reschedule Notification to Manager</fullName>
        <actions>
            <name>Manager_Alert_for_More_Than_3_Times_Reschedule</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sent to Manager Email when work order is rescheduled more than or equal to 3 times</description>
        <formula>(ISCHANGED(Reschedule_Count__c))&amp;&amp;  NOT(ISBLANK( SVMXC__Group_Member__c )) &amp;&amp; NOT(ISBLANK(MemberEmailManager__c)) &amp;&amp;  Reschedule_Count__c &gt;= 3</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>More Than 3 Times Reschedule notification to Group</fullName>
        <actions>
            <name>Group_Alert_for_More_Than_3_Times_Reschedule</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>sent when third party technician and rescheduled more than 3 times. COMM BP : I-237270</description>
        <formula>(ISCHANGED(Reschedule_Count__c))&amp;&amp; Not(ISBLANK( SVMXC__Group_Member__c ))&amp;&amp;(ISBLANK(MemberEmailManager__c)) &amp;&amp; Reschedule_Count__c &gt;= 3</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification Technician when WO is not Accepted</fullName>
        <active>true</active>
        <formula>AND(NOT(ISNULL(SVMXC__Group_Member__c)),ISPICKVAL(Acceptance_Status__c,&apos;&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_Technician_that_WO_is_not_accepted</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>SVMXC__Service_Order__c.SVMXC__First_Assigned_DateTime__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notification when WO is not Accepted</fullName>
        <active>true</active>
        <description>Notify technician&apos;s manager if WO is not accepted by technician with in certain time frame for ex with in 4 hrs.</description>
        <formula>AND(NOT(ISNULL(SVMXC__Group_Member__c)),ISPICKVAL(Acceptance_Status__c,&apos;&apos;),NOT(ISNULL( MemberEmailManager__c )),  $Setup.Notification_Opt_Out__c.COMM_WO_Not_Accepted__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_Manager_and_technician_that_WO_is_not_accepted</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>SVMXC__Service_Order__c.SVMXC__First_Assigned_DateTime__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify Customer On Cancelled</fullName>
        <actions>
            <name>Notify_Customer_On_Cancelled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification will be sent to Customer and Sales rep when a WO will be closed.</description>
        <formula>ISPICKVAL( SVMXC__Order_Status__c , &apos;Canceled&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Customer On Complete</fullName>
        <actions>
            <name>Notify_Customer_on_On_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification will be sent to Customer and Sales rep when a WO will be closed.</description>
        <formula>ISPICKVAL( SVMXC__Order_Status__c , &apos;Closed&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Customer On Dispatch</fullName>
        <actions>
            <name>Notify_Customer_on_On_dispatch</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>T-505924</description>
        <formula>NOT(ISBLANK( SVMXC__Group_Member__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PM Workorder Notification</fullName>
        <actions>
            <name>Notify_Technician_for_PM_Workorders</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>COMM BP : T-506478</description>
        <formula>NOT(ISBLANK( SVMXC__Member_Email__c ))&amp;&amp; NOT(ISBLANK(SVMXC__Group_Member__c)) &amp;&amp;  RecordType.Name = &apos;Service&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Sales Rep Email</fullName>
        <actions>
            <name>Sales_Rep_Email_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stryker COMM BP : Workflow will be populate Email of Sales Rep</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SVMX - When Tech is Assigned</fullName>
        <actions>
            <name>Update_Order_Status_to_Dispatched</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will update Order Status to Dispatched</description>
        <formula>AND(OR(ISCHANGED( SVMXC__Group_Member__c ), ISNEW()),!ISBLANK(SVMXC__Group_Member__c), RecordType.DeveloperName != &apos;Installation&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SVMX - When Work Order Scheduled</fullName>
        <actions>
            <name>Update_Order_Status_to_Scheduled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will update Work Order Status to Scheduled when &quot;Scheduled Date/Time&quot; is not null</description>
        <formula>!ISBLANK( SVMXC__Scheduled_Date_Time__c ) &amp;&amp;  RecordType.DeveloperName != &apos;Installation&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SVMX - When Work Order Un-assigned</fullName>
        <actions>
            <name>Update_Order_Status_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will update Order Status to Open</description>
        <formula>AND(ISCHANGED( SVMXC__Group_Member__c ),ISBLANK(SVMXC__Group_Member__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SVMX_Reopen Work Order Request</fullName>
        <actions>
            <name>SVMX_Reopen_Work_Order_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMX_Reopen_Requested__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Request to Manager to Reopen a Closed Work Order</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Closed on if Not Set</fullName>
        <actions>
            <name>Set_Closed_On_to_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 	ISBLANK(SVMXC__Closed_On__c), 	ISPICKVAL(SVMXC__Order_Status__c, &apos;Closed&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO ID Update</fullName>
        <actions>
            <name>WO_ID_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.RecordTypeId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Work Order is assigned</fullName>
        <actions>
            <name>Sent_when_a_High_Priority_Work_Order_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Member_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Type__c</field>
            <operation>notEqual</operation>
            <value>Installation</value>
        </criteriaItems>
        <description>Sent when a a Work Order is assigned to a technician and is not of WO Type Installation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>No_Technician_is_assigned_on_the_Work_order_and_scheduled_date_is_after_5_days</fullName>
        <assignedToType>owner</assignedToType>
        <description>Work Order should have a technician assigned because there are only 5 days to scheduled date of that order.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>No Technician is assigned on the Work order and scheduled date is after 5 days</subject>
    </tasks>
    <tasks>
        <fullName>Rescheduling_unassigned_work_order</fullName>
        <assignedTo>stephanie.mustain@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Rescheduling unassigned work order</subject>
    </tasks>
</Workflow>
