<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>COMM_Update_Original_Order_number</fullName>
        <description>Update Original Order number from asset.</description>
        <field>Original_Order__c</field>
        <formula>SVMXC__RMA_Shipment_Order__r.SVMX_Asset__r.Order__r.OrderNumber</formula>
        <name>COMM: Update Original Order number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM%3A Update Original Order field on RMA line items</fullName>
        <actions>
            <name>COMM_Update_Original_Order_number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__RMA_Shipment_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RMA</value>
        </criteriaItems>
        <description>Created workflow to display Order no on RMA line items.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
