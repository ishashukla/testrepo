<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Endo_Populate_Unique_Id_on_Account_Conta</fullName>
        <description>Endo Populate Unique Id on Account Contact Association</description>
        <field>Account_Contact_Id__c</field>
        <formula>Associated_Account__c &amp;&quot;_&quot; &amp; Associated_Contact__c</formula>
        <name>Endo Populate Unique Id on Account Conta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Endo Populate Unique Id on Account Contact Association</fullName>
        <actions>
            <name>Endo_Populate_Unique_Id_on_Account_Conta</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Acct_Association__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Endo Populate Unique Id on Account Contact Association</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
