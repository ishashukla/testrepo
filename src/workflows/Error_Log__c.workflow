<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_Alert_To_System_Admin_Public_Group</fullName>
        <description>Send Email Alert To System Admin Public Group</description>
        <protected>false</protected>
        <recipients>
            <recipient>System_Administrator_Users</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Instruments/Error_Log_Record_Email_Template</template>
    </alerts>
    <rules>
        <fullName>Send Email Alert on Error Log Creation</fullName>
        <actions>
            <name>Send_Email_Alert_To_System_Admin_Public_Group</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created as per T-554239, the workflow rule fires whenever a error log object record is created.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
