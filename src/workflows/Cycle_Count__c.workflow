<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_CYCLE_COUNT_Approved</fullName>
        <description>COMM: CYCLE COUNT Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Technician_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_CYCLE_COUNT_Approved</template>
    </alerts>
    <alerts>
        <fullName>COMM_CYCLE_COUNT_Rejected</fullName>
        <description>COMM: CYCLE COUNT Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Technician_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_CYCLE_COUNT_Rejected</template>
    </alerts>
    <alerts>
        <fullName>COMM_Cycle_Count_Complete_Nofification</fullName>
        <description>COMM:Cycle Count Complete Nofification</description>
        <protected>false</protected>
        <recipients>
            <field>Technician_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Cycle_Count_Complete_Nofification</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Notification_to_Technician_New</fullName>
        <description>COMM : Email Notification to Technician New</description>
        <protected>false</protected>
        <recipients>
            <field>Technician_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Email_Notification_to_Technician_New</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_CYCLE_COUNT_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>COMM: CYCLE COUNT Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_CYCLE_COUNT_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>COMM: CYCLE COUNT Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_CYCLE_COUNT_Status_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>COMM: CYCLE COUNT Status Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Cycle_Count_Recalled</fullName>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>COMM:Cycle Count Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Integrate_TO_EBS_True</fullName>
        <field>Integrate_to_EBS__c</field>
        <literalValue>1</literalValue>
        <name>COMM:Integrate TO EBS True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Set_Submit_For_Approval_False</fullName>
        <field>Submit_for_Approval__c</field>
        <literalValue>0</literalValue>
        <name>COMM: Set Submit For Approval False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM %3A Email Notification to Technician New</fullName>
        <actions>
            <name>COMM_Email_Notification_to_Technician_New</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Cycle_Count__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3ACycle Count Complete Nofification</fullName>
        <actions>
            <name>COMM_Cycle_Count_Complete_Nofification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Cycle_Count__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Cycle_Count__c.Due_Date__c</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
