<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_Site_Visit_Approval_Email_Alert</fullName>
        <description>COMM : Site Visit Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Site_Visit_Approval</template>
    </alerts>
    <alerts>
        <fullName>COMM_Site_Visit_Rejection_Email_Alert</fullName>
        <description>COMM : Site Visit Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Site_Visit_Rejection</template>
    </alerts>
    <alerts>
        <fullName>COMM_Submission_Of_Site_Visit_Record_Email_Alert</fullName>
        <description>COMM Submission Of Site Visit Record Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>katie.pruitt@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Site_Visit_Submission</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_Approval_Step</fullName>
        <description>COMM T-502657</description>
        <field>Approval_Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>COMM:Approval Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Visit_Approved</fullName>
        <description>COMM T-502657</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Site Visit Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Visit_Rejected</fullName>
        <description>COMM T-502657</description>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Site Visit Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
