<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approved_Notfication_to_FST</fullName>
        <description>RMA Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/RMA_Approved_2</template>
    </alerts>
    <alerts>
        <fullName>COMM_Notify_TSR_when_Asset_not_covered_by_warranty_or_contract</fullName>
        <description>COMM: Notify TSR when Asset not covered by warranty or contract</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Tech_Support</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Unused_Templates/Asset_not_covered_by_warranty_or_contract</template>
    </alerts>
    <alerts>
        <fullName>Notfiy_FST</fullName>
        <description>Notfiy FST</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Return_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Tech_support_queue_when_RMA_Status_Needs_TSR_Review</fullName>
        <description>Notification to Tech support queue when RMA Status = Needs TSR Review</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Tech_Support</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Notification_for_RMA_approval_to_TSR_queue</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Email_Alert</fullName>
        <description>Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/RMA_Rejected_Alert</template>
    </alerts>
    <alerts>
        <fullName>SYK_RMA_is_Populated</fullName>
        <description>SYK - RMA # is Populated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SVMXC__ServiceMaxEmailTemplates/SYK_RMA_is_Populated</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <description>When Approved :: Update Order Status to Approved by TSR.</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Approved by TSR</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_COMM_CSR_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>COMM_Customer_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to COMM CSR Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMA_Status_Rejected</fullName>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>RMA Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMA_Status_Update</fullName>
        <description>RMA Status updated to Approved by TSR</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Approved by TSR</literalValue>
        <name>RMA Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SVMX_Update_PO_Number</fullName>
        <field>EU_PO__c</field>
        <formula>SVMXC__Service_Engineer__r.FirstName &amp; SVMXC__Service_Engineer__r.LastName</formula>
        <name>SVMX-Update PO Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Set_to_Approved_by_TSR</fullName>
        <description>On approval this will set Status to Approved by TSR</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Approved by TSR</literalValue>
        <name>Status Set to Approved by TSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Set_to_Rejected</fullName>
        <description>On rejection this will set Status to Rejected</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Set to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckSubmitForApproval</fullName>
        <description>COMM-This action unchecks the Submit for Approval field so that the record can be re-submitted if necessary</description>
        <field>COMM_Install_Submit_for_Approval__c</field>
        <literalValue>0</literalValue>
        <name>UncheckSubmitForApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RMA_Status_to_Submitted_for_PM</fullName>
        <description>Updates the RMA Status to &quot;Submitted for PM Review&quot;</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Submitted for PM Review</literalValue>
        <name>Update RMA Status to Submitted for PM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved_by_PM</fullName>
        <description>Updates RMA Status to &quot;Approved by PM&quot; upon approval</description>
        <field>SVMXC__Order_Status__c</field>
        <literalValue>Approved by PM</literalValue>
        <name>Update Status to Approved by PM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM%3A Notify TSR when Asset is not covered</fullName>
        <actions>
            <name>COMM_Notify_TSR_when_Asset_not_covered_by_warranty_or_contract</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>System will warn TSR if the asset associated with the RMA request is not covered by warranty or contract.</description>
        <formula>ISBLANK(SVMX_Asset__r.SVMXC_Coverage__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify TSR Queue for RMA approval</fullName>
        <actions>
            <name>Notification_to_Tech_support_queue_when_RMA_Status_Needs_TSR_Review</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__RMA_Shipment_Order__c.SVMXC__Order_Status__c</field>
            <operation>equals</operation>
            <value>Needs TSR Review</value>
        </criteriaItems>
        <description>Email notification send to TSR queue when RMA status = Needs TSR Review</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SVMX-PO Logic on Parts Order</fullName>
        <actions>
            <name>SVMX_Update_PO_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This will fill in the logic for the parts request on the Parts Order</description>
        <formula>ISBLANK( EU_PO__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SYK - RMA is Populated</fullName>
        <actions>
            <name>SYK_RMA_is_Populated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>RMA # Populated send notification to RMA owner</description>
        <formula>NOT(ISBLANK(Order_Number_Oracle__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Provide_Additional_Information_for_Parts_Order_Approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Provide Additional Information for Parts Order Approval</subject>
    </tasks>
</Workflow>
