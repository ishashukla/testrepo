<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>COMM_Update_Account_Name</fullName>
        <description>COMM: used for search filter</description>
        <field>Account_Name__c</field>
        <formula>Account__r.Name</formula>
        <name>COMM Update Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM Populate Account Name</fullName>
        <actions>
            <name>COMM_Update_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Address__c.Business_Unit_Name__c</field>
            <operation>equals</operation>
            <value>COMM</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
