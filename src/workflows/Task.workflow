<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_COMM_Customer_Service_group_for_the_task</fullName>
        <ccEmails>lbobbitt+commdev@appirio.com</ccEmails>
        <description>Notify COMM Customer Service group for the task</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Task_is_assigned_to_a_group</template>
    </alerts>
    <alerts>
        <fullName>Notify_COMM_Finance_group_for_the_task</fullName>
        <description>Notify COMM Finance group for the task</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Finance</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Task_is_assigned_to_a_group</template>
    </alerts>
    <alerts>
        <fullName>Notify_COMM_Tech_Support_group_for_the_task</fullName>
        <ccEmails>comm.techservices@stryker.com</ccEmails>
        <description>Notify COMM Tech Support group for the task</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Tech_Support</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Task_is_assigned_to_a_group</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Task_as_closed_for_blank_due_dates</fullName>
        <description>Set Task as closed for blank due dates</description>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Set Task as closed for blank due dates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set_as_closed_when_no_due_date_is_present</fullName>
        <actions>
            <name>Set_Task_as_closed_for_blank_due_dates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set_as_closed_when_no_due_date_is_present</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(ISNULL(ActivityDate),OR( RecordType.Name = &apos;Instruments Marketing Task Record&apos;,RecordType.Name = &apos;Instruments Task Record&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task is assigned to COMM Customer Service</fullName>
        <actions>
            <name>Notify_COMM_Customer_Service_group_for_the_task</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If Task is assigned to a group and selected group is  COMM Customer Service</description>
        <formula>AND(OwnerId =  $Label.COMM_Shared_Task_Owner,     ISPICKVAL(Group__c, &apos;COMM Customer Service&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task is assigned to COMM Finance</fullName>
        <actions>
            <name>Notify_COMM_Finance_group_for_the_task</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If Task is assigned to a group and selected group is COMM Finance</description>
        <formula>AND(OwnerId =  $Label.COMM_Shared_Task_Owner,     ISPICKVAL(Group__c, &apos;COMM Finance&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task is assigned to COMM Tech Support</fullName>
        <actions>
            <name>Notify_COMM_Tech_Support_group_for_the_task</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If Task is assigned to a group and selected group is COMM Tech Support</description>
        <formula>AND(OwnerId =  $Label.COMM_Shared_Task_Owner,     ISPICKVAL(Group__c, &apos;COMM Tech Support&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
