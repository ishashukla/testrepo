<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>COMM_Loaner_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved by TSR</literalValue>
        <name>COMM: Loaner Status = Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Loaner_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by TSR</literalValue>
        <name>COMM: Loaner Status = Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Loaner_Submit_for_approval_fals</fullName>
        <field>Submit_for_Approval__c</field>
        <literalValue>0</literalValue>
        <name>COMM : Loaner Submit for approval = fals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Loaner_status_submitted_to_TSR</fullName>
        <field>Status__c</field>
        <literalValue>Submitted to TSR</literalValue>
        <name>COMM : Loaner status = submitted to TSR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Submit_for_Approval_field</fullName>
        <description>This field update is used to uncheck the Submit for Approval field so that it can be reused if needed</description>
        <field>Submit_for_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Submit for Approval field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
