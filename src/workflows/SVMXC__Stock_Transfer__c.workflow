<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Technician_about_new_stock_transfer_request</fullName>
        <description>Notify Technician about new stock transfer request</description>
        <protected>false</protected>
        <recipients>
            <field>ReqFromTechEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/New_Stock_Transfer_Request</template>
    </alerts>
    <alerts>
        <fullName>Rejection_notification_to_Requested_By_Technician</fullName>
        <description>Rejection notification to Requested By Technician</description>
        <protected>false</protected>
        <recipients>
            <field>ReqByTechEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Stock_Transfer_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Stock_transfer_Accepted_Notification_to_Requested_By_Technician</fullName>
        <description>Stock transfer Accepted Notification to Requested By Technician</description>
        <protected>false</protected>
        <recipients>
            <field>ReqByTechEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Stock_transfer_Accepted_Notification_Template</template>
    </alerts>
    <rules>
        <fullName>Notification of New Record</fullName>
        <actions>
            <name>Notify_Technician_about_new_stock_transfer_request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Stock_Transfer__c.ReqFromTechEmail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification of new request creation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification on Rejection</fullName>
        <actions>
            <name>Rejection_notification_to_Requested_By_Technician</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Stock_Transfer__c.Acceptance_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Stock_Transfer__c.ReqByTechEmail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sent when Acceptance Status is Reject</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stock transfer Accepted Notification to Requested By Technician</fullName>
        <actions>
            <name>Stock_transfer_Accepted_Notification_to_Requested_By_Technician</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Stock_Transfer__c.Acceptance_Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Stock_Transfer__c.ReqByTechEmail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sent when Acceptance Status is Accepted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
