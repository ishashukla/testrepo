<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Oppty_RT_to_COMM</fullName>
        <description>COMM T-498070</description>
        <field>Opportunity_Record_Type__c</field>
        <name>Update Oppty RT to COMM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM Update Oppty RT Field of Quote</fullName>
        <actions>
            <name>Update_Oppty_RT_to_COMM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>BigMachines__Quote__c.Opportunity_Record_Type__c</field>
            <operation>equals</operation>
            <value>$Label.COMM</value>
        </criteriaItems>
        <description>COMM T-498070</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
