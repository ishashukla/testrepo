<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Notify_Account_Owner_on_Account_Edit_By_Comm_Finance</fullName>
        <description>Account: Send Email Account Owner on Account Edit By Comm Finance</description>
        <protected>false</protected>
        <recipients>
            <field>COMM_Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/New_Customer_Account_Created_Auto_Response_to_Owner</template>
    </alerts>
    <alerts>
        <fullName>Account_Send_Email_To_Account_Owner_When_Account_Merged</fullName>
        <description>Account: Send Email To Account Owner When Account Merged.</description>
        <protected>false</protected>
        <recipients>
            <field>COMM_Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/New_Customer_Account_Merged_Auto_Response_to_Owner</template>
    </alerts>
    <alerts>
        <fullName>Account_Send_Email_on_unverified_account_creation1</fullName>
        <ccEmails>sccnewaccounts@stryker.com</ccEmails>
        <description>Account: Send Email on unverified account creation</description>
        <protected>false</protected>
        <recipients>
            <field>WF_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>mehul.jain@appirio.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/Account_Unverified_Account_Created</template>
    </alerts>
    <alerts>
        <fullName>COMM_Account_Notify_Contracts_Administrator_when_a_new_Customer_Request_has_been</fullName>
        <ccEmails>joseph.goings@stryker.com</ccEmails>
        <description>COMM USA Account-Notify Contracts Administrator when a new Customer Request has been submitted</description>
        <protected>false</protected>
        <recipients>
            <recipient>jacob.nail@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Unverified_Account_Notification</template>
    </alerts>
    <alerts>
        <fullName>COMM_Account_Send_Email_Account_Owner_on_Account_Edit_By_Integration_User</fullName>
        <description>COMM Account: Send Email Account Owner on Account Edit By Integration User</description>
        <protected>false</protected>
        <recipients>
            <field>COMM_Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/New_Customer_Account_Created_Auto_Response_to_Owner</template>
    </alerts>
    <alerts>
        <fullName>COMM_Sending_Email_to_Merged_Account_Owner</fullName>
        <description>COMM : Sending Email to Merged Account Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Merged_Account_Email_2__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Auto_Response_To_Account_Owner_Of_Merged_Account</template>
    </alerts>
    <alerts>
        <fullName>COMM_Sends_Email_to_The_Account_Owner_of_Mergerd_Account</fullName>
        <description>COMM: Sends Email to The Account Owner of Mergerd Account</description>
        <protected>false</protected>
        <recipients>
            <field>Merged_Account_Email_1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/New_Customer_Account_Merged_Auto_Response_to_Merged_Account_Owner</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_WF_Email_field_update</fullName>
        <field>WF_Email__c</field>
        <formula>$Label.Comm_Finance_Email</formula>
        <name>Account: WF Email field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Account_unverified_field_update</fullName>
        <description>I-206465 - IF approval status is approved, automatically update the unverified field to FALSE</description>
        <field>Unverified__c</field>
        <literalValue>0</literalValue>
        <name>COMM: Account unverified field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Auto_Approve_Corporate_Entity_Accs</fullName>
        <description>I-207263 - Workflow field update created to set unverified field to false and approval status to true</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>COMM Auto Approve Corporate Entity Accs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Billing_Country_by_Code_FieldUpdate</fullName>
        <field>BillingCountry</field>
        <formula>CASE( Country_Code__c, &quot;Afghanistan&quot;, &quot;AF&quot;, &quot;&quot;)</formula>
        <name>COMM Billing Country by Code FieldUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Change_Billing_Country_by_Code</fullName>
        <description>Updates the country field of Account</description>
        <field>BillingCountry</field>
        <formula>CASE(Country_Code__c,&quot;Afghanistan&quot;,&quot;AF&quot;,&quot;Ägypten&quot;,&quot;EG&quot;,&quot;Albanien&quot;,&quot;AL&quot;,&quot;Algerien&quot;,&quot;DZ&quot;,&quot;Amer.Jungferni.&quot;,&quot;VI&quot;,&quot;Andorra&quot;,&quot;AD&quot;,&quot;Angola&quot;,&quot;AO&quot;,&quot;Anguilla&quot;,&quot;AI&quot;,&quot;Antarctica&quot;,&quot;AQ&quot;,&quot;Antigua/Barbuda&quot;,&quot;AG&quot;,&quot;Äquatorialguine&quot;,&quot;GQ&quot;,&quot;Argentinien&quot;,&quot;AR&quot;,&quot;Armenien&quot;,&quot;AM&quot;,&quot;Aruba&quot;,&quot;AW&quot;,&quot;Aserbaidschan&quot;,&quot;AZ&quot;,&quot;Äthiopien&quot;,&quot;ET&quot;,&quot;Australia&quot;,&quot;AU&quot;,&quot;Bahamas&quot;,&quot;BS&quot;,&quot;Bahrain&quot;,&quot;BH&quot;,&quot;Bangladesh&quot;,&quot;BD&quot;,&quot;Barbados&quot;,&quot;BB&quot;,&quot;Belgien&quot;,&quot;BE&quot;,&quot;Belize&quot;,&quot;BZ&quot;,&quot;Benin&quot;,&quot;BJ&quot;,&quot;Bermuda&quot;,&quot;BM&quot;,&quot;Bhutan&quot;,&quot;BT&quot;,&quot;Bolivien&quot;,&quot;BO&quot;,&quot;Bosnien-Herz.&quot;,&quot;BA&quot;,&quot;Botsuana&quot;,&quot;BW&quot;,&quot;Brasilien&quot;,&quot;BR&quot;,&quot;Brunei&quot;,&quot;BN&quot;,&quot;Bulgarien&quot;,&quot;BG&quot;,&quot;Burkina-Faso&quot;,&quot;BF&quot;,&quot;Burma&quot;,&quot;BU&quot;,&quot;Burundi&quot;,&quot;BI&quot;,&quot;Canada&quot;,&quot;CA&quot;,&quot;Chile&quot;,&quot;CL&quot;,&quot;China&quot;,&quot;CN&quot;,&quot;Cookinseln&quot;,&quot;CK&quot;,&quot;Costa Rica&quot;, &quot;CR&quot;,&quot;Dänemark&quot;,&quot;DK&quot;,&quot;Deutschland&quot;,&quot;DE&quot;,&quot;Dominica&quot;,&quot;DM&quot;,&quot;Dschibuti&quot;,&quot;DJ&quot;,&quot;Ecuador&quot;,&quot;EC&quot;,&quot;ElSalvador&quot;,&quot;SV&quot;,&quot;Elfenbeinküste&quot;,&quot;CI&quot;,&quot;Eritrea&quot;,&quot;ER&quot;,&quot;Estland&quot;,&quot;EE&quot;,&quot;Falklandinseln&quot;,&quot;FK&quot;,&quot;Färöer&quot;,&quot;FO&quot;,&quot;Fidschi&quot;,&quot;FJ&quot;,&quot;Finnland&quot;,&quot;FI&quot;,&quot;Frankreich&quot;,&quot;FR&quot;,&quot;Franz.Guayana&quot;,&quot;GF&quot;,&quot;Gabun&quot;,&quot;GA&quot;,&quot;Gambia&quot;,&quot;GM&quot;,&quot;Georgien&quot;,&quot;GE&quot;,&quot;Ghana&quot;,&quot;GH&quot;,&quot;Gibraltar&quot;,&quot;GI&quot;,&quot;Grenada&quot;,&quot;GD&quot;,&quot;Griechenland&quot;,&quot;GR&quot;,&quot;Grönland&quot;,&quot;GL&quot;,&quot;Guadeloupe&quot;,&quot;GP&quot;,&quot;Guam&quot;,&quot;GU&quot;,&quot;Guatemala&quot;,&quot;GT&quot;,&quot;Guinea&quot;,&quot;GN&quot;,&quot;Guinea-Bissau&quot;,&quot;GW&quot;,&quot;Guyana&quot;,&quot;GY&quot;,&quot;Haiti&quot;,&quot;HT&quot;,&quot;Heard/McDon&quot;,&quot;HM&quot;,&quot;Honduras&quot;,&quot;HN&quot;,&quot;Hongkong&quot;,&quot;HK&quot;,&quot;India&quot;,&quot;IN&quot;,&quot;Indonesia&quot;,&quot;ID&quot;,&quot;Irak&quot;,&quot;IQ&quot;,&quot;Iran&quot;,&quot;IR&quot;,&quot;Irland&quot;,&quot;IE&quot;,&quot;Island&quot;,&quot;IS&quot;,&quot;Israel&quot;,&quot;IL&quot;,&quot;Italia&quot;,&quot;IT&quot;,&quot;Jamaika&quot;,&quot;JM&quot;,&quot;Japan&quot;,&quot;JP&quot;,&quot;Jemen&quot;,&quot;YE&quot;,&quot;Jordanien&quot;,&quot;JO&quot;,&quot;Jugoslawien&quot;,&quot;YU&quot;,&quot;Kaimaninseln&quot;,&quot;KY&quot;,&quot;Kambodscha&quot;,&quot;KH&quot;,&quot;Kamerun&quot;,&quot;CM&quot;,&quot;KapVerde&quot;,&quot;CV&quot;,&quot;Kasachstan&quot;,&quot;KZ&quot;,&quot;Kenia&quot;,&quot;KE&quot;,&quot;Kirgistan&quot;,&quot;KG&quot;,&quot;Kiribati&quot;,&quot;KI&quot;,&quot;Kokosinseln&quot;,&quot;CC&quot;,&quot;Kolumbien&quot;,&quot;CO&quot;,&quot;Komoren&quot;,&quot;KM&quot;,&quot;Kongo&quot;,&quot;CG&quot;,&quot;Kosovo&quot;,&quot;XK&quot;,&quot;Kroatien&quot;,&quot;HR&quot;,&quot;Kuba&quot;,&quot;CU&quot;,&quot;Kuwait&quot;,&quot;KW&quot;,&quot;Laos&quot;,&quot;LA&quot;,&quot;Lesotho&quot;,&quot;LS&quot;,&quot;Lettland&quot;,&quot;LV&quot;,&quot;Libanon&quot;,&quot;LB&quot;,&quot;Liberia&quot;,&quot;LR&quot;,&quot;Libyen&quot;,&quot;LY&quot;,&quot;Liechtenstein&quot;,&quot;LI&quot;,&quot;Litauen&quot;,&quot;LT&quot;,&quot;Luxemburg&quot;,&quot;LU&quot;,&quot;Macau&quot;,&quot;MO&quot;,&quot;Madagaskar&quot;,&quot;MG&quot;,&quot;Malawi&quot;,&quot;MW&quot;,&quot;Malaysia&quot;,&quot;MY&quot;,&quot;Malediven&quot;,&quot;MV&quot;,&quot;Mali&quot;,&quot;ML&quot;,&quot;Malta&quot;,&quot;MT&quot;,&quot;Marokko&quot;,&quot;MA&quot;,&quot;Marshall-I.&quot;,&quot;MH&quot;,&quot;Martinique&quot;,&quot;MQ&quot;,&quot;Mauretanien&quot;,&quot;MR&quot;,&quot;Mauritius&quot;,&quot;MU&quot;,&quot;Mayotte&quot;,&quot;YT&quot;,&quot;Mazedonien&quot;,&quot;MK&quot;,&quot;Mexico&quot;,&quot;MX&quot;,&quot;Micronesien&quot;,&quot;FM&quot;,&quot;MinorOutl.Ins.&quot;,&quot;UM&quot;,&quot;Moldau&quot;,&quot;MD&quot;,&quot;Monaco&quot;,&quot;MC&quot;,&quot;Mongolei&quot;,&quot;MN&quot;,&quot;Montenegro&quot;,&quot;ME&quot;,&quot;Montserrat&quot;,&quot;MS&quot;,&quot;Mosambik&quot;,&quot;MZ&quot;,&quot;Myanmar&quot;,&quot;MM&quot;,&quot;N.MarianaIns&quot;,&quot;MP&quot;,&quot;Namibia&quot;,&quot;NA&quot;,&quot;Nauru&quot;,&quot;NR&quot;,&quot;Nepal&quot;,&quot;NP&quot;,&quot;Neukaledonien&quot;,&quot;NC&quot;,&quot;New Zealand&quot;,&quot;NZ&quot;,&quot;Nicaragua&quot;,&quot;NI&quot;,&quot;Niederl.Antill.&quot;,&quot;AN&quot;,&quot;Niederlande&quot;,&quot;NL&quot;,&quot;Niger&quot;,&quot;NE&quot;,&quot;Nigeria&quot;,&quot;NG&quot;,&quot;Niue-Inseln&quot;,&quot;NU&quot;,&quot;Nordkorea&quot;,&quot;KP&quot;,&quot;Norfolkinseln&quot;,&quot;NF&quot;,&quot;Norwegen&quot;,&quot;NO&quot;,&quot;Oman&quot;,&quot;OM&quot;,&quot;OstTimor&quot;,&quot;TP&quot;,&quot;Österreich&quot;,&quot;AT&quot;,&quot;Pakistan&quot;,&quot;PK&quot;,&quot;Palau&quot;,&quot;PW&quot;,&quot;Panama&quot;,&quot;PA&quot;,&quot;Papua-Neugui.&quot;,&quot;PG&quot;,&quot;Paraguay&quot;,&quot;PY&quot;,&quot;Peru&quot;,&quot;PE&quot;,&quot;Philippines&quot;,&quot;PH&quot;,&quot;Polen&quot;,&quot;PL&quot;,&quot;Portugal&quot;,&quot;PT&quot;,&quot;Puerto Rico&quot;,&quot;PR&quot;,&quot;Qatar&quot;,&quot;QA&quot;,&quot;Rep.Dominicana&quot;,&quot;DO&quot;,&quot;RepublikKongo&quot;,&quot;CD&quot;,&quot;Reunion&quot;,&quot;RE&quot;,&quot;Ruanda&quot;,&quot;RW&quot;,&quot;Rumänien&quot;,&quot;RO&quot;,&quot;Russische Foed.&quot;,&quot;RU&quot;,&quot;S.SandwichIns&quot;,&quot;GS&quot;,&quot;S.Tome&quot;,&quot;ST&quot;,&quot;Salomonen&quot;,&quot;SB&quot;,&quot;Sambia&quot;,&quot;ZM&quot;,&quot;Samoa&quot;,&quot;AS&quot;,&quot;SanMarino&quot;,&quot;SM&quot;,&quot;Saudi-Arabien&quot;,&quot;SA&quot;,&quot;Schweden&quot;,&quot;SE&quot;,&quot;Schweiz&quot;,&quot;CH&quot;,&quot;Senegal&quot;,&quot;SN&quot;,&quot;Serbia&quot;,&quot;CS&quot;,&quot;Seychellen&quot;,&quot;SC&quot;,&quot;SierraLeone&quot;,&quot;SL&quot;,&quot;Simbabwe&quot;,&quot;ZW&quot;,&quot;Singapore&quot;,&quot;SG&quot;,&quot;Slowakei&quot;,&quot;SK&quot;,&quot;Slowenien&quot;,&quot;SI&quot;,&quot;Somalia&quot;,&quot;SO&quot;,&quot;Spanien&quot;,&quot;ES&quot;,&quot;SriLanka&quot;,&quot;LK&quot;,&quot;St.Helena&quot;,&quot;SH&quot;,&quot;St.Lucia&quot;,&quot;LC&quot;,&quot;St.Vincent&quot;,&quot;VC&quot;,&quot;Südafrika&quot;,&quot;ZA&quot;,&quot;Sudan&quot;,&quot;SD&quot;,&quot;Südkorea&quot;,&quot;KR&quot;,&quot;Suriname&quot;,&quot;SR&quot;,&quot;Svalbard&quot;,&quot;SJ&quot;,&quot;Swasiland&quot;,&quot;SZ&quot;,&quot;Syrien&quot;,&quot;SY&quot;,&quot;Tadschikistan&quot;,&quot;TJ&quot;,&quot;Taiwan&quot;,&quot;TW&quot;,&quot;Tansania&quot;,&quot;TZ&quot;,&quot;Thailand&quot;,&quot;TH&quot;,&quot;Togo&quot;,&quot;TG&quot;,&quot;Tonga&quot;,&quot;TO&quot;,&quot;Trinidad,Tobago&quot;,&quot;TT&quot;,&quot;Tschad&quot;,&quot;TD&quot;,&quot;Tschech.Rep.&quot;,&quot;CZ&quot;,&quot;Tunesien&quot;,&quot;TN&quot;,&quot;Türkei&quot;,&quot;TR&quot;,&quot;Turkmenistan&quot;,&quot;TM&quot;,&quot;Turks-,Caicosin&quot;,&quot;TC&quot;,&quot;Tuvalu&quot;,&quot;TV&quot;,&quot;Uganda&quot;,&quot;UG&quot;,&quot;Ukraine&quot;,&quot;UA&quot;,&quot;Ungarn&quot;,&quot;HU&quot;,&quot;United Kingdom&quot;,&quot;UK&quot;,&quot;Großbritannien&quot;,&quot;UK&quot;,&quot;Uruguay&quot;,&quot;UY&quot;,&quot;USA&quot;,&quot;US&quot;,&quot;Usbekistan&quot;,&quot;UZ&quot;,&quot;Vanuatu&quot;,&quot;VU&quot;,&quot;Vatikanstadt&quot;,&quot;VA&quot;,&quot;Venezuela&quot;,&quot;VE&quot;,&quot;Ver.Arab.Emir.&quot;,&quot;AE&quot;,&quot;Vietnam&quot;,&quot;VN&quot;,&quot;Weissrussland&quot;,&quot;BY&quot;,&quot;WestSahara&quot;,&quot;EH&quot;,&quot;Westsamoa&quot;,&quot;WS&quot;,&quot;Zaire&quot;,&quot;ZR&quot;,&quot;Zentralaf.Rep.&quot;,&quot;CF&quot;,&quot;Zypern&quot;,&quot;CY&quot;, &quot;Sri Lanka&quot;,&quot;LK&quot;,&quot;Not defined&quot;)</formula>
        <name>COMM Change Billing Country by Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_IntAccount_RT_Update_on_LeadConvert</fullName>
        <description>COMM - Field Update Created for I-208021 to force record type assignment for account on lead conversion for intl accounts</description>
        <field>RecordTypeId</field>
        <lookupValue>COMM_Intl_Accounts</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>COMM IntAccount RT Update on LeadConvert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_US_Account_RT_Update_on_LeadConvert</fullName>
        <description>COMM - Field Update Created for I-208021 to force record type assignment for account on lead conversion
COMM T-523263 -Modified RT</description>
        <field>RecordTypeId</field>
        <lookupValue>Endo_COMM_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>COMM US Account RT Update on LeadConvert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Account_Region</fullName>
        <description>FF Set Account Region</description>
        <field>Flex_Region__c</field>
        <formula>CASE(Territory_Assignment_Attribute__c, 
&quot;Alaska&quot;, &quot;Northwest&quot;,
&quot;Colorado&quot;, &quot;Northwest&quot;,
&quot;Idaho&quot;, &quot;Northwest&quot;,
&quot;Montana&quot;, &quot;Northwest&quot;,
&quot;North Dakota&quot;, &quot;Northwest&quot;,
&quot;Oregon&quot;, &quot;Northwest&quot;,
&quot;South Dakota&quot;, &quot;Northwest&quot;,
&quot;Utah&quot;, &quot;Northwest&quot;,
&quot;Washington&quot;, &quot;Northwest&quot;,
&quot;Wyoming&quot;, &quot;Northwest&quot;,
&quot;Arizona&quot;, &quot;West&quot;,
&quot;California&quot;, &quot;West&quot;,
&quot;Hawaii&quot;, &quot;West&quot;,
&quot;Nevada&quot;, &quot;West&quot;,
&quot;Arkansas&quot;, &quot;Southwest&quot;,
&quot;New Mexico&quot;, &quot;Southwest&quot;,
&quot;Oklahoma&quot;, &quot;Southwest&quot;,
&quot;Texas&quot;, &quot;Southwest&quot;,
&quot;Illinois&quot;, &quot;Midwest&quot;,
&quot;Iowa&quot;, &quot;Midwest&quot;,
&quot;Kansas&quot;, &quot;Midwest&quot;,
&quot;Minnesota&quot;, &quot;Midwest&quot;,
&quot;Missouri&quot;, &quot;Midwest&quot;,
&quot;Nebraska&quot;, &quot;Midwest&quot;,
&quot;Wisconsin&quot;, &quot;Midwest&quot;,
&quot;Indiana&quot;, &quot;Ohio Valley&quot;,
&quot;Kentucky&quot;, &quot;Ohio Valley&quot;,
&quot;Michigan&quot;, &quot;Ohio Valley&quot;,
&quot;Ohio&quot;, &quot;Ohio Valley&quot;,
&quot;West Virginia&quot;, &quot;Ohio Valley&quot;,
&quot;Alabama&quot;, &quot;Gulf Coast&quot;,
&quot;Florida&quot;, &quot;Gulf Coast&quot;,
&quot;Louisiana&quot;, &quot;Gulf Coast&quot;,
&quot;Mississippi&quot;, &quot;Gulf Coast&quot;,
&quot;Connecticut&quot;, &quot;Northeast&quot;,
&quot;Maine&quot;, &quot;Northeast&quot;,
&quot;Massachusetts&quot;, &quot;Northeast&quot;,
&quot;New Hampshire&quot;, &quot;Northeast&quot;,
&quot;New York&quot;, &quot;Northeast&quot;,
&quot;Rhode Island&quot;, &quot;Northeast&quot;,
&quot;Vermont&quot;, &quot;Northeast&quot;,
&quot;Delaware&quot;, &quot;Mid-Atlantic&quot;,
&quot;District of Columbia&quot;, &quot;Mid-Atlantic&quot;,
&quot;Maryland&quot;, &quot;Mid-Atlantic&quot;,
&quot;New Jersey&quot;, &quot;Mid-Atlantic&quot;,
&quot;Pennsylvania&quot;, &quot;Mid-Atlantic&quot;,
&quot;Virginia&quot;, &quot;Mid-Atlantic&quot;,
&quot;Georgia&quot;, &quot;Southeast&quot;,
&quot;North Carolina&quot;, &quot;Southeast&quot;,
&quot;South Carolina&quot;, &quot;Southeast&quot;,
&quot;Tennessee&quot;, &quot;Southeast&quot;,
&quot;International&quot;)</formula>
        <name>Flex Set Account Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Legal_Name_Active_Date</fullName>
        <field>Legal_Name_Active_Date__c</field>
        <formula>NOW()</formula>
        <name>Populate Legal Name Active Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Master_In_Merge_on_Account</fullName>
        <description>after merging process set Is_Master_Checkbox field of account object to false, so this workflow will not fire if simple delete operation is going on</description>
        <field>Is_Master_In_Merge__c</field>
        <literalValue>0</literalValue>
        <name>Update Is Master In Merge on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account%3A Notify Account Owner on Edit By COMM Finance</fullName>
        <actions>
            <name>Account_Notify_Account_Owner_on_Account_Edit_By_Comm_Finance</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>COMM T-523263; Modified as per chatter on T-523266</description>
        <formula>AND(AND( ISCHANGED(Unverified__c),Unverified__c = false),   OR(RecordType.DeveloperName = &apos;COMM_Corporate_Accounts&apos;, RecordType.DeveloperName = &apos;COMM_Inter_Company&apos;, RecordType.DeveloperName = &apos;COMM_Intl_Accounts&apos;, RecordType.DeveloperName = &apos;COMM_Intl_Dealer&apos;, RecordType.DeveloperName = &apos;Endo_COMM_Customer&apos;, RecordType.DeveloperName = &apos;COMM_Intl_Entity&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3ANotify Finance on unverified COMM Account Creation</fullName>
        <actions>
            <name>Account_Send_Email_on_unverified_account_creation1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_WF_Email_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Notify finance when a comm account is created and unverified is true
05/04 - NB - Modified the workflow to skip for Integration users
COMM T-523263 - modified RT</description>
        <formula>AND( Unverified__c,  OR(RecordType.DeveloperName = &apos;COMM_Intl_Accounts&apos;,  RecordType.DeveloperName = &apos;COMM_Intl_Dealer&apos;,  RecordType.DeveloperName = &apos;Endo_COMM_Customer&apos;), $User.FirstName != &apos;Integration&apos;, $Profile.Name != &apos;DataMigrationIntegration&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM %3A Send Mail To Merged Account Owner</fullName>
        <actions>
            <name>COMM_Sending_Email_to_Merged_Account_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Is_Master_In_Merge__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merged_Account_Email_2__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Account%3A Notify Account Owner on Create and Edit By Integration User</fullName>
        <actions>
            <name>COMM_Account_Send_Email_Account_Owner_on_Account_Edit_By_Integration_User</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Integration User inserts a new Account or updates an existing account, notify the Account owner</description>
        <formula>LastModifiedBy.Profile.Name = $Label.COMM_Integration_Profile</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Auto Verify Corporate%2FEntity Accounts</fullName>
        <actions>
            <name>COMM_Account_unverified_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>COMM_Auto_Approve_Corporate_Entity_Accs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-207263 - Workflow rule created to set unverified field to false and approval status to true
I-225920 - The Unverified flag should be set to False and the Approval Status = Approved, for all accounts coming from EBS</description>
        <formula>OR( AND(  OR(Unverified__c , NOT(ISPICKVAL( Approval_Status__c , &apos;Approved&apos;))),  OR(RecordType.DeveloperName = &apos;COMM_Corporate_Accounts&apos;,  RecordType.DeveloperName = &apos;COMM_Inter_Company&apos;,  RecordType.DeveloperName = &apos;COMM_US_Accounts&apos;,  RecordType.DeveloperName = &apos;COMM_Intl_Entity&apos;, RecordType.DeveloperName = &apos;Endo_COMM_Customer&apos;),  $Profile.Name = &apos;DataMigrationIntegration&apos;) , AND(Unverified__c,  OR(RecordType.DeveloperName = &apos;COMM_Corporate_Accounts&apos;,  RecordType.DeveloperName = &apos;COMM_Inter_Company&apos;,  RecordType.DeveloperName = &apos;COMM_Intl_Entity&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Change Billing Country by Code</fullName>
        <actions>
            <name>COMM_Billing_Country_by_Code_FieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notContain</operation>
            <value>NOUSER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM Inactivate an Account</fullName>
        <actions>
            <name>COMM_Inactivateaccount</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <description>Move the account to an &quot;inactive&quot; status so that it cannot be seen by the end user, but still retains all relevant product and contact information.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Set Account RT on Lead Conversion</fullName>
        <actions>
            <name>COMM_US_Account_RT_Update_on_LeadConvert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_RT_on_Lead_Convert__c</field>
            <operation>equals</operation>
            <value>Endo COMM Customer</value>
        </criteriaItems>
        <description>COMM - This rule evaluates when a lead is converted and updated Account RT based on lead country (Ref : I-208021) COMM T-523263 -Modified RT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Set Account RT on Lead Conversion Intl</fullName>
        <actions>
            <name>COMM_IntAccount_RT_Update_on_LeadConvert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_RT_on_Lead_Convert__c</field>
            <operation>equals</operation>
            <value>COMM Intl Accounts</value>
        </criteriaItems>
        <description>COMM - This rule evaluates when a lead is converted and updated Account RT based on lead country for international accounts(Ref : I-208021)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM USA Account-Notify Contracts Admin when a new Customer Account request has been submitted</fullName>
        <actions>
            <name>COMM_Account_Notify_Contracts_Administrator_when_a_new_Customer_Request_has_been</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Country_Code__c</field>
            <operation>equals</operation>
            <value>United States</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <description>Notify Contracts Admin when a new Customer Account request has been submitted</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM-Account Update Unverified on Approval</fullName>
        <actions>
            <name>COMM_Account_unverified_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Account.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Intl Dealer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Corporate Accounts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Intl Accounts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Endo COMM Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Inter Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Intl Entity</value>
        </criteriaItems>
        <description>Update Unverified Field when Record is approved - I-206465
COMM T-523263 -Modified RT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Account Region</fullName>
        <actions>
            <name>Flex_Set_Account_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($Setup.By_Pass_Config__c.Bypass_WF__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New CustomerAccount Auto Response To Owner Rule</fullName>
        <actions>
            <name>Account_Send_Email_To_Account_Owner_When_Account_Merged</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>COMM_Sends_Email_to_The_Account_Owner_of_Mergerd_Account</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Is_Master_In_Merge_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>send a email to Account owner when some other account merge into it.</description>
        <formula>IF( Is_Master_In_Merge__c = true, true,false )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Account Legal Name Active Date Rule</fullName>
        <actions>
            <name>Populate_Legal_Name_Active_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Rule Populated the Legal Name Active Date field on Account , When Legal Name field on Account is Changed.</description>
        <formula>AND(NOT( $Setup.By_Pass_Config__c.Bypass_WF__c), ISCHANGED( Legal_Name__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>COMM_Inactivateaccount</fullName>
        <assignedTo>mehul.jain@appirio.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Inactivate this account in the following manner: 

1. Ensure that there are no existing leads or opportunities associated with this account. 
2. Move the account to a designated &quot;Inactive&quot; user</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Inactivate account</subject>
    </tasks>
</Workflow>
