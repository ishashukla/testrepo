<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UniqueRepIdBU</fullName>
        <description>Merge Mancode #, Sales Rep ID and Business Unit and update on Unique Identifier Field</description>
        <field>Unique_Identifier__c</field>
        <formula>Name+ Sales_Rep__r.Id + TEXT( Business_Unit__c )</formula>
        <name>UniqueMancodeRepIdBU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mancode_Name</fullName>
        <field>Name</field>
        <formula>IF(!ISBLANK(Territory__r.Mancode__c),Territory__r.Mancode__c, Name )</formula>
        <name>Update Mancode Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Mancode Name</fullName>
        <actions>
            <name>Update_Mancode_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This keeps the mancode name in sync with the Territory Mancode number that is tied through the lookup.</description>
        <formula>!ISBLANK(Territory__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Unique Identifier Field</fullName>
        <actions>
            <name>UniqueRepIdBU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule updates Unique Identifier field of Mancode object</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(NOT(ISNULL(TEXT((Business_Unit__c)))),NOT(ISNULL(Sales_Rep__r.Id ))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
