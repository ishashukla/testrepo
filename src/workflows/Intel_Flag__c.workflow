<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_BA_when_Intel_Flag_record_is_created</fullName>
        <description>Email BA when Intel Flag record is created</description>
        <protected>false</protected>
        <recipients>
            <recipient>NV_Business_Admin</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_NV/Intel_Flag_Created</template>
    </alerts>
    <rules>
        <fullName>Email Business Admin on Intel Flag Create</fullName>
        <actions>
            <name>Email_BA_when_Intel_Flag_record_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intel_Flag__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Stryker NV:  This workflow rule sends email to NV Business Admin when new Intel Flag record is created to assist with monitoring and compliance</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
