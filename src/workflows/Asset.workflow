<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Custom_Asset_Description_Field</fullName>
        <field>Asset_Description__c</field>
        <formula>Description</formula>
        <name>Update Custom Asset Description Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Custom Asset Description WR</fullName>
        <actions>
            <name>Update_Custom_Asset_Description_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(   (RecordType.DeveloperName == &apos;Medical&apos;),   OR(     AND(ISNEW(), NOT(ISBLANK(Description))), 	ISCHANGED(Description)   ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
