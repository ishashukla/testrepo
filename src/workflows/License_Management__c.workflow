<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Use_Number_of_License</fullName>
        <description>Use Number of License</description>
        <protected>false</protected>
        <recipients>
            <recipient>LicenseGroup</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Use_Number_of_License</template>
    </alerts>
    <rules>
        <fullName>LicencseInUsed</fullName>
        <actions>
            <name>Use_Number_of_License</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), Lic_In_Use__c  &gt;  Lic_Purchesed__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
