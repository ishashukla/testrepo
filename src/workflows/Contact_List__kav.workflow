<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_List_Article_Review_Reminder</fullName>
        <description>Contact List Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Medical_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/Contact_List_Article_Review_Notification</template>
    </alerts>
    <alerts>
        <fullName>Contact_List_Rejection_Notification</fullName>
        <description>Contact List Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Article_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>ICCKM_Contact_List_Article_Review_Reminder</fullName>
        <description>ICCKM Contact List Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Instrument_CCKM_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/Contact_List_Article_Review_Notification</template>
    </alerts>
    <knowledgePublishes>
        <fullName>Contact_List_Approve_Publish</fullName>
        <action>Publish</action>
        <label>Contact List: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Publish_Approved_Article</fullName>
        <action>Publish</action>
        <label>Publish Approved Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>Contact List Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to Medical Knowledge Group users on Review Date in Contact List Article</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(BEGINS($UserRole.DeveloperName,&quot;Medical&quot;),!ISNULL(Review_Date__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contact_List_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contact_List__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ICCKM Contact List Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to ICCKM Knowledge Group users on Review Date in Contact List Article</description>
        <formula>AND(BEGINS($UserRole.DeveloperName,&quot;Instruments&quot;),!ISNULL(Review_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ICCKM_Contact_List_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contact_List__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
