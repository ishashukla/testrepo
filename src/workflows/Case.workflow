<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Confirmation_for_Repair_Discount</fullName>
        <description>Approval Confirmation for Repair Discount</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Repair_Discount_Approval</template>
    </alerts>
    <alerts>
        <fullName>Approval_Notification</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Notify_Mgr_When_Sample_Refurb_Sale_Sample_Sale_Case_Booked</template>
    </alerts>
    <alerts>
        <fullName>COMM_Awaiting_FST_Schedule</fullName>
        <description>COMM Awaiting FST Schedule</description>
        <protected>false</protected>
        <recipients>
            <recipient>kyle.m.ferguson@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Unused_Templates/Manager_Notification</template>
    </alerts>
    <alerts>
        <fullName>COMM_Awaiting_FST_Schedule_C_Region</fullName>
        <description>COMM Awaiting FST Schedule- C Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>brian.mann@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Unused_Templates/Manager_Notification</template>
    </alerts>
    <alerts>
        <fullName>COMM_Awaiting_FST_Schedule_NE_Region</fullName>
        <description>COMM Awaiting FST Schedule- NE Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>rick.brummundt@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Unused_Templates/Manager_Notification</template>
    </alerts>
    <alerts>
        <fullName>COMM_Awaiting_FST_Schedule_SE_Region</fullName>
        <description>COMM Awaiting FST Schedule-SE Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>thomas.gehring@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/Changed_Stage</template>
    </alerts>
    <alerts>
        <fullName>COMM_Awaiting_FST_Schedule_W_Region</fullName>
        <description>COMM Awaiting FST Schedule- W Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>russell.kashow@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/Changed_Stage</template>
    </alerts>
    <alerts>
        <fullName>COMM_Beacon_Ortho</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>COMM Beacon Ortho</description>
        <protected>false</protected>
        <recipients>
            <recipient>mehul.jain@appirio.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Case_Assignment_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>COMM_Case_Returned_To_Escalation</fullName>
        <description>COMM Case Returned To Escalation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Unused_Templates/Returned_to_Escalation</template>
    </alerts>
    <alerts>
        <fullName>COMM_Change_Request_Case_Creation_Email_Alert</fullName>
        <description>COMM Change Request Case Creation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Templates/COMM_Change_Request_Case_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>COMM_Complaint_Notification_assigned_to_Complaint_Handler</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>COMM Complaint Notification assigned to Complaint Handler</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Complaint_Owner_E_Mail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Complaint_Notifications/Complaint_is_assigned</template>
    </alerts>
    <alerts>
        <fullName>COMM_Complaint_Notification_for_Presenter_once_created</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>Complaint Notification for Presenter once created</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Complaint_Owner_E_Mail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Complaint_Notifications/Complaint_assignment_CS</template>
    </alerts>
    <alerts>
        <fullName>COMM_Complaint_closed_Notification</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>COMM Complaint closed Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Complaint_Owner_E_Mail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Complaint_Notifications/Complaint_closed</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_Notify_Owner_when_More_Information_Is_Required</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>COMM Email Alert - Notify Owner when more information is required</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Additional_To__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/System_Support_Ticket_More_Info_Required_Auto_Response_To_Owner</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_Notify_Owner_when_System_Support_Ticket_has_been_received</fullName>
        <description>COMM Email Alert - Notify Owner when System Support Ticket has been received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Additional_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/System_Support_Ticket_Received_Auto_Response_to_Owner</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_Notify_Owner_when_Ticket_has_been_Closed</fullName>
        <description>COMM Email Alert - Notify Owner when Ticket has been Closed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Additional_To__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/System_Support_Ticket_Resolved_Auto_Response_To_Owner</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_Notify_Person_Assigned_To_System_Support_Ticket</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>COMM Email Alert - Notify person System Support Ticket has been Assigned To</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_To__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/System_Support_Ticket_Auto_Response_To_Assigned_To</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_to_Finance_Team_On_Case_Creation</fullName>
        <description>COMM: Email Alert to Finance Team On Case Creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Finance</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Notify_Finance_on_New_Address_Request</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Notify_Administrator_when_a_new_System_Support_Ticket_has_been_create</fullName>
        <description>COMM Email Alert - Notify Administrator when a new System Support Ticket has been created</description>
        <protected>false</protected>
        <recipients>
            <recipient>debra.meara@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>joseph.goings@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.smith@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thom.uzzle@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Additional_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/System_Support_Ticket_Auto_Response_Administrator</template>
    </alerts>
    <alerts>
        <fullName>Change_Request_Record_CSR_Rejection</fullName>
        <description>Change Request Record CSR Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/ChangeRequest_Record_CSR_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Change_Request_Record_Rejected</fullName>
        <description>Change Request Record Rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>aaron.aran@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/ChangeRequest_Record_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Customer_Support_Case_Closure_email_notification</fullName>
        <description>Customer Support Case Closure email notification</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Endo_Customer_Support/Customer_Support_Case_Closure_email_notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Supervisor_if_Inquiry_Case_in_Awaiting_Status_more_than_2_hours</fullName>
        <description>Email Supervisor if Inquiry Case in Awaiting Status more than 2 hours</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Customer_Support_Inquiry_Escalation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Supervisor_if_Inquiry_Case_in_Awaiting_Status_more_than_2_hours2</fullName>
        <description>Email Supervisor if Inquiry Case in Awaiting Status more than 2 hours</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_ProCare_Inquiry_Escalation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Template_For_COMM_Record_Types</fullName>
        <description>Email Template For COMM Record Types</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Case_Assignment_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Template_For_NON_COMM_Record_Types</fullName>
        <description>Email Template For NON-COMM Record Types</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Support_Case_Assignment_to_Support_Rep</template>
    </alerts>
    <alerts>
        <fullName>Email_To_Sales_Rep_for_Sales_Order_Cases</fullName>
        <description>Email To Sales Rep for Sales Order Cases</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Email_To_Sales_Rep_for_Sales_Order_Cases</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_Matt_Heyl</fullName>
        <description>Endo Notify when Closed Tech Support Case Marked with Feedback Flag</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.curtis@stryker.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_alert_for_Matt_Heyl</template>
    </alerts>
    <alerts>
        <fullName>Endo_Notify_Mgr_when_Sample_Refurb_Sale_Sample_Sale_Case_Booked</fullName>
        <description>Endo Notify Mgr when Sample Refurb Sale / Sample Sale Case Booked</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Notify_Mgr_When_Sample_Refurb_Sale_Sample_Sale_Case_Booked</template>
    </alerts>
    <alerts>
        <fullName>Endo_Notify_Owner_when_Sample_Refurb_Sale_Sample_Sale_Case_Created</fullName>
        <description>Endo Notify Owner when Sample Refurb Sale / Sample Sale Case Created.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Notify_Owner_when_Sample_Refurb_Sale_Sample_Sale_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Endo_Notify_Sales_Order_Case_Owner_if_Critical_Cases_in_Awaiting_Status_1hr</fullName>
        <description>Endo Notify Sales Order Case Owner if Critical Cases in Awaiting Status &gt; 1hr</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Critical_Sales_Order_Notification</template>
    </alerts>
    <alerts>
        <fullName>Endo_Notify_when_Sample_Refurb_Sale_Sample_Sale_Case_Booked</fullName>
        <description>Endo Notify when Sample Refurb Sale / Sample Sale Case Booked</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Notify_when_Sample_Refurb_Sale_Sample_Sale_Case_Booked</template>
    </alerts>
    <alerts>
        <fullName>Endo_Sample_Request_Closed_Notification_Email_Alert</fullName>
        <description>Endo Sample Request Closed Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Sample_Request_Closed_Notification</template>
    </alerts>
    <alerts>
        <fullName>Medical_Call_Center_Notify_Owner_when_Case_is_escalated</fullName>
        <description>Medical Call Center Notify Owner when Case is escalated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Support_Case_Assignment_to_Support_Rep</template>
    </alerts>
    <alerts>
        <fullName>Notify_customer_when_they_send_an_email_who_is_unavailable</fullName>
        <description>Notify customer when they send an email who is unavailable</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Auto_Response_if_Customer_Responds_While_Agent_Unavailable</template>
    </alerts>
    <alerts>
        <fullName>PM_Notification_on_Case_Close</fullName>
        <description>PM Notification on Case Close</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Notify_PM_On_Case_Close</template>
    </alerts>
    <alerts>
        <fullName>Rejection_of_of_repair_discount</fullName>
        <description>Rejection of of repair discount</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Repair_Discount_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Repair_Order_Approval_Request</fullName>
        <ccEmails>service@berchtold.biz</ccEmails>
        <description>Repair Order Approval Request</description>
        <protected>false</protected>
        <senderAddress>endocustomersupport@stryker.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Stryker_Endo/Endo_Notify_Mgr_When_Sample_Refurb_Sale_Sample_Sale_Case_Booked</template>
    </alerts>
    <alerts>
        <fullName>Update_Instruments_Employee_Complaint_Queue_Manager_On_Create</fullName>
        <description>Update Instruments Employee Complaint Queue Manager On Create</description>
        <protected>false</protected>
        <recipients>
            <recipient>tricia.henry@stryker.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Case_In_Instrument_Employee_Complaint_Created</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status</fullName>
        <description>Case status changed once approval is done by the Manager</description>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status1</fullName>
        <description>Status of case once it is approved</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Complaint_Owner_E_Mail</fullName>
        <field>Complaint_Owner_E_Mail__c</field>
        <formula>CASE( Text(Complaint_Owner__c) , 
&quot;Alexander Machill&quot;, &quot;Alexander.Machill@BERCHTOLD.biz&quot;, 
&quot;Franz Brunner&quot;, &quot;Franz.Brunner@BERCHTOLD.biz&quot;, 
&quot;Ismail Geldioglu&quot;, &quot;Ismail.Geldioglu@BERCHTOLD.biz&quot;, 
&quot;Jochen Weisser&quot;, &quot;Jochen.Weisser@BERCHTOLD.biz&quot;, 
&quot;Jens Wittkowski&quot;, &quot;Jens.Wittkowski@BERCHTOLD.biz&quot;, 
&quot;Volker Hornscheidt&quot;, &quot;Volker.Hornscheidt@BERCHTOLD.biz&quot;, &quot;&quot;)</formula>
        <name>COMM Complaint Owner E-Mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_QA_Field_update</fullName>
        <field>QA_Review_Required__c</field>
        <literalValue>Yes</literalValue>
        <name>COMM QA Field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_QA_closed</fullName>
        <field>Closed_by_QA__c</field>
        <literalValue>1</literalValue>
        <name>COMM QA closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Return_to_Escalation</fullName>
        <field>OwnerId</field>
        <lookupValue>DispatchEscalationCaseQueue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>COMM Return to Escalation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status</fullName>
        <description>Case Status changed during the time when it is waiting for approval</description>
        <field>Status</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Priority_on_Email_to_Case</fullName>
        <description>Set the Default Priority to &quot;Medium&quot; for all Email-to-Case Cases (&quot;High&quot; Priority is being used on the Sales Support Sales Order Email-to-Case setup  to differentiate it from the Customer Support Sales Order in the Assignment Rules) - this resets it</description>
        <field>Priority</field>
        <literalValue>Medium</literalValue>
        <name>Default Priority on Email-to-Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Discount_Approved</fullName>
        <description>Once the request has been approved.</description>
        <field>Status</field>
        <literalValue>Discount Approved</literalValue>
        <name>Discount Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Discount_Approved1</fullName>
        <description>Status of the case for which discount has been approved</description>
        <field>Status</field>
        <literalValue>Discount Approved</literalValue>
        <name>Discount Approved1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Discount_Rejected</fullName>
        <description>If discount is rejected</description>
        <field>Status</field>
        <literalValue>Discount Rejected</literalValue>
        <name>Discount Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EndoTechSupport_CloseNotes</fullName>
        <description>EndoTechSupport_CloseNotes</description>
        <field>Close_Notes__c</field>
        <formula>&quot;Closed the case after being marked as Request for Delete&quot;</formula>
        <name>EndoTechSupport_CloseNotes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EndoTechSupport_Status</fullName>
        <description>EndoTechSupport_Status</description>
        <field>Status</field>
        <literalValue>Closed [Solution Found]</literalValue>
        <name>EndoTechSupport_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Escalate_Urgent_Tech_Support_Case</fullName>
        <description>Endo Escalate Urgent Tech Support Case</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Endo Escalate Urgent Tech Support Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Populate_Case_Subject_For_Inquiries</fullName>
        <description>Endo Populate Case Subject For Inquiry &amp; Requests Case Type</description>
        <field>Subject</field>
        <formula>IF (NOT (ISBLANK (Oracle_Order_Ref__c)), TEXT(Sub_Type__c) &amp; &quot; Case for Order # &quot; &amp; Oracle_Order_Ref__c, 

TEXT (Sub_Type__c) &amp; &quot; Case for &quot; &amp; Contact.FirstName &amp; &quot; &quot; &amp; Contact.LastName )</formula>
        <name>Endo Populate Case Subject For Inquiries</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Populate_Case_Subject_For_ProCare</fullName>
        <description>Endo Populate Case Subject For ProCare Inquiry</description>
        <field>Subject</field>
        <formula>TEXT(Reason) &amp; &quot; Case from &quot; &amp; Contact.FirstName &amp; &quot; &quot; &amp; Contact.LastName &amp;&quot; &quot;&amp; Text(Type)</formula>
        <name>Endo Populate Case Subject For ProCare</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Populate_Subject_RMA_Order_Entry</fullName>
        <description>Endo Populate Subject RMA Order Entry</description>
        <field>Subject</field>
        <formula>&quot;Order # &quot; &amp; Oracle_Order_Ref__c &amp; &quot;, RMA # &quot; &amp; RMA__c &amp; &quot;: RMA Order Entry (&quot; &amp; TEXT( Reason ) &amp; &quot;)&quot;</formula>
        <name>Endo Populate Subject RMA Order Entry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Populate_Subject_Tech_Support</fullName>
        <description>Auto-population of the Subject text based on values from the Support Case</description>
        <field>Subject</field>
        <formula>CaseNumber &amp; &quot; &quot; &amp; TEXT(Symptom_Type__c) &amp; &quot; &quot; &amp; TEXT(Symptom__c)</formula>
        <name>Endo Populate Subject Tech Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Repair_Submission_Status</fullName>
        <description>Status of the Repair Billing Case changed to &quot;Awaiting Internal Approval&quot; when it is submitted for approval</description>
        <field>Status</field>
        <literalValue>Awaiting Internal Approval</literalValue>
        <name>Endo Repair Submission Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Update_Email_to_Case_Status_to_New</fullName>
        <description>On Create, ensure all Email-to-Case Cases start in &quot;New&quot; Status</description>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Endo Update Email-to-Case Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Update_Regional_Manager_Email</fullName>
        <description>Endo Update Regional Manager Email</description>
        <field>Regional_Manager_Email__c</field>
        <formula>Contact.Regional_Mgr__r.Email</formula>
        <name>Endo Update Regional Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_Urgent_TS_Cases</fullName>
        <description>Set Escalate flag for Urgent Tech Support Cases</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Escalate Urgent TS Cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_priority_set</fullName>
        <field>Priority</field>
        <literalValue>Low</literalValue>
        <name>Initial priority set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_Sales_Order_to_Sales_Support_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Move Sales Order to Sales Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sample_Audit_Auto_Close</fullName>
        <description>Sample Audit Auto Close of case in case of specific types</description>
        <field>Status</field>
        <literalValue>Closed [Other]</literalValue>
        <name>Sample Audit Auto Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitter_Notes</fullName>
        <description>Reason for discount notes</description>
        <field>Close_Notes__c</field>
        <name>Submitter Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Supervisor_Notes</fullName>
        <field>Close_Notes__c</field>
        <name>Supervisor Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Order_Subject</fullName>
        <field>Subject</field>
        <formula>&quot;Order # &quot; &amp; Oracle_Order_Ref__c &amp; &quot; - Sales Order, &quot; &amp; TEXT( Sub_Type__c )</formula>
        <name>Update Sales Order Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Auto Response if Customer Responds While Agent is Unavailable</fullName>
        <actions>
            <name>Notify_customer_when_they_send_an_email_who_is_unavailable</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Operations,Customer Inquiry &amp; Requests,RMA Order,Repair Order Billing,Sales Order,Sample Refurb Sale / Sample Sale,Samples Request,Tech Support,Samples Audit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Owner_Status__c</field>
            <operation>equals</operation>
            <value>After Call Work,Lunch,Front Desk,Tech Support Training,Training Assign Team Members,Meetings,Misc. Business Projects,Personal,Logged Off</value>
        </criteriaItems>
        <description>Alert a customer when they send an email to an agent who is not available</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Awaiting FST Schedule- SE Region</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>SE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending FST Schedule</value>
        </criteriaItems>
        <description>Awaiting FST Schedule</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_Awaiting_FST_Schedule_SE_Region</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM Awaiting FST Schedule- W Region</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>W</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending FST Schedule</value>
        </criteriaItems>
        <description>Awaiting FST Schedule</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_Awaiting_FST_Schedule_W_Region</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM Awaiting FST Schedule-C - GL Region</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>GL,C</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending FST Schedule</value>
        </criteriaItems>
        <description>Awaiting FST Schedule</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_Awaiting_FST_Schedule_C_Region</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM Awaiting FST Schedule-NE Region</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>NE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending FST Schedule</value>
        </criteriaItems>
        <description>Awaiting FST Schedule</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_Awaiting_FST_Schedule_NE_Region</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM Awaiting FST Schedule-SW - SC Region</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>SC,SW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending FST Schedule</value>
        </criteriaItems>
        <description>Awaiting FST Schedule</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_Awaiting_FST_Schedule</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM Beacon Ortho</fullName>
        <actions>
            <name>COMM_Beacon_Ortho</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SAP_Cust_Number__c</field>
            <operation>equals</operation>
            <value>6000000368</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <description>Notification</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM Change Request Case Creation Notification</fullName>
        <actions>
            <name>COMM_Change_Request_Case_Creation_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Change Request</value>
        </criteriaItems>
        <description>COMM I-232634</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM Complaint --%3E If QM-Status Closed --%3E Closed by QA %3D True %2F%2F Notification %40 User</fullName>
        <actions>
            <name>COMM_Complaint_closed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Complaint Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.QM_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Awaiting FST Schedule</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Complaint Close US</fullName>
        <actions>
            <name>COMM_QA_closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Tech.Support/Field Services Case,COMM US Tech.Support/Prev Maint Master,COMM US Installation Master Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.QM_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Complaint Notification that Complaint is assigned</fullName>
        <actions>
            <name>COMM_Complaint_Notification_assigned_to_Complaint_Handler</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Complaint Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Complaint_Owner__c</field>
            <operation>equals</operation>
            <value>Alexander Machill,Franz Brunner,Ismail Geldioglu,Jochen Weisser,Jens Wittkowski,Volker Hornscheidt</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Complaint Owner E-Mail</fullName>
        <actions>
            <name>COMM_Complaint_Owner_E_Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Complaint Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Complaint_Owner__c</field>
            <operation>equals</operation>
            <value>Alexander Machill,Franz Brunner,Ismail Geldioglu,Jochen Weisser,Jens Wittkowski,Volker Hornscheidt</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Dispatch Escalation Case Queue</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>C,GL,NE,SC,SE,SW,W</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_Case_Returned_To_Escalation</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>COMM_Return_to_Escalation</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM GreatLakes Tech Serv Case Closed</fullName>
        <actions>
            <name>COMM_GreatLakes_Tech_Service_Case_Closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>C</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Installation,Tech Serv Engineering-HOME OFFICE ONLY,Trial,Administrative,Holiday,Jury Duty,Pager,Paid Time Off,Sick,Telephone support,Vacation,Renovation,Relocation,Customer Complaint,Training,Bereavement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Tech.Support/Field Service Case 2012,COMM US Stryker Field Service Case</value>
        </criteriaItems>
        <description>Tech Serv Case Closed, Manager to review case before forwarding to customer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Install Case Close</fullName>
        <actions>
            <name>COMM_InstallationCaseClosedUpdateWarranrtyInfoonAssets</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.INs__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Installation Master Case</value>
        </criteriaItems>
        <description>This workflow triggers a reminder for case owner to update warranty info when an Installation Case closes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Installation Complete</fullName>
        <actions>
            <name>COMM_Installation_Complete</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Installation_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM New Complaint created</fullName>
        <actions>
            <name>COMM_Complaint_Notification_for_Presenter_once_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Complaint Form</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM NorthEast Tech Serv Case Closed</fullName>
        <actions>
            <name>COMM_NE_Tech_Serv_Case_Closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>NE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Installation,Tech Serv Engineering-HOME OFFICE ONLY,Trial,Administrative,Holiday,Jury Duty,Pager,Paid Time Off,Sick,Telephone support,Vacation,Renovation,Relocation,Customer Complaint,Training,Bereavement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Tech.Support/Field Service Case 2012,COMM US Stryker Field Service Case</value>
        </criteriaItems>
        <description>Tech Serv Case Closed, Manager to review case before forwarding to customer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Notify Administrator when a new System Support Ticket created</fullName>
        <actions>
            <name>COMM_Email_Notify_Administrator_when_a_new_System_Support_Ticket_has_been_create</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium,Critical,High,Urgent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Commercial Operations Support</value>
        </criteriaItems>
        <description>Notify Administrator when a new system support ticket has been created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM Notify Owner when More Information is Required</fullName>
        <actions>
            <name>COMM_Email_Alert_Notify_Owner_when_More_Information_Is_Required</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>More Information Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Commercial Operations Support</value>
        </criteriaItems>
        <description>Notification to Owner when More Information is Required</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM Notify Person Assigned to System Support ticket</fullName>
        <actions>
            <name>COMM_Email_Alert_Notify_Person_Assigned_To_System_Support_Ticket</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed,More Information Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_to__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Commercial Operations Support</value>
        </criteriaItems>
        <description>Notify person assigned to resolving the System Support Ticket</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Notify System Support Owner when Ticket Closed</fullName>
        <actions>
            <name>COMM_Email_Alert_Notify_Owner_when_Ticket_has_been_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Commercial Operations Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Problem_Category__c</field>
            <operation>notEqual</operation>
            <value>New Address Request</value>
        </criteriaItems>
        <description>Notify System Support Owner when Ticket has been closed
COMM: I-242641 updated status condition</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM Notify System Support Owner when Ticket Received</fullName>
        <actions>
            <name>COMM_Email_Alert_Notify_Owner_when_System_Support_Ticket_has_been_received</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open,Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Commercial Operations Support</value>
        </criteriaItems>
        <description>Notify System Support Owner when Ticket Received</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM Part to be entered</fullName>
        <actions>
            <name>COMM_PartsUsed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Parts_Used__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <description>Notification if technician has used a part.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Part to be entered2</fullName>
        <actions>
            <name>COMM_Parts_Used</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Parts_Used__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Tech.Support/Field Services Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <description>Notification if technician has used a part.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM QA Investigation Closed</fullName>
        <actions>
            <name>COMM_QA_Investigation_Closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Investigation_Closed_Date__c</field>
            <operation>greaterThan</operation>
            <value>1/1/2013</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Set_QAInvestigationNeeded</fullName>
        <actions>
            <name>COMM_QA_Field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Out_of_Box_Issue__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <description>The field &quot;QA Investigation Needed?&quot; is set automatically, if the field &quot;Out of Box Issue&quot; is checked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM SouthCentral Tech Serv Case Closed</fullName>
        <actions>
            <name>COMM_SouthCentral_Tech_Service_Case_Closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>SW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Installation,Pre installation Activity,Trial,Administrative,Bereavement,Holiday,Jury Duty,Pager,Paid Time Off,Sick,Telephone support,Training,Vacation,Renovation,Relocation,Customer Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <description>Tech Serv Case Closed, Manager to review case before forwarding to customer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM SouthEast Tech Serv Case Closed</fullName>
        <actions>
            <name>COMM_SE_Tech_Service_Case_Closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>SE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Installation,Pre installation Activity,Trial,Administrative,Bereavement,Holiday,Jury Duty,Pager,Paid Time Off,Sick,Telephone support,Training,Vacation,Renovation,Relocation,Customer Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <description>Tech Serv Case Closed, Manager to review case before forwarding to customer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM West Project Manager</fullName>
        <actions>
            <name>COMM_West_Proj_Manager_Movement</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.LastModifiedById</field>
            <operation>equals</operation>
            <value>Joel Snodgrass</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>COMM</value>
        </criteriaItems>
        <description>Tracks Movement of Project Manager Changes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM West Tech Serv Case Closed</fullName>
        <actions>
            <name>COMM_West_Tech_Service_Case_Closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM US Stryker Field Service Case,COMM US Tech.Support/Field Service Case 2012</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Installation,Pre installation Activity,Trial,Administrative,Holiday,Jury Duty,Pager,Paid Time Off,Sick,Telephone support,Training,Vacation,Renovation,Relocation,Customer Complaint,Bereavement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Region_Comm__c</field>
            <operation>equals</operation>
            <value>W</value>
        </criteriaItems>
        <description>Tech Serv Case Closed, Manager to review case before forwarding to customer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A Notify PM on Case Close</fullName>
        <actions>
            <name>PM_Notification_on_Case_Close</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Problem_Category__c</field>
            <operation>equals</operation>
            <value>New Address Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>COMM: T-510491</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case_Default_Non_COMM_Assignment_Notification</fullName>
        <actions>
            <name>Email_Template_For_NON_COMM_Record_Types</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>RMA Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sample Refurb Sale / Sample Sale</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tech Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Samples Audit</value>
        </criteriaItems>
        <description>created for Non-COMM to manage 2 different email notification for case assignments</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email To Sales Rep for Sales Order Cases</fullName>
        <actions>
            <name>Email_To_Sales_Rep_for_Sales_Order_Cases</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email notification to Sales Rep (Contact) when Email-to-Case Cases go from the &quot;Sales Support Queue&quot; to an individual User/Owner</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), ISPICKVAL(Origin, &apos;Email-to-Case&apos;) &amp;&amp; RecordType.Name = &apos;Sales Order&apos; &amp;&amp;  Old_Owner_Name__c = &apos;Sales Support&apos; &amp;&amp;  LEFT(OwnerId, 3) = &apos;005&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email alert for Matt Heyl</fullName>
        <actions>
            <name>Email_alert_for_Matt_Heyl</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tech Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Feedback_Flag__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email alert for Matt Heyl whenever Feeback Flag = Quality</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Endo Customer Support Case Closure email notification</fullName>
        <actions>
            <name>Customer_Support_Case_Closure_email_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Inquiry &amp; Requests,Repair Order Billing,Sales Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>Customer Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>contains</operation>
            <value>Endo Customer Contact,Endo Employee,Endo Internal Contact</value>
        </criteriaItems>
        <description>Customer Support team receive an email to the Web Email/Name when the Customer Support team member closes a support case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Endo Email-to-Case %26 Outlook Email - Set to New on create</fullName>
        <actions>
            <name>Endo_Update_Email_to_Case_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email-to-Case,Email</value>
        </criteriaItems>
        <description>Set Endo Email-to-Case &amp; Outlook Cases to &quot;New&quot; Status when created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Endo Email-to-Case - Reset Sales Support Order Priority</fullName>
        <actions>
            <name>Default_Priority_on_Email_to_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email-to-Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <description>Set Endo Email-to-Case Cases coming to Sales Support back to &quot;Medium&quot; Priority</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Endo Escalate Urgent Tech Support Case</fullName>
        <actions>
            <name>Endo_Escalate_Urgent_Tech_Support_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tech Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Urgent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Endo Escalate Urgent Tech Support Case</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Endo Notify Sales Order Owner when Critical and Awaiting Status over 1 hr</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Order</value>
        </criteriaItems>
        <description>Send the Owner of a Sales Order Case an email if a Critical Case has been in Awaiting Approval Status for longer than one hour.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Endo_Notify_Sales_Order_Case_Owner_if_Critical_Cases_in_Awaiting_Status_1hr</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Endo Notify Supervisor when Inquiry Case in Awaiting Status over 2 hours</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>Awaiting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Inquiry &amp; Requests</value>
        </criteriaItems>
        <description>Send the Manager (Supervisor) of the Owner of an Inquiry &amp; Requests Case an email if a Case has been in any Awaiting Status for longer than two hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Supervisor_if_Inquiry_Case_in_Awaiting_Status_more_than_2_hours2</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Endo Notify when Closed Tech Support Case Marked with Feedback Flag</fullName>
        <actions>
            <name>Email_alert_for_Matt_Heyl</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email notification whenever Feeback Flag = Tech support and a case is closed.</description>
        <formula>AND(PRIORVALUE(IsClosed)= FALSE,  ISCHANGED(Status), INCLUDES(Feedback_Flag__c, &apos;Tech Support&apos;), CONTAINS(TEXT(Status), &apos;Closed&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Endo Notify when Sample Refurb Sale %2F Sample Sale Case Booked</fullName>
        <actions>
            <name>Endo_Notify_Mgr_when_Sample_Refurb_Sale_Sample_Sale_Case_Booked</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Endo_Notify_when_Sample_Refurb_Sale_Sample_Sale_Case_Booked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sample Refurb Sale / Sample Sale</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Booked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Endo Notify when Sample Refurb Sale / Sample Sale Case Booked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Endo Sample Request Closed Notification</fullName>
        <actions>
            <name>Endo_Sample_Request_Closed_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Samples Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>contains</operation>
            <value>Endo Customer Contact,Endo Employee,Endo Internal Contact</value>
        </criteriaItems>
        <description>Endo Sample Request Closed Notification</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Endo Samples Audit Auto Close</fullName>
        <actions>
            <name>Sample_Audit_Auto_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Samples Audit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>With Self,At an Account,Loaned to another Rep</value>
        </criteriaItems>
        <description>Workflow rule to auto close samples audit cases with certain type selections</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Endo Update Regional Manager Email</fullName>
        <actions>
            <name>Endo_Update_Regional_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CaseNumber</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Endo Update Regional Manager Email</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EndoTechSupportReqForDelete</fullName>
        <actions>
            <name>EndoTechSupport_CloseNotes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EndoTechSupport_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Request_for_Delete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>EndoTechSupportReqForDelete</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Finance group on case creation</fullName>
        <actions>
            <name>COMM_Email_Alert_to_Finance_Team_On_Case_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.New_Address_Requested__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>New Address Request</value>
        </criteriaItems>
        <description>COMM: T-510491</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Medical Call Center Notify Owner when Case is escalated</fullName>
        <actions>
            <name>Medical_Call_Center_Notify_Owner_when_Case_is_escalated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Medical Call Center Notify Owner when Case is escalated</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND ( Medical_Send_Email_Escalate_Case__c , OR(RecordType.Name == &apos;Medical - Non Product Issue&apos;,RecordType.Name == &apos;Instruments - Non Product Issue&apos;, RecordType.Name == &apos;Instruments - Potential Product Complaint&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Priority set for Instruments ProCare Approvals Queue</fullName>
        <actions>
            <name>Initial_priority_set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Instruments ProCare Approvals Queue</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Instruments Employee Complaint Queue Manager</fullName>
        <actions>
            <name>Update_Instruments_Employee_Complaint_Queue_Manager_On_Create</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Support_Case_Owner__c</field>
            <operation>contains</operation>
            <value>Instruments Employee Complaint Queue</value>
        </criteriaItems>
        <description>C-00179073</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>COMM_GreatLakes_Tech_Service_Case_Closed</fullName>
        <assignedTo>brian.mann@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Serv Tech Closed Case, review before sending customer copy.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>GreatLakes Tech Service Case Closed</subject>
    </tasks>
    <tasks>
        <fullName>COMM_InstallationCaseClosedUpdateWarranrtyInfoonAssets</fullName>
        <assignedToType>owner</assignedToType>
        <description>An Installation Case of yours has closed, please update the warranty information on related asset records.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Installation Case Closed: Update Warranrty Info on Assets</subject>
    </tasks>
    <tasks>
        <fullName>COMM_Installation_Complete</fullName>
        <assignedTo>tammy.smith@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Installation Complete</subject>
    </tasks>
    <tasks>
        <fullName>COMM_NE_Tech_Serv_Case_Closed</fullName>
        <assignedTo>rick.brummundt@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Serv Tech Closed Case, review before sending customer copy.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>NE Tech Serv Case Closed</subject>
    </tasks>
    <tasks>
        <fullName>COMM_PartsUsed</fullName>
        <assignedTo>tammy.smith@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.ClosedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Parts Used</subject>
    </tasks>
    <tasks>
        <fullName>COMM_Parts_Used</fullName>
        <assignedTo>anabelle.galvan@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.ClosedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Parts Used</subject>
    </tasks>
    <tasks>
        <fullName>COMM_QA_Investigation_Closed</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Investigation_Closed_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>QA Investigation Closed</subject>
    </tasks>
    <tasks>
        <fullName>COMM_SE_Tech_Service_Case_Closed</fullName>
        <assignedTo>thomas.gehring@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Serv Tech Closed Case, review before sending customer copy.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>COMM SE Tech Service Case Closed</subject>
    </tasks>
    <tasks>
        <fullName>COMM_SouthCentral_Tech_Service_Case_Closed</fullName>
        <assignedTo>kyle.m.ferguson@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Serv Tech Closed Case, review before sending customer copy.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>SouthCentral Tech Service Case Closed</subject>
    </tasks>
    <tasks>
        <fullName>COMM_West_Proj_Manager_Movement</fullName>
        <assignedTo>russell.kashow@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>West Proj Manager Movement</subject>
    </tasks>
    <tasks>
        <fullName>COMM_West_Tech_Service_Case_Closed</fullName>
        <assignedTo>russell.kashow@stryker.com.sykprod</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Serv Tech Closed Case, review before sending customer copy.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>West Tech Service Case Closed</subject>
    </tasks>
</Workflow>
