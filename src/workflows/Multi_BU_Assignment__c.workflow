<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_MBU_Unique_Identifier</fullName>
        <description>Populate MBU Unique Identifier Field</description>
        <field>Unique_Identifier__c</field>
        <formula>Rep__r.Id + &apos;-&apos; +  Text(Business_Unit__c)</formula>
        <name>Populate MBU Unique Identifier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate_MBU_Unique_Identifier_Field</fullName>
        <actions>
            <name>Populate_MBU_Unique_Identifier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Multi_BU_Assignment__c.Rep__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Multi_BU_Assignment__c.Business_Unit__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populate MBU Unique Identifier Field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
