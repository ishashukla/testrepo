<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ICCKM_Product_Troubleshooting_Article_Review_Reminder</fullName>
        <description>ICCKM Product Troubleshooting Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Instrument_CCKM_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/Product_Troubleshooting_Article_Review_Notification</template>
    </alerts>
    <alerts>
        <fullName>Product_Troubleshooting_Article_Review_Reminder</fullName>
        <description>Product Troubleshooting Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Medical_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/Product_Troubleshooting_Article_Review_Notification</template>
    </alerts>
    <alerts>
        <fullName>Product_Troubleshooting_Rejection_Notification</fullName>
        <description>Product Troubleshooting Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Article_Rejection_Notification</template>
    </alerts>
    <knowledgePublishes>
        <fullName>Approve_Endo_Product_Troubleshoot_Article</fullName>
        <action>Publish</action>
        <description>Approve Endo Product Troubleshoot Article</description>
        <label>Approve Endo Product Troubleshoot Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Product_TS_Approve_Publish</fullName>
        <action>Publish</action>
        <label>Product TS: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>ICCKM Product Troubleshooting Article Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to ICCKM Knowledge Group users on Review Date in Product Troubleshooting Article</description>
        <formula>AND(BEGINS($UserRole.DeveloperName,&quot;Instruments&quot;),!ISNULL(Review_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ICCKM_Product_Troubleshooting_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Product_Troubleshooting__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Product Troubleshooting Article Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to Medical Knowledge Group users on Review Date in Product Troubleshooting Article</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(BEGINS($UserRole.DeveloperName,&quot;Medical&quot;),!ISNULL(Review_Date__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Product_Troubleshooting_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Product_Troubleshooting__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
