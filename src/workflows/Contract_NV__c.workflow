<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_NV_Region</fullName>
        <description>Populates the NV Sales Region</description>
        <field>NV_Sales_Region__c</field>
        <formula>Account__r.NV_Region__c</formula>
        <name>Populate NV Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_NV_Territory</fullName>
        <description>Populates NV Territory every time Contract is created or edited.</description>
        <field>NV_Sales_Territory__c</field>
        <formula>Account__r.NV_Territory__c</formula>
        <name>Populate NV Territory</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_PR_Account_Contract_for_Search</fullName>
        <description>Stryker NV: Assist with populating the Account name on the Contract object.</description>
        <field>PR_Account_Name__c</field>
        <formula>Program_Request__r.Account__r.Name</formula>
        <name>Populate PR Account Contract for Search</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate PR Account on Contract NV to assist Search</fullName>
        <actions>
            <name>Populate_PR_Account_Contract_for_Search</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract_NV__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>12/8/2013 - DEPRECATED.  No longer needed as direct relationship with Account from Contract NV.

Populates Account name on Contract record from Program Request on create and edit to assist with search.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Sales Territory%2C Region</fullName>
        <actions>
            <name>Populate_NV_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_NV_Territory</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract_NV__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
