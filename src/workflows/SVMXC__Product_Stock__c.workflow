<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Rejected</fullName>
        <description>Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Product_Stock_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Approval_Success</fullName>
        <description>Approval Success</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Product_Stock_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Pending_to_Approved</fullName>
        <field>Approval_for_New_Inventory__c</field>
        <literalValue>Approved</literalValue>
        <name>Pending to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_to_Rejected</fullName>
        <field>Approval_for_New_Inventory__c</field>
        <literalValue>Rejected</literalValue>
        <name>Pending to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_updated_to_Pending</fullName>
        <field>Approval_for_New_Inventory__c</field>
        <literalValue>Pending for Approval</literalValue>
        <name>Status updated to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
