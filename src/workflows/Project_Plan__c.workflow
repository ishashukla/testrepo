<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_CS_Team_Notification</fullName>
        <description>COMM: CS Team Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Plan_CloseTemplate</template>
    </alerts>
    <alerts>
        <fullName>COMM_Customer_Service_Notification_For_Non_Phased_Project_Complete</fullName>
        <description>COMM Customer Service Notification For Non Phased Project Complete</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Plan_Stage_Is_Completed</template>
    </alerts>
    <alerts>
        <fullName>COMM_Customer_Service_Notified_About_Project_Plan_Stage_Completion</fullName>
        <ccEmails>meghna.vijay@appirio.com</ccEmails>
        <description>COMM Customer Service Notified About Project Plan Stage Completion</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Plan_Stage_Is_Completed</template>
    </alerts>
    <alerts>
        <fullName>COMM_Customer_Service_Notified_For_Non_Phased_Project_Completion</fullName>
        <description>COMM Customer Service Notified For Non Phased Project Completion</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Plan_Stage_Is_Completed</template>
    </alerts>
    <alerts>
        <fullName>COMM_Customer_Sign_Off_Completion_Notification</fullName>
        <description>COMM Customer Sign Off Completion Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/COMM_Sign_Off_Completion</template>
    </alerts>
    <alerts>
        <fullName>COMM_Project_Plan_Close_Email_Notification</fullName>
        <description>COMM Project Plan Close Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Regional_Sales_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Plan_CloseTemplate</template>
    </alerts>
    <alerts>
        <fullName>COMM_Project_Plan_Owner_Notification</fullName>
        <description>COMM : Project Plan Owner Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_CloseTemplate</template>
    </alerts>
    <alerts>
        <fullName>COMM_Send_Notification_to_Project_Plan_Owner</fullName>
        <description>COMM Send Notification to Project Plan Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Plan_Creation_Template</template>
    </alerts>
    <alerts>
        <fullName>COMM_Send_Notification_to_Project_Plan_PM</fullName>
        <description>COMM Send Notification to Project Plan PM</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Plan_Reopen_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Approver</fullName>
        <description>Notify Approver</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/Project_Plan_Approval_Process_Init</template>
    </alerts>
    <alerts>
        <fullName>Notify_Project_Approver</fullName>
        <description>Notify Project Approver</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/Project_Plan_Approval_Process_Planning</template>
    </alerts>
    <alerts>
        <fullName>Project_Plan_Init_Approved_Alert</fullName>
        <description>Project Plan (Init) Approved Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/Project_Plan_Approved_Init</template>
    </alerts>
    <alerts>
        <fullName>Project_Plan_Init_Rejected_Alert</fullName>
        <description>Project Plan (Init) Rejected Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/Project_Plan_Rejected_Init</template>
    </alerts>
    <alerts>
        <fullName>Project_Plan_Planning_Approved_Alert</fullName>
        <description>Project Plan (Planning) Approved Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/Project_Plan_Approved_Planning</template>
    </alerts>
    <alerts>
        <fullName>Project_Plan_Planning_Rejected_Alert</fullName>
        <description>Project Plan (Planning) Rejected Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/Project_Plan_Rejected_Planning</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_All_Phase_Complete_Stage_Update</fullName>
        <description>COMM : T-497718</description>
        <field>Stage__c</field>
        <literalValue>Customer Sign Off Ready</literalValue>
        <name>COMM : All Phase Complete Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Update_PPlan_Sign_off_Complete</fullName>
        <description>Ref (I-240269) , this sets status of plan to sign of complete upon BU sign completion</description>
        <field>Stage__c</field>
        <literalValue>Customer Sign Off Complete</literalValue>
        <name>COMM Update PPlan Sign off Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Plan_Approved_Planning</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Project Plan Approved (Planning)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Plan_Rejected_Planning</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Project Plan Rejected (Planning)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Approval_Status</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Not Reviewed</literalValue>
        <name>Reset Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Approval_Status_Planning</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Reviewed</literalValue>
        <name>Reset Approval Status (Planning)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Submitted for Review</literalValue>
        <name>Update Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Approved</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Reviewed</literalValue>
        <name>Update Approval Status (Approved)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Planning</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Approval Status (Planning)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Rejected</fullName>
        <field>Engineering_Approval_Status__c</field>
        <literalValue>Review Rejected</literalValue>
        <name>Update Approval Status (Rejected)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Customer_Sign_Off_Complete</fullName>
        <description>COMM : T-497718</description>
        <field>Stage__c</field>
        <literalValue>Customer Sign Off Complete</literalValue>
        <name>Update Stage Customer Sign Off Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_To_Cstomr_Sign_Off_Ready</fullName>
        <description>COMM : T-497718</description>
        <field>Stage__c</field>
        <literalValue>Customer Sign Off Ready</literalValue>
        <name>Update Stage To Customer Sign Off Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_To_Customer_Sign_Off_Ready</fullName>
        <description>COMM : T-497718</description>
        <field>Stage__c</field>
        <literalValue>Customer Sign Off Ready</literalValue>
        <name>Update Stage To Customer Sign Off Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Planning</fullName>
        <description>COMM: I-239746</description>
        <field>Stage__c</field>
        <literalValue>Planning</literalValue>
        <name>Update Stage to Planning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM %3A Some Phase Complete and Rest Customer Sign Off Ready</fullName>
        <actions>
            <name>Update_Stage_To_Cstomr_Sign_Off_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>COMM : T-497718</description>
        <formula>IF( (Number_of_Completed_Phases__c &gt; 0)  &amp;&amp;   (Number_of_Phases__c =  Number_of_Completed_Phases__c +  Number_of_Signed_Off_Phases__c) &amp;&amp; Phased_Sign_Off__c = false , true, false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM %3A Update Stage</fullName>
        <actions>
            <name>COMM_All_Phase_Complete_Stage_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>IF(( Number_of_Completed_Phases__c &lt; 0) &amp;&amp; Phased_Sign_Off__c = false, true, false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Notification if Project Plan is Closed</fullName>
        <actions>
            <name>COMM_Project_Plan_Close_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>TEXT(Stage__c ) =  &apos;Closed&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Project Plan Docusign customer sign off ready</fullName>
        <actions>
            <name>COMM_Update_PPlan_Sign_off_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This work rule sets the Status of project plan to Customer Sign off complete (Ref : I-240269)</description>
        <formula>AND( Envelope_Signed_Status__c == &apos;Completed&apos;,  Signed_Document_Name__c ==  $Label.Project_Plan_BU_Document_Name )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Send Email Notification to Project Plan Owner</fullName>
        <actions>
            <name>COMM_Send_Notification_to_Project_Plan_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>T-492006- Used for COMM, To send email notification to Owner if Project plan created</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COMM Send Notification to Project Plan PM</fullName>
        <actions>
            <name>COMM_Send_Notification_to_Project_Plan_PM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>T-492006- Used for COMM, To send email notification to PM if Stage is changed</description>
        <formula>AND(ISCHANGED(Stage__c), ISPICKVAL(Stage__c, &apos;Initiation&apos;),  ISPICKVAL(PRIORVALUE(Stage__c), &apos;Cancelled&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A project closeout notification to CS</fullName>
        <actions>
            <name>COMM_CS_Team_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>COMM_Project_Plan_Owner_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Plan__c.Stage__c</field>
            <operation>equals</operation>
            <value>Pending Closure</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project_Plan__c.Phased_Sign_Off__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>COMM: I-230042; updated stage : I-244590</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
