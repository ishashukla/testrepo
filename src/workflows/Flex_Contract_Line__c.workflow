<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Flex_Equipment_Funded_Amount_Adjustmenc</fullName>
        <field>Last_Equipment_Funded_Amt__c</field>
        <formula>PRIORVALUE( Equipment_Funded_Amt__c )</formula>
        <name>Flex Equipment Funded Amount Adjustmenc)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Spread_Amount_Adjustment</fullName>
        <field>Last_Spread_Amount__c</field>
        <formula>Priorvalue(Spread_Amount__c)</formula>
        <name>Flex Spread Amount Adjustment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Amount</fullName>
        <field>Last_Amount__c</field>
        <formula>PRIORVALUE(Service_Sell_Price__c)</formula>
        <name>Update Last Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Legacy_Flex_RFM</fullName>
        <field>Legacy_Flex_RFM__c</field>
        <formula>Flex_Sales_Rep__r.FirstName &amp; &quot; &quot; &amp; Flex_Sales_Rep__r.LastName</formula>
        <name>Update Legacy Flex RFM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Legacy_PSR_Contact</fullName>
        <field>Legacy_PSR_Contact__c</field>
        <formula>PSR_Contact__r.FirstName  &amp; &quot; &quot;  &amp; PSR_Contact__r.LastName</formula>
        <name>Update Legacy PSR Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Flex Equipment Funded Amount Adjustment</fullName>
        <actions>
            <name>Flex_Equipment_Funded_Amount_Adjustmenc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), ISCHANGED(Equipment_Funded_Amt__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Spread Amount Adjustment</fullName>
        <actions>
            <name>Flex_Spread_Amount_Adjustment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), ISCHANGED( Spread_Amount__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Update Last Amount</fullName>
        <actions>
            <name>Update_Last_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Service_Sell_Price__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the Legacy Flex and PSR Value</fullName>
        <actions>
            <name>Update_Legacy_Flex_RFM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Legacy_PSR_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( AND(ISNEW(),  OR(NOT(ISBLANK(Flex_Sales_Rep__c)),NOT(ISBLANK(PSR_Contact__c))) ),  OR(ISCHANGED(Flex_Sales_Rep__c) ,ISCHANGED(PSR_Contact__c ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
