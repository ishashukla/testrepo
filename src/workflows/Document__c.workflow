<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_CAD_Drawing_Complete_Approval_Email_Alert</fullName>
        <description>COMM CAD Drawing Complete Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_CAD_Drawing_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>COMM_CAD_PE_Notify_Opportunity_Document_Record_Owner_On_Approval</fullName>
        <description>COMM: CAD PE - Notify Opportunity Document Record Owner On Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_CAD_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>COMM_CAD_PE_Notify_Opportunity_Document_Record_Owner_On_Rejection</fullName>
        <description>COMM: CAD PE - Notify Opportunity Document Record Owner On Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_CAD_Request_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_Approved_by_CAD</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved by CAD</literalValue>
        <name>COMM: Approved by CAD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Approved_by_Owner</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved by Owner</literalValue>
        <name>COMM: Approved by Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_CAD_Drawing_Complete</fullName>
        <description>COMM I-227190</description>
        <field>Approval_Status__c</field>
        <literalValue>CAD Drawing Complete</literalValue>
        <name>COMM: CAD Drawing Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_CAD_Drawing_in_Progress</fullName>
        <description>COMM I-227190</description>
        <field>Approval_Status__c</field>
        <literalValue>CAD Drawing in Progress</literalValue>
        <name>COMM: CAD Drawing in Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_CAD_PE_Approved_By_PE</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved by PE</literalValue>
        <name>COMM: CAD PE - Approved By PE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_CAD_PE_Rejected_By_PE</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected by PE</literalValue>
        <name>COMM: CAD PE - Rejected By PE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_CAD_PE_Submitted_To_PE</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted to PE</literalValue>
        <name>COMM: CAD PE - Submitted To PE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Rejected_by_CAD</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected by CAD</literalValue>
        <name>COMM: Rejected by CAD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Rejected_by_Owner</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected by Owner</literalValue>
        <name>COMM: Rejected by Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Submitted_For_CAD_Review</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted for CAD Review</literalValue>
        <name>COMM:Submitted For CAD Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_CAD_process</fullName>
        <description>COMM: I-246104</description>
        <field>Approval_Status__c</field>
        <literalValue>Rejected by Owner</literalValue>
        <name>Recall CAD process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
