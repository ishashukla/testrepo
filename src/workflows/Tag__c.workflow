<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Tag_Title</fullName>
        <description>Stryker NV:  updates Tag Title to support search requirements</description>
        <field>Tag_Title__c</field>
        <formula>Title__c</formula>
        <name>Update Tag Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Tag Title</fullName>
        <actions>
            <name>Update_Tag_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tag__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Stryker NV:  Used to populate Tag Title on custom object (Tag) to support ability to return tag during searches.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
