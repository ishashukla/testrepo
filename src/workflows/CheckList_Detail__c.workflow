<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Page_Layout_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>COMM_Text_Question</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Page Layout Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Page_layout</fullName>
        <field>RecordTypeId</field>
        <lookupValue>COMM_Picklist_Question</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Page layout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Record Type</fullName>
        <actions>
            <name>Update_Page_layout</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CheckList_Detail__c.Type__c</field>
            <operation>equals</operation>
            <value>Picklist</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Record Type On type update</fullName>
        <actions>
            <name>Page_Layout_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CheckList_Detail__c.Type__c</field>
            <operation>equals</operation>
            <value>Comment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
