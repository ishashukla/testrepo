<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Endo_Populate</fullName>
        <description>When record created, populate Order Notification Email field with value in Email field.</description>
        <field>Order_Notification_Email__c</field>
        <formula>Email</formula>
        <name>Endo Populate Order Notification Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Populate_Order_Notification_Fax</fullName>
        <description>Populate Order Notification Fax field with value specified in Fax field when Contact created.</description>
        <field>Order_Notification_Fax__c</field>
        <formula>Fax</formula>
        <name>Endo Populate Order Notification Fax</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Endo_Populate_Order_Notification_Phone</fullName>
        <description>Populate Order Notification Phone field with value from Phone field when record created.</description>
        <field>Order_Notification_Phone__c</field>
        <formula>Phone</formula>
        <name>Endo Populate Order Notification Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Endo Update Customer Contact</fullName>
        <actions>
            <name>Endo_Populate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Endo_Populate_Order_Notification_Fax</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Endo_Populate_Order_Notification_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Endo Customer Contact</value>
        </criteriaItems>
        <description>Update Order preference fields on Customer Contact record upon record creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
