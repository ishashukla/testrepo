<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Flex_Notify_Usage_Contract_Shipping_or_Active</fullName>
        <description>Flex Notify Usage Contract Shipping or Active</description>
        <protected>false</protected>
        <recipients>
            <recipient>Flex_Usage_Contract_Notif_Recipients</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Flex/Flex_Usage_Contract_Status_Shipping_Active</template>
    </alerts>
    <fieldUpdates>
        <fullName>Flex_Contract_Set_Service_By_BOTW</fullName>
        <field>Service_by_BOTW__c</field>
        <literalValue>1</literalValue>
        <name>Flex Contract Set Service By BOTW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Contract_Set_Service_By_BOTW_False</fullName>
        <field>Service_by_BOTW__c</field>
        <literalValue>0</literalValue>
        <name>Flex Contract Set Service By BOTW False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Contract_Reason_Terminated</fullName>
        <description>Flex Set Contract Reason Terminated</description>
        <field>Reason_Terminated__c</field>
        <literalValue>Purchased</literalValue>
        <name>Flex Set Contract Reason Terminated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Contract_Status_to_Terminated</fullName>
        <description>Flex Set Contract Status to Terminated</description>
        <field>Status__c</field>
        <literalValue>Terminated</literalValue>
        <name>Flex Set Contract Status to Terminated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM Flex Contract Lease Expiration Date</fullName>
        <active>true</active>
        <description>COMM I-215112</description>
        <formula>CONTAINS(Account__r.RecordType.DeveloperName,$Label.COMM)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Flex_Contract</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Flex_Contract__c.Scheduled_Maturity_Date__c</offsetFromField>
            <timeLength>-182</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Flex Contract Set Service By BOTW</fullName>
        <actions>
            <name>Flex_Contract_Set_Service_By_BOTW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Flex Contract Set Service By BOTW</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(   NOT(ISCHANGED(Service_by_BOTW__c)),   OR(ISCHANGED(Lender_Name__c), 	ISCHANGED(Deal_Type__c), 	ISNEW()   ),   OR(     ISPICKVAL(Lender_Name__c, &apos;DLL&apos;),  	ISPICKVAL(Lender_Name__c, &apos;TCF&apos;),  	ISPICKVAL(Lender_Name__c, &apos;USB&apos;),  	ISPICKVAL(Lender_Name__c, &apos;Internal Rental&apos;)   ),   OR(     ISPICKVAL(Deal_Type__c, &apos;Lease&apos;), 	ISPICKVAL(Deal_Type__c, &apos;ProCare Solution Agreement (PSA)&apos;), 	ISPICKVAL(Deal_Type__c, &apos;Fee Per Case (FPC)&apos;)   ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Contract Set Service By BOTW False</fullName>
        <actions>
            <name>Flex_Contract_Set_Service_By_BOTW_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(  NOT(ISCHANGED(Service_by_BOTW__c)), 	OR(ISCHANGED(Lender_Name__c), 	    ISCHANGED(Deal_Type__c), 	    ISNEW() 	),  NOT(AND(    OR(     ISPICKVAL(Lender_Name__c, &apos;DLL&apos;),  	ISPICKVAL(Lender_Name__c, &apos;TCF&apos;),  	ISPICKVAL(Lender_Name__c, &apos;USB&apos;),  	ISPICKVAL(Lender_Name__c, &apos;Internal Rental&apos;)   ),   OR(     ISPICKVAL(Deal_Type__c, &apos;Lease&apos;), 	ISPICKVAL(Deal_Type__c, &apos;ProCare Solution Agreement (PSA)&apos;), 	ISPICKVAL(Deal_Type__c, &apos;Fee Per Case (FPC)&apos;)   )  ))  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Notify Upcoming Renewal Opportunity</fullName>
        <active>false</active>
        <description>Notify Flex Financial Sales Rep RFM of upcoming renewal opportunity.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(   Scheduled_Maturity_Date__c &lt; Today() + 180,   IsActive__c = &apos;Active&apos; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Flex_Alert_On_Upcoming_Renewal_Opportunity</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Flex_Contract__c.Scheduled_Maturity_Date__c</offsetFromField>
            <timeLength>-180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Flex Notify Usage Contract Shipping or Active</fullName>
        <actions>
            <name>Flex_Notify_Usage_Contract_Shipping_or_Active</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email when Flex Contract (Usage deal type) status changes to Shipping or Active</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(   ISCHANGED(Status__c),   OR( ISPICKVAL(Status__c, &apos;Shipping&apos;)     ),   OR( ISPICKVAL(Deal_Type__c, &apos;Usage Agreement&apos;),       ISPICKVAL(Deal_Type__c, &apos;Usage Rental&apos;)     ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Contract Status to Terminated</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Flex_Contract__c.End_of_Term__c</field>
            <operation>equals</operation>
            <value>$1 out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Flex_Contract__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Renewal,Terminated</value>
        </criteriaItems>
        <description>Flex Set Contract Status to Terminated When Scheduled Maturity Date passed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Flex_Set_Contract_Reason_Terminated</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Flex_Set_Contract_Status_to_Terminated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Flex_Contract__c.Scheduled_Maturity_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Flex_Alert_On_Upcoming_Renewal_Opportunity</fullName>
        <assignedTo>dan.mayo@stryker.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>-90</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Flex_Contract__c.Scheduled_Maturity_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Flex Alert On Upcoming Renewal Opportunity.</subject>
    </tasks>
    <tasks>
        <fullName>Flex_Contract</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Flex Contract</subject>
    </tasks>
</Workflow>
