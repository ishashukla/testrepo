<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Activate_Contract</fullName>
        <description>COMM: I-235492</description>
        <field>Status</field>
        <literalValue>Active</literalValue>
        <name>Activate Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Expiration_to_30_Days</fullName>
        <description>Set Expiration to 30 Days</description>
        <field>OwnerExpirationNotice</field>
        <literalValue>30</literalValue>
        <name>Set Expiration to 30 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_as_IsActive</fullName>
        <field>Is_Active__c</field>
        <literalValue>1</literalValue>
        <name>set as IsActive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Activate Contract for Integration</fullName>
        <actions>
            <name>Activate_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_as_IsActive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Pricing Contract</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>DataMigrationIntegration</value>
        </criteriaItems>
        <description>COMM: I-235492</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ContractExpirationNotification</fullName>
        <actions>
            <name>Set_Expiration_to_30_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Contract Expiration Notifications</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), ISBLANK(TEXT(OwnerExpirationNotice )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Expiring_in_30_Days</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Contract_Expiring_in_30_Days</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>-7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contract.EndDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contract Expiring in 30 Days</subject>
    </tasks>
</Workflow>
