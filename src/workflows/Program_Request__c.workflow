<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_NV_Region</fullName>
        <description>This field updates populates NV Sales Region from Account to Program Request</description>
        <field>NV_Region__c</field>
        <formula>Account__r.NV_Region__c</formula>
        <name>Update NV Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NV_Territory</fullName>
        <description>Populates NV Territory every time Program Request is created or edited.</description>
        <field>NV_Territory__c</field>
        <formula>Account__r.NV_Territory__c</formula>
        <name>Update NV Territory</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Sales Territory%2C Region</fullName>
        <actions>
            <name>Update_NV_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_NV_Territory</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Program_Request__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>12/8/2013 - DEPRECATED based on PR descope - Inactivating.  

Populates Sales Territory and Region each time Program Request created or updated.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
