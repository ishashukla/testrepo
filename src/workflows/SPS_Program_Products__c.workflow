<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SPS_Email_Alert_Notify_on_Auto_Renewal_Program_3_Months</fullName>
        <ccEmails>Brian.Howie@stryker.com</ccEmails>
        <ccEmails>Cassandra.Butus@stryker.com</ccEmails>
        <ccEmails>Allison.Archer@stryker.com</ccEmails>
        <description>SPS Email Alert - Notify on Auto Renewal Program 3 Months</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_SPS/SPS_Email_Notification_for_3Months_Auto_Renewal_Programs</template>
    </alerts>
    <alerts>
        <fullName>SPS_Email_Alert_Notify_on_Auto_Renewal_Program_4Weeks</fullName>
        <ccEmails>Brian.Howie@stryker.com</ccEmails>
        <ccEmails>Cassandra.Butus@stryker.com</ccEmails>
        <ccEmails>Allison.Archer@stryker.com</ccEmails>
        <description>SPS Email Alert - Notify on Auto Renewal Program 4 weeks</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_SPS/SPS_Email_Notification_for_4_weeks_Auto_Renewal_Programs</template>
    </alerts>
    <alerts>
        <fullName>SPS_Email_Alert_Notify_on_Program_Termination_3_Months</fullName>
        <ccEmails>brargagan@gmail.com</ccEmails>
        <ccEmails>koo.kim@stryker.com</ccEmails>
        <ccEmails>Erica.Stowe@stryker.com</ccEmails>
        <description>SPS Email Alert - Notify on Program Termination 3 Months</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_SPS/SPS_Email_Notification_for_Programs_Termination_in_3Months</template>
    </alerts>
    <alerts>
        <fullName>SPS_Email_Alert_Notify_on_Program_Termination_4_weeks</fullName>
        <ccEmails>brargagan@gmail.com</ccEmails>
        <ccEmails>koo.kim@stryker.com</ccEmails>
        <ccEmails>Erica.Stowe@stryker.com</ccEmails>
        <description>SPS Email Alert - Notify on Program Termination 4 weeks</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_SPS/SPS_Email_Notification_for_Program_Termination_in_4Weeks</template>
    </alerts>
    <fieldUpdates>
        <fullName>SPS_Remove_Reenewal_Date</fullName>
        <description>Used by SPS to remove Renewal Date when Auto Renewal is unchecked</description>
        <field>Renewal_Date__c</field>
        <name>SPS Remove Reenewal Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SPS_update_PMA_email</fullName>
        <description>used by SPS</description>
        <field>PMA_Email__c</field>
        <formula>PMA__r.Email</formula>
        <name>SPS update PMA email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SPS Opportunity Action Item 3 Months before Renewal Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>SPS_Program_Products__c.Auto_Renewal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SPS_Program_Products__c.Days_Left_Till_Renewal__c</field>
            <operation>greaterThan</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>SPS_Program_Products__c.Days_Left_Till_Renewal__c</field>
            <operation>lessOrEqual</operation>
            <value>90</value>
        </criteriaItems>
        <description>SPS Opportunity Action Item 3 Months before Renewal Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SPS_Email_Alert_Notify_on_Auto_Renewal_Program_3_Months</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SPS_Program_Product_Auto_Renewal_Reminder_3_Months</name>
                <type>Task</type>
            </actions>
            <offsetFromField>SPS_Program_Products__c.Renewal_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SPS Opportunity Action Item on 4 weeks before Renewal Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>SPS_Program_Products__c.Auto_Renewal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SPS_Program_Products__c.Days_Left_Till_Renewal__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>SPS_Program_Products__c.Days_Left_Till_Renewal__c</field>
            <operation>lessOrEqual</operation>
            <value>30</value>
        </criteriaItems>
        <description>SPS Opportunity Action Item on 4 weeks before Renewal Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SPS_Email_Alert_Notify_on_Auto_Renewal_Program_4Weeks</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SPS_Program_Product_Auto_Renewal_Reminder_4_Weeks</name>
                <type>Task</type>
            </actions>
            <offsetFromField>SPS_Program_Products__c.Renewal_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SPS_Update_PMA_Email_on_Program_Product</fullName>
        <actions>
            <name>SPS_update_PMA_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used by SPS</description>
        <formula>ISCHANGED(PMA__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Program_Product_Termination_Date_3_Months</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is an auto generated task by salesforce to notify Account Owner that a related Program Product is set to terminate in 3 Months.</description>
        <dueDateOffset>-90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>SPS_Program_Products__c.Termination_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Program Product Termination Date 3 Months</subject>
    </tasks>
    <tasks>
        <fullName>Program_Product_Termination_Date_4_Weeks</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is an auto generated task by salesforce to notify Account Owner that a related Program Product is set to terminate in 4 weeks.</description>
        <dueDateOffset>-30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>SPS_Program_Products__c.Termination_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Program Product Termination Date 4 Weeks</subject>
    </tasks>
    <tasks>
        <fullName>SPS_Program_Product_Auto_Renewal_Reminder_3_Months</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is an auto generated task by salesforce to notify Account Owner that a related Program Product is set for auto-renew in 3 Months.</description>
        <dueDateOffset>-90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>SPS_Program_Products__c.Renewal_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Program Product Auto Renewal Reminder 3 Months</subject>
    </tasks>
    <tasks>
        <fullName>SPS_Program_Product_Auto_Renewal_Reminder_4_Weeks</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is an auto generated task by salesforce to notify Account Owner that a related Program Product is set for auto-renew in 4 weeks.</description>
        <dueDateOffset>-30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>SPS_Program_Products__c.Renewal_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Program Product Auto Renewal Reminder 4 Weeks</subject>
    </tasks>
</Workflow>
