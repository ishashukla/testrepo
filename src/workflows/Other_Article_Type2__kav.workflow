<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ICCKM_Other_Article_Type_Article_Review_Reminder</fullName>
        <description>ICCKM Other Article Type Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Instrument_CCKM_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/Other_Article_Type_Article_Review_Notification</template>
    </alerts>
    <alerts>
        <fullName>Other_Article_Type_Article_Review_Reminder</fullName>
        <description>Other Article Type Article Review Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Medical_Knowledge_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Medical_Call_Center/Other_Article_Type_Article_Review_Notification</template>
    </alerts>
    <alerts>
        <fullName>Other_Article_Type_Rejection_Notification</fullName>
        <description>Other Article Type Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Article_Rejection_Notification</template>
    </alerts>
    <knowledgePublishes>
        <fullName>Approve_Endo_Other_Article</fullName>
        <action>Publish</action>
        <description>Approve Endo Other Article</description>
        <label>Approve Endo Other Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Other_Article_Approve_Publish</fullName>
        <action>Publish</action>
        <label>Other Article: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Other_Article_Type_Approve_Publish</fullName>
        <action>Publish</action>
        <label>Other Article Type: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>ICCKM Other Article Type Article Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to ICCKM Knowledge Group users on Review Date in Other Article Type Article</description>
        <formula>AND(BEGINS($UserRole.DeveloperName,&quot;Instruments&quot;),!ISNULL(Review_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ICCKM_Other_Article_Type_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Other_Article_Type2__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Other Article Type Article Review Date Rule</fullName>
        <active>true</active>
        <description>Send a reminder mail to Medical Knowledge Group users on Review Date in Other Article Type Article</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(BEGINS($UserRole.DeveloperName,&quot;Medical&quot;),!ISNULL(Review_Date__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Other_Article_Type_Article_Review_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Other_Article_Type2__kav.Review_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
