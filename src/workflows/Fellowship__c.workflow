<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Account_Name_field</fullName>
        <field>Account_Name__c</field>
        <formula>Account__r.Name</formula>
        <name>Populate Account Name field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Account Name</fullName>
        <actions>
            <name>Populate_Account_Name_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), NOT(ISNULL( Account__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
