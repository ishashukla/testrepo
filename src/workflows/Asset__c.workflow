<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Endo_Update_Asset_Part</fullName>
        <description>Update the Part # on the Asset to reflect the Part # of the Asset&apos;s Product</description>
        <field>Part_Number__c</field>
        <formula>Product__r.ProductCode</formula>
        <name>Endo Update Asset Part #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Endo Populate Part %23 on Asset</fullName>
        <actions>
            <name>Endo_Update_Asset_Part</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate the Part # field of an Asset based on the related Product</description>
        <formula>NOT ISNULL (Product__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
