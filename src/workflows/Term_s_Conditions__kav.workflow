<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Term_s_Conditions_Rejection_Notification</fullName>
        <description>Term&apos;s &amp; Conditions Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Article_Rejection_Notification</template>
    </alerts>
    <knowledgePublishes>
        <fullName>Terms_and_Conditions_Approve_Publish</fullName>
        <action>Publish</action>
        <label>Terms and Conditions: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
