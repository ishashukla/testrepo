<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Customer_On_Rescheduled</fullName>
        <description>Notify Customer On Rescheduled</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sales_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Event_Rescheduled_VF</template>
    </alerts>
    <alerts>
        <fullName>Notify_Customer_On_Scheduled</fullName>
        <description>Notify Customer On Scheduled</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sales_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Event_Scheduled_VF</template>
    </alerts>
    <fieldUpdates>
        <fullName>Sales_Rep_Email_Field_Update</fullName>
        <field>Sales_Rep_Email__c</field>
        <formula>SVMXC__Service_Order__r.Sales_Rep_Email__c</formula>
        <name>Sales Rep Email Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Update_Schedule_to_False</fullName>
        <description>Used to set Update Schedule checkbox to FALSE</description>
        <field>Update_Schedule__c</field>
        <literalValue>0</literalValue>
        <name>Set Update Schedule to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Update_Field</fullName>
        <description>This field update is used to uncheck the Update Schedule field so that it can be reused for additional reschedules</description>
        <field>Update_Schedule__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Update Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ActivityDateTime_with_NewDateTime</fullName>
        <description>This field update is used as part of the rescheduling process for work orders</description>
        <field>SVMXC__ActivityDateTime__c</field>
        <formula>New_DateTime__c</formula>
        <name>Update ActivityDateTime with NewDateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Email</fullName>
        <field>Contact_Email__c</field>
        <formula>SVMXC__Service_Order__r.SVMXC__Contact__r.Email</formula>
        <name>Update Contact Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Duration_in_Minutes</fullName>
        <description>Used to update Duration in Minutes based on the value populated in Reschedule Duration To</description>
        <field>SVMXC__DurationInMinutes__c</field>
        <formula>IF( SVMX_Duration_in_Hours__c  &gt; 0, SVMX_Duration_in_Hours__c * 60,  SVMXC__DurationInMinutes__c )</formula>
        <name>Update Duration in Minutes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EndDateTime</fullName>
        <description>This field update is used during the reschedule work order process to update the End Date/Time details on a SVMX Event</description>
        <field>SVMXC__EndDateTime__c</field>
        <formula>NewEndDateTime__c</formula>
        <name>Update EndDateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SVMX_Event_Activity_Date</fullName>
        <description>Used to update Event&apos;s Activity Date field to that of the related Work Order</description>
        <field>SVMXC__ActivityDate__c</field>
        <formula>DATEVALUE( SVMXC__Service_Order__r.SVMXC__Scheduled_Date_Time__c )</formula>
        <name>Update SVMX Event Activity Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SVMX_Event_End_Time</fullName>
        <description>Used to update SVMX Event End Date/Time to a null value</description>
        <field>SVMXC__EndDateTime__c</field>
        <name>Update SVMX Event End Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SVMX_Event_Start_Time</fullName>
        <description>Used to update the Start Date and Time with the value of the Scheduled Date/Time on the Work Order</description>
        <field>SVMXC__StartDateTime__c</field>
        <formula>SVMXC__Service_Order__r.SVMXC__Scheduled_Date_Time__c</formula>
        <name>Update SVMX Event Start Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_StartTime_with_New_ScheduledTime</fullName>
        <field>SVMXC__StartDateTime__c</field>
        <formula>SVMXC__Service_Order__r.SVMXC__Scheduled_Date_Time__c</formula>
        <name>Update StartTime with New ScheduledTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM - Update new schedule</fullName>
        <actions>
            <name>Uncheck_Update_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_ActivityDateTime_with_NewDateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_EndDateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_StartTime_with_New_ScheduledTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow is used to complete the update schedule date and time on an Event when initiated from the Reschedule SFM</description>
        <formula>AND( (ischanged( New_DateTime__c ))||(ischanged( NewEndDateTime__c )), ( Update_Schedule__c = TRUE) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM - When SVMX Event Duration Changed</fullName>
        <actions>
            <name>Update_Duration_in_Minutes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SVMX_Event_End_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update End Date and Time on event when duration is changed</description>
        <formula>AND(ISCHANGED( SVMX_Duration_in_Hours__c ), !ISBLANK( SVMX_Duration_in_Hours__c ), !Update_Schedule__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM - When SVMX Event Rescheduled</fullName>
        <actions>
            <name>Set_Update_Schedule_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Duration_in_Minutes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SVMX_Event_Activity_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SVMX_Event_End_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SVMX_Event_Start_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will update Start and End Time of ServiceMax Events when rescheduled.</description>
        <formula>AND( ISCHANGED( Update_Schedule__c ), ( Update_Schedule__c ), !ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Customer On Reschedule</fullName>
        <actions>
            <name>Notify_Customer_On_Rescheduled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WF rule will be used to notify customer and sales rep on WO re-schedule.</description>
        <formula>AND(         NOT( ISBLANK( SVMXC__Service_Order__c  )) ,       NOT( ISBLANK(SVMXC__StartDateTime__c)),       NOT( ISBLANK(PRIORVALUE(SVMXC__StartDateTime__c))),       NOT( ISBLANK(SVMXC__EndDateTime__c)),        NOT( ISBLANK(PRIORVALUE(SVMXC__EndDateTime__c))),        OR(          ISCHANGED( SVMXC__StartDateTime__c ) ,          ISCHANGED( SVMXC__EndDateTime__c )        ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Customer On Scheduled</fullName>
        <actions>
            <name>Notify_Customer_On_Scheduled</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Contact_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>WF rule will be used to notify customer and sales rep on WO schedule. It will also be used in populating contact email field.</description>
        <formula>AND(   NOT(ISBLANK( SVMXC__Technician__c )),  NOT(ISBLANK(SVMXC__Service_Order__c)),   not (ISBLANK( SVMXC__StartDateTime__c )),  not (ISBLANK( SVMXC__EndDateTime__c ))   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Sales Rep Email</fullName>
        <actions>
            <name>Sales_Rep_Email_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stryker COMM BP : Workflow will be populate Email of Sales Rep</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
