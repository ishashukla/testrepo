<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Flex_Credit_Status_Changed</fullName>
        <description>Flex Credit Status Changed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>RFM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Flex/Flex_Credit_Status_Changed_Opportunity_V2</template>
    </alerts>
    <alerts>
        <fullName>Flex_Notify_Opportunity_Credit</fullName>
        <description>Flex Notify Opportunity Credit</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Flex/Flex_Notify_Opportunity_Credit</template>
    </alerts>
    <fieldUpdates>
        <fullName>Flex_Credit_Set_RFM_Email</fullName>
        <field>RFM_Email__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>Flex Credit Set RFM Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Populate_Credit_Approve_Date</fullName>
        <description>Flex Populate Credit Approve Date</description>
        <field>Credit_Approve_Date__c</field>
        <formula>TODAY()</formula>
        <name>Flex Populate Credit Approve Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Proposed_Address</fullName>
        <description>Proposed Address = (Account) Billing Address</description>
        <field>Proposed_Address__c</field>
        <formula>Opportunity__r.Account.BillingStreet +&quot; &quot;+ Opportunity__r.Account.BillingCity +&quot; &quot;+ Opportunity__r.Account.BillingState +&quot; &quot;+ Opportunity__r.Account.BillingPostalCode</formula>
        <name>Update Proposed Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Flex Credit Set RFM Email</fullName>
        <actions>
            <name>Flex_Credit_Set_RFM_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), NOT(ISBLANK( Opportunity__r.Owner.Email ) ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flex Credit Status Changed</fullName>
        <actions>
            <name>Flex_Credit_Status_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If a Credit rep updates the Credit Status field on the Flex Credit record to any of the following, Salesforce will trigger an email.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(   ISCHANGED(Credit_Status__c),   OR( ISPICKVAL(Credit_Status__c, &apos;Require Additional Info&apos;),       ISPICKVAL(Credit_Status__c, &apos;Approved&apos;),       ISPICKVAL(Credit_Status__c, &apos;Declined&apos;),       ISPICKVAL(Credit_Status__c, &apos;Contingent Approval&apos;)   ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Notify Opportunity Credit</fullName>
        <actions>
            <name>Flex_Notify_Opportunity_Credit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Flex Notify Opportunity Credit</description>
        <formula>NOT($Setup.By_Pass_Config__c.Bypass_WF__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Populate Credit Approve Date</fullName>
        <actions>
            <name>Flex_Populate_Credit_Approve_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Flex Populate Credit Approve Date</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c),  OR(ISPICKVAL(Credit_Status__c , &apos;Approved&apos;), ISPICKVAL(Credit_Status__c , &apos;Contingent Approval&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Proposed Address when created</fullName>
        <actions>
            <name>Update_Proposed_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Proposed Address from account fields. Proposed Address = (Account) Billing Address.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), NOT(ISBLANK(Name)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
