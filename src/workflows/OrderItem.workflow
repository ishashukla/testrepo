<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_Email_Alert_to_CSR</fullName>
        <description>COMM : Email Alert to CSR</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Auto_Response_To_CSR_on_Status_as_Requested</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Notification_To_Project_Manager_On_Order_Product_Being_Reviewed</fullName>
        <description>COMM: Email Notification To Project Manager On Order Product Being Reviewed</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Auto_Response_To_PM_on_Status_as_Reviewed_On_Project</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Notifucation_to_Project_Manager</fullName>
        <description>COMM : Email Notification to Project Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Auto_Response_To_PM_on_Status_as_Reviewed</template>
    </alerts>
    <alerts>
        <fullName>Notify_SCC</fullName>
        <ccEmails>scc-cs2XXX@XXXstryker.comXXX</ccEmails>
        <ccEmails>apurva.bangar@appirio.com</ccEmails>
        <description>Notify SCC</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Notify_for_Order_Line_Item_VF</template>
    </alerts>
    <alerts>
        <fullName>Notify_Tech_Service</fullName>
        <ccEmails>comm.techservicesXXX@XXXstryker.comXXX</ccEmails>
        <description>Notify Tech Service</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Notify_for_Order_Line_Item_VF</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_Populate_Project_Manager_Email</fullName>
        <description>COMM : T-497413</description>
        <field>Project_Manager_Email__c</field>
        <formula>Project_Phase__r.Project_Plan__r.Project_Manager__r.Email</formula>
        <name>COMM : Populate Project Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Populate_Project_Manager_Fr_Project</fullName>
        <field>Project_Manager_Email__c</field>
        <formula>Project_Plan__r.Project_Manager__r.Email</formula>
        <name>COMM:Populate Project Manager Fr Project</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Loaner_Delinquency_Date_Stamp</fullName>
        <description>This stamps TODAY() on the Order Product field Loaner_Delinquency_Date__c</description>
        <field>Loaner_Delinquency_Date__c</field>
        <formula>TODAY()</formula>
        <name>Loaner Delinquency Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Loaner Delinquency Date Stamp</fullName>
        <actions>
            <name>Loaner_Delinquency_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Endo: This workflow stamps the date the Order Product Status__c field is changed to &quot;Awaiting Return&quot; which translates as delinquent on a Loaner. As of development, only takes into consideration Endo orders.</description>
        <formula>AND( 	OR( 		Order.RecordTypeId  =$Setup.EndoGeneralSettings__c.OrderEndoCompleteOrders__c, 		Order.RecordTypeId  =$Setup.EndoGeneralSettings__c.Order_EndoOrdersRT__c 	), 	OR( 		ISPICKVAL( 			Order.Type,&quot;SJC Credit and Replacement&quot; 		), 		ISPICKVAL( 			Order.Type, &quot;SJC Sample Return&quot; 		), 		ISPICKVAL( 			Order.Type, &quot;SJC CPT Return&quot; 		), 		ISPICKVAL( 			Order.Type, &quot;SJC Credit Return&quot; 		) 	), 	ISPICKVAL(Status__c, &quot;Awaiting Return&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
