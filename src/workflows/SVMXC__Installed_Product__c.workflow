<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Reset_QIP_Completion_By</fullName>
        <description>COMM: I-240679</description>
        <field>Case_Owner__c</field>
        <name>Reset QIP Completion By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_QIP_Completion_Date</fullName>
        <description>COMM: I-240679</description>
        <field>QIP_Completion_Date__c</field>
        <name>Reset QIP Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_QIP_Failure_Reason</fullName>
        <field>QIP_Creation_Failure_Reason__c</field>
        <name>Reset QIP Failure Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_QIP_Notes</fullName>
        <field>QIP_Notes__c</field>
        <name>Reset QIP Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_QIP_Status</fullName>
        <description>COMM: I-240679</description>
        <field>QIP_Status__c</field>
        <name>Reset QIP Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Warranty_Start_to_Install_Date</fullName>
        <description>COMM: I-240498</description>
        <field>SVMXC__Warranty_Start_Date__c</field>
        <formula>SVMXC__Date_Installed__c</formula>
        <name>Set Warranty Start to Install Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM Set Warranty Start Date</fullName>
        <actions>
            <name>Set_Warranty_Start_to_Install_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>COMM: I-240498</description>
        <formula>AND( ISBLANK(PRIORVALUE(SVMXC__Date_Installed__c) ), NOT(ISBLANK(SVMXC__Date_Installed__c)), $Profile.Name != &apos;DataMigrationIntegration&apos;, RecordType.DeveloperName =&apos;COMM_Installed_Product&apos;, ISBLANK( SVMXC__Warranty_Start_Date__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QIP ID change resets QIP data</fullName>
        <actions>
            <name>Reset_QIP_Completion_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_QIP_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_QIP_Failure_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_QIP_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_QIP_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>COMM: I-240679</description>
        <formula>AND( NOT(ISNEW()), ISCHANGED( QIP_ID__c ), $Profile.Name !=&apos;DataMigrationIntegration&apos;, RecordType.DeveloperName =&apos;COMM_Installed_Product&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
