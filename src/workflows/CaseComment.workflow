<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Priority_to_Medium</fullName>
        <description>Field update - priority changes to medium when comments added.</description>
        <field>Priority</field>
        <literalValue>Medium</literalValue>
        <name>Update Priority to Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
</Workflow>
