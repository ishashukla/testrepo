<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Territory_Mancode</fullName>
        <description>Update the territory mancode to be the concatenation of the mancode and the business unit for record key use.</description>
        <field>Territory_Mancode__c</field>
        <formula>Mancode__c + &apos;-&apos; + TEXT(Business_Unit__c)</formula>
        <name>Update Territory Mancode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TerritoryUniqueIdentifier</fullName>
        <actions>
            <name>Update_Territory_Mancode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Territory__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow populates the Territory unique identifier.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
