<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SYK_Update_Duration</fullName>
        <field>SVMXC__Duration__c</field>
        <formula>(SVMXC__End_Time__c -  SVMXC__Start_Time__c)*24</formula>
        <name>SYK - Update Duration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SYK - Populate Duration</fullName>
        <actions>
            <name>SYK_Update_Duration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates duration based on changes to start / end date/time</description>
        <formula>ISCHANGED( SVMXC__End_Time__c) ||  ISCHANGED(SVMXC__Start_Time__c ) ||  NOT(ISBLANK( SVMXC__End_Time__c)) ||  NOT(ISBLANK(SVMXC__Start_Time__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
