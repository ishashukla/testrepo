<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_Approval_Alert</fullName>
        <description>COMM : Approval Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_PO_Approval_Template</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_to_Legal_Team</fullName>
        <description>COMM: Email Alert to Legal Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Legal_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Notify_Legal_When_3rd_Party_Purchaser_flag_is_TRUE</template>
    </alerts>
    <alerts>
        <fullName>COMM_PO_Finance_Approval_Alert</fullName>
        <ccEmails>communicationsaccountsreceivable@stryker.com</ccEmails>
        <description>COMM: PO Finance Approval Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_PO_Finance_Approval_Template</template>
    </alerts>
    <alerts>
        <fullName>COMM_Rejection_Alert</fullName>
        <description>COMM : Rejection Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_PO_Rejection_Template</template>
    </alerts>
    <alerts>
        <fullName>COMM_Stage_Closed_Won</fullName>
        <ccEmails>SCC-CS2@stryker.com</ccEmails>
        <description>COMM Stage - Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>1st US Sales Analyst (CE)</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>2nd US Sales Analyst (CE)</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>3rd US Sales Analyst (CE)</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Atlantic Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMF</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Central Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Executive Sponsor</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Service Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Tech</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Flex Financial</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IVS</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Lead Qualifier</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Midwest Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NSE</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Navigation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>New England Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>North Central Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Northeast Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pre-Sales Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary Field Technician</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ProCare Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Engineer</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Services Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Receptionist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Secondary Field Technician</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Service Provider</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Service and Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Southern Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Southwest Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Surgical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TEST</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Tertiary Field Technician</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Customer Service</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Engineering Services</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Operation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Region Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Sales Associate</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Sales Operations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>West Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Sales_Operations_Support/Changed_Stage_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>COMM_Stage_Closed_Won_and_Amount_25K</fullName>
        <ccEmails>SCC-CS2=stryker.com@example.com</ccEmails>
        <description>COMM Stage - Closed Won and Amount &gt; 25K</description>
        <protected>false</protected>
        <recipients>
            <recipient>1st US Sales Analyst (CE)</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>2nd US Sales Analyst (CE)</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>3rd US Sales Analyst (CE)</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Atlantic Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMF</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Central Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Executive Sponsor</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Service Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Tech</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Flex Financial</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IVS</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Lead Qualifier</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Midwest Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NSE</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Navigation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>New England Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>North Central Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Northeast Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pre-Sales Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary Field Technician</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ProCare Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Engineer</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Services Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Receptionist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Secondary Field Technician</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Service Provider</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Service and Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Southern Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Southwest Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Surgical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TEST</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Tertiary Field Technician</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Customer Service</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Engineering Services</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Operation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Region Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Sales Associate</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Sales Operations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>US Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>West Sales Region</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Changed_Stage_Closed_Won_Amount_Exceeds_25K</template>
    </alerts>
    <alerts>
        <fullName>Flex_Notify_Opportunity_Closed_Won</fullName>
        <description>Flex Notify Opportunity Closed Won</description>
        <protected>false</protected>
        <recipients>
            <field>FR_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Portfolio_Administrators</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Flex/Flex_Notify_Opportunity_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>SPS_Email_Alert_Notify_Legal_when_opportunity_progress_to_Legal_Review_Stage</fullName>
        <ccEmails>timothy.throm@stryker.com</ccEmails>
        <description>SPS Email Alert - Notify Legal when opportunity progress to Legal Review Stage</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_SPS/SPS_email_Template_when_opportunity_progress_to_Legal_Review_Stage</template>
    </alerts>
    <alerts>
        <fullName>SPS_Email_Alert_Notify_PMA_Manager_when_oppty_is_set_to_Closed_Won</fullName>
        <description>SPS Email Alert - Notify PMA Manager when oppty is set to Closed-Won</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Program_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_SPS/SPS_when_opportunity_is_set_to_Close_Won</template>
    </alerts>
    <alerts>
        <fullName>SPS_Email_Alert_Notify_oppty_owner_manager_for_win_probability_greater_than_50</fullName>
        <ccEmails>derick.elliott@stryker.com</ccEmails>
        <description>SPS Email Alert - Notify opportunity owner manager for win probability greater than 50</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Stryker_SPS/SPS_when_opportunity_probability_is_greater_than_50</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_Push_Counter_Actualization</fullName>
        <field>Push_Counter__c</field>
        <formula>IF (ISNULL( Push_Counter__c ), 1, Push_Counter__c + 1)</formula>
        <name>COMM Push Counter Actualization</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Review_Text_Update</fullName>
        <field>Review__c</field>
        <formula>&quot;For this opportunity a &quot; &amp; TEXT( StageName ) &amp; &quot; review is required&quot;</formula>
        <name>COMM Review Text Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Set_PO_Approval_to_Booking</fullName>
        <field>PO_Approval_Status__c</field>
        <literalValue>Booking</literalValue>
        <name>COMM Set PO Approval to Booking</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Set_PO_Approval_to_Customer_Svc_Rev</fullName>
        <field>PO_Approval_Status__c</field>
        <literalValue>Customer Service Review</literalValue>
        <name>COMM Set PO Approval to Customer Svc Rev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Set_PO_Approval_to_FinanceLegal_Rev</fullName>
        <field>PO_Approval_Status__c</field>
        <literalValue>Finance/Legal Review</literalValue>
        <name>COMM Set PO Approval to FinanceLegal Rev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Set_PO_Approval_to_Sales_Rep_Review</fullName>
        <field>PO_Approval_Status__c</field>
        <literalValue>Sales Rep Review</literalValue>
        <name>COMM Set PO Approval to Sales Rep Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Update_INTL_Oppty_RT</fullName>
        <description>COMM : I-214337</description>
        <field>RecordTypeId</field>
        <lookupValue>COMM_Intl_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>COMM : Update INTL Oppty RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Update_US_Oppty_RT</fullName>
        <description>COMM :I-214337</description>
        <field>RecordTypeId</field>
        <lookupValue>COMM_Key_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>COMM : Update US Oppty RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_set_Bypass_to_False</fullName>
        <description>COMM set bypass step 2 to false</description>
        <field>Bypass_PO_Approval_Step_2__c</field>
        <literalValue>0</literalValue>
        <name>COMM set Bypass to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_set_Bypass_to_True</fullName>
        <description>COMM setting bypass of approval step 2 to true</description>
        <field>Bypass_PO_Approval_Step_2__c</field>
        <literalValue>1</literalValue>
        <name>COMM set Bypass to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_On_Opp_Stage_Documentation</fullName>
        <description>Date Submitted when Opp Stage is changed to &quot;Documentation&quot;.</description>
        <field>Date_Submitted__c</field>
        <formula>NOW()</formula>
        <name>Date On Opp Stage Documentation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opportunity_Close_Date</fullName>
        <description>Set Opportunity Close Date to today +90 days</description>
        <field>CloseDate</field>
        <formula>Today() + 90</formula>
        <name>Flex Set Opportunity Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Gulf_Coast1</fullName>
        <field>OwnerId</field>
        <lookupValue>charlie.bunch@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Gulf Coast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Mid_Atlantic</fullName>
        <description>Flex Set Opty Owner For Mid-Atlantic</description>
        <field>OwnerId</field>
        <lookupValue>dan.mayo@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Mid-Atlantic</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Midwest</fullName>
        <description>Flex Set Opty Owner For Midwest</description>
        <field>OwnerId</field>
        <lookupValue>shannon.cannata@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Midwest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Midwest1</fullName>
        <field>OwnerId</field>
        <lookupValue>shannon.cannata@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Midwest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Northeast</fullName>
        <description>Flex Set Opty Owner For Northeast</description>
        <field>OwnerId</field>
        <lookupValue>mike.brandell@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Northeast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Northwest</fullName>
        <description>Flex Set Opty Owner For Northwest</description>
        <field>OwnerId</field>
        <lookupValue>wayne.schlosser@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Northwest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Ohio_Valley</fullName>
        <description>Flex Set Opty Owner For Ohio Valley</description>
        <field>OwnerId</field>
        <lookupValue>todd.tonsing@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Ohio Valley</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Southeast</fullName>
        <description>Flex Set Opty Owner For Southeast</description>
        <field>OwnerId</field>
        <lookupValue>todd.beaver@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Southeast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_Southwest</fullName>
        <description>Flex Set Opty Owner For Southwest</description>
        <field>OwnerId</field>
        <lookupValue>shawn.wilson@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For Southwest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Opty_Owner_For_West</fullName>
        <description>Flex Set Opty Owner For West</description>
        <field>OwnerId</field>
        <lookupValue>bill.fuhs@stryker.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flex Set Opty Owner For West</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Past_Close_Date_Alert</fullName>
        <description>Flex Set Past Close Date Alert</description>
        <field>isClosedDatePast__c</field>
        <literalValue>1</literalValue>
        <name>Flex Set Past Close Date Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flex_Set_Past_Close_Date_Alert_To_False</fullName>
        <description>Flex Set Past Close Date Alert To False</description>
        <field>isClosedDatePast__c</field>
        <literalValue>0</literalValue>
        <name>Flex Set Past Close Date Alert To False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FreezeForecastCategoryUpdateToFalse</fullName>
        <description>Modify value of Freeze Forecast Category to false as per the requirement of the task T-511482 and story S-420977.</description>
        <field>Freeze_Forecast_Category__c</field>
        <literalValue>0</literalValue>
        <name>FreezeForecastCategoryUpdateToFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FreezeForecastCategoryUpdateToTrue</fullName>
        <description>Modify value of Freeze Forecast Category to true as per the requirement of the task T-511482 and story S-420977.</description>
        <field>Freeze_Forecast_Category__c</field>
        <literalValue>1</literalValue>
        <name>FreezeForecastCategoryUpdateToTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO_Approval_Stage_To_Booked</fullName>
        <description>COMM: I-230051</description>
        <field>PO_Approval_Status__c</field>
        <literalValue>Booked</literalValue>
        <name>PO Approval Stage To Booked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO_Number_is_Required</fullName>
        <field>PO_Number_Required__c</field>
        <literalValue>1</literalValue>
        <name>PO Number is Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Percent_Probablity_To_100</fullName>
        <description>COMM I-212576</description>
        <field>Percent_Probability__c</field>
        <literalValue>100- Will Win</literalValue>
        <name>Percent Probablity To 100</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Submitted_Date_Field</fullName>
        <field>Submitted_Resubmitted_Date__c</field>
        <formula>Now()</formula>
        <name>Populate Submitted Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revised_PO_Updation_After_Approval</fullName>
        <field>Revised_PO__c</field>
        <literalValue>0</literalValue>
        <name>Revised PO Updation After Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SPS_Update_Project_Manager_Email_on_Oppy</fullName>
        <description>Used by SPS</description>
        <field>Primary_Program_Manager_Email__c</field>
        <formula>Account.Primary_Program_Manager__r.Email</formula>
        <name>SPS Update Project Manager Email on Oppy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CSR_Owner_flag</fullName>
        <description>COMM: setting this flag to true so process builder can update CSR Owner with current user</description>
        <field>Set_CSR_Owner__c</field>
        <literalValue>1</literalValue>
        <name>Set CSR Owner flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Doc_Request_Count</fullName>
        <description>Set Doc Request Count</description>
        <field>Doc_Requests__c</field>
        <formula>IF(ISNULL(Doc_Requests__c), 1, Doc_Requests__c + 1)</formula>
        <name>Set Doc Request Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PO_Approval_Status_Change_Date_Time</fullName>
        <description>COMM T-523675</description>
        <field>PO_Approval_Status_Change_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Set PO Approval Status Change Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PO_Approval_Status_to_In_Review</fullName>
        <description>COMM setting the approval status to Submitted for Review</description>
        <field>PO_Approval_Status__c</field>
        <name>Set PO Approval Status to In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PO_Approval_Status_to_Not_Submitted</fullName>
        <description>COMM</description>
        <field>PO_Approval_Status__c</field>
        <literalValue>Not Submitted</literalValue>
        <name>Set PO Approval Status to Not Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_PO_Submitted</fullName>
        <description>COMM: set stage to PO Submitted</description>
        <field>Stage__c</field>
        <literalValue>PO submitted</literalValue>
        <name>Set Stage to PO Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Capital_Amount</fullName>
        <field>Capital_Amount__c</field>
        <formula>Amount</formula>
        <name>Update Capital Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Flex_Return_Date</fullName>
        <description>Created as per T-514298 and S-419545. Updates the Flex Return Date field with date when the opportunity stage is made Pending Cust. Acceptance</description>
        <field>Flex_Return_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Flex Return Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IVS_Value</fullName>
        <field>IVS_Value__c</field>
        <formula>CPQ_IVS_Value__c</formula>
        <name>Update IVS Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Infection_Control_Value</fullName>
        <field>Infection_Control_Value__c</field>
        <formula>CPQ_Infection_Control_Value__c</formula>
        <name>Update Infection Control Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NSE_Value</fullName>
        <field>NSE_Value__c</field>
        <formula>CPQ_NSE_Value__c</formula>
        <name>Update NSE Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Neptune_Value</fullName>
        <field>Neptune_Value__c</field>
        <formula>CPQ_Neptune_Value__c</formula>
        <name>Update Neptune Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_RT_To_Instr_ClosedWon_Opp_RT</fullName>
        <description>As per T-512120 and S-420435, If the Opportunity.Stage changes from any stage to &apos;Closed Won&apos; the Opportunity.RecordType should get updated to &apos;Instruments Closed Won Opportunity&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>Instruments_Closed_Won_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Opp RT To Instr ClosedWon Opp RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_RT_To_Instruments_Opp_RT</fullName>
        <description>As per T-512120 and S-420435, If the Opportunity.Stage changes from &apos;Closed Won&apos; to any other stage the Opportunity.RecordType should get updated to &apos;Instruments Opportunity Record Type&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>Instruments_Opportunity_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Opp RT To Instruments Opp RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Software_Value</fullName>
        <field>Software_Value__c</field>
        <formula>CPQ_Software_Value__c</formula>
        <name>Update Software Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_for_Forecast</fullName>
        <field>Submitted_for_Forecast__c</field>
        <literalValue>0</literalValue>
        <name>Update Submitted for Forecast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Surgical_Value</fullName>
        <field>Surgical_Value__c</field>
        <formula>CPQ_Surgical_Value__c</formula>
        <name>Update Surgical Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Navigation_Value</fullName>
        <field>Navigation_Value__c</field>
        <formula>CPQ_Navigation_Value__c</formula>
        <name>update Navigation Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COMM %3A Intl RT on Lead Conver</fullName>
        <actions>
            <name>COMM_Update_INTL_Oppty_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_RT_on_Lead_Convert__c</field>
            <operation>equals</operation>
            <value>COMM Intl Opportunity</value>
        </criteriaItems>
        <description>COMM : I-214337</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM %3A Revised PO Approval</fullName>
        <actions>
            <name>Revised_PO_Updation_After_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.PO_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Booking</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM %3A US RT on Lead Conver</fullName>
        <actions>
            <name>COMM_Update_US_Oppty_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_RT_on_Lead_Convert__c</field>
            <operation>equals</operation>
            <value>COMM Key Opportunity</value>
        </criteriaItems>
        <description>COMM : I-214337</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Stage - Closed Won</fullName>
        <actions>
            <name>COMM_Stage_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Equipment Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>USD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Stage - Closed Won and Amount  %3E25K</fullName>
        <actions>
            <name>COMM_Stage_Closed_Won_and_Amount_25K</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>COMM Equipment Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>USD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterThan</operation>
            <value>&quot;USD 25,000&quot;</value>
        </criteriaItems>
        <description>COMM: I-241157</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM populate PO Approval Status Change Date%2FTime on Opportunity</fullName>
        <actions>
            <name>Set_PO_Approval_Status_Change_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>COMM T-523675</description>
        <formula>ISCHANGED( PO_Approval_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A Legal Notification</fullName>
        <actions>
            <name>COMM_Email_Alert_to_Legal_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.X3rd_Party_Purchaser_flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>COMM: T-501756</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Opportunity Record Type To Instruments Closed Won Opportunity</fullName>
        <actions>
            <name>Update_Opp_RT_To_Instr_ClosedWon_Opp_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created as per T-512120 and S-420435, If Opportunity.Stage changes from any stage to &apos;Closed Won&apos; then Opportunity.RecordType should get updated to &apos;Instruments Closed Won Opportunity&apos;</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(PRIORVALUE(StageName) != &apos;Closed Won&apos;, TEXT(StageName) = &apos;Closed Won&apos;,  $Setup.Record_Type__c.Instruments_Opp_Record_Type__c =  RecordType.DeveloperName))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Opportunity Record Type To Instruments Opportunity Record Type</fullName>
        <actions>
            <name>Update_Opp_RT_To_Instruments_Opp_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created as per T-512120 and S-420435, If the Opportunity.Stage changes from &apos;Closed Won&apos; to any other stage the Opportunity.RecordType should get updated to &apos;Instruments Opportunity Record Type&apos;</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(ISCHANGED(StageName), TEXT(StageName) != &apos;Closed Won&apos;, RecordType.DeveloperName =  $Setup.Record_Type__c.Instruments_Closed_Won_Opportunity__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Generic account opportunity</fullName>
        <actions>
            <name>New_Account_Request</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>S-212686:  workflow to assign task for generic opportunity</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), Account.Name = &apos;Temporary Ghost Account&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opportunity Close Date</fullName>
        <actions>
            <name>Flex_Set_Opportunity_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Flex Set Opportunity Close date if User has not entered this.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(OR(RecordType.DeveloperName =&apos;Flex_Opportunity_Conventional&apos;, RecordType.DeveloperName=&apos;Flex_Opportunity_Rental&apos;),CloseDate = TODAY()))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Gulf Coast</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Gulf_Coast1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Gulf Coast</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Gulf Coast</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Mid-Atlantic</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Mid_Atlantic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Mid-Atlantic</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Mid-Atlantic</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Midwest</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Midwest1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Midwest</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Midwest</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Northeast</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Northeast</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Northeast</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Northeast</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Northwest</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Northwest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Northwest</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Northwest</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Ohio Valley</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Ohio_Valley</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Ohio Valley</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Ohio Valley</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Southeast</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Southeast</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Southeast</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Southeast</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For Southwest</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_Southwest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>Southwest</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For Southwest</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Opty Owner For West</fullName>
        <actions>
            <name>Flex_Set_Opty_Owner_For_West</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Flex Opportunity - Conventional,Flex Opportunity - Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Flex_Region__c</field>
            <operation>equals</operation>
            <value>West</value>
        </criteriaItems>
        <description>Flex Set Opty Owner For West</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Past Close Date Alert</fullName>
        <actions>
            <name>Flex_Set_Past_Close_Date_Alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Flex Set Past Close Date Alert</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(OR(ISNEW(), ISCHANGED(CloseDate)),CloseDate &lt; today()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flex Set Past Close Date Alert To False</fullName>
        <actions>
            <name>Flex_Set_Past_Close_Date_Alert_To_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Flex Set Past Close Date Alert To False</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(OR(ISNEW(), ISCHANGED(CloseDate)),CloseDate &gt;= today()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>For CPQ IVS Value</fullName>
        <actions>
            <name>Update_IVS_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-228242 - T-524957</description>
        <formula>ISCHANGED(CPQ_IVS_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>For CPQ Infection Control Value</fullName>
        <actions>
            <name>Update_Infection_Control_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-228242 - T-524957</description>
        <formula>ISCHANGED(CPQ_Infection_Control_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>For CPQ NSE Value</fullName>
        <actions>
            <name>Update_NSE_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-228242 - T-524957</description>
        <formula>ISCHANGED(CPQ_NSE_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>For CPQ Navigation Value</fullName>
        <actions>
            <name>update_Navigation_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-228242 - T-524957</description>
        <formula>ISCHANGED(CPQ_Navigation_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>For CPQ Neptune Value</fullName>
        <actions>
            <name>Update_Neptune_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-228242 - T-524957</description>
        <formula>ISCHANGED(CPQ_Neptune_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>For CPQ Software Value</fullName>
        <actions>
            <name>Update_Software_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-228242 - T-524957</description>
        <formula>ISCHANGED(CPQ_Software_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>For CPQ Surgical Value</fullName>
        <actions>
            <name>Update_Surgical_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>I-228242 - T-524957</description>
        <formula>ISCHANGED( CPQ_Surgical_Value__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Porto Admin when opp is closed won</fullName>
        <actions>
            <name>Flex_Notify_Opportunity_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>A user with the Porto Admin profile is notified automatically when a Flex Opportunity is moved to Closed Won.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(   ISPICKVAL(StageName, &apos;Closed Won&apos;),   ISCHANGED(StageName),   OR(     RecordType.Name = &apos;Flex Opportunity - Conventional&apos;,     RecordType.Name = &apos;Flex Opportunity - Rental&apos;   ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Percent Probability Should Be 100 When Stage Is Forecasted</fullName>
        <actions>
            <name>Percent_Probablity_To_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Stage__c</field>
            <operation>equals</operation>
            <value>Forecasted</value>
        </criteriaItems>
        <description>COMM I-212576, When stage is Forecasted , Percent Probablity should be 100.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Capital Amount</fullName>
        <actions>
            <name>Update_Capital_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-551533 and I-242063 - workflow to populate capital amount with Amount field on Opportunity if no quote is present .</description>
        <formula>AND(OR(RecordType.DeveloperName = $Setup.Record_Type__c.Instruments_Closed_Won_Opportunity__c,RecordType.DeveloperName = $Setup.Record_Type__c.Instruments_Opp_Record_Type__c),AND( NOT( ISNULL( Amount )),Amount &gt; 0), NOT( CPQ_Quote_Created__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Flex Return Date</fullName>
        <actions>
            <name>Update_Flex_Return_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created as per T-514298 and S-419545. Workflow fires whenever opportunity stage changes to Pending Cust. Acceptance.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), IF(AND(TEXT(PRIORVALUE(StageName)) != &apos;Pending Cust. Acceptance&apos;, TEXT(StageName) = &apos;Pending Cust. Acceptance&apos;), true, false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset Submitted for Forecast</fullName>
        <actions>
            <name>Update_Submitted_for_Forecast</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ( 2 OR 3 )</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Submitted_for_Forecast__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ForecastCategoryName</field>
            <operation>equals</operation>
            <value>Omitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPS Email notification for opportunities greater than 50</fullName>
        <actions>
            <name>SPS_Email_Alert_Notify_oppty_owner_manager_for_win_probability_greater_than_50</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When an Opportunity probability is greater than 50%, send an email to the Sales Manager of opportunity Owner</description>
        <formula>AND( ISCHANGED(Probability), RecordType.DeveloperName = &apos;SPS_Opportunity_Record_Type&apos;, Probability &gt;= 0.50, Probability &lt; 1.00)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPS Email notification when opportunity progresses to Legal Review Stage</fullName>
        <actions>
            <name>SPS_Email_Alert_Notify_Legal_when_opportunity_progress_to_Legal_Review_Stage</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Legal Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>SPS Opportunity Record Type</value>
        </criteriaItems>
        <description>Used by SPS: Email is send to legal (Puja Leekha) when opportunity status changed to Legal Review</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SPS Email to PMA Manager on oppty closed won</fullName>
        <actions>
            <name>SPS_Email_Alert_Notify_PMA_Manager_when_oppty_is_set_to_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>SPS Opportunity Record Type</value>
        </criteriaItems>
        <description>Used by SPS: When an Opportunity is set to &apos;Closed - won&apos;, an email notification needs to be sent to the PMA Manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SPS_Update_Project_Manager_Email_on_Opportunity</fullName>
        <actions>
            <name>SPS_Update_Project_Manager_Email_on_Oppy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used by SPS</description>
        <formula>AND(RecordType.Name = &apos;SPS Opportunity Record Type&apos;, ISCHANGED(Probability))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Doc Request Count</fullName>
        <actions>
            <name>Set_Doc_Request_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Doc Request Count</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND( OR(   RecordType.DeveloperName = &quot;Flex_Opportunity_Conventional&quot;,   RecordType.DeveloperName = &quot;Flex_Opportunity_Rental&quot;   ),   ISPICKVAL(StageName, &apos;Documentation&apos;),   NOT(ISPICKVAL(PRIORVALUE(StageName), &apos;Documentation&apos;)) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Date Submitted Opp Stage Documentation</fullName>
        <actions>
            <name>Date_On_Opp_Stage_Documentation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Date Submitted when Opp Stage is changed to &quot;Documentation&quot;.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(ISPICKVAL(StageName , &apos;Documentation&apos;), ISCHANGED( StageName)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Freeze Forecast Category Field False</fullName>
        <actions>
            <name>FreezeForecastCategoryUpdateToFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is created to change the value of Freeze Forecast Category Field to False as per the requirement of the task T-511482 and story S-420977.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), IF(  AND( OR(RecordType.DeveloperName == &quot;Instruments_Opportunity_Record_Type&quot;,RecordType.DeveloperName == &quot;Instruments_Closed_Won_Opportunity&quot; ), OR( AND(ISPICKVAL( StageName ,&quot;Closed Won&quot;),  NOT(ISPICKVAL(PRIORVALUE(StageName),&quot;Closed Won&quot;))),AND(NOT(ISPICKVAL(ForecastCategoryName ,&quot;Omitted&quot;)), ISPICKVAL(PRIORVALUE(ForecastCategoryName),&quot;Omitted&quot;)) ),ISNEW(),OR(LastModifiedBy.Profile.Name != &quot;System Administrator&quot;,LastModifiedBy.Profile.Name != &quot;DataMigrationIntegration&quot;) ),  True,  False ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Freeze Forecast Category Field True</fullName>
        <actions>
            <name>FreezeForecastCategoryUpdateToTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is created to update Freeze Forecast Category Field when Instruments user manually updates the value of Forecast Category to &quot;Omitted&quot; as per the requirement of the task T-511482 and story S-420977.</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), IF(  AND( OR(RecordType.DeveloperName == &quot;Instruments_Opportunity_Record_Type&quot;, RecordType.DeveloperName == &quot;Instruments_Closed_Won_Opportunity&quot;), ISPICKVAL(ForecastCategoryName ,&quot;Omitted&quot;), NOT(ISPICKVAL(PRIORVALUE(ForecastCategoryName),&quot;Omitted&quot;)),  ISCHANGED(ForecastCategoryName) , OR(LastModifiedBy.Profile.Name != &quot;System Administrator&quot;,LastModifiedBy.Profile.Name != &quot;DataMigrationIntegration&quot;) ) ,  True,  False ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Submitted Date Field</fullName>
        <actions>
            <name>Populate_Submitted_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populates the submitted/resubmitted for forecast field when submitted for forecast or resubmitted for forecast is checked</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(OR(RecordType.DeveloperName = $Setup.Record_Type__c.Instruments_Opp_Record_Type__c,RecordType.DeveloperName =  $Setup.Record_Type__c.Instruments_Closed_Won_Opportunity__c) ,OR(Submitted_for_Forecast__c = true, Resubmitted_for_Forecast__c = true)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updated PO Approval Stage When Stage Closed Won</fullName>
        <actions>
            <name>PO_Approval_Stage_To_Booked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>,COMM Equipment Opportunity,COMM Key Opportunity,COMM Service Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Stage__c</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>COMM: I-230051</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>New_Account_Request</fullName>
        <assignedTo>Flex_Business_Admin</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Please initiate request for new account related to the generic account opportunity created.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Account Request</subject>
    </tasks>
</Workflow>
