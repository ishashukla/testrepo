<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_COMM_RAQA</fullName>
        <description>Notify COMM RAQA</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_RAQA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Notify_COMM_RAQA</template>
    </alerts>
    <rules>
        <fullName>Notify RAQA on Answer Change</fullName>
        <actions>
            <name>Notify_COMM_RAQA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>COMM BP : I-238468</description>
        <formula>!ISNEW()&amp;&amp; Case__r.RecordType.DeveloperName == &apos;COMM_US_Stryker_Field_Service_Case&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify RAQA on RQA Field Change</fullName>
        <actions>
            <name>Notify_COMM_RAQA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow is created to notify RAQA on regulatory question answer field change (T-513186 - Stryker COMM BP)</description>
        <formula>AND ( BEGINS(Case__r.RecordType.DeveloperName, &quot;COMM&quot;), ISPICKVAL(Case__r.Type, &quot;Adverse Event Investigation&quot;), OR( ISPICKVAL(Adverse_Consequences__c , &quot;Yes&quot;) ,ISPICKVAL(Answer_8__c  , &quot;Yes - Impact - See Adverse Consequences&quot;),ISPICKVAL(Answer_9__c, &quot;Yes&quot;) )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
