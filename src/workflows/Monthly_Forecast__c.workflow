<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ForecastReminderEmail</fullName>
        <description>ForecastReminderEmail</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Instruments/Monthly_Forecast_Reminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>Reset_Forecast_Submitted</fullName>
        <field>Forecast_Submitted__c</field>
        <literalValue>0</literalValue>
        <name>Reset Forecast Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pending_Unlock</fullName>
        <field>Unlock_Request_Pending__c</field>
        <literalValue>0</literalValue>
        <name>Update Pending Unlock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pending_Unlock1</fullName>
        <field>Unlock_Request_Pending__c</field>
        <literalValue>0</literalValue>
        <name>Update Pending Unlock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unlock_Pending</fullName>
        <field>Unlock_Request_Pending__c</field>
        <literalValue>1</literalValue>
        <name>Update Unlock Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Monthly Forecast Non Submission Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Monthly_Forecast__c.Month_End_Reminder__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Monthly_Forecast__c.Forecast_Submitted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Monthly_Forecast__c.Month_End_Reminder__c</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Forecast_Year__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rep Forecast</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ForecastReminderEmail</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Monthly_Forecast__c.Month_End_Reminder__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
