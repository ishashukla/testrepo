<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Date_Completed</fullName>
        <field>Date_Completed__c</field>
        <formula>NOW()</formula>
        <name>Update Date Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Track Conditions of Approval Completed Status</fullName>
        <actions>
            <name>Update_Date_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Completed date on Conditions of Approval in case of status completed</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), AND(ISPICKVAL(Status__c, &apos;Completed&apos;), ISCHANGED(Status__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
