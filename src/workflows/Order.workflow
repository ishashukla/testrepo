<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>COMM_Finance_Comments</fullName>
        <field>COMM_Finance_Comments__c</field>
        <formula>Opportunity.Finance_Comments__c</formula>
        <name>COMM Finance Comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COMM_Trade_in_Table_Notes</fullName>
        <field>Trade_in_Table_Notes_c__c</field>
        <formula>Opportunity.Trade_in_Table_Notes__c</formula>
        <name>COMM Trade in Table Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Links_To_DocView_Invoices</fullName>
        <description>Populate Links To DocView Invoices</description>
        <field>Link_to_DocView_Invoices__c</field>
        <formula>&quot;http://hydra.med.strykercorp.com/contentexplorer/servlet/VipDms?NAME=JDEINVOICE&amp;TYPE=DL&amp;DN=Docview&amp;DC=all&amp;-OE%23=&quot; +  JDE_Order_Number__c</formula>
        <name>Populate Links To DocView Invoices</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Links_To_DocView_Order_Docs</fullName>
        <description>Populate Links To DocView Order Docs for Medical Division</description>
        <field>Link_to_DocView_Order_Docs__c</field>
        <formula>&quot;http://hydra.med.strykercorp.com/contentexplorer/servlet/VipDms?NAME=ORDER%5fDOCS&amp;TYPE=DL&amp;DN=Docview&amp;DC=all&amp;-ORDER%5fNUM=&quot; +   JDE_Order_Number__c</formula>
        <name>Populate Links To DocView Order Docs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Links_To_DocView_Ship_Docs</fullName>
        <description>Populate Links To DocView Ship Docs for Medical Division</description>
        <field>Link_to_DocView_Ship_Docs__c</field>
        <formula>&quot;http://hydra.med.strykercorp.com/contentexplorer/servlet/VipDms?NAME=SHIP%5fDOC&amp;TYPE=DL&amp;DN=Docview&amp;DC=all&amp;-ORDER=&quot; +  JDE_Order_Number__c</formula>
        <name>Populate Links To DocView Ship Docs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RT_changed_to_Endo_Complete_Orders</fullName>
        <description>Updates record type from &quot;Endo Orders&quot; to &quot;Endo Complete Orders&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>Endo_Complete_Orders</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RT changed to Endo Complete Orders</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Endo Entered RecordType</fullName>
        <actions>
            <name>RT_changed_to_Endo_Complete_Orders</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Endo Orders</value>
        </criteriaItems>
        <description>Changes record type from &apos;Endo Orders&apos; to &apos;Endo Complete Orders&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Loaner Delinquency Date</fullName>
        <active>false</active>
        <formula>AND(OR( RecordTypeId =$Setup.EndoGeneralSettings__c.OrderEndoCompleteOrders__c,RecordTypeId =  $Setup.EndoGeneralSettings__c.Order_EndoOrdersRT__c), OR( ISPICKVAL(Type, &quot;SJC Credit and Replacement&quot;), ISPICKVAL(Type, &quot;SJC Sample Return&quot;),ISPICKVAL(Type, &quot;SJC CPT Return&quot;),ISPICKVAL(Type, &quot;SJC Credit Return&quot;)),ISPICKVAL(Status, &quot;Awaiting Return&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Links To DocView</fullName>
        <actions>
            <name>Populate_Links_To_DocView_Invoices</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Links_To_DocView_Order_Docs</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Links_To_DocView_Ship_Docs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate Links To DocView by adding JDE Number for Medical Division</description>
        <formula>AND(NOT($Setup.By_Pass_Config__c.Bypass_WF__c), OR(ISNEW(), ISCHANGED(JDE_Order_Number__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Regional Manager and Sales Director</fullName>
        <active>false</active>
        <description>I-234330 - T-543351</description>
        <formula>OR(    RecordType.DeveloperName == &apos;Endo_Orders&apos;,   RecordType.DeveloperName == &apos;Endo_Complete_Orders&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
