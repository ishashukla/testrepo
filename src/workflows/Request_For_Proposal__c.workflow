<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RFP_Approval_Notification</fullName>
        <description>RFP Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/RFP_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>RFP_Rejection_Notification</fullName>
        <description>RFP Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/RFP_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>RFP_Submission_Notification</fullName>
        <description>RFP Submission Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>yaron.shiba@stryker.com.sykprod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/RFP_Submission_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_RFP_IsApproved_to_False</fullName>
        <description>COMM: story S377017</description>
        <field>IsApproved__c</field>
        <literalValue>0</literalValue>
        <name>Update RFP IsApproved to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RFP_IsApproved_to_True</fullName>
        <description>COMM story S-377017</description>
        <field>IsApproved__c</field>
        <literalValue>1</literalValue>
        <name>Update RFP IsApproved to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RFP_Status_to_Requested</fullName>
        <field>Status__c</field>
        <literalValue>Requested</literalValue>
        <name>Update RFP Status to Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RFP_Status_to_WIP</fullName>
        <field>Status__c</field>
        <literalValue>WIP</literalValue>
        <name>Update RFP Status to WIP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RFP_status_to_New</fullName>
        <description>COMM : S-377017</description>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <name>Update RFP status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
