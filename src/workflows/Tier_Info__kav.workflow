<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Tier_Info_Rejection_Notification</fullName>
        <description>Tier Info Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Article_Rejection_Notification</template>
    </alerts>
    <knowledgePublishes>
        <fullName>Tier_Info_Approve_Publish</fullName>
        <action>Publish</action>
        <label>Tier Info: Approve &amp; Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
