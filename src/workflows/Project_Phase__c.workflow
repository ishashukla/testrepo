<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COMM_Customer_Service_Notified_About_Project_Phase_Completion</fullName>
        <ccEmails>SCC-CS2@stryker.com</ccEmails>
        <description>COMM Customer Service Notified About Project Phase Completion</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Project_Phase_Is_Completed</template>
    </alerts>
    <alerts>
        <fullName>COMM_Customer_Sign_Off_Notification</fullName>
        <description>COMM Customer Sign Off Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>COMM_Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>COMM_Automated/COMM_Sign_Off_Completion</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_Notify_Sales_rep_5_days_before_Project_Phase_install_date</fullName>
        <description>COMM Email Alert- Notify Sales rep 5 days before Project Phase install date</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/Notify_Sales_rep_5_days_before_Project_Phase_install_date</template>
    </alerts>
    <alerts>
        <fullName>COMM_Email_Alert_to_Project_Manager</fullName>
        <description>COMM : Email Alert to Project Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COMM_Automated/COMM_Template_For_Project_Manager_on_Project_Completion</template>
    </alerts>
    <fieldUpdates>
        <fullName>COMM_Update_PPhase_Sign_Complete</fullName>
        <description>Ref (I-240269) , this sets status of phase to sign of complete upon BU sign completion</description>
        <field>Milestone__c</field>
        <literalValue>Customer Sign Off Complete</literalValue>
        <name>COMM Update PPhase Sign off complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Project_Stage</fullName>
        <description>COMM: I-230042</description>
        <field>Stage__c</field>
        <literalValue>Closed</literalValue>
        <name>Close Project Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Project_Plan__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>COMM %3A Send an email to Sales Rep 5 days before Project Phase install date</fullName>
        <active>true</active>
        <description>Send an email to Sales Rep 5 days before Project Phase install date</description>
        <formula>NOT(ISBLANK(Install_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>COMM_Email_Alert_Notify_Sales_rep_5_days_before_Project_Phase_install_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Project_Phase__c.Install_Date__c</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>COMM Customer Service Notified About Project Phase Completion</fullName>
        <actions>
            <name>COMM_Customer_Service_Notified_About_Project_Phase_Completion</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Project_Phase__c.Milestone__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project_Phase__c.Milestone__c</field>
            <operation>equals</operation>
            <value>Customer Sign Off Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project_Phase__c.Phase_Project__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>COMM T-497731</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Customer Sign Off Notification</fullName>
        <actions>
            <name>COMM_Customer_Sign_Off_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project_Phase__c.Milestone__c</field>
            <operation>contains</operation>
            <value>Customer Sign Off Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Phase Completion Rule</fullName>
        <actions>
            <name>COMM_Email_Alert_to_Project_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Phase__c.Milestone__c</field>
            <operation>equals</operation>
            <value>Installation Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM Project Phase Docusign customer sign off complete</fullName>
        <actions>
            <name>COMM_Update_PPhase_Sign_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This work rule sets the Status of project phase to Customer Sign off complete (Ref : I-240269)</description>
        <formula>AND( Envelope_Signed_Status__c == &apos;Completed&apos;, Signed_Document_Name__c == $Label.Project_Phase_BU_Document_Name)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COMM%3A Close Plan when Phase Closed</fullName>
        <actions>
            <name>COMM_Customer_Service_Notified_About_Project_Phase_Completion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>COMM: I-230042; updated milestone: I-244590</description>
        <formula>AND(Text(Milestone__c) = &apos;Pending Closure&apos;, Project_Plan__r.Phased_Sign_Off__c = True )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
