<!--
/**================================================================      
* Appirio, Inc
* Name: COMM_ProjectPlanCreationPage
* Description: Used to create a record of new project plan (T-492006)
* Created Date: 12th April 2016
* Created By: Kirti Agarwal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 15th April 2016    Kirti Agarwal    T-491567 - populate OwnerId and Project_Manager__c fields
* July 2016          Rahul Aeran      To make the page SF1 and LEX compatible  
* 4th August 2016    Nitish Bansal    I-228021 - Re-desigend the page to make sure PM is able to create project plan record with correct values in classic as well as LEX
* 02th August 2016   ralf hoffmann    made Completion Date optional I-232743
==================================================================*/
-->
<apex:page standardcontroller="Project_Plan__c" extensions="COMM_ProjectPlanCreationController" id="pid">
  <apex:variable var="newUI" value="newSkinOn" 
    rendered="{!$User.UIThemeDisplayed = 'Theme4t'}">
    <apex:stylesheet value="{!URLFOR($Resource.slds, '/assets/styles/salesforce-lightning-design-system-ltng.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.slds, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
    
    </apex:variable>
  <apex:form id="form1">
    <style>
      .bPageTitle .ptBody .content {
        float: left;
        vertical-align: middle;
        padding-left: 5px;
        width: 70%;
      }
    </style>
    <!--<script>
      function validatePlanName() {
        var x = document.getElementById('pid:form1:pg1:psec1:pbsec1:planName').value;
        alert('doc id='+x);
        
        if (x == null || x == "") {
            document.getElementById('errorBlock').style.display='block';
            return false;
        }else{
            document.getElementById('errorBlock').style.display='none';
        }
        
        
    } 
  
  </script>-->
    <apex:pagemessages />
    <!-- This is desktop version -->
    <div class="bPageTitle" style="{!IF($User.UIThemeDisplayed != 'Theme4t', 'display:block;', 'display:none;')}">
      <div class="ptBody">
        <div class="content"> <img src="/s.gif" alt="Custom" class="pageTitleIcon" title="Custom" />
          <h1 class="pageType">Project Plan Edit<span class="titleSeparatingColon">:</span></h1>
          <h2 class="pageDescription"> New Project Plan </h2>
          <div class="blank">&nbsp;</div>
        </div>
      </div>
    </div>
    
    <apex:pageblock title="Project Plan Edit" id="pg1" rendered="{!$User.UIThemeDisplayed != 'Theme4t'}">
      <apex:pageblocksection columns="1" title="Information" id="psec1">
        <apex:inputField value="{!Project_Plan__c.Account__c}" required="true"/><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <apex:inputField value="{!Project_Plan__c.Start_Date__c}" required="true"/><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <apex:inputField value="{!Project_Plan__c.Completion_Date__c}" required="false"/><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view--><!-- RH 8/30 made optional I-232743 -->
        <apex:pageBlockSectionItem id="pbsec1">
        <apex:outputLabel value="Project Plan Name"/>
        <apex:outputPanel styleClass="requiredInput" layout="block" id="Name">
        <apex:outputPanel styleClass="requiredBlock" layout="block"/>
        <apex:inputText value="{!Project_Plan__c.Name}" required="true" maxlength="50" id="planName"/><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <!--<div class="errorMsg" style="display:none" id="errorBlock"><strong>Error:</strong> You must enter a value</div>-->
        </apex:outputPanel>
        </apex:pageBlockSectionItem>
        <apex:inputField value="{!Project_Plan__c.Stage__c}" required="true"/><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <apex:inputField value="{!Project_Plan__c.Project_Manager__c}" /><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <apex:inputField value="{!Project_Plan__c.Project_Engineer__c}" /><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <apex:inputField value="{!Project_Plan__c.Long_Name__c}" required="true"/><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <apex:inputField value="{!Project_Plan__c.Order_Type__c}" required="true"/><!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
      </apex:pageblocksection>
      
      <apex:pageBlockButtons >
        <apex:commandButton value="Save" action="{!saveProjectPlan}"/>
        <apex:commandButton value="Cancel" action="{!cancelRecord}" immediate="true"/>
      </apex:pageBlockButtons>
      
    </apex:pageblock>
    <!--<apex:outputLabel value="{!$User.UIThemeDisplayed}"/>-->
    <!-- This is sf1 version -->
    <apex:outputPanel rendered="{!$User.UIThemeDisplayed = 'Theme4t'}"> <!-- NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view-->
        <div class="slds" style="{!IF($User.UIThemeDisplayed = 'Theme4t', 'display:block;', 'display:none;')}">
        <div class="header">
           <div class="slds-page-header" role="banner">
             <div class="slds-media">
               <div class="slds-media__figure">                 
               </div>
               <div class="slds-media__body">
                 <apex:outputPanel >
                  Project Plan Edit<br/>
                  New Project Plan
               </apex:outputPanel>
               </div>
             </div>
           </div>
         </div>
         <div class="slds-button-group slds-align-center" role="group">
          <apex:commandButton value="Save"  styleClass="slds-button slds-button--neutral" action="{!saveProjectPlan}"/> <!--  onclick="redirectToRecord(saveAF());" --> 
              <apex:commandButton value="Cancel"  action="{!cancelRecord}" immediate="true" styleClass="slds-button slds-button--neutral"/> <!-- onclick="redirectToRecord(null);"  -->
         </div>
         <div class="slds-form--stack">       
           <apex:outputLabel value="Account" for="txtAccount" />
           <c:AutoCompleteSearchbox id="txtAccount" sObject="{!accList}" fld="Name" req="{!IF($User.UIThemeDisplayed = 'Theme4t', 'false', 'false')}"/><!-- NB - 08/04 - I-228021 - Made the required true conditional, so it's required only in LEX and doesn't impact post back call on save in classic -->                
           <c:LEXInputFieldComponent SObject="{!projectPlan}" Field="Start_Date__c"  require="{!IF($User.UIThemeDisplayed = 'Theme4t', 'true', 'false')}"/><!-- NB - 08/04 - I-228021 - Made the required true conditional, so it's required only in LEX and doesn't impact post back call on save in classic -->                
           <c:LEXInputFieldComponent SObject="{!projectPlan}" Field="Completion_Date__c"  require="{!IF($User.UIThemeDisplayed = 'Theme4t', 'false', 'false')}" lbl="Completion Date"/><!-- NB - 08/04 - I-228021 - Made the required true conditional, so it's required only in LEX and doesn't impact post back call on save in classic --><!-- RH 8/30 made optional I-232743 -->
                 <c:LEXInputFieldComponent SObject="{!projectPlan}" Field="Name"  require="{!IF($User.UIThemeDisplayed = 'Theme4t', 'true', 'false')}"/><!-- NB - 08/04 - I-228021 - Made the required true conditional, so it's required only in LEX and doesn't impact post back call on save in classic -->
                 <c:LEXInputFieldComponent SObject="{!projectPlan}" Field="Stage__c"  require="{!IF($User.UIThemeDisplayed = 'Theme4t', 'true', 'false')}"/><!-- NB - 08/04 - I-228021 - Made the required true conditional, so it's required only in LEX and doesn't impact post back call on save in classic -->
                 <apex:outputLabel value="Project Manager" for="txtProjectManager" />
             <c:AutoCompleteSearchbox id="txtProjectManager" sObject="{!userList}" fld="Name"  />
                 <apex:outputLabel value="Project Engineer" for="txtProjectEngineer" />
                   <c:AutoCompleteSearchbox id="txtProjectEngineer" sObject="{!projectEngineerList}" fld="Name"  />
                 <c:LEXInputFieldComponent SObject="{!projectPlan}" Field="Long_Name__c"  require="{!IF($User.UIThemeDisplayed = 'Theme4t', 'true', 'false')}"/><!-- NB - 08/04 - I-228021 - Made the required true conditional, so it's required only in LEX and doesn't impact post back call on save in classic -->                
                 <c:LEXInputFieldComponent SObject="{!projectPlan}" Field="Order_Type__c"  require="{!IF($User.UIThemeDisplayed = 'Theme4t', 'true', 'false')}"/><!-- NB - 08/04 - I-228021 - Made the required true conditional, so it's required only in LEX and doesn't impact post back call on save in classic -->                                   
         </div>
      </div>
      
    </apex:outputPanel>
  </apex:form>
</apex:page>