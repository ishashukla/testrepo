<!--
/**=====================================================================
 * Appirio, Inc
 * Name: OrderLineItemAddressPage.page
 * Description: OrderLineItemAddressPage on OLI related list giving 
                capability to update shipping address and other details
 * Created Date: 04/13/2016
 * Created By: Rahul Aeran (T-497412 COMM)
 * 
 * Date Modified      Modified By              Description of the update
 * 05/02/2016         Rahul Aeran              T-497412(COMM)
 * 10 Aug 2016        Kanika Mathur            I-229125(COMM): To remove PO Number, Product Code columns from page
 * 31 Oct 2016    	  Ralf Hoffmann			   I-242229 (COMM): added 2 columsns Softwared, Assets to be installed.
 * 01 Nov 2016  	  Ralf Hoffmann			   I-242315 (COMM): Resetting sorting to Oracle Order number and OLI sort sequence
 *																Removed Description, it is same as Product Name/Description already displayed 
 * =====================================================================*/
-->
<apex:page controller="OrderLineItemAddressController" sidebar="false">
   
    <apex:form >
        <apex:pageMessages id="pgMsg"/>
        
        <apex:pageBlock title="Available Addresses" id="pbAddress">
            
          <apex:pageBlockSection collapsible="false" columns="3">
              <apex:pageBlockSectionItem >
                <apex:outputLabel value="Project Phase: "/>
                <apex:outputLink value="/{!selectedProjectPhase.Id}">{!selectedProjectPhase.Name}</apex:outputLink> 
                </apex:pageBlockSectionItem>
              <apex:pageBlockSectionItem >
                <apex:outputLabel value="Project Plan: "/>
                <apex:outputLink value="/{!selectedProject.Id}">{!selectedProject.Name}</apex:outputLink>
                </apex:pageBlockSectionItem>
              <apex:pageBlockSectionItem >
                  <apex:outputLabel value="Account: "/>
                  <apex:outputLink value="/{!selectedProject.Account__c}">{!selectedProject.Account__r.Name}</apex:outputLink>
                  </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
           
          <apex:pageBlockTable value="{!lstSelectedAddressObject}" var="add" rendered="{!lstSelectedAddressObject.size>0}">
                <apex:column headerValue="Address">
                   <apex:selectList id="selectCriteria" size="1" value="{!selectedAddress}">
                        <apex:actionSupport event="onchange" action="{!setAddress}" reRender="pbAddress" status="myStatus"/>
                        <apex:selectOptions value="{!lstAvailableAddress}"/>
                    </apex:selectList> 
                </apex:column>
                <apex:column headerValue="Status" value="{!add.Status__c}">
                </apex:column>
                <apex:column headerValue="Start Date" value="{!add.Start_Date__c}">
                </apex:column>
                <apex:column headerValue="End Date" value="{!add.End_Date__c}">
                
                   
                </apex:column>
          </apex:pageBlockTable>
      </apex:pageBlock>
      <apex:pageBlock title="Update Information">
        <apex:pageBlockSection columns="2">
          <apex:pageBlockSectionItem >
            <apex:outputLabel value="Requested Ship Date" for="txtShipDate"/>
            <apex:inputField value="{!dummyOrderItem.Requested_Ship_Date__c}" id="txtShipDate"/>
            
          </apex:pageBlockSectionItem>
          <apex:pageBlockSectionItem >
            <apex:outputLabel value="Approval Status" for="txtApprovalStatus"/>
            <apex:inputField value="{!dummyOrderItem.Approval_Status__c}" id="txtApprovalStatus"/>
          </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
      </apex:pageBlock>
        <apex:pageBlock mode="detail" id="membersPb">
            <apex:actionStatus id="myStatus">
              <apex:facet name="start" >
                <div style="position: fixed; top: 0; left: 0; right: 0; bottom: 0; opacity: 0.25; z-index: 1000; background-color: black;">
                    &nbsp;
                </div>
                <div style="position: fixed; left: 0; top: 0; bottom: 0; right: 0; z-index: 1001; margin: 15% 50%">
                    <div style="display: inline-block; padding: 2px; background-color: #fff; width: 125px;">
                        <img src="/img/loading.gif" style="float: left; margin: 8px;" />
                        <span style="display: inline-block; padding: 10px 0px;">Please Wait...</span>
                    </div>
                </div>                 
              </apex:facet>
            </apex:actionStatus>

            <apex:pageBlockButtons location="top">        
                <!-- 11/1/2016 RH I-242315 Restting sorting to Oracle Order number and OLI sort sequence -->
                <apex:commandButton value="Reset Sort Order" action="{!setOrderBy}" reRender="pgMsg,pbItem">
                	<apex:param value="Order_Number_in_Oracle__c,Oracle_Order_Line_Number_To_Sort__c" assignTo="{!sortExpression}"></apex:param>
                </apex:commandButton>
                <apex:commandButton value="Update Address" action="{!updateAddress}" rerender="pgMsg,pbItem" oncomplete="uncheckAll();" status="myStatus" rendered="{!lstAvailableAddress.size>0 && items.size > 0}"/>
                <apex:commandButton value="Update Date" action="{!updateDate}" rerender="pgMsg,pbItem" oncomplete="uncheckAll();" status="myStatus"/>            
                <apex:commandButton value="Update Status" action="{!updateStatus}" rerender="pgMsg,pbItem" oncomplete="uncheckAll();" status="myStatus"/>
                

            </apex:pageBlockButtons>   
            
            <apex:pageBlockTable value="{!items}" var="oi" rendered="{!items.size > 0}" id="pbItem">
            
                <apex:column >
                    <apex:facet name="header">
                       <apex:inputCheckbox id="checkAllDone" onClick="checkAll(this);">
                       </apex:inputCheckbox>
                    </apex:facet>
                    <apex:inputCheckbox value="{!oi.isSelected}" id="checkedone" onclick="uncheckAll();"></apex:inputCheckbox>
                </apex:column>
                
                <!-- rh 15 oct 2016 I-240239; rearranged order of columns to show Oracle data instead of SFDC -->
                <apex:column value="{!oi.oli.Order_Number_in_Oracle__c}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Oracle Order#{!IF(sortExpression=='Order_Number_in_Oracle__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Order_Number_in_Oracle__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <apex:column value="{!oi.oli.Oracle_Order_Line_Number__c}"> 
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Oracle Item#{!IF(sortExpression=='Oracle_Order_Line_Number__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Oracle_Order_Line_Number__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <!-- MV 17th Oct. Added Product Code to the page I-239941-->
                <apex:column value="{!oi.oli.PriceBookentry.product2.ProductCode}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Product Code{!IF(sortExpression=='PriceBookentry.product2.ProductCode',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="PriceBookentry.product2.ProductCode" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <apex:column value="{!oi.oli.PriceBookentry.product2.Description}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Product Description{!IF(sortExpression=='PriceBookentry.product2.Description',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="PriceBookentry.product2.Description" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                 <apex:column value="{!oi.oli.PriceBookentry.product2.QIP_Required__c}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="QIP Required{!IF(sortExpression=='PriceBookentry.product2.QIP_Required__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="PriceBookentry.product2.QIP_Required__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <apex:column value="{!oi.oli.PriceBookentry.product2.Tracked_as_Software__c}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Software{!IF(sortExpression=='PriceBookentry.product2.Tracked_as_Software__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="PriceBookentry.product2.Tracked_as_Software__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <apex:column value="{!oi.oli.PriceBookentry.product2.Tracked_as_Asset_in_SFDC__c}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Asset to be Installed{!IF(sortExpression=='PriceBookentry.product2.Tracked_as_Asset_in_SFDC__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="PriceBookentry.product2.Tracked_as_Asset_in_SFDC__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <!--<apex:column value="{!oi.oli.Order.PoNumber}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="PO Number{!IF(sortExpression=='Order.PoNumber',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Order.PoNumber" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>-->
                
                <!-- 11/1/2016 RH I-242315 Removed Description, it is same as Product Name/Description already displayed 
                <apex:column value="{!oi.oli.Description}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Description{!IF(sortExpression=='Description',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Description" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column> -->
                
                <apex:column headerValue="Hold Description" value="{!oi.strDescription}">
                </apex:column>
                
                <apex:column value="{!oi.oli.Status__c}" >
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Status{!IF(sortExpression=='Status__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Status__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>   
                <apex:column value="{!oi.oli.Locked__c}" >
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Locked{!IF(sortExpression=='Status__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Status__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <apex:column value="{!oi.oli.Project_Phase__c}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Phase{!IF(sortExpression=='Project_Phase__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Project_Phase__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
                
                <apex:column value="{!oi.oli.Product_Shipping_Address__c}">
                  <apex:facet name="header">
                    <apex:commandLink action="{!setOrderBy}" value="Shipping Address{!IF(sortExpression=='Product_Shipping_Address__c',IF(sortDirection='ASC','▼','▲'),'')}">
                      <apex:param value="Product_Shipping_Address__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                    </apex:commandLink>
                  </apex:facet>
                </apex:column>
               
                <apex:column >
                    <apex:outputField value="{!oi.oli.Requested_Ship_Date__c}" rendered="{!!isFieldEditable}"/>
                    <apex:inputField value="{!oi.oli.Requested_Ship_Date__c}" rendered="{!isFieldEditable}"/>
                    <apex:facet name="header">
                      <apex:commandLink action="{!setOrderBy}" value="Requested Ship Date{!IF(sortExpression=='Requested_Ship_Date__c',IF(sortDirection='ASC','▼','▲'),'')}">
                        <apex:param value="Requested_Ship_Date__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                      </apex:commandLink>
                    </apex:facet>
                </apex:column>
                
                <apex:column >
                    <apex:inputField value="{!oi.oli.Approval_Status__c}" rendered="{!isFieldEditable}"/>
                    <apex:outputField value="{!oi.oli.Approval_Status__c}" rendered="{!!isFieldEditable}"/>
                    <apex:facet name="header">
                      <apex:commandLink action="{!setOrderBy}" value="Approval Status{!IF(sortExpression=='Approval_Status__c',IF(sortDirection='ASC','▼','▲'),'')}">
                        <apex:param value="Approval_Status__c" name="column" assignTo="{!sortExpression}" ></apex:param>
                      </apex:commandLink>
                    </apex:facet>
                </apex:column>
                
                <apex:column >
                    
                    <c:HelpIcon helpText="{!oi.oli.Callout_Result__c}" rendered="{!IF(oi.oli.Callout_Result__c != null, true, false)}"/>
                    <apex:facet name="header">
                      <apex:outputText value="Error"/>
                    </apex:facet>
                </apex:column>
                <!--<apex:column headerValue="Ship Date" value="{!oi.oli.Shipped_Date__c}"/>-->
            </apex:pageBlockTable>             
        </apex:pageBlock>   
    </apex:form>
    
    
    <script type="text/javascript">

    var checkAll = function(cb){
       var inputElem = document.getElementsByTagName("input");
       for(var i=0; i<inputElem.length; i++){
           if(inputElem[i].id.indexOf("checkedone") != -1 && inputElem[i].disabled == false){
             inputElem[i].checked = cb.checked;
           }
       }
    }
       
    /* Uncheck CheckAll checkbox if any one of checkboxes are unchecked*/
    var uncheckAll = function(){
      var inputElem = document.getElementsByTagName("input");
      var flag = true; 
      var checkAll; 
      for(var i=0; i<inputElem.length; i++){
        if(inputElem[i].id.indexOf("checkAllDone") != -1){
         checkAll = inputElem[i];
        }
        if(inputElem[i].id.indexOf("checkedone") != -1 && !inputElem[i].checked){
          flag = false;
          break; 
        }                
      }
      checkAll.checked = flag;
    }
        
    </script>
</apex:page>