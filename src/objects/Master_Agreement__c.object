<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stryker Flex: Object to store and track Master Agreements for a customer Account.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Master Agreements</relationshipLabel>
        <relationshipName>Master_Agreements</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Agreement_Type__c</fullName>
        <description>Flex Master Agreement Type</description>
        <externalId>false</externalId>
        <label>Agreement Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Master Agreement</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Master Lease Agreement</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Master Fee Per Disposable Agreement</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Master Rental Agreement</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Master Fee Per Implant Agreement</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Unique external-id field to support integrations, migrations.</description>
        <externalId>true</externalId>
        <inlineHelpText>Unique external-id field to support integrations, migrations.</inlineHelpText>
        <label>External ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Master_Agreement_Number__c</fullName>
        <externalId>false</externalId>
        <label>Master Agreement Number</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Master_Agreement_Signed__c</fullName>
        <externalId>false</externalId>
        <label>Master Agreement Signed</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Yes</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type_of_Terms_and_Conditions__c</fullName>
        <externalId>false</externalId>
        <label>Type of Terms and Conditions</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Special</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Non-Standard</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Master Agreement</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Master Agreement Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Master Agreements</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Master_Agreement_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Master_Agreement_Signed__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type_of_Terms_and_Conditions__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Master_Agreement_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Master_Agreement_Signed__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Type_of_Terms_and_Conditions__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
