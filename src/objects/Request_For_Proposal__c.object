<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Request_For_Proposal_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>COMM: T-484348</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>IF(RecordType.Name == &apos;COMM Product RFP&apos; ,Opportunity__r.Account.Name,Account__r.Name)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Region__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.Account.COMM_Region_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Region</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>COMM: Account with filter to GPO RT only (S-341024)</description>
        <externalId>false</externalId>
        <inlineHelpText>Can only select a GPO account</inlineHelpText>
        <label>Account</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Can only select GPO accounts for a Pricing RFP</errorMessage>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>COMM_Corporate_Accounts</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Request For Proposals</relationshipLabel>
        <relationshipName>Request_For_Proposals</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <externalId>false</externalId>
        <label>Business Unit</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>COMM</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Empower</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Endo</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Flex</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Med Surg</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Completion_Date__c</fullName>
        <description>COMM: date when the status changes to Closed</description>
        <externalId>false</externalId>
        <label>Completion Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Due_Date__c</fullName>
        <externalId>false</externalId>
        <label>Due Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Est_Value__c</fullName>
        <externalId>false</externalId>
        <label>Est. Value</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>IsApproved__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM: story S-377017</description>
        <externalId>false</externalId>
        <label>IsApproved</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Issue_Date__c</fullName>
        <description>COMM: Issue Date</description>
        <externalId>false</externalId>
        <label>Issue Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Opportunity_Owner__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.Owner.FirstName +&apos; &apos;+Opportunity__r.Owner.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Opportunity Owner</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Request For Proposals</relationshipLabel>
        <relationshipName>Request_For_Proposals</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Post_Award_Analysis__c</fullName>
        <externalId>false</externalId>
        <label>Post Award Analysis</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Questions_Due_Date__c</fullName>
        <externalId>false</externalId>
        <label>Questions Due Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RFP_Name__c</fullName>
        <description>COMM : I-213501</description>
        <externalId>false</externalId>
        <label>RFP Name</label>
        <length>100</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Request_Date__c</fullName>
        <externalId>false</externalId>
        <label>Request Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Requested</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>WIP</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Questions</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Submitted</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Closed</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Submission_Date__c</fullName>
        <externalId>false</externalId>
        <label>Submission Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Booms</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Lights</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tables</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Integration</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Service</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>5</visibleLines>
    </fields>
    <label>Request For Proposal</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_New_RFPS</fullName>
        <columns>NAME</columns>
        <columns>RFP_Name__c</columns>
        <columns>Account_Name__c</columns>
        <columns>Opportunity_Owner__c</columns>
        <columns>Type__c</columns>
        <columns>Submission_Date__c</columns>
        <columns>Issue_Date__c</columns>
        <columns>Due_Date__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </filters>
        <label>All New RFPs</label>
    </listViews>
    <listViews>
        <fullName>All_Requested_RFPS</fullName>
        <columns>NAME</columns>
        <columns>RFP_Name__c</columns>
        <columns>Account_Name__c</columns>
        <columns>Opportunity_Owner__c</columns>
        <columns>Type__c</columns>
        <columns>Submission_Date__c</columns>
        <columns>Issue_Date__c</columns>
        <columns>Due_Date__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </filters>
        <label>All Requested RFPs</label>
    </listViews>
    <listViews>
        <fullName>All_Submitted_RFPS</fullName>
        <columns>NAME</columns>
        <columns>RFP_Name__c</columns>
        <columns>Account_Name__c</columns>
        <columns>Opportunity_Owner__c</columns>
        <columns>Type__c</columns>
        <columns>Submission_Date__c</columns>
        <columns>Issue_Date__c</columns>
        <columns>Due_Date__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </filters>
        <label>All Submitted RFPs</label>
    </listViews>
    <listViews>
        <fullName>My_Submitted_RFPS</fullName>
        <columns>NAME</columns>
        <columns>RFP_Name__c</columns>
        <columns>Account_Name__c</columns>
        <columns>Opportunity_Owner__c</columns>
        <columns>Type__c</columns>
        <columns>Submission_Date__c</columns>
        <columns>Issue_Date__c</columns>
        <columns>Due_Date__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </filters>
        <label>My Submitted RFPs</label>
    </listViews>
    <listViews>
        <fullName>RFP_Team_Request_For_Proposal</fullName>
        <filterScope>Queue</filterScope>
        <label>RFP Team</label>
        <queue>RFP_Team</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>RFP-{0}</displayFormat>
        <label>Request For Proposal Number</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Request For Proposals</pluralLabel>
    <recordTypeTrackFeedHistory>false</recordTypeTrackFeedHistory>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>COMM_Pricing_RFP</fullName>
        <active>true</active>
        <label>COMM Pricing RFP</label>
        <picklistValues>
            <picklist>Business_Unit__c</picklist>
            <values>
                <fullName>COMM</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Empower</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Endo</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Flex</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Med Surg</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Cancelled</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Closed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>New</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Questions</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Requested</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Submitted</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>WIP</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Booms</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Integration</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lights</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Service</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Tables</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>COMM_Product_RFP</fullName>
        <active>true</active>
        <label>COMM Product RFP</label>
        <picklistValues>
            <picklist>Business_Unit__c</picklist>
            <values>
                <fullName>COMM</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Empower</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Endo</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Flex</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Med Surg</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Cancelled</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Closed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>New</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Questions</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Requested</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Submitted</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>WIP</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Booms</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Integration</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lights</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Service</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Tables</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Account_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Opportunity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RECORDTYPE</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Account_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Opportunity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RECORDTYPE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>RFP_status_cannot_advance_unless_approve</fullName>
        <active>true</active>
        <description>COMM story S-377017. can only change status beyond WIP if apprvoed</description>
        <errorConditionFormula>AND(Not( IsApproved__c )
,Not(ISPICKVAL(Status__c,&apos;New&apos;))

)</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>RFP Must be approved before continuing.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>New_Request_For_Proposal</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Request For Proposal</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/COMM_ConvertButtonForLEX?buttonid=rFProposal&amp;opptyid={!Opportunity.Id}&amp;accid={!Account.Id}&amp;accname={!JSENCODE(Account.Name)}&amp;&amp;opptyname={!JSENCODE(Opportunity.Name)}</url>
    </webLinks>
</CustomObject>
