<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stryker Medical Object  - Asset UMDN Junction</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Asset_UMDN_Unique_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Holds a concatenation value of “Identification Number” + Assets “Serial Number”</description>
        <externalId>true</externalId>
        <inlineHelpText>Holds a concatenation value of “Identification Number” + Assets “Serial Number”</inlineHelpText>
        <label>Asset UMDN Unique Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Asset__c</fullName>
        <description>Stryker Medical  : Get the Id of the Asset to which record is associated.</description>
        <externalId>false</externalId>
        <label>Asset</label>
        <referenceTo>Asset</referenceTo>
        <relationshipLabel>UMDN Related to Asset</relationshipLabel>
        <relationshipName>Asset_UMDN_Junctions_del</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Date_Completed__c</fullName>
        <description>Stryker Medical  : Date for Asset UMDN Notifications to be visible.</description>
        <externalId>false</externalId>
        <label>Date Completed</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>UMDN_Status__c</fullName>
        <description>Stryker Medical  : Indicates if the asset has had any corrections related to the UMDN</description>
        <externalId>false</externalId>
        <label>UMDN Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>UMDN__c</fullName>
        <description>Stryker Medical  : Get the Id of the UMDN to which record is associated.</description>
        <externalId>false</externalId>
        <label>UMDN</label>
        <referenceTo>UMDN__c</referenceTo>
        <relationshipLabel>Assets Related to UMDN</relationshipLabel>
        <relationshipName>Asset_UMDN_Junctions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Asset UMDN Junction</label>
    <nameField>
        <displayFormat>UMDN-{0000}</displayFormat>
        <label>UMDN Record Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Asset UMDN Junctions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
