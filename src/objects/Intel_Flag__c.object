<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stryker NV:  this object stories the incidents where Intel post or comment was flagged.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Disposition__c</fullName>
        <description>Stryker NV</description>
        <externalId>false</externalId>
        <inlineHelpText>Captures the resolved reason populated by the designated Business Admin responsible for reviewing the flagged post/comment.</inlineHelpText>
        <label>Disposition</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Indication for Use</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Product Complaint</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Code of Conduct</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Flagged in Error / Accidentally Flagged</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Entity_Id__c</fullName>
        <description>Field to hold Post Id or Comment Id which is flagged.</description>
        <externalId>false</externalId>
        <label>Entity Id</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Entity_Type__c</fullName>
        <externalId>false</externalId>
        <label>Entity Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Post</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Comment</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Unique external-id field to support integrations, migrations.</description>
        <externalId>true</externalId>
        <inlineHelpText>Unique external-id field to support integrations, migrations.</inlineHelpText>
        <label>External ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Flag_Severity__c</fullName>
        <externalId>false</externalId>
        <label>Flag Severity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>0</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>1</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Intel__c</fullName>
        <description>Stryker NV</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is a required field that associates the Intel Flag with the parent Intel record.</inlineHelpText>
        <label>Intel</label>
        <referenceTo>Intel__c</referenceTo>
        <relationshipLabel>Intel Flag</relationshipLabel>
        <relationshipName>Intel_Flag</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Occurrence_Date__c</fullName>
        <description>Stryker NV</description>
        <externalId>false</externalId>
        <inlineHelpText>Date and time the post/comment was made.</inlineHelpText>
        <label>Occurrence Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Post_Comment__c</fullName>
        <description>Stryker NV</description>
        <externalId>false</externalId>
        <inlineHelpText>This field displays the post or comment that was flagged as potentially inappropriate via mobile intel app.</inlineHelpText>
        <label>Post / Comment</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Posting_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Stryker NV</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup relationship to the user who made initial post or comment.</inlineHelpText>
        <label>Posting User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Intel_Flag</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Resolution_Comment__c</fullName>
        <externalId>false</externalId>
        <label>Resolution Comment</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Resolution_Date__c</fullName>
        <description>Resolution_Date</description>
        <externalId>false</externalId>
        <label>Resolution Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Resolution__c</fullName>
        <description>Stryker NV</description>
        <externalId>false</externalId>
        <inlineHelpText>This field captures the ultimate resolution/action that was taken in response to the flagged intel post/comment.</inlineHelpText>
        <label>Resolution</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Removed post</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Replied to post</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Contacted user offline</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>No action needed</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Intel Flag</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Occurrence_Date__c</columns>
        <columns>Post_Comment__c</columns>
        <columns>NAME</columns>
        <columns>Posting_User__c</columns>
        <columns>Disposition__c</columns>
        <columns>Resolution__c</columns>
        <columns>Resolution_Comment__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Intel__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_null</fullName>
        <columns>Occurrence_Date__c</columns>
        <columns>Post_Comment__c</columns>
        <columns>NAME</columns>
        <columns>Posting_User__c</columns>
        <columns>Disposition__c</columns>
        <columns>Resolution__c</columns>
        <columns>Resolution_Comment__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Intel__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>NAME</field>
            <operation>equals</operation>
        </filters>
        <label>All - null</label>
    </listViews>
    <listViews>
        <fullName>Recently_Flagged_Intel_records</fullName>
        <columns>CREATED_DATE</columns>
        <columns>Occurrence_Date__c</columns>
        <columns>Post_Comment__c</columns>
        <columns>NAME</columns>
        <columns>Posting_User__c</columns>
        <columns>Disposition__c</columns>
        <columns>Resolution__c</columns>
        <columns>Resolution_Comment__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Intel__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>LAST_N_DAYS:7</value>
        </filters>
        <label>Recently Flagged Intel records</label>
    </listViews>
    <nameField>
        <displayFormat>Flag-{0000}</displayFormat>
        <label>Flag ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Intel Flag</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
