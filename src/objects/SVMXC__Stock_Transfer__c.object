<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>SVMXC__SVMX_LaunchHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This object is used to capture transfer of inventory between two locations through hand-pick/delivery. This is used when Field engineers drop by at a warehouse for picking up inventory or when parts are moved between a warehouse and a repair facility both located in the same building.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Acceptance_Status__c</fullName>
        <externalId>false</externalId>
        <label>Acceptance Status</label>
        <picklist>
            <picklistValues>
                <fullName>Accepted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not Accepted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rejected</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Callout_Result__c</fullName>
        <externalId>false</externalId>
        <label>Callout Result</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Integration_Status__c</fullName>
        <description>Used by Integration batch process to determine new &amp; synced records.</description>
        <externalId>false</externalId>
        <label>Integration Status</label>
        <picklist>
            <picklistValues>
                <fullName>New</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Success</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>InProgress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Failed</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Reject_Reason__c</fullName>
        <externalId>false</externalId>
        <label>Reject Reason</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>ReqByTechEmail__c</fullName>
        <description>Requested By Technican Email</description>
        <externalId>false</externalId>
        <label>ReqByTechEmail</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ReqFromTechEmail__c</fullName>
        <description>Requested From Technican Email</description>
        <externalId>false</externalId>
        <label>ReqFromTechEmail</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__Additional_Information__c</fullName>
        <deprecated>false</deprecated>
        <description>Any additional information relevant to this stock transfer.</description>
        <externalId>false</externalId>
        <inlineHelpText>Any additional information relevant to this stock transfer..</inlineHelpText>
        <label>Additional Information</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SVMXC__Destination_Location__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Physical Location where stock is transfered to. This is a lookup to an existing site record.</description>
        <externalId>false</externalId>
        <inlineHelpText>Physical Location where stock is transfered to. This is a lookup to an existing site record.</inlineHelpText>
        <label>Destination Location</label>
        <referenceTo>SVMXC__Site__c</referenceTo>
        <relationshipLabel>Stock Transfer (Destination Location)</relationshipLabel>
        <relationshipName>Stock_Transfer_To</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__IsPartnerRecord__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Flag indicates if transaction is for/ by a Partner or not.</description>
        <externalId>false</externalId>
        <inlineHelpText>Flag indicates if transaction is for/ by a Partner or not.</inlineHelpText>
        <label>IsPartnerRecord</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SVMXC__IsPartner__c</fullName>
        <deprecated>false</deprecated>
        <description>Flag indicates if transaction is for/ by a Partner or not.</description>
        <externalId>false</externalId>
        <formula>IF( SVMXC__IsPartnerRecord__c ,&quot;True&quot;, &quot;False&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Flag indicates if transaction is for/ by a Partner or not.</inlineHelpText>
        <label>IsPartner</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__Not_Posted_Item_Count__c</fullName>
        <deprecated>false</deprecated>
        <description>Count of line items that are not posted.</description>
        <externalId>false</externalId>
        <inlineHelpText>Count of line items that are not posted.</inlineHelpText>
        <label>Not Posted Item Count</label>
        <summaryFilterItems>
            <field>SVMXC__Stock_Transfer_Line__c.SVMXC__Posted_To_Inventory__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>SVMXC__Stock_Transfer_Line__c.SVMXC__Stock_Transfer__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>SVMXC__Partner_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Account, set by trigger</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to Partner Account.</inlineHelpText>
        <label>Partner Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Stock Transfer</relationshipLabel>
        <relationshipName>Stock_Transfer</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Partner_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Contact, set by trigger</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to Partner Contact.</inlineHelpText>
        <label>Partner Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Stock Transfer</relationshipLabel>
        <relationshipName>Stock_Transfer</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Source_Location__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Physical Location from where stock is transfered. This is a lookup to an existing site record.</description>
        <externalId>false</externalId>
        <inlineHelpText>Physical Location from where stock is transfered. This is a lookup to an existing site record.</inlineHelpText>
        <label>Source Location</label>
        <referenceTo>SVMXC__Site__c</referenceTo>
        <relationshipLabel>Stock Transfer (Source Location)</relationshipLabel>
        <relationshipName>Stock_Transfer_From</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Stock Transfer</label>
    <listViews>
        <fullName>SVMXC__All_Stock_Transfers</fullName>
        <columns>NAME</columns>
        <columns>SVMXC__Source_Location__c</columns>
        <columns>SVMXC__Destination_Location__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All Stock Transfers</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>SXFR-{00000000}</displayFormat>
        <label>Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Stock Transfer</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>SVMXC__Source_Location__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SVMXC__Destination_Location__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Source_Location__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Destination_Location__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Source_Location__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Destination_Location__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>SVMXC__Destination_Location__c</searchFilterFields>
        <searchFilterFields>SVMXC__Source_Location__c</searchFilterFields>
        <searchFilterFields>CREATEDBY_USER</searchFilterFields>
        <searchResultsAdditionalFields>SVMXC__Source_Location__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SVMXC__Destination_Location__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>SVMXC__SVMXC_ValidateDestinationStockable</fullName>
        <active>true</active>
        <description>Destination location should be a stocking location.</description>
        <errorConditionFormula>( SVMXC__Destination_Location__r.SVMXC__Stocking_Location__c = false)</errorConditionFormula>
        <errorDisplayField>SVMXC__Destination_Location__c</errorDisplayField>
        <errorMessage>Destination location is not a stocking location. Please select stocking locations only.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>SVMXC__SVMXC_ValidateSourceStockable</fullName>
        <active>true</active>
        <description>Source location should be a stocking location.</description>
        <errorConditionFormula>( SVMXC__Source_Location__r.SVMXC__Stocking_Location__c = false)</errorConditionFormula>
        <errorDisplayField>SVMXC__Source_Location__c</errorDisplayField>
        <errorMessage>Source location is not a stocking location. Please select stocking locations only.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>SVMXC__SVMXC_ValidateTransferLocations</fullName>
        <active>true</active>
        <description>Source and destination locations cannot be the same.</description>
        <errorConditionFormula>( SVMXC__Source_Location__c =  SVMXC__Destination_Location__c)</errorConditionFormula>
        <errorDisplayField>SVMXC__Source_Location__c</errorDisplayField>
        <errorMessage>Source and destination locations cannot be the same.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>SVMXC__Add_Lines</fullName>
        <availability>online</availability>
        <description>Launch Screen to add Stock Transfer Lines</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Create/Edit Lines</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR($Site.Prefix+&apos;/apex/SVMXC__ServiceMaxConsole?SVMX_recordId=&apos;+CASESAFEID(SVMXC__Stock_Transfer__c.Id)+&apos;&amp;SVMX_processId=TDM013&amp;SVMX_retURL=&apos;+URLFOR($Site.Prefix+&apos;/&apos;+CASESAFEID(SVMXC__Stock_Transfer__c.Id)))}</url>
    </webLinks>
    <webLinks>
        <fullName>SVMXC__Post_To_Inventory</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Post To Inventory</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR($Site.Prefix+&apos;/apex/SVMXC__INVT_SmartEngine?hdrId=&apos;+SVMXC__Stock_Transfer__c.Id+&apos;&amp;SMid=SXFR001&apos;)}</url>
    </webLinks>
</CustomObject>
