<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Site_Visit_Record_Page2</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>COMM T-484350</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Background__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Account Background</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Additional_Expenses__c</fullName>
        <description>COMM T-502659</description>
        <externalId>false</externalId>
        <label>Additional Expenses</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Additional_Info__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Additional Info</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Address__c</fullName>
        <description>COMM I-240198</description>
        <externalId>false</externalId>
        <label>Location</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Air_Travel_Costs__c</fullName>
        <description>COMM T-502659</description>
        <externalId>false</externalId>
        <label>Air Travel Costs</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Approval_Status__c</fullName>
        <description>COMM T-502657</description>
        <externalId>false</externalId>
        <label>Approval Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Not Submitted</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Submitted for Approval</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Booms_Lights__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM I-220353</description>
        <externalId>false</externalId>
        <label>Booms &amp; Lights</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Car_Service__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM I-220353</description>
        <externalId>false</externalId>
        <label>Car Service</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Cell_Phone__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Cell Phone</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Comm_Rep_Oppty__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;/&apos;&amp;Opportunity__r.Owner.Id, Opportunity__r.Owner.FirstName &amp; &apos; &apos; &amp; Opportunity__r.Owner.LastName)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Comm Rep</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Competition__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Competition</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Desired_Presenter1__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Desired_Presenter2__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Desired_Presenter3__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Desired_Presenter4__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Desired_Presenter5__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Desired_Presenter6__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Desired_Presenter7__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Desired_Presenter__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Desired Presenter</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Endoscopy_Equipment__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM I-220353</description>
        <externalId>false</externalId>
        <label>Endoscopy Equipment</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Facility_Tour__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Facility Tour?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hospital_Name__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <formula>Opportunity__r.Account.Name</formula>
        <label>Hospital Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Hot_Buttons__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>&quot;Hot Buttons&quot;</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Hotel_Costs__c</fullName>
        <description>COMM T-502659</description>
        <externalId>false</externalId>
        <label>Hotel Costs</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Integration__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM I-220353</description>
        <externalId>false</externalId>
        <label>Integration</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Knowledge_of_Account__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Knowledge of Account</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Meal_Costs__c</fullName>
        <description>COMM T-502659, COMM I-220353</description>
        <externalId>false</externalId>
        <label>Meal Costs</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Number_Of_Clients__c</fullName>
        <description>COMM T-502659</description>
        <externalId>false</externalId>
        <label>Number Of Clients</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <description>COMM T-484350</description>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Site Visits</relationshipLabel>
        <relationshipName>Site_Visits</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Project_Scope__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Project Scope</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Project_Timeline__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Project Timeline</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Region__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <formula>Opportunity__r.Account.COMM_Region_Name__c</formula>
        <label>Region</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Rental_Car__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM I-220353</description>
        <externalId>false</externalId>
        <label>Rental Car</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Site_Visit_Date__c</fullName>
        <externalId>false</externalId>
        <label>Site Visit Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Specific_Site_Visit_Goals__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Specific Site Visit Goals</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Tables__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM I-220353</description>
        <externalId>false</externalId>
        <label>Tables</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Time1__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time2__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time3__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time4__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time5__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time6__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time7__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Time</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Top_3_Concerns__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Top 3 Concerns</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Topic1__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic2__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic3__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic4__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic5__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic6__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic7__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topic__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Topics_to_Avoid__c</fullName>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>Topics to Avoid</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Total_Cost__c</fullName>
        <description>COMM T-502659</description>
        <externalId>false</externalId>
        <formula>Air_Travel_Costs__c  +  Additional_Expenses__c  +  Hotel_Costs__c  +  Transportation_Expenses__c +   Meal_Costs__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Transportation_Expenses__c</fullName>
        <description>COMM T-502659</description>
        <externalId>false</externalId>
        <label>Transportation Expenses</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>iSuite_Tour__c</fullName>
        <defaultValue>false</defaultValue>
        <description>COMM T-502656</description>
        <externalId>false</externalId>
        <label>iSuite Tour?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Site Visit</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <sharedTo>
            <roleAndSubordinates>Stryker_COMM_Future_State</roleAndSubordinates>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>All_Site_Visits_Approved</fullName>
        <columns>NAME</columns>
        <columns>Opportunity__c</columns>
        <columns>Site_Visit_Date__c</columns>
        <columns>Top_3_Concerns__c</columns>
        <columns>Total_Cost__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </filters>
        <label>All Site Visits Approved</label>
        <sharedTo>
            <roleAndSubordinates>Stryker_COMM_Future_State</roleAndSubordinates>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>All_Site_Visits_Submitted_for_Approval</fullName>
        <columns>NAME</columns>
        <columns>Opportunity__c</columns>
        <columns>Site_Visit_Date__c</columns>
        <columns>Top_3_Concerns__c</columns>
        <columns>Total_Cost__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Approval_Status__c</field>
            <operation>equals</operation>
            <value>Submitted for Approval</value>
        </filters>
        <label>All Site Visits Submitted for Approval</label>
        <sharedTo>
            <roleAndSubordinates>Stryker_COMM_Future_State</roleAndSubordinates>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>My_Site_Visits_Submitted_for_Approval</fullName>
        <columns>NAME</columns>
        <columns>Opportunity__c</columns>
        <columns>Site_Visit_Date__c</columns>
        <columns>Top_3_Concerns__c</columns>
        <columns>Total_Cost__c</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>Approval_Status__c</field>
            <operation>equals</operation>
            <value>Submitted for Approval</value>
        </filters>
        <label>My Site Visits Submitted for Approval</label>
        <sharedTo>
            <roleAndSubordinates>Stryker_COMM_Future_State</roleAndSubordinates>
        </sharedTo>
    </listViews>
    <nameField>
        <label>Site Visit Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Site Visits</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>New_Site_Visit</fullName>
        <availability>online</availability>
        <description>COMM T-502656
Coverted to URL for T-509671</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Site Visit</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/apex/COMM_SiteVisitAddressLookup?{!$Setup.COMM_Constant_Setting__c.Oppty_Field_Id_on_Site_Visit__c}={!Opportunity.Id}&amp;sfdc.override=1</url>
    </webLinks>
    <webLinks>
        <fullName>Report_Comm_Admin_Reports_COMM_Site_Visits_grouped_by_Opportunity</fullName>
        <availability>online</availability>
        <displayType>link</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Report: Comm Admin Reports - COMM Site Visits grouped by Opportunity</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>.</url>
    </webLinks>
</CustomObject>
