<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fieldSets>
        <fullName>ActiveChairsFieldSet</fullName>
        <description>Contains fields that are shown for Active Chair Fellowship members</description>
        <displayedFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Contact__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Primary__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Type__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>ActiveChairsFieldSet</label>
    </fieldSets>
    <fieldSets>
        <fullName>EnrolledMembersFieldSet</fullName>
        <description>Contains field which are displayed for Enrolled Members Fellowship members page.</description>
        <displayedFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Contact__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>EnrolledMembersFieldSet</label>
    </fieldSets>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK( Fellowship__r.Account__r.Id , Fellowship__r.Account__r.Name, &quot;_top&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Chair__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Chair</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Fellowship Members (Chair)</relationshipLabel>
        <relationshipName>Fellowship_Members1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Fellowship Members</relationshipLabel>
        <relationshipName>Fellowship_Members</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <externalId>false</externalId>
        <formula>Contact__r.Email</formula>
        <label>Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>Created as per T-538446. Holds the value for End Date.</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Unique external-id field to support integrations, migrations.</description>
        <externalId>true</externalId>
        <inlineHelpText>Unique external-id field to support integrations, migrations.</inlineHelpText>
        <label>External ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Fellowship__c</fullName>
        <externalId>false</externalId>
        <label>Fellowship</label>
        <referenceTo>Fellowship__c</referenceTo>
        <relationshipLabel>Fellowship Members</relationshipLabel>
        <relationshipName>Fellowship_Members</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Phone__c</fullName>
        <externalId>false</externalId>
        <formula>Contact__r.Phone</formula>
        <label>Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Primary__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Primary</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>Created as per T-538446. Holds the value for start Date.</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Enrolled</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Graduate</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Non-Graduate</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Fellow</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Chair</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Fellowship Member</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>FM-{0000}</displayFormat>
        <label>Fellowship Member Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Fellowship Members</pluralLabel>
    <recordTypes>
        <fullName>Fellows</fullName>
        <active>true</active>
        <label>Fellows</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Enrolled</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Graduate</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Non-Graduate</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Fellow</fullName>
                <default>true</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Fellowship_Chairs</fullName>
        <active>true</active>
        <label>Fellowship Chairs</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Chair</fullName>
                <default>true</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <searchResultsCustomButtons>Add_Membership</searchResultsCustomButtons>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>Add_Membership</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Add Membership</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/setup/ui/recordtypeselect.jsp?ent={!$Setup.Fellowship_Member_Ids__c.Fellowship_Member_Object_ID__c}&amp;retURL={!Contact.Id}&amp;save_new_url={!$Setup.Fellowship_Member_Ids__c.Fellowship_Member_Object_Prefix__c}/e?CF{!$Setup.Fellowship_Member_Ids__c.Fellowship_Member_Contact_Id__c}={!Contact.Name}&amp;CF{!$Setup.Fellowship_Member_Ids__c.Fellowship_Member_Contact_Id__c}_lkid={!Contact.Id}&amp;retURL={!Contact.Id}</url>
    </webLinks>
</CustomObject>
