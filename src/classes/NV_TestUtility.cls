//
// (c) 2015 Appirio, Inc.
//
// Apex Class Name: NV_TestUtility
// Description: This is a utility class is create test data.
//
// 21st December 2015   Hemendra Singh Bhati   Modified (Task # T-459199) - Added Method: createOrderInvoice()
//                                                                        - Added Method: createNVFollow()
//
public class NV_TestUtility {
	
	public static Account createAccount(String name, Id recordTypeId, Boolean isInsert) {
		Account record = new Account( Name = name,
										RecordTypeId = recordTypeId);
		if(isInsert) insert record;
		return record;
	}

  public static Contact createContact(String firstname,String lastname, Id accountId, Boolean isInsert) { 
    Contact record = new Contact( FirstName = firstname ,LastName = lastname,
                    AccountId = accountId);
    if(isInsert) insert record;
    return record;
  }

	public static Contract createContract(Id accountId, Boolean isInsert) {
		Contract record = new Contract(StartDate = Date.newInstance(Date.today().year(), 1, 1),
											ContractTerm = 12, Status = 'Draft', AccountId = accountId,
											External_Integration_Id__c = Math.random()+ '123');
		if(isInsert) insert record;
		return record;		
	}

	public static Order createOrder(Id accountId, Id contractId, 
										Id recordTypeId, Boolean isInsert){
		Order record = new Order(AccountId = accountId, 
									ContractId = contractId,
									RecordTypeId = recordTypeId);
		if(isInsert) insert record;
		return record;
	}

  /*
  @method      : createOrderInvoice
  @description : This method is used to create a new order invoice record.
  @params      : Id orderId, Date invoiceDueDate, String invoiceClass, String invoiceType, String oracleInvoiceNumber, Boolean isInsert
  @returns     : Order_Invoice__c theRecord
  */
  public static Order_Invoice__c createOrderInvoice(
    Id orderId,
    Date invoiceDueDate,
    String invoiceClass,
    String invoiceType,
    String oracleInvoiceNumber,
    Boolean isInsert
  ) {
    Order_Invoice__c theRecord = new Order_Invoice__c(
      Order__c = orderId,
      Invoice_Due_Date__c = invoiceDueDate,
      Invoice_Class__c = invoiceClass,
      Invoice_Type__c = invoiceType,
      Oracle_Invoice_Number__c = oracleInvoiceNumber
    );
    if(isInsert) {
      insert theRecord;
    }
    return theRecord;
  }

  /*
  @method      : createNVFollow
  @description : This method is used to create a new NV follow record.
  @params      : String theRecordId, String theRecordType, Id theFollowingUserId, Boolean isInsert
  @returns     : NV_Follow__c theRecord
  */
  public static NV_Follow__c createNVFollow(String theRecordId, String theRecordType, Id theFollowingUserId, Boolean isInsert) {
    NV_Follow__c theRecord = new NV_Follow__c(
      Record_Id__c = theRecordId,
      Record_Type__c = theRecordType,
      Following_User__c = theFollowingUserId
    );
    if(isInsert) {
      insert theRecord;
    }
    return theRecord;
  }

	public static User createUser(String userName, String email, 
									Id profileId, Id roleId,
									String uSuffix, Boolean isInsert){
      User testUser = new User();
      testUser.ProfileId = profileId;
      if(roleId!=null) testUser.UserRoleId = roleId;
      testUser.FirstName = 'FName'+uSuffix;
      testUser.LastName = 'LName' + uSuffix;
      testUser.Username = userName;
      testUser.Email = email;
      testUser.Alias = uSuffix;
      testUser.CommunityNickname = 'testUser' + uSuffix;
      testUser.LanguageLocaleKey = 'en_US';
      testUser.LocaleSidKey = 'en_US';
      testUser.EmailEncodingKey = 'UTF-8';
      testUser.TimeZoneSidKey = 'America/Los_Angeles';
      testUser.IsActive = true;
	    if(isInsert) insert testUser;
	    return testUser;
  	}

  	public static Product2 createProduct(String productName, String catalogNo, Id recordTypeId, Boolean isInsert){
  		Product2 record = new Product2(Name = productName, Catalog_Number__c = catalogNo,
  										RecordTypeId = recordTypeId);
  		if(isInsert) insert record;
  		return record;
  	}

  	public static Pricebook2 createPricebook(String name, Boolean isActive, Boolean isInsert){
  		Pricebook2 record = new Pricebook2(Name=name, isActive=isActive);
        if(isInsert) insert record;
  		return record;
  	}

    public static PricebookEntry createPricebookEntry(Id priceBookId, Id productId, Decimal unitPrice, 
                              Boolean isActive, Boolean isInsert){
      PricebookEntry record = new PricebookEntry(
            Pricebook2Id = priceBookId, Product2Id = productId,
            UnitPrice = unitPrice, IsActive = isActive);
      if(isInsert) insert record;
      return record;
    }


  	public static PricebookEntry addPricebookEntries(Id priceBookId, Id productId, Decimal unitPrice, 
  														Boolean isActive, Boolean isInsert) {
        
      Id stdPricebookId = Test.getStandardPricebookId();
      
      // 1. Insert a price book entry for the standard price book.
      // Standard price book entries require the standard price book ID we got earlier.
      PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = stdPricebookId, Product2Id = productId,
          UnitPrice = unitPrice, IsActive = true);
      insert standardPrice;
      
      // 2. Insert a price book entry with a custom price.
      PricebookEntry record = new PricebookEntry(
          Pricebook2Id = priceBookId, Product2Id = productId,
          UnitPrice = unitPrice, IsActive = isActive);
      if(isInsert) insert record;
		  return record;
    }

    public static Notification_Settings__c createNotificationSettings(Id userId, Boolean isInsert){
      Notification_Settings__c record = new Notification_Settings__c(User__c = userId);
      if(isInsert) insert record;
      return record;
    }
}