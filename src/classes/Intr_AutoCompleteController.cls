//
// (c) 2015 Appirio, Inc.
//
// Apex Controller for Visualforce component Intr_AutoComplete 
//
// This Component is using by CustomLookUpCtrl Component.
//
// Created by : Shreerath Nair (Appirio)

global class Intr_AutoCompleteController {
    
     
    
    @RemoteAction
    global static SObject[] findSObjects(string obj, string qry, string addFields,String targetProfile,String ExtraProfileIds) {
        
        //I-227667(SN) Start
        Id profileId;
        List<String> lstExtraProfiles;
        if(!String.isBlank(ExtraProfileIds)){
            lstExtraProfiles = ExtraProfileIds.split(',');
        }

        if(!String.isBlank(targetProfile)){
            profileId = targetProfile;
        }else{
            Profile medicalProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Medical - Call Center'].get(0);
            profileId = medicalProfile.Id;
        }
        
        if(obj ==  'User') {
        Set<Id> setOwnerId =new Set<Id>();
            for(Event e :[SELECT Id, OwnerId FROM Event WHERE ShowAs = 'OutOfOffice' AND EndDateTime >= :DateTime.Now()]) {
                System.debug('@@@@###' + e.OwnerId);
            setOwnerId.add(e.OwnerId);
            }
        }   
        
        //I-227667(SN) Stop
        
        // more than one field can be passed in the addFields parameter
        // split it into an array for later use
        List<String> fieldList;
        if (String.isNotBlank(addFields)) fieldList = addFields.split(',');
       // check to see if the object passed is valid
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) {
            // Object name not valid
            return null;
        }
        // create the filter text
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        //begin building the dynamic soql query
        String soql = 'select id, Name';
        // if an additional field was passed in add it to the soql
        if (fieldList != null) {
            for (String s : fieldList) {
                soql += ', ' + s;
            }
        }
        // add the object and filter by name to the soql
        soql += ' from ' + obj + ' where name' + filter;
        
        //I-227667(SN) Start
        if(obj == 'User'){
            soql+= ' AND Id NOT IN : setOwnerId AND (ProfileId in :lstExtraProfiles OR ProfileId = :profileId)  AND IsActive = true ';
        }
        //I-227667(SN) Start
        
        // add the filter by additional fields to the soql
        if (fieldList != null) {
            for (String s : fieldList) {
                soql += ' or ' + s + filter;
            }
        }
        soql += ' order by Name limit 20';
        System.debug('shree>>>'+soql);
        List<sObject> L = new List<sObject>();
        try {
            L = Database.query(soql);
        }
        catch (QueryException e) {
            return null;
        }
        return L;
   }
}