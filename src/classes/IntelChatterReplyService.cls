/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 20, 2013
 Purpose       :  REST Webservice for saving Intel tags and Chatter replies.
 Reference     :  Task T-205633
 Modifications :  Issue I-87335    Dec 20, 2013    Sonal Shrivastava
*******************************************************************************/
@RestResource(urlMapping='/IntelChatterReplyService/*') 
global with sharing class IntelChatterReplyService {
  
  private static final String ERROR = 'Error : ';
  private static final String INVALID_DATA = 'Invalid Data';
  private static final String SAVE_SUCCESS = 'Records saved successfully';
  private static final String NO_RECORDS_FOUND = 'No records found';
  
  //****************************************************************************
  // POST Method for REST service
  //****************************************************************************  
  @HttpPost 
  global static Map<String, String> doPost() {
    RestRequest req = RestContext.request;
    Map<String, String> res = new Map<String,String>();
    String result;    
    try {   
      //parse the requestBody into a string
      if(req.requestBody != null) {                
        Blob reqBody = req.requestBody;   
        result = parseRecords(reqBody.toString());      
      } 
      else {
        result = INVALID_DATA;
      }
    }
    catch(Exception ex){
      res.put('error', ex.getMessage());
      res.put('requestBody', req.requestBody.toString());
      return res;
    }    
    res.put('response', result);
    return res;
  }
  
  //****************************************************************************
  // Method for getting tag and comment records from the Request string
  //****************************************************************************
  private static string parseRecords(String reqBody){
    
    IntelChatterReplyWrapper wrap = (IntelChatterReplyWrapper)JSON.deserialize(reqBody, IntelChatterReplyWrapper.class);
    
    String result;
    Map<String, Intel__c> mapIntelId_Intel = new Map<String, Intel__c>();
    Map<String, List<FeedComment>> mapIntelId_commentList = new Map<String, List<FeedComment>>(); 
    Map<String, List<Tag__c>> mapIntelId_TagList = new Map<String, List<Tag__c>>();
    
    try {
    	if(wrap != null && wrap.intelReplyList != null && wrap.intelReplyList.size() > 0){
    		
    		for(IntelReplyJSON intelwrap : wrap.intelReplyList){
    			if(intelWrap.intelId != null && intelWrap.intelId != ''){
    				
	    			//Add intel ids in a map as key and list of comments as value
	    			if(!mapIntelId_commentList.containsKey(intelWrap.intelId)){
	    				mapIntelId_commentList.put(intelWrap.intelId, new List<FeedComment>());
	    			}
	    			if(intelwrap.text != null && intelwrap.text != ''){
	    				FeedComment comment = new FeedComment();
	    				comment.CommentBody = intelWrap.text;    				
	    				if(intelWrap.postedById != null && intelWrap.postedById != ''){ 
	    				  comment.CreatedById = intelWrap.postedById;
	    				}    				
	    				mapIntelId_commentList.get(intelWrap.intelId).add(comment);
	    			}
	    			
	    			//Add Tag records List in a map 
	          if((intelwrap.tags != null) && (intelwrap.tags.size() > 0)){
	            Tag__c tag;
	            for(TagJson tagWrap : intelwrap.tags){
	              tag = new Tag__c(Account__c = tagWrap.Account,
	                               Contact__c = tagWrap.Contact,
	                               Product__c = tagWrap.Product,
	                               User__c    = tagWrap.User,
	                               Intel__c   = intelWrap.intelId );
	              if(!mapIntelId_TagList.containsKey(intelWrap.intelId)){
	                mapIntelId_TagList.put(intelWrap.intelId, new List<Tag__c>());
	              } 
	              mapIntelId_TagList.get(intelWrap.intelId).add(tag);                                  
	            }
	          } 
    			}
    		}
    		if(mapIntelId_commentList.keySet().size() > 0){
    			for(Intel__c intel : [SELECT Id, Chatter_Post__c, Flagged_As_Inappropriate__c 
    			                      FROM Intel__c 
    			                      WHERE ID IN : mapIntelId_commentList.keySet()]){
    				mapIntelId_Intel.put(intel.Id, intel);
    			}
    		}
    		result = insertRecords(mapIntelId_Intel, mapIntelId_TagList, mapIntelId_commentList); 
    	}
    	else{
        result = NO_RECORDS_FOUND;
    	}
    }
    catch(Exception ex){
      return (ERROR + ex.getMessage());
    }
    return result;
  }
  
  //***************************************************************************/
  // Method for saving Intel and its child records
  //***************************************************************************/
  private static String insertRecords(Map<String, Intel__c> mapIntelId_Intel,
                                      Map<String, List<Tag__c>> mapIntelId_TagList,
                                      Map<String, List<FeedComment>> mapIntelId_commentList){
    String result; 
    Map<String, String> mapIntelId_TagNames = new Map<String, String>();
    List<Tag__c> tagList                    = new List<Tag__c>();
    List<FeedComment> commentList           = new List<FeedComment>();
    Savepoint sp = Database.setSavepoint();
    try{
	    if(mapIntelId_TagList.size() > 0){
	      for(String intelId : mapIntelId_TagList.keySet()){
	        for(Tag__c tag : mapIntelId_TagList.get(intelId)){
	          tagList.add(tag); 
	        }
	      } 
	      if(tagList.size() > 0){         
	        insert tagList;
	      }          
	      //Query tags for Tag Name
	      for(Tag__c tag : [SELECT Id, Name, Title__c, Intel__c FROM Tag__c WHERE Id IN :tagList order by Name]){
	        if(!mapIntelId_TagNames.containsKey(tag.Intel__c)){
	          mapIntelId_TagNames.put(tag.Intel__c, ' ' + Label.RELATED_TAGS + ' ' + tag.Title__c);
	        }else{
	          mapIntelId_TagNames.put(tag.Intel__c, mapIntelId_TagNames.get(tag.Intel__c) + ', ' + tag.Title__c);
	        }
	      }
	    }
	    
	    //Insert FeedComment records
	    if(mapIntelId_CommentList.size() > 0){
	      for(String intelId : mapIntelId_CommentList.keySet()){
	      	Intel__c intel = mapIntelId_Intel.get(intelId);
	      	
	        if((intel.Flagged_As_Inappropriate__c != true) &&
	           (intel.Chatter_Post__c != null) && (intel.Chatter_Post__c != '')){
	          
	          String relatedTags = mapIntelId_TagNames.containsKey(intelId) ? mapIntelId_TagNames.get(intelId) : '';
	          
		        for(FeedComment comment : mapIntelId_CommentList.get(intelId)){              
		          comment.FeedItemId  = intel.Chatter_Post__c;
		          comment.CommentBody = comment.CommentBody + relatedTags; 
		          commentList.add(comment); 
		        }
	        }
	      } 
	      if(commentList.size() > 0){         
	        insert commentList;
	      }
	    } 
      result = SAVE_SUCCESS;
    }
    catch(Exception ex){
    	Database.rollback(sp);
      throw ex;
    }
    return result;
  }
  
  //***************************************************************************/
  // Wrapper class for chatter reply
  //***************************************************************************/
  public class IntelChatterReplyWrapper{
  	public List<IntelReplyJSON> intelReplyList;
  	
  	public IntelChatterReplyWrapper(){}
  }
  
  public class IntelReplyJSON {
  	public String intelId;
  	public String text;
  	public String postedById;
  	public String postedBy;
  	public List<TagJson> tags;
  	
  	public IntelReplyJSON(){}
  	
  	public IntelReplyJSON(String intelId, String body, String createdById, String createdBy, List<TagJson> tagList){
      this.intelId    = intelId;
      this.text       = body;
      this.postedById = createdById;
      this.postedBy   = createdBy;
      this.tags       = tagList;
  	}
  }
  
  public class TagJson {
    public String Id;
    public String Name;
    public String Title;
    public String Account;
    public String Contact;
    public String Product;
    public String User;
    public DateTime CreatedDate;
    
    public TagJson(){}
    
    public TagJson(Tag__c tag){
      this.Id          = tag.Id;
      this.Name        = tag.Name;
      this.Title       = tag.Title__c;
      this.Account     = tag.Account__c;
      this.Contact     = tag.Contact__c;
      this.Product     = tag.Product__c;
      this.User        = tag.User__c;
      this.CreatedDate = tag.CreatedDate;
    }
  }
}