@isTest
private class NV_FollowTriggerHandlerTest {

    @testSetup static void setup() {
        List<User> testUsers = new List<User>();
        Profile sysAdminProfile = NV_Utility.getProfileByName('System Administrator');
        for (Integer i=1;i<=3;i++){
            testUsers.add(NV_TestUtility.createUser('testuser_fts'+i+'@mail.com', 'testuser_fts'+i+'@mail.com',
                                                            sysAdminProfile.Id, null, 'ft'+i+'', false));
        }
        insert testUsers;
        
        Map<String, Id> accountRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');
        Account testAccount = NV_TestUtility.createAccount('TestAccount',accountRecordTypes.get('NV_Customer'), true);
        Contract testContract = NV_TestUtility.createContract(testAccount.Id, true);
        testContract.Status = 'Activated';
        update testContract;

        Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
        List<Order> orderList = new List<Order>();
        Order testOrder;
        for(Integer i=0; i<5;i++){
            testOrder = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, 
                                                        orderRecordTypes.get('NV_Standard'), false);
            testOrder.Customer_PO__c = 'PO00001';
            testOrder.EffectiveDate = Date.today();
            testOrder.Status = 'Booked';
            orderList.add(testOrder);
        }
        insert orderList;
    }
    
    @isTest static void testFollowTrigger() {
        List<User> testUsers = [SELECT Id FROM User WHERE Username like '%testuser_fts%'];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
        List<Order> orderList = [SELECT Id FROM Order WHERE AccountId =: testAccount.Id];

        System.assertEquals(0, [SELECT count() FROM NV_Follow__c]);

        List<NV_Follow__c> followRecords = new List<NV_Follow__c>();
        followRecords.add(new NV_Follow__c(Following_User__c = testUsers[0].Id,
                                                    Record_Id__c = String.valueOf(testAccount.Id).substring(0,15)));
        followRecords.add(new NV_Follow__c(Following_User__c = testUsers[1].Id,
                                                    Record_Id__c = testAccount.Id));
        followRecords.add(new NV_Follow__c(Following_User__c = testUsers[1].Id,
                                                    Record_Id__c = orderList[0].Id));
        followRecords.add(new NV_Follow__c(Following_User__c = testUsers[2].Id,
                                                    Record_Id__c = orderList[0].Id));
        insert followRecords;

        //1+5 + 1+5 + 1
        System.assertEquals(13, [SELECT count() FROM NV_Follow__c]);
        
        //Start - Added for Story S-389745
        for(NV_Follow__c  follow: [Select Id ,Record_Id__c,Following_User__c  From NV_Follow__c where Record_Id__c =: testAccount.Id and Following_User__c =: testUsers[1].Id]){
            delete follow;
        }
        System.assertEquals(7, [SELECT count() FROM NV_Follow__c]);
        //End - S-389745

    }
    
}