// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_InstallBaseTriggerHandler
// Description: Handler Class for InstallBaseTrigger. 
//
// April 13 2016, Prakarsh Jain  Original 
//
public class Instruments_InstallBaseTriggerHandler {
  //Method to restrict users to delete or edit records if they are not the owner
  //Prakarsh Jain 13 April 2016 - UAT Issues sheet shared by Devin
  public static void restrictEditDeleteOfRecords(List<Install_Base__c> newList, Map<Id, Install_Base__c> oldMap, Boolean IsDelete){
    if(IsDelete){
      for(Install_Base__c installBase : oldMap.values()){
        if(installBase.ownerId  != UserInfo.getUserId() && UserInfo.getProfileId() != Label.Administrator_Profile_ID){
          installBase.addError(Label.Delete_Permission_Error);
        }
      }
    }
  /*  else{
      for(Install_Base__c installBase : newList){
        if(installBase.ownerId != UserInfo.getUserId() && String.valueOf(UserInfo.getProfileId()).subString(0,15) != Label.Administrator_Profile_ID.subString(0,15)){
          installBase.addError(Label.Edit_Permission_Error);
        }
      }
    } */
  }
    
    public static void shareInstallBaseRecords(List<Install_Base__c> newList , Map<Id,Install_Base__c> oldMap) {
        List<String> newRecord = new List<String>();
        List<String> deleteRecord = new List<String>();
        Set<Id> accId = new Set<Id>();
        for(Install_Base__c installBase : newList) {
            if(oldMap == Null) {
                newRecord.add(installBase.Account__c + '+' + installBase.Id);
                accId.add(installBase.Account__c);
            } /*else if (oldMap != Null && installBase.Account__c != oldMap.get(installBase.Id).Account__c) {
                newRecord.add(installBase.Account__c + '+' + installBase.Id);
                deleteRecord.add(oldMap.get(installBase.Id).Account__c + '+' + installBase.Id);
            }*/
        }
        List<Account> accountList = new List<Account>([SELECT OwnerId FROM Account WHERE Id IN :accId]);
        Map<Id,Id> accountToOwnerMap = new Map<Id,Id>();
        Map<Id,Set<Id>> accountToAccountTeamMember = new Map<Id,Set<Id>>();
        for(Account acc : accountList) {
            accountToOwnerMap.put(acc.Id,acc.OwnerId);
        }
        System.debug('accountToOwnerMap==='+accountToOwnerMap);
        List<AccountTeamMember> accontTeamMemberList = new List<AccountTeamMember>();
        accontTeamMemberList = [Select UserId,AccountId From AccountTeamMember WHERE AccountId IN:accId];
        System.debug('accontTeamMemberList======='+accontTeamMemberList);
        for(AccountTeamMember member : accontTeamMemberList){
            if(!accountToAccountTeamMember.containsKey(member.AccountId)){
                accountToAccountTeamMember.put(member.AccountId,new Set<Id>());
            }
            accountToAccountTeamMember.get(member.AccountId).add(member.UserId);
        }
        System.debug('>>>>>>'+accountToAccountTeamMember);
        if(newRecord.size() > 0) {
            List<Install_Base__Share> installBaseShareList = new List<Install_Base__Share>();
            for(String accKey : newRecord) {
                List<String> tempList = new List<String>();
                tempList = accKey.split('\\+');
                Id accountOwner = accountToOwnerMap.get(templist.get(0));
                if(accountOwner!= UserInfo.getUserId()){
                    installBaseShareList.add(createInstallBaseShare(accountOwner,templist.get(1)));
                }
                Set<Id> teamMembers = new Set<Id>();
                if(accountToAccountTeamMember.containsKey(templist.get(0))){
                    teamMembers = accountToAccountTeamMember.get(templist.get(0));
                    for(Id user : teamMembers){
                            if(user!=accountOwner && user!=UserInfo.getUserId()){
                            installBaseShareList.add(createInstallBaseShare(user,templist.get(1)));
                        }
                    }
                }
            }
            insert installBaseShareList;
        }
   /*     Set<Id> accOwnerIdToDel = new Set<Id>();
        Set<Id> userIdToDel = new Set<Id>();
        if(deleteRecord.size() > 0) {
            for(String delRecord : deleteRecord) {
                List<String> tempList = new List<String>();
                tempList = delRecord.split('\\+');
                accOwnerIdToDel.add(accountToOwnerMap.get(templist.get(0)));
                userIdToDel.add(tempList.get(1));
            }
        }
        List<Install_Base__Share> installBaseDeleteList = new List<Install_Base__Share>();
        if(accOwnerIdToDel.size() > 0 && userIdToDel.size() > 0) {
            installBaseDeleteList =[SELECT Id,ParentId,UserOrGroupId,RowCause 
                                    FROM Install_Base__Share 
                                    WHERE ParentId IN :userIdToDel 
                                    AND UserOrGroupId IN :accOwnerIdToDel 
                                    AND RowCause =:Schema.AccountShare.RowCause.Manual];
                        
        }
        if(installBaseDeleteList.size() > 0) {
            delete installBaseDeleteList;
        } */
    }
    public static Install_Base__Share createInstallBaseShare(Id userId,Id installBaseId){
        Install_Base__Share ibShare = new Install_Base__Share();
        ibShare.UserOrGroupId = userId;
        ibShare.ParentId = installBaseId;
        ibShare.AccessLevel = 'Read';
        ibShare.RowCause = Schema.AccountShare.RowCause.Manual;
        return ibShare;
    }
}