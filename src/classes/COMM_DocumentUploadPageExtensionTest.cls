/**================================================================      
* Appirio, Inc
* Name: COMM_DocumentUploadPageExtensionTest
* Description: Test class of COMM_DocumentUploadPageExtension
* Created Date: 30th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 

@isTest
private class COMM_DocumentUploadPageExtensionTest
{
	static User userRecord;
    static User OppMem1;
    static List<Opportunity> oppList2;

    private static void createData() {
        userRecord = TestUtils.createUser(1, 'System Administrator', false).get(0);
        OppMem1 = TestUtils.createUser(1, 'Strategic Sales Manager - COMM US', false).get(0);
        OppMem1.Email = 'test@test.com';
        insert OppMem1;
        userRecord.ManagerId = OppMem1.Id;
        insert userRecord;
        
        Pricebook2 pricebook = new Pricebook2(Name = 'COMM Price Book');
        insert pricebook;
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('COMM Service Opportunity').getRecordTypeId();
        Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Account acc = TestUtils.createAccount(1, false).get(0);
        acc.RecordTypeId = accRT;
        acc.Endo_Active__c = true;
        acc.COMM_Shipping_Country__c = 'United States';
        acc.COMM_Shipping_Address__c = 'test';
        acc.COMM_Shipping_State_Province__c  = 'Arizona';
        acc.COMM_Shipping_PostalCode__c = '123';
        acc.COMM_Shipping_City__c = 'testcity';
        insert acc;
        List<Opportunity> oppList = TestUtils.createOpportunity(1, acc.Id, false);
        oppList[0].RecordTypeId = rtOpp;
        insert oppList;
        OpportunityTeamMember newOTM = new OpportunityTeamMember();
        newOTM.OpportunityId = oppList[0].Id;
        newOTM.UserId = OppMem1.Id;
        insert newOTM;
        oppList2 = TestUtils.createOpportunity(1, acc.Id, false);
        oppList2[0].RecordTypeId = rtOpp;
        insert oppList2;
        
    }

	@isTest
	static void itShouldCreateAttachment()
	{
		String myString = 'StringToBlob';
		Blob myBlob = Blob.valueof(myString);
		createData();
        Test.startTest();
        	ApexPages.StandardController cntrl = new ApexPages.StandardController(oppList2[0]);
	        COMM_DocumentUploadPageExtension classInstance = new COMM_DocumentUploadPageExtension(cntrl);
	        classInstance.fileName = myString;
	        classInstance.fileBody = myBlob;
	        classInstance.description = myString;
	        classInstance.documenttype = 'text';
	        classInstance.linkToAccount = true;
	        classInstance.processUpload();
	        classInstance.back();
	        system.runAs(TestUtils.createUser(1, 'Chatter Free User', false).get(0)){
	        	classInstance.processUpload();
	        }
        Test.stopTest();
        List<Attachment> attachments=[select id, name from Attachment];
        System.assertEquals(1, attachments.size());

	}
}