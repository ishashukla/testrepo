// 
// (c) 2016 Appirio, Inc.
//
// Name : ForecastYearTriggerHandler 
// Used to populate Email fields on Order object via COMM_OrderTrigger
//
// 30th MAy 2016     Shubham Dhupar      Original(T-504210)
// Set the division field of User to BU of Forecast Year having Comma seprated Value
//
public class ForecastYearTriggerHandler{
  public static Map<ID, Schema.RecordTypeInfo> forecastRtMap;
  public static void afterInsertUpdateDelete(List <Forecast_Year__c > newList, Map <Id,Forecast_Year__c > OldMap) {
        rollUpBusinessUnit(newList,OldMap);
    }
    
    //=================================================================================== 
  // Name         : rollUpBusinessUnit
  // Description  : Used to call populateDivisionField f/n by passing the user2__c of forecast
  // Created Date : 30th MAy 2016
  // Created By   : Shubham Dhupar (Appirio)
  // Task         : T-504210
  //====================================================================================
  public static void rollUpBusinessUnit(List <Forecast_Year__c > newList, Map <Id,Forecast_Year__c > OldMap) {
     forecastRtMap= Schema.SObjectType.Order.getRecordTypeInfosById();
    Boolean isUpdate = oldMap != null ? true : false; // To check for  oldMap is Null
    Boolean isDelete= newList == null ? true : false;  // To check for  newListis Null
    List<Forecast_Year__c> oldList = new List<Forecast_Year__c>();
    if(isDelete) {
      oldList.addAll(oldMap.values());
    }
    List<User> userList = new List<User>();
// Create a Map Which stores Project Plan id(Parent Record id) with order id as key.
    Map<Id,Id> newForecastIdMap = new Map <Id,Id>();
    if(!isDelete){
      for(Forecast_Year__c fore : newList) {
        //updated by Rahul Aeran to evade null pointer exception
        if(fore.User2__c != NULL && forecastRtMap.containsKey(fore.RecordTypeId) && ((forecastRtMap.get(fore.RecordTypeID).getName().contains(Constants.Forecast)))) {
          newForecastIdMap.put(fore.Id, fore.User2__c);
        }
      }
    }else if(isDelete) {
      for(Forecast_Year__c fore : oldList ) {
        //updated by Rahul Aeran to evade null pointer exception
        if(fore.User2__c != NULL && forecastRtMap.containsKey(fore.RecordTypeId) && ((forecastRtMap.get(fore.RecordTypeId).getName().contains(Constants.Forecast)))) {
          newForecastIdMap.put(fore.Id, fore.User2__c);
        }
      }
      
    }
    for(User us : [Select Id,Division
                    From User
                    Where Id in : newForecastIdMap.values()]) {
        userList.add(us);
    }
    populateDivisionField(userList);
  }
  
    //=================================================================================== 
  // Name         : populateDivisionField
  // Description  : Populate DIvision field on User with Comma Seprated values.
  // Created Date : 30th MAy 2016
  // Created By   : Shubham Dhupar (Appirio)
  // Task         : T-504210
  //====================================================================================
  public static void populateDivisionField(List<User> users) {
    List<Id> forecastIds = new List<Id>();
    List<User> userList = new List<User>();
    Map<id,List<Forecast_Year__c>> updatemap = new Map<id,List<Forecast_Year__c>>();
    for(User us: users) {
      forecastIds.add(us.Id);
    }
    for(Forecast_Year__c temp : [select Id,
                                    Business_Unit__c,
                                    User2__c
                                  from Forecast_Year__c
                                  where User2__c IN :forecastIds]) {
      if((forecastRtMap.get(temp.RecordTypeID).getName().contains(Constants.Forecast))) {
        List<Forecast_Year__c> forecasts = updatemap.get(temp.User2__c) == null ?
                                                         new  List<Forecast_Year__c>() :
                                                         updatemap.get(temp.User2__c);
        forecasts.add(temp);
        updatemap.put(temp.User2__c, forecasts);
      }
    }
    Integer i=0;
    for(User us: users) {
      if(updatemap.containsKey(us.Id)) {
        us.Division = '';
        for(Forecast_Year__c ord: updatemap.get(us.Id)) {
          if(ord.Business_Unit__c == null){
            ord.Business_Unit__c = '';
          }
          if(i==0) {
            us.Division += ord.Business_Unit__c ;
            i++;
          }
          else {
            us.Division += + ', ' + ord.Business_Unit__c ;
          }
          
        }
        userList.add(us);
      }
    }  
    update userList;
   }
}