/**================================================================      
* Appirio, Inc
* Name: COMM_SendRQAToTrackwiseTest
* Description: Test class for COMM_SendRQAToTrackwise
* Created Date: 18 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class COMM_SendRQAToTrackwiseTest {
    @isTest static void testCallout() {
    Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();      
    RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                     Object_API_Name__c='Case',
                                     Division__c = 'COMM',
                                     RT_Name__c='COMM US Stryker Field Service Case');
       insert rtMap;
        Account acc = TestUtils.createAccount(1, true).get(0);
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.Trackwise_Status__c = 'Planned';
        caseRecord.RecordTypeId = rtCase;
        insert caseRecord;
        Regulatory_Question_Answer__c rqa = new Regulatory_Question_Answer__c(Case__c = caseRecord.Id);
        insert rqa;
        Test.startTest();
        COMM_SendRQAToTrackwise.callWebService(caseRecord.Id, caseRecord.Trackwise_Status__c);  
        Test.stopTest();
        String status = [SELECT Trackwise_Status__c FROM Case WHERE Id = :caseRecord.Id].Trackwise_Status__c;
        system.assertEquals(status, 'Complete');
    }
}