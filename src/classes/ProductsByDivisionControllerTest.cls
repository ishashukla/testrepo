// (c) 2015 Appirio, Inc.
//
// Class Name: ProductsByDivisionControllerTest 
// Description: Test Class for ProductsByDivisionController class.
// 
// April 13 2016, Isha Shukla  Original 
//
@isTest
private class ProductsByDivisionControllerTest {
    static Quick_Link_Section__c qlsecRecord;
    static BusinessUnitAndPricebookName__c bupRecord;
    static PriceBook2 pricebook2;
    static PriceBookEntry entrystd;
    static PriceBookEntry entrycus;
    static List<Product2> productList;
    @isTest static void testproductsByDivisionController() {
        createData();
        system.assertEquals(1, productList.size());
        PageReference pageRef = Page.ProductsByDivision;
        Test.setCurrentPage(pageRef);
        ProductsByDivision_Controller controller = new ProductsByDivision_Controller();
        controller.allSearchResults = productList;
        controller.next();
        controller.previous();
        Boolean hasNext = controller.hasNext;
        Boolean hasPrevious = controller.hasPrevious;
        Integer pageNumber = controller.pageNumber;
        System.assertEquals(1,controller.allSearchResults.size());
    }
    @isTest static void testSortChar() {
        createData();
        system.assertEquals(1, productList.size());
        PageReference pageRef = Page.ProductsByDivision;
        Test.setCurrentPage(pageRef);
        ProductsByDivision_Controller controller = new ProductsByDivision_Controller();
        controller.allSearchResults = productList;
        controller.sortChar = 'I';
        controller.search();
    }
    @isTest static void testNullCondition() {
        PageReference pageRef = Page.ProductsByDivision;
        Test.setCurrentPage(pageRef);
        ProductsByDivision_Controller controller = new ProductsByDivision_Controller();
    }
    public static void createData() {
      qlsecRecord = new Quick_Link_Section__c(Name = 'IVS BCD Travel',Section__c = 'IVS',URL__c='https://portal.bcdtravel.com/stryker/StrykerLogin/tabid/4940/Default.aspx',System_Name__c='BCD Travel');
      insert qlsecRecord;
      bupRecord = new BusinessUnitAndPricebookName__c(Name='IVS',System_Type__c='Stryker',Price_Book_Name__c='IVS Price Book');
      insert bupRecord;
      productList = TestUtils.createProduct(1, True);
      Id pricebookId = Test.getStandardPricebookId();
      pricebook2 = new PriceBook2(Name='IVS Price Book',isActive = True);
      insert pricebook2;
      entrystd = new PriceBookEntry(Pricebook2Id = pricebookId,Product2Id = productList[0].Id,UnitPrice = 1000,isActive = True);
      insert entrystd;
      entrycus = new PriceBookEntry(Pricebook2Id = pricebook2.Id,Product2Id = productList[0].Id,UnitPrice = 1000,isActive = True);
      insert entrycus;
    }
}