/*
Name                    - OpptyDocumentTriggerHandlerTest
Created By              - Nitish Bansal
Created Date            - 02/25/2016
Purpose                 - T-459652
*/
@isTest
private class OpptyDocumentTriggerHandlerTest
{
    @isTest
    static void itShouldPreventDeletion()
    {
        //Creating a System Admin User
        List<User> userList = TestUtils.createUser(1, 'System Administrator', true);
        List<Document__c> oppDocList = new List<Document__c>() ;
        List<Document__c> delOppDocList = new List<Document__c>() ;
        Document__c oppDoc = new Document__c();
        //Creating an account, opprtunity and multile documents as the Admin user
        system.runAs(userList.get(0)){
            List<Account> accountList = TestUtils.createAccount(1, true);
            TestUtils.createCommConstantSetting();
            List<Opportunity> opportunityList = TestUtils.createOpportunity(1, accountList.get(0).Id, true);
            for(Integer i = 0; i <= 5; i++){
                oppDoc = new Document__c();
                oppDoc.Name = 'Test Opportunity Document' + String.valueOf(i);
                oppDoc.Opportunity__c = opportunityList.get(0).Id;
                oppDoc.Account__c = accountList.get(0).Id;
                oppDoc.Document_type__c = Constants.RESTRICTED_DOCUMENT_TYPE;
                oppDocList.add(oppDoc);
            }
            insert oppDocList;
                
            for(Integer i = 0; i <= 2; i++){
                delOppDocList.add(oppDocList.get(i));
            }

            //deleting multiple records (logged-in user has no role)
            try{
                delete delOppDocList;
            } catch(Exception e){
                Boolean messagefound = false; 
                //Checking that user gets excepted error message
                if(e.getMessage().contains(System.Label.Please_contact_an_Administrator_to_delete_Combined_Proposals)){
                    messagefound = true;
                }
                system.assertEquals(true, messagefound);
            }
        }   
    }

    @isTest
    static void itShouldDeleteShareRecords()
    {
        Set<Id> userIdToDelSet = new Set<Id>();
        Set<Id> purchaseOrderDocIds = new Set<Id>();
        //Creating Users
        List<User> userList = TestUtils.createUser(1, 'System Administrator', false);
        userList.addAll(TestUtils.createUser(1, Constants.COMM_INT_SPL_PROFILE, false));
        insert userList;
        List<Document__c> oppDocList = new List<Document__c>() ;
        List<Document__c> delOppDocList = new List<Document__c>() ;
        Document__c oppDoc = new Document__c();
        //Creating an account, opprtunity and multile documents as the Admin user
        Test.startTest();
            system.runAs(userList.get(0)){
                List<Account> accountList = TestUtils.createAccount(1, true);
                TestUtils.createCommConstantSetting();
                List<Opportunity> opportunityList = TestUtils.createOpportunity(1, accountList.get(0).Id, true);
                for(Integer i = 0; i <= 5; i++){
                    oppDoc = new Document__c();
                    oppDoc.Name = 'Test Opportunity Document' + String.valueOf(i);
                    oppDoc.Opportunity__c = opportunityList.get(0).Id;
                    oppDoc.Account__c = accountList.get(0).Id;
                    oppDoc.Document_type__c = Constants.PO_CONST;
                    oppDocList.add(oppDoc);
                }
                insert oppDocList;
            }
        Test.stopTest();
        List<Profile> intProfile = [SELECT Id, Name FROM Profile WHERE Name =: Constants.COMM_INT_SPL_PROFILE];
        for(User intUser : [Select Id from User Where ProfileId =:intProfile.get(0).Id]){
            userIdToDelSet.add(intUser.Id);   // Set to store ID of users
        }
        for(Document__c newRecord : [Select Id, Document_type__c from Document__c]){
            //storing all document id for those records whose type is 'Purchase Order'
            if(newRecord.Document_type__c != null && newRecord.Document_type__c == Constants.PO_CONST){
              purchaseOrderDocIds.add(newRecord.Id);     //Set to store ID of record
            }
        }
        List<Document__Share> deleteShareRecords = [SELECT Id, ParentId, UserOrGroupId, RowCause 
                                              FROM Document__Share 
                                              WHERE ParentId IN :purchaseOrderDocIds 
                                              AND UserOrGroupId IN :userIdToDelSet];
        system.assertEquals(0, deleteShareRecords.size())                                      ;
    }       

    @isTest
    static void itShouldShareOppDocs()
    {
        //Creating a System Admin User
        List<User> userList = TestUtils.createUser(6, 'System Administrator', true); //NB - 11/9 - I-242624
        List<Document__c> oppDocList = new List<Document__c>() ;
        List<Document__c> delOppDocList = new List<Document__c>() ;
        Document__c oppDoc = new Document__c();
        Custom_Account_Team__c projectMem = new Custom_Account_Team__c();
        List<Custom_Account_Team__c> projectMemList = new List<Custom_Account_Team__c>();
        //Creating an account, opprtunity and multile documents as the Admin user
        system.runAs(userList.get(0)){
            List<Account> accountList = TestUtils.createAccount(1, true);
            TestUtils.createCommConstantSetting();
            List<Opportunity> opportunityList = TestUtils.createOpportunity(2, accountList.get(0).Id, true); //NB - 11/9 - I-242624
            List<Project_Plan__c> projectList = TestUtils.createProjectPlan(1, accountList.get(0).Id, true);
            opportunityList.get(0).Project_Plan__c = projectList.get(0).Id;
            update opportunityList;

            for(Integer i = 0; i <= 5; i++){
                if(i < 4){
                    projectMem = new Custom_Account_Team__c();
                    projectMem.Project__c = projectList.get(0).Id;
                    projectMem.Account_Access_Level__c = 'Read';
                    projectMem.User__c = userList.get(i).Id;
                    //NB - 11/9 - I-242624 Start
                    if(i == 0 || i == 1)
                        projectMem.Team_Member_Role__c = 'Project Manager';
                    if(i == 2)    
                        projectMem.Team_Member_Role__c ='Integration Specialist (3rd Party)';
                    if(i == 3)    
                        projectMem.Team_Member_Role__c = 'Project Engineer';
                    //NB - 11/9 - I-242624 end    
                    projectMem.RecordTypeId = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
                    projectMemList.add(projectMem);
                }
                
                oppDoc = new Document__c();
                oppDoc.Name = 'Test Opportunity Document' + String.valueOf(i);
                oppDoc.Opportunity__c = opportunityList.get(0).Id;
                oppDoc.Account__c = accountList.get(0).Id;
                oppDoc.Document_type__c = 'Other';
                oppDoc.Project_Plan__c = projectList.get(0).Id; //NB - 11/9 - I-242624
                oppDocList.add(oppDoc);
            }
            insert projectMemList;
            insert oppDocList;
            //NB - 11/9 - I-242624 Start
            List<OpportunityTeamMember> opptyTeamMemberList = TestUtils.createOpportunityTeamMember(2,opportunityList.get(0).Id,userList.get(0).Id,false);
            opptyTeamMemberList.get(0).TeamMemberRole = 'Project Engineer';
            opptyTeamMemberList.get(1).TeamMemberRole = 'Project Manager';
            opptyTeamMemberList.get(1).OpportunityId = opportunityList.get(1).Id;
            insert opptyTeamMemberList;
            //NB - 11/9 - I-242624 End
        }   
      
          
        List<Document__Share> docSharerecords = [Select Id from Document__Share where 
                                                rowCause =:Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c];
        system.assertNotEquals(null, docSharerecords);
        //NB - 11/9 - I-242624 Start
        oppDocList[0].Document_type__c = Constants.PO_CONST;
        oppDocList[1].Document_type__c = Constants.PHOENIX_PROPOSAL_CONST;
        oppDocList[2].Document_type__c = 'Purchase Order';
        oppDocList[2].Revised_Document__c = true;
        update oppDocList;
        delete oppDocList;
        //NB - 11/9 - I-242624 end
    }
}