//------------------------------------------------------------------------------
// Project Name..........: Stryker SPS
// File..................: SPS_AccountTriggerHandler>>
// Created by............: gagandeep.brar@stryker.com
// Last Modified by......: gagandeep.brar@stryker.com
// Description......     : Populate Strategic account and Negotiated Contract checkbox based on parent account specified on a account

//Updated By              - Nitish Bansal
//Updated Date            - 02/26/2016
//Purpose                 - T-459647
//-------------------------------------------------------------------------------

public with sharing class SPS_AccountTriggerHandler {

  private static Set<Id> SPS_RecordTypes{
    get{
      if(SPS_RecordTypes == null){
        SPS_RecordTypes = new Set<Id>();
        SPS_RecordTypes.add(Schema.sObjectType.Account.getRecordTypeInfosByName().get('SPS Customer').getRecordTypeId());
      }
      return SPS_RecordTypes;
    }
  }

  	// Methods to Populate Strategic account and Negotiated Contract checkbox based on parent account specified on a account
	// @param oldMap - map to hold Trigger.oldMap 
	// @param newRecords - List to hold Trigger.new 
    // @return Nothing 
	public static void PopulateValuesToChildAccounts(Map<Id, Account> mapOld, List<Account> newValues){
		Set<Id> accountIdSet = new Set<Id>();
		Set<Id> newChildIdSet = new Set<Id>();
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        Set <Id> childAccounts = new Set<Id>();
        List<Account> updateChildAccounts = new List<Account>();
		
			//Create a set of Account
			for(Account acc1 : newValues)
			{
				//In case of record update
				if(mapOld != null){
					if(SPS_RecordTypes.contains(acc1.RecordTypeId) && (acc1.ParentId == mapOld.get(acc1.Id).ParentId)){
						accountIdSet.add(acc1.Id);
						mapAccount.put(acc1.Id, acc1);
					}
					else if (SPS_RecordTypes.contains(acc1.RecordTypeId) && (acc1.ParentId != mapOld.get(acc1.Id).ParentId)){
						newChildIdSet.add(acc1.ParentId);
					}
				} else { //In case of record insert
					if(SPS_RecordTypes.contains(acc1.RecordTypeId) && (acc1.ParentId != null)){
						accountIdSet.add(acc1.Id);
						mapAccount.put(acc1.Id, acc1);
					}
				}
			}
			System.debug('===accountIdSet==='+accountIdSet);
			System.debug('===newChildIdSet==='+newChildIdSet);
			
        	Map<Id, Account> mapNewChildAccount = new Map<Id, Account>();
			if(newChildIdSet.size()>0){
            	//Getting all the Account to populate Strategic account and Negotiated Contract checkbox based on parent account
            	mapNewChildAccount = new Map<Id, Account>([Select Id,  Strategic_Account__c, ParentId, Negotiated_Contract__c, Name, AccountNumber, RecordTypeId from Account where Id in : newChildIdSet]);
        	}
        	System.debug('===mapAccount==='+mapNewChildAccount);
			if(newChildIdSet.size()>0){
		        for(Account acc2 : newValues){  	
		        	if(acc2.Id != null && SPS_RecordTypes.contains(acc2.RecordTypeId)) {
		        		if(acc2.ParentId != null && mapNewChildAccount.get(acc2.ParentId)!=null){
		        			acc2.Strategic_Account__c = mapNewChildAccount.get(acc2.ParentId).Strategic_Account__c;
		            		acc2.Negotiated_Contract__c = mapNewChildAccount.get(acc2.ParentId).Negotiated_Contract__c;
		            		System.debug('===AccountName==='+acc2.Name);
		            		System.debug('===ParentAccountName==='+mapNewChildAccount.get(acc2.ParentId).Name);
		        		}
		        	}
		        }
			}
			
			List<Account> childAccountList = new List<Account>();
			if(accountIdSet.size()>0){
				//Getting all the Account to populate Strategic account and Negotiated Contract checkbox based on parent account
				childAccountList = new List<Account>([Select Id, Strategic_Account__c, ParentId, Negotiated_Contract__c, Name, AccountNumber, RecordTypeId from Account where ParentId in : accountIdSet]);
			}
			System.debug('===mapAccount==='+childAccountList);
			if(childAccountList!=null && childAccountList.size()>0){
				for(Account acc1 : childAccountList)
				{
					if(mapAccount.get(acc1.ParentId) != null && SPS_RecordTypes.contains(acc1.RecordTypeId) ){
						acc1.Strategic_Account__c = mapAccount.get(acc1.ParentId).Strategic_Account__c;
						acc1.Negotiated_Contract__c = mapAccount.get(acc1.ParentId).Negotiated_Contract__c;
					}
					updateChildAccounts.add(acc1);
					system.debug('----acc----'+acc1);
				}
			}
			if(updateChildAccounts.size()>0){
				update updateChildAccounts;
			}
	}
}