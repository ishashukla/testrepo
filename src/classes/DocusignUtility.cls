/**=====================================================================
 * Appirio, Inc
 * Name: DocusignUtility, created for COMM
 * Description: T-491957
 * Created Date: 05/12/2016
 * Created By: Rahul Aeran
 *
=====================================================================*/
global class DocusignUtility {
  static String subject = Label.Docusign_Email_Template_Subject;
  static String body = Label.Docusign_Email_Template_Body;
  static String USER_TYPE = 'User';
  static String CONTACT_TYPE = 'Contact';
  static String CHATTER_TEXT = 'Envelope for [SourceType] "[SourceName]" was completed by [Recipient] in DocuSign.';
  static String DOCUMENT_ID_TEXT = 'Select Document ID';
  static Integer REPEAT_INTERVAL = 10;
  static Integer REMINDER_INTERVAL  = 60;
  static String DOC_TYPE_TEMPLATE = 'DSTemplate';
  static String DOC_TYPE_ATTACHMENT = 'Attachment';
  static String SIGNER = 'Signer';
  static String BASE_URL = '/apex/dsfs__DocuSign_CreateEnvelope?';
  static String ATTACHMENT_NAME = Label.Docusign_Project_Packet_Attachment_Name;
  static String DSE_ID_PARAM = 'DSEID';
  static String SOURCE_ID_PARAM = 'SourceID';
  //Roles for sending emails
  static String CUSTOMER =  'Customer';
  static String PROJECT_MANAGER = 'Stryker PM';
  static String SALES_REP = 'Stryker Sales';
  static String Finance = 'Finance';
  static String usName;
  /*
   webservice static String customersignoffdocument(Id ppId) { 
    ATTACHMENT_NAME = 'Test Document';
    String templateId = 'C52889D7-E532-41CB-B2F2-670547E7DCC7'; 
    //For document id we need to trim the text starting from second last hyphen delimiter
    String documentId =  templateId.substring(0,templateId.subString(0,templateId.lastindexOf(Constants.HYPHEN_DELIMITER)).lastIndexOf(Constants.HYPHEN_DELIMITER));
    String strResponse = '';
    //We will first query all the opportunity documents for our project plan
    List<Project_Plan__c> lstProjectPlans = [SELECT Id,OwnerId,Owner.Id,Owner.Email,Owner.FirstName,Owner.LastName,Owner.Name,Project_Manager__c,Project_Manager__r.Email,
                                              Project_Manager__r.FirstName,Project_Manager__r.LastName,Project_Manager__r.Name,Sales_Rep__c,Sales_Rep__r.Email,
                                              Sales_Rep__r.FirstName,Sales_Rep__r.LastName,Sales_Rep__r.Name,Sales_Rep__r.Id,
                                              Customer__c,Customer__r.Email,
                                              Customer__r.FirstName,Customer__r.LastName,Customer__r.Name,Customer__r.Id,
                                              Finance__c,Finance__r.Email,
                                              Finance__r.FirstName,Finance__r.LastName,Finance__r.Name,Finance__r.Id
                                              FROM Project_Plan__c
                                              WHERE Id = :ppId];
    Project_Plan__c pp;
    if(!lstProjectPlans.isEmpty()){
      Integer serialNumber = 1;
      pp  = lstProjectPlans.get(0);
      //Inserting the envelope first
      dsfs__DocuSign_Envelope__c envelope = insertEnvelope(pp.Id);
     
      
   //   Integer serialNumber = 1;
      //Inserting document from the uploaded template
     List<dsfs__DocuSign_Envelope_Document__c> lstDocumentsToInsert = new List<dsfs__DocuSign_Envelope_Document__c>();
      lstDocumentsToInsert.add(insertTemplateDocument(templateId,documentId,envelope,serialNumber));
      serialNumber++;         
      
      
      if(!lstDocumentsToInsert.isEmpty())
          insert lstDocumentsToInsert;
      
      //Resetting the counter for inserting users    
      serialNumber = 1; 
      //Inserting the receiver of the documents
      List<dsfs__DocuSign_Envelope_Recipient__c> lstUsers = new List<dsfs__DocuSign_Envelope_Recipient__c>();
      if(pp != null){
        if(pp.Sales_Rep__c != null){
          lstUsers.add(addRecipient(pp.Sales_rep__r,Sales_Rep,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
       // User us = new User(Id= pp.Owner.Id,Email = pp.Owner.Email );
        if(pp.Project_Manager__c != null){
          lstUsers.add(addRecipient(pp.Project_Manager__r,PROJECT_MANAGER,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
        if(pp.customer__c != null){
          lstUsers.add(addRecipient(pp.Customer__r,Customer,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
        
        
      }
      if(!lstUsers.isEmpty())
          insert lstUsers;
      
      String url = BASE_URL + 'DSEID='+envelope.Id+'&SourceID='+pp.Id;
      strResponse = url;
    }
    return strResponse;
  }
  
  webservice static String insertBeneficialProjectDocument(Id ppId) { 
    ATTACHMENT_NAME = Label.Docusign_Beneficial_Use_Attachment_Name;
    String templateId = Label.Project_Plan_Template_Id; 
    //For document id we need to trim the text starting from second last hyphen delimiter
    String documentId =  templateId.substring(0,templateId.subString(0,templateId.lastindexOf(Constants.HYPHEN_DELIMITER)).lastIndexOf(Constants.HYPHEN_DELIMITER));
    String strResponse = '';
    //We will first query all the opportunity documents for our project plan
    List<Project_Plan__c> lstProjectPlans = [SELECT Id,OwnerId,Owner.Id,Owner.Email,Owner.FirstName,Owner.LastName,Owner.Name,Project_Manager__c,Project_Manager__r.Email,
                                              Project_Manager__r.FirstName,Project_Manager__r.LastName,Project_Manager__r.Name,Sales_Rep__c,Sales_Rep__r.Email,
                                              Sales_Rep__r.FirstName,Sales_Rep__r.LastName,Sales_Rep__r.Name,Sales_Rep__r.Id,
                                              Customer__c,Customer__r.Email,
                                              Customer__r.FirstName,Customer__r.LastName,Customer__r.Name,Customer__r.Id,
                                              Finance__c,Finance__r.Email,
                                              Finance__r.FirstName,Finance__r.LastName,Finance__r.Name,Finance__r.Id
                                              FROM Project_Plan__c
                                              WHERE Id = :ppId];
    Project_Plan__c pp;
    if(!lstProjectPlans.isEmpty()){
      Integer serialNumber = 1;
      pp  = lstProjectPlans.get(0);
      //Inserting the envelope first
      dsfs__DocuSign_Envelope__c envelope = insertEnvelope(pp.Id);
     
      
   //   Integer serialNumber = 1;
      //Inserting document from the uploaded template
     List<dsfs__DocuSign_Envelope_Document__c> lstDocumentsToInsert = new List<dsfs__DocuSign_Envelope_Document__c>();
      lstDocumentsToInsert.add(insertTemplateDocument(templateId,documentId,envelope,serialNumber));
      serialNumber++;         
      
      
      if(!lstDocumentsToInsert.isEmpty())
          insert lstDocumentsToInsert;
      
      //Resetting the counter for inserting users    
      serialNumber = 1; 
      //Inserting the receiver of the documents
      List<dsfs__DocuSign_Envelope_Recipient__c> lstUsers = new List<dsfs__DocuSign_Envelope_Recipient__c>();
      if(pp != null){
        if(pp.Sales_Rep__c != null){
          lstUsers.add(addRecipient(pp.Sales_rep__r,Sales_Rep,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
       // User us = new User(Id= pp.Owner.Id,Email = pp.Owner.Email );
        if(pp.Project_Manager__c != null){
          lstUsers.add(addRecipient(pp.Project_Manager__r,PROJECT_MANAGER,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
        if(pp.Finance__c != null){
          lstUsers.add(addRecipient(pp.Finance__r,Finance,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
        
        
      }
      if(!lstUsers.isEmpty())
          insert lstUsers;
      
      String url = BASE_URL + 'DSEID='+envelope.Id+'&SourceID='+pp.Id;
      strResponse = url;
    }
    return strResponse;
  }
  
  */
 
  
  /*
  
  Method being used For Docu Sign Project Plan
  
  
  
  
  
  */
   //This method is used for send project packet from project plan
  webservice static String insertDocusignDocuments(Id ppId) { 
    String templateId = Label.Docusign_Project_Template_Id; 
    //For document id we need to trim the text starting from second last hyphen delimiter
    String documentId =  templateId.substring(0,templateId.subString(0,templateId.lastindexOf(Constants.HYPHEN_DELIMITER)).lastIndexOf(Constants.HYPHEN_DELIMITER));
    String strResponse = '';
    //We will first query all the opportunity documents for our project plan
    List<Project_Plan__c> lstProjectPlans = [SELECT Id,OwnerId,Owner.Id,Owner.Email,Owner.FirstName,Owner.LastName,Owner.Name,Project_Manager__c,Project_Manager__r.Email,
                                              Project_Manager__r.FirstName,Project_Manager__r.LastName,Sales_Rep__c,Sales_Rep__r.Email,
                                              Sales_Rep__r.FirstName,Sales_Rep__r.LastName,Sales_Rep__r.Name,Sales_Rep__r.Id,
                                              Customer_Signer__c,Customer_Signer__r.Email,
                                              Customer_Signer__r.FirstName,Customer_Signer__r.LastName,Customer_Signer__r.Name,Customer_Signer__r.Id
                                              FROM Project_Plan__c
                                                WHERE Id = :ppId];
    Project_Plan__c pp;
    if(!lstProjectPlans.isEmpty()){
      pp  = lstProjectPlans.get(0); 
      //Inserting the envelope first
      dsfs__DocuSign_Envelope__c envelope = insertEnvelope(pp.Id);
      //We need to query the docusign document and insert if the corresponding docusign document record is not there
      //
      
      
      Integer serialNumber = 1;
      //Inserting document from the uploaded template
      List<dsfs__DocuSign_Envelope_Document__c> lstDocumentsToInsert = new List<dsfs__DocuSign_Envelope_Document__c>(); 
      lstDocumentsToInsert.add(insertTemplateDocument(templateId,documentId,envelope,serialNumber));
      serialNumber++;         
      
      List<Document__c> lstDocuments = getDocumentListOfProject(pp.Id);
      //Inserting opportunity documents
      if(!lstDocuments.isEmpty()){
        Attachment att = null;
        for(Document__c doc :lstDocuments){
          att = null;
          if(doc.Attachments != null   && doc.Attachments.size() >  0 && doc.DocumentId__c != null){
            for(Attachment a : doc.Attachments){
                if(a.Id == doc.DocumentId__c){
                    att = doc.Attachments.get(0);
                }  
            }
            if(att != null){
              lstDocumentsToInsert.add(insertAttachmentAsDocusignDocuments(att,serialNumber,envelope));
              serialNumber++;    
            }
            
          }
        }
      }
      system.debug('lstDocumentsToInsert :  ' + lstDocumentsToInsert);
      if(!lstDocumentsToInsert.isEmpty()){
        insert lstDocumentsToInsert;
      }
      //Resetting the counter for inserting users    
      serialNumber = 1; 
      //Inserting the receiver of the documents
      List<dsfs__DocuSign_Envelope_Recipient__c> lstUsers = new List<dsfs__DocuSign_Envelope_Recipient__c>();
      if(pp != null){
        if(pp.Customer_Signer__c != null){
          lstUsers.add(addRecipient(pp.Customer_Signer__r,CUSTOMER,serialNumber,envelope,CONTACT_TYPE));
          serialNumber++;
        }
        User us = new User(Id= pp.Owner.Id,Email = pp.Owner.Email );
        lstUsers.add(addRecipient(us,PROJECT_MANAGER,serialNumber,envelope,USER_TYPE));
        serialNumber++;
        /*if(pp.Sales_Rep__c != null){
          lstUsers.add(addRecipient(pp.Sales_Rep__r,SALES_REP,serialNumber,envelope));
          serialNumber++;
        }*/
        
        
      }
      if(!lstUsers.isEmpty())
          insert lstUsers;
      
      String url = BASE_URL + 'DSEID='+envelope.Id+'&SourceID='+pp.Id;
      strResponse = url;
    }
    return strResponse;
  }
  
  /* 
  webservice static String insertBeneficialPhaseDocument(Id phaseId) { 
    ATTACHMENT_NAME = Label.Docusign_Beneficial_Use_Attachment_Name; 
    String templateId = Label.Project_Phase_Template_Id; 
    //For document id we need to trim the text starting from second last hyphen delimiter
    String documentId = templateId.substring(0,templateId.subString(0,templateId.lastindexOf(Constants.HYPHEN_DELIMITER)).lastIndexOf(Constants.HYPHEN_DELIMITER));
    String strResponse = '';
    //We will first query all the opportunity documents for our project plan
    List<Project_Phase__c> lstProjectPhases = [SELECT Id,Project_Plan__c,Project_Plan__r.OwnerId,Project_Plan__r.Owner.Id,
                                              Project_Plan__r.Owner.Email,Project_Plan__r.Owner.Name,Project_Plan__r.Project_Manager__c,
                                              Project_Plan__r.Project_Manager__r.Email,Project_Plan__r.Project_Manager__r.Name,
                                              Project_Plan__r.Sales_Rep__c,Project_Plan__r.Sales_Rep__r.Email,
                                              Project_Plan__r.Sales_Rep__r.Name, Project_Plan__r.Sales_Rep__r.Id,
                                              Project_Plan__r.Finance__c, Project_Plan__r.Finance__r.Email,
                                              Project_Plan__r.Finance__r.Name, Project_Plan__r.Finance__r.Id,
                                              Project_Plan__r.Customer__c, Project_Plan__r.Customer__r.Email,
                                              Project_Plan__r.Customer__r.Name, Project_Plan__r.Customer__r.Id
                                              FROM Project_Phase__c
                                              WHERE Id = :phaseId];
    Project_Phase__c phase;
    if(!lstProjectPhases.isEmpty()){
      System.debug('Go to Heaven');
      phase  = lstProjectPhases.get(0);
      //Inserting the envelope first
      dsfs__DocuSign_Envelope__c envelope = insertEnvelope(phase.Id);
     
      
      Integer serialNumber = 1;
      //Inserting document from the uploaded template
      List<dsfs__DocuSign_Envelope_Document__c> lstDocumentsToInsert = new List<dsfs__DocuSign_Envelope_Document__c>();
      lstDocumentsToInsert.add(insertTemplateDocument(templateId,documentId,envelope,serialNumber));
      serialNumber++;         
      
     
      if(!lstDocumentsToInsert.isEmpty())
          insert lstDocumentsToInsert;
      
      
      //Resetting the counter for inserting users    
      serialNumber = 1; 
      //Inserting the receiver of the documents
      List<dsfs__DocuSign_Envelope_Recipient__c> lstUsers = new List<dsfs__DocuSign_Envelope_Recipient__c>();
      if(phase != null){
        if(phase.Project_Plan__r.SALES_Rep__c != null)  {
          lstUsers.add(addRecipient(phase.Project_Plan__r.Sales_Rep__r,SALES_REP,serialNumber,envelope,User_TYPE));
          serialNumber++;
         
        }
        //Adding the owner
      //  User us = new User(Id= phase.Project_Plan__r.Owner.Id, Email = phase.Project_Plan__r.Owner.Email );
        if(phase.Project_Plan__r.Project_Manager__c != null) {
          lstUsers.add(addRecipient(phase.Project_Plan__r.Project_Manager__r,PROJECT_MANAGER,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
        if(phase.Project_Plan__r.Finance__c != null) {
           lstUsers.add(addRecipient(phase.Project_Plan__r.Finance__r,Finance,serialNumber,envelope,USER_TYPE));
          serialNumber++;
        }
      }
      if(!lstUsers.isEmpty())
          insert lstUsers;
      
      String url = BASE_URL + 'DSEID='+envelope.Id+'&SourceID='+phase.Id;
      strResponse = url;
    }
    return strResponse;
  }
  
  */
  //Method used for project phase on send project packet button
  webservice static String insertDocusignDocsFromPhase(Id phaseId) { 
    String templateId = Label.Docusign_Phase_Template_Id; 
    //For document id we need to trim the text starting from second last hyphen delimiter
    String documentId = templateId.substring(0,templateId.subString(0,templateId.lastindexOf(Constants.HYPHEN_DELIMITER)).lastIndexOf(Constants.HYPHEN_DELIMITER));
    String strResponse = '';
    //We will first query all the opportunity documents for our project plan
    List<Project_Phase__c> lstProjectPhases = [SELECT Id,Project_Plan__c,Project_Plan__r.OwnerId,Project_Plan__r.Owner.Id,
                                              Project_Plan__r.Owner.Email,Project_Plan__r.Owner.Name,
                                              Project_Plan__r.Project_Manager__c,Project_Plan__r.Project_Manager__r.Email,Project_Plan__r.Project_Manager__r.Name,
                                              Project_Plan__r.Sales_Rep__c,Project_Plan__r.Sales_Rep__r.Email,
                                              Project_Plan__r.Sales_Rep__r.Name,Project_Plan__r.Sales_Rep__r.Id,
                                              Project_Plan__r.Customer__c, Project_Plan__r.Customer__r.Email,
                                              Project_Plan__r.Customer__r.Name, Project_Plan__r.Customer__r.Id
                                              FROM Project_Phase__c
                                             WHERE Id = :phaseId];
                                            /* System.debug('<<<<<<<<<<'+'lstProjectPhases'+lstProjectPhases.get(0).Project_Plan__r.Owner.Name);
                                             System.debug('<<<<<<<<<<'+'lstProjectPhases'+lstProjectPhases.get(0).Project_Plan__r.Sales_Rep__r.Name);
                                             System.debug('<<<<<<<<<<'+'lstProjectPhases'+lstProjectPhases.get(0).Project_Plan__r.Customer__r.Name);*/
    Project_Phase__c phase;
    if(!lstProjectPhases.isEmpty()){
      phase  = lstProjectPhases.get(0);
      //Inserting the envelope first
      dsfs__DocuSign_Envelope__c envelope = insertEnvelope(phase.Id);
      //We need to query the docusign document and insert if the corresponding docusign document record is not there
      List<Document__c> lstDocuments = getDocumentListOfProject(phase.Project_Plan__c);
      
      Integer serialNumber = 1;
      //Inserting document from the uploaded template
      List<dsfs__DocuSign_Envelope_Document__c> lstDocumentsToInsert = new List<dsfs__DocuSign_Envelope_Document__c>();
      lstDocumentsToInsert.add(insertTemplateDocument(templateId,documentId,envelope,serialNumber));
      serialNumber++;         
      
      //Inserting opportunity documents
      if(!lstDocuments.isEmpty()){
        
        Attachment att = null;
        for(Document__c doc :lstDocuments){
          att = null;
          if(doc.Attachments != null   && doc.Attachments.size() >  0 && doc.DocumentId__c != null){
            for(Attachment a : doc.Attachments){
                if(a.Id == doc.DocumentId__c){
                    att = doc.Attachments.get(0);
                }  
            }
            if(att != null){
              lstDocumentsToInsert.add(insertAttachmentAsDocusignDocuments(att,serialNumber,envelope));
              serialNumber++;    
            }
            
          }
        }
        
      }
      system.debug('lstDocumentsToInsert :  ' + lstDocumentsToInsert);
      if(!lstDocumentsToInsert.isEmpty())
          insert lstDocumentsToInsert;
      
      
      //Resetting the counter for inserting users    
      serialNumber = 1; 
      //Inserting the receiver of the documents
      List<dsfs__DocuSign_Envelope_Recipient__c> lstUsers = new List<dsfs__DocuSign_Envelope_Recipient__c>();
      if(phase != null){
        if(phase.Project_Plan__r.Customer__c != null){
          lstUsers.add(addRecipient(phase.Project_Plan__r.Customer__r,CUSTOMER,serialNumber,envelope,CONTACT_TYPE));
          serialNumber++;
        }
        //Adding the owner
        //User us = new User(Id= phase.Project_Plan__r.Owner.Id,Email = phase.Project_Plan__r.Owner.Email);
        //usName = [SELECT Name FROM User WHERE Id=:phase.Project_Plan__r.Owner.Id].Name;
        lstUsers.add(addRecipient(phase.Project_Plan__r.Project_Manager__r,PROJECT_MANAGER,serialNumber,envelope,USER_TYPE));
        serialNumber++;
        
        /*if(phase.Project_Plan__r.Sales_Rep__c != null){
          lstUsers.add(addRecipient(phase.Project_Plan__r.Sales_Rep__r,SALES_REP,serialNumber,envelope));
          serialNumber++;
        }*/
        
      }
      if(!lstUsers.isEmpty())
          insert lstUsers;
      
      String url = BASE_URL + 'DSEID='+envelope.Id+'&SourceID='+phase.Id;
      strResponse = url;
    }
    return strResponse;
  }
  
  
    
    
    
  //Method used for Case CO Layout to sending receivers
  webservice static String redirectToCODocusign(Id caseId) { 
    String templateId = Label.Docusign_CO_Project_Template_Id; 
    ATTACHMENT_NAME = Label.Docusign_CO_Attachment_Name;
    //For document id we need to trim the text starting from second last hyphen delimiter
    String documentId = templateId.substring(0,templateId.subString(0,templateId.lastindexOf(Constants.HYPHEN_DELIMITER)).lastIndexOf(Constants.HYPHEN_DELIMITER));
    String strResponse = '';
    subject = Label.Docusign_Email_CO_Template_Subject;
    List<Case> lstCases = [SELECT Id,Opportunity__c,Project_Plan__c
                            FROM Case WHERE Id = :caseId
                           ];
    if(!lstCases.isEmpty()){
      Case cs = lstCases.get(0);
      List<Project_Plan__c> lstProjectPlans = [SELECT Id,OwnerId,Owner.Id,Owner.Email,Owner.FirstName,Owner.LastName,
                                                  Owner.Name,Project_Manager__c,Project_Manager__r.Email,
                                                  Project_Manager__r.FirstName,Project_Manager__r.LastName,
                                                  Customer__c,Customer__r.Email,
                                                  Customer__r.FirstName,Customer__r.LastName,Customer__r.Name,Customer__r.Id
                                                  FROM Project_Plan__c
                                                    WHERE Id = :cs.Project_Plan__c];
      if(!lstProjectPlans.isEmpty()){
        Project_Plan__c pp = lstProjectPlans.get(0);
        //Inserting the envelope first
        dsfs__DocuSign_Envelope__c envelope = insertEnvelope(cs.Id);
          
          
        Integer serialNumber = 1;
        //Inserting document from the uploaded template
        List<dsfs__DocuSign_Envelope_Document__c> lstDocumentsToInsert = new List<dsfs__DocuSign_Envelope_Document__c>();
        lstDocumentsToInsert.add(insertTemplateDocument(templateId,documentId,envelope,serialNumber));
        if(!lstDocumentsToInsert.isEmpty()){
          insert lstDocumentsToInsert;      
        } 
        //Inserting the receiver of the documents
        serialNumber = 1;
        List<dsfs__DocuSign_Envelope_Recipient__c> lstUsers = new List<dsfs__DocuSign_Envelope_Recipient__c>();
        if(pp.Customer__c != null){
          lstUsers.add(addRecipient(pp.Customer__r,CUSTOMER,serialNumber,envelope,CONTACT_TYPE));
          serialNumber++;
        }
        User us = new User(Id= pp.Owner.Id,Email = pp.Owner.Email );
        lstUsers.add(addRecipient(pp.Project_Manager__r,PROJECT_MANAGER,serialNumber,envelope,USER_TYPE));
        serialNumber++;
        if(!lstUsers.isEmpty()){
          insert lstUsers;
        }
        String url = BASE_URL + 'DSEID='+envelope.Id+'&SourceID='+cs.Id;
        strResponse = url;  
      }
      
      
    }
    
    return strResponse;
  }  
    
  // Method for getting the url for standard docusign button, will be used only in LEX
  webservice static String getStandardDocusignRedirectUrl(String sourceObjectId) {
    String url = '/';
    if(!Test.IsRunningTest()) {
    if(sourceObjectId != null){
      //Inserting the envelope first
      dsfs__DocuSign_Envelope__c envelope = insertEnvelope(sourceObjectId);
      url = BASE_URL + DSE_ID_PARAM + '='+envelope.Id+'&'+SOURCE_ID_PARAM+'='+sourceObjectId;
        }
    }
    return url;
  } 
    
    
  static List<Document__c> getDocumentListOfProject(Id projectPlanId){
    List<Document__c> lstDocuments = new List<Document__c>();
    Map<ID, Opportunity> opportunityMap = new Map<ID, Opportunity>([SELECT ID 
                                          FROM Opportunity 
                                          WHERE Project_Plan__c = :projectPlanId]);
    if(!opportunitymap.keySet().isEmpty()){
      Map<ID, BigMAchines__Quote__c> oracleQuotes = new Map<ID, BigMAchines__Quote__c>([SELECT ID 
                                          FROM BigMAchines__Quote__c 
                                          WHERE BigMachines__Opportunity__r.id in : opportunityMap.keyset()
                                          AND BigMAchines__is_primary__c = true]); 
      lstDocuments = [SELECT Id,Name,Project_Plan__c,Document_Link__c,Document_type__c,DocumentId__c,
                                        (SELECT Id,Name from Attachments)
                                        FROM Document__c WHERE (Opportunity__r.id =: opportunityMap.keyset()
                                         OR Oracle_Quote__r.id =: oracleQuotes.keyset())
                                         AND Use_for_Sign_Off__c = true];
    }
                                          
    return lstDocuments;
  }
    
    
  static dsfs__DocuSign_Envelope__c  insertEnvelope(String srcObjectId){
    dsfs__DocuSign_Envelope__c envelope = new dsfs__DocuSign_Envelope__c();
    envelope.dsfs__DocuSign_Email_Subject__c = subject;
    envelope.dsfs__Source_Object__c = srcObjectId;
    envelope.dsfs__ChatterUpdatesEnabled__c = true;
    envelope.dsfs__ChatterEnvCompletedText__c = CHATTER_TEXT  ;
    envelope.dsfs__DocumentID__c = DOCUMENT_ID_TEXT  ;
    envelope.dsfs__DocuSign_Email_Message__c = body;
    envelope.dsfs__Send_Reminder__c = true;
    envelope.dsfs__Reminder_Repeat_Interval_in_Days__c = REPEAT_INTERVAL ;
    envelope.dsfs__Reminder_Interval__c = REMINDER_INTERVAL ;
    envelope.Is_Project_Package__c = true; // defaulting it to true to identify custom click or project package document
    insert envelope;
    return envelope;
  }
  
  static dsfs__DocuSign_Envelope_Recipient__c addRecipient(Sobject obj,String role,Integer serialNumber,dsfs__DocuSign_Envelope__c envelope,String type){
    
    dsfs__DocuSign_Envelope_Recipient__c recipient = new dsfs__DocuSign_Envelope_Recipient__c(dsfs__DocuSign_EnvelopeID__c = envelope.Id,dsfs__DocuSign_Recipient_Role__c = role,
    dsfs__RoleName__c = role,
    dsfs__Email_Body__c = body,dsfs__Email_Subject__c = subject,
    dsfs__Routing_Order__c = serialNumber,
    dsfs__DocuSign_Signer_Type__c = SIGNER, 
    dsfs__RoleValue__c = serialNumber,                                                                                        
    dsfs__Salesforce_Recipient_Type__c = type,
    dsfs__SignNow__c = false);
    if(obj instanceof Contact){
      Contact  con = (Contact) obj; 
      recipient.dsfs__Recipient_Email__c = con.Email;
      recipient.dsfs__SignInPersonName__c =  con.Name;
      recipient.dsfs__DocuSign_Signature_Name__c = con.Name;
      recipient.dsfs__DSER_ContactID__c = con.Id;
    }else if(obj instanceof User){
      User  us = (User) obj;
      recipient.dsfs__Recipient_Email__c = us.Email;
      recipient.dsfs__SignInPersonName__c =  us.Name;
      recipient.dsfs__DocuSign_Signature_Name__c = us.Name;
      recipient.dsfs__DSER_UserID__c = us.Id;
    }
    
    return recipient;
     
  }
  
  static dsfs__DocuSign_Envelope_Document__c insertTemplateDocument(String templateId,String documentId,dsfs__DocuSign_Envelope__c envelope,Integer serialNumber){
    dsfs__DocuSign_Envelope_Document__c document =  new dsfs__DocuSign_Envelope_Document__c(dsfs__Attachment_Name__c= ATTACHMENT_NAME ,
                                dsfs__Document_Name__c=ATTACHMENT_NAME,
                                dsfs__External_Document_Id__c = templateId ,
                                dsfs__Document_ID__c  = documentId ,
                                dsfs__DocuSign_EnvelopeID__c = envelope.Id,
                                dsfs__SFDocument_Type__c = DOC_TYPE_TEMPLATE ,
                                dsfs__Document_Order__c = serialNumber);
    return document;
  }
  
  static dsfs__DocuSign_Envelope_Document__c insertAttachmentAsDocusignDocuments(Attachment att,Integer serialNumber,dsfs__DocuSign_Envelope__c envelope){
    dsfs__DocuSign_Envelope_Document__c document = new dsfs__DocuSign_Envelope_Document__c(dsfs__Attachment_ID__c = att.Id, 
                                dsfs__Attachment_Name__c= att.Name ,dsfs__Document_ID__c = att.Id ,dsfs__Document_Name__c=att.Name,
                                dsfs__DocuSign_EnvelopeID__c = envelope.Id,
                                dsfs__SFDocument_Type__c = DOC_TYPE_ATTACHMENT,
                                dsfs__Attachment_NameEx__c = att.Name,dsfs__Document_Order__c = serialNumber);
                                
    return document;
  } 
}