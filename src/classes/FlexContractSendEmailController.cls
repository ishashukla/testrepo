/************************************************************************
Name       : FlexContractSendEmailController
Author     : Sunil (Appirio)
Date       : 18 Sept, 2015
Description: Get Recipients emails for Flex Credit custom button send email.

Revision History:
    2015-09-18  N.Shiwani    S-326196 Original development story.
    2015-11-17  S.Gupta      Deployed to Production.
    2016-03-07  H.Whitacre   I-205280 remove Acct.Owner from recipients.
*************************************************************************/
public with sharing class FlexContractSendEmailController {

  public Flex_Contract__c currentFlexContract {get;set;}

  private String strEmails = '';

  public FlexContractSendEmailController(ApexPages.StandardController stdController){
    Set<String> recepientMailsIds = new Set<String>();
    currentFlexContract = (Flex_Contract__c)stdController.getRecord();

    currentFlexContract = [ SELECT Id, Opportunity__c, OwnerId, Opportunity__r.Owner.Email, Owner.Email, Account__r.Owner.Email
                           FROM Flex_Contract__c
                           WHERE Id = :currentFlexContract.Id Limit 1];

    // I-205280 - removing account owner from recipients
    //if(String.isBlank(currentFlexContract.Account__r.Owner.Email) == false){
    //    recepientMailsIds.add(currentFlexContract.Account__r.Owner.Email);
    //}
    if(String.isBlank(currentFlexContract.Opportunity__r.Owner.Email) == false){
      recepientMailsIds.add(currentFlexContract.Opportunity__r.Owner.Email);
    }

    for(OpportunityLineItem oppLineItem : [ SELECT PSR_Contact__r.Email, PSR_Contact__c
                                            FROM OpportunityLineItem
                                            WHERE OpportunityId =: currentFlexContract.Opportunity__c]){

      if(String.isBlank(oppLineItem.PSR_Contact__r.Email) == false){
        recepientMailsIds.add(oppLineItem.PSR_Contact__r.Email);
      }

    }

    for(String str :recepientMailsIds){
      strEmails = strEmails + str + ',';
    }

    if(strEmails.length() > 0){
      strEmails = strEmails.substring(0, strEmails.length() - 1);
    }

  }

  // This method being called from page action to Send Email Page.
  public pageReference redirectPage(){
    //https://stryker--flexdp1.cs15.my.salesforce.com/_ui/core/email/author/EmailAuthor?p3_lkid=a0Oe00000062Hiz&retURL=%2Fa0Oe00000062Hiz
    //Schema.DescribeSObjectResult r = Flex_Contract__c.sObjectType.getDescribe();
    //String keyPrefix = r.getKeyPrefix();

    PageReference pg = new PageReference('/_ui/core/email/author/EmailAuthor?');

    //for (String paramName : ApexPages.currentPage().getParameters().keySet()) {
    //  pg.getParameters().put(paramName, ApexPages.currentPage().getParameters().get(paramName));
    //}
    pg.getParameters().put('p3_lkid', currentFlexContract.id);
    pg.getParameters().put('retURL', currentFlexContract.id);
    pg.getParameters().put('p24', strEmails);
    //pg.getParameters().put('nooverride', '1');
    //pg.getParameters().remove('save_new');
    return pg;
  }
}