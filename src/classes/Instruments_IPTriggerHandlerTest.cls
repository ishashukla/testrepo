// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_IPTriggerHandlerTest
// Description: Test Class for Instruments_IPTriggerHandler   
// April 15, 2016    Meena Shringi    Original 
//
@isTest 
public class Instruments_IPTriggerHandlerTest {
    static testMethod void testInstruments_IPTriggerHandler(){ 
        Product2 product = TestUtils.createProduct(1,false).get(0);
        product.Description = 'test product';
        insert product;
        List<InstalledProduct__c> installprodlst = TestUtils.createInstalledProduct(10, product.Id,true);
        List<InstalledProduct__c> installProductlst = [select Prod_Description__c from InstalledProduct__c];
        for(InstalledProduct__c installProd : installProductlst) {
            
           system.assertEquals(product.Description, installProd.Prod_Description__c, 'description on base product must be populated from product');
        }
    }
}