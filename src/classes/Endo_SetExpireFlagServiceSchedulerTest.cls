/******************************************************************************************
 *  Purpose : Test Class for Endo_SetExpireFlagServiceScheduler
 *  Author  : Shubham Paboowal
 *  Date    : September 13, 2016
 *  Story	: S-437706
*******************************************************************************************/  
@isTest
private class Endo_SetExpireFlagServiceSchedulerTest {

    static testMethod void testScheduler() {
    	String CRON_EXP = '0 0 0 * * ? *'; // this will schedule for 12:00:00 AM every day.
    	Test.startTest();
        Id jobId = System.schedule('Set Expire Flag Scheduler', CRON_EXP, new Endo_SetExpireFlagServiceScheduler());
        Test.stopTest();
        System.assert(jobId != null);  // ensure the class was scheduled.
    }
}