/**==================================================================================================================================
 * Appirio, Inc
 * Name: Endo_LinkCasesWithOrdersSchedularTest
 * Description: 
 * Created Date: 
 * Created By: 
 *
 * Date Modified           Modified By             Description
 * 05-09-2016 (May 9th? Sept 5?) Chandra Shekhar   To Fix Test Class
 * Oct 26th, 2016          Nathalie Le Guay        Replace references of Case.Current_Case_Order__c with Case.Current_Case_StdOrder__c
 *                                                 Replace references of Order__c.Order_Contact__c with Order.Contact__c
 ==========================================================================================================================================*/
@isTest
private class Endo_LinkCasesWithOrdersSchedularTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test(){
    
    Account acc = new Account(Name = 'Test Account');
    insert acc;
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    insert con;
    
    Case cs = new Case(Subject = 'Test Subject', ContactId = con.Id, Oracle_Order_Ref__c = '1234');
    cs.contactId = con.Id;
    //Modified By Chandra Shekhar Sharma to add record types on case Object
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
    cs.recordTypeId = rtByName.getRecordTypeId();
    insert cs;
    
    Order ordr = new Order(Name='1234', AccountId = acc.Id, Oracle_Order_Number__c='1234',Status = 'Entered', EffectiveDate = System.today());
    insert ordr;
    
    test.startTest();
    
    //Schedule the test job
    String jobId = System.schedule('Endo_LinkCasesWithOrdersSchedularTest', CRON_EXP, new Endo_LinkCasesWithOrdersSchedular());
    
    //Get info from the CronTrigger API object
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime From CronTrigger WHERE id = :jobId];
    
    //Verify  the expression are the same
    System.assertEquals(CRON_EXP, ct.CronExpression);
    
    //Verify the job has not run
    System.assertEquals(0, ct.TimesTriggered);
    
    //Verify the next time the job will run
    System.assertEquals('2022-03-15 00:00:00',String.valueOf(ct.NextFireTime));
    
    test.stopTest();
    
    Case c = [Select Id, Current_Case_StdOrder__c FROM Case WHERE Id =:cs.Id];
    System.debug('CurrentCaseOrder: '+c.Current_Case_StdOrder__c);
    System.debug('OrderId: '+ordr.Id);
    
    System.assertEquals(c.Current_Case_StdOrder__c, NULL);
      
    //Order objOrder = [Select Id, Order_Contact__c FROM Order WHERE Id =:ordr.Id];
    Order objOrder = [Select Id, Contact__c FROM Order WHERE Id =:ordr.Id];
    System.debug('OrderContact: '+objOrder.Contact__c);
    System.debug('ConId: '+con.Id);
    System.assertEquals(objOrder.Contact__c,NULL);
    
    
    }
}