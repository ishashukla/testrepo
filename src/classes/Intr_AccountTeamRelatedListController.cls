// 
// (c) 2016 Appirio, Inc.
//
// T-507913, This is a batch class for updating Cases for Inactivity
//
// June 2, 2016  Shreerath Nair  Original

public with sharing class Intr_AccountTeamRelatedListController {
    
    public List<Id> accountList {get;set;}
    
    public Intr_AccountTeamRelatedListController(ApexPages.StandardController controller) {
        
        //Case cs = (Case)controller.getRecord();
        Case cs = [SELECT AccountId from Case where Id = : controller.getId()];

        accountList = new List<Id>();
        accountList.add(cs.AccountId);
        
    }
}