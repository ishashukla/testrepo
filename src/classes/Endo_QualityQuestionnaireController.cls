/**================================================================      
 * Appirio, Inc
 * Name: Endo_QualityQuestionnaireController
 * Description: To save the Quality Questionnaire for a Case.[T-515758,S-372032]
 * Created Date: [06/29/2016]
 * Created By: Ashu Gupta (Appirio)
 * 
 * Date Modified      Modified By      Description of the update
 * 
 * 12 Sept 2016       Shreerath Nair(Appirio)   T-534019 Repalced Product_Family__c  with Line_Type__c         
 * 12 Sept 2016       Daniel Saldana (Appirio)  I-234448 Repalced Line_Type__c with 
 ==================================================================*/


public with sharing class Endo_QualityQuestionnaireController {

    public Quality_Questionnaire__c QualityQuestionnaire {get;set;}  
    public List<QuestionnaireWrapper> qualityQuestions {get;set;}
    public List<QuestionnaireWrapper> regulatoryQuestions {get;set;}
    
    public List<QualityQuestionnaire> quaQuestions {
        get{
            
            if(quaQuestions.size() == 0 && productLine != null){
                // T-534019 Shreerath Nair (Appirio) - Repalced Product_Family__c  with Line_Type__c 
                for (Question__c question :  [SELECT id, Question__c, Product_Line__c, Type__c 
                                                from Question__c
                                                where Product_Line__c = : productLine 
                                                ORDER BY Display_Order__c]){
                    if(question.Type__c == 'Quality'){
                        quaQuestions.add(new QualityQuestionnaire(question));
                    } else if(question.Type__c == 'Regulatory'){
                        regQuestions.add(new QualityQuestionnaire(question));
                    }                             
                }
            }
        
            return quaQuestions;
            }
        
        set;}
        
    public List<QualityQuestionnaire> regQuestions {get;set;}
    // T-534019 Shreerath Nair (Appirio) - Repalced Prodfamily  with LineType
    public String productLine {get;set;}
    
    public String CaseId {get;set;}
    
    // Constructor
    public Endo_QualityQuestionnaireController(ApexPages.StandardController stdController){
        CaseId = ApexPages.currentPage().getParameters().get('CaseId');
        QualityQuestionnaire = new Quality_Questionnaire__c ();
        if(CaseId != null && CaseId != '') {
            QualityQuestionnaire.Case__c = CaseId;
        }
        qualityQuestions = new List<QuestionnaireWrapper>();
        regulatoryQuestions = new List<QuestionnaireWrapper>();
    }
    
    public Endo_QualityQuestionnaireController(){
       
    }
    
    //  To show the questions on choosing a product
    //  @param void
    //  @return void
    public void showNextQuestions() {
        qualityQuestions.clear();
        regulatoryQuestions.clear();
        if(QualityQuestionnaire.Product__c != null){
            for (Question__c question :  [SELECT id, Question__c, Type__c 
                                            from Question__c
                                            //where Product_Family__c = : QualityQuestionnaire.product_Family__c 
                                            ORDER BY Display_Order__c]){
                if(question.Type__c == 'Quality'){
                    qualityQuestions.add(new QuestionnaireWrapper(question));
                } else if(question.Type__c == 'Regulatory'){
                    regulatoryQuestions.add(new QuestionnaireWrapper(question));
                }                             
            }
        }
    }
    
  
    
    //  To save the answers in database.
    //  @param null
    //  @return a new PageReferencewith caseiD as parameter.
    public PageReference save() {
        List<Answer__c> answersToInsert = new List<Answer__c>();
        
        try{
            
            // Insert Quality Questionnaire
            insert QualityQuestionnaire;
        
            for(QuestionnaireWrapper qualityWrapper : qualityQuestions){
                qualityWrapper.answer.Quality_Questionnaire__c = QualityQuestionnaire.id;
                answersToInsert.add(qualityWrapper.answer);
            }
            for(QuestionnaireWrapper qualityWrapper : regulatoryQuestions){
                qualityWrapper.answer.Quality_Questionnaire__c = QualityQuestionnaire.id;
                 answersToInsert.add(qualityWrapper.answer);
            }
            
            // Insert answers
            insert answersToInsert;
        }catch(DMLException ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(0)));
            return null;
        }
        return new PageReference('/' + CaseId);
    }
    
    //  Return to the case detail page
    //  @param null
    //  @return a new PageReferencewith caseiD as parameter.
    public PageReference cancel() {
        return new PageReference('/' + CaseId);
    }
    
    // Qustionnaire Wrapper class
    public class QuestionnaireWrapper{
        public Question__c question {get;set;}
        public Answer__c answer {get;set;}
        public QuestionnaireWrapper(Question__c question){
            this.question = question;
            this.answer = new Answer__c(Question__c = question.id);
        }
    }
}