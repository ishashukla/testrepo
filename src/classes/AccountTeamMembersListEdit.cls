/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMembersListEdit
 * Description: 
 * Created Date: 
 * Created By: 
 *
 * Date Modified         Modified By           Description of the update
 * 16th Nov 2016      Varun Vasishtha     I-241384  Added PopulatePreferredTechnician() method to populate preffered technician of associated work orders
  =====================================================================*/
public without sharing class AccountTeamMembersListEdit {
  
  public List<accountTeamWrapper> accountTeamMembers {get;set;}
  public List<accountTeamWrapper> newAccountTeamMembers {get;set;}
  public Account acc {get;set;}
  public Boolean showAddMemberSection {get;set;}
  public Boolean displayAccess {get;set;}
  public List<selectOption> accountOppAccessLevels {get;set;}
  public List<selectOption> caseAccessLevels {get;set;}
  public Id selectedId {get;set;}
  public Integer listSize {get;set;}
  public List<accountTeamWrapper> accountTeamMembersToShow {get;set;}
  public Integer currentSizeShown {get;set;}
  public Boolean reachedMax {get;set;}
  
  //==========================================================================
  // Constructor
  //==========================================================================
  public AccountTeamMembersListEdit ( ApexPages.Standardcontroller std) {
    showAddMemberSection = true;
    displayAccess = false;
    currentSizeShown = 5;
    reachedMax = false;
    accountTeamMembersToShow = new List<accountTeamWrapper>();
    accountOppAccessLevels = new List<selectOption>();
    accountOppAccessLevels.add(new selectOption('Read','Read Only'));
    accountOppAccessLevels.add(new selectOption('Edit','Read/Write'));
    caseAccessLevels = new List<selectOption>();
    caseAccessLevels.add(new selectOption('None','Private'));
    caseAccessLevels.add(new selectOption('Read','Read Only'));
    caseAccessLevels.add(new selectOption('Edit','Read/Write'));
    Id accId = std.getRecord().Id;
    acc = [SELECT Id,Name, ParentID, RecordType.DeveloperName 
           FROM Account
           WHERE Id = :accId];
    fetchExistingAccountTeamMembers();
    listSize = accountTeamMembers.size();
    if( listSize == 5) {
        reachedMax = true;
    }
    addTeamMembers();
    
  }
  
  //==========================================================================
  // Method to fetch the existing Account Team Members and their access levels
  //==========================================================================
  public void fetchExistingAccountTeamMembers () {
    accountTeamMembers = new List<accountTeamWrapper> ();
    accountTeamMembersToShow = new List<accountTeamWrapper>();
    integer i = 0;
    map<Id,AccountShare> accShareMap = new map<Id,AccountShare>();
    for ( AccountShare accShare : [SELECT Id,OpportunityAccessLevel, CaseAccessLevel, UserOrGroupId
                                   FROM AccountShare
                                   WHERE AccountId = :acc.Id]) {
      accShareMap.put(accShare.UserOrGroupId,accShare);
    }
    for ( AccountTeamMember atm : [SELECT Id,TeamMemberRole,UserId,AccountId,
                                          AccountAccessLevel ,
                                          User.Name
                                   FROM AccountTeamMember 
                                   WHERE AccountId = :acc.Id
                                   order by User.Name ASC]) {
      accountTeamWrapper atWrap = new accountTeamWrapper();
      atWrap.member = atm;
      if(accShareMap != null && accShareMap.containsKey(atm.UserId)) {
        AccountShare accShare = accShareMap.get(atm.UserId);
        atWrap.caseAccess = accShare.CaseAccessLevel;
        atWrap.opportunityAccess = accShare.OpportunityAccessLevel;
      }
      accountTeamMembers.add(atWrap);
      if( i < 5) {
        accountTeamMembersToShow.add(atWrap);
        i++;
      }
      
    }
  }
  
  //==========================================================================
  // Method to create new members list and display the add team members section
  //==========================================================================
  public void addTeamMembers () {
    newAccountTeamMembers = new List<accountTeamWrapper>();
    for ( integer i=0; i<5; i++) {
      accountTeamWrapper atm = new accountTeamWrapper();
      atm.member.AccountId = acc.Id;
      newAccountTeamMembers.add(atm);
    }
    showAddMemberSection = true;
  }
  
  //==========================================================================
  // Method to save the newly created team members
  //==========================================================================
  public pagereference saveNewTeamMembers () {
    List<AccountTeamMember> atmToInsert = new List<AccountTeamMember>();
    List<AccountShare> accshareToInsert = new List<AccountShare>();
    List<ContactShare> contactSharesToInsert = new List<ContactShare>();
    Map<ID,Set<ID>> accountIDUserMap = new Map<ID,Set<ID>>();
    Set<String> profileNamesSet = new Set<String>{Constants.Profile1, Constants.Profile2};
    Set<ID> userIDSet = new Set<ID>();
    try {
      for ( accountTeamWrapper atmWrap : newAccountTeamMembers) {
        if ( atmWrap.member.userId != null ) {
          atmToInsert.add(atmWrap.member);
          System.debug('---atmWrap.member.AccountAccessLevel---'+atmWrap.accountAccess);
          if ( atmWrap.accountAccess == 'Edit') {
            AccountShare accshare = new AccountShare();
            accshare.AccountAccessLevel = atmWrap.accountAccess;
            accshare.AccountId = atmWrap.member.AccountId;
            accshare.OpportunityAccessLevel = atmWrap.opportunityAccess;
            accshare.CaseAccessLevel = atmWrap.caseAccess;
            accshare.UserOrGroupId = atmWrap.member.userId;
            accshareToInsert.add(accshare);
          }
          userIDSet.add(atmWrap.member.userId);
          if(accountIDUserMap.containsKey(atmWrap.member.accountID)){
              accountIDUserMap.get(atmWrap.member.accountID).add(atmWrap.member.userID);
          }else{
              accountIDUserMap.put(atmWrap.member.accountID,new Set<ID>{atmWrap.member.userID});
          }
        }
      }
      if(userIDSet.size() > 0){
        for(User usr : [SELECT ID FROM User WHERE ID in :userIDSet
                          AND Profile.Name in : profileNamesSet
                          AND isActive = true]){
          if(acc.parentID != null && acc.RecordType.DeveloperName == Constants.COMM_US_ACCOUNTS){
            AccountShare parentAccshare = new AccountShare();
            parentAccshare.AccountAccessLevel = Constants.PERM_READ;
            parentAccshare.AccountId = acc.parentId;
            parentAccshare.OpportunityAccessLevel = 'None';
            parentAccshare.CaseAccessLevel = 'None';
            parentAccshare.UserOrGroupId = usr.id;
            accshareToInsert.add(parentAccshare);
          }
        }
      } 
      for(Contact contacts : [SELECT Private__c, AccountID, ID from Contact WHERE AccountId in :accountIDUserMap.keyset()]){
        if(!contacts.private__c){
          for(ID userID : accountIDUserMap.get(contacts.AccountID)){
              ContactShare conShare = new ContactShare();
              conShare.UserOrGroupId = userID;
              conShare.ContactAccessLevel = Constants.PERM_READ;
              conShare.contactID = contacts.ID;
              contactSharesToInsert.add(conShare);
          }
        }
      }
      if (!atmToInsert.isEmpty()) {
        insert atmToInsert;
        PopulatePreferredTechnician(atmToInsert);
      }
      if (!accshareToInsert.isEmpty()) {
        Database.insert(accshareToInsert,false);
      }
      if(contactSharesToInsert != null){
          Database.insert(contactSharesToInsert,false);
      }
      return new pagereference('/'+acc.Id);
    }
    catch ( exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, ex.getMessage()));
      return null;
    }
  }
  //==========================================================================
  // Method to save team members and show the blank list again for the user to enter more records
  //==========================================================================
  public void saveAndMore() {
    saveNewTeamMembers();
    addTeamMembers(); 
  }
  
  //==========================================================================
  // Method to cancel and return back to Account page
  //==========================================================================
  public pagereference doCancel () {
    newAccountTeamMembers = null;
    return new pagereference('/'+acc.Id);
  }

  //==========================================================================
  // Wrapper class to hold the Team Member record as well as their access levels
  //==========================================================================
  public class accountTeamWrapper {
    public AccountTeamMember member {get;set;}
    public String accountAccess {get;set;}
    public String opportunityAccess {get;set;}
    public String caseAccess {get;set;}
    public accountTeamWrapper() {
      member = new AccountTeamMember();
      opportunityAccess = '';
      caseAccess = '';
    }
  }
  
  private static void PopulatePreferredTechnician(List<AccountTeamMember> newList){
        Set<Id> allAccId = new Set<Id>();
        Map<Id,Set<Id>> mapAccIdwithUserId = new Map<Id,Set<Id>>();
        for(AccountTeamMember item :newList){
            if(item.UserId != null && item.TeamMemberRole == 'Primary Field Technician'){
                if(!mapAccIdwithUserId.containsKey(item.UserId)){
                    Set<id> accs = new Set<id>{item.AccountId};
                    mapAccIdwithUserId.put(item.UserId,accs);
                }
                else{
                    mapAccIdwithUserId.get(item.UserId).add(item.AccountId);
                }
                allAccId.add(item.AccountId);
            }
        }
        
        Map<Id,string> mapuserIdwithEmail = new Map<Id,string>();
        for(User item :[select Id,Email from User where Id in :mapAccIdwithUserId.keyset()]){
            if(!mapuserIdwithEmail.containsKey(item.Id)){
                mapuserIdwithEmail.put(item.Id,item.Email);
            }
        }

        Map<string,string> mapEmailwithTechId = new Map<string,string>();        
        for(SVMXC__Service_Group_Members__c tech :[SELECT Id,SVMXC__Email__c 
                                                FROM SVMXC__Service_Group_Members__c 
                                                WHERE SVMXC__Email__c in :mapuserIdwithEmail.values()]){
            if(!mapEmailwithTechId.containsKey(tech.SVMXC__Email__c)){
                mapEmailwithTechId.put(tech.SVMXC__Email__c, tech.Id);
            }
        }
        
        Map<Id,List<SVMXC__Service_Order__c>> mapAccIdwithWo = new Map<Id,List<SVMXC__Service_Order__c>>();
        for(SVMXC__Service_Order__c wo :[Select Id,SVMXC__Company__c from SVMXC__Service_Order__c where SVMXC__Company__c in:allAccId]){
            if(!mapAccIdwithWo.containsKey(wo.SVMXC__Company__c)){
                List<SVMXC__Service_Order__c> wos = new List<SVMXC__Service_Order__c>{wo};
                mapAccIdwithWo.put(wo.SVMXC__Company__c,wos);
            }
            else{
                mapAccIdwithWo.get(wo.SVMXC__Company__c).add(wo);
            }
        }
        
        
        Map<Id,List<SVMXC__Installed_Product__c>> mapAccIdwithInstalled = new Map<Id,List<SVMXC__Installed_Product__c>>();
        
        for(SVMXC__Installed_Product__c ip :[Select Id,SVMXC__Company__c from SVMXC__Installed_Product__c where SVMXC__Company__c in:allAccId]){
            if(!mapAccIdwithInstalled.containsKey(ip.SVMXC__Company__c)){
                List<SVMXC__Installed_Product__c> ips = new List<SVMXC__Installed_Product__c>{ip};
                mapAccIdwithInstalled.put(ip.SVMXC__Company__c,ips);
            }
            else{
                mapAccIdwithInstalled.get(ip.SVMXC__Company__c).add(ip);
            }
        }
        
        List<SVMXC__Service_Order__c> woToupdate = new List<SVMXC__Service_Order__c>();
        List<Account> accToUpdate = new List<Account>();
        List<SVMXC__Installed_Product__c> installToUpdate = new List<SVMXC__Installed_Product__c>();
        for(Id userId :mapAccIdwithUserId.keyset()){
            if(mapuserIdwithEmail.containsKey(userId)){
                string email = mapuserIdwithEmail.get(userId);
                if(mapEmailwithTechId.containsKey(email)){
                    string techId = mapEmailwithTechId.get(email);
                    for(Id accId :mapAccIdwithUserId.get(userId)){
                        Account accEntity = new Account(id = accId);
                        accEntity.SVMXC__Preferred_Technician__c = techId;
                        accToUpdate.add(accEntity);
                        for(SVMXC__Service_Order__c workOrder :mapAccIdwithWo.get(accId)){
                            workOrder.SVMXC__Preferred_Technician__c = techId;
                            woToupdate.add(workOrder);
                        }
                        for(SVMXC__Installed_Product__c installeedProduct :mapAccIdwithInstalled.get(accId)){
                            installeedProduct.SVMXC__Preferred_Technician__c = techId;
                            installToUpdate.add(installeedProduct);
                        }
                    }
                }
            }
        }
        if(woToupdate.size() > 0){
            System.Debug('woLimit1'+Limits.getCpuTime());
            update woToupdate;
            System.Debug('woLimit2'+Limits.getCpuTime());
        }
        if(accToUpdate.size() > 0){
            update accToUpdate;
        }
        if(installToUpdate.size() > 0){
            System.Debug('assetLimit1'+Limits.getCpuTime());
            update installToUpdate;
            System.Debug('assetLimit2'+Limits.getCpuTime());
        }
    }
}