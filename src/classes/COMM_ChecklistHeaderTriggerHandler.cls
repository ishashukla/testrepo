/**================================================================      
* Appirio, Inc
* Name: COMM_ChecklistHeaderTriggerHandler
* Description: T-534013 Handler class for ChecklistHeaderTrigger
* Created Date: 09 Sep 2016
* Created By: Kanika Mathur (Appirio)
*
* Date Modified      Modified By      Description of the update
* 12 Sep 2016        Kanika Mathur    T-534015: updateQIPFieldsOnAsset() method to update QIP Asset fields
* 13 Sep 2016        Kanika Mathur    T-534015: updateQIPStatusToInProgress() method to update QIP Status on Asset
==================================================================*/
public class COMM_ChecklistHeaderTriggerHandler {
  public  static final String Comment = 'Comment';
  public  static final String Passed = 'Passed';
  public  static final String Picklist = 'Picklist';
  public  static final String Failed = 'Failed';
  public  static final String Pass = 'Pass';
  public  static final String Incomplete = 'Incomplete';
  public  static final String Fail = 'Fail';
  public  static final String KeyNull = 'Null';
  
  private static Map<Id, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Checklist_Header__c.getRecordTypeInfosById();
  
  //================================================================      
  // Name           : onBeforeUpdate
  // Description    : Trigger Method called on before update
  //================================================================== 
  public static void onBeforeUpdate(List<Checklist_Header__c> newList, Map<Id, Checklist_Header__c> oldMap) {
    evaluateResult(newList,oldMap); //SD 10/6: COMM: T-542383
    populateCompletedByAndOnFields(newList, oldMap);
  }
  
  //================================================================      
  // Name           : onAfterUpdate
  // Description    : Trigger Method called on after update
  //================================================================== 
    public static void onAfterUpdate(List<Checklist_Header__c> newList, Map<Id, Checklist_Header__c> oldMap) {
      //system.debug('First update check ' + newList.get(0).status__c);
        updateQIPFieldsOnAsset(newList, oldMap);
    }
    
    //================================================================      
  // Name           : onAfterInsert
  // Description    : Trigger Method called on after insert
  //================================================================== 
    public static void onAfterInsert(List<Checklist_Header__c> newList) {
      //updateQIPStatusToInProgress(newList);
    }
    
    //================================================================      
  // Name           : onBeforeInsert
  // Description    : Trigger Method called on before insert
  //================================================================== 
    public static void onBeforeInsert(List<Checklist_Header__c> newList){
      updateIsLatestOnClone(newList);
    }
    
    //================================================================      
  // Name           : updateIsLatestOnClone
  // Description    : This method sets isLatest__c flag to false for 
  //                  older checklists header on clone
  //================================================================== 
    private static void updateIsLatestOnClone(List<Checklist_Header__c> newList){
      Map<ID, ID> assetLatestChecklistHeaderMap = new Map<ID, ID>();
      for(Checklist_Header__c header : newList){
        if((rtMap.get(header.RecordtypeID).getName().contains(Constants.COMM))) {
        //system.debug('Header Asset : ' + header.Asset__c);
          if(!assetLatestChecklistHeaderMap.containsKey(header.Asset__c)){
            assetLatestChecklistHeaderMap.put(header.Asset__c, header.id);
          }
          header.isLatest__c = true;
        }
        //system.debug('Setting is latest true: ' + header.isLatest__c);
        //system.debug('Map : ' + assetLatestChecklistHeaderMap);
      }
      List<Checklist_Header__c> obsoleteHeaders = [SELECT isLatest__c, id 
                                                  FROM Checklist_Header__c 
                                                  WHERE Asset__c in :assetLatestChecklistHeaderMap.keyset()
                                                  AND isLatest__c = true];
    if(obsoleteHeaders != null && obsoleteHeaders.size() > 0){                                                
      for(Checklist_Header__c oldHeader : obsoleteHeaders){
        oldHeader.isLatest__c = false;
      }       
      if(obsoleteHeaders.size()>0) {
        update obsoleteHeaders;
      }
      //system.debug('Setting is latest false: ' + obsoleteHeaders);
      }
    }
    
    //================================================================      
  // Name           : evaluateResult
  // Description    : Used to Evaluate Result
  // Modified Date  : 06th Oct 2016 
  // Modified By    : Shubham Dhupar(Appirio)
  // Task           : T-542383
  //================================================================== 
    
  public static void evaluateResult(List<Checklist_Header__c> newList, Map<Id, Checklist_Header__c> oldMap)  {
    //system.debug('Inside Evaluate Result' + newList.get(0).Status__c);
    Set<Id> lstHeader = new Set<Id>(); 
    for(Checklist_Header__c headerCheck: newList) {
      //system.debug('Condition does not meet!!!' + headerCheck.Evaluate_Result__c );
      if((oldmap == null || 
      (headerCheck.Evaluate_Result__c != oldMap.get(headerCheck.Id).Evaluate_Result__c)) 
      && headerCheck.Evaluate_Result__c == true && (rtMap.get(headerCheck.RecordtypeID).getName().contains(Constants.COMM)) ) {
        lstHeader.add(headerCheck.id);
        //system.debug('Condition meets');
      }
    }
    if(lstHeader.size() > 0) { 
      
      List<CheckList_Detail__c> details = [SELECT Id, Result__c, Type__c, Required__c, 
                                            Checklist_Header__c,Comment__c,
                                            CheckList_Item__c, CheckList_Item__r.Required__c 
                                            FROM CheckList_Detail__c WHERE Checklist_Header__r.ID in :lstHeader];
      
      String returnMessage= '';
      String headerId = '';
      Map<Id, String> checklistHeaderIdToResultMap = new Map<Id, String>();
          String results='';
          if(details != null && details.size() > 0){                                                                                    
            for(CheckList_Detail__c chDetail : details) {
              if(chDetail.Required__c == true) {
                if(chDetail.Type__c == Comment) {
                  //system.debug('Comment : ' + chDetail.Comment__c);
                  if(chDetail.Comment__c == null ||
                    chDetail.Comment__c == '') {
                    results += KeyNull;
                  } else {
                     results += Passed;
                  }
                }else if(chDetail.Type__c == Picklist) {
                  if(chDetail.Result__c == Failed)  {
                    results += Failed;
                  }else if(chDetail.Result__c == null
                    || chDetail.Result__c == '') {
                      results += KeyNull;
                  }else {
                      results += Passed;
                  }
                }
              }
              headerId = chDetail.Checklist_Header__c;
            }
            checklistHeaderIdToResultMap.put(headerId, results);
          }
          
      
      for(Checklist_Header__c headerCheck: newList) {
        if(checklistHeaderIdToResultMap.containsKey(headerCheck.Id)) {
          if(!checklistHeaderIdToResultMap.get(headerCheck.Id).contains(KeyNull) 
          && !checklistHeaderIdToResultMap.get(headerCheck.Id).contains(Failed)) {
            if(!String.isBlank(checklistHeaderIdToResultMap.get(headerCheck.Id))) {
                headerCheck.Status__c = Pass;
                returnMessage = Label.COMM_Checklist_Details_Passed;
                //system.debug('<<<<<inPass');
            }
          } else if(checklistHeaderIdToResultMap.get(headerCheck.Id).contains(KeyNull)) {
            //Updated Status__c to Incomplete when a required answer is missing - KM 20 Sep 2016
            headerCheck.Status__c = Incomplete;
            returnMessage = Label.COMM_Checklist_Details_Missing;
              //system.debug('Header Status ' +   headerCheck.Status__c );
            //system.debug('<<<<<inMissing');
          } else if(checklistHeaderIdToResultMap.get(headerCheck.Id).contains(Failed)) {
            headerCheck.Status__c = Fail;
            returnMessage = Label.COMM_Checklist_Details_Failed;
              
            //system.debug('<<<<<inFail');
          }
        }
        //system.debug('Return message : ' + returnMessage);
        headerCheck.Reason__c = returnMessage;
        headerCheck.Evaluate_Result__c =false;
        //system.debug('Header Status222 ' +   headerCheck.Status__c );
        //system.debug('Setting evaluate result to false');
      }
      //system.debug(returnMessage);
      
    }
  }
  
    //================================================================      
  // Name         : updateQIPFieldsOnAsset
  // Description  : Used to update QIP fields on Asset when Checklist Header result is Pass.
  // Modified Date : 12th Sept 2016 
  // Modified By   : Kanika Mathur(Appirio)
  // Task         : T-534015
  //================================================================== 
    private static void updateQIPFieldsOnAsset(List<Checklist_Header__c> newList, Map<Id, Checklist_Header__c> oldMap) {
      //system.debug('In Function');
    Map<Id, Id> checklistAssetMap = new Map<Id, Id>();
    List<SVMXC__Installed_Product__c> assetsToUpdate = new List<SVMXC__Installed_Product__c>();
    Map<Id, Checklist_Header__c> checklistMap = new Map<ID, Checklist_Header__c>();
    for(Checklist_Header__c checkList : newList){
      if(oldMap.get(checkList.Id).Status__c != checkList.Status__c
        && checkList.Status__c == Pass && (rtMap.get(checkList.RecordtypeID).getName().contains(Constants.COMM))) {
        if(!checklistAssetMap.containsKey(checkList.Asset__c)){
          checklistAssetMap.put(checkList.Asset__c, checkList.id);
        }
        checklistMap.put(checkList.id, checkList);
      }
    }
    if(checklistAssetMap != null && checklistAssetMap.keyset().size() > 0){
      for(SVMXC__Installed_Product__c asset : [SELECT Id, QIP_Completion_Date__c, Case_Owner__c, QIP_Status__c
                                              FROM SVMXC__Installed_Product__c
                                              WHERE Id in :checklistAssetMap.keyset()]) {
        if(asset.QIP_Status__c != Constants.COMPLETED){
          asset.QIP_Completion_Date__c = Date.valueof(checklistMap.get(checklistAssetMap.get(asset.Id)).Completed_On__c);
          asset.Case_Owner__c = checklistMap.get(checklistAssetMap.get(asset.Id)).Completed_By__c;
          asset.QIP_Status__c = Constants.COMPLETED;
          assetsToUpdate.add(asset);
        }
      }
    }
    if(assetsToUpdate.size() > 0){
      //system.debug('In Update');
      update assetsToUpdate;
    }
    //system.debug('Out Update');
  }
  
    //================================================================      
  // Name         : updateQIPFieldsOnAsset
  // Description  : Used to update Compketed By and Completed On fields on Asset when Checklist Header result is Pass/Fail.
  // Modified Date : 12th Sept 2016 
  // Modified By   : Kanika Mathur(Appirio)
  // Task         : T-534015
  //==================================================================    
  private static void populateCompletedByAndOnFields(List<Checklist_Header__c> newList, Map<Id, Checklist_Header__c> oldMap) {
    for(Checklist_Header__c ch : newList) {
      if(ch.Status__c != oldMap.get(ch.Id).Status__c
        &&(ch.Status__c == Pass || ch.Status__c == Fail) && (rtMap.get(ch.RecordtypeID).getName().contains(Constants.COMM))) {
        ch.Completed_By__c = UserInfo.getUserId();
        ch.Completed_On__c = Date.valueOf(System.now());
      }
    }
  }
  
  //================================================================      
  // Name         : updateQIPStatusToInProgress
  // Description  : Used to update QIP Status on Asset when ChecklistHeader id inserted.
  // Modified Date : 13th Sept 2016 
  // Modified By   : Kanika Mathur(Appirio)
  // Task         : T-534015
  //==================================================================   
  
}