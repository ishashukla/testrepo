/*******************************************************************************
 Author        :  Appirio JDC (Megha Agarwal)
 Date          :  Oct 5th, 2015
 Description       :  Medi_SmartContactSeachExtension
*******************************************************************************/
public with sharing class Medi_SmartContactSeachExtension {
    //Search criteria fields
  public String contactFirstNameToSearch {set;get;}
  public String contactLastNameToSearch {set;get;}
  public String contactEmail {set;get;}
  //public String contactPhone {set;get;}
  public boolean isAsc{set; get;}
  public Integer showingFrom{get;set;}
  public Integer showingTo{get;set;}
  //public boolean showContactButton{set; get;}
  public boolean hasNext{get;set;}
  public boolean hasPrevious{get;set;}
  public String requestedPage {get;set;}
  public integer totalResults {set; get;}
  public Integer totalPage {set; get;}
  public string query;
  public integer searchCount{set; get;}
  public string searchStatus{set; get;}
  //public string sortField{set;get;}
  //private string previousSortField;
  private string sortOrder;
  private static final Integer DEFAULT_RESULTS_PER_PAGE = 20;
  //private static final string SEARCH_TYPE = ' and ';
  //private static final string DEFAULT_SORT_ORDER = ' ASC ';
  //private static final string DEFAULT_SORT_FIELD = 'Name';
  public String selectedContactId {get;set;}
  public ApexPages.StandardSetController contactResults;
  //private Account currentAccount;
  //public List<Contact_Acct_Association__c> lstAC {get;set;}
  private List<Contact_Acct_Association__c> lstAssociations;

  private Set<Id> associatedContactIds;

  private PageReference canceURL;

  //-----------------------------------------------------------------------------------------------
  // Constructor
  //-----------------------------------------------------------------------------------------------
  public Medi_SmartContactSeachExtension(ApexPages.StandardController controller) {
  	canceURL = controller.cancel();
    resetSearchStatus();
    fillAssociatedContactIds();
    //lstAC = new List<Contact_Acct_Association__c>();
  }
  //public SmartContactSearchExtention(){

    //resetSearchStatus();
    //lstAC = new List<Contact_Acct_Association__c>();
  //}

  public Account currentAccount{
    get{
        Account currentAc;
        String accountId = ApexPages.currentPage().getParameters().get('accId');
      if(String.isBlank(accountId) == false){
        List<Account> accList = [Select Id, Name,AccountNumber FROM Account Where Id = :accountId];
        if(accList != null && accList.size() > 0){
          currentAc = accList.get(0);
        }
      }
      return currentAc;
    }
  }

  private void fillAssociatedContactIds(){
    associatedContactIds = new Set<Id>();
    for(Contact_Acct_Association__c ca :[SELECT Id, Associated_Contact__c FROM Contact_Acct_Association__c WHERE Associated_Account__c = :currentAccount.Id]){
      associatedContactIds.add(ca.Associated_Contact__c);
    }
  }

  //-----------------------------------------------------------------------------------------------
  // Action method for reset search
  //-----------------------------------------------------------------------------------------------
  public void resetSearchStatus(){
    //Reset Contact fields
    //showContactButton = false;
    //lstAssociations = new List<Contact_Acct_Association__c>();
    searchCount = 0;
    searchStatus = '';
    //sortOrder = DEFAULT_SORT_ORDER;
    //sortField = DEFAULT_SORT_FIELD;
    //previousSortField = DEFAULT_SORT_FIELD;
    contactFirstNameToSearch = '';
    contactLastNameToSearch = '';
    contactEmail = '';
    //contactPhone = '';
    isAsc = true;
    hasPrevious = false;
    hasNext = false;
  }





  /*
  private Boolean isContactAssociationAlreadyExists(List<Contact_Acct_Association__c> lst){
    if(currentAccount != null){
        for(Contact_Acct_Association__c ca: lst){
        if(ca.Associated_Account__c == currentAccount.Id){
          return true;
        }
      }
    }
    return false;
  }*/

  //-----------------------------------------------------------------------------------------------
  // Action method for Search
  //-----------------------------------------------------------------------------------------------
  public void performSearch(){
    searchContact();
  }


  private static Id caseRecordTypeId{
    get{
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Case.getRecordTypeInfosByName();
      return rtMapByName.get('Medical - Non Product Issue').getRecordTypeId();
    }
  }
  //-----------------------------------------------------------------------------------------------
  // Action method for create Case
  //-----------------------------------------------------------------------------------------------
  public void createCase(){
    //String selectedContactId = ApexPages.currentPage().getParameters().get('selectedContactId');
    //System.debug('@@@' + selectedContactId);
    if(currentAccount == null || String.isBlank(selectedContactId)){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid input.'));
      return;
    }

    // Create Case
    Case objCase = new Case();
    objCase.RecordTypeId = caseRecordTypeId;
    objCase.ContactId = selectedContactId;
    objCase.AccountId = currentAccount.Id;
    insert objCase;
  }

  //-----------------------------------------------------------------------------------------------
  // Action method for show Associated Accounts
  //-----------------------------------------------------------------------------------------------
  public void showAssociatedAccounts(){
    //String selectedContactId = ApexPages.currentPage().getParameters().get('selectedContactId');
    //System.debug('@@@' + selectedContactId);
    if(currentAccount == null || String.isBlank(selectedContactId)){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid input.'));
        return;
    }
    //Create new junction entry;
    Contact_Acct_Association__c ca = new Contact_Acct_Association__c();
    ca.Associated_Account__c = currentAccount.Id;
    ca.Associated_Contact__c = selectedContactId;
    insert ca;
    fillAssociatedContactIds(); 
    /*lstAC = [SELECT Id, Name, Associated_Account__c, Associated_Account__r.Name, Associated_Contact__c, Associated_Contact__r.Name
              FROM Contact_Acct_Association__c
              WHERE Associated_Contact__c = :selectedContactId];*/
  }


  //-----------------------------------------------------------------------------------------------
  // Action method for Cancel
  //-----------------------------------------------------------------------------------------------
  public PageReference cancel(){
  	return canceURL;
  	//Pagereference pg = new Pagereference('/' + currentAccount.Id);
    //return pg;
  }

  //-----------------------------------------------------------------------------------------------
  // Action method for Next
  //-----------------------------------------------------------------------------------------------
  /*
  public PageReference nextContactPage(){
    if(contactResults.getHasNext()) {
      contactResults.next();
      lstAssociations = contactResults.getRecords();
      showingFrom = showingFrom + contactResults.getPageSize();
      showingTo =  showingTo + lstAssociations.size();
      if(contactResults.getHasNext()) {
        hasNext = true;
      }
      else {
        hasNext = false;
      }
      hasPrevious = true;
    }
    requestedPage = String.valueOf(contactResults.getPageNumber());
    return null;
  }*/

  //-----------------------------------------------------------------------------------------------
  // Action method for Sort Data
  //-----------------------------------------------------------------------------------------------
  /*
  public void sortData(){
    if (previousSortField.equals(sortField)){
      isAsc = !isAsc;
    }
    else{
      isAsc = true;
    }
    sortOrder = isAsc ? ' ASC ' : ' DESC ';
    previousSortField = sortField;
    searchContact();
  }*/


  //-----------------------------------------------------------------------------------------------
  // Action method for Previous
  //-----------------------------------------------------------------------------------------------
  /*
  public PageReference previousContactPage(){
    if(contactResults.getHasPrevious()) {
        showingTo =  showingTo - lstAssociations.size();
      contactResults.previous();
      lstAssociations = contactResults.getRecords();
      showingFrom = showingFrom - contactResults.getPageSize();
      hasNext = true;
      if(contactResults.getHasPrevious()) {
        hasPrevious = true;
      }
      else {
        hasPrevious = false;
      }
    }
    requestedPage = String.valueOf(contactResults.getPageNumber());
    return null;
  }*/

  //-----------------------------------------------------------------------------------------------
  // Action method for Go to Page
  //-----------------------------------------------------------------------------------------------
  /*
  public PageReference goToPageNumber(){
    Boolean check = pattern.matches('[0-9]+',requestedPage);
    Integer pageNo = check? Integer.valueOf(requestedPage) : 0;

    if(pageNo == 0 || pageNo > totalPage){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid page number.'));
      return null;
    }

    contactResults.setPageNumber(pageNo);
    lstAssociations = contactResults.getRecords();
    if(contactResults.getHasPrevious()) {
        hasPrevious = true;
    }
    else {
      hasPrevious = false;
    }

    if(contactResults.getHasNext()) {
      hasNext = true;
    }
    else {
      hasNext = false;
    }
    showingFrom  = (pageNo - 1) * contactResults.getPageSize() + 1;
    showingTo = showingFrom + lstAssociations.size() - 1;
    if(showingTo > totalResults) {
        showingTo = totalResults;
    }
    return null;
  }
  */


  private Boolean isAnyFilterExist{
    get{
        String firstName = String.escapeSingleQuotes(contactFirstNameToSearch.trim());
      String lastName = String.escapeSingleQuotes(contactLastNameToSearch.trim());
      String email = String.escapeSingleQuotes(contactEmail.trim());
      if(String.isBlank(firstName) && String.isBlank(lastName) && String.isBlank(email)){
        return false;
      }
      return true;
    }
  }

  //-----------------------------------------------------------------------------------------------
  // Helper method to search Contact and make list according to pagesize
  //-----------------------------------------------------------------------------------------------
  private void searchContact(){
    //showContactButton = true;
    query = 'SELECT Id, Primary__c, Associated_Contact__r.Title, Associated_Contact__r.Phone, Associated_Contact__r.Owner.Name, '+
            'Associated_Contact__r.Name,Associated_Contact__r.FirstName, Associated_Contact__r.LastName, Associated_Contact__r.Email,  '+
            'Associated_Account__r.Oracle_Account_Number__c, Associated_Account__r.Name '+
            'FROM Contact_Acct_Association__c';
            //'WHERE Associated_Contact__r.RecordType.Name = \'Endo Customer Contact\'';


    //query = 'Select Title, Phone, Owner.Name, Name, Email, Account.Name From Contact c ';
    query = findSearchCondition(query);

    query += ' order by Primary__c desc'; // Do not change this condition, we are filling a map on based of this
    //query += ' order by ' + sortField + sortOrder + ' nulls last'  ;
    System.debug('@@@' + query);
    try{
      lstAssociations = new List<Contact_Acct_Association__c>();
      contactResults = new ApexPages.StandardSetController(Database.query(query));
      contactResults.setPageSize(DEFAULT_RESULTS_PER_PAGE);
      lstAssociations = contactResults.getRecords();
      searchCount = contactResults.getResultSize();
    }
    catch(Exception e){
      searchCount = 0;
    }

    if(searchCount  == 0){
      searchStatus = 'No matching results found.';
    }
    requestedPage = String.valueOf(contactResults.getPageNumber());
    showingFrom = 1;


    totalResults = searchCount;

    //for(List<Sobject> recordBatch:Database.query(query)){
    //  totalResults = totalResults + recordBatch.size();
    //}

    totalPage = 0;
    totalPage = totalResults / contactResults.getPageSize() ;

    if(totalPage * contactResults.getPageSize() < totalResults){
      totalPage++;
    }

    if(searchCount < contactResults.getPageSize()) {
      showingTo = searchCount;
    }
    else {
        showingTo = contactResults.getPageSize();
    }

    if(contactResults.getHasNext()) {
      hasNext = true;
    }
    else {
      hasNext = false;
    }
    hasPrevious = false;
  }


  //-----------------------------------------------------------------------------------------------
  // Helper method for search condition
  //-----------------------------------------------------------------------------------------------
  public String findSearchCondition(String query){
    String firstName = String.escapeSingleQuotes(contactFirstNameToSearch.Trim());
    String lastName = String.escapeSingleQuotes(contactLastNameToSearch.Trim());
    String email = String.escapeSingleQuotes(contactEmail.trim());

    if(String.isBlank(firstName) == false){
      if(query.toUpperCase().contains('WHERE')){
        //query += ' and FirstName like \'%' + firstName + '%\'';
        query += ' and Associated_Contact__r.FirstName like \'%' + firstName + '%\'';

      }
      else{
        //query += ' where FirstName like \'%' + firstName +  '%\'';
        query += ' where Associated_Contact__r.FirstName like \'%' + firstName + '%\'';
      }
    }

    if(String.isBlank(lastName) == false){
      if(query.toUpperCase().contains('WHERE')){
        //query += ' and LastName like \'%' + lastName.Trim() + '%\'';
        query += ' and Associated_Contact__r.LastName like \'%' + lastName.Trim() + '%\'';
      }
      else{
        //query += ' where LastName like \'%' + lastName.Trim() +  '%\'';
        query += ' where Associated_Contact__r.LastName like \'%' + lastName.Trim() +  '%\'';
      }
    }

    if(String.isBlank(email) == false){
      if(query.toUpperCase().contains('WHERE')){
        query += ' and Associated_Contact__r.Email like \'%' + email.Trim() + '%\'';
      }
      else{
        query += ' where Associated_Contact__r.Email like \'%' + email.Trim() +  '%\'';
      }
    }

    // On first load or no filter exist.
    if(isAnyFilterExist == false){
      if(query.toUpperCase().contains('WHERE')){
        query += ' AND Associated_Account__c = \'' + currentAccount.Id + '\'';
      }
      else{
        query += ' WHERE Associated_Account__c = \'' + currentAccount.Id + '\'';
      }
    }
    else{
      //if(query.toUpperCase().contains('WHERE')){
      //  query += ' AND Primary__c = true';
      //}
      //else{
      //  query += ' WHERE Primary__c = true';
      //}
    }

    return query;
  }

  //-----------------------------------------------------------------------------------------------
  // Helper method for getWrapperList
  //-----------------------------------------------------------------------------------------------
  public List<ContactWrapper> getWrapperList() {
    searchContact();
    List<ContactWrapper> lst = new List<ContactWrapper>();
    //Boolean isExist = isContactAssociationAlreadyExists(lstAssociations);
    //System.debug('@@@' + associatedContactIds);
    System.debug('@@@' + lstAssociations);

    for(Contact_Acct_Association__c ca: lstAssociations){
        System.debug('@@@acc' + ca.Associated_Account__r.Name + '@@' + ca.Primary__c);
    }

    Map<Id, List<String>> mapPrimarAccount = new Map<Id, List<String>>();
    for(Contact_Acct_Association__c ca: lstAssociations){
      ContactWrapper obj = new ContactWrapper();
      obj.ca = ca;
      Boolean addInList = false;
      if(isAnyFilterExist == false){
        obj.showOnlyCurrentAccountContacts = true;
        obj.showAssociateAccountLink = false;
        addInList = true;
      }
      else{
        obj.showOnlyCurrentAccountContacts = false;
        if(associatedContactIds.contains(ca.Associated_Contact__c) == false){
          obj.showAssociateAccountLink = true;
        }
        else{
          obj.showAssociateAccountLink = false;
        }

        if(ca.Primary__c == true){
          if(mapPrimarAccount.containsKey(ca.Associated_Contact__r.Id) == false){
            mapPrimarAccount.put(ca.Associated_Contact__r.Id, new List<String>());
            addInList = true;
          }

        }
        else{
          if(mapPrimarAccount.get(ca.Associated_Contact__r.Id) != null){
            mapPrimarAccount.get(ca.Associated_Contact__r.Id).add(ca.Associated_Account__r.Name);
          }
        }

      }


      if(addInList == true){
        lst.add(obj);
      }

      searchCount = lst.size();
    }

    System.debug('@@@' + mapPrimarAccount);
    for(ContactWrapper obj :lst){
      if(mapPrimarAccount.get(obj.ca.Associated_Contact__r.Id) != null && mapPrimarAccount.get(obj.ca.Associated_Contact__r.Id).size() > 0){
        obj.lstSecondaryAccounts = mapPrimarAccount.get(obj.ca.Associated_Contact__r.Id);
        System.debug('@@@');
      }
    }
    //System.debug('@@@lst' + lst);
    return lst;
  }

  public class ContactWrapper{
    public Contact_Acct_Association__c ca{get;set;}
    public Boolean showAssociateAccountLink{get;set;}
    public Boolean showOnlyCurrentAccountContacts{get;set;}
    public List<String> lstSecondaryAccounts{get;set;}
      public ContactWrapper(){

      }
  }


}