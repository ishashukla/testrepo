/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMembersList
 * Description: Controller for AccountTeamMembers.page
 * Created Date: 
 * Created By: 
 *
 * Date Modified         Modified By           Description of the update
 * April 6, 2016         Meghna Vijay          Added Method to add Default Account Team To 
 *                                             Account Team Member Related List ref.-(I-210282)
 * November 15, 2016 	 Ralf Hoffmann		   I-244198 : added account.comm_sales_rep to query
  =====================================================================*/
public with sharing class AccountTeamMembersList {
  
  public List<accountTeamWrapper> accountTeamMembers {get;set;}
  public List<accountTeamWrapper> newAccountTeamMembers {get;set;}
  public Account acc {get;set;}
  public Boolean showAddMemberSection {get;set;}
  public Boolean displayAccess {get;set;}
  public List<selectOption> accountOppAccessLevels {get;set;}
  public List<selectOption> caseAccessLevels {get;set;}
  public Id selectedId {get;set;}
  public Integer listSize {get;set;}
  public List<accountTeamWrapper> accountTeamMembersToShow {get;set;}
  public Integer currentSizeShown {get;set;}
  public Boolean reachedMax {get;set;}
  
  //==========================================================================
  // Constructor
  //==========================================================================
  public AccountTeamMembersList ( ApexPages.Standardcontroller std) {
    showAddMemberSection = false;
    displayAccess = false;
    currentSizeShown = 5;
    reachedMax = false;
    accountTeamMembersToShow = new List<accountTeamWrapper>();
    accountOppAccessLevels = new List<selectOption>();
    accountOppAccessLevels.add(new selectOption('Read','Read Only'));
    accountOppAccessLevels.add(new selectOption('Edit','Read/Write'));
    caseAccessLevels = new List<selectOption>();
    caseAccessLevels.add(new selectOption('None','Private'));
    caseAccessLevels.add(new selectOption('Read','Read Only'));
    caseAccessLevels.add(new selectOption('Edit','Read/Write'));

    accountTeamMembers = new List<accountTeamWrapper>();
    Id accId = std.getRecord().Id;
    acc = null;
    if(accId != null) {
      acc = [SELECT Id,Name, OwnerID 
             FROM Account
             WHERE Id = :accId];
      fetchExistingAccountTeamMembers();
    }

    listSize = accountTeamMembers.size();
    if( listSize == 5) {
        reachedMax = true;
    }
    if( ApexPages.currentPage().getParameters().get('showAddMemberSection') != null && 
     ApexPages.currentPage().getParameters().get('showAddMemberSection') != '' ) {
      //showAddMemberSection = true;
      addTeamMembers();
    }
  }
  
  //==========================================================================
  // Method to fetch the existing Account Team Members and their access levels
  //==========================================================================
  public void fetchExistingAccountTeamMembers () {
    accountTeamMembers = new List<accountTeamWrapper> ();
    accountTeamMembersToShow = new List<accountTeamWrapper>();
    integer i = 0;
    map<Id,AccountShare> accShareMap = new map<Id,AccountShare>();
    for ( AccountShare accShare : [SELECT Id,OpportunityAccessLevel, CaseAccessLevel, UserOrGroupId
                                   FROM AccountShare
                                   WHERE AccountId = :acc.Id]) {
      accShareMap.put(accShare.UserOrGroupId,accShare);
      }
      System.debug('accShareMap'+accShareMap);
      //I-244198 RH 11/15/16: added account.comm_sales_rep__c
    for ( AccountTeamMember atm : [SELECT Id,TeamMemberRole,UserId,AccountId,
                                          AccountAccessLevel ,Account.OwnerID,Account.COMM_Sales_Rep__c,
                                          User.Name
                                   FROM AccountTeamMember 
                                   WHERE AccountId = :acc.Id
                                   order by User.Name ASC]) {
      accountTeamWrapper atWrap = new accountTeamWrapper();
      atWrap.member = atm;
      if(accShareMap != null && accShareMap.containsKey(atm.UserId)) {
        AccountShare accShare = accShareMap.get(atm.UserId);
        atWrap.caseAccess = accShare.CaseAccessLevel;
        atWrap.opportunityAccess = accShare.OpportunityAccessLevel;
      }
      accountTeamMembers.add(atWrap);
      if( i < 5) {
        accountTeamMembersToShow.add(atWrap);
        i++;
      }
      
    }
  }
  
  //==========================================================================
  // Method to create new members list and display the add team members section
  //==========================================================================
  public void addTeamMembers () {
    newAccountTeamMembers = new List<accountTeamWrapper>();
    for ( integer i=0; i<5; i++) {
      accountTeamWrapper atm = new accountTeamWrapper();
      atm.member.AccountId = acc.Id;
      newAccountTeamMembers.add(atm);
    }
    showAddMemberSection = true;
  }
  
  //==========================================================================
  // Method to show the access levels on the list view
  //==========================================================================
  public void showAccess (){
    displayAccess = true;
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Account team members may have greater access than defined by their account team membership.'));
  }
  
  //==========================================================================
  // Method to save the newly created team members
  //==========================================================================
  public pagereference saveNewTeamMembers () {
    List<AccountTeamMember> atmToInsert = new List<AccountTeamMember>();
    List<AccountShare> accshareToInsert = new List<AccountShare>();
    List<ContactShare> contactSharesToInsert = new List<ContactShare>();
    Map<ID,Set<ID>> accountIDUserMap = new Map<ID,Set<ID>>();
    try {
      for ( accountTeamWrapper atmWrap : newAccountTeamMembers) {
        if ( atmWrap.member.userId != null ) {
          atmToInsert.add(atmWrap.member);
          System.debug('---atmWrap.member.AccountAccessLevel---'+atmWrap.accountAccess);
          if ( atmWrap.accountAccess == 'Edit') {
            AccountShare accshare = new AccountShare();
            accshare.AccountAccessLevel = atmWrap.accountAccess;
            accshare.AccountId = atmWrap.member.AccountId;
            accshare.OpportunityAccessLevel = atmWrap.opportunityAccess;
            accshare.CaseAccessLevel = atmWrap.caseAccess;
            accshare.UserOrGroupId = atmWrap.member.userId;
            accshareToInsert.add(accshare);
            if(accountIDUserMap.containsKey(atmWrap.member.accountID)){
                accountIDUserMap.get(atmWrap.member.accountID).add(atmWrap.member.userID);
            }else{
                accountIDUserMap.put(atmWrap.member.accountID,new Set<ID>{atmWrap.member.userID});
            }
          }
        }
      }
      for(Contact contacts : [SELECT Private__c, AccountID, ID from Contact WHERE AccountId in :accountIDUserMap.keyset()]){
        if(!contacts.private__c){
            for(ID userID : accountIDUserMap.get(contacts.AccountID)){
                ContactShare conShare = new ContactShare();
                conShare.UserOrGroupId = userID;
                conShare.ContactAccessLevel = 'Read';
                conShare.contactID = contacts.ID;
                contactSharesToInsert.add(conShare);
            }
        }
      }
      
      if (!atmToInsert.isEmpty()) {
        insert atmToInsert;
      }
      if (!accshareToInsert.isEmpty()) {
        Database.insert(accshareToInsert,false);
      }
      if(contactSharesToInsert != null){
          Database.insert(contactSharesToInsert,false);
      }
      return new pagereference('/'+acc.Id);
    }
    catch ( exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, ex.getMessage()));
      return null;
    }
  }
  
  //==========================================================================
  // Method to save team members and show the blank list again for the user to enter more records
  //==========================================================================
  public void saveAndMore() {
    saveNewTeamMembers();
    addTeamMembers(); 
  }
  
  //==========================================================================
  // Method to cancel and return back to Account page
  //==========================================================================
  public pagereference doCancel () {
    newAccountTeamMembers = null;
    return new pagereference('/'+acc.Id);
  }
  
  //==========================================================================
  // Method to delete the account team member records
  //==========================================================================
  public PageReference doDelete() {
    try{
      if ( selectedId != null ) {
      //System.debug('selectedId '+selectedId );
        integer indexToDel = -1;
        for (accountTeamWrapper atWrap :accountTeamMembers) {
          indexToDel++;
          if ( atWrap.member.Id == selectedId ) {
            break;
          }
        }
        if( indextoDel != -1) {
          accountTeamMembers.remove(indextoDel);
        }
        AccountTeamMember atmToDel = [SELECT Id,UserId,AccountId FROM AccountTeamMember WHERE Id = :selectedId];
        //System.debug('atmToDel'+atmToDel);
        delete atmToDel;
        removeContactShare(atmToDel.userID, atmToDel.AccountID);
        fetchExistingAccountTeamMembers();
      }
    }
    catch ( exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
    }
    return null;
  }
  
  private void removeContactShare(ID UserID,ID AccountID){
      Map<Id, Contact> contactsOfAccount = new Map<ID, Contact>([SELECT ID FROM Contact WHERE AccountID = :accountID AND Private__c = false]);
      //System.debug('contactsOfAccount '+contactsOfAccount);
      List<ContactShare> contactShares = [SELECT ID FROM ContactShare WHERE ContactId in :contactsOfAccount.keyset() AND UserOrGroupId = :UserID AND RowCause = 'Manual'];
      //System.debug('contactShares'+contactShares);
      if(contactShares != null && contactShares.size() > 0){
         Database.delete( contactShares,false);
      }
  }
  //==========================================================================
  // Method to show next set of records in the detail page
  //==========================================================================
  public void showMoreRecords() {
    integer loopEndVariable;
    if( currentSizeShown+5 < accountTeamMembers.size() ) {
      loopEndVariable = currentSizeShown+5;
    }
    else {
      loopEndVariable = accountTeamMembers.size();
      reachedMax = true;
    }
    for( integer i=currentSizeShown; i < loopEndVariable; i++) {
      accountTeamMembersToShow.add(accountTeamMembers[i]);
    }
    currentSizeShown = accountTeamMembersToShow.size();
  }
  
  //-----------------------------------------------------------COMM I-210282 Start----------------------------------------------------------------//
  //Name             :     addDefaultAcctMember Method
  //Description      :     Method created to add Default Account Team
  //Created by       :     Meghna Vijay I-210282 
  //Created Date     :     April 6, 2016
  public PageReference addDefaultAcctMember() {
    //To populate Default Account Team Member of currently logged in user
    System.debug('Testing default member fuction called');
    List<UserAccountTeamMember> u = [SELECT Id,
                                      TeamMemberRole,
                                      AccountAccessLevel,
                                      OpportunityAccessLevel,
                                      ContactAccessLevel,
                                      CaseAccessLevel,
                                      UserId
                                      FROM UserAccountTeamMember
                                      WHERE OwnerId =: UserInfo.getUserId()];
    //To populate contact of the account owner
    List<Contact> conList = [SELECT Private__c, AccountID, ID 
                              from Contact 
                              WHERE AccountId =: acc.Id];
    List<AccountShare> acctShareList = new List<AccountShare>();
    List<ContactShare> conShareList = new List<ContactShare>();
    List<AccountTeamMember> atm1 = new List<AccountTeamMember>();
    //To populate Account Team Member with Default Account Team Member and created sharing
    for(UserAccountTeamMember defaultTeam : u) {
      AccountTeamMember atm = new AccountTeamMember();
      atm.AccountId = acc.Id;
      atm.UserID = defaultTeam.userID;
      atm.TeamMemberRole = defaultTeam.TeamMemberRole;
      atm1.add(atm); 
      AccountShare acctShare = new AccountShare();
      acctShare.AccountAccessLevel = defaultTeam.AccountAccessLevel;
      acctShare.OpportunityAccessLevel = defaultTeam.OpportunityAccessLevel;
      acctShare.CaseAccessLevel = defaultTeam.CaseAccessLevel;
      acctShare.UserOrGroupId = defaultTeam.userID;
      acctShare.AccountId = acc.Id;
      acctShareList.add(acctShare);
      //To allow sharing of contact only when it is not private
      for(Contact con:conList) {
        if(!con.private__c) {
          ContactShare conShare = new ContactShare();
          conShare.UserOrGroupId = defaultTeam.userID;
          conShare.ContactAccessLevel = 'Read';
          conShare.contactID = con.ID;
          conShareList.add(conShare);
        }
      }
    }
    if(atm1.size() > 0) {
      insert atm1;   
    } 
    if(acctShareList.size()>0) {
      Database.insert(acctShareList,false); 
    }  
    if(conShareList.size()>0) {
      Database.insert(conShareList,false);
    }                      
    fetchExistingAccountTeamMembers();
    listSize = accountTeamMembers.size();
    if( listSize == 5) {
      reachedMax = true;
    }
    return null;
  }
  //------------------------------------------------------------------COMM I-210282  End----------------------------------------------------------//
   
   
  //==========================================================================
  // Wrapper class to hold the Team Member record as well as their access levels
  //========================================================================== 
  public class accountTeamWrapper {
    public AccountTeamMember member {get;set;}
    public String accountAccess {get;set;}
    public String opportunityAccess {get;set;}
    public String caseAccess {get;set;}
    public accountTeamWrapper() {
      member = new AccountTeamMember();
      opportunityAccess = '';
      caseAccess = '';
    }
  }
}