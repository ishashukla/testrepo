/*
Name             AssignAssetUnitControllerTest 
Purpose          T-497433 (To replace asset and it's fields via Installed Product)
Modified Date    04/27/2016
Modified By      Nitish Bansal

Modified Date			Modified By				Purpose
05-09-2016				Chandra Shekhar			To Fix Test Class
*/

@isTest
public with sharing class AssignAssetUnitControllerTest {
  
  private static Case createCase(Id accId, Id oppId) {
    return new Case(Subject='TestCase', AccountId=accId, Type = 'Change Order', Status = 'New', CreatedDate= System.today(), Opportunity__c = oppId);
  }
  
  private static SVMXC__Installed_Product__c createInstalledProduct(Id accId) {
    return new SVMXC__Installed_Product__c(Name='TestAsset', SVMXC__Company__c=accId, Serial_Number_1__c='Serial',Unit1__c='Unit');
  }
  
  private static Account createAccount() {
    return new Account(Name='TestAccount');
  }
  
  private static AssetUnit__c createAssetUnit(Id caseId) {
    return new AssetUnit__c(Case__c=caseId, SerialNumber__c='Serial');
  }
  

  @isTest
  public static void testController() {
    Account acc  = createAccount();
    insert acc;
    
    SVMXC__Installed_Product__c ass  = createInstalledProduct(acc.Id);
    insert ass;
    //TestUtils.createCommConstantSetting();
    PriceBook2 priceBook = TestUtils.createPriceBook(1,'test',false).get(0);
    priceBook.name = 'COMM Price Book';
    insert priceBook;
    List<Opportunity> oppList = TestUtils.createOpportunity(1, acc.Id, false);
    for(Opportunity opp : oppList){
      opp.Percent_Probability__c = '10- Highly Improbable';
      opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('COMM Intl Opportunity').getRecordTypeId();
    }
    insert oppList;

    Case c  = createCase(acc.Id, oppList.get(0).Id);
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Change Request');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'COMM Change Request', 'COMM');

	c.recordTypeId = rtByName.getRecordTypeId();
    insert c;
    
    insert createAssetUnit(c.Id);
    
    Test.startTest();
    AssignAssetUnitController aau  = new AssignAssetUnitController(new Apexpages.Standardcontroller(c));
    aau.createPageObjects();
    aau.saveChanges();
    Test.stopTest();
  }
}