/**================================================================      
 * Appirio, Inc
 * Name: Endo_SendEmailtoOrderContactTest
 * Description: Test Class for Endo_SendEmailtoOrderContact  
 * Created Date: 22/07/2016
 * Created By: Pratibha Chhimpa (Appirio)
 * 
 
 Date Modified      Modified By         Description of the update
 27 July 2016       Nathalie Le Guay    Fix test class
 05 Aug  2016       Parul Gupta         I-227221 - After refactoring fix text class
 06 Oct  2016       Parul Gupta         T-542929 | Refer new field "Tracking_Numbers__c"
*******************************************************************************/
@isTest(SeeAllData=false)
private class Endo_SendEmailtoOrderContactTest{
    
  // Test method to test functionality for Sending Email to Order Contact
  static TestMethod void testorderContactBatch() {
    
        Date dtStart = Date.Today();
    
        // Fetch orders
        List<Order> ordList = [SELECT Id, Type, Contact__c, Additional_Contact_for_Tracking_Info__c,
                            (Select Shipped_Date__c, PartNo__c, Description, 
                            Quantity_Shipped__c, OrderId, Order.PoNumber, Status__c, 
                            Order.OrderNumber, Order.Account.AccountNumber, 
                            Order.Account.Name, Oracle_Ordered_Item__c, Quantity, 
                            Unit_of_Measure__c, Shipping_Method__c, Tracking_Numbers__c, 
                            PriceBookEntry.Product2.Name, Line_Type__c From OrderItems 
                            where Shipped_Date__c = :dtStart 
                            AND Line_Type__c = :Label.LINE_TYPE_SJC_STANDARD_LINE
                            AND Status__c =: Label.ORDER_LINE_ITEM_STATUS_SHIPPED) 
                            From Order Where Order.Type = 'SJC CPT Order' and Contact__c != null];                     
        System.assert(ordList.size() > 0);   
    
        Test.startTest();
        Endo_SendEmailtoOrderContact batch = new Endo_SendEmailtoOrderContact();
        Database.executeBatch(batch);
        Test.stopTest();
    }
    
      // Set up test data
    @testSetup static void createTestData(){
        List<Account> accountList = new List<Account>();
        List<Contact> contactList = new List<Contact>();
        List<Product2> prod = new List<Product2>();
        
        EndoGeneralSettings__c cs = new EndoGeneralSettings__c();
        cs.OrderEndoCompleteOrders__c ='012170000008qcX';
        insert cs;
        
        // Creating one Account
        accountList = Endo_TestUtils.createAccount(1,true);
        
        // Creating one Contact
        contactList = TestUtils.createContact(2,accountList[0].id, false);    
        contactList[0].Email = 'test@test.com';
        contactList[1].Email = 'test2@test.com';
        insert contactList;
        
        // Getting Product, pricebook and pricebookEntry for Order and OrderItems
        prod = Endo_TestUtils.createProduct(1, true);
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = Endo_TestUtils.getTestPricebookEntry(pricebookId,prod[0].Id);
        
        // Getting Order 
        Order ord = Endo_TestUtils.createStandardOrder(contactList[0].id, accountList[0].id, false);
        ord.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Complete Orders').getRecordTypeId();
        ord.Type = 'SJC CPT Order'; // For testing
        ord.Pricebook2Id = pricebookId;
        ord.Additional_Contact_for_Tracking_Info__c = contactList[1].id;
        ord.Status = 'Awaiting Shipping';
        insert ord;
        
        // Getting Order Product 
        OrderItem ordItems = Endo_TestUtils.createOrderItem(prod[0].id, ord.id, false);
        ordItems.Status__c = 'Shipped';
        ordItems.Line_Type__c = 'SJC Standard Line';
        ordItems.Shipped_Date__c = System.Today();
        ordItems.PricebookEntryId = standardPrice.Id ;
        insert ordItems;
    }
    
    
    static TestMethod void testorderContactBatchScheduler() {
        
         Test.StartTest();
            
                    //Create object of scheduler class
                    Endo_SendEmailtoOrderContactSchedule sh1 = new Endo_SendEmailtoOrderContactSchedule();
                    String sch = '0 0 23 * * ?';
            
                    //schedule the scheduler class
                    system.schedule('Test send Email', sch, sh1);
                    
                  
                Test.stopTest();
        
    }
    //Added by Shubham for Story S-447620 #Start
    static TestMethod void testEmailUtil() {
        Date dtStart = Date.Today();
        List<Order> ordList = [SELECT Id, Type, Contact__c, Additional_Contact_for_Tracking_Info__c,
                            (Select Shipped_Date__c, PartNo__c, Description, 
                            Quantity_Shipped__c, OrderId, Order.PoNumber, Status__c, 
                            Order.OrderNumber, Order.Type, Order.Account.AccountNumber, 
                            Order.Account.Name, Oracle_Ordered_Item__c, Quantity, 
                            Unit_of_Measure__c, Shipping_Method__c, Tracking_Number__c, 
                            PriceBookEntry.Product2.Name, PriceBookEntry.Name, Line_Type__c From OrderItems 
                            where Shipped_Date__c = :dtStart 
                            AND Line_Type__c = :Label.LINE_TYPE_SJC_STANDARD_LINE
                            AND Status__c =: Label.ORDER_LINE_ITEM_STATUS_SHIPPED) 
                            From Order Where Order.Type = 'SJC CPT Order' and Contact__c != null];                     
        System.assert(ordList.size() > 0);   
        Endo_EmailUitlity util = new Endo_EmailUitlity();
        util.getEmailBodyForConsumer('TestURL', 'TestName', ordList[0].OrderItems);
    }
	//Added by Shubham for Story S-447620 #End       
}