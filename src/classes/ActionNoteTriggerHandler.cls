/**================================================================      
* Appirio, Inc
* Name: ActionNoteTriggerHandler
* Description: T-477246
* Created Date: 23-Feb-2016 (ex: 21-Jul-2016 because of international)
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 24 Feb 2016        Nitish Bansal    T-477244
* 05 May 2016        Meghna Vijay     To bypass Validation error for System Admin Profile I-216723
* 05 Aug 2016        Kanika Mathur    To chnage label name from Account_US_RT to Endo_COMM_Account_RT_API (T-523263)
==================================================================*/
public class ActionNoteTriggerHandler {

  // Methods to be executed before record is Updated
  // @param newRecords - List to hold Trigger.new  
  // @return Nothing
  public void beforeRecordInsert(List<Action_Note__c> newRecords){
    updateShortNotesField(null, null, newRecords);
  }

	// Methods to be executed before record is Updated
	// @param oldMap - map to hold Trigger.oldMap 
	// @param newMap - map to hold Trigger.newMap 
  // @return Nothing 
  public void beforeRecordUpdate(Map<Id, Action_Note__c> oldMap, Map<Id, Action_Note__c> newMap){
    recordEditValidation(oldMap, newMap);
    updateShortNotesField(oldMap, newMap, null);
  }

  //NB - 06/21- I-219767 - Start
  // Methods to be executed before record is deleted
  // @param oldMap - map to hold Trigger.oldMap 
  // @return Nothing 
  public void beforeRecordDelete(Map<Id, Action_Note__c> oldMap){
    recordDeleteValidation(oldMap);
  }

  // Method to limit record deletion by the owner
  // @param oldMap - map to hold Trigger.oldMap 
  // @return Nothing 
  private void recordDeleteValidation(Map<Id, Action_Note__c> oldMap){
    Id loggedInUserId = UserInfo.getUserId();
    Set<Id> setParentIds = new set<Id>();
    List<Profile> profileName = new List<Profile>([SELECT Id, Name FROM Profile WHERE Id =:UserInfo.getProfileId()]);
    for(Id noteId : oldMap.keyset()){
        //Adding the Id of the master Account to the set
        setParentIds.add(oldMap.get(noteId).AccountName__c);
    }
    if(setParentIds.size() > 0){
      //Querying all the account details for the Master accounts asoociated to Action Notes being updated and storing in a map
      Map<Id, Account> mapParentAccounts = new Map<Id, Account> ([Select Id, OwnerId, RecordType.Name, Level__c from Account where Id in : setParentIds]);
    
      for(Id noteId : oldMap.keyset()){
        //If profile is non-admin
        if(profileName.get(0).Name!= Constants.ADMIN_PROFILE){
          //For sales com int user's
          if(profileName.get(0).Name == Label.Sales_COMM_INTL){
            //checking level is system and RT is not COMM Intl Accounts
            if(mapParentAccounts.get(oldMap.get(noteId).AccountName__c).Level__c == Constants.SYSTEM_LEVEL &&
            mapParentAccounts.get(oldMap.get(noteId).AccountName__c).RecordType.Name != Label.COMM_Intl_Accounts){
                oldMap.get(noteId).addError(Label.Action_Note_Delete_Validation_Message);
            }

          } // For Sales rep COMM US user's
          else if(profileName.get(0).Name == Label.Sales_Rep_COMM_US){
            //checking level is system and RT is not COMM US Accounts
            //modified by Kanika Mathur to check label from Account_US_RT to Endo_COMM_Account_RT_API
            if(mapParentAccounts.get(oldMap.get(noteId).AccountName__c).Level__c == Constants.SYSTEM_LEVEL &&
            mapParentAccounts.get(oldMap.get(noteId).AccountName__c).RecordType.Name != Label.Endo_COMM_Account_RT_Label){
                oldMap.get(noteId).addError(Label.Action_Note_Delete_Validation_Message);
            }
          }
        }
      }
    }
  }
  //NB - 06/21- I-219767 - End

  // Method to limit record editing by the owner
	// @param oldMap - map to hold Trigger.oldMap 
	// @param newMap - map to hold Trigger.newMap 
  // @return Nothing 
  private void recordEditValidation(Map<Id, Action_Note__c> oldMap, Map<Id, Action_Note__c> newMap){
  	Id loggedInUserId = UserInfo.getUserId();
  	Set<Id> setParentIds = new set<Id>();
  	List<Profile> profileName = new List<Profile>([SELECT Id, Name FROM Profile WHERE Id =:UserInfo.getProfileId()]);
  	for(Id noteId : newMap.keyset()){
  		//Checking if the record is re-parented
  		if(oldMap.containsKey(noteId) && newMap.get(noteId).AccountName__c != oldMap.get(noteId).AccountName__c){
          setParentIds.add(oldMap.get(noteId).AccountName__c);
      }
      //Adding the Id of the master Account to the set
      setParentIds.add(newMap.get(noteId).AccountName__c);
    }
    if(setParentIds.size() > 0){
    //Querying all the account details for the Master accounts asoociated to Action Notes being updated and storing in a map
      Map<Id, Account> mapParentAccounts = new Map<Id, Account> ([Select Id, OwnerId from Account where Id in : setParentIds]);
    
      for(Id noteId : newMap.keyset()){
      	//Showing error message if logged-in user isn't the record owner 
      	// Bypasses validation error for System Admin Profile I-216723
          //I-219767 - NB - ALlowing note owner to modify the record.
      	if(loggedInUserId != newMap.get(noteId).OwnerId 
      	&& (loggedInUserId != mapParentAccounts.get(newMap.get(noteId).AccountName__c).OwnerId 
      	&& profileName.get(0).Name!= Constants.ADMIN_PROFILE)){
      		newMap.get(noteId).addError(System.Label.Action_Note_Edit_Validation_Message);
      	}
      }
    }
  }

  // Method to update the Notes short field value
  // @param oldMap - map to hold Trigger.oldMap 
  // @param newMap - map to hold Trigger.newMap 
  // @return Nothing 
  public void updateShortNotesField(Map<Id, Action_Note__c> oldMap, Map<Id, Action_Note__c> newMap, List<Action_Note__c> newRecords){
    //If record is updated
    if(oldMap != null && oldMap.size() > 0){
      for(Id noteId : newMap.keyset()){
        //if long text area field has value & the value is being changed
        if(newMap.get(noteId).Notes__c != oldMap.get(noteId).Notes__c){
          //If the field value has more than 255 characters
          if(newMap.get(noteId).Notes__c != null && newMap.get(noteId).Notes__c.length() > 255){
            newMap.get(noteId).Notes_short__c = newMap.get(noteId).Notes__c.substring(0,255);
          } else {
            newMap.get(noteId).Notes_short__c = newMap.get(noteId).Notes__c;
          }
        }
      }
    } else if(oldMap == null && newRecords != null && newRecords.size() > 0){ // If record is inserted
      for(Action_Note__c note : newRecords){
        //if long text area field has value
        if(note.Notes__c != null){
          //If the field value has more than 255 characters
          if(note.Notes__c.length() > 255){
             note.Notes_short__c = note.Notes__c.substring(0,255);
          } else {
             note.Notes_short__c = note.Notes__c;
          }
        }
      }
    } 
  }
}