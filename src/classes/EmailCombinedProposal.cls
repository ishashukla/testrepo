/**
* Sends an email with the attachments(attachments of the Documents section) to the selected addresses(To, CC, BCC).
*
* @author ruslan.vekua@amindsolutions.com
*/
public class EmailCombinedProposal{

    public String to {get;set;}
    public String additionalTo {get;set;}
    public String relatedTo {get;set;}
    public String relatedToKeyPrefix {get;set;}
    public String cc {get;set;}
    public String bcc {get;set;}
    public String ffrom {get;set;}
    public String message {get;set;}
    public String subject {get;set;}
    public String errorMsg{get;set;} 
    public String successMsg{get;set;}
    public List<String> addresses {get;set;}
    public String id {get;set;}
    public String relatedToSfdcId {get;set;}
    public String accountId {get;set;}
    public String attachmentIds {get;set;}
    public Contact contact {get;set;}
    public boolean flagToSendMail;
    private static final String DEL = ';';
  
    public EmailCombinedProposal(){
        this.id = ApexPages.currentPage().getParameters().get('opptyId');
        this.accountId = ApexPages.currentPage().getParameters().get('accountId');
        this.relatedToKeyPrefix = ApexPages.currentPage().getParameters().get('keyprefix');
        this.relatedTo = ApexPages.currentPage().getParameters().get('relatedTo');
        this.relatedToSfdcId = this.id;
        this.contact = getPrimaryContactByOpptyId(this.relatedToSfdcId);
    }
 
    Public List<SelectOption> objList{
         get {
            final List<SelectOption> objList = new List<SelectOption>();
            objList.add(new SelectOption('006', 'Opportunity'));
            return objList;
         }
        set;
     }
    
    public List<SelectOption> emailAddresses {
        get {
            final List<SelectOption> emailAddresses = new List<SelectOption>();
            //final List<OrgWideEmailAddress> oweas = [select Id, DisplayName, Address from OrgWideEmailAddress where IsAllowAllProfiles=true];
            emailAddresses.add(new SelectOption('', '"' + UserInfo.getUserEmail() + '" <' + UserInfo.getUserEmail() + '>'));
            //for (OrgWideEmailAddress owea: oweas) {
            //   emailAddresses.add(new SelectOption(owea.Id, '"' + owea.DisplayName + '" <' + owea.Address + '>'));
            //}           
            return emailAddresses;
        }
        set;
    }
    
    private Contact getPrimaryContactByOpptyId(final String opptyId){        
        final ID accId = getAccountIdByOpptyId(opptyId);
        if(null == accId){
            return null;
        }
        try{
            return [select Id, Name, Email from Contact where Id IN(select ContactId from AccountContactRole where AccountId=:accId and IsPrimary=true) limit 1];
        }catch(Exception e){
            try{
                return [select Id, Name, Email from Contact where AccountId=:accId order by createddate asc limit 1];
            }catch(Exception ex){
                return null;
            }
        }
    }
    
    private ID getAccountIdByOpptyId(final String opptyId){
        try{
            return [select Id, AccountId from Opportunity where id=:opptyId limit 1].AccountId;
        }catch(Exception e){
            //Ignore
            return null;
        }
    }
    
    public List<SAttachment> getSattachments(){
        try{                        
          final System.TimeZone tz = UserInfo.getTimeZone();
            
          final List<Attachment> attachments = [select t.Id, t.Name, t.CreatedById, t.LastModifiedDate, t.ParentId from Attachment t where t.ParentId in (select t1.Id from Document__c t1 where t1.Opportunity__c =:this.id) ORDER BY t.LastModifiedDate DESC];
          
          final String[] ids = new String[attachments.size()];
                 
          for(Attachment att: attachments){
                    ids.add(att.CreatedById);
          }
            
          final List<User> users = [select id, name from User where id in :ids];
          final List<SAttachment> result = new List<SAttachment>();
          SAttachment sa = null;
            
          for(Attachment att: attachments){
              sa = new SAttachment();
              sa.attachment = att;
              for(User u: users){
                  if(u.Id.equals(att.CreatedById)){
                      sa.createdBy = u.Name;
                  }
              }
              
              final Document__c document = [select LastModifiedDate from Document__c where id =:att.ParentId];
              final Datetime dat = document.LastModifiedDate;     
              
              final Integer offsetToUserTimeZone = tz.getOffset(dat) / (1000 * 60 *60);
              sa.LastModifiedDate = dat.addHours(offsetToUserTimeZone);
              
              result.add(sa);
              sa = null;
          }
          ids.clear();
          users.clear();  
          attachments.clear();  
          return result;
        }catch(Exception e){
            return new List<SAttachment>();
        }
    }
    
    public class SAttachment{
        public String createdBy{get;set;}
        public Attachment attachment{get;set;}
        public DateTime LastModifiedDate{get;set;}
    }
    
    private List<Attachment> getAttachmentsByIds(final String[] ids){
        return [select Id, Name, Body, ContentType from Attachment where id in :ids];
    }
    
    public String getEmailByUserName(final String userName){
        try{            
                return [select Email from Contact where Name =: userName and Email != '' limit 1].Email;
        }catch(Exception e){
            //Ignore
            return null;
        }
    }
    
    private String getContactIdByEmail(final String emailAddress){
        try{            
                return [select Id from Contact where Email =: emailAddress limit 1].Id;
        }catch(Exception e){
            //Ignore
            return null;
        }
    }

    public PageReference sendEmail() {
        PageReference pageRef = new PageReference('/'+this.id);
                
        if(subject == ''){
            errorMsg = 'Please enter subject.';
            return null;
        }
        
        try{
        
            if(to != '' || additionalTo != '' || cc != '' || bcc != ''){
                List<String> toAddresses = new List<String>();
                List<String> ccAddresses = new List<String>();
                List<String> bccAddresses = new List<String>();
        
                List<String> tmpAddr = new List<String>();
                if(additionalTo.contains(DEL)){
                    toAddresses = additionalTo.split(DEL);
                    for(String tmp:toAddresses){
                        tmpAddr.add(tmp);
                    }
                }else if(additionalTo != '') {
                    toAddresses.add(additionalTo);
                    tmpAddr.add(additionalTo);
                }
                
                if(to != ''){
                    String emailAddr = null;
                    if(Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}', to)){
                        emailAddr = to;
                    }else{
                        emailAddr = getEmailByUserName(to);
                    }
                    if(null == emailAddr || '' == emailAddr){
                        errorMsg = 'To is invalid.';
                                return null;
                    }
                    toAddresses.add(emailAddr);
                    tmpAddr.add(emailAddr);
                }
                
                if(cc.contains(DEL)){
                    ccAddresses = cc.split(DEL);
                    for(String tmp:ccAddresses){
                        tmpAddr.add(tmp);
                    }
                }else if(cc != ''){
                    ccAddresses.add(cc);
                    tmpAddr.add(cc);
                }
                if(bcc.contains(DEL)){
                    bccAddresses = bcc.split(DEL);
                    for(String tmp:bccAddresses){
                        tmpAddr.add(tmp);
                    }
                }else if(bcc != ''){
                    bccAddresses.add(bcc);
                    tmpAddr.add(bcc);
                }
        
                /* Checking whether mail ids are valid */
                
                addresses = tmpAddr;
                for(String tempStr:addresses){
                    if(!Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}', tempStr)){
                        errorMsg = 'Check To, CC and BCC addresses';
                        successMsg = '';
                        flagToSendMail = false;
                        break;
                    }else{
                        flagToSendMail = true;
                    }           
                }
        
                /* Sending Mail */
                
                
                if(flagToSendMail == true){
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    if(toAddresses != null){
                        email.setToAddresses(toAddresses);
                    }
                    if(ccAddresses != null){
                        email.setccAddresses(ccAddresses);
                    }
                    if(bccAddresses != null){
                        email.setbccAddresses(bccAddresses);
                    }
                                        
                    email.setSubject(subject);  
                    email.setHtmlBody(message);
                    if('' != ffrom){
                        email.setOrgWideEmailAddressId(ffrom);
                    }
                    //this.contact = getPrimaryContactByOpptyId(this.relatedToSfdcId);
                   // if(null != this.contact.Id){
                        //email.setTargetObjectId(this.contact.Id);
                    //}
                   // email.setSaveAsActivity(true);
                    email.setWhatId(this.relatedToSfdcId);
                    email.setBccSender(false);
                   // email.setSenderDisplayName('BBB');
                    //email.setUseSignature(true);
                    List<Messaging.EmailFileAttachment> attachs = null;
                    
                    if('' != attachmentIds){
                        final String[] attachIds = attachmentIds.split(DEL);
                        if(null != attachIds && attachIds.size() > 0){
                           final List<Attachment> attechments = getAttachmentsByIds(attachIds);
                            if(null != attechments && attechments.size() > 0){
                                attachs = new List<Messaging.EmailFileAttachment>();
                                Messaging.EmailFileAttachment attach = null;
                                for(Attachment att: attechments){
                                    attach = new Messaging.EmailFileAttachment();
                                    attach.setContentType(att.contentType);
                                    attach.setFileName(att.Name);
                                    attach.setInline(false);
                                    attach.setBody(att.Body);
                                    attachs.add(attach);
                                    attach = null;
                                }
                                attechments.clear();
                            }                        
                        }
                    }
             
                    if(null != attachs && attachs.size() > 0){
                        email.setFileAttachments(attachs);
                        attachs.clear();
                    }
                    
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
        
                    errorMsg = '';
                    successMsg = 'Mail sent successfully to following recipients';
                    return pageRef; 
                }else{
                    errorMsg = 'Could not send an email.';
                    return null;
                }
            }else{
                errorMsg = 'Please enter To or CC or BCC address';
                return null;
            }
        }catch(Exception e){            
            errorMsg = e.getMessage();
            return null; 
        }
    }
    
}