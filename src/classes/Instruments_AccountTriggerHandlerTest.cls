// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_AccountTriggerHandlerTest
// Description: Test Class for Instruments_AccountTriggerHandler class.
// 
// April 6 2016, Meena Shringi  Original 
//
@isTest 
public class Instruments_AccountTriggerHandlerTest {
    static Id recordTypeInstrument = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type').getRecordTypeId();
    static testMethod void testAccountTriggerHandler(){
        Profile pr =  TestUtils.fetchProfile('System Administrator');
        List<User> usr = TestUtils.createUser(2, pr.Name, true);
        System.runAs(usr[0]) {
            Flex_Rep_and_States__c flexReps = new Flex_Rep_and_States__c(Name = usr[0].Id, User_s_Name__c = usr[0].Name, State__c = 'Nevada,Arizona,California');
            insert flexReps;
            
            Test.startTest();
            Account acc = TestUtils.createAccount(1,false).get(0);        
            acc.ShippingState = '';
            acc.RecordTypeId = recordTypeInstrument;
            insert acc;
            List<System__c> sysList = TestUtils.createSystem(1,'sysName', 'businessunit', True);
            List<Install_Base__c> ibList = TestUtils.createInstallBase(1,acc.Id,usr[0].Id,sysList[0].Id,true);
            Test.stopTest();
            System.assertEquals(True,ibList[0].Account__c != Null);
        }
    }
    static testMethod void testAccountTriggerHandlerWithBillingState() {
        Profile pr =  TestUtils.fetchProfile('System Administrator');
        List<User> usr = TestUtils.createUser(2, pr.Name, true);
        System.runAs(usr[0]) {
            Flex_Rep_and_States__c flexReps = new Flex_Rep_and_States__c(Name = usr[0].Id, User_s_Name__c = usr[0].Name, State__c = 'Nevada,Arizona,California');
            insert flexReps;
            
            Account acc = TestUtils.createAccount(1,false).get(0);        
            acc.BillingState = '';
            acc.RecordTypeId = recordTypeInstrument;
            insert acc;
            List<System__c> sysList = TestUtils.createSystem(1,'sysName', 'businessunit', True);
            List<Install_Base__c> ibList = TestUtils.createInstallBase(1,acc.Id,usr[0].Id,sysList[0].Id,true);
            acc.OwnerId = usr[1].Id;
            Test.startTest();
            update acc;
            Test.stopTest();
            System.assertEquals(True,ibList.size() > 0);
        }
        
    }
  
}