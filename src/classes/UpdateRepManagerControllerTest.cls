// (c) 2016 Appirio, Inc.
//
// Class Name: UpdateRepManagerControllerTest
// Description: Test Class for UpdateRepManagerController
//
// July 5 2016, Isha Shukla  Original
//
@isTest
private class UpdateRepManagerControllerTest {
    static Mancode__c mancodeRecordwithOldManager;
    static User rep2;
    static User newManager;
    // Testing Positive case
    @isTest static void testFirst() {
        createData();
        Test.startTest();
        PageReference pageRef = Page.UpdateRepManager;
		Test.setCurrentPage(pageRef);
        UpdateRepManagerController controller = new UpdateRepManagerController();
        controller.RunBatchUpdate();
        Test.stopTest();
        System.assertEquals([SELECT Manager__c FROM Mancode__c WHERE Sales_Rep__c = :rep2.Id].Manager__c,newManager.Id );
    }
    // Testing Exception case
    @isTest static void testSecond() {
        Test.startTest();
        PageReference pageRef = Page.UpdateRepManager;
		Test.setCurrentPage(pageRef);
        UpdateRepManagerController controller = new UpdateRepManagerController();
        controller.oldManager = mancodeRecordwithOldManager;
        controller.newManager = mancodeRecordwithOldManager;
        controller.RunBatchUpdate();
        Test.stopTest();
    }
    public static void createData() {
        User sysAdmin = TestUtils.createUser(1, 'System Administrator', true).get(0);
        System.runAs(sysAdmin) {
        User rep = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        rep2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        User oldManager = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        newManager = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        mancodeRecordwithOldManager = TestUtils.createMancode(1, rep.Id, oldManager.Id, true).get(0);
        Mancode__c mancodeRecordwithNewManager = TestUtils.createMancode(1, rep2.Id, newManager.Id, true).get(0);
        }
    }
}