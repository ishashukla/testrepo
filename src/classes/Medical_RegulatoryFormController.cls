/**================================================================      
 * Appirio, Inc
 * Name: Apex Controller for Visualforce page Medical_RegulatoryForm
 * Description: This page is using by Medical and Endo Division when a certain types of Case request raised.
 * Created Date: 02/02/2016
 * Created By: Sunil Gupta (Appirio)
 * 
 * Date Modified      Modified By                   Description of the update
 * 09 Feb 2016        Aashita Sharma (Appirio)       
 * 16 Feb 2016        Sunil Gupta (Appirio)         T-485162
 * 24 May 2016        Shreerath Nair                T-504953 (Added Condition for Instrument Product Complaint)
 * 25 May 2016        Shreerath Nair                T-505716 (Redirect if the recordtype changed to Instrument Potential Product) 
 * 10 Jun 2016        Ashu Gupta (Appirio)          T-497033
 * 17 June 2016       Kajal Jalan                   T-511818 (Added Question 4A that displays for Instrument Product Complaint Record Type )
 * 17 June 2016       Kajal Jalan                   T-510599 (To set the Regulatory Form Tab Title)
 * 23 June 2016       Shreerath Nair                I-223722 (Setting tab title and redirecting on save and cancel)
 * 28 Jun 2016        Pratibha Chhimpa (Appirio)    I-224233 - Boolean for Endo users
 * 22 July 2016       Pratibha Chhimpa (Appirio)    T-521986
 * 28 July 2016       Parul Gupta (Appirio)         T-521986
 * 20 July 2016       Parul Gupta (Appirio)         T-521673 (Added Quality Questionnaire 
 *                                                  functionality for Endo - Tech Support profile)
 * 31 Aug  2016       Shreerath Nair(Appirio)       I-232792 (Updated code for Redirect to case on Cacncel)
 * 06 Sept 2016       Nathalie Le Guay              I-226634 / I-233921 - Do not display Regulatory form for "RMA Order" and "Repair Order Billing"
 * 08 Sept 2016       Shreerath Nair(Appirio)       I-233929 Updated the code to redirect to account on cancel 
 * 12 Sept 2016       Shreerath Nair(Appirio)       T-534019 Added Product's LineType for Endo_QualityQuestionaire Component 
 * 12 Sept 2016       Nathalie Le Guay              I-234933, I-234934, I-234932 (Q1, Q2, Q3, Q4 behaviors)
 * 15 Sept 2016       Nathalie Le Guay              I-235086, I-235513, I-235280 (Q15, Q10, Q14)
 * 20 Sept 2016       Nathalie Le Guay              Updates based on Joe's feedback - I-236222, I-236221
 * 12 Oct  2016        Nitish Bansal                 I-239565 - Redirecting to encoded URL to handle special characters in URL
 * 18 Oct  2016       Isha Shukla                   I-240209 (Added check for case recordtype COMM US Stryker Field Service Case)
 * 17 Nov  2016       Isha Shukla                   I-244243 (COMM BP)- Populating Case.Asset_Installed_Product__c field if Case is created from Installed Assets record
 * 30 Nov  2016       Isha Shukla                   I-245813 (COMM BP)- Populating Case.Related_Orders__c field if Case is created from Order record
==================================================================*/
public with sharing class Medical_RegulatoryFormController{
  
  // Public variables used on VF page.
  public Boolean isSuccess{get;set;}
  public String quesString{get;set;}
  public Regulatory_Question_Answer__c regQuestionAnswer1{get;set;}
  
  // T-521673 - Endo quality questionnaire variables
  public List<QualityQuestionnaire> qualityQuestions {get;set;}
  public List<QualityQuestionnaire> regulatoryQuestions {get;set;}
  public boolean isTechSupportUser {get;set;}

  public boolean isInstruments{get;set;}

  // Picklist attributes used on Form
  //public String strHarmReported {get; set;}
  //public String strPatientInvolvementMedDelay {get; set;}

  public List<SelectOption> lstHarmReported {get; set;}
  public List<SelectOption> lstPatientInvolvementMedDelay {get; set;}
  
  //I-232792(SN)
  public String caseId{get;set;}
  
  //I-233929(SN)
  public String accountName{get;set;}
  

  // Private variables
  public Case objNewCase {get;set;}
 // private String contactId; 
 // private Boolean caseAlreadyCreated;

  //I-223722 (SN)
  public String contactId{get;set;}
  public String contactName{get;set;}
  
  private Boolean caseAlreadyCreated;
  //T-505716 (SN) 
  private Id caserecordtypeId;
  
  // ############################I-224233 #######################
  private Boolean hasPermission;

    
  //-----------------------------------------------------------------------------------------------
  //  Cosntructor
  //-----------------------------------------------------------------------------------------------
  public Medical_RegulatoryFormController(ApexPages.StandardController stdController){
    // ############################ I-224233 #######################
    caseId = Apexpages.currentPage().getParameters().get('CaseId');
    caseId = caseId == null ? Apexpages.currentPage().getParameters().get('id') : caseId ;
    //T-505716 (SN)
    caserecordtypeId = Apexpages.currentPage().getParameters().get('RecordType');

    system.debug('Check ###############' + caseId );
    hasPermission = hasPermission('Endo_PermissionOnly');
    system.debug('Check ###############' + hasPermission );
    
    //I-223722 (SN)
    contactName = null;
    
    //I-233929(SN)
    accountName = null;

    // If case is already created - this happens when Case is created from Outlook
    if (String.isBlank(caseId) == false) {
        // T-521673 Parul Gupta (Appirio) - Fetch Product_Family__c for Endo quality questionnaire
        // T-534019 Shreerath Nair (Appirio) - Repalced Product_Family__c  with Product__r.Line_Type__c for Endo quality questionnaire
        objNewCase = [SELECT Id, RecordTypeId,AccountId, ContactId, CaseNumber, Product__r.Line_Type__c, Product__r.ODP_Category__c
                      FROM Case 
                      WHERE Id = :caseId].get(0);
        caseAlreadyCreated = true;
    }
    else {
        objNewCase = new Case();
        caseAlreadyCreated = false;
    }
    //T-511818 (Kajal)
    isInstruments = (caserecordtypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId()) ? true : false;  

    quesString = '1+2+3+4+5';
    regQuestionAnswer1 = new Regulatory_Question_Answer__c();
    isSuccess  = true;

    //strHarmReported = 'No';
    //NLG 3 Aug 2016-T-524214 
    //regQuestionAnswer1.Harm_reported__c = 'No';

    //strPatientInvolvementMedDelay = 'No';
    
    //regQuestionAnswer1.Patient_involvement_Medical_delay__c = 'No'; // NLG 3 Aug 2016 T-524214 
    
    //regQuestionAnswer1.Answer_3__c = system.today(); // NLG 3 Aug 2016 T-524214

    regQuestionAnswer1.Inst_Out_of_Box_Failure_Non_Serviceable__c = false;

    //NLG 3 Aug 2016 T-524214 
    //lstHarmReported = picklistValues(Regulatory_Question_Answer__c.Harm_reported__c);
    lstPatientInvolvementMedDelay = picklistValues(Regulatory_Question_Answer__c.Patient_involvement_Medical_delay__c);



    if(String.isBlank(ApexPages.currentPage().getParameters().get('def_contact_id')) == false){
        contactId = ApexPages.currentPage().getParameters().get('def_contact_id');
        objNewCase.ContactId = contactId;
        //regQuestionAnswer1.Contact__c = contactId;
    }
    else{
        if(objNewCase.ContactId != null){
            //regQuestionAnswer1.Contact__c = objNewCase.ContactId;
            //System.debug('@@@###' + regQuestionAnswer1.Contact__c);
        }
    }

    if(String.isBlank(ApexPages.currentPage().getParameters().get('def_account_id')) == false){
      objNewCase.AccountId = ApexPages.currentPage().getParameters().get('def_account_id');

    }

    // ICCKM
    if(String.isBlank(ApexPages.currentPage().getParameters().get('cas4_lkid')) == false) {
      objNewCase.AccountId = ApexPages.currentPage().getParameters().get('cas4_lkid');
    }
    // ICCKM
    if(String.isBlank(ApexPages.currentPage().getParameters().get('cas3_lkid')) == false) {
      objNewCase.ContactId = ApexPages.currentPage().getParameters().get('cas3_lkid');
    }

    if(String.isBlank(ApexPages.currentPage().getParameters().get('def_asset_id')) == false){
      objNewCase.AssetId = ApexPages.currentPage().getParameters().get('def_asset_id');
    }

    if(String.isBlank(Apexpages.currentPage().getParameters().get('RecordType')) == false){
      objNewCase.RecordTypeId = Apexpages.currentPage().getParameters().get('RecordType');
    }
    // I-244243 (COMM BP)- Populating Case.Asset_Installed_Product__c field if Case is created from Installed Assets record
    if(COMMBPConfigurations__c.getOrgDefaults().New_Case_on_Asset__c != null){
        String installedAssetId = COMMBPConfigurations__c.getOrgDefaults().New_Case_on_Asset__c+'_lkid';
        if(String.isBlank(Apexpages.currentPage().getParameters().get(installedAssetId)) == false) {
            objNewCase.Asset_Installed_Product__c = Apexpages.currentPage().getParameters().get(installedAssetId);
        }
    }
    // I-245813 (COMM BP)- Populating Case.Related_Orders__c field if Case is created from Order record
    if(COMMBPConfigurations__c.getOrgDefaults().Order_On_Case__c != null){
        String orderOnCaseId = COMMBPConfigurations__c.getOrgDefaults().Order_On_Case__c+'_lkid';
        if(String.isBlank(Apexpages.currentPage().getParameters().get(orderOnCaseId)) == false) {
            objNewCase.Related_Orders__c = Apexpages.currentPage().getParameters().get(orderOnCaseId);
        }
    }
      if(COMMBPConfigurations__c.getOrgDefaults().PO_On_Case__c != null){
        String POOnCaseId = COMMBPConfigurations__c.getOrgDefaults().PO_On_Case__c;
        if(String.isBlank(Apexpages.currentPage().getParameters().get(POOnCaseId)) == false) {
            objNewCase.PO__c = Apexpages.currentPage().getParameters().get(POOnCaseId);
        }
    }
    //String accountId = ApexPages.currentPage().getParameters().get('cas4_lkid');
    //String accountId = ApexPages.currentPage().getParameters().get('cas4_lkid');
    //contactId = ApexPages.currentPage().getParameters().get('cas3_lkid');
    
    // T-521673 Parul Gupta (Appirio) - Display Endo quality questions if current user is Tech Support user
    displayQualityQuestions();
  }


  //-----------------------------------------------------------------------------------------------
  //  Helper method to get picklist values.
  //-----------------------------------------------------------------------------------------------
  public List<SelectOption> picklistValues(SObjectField field){
    List<SelectOption> options = new List<SelectOption>();
    Schema.DescribeFieldResult fieldResult = field.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for( Schema.PicklistEntry f : ple){
      options.add(new SelectOption(f.getLabel(), f.getValue()));
    }
    return options;
  }



  //-----------------------------------------------------------------------------------------------
  //  Redirect user to Std. Case creation page based on record type.
  //  This method is calling from visualforce page load event.
  //-----------------------------------------------------------------------------------------------
  public PageReference redirectCaseNew(){
    if(Apexpages.currentPage().getParameters().containsKey('RecordType')) {
      Id recordTypeId = Id.valueOf(Apexpages.currentPage().getParameters().get('RecordType'));

      Map<Id, schema.recordtypeinfo> recordTypeMap = Case.sObjectType.getDescribe().getRecordTypeInfosById();
      String recordTypeName = recordTypeMap.get(recordTypeId).getName();
      
      //T-521986 Parul|Appirio - Again added recordTypeName == 'RMA Order' as per chatter comment
      //T-521986 Pratibha|Appirio  - Removed recordTypeName == 'RMA Order'
      // If Record Type is Medical Product Compaint then came back to this page.
      //T-504953 - Added Condition for Instrument Product Complaint(SN)
      // T-539444 (COMM BP) - Added condition for COMM US Stryker Field Service Case
      if((recordTypeName == 'COMM US Stryker Field Service Case') ||  recordTypeName == 'Medical - Potential Product Complaint' 
                                                                   || recordTypeName == Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT){
                                                               //   || recordTypeName == 'Repair Order Billing' - Nathalie Le Guay - Sept 6th,2016 - I-233921
                                                               //   || recordTypeName == 'RMA Order' - Nathalie Le Guay - Sept 6th,2016 - I-226634
        return null;
      }
    }
    
    //Added by Ashu Gupta for T-497033
    if(Apexpages.currentPage().getParameters().containsKey('isButtonSource')) {
      return null;
    }
    
    // For every other case redirect user to standard case creation page.
    String url = '/500/e?nooverride=0';
    for(String u : ApexPages.currentPage().getParameters().keySet()){
      if( u != 'nooverride' && u != 'save_new'){
        //NB - 10/12 - I-239565 - Start - Redirecting to encoded URL to handle special characters in URL
        if(u == COMM_Constant_Setting__c.getOrgDefaults().Case_Project_Plan_Field_Id__c || 
           u == COMM_Constant_Setting__c.getOrgDefaults().Case_Description_Field_Id__c){
          url += '&'+ u + '=' + EncodingUtil.URLEncode(ApexPages.currentPage().getParameters().get(u),'UTF-8');
        } else {
          url += '&'+ u + '=' + ApexPages.currentPage().getParameters().get(u);
        }
        //NB - 10/12 - I-239565 - End - Redirecting to encoded URL to handle special characters in URL
      }
    }
    system.debug('URL : ' + url);
    PageReference pg = new PageReference(url);
    pg.setRedirect(true);
    return pg;
  }

  public List<SelectOption> q11Values {get;set;} // Added by NLG Endo UAT
  public List<SelectOption> q8Values {get;set;} // Added by NLG Endo UAT
  //-----------------------------------------------------------------------------------------------
  //  Action method - Logic to show next questions.
  //  T-511818, (Kajal) Added condition for Inst_Out_of_Box_Failure_Non_Serviceable__c field 
  //-----------------------------------------------------------------------------------------------
  public void showNextQuestions() {
    q8Values = new List<SelectOption>();
    q8Values.add(new SelectOption('', '--None--'));
    q11Values = new List<SelectOption>();
    q11Values.add(new SelectOption('', '--None--'));

    if (regQuestionAnswer1.Patient_involvement_Medical_delay__c == 'Yes' ) {//|| regQuestionAnswer1.Patient_involvement_Medical_delay__c == 'Complainant Not Aware') {
      //if (regQuestionAnswer1.Answer_8__c == 'No - No Impact' || regQuestionAnswer1.Answer_8__c == 'Complainant Not Aware') {
      // Default value if Q14 not on the TW screen (Q6-22)
      if (regQuestionAnswer1.Formal_complaint__c == false && regQuestionAnswer1.Adverse_Consequences__c == 'No') {
        regQuestionAnswer1.Answer_8__c = 'Yes - No Impact';
      }
        q8Values.add(new SelectOption('Yes - Impact - See Adverse Consequences', 'Yes - Impact - See Adverse Consequences'));
        q8Values.add(new SelectOption('Yes - No Impact', 'Yes - No Impact'));
      //updateString('+10');
      //updateString('+all');
    }
    
    if (regQuestionAnswer1.Formal_complaint__c == false) {
      updateString('1+2+3+4+5');
      replaceString('+all');
    }

    if (regQuestionAnswer1.Patient_involvement_Medical_delay__c == 'No') {
      //updateString('+10');
      q8Values.clear();
      regQuestionAnswer1.Answer_8__c = 'No - No Impact';
      q8Values.add(new SelectOption('No - No Impact', 'No - No Impact'));
      replaceString('+all');
      replaceString('+10');
    }
    
    if (regQuestionAnswer1.Patient_involvement_Medical_delay__c == 'Complainant Not Aware') {
      updateString('+10');
      replaceString('+all');

      // Default value if Q14 not on the TW screen (Q6-22)
      if (regQuestionAnswer1.Formal_complaint__c == false && regQuestionAnswer1.Adverse_Consequences__c == 'No') {
        regQuestionAnswer1.Answer_8__c = 'Complainant Not Aware';
      }
      q8Values.add(new SelectOption('Complainant Not Aware', 'Complainant Not Aware'));
      q8Values.add(new SelectOption('Not Reported', 'Not Reported'));
    }

    if(regQuestionAnswer1.Answer_1__c < System.today().addDays(-1)) {
        updateString('+7');
    }

    if(regQuestionAnswer1.Answer_1__c >= System.today().addDays(-1)) {
        replaceString('+7');
    }

    if (regQuestionAnswer1.Adverse_Consequences__c != null && 
           regQuestionAnswer1.Adverse_Consequences__c == 'No' ) {
          //updateString('+14');
          q11Values.clear();
          q11Values.add(new SelectOption('No', 'No'));
          replaceString('+14');
          regQuestionAnswer1.Answer_11__c = 'No';
          //replaceString('+all');
      } 
      
      if (regQuestionAnswer1.Adverse_Consequences__c != null && regQuestionAnswer1.Adverse_Consequences__c == 'Complainant Not Aware') {
          //updateString('+14');
          //replaceString('+all');
          updateString('+all');
          
          q11Values.add(new SelectOption('Complainant Not Aware', 'Complainant Not Aware'));
          q11Values.add(new SelectOption('Not Reported', 'Not Reported'));
      }

    if((regQuestionAnswer1.Adverse_Consequences__c != null && regQuestionAnswer1.Adverse_Consequences__c == 'Yes') || regQuestionAnswer1.Formal_complaint__c == true || regQuestionAnswer1.Inst_Out_of_Box_Failure_Non_Serviceable__c == true) {
        // NLG 3 Aug 2016 T-524214 
        // || (regQuestionAnswer1.Harm_reported__c != null && regQuestionAnswer1.Harm_reported__c == 'Yes')
        /*if (regQuestionAnswer1.Adverse_Consequences__c != 'Yes') {
          regQuestionAnswer1.Answer_11__c = '';
        }*/
        updateString('+all');
        if (regQuestionAnswer1.Adverse_Consequences__c == 'Yes') {
          q11Values.add(new SelectOption('User', 'User'));
          q11Values.add(new SelectOption('Patient and User', 'Patient and User'));
          q11Values.add(new SelectOption('Patient', 'Patient'));
        }
    }
    

       
      
    if((regQuestionAnswer1.Answer_8__c != null && (regQuestionAnswer1.Answer_8__c == 'Yes - No Impact' || regQuestionAnswer1.Answer_8__c == 'Yes - Impact - See Adverse Consequences')) && quesString.contains('+all')) {
        updateString('+11+12+13');
    }

    if((regQuestionAnswer1.Answer_8__c != null && (regQuestionAnswer1.Answer_8__c != 'Yes - No Impact' && regQuestionAnswer1.Answer_8__c != 'Yes - Impact - See Adverse Consequences')) && quesString.contains('+all')) {
        replaceString('+11+12+13');
    }

    if(((regQuestionAnswer1.Answer_11__c != null && (regQuestionAnswer1.Answer_11__c == 'Patient' || regQuestionAnswer1.Answer_11__c == 'Patient and User' || regQuestionAnswer1.Answer_11__c == 'User'))
       || 
       (regQuestionAnswer1.Answer_9__c != null && regQuestionAnswer1.Answer_9__c == 'Yes') 
       || 
       (regQuestionAnswer1.Answer_10__c != null && regQuestionAnswer1.Answer_10__c == 'Yes'))
      && 
       quesString.contains('+all')) {
        updateString('+15');
    }

    if((quesString.contains('+15') && quesString.contains('+all')) && ((quesString.contains('+12') && regQuestionAnswer1.Answer_9__c != 'Yes') || (!quesString.contains('+12') && regQuestionAnswer1.Answer_9__c == 'Yes')) 
        && 
       ((quesString.contains('+13') && regQuestionAnswer1.Answer_10__c != 'Yes') || (!quesString.contains('+13') && regQuestionAnswer1.Answer_10__c == 'Yes')) 
        && 
       ((regQuestionAnswer1.Answer_11__c != 'Patient' && regQuestionAnswer1.Answer_11__c != 'Patient and User' && regQuestionAnswer1.Answer_11__c != 'User')) ) {
        replaceString('+15');
    }

    if(regQuestionAnswer1.Answer_12_New__c != null && regQuestionAnswer1.Answer_12_New__c == 'No' && quesString.contains('+all')) {
        updateString('+17');
    }

    if(regQuestionAnswer1.Answer_12_New__c != null && regQuestionAnswer1.Answer_12_New__c != 'No' && quesString.contains('+all')) {
        replaceString('+17');
    }

    if(regQuestionAnswer1.Answer_14_New__c != null && regQuestionAnswer1.Answer_14_New__c == 'Yes' && quesString.contains('+all')) {
      updateString('+19+20');
    }

    if(regQuestionAnswer1.Answer_14_New__c != null && regQuestionAnswer1.Answer_14_New__c != 'Yes' && quesString.contains('+all')) {
        replaceString('+19+20');
    }

    if(regQuestionAnswer1.Answer_17_New__c != null && regQuestionAnswer1.Answer_17_New__c != 'No' && regQuestionAnswer1.Answer_17_New__c != '--Please Select--' && quesString.contains('+all')) {
        updateString('+22');
    }

    if(regQuestionAnswer1.Answer_17_New__c != null && regQuestionAnswer1.Answer_17_New__c != '--Please Select--' && regQuestionAnswer1.Answer_17_New__c == 'No' && quesString.contains('+all')) {
      replaceString('+22');
    }

    // NLG 3 Aug 2016 T-524214 
    //if((regQuestionAnswer1.Adverse_Consequences__c != null || regQuestionAnswer1.Harm_reported__c != null) && (regQuestionAnswer1.Adverse_Consequences__c != 'Yes' 
    //    && regQuestionAnswer1.Formal_complaint__c != true && regQuestionAnswer1.Harm_reported__c != 'Yes') && quesString.contains('+all')) {

      if(regQuestionAnswer1.Adverse_Consequences__c != null && regQuestionAnswer1.Adverse_Consequences__c != 'Yes' && regQuestionAnswer1.Adverse_Consequences__c != 'Complainant Not Aware' && regQuestionAnswer1.Formal_complaint__c != true && regQuestionAnswer1.Inst_Out_of_Box_Failure_Non_Serviceable__c != true && quesString.contains('+all')) {
        replaceString('+all');
        replaceString('+6');
        replaceString('+11+12+13');
        replaceString('+15');
        replaceString('+22');
        replaceString('+19+20');
        replaceString('+17');
    }


    //if(regQuestionAnswer1.Answer_1__c > system.today().addDays(-1)) {
    //  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You cannot select a date that occurs in the future for the awareness date.');
    //  ApexPages.addMessage(myMsg);
    //  isSuccess = false;
    //}
    //if(regQuestionAnswer1.Answer_1__c < regQuestionAnswer1.Answer_3__c && quesString.contains('+all')) {
    //  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The event date cannot be after the date of awareness.');
    //  ApexPages.addMessage(myMsg);
    //  isSuccess = false;
    //}
  }


  //----------------------------------------------------------------------------------------------
  // make Questions Required
  // T-511818, (Kajal) Added condition for Inst_Out_of_Box_Failure_Non_Serviceable__c field 
  //----------------------------------------------------------------------------------------------
  public Boolean validateForm() {
      
    // NLG 3 Aug 2016 T-524214 
    //if(quesString.contains('1+2+3+4') && (regQuestionAnswer1.Answer_1__c == null || regQuestionAnswer1.Formal_complaint__c == null || regQuestionAnswer1.Harm_reported__c == null  || regQuestionAnswer1.Patient_involvement_Medical_delay__c == null)) {
    if(quesString.contains('1+2+3+4') && (regQuestionAnswer1.Answer_1__c == null || regQuestionAnswer1.Formal_complaint__c == null || regQuestionAnswer1.Inst_Out_of_Box_Failure_Non_Serviceable__c == null  || regQuestionAnswer1.Adverse_Consequences__c == null || regQuestionAnswer1.Patient_involvement_Medical_delay__c == null || regQuestionAnswer1.Answer_3__c == null )) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }


   
    if(quesString.contains('+6') && regQuestionAnswer1.Adverse_Consequences__c == null) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }
      

    if (quesString.contains('+all') 
      && (regQuestionAnswer1.Answer_17_New__c == null 
           || regQuestionAnswer1.Answer_14_New__c == null 
           || regQuestionAnswer1.Answer_12_New__c == null 
           || regQuestionAnswer1.Answer_11__c == null 
           || regQuestionAnswer1.Answer_8__c == null 
           || regQuestionAnswer1.Answer_5_New__c == null 
           || regQuestionAnswer1.Answer_4__c == null 
           || regQuestionAnswer1.Answer_3__c == null )) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }


    if(quesString.contains('+7') && regQuestionAnswer1.Answer_2_New__c == null) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }
      

    if(quesString.contains('+11+12+13') && (regQuestionAnswer1.Answer_7_New__c == null || regQuestionAnswer1.Answer_9__c == null || regQuestionAnswer1.Answer_10__c == null)) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }

    if(quesString.contains('+15') && regQuestionAnswer1.Additional_Details__c == null) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }

    if(quesString.contains('+17') && regQuestionAnswer1.Answer_13_New__c == null) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }

    if(quesString.contains('+19+20') && (regQuestionAnswer1.Answer_15_New__c == null || regQuestionAnswer1.Answer_16_New__c == null)) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }

    if(quesString.contains('+22') && regQuestionAnswer1.Answer_18_New__c == null) {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'All questions displayed must be answered.');
      ApexPages.addMessage(myMsg);
      return false;
    }
    isSuccess = true;

    // NLG 3 Aug 2016 T-524214 
    Map<String, Set<String>> acceptedValues = new Map<String, Set<String>>();
      
    if (quesString.contains('+3') && regQuestionAnswer1.Adverse_Consequences__c != null) {
      String csqAnswer = regQuestionAnswer1.Answer_11__c;
      acceptedValues = new Map<String, Set<String>> {
        'Yes' => New Set<String> {'User', 'Patient and User', 'Patient'},
        'No'  => New Set<String> {'No'},
        'Complainant Not Aware' => New Set<String> {'Complainant Not Aware', 'Not Reported'}
      };
      if (!acceptedValues.get(regQuestionAnswer1.Adverse_Consequences__c).contains(csqAnswer)) {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 
                                      'If you answer '+ regQuestionAnswer1.Adverse_Consequences__c + ' to Q3, please select one of the following to Q14: ' + acceptedValues.get(regQuestionAnswer1.Adverse_Consequences__c));
        ApexPages.addMessage(myMsg);
        return false;
      }
    } 
      
    // NLG 3 Aug 2016 T-524214 
    if (quesString.contains('+4') && regQuestionAnswer1.Patient_involvement_Medical_delay__c != null) {
      String patAnswer = regQuestionAnswer1.Answer_8__c;
      acceptedValues = new Map<String, Set<String>> {
        'Yes' => New Set<String> {'Yes - Impact - See Adverse Consequences', 'Yes - No Impact'},
        'No'  => New Set<String> {'No - No Impact'},
        'Complainant Not Aware' => New Set<String> {'Complainant Not Aware', 'Not Reported'}
      };
      if (!acceptedValues.get(regQuestionAnswer1.Patient_involvement_Medical_delay__c).contains(patAnswer)) {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 
                                           'If you answer ' + regQuestionAnswer1.Patient_involvement_Medical_delay__c + ' to Q4, please select one of the following for Q10: ' + acceptedValues.get(regQuestionAnswer1.Patient_involvement_Medical_delay__c)) ;
        ApexPages.addMessage(myMsg);
        return false;
      }
    }

    return true;
  }



  //-----------------------------------------------------------------------------------------------
  //  Update Regulatory Form and Insert Case and redirect User to Case detail page.
  //-----------------------------------------------------------------------------------------------
  public PageReference saveForm(){
    if(validateForm() == false){
      isSuccess = false;
      return null;
    }
    // T-521673 Parul Gupta (Appirio) - Append Endo quality questions to Regulatory_Question_Answer__c.Answer_4__c field
    appendEndoQualityQuestions();
    
    Savepoint sp = Database.setSavepoint();
    try{

      /*
      if(regQuestionAnswer1.Harm_reported__c == 'Yes' || regQuestionAnswer1.Adverse_Consequences__c == 'Yes') {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Based on the answers provided, please transfer the customer to the Regulatory Department upon submitting.');
        ApexPages.addMessage(myMsg);
        isSuccess = false;
      }
      */
      if(hasPermission){
      regQuestionAnswer1.Case__c = caseId;      
      }
      else{
      //insert regulatory questions in database
      regQuestionAnswer1.Case__c = objNewCase.Id;
      }

      //regQuestionAnswer1.Harm_reported__c = strHarmReported;
      //regQuestionAnswer1.Patient_involvement_Medical_delay__c = strPatientInvolvementMedDelay;


      // Set default values:

      if(String.isBlank(regQuestionAnswer1.Answer_7_New__c) == true || regQuestionAnswer1.Answer_7_New__c == '--Please Select--'){
        regQuestionAnswer1.Answer_7_New__c = 'No Associated Procedure';
      }

      if(String.isBlank(regQuestionAnswer1.Answer_9__c) == true || regQuestionAnswer1.Answer_9__c == '--Please Select--'){
        regQuestionAnswer1.Answer_9__c = 'No Associated Procedure';
      }

      if(String.isBlank(regQuestionAnswer1.Answer_10__c) == true || regQuestionAnswer1.Answer_10__c == '--Please Select--'){
        regQuestionAnswer1.Answer_10__c = 'No';
      }

      if(String.isBlank(regQuestionAnswer1.Adverse_Consequences__c) == true || regQuestionAnswer1.Adverse_Consequences__c == '--Please Select--'){
        regQuestionAnswer1.Adverse_Consequences__c = 'No';
      }
      if(String.isBlank(regQuestionAnswer1.Answer_11__c) == true || regQuestionAnswer1.Answer_11__c == '--Please Select--'){
        regQuestionAnswer1.Answer_11__c = 'No';
      }

      if(quesString.contains('+19') == false) {
        regQuestionAnswer1.Answer_15_New__c = null;
      }
      // end
      // Insert Case
      // T-539444 (COMM BP) - Added condition for Regulatory form and case is already created or not
      if(objNewCase.Id == null){
        system.debug('@@##');
        if(regQuestionAnswer1.Formal_complaint__c == true || regQuestionAnswer1.Inst_Out_of_Box_Failure_Non_Serviceable__c == true || regQuestionAnswer1.Harm_reported__c == 'Yes' || regQuestionAnswer1.Adverse_Consequences__c == 'Yes' || regQuestionAnswer1.Adverse_Consequences__c == 'Complainant Not Aware') {
          objNewCase.Trackwise_Status__c = 'Planned';
        }
        
        insert objNewCase;
        
      }
      System.debug('@@##' + objNewCase);

        /*
      if(regQuestionAnswer1.Harm_reported__c == 'Yes' || regQuestionAnswer1.Adverse_Consequences__c == 'Yes') {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Based on the answers provided, please transfer the customer to the Regulatory Department upon submitting.');
        ApexPages.addMessage(myMsg);
        isSuccess = false;
      }
      */

      //insert regulatory questions in database
      regQuestionAnswer1.Case__c = objNewCase.Id;

      //regQuestionAnswer1.Harm_reported__c = strHarmReported;
      //regQuestionAnswer1.Patient_involvement_Medical_delay__c = strPatientInvolvementMedDelay;
      // END ICCKM

      System.debug('@@##');
      upsert regQuestionAnswer1;
      System.debug('@@##');
      
      // ############################ I-224233 #######################
      // Redirect User to standard Case detail page.
      String url;
      if(hasPermission) {
          url = '/' + caseId;
      }
      else {
          if(caseAlreadyCreated == true) {
            //T-505716 (SN) when user changes recordtype 
            update objNewCase;
            //url = '/' + objNewCase.id;
            url = '/' + objNewCase.id + '/e?retURL=%2F' + objNewCase.Id;
          }
          else{
            url = '/' + objNewCase.id + '/e?retURL=%2F' + objNewCase.Id;
          }
          objNewCase = [SELECT Id,RecordTypeId,ContactId,AccountId,CaseNumber FROM Case WHERE Id = :objNewCase.Id].get(0);
      }
      PageReference pg = new PageReference(url);
      pg.setRedirect(true);
      //(T-510599),Making this as a comment is because we need to set the tab title(Oncomplete function to work)
      //return pg;
      isSuccess = true;
    }
    catch(DMLException ex) {
      Database.rollback(sp);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(0)));
      // Rollback is not working properly, Id is still exist so doing clone here.
      objNewCase = objNewCase.clone();
      isSuccess = false;
    }
    catch(Exception ex) {
      Database.rollback(sp);
      System.debug('@@##' + ex.getMessage());
      ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error , ex.getMessage()));
      // Rollback is not working properly, Id is still exist so doing clone here.
      objNewCase = objNewCase.clone();
      isSuccess = false;
    }
    return null;
  }


  //-----------------------------------------------------------------------------------------------
  //  Cancel action method called from visualforce
  //-----------------------------------------------------------------------------------------------
  public PageReference cancel() {
    //String recordId = caseId != null ? caseId :(currentContact != null ? currentContact.id : 'home/home.jsp');
    //return new PageReference('/' + recordId);
    //return new PageReference('/home/home.jsp');
    //String url = '/+'contactId;
    
    //I-223722 (SN) (Redirect tab and setting tabtitle)
    if(contactId != null) {
      Contact con  = [SELECT Name FROM Contact where id =: contactId];
      ContactName = con.Name;
      // return new PageReference('/' + contactId);
    }
    else{
      // return new PageReference('/home/home.jsp');
    }
    
    //SN 8 Sept 2016 I-233929 Start 
    if(objNewCase.AccountId != null){
        Account ac = [Select name from Account where id =: objNewCase.AccountId];
        accountName = ac.Name;
    }
     //SN 8 Sept 2016 I-233929 Start 
    
    return null;
  }


  //-----------------------------------------------------------------------------------------------
  //  Helper method to update reRender String
  //-----------------------------------------------------------------------------------------------
  private void updateString(String str) {
    if(quesString.contains(str) == false) {
      quesString = quesString + str;
    }
  }

  //-----------------------------------------------------------------------------------------------
  //  Helper method to update reRender String
  //-----------------------------------------------------------------------------------------------
  private void replaceString(String str) {
    if(quesString.contains(str) == true) {
      quesString = quesString.replace(str,'');
    }
  }

  //-----------------------------------------------------------------------------------------------
  //  Helper method to get permission for I-224233
  //  28th June'16   Pratibha Chhimpa    Added Custom Permission Check
  //-----------------------------------------------------------------------------------------------
  private Boolean hasPermission(String customPermissionName) {
  
    List<id> containsPermission = new List<id>();
    
    // getting Custom permission based on a DeveloperName 
    CustomPermission customPermission = [SELECT Id, DeveloperName 
                                         FROM CustomPermission 
                                         WHERE DeveloperName = :customPermissionName];
    // getting Current user profile Id;                                            
    Id profileId=userinfo.getProfileId();
    
    // check if profileId and CustomPermission have a record. IF profile have any permission, record will be formed else it will return null
    for (SetupEntityAccess entityAccess : [SELECT Id, SetupEntityId,ParentId
                                           FROM SetupEntityAccess 
                                           WHERE parent.ProfileId =:profileId AND SetupEntityId =:customPermission.Id]) {
        containsPermission.add(entityAccess.id);
    }
    // Check if record exsist for this combination, if it does return true
    if (!containsPermission.isEmpty()) {
        return true;
    }
    // return false if no connection found
    return false;                                           
  }

  // T-521673 Parul Gupta (Appirio) - Method sets isTechSupportUser variable to true if current user is Tech support user
  private void displayQualityQuestions(){
      isTechSupportUser = false;
      qualityQuestions = new List<QualityQuestionnaire>();
      regulatoryQuestions = new List<QualityQuestionnaire>();
      for (User user : [Select id, Profile.name from User where id =: UserInfo.getUserId()]){
          if (user.profile.Name == 'Endo - Tech Support' && objNewCase.id != null){
              isTechSupportUser = true;
          }
      }
  }
  
  // T-521673 Parul Gupta (Appirio) - Method to append Endo Quality Questionnaire to Regulatory_Question_Answer__c.Answer_4__c field
  private void appendEndoQualityQuestions(){
    String QualityQuestionnaireStr = '';
    Integer lastIndex = 0;
    for (Integer index = 0; index < qualityQuestions.size(); index++){
      QualityQuestionnaireStr += 'Q' + (index + 1) + ': ' + qualityQuestions[index].question.Question__c + '\n';
      QualityQuestionnaireStr += 'A' + (index + 1) + ': ' + qualityQuestions[index].answer.Answer__c + '\n';
      lastIndex = (index + 1);
    }
    for (Integer index = 0; index < regulatoryQuestions.size(); index++){
      QualityQuestionnaireStr += 'Q' + (lastIndex + 1) + ': ' + regulatoryQuestions[index].question.Question__c + '\n';
      QualityQuestionnaireStr += 'A' + (lastIndex + 1) + ': ' + regulatoryQuestions[index].answer.Answer__c + '\n';
    }
    system.debug('Regulatory Questions string::' + QualityQuestionnaireStr);
    if (QualityQuestionnaireStr != '') {
      if (regQuestionAnswer1.Answer_4__c == null){
          regQuestionAnswer1.Answer_4__c = QualityQuestionnaireStr;
      } else{
        regQuestionAnswer1.Answer_4__c += '\n' + QualityQuestionnaireStr;
      }
    }
 
  }
  
}