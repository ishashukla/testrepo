//
// (c) 2012 Appirio, Inc.
//
//  Test class of  WebserviceAmendOpportunity
//
// 19 Aug 2015     Naresh K Shiwani      Original
//
@isTest
private class GeneratePDFofFinancialProfilesConTest {
  
	static testmethod void testMethodFinancialProfileBlank(){	
		Test.startTest();	
		Account accnt = new Account();
		accnt.Name                      = 'AccntName';
		accnt.Type                      = 'Hospital';
		accnt.Legal_Name__c             = 'Test';
		accnt.Flex_Region__c            = 'test';
		insert accnt;
		System.assertNotEquals(accnt, null);
	  Financial_Profile__c fpObj      = new Financial_Profile__c();
	  fpObj.account__c                 = accnt.id;
	  insert fpObj;
	  System.assertNotEquals(fpObj, null);
	  GeneratePDFofFinancialProfilesController oppSendEmailContObj = new GeneratePDFofFinancialProfilesController(new ApexPages.StandardController(accnt));
	  Test.stopTest();
	}
	static testmethod void testMethodFinancialProfileFilled(){  
    Test.startTest(); 
    Account accnt = new Account();
    accnt.Name                                    = 'AccntName';
    accnt.Type                                    = 'Hospital';
    accnt.Legal_Name__c                           = 'Test';
    accnt.Flex_Region__c                          = 'test';
    insert accnt;
    System.assertNotEquals(accnt, null);
    Integer intVar                                = 101;
    Financial_Profile__c fpObj                    = new Financial_Profile__c();
    fpObj.account__c                              = accnt.id;
    fpObj.Acquisition_Notes__c                    = intVar+'';
		fpObj.Annual_Operating_Lease_Rent_Expense__c  = intVar;
		fpObj.Bond_Rating__c                          = intVar+'';
		fpObj.Capital_Leases_Total__c                 = intVar;
		fpObj.Cash__c                                 = intVar;
		fpObj.Cash_Provided_from_Operations__c        = intVar;
		fpObj.Cash_used_for_Capital_Expenditures__c   = intVar;
		fpObj.Cost_of_Funds__c                        = intVar+'';
		fpObj.Credit_Notes__c                         = intVar+'';
		fpObj.Current_Assets__c                       = intVar;
		fpObj.Current_Liabilities__c                  = intVar;
		fpObj.Current_Portion_of_Long_Term_Debt__c    = intVar;
		fpObj.Depreciation_Amortization__c            = intVar;
		fpObj.Equity_Net_asset_position__c            = intVar;
		fpObj.Goodwill__c                             = intVar;
		fpObj.Interest_Expense__c                     = intVar;
		fpObj.Inventory__c                            = intVar;
		fpObj.Net_Income__c                           = intVar;
		fpObj.Net_Profit__c                           = intVar;
		fpObj.Operating_Expenses__c                   = intVar;
		fpObj.Operating_Profit__c                     = intVar;
		fpObj.Prior_Year_Cash__c                      = intVar;
		fpObj.Prior_Year_Goodwill__c                  = intVar;
		fpObj.Prior_Year_Net_Income__c                = intVar;
		fpObj.Prior_Year_Operating_Revenue__c         = intVar;
		fpObj.Prior_Year_Revenue__c                   = intVar;
		fpObj.Prior_Year_Total_Revenue__c             = intVar;
		fpObj.Prior_Year_Working_Capital__c           = intVar;
		fpObj.Tax_Expense__c                          = intVar;
		fpObj.Total_Assets__c                         = intVar;
		fpObj.Total_Liabilities__c                    = intVar;
		fpObj.Total_Operating_Revenues__c             = intVar;
		fpObj.Total_Revenues__c                       = intVar;
		fpObj.Unrestricted_Net_Assets__c              = intVar;
		fpObj.Year__c                                 = intVar+'';
    insert fpObj;
    System.assertNotEquals(fpObj, null);
    GeneratePDFofFinancialProfilesController oppSendEmailContObj = new GeneratePDFofFinancialProfilesController(new ApexPages.StandardController(accnt));
    Test.stopTest();
  }
}