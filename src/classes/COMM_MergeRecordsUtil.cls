/**================================================================      
* Appirio, Inc
* Name: COMM_MergeRecordsUtil 
* Description:Used to merge the project plan reocrds T-489645
* Created Date: 27th April
* Created By: Kirti Agarwal (Appirio)
*
* Date Modified      Modified By        Description of the update
* July 16, 2013      Prakash
* 2016               Kirti Agarwal      
* October 3,2016     Nitish Bansal      I-237880 (Error handling)
==================================================================*/
public without sharing class COMM_MergeRecordsUtil {
    
    //this guides the number of merged records
    public final static Integer MERGED_RECORDS_COUNT = 2;
    
    //this guides the number of merged records
    public final static String PROJECT_PLAN = 'Project_Plan__c';
    public final static String PROJECT_PLAN_LABEL = 'Project Plan';
    
    //Utility class to handle the master/loosers selection
    public class SelectMasterResult
    {
        public List<SOBject> loserObjs = new List<SObject>();
        public SObject masterObject = null;
    }
    
    //Utility class to handle the single cell
    public class InputHiddenValue
    {
        public InputHiddenValue(String name, Boolean isChecked, Boolean isSelectable)
        {
            this.isChecked = isChecked;
            this.isSelectable = isSelectable;
            this.name = name;
        }
        //checked or not to merge into che master record
        public Boolean isChecked{get;set;}
        //avoid formula/unchangeble fields
        public Boolean isSelectable{get;set;}
        //field name
        public String name{get;set;}
    }   
    
    //Utility class to handle field specs
    public class Field
    {
        public Field(String name, String label, Boolean isWritable)
        {
            this.name = name;
            this.label = label;
            this.isWritable = isWritable;
        }
        public String name{get;set;}
        public String label{get;set;}
        public Boolean isWritable{get;set;}
    }
    
    
    
    public static SelectMasterResult mergeRecords(List<SObject> merginObjects, String masterId)
    {
        SelectMasterResult res = new SelectMasterResult();
        
        //select the master record and the looser ones
        for(SObject sobj : merginObjects)
        {
            String id = (String)sobj.get('Id');
            if(id==null) continue;
            
            if(id == masterId)
            {
                res.masterObject = sobj;
            }
            else
            {
                res.loserObjs.add(sobj);
            }
        }
        return res;
        
    }
    
    /*
        Copy looser records data into master record using the "selectedObjFields" (map of map of InputHddenValue)
    */
    
    /**
        "selectedObjFields" logic is like this:
         
        list of selected fields of all object
        stores:
            {OBJ-ID1, 
                    {FIELD1, TRUE}
                    {FIELD2, TRUE}
                    ...
                    {FIELDN, FALSE}
            OBJ-ID2, 
                    {FIELD1, FALSE}
                    {FIELD2, FALSE}
                    ...
                    {FIELDN, TRUE}
            }
    */  
    public static void copyDataIntoMasterRecord(SelectMasterResult recordSet, Map<ID,Map<String,InputHiddenValue>> selectedObjFields,String objType)
    {
        System.assert(recordset != null, 'RecordSet should not be null.');
        System.assert(recordset.masterObject != null, 'Master object should not be null.');
        System.assert(recordset.loserObjs != null && recordset.loserObjs.size()>0, 'Looser objects should not be null and should be more than 0.');
        
        for(Sobject sobj : recordSet.loserObjs)
        {
            Map<String, InputHiddenValue> flds = selectedObjFields.get((ID)sobj.get('Id'));
            System.assert(flds!=null,'Field Selection Map should not be null');
            for(InputHiddenValue inv : flds.values())
            {
                if(inv.isSelectable==false)continue;
                if(inv.isChecked==false) continue;
                recordSet.masterObject.put(inv.name, sobj.get(inv.name));
            }
            
        }
     System.debug('objType is'+objType);
     Schema.DescribeSObjectResult type =  Schema.getGlobalDescribe().get(objType).getDescribe(); 
     List<Schema.ChildRelationship> child = type.getChildRelationships();
     for(Schema.ChildRelationship relation: child){
     
     //Modified by Prakash @July 16, 2013...
     Schema.DescribeSObjectResult describeResult = relation.getChildSObject().getDescribe();
     String objName = describeResult.getName();
     String fieldName = relation.getField().getDescribe().getName();
     // Sharing Objects handled by SFDC, so no need to process Share objects 
     if(objName.endsWith('Share') ) 
      continue;
     //...Modified by Prakash @July 16, 2013
     
     String query ='';
     for(sObject loser:recordSet.loserObjs){
      query += '\'' + loser.get('id') + '\','; 
     
     }
         query = query.lastIndexOf(',') > 0 ? '(' + query.substring(0,query.lastIndexOf(',')) + ')' : query ;

      System.debug('query string is '+query);
  
     try{
     List<sObject> updateObjectList = new List<sObject>();
     List<sObject> objList = Database.query('SELECT '+fieldName+' FROM '+objName+' WHERE '+fieldName+' IN '+ query);  
     if(objName == 'Attachment') {
       objList = Database.query('SELECT Name, Body, ParentId FROM '+objName+' WHERE '+fieldName+' IN '+ query);
     }
     System.debug('length of sobjects is'+objList.size() );
     //T-477520 - Don't need to create feed items and project plan feed
     if(objName == 'FeedItem' || objName == 'Project_Plan__Feed' ) {
       objList = new List<Sobject>();
     }
     //end T-477520
     if(objList.size() > 0)
     {Schema.sObjectType sss = objList.get(0).getSObjectType(); 
    Schema.DescribeFieldResult description = sss.getDescribe().fields.getMap().get(fieldName).getDescribe();
 
      if( description.isUpdateable() && objName != 'Attachment')
     { for(sObject obj : objList ){
       
      obj.put(fieldName,recordSet.masterObject.get('Id')) ;
      updateObjectList.add(obj);
      }
     }
    else {for(SObject obj:objList )
         {
//Modified by Prakash @July 16, 2013
//Checking Object Schema describe details (Createable/Deleteable) then only allowing.
       if(describeResult.isCreateable() || describeResult.isDeletable()) {
        sObject dupObj = obj.clone(false,true,true,true); 
        if(objName != 'Attachment') {
        dupObj.put(fieldName,recordSet.masterObject.get('Id'));
        }else{
          dupObj.put('ParentId',recordSet.masterObject.get('Id'));
        }
        if(describeResult.isDeletable())
          delete obj;
        if(describeResult.isCreateable())
          insert dupObj;
         
       } 
    }
  //Modified by Prakash @July 16, 2013      
        }     
     }
     //Added by Prakash @July 16, 2013
     if(describeResult.isUpdateable() && !updateObjectList.isEmpty())
     //Added by Prakash @July 16, 2013
        update updateObjectList;
     
     }
      
        catch(System.QueryException e){
        System.debug('Exception is'+e);
        continue;
        }
        //NB - 10/03 - I-237880 - Start
        catch (DMLException exp) {
          System.debug('exp------> ' + exp.getTypeName() + ' - ' + exp.getCause() + ': ' + exp.getMessage());
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, exp.getMessage()));
        }
        //NB - 10/03 - I-237880 - End
      }

    }
    
    /*
        Init all the necessary describes
    */
    
    public class DescribeResult
    {
        public DescribeResult()
        {
            this.allFields = new Map<String,Field>();
            this.uniqueFields = new Map<String,String>();
        }
        //name fields map (API name / label)
        public String nameField {get;set;}
        public String nameFieldLabel {get;set;}

        //all fields
        public Map<String, COMM_MergeRecordsUtil.Field> allFields{get;set;}
        //sorted keyset
        public List<String> allFieldsKeySet{
            get{
    
                List<String> output = new List<String>(allFields.keySet());
                output.sort();
                return output;
            }
        }
        
        //unique fields to show in the search results
        public Map<String,String> uniqueFields {get;set;}
        //returns an ordered "unique fields" keyset
        public List<String> uniqueFieldsKeySet{
            get{
                List<String> ufl = new List<String>(uniqueFields.keySet());
                ufl.sort();
                return ufl;
            }
        }
        public Schema.SobjectType sOType = null;
    }
    
    public static DescribeResult initDescribes(String sObjectType)
    {
        DescribeResult result = new DescribeResult();
        
        //SOBject describe
        result.sOType = Schema.getGlobalDescribe().get(sObjectType);
        System.assert(result.sOType!=null ,'No describe for SObject '+result.sOType);
        
        Map<String, Schema.SObjectField> fieldMap = result.sOType.getDescribe().fields.getMap();

        result.nameField = 'Id';
        result.nameFieldLabel = 'Id';
        //search for "name" fields
        for(String name : fieldMap.keySet())
        {
            Schema.SObjectField field = fieldMap.get(name);
            Schema.DescribeFieldResult dfs = field.getDescribe();
            
            //all fields to display
            Boolean isWritable = dfs.isUpdateable();
            result.allFields.put(dfs.getName(),new Field(dfs.getName(),dfs.getlabel(),isWritable));
            
            if(dfs.isNameField())
            {
                result.nameField = dfs.getName();
                result.nameFieldLabel = dfs.getlabel();
            }
            //also adds unique fields
            else if(dfs.isUnique())
            {
                result.uniqueFields.put(dfs.getName(), dfs.getLabel());
            }
        }
        return result;
    }
}