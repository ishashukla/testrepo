@isTest
private class ProjectTeamAddMemberExtensionTest
{
    @isTest
    static void itShould()
    {
        //Creating a System Admin User
        List<User> userList = TestUtils.createUser(2, 'System Administrator', true);
        List<Document__c> oppDocList = new List<Document__c>() ;
        List<Document__c> delOppDocList = new List<Document__c>() ;
        Document__c oppDoc = new Document__c();
        Custom_Account_Team__c projectMem = new Custom_Account_Team__c();
        List<Custom_Account_Team__c> projectMemList = new List<Custom_Account_Team__c>();
        //Creating an account, opprtunity and multile documents as the Admin user
        Test.startTest();
        system.runAs(userList.get(0)){
            List<Account> accountList = TestUtils.createAccount(1, true);
            TestUtils.createCommConstantSetting();
            List<Opportunity> opportunityList = TestUtils.createOpportunity(1, accountList.get(0).Id, true);
            List<Project_Plan__c> projectList = TestUtils.createProjectPlan(1, accountList.get(0).Id, false);
            projectList[0].Start_Date__c = system.today();
            projectList[0].Completion_Date__c = system.today().addDays(20);
            insert projectList;
        
            for(Integer i = 0; i <= 5; i++){
                oppDoc = new Document__c();
                oppDoc.Name = 'Test Opportunity Document' + String.valueOf(i);
                oppDoc.Opportunity__c = opportunityList.get(0).Id;
                oppDoc.Account__c = accountList.get(0).Id;
                oppDoc.Document_type__c = 'Other';
                oppDoc.Project_Plan__c = projectList.get(0).Id;
                oppDocList.add(oppDoc);
            }
            insert oppDocList;
           
            ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(projectList.get(0));
            ProjectTeamAddMemberExtension  instance = new ProjectTeamAddMemberExtension (std);
            instance.newProjectTeamMembers.get(0).member.User__c = userList.get(1).Id;
            instance.newProjectTeamMembers.get(0).projectAccess = 'Edit';
            instance.saveAndMore();
            system.assertNotEquals(0, instance.newProjectTeamMembers.size());
            instance.doCancel();
            system.assertEquals(null, instance.newProjectTeamMembers);
        }
        Test.stopTest();    
    }
}