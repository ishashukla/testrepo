public with sharing class NV_ModelInProcingLookupController{
    public String CustomerID {get;set;}
    public Datetime EffectiveDate {get;set;}
    public Datetime ModelDate {get;set;}
    public String ProductIdsCSV {get;set;}
    public List<NV_RTPM.ResolvedPriceType> ResolvedPrices {get;set;}

    public NV_ModelInProcingLookupController(){
        CustomerID = '10023';
        ProductIdsCSV = 'M003100620,M003100630,M003100640';
        //EffectiveDate = Datetime.newInstance(2015, 30, 12);
        //ModelDate = Datetime.newInstance(2015, 30, 12);
        ResolvedPrices = new List<NV_RTPM.ResolvedPriceType>();
    }
    
    public void pricingLookupCallout(){
        Map<String, NV_Product_Pricing_Settings__c> pricingSettings = NV_Product_Pricing_Settings__c.getAll();
        NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort obj = new NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort();
        NV_RTPM.RealTimePriceMultiRequestType req = new NV_RTPM.RealTimePriceMultiRequestType();
        req.ConfigName = pricingSettings.get('ConfigName').Value__c==null?'Global':pricingSettings.get('ConfigName').Value__c;
        req.CustomerID = customerId;
        
        Time zeroTime = Time.newInstance(0, 0, 0, 0);
        Date EffectiveDateValue = Date.today();
        Date ModelDateValue = Date.today();
        /*if(pricingSettings.get('Default Effective Date').Value__c!=null){
            EffectiveDateValue = Date.valueOf(pricingSettings.get('Default Effective Date').Value__c);
        }
        if(pricingSettings.get('Default Model Date').Value__c!=null){
            ModelDateValue = Date.valueOf(pricingSettings.get('Default Model Date').Value__c);
        }*/
        
        
        req.EffectiveDate = Datetime.newInstance(EffectiveDateValue, zeroTime);
        req.ModelDate = Datetime.newInstance(ModelDateValue, zeroTime);
        
        
        req.CurrencyCode = pricingSettings.get('CurrencyCode').Value__c==null?'USD':pricingSettings.get('CurrencyCode').Value__c;
        req.OrgUnitID = pricingSettings.get('OrgUnitID').Value__c==null?'NV_US':pricingSettings.get('OrgUnitID').Value__c;
        req.BusSegCode = pricingSettings.get('BusSegCode').Value__c==null?'':pricingSettings.get('BusSegCode').Value__c;
        req.ERPCode = pricingSettings.get('ERPCode').Value__c==null?'EMPR':pricingSettings.get('ERPCode').Value__c;
        NV_RTPM.ProductIDs pIds = new NV_RTPM.ProductIDs();
        pIds.ProductID = ProductIdsCSV.split(',');
        req.ProductIDs = pIds;
        NV_RTPM.RealTimePriceMultiResponseType response = obj.ModelNProductRealTimePriceLookup(req);
        //System.debug();
        //if(response != null && response.ResolvedPrices.ResolvedPrice != null){
        //System.debug(response.ResolvedPrices.ResolvedPrice.size());
        //System.debug(response.ResolvedPrices.ResolvedPrice);
        if(response != null && response.ResolvedPrices != null) {
          ResolvedPrices = response.ResolvedPrices.ResolvedPrice;
        }
        //}
        
    }
}