/**================================================================      
* Appirio, Inc
* Name: COMM_BatchSyncExistingAddressTest
* Description: Test class for COMM_BatchSyncExistingAddress
* Created Date: 11 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class COMM_BatchSyncExistingAddressTest {
    static List<Address__c> addressList;
    @isTest static void testBatch() {
        createData();
        Test.startTest();
        Database.executeBatch(new COMM_BatchSyncExistingAddress());
        Test.stopTest();
        System.assertEquals([SELECT SVMXC__Country__c FROM SVMXC__Site__c WHERE Location_ID__c = :addressList[0].Location_ID__c].SVMXC__Country__c, 'United States');
    }
    private static void createData() {
        Account acc = TestUtils.createAccount(1, true).get(0);
        addressList = TestUtils.createAddressObject(acc.Id, 4, true, 'United States',
                                                                     'state', 'Shipping', '88', false);
        for(integer i = 0;i < 4;i++) {
            addressList[i].Location_ID__c = '123'+i;
            addressList[i].Status__c = 'Active';
        }
        insert addressList;
    }
}