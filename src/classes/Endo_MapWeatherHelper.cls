public class Endo_MapWeatherHelper {
  public static String errorMessage {get; set;}
  
  public static LatLng getLatLng(String address) {
  	errorMessage = null;  	
	  HttpRequest req = new HttpRequest();
	  String googleMapsEndpoint = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false';
    
    //Configure google Maps URL
    googleMapsEndpoint = googleMapsEndpoint + '&address=' + EncodingUtil.urlEncode(address, 'UTF-8');
    googleMapsEndpoint = googleMapsEndpoint + '&key=' + System.Label.Google_API_Key;
	  req.setEndpoint(googleMapsEndpoint);
	  req.setMethod('GET');
	  Endo_MapWeatherHelper.LatLng latLng = null;
	  
	  try {
		  String result = null;
		  if (!Test.isRunningTest()) {
		    //Execute the call to Google Maps API to get lat lng     
		    result = fetchResult(req);
		  }
		  else {
			  result = ' {"location" : {"lat" : 39.6034810,"lng" : -119.6822510}}';
		  }
		
		  System.debug('value - ' + result);
   	  
   	  //Parse the return JSON to get latitude/longitude.
	    JSONParser parser = JSON.createParser(result);
      while (parser.nextToken() != null) {
        if(result.contains('OVER_QUERY_LIMIT')){
          if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'error_message')) {
        	  logError(parser);
					  break;
        	}
        }
        else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location')) {
		      parser.nextToken();		      
		      latLng = (Endo_MapWeatherHelper.LatLng)parser.readValueAs(Endo_MapWeatherHelper.LatLng.class);
		    }
		  }
	  }
	  catch(System.CalloutException e) {
	  	errorMessage = 'Cannot get account local time. GMT: ' + System.now();
      System.debug('Callout Exception: ' + e.getMessage());
	  }
	  catch (JSONException e) {
	  	errorMessage = 'Cannot get account local time. GMT: ' + System.now();
      System.debug('JSON Exception: ' + e.getMessage());
    }
	  return (latLng);
  }
  
  
  @testvisible
  private static void logError(JSONParser parser){
  	parser.nextToken();
	  errorMessage = (String)parser.readValueAs(String.class);
	  System.debug('value - ' + errorMessage);
  }
  
  @testvisible
  private static String fetchResult(HttpRequest req){
  	//req.setTimeout(4000); // timeout in milliseconds
  	Http http = new Http();
  	HttpResponse res = http.send(req);
  	return res.getBody();
  }
  
  public static String getWeather(String city, String state) {
  	errorMessage = null;
  	Http http = new Http();
	  HttpRequest req = new HttpRequest();
	  String weatherEndpoint = 'http://www.weatherforecastmap.com/weather1F.php?zona=' + state + '_' + city;        
	  req.setEndpoint(weatherEndpoint);
	  req.setMethod('GET');
	        
	  String result = null;	        
	  try {
		  if (!Test.isRunningTest()) {
		    //Execute the call to Google Timezone API to get timezone     
		    result = fetchResult(req);
		  }
		  else {
			  result = 'no result found';
		  }
	  }
	  catch(System.CalloutException e) { 
	  	errorMessage = 'Cannot get account local time. GMT: ' + System.now();
      System.debug('Callout Exception: ' + e.getMessage());
	  }
	  catch (JSONException e) { 
	  	errorMessage = 'Cannot get account local time. GMT: ' + System.now();
      System.debug('JSON Exception: ' + e.getMessage());
    }
  	return result;
  }
  
  
  
  public static String getAccountLocalTime(LatLng latLng) {
  	errorMessage = null;
  	Http http = new Http();
	  HttpRequest req = new HttpRequest();
	  String googleTimeZoneEndpoint = 'https://maps.googleapis.com/maps/api/timezone/json?location='+ latLng.lat +',' + latLng.lng + '&timestamp=1331161200&sensor=false';
	  //Configure earth tool URL to get time offset
	  googleTimeZoneEndpoint = googleTimeZoneEndpoint + '&key=' + System.Label.Google_API_Key ;
	  req.setEndpoint(googleTimeZoneEndpoint);
	  req.setMethod('GET');
	  String timezoneId = null;
	  try {
		  String result = null;
		
		  if (!Test.isRunningTest()) {
		    //Execute the call to get weather details     
		    result = fetchResult(req);
		  }
		  else {
			  result = '{"dstOffset" : 0.0,"rawOffset" : -28800.0,"status" : "OK","timeZoneId" : "America/Los_Angeles","timeZoneName" : "Pacific Standard Time"}';
		  }
	    System.debug('webservice result' + result);           
	    //Parse the return json to get offset.
	    //Parse the return JSON to get latitude/longitude.
	    JSONParser parser = JSON.createParser(result);
      while (parser.nextToken() != null) {
        if(result.contains('OVER_QUERY_LIMIT')){
          if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'error_message')) {
        	  logError(parser);
					  break;
        	}
        }
        else{
          TimeZoneResultWrapper tw = (TimeZoneResultWrapper)JSON.deserialize(result, TimeZoneResultWrapper.class);
				  timezoneId = tw.timeZoneId;
        }
      }
		  System.debug('webservice result' + timezoneId);    
		  //Set accountLocal time to date in String format
    }
    catch(System.CalloutException e) { 
    	errorMessage = 'Cannot get weather details';
      System.debug('Callout Exception: ' + e.getMessage());
    }
    catch (JSONException e) { 
    	errorMessage = 'Cannot get weather details';
      System.debug('JSON Exception: ' + e.getMessage());
    }
    return timezoneId;
  }
  
  public class TimeZoneResultWrapper{
  	public double dstOffset{get;set;}
  	public double rawOffset{get;set;}
  	public String status{get;set;}
  	public String timeZoneId{get;set;}
  	public String timeZoneName{get;set;}
  }
  
  public class LatLng {
    public String lat {get; set;}
    public String lng {get; set;}
  }  	
}