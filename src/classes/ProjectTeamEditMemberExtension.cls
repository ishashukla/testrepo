/*
Name            ProjectTeamEditMemberExtension
Purpose         T-500923 (To add project team members)
Created Date    05/06/2016
Created By      Nitish Bansal
*/
public class ProjectTeamEditMemberExtension {

    String PROJECT_TEAM_RECORDTYPE_ID;
    public Id projectId;
    public Id userId;
    public List<selectOption> projectAccessLevels {get;set;}
    public Custom_Account_Team__c teamMemberToEdit{get;set;}
    public Id teamMemberID;
    public String memberName{get;set;}
    public ProjectTeamWrapper wrapper{get;set;}

    public ProjectTeamEditMemberExtension() {
        //Map<Id, Id> projectUserIdMap = new Map<Id, Id>();
        PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
        Id teamMemberID = ApexPages.currentPage().getParameters().get('Id');
        //System.debug('*********'+teamMemberId);
        teamMemberToEdit = [SELECT Id, Project__c, User__c, Team_Member_Role__c, Project_Access_Level__c FROM Custom_Account_Team__c WHERE Id =: teamMemberID];
        //projectUserIdMap.put(teamMemberToEdit.Project__c, teamMemberToEdit.User__c);
        //System.debug('__________'+'teamMemberToEdit' + teamMemberToEdit);
        projectAccessLevels = new List<selectOption>();
        projectAccessLevels.add(new selectOption('Read','Read Only'));
        projectAccessLevels.add(new selectOption('Edit','Read/Write'));

        if(teamMemberToEdit != null ){
           projectId = teamMemberToEdit.Project__c;
           
            //System.debug('&&&&&&&&&'+'teamMemberToEdit.User__c'+teamMemberToEdit.User__c);
           //System.debug('teamMemberToEdit.Project__c'+teamMemberToEdit.Project__c);
            List<Project_Plan__Share> projShare = [SELECT AccessLevel, ParentId,userOrGroupId FROM Project_Plan__Share WHERE  ParentId=:teamMemberToEdit.Project__c
                                                   AND userOrGroupId =:teamMemberToEdit.User__c];
             
            //System.debug('projShare '+projShare);
            List<User> memberUser = [SELECT Name FROM User where ID = :teamMemberToEdit.User__c];
            System.debug('------------'+'Name'+memberUser.get(0).name);
            if(memberUser != null){
                memberName = memberUser.get(0).name;
            }
            if(projShare.size()>0) {
               wrapper = new ProjectTeamWrapper();
               wrapper.member = teamMemberToEdit;
               wrapper.projectAccess = projShare.get(0).AccessLevel;
               wrapper.pjShare= projShare.get(0); 
               
            }
        }
    }

    //==========================================================================
    // Wrapper class to hold the Team Member record as well as their projectess levels
    //==========================================================================
    public class ProjectTeamWrapper {
        public Custom_Account_Team__c member {get;set;}
        public String projectAccess {get;set;}
        public Project_Plan__Share pjShare {get;set;}
        public ProjectTeamWrapper() {
          member = new Custom_Account_Team__c();
          projectAccess = '';
        }
    }

    //==========================================================================
    // Method to cancel and return back to Account page
    //==========================================================================
    public pagereference doCancel () {
        return new pagereference('/'+projectId);
    }

    //==========================================================================
    // Method to called on save button
    //==========================================================================
    public pagereference saveNewTeamMembers () {
            List<Custom_Account_Team__c> projMemToUpdate = new List<Custom_Account_Team__c>();
            List<Project_Plan__Share> projShareToUpdate = new List<Project_Plan__Share>();
            try {
             System.debug('**************'+'wrapper.member'+wrapper);
             //System.debug('(((('+'wrapper.pjShar'+wrapper.pjShare);
                if ( wrapper.member.User__c != null ) {
                  wrapper.member.Project_Access_Level__c = wrapper.projectAccess;
                  projMemToUpdate.add(wrapper.member);
                  
                  if ( wrapper.pjShare != null) {
                    wrapper.pjShare.AccessLevel = wrapper.projectAccess;
                    projShareToUpdate.add(wrapper.pjShare);
                    system.debug(wrapper.pjShare);
                }
              }

              
              if (!projMemToUpdate.isEmpty()) {
              System.debug('********'+projMemToUpdate);
                update projMemToUpdate;
              }
              if (!projShareToUpdate.isEmpty()) {
                update projShareToUpdate;
              }

              return new pagereference('/'+projectId);
            }
            catch ( exception ex) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, ex.getMessage()));
              return null;
            }
    }
}