@isTest
private class TaskIntermediateControllerTest {

    private static List<Account> accounts;
    private static List<Contact> contacts;
    @isTest
	private static void testRelatedTo() {
            createTestData();
        	PageReference pageRef = Page.TaskIntermediatePage;
			Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('who_id' , contacts.get(0).id);
            TaskIntermediateController taskIntermediate = new TaskIntermediateController();
            taskIntermediate.selectedAccId  = accounts.get(0).id;
            taskIntermediate.getSelectedRecord();
            System.assert(taskIntermediate.selectedRecordName  != null , 'Actual Account Name ' + taskIntermediate.selectedRecordName);
            taskIntermediate.next();
            taskIntermediate.cancel();
        	pageRef = taskIntermediate.redirectPage();
            
	}
	
	private static void createTestData(){
	    accounts = TestUtils.createAccount(4, true);
	    contacts = TestUtils.createContact(2, accounts.get(0).id , true);
	}
}