//
// (c) 2015 Appirio, Inc.
//
// Apex Test Class Name: NV_SalesTotalsController_Test
// For Apex Class: NV_SalesTotalsController
//
// 08th January 2015    Hemendra Singh Bhati    Original (Task # T-459194)
//
@isTest(seeAllData=false)
private class NV_SalesTotalsController_Test {
  // Private Data Members.
  private static final String SYSTEM_ADMINISTRATOR_PROFILE_NAME = 'System Administrator';
  private static final String REGIONAL_MANAGER_CENTRAL_ROLE_NAME = 'Regional Manager - Central';
  private static final String REGIONAL_MANAGER_CENTRAL_SUBORDINATE_ROLE_NAME = 'ATM - Central';
  private static final Id ORDER_NV_BILL_ONLY_RECORD_TYPE_ID = Schema.SObjectType.Order.RecordTypeInfosByName.get('NV Bill Only').RecordTypeId;
  private static final Id ORDER_NV_RMA_RECORD_TYPE_ID = Schema.SObjectType.Order.RecordTypeInfosByName.get('NV RMA').RecordTypeId;

  /*
  @method      : validateControllerFunctionality
  @description : This method validates controller functionality.
  @params      : void
  @returns     : void
  */
	private static testMethod void validateControllerFunctionality() {
    // Extracting "System Administrator" Profile Id.
    List<Profile> theProfiles = [SELECT Id FROM Profile WHERE Name = :SYSTEM_ADMINISTRATOR_PROFILE_NAME];
    system.assert(
      theProfiles.size() > 0,
      'Error: The requested user profile does not exist.'
    );

    // Extracting "Regional Manager - Central" Role Id.
    List<UserRole> theUserRoles = [SELECT Id, Name FROM UserRole WHERE Name IN
                                   (:REGIONAL_MANAGER_CENTRAL_ROLE_NAME, :REGIONAL_MANAGER_CENTRAL_SUBORDINATE_ROLE_NAME)];
    system.assert(
      theUserRoles.size() == 2,
      'Error: The requested user roles does not exist.'
    );

    // Setting Up User Role Ids.
    Id theRegionalManagerCentralRoleId = theUserRoles.get(1).Id;
    Id theATMCentralRoleId = theUserRoles.get(0).Id;

    // Instantiating Test User With "Regional Manager - Central" Role.
    User theCentralRegionalManagerUser = new User(
      ProfileId = theProfiles.get(0).Id,
      UserRoleId = theRegionalManagerCentralRoleId,
	    Alias = 'hsbrmc',
	    Email = 'hsingh@appirio.com.rmc',
	    EmailEncodingKey = 'UTF-8',
	    FirstName = 'Hemendra Singh',
	    LastName = 'Bhati',
	    LanguageLocaleKey = 'en_US',
	    LocaleSidKey = 'en_US',
	    TimezoneSidKey = 'Asia/Kolkata',
	    Username = 'hsingh@appirio.com.srtyker.rmc',
	    CommunityNickName = 'hsbrmc',
	    IsActive = true
    );

    // Instantiating Test User With "ATM - Central" Role.
    User theATMCentralUser = new User(
      ProfileId = theProfiles.get(0).Id,
      UserRoleId = theATMCentralRoleId,
	    Alias = 'hsbatmc',
	    Email = 'hsingh@appirio.com.atmc',
	    EmailEncodingKey = 'UTF-8',
	    FirstName = 'Hemendra Singh',
	    LastName = 'Bhati',
	    LanguageLocaleKey = 'en_US',
	    LocaleSidKey = 'en_US',
	    TimezoneSidKey = 'Asia/Kolkata',
	    Username = 'hsingh@appirio.com.srtyker.atmc',
	    CommunityNickName = 'hsbatmc',
	    IsActive = true
    );

    // Inserting Test Users.
    insert new List<User> {
      theCentralRegionalManagerUser,
      theATMCentralUser
    };
    system.debug('TRACE: NV_SalesTotalsController_Test - validateControllerFunctionality - theCentralRegionalManagerUser - ' + theCentralRegionalManagerUser);
    system.debug('TRACE: NV_SalesTotalsController_Test - validateControllerFunctionality - theATMCentralUser - ' + theATMCentralUser);

    // Generating Test Data.
    Account theTestAccount = null;
    Contract theTestContract = null;
    Id standardPriceBookId = null;
    Product2 theTestProduct = null;
    PricebookEntry theTestPBE = null;
    Order theTestOrder = null;
    List<OrderItem> theTestOrderItems = null;

    // Running Thread As Central Regional Manager.
    system.runAs(theCentralRegionalManagerUser) {
      // Inserting Test Account.
      theTestAccount = new Account(
        Name = 'Test Account',
        OwnerId = theCentralRegionalManagerUser.Id
      );
      insert theTestAccount;

      // Inserting Test Contract.
      theTestContract = new Contract(
        AccountId = theTestAccount.Id,
        Status = 'Draft',
        ContractTerm = 1,
        StartDate = Date.today().addDays(-1),
        External_Integration_Id__c = '123123'
      );
      insert theTestContract;

      // Extrating Standard PriceBook Id.
      standardPriceBookId = Test.getStandardPricebookId();

      // Inserting Test Product.
      theTestProduct = new Product2(
        Name = 'Test Product',
        Catalog_Number__c = '1',
        IsActive = true
      );
      insert theTestProduct;

      // Inserting Test Pricebook Entry.
      theTestPBE = new PricebookEntry(
        Pricebook2Id = standardPriceBookId,
        Product2Id = theTestProduct.Id,
        UnitPrice = 99.00,
        isActive = true
      );
      insert theTestPBE;

      // Inserting Test Order.
      theTestOrder = new Order(
        AccountId = theTestAccount.Id,
        ContractId = theTestContract.Id,
        Status = 'Draft',
        EffectiveDate = Date.today(),
        Date_Ordered__c = Datetime.now(),
        Pricebook2Id = standardPriceBookId,
        Oracle_Order_Number__c = '1234567890',
        Invoice_Related_Sales_Order__c = '1234567891',
        Customer_PO__c = 'PO To Follow',
        On_Hold__c = true,
        RecordTypeId = ORDER_NV_BILL_ONLY_RECORD_TYPE_ID,
        OwnerId = theATMCentralUser.Id
      );
      insert theTestOrder;


      // Inserting Test Order Items.
      theTestOrderItems = new List<OrderItem>();
      for(Integer index = 0;index < 3;index++) {
        theTestOrderItems.add(new OrderItem(
          PriceBookEntryId = theTestPBE.Id,
          OrderId = theTestOrder.Id,
          Quantity = 1,
          UnitPrice = 99,
          Line_Amount__c = 99,
          Tracking_Number__c = '12345',
          Status__c = 'Delivered',
          Shipping_Method__c = 'FDX-Parcel-Next Day 8AM',
          On_Hold__c = true,
          Shipped_Date__c = Date.Today() //Connor Flynn S-402754
        ));
      }
  
      insert theTestOrderItems;
    }

    Test.startTest();

    // Running Thread As Central Regional Manager.
    NV_Wrapper.RegionalDataWrapper theDataWrapper = null;
    system.runAs(theCentralRegionalManagerUser) {
      // Setting Up Current Day.
      ControlApexExecutionFlow.theCurrentDay = 'Mon';

      // Test Case 1 - Extracting Regional Data For Monday.
      theDataWrapper = NV_SalesTotalsController.getSalesTotalsAndUsersForToday();

      // Test Case 2 - Extracting Regional Data For Sunday.
      ControlApexExecutionFlow.theCurrentDay = 'Sun';
      theDataWrapper = NV_SalesTotalsController.getSalesTotalsAndUsersForToday();

      // Test Case 3 - Extracting Regional Data For Saturday.
      ControlApexExecutionFlow.theCurrentDay = 'Sat';
      theDataWrapper = NV_SalesTotalsController.getSalesTotalsAndUsersForToday();

      // Updating Test Order.

      theTestOrder.RecordTypeId = ORDER_NV_RMA_RECORD_TYPE_ID;
      update theTestOrder;

      // Test Case 4 - Extracting Regional Data For Tuesday.
      ControlApexExecutionFlow.theCurrentDay = 'Tue';
      theDataWrapper = NV_SalesTotalsController.getSalesTotalsAndUsersForToday();
    }

    Test.stopTest();
	}
}