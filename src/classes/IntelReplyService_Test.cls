/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 18, 2013
 Purpose       :  Test Class for IntelReplyService.cls
 Reference     :  Task T-205160
 Modifications :  Task T-207120    Dec 23, 2013    Sonal Shrivastava
*******************************************************************************/
@isTest
public class IntelReplyService_Test {

  static testMethod void myUnitTest() {
    
    //Create test records
    List<User> usrList = TestUtils.createUser(2, null, true);
    User usr1 = usrList.get(0);
    User usr2 = usrList.get(0);
    List<Intel__c> intelList; 
    List<Tag__c> tagList;
    FeedComment comment;
    FeedLike lik;
    
    System.runAs(usr1){    	    
	    intelList = TestUtils.createIntel(1, true);
	    tagList = TestUtils.createTag(1, intelList.get(0).Id, true);
	        
	    intelList = [SELECT Id, OwnerId, Owner.Name, Owner.Phone, CreatedDate, Details__c, Flagged_As_Inappropriate__c, LastModifiedDate, Chatter_Post__c, Mobile_Update__c, Intel_Flags__c,
	                  (SELECT Id, Name, Title__c, Account__c, Contact__c, Product__c, User__c, CreatedDate FROM Tags__r)
	                 FROM Intel__c WHERE Id = :intelList.get(0).Id];
	    
	    String chatterPostId = intelList.get(0).Chatter_Post__c;              
	    comment = new FeedComment(FeedItemId = chatterPostId, 
	                                          CommentBody = 'Test Comment Body');
	    insert comment;                                      
	    comment = [SELECT Id, CommentBody, CreatedById, CreatedBy.Name, CreatedDate FROM FeedComment WHERE Id = :comment.Id].get(0);
	                                                                               
	    lik = new FeedLike(FeedItemId = chatterPostId);
	    //insert lik; 
    }            
    
    System.runAs(usr2){
    
	    List<IntelListWrapper.IntelJson>   intelJsonList   = new List<IntelListWrapper.IntelJson>();
	    List<IntelListWrapper.TagJson>     tagJsonList     = new List<IntelListWrapper.TagJson>();
	    List<IntelListWrapper.CommentJson> commentJsonList = new List<IntelListWrapper.CommentJson>();
	    List<IntelListWrapper.LikeJson>    likeJsonList    = new List<IntelListWrapper.LikeJson>();
	    List<IntelListWrapper.AttachmentJson> attJsonList  = new List<IntelListWrapper.AttachmentJson>();
	     
	    tagJsonList.add(new IntelListWrapper.TagJson(intelList.get(0).Tags__r.get(0)));
	    commentJsonList.add(new IntelListWrapper.CommentJson(comment, usr1));
	    	    
	    likeJsonList.add(new IntelListWrapper.LikeJson(lik));
	    intelJsonList.add(new IntelListWrapper.IntelJson(intelList.get(0), usr1, tagJsonList, commentJsonList, likeJsonList, attJsonList));
	    
	    IntelListWrapper wrap = new IntelListWrapper();
	    wrap.intelList = intelJsonList;
	    
	    RestRequest req = new RestRequest();
	    RestResponse res = new RestResponse();
	    
	    req.requestURI = '/services/apexrest/IntelReplyService/*';
	    req.httpMethod = 'POST';
	    req.requestBody = Blob.valueOf(JSON.serialize(wrap)); 
	    
	    RestContext.request = req;
	    RestContext.response = res;
	    
	    test.startTest();
	    Map<String, String> result = IntelReplyService.doPost();
	    System.assertEquals(1, [SELECT Id FROM Tag__c WHERE Id NOT IN :tagList].size());  	    
	    test.stopTest();
    }  
  }
  
}