/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_RegulatoryFormController
//
// 02 Oct 2015 , Megha (Appirio)
// This custom lookup has been used on Medical Order creation and Order Intermediate page.
// Mar 01, 2016, Sunil - Fix searching issues.
// 9/29/2016  Jessica Schilling Case 178025 Added fields to popup window
*/

public without sharing class OpenLookupPopupCtrl {
  public transient List<Account> listRecords {get;set;}
  public List<String> listFieldNames {get; set;}
  public String selectedObject;
  public String fieldName;
  public String searchText{get;set;}
  public String fieldSetName;
  public Boolean isEqualSearch {get;set;}
  public String equalSearchText {get;set;}
  public String parentField {get;set;}
  public String currentContactId {get;set;}
  public String showAllAccounts {get;set;}

  public String recordId {get;set;}

  public OpenLookupPopupCtrl(){
    selectedObject = ApexPages.currentPage().getParameters().get('Object');
    fieldName = ApexPages.currentPage().getParameters().get('fieldName');
    searchText = ApexPages.currentPage().getParameters().get('searchText');
    fieldSetName = ApexPages.currentPage().getParameters().get('fieldSetName');
    currentContactId = ApexPages.currentPage().getParameters().get('currentContactId');
    showAllAccounts = ApexPages.currentPage().getParameters().get('showAllAccounts');

    String isEqualSearchStr = ApexPages.currentPage().getParameters().get('isEqualSearch');

    if(isEqualSearchStr != null && (isEqualSearchStr == '1' || isEqualSearchStr  == 'true')){
      isEqualSearch = true;
    }
    else{
      isEqualSearch= false;
    }

    if(searchText == null){
      searchText  = '';
    }

    listFieldNames = new List<String>();

    //START JSCHILLING Case 178025 9/29/2016
    //Added columns and reordered them
    listFieldNames.add('Name');
    listFieldNames.add('AccountNumber');
    listFieldNames.add('Billing_Address_Formula__c');
    listFieldNames.add('Group_Shipping_Address__c');
    listFieldNames.add('Active__c');
    listFieldNames.add('Billing_Type__c');
    listFieldNames.add('Type');
    listFieldNames.add('Division_Name__c');
    listFieldNames.add('Owner.Alias');
    listFieldNames.add('ERP__c');
    //END JSCHILLING Case 178025 9/29/2016


    equalSearchText = ApexPages.currentPage().getParameters().get('equalText');
    parentField  = ApexPages.currentPage().getParameters().get('pField');
    listRecords = new List<Account>();
    runSearch();
  }

  /*************************************************************************************************
  * Method to run the search with parameters passed via Page Parameters
  *************************************************************************************************/
  public PageReference runSearch() {
    String queryString = '';
    String whereClause = '';
    
    //START Hitesh CASE 00175256 09.05.16 - escaping single quotes and replacing searchText string with searchText
    if(searchText.contains('\'')){
      searchText = searchText.replace('\'','\\\'');
    }
   
    //END Hitesh CASE 00175256 09.05.16 - escaping single quotes and replacing searchText string with soqlSearchText
    
    List<Account> allAccounts = new List<Account>();
    recordId   = 'Id' ; //parentField  != null && parentField  != '' ? parentField   : 'Id';

    Set<Id> eligibleAccountIds = new Set<Id>();

    if(showAllAccounts != 'yes'){
      for(Contact_Acct_Association__c caa :[SELECT Associated_Account__c FROM  Contact_Acct_Association__c WHERE Associated_Contact__c = :currentContactId]){
        eligibleAccountIds.add(caa.Associated_Account__c);
      }
    }


    // IF Search string is blank then using SOQL to display all Accounts when request is coming from Intermediate page.
    if(String.isBlank(searchText) == true && showAllAccounts != 'yes'){
      //Modified below query JSCHILLING Case 178025 9/29/2016 Added fields to query
      queryString = 'SELECT Id, Name, AccountNumber, Billing_Type__c, '+
                    'Billing_Address_Formula__c, Group_Shipping_Address__c, ' + 
                    'Owner.Alias, Type, Active__c, ERP__c, Division_Name__c ' +
                   'FROM Account';


      whereClause += ' WHERE Id IN :eligibleAccountIds ';

      if(fieldName == 'Bill_To_Account__c'){
        whereClause += (whereClause  != '' ? ' AND ' : ' Where ') + 'Billing_Type__c != null AND Billing_Type__c != \'Ship To Address Only\' ' ;
      }
      else{
        whereClause += (whereClause  != '' ? ' AND ' : ' Where ') + 'Billing_Type__c != null AND Billing_Type__c != \'Bill To Address Only\' ' ;
      }
      listRecords = Database.query(queryString + whereClause + ' ORDER BY Name');

      return null;
    }


    // IF Search string is not blank then using SOSL to find through all searchable fields.
    if(String.isBlank(searchText) == false){
        searchText = searchText.replace('*', '');
        //START Hitesh CASE 00175256 10.10.16 - escaping hyphen and replacing searchText string with searchText
        searchText = searchText.replace('-', '\\-');
        //END Hitesh CASE 00175256 10.10.16
      //Modified below query JSCHILLING Case 178025 9/29/2016 Added fields to query
      queryString = 'FIND {' + searchText + '*} IN ALL FIELDS RETURNING Account (Id, Name, AccountNumber,Billing_Type__c, Billing_Address_Formula__c, Group_Shipping_Address__c, Owner.Alias, Type, Active__c, ERP__c, Division_Name__c ';

      if(fieldName == 'Bill_To_Account__c'){
        whereClause += (whereClause  != '' ? ' AND ' : ' Where ') + 'Billing_Type__c != null AND Billing_Type__c != \'Ship To Address Only\' ' ;
      }
      else{
        whereClause += (whereClause  != '' ? ' AND ' : ' Where ') + 'Billing_Type__c != null AND Billing_Type__c != \'Bill To Address Only\' ' ;
      }

      // If request are coming from Order Creation page then show all Billing or Shipping Accounts.
      // If request are coming from intermediate page then show only related Contact records from Junction Object.
      if(showAllAccounts != 'yes'){
        whereClause += (whereClause  != '' ? ' AND ' : ' Where ') + 'Id IN :eligibleAccountIds';
      }

      queryString += String.isNotBlank(whereClause) ? whereClause + ') LIMIT 200' : 'LIMIT 200' ;

      System.debug('@@##@@' + queryString);
      List<List<SObject>> searchList = Search.query(queryString);
      listRecords = (List<Account>)searchList.get(0);
    }
    return null;
  }





}