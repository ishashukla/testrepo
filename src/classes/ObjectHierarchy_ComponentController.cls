// 
// (c) 2012 Appirio, Inc.
//
// ObjectHierarchyComponent
//
//
// 30 Nov 2012   	gescandon@appirio.com  	Original
// 18 Apr 2016 		ddhoot@appirio.com 		Level3 Asset Curation
//

global with sharing class ObjectHierarchy_ComponentController {

	//Remote Action to communicate using JS Remoting
	@AuraEnabled 
	@RemoteAction
	global static Map<String, Object> getObjectHierarchyData(String recordId, String nameFieldAPIName, 
												String parentFieldAPIName, String fieldsToDisplay, String hierarchyLevel) {
		//Wrapper Map used to send configurations & data
		Map<String, Object> treeData = new Map<String, Object>();
		try{
			//Get Object Description based on passed Record Id
			Schema.DescribeSObjectResult objectDescribe = Id.valueOf(recordId).getSobjectType().getDescribe();
            //Validate Name Field
            if(!ObjectHierarchy_Utility.validateField(objectDescribe, nameFieldAPIName)){
               throw new ObjectHierarchy_Utility.HierarchyException(ObjectHierarchy_Utility.INVALID_TITLE_FIELD + nameFieldAPIName); 
            }
            
            //Validate Parent Field
            if(!ObjectHierarchy_Utility.validateField(objectDescribe, parentFieldAPIName)){
               throw new ObjectHierarchy_Utility.HierarchyException(ObjectHierarchy_Utility.INVALID_PARENT_FIELD + parentFieldAPIName); 
            }
            
			//Filter fields which are accessible
			Set<String> validFieldsToDisplay = ObjectHierarchy_Utility.getValidFieldsToDisplay(objectDescribe, fieldsToDisplay);

			//Create Query Configuration 
			ObjectHierarchy_Utility.QueryConfig queryConfig = new ObjectHierarchy_Utility.QueryConfig(objectDescribe.getName(),
																										nameFieldAPIName,
																										parentFieldAPIName);
			//Add filtered accessible fields to query configuration
			queryConfig.fieldsToDisplay.addAll(validFieldsToDisplay);
			//Add Current Record as the base filterId
			queryConfig.filterIds = new Set<Id>{Id.valueOf(recordId)};
			
			//Collect Column Configuration
			treeData.put('columns', ObjectHierarchy_Utility.getColumnConfig(objectDescribe, nameFieldAPIName, validFieldsToDisplay));
			//Collect Hierarchy Data
			treeData.put('data', ObjectHierarchy_Utility.getHierarchyData(queryConfig, hierarchyLevel));
			//If SOQL Limit has reached, inform end user as a warning
			if(ObjectHierarchy_Utility.SOQLLimitReached){
				treeData.put('warning', ObjectHierarchy_Utility.SOQL_LIMIT_REACHED_MSG);
				//Reset Limit reached flag
				ObjectHierarchy_Utility.SOQLLimitReached = false;
			}
		}
		catch(ObjectHierarchy_Utility.HierarchyException e){
			treeData.put('error', e.getMessage());
		}
		catch(Exception e){
			throw new ObjectHierarchy_Utility.HierarchyException(e.getMessage());                                           
		}                                            
		return treeData;
	}
}