/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  Jan 27, 2014
 Purpose       :  Test Class for Case TriggerHandler
*******************************************************************************/
@isTest
private class Endo_CaseTriggerHandlerTest {

  //-----------------------------------------------------------------------------------------------
  // Method for test notifySalesRepIfCaseAlreadyExists method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void notifySalesRepIfCaseAlreadyExists(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    Id recordTypeId = rtByName.getRecordTypeId();
    TestUtils.createRTMapCS(recordTypeId, 'Tech Support', 'Endo');

    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, true).get(0);

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;

    Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0);

    //Create test records
    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    objCase.recordTypeId = recordTypeId;
    insert objCase;

    Case objCase2 = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    objCase2.recordTypeId = recordTypeId;
    insert objCase2;



    Test.stopTest();
  }


  //-----------------------------------------------------------------------------------------------
  // Method for test notifySalesRepIfCaseAlreadyExists method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void autoPopulateSalesRep(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    Id recordTypeId = rtByName.getRecordTypeId();
    TestUtils.createRTMapCS(recordTypeId, 'Tech Support', 'Endo');

    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
    //Added for S-397685
    acc.Endo_Active__c = true;
    //end S-397685
    insert acc;

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    con.RecordTypeId =   Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact').getRecordTypeId();
    insert con;

    Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0);

    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    objCase.Sales_Rep__c = null;
    objCase.RecordTypeId = recordTypeId;
    insert objCase;
    Test.stopTest();

    objCase = [SELECT Id, Sales_Rep__c FROM Case WHERE Id = :objCase.Id].get(0);
    // Verify Sales Rep is populating
    System.assertEquals(objCase.Sales_Rep__c, con.Id);

    //Case objCase2 = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    //objCase2.recordTypeId = recordTypeId;
    //insert objCase2;

  }
  //-----------------------------------------------------------------------------------------------
  // Method for test populateAssignedStateForEmailToCase method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testPopulateAssignedStateForEmailToCase(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair Order Billing');
    Id recordTypeId = rtByName.getRecordTypeId();
    TestUtils.createRTMapCS(recordTypeId, 'Repair Order Billing', 'Endo');

    Account acc = Endo_TestUtils.createAccount(1, true).get(0);

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;

    Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0);


    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    objCase.recordTypeId = recordTypeId;
    objCase.Origin = 'Email-to-Case';
    objCase.Description = 'Assigned State<MI>Account Number<123045>';

    Test.startTest();
    insert objCase;
    Test.stopTest();

    objCase = [SELECT Id, Assigned_State__c FROM Case WHERE Id = :objCase.Id].get(0);
    System.assert(objCase.Assigned_State__c == 'MI');
  }
  //-----------------------------------------------------------------------------------------------
  // Method for test populateAssignedStateForEmailToCase method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testUpdateAccountMoodValue(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    Id recordTypeId = rtByName.getRecordTypeId();
    TestUtils.createRTMapCS(recordTypeId, 'Tech Support', 'Endo');

    Account acc = Endo_TestUtils.createAccount(1, true).get(0);

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;

    Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0);


    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    objCase.RecordTypeId = recordTypeId;
    Test.startTest();
      insert objCase;
      objCase.Status = 'Closed';
      objCase.Mood_of_Customer__c = 'Very Satisfied';
      update objCase;
    Test.stopTest();

    acc = [SELECT Id, Mood_of_Customer__c FROM Account WHERE Id = :acc.Id].get(0);
    System.assert(acc.Mood_of_Customer__c == 'Very Satisfied');

  }
}