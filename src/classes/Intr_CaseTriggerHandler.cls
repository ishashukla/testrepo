/**================================================================      
* Appirio, Inc
* Name: Intr_CaseTriggerHandler
* Description: Handler class for Case Trigger(T-500173)
* Created Date: May 05, 2016
* Created By: Kajal Jalan
* 
* Date Modified    Modified By             Description of the update
* 
==================================================================*/
public without sharing class Intr_CaseTriggerHandler implements CaseTriggerHandlerInterface{    
    /**================================================================      
    * Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/
    public static boolean IsActive(){
        return true;
    }
    
    public static void OnBeforeInsert(List<Case> newCases){
        populateLastStatusChange(newCases, null);
    }
    public static void OnAfterInsert(List<Case> newCases){}
    public static void OnAfterInsertAsync(Set<ID> newCaseIDs){}
    
    public static void OnBeforeUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){
        checkNoActivityAssociated(newCases, oldCaseMap);
        populateLastStatusChange(newCases, oldCaseMap);
    }
    public static void OnAfterUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){}
    public static void OnAfterUpdateAsync(Set<ID> updatedCaseIDs){}
    
    public static void OnBeforeDelete(List<Case> CasesToDelete, Map<ID, Case> caseMap){}
    public static void OnAfterDelete(List<Case> deletedCases, Map<ID, Case> caseMap){}
    public static void OnAfterDeleteAsync(Set<ID> deletedCaseIDs){}
    
    public void OnUndelete(List<Case> restoredCases){}
    /**================================================================      
    * End of Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/

    // method to populate last status change field on case when status on case is changed.
    private static void populateLastStatusChange(list<Case> lstNewCases,map<Id,Case> mapOldCases){
        for(Case c : lstNewCases){
            if((mapOldCases == null || mapOldCases.get(c.Id).Status != c.Status)){
                c.Last_Status_Change__c = system.now();                
            }
        }        
    }
    
    //----------------------------------------------------------------------------------------------
    //  check NoActivity Associated before closing a Case
    //----------------------------------------------------------------------------------------------
    public static void checkNoActivityAssociated(List<Case> lstNewCases, Map<Id, Case> oldCaseMap){
        Set<Id> closedCaseIds = new Set<Id>();
        Set<Id> errCaseIds = new Set<Id>();
        
        for(Case cs : lstNewCases){
            if(cs.status == 'Closed' && cs.status != oldCaseMap.get(cs.id).status){
                closedCaseIds.add(cs.id);
            }
        }
        
        if(!closedCaseIds.isEmpty()){
            for(Case cs : [select id , (select id  from OpenActivities limit 1) from Case where id in : closedCaseIds]){
                // check Open Activity
                System.debug('::cs.OpenActivities'+cs.OpenActivities);
                if(!cs.OpenActivities.isEmpty()){
                    //newCaseMap.get(cs.id).addError(System.Label.Case_Closure_VR_Message);
                    errCaseIds.add(cs.id);
                }
            }
        }

        for(Case cs : lstNewCases){
            if(errCaseIds.contains(cs.id) ){
                cs.addError(System.Label.Case_Closure_VR_Message);
            }
        }
    }  
}