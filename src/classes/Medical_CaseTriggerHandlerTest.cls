/*******************************************************************************
 Author        :  Appirio JDC (Megha Agarwal)
 Date          :  Oct 5th, 2015
 Description       :  Test class for Medical_CaseTriggerHandler
*******************************************************************************/
@isTest
private class Medical_CaseTriggerHandlerTest {

  private static List<Account>  accounts;
  private static List<Contact> contacts;
  private static List<Case>  cases;
  //private static List<Asset__c> assets;
  private static List<Asset__c> endoAssets;
  private static List<Case_Asset__c> caseAssets;

  	//Added by Shubham for Story S-437706 #Start
  	private static testMethod void unitTest(){
  		createTestData();
  		List<Regulatory_Question_Answer__c> rqaList = Medical_TestUtils.createRegulatoryQuesAnswer(1, cases[0].Id, true);
  		Set<Id> ids = new Set<Id>();
  		ids.add(rqaList[0].Id);
  		Test.startTest();
  		Medical_TW_Utility.invokeTrackWiseWebService(ids);
  		Test.stopTest();
  	}
  	//Added by Shubham for Story S-437706 #End

	private static testMethod void testOpenActivities() {
	  createTestData();

	  Task newTask = new  Task(Subject = 'test', whatId = cases.get(0).id ,  status = 'Not Started', ActivityDate =Date.today().addDays(10));
	  insert newTask;

	  Test.startTest();
	    List<Task> tasks = [select id , status from Task where whatId = : cases.get(0).id];
	    System.debug('::tasks'+tasks);
        //cases[0].AssetId = assets[0].Id;
	    cases.get(0).status = 'Closed';
	    try{
	      update cases;
	     } catch(Exception ex){
	         System.assert(ex.getMessage().contains(System.Label.Case_Closure_VR_Message));
	     }

	  Test.stopTest();

	}

	private static void createTestData(){
	  accounts = Medical_TestUtils.createAccount(1, true);
	  contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);
    //assets = Medical_TestUtils.createAsset(1, accounts[0].Id, true);

    Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
    TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
	  cases = Medical_TestUtils.createCase(1, accounts.get(0).id, contacts.get(0).id , false);
    cases[0].RecordTypeId = MedicalrecordTypeId;
    insert cases;
    endoAssets = Endo_TestUtils.createAsset(1, accounts.get(0).id, true);
     caseAssets = Endo_TestUtils.createCaseAssetJunction(1, cases.get(0).id, endoAssets.get(0).id, true);
	}
  static testMethod void testCaseAssetJunctionCreation(){
  	createTestData();
  	List<Asset> assets = Medical_TestUtils.createAsset(1,accounts.get(0).Id,true);
  	Case cs = cases.get(0);
  	cs.AssetId = assets.get(0).Id;
  	List<Case_Asset_Junction__c> lstJunction = [SELECT Id FROM Case_Asset_Junction__c];
  	System.assert(lstJunction.isEmpty());

  	update cs;

  	lstJunction = [SELECT Id FROM Case_Asset_Junction__c];
    System.assert(!lstJunction.isEmpty(),'The junction should be inserted');
  }

  static testMethod void testSendCasesToTrackWise(){
  	createTestData();

    Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
    TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
  	Medical_TestUtils.createRegulatoryQuesAnswer(1,cases.get(0).Id,true);
  	Case cs = cases.get(0);
    cs.RecordTypeId = MedicalrecordTypeId;
    //cases[0].AssetId = assets[0].Id;
    cs.Status = 'Closed';
    //insert cs;

  	Test.startTest();
  	update cs;
  	Test.stopTest();
  }


  static testMethod void testAssetIsRequired(){
    createTestData();
    //List<Asset> assets = Medical_TestUtils.createAsset(1,accounts.get(0).Id,true);
    Case cs = cases.get(0);
    cs.Subject = 'test changed';
    cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
    try{
      update cs;
    }
    catch(Exception ex){
    	System.assert(String.isBlank(ex.getMessage()) == false);
    }
  }
}