// 
// (c) 2016 Appirio, Inc.
//
// T-500180, This is a batch class for updating Cases for Inactivity
//
// 11 May, 2016  Shreerath Nair  Original 

global without sharing class  Intr_UpdateCaseStatusBatch  implements Database.Batchable<SObject>, Database.AllowsCallouts {

  // @description: start of batch class 
  //               Gets all cases that are Inactive for a particular time
  // @param: none
  // @return: void
  
  global Database.QueryLocator start(Database.BatchableContext BC){ 
    
    //Get Custom Setting instance for the user
    Instruments_CCKM_Common_Setting__c intrCommSetting = Instruments_CCKM_Common_Setting__c.getInstance();
  
    // create a past date according to the auto case closure delay days
    String caseStatus = Intr_Constants.INSTRUMENTS_CCKM_CASE_STATUS;
    
    
    
    SET<ID> InstrumentRecordTypeIds = new Set<ID>{Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_NON_PRODUCT).getRecordTypeId(),
                                                  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId(),
                                                  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_EMPLOYEE_COMPLAINT).getRecordTypeId(),
                                                  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_DEPOT).getRecordTypeId(),
                                                  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_FIELD_SERVICE).getRecordTypeId()};
    
    System.debug('***RecordTYpeID' +  InstrumentRecordTypeIds);
    //Check the custom setting field if its true or not (If case is awaiting customer for long time then it is set as true)
    if(intrCommSetting!=null){
        
        Date lastActivityDate = System.TODAY() - (INTEGER)(intrCommSetting.Auto_Case_Closure_Delay_Days__c);
        
        if(intrCommSetting.Enable_Auto_Case_Closure__c== true){
            
            //query to get all cases that are inactive 
            
            String query = 'Select Id,Status,Close_Notes__c from Case where status = :caseStatus AND lastModifiedDate < :lastActivityDate AND Last_Status_Change__c < :lastActivityDate AND RecordTypeId IN : InstrumentRecordTypeIds';
            
            return Database.getQueryLocator(query);
        }
        else
            return null;
            
    }
    else
        return null;
        
  }
  // @description: Update Cases
  // @param: none
  // @return: void
 global void execute(Database.BatchableContext BC, List<sObject> caseList){  
     
    try {   
        system.debug('***caseList in execute>>'+caseList);
        
        //loop to traverse all cases that are inactive
        for(Case cas : (List<Case>)caseList) {
            
            system.debug('***update case>>'+cas);
            //update Inactive cases fields
            cas.Status = 'Closed';
            
            cas.Close_Notes__c = 'Automatically Closed Due to Customer Inactivity';
        }
        if(caseList.size() > 0) {
            
            //update cases with new status  
            //update caseList; 
            Database.SaveResult[] results = Database.Update(caseList, false);
            System.debug('****caselist after update' +results );
        }
    }
    catch(Exception ex) {
        System.debug('Batch update failed Intr_UpdateCaseStatusBatch : Error > '+ex);
    }
    
   
 }
  // @description: Finish method of batch job
  // @param: none
  // @return: void
  global void finish(Database.BatchableContext BC){  
    
  }
  
  

}