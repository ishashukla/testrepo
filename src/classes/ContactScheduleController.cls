// (c) 2015 Appirio, Inc.
//
// Class Name: ContactScheduleController
// Description: Contoller Class for ContactSchedule Page. Class to show calendar for blocked schedule on the contact related list.
//
// January 20 2016, Prakarsh Jain  Original (T-454845)
//
public class ContactScheduleController {
   public List<BlockScheduleWrapper> lstBlockScheduleWrapper;
  public String accConPrefix;  
  public Contact con{get;set;}
  public List<Block_Schedule__c> lstBlckSch;
  public Block_Schedule__c blckSchedule{get;set;}
  public BlockScheduleWrapper wrapperBlck;
  public String jSONString{get;set;}
  public User usr{get;set;}
  public String timeZoneString{get;set;}
  public map<String, String> mapMonths; 
  public String dayOfWeek{get;set;}
  public map<String, String> mapDays;
  public String saveDt{get;set;}
  public String strtDate{get;set;}
  public String endDt{get;set;}
  public String titl{get;set;}
  public String blckRecId{get;set;}
  public List<Contact_Acct_Association__c> lstConAccAsso{get;set;}
  public String selectedContactOrAccount{get;set;}
  public String insertContactOrAccount{get;set;}
  public String currentDt{get;set;}
  public String startDt{get;set;}
  public Map<String,String> dateToDayMap{get;set;}
  public String assignToContactOrAccount{get;set;}
  public list<String> lstAcc;
  public Map<id, account> accMap;
  public Account acct;
  public Date datetoday {get;set;}
  public Boolean isPrintView {get;set;}
  public Boolean inContactTimezone{get;set;}
  public List<String> monthStrings = new List<String>{'','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'};
  public Boolean inSF1 {get;set;}
  public String contactTimezone{get;set;}
  
  // Constructor 
  public ContactScheduleController(ApexPages.StandardController sc){
    isPrintView = false;
    lstConAccAsso = new List<Contact_Acct_Association__c>();
    acct = new Account();
    mapMonths = new map<String, String>();
    mapDays = new map<String, String>(); 
    dateToDayMap =new map<String, String>();
    con = new Contact();
    lstBlckSch = new List<Block_Schedule__c>();
    usr = new User();
    lstAcc = new list<String>();
    titl = '';
    inSF1 = false;
    if(UserInfo.getUiTheme() == 'Theme4t'){
      inSF1 = true;
    }
    blckSchedule = new Block_Schedule__c(notes__c = '', title__c = '');
    mapDays = currentDateMap();
    lstBlockScheduleWrapper = new List<BlockScheduleWrapper>();
    mapMonths.put('Jan', '01');
    mapMonths.put('Feb', '02');
    mapMonths.put('Mar', '03');
    mapMonths.put('Apr', '04');
    mapMonths.put('May', '05');
    mapMonths.put('Jun', '06');
    mapMonths.put('Jul', '07');
    mapMonths.put('Aug', '08');
    mapMonths.put('Sep', '09');
    mapMonths.put('Oct', '10');
    mapMonths.put('Nov', '11');
    mapMonths.put('Dec', '12');
    isPrintView = (ApexPages.currentPage().getParameters().get('isPrintView') == 'true') ? true : false;
    selectedContactOrAccount = (ApexPages.currentPage().getParameters().get('prevAccount') != null && ApexPages.currentPage().getParameters().get('prevAccount') != '' ) ? ApexPages.currentPage().getParameters().get('prevAccount') : 'All';
    insertContactOrAccount = 'All';
    assignToContactOrAccount = selectedContactOrAccount;
    accConPrefix = ApexPages.currentPage().getParameters().get('id');
    con = [SELECT Id, Name, AccountId, Account.Name, Timezone__c 
           FROM Contact 
           WHERE Id =: accConPrefix].get(0);
    contactTimezone = con.Timezone__c; 
    accMap = new Map<Id, Account>();
    accMap.putAll([SELECT id, Name FROM Account WHERE Id  =: con.AccountId]);
    if(ApexPages.currentPage().getParameters().get('selectedContactOrAccount')!=null && ApexPages.currentPage().getParameters().get('selectedContactOrAccount')!=''){
      selectedContactOrAccount = ApexPages.currentPage().getParameters().get('selectedContactOrAccount');
      showSchAsPerAccounts(selectedContactOrAccount, accConPrefix);
    }
    else{
      selectedContactOrAccount = 'All';
      contactScheduleCon(accConPrefix);
    }
   }
  
  // Helper method to get the list of all Accounts related to the contact and show the blocked calendar on Contact page.
  public void contactScheduleCon(String accConPrefix){
    usr = [SELECT Id, Name, TimeZoneSidKey
           FROM User 
           WHERE Id =: UserInfo.getUserId()];
    lstConAccAsso = [SELECT Associated_Account__c, Associated_Contact__c, Associated_Account__r.Name
                     FROM Contact_Acct_Association__c
                     WHERE Associated_Contact__c =: accConPrefix];
    if(accConPrefix != null && accConPrefix != ''){
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id, Save_in_Contact_Timezone__c
                    FROM Block_Schedule__c 
                    WHERE Contact__c =: accConPrefix];
    }
    if(lstBlckSch.size() == 0){
      Block_Schedule__c blck = new Block_Schedule__c();
      blck = createBlockScheduleData(blck);
      lstBlckSch.add(blck);
    }
    convertToJSON(lstBlckSch);
  }
  
  //Method to show calendar as per the selected Account from picklist.
  public void showSchAsPerAccounts(String selectedContactOrAccount, String accConPrefix){
    usr = [SELECT Id, Name, TimeZoneSidKey
           FROM User 
           WHERE Id =: UserInfo.getUserId()];
    lstConAccAsso = [SELECT Associated_Account__c, Associated_Contact__c, Associated_Account__r.Name
                     FROM Contact_Acct_Association__c
                     WHERE Associated_Contact__c =: accConPrefix];
    if(accConPrefix != null && accConPrefix != ''){
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id, Save_in_Contact_Timezone__c
                    FROM Block_Schedule__c 
                    WHERE Account__c =: selectedContactOrAccount AND Contact__c =: accConPrefix];
    }
    if(lstBlckSch.size() == 0){
      Block_Schedule__c blck = new Block_Schedule__c();
      blck = createBlockScheduleData(blck);
      lstBlckSch.add(blck);
    }
    convertToJSON(lstBlckSch);
  }
  
  //Method to convert the list of Block Schedules to JSON.
  public void convertToJSON(List<Block_Schedule__c> lstBlckSch){
    lstBlockScheduleWrapper = new List<BlockScheduleWrapper>();
    for(Block_Schedule__c blck : lstBlckSch){
      wrapperBlck = new BlockScheduleWrapper();
      wrapperBlck.account = blck.Account__r.Name;
      wrapperBlck.accountid = blck.Account__r.Id;
      wrapperBlck.id = blck.Id;
      wrapperBlck.start = convertDateTime(blck.starting_Time__c,blck.dow__c);
      wrapperBlck.endDate = convertDateTime(blck.Ending_Time__c,blck.dow__c);
      //TM Checked this and its not causing the account picklist bug
      if(wrapperBlck.endDate.contains('T00:00:00.000')){
        wrapperBlck.endDate = wrapperBlck.endDate.replace('T00:00:00.000', 'T24:00:00.000');
      }
      if(wrapperBlck.account != null || wrapperBlck.account != ''){

        wrapperBlck.title = (blck.title__c != null) ? blck.title__c : '';
        wrapperBlck.title += '</br>Hospital:' + wrapperBlck.account;
      }
      else{
        wrapperBlck.title = (blck.title__c != null) ? blck.title__c : '';
      } 
      lstBlockScheduleWrapper.add(wrapperBlck);
    }
    jSONString = JSON.serialize(lstBlockScheduleWrapper); 
    jSONString = jSONString.replaceAll('endDate', 'end');
  }
  
  //Method to convert the date in String format to use it in JSON
  public String convertDateTime(String dateTimeStartEnd,String dayofWeek){
    String dtStrtEnd = dateTimeStartEnd;
    if(dateTimeStartEnd.contains('PM') && !dateTimeStartEnd.contains('12')){
      dateTimeStartEnd = dateTimeStartEnd.substring(0,2);
      dtStrtEnd = dtStrtEnd.substring(3,5);
      Integer i = integer.valueOf(dateTimeStartEnd);
      i += 12;
      dateTimeStartEnd = String.valueOf(i);
    }
    else if(dateTimeStartEnd.contains('AM') && dateTimeStartEnd.contains('12')){
      dateTimeStartEnd = '00';
      dtStrtEnd = dtStrtEnd.substring(3,5);
    }
    else{
     dateTimeStartEnd = dateTimeStartEnd.substring(0,2);
     dtStrtEnd = dtStrtEnd.substring(3,5); 
    }
    DateTime currentDate = System.Now();
    String customerTimeZoneSidId = usr.TimeZoneSidKey;
    System.debug('>>>>>>>>sb'+customerTimeZoneSidId);
    TimeZone customerTimeZone = TimeZone.getTimeZone(customerTimeZoneSidId);
    Integer offsetToCustomersTimeZone = customerTimeZone.getOffset(currentDate);
    Integer correctTimeInMin = offsetToCustomersTimeZone / (1000 * 60);
    Integer correctTimeInMin1 = offsetToCustomersTimeZone / (1000 * 60);
    if(correctTimeInMin1 < 0){
      correctTimeInMin =correctTimeInMin * -1;
    }
    Integer correctTimeInMinNew = math.mod(correctTimeInMin, 60);
    Integer correctTimeInHrNew = correctTimeInMin/60; 
    String correctTimeInMinNewStr = String.valueOf(correctTimeInMinNew);
    if(correctTimeInMinNew < 10){
      correctTimeInMinNewStr = '0'+correctTimeInMinNewStr;
    }
    String correctTimeInHrNewStr = String.valueOf(correctTimeInHrNew);
    String yr = String.valueOf(currentDate.year());
    String mth = String.valueOf(currentDate.month());
    if(mth.length() == 1){
      mth = '0' + mth;
    }
    String dt = String.valueOf(currentDate.day());
    String hr = dateTimeStartEnd;
    String convertedTime = '';
    if(integer.valueOf(correctTimeInHrNewStr) < 10){
      if(correctTimeInMin1 < 0){
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
      }
      else{
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        
      }
    }
    else{
      if(correctTimeInMin1 < 0){
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
      }
      else{
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
      }
    }
    return convertedTime;
  }
  
  public String getCorrectedTimeZone(String dateTimeStartEnd, String dt){
    String correctedTimeZone;
   if(con.Timezone__c != null){
        String tz = con.Timezone__c;
        String tzSubAfter = tz.substringBetween('(', ')');
        String tempString;
        system.debug('dt+++'+dt);
        String saveDtYr = dt.subString(0,4); 
        String saveDtMon = dt.subString(5,7);
        String saveDtdt = dt.subString(8,10);
        system.debug('saveDtMon>>>'+saveDtMon);
        system.debug('saveDtdt>>>'+saveDtdt);
        system.debug('saveDtYr>>>'+saveDtYr);
       system.debug(dateTimeStartEnd+'dateTimeStartEnd*********');
       
        String strt = dateTimeStartEnd;
        system.debug(strt+'strt*********');
        //system.assert(false, strt + '--leng' + strt.length());
        String strtHr = strt.subString(0,2);
        String strtMin = strt.subString(3,5);
        String strtNew  = dateTimeStartEnd.subString(5,7);
        system.debug(saveDtMon + saveDtdt + saveDtYr);
        system.debug('1>>>'+Integer.valueOf(saveDtYr));
        system.debug('2>>>'+Integer.valueOf(saveDtMon));
        system.debug('3>>>'+Integer.valueOf(saveDtdt));
        system.debug('4>>>'+Integer.valueOf(strtHr));
        system.debug('5>>>'+Integer.valueOf(strtMin));
        DateTime dttime = DateTime.newInstanceGmt(Integer.valueOf(saveDtYr),Integer.valueOf(saveDtMon),Integer.valueOf(saveDtdt),Integer.valueOf(strtHr),Integer.valueOf(strtMin),0);
        system.debug(dttime);
        User usr = [SELECT Id, Name, TimeZoneSidKey
                   FROM User 
                   WHERE Id =: UserInfo.getUserId()];
        String customerTimeZoneSidId = tzSubAfter;
        system.debug('customerTimeZoneSidId>>>'+customerTimeZoneSidId);
        TimeZone customerTimeZone = TimeZone.getTimeZone(customerTimeZoneSidId);
        system.debug('customerTimeZone>>>'+customerTimeZone);
        Integer offsetToCustomersTimeZone = customerTimeZone.getOffset(dttime);
        system.debug('offsetToCustomersTimeZone>>>'+offsetToCustomersTimeZone);
        Integer correctTimeInMin = offsetToCustomersTimeZone / (1000 * 60);
        system.debug('correctTimeInMin>>>'+correctTimeInMin);
        Integer correctTimeInMin1 = (correctTimeInMin < 0) ? (-1) * correctTimeInMin : correctTimeInMin * -1; 
        system.debug('correctTimeInMin1>>>'+correctTimeInMin1);
        DateTime dttime2 = dttime.addMinutes(correctTimeInMin1);
        system.debug('dttime2>>>'+dttime2);
        //String dow = dttime2.format('EEEE');
        correctedTimeZone = String.valueOf(dttime2);
      }
      system.debug('correctedTimeZone>>>'+correctedTimeZone);
      return correctedTimeZone;
  }
  
  //Action Function method to save or update the records.
  public PageReference saveSchedule(){
    PageReference pgRef;
    String saveDtMon = saveDt.subString(1,4);
    String saveDtdt = saveDt.subString(5,7);
    String saveDtYr = saveDt.subString(8,12);
    DateTime convertedDateFromString;
    if(mapMonths.containsKey(saveDtMon)){
      saveDt = saveDtYr+'-'+mapMonths.get(saveDtMon)+'-'+saveDtdt;
      convertedDateFromString = Date.valueOf(saveDt);
      dayOfWeek = dateToDayMap.get(saveDt);
    }
    String strtDateHr;
    String strtDateMin;
    String endDtHr;
    String endDtMin;
  /*  if(inContactTimezone!=null && inContactTimezone){
      String strtDate = getCorrectedTimeZone(strtDate, saveDt);
      system.debug('strtDate>>>'+strtDate);
      String endDt = getCorrectedTimeZone(endDt, saveDt);
      system.debug('endDt>>>'+endDt);
      DateTime dtt = DateTime.valueOf(strtDate);
      system.debug('dtt>>>'+dtt);
      dayOfWeek = dtt.format('EEEE');
      strtDateHr = strtDate.subString(11,13);
      strtDateMin = strtDate.subString(14,16);
      endDtHr = endDt.subString(11,13);
      endDtMin = endDt.subString(14,16);
      system.debug('strtDateHr>>>'+strtDateHr);
      system.debug('strtDateMin>>>'+strtDateMin);
      system.debug('endDtHr>>>'+endDtHr);
      system.debug('endDtMin>>>'+endDtMin);
      strtDate = strtDateHr + ':' + strtDateMin;
      endDt = endDtHr + ':' + endDtMin;
      if(Integer.valueOf(strtDateHr) <= 12){
        strtDate = strtDate + 'AM';
      }
      else if(Integer.valueOf(strtDateHr) > 12){
        Integer tempInt = Integer.valueOf(strtDateHr);
        tempInt = tempInt - 12;
        String tempStr = String.valueOf(tempInt);
        strtDate = tempStr + ':' + strtDateMin + 'PM';
      }
      
      if(Integer.valueOf(endDtHr) <= 12){
        endDt = endDt + 'AM';
      }
      else if(Integer.valueOf(endDtHr) > 12){
        Integer tempInt = Integer.valueOf(endDtHr);
        tempInt = tempInt - 12;
        String tempStr = String.valueOf(tempInt);
        endDt = tempStr + ':' + endDtMin + 'PM';
      }
     
      
    } */
    system.debug('strtDate???'+strtDate);
    system.debug('endDt???'+endDt);
    blckSchedule.dow__c = dayOfWeek;
    blckSchedule.starting_Time__c = strtDate;
    blckSchedule.Ending_Time__c = endDt;
    blckSchedule.title__c = (titl != null && titl != 'null' && titl != '') ? titl : '';
    blckSchedule.Contact__c = accConPrefix;
    if(assignToContactOrAccount == 'All' ||(assignToContactOrAccount == '' && assignToContactOrAccount == null)){
      blckSchedule.Account__c = lstAcc.get(0);
    }
    else{
      blckSchedule.Account__c = assignToContactOrAccount;
    }
    if(lstAcc.size() <= 0){
      blckSchedule.Account__c = acct.Name;
    }
    
    system.debug('blckRecId>>>'+blckRecId);
    try{
      if(blckRecId != null && blckRecId != ''){
        blckSchedule.Id = blckRecId;
        update blckSchedule;
      }
      else{
        insert blckSchedule;
      }
    }catch(Exception e){
    }
    if(assignToContactOrAccount == 'All' ||(assignToContactOrAccount == '' && assignToContactOrAccount == null)){
      //pgRef  = new PageReference('/apex/ContactSchedule?id='+accConPrefix+'&prevAccount='+assignToContactOrAccount+'&selectedContactOrAccount='+lstAcc.get(0));
      system.debug('1>>>');
      selectedContactOrAccount = lstAcc.get(0);
      pgRef  = new PageReference('/apex/ContactSchedule?id='+accConPrefix+'&prevAccount='+assignToContactOrAccount);
    }
    else if(selectedContactOrAccount != assignToContactOrAccount){
     system.debug('2>>>');
     selectedContactOrAccount=assignToContactOrAccount;
     pgRef = new PageReference('/apex/ContactSchedule?id='+accConPrefix+'&prevAccount=All');
    }
    else{
      system.debug('3>>>');
      pgRef = new PageReference('/apex/ContactSchedule?id='+accConPrefix+'&prevAccount='+assignToContactOrAccount+'&selectedContactOrAccount='+assignToContactOrAccount);
    }
    
    pgRef.setRedirect(true);
    return pgRef;
  }
  
  //Action Function method to delete records
  public PageReference deleteSchedule(){
    try{
      delete[SELECT Id 
             FROM Block_Schedule__c 
             WHERE Id =: blckRecId];
    }catch(Exception e){
    }
    PageReference pgRef = new PageReference('/apex/ContactSchedule?id='+accConPrefix);
    pgRef.setRedirect(true);
    return pgRef;
  }
  
  public map<String,String> currentDateMap(){
    Date dt = Date.Today();
    if(dt.day() <= 7){
        dt = dt.addDays(7);
    }else if(dt.day() > 20){
        dt = dt.addDays(-7);
    }
    datetoday = dt;
    Date strtWeek = dt.toStartOfWeek();
    Date strtWeekMon = strtWeek.addDays(1);
    Date strtWeekTue = strtWeek.addDays(2);
    Date strtWeekWed = strtWeek.addDays(3);
    Date strtWeekThur = strtWeek.addDays(4);
    Date strtWeekFri = strtWeek.addDays(5);
    Date endWeek = strtWeek.addDays(6);
    String strtWeekString = String.valueOf(strtWeek);
    String strtWeekStringMon = String.valueOf(strtWeekMon);
    String strtWeekStringTue = String.valueOf(strtWeekTue); 
    String strtWeekStringWed = String.valueOf(strtWeekWed);
    String strtWeekStringThur = String.valueOf(strtWeekThur);
    String strtWeekStringFri = String.valueOf(strtWeekFri);
    String endWeekString = String.valueOf(endWeek);
    mapDays.put('Sunday', strtWeekString);
    mapDays.put('Monday', strtWeekStringMon);
    mapDays.put('Tuesday', strtWeekStringTue);
    mapDays.put('Wednesday', strtWeekStringWed);
    mapDays.put('Thursday', strtWeekStringThur);
    mapDays.put('Friday', strtWeekStringFri);
    mapDays.put('Saturday', endWeekString);
    dateToDayMap.put(strtWeekString,'Sunday');
    dateToDayMap.put(strtWeekStringMon,'Monday');
    dateToDayMap.put(strtWeekStringTue,'Tuesday');
    dateToDayMap.put(strtWeekStringWed,'Wednesday');
    dateToDayMap.put(strtWeekStringThur,'Thursday');
    dateToDayMap.put(strtWeekStringFri,'Friday');
    dateToDayMap.put(endWeekString,'Saturday');
    return mapDays;
  }
  
  public list<selectOption> getLstAssoContactsOrAccounts(){
    //create list of checkboxes
    list<selectOption> myOptions = new list<selectOption>();
    myOptions.add(new selectOption('All','All'));
    if(lstConAccAsso.size() > 0){
      for(Contact_Acct_Association__c conAcc : lstConAccAsso){
        myOptions.add(new selectOption(conAcc.Associated_Account__c,conAcc.Associated_Account__r.Name));
        lstAcc.add(conAcc.Associated_Account__c);
      }
    }
    if(lstConAccAsso.size() <= 0){
      acct = accMap.get(con.AccountId);
    } 
    return myOptions;
  }
  
  public list<selectOption> getLstContactsOrAccounts(){
    //create list of checkboxes
    list<selectOption> myOptions = new list<selectOption>();
    if(lstConAccAsso.size() > 0){
      for(Contact_Acct_Association__c conAcc : lstConAccAsso){
        myOptions.add(new selectOption(conAcc.Associated_Account__c,conAcc.Associated_Account__r.Name));
      }
    }
    return myOptions;
  }
  
  //Method to show Accounts Scheduled for a Contact on Calendar
  public void showSchAccounts(){
    assignToContactOrAccount = selectedContactOrAccount;
    if(selectedContactOrAccount == 'All'){
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id, Save_in_Contact_Timezone__c
                    FROM Block_Schedule__c 
                    WHERE Contact__c =: accConPrefix];
      if(lstBlckSch.size() == 0){
        Block_Schedule__c blck = new Block_Schedule__c();
        blck = createBlockScheduleData(blck);
        lstBlckSch.add(blck);
      }
      convertToJSON(lstBlckSch);
    }
    else{
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id, Save_in_Contact_Timezone__c
                    FROM Block_Schedule__c 
                    WHERE Contact__c =: accConPrefix AND Account__c =: selectedContactOrAccount];
      if(lstBlckSch.size() == 0){
        Block_Schedule__c blck = new Block_Schedule__c();
        blck = createBlockScheduleData(blck);
        lstBlckSch.add(blck);
      }
      convertToJSON(lstBlckSch);
    }
  }
  
  //Blank Method to get Selected Account Value
  public void setInitialDates(){
    
  }
  
  //Method to create dummy data if a new account is created
  public Block_Schedule__c createBlockScheduleData(Block_Schedule__c blck){
    blck.starting_Time__c = '02:00PM';
    blck.Ending_Time__c = '01:00PM';
    blck.title__c = 'test';
    blck.dow__c = 'Sunday';
    return blck;
  }
  
  //Wrapper Class to create JSON
  public class BlockScheduleWrapper{
   public String account;
   public String accountid;
   public String id;
   public String start;
   public String endDate;
   public String title;
  }
}