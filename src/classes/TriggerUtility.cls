/**================================================================      
 * Appirio, Inc
 * Name: CaseTriggerHandlerInterface
 * Description: Top level interface describing the methods available to all Stryker or division specific functionality
 * Created Date: 10 Aug 2016
 * Created By: Nick Whitney (Appirio)
 * 
 * Date Modified      Modified By      Description of the update
 * 
 ==================================================================*/
public class TriggerUtility {
    public static string getUserGroup(){
        return 'COMM';
    }
}