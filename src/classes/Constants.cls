/**================================================================      
* Appirio, Inc
* Name: Constants 
* Description: Class having constant values
* Created Date: Dec 6, 2013
* Created By: Sonal Shrivastava (Appirio)
*
* Modified On       Modified By               Description
* 25 Mar 2015       Naresh Kr Ojha                  Added Flex Constants
* 12 Apr 2016       Noopur                    Modified constants for Opportunity and Contact triggers
* 13 Apr 2016           Kirti                     Added Constant for Order Trigger T-492006
* 18 Apr 2016           Meghna Vijay              Added Constant for Case Record Type T-489095
* 06 May 2016       Meghna Vijay              Added Constant for Project Team RT of Custom_Account_Team Object T-500924
* 11 May 2016       Meghna Vijay              Added Constant for Account Team RT of Custom_Account_Team Object T-500924
* 31 May 2016       Meghna Vijay              Added Constant for Sales Operations Support RT of Case Object I-215998
* 07 Jul 2016       Jagdeep Juneja            Added Constant for Case record type, for T-517828.
* 08 Jul 2016       Jagdeep Juneja            Added Constant for Trigger, T-518090
* 22 Jul 2016       Deepti Maheshwari         Added constants for HistoryUtility (Ref : T-519565)
* 22 Jul 2016       Deepti Maheshwari         Added constants for HistoryUtility (Ref : T-519565)
* 04 Aug 2016       Deepti Maheshwari         Added constants for COMM PRice Book Name
* 08 Aug 2016       Kanika Mathur             Modified string Comm_US_accounts to contain value of ENDO_COMM_US_Customer
* 16 Aug 2016       Kanika Mathur             Modified string Comm_US_accounts to contain value of ENDO_COMM_Customer
* 26 Aug 2016       Meghna Vijay              Added Constants for COMM Site Readiness and COMM COMM Project Daily Report Record Type of Task (I-229980)
Sahil Batra     May 31,2016 Modified (I-I-220516) Added Constants.quoteStageUpdated
* 15 Sept2016       Meghna Vijay              Added Constants for COMM Provate RT, BusinessUnit Of Address (T-509044)
* 3 Oct. 2016       Meghna Vijay              Added constants for COMM_Auto_Response_To_PM_on_Status_as_Reviewed_On_Project Template(I-238143)
* 4th Oct 2016      Nitish Bansal             Update (I-238338)  //Created cosntant to populate external id on COMM PBE.
* 25th Oct 2016     Meghna Vijay              Added Constants for accounting hold, deposit hold I-240992 
* 25th Oct 2016     Deepti Maheshwari         Added Constants Role Stryker - COMM I-241302
* 2nd Nov 2016      Nitish Bansal             I-241409 Moved Task RT constant from handler to constants class
* 21st Nov 2016     Nitish Bansal             I-244792 - Empower prod sync (Record type labels have been changed so keeping new lables and not using the one's from Production)
==================================================================*/
public class Constants { 
  
  public static final String COMM_TASK = 'COMM Task'; //NB - 11/02 - I-241409
  public static final String COMM_SERVICE_OPP = 'COMM Service Opportunity';
  public static final String OPP_TYPE = 'Service Contract';
  public static final String OPP_STAGE = 'Prospecting';
  public static final String PERCENT_PROBABILITY = '10- Highly Improbable';
  public static final String TASK_STATUS = 'Completed';
  public static final String TASK_SERVICE_CONTRACT_RT = 'Service Contract End Date';
  public static final String COMM_PB_STRING = 'COMM_Price_Book_';//NB - 10/04 - I-238338
  public static final String COMM_US_TECH_SUPPORT_FIELD_SERVICES_CASE = 'COMM US Tech.Support/Field Services Case';  // T-517828
  public static final String INTEGRATION_PROFILE = 'DataMigrationIntegration'; //NB - 06/28 - I-224217
  public static final String COMM_INT_SPL_PROFILE = 'Integration Specialist - COMM'; //NB - 04/29- T-497457
  public static final String SYSTEM_LEVEL = 'System'; //NB - 06/21- I-219767
  public static final String FACILITY_LEVEL = 'Facility';
  public static boolean isTriggerExecuted = false; // NB - 04/20 - I-214012
  public static final String ROLE_PM = 'Project Manager'; // NB - 04/26 - T-490786
  public static final String ROLE_RSM = 'Regional Manager'; //RA - 05/09 - T-500952
  public static final String ROLE_SALES_REP = 'Sales Rep';//RA - 05/09 - T-500952
  public static final String ROLE_STRYKER_COMM = 'Stryker - COMM';//DM - 10/25 - I-241302
  public static final String ROLE_PM_SERVICES = 'Project Services Manager'; // NB - 04/26 - T-490786
  public static final String PROJECT_TRIGGER = 'ProjectPlanTrigger'; // NB - 04/26 - T-490786
  public static final String PHASE_TRIGGER = 'ProjectPhaseTrigger'; // RA- 06/02 - T-501829
  public static final String ACCOUNT_TYPE = 'Account';
  public static final String SVMXC_Service_Order_Line_Trigger = 'WorkDetailsTrigger'; // Varun Vasishtha T-534091
  public static final String SVMXC_Product_Stock_Trigger = 'ProductStokeTrigger'; // Varun Vasishtha T-539940
  public static final String Forecast = 'Forecast';
  public static final String CONTACT_TYPE = 'Contact';
  public static final String PRODUCT_TYPE = 'Product'; 
  public static final String USER_TYPE    = 'User';
  public static final String INTEL_TYPE    = 'Intel__c';
  public static final String Upload_Doc = 'Please upload a Document';
  public static final String CASE_OBJECT = 'Case';
  public static final String CUSTOM_ACCOUNT_TEAM = 'Custom_Account_Team__c';
  public static final String RT_CUSTOM_ACC_TEAM_PROJECT_TEAM = 'Project Team';
  public static final String CHECKLIST_TRIGGER = 'ServiceMaxCheckListTrigger';
  public static final String TYPE_POST    = 'Post';
  public static final String TYPE_COMMENT = 'Comment'; 
  public static final String REVIEWED = 'Reviewed';
  public static final String REQUESTED = 'Requested';
  public static final String ACCOUNTING_HOLD = 'Accounting Hold'; // MV I-240992 10/25
  public static final String DEPOSIT_HOLD = 'Deposit Hold'; // MV I-240992 10/25
  public static final String ACCOUNTING_DEPOSIT_HOLD = 'Accounting Hold,Deposit Hold'; // MV I-240992 10/25
  //DM 07/22 T-519565
  public static final String HISTORY_OWNER_TYPE_USER = 'User';
  public static final String HISTORY_OWNER_TYPE_QUEUE = 'Queue';
  
  public static final String ERROR        = 'Error : ';
  public static final String INVALID_DATA = 'Invalid Data';
  public static final String SAVE_SUCCESS = 'Records saved successfully';
  public static final String NO_RECORDS_FOUND = 'No records found';
  public static final String REQUEST_BODY = 'Request Body : ';
  public static final String RESPONSE     = 'Response : ';
  
  public static final String TYPE_INTEL   = 'intel';
  public static final String TYPE_REPLY   = 'reply';
  
  public static final String ACTION_LIKE  = 'like';
  public static final String ACTION_FLAG  = 'flag';
  
  public static final String INSTALLER_ROLE_STRYKER = 'Integration Specialist (Stryker)';
  public static final String INSTALLER_ROLE_3RD_PARTY = 'Integration Specialist (3rd Party)';
  
  public static boolean runUpdateTrigger = true; 
  public static boolean isOpportunityDeleted = false;
  
  //Added for BigMachinesQuoteProductTriggerHandler, MJ- 06/15/2016
  public static final String INVALID_PRODUCT_ON_QUOTE = 'You must enter a valid product for this primary Quote';
  
  public static final String BOOKED = 'Booked';
  
  public static final String ADD_FINAL_DRAWING = 'AddFinalDrawing';
  
  //Added from CM Release 14th April
  //Added for S-397689 
  public static final String DEACTIVE_ACCOUNT_ERROR = 'Notification: This account has been deactivated.  You cannot create new contacts or support cases on a deactivated account.';
  //End S-397689
  
  //Added - T-492006
  public static final String ORDER = 'Order';
  public static final String SINGLE_PO = 'Single PO';
  public static final String INITIATION = 'Initiation';
  public static final String CANCELLED = 'Cancelled';
  public static final String PROJECT_MANAGER = 'Project Manager';
  public static final String PROJECT_SERVICES_MANAGER = 'Project Services Manager';
  public static final String COMM_ORDER_RECORD_TYPE_NAME = 'COMM Order';
  //end T-492006
  
  public static final String INTEL_PREFIX   = Intel__c.SObjectType.getDescribe().getKeyPrefix();
  public static final String ACCOUNT_PREFIX = Account.SObjectType.getDescribe().getKeyPrefix();
  public static final String CONTACT_PREFIX = Contact.SObjectType.getDescribe().getKeyPrefix();
  public static final String PRODUCT_PREFIX = Product2.SObjectType.getDescribe().getKeyPrefix();
  public static final String CHATTERPOST_PREFIX = FeedItem.SObjectType.getDescribe().getKeyPrefix();
  public static final String COMMENT_PREFIX = FeedComment.SObjectType.getDescribe().getKeyPrefix();
  
  public static final String USER_PREFIX = User.SObjectType.getDescribe().getKeyPrefix();
  public static final String RESTRICTED_DOCUMENT_TYPE = 'Combined Proposal'; //NB - 02/25 - T-459652
  public static final String COMM_CONTACT_RTYPE = 'COMM Contact';
  public static final String COMM_CONTACT_RTYPE1 = 'COMM Private Contact';// MV - 09/15 - T-509044
  public static final String COMM_OPPTY_RTYPE1 = 'COMM Intl Opportunity';
  public static final String COMM_OPPTY_RTYPE2 = 'COMM Equipment Opportunity';
  public static final String COMM_US_ACCOUNTS = 'Endo_COMM_Customer';
  public static final String COMM_OPPTY_RTYPE3 = 'COMM Service Opportunity';
  public static final String COMM_OPPTY_RTYPE4 = 'COMM Key Opportunity';
  public static final String COMM_OPPTY_RTYPE5 = 'COMM Service Opportunity';
  public static final String COMM_CASE_RTYPE = 'COMM Change Request';//T-489095,//I-214491
  public static final String COMM_CASE_RTPE1 = 'COMM Commercial Operations Support';//I-215998 , //NB - 09/30 - I-237758; //NB - 11/09 - I-243191
  public static final String COMM_PROJECTTEAM_RTYPE = 'Project Team'; //T-500924
  public static final String COMM_ACCOUNTTEAM_RTYPE = 'Account Team'; //T-500924
  public static final String ROW_CAUSE_MANUAL = 'Manual';
  public static final String PERM_READ = 'Read';
  public static final String PERM_EDIT = 'Edit'; 
  public static final String ADMIN_PROFILE = 'System Administrator'; // harendra for S-415269
  public static final String Data_Migration_Profile  = 'DataMigrationIntegration';
  public static final String OPPTY_TRIGGER = 'OpportunityTrigger';
  public static final String COMM_CONTACTTRIGGER = 'Endo_ContactTrigger';
  public static final String ACCOUNT_TRIGGER = 'AccountTrigger';
  public static final String WORK_ORDER_TRIGGER = 'WorkOrderTrigger';   // T-505268 
  public static final String ACTIONNOTE_TRIGGER = 'ActionNoteTrigger';
  public static final String ATTACHMENT_TRIGGER = 'AttachmentTrigger';
  public static final String TRIAL_TRIGGER = 'TrialTrigger';
  public static final String SERVICE_CONTRACT_LINEITEM_TRIGGER  = 'ServiceContractLineItemTrigger';
  public static final String BMQUOTE_TRIGGER = 'bmQuotePricing';
  public static final String CloseDate = 'CloseDate';
  public static final String Amount = 'Amount';
  public static final String OwnerName = 'Owner.Name';
  public static final String Ownersid = 'Ownerid';
  public static final String StageName = 'StageName';
  public static final String RecordTypeDeveloperName = 'RecordType.DeveloperName';
  public static final String Forecasted = 'Forecasted';
  public static final String POsubmitted = 'PO submitted';
  public static final String ClosedWon = 'Closed Won';
  public static final String COMM_SITE_READINESS_RTYPE = 'COMM Site Readiness';//I-229980
  public static final String COMM_PROJECT_LOG_RTYPE = 'COMM Project Daily Report';//I-229980
  public static final String CASE_TRIGGER = 'CaseTrigger';
  public static final String RMA_SHIPMENT_ORDER_TRIGGER = 'RmaShipmentOrderTrigger';    //T-518090 
  public static final String OPPORTUNITY_TEAM_MEMBER_TRIGGER = 'OpportunityTeamMemberTrigger';
  public static final String ADDRESS_TRIGGER = 'AddressTrigger';
  public static final String OPPTY_DOCUMENT_TRIGGER = 'OpportunityDocumentTrigger';
  public static final String DOCUSIGN_STATUS_TRIGGER = 'DocusignStatusTrigger';
  public static final String RT_FLEX_OPPTY_CONV = 'Flex Opportunity - Conventional';
  public static final String RT_FLEX_OPPTY_RENT = 'Flex Opportunity - Rental';
  public static final String COMM_OPPTY_TASK = 'COMM Opportunity Task';
  public static final String GROUND_BREAKING = 'Ground Breaking';
  public static final String FLEX_CONTRACT = 'Flex Contract';
  public static final String PRESENTATION = 'Presentation';
  public static final String FIRST_CASES = 'First Cases';
  public static final String COMM = 'COMM';
  public static final String OPPTY_DOCUMENT_RT = 'Quote Documents';
  public static final String QUOTE_OBJECT_NAME = 'BigMachines__Quote__c';
  public static final String PHEONIX_PROPOSAL_ROLLUP = 'PHX_Proposal_Attachment_Flag__c';
  public static final String PO_DOCTYPE_ROLLUP = 'Document_Type_Comm__c';
  public static final String DOCUMENT_OBJECT_NAME = 'Document__c';
  public static final String FIELDNAME_DOCUMENT_TYPE = 'Document_type__c';
  public static final String PHOENIX_PROPOSAL_CONST = 'Phoenix Proposal';
  public static final String PO_CONST = 'Purchase Order';
  public static final String PROJECT_ENGINEER = 'Project Engineer';
  public static final String RECEPTIONIST = 'Receptionist';
  public static final String STRATEGIC_SALES_PROFILE = 'Strategic Sales Manager - COMM US';
  //Profiles added for I-214491
  public static final String PROFILE_SALES_REGION_MANAGER = 'Sales Region Manager - COMM US';
  public static final String PROFILE_SALES_REP_US = 'Sales Rep - COMM US';
  public static final String PROFILE_SALES_REP_INTL = 'Sales - COMM Intl';
  //Picklist values for Type field on Address
  public static final String BILLING = 'Billing';
  public static final String SHIPPING = 'Shipping';
  public static final String BUSINESSUNIT = '88';// MV- 09/15 - T-509044
  public static final String INSTALLER_RECORDTYPE = 'Installer Assignment';
  public static final String profile1 = 'Strategic Sales Manager - COMM US';
  public static final  String profile2 = 'Sales Rep - COMM US';
  public static final  String profile3 = 'Sales - COMM Intl';
  //picklist values for Address status field
  public static final String ACTIVE = 'Active';
  
  //Contact_Opportunity_Association__c Record Type
  public static final String ContactAssocRT = 'Contact Project Association';
  //picklist value stage name to validate if opportunity is closed
  public static final String CLOSED_WON =  'Closed Won';
  public static final String CLOSED_LOST = 'Closed Lost';
  
  public static final String ENTERED = 'Entered';
  public static final String COMM_Marketing = 'COM Marketing';
  public static final String CONST_APPROVED = 'APPROVED';
  public static final String CONST_NOTAPPROVED = 'NOT-APPROVED';
  
  //SVMXC__Service_Order__c (Work Order object) record type. T-505268
  public static final String COMM_US_STRYKER_FIELD_SERVICE_CASE_RECORD_TYPE  = 'COMM US Stryker Field Service Case';
  
  //picklist value for Installed Base product install status
  public static final String READY_TO_INSTALL = 'Ready to Install';
  
  //Delimters for separating two values
  public static final String TILDE_DELIMITER = '~';
  public static final String HYPHEN_DELIMITER = '-';
  public static final String COMMA_DELIMITER = ',';
  //Report name for accounts having invalid primary address
  public static final String REPORT_ACCOUNTS_WITH_INVALID_ADDRESS = 'Accounts_With_Invalid_Primary_Address';
  public static final String SENDER_ID = 'SFDC';
  public static final String RECIEVER_ID = 'ENDERP';
  public static final String BUSINESS_UNIT = 'COM';
  public static final String COMPLETED = 'Completed';
  public static final String STATUS_INPROGRESS = 'In Progress';
  public static final String INT_OP_PRJCT_CREATE = 'Create';
  public static final String INT_OP_PRJCT_ASSIGN = 'AssignmentToSO';
  public static final String INT_OP_ORDLINE_UPDATE = 'ShippingInfoUpdate';
  
  //Service contract object and record type
  public static final String RT_COMM_SERVICE_CONTRACT = 'COMM SVMXC Service Contract';
  public static final String SERVICE_CONTRACT_OBJECT = 'SVMXC__Service_Contract__c';
  
  //Service max install product and record type
  public static final String INSTALL_PRODUCT_SOBJECT_ASSET = 'SVMXC__Installed_Product__c';
  public static final String RT_COMM_INSTALL_PRODUCT = 'COMM Installed Product';
  //Trial Object and its record types
  public static final String SOBJECT_TRIAL = 'Trial__c';
  public static final String SOBJECT_PHASE = 'Project_Phase__c';
  public static final String SOBJECT_PROJECT_PLAN = 'Project_Plan__c';
  public static final String TRIAL_RT_REQUEST = 'Request';
  public static final String TRIAL_RT_TRIAL = 'Trial';
  public static final String MONTHLY_FORECAST = 'Monthly_Forecast__c';
  public static final String MF_MONTH_FIELD = 'Month__c';
  public static final String SOBJECT_OPPORTUNITY = 'Opportunity';
  // MJ -Empower main release

  public static final String ORDER_NV_CREDIT  = 'NV Credit';  //  Added By jyotirmaya on 4/19/2016 , for 00166082
  
  public static final String REBOOKED = 'Rebooked'; //added for the story #S-404577
  public static final String New_Con = 'New'; //added for the story #S-404577
  
  //added for the story #S-404578 #START
  
  public static final String ENDO = 'Endo';
  public static final String ENDO_FullForm = 'Endoscopy';
  
  public static final String IVS = 'IVS';
  public static final String IVS_FullForm = 'Interventional Spine';
  
  public static final String NSE = 'NSE';
  public static final String NSE_FullForm = 'Neuro Spine ENT';
  
  public static final String PC = 'PC';
  public static final String PC_FullForm = 'Patient care';
  
  public static final String PH = 'PH';
  public static final String PH_FullForm = 'Patient Handling';
  
  public static final String Surg = 'Surg';
  public static final String Surg_FullForm = 'Surgical';
  
  public static final String NAV = 'Navigation';
  
  //added for the story #S-404578 #END

  // Prod Sync - Mehul Jain 07/12/16
  public static boolean quoteStageUpdated = false;
  public static boolean oracleQuoteUpdated = false; //NB - 11/21 - I-244792
   //Added By Harendra for Story# S-415285

    public static final String TRIGGER_BASEPRODUCTTRIGGER = 'BaseProductTrigger';
    public static final String TRIGGER_BASETRAILSURVEYTRIGGER = 'BaseTrailSurveyTrigger';
    public static final String TRIGGER_CONTACTACCTASSOCIATIONTRIGGER = 'ContactAcctAssociationTrigger';
    public static final String TRIGGER_CONTACTTRIGGER = 'ContactTrigger';
    public static final String TRIGGER_CONTRACTTRIGGER = 'ContractTrigger';
    public static final String TRIGGER_CUSTOMACCOUNTTEAMTRIGGER = 'CustomAccountTeamTrigger';
    public static final String TRIGGER_ENDO_CONTACTTRIGGER = 'Endo_ContactTrigger';
    public static final String TRIGGER_ENDO_ORDERTRIGGER = 'Endo_OrderTrigger';
    public static final String TRIGGER_ENDO_PRICINGLINEITEMTRIGGER = 'Endo_PricingLineItemTrigger';
    public static final String TRIGGER_ENDO_USERTRIGGER = 'Endo_UserTrigger';
    public static final String TRIGGER_EVENTTRIGGER = 'EventTrigger';
    public static final String TRIGGER_FEEDCOMMENTTRIGGER = 'FeedCommentTrigger';
    public static final String TRIGGER_FEEDITEMTRIGGER = 'FeedItemTrigger';
    public static final String TRIGGER_FLEXCREDITTRIGGER = 'FlexCreditTrigger';
    public static final String TRIGGER_FLEX_CONTRACTTRIGGER = 'Flex_ContractTrigger';
    public static final String TRIGGER_FLEX_LEADTRIGGER = 'Flex_LeadTrigger';
    public static final String TRIGGER_FORECASTYEARTRIGGER = 'ForecastYearTrigger';
    public static final String TRIGGER_INSTALLBASETRIGGER = 'InstallBaseTrigger';
    public static final String TRIGGER_INSTALLEDPRODUCTTRIGGER = 'InstalledProductTrigger';
    public static final String TRIGGER_INTELTRIGGER = 'IntelTrigger';
    public static final String TRIGGER_LEADTRIGGER = 'LeadTrigger';
    public static final String TRIGGER_MEDICAL_CASETRIGGER = 'Medical_CaseTrigger';
    public static final String TRIGGER_MONTHLYFORECASTTRIGGER = 'MonthlyForecastTrigger';
    public static final String TRIGGER_NV_CONTACTTRIGGER = 'NV_ContactTrigger';
    public static final String TRIGGER_NV_FOLLOWTRIGGER = 'NV_FollowTrigger';
    public static final String TRIGGER_NV_NOTIFICATIONSETTINGSTRIGGER = 'NV_NotificationSettingsTrigger';
    public static final String TRIGGER_NV_ORDERITEMTRIGGER = 'NV_OrderItemTrigger';
    public static final String TRIGGER_NV_ORDERTRIGGER = 'NV_OrderTrigger';
    public static final String TRIGGER_OPPORTUNITYLINEITEMTRIGGER = 'OpportunityLineItemTrigger';
    public static final String TRIGGER_OPPORTUNITYTRIGGER = 'OpportunityTrigger';
    public static final String TRIGGER_ORACLEQUOTETRIGGER = 'OracleQuoteTrigger';
    public static final String TRIGGER_ORDERPRODUCTSTRIGGER = 'OrderProductsTrigger';
    public static final String TRIGGER_REGULATORYQUESTIONANSWERTRIGGER = 'RegulatoryQuestionAnswerTrigger';
    public static final String TRIGGER_SPS_OPPORTUNITYTRIGGER = 'SPS_OpportunityTrigger';
    public static final String TRIGGER_STRYKER_EMAILTOCASETRIGGER = 'Stryker_EmailtoCaseTrigger';
    public static final String TRIGGER_TASKTRIGGER = 'TaskTrigger';
    public static final String TRIGGER_USERTRIGGER = 'UserTrigger';
    public static final String TRIGGER_SPS_AccountTrigger = 'SPS_AccountTrigger';
    //end Story# S-415285  
    public static final String System_Admin_Profile = 'System Administrator'; //start by harendra for S-415269

  //Roll up utility constants
  public static final String ROLL_UP_COUNT = 'COUNT';
  public static final String ROLL_UP_ID = 'ID';
  //Roll up fields on project
  public static final String COMPLETED_PHASES = 'Number_of_Completed_Phases__c';
  public static final String TOTAL_PHASES = 'Number_of_Phases__c';
  public static final String SIGNED_PHASES = 'Number_of_Signed_Off_Phases__c'; 
  
  public static final String PHASE_ROLL_UP_SIGN_OFF_FILTER = 'Customer Sign Off Complete';
  public static final String PHASE_ROLL_UP_COMPLETE_FILTER = 'Complete';
  public static final String PHASE_MILESTONE_FIELD = 'Milestone__c';
  public static final String CSR_AUTOREP_TEMPLATE = 'COMM_Auto_Response_To_CSR_on_Status_as_Requested';
  public static final String PROJECT_PLAN_COMPLETED_TEMPLATE = 'COMM_Project_Plan_Stage_Is_Completed';
  public static final String COMM_CUST_SERV = 'COMM Customer Service';
  public static final String COMM_PM_REVIEWED_PROJECT_TEMPLATE ='COMM_Auto_Response_To_PM_on_Status_as_Reviewed_On_Project';//MV - 10/03
  //Booking email template
  public static final String BOOKING_EMAIL_TEMPLATE_NAME = 'COMM_Order_Booking_Notification_and_store';

  public static final String EMAIL_SUBJECT_ENDO_CASE_OPENED = 'Stryker Endoscopy CustomerCare Support Case';
  
  public static final String COMM_PRICEBOOK = 'COMM Price Book';
   //Themes for detecting salesforce1
  public static final String THEME_SF1 = 'theme4t';  
  /**
  * This method is used to create the recordtype id
  */
  public static Id getRecordTypeId(String recordType, String objectType) {
    Map < String, Schema.RecordTypeInfo > rtMapByName = null;
    if (rtMapByName == null) {
      Map < String, Schema.SObjectType > gd = Schema.getGlobalDescribe();
      Schema.SObjectType ctype = gd.get(objectType);
      Schema.DescribeSObjectResult d1 = ctype.getDescribe();
      rtMapByName = d1.getRecordTypeInfosByName();
    }
    Schema.RecordTypeInfo recordTypeDetail = rtMapByName.get(recordType);
    if (recordTypeDetail != null) {
      return recordTypeDetail.getRecordTypeId();
    } else {
      return null;
    }
  }
  
  // @pupose : Used for COMM purpose
  // @description : Used to get the record type name for Any object
  // @param oldMap - map to hold Trigger.oldMap 
  // @param newRecords - List to hold Trigger.new 
  // @return Nothing 
  // @task : T-489842
  public static String getRecordTypeName(Id recordType, String objectType) {
    Map < Id, Schema.RecordTypeInfo > rtMapById = null;
    
    if (rtMapById == null) {
      Map < String, Schema.SObjectType > gd = Schema.getGlobalDescribe();
      Schema.SObjectType ctype = gd.get(objectType);
      Schema.DescribeSObjectResult d1 = ctype.getDescribe();
      rtMapById = d1.getRecordTypeInfosById();

    }
    
    Schema.RecordTypeInfo recordTypeDetail = rtMapById.get(recordType);
    if (recordTypeDetail != null) {
      return recordTypeDetail.getName();
    } else {
      return null;
    }
  }
  
  public static Map<String, Schema.SObjectType> GLOBAL_MAP = Schema.getGlobalDescribe();
  private static Map<String , Schema.DescribeSObjectResult> mapSobjectDescribe = new Map<String , Schema.DescribeSObjectResult>();
  //----------------------------------------------------------------------------------------------------------------
  // Get picklist values
  //----------------------------------------------------------------------------------------------------------------
    public static List<SelectOption> getSelectOptionFromPicklist(String objectName, String fieldName){
    List<SelectOption> options = new List<SelectOption>();
    Schema.SObjectType s = GLOBAL_MAP.get(objectName);
    if(!mapSobjectDescribe.containsKey(objectName)){
        Schema.DescribeSObjectResult sobjectDescribe = GLOBAL_MAP.get(objectName).getDescribe();
        mapSobjectDescribe.put(objectName ,sobjectDescribe );
    }
    if (s != null) {
      Map<String, Schema.SObjectField> fMap = mapSobjectDescribe.get(objectName).fields.getMap();
      Schema.DescribeFieldResult fieldResult = fMap.get(fieldName).getDescribe();
      if(fieldResult != null && fieldResult.isAccessible()){
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple){
          options.add(new SelectOption(f.getValue(),f.getLabel())); 
        }   
      }
    }    
    return options;    
  }
    
    /*** Code modified - Padmesh Soni (10/14/2016 Appirio Offshore) - Case #: 00173911 - Start ***/
    public Static final String ORDER_STATUS_ON_HOLD = 'On Hold';
    /*** Code modified - Padmesh Soni (10/14/2016 Appirio Offshore) - Case #: 00173911 - End ***/
 //S-445932-----Start-----Hitesh[Nov 07, 2016]
    public static final string CASE_REQUIRED_DATA_TO_CLOSE = 'Please edit the case and complete all required fields before attempting to close.';
    public static final string EORROR_TO_BE_SHOWN_CLOSE_CASE = 'Please complete all required fields before attempting to close the case.';
    //S-445932-----End-----Hitesh[Nov 07, 2016]
}