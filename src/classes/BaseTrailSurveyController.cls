// 
// 
//
// Base Trail Survey - Main page's handler
//
// 13 Jan 2016     Tom Muse      Added
// 03-Feb 2016	   Sahil Batra   Modifited 	I-201160
// 09 March 2016   Prakarsh Jain Modified   I-204513
//
public with sharing class BaseTrailSurveyController {
	public Map<Id,String> prodIdtoDescriptionMap{get;set;}
	public Map<Id,String> prodIdtoTrailingImpactMap{get;set;}
	public List<Base_Product__c> bps {get;set;}
	public Base_Trail_Survey__c bts {get;set;}
	public String oppId;
	public Boolean showDelete {get;set;}
	public Boolean isEditable {get;set;}
	public Id selectedProduct{get;set;}
	public boolean showProduct{get;set;}
	public Opportunity opp{get;set;}
	public String priceBook{get;set;}
	public String val{get;set;}
	public List<String> args = new String[]{'0','number','###,###,##0'};
  public BaseTrailSurveyController() {
  	showProduct = false;
  	val='';
  	prodIdtoDescriptionMap = new Map<Id,String>();
  	prodIdtoTrailingImpactMap = new Map<Id,String>();
  	showDelete = false;
	  opp = new Opportunity();
	  oppId = ApexPages.currentPage().getParameters().get('id');
	  if(oppId!=null && oppId!=''){
	  	opp = [SELECT Name, Id,OwnerId,Business_Unit__c,Pricebook2Id,CloseDate FROM Opportunity WHERE Id =:oppId];
	  	priceBook='baseTrail'+opp.Pricebook2Id;
	  	if([SELECT count() FROM Base_Trail_Survey__c WHERE Opportunity__c =: opp.id] > 0){
	  		bts = [Select id, Name, Opportunity__c, Opportunity__r.OwnerId,Description__c FROM Base_Trail_Survey__c WHERE Opportunity__c =: opp.id Limit 1];
	  		bps = [Select id, Name, Delete_Record__c, Price__c, Product__c, Quantity__c, Base_Trail_Survey__c, Trail__c,Total__c  FROM Base_Product__c where Base_Trail_Survey__c =: bts.id];
	  		Set<Id> prodIds = new Set<Id>();
	  		for(Base_Product__c baseProduct : bps){
				prodIds.add(baseProduct.Product__c);
				Decimal tempVal = 0.00;
				if(baseProduct.Price__c!=null &&  baseProduct.Quantity__c!=null){
					tempVal = baseProduct.Price__c * baseProduct.Quantity__c;
				}
				prodIdtoTrailingImpactMap.put(baseProduct.Product__c,tempVal <= 0.00 ? '' : '$' + String.format(tempVal.setScale(2).format(), args));
			}
			if(prodIds.size() > 0){
				prodIdtoDescriptionMap.putAll(ProductComponentController.returnDescription(prodIds));
			}
	  	}else{
	  	  bts = new Base_Trail_Survey__c(Description__c = '' );
  	  	//bts.Name = opp.Name+' - Base Trail';
  	  	bts.Opportunity__c = opp.Id;
  	  	bps = new List<Base_Product__c>();
	  	}

    }
	  Id profID = UserInfo.getProfileId();
		Profile queryProfile = [SELECT Id, name FROM Profile WHERE Id =: profID Limit 1];
		isEditable = (queryProfile.Name == 'System Administrator' || opp.OwnerId == UserInfo.getUserId()) ? true : false;
      
  }

	public void addBaseProduct(){
		val='';
		showProduct = true;
	/*	Set<Id> prodIds = new Set<Id>();
		for(Base_Product__c baseProduct : bps){
			prodIds.add(baseProduct.Product__c);
		}
		if(prodIds.size() > 0){
			prodIdtoDescriptionMap.putAll(ProductComponentController.returnDescription(prodIds));
		}
		bps.add(new Base_Product__c(Price__c=0.00, Quantity__c = 0)); */
		
	}
	public PageReference save(){
		List<Base_Product__c> finalListtoInsert = new List<Base_Product__c>();
		Set<Id> productIds = new Set<Id>();
		boolean hasError = false;
		for(Base_Product__c bp : bps){
			if(validateProduct(bp)){
				hasError = true;
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter product value for all added base product'));
				break;
			}
			productIds.add(bp.Product__c);
		}
		if(!hasError){
			upsert bts;
		}
		if(bts.Id !=null){
			Map<Id,Product2> proMap = new Map<ID, Product2>([SELECT Id, Name FROM Product2 WHERE Id in: productIds]);
			for(Base_Product__c bp : bps){
				if(bp.Base_Trail_Survey__c == null){
					bp.Base_Trail_Survey__c = bts.Id;
					bp.Name = proMap.get(bp.Product__c).Name;
				}
				finalListtoInsert.add(bp);
			}
		}
			if(finalListtoInsert.size() > 0 && !hasError){
				upsert finalListtoInsert;
			}
		if(!hasError){
		PageReference pg = new PageReference('/'+bts.Id);
		return pg;
		}
		else{
			return null;
		}	
		} 
	public PageReference cancel(){
		PageReference pg;
		if(bts!=null && bts.Id!=null){
			pg = new PageReference('/'+bts.Id);
		}
		else{
			pg = new PageReference('/'+oppId);
		}
		return pg;
	}
	public void deleteBaseProduct(){
		List<Base_Product__c> deleteList = new List<Base_Product__c>();
		List<Base_Product__c> temp = new List<Base_Product__c>();
		for(Base_Product__c bp : bps){
			temp.add(bp);
		}
		System.debug('>>>>Temp'+temp);
		bps.clear();
		for(Base_Product__c bp : temp){
			if(!bp.Delete_Record__c){
				bps.add(bp);	
			}
			else{
				if(bp.Delete_Record__c && bp.Id !=null){
					deleteList.add(bp);
				}
			}
		}
		System.debug('>>>>new'+bps);
		if(deleteList.size() > 0){
			delete deleteList;
		}
	}
	public boolean validateProduct(Base_Product__c bp){
		boolean hasError = false;
		String prodId = bp.Product__c;
		if(String.isBlank(prodId)){
				hasError = true;
		}
		return hasError;
	}
	public void populateDescription(){
		Set<Id> prodIds = new Set<Id>();
		bps.add(new Base_Product__c(Product__c=selectedProduct));
		for(Base_Product__c baseProduct : bps){
			prodIds.add(baseProduct.Product__c);
			Decimal tempVal = 0.00;
			  if(baseProduct.Price__c!=null &&  baseProduct.Quantity__c!=null){
				  tempVal = baseProduct.Price__c * baseProduct.Quantity__c;
			  }
		    prodIdtoTrailingImpactMap.put(baseProduct.Product__c,tempVal <= 0.00 ? '' : '$' + String.format(tempVal.setScale(2).format(), args));
		}
		if(prodIds.size() > 0){
			prodIdtoDescriptionMap.putAll(ProductComponentController.returnDescription(prodIds));
		}
		val='';
	}
	public void updateTrailingImpact(){
		for(Base_Product__c baseProduct : bps){
			Decimal tempVal = 0.00;
				if(baseProduct.Price__c!=null &&  baseProduct.Quantity__c!=null){
					tempVal = baseProduct.Price__c * baseProduct.Quantity__c;
				}
				else{
					if(baseProduct.Price__c == null || baseProduct.Price__c == 0 ){
						baseProduct.Price__c = null;
					}
					if(baseProduct.Quantity__c == null || baseProduct.Quantity__c == 0){
						baseProduct.Quantity__c = null;
					}
				}
				prodIdtoTrailingImpactMap.put(baseProduct.Product__c,tempVal <= 0.00 ? '' : '$' + String.format(tempVal.setScale(2).format(), args));
			}
	}
}