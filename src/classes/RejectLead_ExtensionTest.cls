/******************************************************************************************
 *  Purpose : Test Class for RejectLead_Extension
 *  Author  : Shubham Paboowal
 *  Date    : September 13, 2016
 *  Story	: S-437706
*******************************************************************************************/  
@isTest
private class RejectLead_ExtensionTest {

    static testMethod void myUnitTest() {
    	Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Instruments Lead Record Type');
        Id recordTypelead = rtByNameRep.getRecordTypeId();
        List<Lead> leadList = TestUtils.createLead(1, recordTypelead, true);
        ApexPages.currentPage().getParameters().put('leadId', leadList[0].Id);
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(leadList[0]);
        RejectLead_Extension ext = new RejectLead_Extension(controller);
        ext.leadRecord.Rejection_Reason__c = 'No Reason';
        ext.checkOfCustomReasonisRequired();
        ext.save();
        ext.cancel();
    }
}