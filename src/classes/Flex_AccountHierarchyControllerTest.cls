/*************************************************************************************************
Created By:    Sunil Gupta
Date:          Sep 16, 2014
Description  : Test class for Flex_AccountHierarchyControllerTest
**************************************************************************************************/
@isTest
private class Flex_AccountHierarchyControllerTest {
	
	//----------------------------------------------------------------------------
  // testMethod1 Check the Account Hiearchy functionality
  //----------------------------------------------------------------------------
  static testMethod void testMethod1() {
  	AccountHierarchyTestData.createTestHierarchy();
  	Account topAccount = Endo_TestUtils.createAccount(1, false).get(0);
    topAccount.Type = 'Hospital';
    topAccount.Legal_Name__c = 'Test';
    insert topAccount;

    Account middleAccount = Endo_TestUtils.createAccount(1, false).get(0);
    middleAccount.Type = 'Hospital';
    middleAccount.Legal_Name__c = 'Test';
    middleAccount.ParentId = topAccount.Id;
    insert middleAccount;

    Account bottomAccount = Endo_TestUtils.createAccount(1, false).get(0);
    bottomAccount.Type = 'Hospital';
    bottomAccount.Legal_Name__c = 'Test';
    bottomAccount.ParentId = middleAccount.Id;
    insert bottomAccount;
    
    test.startTest();

    PageReference p = System.Page.Flex_AccountHierarchy;
    Test.setCurrentPage(p);
    
    ApexPages.StandardController sc = new ApexPages.StandardController(topAccount);

    // Instanciate Controller
    Flex_AccountHierarchyController controller = new Flex_AccountHierarchyController(sc);
    
    List<Flex_AccountHierarchyController.AccountWrapper> lstWrapper = controller.getObjectStructure();
    System.Assert( lstWrapper.size() > 0);
    
    
    test.stopTest();
  }
}