// 
// (c) 2016 Appirio, Inc.
//
// Test class of "CustomAccountTeamTriggerHandler" 
//
// 7th March 2016     Kirti Agarwal      Original(T-477520)
//
//Modified Date       Modified By        Purpose
//10th May 2016       Meghna Vijay       To test populate of project user lookup with project team member(T-500924)
//
@isTest
private class CustomAccountTeamTriggerHandler_Test {
  public static final String STRATEGIC_SALES_MANAGER_COMMUS = 'Strategic Sales Manager - COMM US';
  private static final String PM_PROFILE = 'Service Project Manager - COMM US';
  private static final String INT_SPL_PROFILE = 'Integration Specialist - COMM';
  private static List<Custom_Account_Team__c> customAccTeamList;
  private static List<Project_Plan__c> projects;
  private static List<User> userList;
  //================================================================      
  // Name: testAccountAddress
  // Description: Used to test address updation functionality
  // Created Date: [2/29/2016]
  // Created By: Kirti Agarwal (Appirio)
  //==================================================================
 /* @isTest
  static void CustomAccountTeamTriggerHandler_Test () {
    
    
    List < Account > accountList = TestUtils.createAccount(2, true);
    accountList[0].Intl_Entity_Account__c = accountList[1].id;
    update accountList[0];
    
    List<User> userRec = TestUtils.createUser(1, STRATEGIC_SALES_MANAGER_COMMUS , true);
    
    Custom_Account_Team__c  teamRecord = new Custom_Account_Team__c  ();
    teamRecord.Account__c = accountList[0].id;
    teamRecord.Account_Access_Level__c = 'Read Only';
    teamRecord.User__c = userRec[0].id;
    insert teamRecord;
    
    System.assert([SELECT id FROM Custom_Account_Team__c  WHERE Account__c =:  accountList[1].id].size() > 0);
    
    System.assert([SELECT id FROM AccountTeamMember WHERE AccountId =:  accountList[1].id].size() > 0);
    
  }
*/

  //========================NB - 04/26 - T-490786 Start========================================      
  // Description: Below methods are used to test project share record creation functionality
  // Created Date: [4/26/2016]
  // Created By: Nitish Bansal (Appirio)
 
  @isTest static void testNewRecord() {
    createData();
    Test.startTest();
    insert customAccTeamList;
    Test.stopTest();
    System.assertEquals(3,[SELECT Name From Custom_Account_Team__c].size());
    System.assertEquals(2,[SELECT Id From Project_Plan__Share WHERE AccessLevel = 'Edit'].size());
  }
  @isTest static void testUpdateIsDeletedTrue() {
    createData();
    insert customAccTeamList;
    customAccTeamList[0].IsDeleted__c = True;
    Test.startTest();
    update customAccTeamList;
    Test.stopTest();
    System.assertEquals(2,[SELECT Name From Custom_Account_Team__c WHERE IsDeleted__c = False].size());
  }
  @isTest static void testUpdateIsDeletedFalse() {
    createData();
    customAccTeamList[0].IsDeleted__c = True;
    insert customAccTeamList;
    customAccTeamList[0].IsDeleted__c = False;
    Test.startTest();
    update customAccTeamList;
    Test.stopTest();
    System.assertEquals(3,[SELECT Name From Custom_Account_Team__c WHERE IsDeleted__c = False].size());
  }
  @isTest static void testUpdateTeamMemberRole() {
    createData();
    insert customAccTeamList;
    customAccTeamList[0].Team_Member_Role__c = Constants.PROJECT_SERVICES_MANAGER;
    Test.startTest();
    update customAccTeamList;
    Test.stopTest();
    System.assertEquals(1,[SELECT Name From Custom_Account_Team__c WHERE Team_Member_Role__c = :Constants.PROJECT_SERVICES_MANAGER].size());
  }
  @isTest static void testAccountAccessLevel() {
    createData();
    insert customAccTeamList;
    customAccTeamList[0].Account_Access_Level__c = 'Private';
    Test.startTest();
    update customAccTeamList;
    Test.stopTest();
    System.assertEquals(1,[SELECT Name From Custom_Account_Team__c WHERE Account_Access_Level__c = 'Private'].size());
  }
  public static void createData() {
    userList = TestUtils.createUser(2, PM_PROFILE , true);
    List<Account> accountList = TestUtils.createAccount(1,True);
    projects =  TestUtils.createProjectPlan(2, accountList[0].Id, true);
    customAccTeamList = createCustomAccountTeam(3, accountList[0].Id, userList[0].Id, Constants.PROJECT_MANAGER, false, false); 
  }
  public static List<Custom_Account_Team__c> createCustomAccountTeam(Integer customAccTeamCount,Id accId, Id userId,String role,Boolean isDel ,Boolean isInsert) {
    customAccTeamList = new List<Custom_Account_Team__c>();
    for(Integer i = 0; i < customAccTeamCount; i++) {
      Custom_Account_Team__c customAccTeamRecord = new Custom_Account_Team__c(Account__c = accId,User__c = userId,Team_Member_Role__c = role,IsDeleted__c = isDel);
      customAccTeamList.add(customAccTeamRecord);
    }
    if(isInsert){
      insert customAccTeamList;
    }
    return customAccTeamList;
  }
  //========================NB - 04/26 - T-490786 End ==========================================
  //***************************************************COMM T-500924 Start*******************************//
  //Name             :     testProjectTeam Method
  //Description      :     Test Method created to test populate of project user lookups with project team member
  //Created by       :     Meghna Vijay T-500924 
  //Created Date     :     May 10, 2016
  @isTest static void testProjectTeam() {
    createData();
    Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Custom_Account_Team__c; 
    Map<String,Schema.RecordTypeInfo> projectTeamRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
    Id rtId = projectTeamRecordTypeInfo.get(Constants.COMM_PROJECTTEAM_RTYPE).getRecordTypeId();
    customAccTeamList.get(0).Project__c = projects.get(0).Id;
    customAccTeamList.get(0).IsPrimary__c = true;
    customAccTeamList.get(0).RecordTypeId = rtId;
    Test.startTest();
    insert customAccTeamList;
    Test.stopTest();
  }
  //***********************************************COMM T-500924 End***********************************//


  //Name             :     testPhaseSharingInsertion Method
  //Description      :     Test Method created to test sharing records creation
  //Created by       :     Rahul Aeran T-501829
  //Created Date     :     June 02, 2016
  /*@isTest static void testPhaseSharing() {
    User sysAdmin = TestUtils.createUser(1, Constants.ADMIN_PROFILE , true).get(0);
    List<Account> accountList = TestUtils.createAccount(1,True);
    projects =  TestUtils.createProjectPlan(2, accountList[0].Id, true);
    userList = TestUtils.createUser(2, INT_SPL_PROFILE , true);
    customAccTeamList = createCustomAccountTeam(3, accountList[0].Id, userList[0].Id, Constants.PROJECT_MANAGER, false, false);
    System.runAs(sysAdmin){
      List<Project_Phase__c> lstPhases1 = TestUtils.createProjectPhase(5,projects.get(0).Id,false);
      List<Project_Phase__c> lstPhases2 = TestUtils.createProjectPhase(4,projects.get(1).Id,false);
      List<Project_Phase__c> lstAll = new List<Project_Phase__c>();
      lstAll.addAll(lstPhases1);
      lstAll.addAll(lstPhases2);
      insert lstAll;
      String installerRTID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get(Constants.INSTALLER_RECORDTYPE).getRecordTypeId();
      customAccTeamList.get(0).Project_Phase__c = lstPhases1.get(0).Id;
      customAccTeamList.get(0).RecordTypeId = installerRTID;
      customAccTeamList.get(0).User__c = userList.get(0).Id;
      customAccTeamList.get(1).Project_Phase__c = lstPhases2.get(0).Id;
      customAccTeamList.get(1).RecordTypeId = installerRTID;
      customAccTeamList.get(1).User__c = userList.get(1).Id;
      Test.startTest();
      insert customAccTeamList;
      List<Project_Phase__Share> lstShareRecords = [SELECT Id, ParentId, UserOrGroupId, RowCause 
                                          FROM Project_Phase__Share 
                                          WHERE RowCause = :Constants.ROW_CAUSE_MANUAL 
                                          AND UserOrGroupId = :userList.get(0).Id];
      System.assert(lstShareRecords.size() == 1 , 'Phase Share record has been created');                                          
      lstShareRecords = [SELECT Id, ParentId, UserOrGroupId, RowCause 
                                          FROM Project_Phase__Share 
                                          WHERE ParentId  = :lstPhases2.get(0).Id 
                                          AND UserOrGroupId = :userList.get(1).Id
                                          AND RowCause = :Constants.ROW_CAUSE_MANUAL];
      System.assert(lstShareRecords.size() == 1 , 'Phase Share record has been created');
      customAccTeamList.get(1).RecordTypeId = installerRTID;
      customAccTeamList.get(1).User__c = userList.get(0).Id;
      update customAccTeamList;
      lstShareRecords = [SELECT Id, ParentId, UserOrGroupId, RowCause 
                                          FROM Project_Phase__Share 
                                          WHERE UserOrGroupId = :userList.get(0).Id
                                          AND RowCause = :Constants.ROW_CAUSE_MANUAL];
      System.assert(lstShareRecords.size() == 2 , 'The user should have two manual sharing records');
      delete(customAccTeamList);
      lstShareRecords = [SELECT Id, ParentId, UserOrGroupId, RowCause 
                                          FROM Project_Phase__Share 
                                          WHERE RowCause = :Constants.ROW_CAUSE_MANUAL];
      System.assert(lstShareRecords.isEmpty() , 'The sharing records should be deleted');
      Test.stopTest();
      }
    }*/
  }