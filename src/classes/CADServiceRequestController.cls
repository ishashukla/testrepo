public class CADServiceRequestController {
    
    public Id IdParam {get;set;}
    public List<wrapperClass> wrapperClassList {get{
        return opportunityDocumentForEmail();
    }
    set;}
    public String opportunityDocumentId {get;set;}
    public String opptyId;
   
    public CADServiceRequestController(ApexPages.StandardController controller) {
        opportunityDocumentId = ApexPages.currentPage().getParameters().get('id');
    }
    public CADServiceRequestController() {}
    
    public List<wrapperClass> opportunityDocumentForEmail() {
        List<wrapperClass> wrapperClassList = new List<wrapperClass>();
        wrapperClass wrapperObj = new wrapperClass();
        for(Document__c doc : [SELECT Id, Name, Submitted_date__c, Desired_Completion_Date__c, Request_Type__c, Drawing_Type__c, Instructions__c,
                                   Existing_Drawing_Package__c, Request_Requirements__c, Details_and_Special_Instructions__c, Opportunity__c
                               FROM Document__c
                               WHERE Id =: IdParam]) {
            
            wrapperObj.submittalDate = doc.Submitted_date__c;
            wrapperObj.desiredCompletionDate = doc.Desired_Completion_Date__c;
            wrapperObj.requestType = doc.Request_Type__c;
            wrapperObj.existingDrawingPackage = doc.Existing_Drawing_Package__c;
            wrapperObj.drawingType = doc.Drawing_Type__c;
            wrapperObj.requirements = doc.Request_Requirements__c;
            wrapperObj.details = doc.Details_and_Special_Instructions__c;
            wrapperObj.instructions = doc.Instructions__c;
            opptyId = doc.Opportunity__c;      
        }
        
        for(Opportunity opp : [SELECT Id, Name, Account.Name, Number_of_Rooms__c, Account.ShippingCity, Account.ShippingState
                               FROM Opportunity
                               WHERE Id =: opptyId]) {
            
            wrapperObj.totalRooms = opp.Number_of_Rooms__c;
            wrapperObj.accountName = opp.Account.Name;
            wrapperObj.city = opp.Account.ShippingCity;
            wrapperObj.state = opp.Account.ShippingState;
        }
        wrapperClassList.add(wrapperObj);
        
        return wrapperClassList;
    }
    
    public class wrapperClass {
        public Date submittalDate{get;set;}
        public Date desiredCompletionDate{get;set;}
        public Decimal totalRooms{get;set;}
        public String accountName{get;set;}
        public String projectName{get;set;}
        public String city{get;set;}
        public String state{get;set;}
        public String requestType{get;set;}
        public Decimal existingDrawingPackage{get;set;}
        public String drawingType{get;set;}
        public String requirements{get;set;}
        public String details{get;set;}
        public String instructions{get;set;}
    }
}