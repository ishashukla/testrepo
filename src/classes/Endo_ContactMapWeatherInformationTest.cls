/*******************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 10, 2014 
 Purpose       :  Test Class for Endo_ContactMapWeatherInformation
*******************************************************************************/
@isTest
private class Endo_ContactMapWeatherInformationTest {
	static testmethod void testEndo_AccountMapWeatherInformation(){
	    Account acc = Endo_TestUtils.createAccount(1, true).get(0);
	    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
	    
	    con.MailingCountryCode = 'US';
	    con.MailingState = 'California';
	    insert con;
        
	    Test.startTest();
	    
	    ApexPages.StandardController stdController = new ApexPages.StandardController(con);
	    Endo_ContactMapWeatherInformation comp = new Endo_ContactMapWeatherInformation(stdController);
	    
	    System.assertEquals(con.MailingState, comp.myaddress.state);
	    System.assertEquals(con.MailingCountryCode, comp.myaddress.countryCode);
	    System.assertEquals(con.MailingCountry, comp.myaddress.country);
	    System.assertEquals(con.MailingCity, comp.myaddress.city);
	    
	    comp.contactFields();
	    
	    Test.stopTest();
	}
}