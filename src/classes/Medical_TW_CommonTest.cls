/*************************************************************************************************
Created By:    Rahul Aeran
Date:          April 23, 2016
Description:   Test class for Medical_TW_Common for Medical Division
**************************************************************************************************/
@isTest
private class Medical_TW_CommonTest {
  static testMethod void testMedical_TW_Common(){
    Test.startTest();
      Medical_TW_Common.IntegrationHeaderType objIntegrationHeaderType = new Medical_TW_Common.IntegrationHeaderType();
      System.assert(objIntegrationHeaderType != null, 'The object is initialized');
    Test.stopTest();
  }
}