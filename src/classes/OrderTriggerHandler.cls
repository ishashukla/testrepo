/**================================================================      
 * Appirio, Inc
 * Name: OrderTriggerHandler
 * Description: Handler class for trigger OrderTrigger
 * Created Date: 19/05/2016
 * Created By: Pratibha Chhimpa (Appirio)
 * 
 * Date Modified      Modified By                   Description of the update
 * 13 July 2016       Pratibha Chhimpa(Appirio)     T-518505 (Added WS Callout callCreateSalesOrderWS 
 *                                                  which is Using WSDL classes to create order on Oracle SOA)
 * 
 * 04 Aug 2016        Parul Gupta (Appirio)         Reverted T-518505 and Added T-524385 (Modified method callCreateSalesOrderWS() which  
 *                                                  now supports Http callout to create order on Oracle SOA)
 * 
 * 09 Aug  2016       Parul Gupta (Appirio)         T-525451 | Added entry condition for Rest callout
 * 07 Oct  2016       Parul Gupta (Appirio)         T-542805 | Revert back to std order
 * 21 Nov  2016		  Gagan Brar					I-244441 | Added logic for updating Salesrep details
 ==================================================================*/
public without sharing class OrderTriggerHandler {
  public static Boolean isTriggerInProgress;
  public static Boolean isTriggerExcecuted = false;
    
  // Method to be called on AfterInsert event of trigger
  //  @param List of new after insert orders
  //  @return void.
  public static void onAfterInsert(List<Order> newOrders) {
    List<Order> updateableOrders = new List<Order>();
    List<Id> orderIds = new List<Id>();
    
    Id endoOrdersId1 = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Orders').getRecordTypeId();
    Id endoOrdersId2 = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Complete Orders').getRecordTypeId();
      
    //system.debug('**** Gagan ***** Endo--onAfterInsert: '+ newOrders);
    for(Order ordr : newOrders) {
    
      /* I-236186 - Commented this logic for Endo rollout
      // T-525451 - Add entry condition for WS callout
      if (ordr.Oracle_Order_Number__c == null){
        orderIds.add(ordr.id);
      }
     I-236186 - End of Comment */
      // 07 Oct 2016 T-542805 Added endo specific record type condition
      //system.debug('**** Gagan ***** ordr.Oracle_Order_Number__c: '+ ordr.Oracle_Order_Number__c);
	  //system.debug('**** Gagan ***** ordr.Name: '+ ordr.Name);
      //system.debug('**** Gagan ***** ordr.RecordTypeId: ' + ordr.RecordTypeId + ' ,endoOrdersId1: ' + endoOrdersId1 + ' ,endoOrdersId2: '+ endoOrdersId2);
      if((ordr.RecordTypeId == endoOrdersId1 || ordr.RecordTypeId == endoOrdersId2) && Ordr.Name != null &&ordr.Oracle_Order_Number__c != null) {
        updateableOrders.add(ordr);
      }
    }
    
    if(updateableOrders.size() > 0) {
      addOrdertoSupportCases(updateableOrders);
    }
    
   /* I-236186 - Commented this logic for Endo rollout   
    // T-518505 - WS Callout for Order creation
    if(!isTriggerExcecuted && orderIds.size() > 0){
        callCreateSalesOrderWS(orderIds);
    }
   I-236186 - End of Comment */
  }
    
  // Method to be called on AfterUpdate event of trigger
  //  @param List of new after update orders, old Map of updated orders
  //  @return void.
  public static void onAfterUpdate(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
    List<Order> updateableOrders = new List<Order>();
    Id endoOrdersId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Orders').getRecordTypeId();
    for(Order ordr : newOrders) {
    
      // 07 Oct 2016 T-542805 Added endo specific record type condition
      if(ordr.RecordTypeId == endoOrdersId 
      && ordr.Name != null 
      && ordr.Oracle_Order_Number__c != null
      && isChanged(ordr.Name, oldOrderMap.get(ordr.Id).Name)) {
        updateableOrders.add(ordr);
      }
    }
    if(updateableOrders.size() > 0) {
      addOrdertoSupportCases(updateableOrders);
    }
  }
  
    
  //  Method to Add Order to Support cases and create new Order if case associated contact is null
  //  @param List of all the orders after update
  //  @return void.
  
  private static void addOrdertoSupportCases(List<Order> orders) {
        Map<String,Order> orderMap = new Map<String,Order>();       
        List<Order> updatedOrder = new List<Order>();
        List<Case> updatedCase = new List<Case>();
        
        // Get all Order Name 
        for(Order order : orders) {
            orderMap.put(order.Oracle_Order_Number__c, order);           
        }
        // Getting all the cases which have same orders as Oracle_Order_Ref__c 
        List<Case> lstCases = [SELECT Id, 
                                      Oracle_Order_Ref__c, 
                                      ContactId, 
                                      Current_Case_StdOrder__c 
                               FROM Case
                               WHERE Current_Case_StdOrder__c = null 
                               AND Oracle_Order_Ref__c IN : orderMap.keyset()
                               ];
                               
        for(Case cs : lstCases) {
            if(orderMap.containsKey(cs.Oracle_Order_Ref__c)) {
                updatedCase.add(new Case(Id = cs.Id, Current_Case_StdOrder__c = orderMap.get(cs.Oracle_Order_Ref__c).Id));             
                if(cs.ContactId != null) {
                    updatedOrder.add(new Order(Id = orderMap.get(cs.Oracle_Order_Ref__c).Id, Contact__c = cs.ContactId));
                }
            }
        }

        system.debug('@@@@@ updatedOrder : before ' + updatedOrder );
        system.debug('@@@@@ updatedCase: after >> ' + updatedCase);
        try{         
            if(updatedOrder.size() > 0) {
              isTriggerInProgress = true;
              update updatedOrder;
            }
            if(updatedCase.size() > 0){
              update updatedCase;
            }
        }catch(DmlException e) {
            System.debug('The following exception has occurred in updating Orders: ' + e.getMessage());
        }
   
  }
  
    //  Return a boolean to check Order Number is changed or not 
    //  @param String NewOrderNumber, String OldOrderNumber
    //  @return a boolean whether its changed or not.

    private static boolean isChanged(String newOrderNumber, String oldOrderNumber) {
        if(newOrderNumber == null && oldOrderNumber != null) {
            return true;
        } else if(oldOrderNumber == null && newOrderNumber != null) {
            return true;
        } else if(oldOrderNumber == null && newOrderNumber == null) {
            return false;
        } else {
            return !newOrderNumber.equals(oldOrderNumber);
        }
    }  
    
  //  Method supports Http Callout for Order creation on Oracle SOA
  //  @param List of new after insert orders
  //  @return void.
  /*@future(callout=true)
  public static void callCreateSalesOrderWS(List<ID> ordIds) {   
    List<Order> updateOrderResponse = new List<Order>();
    Order order;
       
    // Fetch orders 
    for(Order ord : [Select id, type, Pricebook2.Name, 
                      PoNumber, Payment_Terms__c, 
                      Ship_To_Address__c, Bill_To_Address__c,
                      Account.AccountNumber, Ship_To_Address__r.Location_ID__c,Service_Results__c, 
                      Bill_To_Address__r.Location_ID__c,
                      Credit_Hold__c, Salesrep_Name__c,
                      (Select id from OrderItems)
                      from Order where id in : ordIds]){
      order = ord;                   
    } 
   
    // Call Rest utility to create order on oracle SOA
    String response = RestCalloutUtility.createOrderRestCallout(order); 
   
    // Save response in field Service_Results__c   
    order.Service_Results__c = response;
    updateOrderResponse.add(order);    
   
    if(!updateOrderResponse.isEmpty()){
      isTriggerExcecuted = true;  
      update updateOrderResponse;
    }
  }*/

  // Method to be called on BeforeInsert event of trigger
  public static void onBeforeInsert(List<Order> newOrders) {
    List<Order> updateableOrders = new List<Order>();
    
    Id endoOrdersId1 = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Orders').getRecordTypeId();
    Id endoOrdersId2 = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Complete Orders').getRecordTypeId();
    
    for(Order ordr : newOrders) {
    	if((ordr.RecordTypeId == endoOrdersId1 || ordr.RecordTypeId == endoOrdersId2) && ordr.Oracle_Order_Number__c != null) {
        	system.debug('**** Gagan ***** updateableOrders: '+ ordr);
        	updateableOrders.add(ordr);
      	}
    }
    if(updateableOrders.size() > 0) {
      UpdateSalesrepDetailOnOrder(updateableOrders, 'onBeforeInsert');
    }

  }

  // Method to be called on BeforeUpdate event of trigger
  public static void onBeforeUpdate(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
    List<Order> updateableOrders = new List<Order>();
    Id endoOrdersId1 = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Orders').getRecordTypeId();
    Id endoOrdersId2 = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Complete Orders').getRecordTypeId();
    
    for(Order ordr : newOrders) {

    	if((ordr.RecordTypeId == endoOrdersId1 || ordr.RecordTypeId == endoOrdersId2)
		&& ordr.Oracle_Order_Number__c != null
		&& (issalesrepChanged(ordr.Salesrep_Id__c, oldOrderMap.get(ordr.Id).Salesrep_Id__c))) {
			updateableOrders.add(ordr);
      }
    }
    if(updateableOrders.size() > 0) {
      UpdateSalesrepDetailOnOrder(updateableOrders, 'onBeforeUpdate');
    }
  }
    
  //  I-244441: Method to update salesrep details on Order Header for Endo Orders
      private static void UpdateSalesrepDetailOnOrder(List<Order> orders, String TriggerType) {
        Map<string,contact> SalesrepMap = new Map<String,contact>();
        List<string> salesrepIds = new List<string>();
        List<Order> updatedOrder = new List<Order>();
        Id endoInternalContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact').getRecordTypeId();

        system.debug('**** Gagan **** UpdateSalesrepDetailOnOrder ' + orders );
        // Get all salesrep Ids
        for(Order odr : orders) {
            salesrepIds.add(odr.Salesrep_Id__c + '.000000000000000');
        }
		system.debug('**** Gagan **** salesrepIds: ' + salesrepIds );
          
        // Getting all the contacts which have same Salesrep ID
        List<contact> lstSalesrep = [SELECT Id, Name, Director__c, Sales_Rep_ID__c, Regional_Mgr__r.Name
                                     FROM Contact
                                     WHERE RecordTypeId =: endoInternalContactId
                                     AND Sales_Rep_ID__c IN : salesrepIds
                                    ];
        system.debug('**** Gagan **** lstSalesrep: ' + lstSalesrep );
          
        for(Contact cnt : lstSalesrep) {
            SalesrepMap.put(cnt.Sales_Rep_ID__c, cnt);
        }
          
        for(Order odr : orders) {
            if(SalesrepMap.containsKey(odr.Salesrep_Id__c+'.000000000000000')){
                contact Salesrep = SalesrepMap.get(odr.Salesrep_Id__c+'.000000000000000');
                //system.debug('**** Gagan **** Salesrep: ' + Salesrep );
                odr.Sales_Director__c = Salesrep.Director__c;
                //system.debug('**** Gagan **** Salesrep.Director__c: ' + odr );
                odr.Salesrep_Name__c = Salesrep.Name;
                //system.debug('**** Gagan **** Salesrep.Name: ' + odr );
                odr.Regional_Manager__c = Salesrep.Regional_Mgr__r.Name;
                //system.debug('**** Gagan **** Salesrep.Regional_Mgr__r: ' + odr );
            }else{
                odr.Sales_Director__c = null;
                odr.Salesrep_Name__c = null;
                odr.Regional_Manager__c = null;
            }
        }
      }

    //  Return a boolean to check if salesrep on a Order is changed or not 
    //  @return a boolean whether its changed or not.

    private static boolean issalesrepChanged(String newsalesrepid, String oldsalesrepid) {
        if(newsalesrepid == null && oldsalesrepid != null) {
            return true;
        } else if(oldsalesrepid == null && newsalesrepid != null) {
            return true;
        } else if(oldsalesrepid == null && newsalesrepid == null) {
            return false;
        } else {
            return !newsalesrepid.equals(oldsalesrepid);
        }
    } 
 
}