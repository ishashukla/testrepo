/**================================================================      
* Appirio, Inc
* Name: ServiceContractOpportunityController
* Description: To create new opportunity and close open task. (I-228773)
* Created By :  Nitish Bansal
* Created Date : 10th Aug 2016
*
* Date Modified       Modified By      Description of the update
* 16th August 2016    Nitish Bansal    Feedback changes (I-228773)
* 2nd September 2016    Nitish Bansal  Redirecting the user back to service contract page in case of exception (I-233075)

==================================================================*/

global class ServiceContractOpportunityController {

    webService static String createOppty(Id serviceContractId, Id accountId) 
    {   
        //Querying service contract details as per the passed record Id
        List<SVMXC__Service_Contract__c> serviceContractList = new List<SVMXC__Service_Contract__c>();
        serviceContractList = [SELECT SVMXC__Company__r.OwnerId, SVMXC__Company__c, SVMXC_Opportunity__c, RecordTypeId, Name, External_ID__c, Id 
                              FROM SVMXC__Service_Contract__c 
                              WHERE Id = :serviceContractId];
        
        if(serviceContractList.size() > 0){
            Savepoint sp = Database.setSavepoint(); //NB - 09/02 - I-233075

            //Creating new service contract opportunity
            Opportunity opp = new Opportunity();
            opp.name = serviceContractList.get(0).Name;
            opp.Type = Constants.OPP_TYPE;
            opp.AccountId = accountId;
            opp.CloseDate = System.today().addMonths(6);
            opp.StageName = Constants.OPP_STAGE;
            opp.Percent_Probability__c = Constants.PERCENT_PROBABILITY;
            opp.Stage__c = Constants.OPP_STAGE;
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.COMM_SERVICE_OPP).getRecordTypeId();  
            opp.OwnerId =  serviceContractList.get(0).SVMXC__Company__r.OwnerId;   
            opp.Originating_Service_Contract_External_ID__c = serviceContractList.get(0).External_ID__c;   
            opp.Originating_Service_Contract_Number__c = serviceContractList.get(0).Name;
            try     //NB - 09/02 - I-233075
            {
                insert opp; 
    
                //associating the opportunity with the service contract
                serviceContractList.get(0).SVMXC_Opportunity__c = opp.Id;
                update serviceContractList;

                List<Task> tasks = new List<Task>();

                //closing all open tasks linked to the service contract
                for(Task task : [SELECT What.Id, WhatId, Status, IsClosed, Id 
                                FROM Task 
                                WHERE What.Id = :serviceContractId 
                                AND RecordTypeId = :Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_SERVICE_CONTRACT_RT).getRecordTypeId()]){
                    if(!task.IsClosed){//checking if the task is open
                        task.Status = Constants.TASK_STATUS;      
                        tasks.add(task);
                    }
                }
                if(tasks.size() > 0){
                    update tasks;
                }
            }
            //NB - 09/02 - I-233075 - Start
            catch(Exception e)
            {
                system.debug('error : ' + e.getMessage());
                Database.rollback(sp);
                return serviceContractId;   //return to original service contract
            } 
            //NB - 09/02 - I-233075 - ENd

            //return to oppotunity page
            return opp.Id;
        } 
        return serviceContractId;   //return to original service contract
    }
}