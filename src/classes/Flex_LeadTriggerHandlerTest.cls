//
// (c) 2012 Appirio, Inc.
//
//  Test class of  WebserviceAmendOpportunity
//
// 19 Aug 2015     Naresh K Shiwani      Original
//
@isTest
private class Flex_LeadTriggerHandlerTest {
    
  static testmethod void testLeadTrigger(){
    Test.startTest();
    Flex_Sales_Rep__c flexSalesCustomSetting  = new Flex_Sales_Rep__c();
    flexSalesCustomSetting.RFM_Username__c    = 'test@dummy.com';
    flexSalesCustomSetting.Name               = 'West';
    insert flexSalesCustomSetting;
    System.assertNotEquals(flexSalesCustomSetting, null);
    
    User usr                                  = TestUtils.createUser(1, null, false).get(0);
    usr.Username                              = 'test123321@dummy.com';
    insert usr;
    System.assertNotEquals(usr, null);
    
    Lead leadObj                              = new Lead();
    leadObj.AnnualRevenue                     = 100;
    leadObj.FirstName                         = 'FirstName';
    leadObj.LastName                          = 'LastName';
    leadObj.Company                           = 'Company';
    leadObj.Status                            = 'Open';
    leadObj.Country                           = 'United States';
    leadObj.State                             = 'Washington';
    //leadObj.StateCode                         = 'WA';
    insert leadObj;
    System.assertNotEquals(leadObj, null);
    
    leadObj.Country                           = 'United States';
    leadObj.State                             = 'New Mexico';
    //leadObj.StateCode                         = 'NM';
    update leadObj;
    
    Test.stopTest();
  }
  
}