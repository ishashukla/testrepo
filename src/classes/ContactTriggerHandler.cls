/*************************************************************************************************
Created By:    Prakarsh Jain
Date:          Sept 28, 2015
Description  : Contact Trigger Handler Class
**************************************************************************************************/
public class ContactTriggerHandler {
    //Method to prevent user to delete contacts with record type Medical Employee Contact and profile Medical - System Admin
    public static void preventDeleteOfMedEmployee(List<Contact> lstContacts){
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Medical Employee Contact').getRecordTypeId();
        String errorMsg = 'You do not have sufficient privileges to delete the record';
        Id usrId = UserInfo.getProfileId();
        system.debug('usrId'+usrId);
        Profile prof = [SELECT Name from Profile WHERE Id =: usrId];
        system.debug('prof'+prof);
        system.debug('prof.Name'+prof.Name);
        if(prof.Name == 'Medical - System Admin'){
            for(Contact con : lstContacts){
                system.debug('con.RecordType.Name'+con.RecordType.Name);
                if(con.RecordTypeId == recordtypeId){
                    con.addError(errorMsg);
                }
            }
        }
    }

     //-----------------------------------------------------------------------------------------------
  // Insert record in contact account junction object.
  //-----------------------------------------------------------------------------------------------
  public static void InsertContactAccountAssociation(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts) {
    Schema.RecordTypeInfo rtByNameCC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Medical Customer Contact');
    Id recordTypeCC = rtByNameCC.getRecordTypeId();

    //Schema.RecordTypeInfo rtByNameSC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
    //Id recordTypeSC = rtByNameSC.getRecordTypeId();


    List<Contact_Acct_Association__c> lstCustomerContacts = new List<Contact_Acct_Association__c>();
    //List<Stryker_Contact__c> lstStrykerContacts = new List<Stryker_Contact__c>();
    System.debug('@@@' + mapOldContacts);
    for(Contact c :lstNewContacts){
        if(mapOldContacts == null || mapOldContacts.get(c.Id).AccountId != c.AccountId){
        //System.debug('@@@' + c.RecordTypeId == recordTypeCC);
        System.debug('c.RecordTypeId' + c.RecordTypeId);
        System.debug('recordTypeCC' + recordTypeCC);
        System.debug('@@@$$$' + c);

          if(c.RecordTypeId == recordTypeCC && c.accountId != null){

            Contact_Acct_Association__c obj = new Contact_Acct_Association__c();
            obj.Associated_Account__c = c.AccountId;
            obj.Associated_Contact__c = c.Id;
            obj.Primary__c = true;
            lstCustomerContacts.add(obj);
          }
        }
    }
    System.debug('@@@###' + lstCustomerContacts);
    if(lstCustomerContacts.size() > 0){
      Database.insert (lstCustomerContacts,false);
    }
    //insert lstStrykerContacts; We are not sure for Internal contact as of now so commented this.
  }
}