/*******************************************************************************
 Author        :  Appirio JDC (Sunil Gupta)
 Date          :  Jan 27, 2014
 Purpose       :  Class having common methods for test data creation
* 
* Date              Updated By          Description
* 17 Sept 2016      Nathalie Le Guay    added createAsset() and createCaseAssetJunction()
*******************************************************************************/
@isTest
public without sharing class Endo_TestUtils {
  
  //****************************************************************************
  // Method to Create Case 
  //****************************************************************************
  public static List<Case> createCase(Integer accCount, Id accountId, Id contactId, Boolean isInsert){
    Account acc = createAccount(1, true).get(0);
    List<Case> lstCase = new List<Case>();
    for(Integer i = 0; i < accCount; i++){
      Case cs = new Case(Subject = 'Test Subject', Oracle_Order_Ref__c = '1234',
                AccountId = accountId, ContactId = contactId);
     
      lstCase.add(cs);
    }
    if(isInsert){
      insert lstCase;
    }
    return lstCase;
  }
  


//****************************************************************************
  // Method to Create Asset
  //****************************************************************************
  public static List<Asset__c> createAsset(Integer count, Id accountId, Boolean isInsert){
    List<Asset__c> lstAsset = new List<Asset__c>();
    for(Integer i = 0; i < count; i++){
      Asset__c assetObj = new Asset__c(Name='Test Asset', Owner_Account__c = accountId,Asset_ID__c= String.valueOf(i));
      lstAsset.add(assetObj);
}
    if(isInsert){
      insert lstAsset;
    }
    return lstAsset;
  }


  //****************************************************************************
  // Method to Create Case_Asset__c Object
  //****************************************************************************
  public static List<Case_Asset__c> createCaseAssetJunction(Integer count, Id caseId, Id assetId, Boolean isInsert){
    List<Case_Asset__c> lstCaseAsset = new List<Case_Asset__c>();
    for(Integer i = 0; i < count; i++){
      Case_Asset__c caseAssetObj = new Case_Asset__c(Case__c= caseId, Asset__c = assetId);
      lstCaseAsset.add(caseAssetObj);
    }
    if(isInsert){
      insert lstCaseAsset;
    }
    return lstCaseAsset;
  }


  //****************************************************************************
  // Method to Create Junction object Stryker Contact 
  //****************************************************************************
  public static List<Stryker_Contact__c> createContactJunction(Integer accCount, Id accId, Id contactId, Boolean isInsert){
    Account acc = createAccount(1, true).get(0);
    Contact con = createContact(1, acc.Id, true).get(0);
    
    List<Stryker_Contact__c> lstSC = new List<Stryker_Contact__c>();
    for(Integer i = 0; i < accCount; i++){
      Stryker_Contact__c sc = new Stryker_Contact__c(Internal_Contact__c = contactId, Customer_Account__c = accId);

    lstSC.add(sc);
    }
    if(isInsert){
      insert lstSC;
    }
    return lstSC;
  }
   
  
  //****************************************************************************
  // Method to fetch profile 
  //****************************************************************************
  public static Profile fetchProfile(String profileName){
    Profile pr;
    if(profileName == null || profileName == ''){
      pr = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'].get(0);
    }
    else{
      for(Profile p : [SELECT Id, Name FROM Profile WHERE Name = :profileName LIMIT 1]){
        pr = p;
      } 
    }
    return pr;
  }
  
  
  //****************************************************************************
  // Method to create test user records
  //****************************************************************************
  public static List<User> createUser(Integer userCount, String profileName, Boolean isInsert ){
    Profile pr = fetchProfile(profileName);
    List<User> userList = new List<User>();
    User u;
    for(Integer i=0; i<userCount; i++){
      u = new User(ProfileId = pr.Id,
                   alias = 'usr' + i, 
                   email = 'user' + i + '@test.com', 
                   emailencodingkey = 'UTF-8', 
                   lastname = 'Testing' + i, 
                   languagelocalekey = 'en_US', 
                   localesidkey = 'en_US', 
                   timezonesidkey = 'America/Los_Angeles', 
username = 'user' + i + '@test.com.stryker' + i);
      userList.add(u);
    }
    if(isInsert){
      insert userList;
    }
    return userList;
  }
  
  //****************************************************************************
// Method to create Assignment Group
  //****************************************************************************
  public static List<Assignment_Group_Name__c > createAssignmentGroup(Integer groupCount, Boolean isInsert){
    List<Assignment_Group_Name__c> lstAssignmentGroups = new List<Assignment_Group_Name__c>();
    for(Integer i = 0; i < groupCount; i++) {
      Assignment_Group_Name__c groupEntry = new Assignment_Group_Name__c(
                                            Name = 'Endo Assignment Group : '+ i,
                                            Type__c = 'Case'
      );
      lstAssignmentGroups.add(groupEntry);
    }
    if(isInsert){
      insert lstAssignmentGroups;
    }
    return lstAssignmentGroups;
  }

  //****************************************************************************
  // Method to create Assignment Group
  //****************************************************************************
  public static Assignment_Groups__c  createAssignmentGroupMember(String userId, String parentId, Boolean isInsert){
    
    Assignment_Groups__c memberEntry = new Assignment_Groups__c(
                                            Active__c = 'True',
                                            Group_Name__c = parentId,
                                            User__c = userId
    );
          
    if(isInsert){
      insert memberEntry;
    }
    return memberEntry;
  }
  //****************************************************************************
  // Method to create test Account records
  //****************************************************************************  
  public static List<Account> createAccount(Integer accCount, Boolean isInsert){
    List<Account> lstAccoount = new List<Account>();
    for(Integer i = 0; i < accCount; i++){
      Account acc = new Account(Name = 'Test Account ' + i,
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States',
                                ShippingCountryCode = 'US',AccountNumber = '1233332');
      lstAccoount.add(acc);
    }
    if(isInsert){
      insert lstAccoount;
    }
    return lstAccoount;
  }
  
  //****************************************************************************
  // Method to create test Contact records
  //****************************************************************************  
  public static List<Contact> createContact(Integer conCount, Id accountId, Boolean isInsert){
    Id recTypeId = Schema.sObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact').getRecordTypeId();
    List<Contact> lstContact = new List<Contact>();
    for(Integer i = 0; i < conCount; i++){
      Contact con = new Contact(FirstName = 'TestFName' + i,
LastName = 'TestLName' + i,Email = 'test@test.com',
                                AccountId = accountId, recordtypeId=recTypeId);
      lstContact.add(con);
    }
    if(isInsert){
      insert lstContact;
    }
    return lstContact;
  }

  
  //****************************************************************************
  // Method to create test Product records
  //****************************************************************************  
  public static List<Product2> createProduct(Integer prodCount, Boolean isInsert){
    List<Product2> lstProduct = new List<Product2>();
    for(Integer i = 0; i < prodCount; i++){
      Product2 prod = new Product2(Name = 'Test Product' + i, Part_Number__c = '12345' + i);
      lstProduct.add(prod);
    }
    if(isInsert){
      insert lstProduct;
    }
    return lstProduct;
  }
  
  
  public static Pricebook2 getStandardPricebook() {
    List<Pricebook2> blist = 
      [select Name from Pricebook2 where isStandard = true];
    return blist[0];
  } 
  
  
  public static PricebookEntry getTestPricebookEntry(Id pricebookId, Id productId) {
    PricebookEntry pbe = new PricebookEntry();
    pbe.Pricebook2Id = pricebookId;
    pbe.Product2Id = productId;
    pbe.IsActive = true;
    pbe.UnitPrice = 0.0;
    insert pbe;
    return pbe;
  }
  

    
  //****************************************************************************
  // Method to create Pricing Contract
  //****************************************************************************  
  public static List<Pricing_Contract__c> createPC(Integer cnt, Boolean isInsert){
    List<Pricing_Contract__c> lst = new List<Pricing_Contract__c>();
    for(Integer i = 0; i < cnt; i++){
      Pricing_Contract__c obj = new Pricing_Contract__c(End_Date__c = System.today(), 
                                    Start_Date__c = System.today(), Is_Active__c = true);
      lst.add(obj);
    }
    if(isInsert){
      insert lst;
    }
    return lst;
  }
  
  
  
  //****************************************************************************
  // Method to create Pricing Contract Items
  //****************************************************************************  
  public static List<Pricing_Contract_Line_Item__c> createPCLI(Integer cnt, Boolean isInsert){
    List<Pricing_Contract_Line_Item__c> lst = new List<Pricing_Contract_Line_Item__c>();
    for(Integer i = 0; i < cnt; i++){
      Pricing_Contract_Line_Item__c obj = new Pricing_Contract_Line_Item__c(End_Date__c = System.today(), 
                                    Start_Date__c = System.today(), Major_Code__c = '123');
      lst.add(obj);
    }
    if(isInsert){
      insert lst;
    }
    return lst;
  }
  
  //****************************************************************************
  // Method to create Account Pricing Contract Junction
  //****************************************************************************  
public static List<Pricing_Contract_Account__c> createAccountPricingContractJunction(Integer cnt, Id accountId, Id priceContractId, Boolean isInsert){
    List<Pricing_Contract_Account__c> lst = new List<Pricing_Contract_Account__c>();
    for(Integer i = 0; i < cnt; i++){
      Pricing_Contract_Account__c obj = new Pricing_Contract_Account__c(Account__c = accountId, 
                                    Contract_Type__c = 'Local', Pricing_Contract__c = priceContractId);
      lst.add(obj);
    }
    if(isInsert){
      insert lst;
    }
    return lst;
  }
  
  //****************************************************************************
  // Method to create Pricing Contract Junction
  //****************************************************************************  
  public static Pricing_Contract_Account__c createPricingContractAccount(Id accId, Id pcId, Boolean isInsert){
      Pricing_Contract_Account__c pca;
    pca = new Pricing_Contract_Account__c(Account__c = accId, Pricing_Contract__c = pcId);
    if(isInsert){
      insert pca;
    }
    return pca;
  }

  //****************************************************************************
  // Method to create Question
  //****************************************************************************  
 public static Question__c createQuestion(String questionStr, String questionType, Boolean isInsert){
    Question__c question = new Question__c();
    question.Type__c = questionType;
    question.Question__c = questionStr ;
    if(isInsert){
      insert question;
    }
    return question;
  }
  
  //****************************************************************************
  // Method to create Order
  //****************************************************************************  
  public static Order createStandardOrder(Id conId, Id accId, Boolean isInsert){
    Order ord = new Order(); 
    ord.Contact__c = conId;
    ord.AccountId = accId ;
    ord.RMA_Number__c = '123';
    ord.PoNumber = '12321';
    ord.EffectiveDate = system.Today();
    
    if(isInsert){
      insert ord ;
    }
    return ord ;
  }
  
   //****************************************************************************
  // Method to create Order Product
  //****************************************************************************  
  public static OrderItem createOrderItem(Id prodId, Id ordId, Boolean isInsert){
    OrderItem ordItems = new OrderItem();   
    ordItems.ServiceDate = system.today();
    ordItems.Shipped_Date__c = system.today();
    ordItems.Part_Number__c = prodId;
    ordItems.Description = 'checking';
    ordItems.Quantity_Shipped__c  = 12;
    ordItems.OrderId = ordId;
    ordItems.Oracle_Ordered_Item__c = 'testingg';
    ordItems.Quantity = 1.00 ;
    ordItems.UnitPrice = 2.00;
    ordItems.Line_Number__c = 1.123;
    if(isInsert){
      insert ordItems ;
    }
    return ordItems ;
  }
  
}