// (c) 2015 Appirio, Inc.
//
// Class Name: ManagerSalesForecastControllerTest
// Description: Test Class for ManagerSalesForecastController   
// April 21, 2016    Meena Shringi    Original 
// June 17, 2016     Isha Shukla      Modified (T-512566)
@isTest
private class ManagerSalesForecastControllerTest {
    static Forecast_Year__c ForcastYearRep;
    static List<Opportunity> opp;
    static List<User> userList;
    static User rep, manager, SysAdmin, managerUser, repUser1, manUser1;
    static Id recordTypeRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast').getRecordTypeId();
    static Id recordTypeManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast').getRecordTypeId();
    // Method to test forecast in submitted view as Instruments manager and division IVS and Rep having mancode type Regional Manager
    @isTest static void testRepRecordSubmittedview() { 
        testData();
        insert userList;
        
        /*User rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        rep.Division = 'IVS';
        rep.UserRoleId = role.Id;
        insert rep;
        User manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role2 = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        manager.Division = 'IVS';
        manager.UserRoleId = role2.Id;
        insert manager;
        User SysAdmin = TestUtils.createUser(1,'System Administrator',true).get(0);
        User managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'IVS';
        insert managerUser;
        User repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'IVS';
        insert repUser1;
        User manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'IVS';
        insert manUser1;*/
        // creating manacode records 
        System.runAs(SysAdmin) {
            List<Mancode__c> mancodeList = new List<Mancode__c>();
            mancodeList = TestUtils.createMancode(3, rep.Id, manager.Id, false);
            mancodeList[0].Business_Unit__c = 'IVS';  
            mancodeList[1].Business_Unit__c = 'IVS';
            mancodeList[1].Sales_Rep__c =  managerUser.Id;
            mancodeList[0].Type__c = 'VP';
            mancodeList[1].Type__c = 'Regional Manager';
            mancodeList[2].Sales_Rep__c = repUser1.Id;
            mancodeList[2].Manager__c = manUser1.Id;
            mancodeList[2].Business_Unit__c = 'IVS';
            mancodeList[2].Type__c = 'Director';
            insert mancodeList;
            List<Account> accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'IVS';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = rep.Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            
        }
        // creating forecast for rep
        System.runAs(rep) {
            ForcastYearRep = TestUtils.createForcastYear(1,rep.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='IVS';
            ForcastYearRep.Year_External_Id__c= '1234';
            insert ForcastYearRep;
            insert opp;
        }
        System.runAs(manager) {
            Forecast_Year__c ForcastYearManager = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            ForcastYearManager.Year__c = String.valueOf(System.today().year());
            ForcastYearManager.Business_Unit__c='IVS';
            ForcastYearManager.Year_External_Id__c= '12345';
            insert ForcastYearManager;
            Forecast_Year__c nfy = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            nfy.Year__c = '2017';
            PageReference pageRef = Page.ManagerSalesForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', manager.Id); 
            System.currentPageReference().getParameters().put('managerView','true');
            
            ManagerSalesForecastController managerSalesForecast = new ManagerSalesForecastController();
            managerSalesForecast.isRM = true;
            managerSalesForecast.processOpenForecastAmountforRM();
            managerSalesForecast.showSummaryData(String.valueOf(System.today().month()));
            managerSalesForecast.nfy = nfy;
            managerSalesForecast.createNewForecastYear();
            system.debug('yearoption=='+managerSalesForecast.yearOptions);
            managerSalesForecast.cancelNew();
            List<String> monthStrings = new List<String>{'','January','February','March','April','May','June','July','August','September','October','November','December'};
            managerSalesForecast.selectedView = 'Submitted View';
            managerSalesForecast.baseTrailMonth =monthStrings.get(System.today().month());
            managerSalesForecast.baseTrailYear = String.valueOf(System.today().year());
            managerSalesForecast.forecastYr = String.valueOf(System.today().year());
            managerSalesForecast.baseTrailUserId = String.valueOf(manager.Id);
            //managerSalesForecast.nfy = nfy;
            managerSalesForecast.doSort = false;
            managerSalesForecast.updateTotal();
            Test.startTest();
            managerSalesForecast.getUserBaseTrailRecords();
            managerSalesForecast.getColumnsToDisplay();
            managerSalesForecast.collapseList();
            managerSalesForecast.setYear();
            managerSalesForecast.first();
            managerSalesForecast.last();
            managerSalesForecast.previous();
            managerSalesForecast.next();
            managerSalesForecast.sortData();
            managerSalesForecast.getUserOpportunityRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerBaseTrailRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerOpportunityRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getUserOpportunityUpsideRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerOpportunityUpsideRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.updateForecast();
            managerSalesForecast.processOpenForecastAmountforVP();
            managerSalesForecast.processOpenForecastAmountforDirector();
            System.assertEquals(managerSalesForecast.yearOptions != null, true);
            Test.stopTest();
        }
    }
    
    @isTest static void testControllerWhenSummaryView() {
        testData();
        insert userList;
        /*User rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        rep.Division = 'IVS';
        rep.UserRoleId = role.Id;
        insert rep;
        User manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role2 = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        manager.Division = 'IVS';
        manager.UserRoleId = role2.Id;
        insert manager;
        User SysAdmin = TestUtils.createUser(1,'System Administrator',true).get(0);
        User managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'IVS';
        insert managerUser;
        User repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'IVS';
        insert repUser1;
        User manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'IVS';
        insert manUser1;*/
        // creating manacode records 
        System.runAs(SysAdmin) {
            List<Mancode__c> mancodeList = new List<Mancode__c>();
            mancodeList = TestUtils.createMancode(3, rep.Id, manager.Id, false);
            mancodeList[0].Business_Unit__c = 'IVS';  
            mancodeList[1].Business_Unit__c = 'IVS';
            mancodeList[1].Sales_Rep__c =  managerUser.Id;
            mancodeList[0].Type__c = 'VP';
            mancodeList[1].Type__c = 'Regional Manager';
            mancodeList[2].Sales_Rep__c = repUser1.Id;
            mancodeList[2].Manager__c = manUser1.Id;
            mancodeList[2].Business_Unit__c = 'IVS';
            mancodeList[2].Type__c = 'Director';
            insert mancodeList;
            List<Account> accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'IVS';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = rep.Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            opp[0].StageName = 'In Development';
            opp[0].ForecastCategoryName = 'Pipeline';
            opp[0].Service_Monthly__c = 1.00;
            opp[0].Total_Trailing_Impact__c = 1.00;
            opp[0].CloseDate = System.today();
            
        }
        
        // creating forecast for rep
        System.runAs(rep) {
            ForcastYearRep = TestUtils.createForcastYear(1,rep.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='IVS';
            ForcastYearRep.Year_External_Id__c= '1234';
            insert ForcastYearRep;
            insert opp;
        }
        System.runAs(manager) {
            Forecast_Year__c ForcastYearManager = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            ForcastYearManager.Year__c = String.valueOf(System.today().year());
            ForcastYearManager.Business_Unit__c='IVS';
            ForcastYearManager.Year_External_Id__c= '12345';
            insert ForcastYearManager;
            Forecast_Year__c nfy = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            nfy.Year__c = '2017';
            // List<Monthly_Forecast__c> mFList = new List<Monthly_Forecast__c>();
            // mFList = [SELECT Id,Month__c,Manager__c,Forecast_Resubmitted__c,Business_Unit__c ,Forecast_Submitted__c FROM Monthly_Forecast__c WHERE Forecast_Year__c=:ForcastYearRep.id];
            // mFList[0].Forecast_Submitted__c = true;
            // update mFList;//
            PageReference pageRef = Page.ManagerSalesForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', manager.Id); 
            System.currentPageReference().getParameters().put('managerView','true');
            Test.startTest();
            ManagerSalesForecastController managerSalesForecast = new ManagerSalesForecastController();
            managerSalesForecast.selectedView = 'Summary View';
            managerSalesForecast.isDirector = true;
            List<String> monthStrings = new List<String>{'','January','February','March','April','May','June','July','August','September','October','November','December'};
           // managerSalesForecast.selectedView = 'Submitted View';
            managerSalesForecast.processOpenForecastAmountforDirector();
            managerSalesForecast.showSummaryData('All');
            managerSalesForecast.switchView();
            managerSalesForecast.getColumnsToDisplay();
            managerSalesForecast.baseTrailMonth =monthStrings.get(System.today().month());
            managerSalesForecast.baseTrailYear = String.valueOf(System.today().year());
            managerSalesForecast.forecastYr = String.valueOf(System.today().year());
            managerSalesForecast.baseTrailUserId = String.valueOf(manager.Id);
            //managerSalesForecast.nfy = nfy;
            managerSalesForecast.doSort = false;
            managerSalesForecast.updateTotal();
            managerSalesForecast.getUserBaseTrailRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getUserOpportunityRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerBaseTrailRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerOpportunityRecords();
            managerSalesForecast.sortData();
            Test.stopTest();
            managerSalesForecast.getUserOpportunityUpsideRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerOpportunityUpsideRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.switchViewRefresh();
            managerSalesForecast.selectedView = 'Current Month View';
            managerSalesForecast.updateTotal();
            managerSalesForecast.switchView();
            managerSalesForecast.collapseList();
            managerSalesForecast.save();
            
        }
        
    }
    @isTest static void testControllerWhenVP() {
        testData();
        
          rep.Division = 'NSE';
          manager.Division = 'NSE';
          managerUser.Division = 'NSE';
          repUser1.Division = 'NSE';
          manUser1.Division = 'NSE';
          insert userList;
        
        /*User rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        rep.Division = 'NSE';
        rep.UserRoleId = role.Id;
        insert rep;
        User manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role2 = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        manager.Division = 'NSE';
        manager.UserRoleId = role2.Id;
        insert manager;
        User SysAdmin = TestUtils.createUser(1,'System Administrator',true).get(0);
        User managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'NSE';
        insert managerUser;
        User repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'NSE';
        insert repUser1;
        User manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'NSE';
        insert manUser1;*/
        // creating manacode records 
        System.runAs(SysAdmin) {
            List<Mancode__c> mancodeList = new List<Mancode__c>();
            mancodeList = TestUtils.createMancode(3, rep.Id, manager.Id, false);
            mancodeList[0].Business_Unit__c = 'NSE';  
            mancodeList[1].Business_Unit__c = 'NSE';
            mancodeList[1].Sales_Rep__c =  managerUser.Id;
            mancodeList[0].Type__c = 'VP';
            mancodeList[1].Type__c = 'Regional Manager';
            mancodeList[2].Sales_Rep__c = repUser1.Id;
            mancodeList[2].Manager__c = manUser1.Id;
            mancodeList[2].Business_Unit__c = 'NSE';
            mancodeList[2].Type__c = 'Director';
            insert mancodeList;
           
            List<Account> accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = rep.Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            opp[0].StageName = 'In Development';
            opp[0].ForecastCategoryName = 'Pipeline';
            opp[0].Service_Monthly__c = 1.00;
            opp[0].Total_Trailing_Impact__c = 1.00;
            opp[0].CloseDate = System.today();
            
        }
        // creating forecast for rep
        System.runAs(rep) {
            ForcastYearRep = TestUtils.createForcastYear(1,rep.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='NSE';
            ForcastYearRep.Year_External_Id__c= '1234';
            insert ForcastYearRep;
            insert opp;
        }
        System.runAs(manager) {
            Forecast_Year__c ForcastYearManager = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            ForcastYearManager.Year__c = String.valueOf(System.today().year());
            ForcastYearManager.Business_Unit__c='NSE';
            ForcastYearManager.Year_External_Id__c= '12345';
            insert ForcastYearManager;
            Forecast_Year__c nfy = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            nfy.Year__c = '2017';
            PageReference pageRef = Page.ManagerSalesForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', manager.Id); 
            System.currentPageReference().getParameters().put('managerView','true');
            Test.startTest();
            ManagerSalesForecastController managerSalesForecast = new ManagerSalesForecastController();
            managerSalesForecast.selectedView = 'Summary View';
            managerSalesForecast.isVP = true;
            List<String> monthStrings = new List<String>{'','January','February','March','April','May','June','July','August','September','October','November','December'};
           // managerSalesForecast.selectedView = 'Submitted View';
            managerSalesForecast.processOpenForecastAmountforVP();
            managerSalesForecast.showSummaryData('All');
            managerSalesForecast.switchView();
            managerSalesForecast.getColumnsToDisplay();
            managerSalesForecast.baseTrailMonth =monthStrings.get(System.today().month());
            managerSalesForecast.baseTrailYear = String.valueOf(System.today().year());
            managerSalesForecast.forecastYr = String.valueOf(System.today().year());
            managerSalesForecast.baseTrailUserId = String.valueOf(manager.Id);
            //managerSalesForecast.nfy = nfy;
            managerSalesForecast.doSort = false;
            managerSalesForecast.updateTotal();
            managerSalesForecast.getUserBaseTrailRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getUserOpportunityRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerBaseTrailRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getManagerOpportunityRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getUserOpportunityUpsideRecords();
            managerSalesForecast.sortData();

            Test.stopTest();
            managerSalesForecast.getManagerOpportunityUpsideRecords();
            managerSalesForecast.sortData();
            managerSalesForecast.getMissingForecast();
            managerSalesForecast.updateInitialForecast();
        }
    }

    static void testData() {
        userList = new List<User>();
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        
        rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        rep.Division = 'IVS';
        rep.UserRoleId = role.Id;
        userList.add(rep);
        
        manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        manager.Division = 'IVS';
        manager.UserRoleId = role.Id;
        userList.add(manager);
        
        SysAdmin = TestUtils.createUser(1,'System Administrator',false).get(0);
        userList.add(SysAdmin);
        
        managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'IVS';
        userList.add(managerUser);
        
        repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'IVS';
        userList.add(repUser1);
        
        manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'IVS';
        userList.add(manUser1);

    }
    
}