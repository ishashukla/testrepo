/*
//
// (c) 2015 Appirio, Inc.
//
// Apex Controller for Visualforce component CustomLookup
//
// This Component is using by Medical_ChangeCaseOwner Page.
//
// Created by : Surabhi Sharma (Appirio)
//
*/
public without sharing class CustomLookupCtrl {
	public String fieldName {get;set;}
	public String objName {get;set;}
  public String selectedId {get;set;}
  public String rowIndex {get;set;}
  public SObject currentRecord{get;set;}
  public String selectedRecordName {get;set;}
  public String fieldPopulatingId {get;set;}
  public String fieldSetNameAPI {get;set;}

  //-----------------------------------------------------------------------------------------------
  //  Cosntructor
  //-----------------------------------------------------------------------------------------------
  public CustomLookupCtrl(){
  	fieldName = '';
    objName = '';
    currentRecord = null;
  }

  //-----------------------------------------------------------------------------------------------
  //  Action method called from visualforce Component
  //-----------------------------------------------------------------------------------------------
  public PageReference getSelectedRecord(){
  	String query = '';
  	if(selectedId != null && selectedId != ''){
  		string currentRecordId = this.selectedId;
      if(currentRecordId <> null){
      	if(this.objName == 'Order') {
      		query = 'SELECT Id, Name, OrderNumber, RecordTypeId, ';
        }
        else if (this.objName == 'Group') {
        	query = 'SELECT Id, ';
        }
        else {
        	query = 'SELECT Id, ';
        }

        System.debug('==========Query Returned======='+ query + this.fieldName + ' FROM ' + this.objName);

        List<Sobject> sobjectList = Database.query(query + this.fieldName + ' FROM ' + this.objName + ' WHERE ID = : currentRecordId');

        if(!sobjectList.isEmpty()){
        	this.currentRecord = sobjectList[0];
          if(this.objName == 'Order') {
          	selectedRecordName = (String) sobjectList[0].get('OrderNumber');
          }
          else if(this.objName == 'Group') {
          	//String selectedRecordId = (String) sobjectList[0].get('groupId');
            //selectedRecordName = [Select Name From Group Where Id =: selectedRecordId][0].Name;
            selectedRecordName = (String) sobjectList[0].get('Name');
          }
          else {
          	selectedRecordName = (String) sobjectList[0].get('Name');
          }
        }
      }
  	}
  	return null;
  }
}