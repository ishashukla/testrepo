/**================================================================      
 * Appirio, Inc
 * Name: MonthlyForecastTriggerHandler 
 * Description: Trigger Handle class for MonthlyForecast Trigger
 * Created Date: 20 Jan,2016
 * Created By: Sahil Batra (Appirio)
 * 
 * Date Modified      Modified By   Description of the update
   20 Jan 2016     Sahil Batra       Original(T-463913)
   09 Feb 2016	   Sahil Batra		 Modified(S-374034)
   15 Feb 2016	   Sahil Batra		 Modified(T-476380)
   02 June 2015    Prakarsh Jain     Modified(T-507904)
   03 June 2016    Sahil Batra       Modified(T-507904)
   June 3,2016      Sahil Batra      Modified (T-508219) Added method updateReocrds to Handle Manager/Director/VP Forecast Updation.
   22 Sep 2016	   Sahil Batraa		 Modified(T-539766) - Updated method updateManagerForecastMonthlyRecord and associated method to inlucde Director Level changes on Forecast for Manual Adjustment
==================================================================*/
public class MonthlyForecastTriggerHandler {
	public static Id recordTypeRep;
	public static Id recordTypeManager;
	public static Map<Id,String> userIDtoManagerTitleMap = new Map<Id,String>();
	static{
		    // getting record types
    		List<RecordType> recordTypeList = new List<RecordType>();
    		recordTypeList = [SELECT SobjectType, Id, DeveloperName 
    				  		  FROM RecordType  
    				  		  WHERE SobjectType = 'Forecast_Year__c'];
   		 	Map<String,Id> recordTypeDeveloperNametoID = new Map<String,Id>();
    		if(recordTypeList.size() > 0){
    			for(RecordType rt : recordTypeList){
    				recordTypeDeveloperNametoID.put(rt.DeveloperName,rt.Id);
    			}
    		}
    if(recordTypeDeveloperNametoID.containsKey('Manager_Forecast')){
    	recordTypeManager = recordTypeDeveloperNametoID.get('Manager_Forecast');
    }
    if(recordTypeDeveloperNametoID.containsKey('Rep_Forecast')){
    	recordTypeRep = recordTypeDeveloperNametoID.get('Rep_Forecast');
    }
    // getting record types End
	}
	public MonthlyForecastTriggerHandler(){
		
	}
  /*
  @method      : updateOpportunityOnUnlock
  @description : Method is called when Monthly forecast is unlocked
  */
	public static void updateOpportunityOnUnlock(Map<Id, Monthly_Forecast__c> mapOld ,List<Monthly_Forecast__c> forecastList){
		Set<String> userIds = new Set<String>();
		Set<String> monthYear = new Set<String>();
		Set<String> BUSet = new Set<String>();
		Map<String,String> montlyForecastIdtoMonthString = new Map<String,String>();
		List<Opportunity> opportunitytoUpdate = new List<Opportunity>();
		Map<String,List<Opportunity>> monthToOpportunityMap = new Map<String,List<Opportunity>>();
		Map<String,List<Opportunity>> finalMap = new Map<String,List<Opportunity>>();
		for(Monthly_Forecast__c mf :forecastList){
			if(mf.Forecast_Record_Type_Id__c.length() > 15 && mf.Forecast_Record_Type_Id__c == recordTypeRep && !mf.Forecast_Submitted__c && (mapOld.get(mf.Id).Forecast_Submitted__c != mf.Forecast_Submitted__c)){
				String monthYearString =mf.Month__c+'-'+mf.Year__c;
				userIds.add(mf.Forecast_Owner__c);
				monthYear.add(monthYearString);
				BUSet.add(mf.Business_Unit__c);
				String keyString = mf.Forecast_Owner__c+'-'+monthYearString+'-'+mf.Business_Unit__c;
				montlyForecastIdtoMonthString.put(mf.Id,keyString);
			}
		}
		System.debug('>>>>>1'+montlyForecastIdtoMonthString);
		if(montlyForecastIdtoMonthString.size() > 0){
			List<Opportunity> oppList = new List<Opportunity>();
			oppList = [SELECT Name, Business_Unit__c,Is_Deleted__c,ForecastCategoryName,Submitted_for_Forecast__c, OwnerId, CloseDate, Id, Close_Date_Month_Year__c, Amount 
                       FROM Opportunity
                       WHERE OwnerId IN :userIds 
                       AND Close_Date_Month_Year__c IN :monthYear
                       AND ForecastCategoryName!='Omitted'
                       AND Is_Deleted__c!=true
                       AND Business_Unit__c IN:BUSet];
            for(Opportunity opp : oppList){
            	String userIdString = opp.OwnerId;
            	String keyString = userIdString+'-'+opp.Close_Date_Month_Year__c+'-'+opp.Business_Unit__c;
            	 if(!monthToOpportunityMap.containsKey(keyString)){
            	 	monthToOpportunityMap.put(keyString,new List<Opportunity>());
            	 }
            	 monthToOpportunityMap.get(keyString).add(opp);
            }
		}
		System.debug('>>>>>2'+monthToOpportunityMap);
		if(monthToOpportunityMap.size() > 0){
			for(String Ids : montlyForecastIdtoMonthString.keySet()){
				String keyVal = montlyForecastIdtoMonthString.get(Ids);
				if(monthToOpportunityMap.containsKey(keyVal)){
					finalMap.put(Ids,monthToOpportunityMap.get(keyVal));
				}
			}
		}
		System.debug('>>>>>3'+finalMap);
		if(finalMap.size() > 0){
			for(String Ids : finalMap.keySet()){
				List<Opportunity> oppList = new List<Opportunity>();
				oppList = finalMap.get(Ids);
				for(Opportunity opp : oppList){
					if(opp.Submitted_for_Forecast__c){
						opp.Submitted_for_Forecast__c = false;
						opportunitytoUpdate.add(opp);
					}
				}
			}
			if(opportunitytoUpdate.size() > 0){
				update opportunitytoUpdate;
			}
		}
	}
	/*
  	@method      : updateManagerForecastMonthlyRecord
  	@description :Method added to populate Manager forecast values on updation - (S-374034)
   */
	public static void updateManagerForecastMonthlyRecord(Map<Id, Monthly_Forecast__c> oldMap ,List<Monthly_Forecast__c> forecastList){
		Set<Id> forecastYearIds = new Set<Id>();
		Set<String> yearSet = new Set<String>();
		Set<Id> allManagers = new Set<Id>();
		Set<String> salesRepBUSet = new Set<String>();
		Map<String, String> salesRepToManager = new Map<String, String>();
		Map<String, String> salesManagerToDirector = new Map<String, String>();
		Map<String, String> DirectorToVP = new Map<String, String>();
		Map<Id, Set<Id>> managerToDirectorMap = new Map<Id, Set<Id>>();
		Set<Id> allSalesRepSet = new Set<Id>(); 
		for(Monthly_Forecast__c mf :forecastList){
			if(oldMap!=null && (oldMap.get(mf.Id).Base_Forecast__c != mf.Base_Forecast__c
							||  oldMap.get(mf.Id).Historical_Service__c != mf.Historical_Service__c
							||  oldMap.get(mf.Id).Capital_Forecast__c != mf.Capital_Forecast__c
							||  oldMap.get(mf.Id).Upside__c != mf.Upside__c
							||  oldMap.get(mf.Id).Quota__c != mf.Quota__c
							||  oldMap.get(mf.Id).Total_Sales__c != mf.Total_Sales__c
							||  oldMap.get(mf.Id).Trail_Forecast_Total__c != mf.Trail_Forecast_Total__c
							||  oldMap.get(mf.Id).Incrememntal_Service_Forecast__c != mf.Incrememntal_Service_Forecast__c
							||  oldMap.get(mf.Id).Manual_Adjustments__c != mf.Manual_Adjustments__c
							||  oldMap.get(mf.Id).Director_Manual_Adjustment__c != mf.Director_Manual_Adjustment__c
							||  oldMap.get(mf.Id).Bill_Only_Forecast__c != mf.Bill_Only_Forecast__c)){
			   if(mf.Forecast_Record_Type_Id__c.length() > 15 && (mf.Forecast_Record_Type_Id__c == recordTypeRep)){
					 yearSet.add(mf.Year__c);
					 //T-507904 changes start	
					 salesRepBUSet.add(mf.Forecast_Owner__c+'+'+mf.Business_Unit__c);
					 allSalesRepSet.add(mf.Forecast_Owner__c);
				 }
			}
	 	}
	 	System.debug('>>>>>>>>>>>>911'+salesRepBUSet);
	 	System.debug('>>>>>>>>>>>>912'+allSalesRepSet);
		salesRepToManager = DoubleBaggerUtility.getManagerandBUforUser(salesRepBUSet);
		System.debug('>>>>>>>>>>>>913'+salesRepToManager);
		Set<Id> allManagerIDs = new Set<Id>();
		for(String userIdBu : salesRepToManager.values()){
			List<String> managerIdAndBu = new List<String>();
            managerIdAndBu = userIdBu.split('\\+');
            if(managerIdAndBu.get(0) != null && managerIdAndBu.get(0) != '' &&  managerIdAndBu.get(0) != 'null'){
              allManagerIDs.add(managerIdAndBu.get(0));
            }
		}
		System.debug('>>>>>>>>>>>>914'+allManagerIDs);
		salesManagerToDirector = DoubleBaggerUtility.getDirectorOrVPFromManager(allManagerIDs);
		Set<Id> allDirectorIDs = new Set<Id>();
		for(String userIdBu : salesManagerToDirector.values()){
			List<String> directorIdAndBu = new List<String>();
            directorIdAndBu = userIdBu.split('\\+');
            if(directorIdAndBu.get(0) != null && directorIdAndBu.get(0) != '' && directorIdAndBu.get(0) != 'null'){
              allDirectorIDs.add(directorIdAndBu.get(0));
            }
			
		}
		System.debug('>>>>>>>>>>>>915'+salesManagerToDirector);
		System.debug('>>>>>>>>>>>>916'+allDirectorIDs);
		DirectorToVP = DoubleBaggerUtility.getDirectorOrVPFromManager(allDirectorIDs);
		Set<Id> allVPIds = new Set<Id>();
		for(String userIdBu : DirectorToVP.values()){
			List<String> vpIdAndBu = new List<String>();
            vpIdAndBu = userIdBu.split('\\+');
            if(vpIdAndBu.get(0) != null && vpIdAndBu.get(0) != '' && vpIdAndBu.get(0) != 'null'){
              allVPIds.add(vpIdAndBu.get(0));
            }
			
		}
						
		System.debug('>>>>>>>>>>>>916'+DirectorToVP);
		System.debug('>>>>>>>>>>>>917'+allVPIds);
	try{
		updateReocrds(yearSet,salesRepToManager,salesManagerToDirector,DirectorToVP);
	}catch(Exception ex){
       ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(),'MonthlyForecastTriggerHandler', true);
	}
	}
	/*
  	@method      : updateReocrds
  	@description :Method used in forecast updation from Trigger handler and ManagerForecsatController
   */
   //T-508219 Changes Start
	public static void updateReocrds(Set<String> yearSet,Map<String, String> salesRepToManager,Map<String, String> salesManagerToDirector,Map<String, String> DirectorToVP){
		Set<Id> allRep = new Set<Id>();
		Set<Id> allManager = new Set<Id>();
		Set<Id> allDirector = new Set<Id>();
		Set<Id> allVP = new Set<Id>();
		Set<Id> allUsers = new Set<Id>();
		Set<Id> idSetofRepManagerDirector = new Set<Id>();
		List<Monthly_Forecast__c> listToUpdate = new List<Monthly_Forecast__c>();
		if(salesRepToManager.size() > 0){
			for(String userIdBu : salesRepToManager.values()){
				List<String> managerIdAndBu = new List<String>();
				managerIdAndBu = userIdBu.split('\\+');
				if(managerIdAndBu.get(0) != null && managerIdAndBu.get(0) != '' &&  managerIdAndBu.get(0) != 'null'){
					allManager.add(managerIdAndBu.get(0));
				}
			//	idSetofRepManagerDirector.addAll(allManager);
			}
		}
		if(allManager.size() > 0){
			allRep = getUserReportee(allManager);
			//idSetofRepManagerDirector.addAll(allRep);
			
		}
		if(salesManagerToDirector.size() > 0){
			for(String userIdBu : salesManagerToDirector.values()){
				List<String> directorIdAndBu = new List<String>();
				directorIdAndBu = userIdBu.split('\\+');
			if(directorIdAndBu.get(0) != null && directorIdAndBu.get(0) != '' && directorIdAndBu.get(0) != 'null'){
				allDirector.add(directorIdAndBu.get(0));
			}
			//	idSetofRepManagerDirector.addAll(allDirector);
			}
		}
		if(DirectorToVP.size() > 0){
			for(String userIdBu : DirectorToVP.values()){
				List<String> vpIdAndBu = new List<String>();
				vpIdAndBu = userIdBu.split('\\+');
			if(vpIdAndBu.get(0) != null && vpIdAndBu.get(0) != '' && vpIdAndBu.get(0) != 'null'){
				allVP.add(vpIdAndBu.get(0));
			}
			//	idSetofRepManagerDirector.addAll(allVP);
			}
		}
		System.debug('>>>>>>>>>>>>918'+allRep);
		System.debug('>>>>>>>>>>>>919'+allManager);
		System.debug('>>>>>>>>>>>>920'+allDirector);
		System.debug('>>>>>>>>>>>>921'+allVP);
		Map<Id,Set<Id>> managerToRep = new Map<Id,Set<Id>>();
		if(allManager.size() > 0){
			managerToRep = DoubleBaggerUtility.getRepforManagerFromMancode(allManager);
		}
		Map<Id,Set<Id>> directorToManager = new Map<Id,Set<Id>>();
		if(allDirector.size() > 0){
			directorToManager = DoubleBaggerUtility.getRepforManagerFromMancode(allDirector);
		}
		Map<Id,Set<Id>> vpToDirector = new Map<Id,Set<Id>>();
		if(allVP.size() > 0){
			vpToDirector = DoubleBaggerUtility.getRepforManagerFromMancode(allVP);
		}
		for(Id user : managerToRep.keySet()){
			Set<Id> reporteeSet = managerToRep.get(user);
			idSetofRepManagerDirector.add(user);
			idSetofRepManagerDirector.addAll(reporteeSet);
		}
		for(Id user : directorToManager.keySet()){
			Set<Id> reporteeSet = directorToManager.get(user);
			idSetofRepManagerDirector.add(user);
			idSetofRepManagerDirector.addAll(reporteeSet);
		}
		for(Id user : vpToDirector.keySet()){
			Set<Id> reporteeSet = vpToDirector.get(user);
			idSetofRepManagerDirector.add(user);
			idSetofRepManagerDirector.addAll(reporteeSet);
		}
		System.debug('>>>>>>>>>>>>1111'+managerToRep);
		System.debug('>>>>>>>>>>>>1111'+directorToManager);
		System.debug('>>>>>>>>>>>>1111'+vpToDirector);
		System.debug('>>>>>>>>>>>>922'+idSetofRepManagerDirector);
		// Update all values to 0 , if no Reportee for manager is present.
		if(idSetofRepManagerDirector.size() == 0){
			if(salesRepToManager.containsKey('Manager1')){
				if(allManager.size() == 1 && managerToRep.size() == 0){
					for(Monthly_Forecast__c rec : [SELECT  Id, Trail_Forecast_Total__c,Incrememntal_Service_Forecast__c, Forecast_Owner__c, Forecast_Record_Type_Id__c,
																						  Forecast_Year__r.User2__c,Forecast_Year__c,Year__c,Total_Sales__c,Business_Unit__c,
																						  Quota__c,Manual_Adjustments__c,Director_Manual_Adjustment__c,Upside__c,Capital_Forecast__c, Month__c, Base_Forecast__c,Historical_Service__c,Forecast_Resubmitted__c,Bill_Only_Forecast__c
											  											  FROM Monthly_Forecast__c 
											  											  WHERE Forecast_Year__r.User2__c IN :allManager
											  											  AND Year__c IN :yearSet]){
						rec.Base_Forecast__c = 0;
				 		rec.Capital_Forecast__c = 0;
						rec.Quota__c = 0;
				 		rec.Total_Sales__c = 0;
				 		rec.Trail_Forecast_Total__c = 0;
				 		rec.Bill_Only_Forecast__c = 0;
				 		rec.Upside__c = 0;
				 		rec.Incrememntal_Service_Forecast__c = 0;
				 		rec.Historical_Service__c = 0;
				 		listToUpdate.add(rec);		  											  	
					}
				}
			}
		}
		// Get all records for monthly foreacast.
		if(idSetofRepManagerDirector.size() > 0){
			userIDtoManagerTitleMap = new Map<Id,String>();
			for(Mancode__c mancode : [SELECT Id,Type__c,Sales_Rep__c,Manager__c 
                          			  FROM Mancode__c 
                          			  WHERE Sales_Rep__c IN:idSetofRepManagerDirector
                          			  AND Type__c != null]){
                          					userIDtoManagerTitleMap.put(mancode.Sales_Rep__c,mancode.Type__c);  	
                          			  }
		}
		if(idSetofRepManagerDirector.size() > 0){
			List<Monthly_Forecast__c> monthlyForecastList = new List<Monthly_Forecast__c>([SELECT  Id, Trail_Forecast_Total__c,Incrememntal_Service_Forecast__c, Forecast_Owner__c, Forecast_Record_Type_Id__c,
																						  Forecast_Year__r.User2__c,Forecast_Year__c,Year__c,Total_Sales__c,Business_Unit__c,
																						  Quota__c,Manual_Adjustments__c,Director_Manual_Adjustment__c,Upside__c,Capital_Forecast__c, Month__c, Base_Forecast__c,Historical_Service__c,Forecast_Resubmitted__c,Bill_Only_Forecast__c
											  											  FROM Monthly_Forecast__c 
											  											  WHERE Forecast_Year__r.User2__c IN :idSetofRepManagerDirector
											  											  AND Year__c IN :yearSet]);
			Map<Id,List<Monthly_Forecast__c>> userIdToMonthlyForecast = new Map<Id,List<Monthly_Forecast__c>>();
			for(Monthly_Forecast__c monthlyForecast : monthlyForecastList ) {
				if(!userIdToMonthlyForecast.containsKey(monthlyForecast.Forecast_Year__r.User2__c)) {
					userIdToMonthlyForecast.put(monthlyForecast.Forecast_Year__r.User2__c,new List<Monthly_Forecast__c>());
				}
				userIdToMonthlyForecast.get(monthlyForecast.Forecast_Year__r.User2__c).add(monthlyForecast);
			}
			System.debug('>>>>>>>>>>>>923'+userIdToMonthlyForecast);
			Map<String,Monthly_Forecast__c> managerToMonthlyForecast = new Map<String,Monthly_Forecast__c>();
			Map<String,Monthly_Forecast__c> directorToMonthlyForecast = new Map<String,Monthly_Forecast__c>();
			Map<String,Monthly_Forecast__c> VPToMonthlyForecast = new Map<String,Monthly_Forecast__c>();
			if(managerToRep.size() > 0){
				managerToMonthlyForecast = getUpdatedManagerDirectorOrVPrecords(userIdToMonthlyForecast,managerToRep);
				if(managerToMonthlyForecast.size() > 0){
					userIdToMonthlyForecast = updateMap(userIdToMonthlyForecast,managerToMonthlyForecast);
				}
			}
			if(directorToManager.size() > 0){
				directorToMonthlyForecast = getUpdatedManagerDirectorOrVPrecords(userIdToMonthlyForecast,directorToManager);
				if(directorToMonthlyForecast.size() > 0){
					userIdToMonthlyForecast = updateMap(userIdToMonthlyForecast,directorToMonthlyForecast);
				}
			}
			if(vpToDirector.size() > 0){
				VPToMonthlyForecast = getUpdatedManagerDirectorOrVPrecords(userIdToMonthlyForecast,vpToDirector);
				if(VPToMonthlyForecast.size() > 0){
					userIdToMonthlyForecast = updateMap(userIdToMonthlyForecast,VPToMonthlyForecast);
				}
			}
			System.debug('>>>>>>>>>>>>924'+userIdToMonthlyForecast);
			if(allManager.size() > 0){listToUpdate.addAll(getListtoUpdate(userIdToMonthlyForecast,allManager));}
			if(allDirector.size() > 0){listToUpdate.addAll(getListtoUpdate(userIdToMonthlyForecast,allDirector));}
			if(allVP.size() > 0){listToUpdate.addAll(getListtoUpdate(userIdToMonthlyForecast,allVP));}	
		}
		System.debug('>>>>>>>>>>>>924'+listToUpdate);	
		if(listToUpdate.size() > 0){
		try{
			update listToUpdate;
			Constants.runUpdateTrigger = false;
		}catch(Exception ex){
       		ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(),'MonthlyForecastTriggerHandler', true);
		}
		}	
	}
	//T-508219 Changes Start
	// T-507904 Changes start
	/*
  	@method      : getUserReportee
  	@description :Method used in forecast updation  to get user Reportee
   */
	public static Set<Id> getUserReportee(Set<Id> userIDset){
    	Map<Id,set<Id>> userToReporteeMap = new Map<Id,set<Id>>();
    	Set<Id> tempuserToReporteeSet = new Set<Id>();
    	userToReporteeMap = DoubleBaggerUtility.getRepforManagerFromMancode(userIDset);
    	if(userToReporteeMap.size() > 0){
    		for(Set<Id> userSet : userToReporteeMap.values()){
    			tempuserToReporteeSet.addAll(userSet);
    		}
    	}
    		return tempuserToReporteeSet;
    }
    // T-507904 Changes End
   /*
  	@method      : getUpdatedManagerDirectorOrVPrecords
  	@description :Method used in forecast updation
   */
    public static Map<String,Monthly_Forecast__c> getUpdatedManagerDirectorOrVPrecords(Map<Id,List<Monthly_Forecast__c>> userIdToMonthlyForecast,Map<Id,Set<Id>> managerToReportee){
    	Map<String,Monthly_Forecast__c> managerToMonthlyForecast = new Map<String,Monthly_Forecast__c>();
		Map<String,Monthly_Forecast__c> repToMonthlyForecast = new Map<String,Monthly_Forecast__c>();
		for(Id managerId : managerToReportee.keySet()) {
			String BusinessUnit = '';
			List<Monthly_Forecast__c> managerMonthlyForecastList = new List<Monthly_Forecast__c>();
			System.debug('>>>>>>>>>>>>9244'+managerId);
			if(userIdToMonthlyForecast.containsKey(managerId)){
				managerMonthlyForecastList = userIdToMonthlyForecast.get(managerId);
			System.debug('>>>>>>>>>>>>9255'+managerMonthlyForecastList);
			
			for(Monthly_Forecast__c monthlyForecastRecord : managerMonthlyForecastList) {
				String key = monthlyForecastRecord.Forecast_Year__r.User2__c+'+'+monthlyForecastRecord.Business_Unit__c+'+'+monthlyForecastRecord.Month__c+'+'+monthlyForecastRecord.Year__c;
				BusinessUnit = monthlyForecastRecord.Business_Unit__c;
				if(!managerToMonthlyForecast.containsKey(key)) {
					managerToMonthlyForecast.put(key,monthlyForecastRecord);
				}
			}
			Set<Id> reportees = managerToReportee.get(managerId);
			for(Id reporteeId : reportees) {
				List<Monthly_Forecast__c> repRecordList = new List<Monthly_Forecast__c>();
				if(userIdToMonthlyForecast.containsKey(reporteeId)){
				repRecordList = userIdToMonthlyForecast.get(reporteeId);
				for(Monthly_Forecast__c rec : repRecordList){
					if(rec.Business_Unit__c == BusinessUnit){
						String key = managerId+'+'+BusinessUnit+'+'+rec.Month__c+'+'+rec.Year__c+'+'+rec.Forecast_Year__r.User2__c;
						repToMonthlyForecast.put(key,rec);
					}
				}
				}
			}
		for(String managerKey : managerToMonthlyForecast.keySet()){
				 decimal manualAdjustment = 0;
				 decimal directorManualAdjustment = 0;
				 String key = managerKey;
				 Monthly_Forecast__c rec = new Monthly_Forecast__c();
				 rec = managerToMonthlyForecast.get(key);
				 System.debug('>>>>>>>>>>>>925'+userIdToMonthlyForecast);
				 System.debug('>>>>>>>>>>>>926'+key);
				 System.debug('>>>>>>>>>>>>927'+rec);
				 if(reportees.size() > 0){
				 	rec.Historical_Service__c = 0;
				 	rec.Base_Forecast__c = 0;
				 	rec.Capital_Forecast__c = 0;
					rec.Quota__c = 0;
				 	rec.Total_Sales__c = 0;
				 	rec.Trail_Forecast_Total__c = 0;
				 	rec.Bill_Only_Forecast__c = 0;
				 	rec.Upside__c = 0;
				 	rec.Incrememntal_Service_Forecast__c = 0;
				 }
				 System.debug('>>>>>>>>>>9999'+reportees);
				 for(Id reporteeId : reportees) {
				 	key = managerKey +'+'+reporteeId;
				 	System.debug('>>>>>>>>>>9991'+key);
				 	if(repToMonthlyForecast.containsKey(key)){
				 		System.debug('>>>>>>>>>>9992'+repToMonthlyForecast.get(key));
						Monthly_Forecast__c tempmf = repToMonthlyForecast.get(key);
						rec.Base_Forecast__c = rec.Base_Forecast__c + getValue(tempmf.Base_Forecast__c);
						rec.Historical_Service__c = rec.Historical_Service__c + getValue(tempmf.Historical_Service__c);
						rec.Capital_Forecast__c = rec.Capital_Forecast__c + getValue(tempmf.Capital_Forecast__c);
						rec.Upside__c = rec.Upside__c + getValue(tempmf.Upside__c);
						rec.Quota__c = rec.Quota__c + getValue(tempmf.Quota__c);
						rec.Total_Sales__c = rec.Total_Sales__c + getValue(tempmf.Total_Sales__c);
						rec.Trail_Forecast_Total__c = rec.Trail_Forecast_Total__c + getValue(tempmf.Trail_Forecast_Total__c);
						rec.Incrememntal_Service_Forecast__c = rec.Incrememntal_Service_Forecast__c + getValue(tempmf.Incrememntal_Service_Forecast__c);
						rec.Bill_Only_Forecast__c = rec.Bill_Only_Forecast__c + getValue(tempmf.Bill_Only_Forecast__c);
						if(rec.Forecast_Record_Type_Id__c != recordTypeRep){
							System.debug('@@@@@@@@@@>>>>>'+tempmf.Manual_Adjustments__c);
							manualAdjustment = manualAdjustment + getValue(tempmf.Manual_Adjustments__c);
							directorManualAdjustment = directorManualAdjustment + getValue(tempmf.Director_Manual_Adjustment__c);
							System.debug('11@@@@@@@@@@>>>>>'+manualAdjustment);
						}
				 	}
				 }
				 System.debug('222@@@@@@@@@@>>>>>'+manualAdjustment);
				 if(manualAdjustment > 0 || manualAdjustment < 0){
				 	rec.Manual_Adjustments__c = manualAdjustment;
				 }
				 else{
				 	if(userIDtoManagerTitleMap.containsKey(rec.Forecast_Year__r.User2__c)){
				 		if((userIDtoManagerTitleMap.get(rec.Forecast_Year__r.User2__c) == 'Director' || userIDtoManagerTitleMap.get(rec.Forecast_Year__r.User2__c) == 'VP') && manualAdjustment == 0){
				 			rec.Manual_Adjustments__c = 0;
				 		}
				 	}else if(rec.Manual_Adjustments__c == null){
				 		rec.Manual_Adjustments__c = 0;
				 	}
				 }
				 if(directorManualAdjustment > 0 || directorManualAdjustment < 0){
				 	rec.Director_Manual_Adjustment__c = directorManualAdjustment;
				 }
				 else{
				 	if(userIDtoManagerTitleMap.containsKey(rec.Forecast_Year__r.User2__c)){
				 		if(userIDtoManagerTitleMap.get(rec.Forecast_Year__r.User2__c) == 'VP' && directorManualAdjustment == 0){
				 			rec.Director_Manual_Adjustment__c = 0;
				 		}
				 	}
				 else if(rec.Director_Manual_Adjustment__c == null){
				 		rec.Director_Manual_Adjustment__c = 0;
				 	}
				 }
				 
				 System.debug('>>>>>>>>>>9993'+rec);
				managerToMonthlyForecast.put(managerKey,rec);
		}
		}
		}	
		return managerToMonthlyForecast;
    }
    /*
  	@method      : updateMap
  	@description :Method used in forecast updation
   */
    public static Map<Id,List<Monthly_Forecast__c>> updateMap(Map<Id,List<Monthly_Forecast__c>> userIdToMonthlyForecast,Map<String,Monthly_Forecast__c> IDtoMonthlyForecastRecord){
    	Map<Id,List<Monthly_Forecast__c>> tempMap = new Map<Id,List<Monthly_Forecast__c>>();
		for(String key : IDtoMonthlyForecastRecord.keySet()){
			Id managerId = Id.valueOf(key.split('\\+').get(0));
			if(!tempMap.containsKey(managerId)){
				tempMap.put(managerId,new List<Monthly_Forecast__c>());
			}
			tempMap.get(managerId).add(IDtoMonthlyForecastRecord.get(key));
		}
		if(tempMap.size() > 0){
			for(Id key : tempMap.keySet()){
				if(userIdToMonthlyForecast.containsKey(key)){
					userIdToMonthlyForecast.put(key,new List<Monthly_Forecast__c>());
					userIdToMonthlyForecast.get(key).addAll(tempMap.get(key));
				}
				
			}
		}
		return userIdToMonthlyForecast;
    }
    public static List<Monthly_Forecast__c> getListtoUpdate(Map<Id,List<Monthly_Forecast__c>> userIdToMonthlyForecast,Set<id> userIDs){
    	List<Monthly_Forecast__c> tempList = new List<Monthly_Forecast__c>();
    	for(Id user : userIDs){
    		if(userIdToMonthlyForecast.containsKey(user)){
    			tempList.addAll(userIdToMonthlyForecast.get(user));
    		}
    	}
    	return tempList;
    }
    /*
  	@method      : getValue
  	@description :Method used in forecast updation and return 0 in case of null
   */
    public static Decimal getValue(Decimal val){
    	if(val == null){
    		return 0;
    	}
    	else{
    		return val;
    	}
    }
}