// (c) 2016 Appirio, Inc.
//
// Show action notes data for associated account
//
// 24 Feb 2016    Deepti Maheshwari Reference  T-477245
// 5th Apr 2016   Kirti Agarwal      (Reference I-210229)
public with sharing class COMM_ActionNotesOnAccountController
{
    public Account acct{get;set;}
    public List<ActionNoteWrapper> notesWrapper;
    public String newNoteUrl{get;set;}
    public String sysAdmin{get{
      return Constants.ADMIN_PROFILE;
    }set;}
    
    
    //Constructor of extension controller
    public COMM_ActionNotesOnAccountController(ApexPages.StandardController stdController) {
        
        this.acct = null;
        for(Account theRecord : [Select Name, ID, Type, OwnerID 
                    FROM Account 
                    WHERE ID =: stdController.getRecord().ID]) {
          this.acct = theRecord;
        }
        if(this.acct != null) {
          newNoteUrl = '/' + Schema.SObjectType.Action_Note__c.getKeyPrefix() + '/e?CF';    
          newNoteUrl += Label.Action_Note_AccountID + '=' + EncodingUtil.urlEncode(acct.name, 'UTF-8');
          newNoteUrl += '&CF' + Label.Action_Note_AccountID + '_lkid=';
          newNoteUrl += acct.id + '&retURL=%2F' + acct.id;
          newNoteUrl += '&saveURL=%2F' + acct.id;
          system.debug('New Note url : ' + newNoteUrl);
        }
        notesWrapper = new List<ActionNoteWrapper>();
    }

    // Method to get notes on associated account
    // @param Nothing
    // @return List<Action_Note__c> 
    public List<ActionNoteWrapper> getNotesWrapper() {
      if(this.acct == null) {
        return notesWrapper;
      }
        Map<ID, String> heirarchichalAccountIDs = new Map<ID, String>();
        List<Account> accounts = [SELECT ID, Type, ParentID, Parent.Type,  
                                 Parent.ParentID, Parent.Parent.Type,
                                 Parent.Parent.ParentID, Parent.Parent.Parent.Type, 
                                 Parent.Parent.Parent.ParentID, Parent.Parent.Parent.Parent.Type,
                                 Parent.Parent.Parent.Parent.ParentID, 
                                 Parent.Parent.Parent.Parent.Parent.Type 
                                 FROM Account 
                                 WHERE ID =:acct.id];
    if(accounts != null && accounts.size() > 0) {
        for(Account acc : accounts) {
            heirarchichalAccountIDs.put(acc.id, acc.Type);
            if(acc.ParentID != null) {
                heirarchichalAccountIDs.put(acc.ParentID, acc.Parent.Type);
            }
            if(acc.Parent.ParentID != null) {
                heirarchichalAccountIDs.put(acc.Parent.ParentId, acc.Parent.Parent.Type);
            }
            if(acc.Parent.Parent.ParentID != null) {
                heirarchichalAccountIDs.put(acc.Parent.Parent.ParentId, 
                                                  acc.Parent.Parent.Parent.Type);
            }
            if(acc.Parent.Parent.Parent.ParentID != null) {
                heirarchichalAccountIDs.put(acc.Parent.Parent.Parent.ParentID, 
                                          acc.Parent.Parent.Parent.Parent.Type);
            }
            if(acc.Parent.Parent.Parent.Parent.ParentID != null) {
                heirarchichalAccountIDs.put(acc.Parent.Parent.Parent.Parent.ParentID,
                                   acc.Parent.Parent.Parent.Parent.Parent.Type);
            }
            }
            
            if(heirarchichalAccountIDs != null && heirarchichalAccountIDs.size() > 0){
            //I-210229 - Updated Order by parameters in query
            for(Action_Note__c notes : [SELECT AccountName__r.name, Name,Notes_short__c, 
                                        Type__c, AccountName__r.ID, ID,createdBy.name, CreatedDate 
                                        FROM Action_Note__c 
                                        WHERE AccountName__c in :heirarchichalAccountIDs.keyset() 
                                        Order By LastModifiedDate desc, AccountName__r.Type asc]){
                notesWrapper.add(new ActionNoteWrapper(notes, heirarchichalAccountIDs.get(notes.AccountName__r.ID)));         
            }
        }
    }
    return notesWrapper;
  }
  
  // Wrapper Class for Action Note display    
    public Class ActionNoteWrapper{
        public Action_Note__c note{get;set;}
        public String Type{get;set;}
        
        public ActionNoteWrapper(Action_Note__c note, String Type){
            this.note = note;
            this.Type = Type;
        }
  }    
}