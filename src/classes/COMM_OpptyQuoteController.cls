/**================================================================      
* Appirio, Inc
* Name: COMM_OpptyQuoteController  
* Description: I-216827 (To show only one record of the quote with same name in an inline VF page)
* Created Date: 05/05/2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* June               Rahul Aeran      I-217262 (To show primary quotes first)
* 5th August 2016    Nitish Bansal    I-227047 - To resolve Inline VF page error in LEX
* 22nd August 2016   Nitish Bansal    I-229603 (Set primary quote as per pacakged code methods) 
* 28th October 2016  Ralf hoffmann    I-242119 added Order_OE_NUmber to query
* 7th November 2016   Nitish Bansal   I-242913 - To sort all the records, not just the current page records.
* 1st December 2016   Nitish Bansal   I-242624
==================================================================*/

global class COMM_OpptyQuoteController {

    public Boolean isQuoteSet {get;set;}    //NB - 08/22- I-229603
    public Opportunity opportunityRecord{get;set;} 
    public Id selectedID{get;set;}
    //public String newDocumentURL{get;set;} //NB - 12/01/2016 - I-242624
    //public COMM_Conga_Setting__c congaSetting{get;set;} //NB - 12/01/2016 - I-242624
    public List<BigMachines__Quote__c> quoteListToShow{get;set;}
    public List<BigMachines__Quote__c> quoteList{get;set;}
    public String siteId {get;set;}
    public String sortField{get;set;}
    public String order{get;set;}
    public Boolean isAsc{get;set;}
    String previousSortField{get;set;}

    public Integer listSize {get;set;}
    public Integer pageSize{get;set;}
    public Integer pageNumber{get;set;}

    public COMM_OpptyQuoteController(ApexPages.StandardController std) {
    //I-227047 - Nb - 08/05 - Start
        isQuoteSet = false;
        opportunityRecord = (Opportunity)std.getRecord(); 
        pageSize = (Integer)COMM_Constant_Setting__c.getOrgDefaults().Quote_Document_Pagesize__c;
        pageNumber = 1;     
        if(opportunityRecord != null && opportunityRecord.Id != null){
            quoteList = getQuoteList();
            getQuoteWrapper(getQuoteListToShow());
            isAsc = true;
            previousSortField = sortField =
                'Name';
            siteId = BigMachines.BigMachinesFunctionLibrary.getOnlyActiveSiteId();
        } else {
            quoteList = new List<BigMachines__Quote__c>();
            quoteListToShow = new List<BigMachines__Quote__c>();
            listSize = 0;
        }
    //I-227047 - Nb - 08/05 - Start    
    }

    public List<BigMachines__Quote__c> getQuoteList(){
        quoteList = new List<BigMachines__Quote__c>();
        quoteListToShow = new List<BigMachines__Quote__c>();
        //I-242119 - rh 10/28 added Order_OE_Number and BigMachines__Total_Amount__c to query;
        String query = 'SELECT BigMachines__Opportunity__c, Id, Name, Revision_Number__c, BigMachines__Description__c,'
                        + ' BigMachines__Status__c, BigMachines__Total__c, BigMachines__Is_Primary__c, Budgetary_Quote__c, '
                        + ' Unverified__c, SAP_Order_Number__c, Order_OE_Number__c, BigMachines__Total_Amount__c, Final_Production_Configuration__c'
                        + ', LastModifiedBy.Name ' //NB - 12/01/2016 - I-242624
                        + ' FROM BigMachines__Quote__c '
                        + ' WHERE ';
        query += ' BigMachines__Opportunity__c = \'' + opportunityRecord.id + '\'';
        //Primary order by added by Rahul Aeran I-217262
        query += ' ORDER BY BigMachines__Is_Primary__c desc, Revision_Number__c DESC';
         
        List<BigMachines__Quote__c> doclist = getOnlyOneRecordPerName(query);
        if(doclist != null){
          listSize = docList.size();
        }
        return doclist;
    }

    public List<BigMachines__Quote__c> getOnlyOneRecordPerName(String query){
        List<BigMachines__Quote__c> sameNameQuotes, tempQuoteList, finalQuoteList;
        Map<String, List<BigMachines__Quote__c>> sameNametoQuotesMap = new Map<String, List<BigMachines__Quote__c>>();
        for(BigMachines__Quote__c qt : Database.query(query)){
            if(!sameNametoQuotesMap.containsKey(qt.Name)){
                sameNameQuotes = new List<BigMachines__Quote__c>();
                sameNameQuotes.add(qt);
                sameNametoQuotesMap.put(qt.Name, sameNameQuotes);
            } else {
                sameNameQuotes = new List<BigMachines__Quote__c>();
                sameNameQuotes.addAll(sameNametoQuotesMap.get(qt.Name));
                sameNameQuotes.add(qt);
                sameNametoQuotesMap.put(qt.Name, sameNameQuotes);
            }
        }

        finalQuoteList = new List<BigMachines__Quote__c>();

        for(String quoteName : sameNametoQuotesMap.keySet()){
            tempQuoteList = new List<BigMachines__Quote__c>();
            sameNameQuotes = new List<BigMachines__Quote__c>();
            for(BigMachines__Quote__c qot : sameNametoQuotesMap.get(quoteName)){
                tempQuoteList.add(qot);
                if(qot.BigMachines__Is_Primary__c){
                    sameNameQuotes.add(qot);
                    finalQuoteList.add(qot);
                    break;
                }
            }
            if(sameNameQuotes.size() != 1){
                finalQuoteList.add(tempQuoteList.get(0));
            }
        }
        return finalQuoteList;
    }

    //----------------------------------------------------------------------------
      // Name : doDelete() Method to delete selected opportunityRecord
      // Params : None
      // Returns : list of quotes
      //----------------------------------------------------------------------------
      public void doDelete(){
        List<BigMachines__Quote__c> docList = [SELECT Id 
                                      FROM BigMachines__Quote__c 
                                      WHERE ID = :selectedID];
          if(docList != null && docList.size() > 0){
              delete docList;
          }
          quoteList = getQuoteList();
          pageNumber = 1;
          getQuoteWrapper(getQuoteListToShow());
      }

    //Pagination methods
  
      public Boolean hasPrevious{get{
          if(PageNumber == 1){
              return false;
          }else{
              return true;
          }
      }set;}
      
      public Boolean hasNext{get{
          if(PageNumber == 1){
              if((listSize / pageSize  > 1)
              || (((listSize / pageSize ) == 1)
              && (math.mod(listSize, pageSize) != 0))){
                  //system.debug('Return FROM Here');
                  return true;
              }else{
                  //system.debug('Return FROM tHere');
                  return false;
              }
          }else{
              if((pageNumber > (listSize / pageSize))
              || (pageNumber == (listSize / pageSize)
              && (math.mod(listSize, pageSize) == 0))){
                  //system.debug('Return FROM Hereee');
                  return false;
              }else{
                  //system.debug('Return FROM Here1');
                  return true;
              }
          }
      }set;}
      
      public void next() 
      {
          pageNumber++;
          //system.debug('Page Number : ' + pageNumber);
          getQuoteWrapper(getQuoteListToShow());
      }
      
      public void previous() 
      {
          pageNumber--;
          getQuoteWrapper(getQuoteListToShow());
      }
      
      public void first() 
      {
          pageNumber = 1;
          getQuoteWrapper(getQuoteListToShow());
      }
      
      
      public void Last() 
      {
        if(Math.mod(listSize,pageSize) == 0){
          pageNumber = listSize / pageSize;
        }else{
          if(listSize / pageSize >= 1){
            pageNumber = listSize / pageSize + 1;
          }else{
            pageNumber = 1;
          }
        }
        
        getQuoteWrapper(getQuoteListToShow());
      }  

    public List<BigMachines__Quote__c> getQuoteListToShow() {
        quoteListToShow = new List<BigMachines__Quote__c>();
        if(quoteList != null && quoteList.size() > 0){
          if(pageNumber == 1){
            //system.debug('I am in second If Page 1');
            if(quoteList.size() < pagesize){
              for(Integer i=0; i<quoteList.size();i++){
                  quoteListToShow.add(quoteList.get(i));
              }
            }else{
              for(Integer i=0; i<pagesize;i++){
                  quoteListToShow.add(quoteList.get(i));
              }
            }
            //system.debug('in if');
          }else{
            if((pageNumber-1) == (quoteList.size() / pageSize)){
              //system.debug('I am in second If');
              for(Integer i= ((pageNumber * pageSize) - pageSize); i < quoteList.size(); i++){
                      quoteListToShow.add(quoteList.get(i));
              }
            }else{
              //system.debug('I am in second If else');
              for(Integer i= ((pageNumber * pageSize) - pageSize); i<(pageNumber * pageSize);i++){
                  quoteListToShow.add(quoteList.get(i));
              }
            }
          }
        }
        return quoteListToShow;
    }  

    public void sortList(){
        //system.debug('Sort Field : ' + sortField + ':::order::' + order + 'previousSortField' + previousSortField);
        
        isAsc = previousSortField.equals(sortField)? !isAsc : true; 
        previousSortField = sortField;
        order = isAsc ? 'ASC' : 'DESC';
        List<BigMachines__Quote__c> resultList = new List<BigMachines__Quote__c>();
        //Create a map that can be used for sorting 
        Map<object, List<BigMachines__Quote__c>> objectMap = new Map<object, List<BigMachines__Quote__c>>();
        //for(BigMachines__Quote__c ob : quoteListToShow){ //NB - 11/07 - I-242913
        for(BigMachines__Quote__c ob : quoteList){  //NB - 11/07 - I-242913
          if(sortField == 'LastModifiedBy'){
            if(objectMap.get(ob.LastModifiedBy.Name) == null){
              objectMap.put(ob.LastModifiedBy.Name, new List<Sobject>()); 
            }
            objectMap.get(ob.LastModifiedBy.Name).add(ob);
          }else{
            if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
                objectMap.put(ob.get(sortField), new List<Sobject>()); 
            }
            objectMap.get(ob.get(sortField)).add(ob);
          }
        }       
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();
        for(object key : keys){ 
          resultList.addAll(objectMap.get(key)); 
        }
        //Apply the sorted values to the source list
        //quoteListToShow.clear(); //NB - 11/07 - I-242913
        quoteList.clear(); //NB - 11/07 - I-242913
        if(order.toLowerCase() == 'asc'){
        //  quoteListToShow.addAll(resultList); //NB - 11/07 - I-242913
          quoteList.addAll(resultList); //NB - 11/07 - I-242913
        }else if(order.toLowerCase() == 'desc'){
          for(integer i = resultList.size()-1; i >= 0; i--){
          //  quoteListToShow.add(resultList[i]);  //NB - 11/07 - I-242913
            quoteList.add(resultList[i]); //NB - 11/07 - I-242913
          }
        }
        quoteWrapperList = null;
        
        //getQuoteWrapper(quoteListToShow); //NB - 11/07 - I-242913
        getQuoteWrapper(getQuoteListToShow()); //NB - 11/07 - I-242913
    }
    
    //-------------------------------NB - 06/27 - I-224056 Start - To allow user to set primary quote----------------------
    
    public List<QuoteWrapper> quoteWrapperList {get;set;}
    public Boolean isSingleQuoteSelected{get;set;}
    public Boolean showSection {get;set;}
    public BigMachines__Quote__c selectedQuote {get;set;}
    
    //wrapper class so that input checkbox can be shown on the vf page
    public class QuoteWrapper {
        public Boolean isSelected {get;set;}
        public BigMachines__Quote__c quote{get;set;}
        
        public QuoteWrapper(BigMachines__Quote__c oracleQuote){
            isSelected = false;
            quote = oracleQuote;
        }
    }
    
    //populating the wrapper list to display quotes on the page 
    public List<QuoteWrapper> getQuoteWrapper (List<BigMachines__Quote__c> quoteList){
        quoteWrapperList = new List<QuoteWrapper>();
    
        for(BigMachines__Quote__c oracleQuote : quoteList){
            //system.debug('Page Number : ' + quoteList);
            quoteWrapperList.add(new QuoteWrapper(oracleQuote));
        }
        return quoteWrapperList;
    }
    
    //to perform action on selected quote
    public PageReference processSelected (){
        
        List<BigMachines__Quote__c> selectedQuoteList = new List<BigMachines__Quote__c>();
        showSection = true;
        for(QuoteWrapper wrap : quoteWrapperList){
            if(wrap.isSelected){
                selectedQuoteList.add(wrap.quote);
            }
        }
        //checking if single quote is selected or not
        if(selectedQuoteList.size() == 1){
        //allowing user to set the single selected quote as primary
            isSingleQuoteSelected = true;
            selectedQuote = selectedQuoteList[0];
        } else {
            isSingleQuoteSelected = false;
        }
        return null;
    }
    
    //updating the quotes such that selected quote is set as primary and original primary is reset as non-primary
    public PageReference setQuote(){
    //NB - 08/22- I-229603 - Start
       BigMachines__Quote__c tempQuote = new BigMachines__Quote__c(); 
       
       List<BigMachines__Quote__c> updateQuoteList = new List<BigMachines__Quote__c>();
       for(QuoteWrapper wrap : quoteWrapperList){
            if(wrap.quote.Id == selectedQuote.Id){
                tempQuote = wrap.quote;
                wrap.quote.BigMachines__Is_Primary__c = true;
                updateQuoteList.add(wrap.quote);
            } else if(wrap.quote.Id != selectedQuote.Id && wrap.quote.BigMachines__Is_Primary__c){
                wrap.quote.BigMachines__Is_Primary__c = false;
                updateQuoteList.add(wrap.quote);
            }
        }
        if(!Test.isRunningTest()){ //NB - 12/01 - I-242624
          BigMachines.BigMachinesFunctionLibrary.setQuoteAsPrimary(tempQuote.Id);  
        }
        
        if(updateQuoteList.size() > 0){
         //  update updateQuoteList;
           //resetting the flags and lists
           showSection = false;
           isSingleQuoteSelected  = false;
           isQuoteSet = true;
           for(QuoteWrapper wrap : quoteWrapperList){
                if(wrap.isSelected){
                   wrap.isSelected = false;
                }
           }
           selectedQuote = null;
        }
        return null;
     //NB - 08/22- I-229603 - End
    }
    
    //----------------------------------------NB - 06/27 - I-224056 End----------------------------------------------------
}