/**================================================================      
* Appirio, Inc
* Name: UpdateDeleteSATAndAccountShareBatchTest 
* Description: Test class for UpdateDeleteSATAndAccountShareBatch class
* Created Date: 19 Aug,2016
* Created By: Isha Shukla (Appirio)
* 
* Date Modified      Modified By      Description of the update
* 19 Aug 2016        Isha Shukla         Class creation.T-528109
==================================================================*/
@isTest
private class UpdateDeleteSATAndAccountShareBatchTest {
    static User adminUser;
    static List<Account> accountList;
    static List<AccountTeamMember> atmList;
    static Id accountInstrumentsRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type').getRecordTypeId();
    @isTest static void testFirst() {
        createData();
        List<Custom_Account_Team__c> customatmList = TestUtils.createCustomAccountTeam(1, accountList[0].Id, adminUser.Id, 'IVS', false, true);
        Test.startTest();
        try {
            Database.executeBatch(new UpdateDeleteSATAndAccountShareBatch());
        } catch(Exception e) {
        }
        Test.stopTest();
        System.assertEquals(customatmList[0].Account__c,atmList[0].AccountId);
    }
    @isTest static void testSecond() {
        createData();
        List<Custom_Account_Team__c> customatmList = TestUtils.createCustomAccountTeam(1, accountList[1].Id, adminUser.Id, 'IVS', false, true);
        Test.startTest();
        try {
            Database.executeBatch(new UpdateDeleteSATAndAccountShareBatch());
        } catch(Exception e) {
        }
        Test.stopTest();
        System.assertEquals(customatmList[0].Account__c != atmList[0].AccountId,True);
    }
    public static void createData() {
        adminUser = TestUtils.createUser(1, 'System Administrator', True).get(0);
        System.runAs(adminUser) {
            accountList = TestUtils.createAccount(2, false);
            accountList[0].RecordTypeId = accountInstrumentsRecTypeId;
            insert accountList;
            atmList = TestUtils.createAccountTeamMember(1, accountList[0].Id, adminUser.Id, false);
            atmList[0].TeamMemberRole = 'IVS';
            insert atmList;
        }
    }
}