/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_OrderIntermediateExtensions
//
// 27 Nov 2015 , Megha (Appirio JDC)
//
// Populate Ship To And Bill To Accounts.
// 25 Jan 2016, Sunil (Billing Type Changes.)
*/
public without sharing class Medical_OrderIntermediateExtensions{

  // Public variables used on VF page.
  public String billToAccount_Name{get;set;}
  public String billToAccount_Id{get;set;}

  public String shipToAccount_Name{get;set;}
  public String shipToAccount_Id{get;set;}

  public String selectedAccId {get;set;}
  public String fieldName {get;set;}
    public Order newOrder{get;set;}

  // Private variables
    private Contact currentContact{get;set;}
  private Map<String, String> urlParameters;
    private String caseId;


    //-----------------------------------------------------------------------------------------------
  //  Cosntructor
  //-----------------------------------------------------------------------------------------------
  public Medical_OrderIntermediateExtensions(){

    newOrder = new Order();
    urlParameters = ApexPages.currentPage().getParameters();
    String contactId = ApexPages.currentPage().getParameters().get('contact_id');
    caseId = ApexPages.currentPage().getParameters().get('case_id');

    // When Order is created from Case
    if(String.isBlank(caseId) == false){
        //Updated by Shubham for the Story S-436452
        for(Case cs : [SELECT Contact.Id, Contact.Name, Contact.AccountId, Contact.Account.Name, Contact.Account.Billing_Type__c, Contact.Account.Bill_To_Account__c, Contact.Account.Bill_To_Account__r.Name FROM Case WHERE Id =: caseId] ){
        currentContact = cs.Contact;
        }
    }

    // When Order is created from Contact
    if(String.isBlank(contactId) == false){
        //Updated by Shubham for the Story S-436452
        for(Contact con : [SELECT Id, Name, AccountId, Contact.Account__c, Account.Name, Account.Billing_Type__c, Contact.Account.Bill_To_Account__c, Contact.Account.Bill_To_Account__r.Name FROM Contact WHERE Id = : contactId]){
        currentContact = con;
        }
    }

    // Set Order's Account fields
    populateAccountsFields();

    System.debug('@@@newOrder' + newOrder);
  }

  //-----------------------------------------------------------------------------------------------
  //  Helper method populate Billing & Shipping Accounts
  //-----------------------------------------------------------------------------------------------
  private void populateAccountsFields(){

    if(currentContact.AccountId != null){
      newOrder.AccountId = currentContact.AccountId;
      newOrder.Contact__c = currentContact.Id;
    }

    if(currentContact.Account.Billing_Type__c == 'Bill To Address Only'){
        billToAccount_Name = currentContact.Account.Name;
        billToAccount_Id = currentContact.Account.Id;
    }
    else if(currentContact.Account.Billing_Type__c == 'Ship To Address Only'){
    //Added by Shubham for the Story S-436452 #Start
        if(currentContact.Account.Bill_To_Account__c != NULL){
        billToAccount_Name = currentContact.Account.Bill_To_Account__r.Name;
        billToAccount_Id = currentContact.Account.Bill_To_Account__c;
      }
    //Added by Shubham for the Story S-436452 #End
      shipToAccount_Name = currentContact.Account.Name;
      shipToAccount_Id = currentContact.Account.Id;
    //Added by Akanksha for story S-418411
      if(currentContact.Account.Bill_To_Account__c != null)
      {
          billToAccount_Name = currentContact.Account.Bill_To_Account__r.name;
          billToAccount_Id = currentContact.Account.Bill_To_Account__c;
      }
      //End by Akanksha for story S-418411
    }
  	else if(currentContact.Account.Billing_Type__c == 'Bill To and Ship To Address'){
      shipToAccount_Name = billToAccount_Name = currentContact.Account.Name;
      shipToAccount_Id = billToAccount_Id = currentContact.Account.Id;

      //Added by Shubham for the Story S-436452 #Start
      if(currentContact.Account.Bill_To_Account__c != NULL){
        billToAccount_Name = currentContact.Account.Bill_To_Account__r.Name;
        billToAccount_Id = currentContact.Account.Bill_To_Account__c;
      }
      else{
        billToAccount_Name = currentContact.Account.Name;
        billToAccount_Id = currentContact.Account.Id;
      }
      //Added by Shubham for the Story S-436452 #End
      //Updated by Shubham for the Story S-436452 #Start
      shipToAccount_Name = currentContact.Account.Name;
      shipToAccount_Id = currentContact.Account.Id;
     //Updated by Shubham for the Story S-436452 #End
     if(currentContact.Account.Bill_To_Account__c != null)
      {
          billToAccount_Name = currentContact.Account.Bill_To_Account__r.name;
          billToAccount_Id = currentContact.Account.Bill_To_Account__c;
      }
      //End by Akanksha for story S-418411
    }
  }

  //-----------------------------------------------------------------------------------------------
  //  Page Reference method called from on load of Visualforce
  //-----------------------------------------------------------------------------------------------
  public PageReference redirectPage(){
    return null;
  }


  //-----------------------------------------------------------------------------------------------
  //  Visualforce action method when Account is selected from custom lookup
  //-----------------------------------------------------------------------------------------------
  public PageReference getSelectedRecord(){
    System.debug('###' + selectedAccId);
    if(String.isBlank(selectedAccId) == false){
        List<Account> lstAccounts = [SELECT Id, Name, AccountNumber,Bill_To_Account__r.name,Bill_To_Account__c,Billing_Type__c FROM Account WHERE ID = : selectedAccId];//Added the Bill to account by Akanksha for story S-418411
        if(lstAccounts.size() == 0){
            return null;
        }

        System.debug('###' + lstAccounts.get(0).Name);

        if(fieldName == 'Bill_To_Account__c'){
        billToAccount_Id = lstAccounts.get(0).Id;
        billToAccount_Name = lstAccounts.get(0).Name;
      }

        else if(fieldName == 'Ship_To_Account__c'){
        shipToAccount_Id = lstAccounts.get(0).Id;
        shipToAccount_Name = lstAccounts.get(0).Name;

        //Added by Akanksha for story S-418411
          if(lstAccounts.get(0).Bill_To_Account__c != null)
          {
              billToAccount_Name = lstAccounts.get(0).Bill_To_Account__r.name;
              billToAccount_Id = lstAccounts.get(0).Bill_To_Account__c;
          }
          else if(lstAccounts.get(0).Billing_Type__c == 'Bill To and Ship To Address' && lstAccounts.get(0).Bill_To_Account__c == null)
          {
              billToAccount_Name = lstAccounts.get(0).Name;
              billToAccount_Id = lstAccounts.get(0).id;
          }
          
        //End by Akanksha for story S-418411
        }
    }
    return null;
  }



  //-----------------------------------------------------------------------------------------------
  //  Page Reference method called from VF page next button.
  //-----------------------------------------------------------------------------------------------
  public PageReference next(){

    // If User remove the Account names from text box then update Id field to null
    if(String.isBlank(billToAccount_Name) == true){
      billToAccount_Name = billToAccount_Id = null;
    }
    if(String.isBlank(shipToAccount_Name) == true){
      shipToAccount_Name = shipToAccount_Id = null;
    }

    // Validate if Account name is not selected from Lookup field (User just typed in text box without selecting from lookup)
    if(String.isBlank(billToAccount_Name) != true && String.isBlank(billToAccount_Id) == true){
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Bill To Account from lookup window.');
      ApexPages.addMessage(myMsg);
      return null;
    }
    if(String.isBlank(shipToAccount_Name) != true && String.isBlank(shipToAccount_Id) == true){
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Ship To Account from lookup window.');
      ApexPages.addMessage(myMsg);
      return null;
    }



    String url = '/apex/Medical_CreateOrder?';

    for(String param : urlParameters.keySet()){
      url += '&' + param + '=' + urlParameters.get(param);
    }

    if(String.isBlank(billToAccount_Id) != true && String.isBlank(billToAccount_Name) != true){
       url += '&BillTo=' + billToAccount_Id;
       url += '&BillToName=' + billToAccount_Name;
    }

    if(String.isBlank(shipToAccount_Id) != null && String.isBlank(shipToAccount_Name) != true){
      url += '&ShipTo=' + shipToAccount_Id;
      url += '&ShipToName=' + shipToAccount_Name;
    }

    return new PageReference(url);
  }


  //-----------------------------------------------------------------------------------------------
  //  Cancel action method called from visualforce
  //-----------------------------------------------------------------------------------------------
  public PageReference cancel(){
    String recordId = caseId != null ? caseId :(currentContact != null ? currentContact.id : 'home/home.jsp');
    return new PageReference('/' + recordId);
  }

}