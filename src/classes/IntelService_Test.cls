/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 07, 2013
 Purpose       :  Test Class for IntelService.cls
 Reference     :  Task T-205140
*******************************************************************************/
@isTest
private class IntelService_Test {

    static testMethod void myUnitTest() {

    //Create test records
    List<User> userList = TestUtils.createUser(2, null, true);
    Intel__c intel1;

    System.runAs(userList.get(0)){
      List<Account> accList = TestUtils.createAccount(1, true);
      List<Intel__c> intelList = TestUtils.createIntel(3, true);
      List<Tag__c> tagList = TestUtils.createTag(2, intelList.get(0).Id, false);
      tagList.get(0).Account__c = accList.get(0).Id;
      tagList.get(1).Account__c = accList.get(0).Id;
      insert tagList;

      intelList = [SELECT Id, Chatter_Post__c FROM Intel__c WHERE Id IN :intelList];
      intel1 = intelList.get(0);

      List<FeedComment> commentList = TestUtils.createComment(2, intelList.get(0).Chatter_Post__c, true);
      FeedLike lik = new FeedLike(FeedItemId = intelList.get(0).Chatter_Post__c);
      insert lik;
    }

    List<Intel__c> intelList ;
    System.runAs(userList.get(1)){
      intelList = TestUtils.createIntel(1, true);
      intelList = [SELECT Id, Chatter_Post__c FROM Intel__c WHERE Id IN :intelList];
      FeedComment comment1 = TestUtils.createComment(1, intelList.get(0).Chatter_Post__c, true).get(0);
      FeedComment comment2 = TestUtils.createComment(1, intel1.Chatter_Post__c, true).get(0);
    }

    test.startTest();

    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/IntelService/*';
    req.httpMethod = 'GET';
    req.addParameter('batchOffset', '1');
    req.addParameter('searchTerm', 'T');
    req.addParameter('userId', userList.get(0).Id);
   // req.addParameter('lastSync', String.valueOf(datetime.now()));
    RestContext.request = req;
    RestContext.response = res;

    IntelListWrapper result1 = IntelService.doGet();
    System.assertNotEquals(null, result1.intelList);
    System.assertNotEquals(0, result1.intelList.size());

    req = new RestRequest();
    res = new RestResponse();

    req.requestURI = '/services/apexrest/IntelService/*';
    req.httpMethod = 'GET';
    req.addParameter('userId', userList.get(1).Id);
    RestContext.request = req;
    RestContext.response = res;

    IntelListWrapper result2 = IntelService.doGet();
    System.assertNotEquals(null, result2.intelList);
    System.assertNotEquals(0, result2.intelList.size());
    
    req = new RestRequest();
    res = new RestResponse();

    req.requestURI = '/services/apexrest/IntelService/*';
    req.httpMethod = 'GET';
    req.addParameter('intelId', intel1.Id);
    RestContext.request = req;
    RestContext.response = res;

    IntelListWrapper result3 = IntelService.doGet();
    System.assertNotEquals(null, result3.intelList);
    System.assertNotEquals(0, result3.intelList.size());

    test.stopTest();
  }
}