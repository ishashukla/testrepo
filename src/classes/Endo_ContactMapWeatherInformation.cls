/*******************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 10, 2014 
 Purpose       :  This will be used for showing map, weather and local time of contact.
*******************************************************************************/
public with sharing class Endo_ContactMapWeatherInformation {
	public Contact con{get;set;}
    public Endo_Address myaddress{get; set;}
    public String error{get; set;}
    public Endo_ContactMapWeatherInformation(ApexPages.StandardController stdController){
        if(!Test.isRunningTest()){ // for test classes - addFields are not working with test classes
            stdController.addFields(contactFields());
        }
        con = (Contact)stdController.getRecord();
        myaddress = new Endo_Address();
        myaddress.city = con.MailingCity;
        myaddress.state = con.MailingState;
        myaddress.country = con.MailingCountry;
        myaddress.countryCode = con.MailingCountryCode;
        myaddress.zipCode = con.MailingPostalCode;
        myaddress.street = con.MailingStreet;
        if(myaddress.zipCode == null && myaddress.city == null && myaddress.state == null && myaddress.country == null && myaddress.countryCode == null){
        	error = 'Cannot display location information for this Customer Contact: No Mailing Address is listed.';
        }         
    }
    //To get account fields
    @TestVisible
    private List<String> contactFields(){
        List<string> fields = new List<String>();
        SObjectType objToken = Schema.getGlobalDescribe().get('Contact');
        Map<String, SObjectField> fieldsDesc = objToken.getDescribe().fields.getMap();
        for(String f : fieldsDesc.keySet()){
            fields.add(f);
        }   
        return fields;
    }
}