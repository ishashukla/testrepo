public class DDMController {
    public String generatedURL{get;set;}
    public String ddmurl {get;set;}
    public String ddmsalt {get;set;}
    public String ddmemail{get;set;}
    public String ddmsecretKey {get;set;}
    public String ddmgeneratedHash {get;set;}
    public boolean isMultiRep{get;set;}
    public Map<String,DDM_Configuration__c> BUtoDDMRecord{get;set;}
    public Map<String,String> BUtoURL{get;set;}
    public Map<String,String> BUtoURLHash{get;set;}
    public DDMController(){
    	BUtoURLHash = new Map<String,String>();
    	generatedURL = '';
    	List<String> buList = new List<String>();
    	BUtoURL = new Map<String,String>();
    	BUtoDDMRecord = new Map<String,DDM_Configuration__c>();
    	isMultiRep = true;
    	User currentUser = [SELECT Id,email,Division FROM User WHERE Id =:UserInfo.getUserId()];
    	ddmemail = currentUser.email;
    	ddmsalt  = String.valueOf(DateTime.now().getTime());
    	if(currentUser.Division!=null){
    		buList = currentUser.Division.split(';');
    	}
    	List<DDM_Configuration__c> ddmConfig = DDM_Configuration__c.getall().values();
    	system.debug('>>>>>>'+ddmConfig);
    	for(DDM_Configuration__c ddm : ddmConfig){
    		BUtoDDMRecord.put(ddm.Name,ddm);
    	}
    	system.debug('>>>>>>1'+BUtoDDMRecord);
    	system.debug('>>>>>>2'+buList);
    	for(String bu : buList){
    		if(BUtoDDMRecord.containsKey(bu)){
    			DDM_Configuration__c ddm = BUtoDDMRecord.get(bu);
    			String url = ddm.URL__c;
    			String secretKey = ddm.Shared_Key__c;
    			String tempString = ddmemail+':'+ddmsalt+':'+secretKey;
    			System.debug('>>>>>>>3'+tempString);
    			Blob blobData = Blob.valueOf(tempString);
    			Blob hash = Crypto.generateDigest('SHA1',blobData);
    			String generatedHash = EncodingUtil.convertToHex(hash);
    			String generatedDDMURL = url+'?email='+ddmemail+'&n='+ddmsalt+'&k='+generatedHash;
    			BUtoURL.put(bu,generatedDDMURL);
    			BUtoURLHash.put(bu,generatedHash);
    		}
    	
    	}
    	if(BUtoURLHash.size() == 1){
    		for(String bu : BUtoURLHash.keySet()){
    			ddmgeneratedHash = BUtoURLHash.get(bu);
    			ddmurl = BUtoDDMRecord.get(bu).URL__c;
    			isMultiRep = false;
    		}
    	}
    }
}