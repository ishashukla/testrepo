/*******************************************************************************
 Author        :  Appirio JDC (Sunil Gupta)
 Date          :  May 07, 2014
 Purpose       :  Test class for Endo_LinkCasesWithOrders
*******************************************************************************/
global class Endo_LinkCasesWithOrdersSchedular implements Schedulable {
  
  // Call a batch class
  global void execute(SchedulableContext sc) {
  	Endo_LinkCasesWithOrders objBatch = new Endo_LinkCasesWithOrders();
  	Database.executeBatch(objBatch);
  }
}