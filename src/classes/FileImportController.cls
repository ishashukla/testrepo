/************************************************************************
Name  : FileImportController
Author: Dirk Koechner (Appirio)
Date  : April, 2015
Description:    Single click file import tool.   Can be embedded in any VF page or linked to custom button/link.
                File format expected: text with delimiter (tab, comma, etc.)
                Default mapping: File name defines object, column headers define fields.
                Compound " .. " fields supported.
Date        Update By       Description                             Ref-Id
07-07-2015  Naresh K Shiw   Update upload() & parseCSV()            I-170626
13-07-2015  Naresh K Shiw   Added close()			            	I-171477
*************************************************************************/

public with sharing class FileImportController {

  static final String fileDelimiter = ',';      // File delimiter set to tab, change this as needed

  Public Flex_Credit__c currentRecord {get;set;}

  public FileImportController(ApexPages.StandardController sc) {
  	currentRecord = (Flex_Credit__c)sc.getRecord();
  }

  public Attachment attachment {                // attachment not required, defined here for demo purposes
  get {
      if (attachment == null)
        attachment = new Attachment();
      return attachment;
    }
  set;
  }

  public PageReference close() {
  	return new Pagereference('/' + currentRecord.Id);
  }

  public PageReference upload() {

    List<List<String>> csvOutput;
    Schema.SObjectType sObjType;
    Map<String, Schema.SObjectField> mapObjFields;
    List<SObject> listsObject;

    try {

        // Option 1: set object name based on file name
        //system.debug('attachment.name:'+attachment.name);
        //List<String> filename = new List<String>();
        //filename = attachment.Name.split('[.]');
        //String sObjectName = filename[0].trim();
        String sObjectName = 'Flex_Credit__c';

        //system.debug('sObjectName:' + sObjectName);

        // Option 2: hard code object name, define as needed
        //String sObjectName = 'Student_Loan_Disbursement__c';
        System.debug('attachment.body.toString()==='+attachment.body.toString());
        csvOutput = parseCSV( (String) attachment.body.toString() );

        System.debug('Number of Rows in File:' + csvOutput.size());

        if (csvOutput.size() > 10000) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error importing file. Row limit exceeds 10,000 rows. Break into smaller files.'));
            return null;
        }
        else if (csvOutput.size() < 2) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error importing file. No rows to insert. First row must be field names.'));
            return null;
        }

        sObjType = Schema.getGlobalDescribe().get(sObjectName);
        system.debug('Schema.getGlobalDescribe().size():'+Schema.getGlobalDescribe().size());
        if(sObjType == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error importing file. Can not identify Object: '+sObjectName));
            return null;
        }
        mapObjFields = sObjType.getDescribe().fields.getMap();

        listsObject = new List<SObject>();

    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error importing file. Incorrect file name or format. ' + e.getMessage()));
      return null;
    }

    integer fieldpos;
    integer rowpos;
    try {

        rowpos = 0;
        for( List<String> listRow : csvOutput) {
            if (rowpos > 0) {
                system.debug('listrow:'+listrow);
                //Sobject importObject = sObjType.newSobject();
                Flex_Credit__c importObject = new Flex_Credit__c();
                fieldpos = 0;
                for ( String field : listRow) {
                    // Dynamically populate sObject fieldname, value
                    String fieldType = String.valueOf(mapObjFields.get(csvOutput[0][fieldpos]).getDescribe().getType());
                    system.debug('Processing Column:' + csvOutput[0][fieldpos] + '  Type:' + fieldType +'   Value:' + field );
                    // Note: not all types included, expand as needed
                    // Naresh Added PERCENT type and Updated logic for DATE type    I-170626
                    if (fieldType == 'String' || fieldType == 'TextArea' || fieldType == 'Phone' ||  fieldType == 'Picklist'){
                        if(csvOutput[0][fieldpos] == 'Salesforce_com_ID__c'){
                            importObject.put('Id', (String)field );
                            fieldpos++;
                            continue;
                        } else
                            importObject.put(csvOutput[0][fieldpos], (String)field );
                    }
                    else if (fieldType == 'Decimal' || fieldTYpe == 'Double' || fieldType == 'Currency' || fieldType == 'PERCENT' )
                        importObject.put(csvOutput[0][fieldpos], Decimal.valueOf(field) );
                    else if (fieldType == 'Integer')
                        importObject.put(csvOutput[0][fieldpos], Integer.valueOf(field) );
                    else if (fieldType == 'Boolean')
                        importObject.put(csvOutput[0][fieldpos], Boolean.valueOf(field) );
                    else if (fieldType == 'Date'){
                        String[] strDate    = field.split('[-/]');
                        Integer myIntMonth  = integer.valueOf(strDate[0]);
                        Integer myIntDate   = integer.valueOf(strDate[1]);
                        Integer myIntYear   = integer.valueOf(strDate[2]);
                        Date dateField      = Date.newInstance(myIntYear, myIntMonth, myIntDate);
                        importObject.put(csvOutput[0][fieldpos], dateField );
                    }
                    else {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error importing file. Undefined field data type:'+fieldType));
                        return null;
                    }

                    fieldpos++;
                }
                listsObject.add(importObject);
            }
            rowpos++;
        }
        system.debug('listsObject==========='+listsObject);
        update listsObject;

        // OPTIONAL FUNCTIONALITY: insert import file as an (archival) attachment
        //attachment.OwnerId = UserInfo.getUserId();
        //attachment.ParentId = 'a0M17000000HNEw'; // the record the file should attached to
        //attachment.IsPrivate = true;
        //insert attachment;

    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error importing file. Incorrect file column header or data field. Column:'+(fieldpos+1)+' Row:'+rowpos+'.  '+ e.getMessage()));
      return null;
    }

    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File imported successfully. '+ (csvOutput.size() - 1) +' records uploaded.'));
    return null;
}

// parse CSV file, return list of lists of strings
//  - treat all content inside quotes (" , tab , etc ") as a single token

public static List<List<String>> parseCSV(String contents) {
	      System.debug('contents======='+contents);
        List<List<String>> allFields = new List<List<String>>();

        List<String> lines = new List<String>();
        // Naresh Added [\r\n] in place of \n   I-170626
        try {
            lines = contents.split('[\r\n]');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
            return allFields;
        }
        Integer num = 0;
        for(String line : lines) {
            System.debug('line======='+line);
            line = line.replaceAll('\r','').replaceAll('\n','').trim();

            system.debug('Loading row:'+line);
            if(line == ''){
                continue;
            }
            // Option: set token blow to file delimiter (comma, tab, etc.)
            List<String> fields = line.split(fileDelimiter);
            List<String> listFieldRow = new List<String>();
            String compositeField = '';

            for(String field : fields) {
                //system.debug('Processing current field >' + field + '<');
                if (field.endsWith('"')) {
                    //listFieldRow.add( 'COl HAD END QUOTE' );
                    compositeField += field;
                    listFieldRow.add(compositeField);
                    compositeField = '';
                }
                else if ( field.startsWith('"') || compositeField != '') {
                    //listFieldRow.add( 'COl HAD START QUOTE' );
                    compositeField += field;
                }
                else
                    listFieldRow.add(field);
            }

            allFields.add(listFieldRow);
        }

        return allFields;
    }

}