/*
//
// (c) 2011 Appirio, Inc.
//
// Apex Controller for Visualforce page Medical_OrderProducts
//
// This page is used to dispaly inline VF page on Order Record.
//
*/
public class Medical_OrderProductsExtensions {
  public Map<Decimal, OrderProductWrapper> orderProducts {get;set;}

  public String orderId {get;set;}
  public Boolean isProductFound {
    get{
      if(orderProducts != null && orderProducts.size() > 0){
        return true;
      }
      return false;
    }
    set;
  }

  // Constructor
  public Medical_OrderProductsExtensions(ApexPages.StandardController stdController){
    orderId = ApexPages.currentPage().getParameters().get('id');
    if(orderId != null){
      loadOrderProducts();
    }
  }


  // Helper method to load data
  private void loadOrderProducts(){
  	orderProducts = new Map<Decimal , OrderProductWrapper>();
    for(OrderItem oi : [SELECT id , Part_Number__c, Part_Number__r.Name,OrderId, OrderItemNumber, Order.Original_Order_Number__c,Order.Type ,Promised_Ship_Date__c,
                        Order.PoNumber, Order_Date__c, Line_Number__c, Description, Order.OrderNumber, Quantity,Quantity__c,ListPrice,UnitPrice, Request_Date__c, Promised_Delivery_Date__c,
                        Tracking_Number__c, Order_Line_Status__c, PricebookEntry.ProductCode
                        FROM OrderItem where OrderId = : orderId
                        AND Line_Number__c != null order by Line_Number__c]){

      Decimal lineNumKey = Decimal.valueOf(Integer.valueOf(oi.Line_Number__c));
      Decimal decimalNum = (oi.Line_Number__c - lineNumKey) * (10.00).pow(oi.Line_Number__c.scale());

      if(decimalNum > 0){
      	if(orderProducts.containsKey(lineNumKey)){
          orderProducts.get(lineNumKey).childOrderProducts.add(oi);
        }
        else{
        	orderProducts.put(oi.Line_Number__c, new OrderProductWrapper(oi));
        }
      }
      else{
        orderProducts.put(lineNumKey, new OrderProductWrapper(oi));
      }
    }
  }


  // Wrapper Class.
  public class OrderProductWrapper {
    public OrderItem oItem {get;set;}
    public List<OrderItem> childOrderProducts {get;set;}

    public OrderProductWrapper(){
      this.childOrderProducts = new List<OrderItem>();
    }

    public OrderProductWrapper(OrderItem oItem){
      this.oItem = oItem;
      this.childOrderProducts = new List<OrderItem>();
      //this.childOrderProducts.add(oItem);
    }
  }
}