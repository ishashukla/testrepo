/*************************************************************************************************
Created By:    Sunil Gupta
Date:          Feb 13, 2014
Description  : Controller class for Endo_CalculatePrice

Modified: 03  March 2016    Connor Flynn      S-397622 Added Pricing Contract start and end dates
**************************************************************************************************/
public without sharing class Endo_CalculatePriceController { 
    // Public Varibales.
    public Account currentAccount{get;set;}
    public Pricing_Contract_Line_Item__c currentPLI{get;set;}
    public List<Endo_PriceWrapper> lstSalesPrices{get;set;}
    public List<Endo_PriceWrapper> lstListPrices{get;set;}
    public String sortDirection{get;set;}
    private Set<Id> allAssociatedContracts;
    public boolean isAnyItemIsExcluded{get;set;}
    public String accName{get;set;}
    public String accId{get;set;}
   // public String prdName{get;set;}
    public Account accountForNewCase{get;set;}
    
    //-----------------------------------------------------------------------------------------------
  // Constructor
  //-----------------------------------------------------------------------------------------------
  public Endo_CalculatePriceController(ApexPages.StandardController sc){
    this.currentAccount = (Account)sc.getRecord();
    this.currentAccount = [SELECT Id, Name,AccountNumber FROM Account WHERE Id = :currentAccount.Id].get(0);
    currentPLI = new Pricing_Contract_Line_Item__c();
    sortDirection = 'ASCENDING';
    showPriceList();
  }
  
  
  //-----------------------------------------------------------------------------------------------
  // Action method called on show price list button
  //-----------------------------------------------------------------------------------------------
  public void showPriceList(){
    lstListPrices = new List<Endo_PriceWrapper>();
    lstSalesPrices = new List<Endo_PriceWrapper>();
    
    String selectedProductId;
    String selectedMajorCode;
    String selectedPartNumber;
    
    if(currentPLI != null && currentPLI.Product__c != null){
      selectedProductId = String.escapeSingleQuotes(currentPLI.Product__c);
    }
    if(String.isBlank(selectedProductId) == false){
      List<Product2> lstProducts = [SELECT Id, Major_Code__c, Part_Number__c FROM Product2 WHERE Id = :selectedProductId];
      if(lstProducts.size() > 0 && lstProducts.get(0).Major_Code__c != null){
        selectedMajorCode = lstProducts.get(0).Major_Code__c;
      }
      
      if(lstProducts.size() > 0 && lstProducts.get(0).Part_Number__c != null){
        selectedPartNumber = lstProducts.get(0).Part_Number__c;
      }
      
    }
    
    String qryForListPrice = getQueryForListPrice(selectedProductId);
    
    System.debug('@@@' + qryForListPrice);
    List<PricebookEntry> lst1 = new List<PricebookEntry>();
    if(String.isBlank(qryForListPrice) == false){
        lst1 = Database.query(qryForListPrice);
    }
    System.debug('@@@' + lst1);
    for(PricebookEntry item :lst1){
          Endo_PriceWrapper wrp = new Endo_PriceWrapper();
          wrp.priceBookEntry = item;
          lstListPrices.add(wrp);
      }
    
    
    String qryForContractPrice = getQueryForContractPrice(selectedProductId, selectedMajorCode, selectedPartNumber);
    System.debug('@@@' + qryForContractPrice);
    
    
    List<Pricing_Contract_Line_Item__c> lst2 = new List<Pricing_Contract_Line_Item__c>();
    if(String.isBlank(qryForContractPrice) == false){
        if(String.isBlank(qryForListPrice) == false){
          lst2 = Database.query(qryForContractPrice);
        }
    }
    
    System.debug('@@@' + lst2);
    
    
    // Exclude the major code type item if any product type item is excluded for the same price contract.
    //I-107988
    //map<Id, Boolean> mapItems = new map<Id, Boolean>(); 
    //for(Pricing_Contract_Line_Item__c item :lst2){
    //  If(mapItems.containsKey(item.Pricing_Contract__c) == false){
    //    mapItems.put(item.Pricing_Contract__c, false);
    //  }
    //  if(item.Excluder__c == true){
    //    mapItems.put(item.Pricing_Contract__c, true);
    //  }
    //}
    
    
    isAnyItemIsExcluded = false;
    for(Pricing_Contract_Line_Item__c item :lst2){
        //if(item.Excluder__c == true){
        //  isAnyItemIsExcluded = true;
        //  continue;
        //}
        
        // If Type is Major Code and Excluder object has some entries for this line item.
        if(String.isBlank(item.Major_Code__c) == false){
        List<Excluder_Line_Item__c> lstExcludesItems = item.Excluder_Line_Items__r;
        System.debug('@@@' + lstExcludesItems.size());
        if(lstExcludesItems.size() > 0){
          isAnyItemIsExcluded = true;
          continue; 
        }
      } 
      
        // Exclude other item of the same price contract 
      //if(mapItems.get(item.Pricing_Contract__c) == true){
      //  if(String.isBlank(item.Major_Code__c) == false){
      //    isAnyItemIsExcluded = true;
      //    continue;
      //  } 
      //}
      
      Endo_PriceWrapper wrp = new Endo_PriceWrapper();
        //if(item.Application_Method__c.replace(' ', '').toUpperCase() == 'NEWPRICE'){
        //    wrp.priceTemp = item.Value__c;
        //}
        //else if(item.Application_Method__c == '%'){
        //    if(lstListPrices.size() > 0){
        //        Decimal lstPrice = lstListPrices.get(0).priceBookEntry.UnitPrice;
        //        Decimal discountAmount = (lstPrice * item.Value__c)/100 ;
        //        wrp.priceTemp = lstPrice - discountAmount;
        //    }
        //}
        //wrp.priceTemp = item.Sell_Price__c;
        wrp.pcli = item;
        wrp.isLowest = false;
        wrp.discount = calculateDiscount(item.Contract_Price__c);
        lstSalesPrices.add(wrp);
    }
    System.debug('@@@' + lstSalesPrices);
    
    lstSalesPrices.sort();
    if(lstSalesPrices.size() > 0){
        lstSalesPrices.get(0).isLowest = true;
    }
     
    System.debug('@@@' + lstSalesPrices);
  }
  
  
  //-----------------------------------------------------------------------------------------------
  // Helper method to Calculate Discount
  //-----------------------------------------------------------------------------------------------
  private Decimal calculateDiscount(Decimal sellPrice){
    Decimal listPrice = 0;
    if(lstListPrices.size() > 0 && lstListPrices.get(0).priceBookEntry.UnitPrice > 0){
      listPrice = lstListPrices.get(0).priceBookEntry.UnitPrice;     
    }
    
    try{
        if(listPrice > sellPrice){
            Decimal temp = listPrice - sellPrice;
            Decimal percen = (temp/listPrice) * 100;
            return percen; 
        }
    }
    catch(Exception ex){
        return 0;
    }
    return 0;
  }
  
  //-----------------------------------------------------------------------------------------------
  // Helper method to query for list price
  //-----------------------------------------------------------------------------------------------
  private String getQueryForListPrice(String selectedProductId){
    System.debug('selectedProductId in getQueryForListPrice :' + selectedProductId);
    if(String.isBlank(selectedProductId) == false){
        return 'SELECT UnitPrice, ProductCode, Product2Id, Product2.Name, Product2.Major_Code__c, Pricebook2Id, Pricebook2.Name, Pricebook2.Id, Name, IsActive, Id ' +
                 'FROM PricebookEntry WHERE Pricebook2.Name = \'Standard Price Book\' AND IsActive = true AND Product2Id = \'' + selectedProductId + '\'';
    }
    else{
      return '';
    }
  }
  
  //-----------------------------------------------------------------------------------------------
  // Helper method to query for contract price
  //-----------------------------------------------------------------------------------------------
  private String getQueryForContractPrice(String selectedProductId, String selectedMajorCode, String selectedPartNumber){
    allAssociatedContracts = new Set<Id>();
    
    //Local Contracts.
    //for(Pricing_Contract__c objPC :[SELECT Id FROM Pricing_Contract__c WHERE Account__c = :currentAccount.Id AND Is_Active__c = TRUE]){
    //  allAssociatedContracts.add(objPC.Id);
    //}
    
    //Junction object Contracts.
    for(Pricing_Contract_Account__c junction :[SELECT Pricing_Contract__c, Account__c FROM Pricing_Contract_Account__c 
                            WHERE Account__c = :currentAccount.Id 
                            //AND Contract_Type__c = 'GPO - Buying Group' 
                            //AND Pricing_Contract__r.Is_Active__c = TRUE]){
                            AND IsActive__c = TRUE]) {
      allAssociatedContracts.add(junction.Pricing_Contract__c);
    }
    
    if(allAssociatedContracts.size() < 1){
      return '';
    }
    
    System.debug('allAssociatedContracts :' + allAssociatedContracts);
    String qry = 'SELECT Id, Contract_Price__c, Name, End_Date__c, Value__c, Application_Method__c,  Is_Active__c, Related_Part_Num__c, Major_Code__c, Description__c, Part_Major_Code__c, '+
                 'Pricing_Line_Id__c, Product__c,Product__r.Name,Pricing_Contract__r.Name, Pricing_Contract__c, '+
                 'Pricing_Contract__r.Type__c, Product__r.Major_Code__c, Start_Date__c, Type__c, ' +
                 'Pricing_Contract__r.End_Date__c, Pricing_Contract__r.Start_Date__c, ' + //Connor Flynn S-397622
                 'Pricing_Contract__r.Buying_Group_Name__c,' +
                 '(SELECT Id FROM Excluder_Line_Items__r WHERE IsActive__c = true AND Product_Code__c = :selectedPartNumber) ' + 
                                   

                 'FROM Pricing_Contract_Line_Item__c WHERE Pricing_Contract__c IN :allAssociatedContracts ';
    
    
    if(String.isBlank(selectedMajorCode) == false && String.isBlank(selectedProductId) == false){
      if(qry.toUpperCase().contains('WHERE')){
        qry += 'AND (Product__c = \'' + selectedProductId + '\' OR Major_Code__c = \'' + selectedMajorCode + '\') ';
      }
      else{
        //qry += ' WHERE Major_Code__c = \'' + selectedMajorCode + '\' AND Pricing_Contract__r.AccountId = \'' + currentAccount.Id +'\'';
      }
    }
    else if(String.isBlank(selectedProductId) == false){
        if(qry.toUpperCase().contains('WHERE')){
            qry += ' AND Product__c = \'' + selectedProductId + '\'';
      }
      else{
        
      }
    }
        
    if(String.isBlank(selectedProductId) && String.isBlank(selectedMajorCode)){
        return '';
    }
    System.debug('qry in getQueryForContractPrice:' + qry);
    return qry;
  }
  
  
  //-----------------------------------------------------------------------------------------------
  // Action method to sort price List
  //-----------------------------------------------------------------------------------------------
  public void sortPriceAction(){
    if(sortDirection == 'ASCENDING'){
        sortDirection = 'DESCENDING';
    }
    else{
        sortDirection = 'ASCENDING';
    }
    Endo_PriceWrapper.SORT_DIR = sortDirection;
    System.debug('@@@' + sortDirection);
    system.debug('abc@@@' + lstSalesPrices);
    lstSalesPrices.sort();
  }
  
 //-----------------------------------------------------------------------------------------------
 // Action method to navigate the user to new case create page
 //--
 public PageReference createNewCase()
 {
    PageReference pg = new PageReference('/500/e?retURL=%2F500%2Fo&RecordType=012i0000001EEVP&ent=Case&00Ni000000EFAaT=Pricing%20Inquiry');
    this.accountForNewCase= [SELECT Id, Name,RecordTypeId FROM Account WHERE AccountNumber = :currentAccount.AccountNumber  and  Id = :currentAccount.Id].get(0);
    accName=this.accountForNewCase.Name;
    accId=this.accountForNewCase.Id;
    pg.getParameters().put('cas4',accName);
    pg.getParameters().put('cas4_lkid',accId);
    String selectedProductId;
    if(currentPLI != null && currentPLI.Product__c != null){
      selectedProductId = String.escapeSingleQuotes(currentPLI.Product__c);
    }
    if(String.isBlank(selectedProductId) == false){
      List<Product2> lstProducts = [SELECT Id, Name FROM Product2 WHERE Id = :selectedProductId];
     if(lstProducts.size() == 1 ){
     String prodId =   lstProducts.get(0).Id;
     String prodName =   lstProducts.get(0).Name;
    pg.getParameters().put('CF00Ni000000EFAaJ_lkid',prodId);
    pg.getParameters().put('CF00Ni000000EFAaJ',prodName);
     }
   }
    pg.getParameters().put('nooverride','1');// Important to avoid recursion
    return pg;
 } 
}