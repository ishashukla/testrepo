// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_BaseProductTriggerHandler
// Description: Handler Class for BaseProductTrigger.
//
// April 6 2016, Prakarsh Jain  Original 
// June 23 2016, Isha Shukla    Modified(T-513054)
// July 1  2016, Isha Shukla    Modified(T-516161)
// July 7  2016, Isha Shukla    Modified(I-225271)
public class Instruments_BaseProductTriggerHandler {
  //Method to populate product description field on Base product(Stryker Ipad Issues workbook)
  
  // This method is commented for the test class coverage as Tom Muse has commented this method call in BaseProductTrigger
 /* public static void populateProductDescriptionOnBaseProduct(List<Base_Product__c> newlist){
    Set<Id> productSet = new Set<Id>();
    Set<Id> ibSet = new Set<Id>();
    List<Product2> lstProducts = new List<Product2>(); 
    Map<Id, String> mapIdToDescription = new Map<Id, String>();
    List<Base_Product__c> updatedList = new List<Base_Product__c>();
    List<Base_Product__c> lstToBeUpdated = new List<Base_Product__c>();
    for(Base_Product__c ip : newlist){
      if(ip.Product__c != null){
        productSet.add(ip.Product__c);
        ibSet.add(ip.Id);
      }
    }
    updatedList = [SELECT Id, Name, Prod_Description__c, Product__c FROM Base_Product__c WHERE Id IN: ibSet];
    lstProducts = [SELECT Id, Name, Description FROM Product2 WHERE Id IN: productSet];
    for(Product2 pro : lstProducts){
      if(!mapIdToDescription.containsKey(pro.Id)){
        mapIdToDescription.put(pro.Id, pro.Description);
      }
    }
    for(Base_Product__c ip : updatedList){
      if(mapIdToDescription.containsKey(ip.Product__c)){
        ip.Prod_Description__c = mapIdToDescription.get(ip.Product__c);
        lstToBeUpdated.add(ip);
      }
    }
    if(lstToBeUpdated.size()>0){
      update lstToBeUpdated;
    }
  } */
  
  
  // Method to populate the Base_Product__c.Oracle_Quote__c to Base_Trail_Survey__c.Oracle_Quote__c and Opportunity.Base_Trail_Survey__c to Base_Product__c.Base_Trail_Survey__c
  public static void populateQuoteOnBaseTrailAndOppBaseTrailToBaseProductBaseTrail(List<Base_Product__c> triggerNewList) {
  	List<Base_Trail_Survey__c> baseTrailList = new List<Base_Trail_Survey__c>();
  	Set<Id> oracleQuoteIds = new Set<Id>();
  	Map<Id,Id> baseTrailIdsTobpQuote = new Map<Id,Id>();
  	// creating set of oracle quote ids related with base product
  	for(Base_Product__c baseProduct : triggerNewList) {
  		oracleQuoteIds.add(baseProduct.Oracle_Quote__c);
  	}
    Set<String> oppIds = new Set<String>();
    Map<String,String> quoteToOppMap = new Map<String,String>();
    // creating set of opportunity ids related with oracle quote 
   for( BigMachines__Quote__c quote : [SELECT Id,BigMachines__Opportunity__c 
  											FROM BigMachines__Quote__c
  											WHERE BigMachines__Is_Primary__c = true AND
  											BigMachines__Opportunity__r.Is_Deleted__c != true AND //added as per T-520740(Prakarsh Jain 19 July 2016)
  											Id IN :oracleQuoteIds] ) {
    System.debug('quote.BigMachines__Opportunity__c=='+quote.BigMachines__Opportunity__c);
    String quoteOpportunity = quote.BigMachines__Opportunity__c;
    	if(quoteOpportunity != null && quoteOpportunity != '') {
    		oppIds.add(quote.BigMachines__Opportunity__c);
    		if(!quoteToOppMap.containsKey(quote.Id)) {
    			quoteToOppMap.put(quote.Id,quote.BigMachines__Opportunity__c);
    		}
    	}
    }
    
    System.debug('oppIds=='+oppIds);
 	Map<Id,Id> oppToBaseTrailMap = new Map<Id,Id>();
 	// creating map of parent opportunity having child base trail 
 	for(Base_Trail_Survey__c bts : [SELECT Id,Opportunity__c 
                                    FROM Base_Trail_Survey__c 
                                    WHERE Opportunity__c IN :oppIds]) {
 		if(!oppToBaseTrailMap.containsKey(bts.Opportunity__c)) {
 			oppToBaseTrailMap.put(bts.Opportunity__c,bts.Id);
 		}
 	}
 	List<Base_Product__c> bpListToInsert = new List<Base_Product__c>();
 	List<Base_Trail_Survey__c> baseTrailToInsert = new List<Base_Trail_Survey__c>();
 	if(oppIds.size() > 0) {
 		for(Id oppId : oppIds) {
 			// if opportunity have child base trail then base trail id asigned to Base_Product__c.Base_Trail_Survey__c
 			if(oppToBaseTrailMap.containsKey(oppId)) {
 				for(Base_Product__c baseProduct : triggerNewList) {
 					if(quoteToOppMap.get(baseProduct.Oracle_Quote__c) == oppId && oppToBaseTrailMap.containsKey(quoteToOppMap.get(baseProduct.Oracle_Quote__c))) {
 						baseProduct.Base_Trail_Survey__c = oppToBaseTrailMap.get(oppId);
 						bpListToInsert.add(baseProduct);
 						if(!baseTrailIdsTobpQuote.containsKey(baseProduct.Base_Trail_Survey__c)) {
  							baseTrailIdsTobpQuote.put(baseProduct.Base_Trail_Survey__c,baseProduct.Oracle_Quote__c);
  						}
 					}
 				}
 			} else {
 			// if opportunity does not have child base trail then base trail record is created 
 				Base_Trail_Survey__c btsRecord = new Base_Trail_Survey__c(Opportunity__c = oppId);
 				baseTrailToInsert.add(btsRecord);
 			}
 		}
 	}
 	
 	if(baseTrailToInsert.size() > 0) {
 		insert baseTrailToInsert;
	 	for(Base_Trail_Survey__c bts : baseTrailToInsert) {
	 		if(!oppToBaseTrailMap.containsKey(bts.Opportunity__c)) {
	 			oppToBaseTrailMap.put(bts.Opportunity__c,bts.Id);
	 		}
	 	}
	 	// Opportunity.Base_Trail_Survey__c assigned to Base_Product__c.Base_Trail_Survey__c
	 	for(Base_Product__c baseProduct : triggerNewList) {
	 		baseProduct.Base_Trail_Survey__c = oppToBaseTrailMap.get(quoteToOppMap.get(baseProduct.Oracle_Quote__c));
	 		bpListToInsert.add(baseProduct);
	 		if(!baseTrailIdsTobpQuote.containsKey(baseProduct.Base_Trail_Survey__c)) {
  				baseTrailIdsTobpQuote.put(baseProduct.Base_Trail_Survey__c,baseProduct.Oracle_Quote__c);
  			}
	 	}
 	
 	}
 	// populate the Base_Product__c.Oracle_Quote__c to Base_Trail_Survey__c.Oracle_Quote__c
 	for(Base_Trail_Survey__c bts : [SELECT Id, Oracle_Quote__c 
                                        FROM Base_Trail_Survey__c 
                                        WHERE Id IN :baseTrailIdsTobpQuote.keySet()]) {
  			bts.Oracle_Quote__c = baseTrailIdsTobpQuote.get(bts.Id);
            baseTrailList.add(bts);                                
  	}
  	update baseTrailList; 
  } 
  // Method to roll up Price * Quantity value of base product to monthlty total of base trail survey 
  public static void rollUpMonthlyTotal(Map<Id,Base_Product__c> oldTriggerMap, List<Base_Product__c> newTriggerList) {
  	 Set<Id> setofBaseTrailSurveyid = new Set<Id>();
  	 Map<Id,decimal> mapBaseTrailToTotal = new Map<Id,decimal>();
        //insert case 
        if(newTriggerList != Null) {
        	for(Base_Product__c baseProductNewlist : newTriggerList) {
                //update case
            	if(oldTriggerMap == Null) {
                	setofBaseTrailSurveyid.add(baseProductNewlist.Base_Trail_Survey__c );
              	} else if(oldTriggerMap != Null && (baseProductNewlist.Price__c != oldTriggerMap.get(baseProductNewlist.Id).Price__c || baseProductNewlist.Quantity__c != oldTriggerMap.get(baseProductNewlist.Id).Quantity__c)) {
                    	setofBaseTrailSurveyid.add(oldTriggerMap.get(baseProductNewlist.Id).Base_Trail_Survey__c);
                } else if(oldTriggerMap != Null && baseProductNewlist.Base_Trail_Survey__c != oldTriggerMap.get(baseProductNewlist.Id).Base_Trail_Survey__c && baseProductNewlist.Base_Trail_Survey__c == null) {
                	setofBaseTrailSurveyid.add(oldTriggerMap.get(baseProductNewlist.Id).Base_Trail_Survey__c);
                }
        	}
        } 
      //  else if(oldTriggerList != Null) { // delete case
      //          for(Base_Product__c baseProductOldlist : oldTriggerList){
       //             setofBaseTrailSurveyid.add(baseProductOldlist.Base_Trail_Survey__c);
      //          }
     //  }
        
  List<Base_Trail_Survey__c> baseTrailList = new List<Base_Trail_Survey__c>();
  for(Base_Trail_Survey__c bts : [SELECT id,Roll_Up_Total__c FROM Base_Trail_Survey__c WHERE Id IN :setofBaseTrailSurveyid]) {
  	bts.Roll_Up_Total__c = 0;
  	mapBaseTrailToTotal.put(bts.Id,bts.Roll_Up_Total__c);
  }
  System.debug('mapBaseTrailToTotal=='+mapBaseTrailToTotal);
  decimal temp = 0;
  
  for(Base_Product__c baseProduct : [SELECT Price__c,Quantity__c,Base_Trail_Survey__c FROM  Base_Product__c WHERE Base_Trail_Survey__c IN :setofBaseTrailSurveyid ]) {
  	if(mapBaseTrailToTotal.containskey(baseProduct.Base_Trail_Survey__c) && baseProduct.Price__c != null && baseProduct.Quantity__c != null) {
         temp = (baseProduct.Price__c * baseProduct.Quantity__c);
         mapBaseTrailToTotal.put(baseProduct.Base_Trail_Survey__c,mapBaseTrailToTotal.get(baseProduct.Base_Trail_Survey__c) + temp);
     }
  }
 List<Base_Trail_Survey__c> baseTrailListUpdate = new List<Base_Trail_Survey__c>();
        for(Id idset : mapBaseTrailToTotal.keySet()) {
            Base_Trail_Survey__c btsRecord = new Base_Trail_Survey__c(Id = idset , Roll_Up_Total__c = mapBaseTrailToTotal.get(idset));
            baseTrailListUpdate.add(btsRecord);
        }
  if(baseTrailListUpdate.size() > 0) {
  	update baseTrailListUpdate;
  }
  }
  // Method to calculate Monthly total at base trail when base product deletes
  public static void baseProductOnBeforeDelete(List<Base_Product__c> oldList) {
  	Map<Id,decimal> baseTrailToTotal = new Map<Id,decimal>();
  	system.debug('oldMap>>>'+oldList);
  	for(Base_Product__c baseProduct : oldList) {
  		if(!baseTrailToTotal.containsKey(baseProduct.Base_Trail_Survey__c)) {
  			system.debug('IF');
  			baseTrailToTotal.put(baseProduct.Base_Trail_Survey__c,0);
  		}
  		Decimal tempVal = 0;
  		tempVal = baseTrailToTotal.get(baseProduct.Base_Trail_Survey__c);
  		Decimal total = 0;
  		if(baseProduct.Price__c!=null && baseProduct.Quantity__c!=null && baseProduct.Price__c > 0 && baseProduct.Quantity__c > 0){
  			total = baseProduct.Price__c * baseProduct.Quantity__c;	
  		}
  		tempVal = tempVal + total;
  		baseTrailToTotal.put(baseProduct.Base_Trail_Survey__c,tempVal);
  	}
	System.debug('baseTrailToTotaljuly8=='+baseTrailToTotal);
  	List<Base_Trail_Survey__c> baseTrailListToUpdate = new List<Base_Trail_Survey__c>();
  	for(Base_Trail_Survey__c bts : [SELECT Roll_Up_Total__c FROM Base_Trail_Survey__c WHERE Id IN :baseTrailToTotal.keySet()]) {
  		bts.Roll_Up_Total__c = bts.Roll_Up_Total__c - baseTrailToTotal.get(bts.Id);
  		baseTrailListToUpdate.add(bts);
  	}
  	system.debug('baseTrailListToUpdate=='+baseTrailListToUpdate);
  	if(baseTrailListToUpdate.size() > 0) {
  		update baseTrailListToUpdate;
  	}
  }
}