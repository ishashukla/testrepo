/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_RegulatoryFormControllerTest
//
// April 26, 2016, Sunil (Appirio JDC)
//
// Modified By : Kajal Jalan, June 17,2016
//
// Test class for Medical_RegulatoryFormController
*/
@isTest
private class Medical_RegulatoryFormControllerTest {
  private static List<Account> accounts;
  private static List<Contact> contacts;
  private static Order ord;
  private static Case cs;
  private static List<Asset> assets;
  private static COMMBPConfigurations__c customSettingRecord;
  private static SVMXC__Installed_Product__c asset;
  static testMethod void testMedical_RegulatoryFormControllerTest() {
    createTestData(4);
    Test.startTest();
      ApexPages.currentPage().getParameters().put('def_contact_id' , contacts.get(0).Id);
      ApexPages.currentPage().getParameters().put('def_account_id' , accounts.get(0).Id);
      ApexPages.currentPage().getParameters().put('def_asset_id' , contacts.get(0).Id);
      Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('RecordType' ,rtCase );

      ApexPages.StandardController sc = new ApexPages.StandardController(cs);
      Medical_RegulatoryFormController cont = new Medical_RegulatoryFormController(sc);
      PageReference pageRef = Page.Medical_RegulatoryForm;
      Test.setCurrentPage(pageRef);
      cont.redirectCaseNew();
      cont.showNextQuestions();

      //populating answers for the question
      cont.regQuestionAnswer1.Formal_complaint__c = true;
      cont.regQuestionAnswer1.Harm_reported__c = 'No';
      cont.regQuestionAnswer1.Adverse_Consequences__c = 'Yes';
      cont.regQuestionAnswer1.Answer_2_New__c  = 'Field Service';
      cont.regQuestionAnswer1.Answer_4__c = 'Answer';
      cont.regQuestionAnswer1.Answer_5_New__c= 'Test Answer';
      cont.regQuestionAnswer1.Answer_7_New__c = 'No';
      cont.regQuestionAnswer1.Answer_8__c = 'Yes - No Impact';
      cont.regQuestionAnswer1.Answer_9__c = 'No';
      cont.regQuestionAnswer1.Answer_10__c = 'Yes';
      cont.regQuestionAnswer1.Answer_11__c = 'Patient';
      cont.regQuestionAnswer1.Answer_12_New__c = 'No';
      cont.regQuestionAnswer1.Answer_13_New__c = 'Test';
      cont.regQuestionAnswer1.Answer_14_New__c = 'Yes';
      cont.regQuestionAnswer1.Answer_15_New__c = ' (USA) Food and Drug Admininstration';
      cont.regQuestionAnswer1.Answer_16_New__c = '1234';
      cont.regQuestionAnswer1.Answer_17_New__c = 'Customer Only';
      cont.regQuestionAnswer1.Patient_involvement_Medical_delay__c = 'Yes';
      cont.regQuestionAnswer1.Answer_1__c = Date.today().addDays(-2);
      cont.regQuestionAnswer1.Adverse_Consequences__c = 'Yes';
      cont.regQuestionAnswer1.Answer_3__c = Date.today();
      cont.regQuestionAnswer1.Additional_Details__c = 'test';
      cont.regQuestionAnswer1.Answer_18_New__c = 'Email';
      cont.showNextQuestions();
      cont.saveForm();
      
      
    Test.stopTest();

  }


  static testMethod void testMedical_RegulatoryFormControllerTest1() {
    createTestData(4);
    Test.startTest();
      ApexPages.currentPage().getParameters().put('def_contact_id' , contacts.get(0).Id);
      Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('RecordType' ,rtCase );

      ApexPages.StandardController sc = new ApexPages.StandardController(cs);
      Medical_RegulatoryFormController cont = new Medical_RegulatoryFormController(sc);
      PageReference pageRef = Page.Medical_RegulatoryForm;
      Test.setCurrentPage(pageRef);
      cont.redirectCaseNew();
      cont.showNextQuestions();

      //populating answers for the question
      cont.regQuestionAnswer1.Formal_complaint__c = true;
      cont.regQuestionAnswer1.Harm_reported__c = 'No';
      cont.regQuestionAnswer1.Adverse_Consequences__c = 'No';
      cont.regQuestionAnswer1.Answer_4__c = 'Answer';
      cont.regQuestionAnswer1.Answer_5_New__c= 'Test Answer';
      cont.regQuestionAnswer1.Answer_7_New__c = 'No';
      cont.regQuestionAnswer1.Answer_8__c = 'Yes - No Impact';
      cont.regQuestionAnswer1.Answer_11__c = 'Patient';
      cont.regQuestionAnswer1.Answer_12_New__c = 'Yes';
      cont.regQuestionAnswer1.Answer_13_New__c = 'Test';
      cont.regQuestionAnswer1.Answer_14_New__c = 'No';
      cont.regQuestionAnswer1.Answer_15_New__c = ' (USA) Food and Drug Admininstration';
      cont.regQuestionAnswer1.Answer_16_New__c = '1234';
      cont.regQuestionAnswer1.Answer_17_New__c = 'Yes';
      cont.regQuestionAnswer1.Patient_involvement_Medical_delay__c = 'Yes';
      cont.regQuestionAnswer1.Answer_1__c = Date.today();
      cont.regQuestionAnswer1.Adverse_Consequences__c = 'Yes';
      cont.regQuestionAnswer1.Harm_reported__c = 'No';
      cont.regQuestionAnswer1.Formal_complaint__c = false;
      cont.quesString += '+all';
      cont.showNextQuestions();

      //updating for replace all method
      cont.regQuestionAnswer1.Adverse_Consequences__c = 'No';
      cont.regQuestionAnswer1.Formal_complaint__c = false;
      cont.showNextQuestions();
      //Generating exceptions
      cont.quesString = '+7';
      cont.saveForm();
      cont.regQuestionAnswer1.Adverse_Consequences__c = null;
      cont.regQuestionAnswer1.Answer_11__c = null;
      cont.saveForm();

      cont.cancel();
    Test.stopTest();

  }
  static testMethod void testWithNoParameters(){
    createTestData(4);
    Test.startTest();
      ApexPages.currentPage().getParameters().put('Id' , cs.Id);
      ApexPages.StandardController sc = new ApexPages.StandardController(cs);
      Medical_RegulatoryFormController cont = new Medical_RegulatoryFormController(sc);
      PageReference pageRef = Page.Medical_RegulatoryForm;
      Test.setCurrentPage(pageRef);
      Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('RecordType' ,rtCase );
      cont.redirectCaseNew();
      cont.showNextQuestions();
    Test.stopTest();
  }
  static testMethod void validateForm(){
    createTestData(4);
    Test.startTest();
      ApexPages.currentPage().getParameters().put('def_contact_id' , contacts.get(0).Id);
      Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('RecordType' ,rtCase );

      ApexPages.StandardController sc = new ApexPages.StandardController(cs);
      Medical_RegulatoryFormController cont = new Medical_RegulatoryFormController(sc);
      PageReference pageRef = Page.Medical_RegulatoryForm;
      Test.setCurrentPage(pageRef);

      cont.quesString = '1+2+3+4';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+6';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+all';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+7';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+11+12+13';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+15';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+17';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+19+20';
      System.assert(!cont.validateForm(),'Error in form');
      cont.quesString = '+22';
      System.assert(!cont.validateForm(),'Error in form');
      cont.saveForm();

      cont.cancel();
    Test.stopTest();
  }
  static testMethod void redirectingToStandardPage(){
    createTestData(4);
    Test.startTest();
      ApexPages.StandardController sc = new ApexPages.StandardController(cs);

      PageReference pageRef = Page.Medical_RegulatoryForm;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('Id' ,cs.Id );
      Medical_RegulatoryFormController cont = new Medical_RegulatoryFormController(sc);
      cont.redirectCaseNew();
      cont.showNextQuestions();
    Test.stopTest();
  }
    static testMethod void testForCOMM() {
    createTestData(4);
        Order orderRecordTest = TestUtils.createOrder(1, accounts[0].Id, 'Entered', true).get(0);
    Test.startTest();
      ApexPages.currentPage().getParameters().put('def_contact_id' , contacts.get(0).Id);
      ApexPages.currentPage().getParameters().put('def_account_id' , accounts.get(0).Id);
      ApexPages.currentPage().getParameters().put('def_asset_id' , contacts.get(0).Id);
      ApexPages.currentPage().getParameters().put('cas4_lkid' , accounts.get(0).Id);
      ApexPages.currentPage().getParameters().put('cas3_lkid' , contacts.get(0).Id);
      ApexPages.currentPage().getParameters().put(customSettingRecord.New_Case_on_Asset__c+'_lkid' ,asset.Id );
      ApexPages.currentPage().getParameters().put(customSettingRecord.Order_On_Case__c+'_lkid' ,orderRecordTest.Id );
      Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('RecordType' ,rtCase );

      ApexPages.StandardController sc = new ApexPages.StandardController(cs);
      Medical_RegulatoryFormController cont = new Medical_RegulatoryFormController(sc);
      PageReference pageRef = Page.Medical_RegulatoryForm;
      Test.setCurrentPage(pageRef);
      cont.redirectCaseNew();
      cont.showNextQuestions();

      //populating answers for the question
      cont.regQuestionAnswer1.Formal_complaint__c = false;
      cont.regQuestionAnswer1.Harm_reported__c = 'No';
      cont.regQuestionAnswer1.Adverse_Consequences__c = 'Yes';
      cont.regQuestionAnswer1.Answer_2_New__c  = 'Field Service';
      cont.regQuestionAnswer1.Answer_4__c = 'Answer';
      cont.regQuestionAnswer1.Answer_5_New__c= 'Test Answer';
      cont.regQuestionAnswer1.Answer_7_New__c = 'No';
      cont.regQuestionAnswer1.Answer_8__c = 'Yes - No Impact';
      cont.regQuestionAnswer1.Answer_9__c = 'No';
      cont.regQuestionAnswer1.Answer_10__c = 'Yes';
      cont.regQuestionAnswer1.Answer_11__c = 'Patient';
      cont.regQuestionAnswer1.Answer_12_New__c = 'No';
      cont.regQuestionAnswer1.Answer_13_New__c = 'Test';
      cont.regQuestionAnswer1.Answer_14_New__c = 'Yes';
      cont.regQuestionAnswer1.Answer_15_New__c = ' (USA) Food and Drug Admininstration';
      cont.regQuestionAnswer1.Answer_16_New__c = '1234';
      cont.regQuestionAnswer1.Answer_17_New__c = 'Customer Only';
      cont.regQuestionAnswer1.Patient_involvement_Medical_delay__c = 'Complainant Not Aware';
      cont.regQuestionAnswer1.Answer_1__c = Date.today().addDays(-2);
      cont.regQuestionAnswer1.Adverse_Consequences__c = 'No';
      cont.regQuestionAnswer1.Answer_3__c = Date.today();
      cont.regQuestionAnswer1.Additional_Details__c = 'test';
      cont.regQuestionAnswer1.Answer_18_New__c = 'Email';
      cont.showNextQuestions();
      cont.saveForm();
      
      
    Test.stopTest();

  }
  private static void createTestData(Integer count){
    accounts = Medical_TestUtils.createAccount(count + 1, false);
    accounts.get(0).Billing_Type__c = 'Bill To Address Only';
    insert accounts;
    contacts = Medical_TestUtils.createContact(2, accounts.get(0).id , false);
    contacts.get(0).Phone = '(999) 999-9999';
    insert contacts;
    List<Contact_Acct_Association__c> contactAccAssociations = new List<Contact_Acct_Association__c>();
    for(integer i = 1 ; i <= count; i++) {
      contactAccAssociations.add(new Contact_Acct_Association__c(Associated_Contact__c = contacts.get(0).id, Associated_Account__c = accounts.get(i).id, Account_Contact_Id__c = contacts.get(0).id + '~' + accounts.get(i).id));
    }
    insert contactAccAssociations;

    assets = Medical_TestUtils.createAsset(1,accounts.get(0).Id,true);

    Id medicalNonProductCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Non Product Issue').getRecordTypeId();
    TestUtils.createRTMapCS(medicalNonProductCaseRT, 'Medical - Non Product Issue', 'Medical');

    cs = Medical_TestUtils.createCase(1, accounts.get(0).Id, contacts.get(0).Id, false).get(0);
    cs.RecordTypeId = medicalNonProductCaseRT;
    insert cs;

    Product2 prod = Medical_TestUtils.createProduct(true);
    Pricebook2 pb = Medical_TestUtils.createPricebook(1,'Test PriceBook', true).get(0);
    PricebookEntry pbe1 = Medical_TestUtils.createPricebookEntry(prod.Id,pb.Id,false);
    PricebookEntry pbe2 = Medical_TestUtils.createPricebookEntry(prod.Id,Test.getStandardPricebookId(), false);
    insert (new List<PricebookEntry>{pbe2,pbe1});
    Medical_Common_Settings__c setting = Medical_TestUtils.createMedicalCommonSetting(true);
    /*ord = Medical_TestUtils.createOrder(accounts.get(0).Id,contacts.get(0).Id, pb.Id,false);
    ord.Is_Scheduled_Order_Mail_Sent__c = false;
    ord.Contact_PhoneNumber__c = '(999) 999-9999';
    ord.PoNumber = '123456';
    insert ord;*/

    Medical_JDE_Settings__c jdeSetting = Medical_TestUtils.createMedicalJDESetting(true);
    Medical_QB_Settings__c qbSetting = Medical_TestUtils.createMedicalQBSetting(true);
    customSettingRecord = new COMMBPConfigurations__c(New_Case_on_Asset__c = 'CF00Nc0000001QcOP',Order_On_Case__c='CF00Nc0000001W0qT');
    insert customSettingRecord;
    asset = new SVMXC__Installed_Product__c();
    insert asset;
  }

}