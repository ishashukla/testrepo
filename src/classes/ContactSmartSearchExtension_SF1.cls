public with sharing class ContactSmartSearchExtension_SF1 {

	private final sObject mysObject;
    public Contact nContact {get;set;}
    public Boolean onloadRedirect {get;set;}
    public String allCreate;
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public ContactSmartSearchExtension_SF1(ApexPages.StandardController stdController) {
        this.onloadRedirect = false;
        this.mysObject = (sObject)stdController.getRecord();
        this.nContact = new Contact( FirstName = ApexPages.currentPage().getParameters().get('firstName'),
                                LastName = ApexPages.currentPage().getParameters().get('lastName'),
                                Email = ApexPages.currentPage().getParameters().get('email'),
                                Phone = ApexPages.currentPage().getParameters().get('phone'),
                                AccountId = ApexPages.currentPage().getParameters().get('accId'),
                                Allow_Create__c = true
            );
        Id existingId = this.nContact.id;
        if(existingId != null){
            this.onloadRedirect = (String.ValueOf(existingId).length() > 0) ? true : false;
        }
    }
    public void save(){
        try{
            upsert nContact;
        }catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
    public String getRecordName() {
        return 'Hello ' + (String)mysObject.get('name') + ' (' + (Id)mysObject.get('Id') + ')';
    }
}