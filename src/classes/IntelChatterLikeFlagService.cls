/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 20, 2013
 Purpose       :  REST Webservice for saving Chatter Post Likes and Intel Flag.
 Reference     :  Task T-205633
 Modifications :  Task T-205594    Nov 25, 2013     Sonal Shrivastava    
                  Task T-216321    Nov 29, 2013     Sonal Shrivastava
                  Task T-216320    Nov 30, 2013     Sonal Shrivastava
*******************************************************************************/
@RestResource(urlMapping='/IntelChatterLikeFlagService/*') 
global without sharing class IntelChatterLikeFlagService {
  
  private static String intel_Id;
  private static String reply_Id;
  private static String chatterPostId;
  
  //****************************************************************************
  // GET Method for REST service
  //****************************************************************************
  @HttpGet 
  global static IntelLikeFlagWrapper doGet(){
    try { 
      RestRequest req = RestContext.request;
      //get intel id from url params
	    if(req.params.containsKey('intelId')){
	      intel_Id = req.params.get('intelId');
	    }
	    if(req.params.containsKey('replyId')){
	      reply_Id = req.params.get('replyId');
	    }
      if((intel_Id != null && intel_Id != '') || 
         (reply_Id != null && reply_Id != '')){
      	return fetchAllRecords();
      }    
      return new IntelLikeFlagWrapper();
    } 
    catch(Exception ex) {
      return new IntelLikeFlagWrapper();
    } 
    return null;
  }
  
  //****************************************************************************
  // POST Method for REST service
  //****************************************************************************  
  @HttpPost 
  global static Map<String, String> doPost() {
    RestRequest req = RestContext.request;
    Map<String, String> res = new Map<String,String>();
    String result;    
    try {   
      //parse the requestBody into a string
      if(req.requestBody != null) {                
        Blob reqBody = req.requestBody;   
        result = parseRecords(reqBody.toString());      
      } 
      else {
        result = Constants.INVALID_DATA;
      }
    }
    catch(Exception ex){
      res.put(Constants.ERROR, ex.getMessage());
      res.put(Constants.REQUEST_BODY, req.requestBody.toString());
      return res;
    }    
    res.put(Constants.RESPONSE, result);
    return res;
  } 
  
  //****************************************************************************
  // Method for fetching all intel flag records and like records related to the
  // mentioned intel id
  //****************************************************************************
  private static IntelLikeFlagWrapper fetchAllRecords(){
  	IntelLikeFlagWrapper flagWrapper = new IntelLikeFlagWrapper();
  	List<IntelLikeFlagJSON> likeFlagList = new List<IntelLikeFlagJSON>();   	
  	
  	if(intel_Id != null && intel_Id != '') {
	  	//Get Intel Flag records
	  	fetchIntelFlags(likeFlagList);	  	
	  	//Get Post likes
	  	fetchPostLikes(likeFlagList);
  	}
  	
  	if(reply_Id != null && reply_Id != '') {
  		//Fetch Reply Comment likes
  		fetchReplyLikes(likeFlagList);
  	}  	
        
  	flagWrapper.intelLikeFlagList = likeFlagList;
  	return flagWrapper;  	
  }
  
  //****************************************************************************
  // Method for fetching all intel flag records 
  //****************************************************************************
  private static void fetchIntelFlags(List<IntelLikeFlagJSON> likeFlagList){
    for(Intel__c intel : [SELECT Id, Chatter_Post__c,
    	                     (SELECT Id, Entity_Type__c, Entity_Id__c, 
                                  Occurrence_Date__c, Posting_User__c, 
                                  Posting_User__r.Name, Post_Comment__c,
                                  CreatedDate 
                            FROM Intel_Flag__r)
                          FROM Intel__c
                          WHERE Id = :intel_Id]){
      
      if(intel.Chatter_Post__c != null){
    		chatterPostId = intel.Chatter_Post__c;
    		
    		if(intel.Intel_Flag__r.size() > 0){
    			for(Intel_Flag__c intelFlag : intel.Intel_Flag__r){
    				likeFlagList.add(new IntelLikeFlagJSON(intel_Id,
			                                             intelFlag.Entity_Type__c == Constants.TYPE_POST ? Constants.TYPE_INTEL : Constants.TYPE_REPLY,
			                                             Constants.ACTION_FLAG,
			                                             intelFlag.Posting_User__c,
			                                             intelFlag.Posting_User__r.Name,
			                                             intelFlag.CreatedDate,
			                                             intelFlag.Post_Comment__c,
			                                             intelFlag.Entity_Id__c)); 
    			}
    		}
    	}
    }
  }
  
  //****************************************************************************
  // Method for fetching Post like records 
  //****************************************************************************
  private static void fetchPostLikes(List<IntelLikeFlagJSON> likeFlagList){
  	if(chatterPostId != null){
	  	for(Intel__Feed intelFeed : [SELECT ParentId, LikeCount,
	                                   (SELECT Id, FeedItemId, CreatedById, CreatedBy.Name, CreatedDate
	                                    FROM FeedLikes 
	                                    WHERE FeedItemId = :chatterPostId)
	                                 FROM Intel__Feed 
	                                 WHERE ParentId = :intel_Id AND LikeCount > 0]){
	      if(intelFeed.FeedLikes.size() > 0){
	      	for(FeedLike lik : intelFeed.FeedLikes){
            likeFlagList.add(new IntelLikeFlagJSON(intel_Id,
                                                   Constants.TYPE_INTEL,
                                                   Constants.ACTION_LIKE,
                                                   lik.CreatedById,
                                                   lik.CreatedBy.Name,
                                                   lik.CreatedDate,
                                                   null,
                                                   chatterPostId)); 
	      	} 
	      }                          
	    }
  	}
  }
  
  //****************************************************************************
  // Method for fetching Comment like records 
  //****************************************************************************
  private static void fetchReplyLikes(List<IntelLikeFlagJSON> likeFlagList){
  	FeedComment reply;
  	for(FeedComment fc : [SELECT Id, ParentId FROM FeedComment WHERE Id = :reply_Id]){
  		reply = fc;
  	} 
  	if(reply != null && !Test.isRunningTest() ){
  		ConnectApi.ChatterLikePage likePage = ConnectApi.ChatterFeeds.getLikesForComment(null, reply.Id);
	    for(ConnectApi.ChatterLike chLike : likePage.likes){
	      likeFlagList.add(new IntelLikeFlagJSON( reply.ParentId,
			                                          Constants.TYPE_REPLY,
			                                          Constants.ACTION_LIKE,
			                                          chLike.user.id,
			                                          chLike.user.name,
			                                          null,
			                                          null,
			                                          chLike.likedItem.Id ));
	    }
  	}  	
  }  
   
  //****************************************************************************
  // Method for getting tag and comment records from the Request string
  //****************************************************************************
  private static string parseRecords(String reqBody){
    
    IntelLikeFlagWrapper wrap = (IntelLikeFlagWrapper)JSON.deserialize(reqBody, IntelLikeFlagWrapper.class);
    
    String result;
    Set<String> setIntelIds = new Set<String>();
    List<Intel_Flag__c> lstIntelFlag = new List<Intel_Flag__c>(); 
    List<FeedLike> likeList = new List<FeedLike>();  
    Map<String, List<FeedLike>> mapIntelId_likeList = new Map<String, List<FeedLike>>();
    
    try {
	    if(wrap != null && wrap.intelLikeFlagList != null && wrap.intelLikeFlagList.size() > 0){
	      for(IntelLikeFlagJSON intelWrap : wrap.intelLikeFlagList){
	        
	        if(intelWrap.id != null && intelWrap.id != ''){
	        	System.debug('intelWrap --->>> ' + intelWrap);
	          setIntelIds.add(intelWrap.Id);
	          
	          //Create Feed Like records
          	if(intelWrap.action == Constants.ACTION_LIKE){
              if(intelWrap.postedById != null && intelWrap.postedById != '' && intelWrap.entityId != null && intelWrap.entityId != ''){
                FeedLike lik = new FeedLike(CreatedById = intelWrap.postedById);                
                //Like records for Chatter Post
                if(intelWrap.type == Constants.TYPE_INTEL){
                	lik.FeedItemId = intelWrap.entityId;
                }                
                //Like records for Chatter Post Reply
                else if(intelWrap.type == Constants.TYPE_REPLY){
                	lik.FeedEntityId = intelWrap.entityId;
                }
                likeList.add(lik);
              }
            }
            //Create Intel Flag records
            else if(intelWrap.action == Constants.ACTION_FLAG){
            	System.debug('intelWrap.postedOn --->>> ' + intelWrap.postedOn);
              lstIntelFlag.add(new Intel_Flag__c( Posting_User__c    = intelWrap.postedById,
                                                  Post_Comment__c    = intelWrap.body,
                                                  Intel__c           = intelWrap.id,
                                                  Occurrence_Date__c = intelWrap.postedOn, //System.now(),
                                                  Entity_Type__c     = intelWrap.type == Constants.TYPE_INTEL ? Constants.TYPE_POST 
											                                                                                        : (intelWrap.type == Constants.TYPE_REPLY 
											                                                                                           ? Constants.TYPE_COMMENT : null),
                                                  Entity_Id__c       = intelWrap.entityId));
            }	          
	        }
	      }
	      if(setIntelIds.size() > 0){
	        result = insertRecords(lstIntelFlag, likeList);   
	      }        
	    }
	    else{
        result = Constants.NO_RECORDS_FOUND;
	    }      
    }
    catch(Exception ex){
      return (Constants.ERROR + ex.getMessage());
    }
    return result;
  }
  
  //***************************************************************************/
  // Method for saving Intel and Feed Like records.
  //***************************************************************************/
  private static String insertRecords(List<Intel_Flag__c> lstIntelFlag,
                                      List<FeedLike> likeList){
    String result;
    Savepoint sp = Database.setSavepoint();
    try {
      if(lstIntelFlag.size() > 0){
      	System.debug('lstIntelFlag --->>> ' + lstIntelFlag);
        insert lstIntelFlag;
      }
      if(likeList.size() > 0){
      	insert likeList;
      }
      result = Constants.SAVE_SUCCESS;
    }
    catch(Exception ex){
      Database.rollback(sp);
      throw ex;
    }
    return result;
  }
  
  //***************************************************************************/
  // Wrapper class for chatter like or flag
  //***************************************************************************/
  global class IntelLikeFlagWrapper{
    public List<IntelLikeFlagJSON> intelLikeFlagList;
    
    public IntelLikeFlagWrapper(){
    	this.intelLikeFlagList = new List<IntelLikeFlagJSON>();
    }
  }
  
  public class IntelLikeFlagJSON {
    public String id;
    public String type;
    public String action;
    public String postedById;
    public String postedBy;
    public DateTime postedOn;
    public String body;
    public String entityId;
    
    public IntelLikeFlagJSON(){}
    
    public IntelLikeFlagJSON(String id, String type, String action, String postedById, 
                             String postedBy, DateTime postedOn, String commentBody,
                             String entityId){
      this.id          = id;
      this.type        = type;
      this.action      = action;
      this.postedById  = postedById;
      this.postedBy    = postedBy;
      this.postedOn    = postedOn;
      this.body        = commentBody;
      this.entityId    = entityId;
    }
  }
}