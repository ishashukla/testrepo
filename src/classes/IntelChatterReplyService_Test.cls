/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 20, 2013
 Purpose       :  Test Class for IntelChatterReplyService.cls
 Reference     :  Task T-205633
*******************************************************************************/
@isTest
private class IntelChatterReplyService_Test {

	static testMethod void myUnitTest() {
    //Create test records
    List<Intel__c> intelList = TestUtils.createIntel(1, true);
    List<Tag__c> tagList = TestUtils.createTag(1, intelList.get(0).Id, true);
    User usr = TestUtils.createUser(1, null, true).get(0);
    
    FeedItem chatterPost = new FeedItem(parentId = intelList.get(0).Id,
                                        body = 'Test Body');
    insert chatterPost;
    
    intelList.get(0).Chatter_Post__c = chatterPost.Id;
    update intelList.get(0);
    
    intelList = [SELECT Id, OwnerId, Owner.Name, CreatedDate, Details__c, Flagged_As_Inappropriate__c, LastModifiedDate, Chatter_Post__c,
                  (SELECT Id, Name, Title__c, Account__c, Contact__c, Product__c, User__c, CreatedDate FROM Tags__r)
                 FROM Intel__c WHERE Id = :intelList.get(0).Id];
    
    //Test functionality 
    System.runAs(usr){
    
      List<IntelChatterReplyService.IntelReplyJSON> intelJsonList = new List<IntelChatterReplyService.IntelReplyJSON>();
      List<IntelChatterReplyService.TagJson> tagJsonList = new List<IntelChatterReplyService.TagJson>();
       
      tagJsonList.add(new IntelChatterReplyService.TagJson(intelList.get(0).Tags__r.get(0)));
      intelJsonList.add(new IntelChatterReplyService.IntelReplyJSON(intelList.get(0).Id, 'Test reply', usr.Id, usr.lastName, tagJsonList));
      
      IntelChatterReplyService.IntelChatterReplyWrapper wrap = new IntelChatterReplyService.IntelChatterReplyWrapper();
      wrap.intelReplyList = intelJsonList;
      
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();
      
      req.requestURI = '/services/apexrest/IntelChatterReplyService/*';
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueOf(JSON.serialize(wrap)); 
      
      RestContext.request = req;
      RestContext.response = res;
      
      test.startTest();
      Map<String, String> result = IntelChatterReplyService.doPost();
      System.assertEquals(1, [SELECT Id FROM Tag__c WHERE Id NOT IN :tagList].size());    
      System.assertEquals(1, [SELECT Id FROM FeedComment WHERE FeedItemId = :chatterPost.Id].size());        
      test.stopTest();
    }  
	}
}