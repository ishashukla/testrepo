// 
// (c) 2016 Appirio, Inc.
//
// T-538799, Controller Class To show Tracking link on Order Product detail page 
// I-235766, Controller Class To show Tracking link on Order Product detail page 
//
// Sept 20, 2016  Isha Shukla  Original 
// Sept 17, 2016  Shreerath Nair  Original 
// Oct  04, 2016  Parul Gupta  T-542929 - Refer new Tracking_Numbers__c field

Public class OrderItemTrackingController{
    
    //variable to retrieve the order Product record
    public OrderItem ordrItem;
    
    //variable used in page for the Tracking link
    public String trackingLink{get;set;}
    
    //name to bind with the tracking link
    public String trackingLinkName{get;set;}
    
    //contructor
    public OrderItemTrackingController(ApexPages.StandardController stdController) {
        
        this.ordrItem= (OrderItem)stdController.getRecord();
        
        //query the shipping method ,tracking number and order's PO number of current order Product record
        ordrItem= [SELECT Shipping_Method__c, Tracking_Numbers__c, Order.PoNumber
                   FROM OrderItem
                   WHERE Id =: ordrItem.Id];
        
        //put shipping method into a string            
        String shippingMethod = ordrItem.Shipping_Method__c;
        
        //if condition to match the data of shipping method and put the respective tracking link
        
        if(shippingMethod!=null && shippingMethod.contains('FDX')){
            
            trackingLink = 'http://www.fedex.com/Tracking?tracknumbers='+ ordrItem.Tracking_Numbers__c;
            trackingLinkName = 'FDX Website';
        }
        else if(shippingMethod!=null && shippingMethod.contains('UPS')){
            
            trackingLink = 'https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums='+ ordrItem.Tracking_Numbers__c;
            trackingLinkName = 'UPS Website';
        }
        else if(shippingMethod!=null && shippingMethod.contains('MAN-LTL')){
            
            trackingLink = 'http://www.trumpcardinc.com/SearchTrackingMultiple.aspx?SearchByAirbill='+ ordrItem.Tracking_Numbers__c +'&SearchByRefPO=' + ordrItem.Order.PoNumber ;
            trackingLinkName = 'Trumpcard Website';
        }
        else if(shippingMethod!=null && shippingMethod.contains('xxxx')){
            
            trackingLink = 'http://mds.mfsclarity.com/Clarity/application/tracker/liftairbill.asp?TransID=&Airbill='+ ordrItem.Tracking_Numbers__c;
            trackingLinkName = 'Manna Website';
        }
        //If no condition matches then return a message of unavailable link
        else{
            
            trackingLinkName = 'Shipping link unavailable for this provider';
            trackingLink = null;
        }
        
        
    }
    
}