@isTest
private class WorkDetailsTiggerHandlerTest {
    static testMethod void test() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Id workOrderRTId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        SVMXC__Service_Order__c woRecord = new SVMXC__Service_Order__c(RecordTypeId = workOrderRTId);
        insert woRecord;
        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 10; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            soRecord.SVMXC__Line_Type__c = 'Parts';
            soRecord.SVMXC__Line_Status__c = 'Open';
            soRecord.SVMXC__Discount__c = 16;
            soRecord.SVMXC__Service_Order__c = woRecord.Id;
            workDetails.add(soRecord);
        }
        Test.startTest();
        insert workDetails;
        Test.stopTest();
        System.assertEquals([SELECT Discount_Approval__c FROM SVMXC__Service_Order__c WHERE Id = :woRecord.Id].Discount_Approval__c, 'Pending Approval');
	}
    static testMethod void testSecond() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Id workOrderRTId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        SVMXC__Service_Order__c woRecord = new SVMXC__Service_Order__c(RecordTypeId = workOrderRTId);
        insert woRecord;
        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 10; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            soRecord.SVMXC__Line_Type__c = 'Parts';
            soRecord.SVMXC__Line_Status__c = 'Completed';
            soRecord.SVMXC__Discount__c = 16;
            soRecord.SVMXC__Service_Order__c = woRecord.Id;
            workDetails.add(soRecord);
        }
        Test.startTest();
        insert workDetails;
        List<SVMXC__Service_Order_Line__c> workDetailsUpdateList = new List<SVMXC__Service_Order_Line__c>();
        for(SVMXC__Service_Order_Line__c wl : workDetails) {
            wl.SVMXC__Discount__c = 20;
            workDetailsUpdateList.add(wl);
        }
        update workDetailsUpdateList;
        Test.stopTest();
        System.assertEquals([SELECT Discount_Approval__c FROM SVMXC__Service_Order__c WHERE Id = :woRecord.Id].Discount_Approval__c, 'Pending Approval');
	}
    @isTest static void testUpdateQuantity() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Id workOrderRTId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        SVMXC__Service_Order__c woRecord = new SVMXC__Service_Order__c(RecordTypeId = workOrderRTId);
        insert woRecord;
        SVMXC__Product_Stock__c stockPRoduct = new SVMXC__Product_Stock__c(SVMXC__Quantity2__c=30);
        insert stockProduct;
        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 10; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            soRecord.SVMXC__Line_Type__c = 'Labor';
            soRecord.SVMXC__Service_Order__c = woRecord.Id;
            soRecord.SVMX_Consumed_Part__c = stockProduct.Id;
            soRecord.SVMXC__Billable_Quantity__c = 20;
            workDetails.add(soRecord);
        }
        Test.startTest();
        insert workDetails;
        Test.stopTest();
        System.assertEquals([SELECT SVMXC__Line_Status__c 
                             FROM SVMXC__Service_Order_Line__c 
                             WHERE Id = :workDetails[0].Id].SVMXC__Line_Status__c, 'Completed');
    }
    @isTest static void testUpdateStockedSerial() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Id workOrderRTId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        SVMXC__Service_Order__c woRecord = new SVMXC__Service_Order__c(RecordTypeId = workOrderRTId);
        insert woRecord;
        SVMXC__Product_Stock__c stockPRoduct = new SVMXC__Product_Stock__c(SVMXC__Quantity2__c=30);
        insert stockProduct;
        SVMXC__Product_Serial__c serail = new SVMXC__Product_Serial__c(SVMXC__Product_Stock__c=stockProduct.Id);
        insert serail;
        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 10; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            soRecord.SVMXC__Line_Type__c = 'Labor';
            soRecord.SVMXC__Service_Order__c = woRecord.Id;
            soRecord.SVMX_Consumed_Part__c = stockProduct.Id;
            soRecord.SVMXC__Billable_Quantity__c = 20;
            soRecord.SVMX_Consumed_Serial_Number__c = serail.Id;
            workDetails.add(soRecord);
        }
        Test.startTest();
        insert workDetails;
        Test.stopTest();
        System.assertEquals([SELECT SVMXC__Line_Status__c 
                             FROM SVMXC__Service_Order_Line__c 
                             WHERE Id = :workDetails[0].Id].SVMXC__Line_Status__c, 'Completed');
    }
    static testMethod void testThird() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Id workOrderRTId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        SVMXC__Service_Order__c woRecord = new SVMXC__Service_Order__c(RecordTypeId = workOrderRTId);
        insert woRecord;
        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 10; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            soRecord.SVMXC__Line_Type__c = 'Labor';
            soRecord.SVMXC__Line_Status__c = 'Open';
            soRecord.SVMXC__Service_Order__c = woRecord.Id;
            workDetails.add(soRecord);
        }
        Test.startTest();
        insert workDetails;
        List<SVMXC__Service_Order_Line__c> workDetailsUpdateList = new List<SVMXC__Service_Order_Line__c>();
        for(SVMXC__Service_Order_Line__c wl : workDetails) {
            wl.SVMXC__Billable_Line_Price__c = 2600;
            workDetailsUpdateList.add(wl);
        }
        update workDetailsUpdateList;
        Test.stopTest();
        System.assertEquals([SELECT Discount_Approval__c FROM SVMXC__Service_Order__c WHERE Id = :woRecord.Id].Discount_Approval__c, 'Pending Approval');
	}

}