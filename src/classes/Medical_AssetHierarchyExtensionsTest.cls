/*************************************************************************************************
Created By:    Rahul Aeran
Date:          April 22, 2016
Description  : Test class for Medical_AssetHierarchyExtensions
**************************************************************************************************/
@isTest
private class Medical_AssetHierarchyExtensionsTest {
  static testMethod void testMedicalAssetHierarchyExtension(){
  	Account objAccount = Medical_TestUtils.createAccount(1, true).get(0);

  	Asset objAsset = Medical_TestUtils.createAsset(1,objAccount.Id,false).get(0);
  	objAsset.Shipped_Date__c = System.today();
  	insert objAsset;

  	Product2 objProduct1 = Medical_TestUtils.createProduct(false);
  	objProduct1.Name = 'Parent1';

  	Product2 objProduct2 = Medical_TestUtils.createProduct(false);
  	objProduct2.Name = 'Parent2';

  	Product2 objProduct3 = Medical_TestUtils.createProduct(false);
  	objProduct3.Name = 'Child1';

  	Product2 objProduct4 = Medical_TestUtils.createProduct(false);
  	objProduct4.Name = 'Child2';

  	Product2 objProduct5 = Medical_TestUtils.createProduct(false);
    objProduct5.Name = 'Alternate';

    Product2 objProduct6 = Medical_TestUtils.createProduct(false);
    objProduct6.Name = 'Grand Child1';

    Product2 objProduct7 = Medical_TestUtils.createProduct(false);
    objProduct7.Name = 'Grand Child2';

  	insert (new List<Product2>{objProduct1,objProduct2,objProduct3,objProduct4,objProduct5,objProduct6,objProduct7});

  	List<Asset_Configuration_Line__c> lstAssetConfigLine1 = Medical_TestUtils.createAssetConfigLine(1,objAsset.Id,objProduct1.Id,false);
  	List<Asset_Configuration_Line__c> lstAssetConfigLine2 = Medical_TestUtils.createAssetConfigLine(1,objAsset.Id,objProduct2.Id,false);
  	List<Asset_Configuration_Line__c> lstAllAssetConfigLine = new List<Asset_Configuration_Line__c>();
  	lstAllAssetConfigLine.addAll(lstAssetConfigLine1);
  	lstAllAssetConfigLine.addAll(lstAssetConfigLine2);
  	insert lstAllAssetConfigLine;


  	List<Product_Relationship__c> lstRelationship1 =  Medical_TestUtils.createProductRelationship(1,objProduct1.Id,objProduct3.Id,null,false);
  	List<Product_Relationship__c> lstRelationship2 =  Medical_TestUtils.createProductRelationship(1,objProduct2.Id,objProduct4.Id,null,false);
  	List<Product_Relationship__c> lstRelationship3 =  Medical_TestUtils.createProductRelationship(1,objProduct3.Id,objProduct6.Id,null,false);
  	List<Product_Relationship__c> lstRelationship4 =  Medical_TestUtils.createProductRelationship(1,objProduct4.Id,objProduct7.Id,null,false);
  	List<Product_Relationship__c> lstAllRelationships = new List<Product_Relationship__c>();
    lstAllRelationships.addAll(lstRelationship1);
    lstAllRelationships.addAll(lstRelationship2);
    lstAllRelationships.addAll(lstRelationship3);
    lstAllRelationships.addAll(lstRelationship4);
    insert lstAllRelationships;

  	Test.startTest();
  	  ApexPages.StandardController sc = new ApexPages.StandardController(objAsset);
  	  Medical_AssetHierarchyExtensions ext = new Medical_AssetHierarchyExtensions(sc);
      PageReference pageRef = Page.Medical_AssetHierarchy;
      Test.setCurrentPage(pageRef);
      System.assert(ext.currentAsset != null , 'The asset should not be null');
      System.assert(!ext.wrapperList.isEmpty(), 'The wrapper list should contain the elements');
    Test.stopTest();
  }
}