/*************************************************************************************************
Created By:    Sunil Gupta
Date:          March 13, 2014
Description  : Controller class for Endo_showEmployees
**************************************************************************************************/

public with sharing class Endo_showEmployeesController {
  
  // Public Varibales.
  public static Product2 currentProduct{get;set;}
  
  //-----------------------------------------------------------------------------------------------
  // Constructor
  //-----------------------------------------------------------------------------------------------
  public Endo_showEmployeesController(ApexPages.StandardController sc){
  	currentProduct = (Product2)sc.getRecord();
  	currentProduct = [SELECT Id, Name, ODP_Category__c FROM Product2 WHERE Id = :currentProduct.Id].get(0);
  }
  
  //-----------------------------------------------------------------------------------------------
  // Helper method to populate contact list
  //-----------------------------------------------------------------------------------------------
  public List<Contact> getContactList(){
    List<Contact> lstContacts = new List<Contact>();
    String productLine = currentProduct.ODP_Category__c;
    if(String.isBlank(productLine) == false){
      lstContacts = [SELECT Id, Name, Title, Role__c, Department, Phone, MobilePhone, Email, Product_Line__c FROM Contact WHERE RecordType.Name='Endo Employee' AND Product_Line__c INCLUDES(:productLine)];
    }
    return lstContacts;
  }
}