// (c) 2015 Appirio, Inc.
//
// Class Name: InstrumentsEventTriggerHandlerTest 
// Description: Test Class for InstrumentsEventTriggerHandler class.
// 
// April 18 2016, Isha Shukla  Original 
//
@isTest
private class InstrumentsEventTriggerHandlerTest {
    static Event event;
    static List<User> user;
    @isTest static void testDelete() {
        createData();
        Test.startTest();
        System.runAs(user[0]) {
            
            //Test.startTest();
            try {
                delete event;
            } catch(Exception e) {
                e.setMessage('Deletion Failed');
            }
            Test.stopTest();
            System.assertEquals(true,event != Null);
        }
    }
    @isTest static void testEdit() {
        createData();
        Test.startTest();
        System.runAs(user[0]) {
            
            event.ActivityDate=date.parse('4/28/2016');
            //Test.startTest();
            try {
                update event;
            } catch(Exception e) {
                e.setMessage('Updation Failed');
            }
            Test.stopTest();
            System.assertEquals(true,event != Null);
        }
    }
    public static void createData() {
        Profile profile = TestUtils.fetchProfile('System Administrator');
        user = TestUtils.createUser(1, profile.Name, true); 
        List<SVMXC__ServiceMax_Processes__c> svmxcProcessTestData = TestUtils.createServiceMaxProcess(1,user.get(0).Id, true);// Meghna Vijay T-509044 (09/08/2016)- 
                                                                                                                              //Added to remove System.null pointer exception caused by svmx.
        List<SVMXC__ServiceMax_Config_Data__c> svmxctestData = TestUtils.createServiceMaxConfig(1, svmxcProcessTestData.get(0).Id,false);
        
        insert svmxctestData;
        System.runAs(user[0]) {
            event = new Event(IsAllDayEvent=true,ActivityDate=date.parse('4/18/2016'));
            event.RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('PTO').getRecordTypeId();
            event.Subject='Call';
            event.OwnerId=user[0].Id;
            event.StartDateTime=date.parse('4/18/2016');
            event.EndDateTime=date.parse('4/18/2016') + 12;
            insert event;
        }
    }
}