// (c) 2016 Appirio, Inc.
//
// Class Name: ServiceContractLineItemTriggerHandler - Trigger Handller for SVMXC__Service_Contract_Products__c
//
// 06/15/2016, Rahul Aeran       (T-510094) Created for COMM to update contract end date 
  
public class ServiceContractLineItemTriggerHandler{
  public static void onAfterInsert(List<SVMXC__Service_Contract_Products__c> lstNew){
    updateContractEndDateOnAsset(lstNew,null);
    
  }
  
  
  //----------------------------------------------------------------------------
  //  Updating the Contract End date and record type on installed product field
  //----------------------------------------------------------------------------
  private static void updateContractEndDateOnAsset(List<SVMXC__Service_Contract_Products__c> lstNew, map<Id,SVMXC__Service_Contract_Products__c> mapOld){
    Set<Id> setContractIds = new Set<Id>();
    for(SVMXC__Service_Contract_Products__c lineItem : lstNew){
      if(lineItem.SVMXC__Installed_Product__c != null){
        setContractIds.add(lineItem.SVMXC__Service_Contract__c);
      }
    }
    
    if(setContractIds.isEmpty()){
      return;
    }
    map<Id,SVMXC__Service_Contract__c> mapContractIdToContract = new map<Id,SVMXC__Service_Contract__c>();
    
    Id rtCOMMContract = Constants.getRecordTypeId(Constants.RT_COMM_SERVICE_CONTRACT , Constants.SERVICE_CONTRACT_OBJECT);
    for(SVMXC__Service_Contract__c contract : [SELECT Id,SVMXC__End_Date__c,RecordType.Name 
                                                FROM SVMXC__Service_Contract__c 
                                                WHERE RecordTypeId =: rtCOMMContract
                                                AND Id IN :setContractIds]){
      mapContractIdToContract.put(contract.Id,contract);   
    }
    System.debug('mapContractIdToContract'+mapContractIdToContract);
    if(mapContractIdToContract.keySet().isEmpty()){
    	return;
    }
    List<SVMXC__Installed_Product__c> lstAssetToUpdate = new List<SVMXC__Installed_Product__c>();
    SVMXC__Installed_Product__c prod = null;
    for(SVMXC__Service_Contract_Products__c lineItem : lstNew){
      if(lineItem.SVMXC__Installed_Product__c != null && mapContractIdToContract.containsKey(lineItem.SVMXC__Service_Contract__c)){
      	prod = new SVMXC__Installed_Product__c(Id = lineItem.SVMXC__Installed_Product__c,Contract_Record_Type__c = mapContractIdToContract.get(lineItem.SVMXC__Service_Contract__c).RecordType.Name);
      	if(mapContractIdToContract.get(lineItem.SVMXC__Service_Contract__c).SVMXC__End_Date__c != null){
      		prod.SVMXC__Service_Contract_End_Date__c = mapContractIdToContract.get(lineItem.SVMXC__Service_Contract__c).SVMXC__End_Date__c;
      	}
        lstAssetToUpdate.add(prod);
      }
      
    }
    
    if(!lstAssetToUpdate.isEmpty()){
      update lstAssetToUpdate;
    }
    
  }
}