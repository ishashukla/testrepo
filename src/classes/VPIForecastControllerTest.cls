// (c) 2015 Appirio, Inc.
//
// Class Name: VPIForecastControllerTest
// Description: Test Class for VPIForecastController   
// July 8, 2016    Kajal Jalan    Original 
// July 17, 2016   Isha Shukla      Modified (I-226166)
@isTest
private class VPIForecastControllerTest {
    static Forecast_Year__c ForcastYearRep;
    static List<Opportunity> opp;
    static List<User> userList;
    static User rep, manager, SysAdmin, managerUser, repUser1, manUser1;
    static Id recordTypeRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast').getRecordTypeId();
    static Id recordTypeManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast').getRecordTypeId();
    // Method to test creating new forecast
    @isTest static void testRepRecordSubmittedview() { 
        testData();
        insert userList;
        /*User rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        rep.Division = 'IVS';
        rep.UserRoleId = role.Id;
        insert rep;
        User manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role2 = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        manager.Division = 'IVS';
        manager.UserRoleId = role2.Id;
        insert manager;
        User SysAdmin = TestUtils.createUser(1,'System Administrator',true).get(0);
        User managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'IVS';
        insert managerUser;
        User repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'IVS';
        insert repUser1;
        User manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'IVS';
        insert manUser1;*/
        // creating manacode records 
        System.runAs(SysAdmin) {
            List<Mancode__c> mancodeList = new List<Mancode__c>();
            mancodeList = TestUtils.createMancode(5, rep.Id, manager.Id, false);
            mancodeList[0].Business_Unit__c = 'IVS'; 
            mancodeList[0].Type__c = 'Sales Rep';
            mancodeList[1].Business_Unit__c = 'IVS';
            mancodeList[1].Sales_Rep__c =  manager.Id;
            mancodeList[1].Manager__c = managerUser.Id;
            mancodeList[1].Type__c = 'Regional Manager';
            mancodeList[2].Sales_Rep__c = managerUser.Id;
            mancodeList[2].Manager__c = manUser1.Id;
            mancodeList[2].Business_Unit__c = 'IVS';
            mancodeList[2].Type__c = 'Director';
            mancodeList[3].Manager__c = SysAdmin.Id;
            mancodeList[3].Sales_Rep__c = manUser1.Id;
            mancodeList[3].Business_Unit__c = 'IVS';
            mancodeList[3].Type__c = 'VP';
            mancodeList[4].Sales_Rep__c = SysAdmin.Id;
            mancodeList[4].Business_Unit__c = 'IVS';
            mancodeList[4].Type__c = 'VPI';
            insert mancodeList;
            List<Account> accountList = TestUtils.createAccount(1, false);
            accountList[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
            insert accountList;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'IVS';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = rep.Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            
        }
        // creating forecast records
        System.runAs(rep) {
            ForcastYearRep = TestUtils.createForcastYear(1,rep.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='IVS';
            ForcastYearRep.Year_External_Id__c= '1234';
            insert ForcastYearRep;
            insert opp;
        }
        System.runAs(manager) {
            Forecast_Year__c ForcastYearManager = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            ForcastYearManager.Year__c = String.valueOf(System.today().year()+1);
            ForcastYearManager.Business_Unit__c='IVS';
            ForcastYearManager.Year_External_Id__c= '12345';
            insert ForcastYearManager;
            Forecast_Year__c nfy = TestUtils.createForcastYear(1,manager.Id,recordTypeManager,false).get(0);
            nfy.Year__c = '2017';
            PageReference pageRef = Page.VPIForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', manager.Id); 
            System.currentPageReference().getParameters().put('managerView','true');
            System.currentPageReference().getParameters().put('fromASR','true');
            Test.startTest();
            VPIForecastController controller = new VPIForecastController();
            controller.updateForecast();
            controller.isRM = true;
            controller.selectedView = 'Submitted View';
            controller.nfy = nfy;
            controller.createNewForecastYear();
            controller.updateForecast();
            controller.getSalesRepforManager(manUser1.Id, true, false);
            controller.collapseList();
            controller.updateTotal();
            controller.save();
            controller.first();
            controller.last();
            controller.previous();
            controller.next();
            controller.forecastYr = 'New';
            controller.switchView();
            Test.stopTest();
        }
        System.assertEquals([SELECT Id FROM Forecast_Year__c WHERE User2__c = :manager.Id].size() > 0, true);
    } 
    // Testing when Surgical mancode hierarchy is used
    @isTest static void testSecond() { 
        testData();
        rep.Division = 'Surgical';
        manager.Division = 'Surgical';
        managerUser.Division = 'Surgical';
        repUser1.Division = 'Surgical';
        manUser1.Division = 'Surgical';
        insert userList;
        /*User rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        rep.Division = 'Surgical';
        rep.UserRoleId = role.Id;
        insert rep;
        User manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role2 = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        manager.Division = 'Surgical';
        manager.UserRoleId = role2.Id;
        insert manager;
        User SysAdmin = TestUtils.createUser(1,'System Administrator',true).get(0);
        User managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'Surgical';
        insert managerUser;
        User repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'Surgical';
        insert repUser1;
        User manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'Surgical';
        insert manUser1;*/
        // creating manacode records 
        System.runAs(SysAdmin) {
            List<Mancode__c> mancodeList = new List<Mancode__c>();
            mancodeList = TestUtils.createMancode(5, rep.Id, manager.Id, false);
            mancodeList[0].Business_Unit__c = 'Surgical'; 
            mancodeList[0].Type__c = 'Sales Rep';
            mancodeList[1].Business_Unit__c = 'Surgical';
            mancodeList[1].Sales_Rep__c =  manager.Id;
            mancodeList[1].Manager__c = managerUser.Id;
            mancodeList[1].Type__c = 'Regional Manager';
            mancodeList[2].Sales_Rep__c = managerUser.Id;
            mancodeList[2].Manager__c = manUser1.Id;
            mancodeList[2].Business_Unit__c = 'Surgical';
            mancodeList[2].Type__c = 'Director';
            mancodeList[3].Manager__c = SysAdmin.Id;
            mancodeList[3].Sales_Rep__c = manUser1.Id;
            mancodeList[3].Business_Unit__c = 'Surgical';
            mancodeList[3].Type__c = 'VP';
            mancodeList[4].Sales_Rep__c = SysAdmin.Id;
            mancodeList[4].Business_Unit__c = 'Surgical';
            mancodeList[4].Type__c = 'VPI';
            insert mancodeList;
            List<Account> accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'Surgical';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = rep.Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            opp[0].CloseDate = System.today();
            opp[0].Service_Monthly__c = 1.00;
            
        }
        // creating forecast records
         System.runAs(rep) {
            ForcastYearRep = TestUtils.createForcastYear(1,rep.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='Surgical';
            ForcastYearRep.Year_External_Id__c= '1234';
            insert ForcastYearRep;
            insert opp;
        }
        System.runAs(manager) {
            ForcastYearRep = TestUtils.createForcastYear(1,manager.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='Surgical';
            ForcastYearRep.Year_External_Id__c= '12345';
            insert ForcastYearRep;
        }
        System.runAs(managerUser) {
            ForcastYearRep = TestUtils.createForcastYear(1,managerUser.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='Surgical';
            ForcastYearRep.Year_External_Id__c= '12346';
            insert ForcastYearRep;
        }
        System.runAs(manUser1) {
            Forecast_Year__c ForcastYearManager = TestUtils.createForcastYear(1,manUser1.Id,recordTypeManager,false).get(0);
            ForcastYearManager.Year__c = String.valueOf(System.today().year());
            ForcastYearManager.Business_Unit__c='Surgical';
            ForcastYearManager.Year_External_Id__c= '12347';
            insert ForcastYearManager;
            Forecast_Year__c nfy = TestUtils.createForcastYear(1,manUser1.Id,recordTypeManager,false).get(0);
            nfy.Year__c = '201';
            PageReference pageRef = Page.VPIForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', manUser1.Id); 
            System.currentPageReference().getParameters().put('managerView','true');
            System.currentPageReference().getParameters().put('fromASR','true');
            Test.startTest();
            VPIForecastController controller = new VPIForecastController();
            controller.selectedView = 'Summary View';
            controller.nfy = nfy;
            controller.createNewForecastYear();
            controller.updateForecast();
            controller.collapseList();
            controller.updateTotal();
            Boolean hasNext = controller.hasNext;
            Boolean hasPrevious = controller.hasPrevious;
            Integer pageNumber = controller.pageNumber;
            controller.getColumnsToDisplay();
            controller.getSalesRepforManager(SysAdmin.Id, false, true);
            controller.getMissingForecast();
            controller.cancelNew();
            Test.stopTest();
        } 
        System.assertEquals([SELECT Id FROM Forecast_Year__c WHERE User2__c = :manUser1.Id].size() > 0, true);
    }
    // Testing when NSE mancode hierarchy is used 
    @isTest static void testThird() { 
        testData();
        rep.Division = 'NSE';
        manager.Division = 'NSE';
        managerUser.Division = 'NSE';
        repUser1.Division = 'NSE';
        manUser1.Division = 'NSE';
        insert userList;
        /*User rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        rep.Division = 'NSE';
        rep.UserRoleId = role.Id;
        insert rep;
        User manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role2 = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        manager.Division = 'NSE';
        manager.UserRoleId = role2.Id;
        insert manager;
        User SysAdmin = TestUtils.createUser(1,'System Administrator',false).get(0);
        SysAdmin.Division = 'NSE';
        insert SysAdmin;
        User managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'NSE';
        insert managerUser;
        User repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'NSE';
        insert repUser1;
        User manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'NSE';
        insert manUser1;*/
        // creating manacode records 
        System.runAs(SysAdmin) {
            List<Mancode__c> mancodeList = new List<Mancode__c>();
            mancodeList = TestUtils.createMancode(5, rep.Id, manager.Id, false);
            mancodeList[0].Business_Unit__c = 'NSE'; 
            mancodeList[0].Type__c = 'Sales Rep';
            mancodeList[1].Business_Unit__c = 'NSE';
            mancodeList[1].Sales_Rep__c =  manager.Id;
            mancodeList[1].Manager__c = managerUser.Id;
            mancodeList[1].Type__c = 'Regional Manager';
            mancodeList[2].Sales_Rep__c = managerUser.Id;
            mancodeList[2].Manager__c = manUser1.Id;
            mancodeList[2].Business_Unit__c = 'NSE';
            mancodeList[2].Type__c = 'Director';
            mancodeList[3].Manager__c = SysAdmin.Id;
            mancodeList[3].Sales_Rep__c = manUser1.Id;
            mancodeList[3].Business_Unit__c = 'NSE';
            mancodeList[3].Type__c = 'VP';
            mancodeList[4].Sales_Rep__c = SysAdmin.Id;
            mancodeList[4].Business_Unit__c = 'NSE';
            mancodeList[4].Type__c = 'VPI';
            insert mancodeList;
            List<Account> accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = rep.Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            opp[0].CloseDate = System.today();
            opp[0].StageName = 'Closed Won';
            opp[0].ForecastCategoryName = 'Closed';
            opp[0].Service_Monthly__c = 1.00;
            opp[0].Total_Trailing_Impact__c =1.00;
            
        }
        // creating forecast records
        System.runAs(rep) {
            ForcastYearRep = TestUtils.createForcastYear(1,rep.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='NSE';
            ForcastYearRep.Year_External_Id__c= '1234';
            insert ForcastYearRep;
            insert opp;
        }
        System.runAs(manager) {
            ForcastYearRep = TestUtils.createForcastYear(1,manager.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='NSE';
            ForcastYearRep.Year_External_Id__c= '12345';
            insert ForcastYearRep;
        }
        System.runAs(managerUser) {
            ForcastYearRep = TestUtils.createForcastYear(1,managerUser.Id,recordTypeRep,false).get(0);
            ForcastYearRep.Year__c = String.valueOf(System.today().year());
            ForcastYearRep.Business_Unit__c='NSE';
            ForcastYearRep.Year_External_Id__c= '12346';
            insert ForcastYearRep;
        }
        System.runAs(manUser1) {
            Forecast_Year__c ForcastYearManager = TestUtils.createForcastYear(1,manUser1.Id,recordTypeManager,false).get(0);
            ForcastYearManager.Year__c = String.valueOf(System.today().year());
            ForcastYearManager.Business_Unit__c='NSE';
            ForcastYearManager.Year_External_Id__c= '12347';
            insert ForcastYearManager;
            Forecast_Year__c nfy = TestUtils.createForcastYear(1,manUser1.Id,recordTypeManager,false).get(0);
            nfy.Year__c = String.valueOf(System.today().year());
            PageReference pageRef = Page.VPIForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', manUser1.Id); 
            System.currentPageReference().getParameters().put('managerView','true');
            System.currentPageReference().getParameters().put('fromASR','true');
            Test.startTest();
            VPIForecastController controller = new VPIForecastController();
            controller.updateForecast();
            controller.selectedView = 'Summary View';
            controller.nfy = nfy;
            controller.createNewForecastYear();
            controller.updateForecast();
            controller.updateTotal();
            controller.getColumnsToDisplay();
            controller.getSalesRepforManager(manUser1.Id, true, false);
            controller.getMissingForecast();
            controller.cancelNew();
            Test.stopTest();
        }
      System.assertEquals([SELECT Id FROM Forecast_Year__c WHERE User2__c = :manUser1.Id].size() > 0, true);  
    }

    static void testData() {
        userList = new List<User>();

        rep = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        UserRole role = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        rep.Division = 'IVS';
        rep.UserRoleId = role.Id;
        userList.add(rep);

        manager = TestUtils.createUser(1,'Instruments Sales Ops',false).get(0);
        //UserRole role2 = [select Id from userRole where Name LIKE 'Flex Business Admin'].get(0);
        manager.Division = 'IVS';
        manager.UserRoleId = role.Id;
        userList.add(manager);

        SysAdmin = TestUtils.createUser(1,'System Administrator',false).get(0);
        userList.add(SysAdmin);

        managerUser = TestUtils.createUser(1,'Instruments Sales Manager',false).get(0);
        managerUser.Division = 'IVS';
        userList.add(managerUser);

        repUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        repUser1.Division = 'IVS';
        userList.add(repUser1);
        
        manUser1 = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
        manUser1.Division = 'IVS';
        userList.add(manUser1);
    } 
}