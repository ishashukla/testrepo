/**================================================================      
* Appirio, Inc
* Name: COMM_CaseUtil
* Description: Util Class for Case Trigger Handlers COMM BP : I-235428
* Created Date: 20th Sept 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update

==================================================================*/
// Check if owner id is UserId type.
public class COMM_CaseUtil {
    public static boolean IsUser(String ownerId)
    {
        if(ownerId != null && ownerId.left(3) == '005')
        {
            return true;
        }
        return false;
    }

}