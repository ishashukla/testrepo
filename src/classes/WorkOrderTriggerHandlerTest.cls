@isTest
public class WorkOrderTriggerHandlerTest {
    public testMethod static void testworkorder()
    {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        
        User u =  TestUtils.createUser(1, 'System Administrator', true).get(0);
        
        SVMXC__Service_Group__c sgroup = new SVMXC__Service_Group__c();
        insert sgroup;
        SVMXC__Service_Group_Members__c grpmem= new SVMXC__Service_Group_Members__c(SVMXC__Salesforce_User__c= u.Id, SVMXC__Service_Group__c= sgroup.Id);
        insert grpmem;
        SVMXC__Site__c location = new SVMXC__Site__c();
        insert location;
        SVMXC__Site__c location2 = new SVMXC__Site__c();
        insert location2;
        List<SVMXC__Service_Order__c> lst=new List<SVMXC__Service_Order__c>();
        Id WorkorderrecordTypeId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        lst=TestUtils.createServiceOrder(WorkorderrecordTypeId,10);
        system.debug('>>>>>>>>>lst' + lst);	
        for(SVMXC__Service_Order__c ord:lst){
            ord.SVMXC__Order_Status__c = 'Open';
            ord.SVMXC__Site__c = location.ID;
        }
        Test.startTest();
        insert lst;
        
        for(SVMXC__Service_Order__c ord:lst){
            ord.IsSubmitforApproval__c=true;
            ord.SVMXC__Site__c = location2.ID;
        }
        update lst;
        Test.stopTest();
        
    }
    public testMethod static void testworkorder2()
    {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Account acc = TestUtils.createAccount(1, true).get(0);
        SVMXC__Site__c location = new SVMXC__Site__c();
        insert location;
        SVMXC__Site__c location2 = new SVMXC__Site__c();
        insert location2;
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeID = rtCase;
        caseRecord.SVMXC__Site__c = location.Id;
        insert caseRecord;
        List<SVMXC__Service_Order__c> lst=new List<SVMXC__Service_Order__c>();
        Id WorkorderrecordTypeId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        lst=TestUtils.createServiceOrder(WorkorderrecordTypeId,10);
        system.debug('>>>>>>>>>lst' + lst);	
        for(SVMXC__Service_Order__c ord:lst){
            ord.SVMXC__Order_Status__c = 'Open';
            ord.SVMXC__Site__c = location.ID;
            ord.SVMXC__Case__c = caseRecord.Id;
        }
        lst[0].SVMXC__Case__c = null;
        Test.startTest();
        insert lst;
        
        for(SVMXC__Service_Order__c ord:lst){
            ord.SVMXC__Site__c = location2.ID;
        }
        update lst;
        Test.stopTest();
    }
    @isTest static void testUpdate1() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Account acc = TestUtils.createAccount(1, true).get(0);
        SVMXC__Site__c location = new SVMXC__Site__c();
        insert location;
        SVMXC__Site__c location2 = new SVMXC__Site__c();
        insert location2;
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeID = rtCase;
        caseRecord.SVMXC__Site__c = location.Id;
        insert caseRecord;
        Project_Plan__c projectPlan = TestUtils.createProjectPlan(1, acc.Id, false).get(0);
        projectPlan.Engineering_Approval_Status__c = 'Approved';
        insert projectPlan;
        Project_Phase__c projectPhase = TestUtils.createProjectPhase(1, projectPlan.Id, true).get(0);
        Project_Phase__c projectPhase2 = TestUtils.createProjectPhase(1, projectPlan.Id, true).get(0);
        List<SVMXC__Service_Order__c> lst=new List<SVMXC__Service_Order__c>();
        Id WorkorderrecordTypeId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        lst=TestUtils.createServiceOrder(WorkorderrecordTypeId,10);
        system.debug('>>>>>>>>>lst' + lst);	
        for(SVMXC__Service_Order__c ord:lst){
            ord.SVMXC__Order_Status__c = 'Open';
            ord.SVMXC__Site__c = location.ID;
            ord.SVMXC__Case__c = caseRecord.Id;
            ord.IsSubmitforApproval__c = false;
        }
        lst[0].SVMXC__Case__c = null;
        lst[1].Project_Phase__c = projectPhase.Id;
        lst[2].Project_Phase__c = projectPhase2.Id;
        insert lst;
        Test.startTest();
        lst[1].IsSubmitforApproval__c = true;
        update lst;
        Test.stopTest();
        System.assertEquals([SELECT Milestone__c FROM Project_Phase__c WHERE Id = :projectPhase.Id ].Milestone__c,'Schedule Requested');
    }
    @isTest(SeeAllData=true)
    static void testActivity() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        Account acc = TestUtils.createAccount(1, true).get(0);
        SVMXC__Site__c location = new SVMXC__Site__c();
        insert location;
        SVMXC__Site__c location2 = new SVMXC__Site__c();
        insert location2;
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeID = rtCase;
        caseRecord.SVMXC__Site__c = location.Id;
        insert caseRecord;
        Project_Plan__c projectPlan = TestUtils.createProjectPlan(1, acc.Id, false).get(0);
        projectPlan.Engineering_Approval_Status__c = 'Approved';
        insert projectPlan;
        User u =  TestUtils.createUser(1, 'System Administrator', true).get(0);
        SVMXC__Service_Group__c sgroup = new SVMXC__Service_Group__c();
        insert sgroup;
        SVMXC__Service_Group_Members__c grpmem= new SVMXC__Service_Group_Members__c(SVMXC__Salesforce_User__c= u.Id, SVMXC__Service_Group__c= sgroup.Id);
        insert grpmem;
        User u2 =  TestUtils.createUser(1, 'System Administrator', true).get(0);
        SVMXC__Service_Group__c sgroup2 = new SVMXC__Service_Group__c();
        insert sgroup2;
        SVMXC__Service_Group_Members__c grpmem2= new SVMXC__Service_Group_Members__c(SVMXC__Salesforce_User__c= u2.Id, SVMXC__Service_Group__c= sgroup2.Id);
        insert grpmem2;
        Project_Phase__c projectPhase = TestUtils.createProjectPhase(1, projectPlan.Id, true).get(0);
        Project_Phase__c projectPhase2 = TestUtils.createProjectPhase(1, projectPlan.Id, true).get(0);
        List<SVMXC__Service_Order__c> lst=new List<SVMXC__Service_Order__c>();
        Id WorkorderrecordTypeId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        lst=TestUtils.createServiceOrder(WorkorderrecordTypeId,10);
        system.debug('>>>>>>>>>lst' + lst);	
        for(SVMXC__Service_Order__c ord:lst){
            ord.SVMXC__Order_Status__c = 'Open';
            ord.SVMXC__Site__c = location.ID;
            ord.SVMXC__Case__c = caseRecord.Id;
            ord.IsSubmitforApproval__c = false;
        }
        lst[0].SVMXC__Case__c = null;
        lst[1].Project_Phase__c = projectPhase.Id;
        lst[2].Project_Phase__c = projectPhase2.Id;
        lst[5].SVMXC__Group_Member__c = grpmem.Id;
        Test.startTest();
        insert lst;
        Field_Service_Investigation__c fsi = new Field_Service_Investigation__c(
        Work_Order__c = lst[0].ID,B_Repair_Fix_the_Reported_Issue__c = 'Y',
            C_Steps_How_was_Issue_Resolved__c ='test',D_Steps_How_was_the_Fix_Verified__c='test'
        );
        insert fsi;
        List<Task> tasks = new List<Task>();
        tasks.add(new Task(
            ActivityDate = Date.today().addDays(7),
            Subject='Sample Task',
            WhatId = lst[0].Id,
            OwnerId = UserInfo.getUserId(),
            Status='In Progress'));
        insert tasks;
        lst[0].SVMXC__Order_Status__c = 'Closed';
        lst[5].SVMXC__Group_Member__c = grpmem2.Id;
        Database.SaveResult[] srList = Database.update(lst, false);
        Test.stopTest();
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.assertEquals('There are some Open Activities on this Work Order so you can not close it.', err.getMessage());
                }
            }
        }
        
    }
    @isTest static void testFSI() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Account acc = TestUtils.createAccount(1, true).get(0);
        SVMXC__Site__c location = new SVMXC__Site__c();
        insert location;
        SVMXC__Site__c location2 = new SVMXC__Site__c();
        insert location2;
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeID = rtCase;
        caseRecord.SVMXC__Site__c = location.Id;
        insert caseRecord;
        Project_Plan__c projectPlan = TestUtils.createProjectPlan(1, acc.Id, false).get(0);
        projectPlan.Engineering_Approval_Status__c = 'Approved';
        insert projectPlan;
        Project_Phase__c projectPhase = TestUtils.createProjectPhase(1, projectPlan.Id, true).get(0);
        Project_Phase__c projectPhase2 = TestUtils.createProjectPhase(1, projectPlan.Id, true).get(0);
        List<SVMXC__Service_Order__c> lst=new List<SVMXC__Service_Order__c>();
        Id WorkorderrecordTypeId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        lst=TestUtils.createServiceOrder(WorkorderrecordTypeId,10);
        system.debug('>>>>>>>>>lst' + lst);	
        for(SVMXC__Service_Order__c ord:lst){
            ord.SVMXC__Order_Status__c = 'Open';
            ord.SVMXC__Site__c = location.ID;
            ord.SVMXC__Case__c = caseRecord.Id;
            ord.IsSubmitforApproval__c = false;
        }
        lst[0].SVMXC__Case__c = null;
        lst[1].Project_Phase__c = projectPhase.Id;
        lst[2].Project_Phase__c = projectPhase2.Id;
        insert lst;
        Test.startTest();
        lst[0].SVMXC__Order_Status__c = 'Closed';
        Database.SaveResult[] srList = Database.update(lst, false);
        Test.stopTest();
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Success' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.assertEquals('A Field Service Investigation must be completed before closing this Work Order', err.getMessage());
                }
            }
        }
        
    }
}