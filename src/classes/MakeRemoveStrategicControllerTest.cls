// (c) 2015 Appirio, Inc.
//
// Class Name: MakeRemoveStrategicControllerTest
// Description: Test Class for MakeRemoveStrategicController
// June 28 2016, Prakarsh Jain  Original (T-514072)
//
@isTest
public class MakeRemoveStrategicControllerTest {
  public static Account acc;
  public static Strategic_Account__c stratAccount;
  
  public static List<Messages__c> customSettingList; 
  
  //This Test method tests what should happen if an Account already marked as Strategic for the logged in User
  static testMethod void testMakeRemoveStrategicController(){
    createTestData();
    List<Messages__c> lstCS = new List<Messages__c>();
    Messages__c msgs = new Messages__c();
    msgs.Name = 'PreviouslyMarkedAsStrategic';
    msgs.Message_Text__c = 'You have previously marked this Account as Strategic, would you like to remove the Strategic Indicator?';
    lstCS.add(msgs);
    insert lstCS;
    MakeRemoveStrategicController makeRemoveStratAccount = new MakeRemoveStrategicController();  
    PageReference pageRef = Page.MakeRemoveStrategicPage;
    Test.setCurrentPage(pageRef);  
    System.currentPageReference().getParameters().put('accId', String.valueOf(acc.Id));
    stratAccount = new Strategic_Account__c();      
    stratAccount.Account__c = makeRemoveStratAccount.accId;
    stratAccount.User__c = UserInfo.getUserId();
    insert stratAccount;
    makeRemoveStratAccount.insertRemove();
    system.assertEquals(stratAccount.Account__c, makeRemoveStratAccount.accId);
    //makeRemoveStratAccount.removeStrategic();
    makeRemoveStratAccount.refreshPage();  
  }
  
  //This Test method tests what should happen if an Account is not marked as Strategic for the logged in User  
  static testMethod void testMakeRemoveStrategicController1(){
    createTestData();
    List<Messages__c> lstCS = new List<Messages__c>();  
    Messages__c msgs = new Messages__c();
    msgs.Name = 'AccountMadeStrategicSuccess';
    msgs.Message_Text__c = 'The account has been made a strategic account.';
    lstCS.add(msgs);
    insert lstCS;  
    MakeRemoveStrategicController makeRemoveStratAccount = new MakeRemoveStrategicController();  
    PageReference pageRef = Page.MakeRemoveStrategicPage;
    Test.setCurrentPage(pageRef);  
    System.currentPageReference().getParameters().put('accId', String.valueOf(acc.Id));
    
    makeRemoveStratAccount.insertRemove();
    makeRemoveStratAccount.removeStrategic();
    system.assertEquals(null, makeRemoveStratAccount.accId);  
    makeRemoveStratAccount.refreshPage();  
  }  
  
  public static void createTestData(){
    acc = TestUtils.createAccount(1, true).get(0);
  }
}