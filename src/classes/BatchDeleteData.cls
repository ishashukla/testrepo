/*
* Class : BatchDeleteData 
* Created By : Sunil
* Created Date : Jun 17, 2014
* Updated By : Will, added Instructions
* Updated Date: Jun 20, 2014
* Purpose : Batch class that clears a table 
* Instructions : 1 Update OBJECT_NAME, OBJECT_FIELD, SAMPLE_VALUE text with the object, and where criteria you need.
* Instructions : 2 Save the Class
* Instructions : 3 Run the following in Developer Console: Database.executeBatch(new BatchDeleteData());
*/
global class BatchDeleteData implements Database.Batchable<sObject>{
    //Afelix make custom setting
    User__c myCSU = User__c.getValues('BatchDeleteData');
    String myUserVal = myCSU.User__c;
    public String query = 'Select Id from Attachment';
   //Start the batch
   global Database.QueryLocator start(Database.BatchableContext BC){
      //return Database.getQueryLocator('Select Id from ACCOUNT WHERE CreatedById = \'005i0000003GOh0\'');
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
     delete scope;
   }

   global void finish(Database.BatchableContext BC){
   }

}