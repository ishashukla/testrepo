// (c) 2016 Appirio, Inc.
//
// Class Name: OmittedNot_OmittedControllerTest
// Description: Test Class for OmittedNot_OmittedController
// 
// July 5 2016, Isha Shukla  Original 
//
@isTest
private class OmittedNot_OmittedControllerTest {
    // Testing when opportunity stage is In Development and Forecast Category is Pipeline
    @isTest static void testFirst() {
        User usr = TestUtils.createUser(1, 'System Administrator', false).get(0);
        usr.Division = 'NSE';
        insert usr;
        System.runAs(usr) {
            Test.startTest();
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            Account acc = TestUtils.createAccount(1,true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1, acc.Id, false).get(0);
            opp.CloseDate = System.today();
            opp.Business_Unit__c = 'NSE';
            opp.recordTypeId = recordTypeInstrument;
            opp.StageName = 'In Development';
            opp.ForecastCategoryName = 'Pipeline';
            insert opp;
            PageReference pageRef = Page.OmittedNot_Omitted;
		    Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);  
            OmittedNot_OmittedController controller = new OmittedNot_OmittedController(sc);
            controller.omitNotOmit();
            Test.stopTest();
            System.assertEquals([SELECT ForecastCategoryName FROM Opportunity WHERE Id = :opp.Id].ForecastCategoryName, 'Omitted');
        }
    }
     // Testing when opportunity stage is In Development and Forecast Category is Omitted
    @isTest static void testSecond() {
        User usr = TestUtils.createUser(1, 'System Administrator', false).get(0);
        usr.Division = 'NSE';
        insert usr;
        System.runAs(usr) {
            Test.startTest();
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            Account acc = TestUtils.createAccount(1,true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1, acc.Id, false).get(0);
            opp.CloseDate = System.today();
            opp.Business_Unit__c = 'NSE';
            opp.recordTypeId = recordTypeInstrument;
            opp.StageName = 'In Development';
            opp.ForecastCategoryName = 'Omitted';
            insert opp;
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);  
            OmittedNot_OmittedController controller = new OmittedNot_OmittedController(sc);
            controller.omitNotOmit();
            Test.stopTest();
            System.assertEquals([SELECT ForecastCategoryName FROM Opportunity WHERE Id = :opp.Id].ForecastCategoryName, 'Pipeline');
        }
    }
     // Testing when opportunity stage is Closed Won and Forecast Category is Omitted
    @isTest static void testThird() {
        User usr = TestUtils.createUser(1, 'System Administrator', false).get(0);
        usr.Division = 'NSE';
        insert usr;
        System.runAs(usr) {
            Test.startTest();
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            Account acc = TestUtils.createAccount(1,true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1, acc.Id, false).get(0);
            opp.CloseDate = System.today();
            opp.Business_Unit__c = 'NSE';
            opp.recordTypeId = recordTypeInstrument;
            opp.StageName = 'Closed Won';
            opp.ForecastCategoryName = 'Omitted';
            insert opp;
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);  
            OmittedNot_OmittedController controller = new OmittedNot_OmittedController(sc);
            controller.omitNotOmit();
            Test.stopTest();
            System.assertEquals([SELECT ForecastCategoryName FROM Opportunity WHERE Id = :opp.Id].ForecastCategoryName, 'Closed');
        }
    }
}