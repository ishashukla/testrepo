// (c) 2015 Appirio, Inc.
//
// Class Name: RecordTypeSelectionControllerTest
// Description: Test Class for RecordTypeSelectionController   
// April 15, 2016    Meena Shringi    Original
//
@isTest
private class RecordTypeSelectionControllerTest
{
    @isTest
    static void testRecordTypeSelectionController()
    {   Schema.RecordTypeInfo rtByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional');
        Id flexRecordTypeId = rtByName.getRecordTypeId();
        User usr = TestUtils.createUser(1, 'System Administrator', false).get(0);
        usr.Division = 'NSE';
     System.runAs(usr) {
         PageReference pageRef = Page.RecordTypeSelection;
        Test.setCurrentPage(pageRef);       
        Account acc = TestUtils.createAccount(1, true).get(0);
        TestUtils.createCommConstantSetting();
        Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
         opp.recordTypeId = flexRecordTypeId;
         Opp.Business_Unit__c = 'NSE';
         insert opp;
         System.currentPageReference().getParameters().put('id', String.valueOf(opp.Id));
         System.assert(System.currentPageReference().getParameters().get('id')!= null,'Id must not be null');
         System.currentPageReference().getParameters().put('retURL',String.valueOf(opp.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        RecordTypeSelectionController contrl = new RecordTypeSelectionController(sc);
         contrl.selectedOppRecordType = 'Flex Opportunity - Conventional';
         contrl.selectRecordType();
         contrl.cancel();
     }
    }
}