@isTest

private class BigMachinesChooseQuoteToCloneExt_Test{

    @isTest
    static void testGetQuoteListFromOpty() {
        //BigMachinesController controller = new BigMachinesController();
        
        // Create an opportunity record.
        List<Pricebook2> priceBookList = TestUtils.createpricebook(2,' ',false);
        priceBookList.get(0).Name = 'COMM Price Book';
        insert priceBookList;
        Opportunity opty = new Opportunity();
        opty.Name = 'BigMachines test Opportunity for testGetQuoteList';
        opty.StageName = 'Prospecting';
        opty.CloseDate = Date.today();
        insert opty;
        
        // Create Oracle Quote record.
        List<BigMachines__Configuration_Record__c> bigMachinesConfigList = TestUtils.createBigMachineConfig(2,true);
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'BigMachines test quote for testGetQuoteListFromOpty';
        quote.BigMachines__Opportunity__c = opty.id;
        quote.BigMachines__Site__c = bigMachinesConfigList.get(0).Id;
        insert quote;
        
        ApexPages.StandardSetController stdSetCtrl = new ApexPages.StandardSetController([select id from BigMachines__Quote__c limit 1]);
        ApexPages.currentPage().getParameters().put('oppId', opty.id);
        BigMachinesChooseQuoteToCloneExtension controller = new BigMachinesChooseQuoteToCloneExtension(stdSetCtrl);
        controller.getOppName();
        controller.getOppId();
        controller.getStartOfRange();
        controller.getEndOfRange();
        controller.cloneQuote();
        controller.setSelectedQuoteId(quote.id);
        controller.getSelectedQuoteId();
        controller.cloneQuote();
        controller.cancel();
    }
    
 }