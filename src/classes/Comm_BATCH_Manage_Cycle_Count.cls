/**================================================================      
* Appirio, Inc
* Name: [Comm_BATCH_Manage_Cycle_Count]
* Description: [batch Class for Creating cycle Count Header records]
* Created Date: [14-09-2016]
* Created By: [Gaurav Sinha] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 14 Sep 2016        Gaurav Sinha      T-509402
* 14 Nov 2016        Isha Shukla       S-399758
==================================================================*/
public class Comm_BATCH_Manage_Cycle_Count implements Database.Batchable<SObject>, Database.Stateful{
    Public Cycle_Count__c varCycleCount;
    public set<id> productId;
    public String query = 'SELECT SVMXC__Inventory_Location__c ,SVMXC__Email__c,SVMXC__Salesforce_User__c,SVMXC__Salesforce_User__r.Managerid FROM SVMXC__Service_Group_Members__c WHERE svmxc__Active__c = true';
    /*
* @method:  Comm_BATCH_Manage_Cycle_Count
* @param : query : to reinitalize the query string
* @Description: Constructor for reintialze the Query.
*/
    public Comm_BATCH_Manage_Cycle_Count(String query,Cycle_Count__c varCycleCount,set<id> productId){
        this.query = query;
        this.varCycleCount = varCycleCount;
        this.productId = productId;
    }
    
    /*
* @method:  Comm_BATCH_Manage_Cycle_Count
* @param : query : to reinitalize the query string
* @Description: Constructor for reintialze the Query.
*/
    public Comm_BATCH_Manage_Cycle_Count(Cycle_Count__c varCycleCount,set<id> productId){
        this.varCycleCount = varCycleCount;
        this.productId = productId;
    }
    /*
* @method:  finish
* @param : context : Database.BatchableContext
* @Description: Method implemented for the interface Schedulable
*/
    public void finish(Database.BatchableContext context) {
    }
    /*
* @method:  start
* @param : context : Database.BatchableContext
* @Description: Method implemented for the interface Batchable to get the records on which the batch processing needs to be applied.
*/
    public Database.QueryLocator start(Database.BatchableContext context){
        return  Database.getQueryLocator(query);
    }
    /*
* @method:  execute
* @param : context : Database.BatchableContext : Batch Context
* @param : objects : List<SVMXC__Warranty__c> : List of obejct returned in the batch context.
* @Description: Method implemented for the interface Batchable to apply the processing of copying the records from Opportunity_Period__c to Opportunity_Forecast_Snapshot__c
*/
    public void execute(Database.BatchableContext context, List<SVMXC__Service_Group_Members__c> objects){
        set<id> TempLocation = new Set<Id>();
        Savepoint sp = Database.setSavepoint();
        try{
            List<Cycle_Count__c> listHeaderToinsert = new List<Cycle_Count__c>();
            Map<String,SVMXC__Service_Group_Members__c> localLocation = new Map<string,SVMXC__Service_Group_Members__c>();
            List<SVMXC__Service_Group_Members__c> holder = new List<SVMXC__Service_Group_Members__c>();
            Map<Id,Id> techToLocationMap = new Map<Id,Id>();
            for(SVMXC__Service_Group_Members__c varloop: objects){
                localLocation.put(varloop.id,varloop);
                holder.add(varloop);
                if(varloop.SVMXC__Inventory_Location__c!=null) {
                    TempLocation.add(varloop.SVMXC__Inventory_Location__c);
                    if(!techToLocationMap.containsKey(varloop.Id)) {
                        techToLocationMap.put(varloop.Id,varloop.SVMXC__Inventory_Location__c);
                    }
                }
                
            }
            Cycle_Count__c temCycleCount = new Cycle_Count__c();
            for(SVMXC__Service_Group_Members__c varLoop : holder){
                temCycleCount = new Cycle_Count__c();
                temCycleCount.Technician__c = varloop.id;
                temCycleCount.Technician_Email__c = localLocation.get(varLoop.id).SVMXC__Email__c;
                temCycleCount.Technician_Salesforce_user__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__c;
                temCycleCount.Technician_Salesforce_user_Manger__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__r.Managerid;
                temCycleCount.Location__c =localLocation.get(varLoop.id).SVMXC__Inventory_Location__c;
                temCycleCount.name = varCycleCount.name;
                temCycleCount.status__c = varCycleCount.status__c;
                temCycleCount.Division__c = varCycleCount.Division__c;
                temCycleCount.Due_Date__c = varCycleCount.Due_Date__c;
                temCycleCount.Service_Team__c = varCycleCount.Service_Team__c;
                listHeaderToinsert.add(temCycleCount);
            }
            list<Cycle_Count_Line__c> lineItemsTOinserted = new List<Cycle_Count_Line__c>();
            if(!listHeaderToinsert.isEmpty()){
                insert listHeaderToinsert;
            }
            
            Map<String,SVMXC__Product_Stock__c> mapProductQUantity = new Map<String,SVMXC__Product_Stock__c>();
            if(productId == null){
                system.debug('quaterly');
                for(SVMXC__Product_Stock__c varLoop : [SELECT id,SVMXC__Product__c,SVMXC__Location__c,On_Hand_Qty__c,SVMXC__Quantity2__c 
                                                       FROM SVMXC__Product_Stock__c
                                                       WHERE SVMXC__Location__r.Exclude_Location_from_Inventory__c = false
                                                       AND SVMXC__Location__c in : TempLocation
                                                       AND  SVMXC__Quantity2__c > 1
                                                       AND SVMXC__Product__r.include_in_cycle_counts__c = true
                                                      ]){
                                                          for(Cycle_Count__c var: listHeaderToinsert){
                                                              Cycle_Count_Line__c cycleLine = new Cycle_Count_Line__c();
                                                              cycleLine.Cycle_Count__c = var.Id;
                                                              cycleLine.Product__c = varloop.SVMXC__Product__c;
                                                              if(techToLocationMap.containsKey(var.Technician__c) && techToLocationMap.get(var.Technician__c) == varloop.SVMXC__Location__c) {
                                                                  cycleLine.Product_Stock__c = varloop.Id;
                                                                  cycleLine.Expected_Quantity__c = varloop.On_Hand_Qty__c;
                                                              }
                                                              lineItemsTOinserted.add(cycleLine);
                                                          }
                                                      }
            }
            else{
                system.debug('monthly');
                Map<String,SVMXC__Product_Stock__c> dictionaryProductStock = new Map<String,SVMXC__Product_Stock__c>();
                for(SVMXC__Product_Stock__c varLoop : [SELECT id,SVMXC__Product__c,SVMXC__Location__c,On_Hand_Qty__c  
                                                       FROM SVMXC__Product_Stock__c
                                                       WHERE SVMXC__Location__r.Exclude_Location_from_Inventory__c = false
                                                       AND SVMXC__Location__c in : TempLocation
                                                       AND  SVMXC__Product__c in: productId
                                                       AND SVMXC__Product__r.include_in_cycle_counts__c = true
                                                      ]){
                                                          if(!dictionaryProductStock.containsKey(varLoop.SVMXC__Location__c+'+'+varLoop.SVMXC__Product__c)) {
                                                              dictionaryProductStock.put(varLoop.SVMXC__Location__c+'+'+varLoop.SVMXC__Product__c,varLoop);                            
                                                          }
                                                      }
                for(Cycle_Count__c cycleCount: listHeaderToinsert){
                    
                    for(Id prod : productId) {
                        Cycle_Count_Line__c cycleLine = new Cycle_Count_Line__c();
                        cycleLine.Cycle_Count__c = cycleCount.id;
                        cycleLine.Product__c = prod;
                        if(dictionaryProductStock.containsKey(cycleCount.Location__c+'+'+prod)) {
                            cycleLine.Product_Stock__c = dictionaryProductStock.get(cycleCount.Location__c+'+'+prod).Id;
                            cycleLine.Expected_Quantity__c = dictionaryProductStock.get(cycleCount.Location__c+'+'+prod).On_Hand_Qty__c;
                        }
                        lineItemsTOinserted.add(cycleLine);
                    }  
                    
                }
            }
            if(!lineItemsTOinserted.isEmpty()){
                insert lineItemsTOinserted;
            }
            createWorkorder(listHeaderToinsert);
        }
        catch(Exception e){
            system.debug('exception>>'+e.getMessage());
            Database.rollback(sp);
            return;
        }
    }
    
    public void createWorkorder(List<Cycle_Count__c> listHeaderToinser){
        String rtID ='';
        for(recordtype var : [select id from recordtype where developername = 'Inventory_Management' limit 1]){
            rtid = var.id;
        }
        List<SVMXC__Service_Order__c> LiWO = new list<SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c varWO = new SVMXC__Service_Order__c();
        for(Cycle_Count__c varLoop:listHeaderToinser ){
            varWO = new SVMXC__Service_Order__c();
            varWO.RecordTypeid= rtid;
            varwo.Cycle_Count__c = varCycleCount.id; 
            LiWO.add(varwo);
        }
        if(!LiWO.isEmpty()){
            insert liWO;    
        }        
    }    
}