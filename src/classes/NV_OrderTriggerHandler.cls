/*************************************************************************************************
* Created By:    Durgesh Dhoot
* Date:          Aug 26, 2015
* Description  : Order Trigger Handler
* Modified     : July 19, 2016   Parul Gupta  T-520738
* 06 Sep 2016    Nathalie Le Guay     I-233892 Added RT check so Status__c = 'Entered' doesn't run for closed Endo Orders
* 16 Sep 2016    Nathalie Le Guay     I-234077 Adding logic so that EffectiveDate is not updated for Endo orders
// 21st Oct 2016     Nitish Bansal       I-240941 // TOO Many SOQL resolution (Proper list checks before SOQL and DML)
**************************************************************************************************/
public with sharing class NV_OrderTriggerHandler {
  public static Boolean IsToProcessAfterUpdate = true;
  public static Set<String> recordTypeId_bypassNVSet = new Set<String>();
  public static Set<String> recordTypeBypassNVLogic = new Set<String>{'Endo Orders','Endo Complete Orders','COMM Order'}; 
  private static void setBypassRT() {
    Map<String, Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Order.getRecordTypeInfosByName();
    for(Schema.RecordTypeInfo rti : rtMapByName.values()) {
      if(recordTypeBypassNVLogic.contains(rti.getName())) {
        recordTypeId_bypassNVSet.add(rti.getRecordTypeId());
      }
    }
  }

  static Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');

  public static void processOrdersBeforeInsertUpdate(Map<Id, Order> oldRecords,List<Order> newRecords){
        //Set<String> statusToConsider = new Set<String>{'Partial Ship','Shipped', 'Delivered'};
        Set<Id> recordTypesForClosed = new Set<Id>();
        setBypassRT();
        
        if(orderRecordTypes.containsKey('NV_Bill_Only')) recordTypesForClosed.add(orderRecordTypes.get('NV_Bill_Only'));
        if(orderRecordTypes.containsKey('NV_RMA')) recordTypesForClosed.add(orderRecordTypes.get('NV_RMA'));
        Set<Id> statusChangedOrders = new Set<Id>();
        //Id endoOrdersId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Orders').getRecordTypeId();
        //Id endoCompleteOrdersId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Complete Orders').getRecordTypeId();
        //Id commOrderId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        Order oldRecord;
        for(Order newRecord: newRecords){
            oldRecord = new Order();
            if(newRecord.Id == null && !recordTypeId_bypassNVSet.contains(newRecord.RecordTypeId)) {                            
                //System.debug('recordTypeId_bypassNVSet.contains(newRecord.RecordTypeId)'+recordTypeId_bypassNVSet.contains(newRecord.RecordTypeId));
                newRecord.Status = 'Entered';
                newRecord.Creation_Notified__c = false; //Set to false to differentiate between pre-existing and new records.
            }
            if(oldRecords != null && newRecord.Id != null && oldRecords.containsKey(newRecord.Id))
                oldRecord = oldRecords.get(newRecord.Id);

            if(oldRecord.Status != newRecord.Status){
                // T-520738 :: Added Endo RecordType check so that status don't change 
                if( !recordTypeId_bypassNVSet.contains(newRecord.RecordTypeId)) { // MV - 10 Oct 2016 - I-238091
                    statusChangedOrders.add(newRecord.Id);
                }
                if(newRecord.Status == 'Closed' && recordTypesForClosed.contains(newRecord.RecordTypeId)){
                    newRecord.Closed_Date__c = Date.today();
                }
                else if(newRecord.Status == 'Cancelled') {
                  String thridElement = (new List<String>(recordTypeId_bypassNVSet))[2];
                  if(newRecord.RecordTypeId != thridElement) {
                    newRecord.Cancelled_Date__c = Datetime.now(); 
                  }
                }
            }
            if(!recordTypeId_bypassNVSet.contains(newRecord.RecordTypeId)) { // MV - 10 Oct 2016 - I-238091
                newRecord.On_Hold__c = !String.isEmpty(newRecord.Hold_Type__c);
            }

            if(oldRecord.Date_Ordered__c != newRecord.Date_Ordered__c && newRecord.Date_Ordered__c!= null
              && !recordTypeId_bypassNVSet.contains(newRecord.RecordTypeId)) { // MV - 10 Oct 2016 - I-238091
                //11-2-15 | Barb has asked that this go back to reflecting exactly what would display in Oracle
                //, which is PST rather than EST.
                String dateInPST =  newRecord.Date_Ordered__c.format('yyyy-MM-dd', 'America/Los_Angeles');
                newRecord.EffectiveDate = Date.valueOf(dateInPST);
            }
        }
        
        if (statusChangedOrders.size() > 0){
            Map<Id,Order> orderData = getOrdersByIds(statusChangedOrders);
            for(Order o : orderData.values()){
                for(Order newOrder : newRecords){
                    if(newOrder.Id == o.Id && !recordTypeId_bypassNVSet.contains(newOrder.RecordTypeId)){
                        o.Status = newOrder.Status;
                        if(getOrderNewStatus(o)){
                            newOrder.Status = o.Status;
                        }
                    }
                }
            }
        }
    }

    public static void processOrdersAfterInsert(List<Order> newRecords){

        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

        Map<Id, Id> orderToAccountMap = new Map<Id, Id>();
        Set<Id> accountIds = new Set<Id>();
        for(Order record: newRecords){
            if(record.AccountId!=null && !recordTypeId_bypassNVSet.contains(record.RecordTypeId)){
                accountIds.add(record.AccountId);
                orderToAccountMap.put(record.Id, record.AccountId);
            }
        }

        Set<Id> userIds = new Set<Id>();
        Map<Id, Set<Id>> accountToUsersMap = new Map<Id, Set<Id>>();
        //Logic to create NV_Follow Records
        if(accountIds.size()>0){
            for(NV_Follow__c follow : [Select Id, Following_User__c, Record_Id__c
                                        From NV_Follow__c
                                        Where Record_Id__c in: accountIds]){
                Id recordId = Id.valueOf(follow.Record_Id__c);
                if(!accountToUsersMap.containskey(recordId))
                    accountToUsersMap.put(recordId, new Set<Id>());
                accountToUsersMap.get(recordId).add(follow.Following_User__c);
            }
        }

        Set<Id> userForSettings = new Set<Id>();
        if(accountIds.size() > 0){
            Map<Id,Account> accountData = getAccountsByIds(accountIds);
            for(Account record : accountData.values()){
                userForSettings.add(record.OwnerId);
            }
        }
        
        //Logic to create NV_Follow Records
        List<NV_Follow__c> followRecordsToCreate = new List<NV_Follow__c>();
        Set<String> statusToSkip = new Set<String>{'Shipped', 'Delivered'};
        for(Order record: newRecords){
            if(!statusToSkip.contains(record.Status) && !recordTypeId_bypassNVSet.contains(record.RecordTypeId)){
                if(orderToAccountMap.containsKey(record.Id)
                    && accountToUsersMap.containsKey(orderToAccountMap.get(record.Id))){
                    for(Id userId : accountToUsersMap.get(orderToAccountMap.get(record.Id))){
                        followRecordsToCreate.add(new NV_Follow__c(Following_User__c = userId,
                                                                        Record_Id__c = record.Id,
                                                                        External_Id__c = userId+'_'+record.Id));
                    }
                }
            }
        }

        if(followRecordsToCreate.size()>0){
            upsert followRecordsToCreate External_Id__c;
        }
    }

    public static void processOrdersAfterUpdate(Map<Id, Order> oldRecords, Map<Id, Order> newRecords){

        if(IsToProcessAfterUpdate){
            List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

            Set<Id> accountIds = new Set<Id>();
            Set<Id> recordIds = new Set<Id>();
            for(Order record: newRecords.values()){
                if(!recordTypeId_bypassNVSet.contains(record.RecordTypeId)){
                    if(record.AccountId!=null) accountIds.add(record.AccountId);
                    recordIds.add(record.Id);
                }
                
            }
            Set<Id> userForSettings = new Set<Id>();
            Map<Id,Account> accountData = new Map<Id,Account>();
            if(accountIds.size() > 0){
                accountData = getAccountsByIds(accountIds);
                for(Account record : accountData.values()){
                    userForSettings.add(record.OwnerId);
                }
            }
            
            Map<Id, Set<Id>> orderToUserMap = new Map<Id, Set<Id>>();
            Map<Id,Notification_Settings__c> userSettingsMap = new Map<Id,Notification_Settings__c>();

            //Collect all users for each order
            if(recordIds.size() > 0 && userForSettings.size() >0)
                orderToUserMap = NV_Utility.getFollowUsersForRecords(recordIds, userForSettings);
            
            //Add Current User In Set
            userForSettings.add(UserInfo.getUserId());

            //added for Story S-389739
            //Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');

            //Get Notification Settings for all users
            if(userForSettings.size() > 0 && orderToUserMap.size() > 0)
                userSettingsMap = NV_Utility.getNotificationSettingsForUsers(userForSettings);
            Notification_Settings__c userSettings;

            for(Order newRecord: newRecords.values()){
                Order oldRecord = null;
                if(!recordTypeId_bypassNVSet.contains(newRecord.RecordTypeId)){
                    if(oldRecords.containsKey(newRecord.Id))
                        oldRecord = oldRecords.get(newRecord.Id);

                    String accountName = '';
                    Account accountRecord = null;
                    if(newRecord.AccountId!= null && accountData.containsKey(newRecord.AccountId)){
                        accountName = 'for ' + accountData.get(newRecord.AccountId).Name;
                        accountRecord = accountData.get(newRecord.AccountId);
                    }

                    if(oldRecord != null){

                        Set<Id> usersToMentionForStatusChange = new Set<Id>();
                        Set<Id> usersToMentionForHoldChange = new Set<Id>();

                        //Runs Only if there is change in Status Or Hold Type
                        if(oldRecord.Status != newRecord.Status || oldRecord.Hold_Type__c != newRecord.Hold_Type__c){
                            //Iterate over all users to get mention records
                            if(orderToUserMap.containsKey(newRecord.Id)){
                                for(Id userId : orderToUserMap.get(newRecord.Id)){
                                    // Start - Added for S-389739
                                    if(userSettingsMap.containsKey(userId) && userSettingsMap.get(userId).Bill_Closed__c == true){
                                        //Filtering Users for Status Notification
                                        if(oldRecord.Status != newRecord.Status
                                            && userSettingsMap.containsKey(userId)
                                            && newRecord.RecordTypeId == orderRecordTypes.get('NV_Bill_Only')
                                            && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userId), newRecord.Status, false)){
                                                usersToMentionForStatusChange.add(userId);
                                        }else{

                                            if(oldRecord.Status != newRecord.Status
                                            && userSettingsMap.containsKey(userId)
                                            && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userId), newRecord.Status, false)){
                                                usersToMentionForStatusChange.add(userId);
                                            }

                                        }
                                    }else{
                                    // END - Story S-389739
                                        //Filtering Users for Status Notification
                                        if(oldRecord.Status != newRecord.Status
                                            && userSettingsMap.containsKey(userId)
                                            && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userId), newRecord.Status, false)){
                                                usersToMentionForStatusChange.add(userId);
                                        }
                                    } //Added for S-389739
                                    //Filtering Users for Hold Type Notification
                                    if(oldRecord.Hold_Type__c != newRecord.Hold_Type__c
                                        && userSettingsMap.containsKey(userId)
                                        && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userId), 'OnHold', false)){
                                        usersToMentionForHoldChange.add(userId);
                                    }
                                }
                            }


                            if(accountRecord != null){
                                Id userIdForAllActive = accountRecord.OwnerId;
                                //Logic for Context User for Status Notification

                                // Start - Added for S-389739
                                    if(userSettingsMap.containsKey(userIdForAllActive) && userSettingsMap.get(userIdForAllActive).All_Bill_Closed__c == true){
                                        //Filtering Users for Status Notification
                                        if(oldRecord.Status != newRecord.Status
                                            && userSettingsMap.containsKey(userIdForAllActive)
                                            && newRecord.RecordTypeId == orderRecordTypes.get('NV_Bill_Only')
                                            && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userIdForAllActive), newRecord.Status, true)){
                                                usersToMentionForStatusChange.add(userIdForAllActive);
                                        }else{

                                            if(!usersToMentionForStatusChange.contains(userIdForAllActive)
                                            && userSettingsMap.containsKey(userIdForAllActive)
                                            && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userIdForAllActive), newRecord.Status, true)){
                                                usersToMentionForStatusChange.add(userIdForAllActive);
                                            }
                                        }
                                    }else{
                                    // END - Story S-389739
                                        //Filtering Users for Status Notification
                                        if(!usersToMentionForStatusChange.contains(userIdForAllActive)
                                            && userSettingsMap.containsKey(userIdForAllActive)
                                            && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userIdForAllActive), newRecord.Status, true)){
                                                usersToMentionForStatusChange.add(userIdForAllActive);
                                        }
                                    } //Added for S-389739


                                //Logic for Context User for Hold Type Notification
                                //If User has opted for all Active Order, than only add him in mention
                                if(!usersToMentionForHoldChange.contains(userIdForAllActive)
                                    && userSettingsMap.containsKey(userIdForAllActive)
                                    //&& userSettingsMap.get(userIdForAllActive).All_Active_Orders__c){
                                    && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userIdForAllActive), 'OnHold', true)){
                                        usersToMentionForHoldChange.add(userIdForAllActive);
                                }
                            }




                            //Create Feed for Status
                            if(oldRecord.Status != newRecord.Status){
                                String feedText = 'Order is now '+newRecord.Status + ' '+ accountName + '.';
                                if(usersToMentionForStatusChange.size() > 0){
                                    ConnectApi.FeedItemInput input = NV_ChatterApexUtility.getTextFeedItemInputWithMentionsAtEnd(newRecord.Id,feedText,new List<Id>(usersToMentionForStatusChange));
                                    batchInputs.add(new ConnectApi.BatchInput(input));
                                }
                            }

                            //Create Feed for Hold Type
                            if(oldRecord.Hold_Type__c != newRecord.Hold_Type__c){
                                String feedText = null;
                                if((String.isBlank(oldRecord.Hold_Type__c) || oldRecord.Hold_Type__c == 'Credit Check Failure') && !String.isBlank(newRecord.Hold_Type__c) && newRecord.Hold_Type__c != 'Credit Check Failure'){
                                    feedText = 'Order on Hold '+ accountName + '.';
                                }
                                else if(!String.isBlank(oldRecord.Hold_Type__c) && oldRecord.Hold_Type__c != 'Credit Check Failure' && String.isBlank(newRecord.Hold_Type__c)){
                                    feedText = 'Order no longer on Hold '+ accountName + '.';
                                }
                                if(feedText != null && usersToMentionForHoldChange.size()>0){
                                    ConnectApi.FeedItemInput input = NV_ChatterApexUtility.getTextFeedItemInputWithMentionsAtEnd(newRecord.Id,feedText,new List<Id>(usersToMentionForHoldChange));
                                    batchInputs.add(new ConnectApi.BatchInput(input));
                                }
                            }
                        }
                    }
                }    
            }

            if(!Test.isRunningTest() && batchInputs.size()>0){
                ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
            }
        }
    }

    public static void processOrdersAfterDelete(List<Order> oldRecords){
        Set<Id> orderIds = new Set<Id>();
        for(Order record : oldRecords){
            if(!recordTypeId_bypassNVSet.contains(record.RecordTypeId))
                orderIds.add(record.Id);
        }

        if(orderIds.size()>0){
            List<NV_Follow__c> followRecordsToDelete = [SELECT Id From NV_Follow__c Where Record_Id__c in: orderIds];
            if(followRecordsToDelete.size()>0){
                delete followRecordsToDelete;
            }
        }
    }

    public static Map<Id, Account> getAccountsByIds(Set<Id> accountIds){
        Map<Id, Account> accountData  = new Map<Id, Account>([Select Id, Name, OwnerId From Account Where Id in: accountIds]);
        return accountData;
    }

    public static Map<Id, Order> getOrdersByIds(Set<Id> recordIds){
        Map<Id, Order> data  = new Map<Id, Order>([Select Id, AccountId, Account.Name, Account.OwnerId, Status, Hold_Type__c, Contract.OwnerId,
                                                        RecordType.DeveloperName, Date_Ordered__c,
                                                    (Select Id, Status__c, Hold_Description__c, Order_Delivered__c  From OrderItems)
                                                    From Order Where Id in: recordIds]);
        return data;
    }

    public static Boolean getOrderNewStatus(Order orderRecord){

        Integer value1, value2;
        String newStatus = orderRecord.Status;
        Integer isAtleastOneOrderShipped = 0;
        Integer isAtleastOneOrderBackordered = 0;
        Integer isOrderDelivered = 1;

        value1 = -1;
        for(OrderItem record: orderRecord.OrderItems){
            //if(!recordTypeId_bypassNVSet.contains(record.RecordTypeId)){

                if(!String.isBlank(record.Status__c)){
                    if(statusMapping.containsKey(orderRecord.RecordType.DeveloperName)
                    && statusMapping.get(orderRecord.RecordType.DeveloperName).containsKey(record.Status__c)){
                        value2 = statusMapping.get(orderRecord.RecordType.DeveloperName).get(record.Status__c);
                        if(value2 > value1){
                            value1 = value2;
                            newStatus = record.Status__c;
                        }
                    }
                    if(record.Status__c == 'Backordered' || record.Status__c == 'Partial Shipping'){
                        isAtleastOneOrderBackordered = 1;
                    }
                }

                System.debug('[DD] Status : '+ record.Status__c);
                System.debug('[DD] value1 : '+ value1);
                System.debug('[DD] value2 : '+ value2);
                System.debug('[DD] isAtleastOneOrderShipped : '+ isAtleastOneOrderShipped);

                //1= Delivered, 2 = Closed, 3 = Shipped
                if(orderRecord.RecordType.DeveloperName == 'NV_Standard'
                    && value1 > 0 && value2 < 4 && value2 != 0
                    && isAtleastOneOrderShipped != 1){
                    isAtleastOneOrderShipped = 1;
                }

                //Check for Delivered
                if(orderRecord.RecordType.DeveloperName == 'NV_Standard' && isOrderDelivered != 0 && record.Order_Delivered__c) isOrderDelivered = 1;
                else isOrderDelivered = 0;

           // }    
        }

        if(orderRecord.RecordType.DeveloperName == 'NV_Standard' && isOrderDelivered == 1 && newStatus != 'Cancelled') newStatus = 'Delivered';
        if(newStatus == 'Delivered' && isOrderDelivered != 1 ) newStatus = 'Shipped';

        System.debug('[DD] value1 : '+ value1);
        System.debug('[DD] value2 : '+ value2);
        System.debug('[DD] isAtleastOneOrderShipped : '+ isAtleastOneOrderShipped);

        if(value1 > 3 && isAtleastOneOrderShipped == 1) newStatus = 'Partial Ship';
        if(isAtleastOneOrderShipped == 0 && isAtleastOneOrderBackordered == 1) newStatus = 'Backordered';

        if(newStatus != orderRecord.Status) {
            orderRecord.Status = newStatus;
            return true;
        }
        else return false;
    }

    static Map<String, Map<String, Integer>> statusMapping;
    static{
        Map<String, String> statusPriorityMap = new Map<String, String>();
        statusPriorityMap.put('NV_Standard', 'Cancelled,Delivered,Closed,Shipped,Backordered,Awaiting Shipping,Booked,Entered');
        statusPriorityMap.put('NV_RMA', 'Cancelled,Closed,Returned,Awaiting Return,Booked,Entered');
        statusPriorityMap.put('NV_Bill_Only', 'Cancelled,Closed,Awaiting PO,Booked,Entered');
        statusMapping = new Map<String, Map<String, Integer>>();
        Integer index;
        for(String type : statusPriorityMap.keySet()){
            index = 0;
            statusMapping.put(type, new Map<String, Integer>());
            for(String status : statusPriorityMap.get(type).split(',')){
                statusMapping.get(type).put(status, index++);
            }
        }
    }

}