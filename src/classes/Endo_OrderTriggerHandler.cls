/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  Jan 27, 2014
 Purpose       :  Handler class for trigger Endo_OrderTrigger
 Reference     :  Task T-237810
 06 Oct 2016   Parul Gupta   T-542805 (Endo is using Stardard Order now, Commenting whole code)
*******************************************************************************/

public without sharing class Endo_OrderTriggerHandler {
  /*public static Boolean isTriggerInProgress;
    
  // Method to be called on AfterInsert event of trigger
  public static void onAfterInsert(List<Order__c> newOrders) {
    List<Order__c> updateableOrders = new List<Order__c>();
    System.debug('@@@' + newOrders);
    for(Order__c ordr : newOrders) {
      if(ordr.Name != null) {
        updateableOrders.add(ordr);
      }
    }
    System.debug('@@@' + updateableOrders);
    if(updateableOrders.size() > 0) {
      addOrdertoSupportCases(updateableOrders);
    }
  }
    
  // Method to be called on AfterUpdate event of trigger
  
  public static void onAfterUpdate(List<Order__c> newOrders, Map<Id, Order__c> oldOrderMap) {
    List<Order__c> updateableOrders = new List<Order__c>();
    for(Order__c ordr : newOrders) {
      if(isChanged(ordr.Name, oldOrderMap.get(ordr.Id).Name)) {
        updateableOrders.add(ordr);
      }
    }
    if(updateableOrders.size() > 0) {
      addOrdertoSupportCases(updateableOrders);
    }
  }
  
    
  // Helper method to add orders
  private static void addOrdertoSupportCases(List<Order__c> orders) {
    Set<String> orderNumbers = new Set<String>();
    
    for(Order__c ordr : orders) {
        orderNumbers.add(ordr.Name);
    }
    
    system.debug('@@@@@ orderNumbers : ' + orderNumbers);
    
    Map<String, List<Case>> orderCaseMap = new Map<String, List<Case>>();
    
    for(Case cs : [SELECT Id, 
                          Oracle_Order_Ref__c, contactId,
                          Current_Case_Order__c
                   FROM     Case
                   WHERE  Current_Case_Order__c = null
                   AND      Oracle_Order_Ref__c IN : orderNumbers]) {
        if(!orderCaseMap.containsKey(cs.Oracle_Order_Ref__c)) {
            orderCaseMap.put(cs.Oracle_Order_Ref__c, new List<Case>());
        }
        orderCaseMap.get(cs.Oracle_Order_Ref__c).add(cs);
    }
    
    system.debug('@@@@@ orderCaseMap : ' + orderCaseMap);
    if(orderCaseMap.size() == 0)
      return;
      
    map<Id, Case> updateableCases = new map<Id, Case>();    
    
    List<Order__c> lstOrders = new List<Order__c>();
    for(Order__c ordr : orders) {
      Id contactId;
      if(orderCaseMap.containsKey(ordr.Name)) {
        for(Case cs : orderCaseMap.get(ordr.Name)) {
          cs.Current_Case_Order__c = ordr.Id;
          updateableCases.put(cs.Id, cs);
          
          if(cs.ContactId != null){
            contactId = cs.ContactId;
          }
        }
      }
      //System.debug('@@@' + contactId);
      //System.debug('@@@' + ordr.Order_Contact__c);
      if(contactId != null && ordr.Order_Contact__c != contactId){
        Order__c updateOrder = new Order__c(Id = ordr.Id);
        updateOrder.Order_Contact__c = contactId;
        lstOrders.add(updateOrder);
      }
      
    }
    
    system.debug('@@@@@ updateableCases : ' + updateableCases);
    
    if(updateableCases.size() > 0) {
          update updateableCases.values();
    }
    
    
    //Update Order itself for updating contact lookup from case
    System.debug('@@@' + lstOrders);
    if(lstOrders.size() > 0){
      isTriggerInProgress = true;
      update lstOrders;
    }
    
  }
 /**
  * Method to check if provided 2 string are same or not.
  
    private static boolean isChanged(String newOrderNumber, String oldOrderNumber) {
        if(newOrderNumber == null && oldOrderNumber != null) {
            return true;
        } else if(oldOrderNumber == null && newOrderNumber != null) {
            return true;
        } else {
            return !newOrderNumber.equals(oldOrderNumber);
        }
    }*/
}