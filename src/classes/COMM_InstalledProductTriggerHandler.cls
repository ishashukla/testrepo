/**================================================================  
* Appirio, Inc
* Name: COMM_InstalledProductTriggerHandler 
* Description: Trigger Handller for SVMXC__Installed_Product__c (T-490747)
* Created Date: 04/28/2016
* Created By: Shubham Dhupar (Appirio)

// 05/09/2016, Rahul Aeran       (T-501084) update
// 06/30/2016, Rahul Aeran       (T-516414) update
// 07/04/2016, Rahul Aeran       (T-517131)update sync QIP ID VALUE from corresponding product
// 08/09/2016, Gaurav Sinha      (T-514102) Trigger to decommission child assets
// 13 Sept 2016       Varun Vasishtha  T-514084 - To Send a mail for Hold Release
// 17th October 2016     Nitish Bansal      Update (I-240207)  //To populate phase on install product as per OLI
// 24th October 2016     Nitish Bansal    Update (I-238609) // Warranty end date calculation
// 26th October 2016  Deepti Maheshwari  Update(I-241506, I-241501) //Reseting QIP Fields 
// 25 November 2016   Deepti Maheshwari  Update(I-245340)
==================================================================*/
  //----------------------------------------------------------------------------
  //  Method called on After insert of records
  //----------------------------------------------------------------------------
public class COMM_InstalledProductTriggerHandler {

  static Id rtCOMM = Constants.getRecordTypeId(Constants.RT_COMM_INSTALL_PRODUCT,Constants.INSTALL_PRODUCT_SOBJECT_ASSET);
    //Updated by Varun Vasistha changed Inactive to Terminated
  Public static final String const_INACTIVE ='Terminated';
  Public static final String const_Yes ='Yes';
  Public static final String const_NO ='No';
  public static final string holdTemplate = 'Hold_Released_for_Asset_VF';
  public static void AfterInsert(List <SVMXC__Installed_Product__c> newlist) {
    updateProjectPhaseOnIPUpdate(newList,null);
  }
  public static void onBeforeInsert(List <SVMXC__Installed_Product__c> newlist){
    updateProjectPlanOnProduct(newList,null);
    updateProjectPhaseOnProduct(newList,null); //NB - 10/17 - I-240207
    setWarrantyDate(newList,null); //NB - 10/24 - I-238609
    updateInstallStatusField(newList,null);
    syncQIPIdFieldFromProduct(newList,null);
  }
  public static void onBeforeUpdate(List <SVMXC__Installed_Product__c> newlist , map<Id,SVMXC__Installed_Product__c> mapOld){
    //NB - 10/17 - I-240207 - Start
    updateProjectPlanOnProduct(newList,mapOld); 
    updateProjectPhaseOnProduct(newList,mapOld); 
    //NB - 10/17 - I-240207 - End
    setWarrantyDate(newList,mapOld); //NB - 10/24 - I-238609
    updateInstallStatusField(newList,mapOld);
    syncQIPIdFieldFromProduct(newList,mapOld);
    createChecklistHeader(newList,mapOld);
  }
  public static void onAfterUpdate(List <SVMXC__Installed_Product__c> newlist , map<Id,SVMXC__Installed_Product__c> mapOld){
    updateProjectPhaseOnIPUpdate(newList,mapOld);
    syncAllChildAssets(newList,mapOld);
    decommission_child_assets(newList,mapOld);
    HoldRelesedNotification(newlist,mapOld);
  }
  
  // @mthod name : createChecklistHeader
  // @param : newlist - New Reocrds
  // @param : mapold - Old Reocrds
  // @Description : This method creates new checklist headers and detail records based on a flag set from SFM
  // @Author: Shubham Dhupar
  // @Updated By : Deepti Maheshwari : Issue fixes
  public static void createChecklistHeader(List<SVMXC__Installed_Product__c> newAssetList,Map<Id,SVMXC__Installed_Product__c> oldMap) {
    String qipId = '';
    Map<Id, SVMXC__Installed_Product__c> assetIdMap = new Map<Id, SVMXC__Installed_Product__c>();
    Boolean noQIPFound = false;
    //system.debug('Flag' + newAssetList.get(0).Create_Checklist__c);
    for(SVMXC__Installed_Product__c install : newAssetList) {
      if(install.RecordTypeID == rtCOMM
        && install.QIP_ID__c != null){
        if(install.Create_Checklist__c != oldMap.get(install.Id).Create_Checklist__c 
          && install.Create_Checklist__c == true){
          qipId = install.QIP_ID__c;
          assetIdMap.put(install.Id, install);
        }
      }else{
        noQIPFound = true;
      }
    }
    Set<Id> checklistIdSet = new Set<Id>();
    List <Checklist_Header__c> checkHeadList = new List<Checklist_Header__c>();
    Id Checklistid;
    if(!noQIPFound && (qipId != '' && qipId != null)){
      
      //DM Get Checklist from Checklist Custom Object
      for(ChecklistCUST__c checkCust : [SELECT id,Name 
                                        FROM ChecklistCUST__c
                                        WHERE Name = :qipId]) {
        checklistIdSet.add(checkCust.id);
        Checklistid = checkCust.id;
      }
      
      if(checklistIdSet.size() > 0 && assetIdMap.size() > 0){
        List<Checklist_Header__c> obsoleteChecklist = [SELECT ID, isLatest__c 
                                                      FROM Checklist_Header__c 
                                                      WHERE isLatest__c = true
                                                      AND Asset__r.id in :assetIdMap.keyset()];
        if(obsoleteChecklist.size() > 0){
          
          for(Checklist_Header__c obsoleteHeads : obsoleteChecklist){
            obsoleteHeads.isLatest__c = false;
          }
          update obsoleteChecklist;
        } 
        
        Map<String, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Checklist_Header__c.getRecordTypeInfosByName();
        for(SVMXC__Installed_Product__c install : assetIdMap.values()) {
          Checklist_Header__c checkHeader = new Checklist_Header__c (Asset__c = install.id,
                                            Checklist_Type__c = qipId, Status__c = 'New',
                                            Initial_Product__c = install.SVMXC__Product__c,
                                            RecordTypeId = rtMap.get(Label.COMM_CheckList_Header).getRecordTypeID(),
                                            checklist__c = Checklistid,
                                            isLatest__c = true); 
                                            // SD:9/19[Auto populate Product and checklist lookup on Creation,Ref: T-536950]
      
          checkHeadList.add(checkHeader);
        }
        if(!checkHeadList.isEmpty()){
          insert checkHeadList;  
        }
    
        List<CheckList_Detail__c> checkDetailList = new List<CheckList_Detail__c>();
        for(CheckList_Item__c itemCheck : [SELECT id,Name,Type__c,Order__c,Checklist__c,
                                           Checklist_Group__c,Maximum__c,Minimum__c,
                                           Question__c,Question_ID__c,Active__c,Result_Option__c
                                           FROM CheckList_Item__c
                                           WHERE Checklist__c in: checklistIdSet]) {
          
          if(itemCheck.Active__c) { 
            for(Checklist_Header__c headCheck :checkHeadList) { 
              CheckList_Detail__c detailCheck = new CheckList_Detail__c(name = itemCheck.Name + ' 1',
                                                Checklist__c = itemcheck.Checklist__c,Checklist_Group__c =itemCheck.Checklist_Group__c,
                                                Maximum__c = itemCheck.Maximum__c,Minimum__c = itemCheck.Minimum__c,
                                                Question__c = itemCheck.Question__c,Question_ID__c = itemCheck.Question_ID__c,
                                                CheckList_Item__c = itemCheck.id, Checklist_Header__c = headCheck.id,
                                                Result__c = itemCheck.Result_Option__c,Type__c = itemCheck.Type__c,
                                                Order__c=itemCheck.Order__c,Asset__c  = headCheck.Asset__c );
              checkDetailList.add(detailCheck);
            }
          }
        }
        if(checkDetailList.size() > 0){
          insert checkDetailList;  
        }
      }else{
        noQIPFound = true;
      }
    }
    //Setting create checklist to false
    
    for(SVMXC__Installed_Product__c install : newAssetList) {
      if(install.RecordTypeId == rtCOMM){
        if(noQIPFound && install.Create_Checklist__c){
          install.QIP_Creation_Failure_Reason__c = Label.No_QIP_Found;
        }
        //DM Fix for I-241506
        if(install.QIP_ID__c != null
        && install.QIP_Status__c != Constants.STATUS_INPROGRESS
        && install.QIP_Status__c != Constants.COMPLETED
        && install.Create_Checklist__c){
          install.QIP_Status__c = Constants.STATUS_INPROGRESS;
        }
        //DM fix for I-241501
        else if(install.QIP_ID__c != null
        && install.QIP_ID__c != oldMap.get(install.Id).QIP_ID__c
        && !install.Create_Checklist__c){
          install.QIP_Status__c = '';
          install.QIP_Creation_Failure_Reason__c = '';
          install.QIP_Completion_Date__c = null;
          install.Case_Owner__c  = null;
          install.QIP_Notes__c = null;
        }
        install.Create_Checklist__c = false;
      }
    }
  }
  
  // @mthod name : decommission_child_assets
  // @param : newlist - New Reocrds
  // @param : mapold - Old Reocrds
  // @Description : Method updates all the child Installed Product with Status as inactive depening on the parent status
  // @Author: Gaurav Sinha
  public static void decommission_child_assets(List <SVMXC__Installed_Product__c> newlist,map<Id,SVMXC__Installed_Product__c> mapOld){
      boolean isinsert = mapold==null;
      map<id,SVMXC__Installed_Product__c> mapInstalledProduct = new map<id,SVMXC__Installed_Product__c>();
      for(SVMXC__Installed_Product__c varloop: newlist){
        if(
                ( varloop.SVMXC__status__c == CONST_Inactive ) && 
                (varloop.Inactivate_Child_Assets__c == CONST_YES) &&
                ( isinsert || ( varloop.SVMXC__status__c != mapold.get(varloop.id).SVMXC__status__c ) ) &&
                ( isinsert || ( varloop.Inactivate_Child_Assets__c != mapold.get(varloop.id).Inactivate_Child_Assets__c ))
        ){
            //system.debug('================>'+( isinsert || ( varloop.SVMXC__status__c != mapold.get(varloop.id).SVMXC__status__c ) ));
            //system.debug('================>'+( isinsert || ( varloop.Inactivate_Child_Assets__c != mapold.get(varloop.id).Inactivate_Child_Assets__c) ));
            //system.debug('================>'+varloop.SVMXC__status__c);
            //system.debug('================>'+varloop.Inactivate_Child_Assets__c);
            //system.debug('================>'+mapold.get(varloop.id).SVMXC__status__c);
            //system.debug('================>'+mapold.get(varloop.id).Inactivate_Child_Assets__c);            
            mapInstalledProduct.put(varloop.id,varloop);
        }          
      }
      if(mapInstalledProduct.size() > 0 ){
          List<SVMXC__Installed_Product__c> liUpdateDecomission = new list<SVMXC__Installed_Product__c>();
          for(SVMXC__Installed_Product__c varloop : [select id,SVMXC__status__c,Inactivate_Child_Assets__c
                                                     from SVMXC__Installed_Product__c
                                                     where SVMXC__Parent__c in : mapInstalledProduct.keySet()
                                                     ]){
                liUpdateDecomission.add(
                                        new SVMXC__Installed_Product__c(
                                                id=varloop.id,
                                                SVMXC__status__c=const_inactive,
                                                Inactivate_Child_Assets__c=const_No
                                            )
                                );
        }
        if(!liUpdateDecomission.isEmpty())
            update liUpdateDecomission;
      }
  }
  public static void updateProjectPhaseOnIPUpdate(List <SVMXC__Installed_Product__c> newlist , map<Id,SVMXC__Installed_Product__c> oldMap) {
    
    Boolean isUpdate = oldMap != null ? true : false; // To check for  oldMap is Null
    Boolean isDelete= newList == null ? true : false;  // To check for  newListis Null
    List<SVMXC__Installed_Product__c> oldList = new List<SVMXC__Installed_Product__c>();
     List <SVMXC__Installed_Product__c> instProdList = new List <SVMXC__Installed_Product__c>();
    if(isDelete) {
      oldList.addAll(oldMap.values());
      instProdList.addAll(oldMap.values());
    }
    List<Project_Phase__c> projectList = new List<Project_Phase__c>();
// Create a Map Which stores Project Plan id(Parent Record id) with SVMXC__Installed_Product__c id as key.
    Map<Id,Id> newInstProdIdMap = new Map <Id,Id>();
    if(!isDelete){
    instProdList.addAll(newList);
      for(SVMXC__Installed_Product__c instProd : newList) {
        if(instProd.Project_Phase__c != NULL) {
          newInstProdIdMap.put(instProd.Id, instProd.Project_Phase__c);
         
        }
      }
    }else if(isDelete) {
      for(SVMXC__Installed_Product__c instProd : oldList) {
        if(instProd.Project_Phase__c != NULL) {
          newInstProdIdMap.put(instProd.Id, instProd.Project_Phase__c);
        }
      }
      
    }
   
    if(newInstProdIdMap.size() > 0){
      for(Project_Phase__c project : [Select Id,Milestone__c
                                    From Project_Phase__c
                                    Where Id in : newInstProdIdMap.values()]) {
        ProjectList.add(project);
      }
    }
    if(ProjectList.size() > 0)
      updateProjectMilestoneOnAllStatusQIPComplete(projectList,instProdList);
  }
  public static void updateProjectMilestoneOnAllStatusQIPComplete(List<Project_Phase__c> projects,List <SVMXC__Installed_Product__c> instProdList) {
    List<Id> projectIds = new List<Id>();
    List<Project_Phase__c> projectList = new List<Project_Phase__c>();
    Map<id,List<SVMXC__Installed_Product__c>> updatemap = new Map<id,List<SVMXC__Installed_Product__c>>();
    for(Project_Phase__c project: projects) {
      projectIds.add(project.Id);
    }
    if(projectIds.size() > 0){
      for(SVMXC__Installed_Product__c temp : [select Id,
                                                     QIP_Status__c,
                                                    Project_Phase__c
                                               from SVMXC__Installed_Product__c
                                               where Project_Phase__c IN :projectIds]) {
        List<SVMXC__Installed_Product__c> instProds = updatemap.get(temp.Project_Phase__c) == null ?
                                                         new  List<SVMXC__Installed_Product__c>() :
                                                         updatemap.get(temp.Project_Phase__c);
        instProds.add(temp);
        updatemap.put(temp.Project_Phase__c, instProds);
      }
    }
    
    Integer flag=0;
    for(Project_Phase__c project: projects) {
      if(updatemap.containsKey(project.Id)) {
        for(SVMXC__Installed_Product__c ord: updatemap.get(project.Id)) {
          if(ord.QIP_Status__c != label.QIP_Complete) {
            flag++;
          }
        }
        if(flag == 0) {
          //system.debug('@@@@@@@@@@@@@@@@@@@@@@@@    ' + flag);
          projectList.add(project);
        }
      }
    } 
    Integer count = 0;
    List <Project_phase__c> finalProjectList = new List <Project_phase__c>();
    if(projectList.size() > 0){
      for(Project_Phase__c project : [Select Id,Project_Plan__r.Engineering_Approval_Status__c,Milestone__c
                                      From Project_Phase__c
                                      Where Id in : ProjectList]) {
        if(project.Project_Plan__r.Engineering_Approval_Status__c != 'Approved' && flag == 0) {
          count = 1;
        }
        else if(project.Project_Plan__r.Engineering_Approval_Status__c == 'Approved' && flag == 0) {
          project.Milestone__c = label.Installation_Complete;
          finalProjectList.add(project);
        }
      }
      for(SVMXC__Installed_Product__c instProd :instProdList ) {
        if(count == 1) {
          instProd.addError(label.Project_Plan_Error);
        }
      }
    }
    if(finalProjectList.size() > 0)
      update finalProjectList;
   }
    
  //----------------------------------------------------------------------------
  //  Setting the project plan as per the installed product's order project plan
  //----------------------------------------------------------------------------
  public static void updateProjectPlanOnProduct(List<SVMXC__Installed_Product__c> newList, map<Id,SVMXC__Installed_Product__c> oldMap){
    Boolean isUpdate = oldMap == null ? false : true;
    Set<Id> setOrderIds = new Set<Id>();
    //Id rtCOMM = Constants.getRecordTypeId(Constants.RT_COMM_INSTALL_PRODUCT,Constants.INSTALL_PRODUCT_SOBJECT_ASSET);
    
    for(SVMXC__Installed_Product__c prod : newList){
      if((!isUpdate || prod.Order__c != oldMap.get(prod.Id).Order__c) && prod.RecordTypeid == rtCOMM && prod.Order__c != null){
        // We need to set the project plan to the order project plan
        setOrderIds.add(prod.Order__c);
      }
    }
    
    //Now we need to query the order and update the project plan for the same
    if(setOrderIds.size() > 0){
      map<Id,Order> mapOrderIdToOrder = new map<Id,Order>([SELECT Id,Project_Plan__c FROM Order WHERE Id IN:setOrderIds
                                                            AND Project_Plan__c != null]);
      for(SVMXC__Installed_Product__c prod : newList){
        if((!isUpdate || prod.Order__c != oldMap.get(prod.Id).Order__c)&& prod.RecordTypeid == rtCOMM){
          // Setting the project plan from the queried order
          if(mapOrderIdToOrder.containsKey(prod.Order__c)){
            prod.Project_Plan__c = mapOrderIdToOrder.get(prod.Order__c).Project_Plan__c;
          }else{
            //setting it to null
            prod.Project_Plan__c = null;
          }
        }
      }
    }
 }
  
  //-----------------------NB - 10/17 - I-240207 Start-----------------------------------------------------------
  //  Setting the project phase as per the installed product's order product's phase
  public static void updateProjectPhaseOnProduct(List<SVMXC__Installed_Product__c> newList, map<Id,SVMXC__Installed_Product__c> oldMap){
    Boolean isUpdate = oldMap == null ? false : true;
    Set<Id> setOrderItemIds = new Set<Id>();
    //Id rtCOMM = Constants.getRecordTypeId(Constants.RT_COMM_INSTALL_PRODUCT,Constants.INSTALL_PRODUCT_SOBJECT_ASSET);
    
    for(SVMXC__Installed_Product__c prod : newList){
      if((!isUpdate || prod.Order_Product__c != oldMap.get(prod.Id).Order_Product__c) && prod.RecordTypeid == rtCOMM && prod.Order_Product__c != null){
        // We need to set the project phase to the order item project phase
        setOrderItemIds.add(prod.Order_Product__c);
      }
    }
    
    if(setOrderItemIds.size() > 0){
      //Now we need to query the order item and update the project phase for the same
      map<Id,OrderItem> mapOrderItemIdToOrderItem = new map<Id,OrderItem>([SELECT Id, Project_Phase__c, Project_Plan__c FROM OrderItem WHERE Id IN:setOrderItemIds
                                                            AND Project_Phase__c != null]);
      for(SVMXC__Installed_Product__c prod : newList){
        if((!isUpdate || prod.Order_Product__c != oldMap.get(prod.Id).Order_Product__c)&& prod.RecordTypeid == rtCOMM){
          // Setting the project phase from the queried order item
          if(mapOrderItemIdToOrderItem.containsKey(prod.Order_Product__c) ){
              prod.Project_Phase__c = mapOrderItemIdToOrderItem.get(prod.Order_Product__c).Project_Phase__c;
          }else{
            //setting it to null
            prod.Project_Phase__c = null;
          }
        }
      }
    }  
  }  

  //-----------------------NB - 10/17 - I-240207 End-----------------------------------------------------------  
  
  //----------------------------------------------------------------------------
  //  Setting the install status picklist field 
  //----------------------------------------------------------------------------
  public static void updateInstallStatusField(List<SVMXC__Installed_Product__c> newList, map<Id,SVMXC__Installed_Product__c> oldMap){
    Boolean isUpdate = oldMap == null ? false : true;
    //Id rtCOMM = Constants.getRecordTypeId(Constants.RT_COMM_INSTALL_PRODUCT,Constants.INSTALL_PRODUCT_SOBJECT_ASSET);
    Set<Id> setProductIds = new Set<Id>();
    for(SVMXC__Installed_Product__c prod : newList){
      if((!isUpdate || prod.SVMXC__Product__c != oldMap.get(prod.Id).SVMXC__Product__c) && prod.RecordTypeid == rtCOMM){
        setProductIds.add(prod.SVMXC__Product__c);
      }
    }
    
    if(setProductIds.size() > 0){
      map<Id,Product2> mapProduct2ToProduct = new map<Id,Product2>([SELECT Id,Track_In_IB__c FROM Product2 
                                                                      WHERE Id IN:setProductIds
                                                                      AND Track_In_IB__c = true]);
      for(SVMXC__Installed_Product__c prod : newList){
        if((!isUpdate || prod.SVMXC__Product__c != oldMap.get(prod.Id).SVMXC__Product__c) && prod.RecordTypeid == rtCOMM){
          // Setting the picklist field 
          if(mapProduct2ToProduct.containsKey(prod.SVMXC__Product__c)){
            prod.Install_Status__c = Constants.READY_TO_INSTALL;
          }else{
            //setting it to null
            prod.Install_Status__c = null;
          }
        }
      } 
    } 
  }
  
  
   //----------------------------------------------------------------------------
  //  Sync All the childs selected fields from the parent  T-516414
  //----------------------------------------------------------------------------
  private static void syncAllChildAssets(List<SVMXC__Installed_Product__c> newList, map<Id,SVMXC__Installed_Product__c> oldMap){
    map<Id,SVMXC__Installed_Product__c> mapParentProductIdToProduct = new map<Id,SVMXC__Installed_Product__c>();
    //Id rtCOMM = Constants.getRecordTypeId(Constants.RT_COMM_INSTALL_PRODUCT,Constants.INSTALL_PRODUCT_SOBJECT_ASSET);
    for(SVMXC__Installed_Product__c prod : newList){
      if((prod.Install_Status__c != oldMap.get(prod.Id).Install_Status__c ||
        prod.QIP_Status__c != oldMap.get(prod.Id).QIP_Status__c ||
        prod.QIP_ID__c != oldMap.get(prod.Id).QIP_ID__c ||
        prod.QIP_Creation_Failure_Reason__c != oldMap.get(prod.Id).QIP_Creation_Failure_Reason__c ||        
        prod.Installed_At_Address__c != oldMap.get(prod.Id).Installed_At_Address__c ||
        prod.Room_Type__c != oldMap.get(prod.Id).Room_Type__c ||
        prod.Room_Number__c != oldMap.get(prod.Id).Room_Number__c ||
        prod.SVMXC__Date_Installed__c != oldMap.get(prod.Id).SVMXC__Date_Installed__c ||
        prod.SVMXC__Installation_Notes__c != oldMap.get(prod.Id).SVMXC__Installation_Notes__c ||
        prod.Case_Owner__c != oldMap.get(prod.Id).Case_Owner__c ||
        prod.QIP_Completion_Date__c != oldMap.get(prod.Id).QIP_Completion_Date__c ||
        prod.QIP_Notes__c != oldMap.get(prod.Id).QIP_Notes__c ||
        prod.SVMXC__Street__c != oldMap.get(prod.Id).SVMXC__Street__c ||
        prod.SVMXC__City__c != oldMap.get(prod.Id).SVMXC__City__c ||
        prod.SVMXC__State__c != oldMap.get(prod.Id).SVMXC__State__c ||
        prod.SVMXC__Zip__c != oldMap.get(prod.Id).SVMXC__Zip__c ||
        prod.SVMXC__Country__c != oldMap.get(prod.Id).SVMXC__Country__c ) && prod.RecordTypeid == rtCOMM){
          //Need to add manual address field change as well
          mapParentProductIdToProduct.put(prod.Id,prod);
        }
    }
    
    
    if(mapParentProductIdToProduct.keySet().isEmpty() || !(mapParentProductIdToProduct.size() > 0)){
      return;
    }
    List<SVMXC__Installed_Product__c> lstProductsToUpdate = new List<SVMXC__Installed_Product__c>();
    SVMXC__Installed_Product__c objProd = null;
    //We need to query all the childs of the asset and sync these values
    for(SVMXC__Installed_Product__c prod : [SELECT Id,SVMXC__Parent__c 
                                              FROM SVMXC__Installed_Product__c
                                              WHERE SVMXC__Parent__c IN :mapParentProductIdToProduct.keySet()]){
      objProd = new SVMXC__Installed_Product__c(Id = prod.Id);
      objProd.Install_Status__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).Install_Status__c;
      if(objProd.Install_Status__c == 'Installed') { // SD 10/24 : Added the condition to Handle Exception [Ref:I-241137]
        objProd.Installed_by__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).Installed_by__c;
        objProd.SVMXC__Date_Installed__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__Date_Installed__c;
      }
      objProd.QIP_Status__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).QIP_Status__c;
      //Fix for I-245340 starts
      objProd.QIP_ID__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).QIP_ID__c;
      objProd.QIP_Creation_Failure_Reason__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).QIP_Creation_Failure_Reason__c;
      //Fix for I-245340 ends
      objProd.Installed_At_Address__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).Installed_At_Address__c;
      objProd.Room_Type__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).Room_Type__c;
      objProd.Room_Number__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).Room_Number__c;
      objProd.SVMXC__Date_Installed__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__Date_Installed__c;
      objProd.SVMXC__Installation_Notes__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__Installation_Notes__c;
      objProd.Case_Owner__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).Case_Owner__c;
      objProd.QIP_Completion_Date__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).QIP_Completion_Date__c;
      objProd.QIP_Notes__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).QIP_Notes__c;
      
      objProd.SVMXC__Street__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__Street__c;
      objProd.SVMXC__City__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__City__c;
      objProd.SVMXC__State__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__State__c;
      objProd.SVMXC__Zip__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__Zip__c;
      objProd.SVMXC__Country__c = mapParentProductIdToProduct.get(prod.SVMXC__Parent__c).SVMXC__Country__c;
      
      lstProductsToUpdate.add(objProd);                                          
    }
    
    if(!lstProductsToUpdate.isEmpty()){
      update lstProductsToUpdate;
    }
  }
  
  
  //----------------------------------------------------------------------------
  //  Sync QIP Id picklist field from the standard product field on Asset.  T-517131
  //----------------------------------------------------------------------------
  
  private static void syncQIPIdFieldFromProduct(List<SVMXC__Installed_Product__c> newList, map<Id,SVMXC__Installed_Product__c> oldMap){
    Boolean isInsert = oldMap == null ? true : false;
    //Id rtCOMM = Constants.getRecordTypeId(Constants.RT_COMM_INSTALL_PRODUCT,Constants.INSTALL_PRODUCT_SOBJECT_ASSET);
    Set<Id> setProductIds = new Set<Id>();
    
    for(SVMXC__Installed_Product__c prod : newList){
      if((isInsert || (prod.SVMXC__Product__c != oldMap.get(prod.Id).SVMXC__Product__c) ) && prod.RecordTypeid == rtCOMM){
        setProductIds.add(prod.SVMXC__Product__c);
      }
    }
    
    if(setProductIds.isEmpty()){
      return;
    }
    
    if(setProductIds.size() > 0){
      map<Id,Product2> mapProductIdToProduct = new map<Id,Product2>([SELECT Id,QIP_ID__c FROM Product2 WHERE Id IN: setProductIds]);
    
      for(SVMXC__Installed_Product__c prod : newList){
        if((isInsert || (prod.SVMXC__Product__c != oldMap.get(prod.Id).SVMXC__Product__c) ) && prod.RecordTypeid == rtCOMM){
          if(prod.SVMXC__Product__c != null){
            prod.QIP_ID__c = mapProductIdToProduct.get(prod.SVMXC__Product__c).QIP_ID__c;
          }else{
            prod.QIP_ID__c = null;
          }
          
        }
      }
    }
  }
  
  //Send a mail to Related case owner and technician if there a hold on account(Varun Vasishtha T-514084) 
  private static void HoldRelesedNotification(List<SVMXC__Installed_Product__c> newItems,Map<Id,SVMXC__Installed_Product__c> oldItems)
  {
    Set<Id> assetList = new Set<Id>();
    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    Id templateId = [select id from EmailTemplate where DeveloperName = :holdTemplate].id;
    for(SVMXC__Installed_Product__c entity : newItems)
    {
        SVMXC__Installed_Product__c oldEntity = oldItems.get(entity.Id);
        if(oldEntity.Hold_Status__c !=  entity.Hold_Status__c && string.isBlank(entity.Hold_Status__c))
        {
            assetList.add(entity.Id);
        }
    }
    if(assetList.size() > 0 && templateId != null){
      List<string> woStatus = new List<string>{'Closed','Canceled','Rejected'};
      List<SVMXC__Service_Order__c> relatedWorkOrders = [select SVMXC__Component__c,SVMXC__Component__r.Name,SVMXC__Case__r.OwnerId,SVMXC__Group_Member__r.SVMXC__Salesforce_User__c,
      SVMXC__Member_Email__c from  SVMXC__Service_Order__c where SVMXC__Component__c IN: assetList and SVMXC__Order_Status__c not in :woStatus];
      for(SVMXC__Service_Order__c related : relatedWorkOrders)
      {
          boolean isUser = COMM_CaseUtil.IsUser(related.SVMXC__Case__r.OwnerId);
          if(related.SVMXC__Case__r.OwnerId != null && isUser)
          {
              Messaging.SingleEmailMessage caseMail = new Messaging.SingleEmailMessage();
              caseMail.setTemplateId(templateId);
              caseMail.setSaveAsActivity(false);
              caseMail.setTargetObjectId(related.SVMXC__Case__r.OwnerId);
              caseMail.setWhatId(related.SVMXC__Component__c);
              emails.add(caseMail);
          }
          if(string.isNotBlank(related.SVMXC__Member_Email__c))
          {
              string body = '<p>A hold has been released on the following record:</p>';
              body += '<p>Asset Name :'+ related.SVMXC__Component__r.Name+'</p>';
              body += '<p>For detail please follow this link : <a href='+ System.Label.COMMBaseURL +'/'+ related.SVMXC__Component__c +'>'+related.SVMXC__Component__r.Name+'</a></p>';
              string subject = 'A Hold has been released';
              Messaging.SingleEmailMessage caseMail = new Messaging.SingleEmailMessage();
              caseMail.setSaveAsActivity(false);
              caseMail.setToAddresses(new List<string>{related.SVMXC__Member_Email__c});
              caseMail.setHtmlBody(body);
              caseMail.setSubject(subject);
              emails.add(caseMail);
              
          }
      }
      if(emails.size() > 0)
        Messaging.sendEmail(emails); 
    }
    
  }

  //-----------------------NB - 10/24 - I-238609 Start-----------------------------------------------------------
  // Method to calculate warranty end date of Asset
  public static void setWarrantyDate(List<SVMXC__Installed_Product__c> newList, map<Id,SVMXC__Installed_Product__c> oldMap){
    Set<Id> setProductIds = new Set<Id>();
    Boolean isUpdate = oldMap == null ? false : true;

    for(SVMXC__Installed_Product__c prod : newList){
      if((!isUpdate || oldMap.get(prod.Id).SVMXC__Warranty_Start_Date__c != prod.SVMXC__Warranty_Start_Date__c) && prod.RecordTypeid == rtCOMM){//Start date is changed 
        if(prod.SVMXC__Warranty_Start_Date__c != null && prod.SVMXC__Product__c != null){ //Start date not equal to null
          setProductIds.add(prod.SVMXC__Product__c);
        } else if(prod.SVMXC__Warranty_Start_Date__c == null){ //Start date equal to null
          prod.SVMXC__Warranty_End_Date__c = null;
        }  
      }
    } 

    if(setProductIds.size() > 0){
      map<Id,Product2> mapProductIdToProduct = new map<Id,Product2>([SELECT Id, Product_Class_Code__c FROM Product2 WHERE Id IN: setProductIds]);
      //map<Id, ProductCodeCustomSetting__c> productClassCodeCustomSetting = new map<Id, ProductCodeCustomSetting__c>([Select Id, Name, Class_Code__c, Warranty_Duration_Months__c From ProductCodeCustomSetting__c Where Warranty_Duration_Months__c != null]);
      map<String, ProductCodeCustomSetting__c> productClassCodeCustomSetting = ProductCodeCustomSetting__c.getall();
      Boolean reset = false;
      for(SVMXC__Installed_Product__c prod : newList){
        if((!isUpdate || oldMap.get(prod.Id).SVMXC__Warranty_Start_Date__c != prod.SVMXC__Warranty_Start_Date__c)
          && prod.RecordTypeid == rtCOMM && prod.SVMXC__Warranty_Start_Date__c != null && prod.SVMXC__Product__c != null){
          if(mapProductIdToProduct.get(prod.SVMXC__Product__c).Product_Class_Code__c != null){ //Product class code doesn't equal to null
            reset = false;
            for(ProductCodeCustomSetting__c customSetting : productClassCodeCustomSetting.values()){
              //if product class code matches and warranty duration is not null
              if(customSetting.Class_Code__c != null && customSetting.Warranty_Duration_Months__c != null && 
                customSetting.Class_Code__c == mapProductIdToProduct.get(prod.SVMXC__Product__c).Product_Class_Code__c){ 
                prod.SVMXC__Warranty_End_Date__c = prod.SVMXC__Warranty_Start_Date__c.addMonths(Integer.valueOf(customSetting.Warranty_Duration_Months__c));
                reset = true;
                break;
              }
            }
            if(!reset){ //if warranty duration is null or product class code deosn't match
              prod.SVMXC__Warranty_Start_Date__c = null;
              prod.SVMXC__Warranty_End_Date__c = null;
            }
          }else{ //product class code is null
            prod.SVMXC__Warranty_End_Date__c = null;
            prod.SVMXC__Warranty_Start_Date__c = null;
          }
        }
      }
    }
  }  
  //-----------------------NB - 10/24 - I-238609 End-----------------------------------------------------------  
  
  
}