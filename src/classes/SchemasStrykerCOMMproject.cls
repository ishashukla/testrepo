/**================================================================      
* Appirio, Inc
* Name: SchemasStrykerCOMMproject  
* Description: T-492006 (WSDL Generated class for project interface)
* Created Date: 05/15/2016
* Created By: Deepti Maheshwari (Appirio)
*
* Date Modified      Modified By      Description of the update
 
==================================================================*/
public class SchemasStrykerCOMMproject {
    public class ProjectsType {
        public SchemasStrykerCOMMproject.ProjectType[] Project;
        private String[] Project_type_info = new String[]{'Project','http://schemas.stryker.com/LocalProject/V1',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.stryker.com/LocalProject/V1','true','false'};
        private String[] field_order_type_info = new String[]{'Project'};
    }
    public class ProjectRequestType {
        public SchemasStrykerCOMMproject.IntegrationHeaderType IntegrationHeader;
        public SchemasStrykerCOMMproject.ProjectsType Projects;
        private String[] IntegrationHeader_type_info = new String[]{'IntegrationHeader','http://schemas.stryker.com/LocalProject/V1',null,'1','1','false'};
        private String[] Projects_type_info = new String[]{'Projects','http://schemas.stryker.com/LocalProject/V1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.stryker.com/LocalProject/V1','true','false'};
        private String[] field_order_type_info = new String[]{'IntegrationHeader','Projects'};
    }
    public class ProjectType {
        public String businessUnit;
        public String projectNumber;
        public String projectDescription;
        public Date startDate;
        public Date completionDate;
        public String resourceId;
        public String projectId;
        public Integer salesOrderId;
        public String longName;
        public String projectOrganization;
        public String projectStatus;
        private String[] businessUnit_type_info = new String[]{'businessUnit','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] projectNumber_type_info = new String[]{'projectNumber','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] projectDescription_type_info = new String[]{'projectDescription','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] startDate_type_info = new String[]{'startDate','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] completionDate_type_info = new String[]{'completionDate','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] resourceId_type_info = new String[]{'resourceId','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] projectId_type_info = new String[]{'projectId','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] salesOrderId_type_info = new String[]{'salesOrderId','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] longName_type_info = new String[]{'longName','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] projectOrganization_type_info = new String[]{'projectOrganization','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] projectStatus_type_info = new String[]{'projectStatus','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.stryker.com/LocalProject/V1','true','false'};
        private String[] field_order_type_info = new String[]{'businessUnit','projectNumber','projectDescription','startDate','completionDate','resourceId','projectId','salesOrderId','longName','projectOrganization','projectStatus'};
    }
    public class ProjectsResponseType {
        public SchemasStrykerCOMMproject.ResponseType[] Project;
        private String[] Project_type_info = new String[]{'Project','http://schemas.stryker.com/LocalProject/V1',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.stryker.com/LocalProject/V1','true','false'};
        private String[] field_order_type_info = new String[]{'Project'};
    }
    public class IntegrationHeaderType {
        public String MessageID;
        public String CreationDateTime;
        public String SenderIdentifier;
        public String ReceiverIdentifier;
        private String[] MessageID_type_info = new String[]{'MessageID','http://schemas.stryker.com/LocalProject/V1',null,'1','1','false'};
        private String[] CreationDateTime_type_info = new String[]{'CreationDateTime','http://schemas.stryker.com/LocalProject/V1',null,'1','1','false'};
        private String[] SenderIdentifier_type_info = new String[]{'SenderIdentifier','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] ReceiverIdentifier_type_info = new String[]{'ReceiverIdentifier','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.stryker.com/LocalProject/V1','true','false'};
        private String[] field_order_type_info = new String[]{'MessageID','CreationDateTime','SenderIdentifier','ReceiverIdentifier'};
    }
    public class ProjectResponseType {
        public SchemasStrykerCOMMproject.IntegrationHeaderType IntegrationHeader;
        public SchemasStrykerCOMMproject.ProjectsResponseType Projects;
        private String[] IntegrationHeader_type_info = new String[]{'IntegrationHeader','http://schemas.stryker.com/LocalProject/V1',null,'1','1','false'};
        private String[] Projects_type_info = new String[]{'Projects','http://schemas.stryker.com/LocalProject/V1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.stryker.com/LocalProject/V1','true','false'};
        private String[] field_order_type_info = new String[]{'IntegrationHeader','Projects'};
    }
    public class ResponseType {
        public String sourceIdentifier;
        public String responseCode;
        public String responseMessage;
        private String[] sourceIdentifier_type_info = new String[]{'sourceIdentifier','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] responseCode_type_info = new String[]{'responseCode','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] responseMessage_type_info = new String[]{'responseMessage','http://schemas.stryker.com/LocalProject/V1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.stryker.com/LocalProject/V1','true','false'};
        private String[] field_order_type_info = new String[]{'sourceIdentifier','responseCode','responseMessage'};
    }
}