//
// (c) 2016 Appirio, Inc.
//
//  Extention class for visualforce page CaseAssetsAssociations
//
// 13 July 2016   Jagdeep Juneja
// 01 Aug , 2016  Sahil Batra  I-227043 Modified (Rewritten code to add Installed products)
// 01 Dec , 2016  Isha Shukla  I-245737 Modified (Added page message when Asset is not related to Account)
public class CaseAssetsAssociationsExtension {
    // variable declarations
    public String caseObjId{get;set;}
    public Id selectedAssett{get;set;}
    public Id selectedInstalledProduct{get;set;}
    public Case caseObject{get;set;}
    public List<Case_Asset_Relationship__c> caseAssetList{get;set;}
    public Map<Id,String> idToSerialMap{get;set;}
    public Integer totalCount{get;set;}
    public Integer minLen{get;set;}
    
    // Constructor
    public CaseAssetsAssociationsExtension() {
        caseObjId = ApexPages.currentPage().getParameters().get('Id');
        caseObject = new Case();
        totalCount = 0;
        minLen = 2;
        if(caseObjId != null){
            caseObject = [SELECT id , AccountId FROM Case WHERE ID=:caseObjId ];
            Id accountId = caseObject.AccountId;
            totalCount = database.countQuery('SELECT count() FROM SVMXC__Installed_Product__c WHERE SVMXC__Company__c =:accountId LIMIT 50');
            if(totalCount < 6){
                minLen = 0;
            }
        }
        idToSerialMap = new Map<Id,String>();
        caseAssetList = new List<Case_Asset_Relationship__c>();
    }
    
    //add new record to List
    public void addInstalledProduct(){
        Case_Asset_Relationship__c obj = new Case_Asset_Relationship__c();
        obj.Case__c = caseObjId;
        obj.Asset__c = selectedInstalledProduct;
        idToSerialMap.put(selectedInstalledProduct,'-');
        if(selectedInstalledProduct!=null){
            SVMXC__Installed_Product__c tempObj = new SVMXC__Installed_Product__c();
            tempObj = [SELECT SVMXC__Serial_Lot_Number__c FROM SVMXC__Installed_Product__c WHERE Id=:selectedInstalledProduct];
            if(tempObj!=null && tempObj.SVMXC__Serial_Lot_Number__c != null && tempObj.SVMXC__Serial_Lot_Number__c != ''){
                idToSerialMap.put(selectedInstalledProduct,tempObj.SVMXC__Serial_Lot_Number__c);
            }
        }
        caseAssetList.add(obj);
        selectedAssett =null;
    }
    
    // save method
    public PageReference save(){
        if(caseAssetList.size() > 0){
            insert caseAssetList;
            PageReference page = new PageReference('/' + caseObjId);
        	page.setRedirect(true);
        	return page;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'No Match Found for this Asset.'));
            return null;
        }
        
    }
    
    //cancel method
    public PageReference cancel(){
        PageReference page = new PageReference('/' + caseObjId);
        page.setRedirect(true);
        return page;
    }
    
    //delete product
     public void deleteProduct(){
         List<Case_Asset_Relationship__c> temp = new List<Case_Asset_Relationship__c>();
         for(Case_Asset_Relationship__c obj : caseAssetList){
            if(!obj.is_Delete__c){
                temp.add(obj);
            }
         }
         caseAssetList = new List<Case_Asset_Relationship__c>();
         caseAssetList = temp;
     }
}