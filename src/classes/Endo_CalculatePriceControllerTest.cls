/*************************************************************************************************
Created By:    Sunil Gupta
Date:          Feb 17, 2014
Description  : Test class for Endo_CalculatePriceController
**************************************************************************************************/
@isTest(seeAllData = true)
private class Endo_CalculatePriceControllerTest {
  static testMethod void testMethod1() {
    // Create test data
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    insert acc;
        
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;
      
    Product2 product = Endo_TestUtils.createProduct(1, false).get(0);
    product.Part_Number__c = '12345';
    product.Major_Code__c='12367';
    insert product;
    
    
    Pricebook2 pb = Endo_TestUtils.getStandardPricebook();
    
    PricebookEntry standardPrice = Endo_TestUtils.getTestPricebookEntry(pb.Id, product.Id);
    standardPrice.UnitPrice = 10.0;
    update standardPrice;
      
    Pricing_Contract__c pc = Endo_TestUtils.createPC(1, false).get(0);
    insert pc;
    
    Pricing_Contract_Account__c junction = Endo_TestUtils.createAccountPricingContractJunction(1, acc.Id, pc.Id, false).get(0);
    junction.IsActive__c=true;
    insert junction; 
    
    Pricing_Contract_Line_Item__c pcli = Endo_TestUtils.createPCLI(1, false).get(0);
    pcli.Product__c = product.Id;
    pcli.Application_Method__c = 'Newprice';
    pcli.Value__c = 50;
    pcli.Pricing_Contract__c = pc.Id;
    insert pcli;
    
    Pricing_Contract_Line_Item__c pcli2 = Endo_TestUtils.createPCLI(1, false).get(0);
    pcli2.Product__c = product.Id;
    pcli2.Pricing_Contract__c = pc.Id;
    pcli2.Application_Method__c = 'Newprice';
    pcli2.Value__c = 60;
    insert pcli2;
    
    
    Test.startTest();
      
    Endo_CalculatePriceController obj = new Endo_CalculatePriceController(new ApexPages.StandardController(acc));
    obj.currentPLI = pcli;
    obj.showPriceList();
    List<Endo_PriceWrapper> lst = obj.lstSalesPrices;
    obj.sortPriceAction(); 
    obj.createNewCase();
    //System.assertEquals();
    Test.stopTest();
  }
    
    
    // method to test scenario when list price is greater than sell price
  static testMethod void testMethod2() {
    // Create test data
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    insert acc;
        
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;
      
    Product2 product = Endo_TestUtils.createProduct(1, false).get(0);
    product.Part_Number__c = '12345';
    product.Major_Code__c='12367';
    insert product;
    
    
    Pricebook2 pb = Endo_TestUtils.getStandardPricebook();
    
    PricebookEntry standardPrice = Endo_TestUtils.getTestPricebookEntry(pb.Id, product.Id);
    standardPrice.UnitPrice = 1000.0;
    update standardPrice;
      
    Pricing_Contract__c pc = Endo_TestUtils.createPC(1, false).get(0);
    insert pc;
    
    Pricing_Contract_Account__c junction = Endo_TestUtils.createAccountPricingContractJunction(1, acc.Id, pc.Id, false).get(0);
    junction.IsActive__c=true;
    insert junction; 
    
    Pricing_Contract_Line_Item__c pcli = Endo_TestUtils.createPCLI(1, false).get(0);
    pcli.Product__c = product.Id;
    pcli.Application_Method__c = 'Newprice';
    pcli.Value__c = 50;
    pcli.Pricing_Contract__c = pc.Id;
    insert pcli;
    
    Pricing_Contract_Line_Item__c pcli2 = Endo_TestUtils.createPCLI(1, false).get(0);
    pcli2.Product__c = product.Id;
    pcli2.Pricing_Contract__c = pc.Id;
    pcli2.Application_Method__c = 'Newprice';
    pcli2.Value__c = 60;
    insert pcli2;
    
    
    Test.startTest();
      
    Endo_CalculatePriceController obj = new Endo_CalculatePriceController(new ApexPages.StandardController(acc));
    obj.currentPLI = pcli;
    obj.showPriceList();
    List<Endo_PriceWrapper> lst = obj.lstSalesPrices;
    obj.sortPriceAction(); 
    obj.createNewCase();
    //System.assertEquals();
    Test.stopTest();
  }  
}