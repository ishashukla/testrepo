/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 11, 2013
 Purpose       :  REST Webservice for saving Intel and its child records data.
 Reference     :  Task T-205116
 Modifications :  Task T-214509     Nov 29, 2013    Sonal Shrivastava
                  Task T-205602     Dec 17, 2013    Sonal Shrivastava
                  ID Return Issue   Dec 19, 2013    Brandon Jones
*******************************************************************************/
@RestResource(urlMapping='/IntelSaveService/*') 
global without sharing class IntelSaveService {
    
	private static final String ERROR = 'Error : ';
	private static final String INVALID_DATA = 'Invalid Data';
	private static final String SAVE_SUCCESS = 'Records saved successfully';
	private static final String NO_RECORDS_FOUND = 'No records found';
  
  //****************************************************************************
  // POST Method for REST service
  //****************************************************************************  
  @HttpPost 
  global static Map<String, String> doPost() {
    RestRequest req = RestContext.request;
    Map<String, String> res = new Map<String,String>();
    
    String result;    
    try {   
      //parse the requestBody into a string
      if(req.requestBody != null) {                
        Blob reqBody = req.requestBody;   
        res = parseRecords(reqBody.toString());      
      } 
      else{
        res.put(ERROR, INVALID_DATA);
        res.put('requestBody', req.requestBody.toString());
      }
    }
    catch(Exception ex){
      res.put('error', ex.getMessage() + ' on line ' + ex.getLineNumber());
      res.put('requestBody', req.requestBody.toString());
      return res;
    }    
    return res;
  }
  
  //****************************************************************************
  // Method for getting Intel and child records from the Request string
  //****************************************************************************
  private static Map<String, String> parseRecords(String reqBody){
    Map<String, String> mapIntelId_ChatterPostId = new Map<String, String>();
    IntelListWrapper wrap = (IntelListWrapper)JSON.deserialize(reqBody, IntelListWrapper.class);
    String result;          
    Map<Integer, Intel__c> mapCount_Intel = new Map<Integer, Intel__c>();
    Map<Integer, List<Tag__c>> mapCount_TagList = new Map<Integer, List<Tag__c>>();
    Intel__c intel;
    Integer cnt = 0;
    try{ 
      if(wrap != null && wrap.intelList != null && wrap.intelList.size() > 0){ 
        for(IntelListWrapper.IntelJson intelwrap : wrap.intelList){
            //Add Intel records in a map
            intel = new Intel__c( Details__c = intelwrap.content,
                                  OwnerId = intelwrap.postedById);
            
            //Add Tag records List in a map 
            if((intelwrap.tags != null) && (intelwrap.tags.size() > 0)){
                Tag__c tag;
                for(IntelListWrapper.TagJson tagWrap : intelwrap.tags){
                    
                //Include Tag Title in Intel Related Tags field 
                if(tagWrap.Title != null && tagWrap.Title != ''){
                    intel.Related_Tags__c = (intel.Related_Tags__c == null) ? (tagWrap.Title) : (intel.Related_Tags__c + ', ' + tagWrap.Title);
                }                   
                //Create a tag object
                tag = new Tag__c();
                if(tagWrap.Account != null) tag.Account__c = tagWrap.Account;
                if(tagWrap.Contact != null) tag.Contact__c = tagWrap.Contact;
                if(tagWrap.Product != null) tag.Product__c = tagWrap.Product;
                if(tagWrap.User != null) tag.User__c = tagWrap.User;                
                
                if(!mapCount_TagList.containsKey(cnt)){
                mapCount_TagList.put(cnt, new List<Tag__c>());
                } 
                mapCount_TagList.get(cnt).add(tag);                                 
                }
            }
            mapCount_Intel.put(cnt, intel);           
            cnt ++;
        }
        }
        mapIntelId_ChatterPostId = insertRecords(mapCount_Intel, mapCount_TagList); 
    }
    catch(Exception ex){
        mapIntelId_ChatterPostId.put(ERROR, ex.getMessage() + ' on line ' + ex.getLineNumber());
        return mapIntelId_ChatterPostId;
    }
    return mapIntelId_ChatterPostId;
  }
  
  //****************************************************************************
  // Method for saving Intel and its child records
  //****************************************************************************
  private static Map<String, String> insertRecords(Map<Integer, Intel__c> mapCount_Intel, 
                                      Map<Integer, List<Tag__c>> mapCount_TagList){
    String result;
    List<Tag__c> tagList = new List<Tag__c>();
    Map<String, String> mapIntelId_ChatterPostId = new Map<String, String>();
    
    Savepoint sp = Database.setSavepoint();
    try{
      if(mapCount_Intel.size() > 0){
        insert mapCount_Intel.values();
        
        if(mapCount_TagList.size() > 0){
            for(Integer cnt : mapCount_TagList.keySet()){
            String intelId = mapCount_Intel.get(cnt).Id;
            for(Tag__c tag : mapCount_TagList.get(cnt)){
                tag.Intel__c = intelId;
                tagList.add(tag);   
            }
            }
        }
        if(tagList.size() > 0){
          insert tagList;
        }         
        result = SAVE_SUCCESS;
      }
      else{
        result = NO_RECORDS_FOUND;
      }
      mapIntelId_ChatterPostId.put('response', result);
      getChatterPostIds(mapCount_Intel.values(), mapIntelId_ChatterPostId);      
    }
    catch(Exception ex){
        Database.rollback(sp);
        mapIntelId_ChatterPostId.put(ERROR, ex.getMessage() + ' on line ' + ex.getLineNumber());
      return mapIntelId_ChatterPostId;
    }
    return mapIntelId_ChatterPostId;
  }
  
  //****************************************************************************
  // Method for fetching chatter post Ids
  //****************************************************************************
  private static void getChatterPostIds(List<Intel__c> intelList,
                                                       Map<String, String> mapIntelId_ChatterPostId){
    for(Intel__c intel : [SELECT Id, Chatter_Post__c 
                          FROM Intel__c 
                          WHERE Id IN :intelList]){
        mapIntelId_ChatterPostId.put('IntelId',  intel.Id);
        mapIntelId_ChatterPostId.put('ChatterPostId', intel.Chatter_Post__c);
    }
  }
}