//
// (c) 2015 Appirio, Inc.
//
// 04 Feb 2016 Sunil
//
// Utility Class to parse Quote Builder Result
//
//


public with sharing class Medical_QBUtility {

	public static QBWrapper getFormattedResult(String resultBody){
    QBWrapper objWrapper = new QBWrapper();
    resultBody = resultBody;
    resultBody = resultBody.replaceAll('&lt;', '<');
    resultBody = resultBody.replaceAll('&gt;', '>');
    System.debug('###' + resultBody);

    // Reultant string is having extra line in XML file
    resultBody = resultBody.replace('<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">', '');
    resultBody = resultBody.replace('</string>', '');

    // Need to check if we can use "Dom.Document doc = res.getBodyDocument();" here

    Dom.Document doc = new Dom.Document();
    doc.load(resultBody);

		//Retrieve the root element for this document.
		Dom.XMLNode address = doc.getRootElement();
		objWrapper.errorCode = address.getChildElement('ErrorCode', null).getText();

		objWrapper.errrorDescription = address.getChildElement('ErrorDescription', null).getText();

		objWrapper.uniqueReqId = address.getChildElement('UniqueReqId', null).getText();

		// Alternatively, loop through the child elements.
		// This prints out all the elements of the address
		//for(Dom.XMLNode child : address.getChildElements()) {
		//   System.debug('@@@' + child.getText());
		//}

    return objWrapper;
  }

	public class QBWrapper{
		public String errorCode;
		public String errrorDescription;
		public String uniqueReqId;
	}



}