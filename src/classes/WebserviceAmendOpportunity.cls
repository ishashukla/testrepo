//
// (c) 2012 Appirio, Inc.
//
//  Webservice class called from Opportunity custom button
//
// 20 July 2015     Sunil
//Updated By      Date(d-m-y) Ref No      Description
//Naresh Shiwani  21-07-2015  T-412274    Updated whole logic of amendOppRecord()
//                                        and added getCreatableFieldsSOQL()
//Naresh Shiwani  27-07-2015  I-173316    Updated amendOppRecord() to get max Opportunity_Number__c
//                                        and update Opportunity_Number__c accordingly 
//Naresh Shiwani  28-07-2015  I-173316    Updated amendOppRecord() set Starting_Schedule_Number__c to null
//                                        And queried Max Opportunity_Number_Value__c.
//Naresh Shiwani  28-07-2015  I-173276    Updated amendOppRecord() modified StageName for new Amended Opportunity.
//Naresh Shiwani  05-08-2015  I-174129    Updated amendOppRecord() to clone flex contact record.
//Naresh Shiwani  05-08-2015  I-174819    Updated amendOppRecord() for new opp quoting stage.
//Naresh Shiwani  28-08-2015  Test Class  Set Contract_Sign_Date__c of new opp to null because of Validation rule
//                                        failure in test class.                                          
//Herb Whitacre   10-12-2015  I-195079    Update amendOppRecord() to use Opportunity_Number from new opp (not old cloned value).
//Herb Whitacre   28-12-2015  I-196857    During Amend Opportunity don't create new or modify old Contract.
global class WebserviceAmendOpportunity{

  // This webservice method is being called from Custom button on Opportunity.
  webservice static String amendOppRecord(Id  recordId){
    SavePoint              sp                       = Database.setSavepoint();
    Long                   maxOppNumber;
    AggregateResult[]      groupedResults;
    //List<Flex_Contract__c> existingFlexContractList = new List<Flex_Contract__c>();
    //List<Flex_Contract__c> newFlexContractList      = new List<Flex_Contract__c>();
    //Flex_Contract__c       newFlexContract;
      
    try{
      groupedResults = [SELECT Max(Opportunity_Number_Value__c)maxOppNum FROM Opportunity];        
      maxOppNumber = Long.valueOf(groupedResults[0].get('maxOppNum')+'');
      if(maxOppNumber == null || maxOppNumber < 1){
        maxOppNumber = 0;
      }
      String soql = WebserviceAmendOpportunity.getCreatableFieldsSOQL('Opportunity','id=\''+recordId+'\'');
      // Fetched Existing Opportunity & Cloned it.
      Opportunity existingOpp = (Opportunity)Database.query(soql);
      if(maxOppNumber == 999999999){
        existingOpp.Opportunity_Number__c.addError('Reached Max Limit of Opportunity Number');
      }
      Opportunity newOpp = existingOpp.clone(false, true);
      newOpp.Opportunity_Number__c =  String.valueOf(maxOppNumber + 1);
      newOpp.Starting_Schedule_Number__c= null;
      newOpp.StageName = 'Quoting';
      newOpp.Probability = 10;
      newOpp.Amend_Opportunity__c = existingOpp.Id;
      newOpp.CloseDate = System.today() + 90;
      newOpp.Contract_Sign_Date__c = null;
      insert newOpp;        
      
/*    //Start I-196857 - Herb W. - Commenting out the following block
      if(existingOpp.StageName=='Closed Won'){
        //Start I-174129 Naresh K Shiwani Changed Position of code added in condition "Closed Won"
        // Fetch Flex Contract records for existing Opp & Clone Flex Contract records and associate with new Opp.
          soql = WebserviceAmendOpportunity.getCreatableFieldsSOQL('Flex_Contract__c','Opportunity__c=\''+recordId+'\'');
          for(Flex_Contract__c existingFlexContract : Database.query(soql)){
            existingFlexContract.Status__c = 'Rebooked';
            existingFlexContractList.add(existingFlexContract);
            newFlexContract = existingFlexContract.clone(false, true);
            newFlexContract.Status__c = 'New';
            newFlexContract.LPO_Nbr__c = null;
            newFlexContract.Opportunity__c = newOpp.Id;
            newFlexContract.Opportunity_Number__c = newOpp.Opportunity_Number__c; //Issue I-195079
            newFlexContractList.add(newFlexContract);
          }
          if(existingFlexContractList.size() > 0 && newFlexContractList.size() > 0){
            update existingFlexContractList;
            insert newFlexContractList;
          }
      }
      //End I-174129
*/    //End I-196857 (comment out the block)
      
      return (newOpp.Id);
    }
    catch(Exception ex){
      Database.rollback(sp);
      return 'error:' + ex.getMessage();
    }
  }

  // Returns a dynamic SOQL statement for the whole object, includes only creatable fields
  // since we will be inserting a cloned result of this query
  public static string getCreatableFieldsSOQL(String objectName, String whereClause){
    String selects = '';
    if (whereClause == null || whereClause == ''){
      return null;
    }
    // Get a map of field name and field token
    Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
    list<string> selectFields = new list<string>();
    if (fMap != null){
      for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
        Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
        if (fd.isCreateable()){ // field is creatable
          selectFields.add(fd.getName());
        }
      }
    }
    if (!selectFields.isEmpty()){
      for (string s:selectFields){
        selects += s + ',';
      }
      if (selects.endsWith(',')){
          selects = selects.substring(0,selects.lastIndexOf(','));
        }
    }
    return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
  }

}