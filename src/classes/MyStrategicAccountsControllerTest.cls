@isTest
public class MyStrategicAccountsControllerTest
{
	@isTest
	static void testingMyStrategicAccount() {
		List<Account> tas = TestUtils.createAccount(1,false);
		insert tas;
		Strategic_Account__c sac = new Strategic_Account__c(Account__c = tas[0].id, User__c = UserInfo.getUserId() );
		insert sac;
		test.startTest();
		PageReference pageRef = Page.MyStrategicAccounts;
    Test.setCurrentPage(pageRef);
    ApexPages.StandardController sc = new ApexPages.standardController(tas[0]);
    MyStrategicAccountsController sic = new MyStrategicAccountsController(sc);
	  test.stopTest();
		
	}
}