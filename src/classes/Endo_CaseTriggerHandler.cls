/*************************************************************************************************
* Created By:    Sunil Gupta
* Date:          Jan 27, 2014
* Description  : Handler class for Case Trigger
* 
* Date           Updated by              Description
* 2016-12-14     Nathalie Le Guay        I-247623 - Added call to Medical_CaseTriggerHandler.sendCasesToTrackwise()
**************************************************************************************************/
public class Endo_CaseTriggerHandler { 
    //Added by Jyoti for Case 00168349
    public static final String ENDO_CUSTOMER = 'Endo COMM Customer';
    public static final String ENDO_SALES_REP = 'Endo Sales Rep';
    //End - Case 00168349
    
    
    //-----------------------------------------------------------------------------------------------
   /**================================================================      
    * Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/
    public static boolean IsActive(){
        return (TriggerState.isActive('Endo_CaseTrigger') || 
                (!UserInfo.getUserName().startsWith('dataload.usernt@stryker.com')));
    }
    
    public static void OnBeforeInsert(List<Case> newCases){
        // T-242025 Auto-populate "Sales Rep" lookup for Case type :Sales Order Entry
        //Endo_CaseTriggerHandler.populateSalesRepEmailFieldOnCase(newEndoCase, Trigger.oldMap); // Update Sales Rep Email Field on Case

        populateSalesRep(newCases, null);
        populateSaleSupportAgent(newCases, null);
        populateAssignedStateForEmailToCase(newCases);

    }
    public static void OnAfterInsert(List<Case> newCases){
        // Notify If Tech Support case is already Exists in previous 7 days.
        notifySalesRepIfCaseAlreadyExists(newCases,null);
        TrackHistory(newCases, null);

        //  T-253588 Add Sales Support Agent to Support Case
        //Endo_CaseTriggerHandler.populateSalesSupportAgent(Trigger.new, Trigger.oldMap); 
        
        // NLG - 2016-12-14 - Added as Endo uses Trackwise trigger-integration as well
        Medical_CaseTriggerHandler.sendCasesToTrackwise(newCases, null);
    }
    public static void OnAfterInsertAsync(Set<ID> newCaseIDs){}
    
    public static void OnBeforeUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){
        // T-242025 Auto-populate "Sales Rep" lookup for Case type :Sales Order Entry
        //Endo_CaseTriggerHandler.populateSalesRepEmailFieldOnCase(newEndoCase, Trigger.oldMap); // Update Sales Rep Email Field on Case

        populateSalesRep(newCases, oldCaseMap);
        populateSaleSupportAgent(newCases, oldCaseMap);
    }
    public static void OnAfterUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){
        // T-237859 Update Mood Value on Related Account.
        updateAccountMoodValue(newCases, oldCaseMap);
        TrackHistory(newCases, oldCaseMap);

        //  T-253588 Add Sales Support Agent to Support Case
        //Endo_CaseTriggerHandler.populateSalesSupportAgent(Trigger.new, Trigger.oldMap); 
        
        // NLG - 2016-12-14 - Added as Endo uses Trackwise trigger-integration as well
        Medical_CaseTriggerHandler.sendCasesToTrackwise(newCases, oldCaseMap);
    }
    public static void OnAfterUpdateAsync(Set<ID> updatedCaseIDs){}
    
    public static void OnBeforeDelete(List<Case> CasesToDelete, Map<ID, Case> caseMap){}
    public static void OnAfterDelete(List<Case> deletedCases, Map<ID, Case> caseMap){}
    public static void OnAfterDeleteAsync(Set<ID> deletedCaseIDs){}
    
    public static void OnUndelete(List<Case> restoredCases){}
    /**================================================================      
    * End of Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/

    
    
    
  //-----------------------------------------------------------------------------------------------
  // T-242025 populate Sales Rep lookup
  //-----------------------------------------------------------------------------------------------
  public static void populateSalesRep(List<Case> lstNewCases, Map<Id, Case> mapOldCases) {
    //Get Record Type Id.
    //Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo Customer').getRecordTypeId();
    Id[] accountRecordTypeIds = new id[]{};
    accountRecordTypeIds.add(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo Sales Rep').getRecordTypeId());
    accountRecordTypeIds.add(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId());
    Id contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact').getRecordTypeId();

    String caseAccount = null;
    String contAccount = null;
    Contact tempContact = new Contact(); 
    Contact caseCont = new Contact();
    List<Id> contList = new List<Id>(); 
    
    System.debug('*** accountRecordTypeIds = ' + accountRecordTypeIds );
    System.debug('*** contactRecordTypeId = ' + contactRecordTypeId );

    //Filter case records
    Set<Id> setAccountIds = new Set<Id>();
    List<Case> selectedCases = new List<Case>();
    for(Case c :lstNewCases){
      if((mapOldCases == null || mapOldCases.get(c.Id).AccountId != c.AccountId || mapOldCases.get(c.Id).ContactId != c.ContactId)){
        setAccountIds.add(c.AccountId);
        selectedCases.add(c);
        if(c.ContactId != null){
           contList.add(c.ContactId);  
        }
      }
    }
    System.debug('@@@ss' + setAccountIds);
    
    List<Stryker_Contact__c> lstStrykerContacts = [SELECT Customer_Account__c, Internal_Contact__c FROM Stryker_Contact__c 
                                  WHERE Customer_Account__c IN :setAccountIds
                                  AND Internal_Contact__r.Title Like 'Sales Rep%'
                                  AND IsActive__c = true
                                  AND Customer_Account__r.RecordTypeId IN :accountRecordTypeIds];
    
    System.debug('@@@ss' + lstStrykerContacts);
    
    // Prepare map for Account vs Contact(Title= 'sales Rep')
    map<Id, List<Stryker_Contact__c>> mapAccountIdContacts = new map<Id, List<Stryker_Contact__c>>();
    for(Stryker_Contact__c con :lstStrykerContacts){
      if(!mapAccountIdContacts.containsKey(con.Customer_Account__c)){
        mapAccountIdContacts.put(con.Customer_Account__c, new List<Stryker_Contact__c>());
      }
      mapAccountIdContacts.get(con.Customer_Account__c).add(con);
    }
    System.debug('@@@ss' + mapAccountIdContacts);
    
    // Update Sales Rep lookup on Case with Stryker Contact
    for(Case c :selectedCases){
      if(c.AccountId != null && mapAccountIdContacts.get(c.AccountId) != null && mapAccountIdContacts.get(c.AccountId).size() > 0){
        c.Sales_Rep__c = mapAccountIdContacts.get(c.AccountId).get(0).Internal_Contact__c;
        System.debug('@@@' + c.Sales_Rep__c);
      }
    }
    
    // Prepare set of ContactIds
    Set<Id> contactIds = new set<Id>();
    for(Case c :selectedCases){
        contactIds.add(c.ContactId);
        contactIds.add(c.Sales_Rep__c);
    }
    Map<ID, Contact> allContact = new Map<Id, Contact>([SELECT Id, AccountId, Title From Contact where Id in :contList]);
    System.debug('$$$$ allContact = ' + allContact );
    System.debug('$$$$ contList = ' + contList);
 
    // Update Sales Rep lookup on Case with Stryker Contact
    for(Case c :selectedCases){
      if(allContact.containsKey(c.ContactId)){
        if(allContact.get(c.ContactId).Title != null && allContact.get(c.ContactId).Title.containsIgnoreCase('Sales Rep')  && (allContact.get(c.ContactId).AccountId == c.AccountId)&&(allContact.get(c.ContactId).AccountId != null) ){
          c.Sales_Rep__c = allContact.get(c.ContactId).Id;
          System.debug('@@@' + c.Sales_Rep__c);
        }
      } else {    
        if(c.AccountId != null && mapAccountIdContacts.get(c.AccountId) != null && mapAccountIdContacts.get(c.AccountId).size() > 0){
          c.Sales_Rep__c = mapAccountIdContacts.get(c.AccountId).get(0).Internal_Contact__c;
          System.debug('@@@' + c.Sales_Rep__c);
        }
      }
    }
    
    // prepare map for Contacts
    map<Id, Contact> mapContacts = new map<Id, Contact>([SELECT Id, RecordTypeId FROM Contact Where Id IN :contactIds]);
    
    for(Case c :selectedCases){
      // Check if Sales Rep is still null or same (in edit case) then populate Sales rep with Case.ContactId itself
      if(c.Sales_Rep__c == null || (mapOldCases != null && mapOldCases.get(c.Id).Sales_Rep__c == c.Sales_Rep__c)){
        if(c.ContactId != null && mapContacts.get(c.ContactId).RecordTypeId == contactRecordTypeId){
          c.Sales_Rep__c = c.ContactId;
          System.debug('@@@' + c.Sales_Rep__c);
        } else {
          c.Sales_Rep__c = null;
        }
      }
    }
  }
  
  //-----------------------------------------------------------------------------------------------
  // This method contain logic related to T-259190 Sales Support Agent - override with Top 35 Sales Agent
  // This method contain logic related to T-259092 for populating old owner Id
  // Populate Sales Support Agent on Case
  //-----------------------------------------------------------------------------------------------
  public static void populateSaleSupportAgent(List<Case> lstNewCases, Map<Id, Case> mapOldCases) {
    // Populate Old Owner
    List<Group> queueList = [SELECT Id FROM Group WHERE (Name = 'Sales Support' or DeveloperName = 'Sales_Support') 
                                AND Type = 'Queue' limit 1];    
    Id salesSupportQueueId = null;
    if(!queueList.isEmpty()){
      salesSupportQueueId = queueList.get(0).Id;
    }
    
    for(Case c :lstNewCases){
      if(mapOldCases != null && mapOldCases.get(c.Id).OwnerId != c.OwnerId && salesSupportQueueId != null && mapOldCases.get(c.Id).OwnerId == salesSupportQueueId){
        c.Old_Owner_Name__c = 'Sales Support';
      } else {
        c.Old_Owner_Name__c = '';
      }
    }
    
    // prepare set of AccountIds
    set<Id> setCasesAccounts = new set<Id>();
    for(Case c :lstNewCases){
      if((mapOldCases == null || mapOldCases.get(c.Id).AccountId != c.AccountId || mapOldCases.get(c.Id).ContactId != c.ContactId || mapOldCases.get(c.Id).Sales_Rep__c != c.Sales_Rep__c)){
        if(c.AccountId != null){
          setCasesAccounts.add(c.AccountId);
        }
      }
    }
    
    if(setCasesAccounts.size() == 0)return;
    
    Map<Id, Account> accountRegionMap = new Map<Id, Account>([SELECT Id, Endo_Region_Name__c FROM Account WHERE Id IN :setCasesAccounts]);
    String strRegions = '';
    for(Account acc : accountRegionMap.values()){
      strRegions = strRegions + '\'' + acc.Endo_Region_Name__c + + '\'' + ',';
    }
    if(strRegions.endsWith(',')){
      strRegions = strRegions.subString(0, strRegions.length() - 1);
    }
    if(strRegions.length() > 0){
      strRegions = '(' + strRegions + ')';
    }
    System.debug('@@@' + strRegions);
    
    Id top35SalesAgentId = null;
    
    // Query Users on base of Regions.
    List<User> lstUsers = new List<User>();
    if(String.isBlank(strRegions) == false){
      String qry = 'SELECT Id ,Name, Title, Endo_Support_Regions__c, Top_35_Sales_Agent__c FROM User WHERE IsActive = true AND (Top_35_Sales_Agent__c = true OR Endo_Support_Regions__c INCLUDES' + strRegions + ')';
      lstUsers = Database.query(qry);
    }
    
    // Fetch users which contains the Regions as a value in u.Endo_Support_Regions__c
    map<String, String> mapSalesUsers = new map<String, String>(); 
    map<String, String> mapSampleUsers = new map<String, String>();   
      
    for(User u :lstUsers){
      if(u.Top_35_Sales_Agent__c == true){
        top35SalesAgentId = u.Id;
      } else {      
        for(String region :u.Endo_Support_Regions__c.split(';')){
          if(String.isBlank(region) == false){
            if(u.title!=null && u.title.indexof('Sales')!=-1){
              mapSalesUsers.put(region.toLowerCase(), u.Id);
            }else if(u.title!=null && (u.title.indexof('Sample')!=-1 || u.title.indexof('Samples')!=-1 )){
                  mapSampleUsers.put(region.toLowerCase(), u.Id);
            }
          }
        }    
      } 
    }
    System.debug('@@@ Sales Users' + mapSalesUsers);
    System.debug('@@@ Sample Users' + mapSampleUsers);
    System.debug('@@@' + top35SalesAgentId);
    
    // As Top_35_Sales_Rep__c is a formula field 'Sales_Rep__r.Top_35_Sales_Agent__c' which
    // do not populate in before trigger so quering this.
    set<Id> setSalesRepIds = new set<Id>();
    for(Case c :lstNewCases){
        setSalesRepIds.add(c.Sales_Rep__c);
    }
    map<Id, Contact> mapTopSalesRep = new map<Id, Contact>([SELECT Id, Top_35_Sales_Agent__c FROM Contact 
                                                          WHERE Id IN :setSalesRepIds
                                                          AND Top_35_Sales_Agent__c = true]);
    
    String region;
    for(Case c :lstNewCases){
      //code to assign sample support agent
      if(accountRegionMap.containsKey(c.AccountId)){
        region = accountRegionMap.get(c.AccountId).Endo_Region_Name__c;
        if(region != null){
          if(mapSampleUsers.containsKey(region.toLowerCase())){
            c.samples_agent__c = mapSampleUsers.get(region.toLowerCase());   
          }
        } else {
          c.samples_agent__c= null;
        }
      } else {
        c.samples_agent__c= null;
      }
      
        
      //code to assign sales support agent
      system.debug('@@@@' + c.Sales_Rep__c);
      if(mapTopSalesRep.containsKey(c.Sales_Rep__c)){
        c.Sales_Support_Agent__c = top35SalesAgentId;
      } else {
        if(accountRegionMap.containsKey(c.AccountId)){
          region = accountRegionMap.get(c.AccountId).Endo_Region_Name__c;
          if(region != null){
            if(mapSalesUsers.containsKey(region.toLowerCase())){
              c.Sales_Support_Agent__c = mapSalesUsers.get(region.toLowerCase());   
            }
          } else {
            c.Sales_Support_Agent__c = null; 
          }
        } else {
          c.Sales_Support_Agent__c = null;  
        }
      }
    }
    
    System.debug('@@@' + lstNewCases);
  }
  
  //-----------------------------------------------------------------------------------------------
  //  Populate Sales Support Agent on Case
  //-----------------------------------------------------------------------------------------------
  /*public static void populateSalesSupportAgent(List<Case> lstNewCases, Map<Id, Case> mapOldCases) {
    
    //Schema.RecordTypeInfo rtByNameCC = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Endo Internal Contact');
    //Id recordTypeCC = rtByNameCC.getRecordTypeId();
    
    //Prepare map for Region with List of Contacts
    Map<String, List<Case>> mapCases = new Map<String, List<Case>>();
    System.debug('@@@' + mapOldCases);
    
    for(Case c :lstNewCases){
      if(!c.Top_35_Sales_Rep__c && (mapOldCases == null) || (c.Region__c != null && mapOldCases.get(c.Id).Region__c != c.Region__c)){      
        if(mapCases.containsKey(c.Region__c) == false){
          mapCases.put(c.Region__c, new List<Case>());
        }
        mapCases.get(c.Region__c).add(c);
      }
      
    }
    System.debug('@@@' + mapCases);
    if(mapCases.isEmpty()) return;
    String strRegions = '';
    for(String s :mapCases.keySet()){
      strRegions = strRegions + '\'' + s + + '\'' + ',';
    }
    if(strRegions.endsWith(',')){
      strRegions = strRegions.subString(0, strRegions.length() - 1);
    }
    if(strRegions.length() > 0){
      strRegions = '(' + strRegions + ')';
    }
    System.debug('@@@' + strRegions);
    
    // Query Users on base of Regions.
    List<User> lstUsers = new List<User>();
    if(String.isBlank(strRegions) == false){
      String qry = 'SELECT Id, Endo_Support_Regions__c FROM User WHERE Endo_Support_Regions__c INCLUDES' + strRegions;
      System.debug('@@@' + qry);
      lstUsers = Database.query(qry);
    }
    System.debug('@@@' + lstUsers);
    
       
    // Fetch users which contains the Regions as a value in u.Endo_Support_Regions__c
    map<String, Id> mapUsers = new map<String, String>(); 
    for(User u :lstUsers){      
          for(String region :u.Endo_Support_Regions__c.split(';')){
            System.debug('@@@' + region);
            if(String.isBlank(region) == false){
              mapUsers.put(region.toLowerCase(), u.Id);
            }
          }    
      //} 
    }
    System.debug('@@@' + mapUsers);
    
    List<Case> toBeUpdate = new List<Case>();
    for(String rgn :mapCases.keySet()){
      //if(mapUsers.get(rgn) != null){
      if(rgn == null) continue;
        Id supportAgentId = mapUsers.get(rgn.toLowerCase());
        for(Case c :mapCases.get(rgn)){         
            toBeUpdate.add(new Case(Id = c.Id, Sales_Support_Agent__c = supportAgentId));
        } 
     // }
    }   
    
    System.debug('@@@' + toBeUpdate);
    update toBeUpdate;
    
  }*/
  
  //-----------------------------------------------------------------------------------------------
  // Notify Sales Rep if an urgent Tech Support case is opened (Patient on the table)
  //-----------------------------------------------------------------------------------------------
  /*public static void populateSalesRepEmailFieldOnCase(List<Case> lstNewCases, Map<Id, Case> mapOldCases) {
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    Id recordTypeId = rtByName.getRecordTypeId();
    System.debug('@@@' + lstNewCases);
    System.debug('@@@' + recordTypeId);
    Set<Id> setAccountIds = new Set<Id>();
    for(Case c :lstNewCases){
      //c.RecordTypeId == recordTypeId &&
      if((mapOldCases == null || mapOldCases.get(c.Id).AccountId != c.AccountId)){
        setAccountIds.add(c.AccountId);
      }
    }
    System.debug('@@@' + setAccountIds);
      
    List<Stryker_Contact__c> lstStrykerContacts = [SELECT Customer_Account__c, Internal_Contact__c, Internal_Contact__r.Email 
                                  FROM Stryker_Contact__c 
                                  WHERE Customer_Account__c IN :setAccountIds
                                  AND Internal_Contact__r.Title LIKE 'Sales Rep%'];
    
    System.debug('@@@' + lstStrykerContacts);
    
    Map<Id, String> mapStrykerContacts = new Map<Id, String>();
    for(Stryker_Contact__c sc :lstStrykerContacts ){
      if(mapStrykerContacts.containsKey(sc.Customer_Account__c) == false){
        if(sc.Internal_Contact__r.Email != null){
          mapStrykerContacts.put(sc.Customer_Account__c, sc.Internal_Contact__r.Email);
        }
      }
    }
    System.debug('@@@' + mapStrykerContacts);
    
    for(Case c :lstNewCases){
        if((mapOldCases == null || mapOldCases.get(c.Id).AccountId != c.AccountId)){
          if(mapStrykerContacts.get(c.AccountId) != null){
            c.Sales_Rep_Email__c = mapStrykerContacts.get(c.AccountId);
          }
        }
    }
                                              
  }*/
  
  
    
  //-----------------------------------------------------------------------------------------------
  // Notify If Tech Support case is already Exists in previous 7 days.
  //-----------------------------------------------------------------------------------------------
  public static void notifySalesRepIfCaseAlreadyExists(List<Case> lstNewCases, Map<Id, Case> mapNewCases) {
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    Id recordTypeId = rtByName.getRecordTypeId();
    System.debug('@@@' + lstNewCases);
    System.debug('@@@' + recordTypeId);
    Set<Id> setAccountIds = new Set<Id>();
    for(Case c :lstNewCases){
      if(c.RecordTypeId == recordTypeId){
        setAccountIds.add(c.AccountId);
      }
    }
    System.debug('@@@' + setAccountIds);
        
    List<Case> existingCases = [SELECT Id, CaseNumber, AccountId FROM Case WHERE AccountId IN :setAccountIds
                                  AND CreatedDate = LAST_N_DAYS:7
                                  AND RecordTypeId = :recordTypeId
                                  AND isClosed = false];
        
    System.debug('@@@' + existingCases);
    map<Id, List<Case>> mapListOfCase = new map<Id, List<Case>>();
    for(Case c :existingCases){
      if(!mapListOfCase.containsKey(c.AccountId)){
        mapListOfCase.put(c.AccountId, new List<Case>());
      }
      mapListOfCase.get(c.AccountId).add(c);
    }
    System.debug('@@@' + mapListOfCase);  
    
    //Select Accounts for which we need to send emails.
    Set<Id> selectedAccountIds = new Set<Id>();
    for(Id accountId :mapListOfCase.keySet()){
      List<Case> lstCases = mapListOfCase.get(accountId);
      if(lstCases.size() > 1){
        selectedAccountIds.add(accountId);
      }
    }
    System.debug('@@@' + selectedAccountIds);
    
    /*
      List<Contact> lstContacts = [SELECT Id, Name, AccountId, Phone, Email FROM Contact WHERE Id 
                                  IN 
                                  (SELECT Internal_Contact__c FROM Stryker_Contact__c 
                                  WHERE Customer_Account__c IN :selectedAccountIds)
                                  AND Title = 'Sales Rep'];
   */
  
    List<Stryker_Contact__c> lstStrykerContacts = [SELECT Customer_Account__c, Customer_Account__r.Oracle_Account_Number__c, Internal_Contact__c, Internal_Contact__r.FirstName 
                                                  FROM Stryker_Contact__c 
                                                  WHERE Customer_Account__c IN :selectedAccountIds
                                                  AND Internal_Contact__r.Title = 'Sales Rep'];
  
    System.debug('@@@' + lstStrykerContacts);
  
    Map<Id, List<Stryker_Contact__c>> mapStrykerContacts = new Map<Id, List<Stryker_Contact__c>>();
    for(Stryker_Contact__c sc :lstStrykerContacts ){
      if(mapStrykerContacts.containsKey(sc.Customer_Account__c) == false){
        mapStrykerContacts.put(sc.Customer_Account__c, new List<Stryker_Contact__c>());
      }
      mapStrykerContacts.get(sc.Customer_Account__c).add(sc);
    }
    System.debug('@@@' + mapStrykerContacts);
  
    //Send Email
    EmailTemplate template = [SELECT Id FROM EmailTemplate 
                              WHERE DeveloperName = 'Endo_Send_Alert_Case_Already_exists'];

    System.debug('@@@' + template);

    List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
    for(Id accountId :mapStrykerContacts.keySet()){
      for(Stryker_Contact__c c :mapStrykerContacts.get(accountId)){
        //if(mapAccountDetails.get(c.AccountId) != null && mapListOfCase.get(c.AccountId) != null && mapListOfCase.get(c.AccountId).size() > 0){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateID(template.Id);
        mail.setTargetObjectId(c.Internal_Contact__c);
        mail.setWhatId(accountId);
        mail.setSaveAsActivity(false);
  
        //String emailText = generateEmail(mapAccountDetails.get(c.Customer_Account__c), mapListOfCase.get(c.Customer_Account__c), c.Internal_Contact__r.FirstName);
        //System.debug('@@@' + emailText);
        //mail.setSubject('Support Case Alert for Account '+ mapAccountDetails.get(c.Customer_Account__c).Name );
        //mail.setPlainTextBody(emailText);
        //mail.setHtmlBody(emailText);
        allmsg.add(mail);
        //}
      }
    }

    if(allmsg != null && allmsg.size() > 0){
      Messaging.sendEmail(allmsg, false);
    }                                          
  }
    
  //-----------------------------------------------------------------------------------------------
  // Helper method to generate email text
  //-----------------------------------------------------------------------------------------------
  /*private static String generateEmail(Account acc, List<Case> lstCases, String salesRepFirstName) {
    System.debug('@@@' + acc);
    System.debug('@@@' + lstCases);
    String str = '';
    str = str + 'Good Morning/Afternoon '+ salesRepFirstName + ',<br /><br />';
    str = str + 'Support Case Alert for Account: ' + acc.Name + '.<br />' +
           'Oracle Account Number: '+ acc.Oracle_Account_Number__c +
           '<br /><br />The following Tech Support Troubleshooting Cases have been opened within the past 7 calendar days for this Account:<br />';
    
    for(Case c :lstCases){
        str = str +  '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() + '/' + c.Id +'">' + c.CaseNumber + '</a><br />';
    }
           
    return str;
  }*/
  
  //-----------------------------------------------------------------------------------------------
  // T-237859 Update Mood Value on Related Account.
  //-----------------------------------------------------------------------------------------------
  public static void updateAccountMoodValue(List<Case> lstNewCases, Map<Id, Case> mapOldCases) {
    Set<Id> setAccountIds = new Set<Id>();
    for(Case c :lstNewCases){
      if(c.AccountId != null && c.IsClosed == true){// &&  mapOldCases.get(c.Id).Mood_of_Customer__c != c.Mood_of_Customer__c){
        setAccountIds.add(c.AccountId);
      }
    }
    System.debug('@@@' + setAccountIds);
    
    Map<Id, List<Case>> mapAccountIdCases = new Map<Id, List<Case>>();
    for(Id accId :setAccountIds){
      mapAccountIdCases.put(accId, new List<Case>());
    }

    for(Case c :[SELECT Id, CaseNumber, Mood_of_Customer__c, AccountId FROM Case WHERE AccountId IN :setAccountIds AND ClosedDate != NULL ORDER By ClosedDate DESC ]){
      if(mapAccountIdCases.get(c.AccountId).size() < 5 && (String.isBlank(c.Mood_of_Customer__c) == false)){ //Consider only recent 5 Cases
        mapAccountIdCases.get(c.AccountId).add(c);
      }
    }
    System.debug('@@@' + mapAccountIdCases);
    
    //Added by Jyoti for Case 00168349     
    Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, RecordtypeId,Recordtype.Name , Endo_Active__c From account where Id IN: mapAccountIdCases.keySet() AND Endo_Active__c = false AND (Recordtype.Name =: ENDO_CUSTOMER  OR Recordtype.Name=: ENDO_SALES_REP )]);
    //End - Case 00168349
    
    List<Account> lstAccountsToUpdate = new List<Account>();
    for(Id accountId : mapAccountIdCases.keySet()){
      //Added if condition by Jyoti for Case 00168349 
      if(accountMap == null || !accountMap.containsKey(accountId)){ 
          List<Case> lst = mapAccountIdCases.get(accountId);
          Account acc = new Account(Id = accountId);
          if(lst.size() > 0){
            Double countA = 0;
            Double sum = 0;
            for(Case c :lst){
              countA = countA + 1;
              sum = sum + getMoodNumber(c.Mood_of_Customer__c);
              //System.debug('@@@' + getMoodNumber(c.Mood_of_Customer__c));
            }
            //System.debug('@@@' + sum);
            //System.debug('@@@' + countA);
            Integer averageValue = Integer.valueOf(System.Math.floor((sum / countA) + 0.5));
            //System.debug('@@@' + averageValue);
            acc.Mood_of_Customer__c = getMood(averageValue);
          } else {
            acc.Mood_of_Customer__c = null;
          }    
          lstAccountsToUpdate.add(acc);
        } //End - Case 00168349
      
    }
    System.debug('@@@' + lstAccountsToUpdate);
    update lstAccountsToUpdate;
  }
  
  private static Integer getMoodNumber(String mood){
    if(mood == 'Very Dissatisfied') return 1;
    else if(mood == 'Dissatisfied') return 2;
    else if(mood == 'Content') return 3;
    else if(mood == 'Satisfied') return 4;
    else if(mood == 'Very Satisfied') return 5;
    return 3;
  }
  
  private static String getMood(Integer moodNumber){
    if(moodNumber == 1 ) return 'Very Dissatisfied';
    else if(moodNumber == 2 ) return  'Dissatisfied';
    else if(moodNumber == 3 ) return  'Content';
    else if(moodNumber == 4 ) return 'Satisfied';
    else if(moodNumber == 5 ) return  'Very Satisfied';
    return 'Content';
  }
  
  //-----------------------------------------------------------------------------------------------
  // T-239688 Custom code for email parsing of ProCare Billing Email-to-Case cases
  //-----------------------------------------------------------------------------------------------
  public static void populateAssignedStateForEmailToCase(List<Case> lstNewCases){
    //Get Record Type Id.
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair Order Billing');
    if(rtByName == null) return;
    Id recordTypeId = rtByName.getRecordTypeId();
    string assignedStateLabel = 'Assigned State';
    string accountNumberLabel = 'Account Number';   
    
    for(Case newCase : lstNewCases){
      if(newCase.recordTypeId == recordTypeId && newCase.Origin == 'Email-to-Case' && String.isNotEmpty(newCase.Description)){
        newCase.Assigned_State__c = getValuesFromCaseDesc(newCase.Description, assignedStateLabel, accountNumberLabel);
      }
    }
  }
  
  @testvisible
  private static String getValuesFromCaseDesc(String description, string startLabel, string endLabel){
    String value = null;
    Integer startIndex;
    Integer endIndex;

    if(description.containsIgnoreCase(startLabel)){
      startIndex = description.indexOf(startLabel);
      if(startIndex != -1) {
        endIndex = description.indexOf(endLabel, startIndex);
        if(endIndex != -1) {
          value = description.substring(startIndex + startLabel.length(), endIndex);
          if(value != null){
            value = value.replaceAll('[^a-zA-Z0-9]', '');
            value = value.trim();
          }
        }
      }
    }
    return value;
  }
  
  /*@testvisible
    private static String getValuesFromCaseDesc(String description, string assignedStateLabel,
        string assignedStateTdStartTag, string assignedStateStartTag, string assignedStateEndTag){
        string assignedStateTdStartTag = '<td';
        string assignedStateStartTag = '>';
        string assignedStateEndTag = '</td>';
        string assignedState = null;
        integer assignedStateIndex = -1;
        integer assignedStateTdStartTagIndex = -1;
        integer assignedStateStartTagIndex = -1;
        integer assignedStateEndTagIndex = -1;
        if(description.containsIgnoreCase(assignedStateLabel)){
            assignedStateIndex = description.indexOf(assignedStateLabel);
            if(assignedStateIndex != -1) {
                assignedStateTdStartTagIndex = description.indexOf(assignedStateTdStartTag, assignedStateIndex);
                if(assignedStateTdStartTagIndex != -1) {
                    assignedStateStartTagIndex = description.indexOf(assignedStateStartTag, assignedStateTdStartTagIndex);
                    if(assignedStateStartTagIndex != -1) {
                        assignedStateEndTagIndex = description.indexOf(assignedStateEndTag, assignedStateStartTagIndex);
                        if(assignedStateEndTagIndex != -1) {
                            assignedState = description.substring(assignedStateStartTagIndex + assignedStateStartTag.length(), assignedStateEndTagIndex);
                        }
                    }
                }
            }
        }
        return assignedState;
  }*/
  
  //-----------------------------------------------------------------------------------------------
  // TrackHistory
  //-----------------------------------------------------------------------------------------------
  public static void TrackHistory(List<Case> lstNewCases, Map<Id, Case> mapOldCases){
    List<Case_History__c> listCaseHistory = new List<Case_History__c>();
    Case_History__c caseHistory;
    Case_History__c oldCaseHistory;
    Set<Id> setCaseChangeId = new Set<Id>();
    
    /* Creating Case History */
    for(case c : lstNewCases){      
      if(mapOldCases == null || c.Status != mapOldCases.get(c.Id).Status){
        caseHistory = new Case_History__c(Case__c = c.Id, Status__c = c.Status, Start_time__c = DateTime.now());
        listCaseHistory.add(caseHistory);
        if(mapOldCases != null){
          setCaseChangeId.add(c.Id);
        }
      }
    }
    
    if(!setCaseChangeId.isEmpty()){
      for(Case c : [SELECT Id, Status, IsClosed, (Select Id, End_time__c, Start_Time__c from Case_Histories__r order by Createddate desc limit 1) FROM Case WHERE Id IN :setCaseChangeId]){
          if(c.Case_Histories__r != null && !c.Case_Histories__r.isEmpty()){
            oldCaseHistory = c.Case_Histories__r.get(0);
            caseHistory = new Case_History__c(Id = oldCaseHistory.Id, End_time__c = DateTime.now());
            caseHistory.Status_in_Mins__c = getDifferenceInMinutes(caseHistory.End_time__c, oldCaseHistory.Start_Time__c);
            listCaseHistory.add(caseHistory);               
          }
      }
    }
  
    /* Inserting Case History */
    if(!listCaseHistory.isEmpty()){
    //S-445932-----START-----Hitesh[Nov 07, 2016]
      try{
          upsert listCaseHistory;
      }catch(DmlException dmlException){          
          if(dmlException.getMessage().containsIgnoreCase(Constants.CASE_REQUIRED_DATA_TO_CLOSE) && lstNewCases != null && lstNewCases.size() > 0){
            lstNewCases[0].addError(Constants.EORROR_TO_BE_SHOWN_CLOSE_CASE);
          }
      }
    //S-445932-----END-----Hitesh[Nov 07, 2016]
    }
  }
  
  public static long getDifferenceInMinutes(datetime firstDT, datetime secondDT){  //This calculates the the difference between the dates in minutes
    long dt1 = firstDT.getTime() / 1000 / 60;  //getTime gives us milliseconds, so divide by 1000 for minutes
    long dt2 = secondDT.getTime() / 1000 / 60;
    long d = dt1 - dt2;
    return d;
  }
}