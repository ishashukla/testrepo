/*
Modified By :   Jessica Schilling
Date        :   6/02/2016
Case        :   Case 169518
*/

global class NV_ScheduledFedExStatus implements Database.Batchable<SObject>, Schedulable {
    Integer intervalMinutes;
    String query;
    //START JSchilling Case 169518 6/02/2016
    //Added code below to allow the query to only look at records so many days in the past, which is driven by the Label
    Integer days = Integer.valueOf(Label.Last_n_days_for_FedEx_Job);
    Date dateForQuery = System.today() - days;
    //END JSchilling Case 169518 6/02/2016
    global NV_ScheduledFedExStatus(){
        //JSchilling Case 169518 6/02/2016
        //Added Shipped_Date__c condition to below query
        //Logic for S-371704, Added Tracked_Number__c field in the Query
        query = 'SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c FROM OrderItem WHERE Order_Delivered__c = false AND Tracking_Number__c != NULL AND Shipped_Date__c >= :dateForQuery';
    }
    
    global NV_ScheduledFedExStatus(Integer intervalMinutes) {
        this.intervalMinutes = intervalMinutes;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        List<OrderItem> orderItemList = (List<OrderItem>) scope;
        if(orderItemList.size()>0){
            new NV_FedExRequest().TrackFedExRequest(orderItemList);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

    global void execute(SchedulableContext SC) {
        //NV_ScheduledFedExStatus batch = new NV_ScheduledFedExStatus();
        //ID batchprocessid = Database.executeBatch(batch, 10);
        NV_FedExRequest.processOrderItemsForFedEx();
    }
        
}