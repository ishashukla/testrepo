@isTest
private class QuickLinksController_Test {
	public static List<User> testUsers;
	static User adminUser;
	@isTest static void test_method_one() {
	  Test.startTest();
	    insertuser();
	   Test.stopTest();
    System.runAs(adminUser) {
  		List<Quick_Link_Section__c> lstQuickLinkSection = new List<Quick_Link_Section__c> ();
  		lstQuickLinkSection.add(new Quick_Link_Section__c(Name = 'IVS BCD Travel', Section__c = 'IVS',
  															System_Name__c = 'Test New Website', URL__c = 'www.testnewweb.com'));
  		lstQuickLinkSection.add(new Quick_Link_Section__c(Name = 'Navigation BCD Travel', Section__c = 'Navigation',
  															System_Name__c = 'Test New Websites', URL__c = 'www.testnewwebs.com'));	
  		insert lstQuickLinkSection;
    }
		System.runAs(testUsers.get(0)) {
			QuickLinksController qLinkC = new QuickLinksController();
		}
	}
	@future
	public static void insertUser(){
	  adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
    insert adminUser;
	  testUsers = Endo_TestUtils.createUser(1, 'Instruments Sales User', false);
		testUsers.get(0).Division = 'IVS; Navigation';
		insert testUsers;
	}
}