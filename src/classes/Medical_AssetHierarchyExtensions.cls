//
// (c) 2012 Appirio, Inc.
//
//  Controller class for VF page Medical_AssetHierarchy
//
// 25 Feb 2016; Sunil Gupta


public class Medical_AssetHierarchyExtensions {

  // Public visualforce variables.
  public transient Asset currentAsset {get;set;}
  public transient List<AssetWrapper> wrapperList{get;set;}

  // Private Variables.
  private transient List<AssetWrapper> lstParent = new List<AssetWrapper>();
  private transient Map<String, List<AssetWrapper>> mapChild_Level_1 = new Map<String, List<AssetWrapper>>();
  private transient Map<String, List<AssetWrapper>> mapChild_Level_2 = new Map<String, List<AssetWrapper>>();

  private transient Date assetShippedDate;


  //----------------------------------------------------------------------------------------------
  //  Constructor
  //----------------------------------------------------------------------------------------------
  public Medical_AssetHierarchyExtensions(ApexPages.standardController stdController){
    wrapperList =  new List<AssetWrapper>();

    if (!Test.isRunningTest()) {
    	stdController.addFields(new List<String>{'Shipped_Date__c'});
    }

    currentAsset = (Asset)stdController.getRecord();
    assetShippedDate = currentAsset.Shipped_Date__c;

    // Fetch child data from AssetLine and Product Relationship Objects
    fetchData();

    // Set order or records based on Hierarchy.
    setRecordsOrder();
  }



  //----------------------------------------------------------------------------------------------
  //  Helper method to Fetch Asset Lines and corresponding child products
  //----------------------------------------------------------------------------------------------
  private void fetchData(){
    // Parent Object
    List<Asset_Configuration_Line__c> lstLines = [SELECT Id, Name, Line_Number__c, Product__c, Product__r.Name, Product__r.ProductCode
                                                  FROM Asset_Configuration_Line__c
                                                  WHERE Asset__c = :currentAsset.Id ORDER BY Line_Number__c ASC];

    Set<Id> parentIds = new Set<Id>();
    Set<Id> uniqueProudcts = new Set<Id>();
    for(Asset_Configuration_Line__c obj :lstLines){
      AssetWrapper objWrapper = new AssetWrapper();
      if(obj.Product__c != null && uniqueProudcts.contains(obj.Product__c) == false){
      	uniqueProudcts.add(obj.Product__c);
        parentIds.add(obj.Product__c);
        objWrapper.productName = obj.Product__r.Name;
        System.debug('@@@###' + objWrapper.productName);
        objWrapper.productCode = obj.Product__r.ProductCode;
        objWrapper.productId = obj.Product__c;
        objWrapper.recordName = obj.Name;
        objWrapper.lineNumber = String.valueOf(obj.Line_Number__c);
        objWrapper.recordId = obj.Id;
        objWrapper.uniqueId = obj.Id + randomNumber();
        objWrapper.nodeType = 'end'; //'parent_end';
        //objWrapper.thisObject = obj;
        objWrapper.levelFlag =  new Boolean[]{false};
        objWrapper.parentId = '';
        objWrapper.intLevel = 1;
        lstParent.add(objWrapper);
      }
    }

    if(assetShippedDate == null){
    	// do not fetch child data.
    	return;
    }


    // Child Object. Level 1
    List<Product_Relationship__c> childProducts_1 = [SELECT Id, Name, Line_Number__c, Product__c, Product__r.Name, Part__c, Part__r.Name,Part__r.ProductCode
                                                FROM Product_Relationship__c
                                                WHERE Product__c IN :parentIds
                                                AND Part__c != null
                                                AND Effective_Start_Date__c <= :assetShippedDate
                                                AND Effective_End_Date__c >=  :assetShippedDate
                                                ORDER BY Line_Number__c ASC];
    parentIds = new Set<Id>();
    for(Product_Relationship__c objChild_1 :childProducts_1){
      parentIds.add(objChild_1.Part__c);
      AssetWrapper objWrapper = new AssetWrapper();
      //objWrapper.thisObject = objChild_1;
      //objWrapper.childObject = objChild_1;
      objWrapper.productName = objChild_1.Part__r.Name;
      System.debug('@@@###' + objWrapper.productName);
      objWrapper.productCode = objChild_1.Part__r.ProductCode;
      objWrapper.productId = objChild_1.Part__c;
      objWrapper.recordName = objChild_1.Name;
      objWrapper.lineNumber = String.valueOf(objChild_1.Line_Number__c);
      objWrapper.recordId = objChild_1.Id;
      objWrapper.uniqueId = objChild_1.Id + randomNumber();
      objWrapper.levelFlag =  new Boolean[]{false,false};
      objWrapper.nodeType = 'child';
      objWrapper.intLevel = 2;
      if(mapChild_Level_1.containsKey(objChild_1.Product__c) == false){
        mapChild_Level_1.put(objChild_1.Product__c, new List<AssetWrapper>());
      }
      mapChild_Level_1.get(objChild_1.Product__c).add(objWrapper);
    }




    // Child Object. Level 2
    List<Product_Relationship__c> childProducts_2 = [SELECT Id, Name, Line_Number__c, Product__c, Product__r.Name, Part__c, Part__r.Name,Part__r.ProductCode
                                                FROM Product_Relationship__c
                                                WHERE Product__c IN :parentIds
                                                AND Part__c != null
                                                AND Effective_Start_Date__c <= :assetShippedDate
                                                AND Effective_End_Date__c >=  :assetShippedDate
                                                ORDER BY Line_Number__c ASC];

    parentIds = new Set<Id>();
    for(Product_Relationship__c objChild_2 :childProducts_2){
      parentIds.add(objChild_2.Part__c);
      AssetWrapper objWrapper = new AssetWrapper();
      //objWrapper.thisObject = objChild_2;
      //objWrapper.childObject = objChild_2;
      objWrapper.productName = objChild_2.Part__r.Name;
      System.debug('@@@###' + objWrapper.productName);
      objWrapper.productCode = objChild_2.Part__r.ProductCode;
      objWrapper.productId = objChild_2.Part__c;
      objWrapper.recordName = objChild_2.Name;
      objWrapper.lineNumber = String.valueOf(objChild_2.Line_Number__c);
      objWrapper.recordId = objChild_2.Id;
      objWrapper.uniqueId = objChild_2.Id + randomNumber();
      objWrapper.levelFlag =  new Boolean[]{false,false, false};
      objWrapper.nodeType = 'child';
      objWrapper.intLevel = 3;
      if(mapChild_Level_2.containsKey(objChild_2.Product__c) == false){
        mapChild_Level_2.put(objChild_2.Product__c, new List<AssetWrapper>());
      }
      mapChild_Level_2.get(objChild_2.Product__c).add(objWrapper);
    }

  }


  //----------------------------------------------------------------------------------------------
  //  Set order or records based on Hierarchy.
  //----------------------------------------------------------------------------------------------
  private void setRecordsOrder(){
    // Add Asset Line records
    Map<Integer, AssetWrapper> indexList = new Map<Integer, AssetWrapper>();
    for(AssetWrapper obj :lstParent){
      wrapperList.add(obj);
    }

    // Place child Level 1 at right position
    for(Integer index = 0; index < wrapperList.size(); index++){
      AssetWrapper objParent = wrapperList.get(index);
      for(String parentId :mapChild_Level_1.keySet()){
        if(parentId == objParent.productId){
          for(AssetWrapper objChild :mapChild_Level_1.get(parentId)){
            index = index + 1;
            objChild.parentId = objParent.uniqueId; // objParent.recordId;
            objParent.nodeType = 'parent';
            if(index == wrapperList.size()){
              wrapperList.add(objChild);
            }
            else{
              wrapperList.add(index, objChild);
            }
          }
        }
      }
    }
    System.debug('@@@wrapperList' + wrapperList);

    // Place child Level 2 at right position
    for(Integer index = 0; index < wrapperList.size(); index++){
      AssetWrapper objParent = wrapperList.get(index);
      if(objParent.intLevel < 2){
      	// we are including only just parent, because some parent product also exist as a child.
      	// this condition ensures that child is adding under just parent.
      	continue;
      }
      for(String parentId :mapChild_Level_2.keySet()){
        if(parentId == objParent.productId){
          for(AssetWrapper objChild :mapChild_Level_2.get(parentId)){
            index = index + 1;
            objChild.parentId = objParent.uniqueId; // objParent.recordId;
            objParent.nodeType = 'parent';
            if(index == wrapperList.size()){
              wrapperList.add(objChild);
            }
            else{
              wrapperList.add(index, objChild);
            }
          }
        }
      }
    }
    System.debug('@@@wrapperList' + wrapperList);

    // set blank all collections, to save the local viewstate
    lstParent = new List<AssetWrapper>();
    mapChild_Level_2 = new Map<String, List<AssetWrapper>>();
    mapChild_Level_1 = new Map<String, List<AssetWrapper>>();
  }

  public String randomNumber(){
  	return '_' + String.valueOf(Math.round(Math.random()*10000));
  }


  //----------------------------------------------------------------------------------------------
  // Wrapper class
  //----------------------------------------------------------------------------------------------
  public class AssetWrapper{
    public transient Boolean[] levelFlag{get;set;}
    public transient String nodeType{get;set;} //parent; parent_end; child; child_end
    //public transient Sobject thisObject {get; set;} // comment out to save viewstate
    //public transient Product_Relationship__c childObject {get;set;} // comment out to save viewstate
    public transient String lineNumber {get;set;}
    public transient String parentId {get;set;}
    public transient String productId{get;set;}
    public transient String productName{get;set;}
    public transient String productCode{get;set;}
    public transient String recordName{get;set;}
    public transient String recordId{get;set;}
    public transient String uniqueId{get;set;}
    public transient Integer intLevel{get;set;}

    public AssetWrapper(){
      levelFlag = new Boolean[]{};
    }
  }
}