/**================================================================      
 * Appirio, Inc
 * Name: NoteTriggerHandlerTest
 * Description: Test class for NotesTriggerHandler
 * Created Date: Oct 5, 2016
 * Created By: Shreerath Nair
 * 
 * Date Modified      Modified By      Description of the update
 * 19th Oct 2016      Shreerath Nair   test methods to cover shareNotesRecordWithASR of NoteTriggerhandler
 ==================================================================*/
@IsTest
private class NoteTriggerHandlerTest {

    Static List<Account> accountList;
    Static List<Contact> contactList;
    Static List<Opportunity> oppList;
    Static List<Notes__c> noteList;
    Static List<User> user;
    static List<User> ASRUserList;
    Static ASR_Relationship__c asrUserRel; 
    Static List<RecordType> recordtypeListforPreNotes = new List<RecordType>([SELECT SobjectType,IsActive,Id,DeveloperName FROM RecordType 
                                                                              WHERE SobjectType = 'Notes__c' 
                                                                              AND DeveloperName = 'Preference_Cards']);
    
    
    
    //20 Oct, 2016  Shreerath Nair   T-542919
    //Positive test Method to check share records are created for ASR Users of Sales Rep
    @isTest static void testshareNotesRecordWithASRPostive() {
    
        
        createData();
        
        System.runAs(user[0]){
            
            
            noteList = createNote(1,'title',contactList[0].Id,false);
            noteList[0].recordtypeId = recordtypeListforPreNotes[0].Id;
            insert noteList;
        }
        List<User> Adminuser = TestUtils.createUser(1, 'System Administrator', false );
        Adminuser[0].Division = 'NSE';
        insert Adminuser;
        System.runAs(Adminuser[0]){
              
            List<Notes__Share> noteShare = [SELECT id FROM Notes__Share where UserOrGroupId =: ASRUserList[0].Id AND ParentId =:noteList[0].Id];
            System.assertEquals(noteShare.size(),1);
            
       }     
    }
    
    //20 Oct, 2016  Shreerath Nair   T-542919
    //negative test Method to check no share records are created if ASR Users not related to Sales Rep via View as Relationship object.
    @isTest static void testshareNotesRecordWithASRNegative() {
    
        
        createData();
        delete asrUserRel;
        System.runAs(user[0]){
        
            noteList = createNote(1,'title',contactList[0].Id,false);
            noteList[0].recordtypeId = recordtypeListforPreNotes[0].Id;
            insert noteList;
        }
        List<User> Adminuser = TestUtils.createUser(1, 'System Administrator', false );
        Adminuser[0].Division = 'NSE';
        insert Adminuser;
        System.runAs(Adminuser[0]){
              
            List<Notes__Share> noteShare = [SELECT id FROM Notes__Share where UserOrGroupId =: ASRUserList[0].Id AND ParentId =:noteList[0].Id];
            System.assertEquals(noteShare.size(),0);
          
            
       }     
    }
    
    //20 Oct, 2016  Shreerath Nair   T-542919
    //bulk test Method to check share records are created for ASR Users of Sales Rep
    @isTest static void testshareNotesRecordWithASRBulk() {
    
        
        createData();
        map<id,Notes__c> notesMap = new Map<id,Notes__c>();
        System.runAs(user[0]){
          
            noteList = createNote(200,'title',contactList[0].Id,true);
        }
        
        for(Notes__c notes:noteList){
            notesMap.put(notes.Id,notes);
        }
        
        List<User> Adminuser = TestUtils.createUser(1, 'System Administrator', false );
        Adminuser[0].Division = 'NSE';
        insert Adminuser;
        System.runAs(Adminuser[0]){
             
            List<Notes__Share> noteShare = [SELECT id FROM Notes__Share where UserOrGroupId =: ASRUserList[0].Id AND ParentId IN :notesMap.KeySet()];
            System.assertEquals(noteShare.size(),200);
 
            
       }     
    }
    
    
    public static void createData() {
        Schema.RecordTypeInfo rtByNameContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        Id recordTypeContact = rtByNameContact.getRecordTypeId();
        Test.StartTest();
        insertUser();
        Test.StopTest();
        //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=ASRUserList[0].Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
        //insert terr;
        Mancode__c mancode = TestUtils.createMancode(1,ASRUserList[0].Id, null, false).get(0);
        mancode.Name = '000000';
        mancode.Type__c = 'ASR';
        insert mancode;
        System.runAs(user[0]) {
            
            accountList = TestUtils.createAccount(2, True);
            contactList = TestUtils.createContact(1,accountList[0].Id, false);
            contactList[0].RecordTypeId = recordTypeContact;
            contactList[0].Allow_Create__c = true;
            insert contactList;
            
            
        }
        asrUserRel = new ASR_Relationship__c();
        asrUserRel.Mancode__c = mancode.Id;
        asrUserRel.ASR_User__c = user[0].Id;
        insert asrUserRel;
    }
    // method to create Notes custom object records list
    public static List<Notes__c> createNote(Integer noteCount,String title,Id contactId,Boolean isInsert) {
        noteList = new List<Notes__c>();
        
        for(Integer i = 0; i < noteCount; i++) {
            Notes__c noteRecord = new Notes__c(Name = title, Contact__c = contactId);
            noteList.add(noteRecord);
        }
        if(isInsert) {
            insert noteList;
        } 
        return noteList;
    }
    @future
    public static void insertUser() {
        user = TestUtils.createUser(1, 'Instruments Sales User', false );
        user[0].Division = 'NSE';
        insert user;
        ASRUserList = TestUtils.createUser(1, 'Instruments ASR User', false);
        ASRUserList[0].Division = 'NSE';
        insert ASRUserList;
    }

}