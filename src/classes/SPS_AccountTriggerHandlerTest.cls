/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SPS_AccountTriggerHandlerTest {

    static testMethod void SPS_AccountTestMethod1() {
    Test.startTest();

    String profileName = [SELECT Id, Name FROM Profile WHERE Id =:userinfo.getProfileId()].Name;
    System.debug('<--- profileName --->' + profileName);
    	Id SPSrecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('SPS Customer').getRecordTypeId();
    	Account ParentAccount = new Account(Name = 'Parent Test Account',
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States',recordtypeid = SPSrecordTypeId);
        ParentAccount.Strategic_Account__c = true;
        insert ParentAccount;
        
		Account ChildAccount = new Account(Name = 'Child Test Account 01',
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States',recordtypeid = SPSrecordTypeId);
        insert ChildAccount;
        
        ChildAccount.ParentId = ParentAccount.Id;
        update ChildAccount;
        
        ParentAccount.Negotiated_Contract__c = true;
        update ParentAccount;
        
        Test.stopTest();
        
        Account TestAccount = [SELECT Id, ParentId, Strategic_Account__c, Negotiated_Contract__c,  Name FROM Account WHERE Id = :ChildAccount.Id];
        						
        System.assertEquals(TestAccount.Strategic_Account__c, true);
        System.assertEquals(TestAccount.Negotiated_Contract__c, true);
    
    }
}