public class OpportunityQuickActionController{
    public String opptyId;
    public OpportunityQuickActionController(ApexPages.StandardController std){
      opptyId= std.getRecord().id; 
    
    }
    public PageReference setRecordTypeToKeyOppty(){
	    Opportunity opp = new Opportunity(Id = opptyId);
	    Id opptyRecordTypeId = Constants.getRecordTypeId(Constants.COMM_OPPTY_RTYPE4,Constants.SOBJECT_OPPORTUNITY);
	    opp.RecordTypeid = opptyRecordTypeId;
	    update opp;
	    PageReference page = new PageReference('/' + opptyId);
	  	page.setRedirect(true);
	  	return page;
    }
}