// (c) 2016 Appirio, Inc.
//
// Description Test Class for COMM_ActionNotesOnAccountController
//
// 02/23/2016    Deepti Maheshwari Reference T-477245
//
@isTest
private class COMM_ActionNotesOnAccountControllerTest
{
  @isTest
  static void testMethod1()
  {
    //Creating multiple users
    List<User> userList = TestUtils.createUser(1, 'System Administrator', true);
    List<Action_Note__c> noteList = new List<Action_Note__c>() ;
    Action_Note__c note = new Action_Note__c();

    //Creating an account and multile action notes as one user
    system.runAs(userList.get(0)){
      List<Account> parentAccount = TestUtils.createAccount(1, true);
      List<Account> accountList = TestUtils.createAccount(1, false);
      accountList.get(0).ParentID = parentAccount.get(0).id;
      insert accountList;
      for(Integer i = 0; i <= 5; i++){
        note = new Action_Note__c();
        note.AccountName__c = accountList[0].Id;
        noteList.add(note);
      }
      
      insert noteList;
      //Update due to change in task T-477245 
      //Partner partner = new Partner();
      //partner.AccountToId = accountList[0].id;
      //partner.AccountFromId = accountList[1].id;
      //partner.role = 'ABC';
      //insert partner;
      PageReference pref = Page.COMM_ActionNotesOnAccountPage;
      pref.getParameters().put('id', accountList.get(0).id);
	    Test.setCurrentPage(pref);
	    ApexPages.StandardController sc = new ApexPages.StandardController(accountList.get(0));
	    COMM_ActionNotesOnAccountController assAccActionNoteCtrl = new COMM_ActionNotesOnAccountController(sc);
	    assAccActionNoteCtrl.getNotesWrapper();
	    system.assert(assAccActionNoteCtrl.notesWrapper.size() > 0);            
    }
  }
}