@isTest
private class NV_NotificationSettingsTriggerTest {
	@testSetup static void setup() {
		Profile sysAdminProfile = NV_Utility.getProfileByName('System Administrator');
		List<User> userData = new List<User>();
		for(Integer i=1; i<=4 ;i++){
			userData.add(NV_TestUtility.createUser('testuser_s'+i+'@mail.com', 'testuser_s'+i+'@mail.com', 
														sysAdminProfile.Id, null, ''+i, false));
		}
		insert userData;
	}

	@isTest static void testPositiveInsert() {
		List<User> userData = [SELECT Id, Username FROM User WHERE Username like 'testuser_s%'];

		System.assertEquals(0, [SELECT count() FROM Notification_Settings__c]);

		List<Notification_Settings__c> settings = new List<Notification_Settings__c>();
		settings.add(NV_TestUtility.createNotificationSettings(userData[0].Id, false));
		settings.add(NV_TestUtility.createNotificationSettings(userData[1].Id, false));
		insert settings;

		System.assertEquals(2, [SELECT count() FROM Notification_Settings__c]);
	}
	
	@isTest static void testPositiveUpdate() {
		List<User> userData = [SELECT Id, Username FROM User WHERE Username like 'testuser_s%'];

		System.assertEquals(0, [SELECT count() FROM Notification_Settings__c]);

		List<Notification_Settings__c> settings = new List<Notification_Settings__c>();
		settings.add(NV_TestUtility.createNotificationSettings(userData[0].Id, false));
		settings.add(NV_TestUtility.createNotificationSettings(userData[1].Id, false));
		insert settings;

		settings[0].User__c = userData[2].Id;
		settings[1].User__c = userData[3].Id;
		update settings;

		System.assertEquals(userData[2].Id, [SELECT User__c FROM Notification_Settings__c WHERE Id =: settings[0].Id].User__c);
	}

	@isTest static void testNegativeInsert() {
		List<User> userData = [SELECT Id, Username FROM User WHERE Username like 'testuser_s%'];
		Notification_Settings__c settingWithError;
		Database.SaveResult dsr;

		settingWithError = NV_TestUtility.createNotificationSettings(null, false);
		dsr = Database.insert(settingWithError,false);
		System.assertEquals(false, dsr.isSuccess());
		System.assertEquals(NV_NotificationSettingsTriggerHandler.UserRequiredErrorMsg,dsr.getErrors()[0].getMessage());

		NV_TestUtility.createNotificationSettings(userData[0].Id, true);
		System.assertEquals(1, [SELECT count() FROM Notification_Settings__c]);

		settingWithError = NV_TestUtility.createNotificationSettings(userData[0].Id, false);
		dsr = Database.insert(settingWithError,false);
		System.assertEquals(false, dsr.isSuccess());
		System.assertEquals(NV_NotificationSettingsTriggerHandler.UserSettingUniqueErrorMsg,dsr.getErrors()[0].getMessage());

		System.assertEquals(1, [SELECT count() FROM Notification_Settings__c]);
	}
	
	@isTest static void testNegativeUpdate() {
		List<User> userData = [SELECT Id, Username FROM User WHERE Username like 'testuser_s%'];

		System.assertEquals(0, [SELECT count() FROM Notification_Settings__c]);

		List<Notification_Settings__c> settings = new List<Notification_Settings__c>();
		settings.add(NV_TestUtility.createNotificationSettings(userData[0].Id, false));
		settings.add(NV_TestUtility.createNotificationSettings(userData[1].Id, false));
		insert settings;

		settings[0].User__c = userData[1].Id;

		Database.SaveResult dsr = Database.update(settings[0],false);
		System.assertEquals(false, dsr.isSuccess());
		System.assertEquals(NV_NotificationSettingsTriggerHandler.UserSettingUniqueErrorMsg,dsr.getErrors()[0].getMessage());

		System.assertEquals(userData[0].Id, [SELECT User__c FROM Notification_Settings__c WHERE Id =: settings[0].Id].User__c);
	}
	
}