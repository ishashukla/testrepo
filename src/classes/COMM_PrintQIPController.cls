/**================================================================      
* Appirio, Inc
* Name: COMM_PrintQIPController
* Description: Controller for Print QIP
* Created Date: 12-Sep-2016
* Created By: Shubham Dhupar(Appirio)
==================================================================*/
public class COMM_PrintQIPController {
  public SVMXC__Installed_Product__c Install {get;set;}
  public Checklist_Header__c checkheader {get;set;}
  public List<CheckList_Detail__c>  checkdetail{get;set;}
  public Map<String,List<CheckList_Detail__c>>  groupDetailMap{get;set;}
  public List<Checklist_Group__c> checkName{get;set;}
  public String installDate{get;set;}
  public String completionDate{get;set;}
//  public SVMXC__Installed_Product__c inst {get;set;}

  public List<Schema.FieldSetMember> getFields() {
      return SObjectType.SVMXC__Installed_Product__c.FieldSets.Installed.getFields();
  }
  private SVMXC__Installed_Product__c getAsset(id assetid) {
      String query = 'SELECT ';
      for(Schema.FieldSetMember f : this.getFields()) {
          query += f.getFieldPath() + ', ';
      }
      query += 'Id, Name,SVMXC__Company__r.Name,SVMXC__Contact__r.Name,Project_Plan__r.Project_Order_Numbers__c,Project_Phase__r.Phase_Order_Numbers__c,Installed_At_Address__r.Name,Project_Plan__r.Name,Project_Plan__r.Long_Name__c,Project_Phase__r.Project_Phase_Number__c,Case_Owner__r.Name FROM SVMXC__Installed_Product__c where Id = \''+ assetid +'\'';
      return Database.query(query);
  }
  public COMM_PrintQIPController(ApexPages.StandardController cntrl) {
    groupDetailMap = new Map<String,List<CheckList_Detail__c>>();
    String headId = ApexPages.currentPage().getParameters().get('headid');
    if(headId != null){
      checkheader =  [select id,Asset__c,Status__c
                                          from Checklist_Header__c
                                          where id =: headId];
      
      String installId = checkheader.Asset__c;
      this.install = getAsset(installId);
    }
    Integer Day;
    Integer Month;
    Integer Year;
    if(install.SVMXC__Date_Installed__c != null){
      Day = install.SVMXC__Date_Installed__c.day();
      Month = install.SVMXC__Date_Installed__c.month();
      Year = install.SVMXC__Date_Installed__c.year();
      installDate = Month + '/' +Day +'/'+Year;
    }
    if(install.QIP_Completion_Date__c != null){
      Day = install.QIP_Completion_Date__c.day();
      Month = install.QIP_Completion_Date__c.month();
      Year = install.QIP_Completion_Date__c.year();
      completionDate = Month + '/' +Day +'/'+Year;
    }
    Set<Id> groupids = new Set<Id>();
    List<CheckList_Detail__c> checklist  =new  List<CheckList_Detail__c>();
    Set<Id> headerids = new Set<Id>(); 
    if(headId != null){
      checkdetail = [SELECT id,Checklist_Header__c,Question__c,Result__c,Checklist_Group__c,Order__C
                     FROM CheckList_Detail__c
                     WHERE Checklist_Header__c =:headId];
      for(CheckList_Detail__c check : checkdetail) {
        groupids.add(check.Checklist_Group__c);
        headerids.add(check.checklist_header__c);
      }
      checkName = new List<Checklist_Group__c>();
      for(Checklist_Group__c  checkgroup : [SELECT id,name,sequence__c FROM checklist_group__c WHERE id in :groupids order by sequence__c ASC]) {
        checkName.add(checkgroup);
        checklist = new List<CheckList_Detail__c>();
        system.debug('Checkgroup Name ' + checkgroup.Name);
        for(checklist_detail__c detail: [SELECT id,Checklist_Header__c,Question__c,Result__c,type__c,Comment__c,
                                        Checklist_Group__c,order__C 
                                        FROM checklist_detail__c 
                                        WHERE Checklist_Group__c =: checkgroup.id 
                                        AND Checklist_Header__c in: headerids Order by Order__c ASC]) {
          checklist.add(detail);
        }
        groupDetailMap.put(checkgroup.name,checklist);
      }
    }
  }
}