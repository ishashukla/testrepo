/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMemberEdit_Test
 * Description: Test class for AccountTeamMemberEdit (T-477657)
 * Created Date: 3rd March 2016
 * Created By: Deepti Maheshwari(Appirio)
=======================================================================*/
@isTest
private class AccountTeamMemberEdit_Test {
   
    static AccountTeamMember atms;
    public static void createTestData() {
        List<User> testUser = TestUtils.createUser(1,'System Administrator', true);
        List<User> testUser1 = TestUtils.createUser(1,'Sales - COMM INTL', false);
        testUser1.get(0).email = 'testComm@gamil.com';
        insert testUser1;
        List<Account> accList = TestUtils.createAccount(1, true);
        atms = TestUtils.createTeamMembers(accList.get(0).id, testUser1.get(0).id, 'Sales Manager', true);
    }
    static testMethod void basicTest() {
        createTestData();
        //fetch the created Account and User
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account 0'];
        User usr = [SELECT Id FROM User WHERE Email = 'testComm@gamil.com' limit 1];
        ApexPages.currentPage().getParameters().put('Id', atms.id);
          // Set the standard controller and call the constructor.
        AccountTeamMemberEdit editController = new AccountTeamMemberEdit();
    
        // Call the methods and create new team member
        editController.wrapper.member.TeamMemberRole = 'Sales Executive';
        editController.wrapper.accountAccess = 'Edit';
        editController.wrapper.opportunityAccess = 'Read';
        editController.wrapper.caseAccess = 'Read';
        editController.saveNewTeamMembers();    
        
    
        List<AccountTeamMember> atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
        System.assertEquals('Sales Executive', atm.get(0).TeamMemberRole);
        
        // Cancel (removes ATM from the list)
        editController.doCancel();

    }
}