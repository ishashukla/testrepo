/**
** This is the test class to test the chatter post .
** For connectApi seeAllData need to be set as true 
**/
@isTest(SeeAllData=true)
private class NV_ChatterUtilsTest {

    static testMethod void PostTest() {
       Test.startTest();    
            NV_ChatterUtils.mentionTextPost(userInfo.getUserId(),userInfo.getUserId(), 'Sample Post');
            List<FeedItem> feedItems = [SELECT Id, ParentId, Type, CreatedById, CreatedDate, IsDeleted, LastModifiedDate, SystemModstamp, CommentCount, LikeCount, Title, Body, LinkUrl, ContentData, RelatedRecordId, ContentFileName, ContentDescription FROM FeedItem where CreatedById =:userInfo.getUserId()];
            if(feedItems.size()> 0){
                System.assertEquals(true,true);
            }
            else{
                System.assertEquals(true,false);
            }
       Test.stopTest();      
    }
}