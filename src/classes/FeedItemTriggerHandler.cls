/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Jan 23, 2014 
 Purpose       :  Handler class for trigger FeedItemTrigger
 Reference     :  Task T-241113
 Modifications :  Task T-249942     Feb 24, 2013     Sonal Shrivastava
*******************************************************************************/
public with sharing class FeedItemTriggerHandler {
  
  //****************************************************************************
  // Method called on after delete
  //****************************************************************************
  public static void onAfterDelete(List<FeedItem> oldList){
    deleteIntelChatterPost(oldList);
  }
  
  //****************************************************************************
  // Method to delete ChatterPost field on Intel object
  //****************************************************************************
  private static void deleteIntelChatterPost(List<FeedItem> oldList){
    Map<String, Intel__c> mapIntelId_Intel = new Map<String, Intel__c>();    
    for(FeedItem post : oldList){
      //If it is Intel chatter post, add the intel record in a map with
      //key as intel Id and Intel record with Chatter Post as null and inactive.
      if(String.valueOf(post.ParentId).startsWith(Constants.INTEL_PREFIX)){
        if(!mapIntelId_Intel.containsKey(post.ParentId)){
          mapIntelId_Intel.put(post.ParentId, new Intel__c(Id = post.ParentId,
                                                           Chatter_Post__c = null,
                                                           Intel_Inactive__c = true));
        }
      }
    }
    //Update Intel Records
    if(mapIntelId_Intel.size() > 0){
      update mapIntelId_Intel.values();
    }
  } 
  
}