/*************************************************************************************************
Created By:    Rahul Aeran
Date:          April 23, 2016
Description:   Test class for Medical_TW_ComplaintInfo for Medical Division
**************************************************************************************************/
@isTest
private class Medical_TW_ComplaintInfoTest {
  static testMethod void testMedical_TW_ComplaintInfoTest(){
    Test.startTest();
      Medical_TW_ComplaintInfo.CreateComplaintRequest_element objCreateComplaintRequest = new Medical_TW_ComplaintInfo.CreateComplaintRequest_element();
      Medical_TW_ComplaintInfo.IntakeAttachment objIntakeAttachment = new Medical_TW_ComplaintInfo.IntakeAttachment();
      Medical_TW_ComplaintInfo.ReprocessedProductsGrid objReprocessedProductsGrid = new Medical_TW_ComplaintInfo.ReprocessedProductsGrid();
      Medical_TW_ComplaintInfo.ProductGridRow objProductGridRow = new Medical_TW_ComplaintInfo.ProductGridRow();
      Medical_TW_ComplaintInfo.ContactGridRow objContactGridRow = new Medical_TW_ComplaintInfo.ContactGridRow();
      Medical_TW_ComplaintInfo.AddAttachmentResponse_element objAddAttachmentResponse_element = new Medical_TW_ComplaintInfo.AddAttachmentResponse_element();
      
      
      Medical_TW_ComplaintInfo.ResultMessage objResultMessage = new Medical_TW_ComplaintInfo.ResultMessage();
      Medical_TW_ComplaintInfo.ProductGrid objProductGrid = new Medical_TW_ComplaintInfo.ProductGrid();
      Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGrid objRegAuthorityNotifiedBodyGrid = new Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGrid();
      Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGridRow objRegAuthorityNotifiedBodyGridRow = new Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGridRow();
      
      Medical_TW_ComplaintInfo.ExceptionMessage objExceptionMessage = new Medical_TW_ComplaintInfo.ExceptionMessage();
      Medical_TW_ComplaintInfo.ContactGrid objContactGrid = new Medical_TW_ComplaintInfo.ContactGrid();
      Medical_TW_ComplaintInfo.AddAttachmentRequest_element objAddAttachmentRequest_element = new Medical_TW_ComplaintInfo.AddAttachmentRequest_element();
      Medical_TW_ComplaintInfo.ProductsToBeReturnedList objProductsToBeReturnedList = new Medical_TW_ComplaintInfo.ProductsToBeReturnedList();
      Medical_TW_ComplaintInfo.ComplaintInfo objComplaintInfo = new Medical_TW_ComplaintInfo.ComplaintInfo();
      Medical_TW_ComplaintInfo.CreateComplaintResponse_element objCreateComplaintResponse_element = new Medical_TW_ComplaintInfo.CreateComplaintResponse_element();
      
      System.assert(objCreateComplaintRequest != null , 'Object created');
      System.assert(objReprocessedProductsGrid != null , 'Object created');
      System.assert(objProductGridRow != null , 'Object created');
      System.assert(objContactGridRow != null , 'Object created');
      System.assert(objAddAttachmentResponse_element != null , 'Object created');
      System.assert(objResultMessage != null , 'Object created');
      System.assert(objProductGrid != null , 'Object created');
      System.assert(objRegAuthorityNotifiedBodyGrid != null , 'Object created');
      System.assert(objRegAuthorityNotifiedBodyGridRow != null , 'Object created');      
    Test.stopTest();
  }
}