public class SPS_OpportunityTriggerHandler {
//Commented by Shubham for Story S-447620
/*
  private static Set<Id> SPS_RecordTypes{
    get{
      if(SPS_RecordTypes == null){
        SPS_RecordTypes = new Set<Id>();
        SPS_RecordTypes.add(Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('SPS Opportunity Record Type').getRecordTypeId());
      }
      return SPS_RecordTypes;
    }
  }
  
 public static void populateSPSprograms(Map<Id, Opportunity> mapOldOps, Map<Id, Opportunity> mapNewOps, String triggerAction)
 {
    // Eligible Opportunities.
    Map<Id, Opportunity> mapEligibleOps = new Map<Id, Opportunity>();
    Set <Id> OpptyAccountIds = new Set<Id>();
    
    for(Opportunity opp :mapNewOps.values()){
      if(SPS_RecordTypes.contains(opp.RecordTypeId) && (opp.StageName == 'Closed Won')){
      	if (triggerAction == 'isInsert'){
        	mapEligibleOps.put(opp.Id, opp);
        	OpptyAccountIds.add(opp.AccountId);
      	}
      	if(triggerAction == 'isUpdate'){
      		if(opp.StageName != mapOldOps.get(opp.Id).StageName && (opp.Amount > 0)){
      			mapEligibleOps.put(opp.Id, opp);
        		OpptyAccountIds.add(opp.AccountId);
      		}
      	}
      }
    }
    system.debug('----mapEligibleOps----' + mapEligibleOps);
  
    Map<Id, Account> mapAccounts = new Map<Id, Account>();
	if(OpptyAccountIds.size()>0){
    	//Getting all the Account to populate Strategic account and Negotiated Contract checkbox based on parent account
    	mapAccounts = new Map<Id, Account>([Select Id, Name, RecordTypeId, Type from Account where Id in : OpptyAccountIds]);
    }
    System.debug('===mapAccount==='+mapAccounts);
    
    for(Opportunity opp :mapEligibleOps.values())
    {
    	if(mapAccounts.get(opp.AccountId).Type == 'System'){
    		mapEligibleOps.remove(opp.Id);
    	}
    }
    system.debug('----mapEligibleOps----' + mapEligibleOps);
    
    // Create Program
    List<SPS_Program__c> lstProgram = new List<SPS_Program__c>();
    for(Opportunity opp :mapEligibleOps.values())
    {
        SPS_Program__c objProgram = new SPS_Program__c();
        objProgram.Account__c = opp.AccountId;
        objProgram.Opportunity__c = opp.Id;
        objProgram.Name = opp.Name;
        objProgram.Program_Value__c = opp.Amount;
        objProgram.SPS_Client_Executive__c = opp.OwnerId;
        objProgram.Contract_Sign_Date__c = opp.Contract_Sign_Date__c;
        objProgram.Description__c = opp.Description;
        lstProgram.add(objProgram);
    }
    System.debug('----lstProgram----' + lstProgram);
    Database.insert(lstProgram);

    // prepare map for Opportunity versus SPS Program
    Map<Id, SPS_Program__c> mapSPSprogram = new Map<Id, SPS_Program__c>();
    for(SPS_Program__c SP :lstProgram){
        mapSPSprogram.put(SP.Opportunity__c, SP);
    }
    
    // Create Program Product
    List<OpportunityLineItem> lstOpsItmes = [SELECT Id, TotalPrice, OpportunityId, Description, UnitPrice, 
                                            Product2.Family, Product2Id, Product2.Name, Quantity, Discount
                                            FROM OpportunityLineItem
                                            WHERE OpportunityId = :mapEligibleOps.keySet()];


 
     List<SPS_Program_Products__c> lstProgramProducts = new List<SPS_Program_Products__c>();
        for(OpportunityLineItem oppItem :lstOpsItmes){
            Id programId = mapSPSprogram.get(oppItem.OpportunityId).Id;
            Id accountId = mapEligibleOps.get(oppItem.OpportunityId).AccountId;
            System.debug('----programId----' + programId);
        
            SPS_Program_Products__c lstProgramProduct = new SPS_Program_Products__c();
            
            lstProgramProduct.Name = oppItem.Product2.Name;
            lstProgramProduct.Program__c = programId;
            lstProgramProduct.Account__c = accountId;
            lstProgramProduct.Product__c = oppItem.Product2Id;
            lstProgramProduct.Quantity__c = oppItem.Quantity;
            lstProgramProduct.Sales_Price__c = oppItem.UnitPrice;
            lstProgramProduct.Total_Price__c = oppItem.TotalPrice;
            lstProgramProduct.Discount__c = oppItem.Discount;
            lstProgramProduct.Line_Description__c = oppItem.Description;
            lstProgramProducts.add(lstProgramProduct);
         }
        
        // Insert new records.
        System.debug('----lstProgramProducts----' + lstProgramProducts);
        Database.insert(lstProgramProducts);
     }
*/
}