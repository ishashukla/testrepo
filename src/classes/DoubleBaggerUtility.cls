//
// (c) 2014 Appirio, Inc.
// Class - DoubleBaggerUtility
// 
//
// 8 March 2016     Sahil Batra       Original
// 2 June 2016		Isha Shukla 	  Modified(T-507898) Commented logic to get Manager from Roles and Hierarchy
// 2 June 2016		Sahil Batra		  Modified(T-507910) Added method getRepforManagerFromMancode to get Sales Rep for Managers
public class DoubleBaggerUtility {
    //Method to get business unit for user
    public static Map<Id,String> getUsersBusinessUnits(Set<Id> userId){
        Map<Id,String> userToBUMap = new Map<Id,String>();
        List<User> userList = new List<User>();
        userList = [SELECT Id,Division FROM User WHERE Id IN:userId];
        for(User user : userList){
            userToBUMap.put(user.Id,user.Division);
        }
        return userToBUMap;
    }
    //Method to get Manager for particular user based on BU
    public static Map<String,Id> getManagerforUser(Set<String> userBUSet){
        String profileName = Label.Sales_Manager_Profile;
        List<Profile> profileList = new List<Profile>();
        profileList = [SELECT Id,Name FROM Profile WHERE Name =:profileName];
        Id profileId;
        if(profileList.size() > 0){
            profileId = profileList.get(0).Id;
        }
        Set<Id> userIdSet  = new Set<Id>();
        Set<String> businessUnit  = new Set<String>();
        Set<String> doubleBaggerUsers  = new Set<String>();
        Map<String,Id> userToManagerMap = new Map<String,Id>();
        for(String userBU : userBUSet){
            List<String> userBULIst = new List<String>();
            userBULIst = userBU.split('\\+');
            userIdSet.add(userBULIst.get(0));
            businessUnit.add(userBULIst.get(1));
        }
        System.debug('businessUnit==='+businessUnit+'userIdSet==='+userIdSet);
        // T-507898 changes start
        List<Mancode__c> assignmentList = new List<Mancode__c>();
        assignmentList = [SELECT Id,Business_Unit__c,Sales_Rep__c,Manager__c 
                          FROM Mancode__c 
                          WHERE Business_Unit__c IN:businessUnit 
                          AND Sales_Rep__c IN:userIdSet];
        for(Mancode__c user : assignmentList){
            userToManagerMap.put(user.Sales_Rep__c+'+'+user.Business_Unit__c,user.Manager__c);
        }
        // T-507898 changes end
        return userToManagerMap;
    }
    // T-507910 changes start
    public static Map<Id,Set<Id>> getRepforManagerFromMancode(Set<Id> managerIdSet){
    	List<Mancode__c> recordList = new List<Mancode__c>();
    	recordList = [SELECT Id,Business_Unit__c,Sales_Rep__c,Manager__c 
                      FROM Mancode__c 
                      WHERE Manager__c IN:managerIdSet];
        Map<Id,Set<Id>> mancodeManagertoUserMap = new Map<Id,Set<Id>>();
        for(Mancode__c mancodeRecord : recordList){
        	if(!mancodeManagertoUserMap.containsKey(mancodeRecord.Manager__c)){
        		mancodeManagertoUserMap.put(mancodeRecord.Manager__c,new Set<Id>());
        	}
        	mancodeManagertoUserMap.get(mancodeRecord.Manager__c).add(mancodeRecord.Sales_Rep__c);
        }
        return mancodeManagertoUserMap;
    }
    // T-507910 changes end
    //Method to get director or VP for a particular Manager
    public static Map<String,String> getDirectorOrVPFromManager(Set<Id> managerIdSet) {
    	List<Mancode__c> mancodeList = new List<Mancode__c>();
    	mancodeList = [SELECT Id,Business_Unit__c,Sales_Rep__c,Manager__c 
                      FROM Mancode__c 
                      WHERE Sales_Rep__c IN:managerIdSet];
        Map<String,String> directorOrVPIdToMangerMap = new Map<String,String>();
        for(Mancode__c mancode : mancodeList) {
        	if(!directorOrVPIdToMangerMap.containsKey(mancode.Sales_Rep__c+'+'+mancode.Business_Unit__c)) {
        		directorOrVPIdToMangerMap.put(mancode.Sales_Rep__c+'+'+mancode.Business_Unit__c,mancode.Manager__c+'+'+mancode.Business_Unit__c);
        	}
        }
       return directorOrVPIdToMangerMap;
    }
    //This method is used to ger manager for particular user with BU
     public static Map<String,String> getManagerandBUforUser(Set<String> userBUSet){
        Set<Id> usrIdSet  = new Set<Id>();
        Set<String> bUnit  = new Set<String>();
        Map<String,String> userBUToManagerBUMap = new Map<String,String>();
        for(String userBU : userBUSet){
            List<String> userBULIst = new List<String>();
            userBULIst = userBU.split('\\+');
            usrIdSet.add(userBULIst.get(0));
            bUnit.add(userBULIst.get(1));
        }
        List<Mancode__c> mancodeList = new List<Mancode__c>();
        mancodeList = [SELECT Id,Business_Unit__c,Sales_Rep__c,Manager__c 
                          FROM Mancode__c 
                          WHERE Business_Unit__c IN:bUnit 
                          AND Sales_Rep__c IN:usrIdSet];
        for(Mancode__c user : mancodeList){
            userBUToManagerBUMap.put(user.Sales_Rep__c+'+'+user.Business_Unit__c,user.Manager__c+'+'+user.Business_Unit__c);
        }
            return userBUToManagerBUMap;
    }
}