/**================================================================      
* Appirio, Inc
* Name: INS_ProductPricingImpl.cls
* Description: Strategic implementation of the Product Pricing for
*              the lightning component for INS
* Created Date: 03-AUG-2016
* Created By: Ray Dehler (Appirio)
*
* Date Modified      Modified By      Description of the update
* 03 AUG 2016        Ray Dehler       Added header and added support for Instruments S-423983
==================================================================*/
global with sharing class INS_ProductPricingImpl implements Mobile_ProductPricing {
	// Instruments Accounts should show all Accounts for which I am on the team
	global List<Account> getMyCustomerAccounts() {
        return [SELECT Id, Name, AccountNumber, ShippingCity, ShippingState, Nickname__c, OwnerId, Owner.Name
        		FROM Account
        		WHERE Id in (SELECT AccountId
        					 FROM AccountTeamMember
        					 WHERE UserId =: UserInfo.getUserId())
        		ORDER BY Name];
	}

	global Boolean isBigDataMode() {
		return true;
	}

	global List<NV_Wrapper.ProductLineData> getAllProducts(){
		Set<String> priceBookNames = getPricebookNames();

		Map<String, NV_Wrapper.ProductLineData> productLineData = new Map<String, NV_Wrapper.ProductLineData>();
		for (AggregateResult ar : [SELECT ODP_Category__c, Segment__c, count(Id)
								   FROM Product2
								   WHERE IsActive = true
								   AND Id in (SELECT Product2Id FROM PriceBookEntry
										   WHERE PriceBook2.Name in :priceBookNames
											     AND IsActive = true)
								   GROUP BY ODP_Category__c, Segment__c
								   ORDER BY ODP_Category__c]) {

			String category = String.isBlank((String)ar.get('ODP_Category__c')) ? 'No Product Line' : (String)ar.get('ODP_Category__c');
            if(!productLineData.containsKey(category)){
                productLineData.put(category, new NV_Wrapper.ProductLineData(category));
            }	
            productLineData.get(category).addNewSegment((String)ar.get('Segment__c'));
		}

        List<NV_Wrapper.ProductLineData> finalData = productLineData.values();
        for(integer i = 0; i < finalData.size(); i++){
            NV_Wrapper.ProductLineData pld = finalData[i];
            pld.finalize();
        }
		return finalData;
	}

	global List<NV_Wrapper.ProductData> getProductsForSegment(String segment, String productLine) {
		Set<String> priceBookNames = getPricebookNames();

		if (String.isBlank(segment)) {
			segment = null;
		}
		if (String.isBlank(productLine) || productLine == 'No Product Line') {
			productLine = null;
		}

		List<NV_Wrapper.ProductData> productData = new List<NV_Wrapper.ProductData>();
		for (Product2 prod : [SELECT Id, Name, ProductCode, Part_Number__c, Segment__c, Description__c, Long_Description__c,
							ODP_Category__c, Catalog_Number__c, GTIN__c, 
							(SELECT Id, UnitPrice FROM PricebookEntries 
								WHERE PriceBook2.Name in :priceBookNames AND IsActive = true LIMIT 1)
							FROM Product2
							WHERE IsActive = true
							AND Id in (SELECT Product2Id FROM PriceBookEntry
									   WHERE PriceBook2.Name in :priceBookNames
									     AND IsActive = true)
							AND Segment__c = :segment
							AND ODP_Category__c = :productLine
							ORDER BY ODP_Category__c]) {
			productData.add(new NV_Wrapper.ProductData(prod));
		}
		return productData;
	}	

	global List<NV_Wrapper.ProductLineData> searchForProducts(String searchText) {
		if (String.isBlank(searchText) || String.isBlank(searchText.trim())) {
			return getAllProducts();
		} else {
			Set<String> priceBookNames = getPricebookNames();

			searchText = '%' + searchText + '%';

			Map<String, NV_Wrapper.ProductLineData> productLineData = new Map<String, NV_Wrapper.ProductLineData>();
			for (Product2 prod : [SELECT Id, Name, ProductCode, Part_Number__c, Segment__c, Description__c, Long_Description__c,
								ODP_Category__c, Catalog_Number__c, GTIN__c, 
								(SELECT Id, UnitPrice FROM PricebookEntries 
									WHERE PriceBook2.Name in :priceBookNames AND IsActive = true LIMIT 1)
								FROM Product2
								WHERE IsActive = true
								AND Id in (SELECT Product2Id FROM PriceBookEntry
										   WHERE PriceBook2.Name in :priceBookNames
										     AND IsActive = true)
								AND (Name like :searchText OR ProductCode like :searchText OR Segment__c like :searchText)
								ORDER BY ODP_Category__c]) {
				String category = String.isBlank(prod.ODP_Category__c) ? 'No Product Line' : prod.ODP_Category__c;
	            if(!productLineData.containsKey(category)){
	                productLineData.put(category, new NV_Wrapper.ProductLineData(category));
	            }
	            productLineData.get(category).addNewProduct(prod, isBigDataMode(), true);
			}

	        List<NV_Wrapper.ProductLineData> finalData = productLineData.values();
	        for(integer i = 0; i < finalData.size(); i++){
	            NV_Wrapper.ProductLineData pld = finalData[i];
	            pld.finalize();
	        }
			return finalData;
		}
	}

	global List<User> getFollowableUsers() {
		return new List<User>();
	}

	global List<NV_Wrapper.ModelNPricing> retrievePricing(String customerId, String productIdsCSV) {
		System.debug('[DD] CustomerID : '+customerId);
		System.debug('[DD] productIdsCSV : '+productIdsCSV);
		Map<String, String> pricingSettings = new Map<String, String>();

		Map<String, Instruments_Product_Pricing_Settings__c> instrumentsProductPricingSettings =
    		Instruments_Product_Pricing_Settings__c.getAll();

		for (String key : instrumentsProductPricingSettings.keySet()) {
			pricingSettings.put(key, instrumentsProductPricingSettings.get(key).Value__c);
		}

		List<NV_Wrapper.ModelNPricing> pricingData;
		try{
			Datetime today = Datetime.now();
			NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort obj = new NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort();
	        NV_RTPM.RealTimePriceMultiRequestType req = new NV_RTPM.RealTimePriceMultiRequestType();
	        req.ConfigName = pricingSettings.get('ConfigName')==null?'Global':pricingSettings.get('ConfigName');
	        req.CustomerID = getCleanedCustomerId(customerId);
	        req.EffectiveDate = today;
	        req.ModelDate = today;
	        req.CurrencyCode = pricingSettings.get('CurrencyCode')==null?'USD':pricingSettings.get('CurrencyCode');
	        req.OrgUnitID = pricingSettings.get('OrgUnitID')==null?'NV_US':pricingSettings.get('OrgUnitID');
	        req.BusSegCode = pricingSettings.get('BusSegCode')==null?'':pricingSettings.get('BusSegCode');
	        req.ERPCode = pricingSettings.get('ERPCode')==null?'EMPR':pricingSettings.get('ERPCode');
	        NV_RTPM.ProductIDs pIds = new NV_RTPM.ProductIDs();
	        pIds.ProductID = productIdsCSV.split(',');
	        req.ProductIDs = pIds;
	        NV_RTPM.RealTimePriceMultiResponseType response = obj.ModelNProductRealTimePriceLookup(req);
	        System.debug('[DD] 1. ResolvedPrices: '+ response.ResolvedPrices);
	        System.debug('[DD] 2. ResolvedPrices.ResolvedPrice: '+ response.ResolvedPrices.ResolvedPrice);
	        if(response.ResolvedPrices != null && 
	        		response.ResolvedPrices.ResolvedPrice != null && 
	        		response.ResolvedPrices.ResolvedPrice.size()>0){
            	pricingData = new List<NV_Wrapper.ModelNPricing>();
            	for(NV_RTPM.ResolvedPriceType record: response.ResolvedPrices.ResolvedPrice){
            		NV_Wrapper.ModelNPricing pricing = new NV_Wrapper.ModelNPricing(record);
            		//pricing.ProductName = record.product
            		pricingData.add(pricing);
            	}
	        }
		}
		catch(Exception e){
			System.debug(e.getStackTraceString());
		}
		return pricingData;
	}

	// for INS, Pricebook Names are semicolon delimited on the User's division
	private Set<String> getPricebookNames() {
		Set<String> priceBookNames = new Set<String>();

		String userDivision = [select Division from User where Id = :UserInfo.getUserId()][0].Division;
		if (userDivision == null) {
			priceBookNames.add('Surgical');
		} else {
			for (String division : userDivision.split(';')) {
				priceBookNames.add(division.trim() + ' Price Book');
			}
		}

		return priceBookNames;
	}

	private static String getCleanedCustomerId(String customerId) {
		return customerId == null ? null : customerId.replaceAll('-[\\d]*$', '');
	}


}