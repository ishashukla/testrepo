/**================================================================      
 * Appirio, Inc
 * Name: UpdateManagerBatchTest
 * Description: Test Class for UpdateManagerBatch
 * Created Date: [06/03/2016]
 * Created By: [Isha Shukla] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/ 
@isTest
private class UpdateManagerBatchTest {
    static List<Mancode__c> recordsWithOldManager;
    static List<User> userList;
    static User adminUser;
    static List<Opportunity> oppList;
    @isTest static void testUpdateManagerBatch() {
        Constants cs = new Constants();
        createData();
        Test.startTest();
        try {
        Database.executeBatch(new UpdateManagerBatch(recordsWithOldManager[0].Manager__c,userList[2].Id));
        } catch(Exception e) {
        }
        Test.stopTest();
        System.assertEquals(userList[2].Id,[SELECT Manager__c FROM Mancode__c WHERE Id = :recordsWithOldManager[0].Id].Manager__c);
        System.assertEquals(userList[2].Id,[SELECT BU_Manager__c FROM Opportunity WHERE Id = :oppList[0].Id].BU_Manager__c);
    }
    public static void createData() {
        adminUser = TestUtils.createUser(1,'System Administrator',false).get(0);
        adminUser.Division = 'IVS';
        insert adminUser;
        userList = TestUtils.createUser(3,'Instruments Sales User', True);
        System.runAs(adminUser) {
        //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=adminUser.Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
        //insert terr;
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
        Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        recordsWithOldManager = TestUtils.createMancode(1,userList[0].Id,userList[1].Id,false);
        //recordsWithOldManager[0].territory__c = terr.id;
        insert recordsWithOldManager;
        List<Account> accList = TestUtils.createAccount(1,True);
        oppList = TestUtils.createOpportunity(1,accList[0].Id,False);
        oppList[0].RecordTypeId = recordTypeInstrument;
        oppList[0].BU_Manager__c = userList[1].Id;
        oppList[0].Business_Unit__c = 'IVS';
        insert oppList;
        Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
        Id recordTypeRep = rtByNameRep.getRecordTypeId();
        List<Forecast_Year__c> lstForcastYear = TestUtils.createForcastYear(1,recordsWithOldManager[0].Manager__c,recordTypeRep,false);
        lstForcastYear[0].Business_Unit__c = 'NSE';
        lstForcastYear[0].Manager__c = userList[1].Id;
        lstForcastYear[0].Year__c = String.valueOf(System.today().year());
        Schema.RecordTypeInfo rtByNameManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast');
        Id recordTypeManager = rtByNameManager.getRecordTypeId();
        lstForcastYear.add(TestUtils.createForcastYear(1,userList[2].Id,recordTypeManager,false).get(0));
        lstForcastYear[1].Manager__c = userList[1].Id;
        lstForcastYear[1].Business_Unit__c = 'NSE';
        lstForcastYear[1].Year__c = String.valueOf(System.today().year());
        insert lstForcastYear;
        }
    }
}