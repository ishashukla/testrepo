// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncCycleCountTransaction_SCH -  Scheduled Apex to sync new cycle count records
//
// 28 Oct 2016, Isha Shukla (T-550259) 
global class COMM_SyncCycleCountTransaction_SCH implements Schedulable{
    global void execute(SchedulableContext  sc){
      List<SVMXC__Stock_Adjustment__c> recordList = new List<SVMXC__Stock_Adjustment__c>();
        // Querying all records of stock transfer and stock transfer line.
        recordList = [SELECT Asset__r.External_ID__c,SVMXC__Product__r.ProductCode,SVMXC__Location__r.Location_ID__c,
                      SVMXC__New_Quantity2__c,Asset__r.SVMXC__Serial_Lot_Number__c,Asset__r.Quantity__c
                      FROM SVMXC__Stock_Adjustment__c WHERE Integration_Status__c ='New'];
        List<SVMXC__Stock_Adjustment__c> listOfStockAdjust = new List<SVMXC__Stock_Adjustment__c>();
        List<SVMXC__Stock_Adjustment__c> listOfStockAdjustToUpdate = new List<SVMXC__Stock_Adjustment__c>();
        // Number of records to be passed at a time to Queueable class.
        COMMRecordCount__c customSetting = COMMRecordCount__c.getOrgDefaults();
        Integer count = 0;
        if(customSetting != null) {
            count = Integer.valueOf(customSetting.Cycle_Count__c);   
        }
        Integer i=0;
        // Passing records to Queueable class and updating Integration Status.
        if(recordList.size() > 0 && count > 0) {
            for(SVMXC__Stock_Adjustment__c stockAdjust : recordList) {
                if(i != count && i < count) {
                    stockAdjust.Integration_Status__c = 'InProgress';
                    listOfStockAdjust.add(stockAdjust);
                    listOfStockAdjustToUpdate.add(stockAdjust);
                    i = i+1;
                    if(listOfStockAdjust.size() == count) {
                        COMM_SyncCycleCountTransaction_Queue queueObj = new COMM_SyncCycleCountTransaction_Queue(listOfStockAdjust);
                        ID jobID = System.enqueueJob(queueObj);
                        i = 0 ;
                        listOfStockAdjust.clear();
                    }
                    
                } 
                
            }
            if(listOfStockAdjust.size() > 0){
                COMM_SyncCycleCountTransaction_Queue queueObj = new COMM_SyncCycleCountTransaction_Queue(listOfStockAdjust);
                ID jobID = System.enqueueJob(queueObj);
                listOfStockAdjust.clear();
            }
            if(listOfStockAdjustToUpdate.size() > 0) {
                update listOfStockAdjustToUpdate;
            }
        }
      
    }
}