global class NV_Wrapper {
    
    public class OrderWrapper {
        @AuraEnabled
        public Order OrderData {get;set;}
        //Start by Harendra for story S-412528
        @AuraEnabled
      public Decimal TotalOpen {get;set;}
        @AuraEnabled
      public Map<String,decimal> OrderLineStatusTotalMap = new Map<String,decimal>();
      //End - Story S-412528
        @AuraEnabled        
        public Decimal BookedTotal {get;set;}
        @AuraEnabled
        public Decimal ShippedTotal {get;set;}
        @AuraEnabled
        public Date startDate, endDate;
        @AuraEnabled
        public Boolean OnHold {get;set;}
        @AuraEnabled
        public Boolean Expedited {get;set;}

      @AuraEnabled public Boolean pastDue { get; set; }
      @AuraEnabled public List<Order_Invoice__c> openOrderInvoices { get; set; }
	
	/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - Start ***/
	@AuraEnabled
	public List<OrderItemWrapper> orderItemWrap = new List<OrderItemWrapper>();
	/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - End ***/
	
        public OrderWrapper(Order record){
            this.OrderData = record;
            TotalOpen = 0;  //Added by Harendra for story S-412528
            BookedTotal = 0;
            ShippedTotal = 0;
            this.OnHold = false;
            this.pastDue = false;
            openOrderInvoices = new List<Order_Invoice__c>();
        }
        public OrderWrapper(Order record, Date startDate, Date endDate){
            this(record);
            this.startDate = startDate;
            this.endDate = endDate;
            this.pastDue = false;
            openOrderInvoices = new List<Order_Invoice__c>();
        }
    }

    public class UserOrders{
        @AuraEnabled
        public Id UserId {get;set;}
        @AuraEnabled
        public String UserName {get;set;}
        @AuraEnabled
        public List<NV_Wrapper.OrderWrapper> Orders{get;set;}

      @AuraEnabled public List<NV_Wrapper.OrderWrapper> pendingInvoices { get; set; }

        public UserOrders(Id userId, String userName){
            this.UserId = userId;
            this.UserName = userName;
            Orders = new List<NV_Wrapper.OrderWrapper>();
            pendingInvoices = new List<NV_Wrapper.OrderWrapper>();
        }

    }

    global class ProductLineData implements Comparable{
        @AuraEnabled
        public String ProductLineName {get;set;}
        @AuraEnabled
        public List<NV_Wrapper.SegmentData> segments {get;set;}
        private Map<String, NV_Wrapper.SegmentData> segmentMap;
        
        public List<NV_Wrapper.SegmentData> getSegments(){
            List<NV_Wrapper.SegmentData> segmentDataSorted = segmentMap.values();
            segmentDataSorted.sort();
            return segmentDataSorted;
        }

        public ProductLineData(String name){
            this.ProductLineName = name;
            segmentMap = new Map<String, NV_Wrapper.SegmentData>();
        }

        public void addNewProduct(Product2 productRecord){
             addNewProduct(productRecord, false, false);
        }

        // if we are adding a product from search, always add it
        public void addNewProduct(Product2 productRecord, Boolean isBigDataMode, Boolean isFromSearch) {
            String segment = productRecord.Segment__c == null ? '' : productRecord.Segment__c;
            // if Segment is blank and we are doing the Instruments flow, still add it to the list
            if(String.isNotBlank(segment) || isBigDataMode){
                if(!segmentMap.containsKey(segment.toLowerCase())){
                    segmentMap.put( segment.toLowerCase() , new NV_Wrapper.SegmentData(segment));
                }
                // if we are in "big data mode", do not add individual products to be cached locally
                if (isFromSearch || !isBigDataMode) {
                    segmentMap.get(segment.toLowerCase()).addNewProduct(productRecord);
                }
            }
        }

        public void addNewSegment(String segment) {
            if (segment == null) {
                segment = '';
            }
            segmentMap.put(segment.toLowerCase(), new NV_Wrapper.SegmentData(segment));
        }

        public Integer compareTo(Object compareToObject){
            ProductLineData compareTo = (ProductLineData) compareToObject;
            return (this.ProductLineName > compareTo.ProductLineName) ? 1 : 0;
        }
        
        public void finalize(){
            System.debug('TRACE: FINALIZE DATA');
            this.Segments = this.getSegments();
        }

    }

    public class SegmentData implements Comparable{
        @AuraEnabled
        public String SegmentName {get;set;}
        @AuraEnabled
        public List<NV_Wrapper.ProductData> productsList;

        
        public List<NV_Wrapper.ProductData> getProducts(){
            productsList.sort();
            return productsList;
        }

        public SegmentData(String name){
            this.SegmentName = name;
            productsList = new List<NV_Wrapper.ProductData>();
        }
        
        public void addNewProduct(Product2 productRecord){
            productsList.add(new NV_Wrapper.ProductData(productRecord));
        }

        public Integer compareTo(Object compareToObject){
            SegmentData compareTo = (SegmentData) compareToObject;
            return (this.SegmentName > compareTo.SegmentName) ? 1 : 0;
        }
        public void finalize(){
            
        }
    }

    global class ProductData implements Comparable{
        @AuraEnabled
        public Product2 Product {get;set;}
        @AuraEnabled
        public Decimal ListPrice {get;set;}

        public ProductData(Product2 productRecord){
            this.Product = productRecord;
            if(productRecord.PricebookEntries.size()>0){
                ListPrice = productRecord.PricebookEntries[0].UnitPrice;
            }
        }

        public Integer compareTo(Object compareToObject){
            ProductData compareTo = (ProductData) compareToObject;
            return (this.Product.Name > compareTo.Product.Name) ? 1 : 0;
        }


    }

    global class ModelNPricing{
        @AuraEnabled
        public String CatalogNumber {get;set;}

        @AuraEnabled
        public NV_RTPM.ResolvedPriceType ResolvedPrice {get;set;}

        public ModelNPricing(NV_RTPM.ResolvedPriceType record){
            this.CatalogNumber = record.ProductID;
            this.ResolvedPrice = record;
        }

    }

    public class RegionalDataWrapper{
        @AuraEnabled
        public Decimal RegionalBookedTotal {get;set;}
        
        @AuraEnabled
        public Decimal RegionalShippedTotal {get;set;}
        
        @AuraEnabled
        public Decimal RegionalOpenTotal {get;set;}
        
        @AuraEnabled
        public Decimal RegionalPendingPOTotal {get;set;}
        
        @AuraEnabled
        public Decimal RegionalCreditTotal {get;set;}
        
        @AuraEnabled
        public List<NV_Wrapper.UserWrapper> users{get;set;}
        
        public Map<Id, NV_Wrapper.UserWrapper> userWrapperMap;
        
        //As of Spring '16 this doesn't return correctly.
        public List<NV_Wrapper.UserWrapper> getUserWrapperMap(){
            return userWrapperMap.values();
        }


        public RegionalDataWrapper(){
            userWrapperMap = new Map<Id, NV_Wrapper.UserWrapper>();
            RegionalBookedTotal = 0;
            RegionalShippedTotal = 0;
            RegionalOpenTotal = 0;
            RegionalPendingPOTotal = 0;
            RegionalCreditTotal = 0;
        }
        
        public void finalize(){
            users = getUserWrapperMap();
        }
    }

    public class UserWrapper{
        @AuraEnabled
        public User User {get;set;}

        @AuraEnabled
        public Id UserId {get;set;}

        @AuraEnabled
        public Decimal BookedTotal {get;set;}

        @AuraEnabled
        public Decimal ShippedTotal {get;set;}

        @AuraEnabled
        public Decimal OpenTotal {get;set;}

        @AuraEnabled
        public Decimal PendingPOTotal {get;set;}

        @AuraEnabled
        public Decimal CreditTotal {get;set;}

        public UserWrapper(User userRecord){
            UserId = userRecord.Id;
            User = userRecord;
            BookedTotal = 0;
            ShippedTotal = 0;
            OpenTotal = 0;
            PendingPOTotal = 0;
            CreditTotal = 0;
        }
    }

	/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - Start ***/
	public class OrderItemWrapper{
        
		@AuraEnabled
		public Decimal totalPrice {get;set;}

		@AuraEnabled
		public Id orderItemId {get;set;}
			
		@AuraEnabled
		public String status {get;set;}

		@AuraEnabled
		public String oldHold {get;set;}

		public OrderItemWrapper(OrderItem orderItem){
		    totalPrice = Math.round(orderItem.Sub_Total__c);
		    orderItemId = orderItem.Id;
		    status = orderItem.Status__c;
		    oldHold = ((orderItem.On_Hold__c || orderItem.Order.On_Hold__c) ? 'On Hold' : '');
		}
	}
	/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - End ***/
}