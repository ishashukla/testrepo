// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncInventoryTransaction_SCH -  Scheduled Apex to sync new Inventory transfer between Org.
//
// 31 Oct 2016, Mohan Panneerselvam (T-550285) 
// 
global class COMM_SyncPartsOrderToEBS_SCH implements Schedulable{
    global void execute(SchedulableContext  sc){
        List<SVMXC__RMA_Shipment_Order__c> recordList = new List<SVMXC__RMA_Shipment_Order__c>();
        // Querying PO's that sld be synced to EBS.
        recordList = [SELECT id
                      FROM SVMXC__RMA_Shipment_Order__c WHERE SVMXC__Order_Status__c in('Closed','Submitted')
                      AND SVMX_Order__c = null];        
        for(SVMXC__RMA_Shipment_Order__c tempObj: recordList){
            if( !Test.isRunningTest() ) {
                System.enqueueJob(new COMM_CreateStdOrderForPartsOrderQueue(tempObj));
            }
        }
    }
}