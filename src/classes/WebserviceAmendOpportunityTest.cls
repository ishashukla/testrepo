//
// (c) 2012 Appirio, Inc.
//
//  Test class of  WebserviceAmendOpportunity
//
// 21 July 2015     Naresh K Shiwani      Original
//
@isTest
private class WebserviceAmendOpportunityTest {

  // verify the case when Opportunity Id is passed.
  static testmethod void testMethodWithId(){
    
    Test.startTest();
    TestUtils.createCommConstantSetting();
    Account accnt = new Account();
    accnt.Name            = 'AccntName';
    accnt.Type            = 'Hospital';
    accnt.Legal_Name__c   = 'Test';
    accnt.Flex_Region__c  = 'test';
    insert accnt;
    
    Id flexRTOnlyConventional 		= Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();  
    Opportunity opp                 = new Opportunity();
    opp.StageName                   = 'Quoting';
    opp.AccountId                   = accnt.Id;
    opp.CloseDate                   = System.today();
    opp.Name                        = 'test value';
    opp.StageName                   = 'Closed Won';
    opp.Contract_Sign_Date__c       = System.today();
    opp.Opportunity_Number__c       = '101';
    opp.RecordTypeId 				= flexRTOnlyConventional;
    insert opp;
    System.debug('opp========'+opp);
    Flex_Contract__c contract = new Flex_Contract__c();
    contract.Account__c = accnt.Id;
    contract.Opportunity__c = opp.Id;
    contract.Name = opp.Name;
    contract.Scheduled_Maturity_Date__c = System.today();
    insert contract;

    String result = WebserviceAmendOpportunity.amendOppRecord(opp.Id);
    
    opp.Opportunity_Number__c       = '999999999';
    update opp;
    String result1 = WebserviceAmendOpportunity.amendOppRecord(opp.Id);
    
    opp.Opportunity_Number__c       = '0';
    update opp;
    
    String result2 = WebserviceAmendOpportunity.amendOppRecord(opp.Id);
    Test.stopTest();
    // Verifing Results
    System.assertNotEquals(result, null);

  }
  
  // verify the case when wrong Opportunity Id is passed.
  static testmethod void testMethodWrongId(){
    
    Test.startTest();
    
    Account accnt = new Account();
    accnt.Name            = 'AccntNameNew';
    accnt.Type            = 'Hospital';
    accnt.Legal_Name__c   = 'Test';
    accnt.Flex_Region__c  = 'test';
    insert accnt;
    TestUtils.createCommConstantSetting();
    Id flexRTOnlyConventional 		= Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();  
    Opportunity opp                 = new Opportunity();
    opp.StageName                   = 'Quoting';
    opp.AccountId                   = accnt.Id;
    opp.CloseDate                   = System.today();
    opp.Name                        = 'test value';
    opp.StageName                   = 'Closed Won';
    opp.Contract_Sign_Date__c       = System.today();
//    opp.Opportunity_Number__c       = '101';
	opp.RecordTypeId 			= flexRTOnlyConventional;
    insert opp;
    System.debug('opp========'+opp);
    Flex_Contract__c contract = new Flex_Contract__c();
    contract.Account__c = accnt.Id;
    contract.Opportunity__c = opp.Id;
    contract.Name = opp.Name;
    contract.Scheduled_Maturity_Date__c = System.today();
    insert contract;

    String result = WebserviceAmendOpportunity.amendOppRecord(contract.Id);
    // verify the case when wrong Id is passed.
    System.assertEquals(result.contains('error:'), true);
    
    // Calling directly static method for one test case if "where" clause is blank or null
    result = WebserviceAmendOpportunity.getCreatableFieldsSOQL('Opportunity','');
    System.assertEquals(result, null);
    Test.stopTest();
    
  }
}