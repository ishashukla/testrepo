// (c) 2016 Appirio, Inc. 
//
// Description (CMC Asset to populate roll up summary fields)
//
// 03/30/2016   Deepti Maheshwari Reference : T-487784
//
/*****************************************************************************
ROLLUP SUMMARY 
This clas is used as rollup summary utility. You can develop trigger on any object and use 
Utility class to automate rollup summary on fields

COUNT example:
-----------------------------------------------------------------------------------------
list<RSFUtility.fieldDefinition> fieldDefinitions = new list<RSFUtility.fieldDefinition> {
    new RSFUtility.fieldDefinition ('COUNT', 'ID','Complete_Tasks_in_Tasks_RS__c') 
  // new RSFUtility.fieldDefinition ('COUNT', 'ID','<Field on Parent>') 
};

RSFUtility.rollUpTrigger(fieldDefinitions, trigger.new, 'Task__c', 'Project__c', 'Milestone__c', ' AND Complete__c = true');
// RSFUtility.rollUpTrigger(fieldDefinitions, trigger.new, '<Child Object>', '<Child-Parent Lookup Field>', '<Parent Object>', '<Where Condition>');
-----------------------------------------------------------------------------------------


SUM example:
-----------------------------------------------------------------------------------------
list<RSFUtility.fieldDefinition> fieldDefinitions = new list<RSFUtility.fieldDefinition> {
    new RSFUtility.fieldDefinition ('SUM', 'Total_Expense__c','Actual_Expense_From_Tasks_RS__c') 
  // new RSFUtility.fieldDefinition ('SUM', '<Field on Child to Rollup>','<Field on Parent>') 
};

RSFUtility.rollUpTrigger(fieldDefinitions, trigger.new, '<Child Object>', '<Child-Parent Lookup Field>', '<Parent Object>', '');
-----------------------------------------------------------------------------------------

******************************************************************************/
public with sharing class COMM_RollUpSummaryUtility {
//the following class will be used to house the field names
    //and desired operations
  
    public class fieldDefinition {
        public String operation {get;set;}
        public String childField {get;set;}
        public String parentField {get;set;}
        public String extraParentFields {get;set;} // NB - 04/20 - I-214012
       
        public fieldDefinition (String o, String c, String p, String extraFields) {
            operation = o;
            childField = c;
            parentField = p;
            extraParentFields = extraFields; // NB - 04/20 - I-214012
        }
    }
     
    public static List<sObject> rollUpTrigger(list<fieldDefinition> fieldDefinitions,
  
    list<sObject> records, String childObject, String childParentLookupField, 
  
    String parentObject, String queryFilter, Boolean performDML) {
         
        //Limit the size of list by using Sets which do not contain duplicate
        //elements prevents hitting governor limits
        set<Id> parentIds = new set<Id>();
         
        for(sObject s : records) {
            parentIds.add((Id)s.get(childParentLookupField));
        }
         
        //populate query text strings to be used in child aggregrator and 
        //parent value assignment
        String fieldsToAggregate = '';
        String parentFields = '';
         
        for(fieldDefinition d : fieldDefinitions) {
            fieldsToAggregate += d.operation + '(' + d.childField + ') ' + 
            ', ';
            parentFields += d.parentField + ', ';
            // NB - 04/20 - I-214012
            if(d.extraParentFields != null && d.extraParentFields.length() > 0){ 
                parentFields += d.extraParentFields + ', ';
            }
            // NB - 04/20 - I-214012 End
        }
         
        //Using dynamic SOQL with aggergate results to populate parentValueMap
    
        String aggregateQuery = 'Select ' + fieldsToAggregate + 
        childParentLookupField + ' from ' + childObject + ' where  ' + 
        childParentLookupField + ' IN :parentIds ' + queryFilter + ' ' +
        ' group by ' + childParentLookupField;
         
        //Map will contain one parent record Id per one aggregate object
        map<Id, AggregateResult> parentValueMap = 
        new map <Id, AggregateResult>();
         
        for(AggregateResult q : Database.query(aggregateQuery)){
            parentValueMap.put((Id)q.get(childParentLookupField), q);
        }
         
        //list of parent object records to update
        list<sObject> parentsToUpdate = new list<sObject>();
         
        String parentQuery = 'select ' + parentFields + ' Id ' +
         ' from ' + parentObject + ' where Id IN :parentIds';
         
        //for each affected parent object, retrieve aggregate results and 
        //for each field definition add aggregate value to parent field
        for(sObject s : Database.query(parentQuery)) {
             
            Integer row = 0; //row counter reset for every parent record
            for(fieldDefinition d : fieldDefinitions) {
                String field = 'expr' + row.format();
                AggregateResult r = parentValueMap.get(s.Id);
                 
                if(r != null) { 
                    Decimal value = ((Decimal)r.get(field) == null ) ? 0 : 
                        (Decimal)r.get(field);
                    s.put(d.parentField, value);
                } else {
                    s.put(d.parentField, 0);
                }
                row += 1; //plus 1 for every field definition after first
            }
            parentsToUpdate.add(s);
        }
         
        //if parent records exist, perform update of all parent records 
        //with a single DML statement
        if(parentsToUpdate.Size() > 0 && performDML) {
            update parentsToUpdate;
        } 
        return parentsToUpdate;
    }
}