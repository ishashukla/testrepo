// (c) 2015 Appirio, Inc.
//
// Class Name: SyncStandardAccountTeamMemberTest - Test Class for SyncStandardAccountTeamMember
// March 11 2016, Deepti Maheshwari Updation of contacts shares (Ref : T-477658)
// March 15 2016, Deepti Maheshwari Updated for task T-484874 (Ref :T-484874)
// March 18 2016, Meghna Vijay Updated for task T-484598 S-371029
// March 23 2016, Meghna Vijay Updated to insert more COMM Oppty Record Types  ref- T-484346
// May 14 2016, Ralf Hoffmann updated to replace Styker_Contact__c references with new native AccountContatRelation object (Ref S-414651)
@isTest 
private class SyncStandardAccountTeamMemberTest {
    static List <Opportunity> opportunityList = new List<Opportunity>();
    static List <Custom_Account_Team__c> customAccountTeamList = new List <Custom_Account_Team__c> ();
    static testMethod void testSyncStandardAccountTeamMember(){
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
         System.runAs(usr) {
        Account acc = TestUtils.createAccount(1, true).get(0);
        AccountTeamMember atm = TestUtils.createTeamMembers(acc.Id,usr.Id,'Surgical', true);
        Contact commContact = TestUtils.createCOMMContact(acc.id,'testCon',true);
        Custom_Account_Team__c cat = TestUtils.createCustomAccountTeam(acc.Id,commContact.id,usr.Id,'Navigation', true);
        Account acc1 = TestUtils.createAccount(1, false).get(0);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
        acc.RecordTypeId = rtByName.getRecordTypeId();
        insert acc1;
        AccountTeamMember atm1 = new AccountTeamMember();
        atm1.AccountId = acc1.Id;
        atm1.TeamMemberRole = 'Surgical';
        atm1.UserId = usr.Id;
        insert atm1;
        Custom_Account_Team__c cat1 = new Custom_Account_Team__c();
        cat1.Account__c = acc.Id;
        cat1.User__c = usr.Id;
        cat1.Team_Member_Role__c = 'Navigation';
        insert cat1;
            
        Test.startTest();
        SyncStandardAccountTeamMember sync = new SyncStandardAccountTeamMember();
        database.executeBatch(sync);
        System.assertEquals(atm.TeamMemberRole = 'Navigation', 'Navigation');

        cat.IsDeleted__c = true;
        cat.Team_Member_Role__c = 'Surgical';
        update cat;
        SyncStandardAccountTeamMember sync1 = new SyncStandardAccountTeamMember();
        database.executeBatch(sync1); 
        Test.stopTest();
        }
      }
    
    static testMethod void testSyncStandardAccountTeamMember1(){
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        Custom_Account_Team__c cat;
        Contact testCon;
        Account acc;
        System.runAs(usr) {
        acc  = TestUtils.createAccount(1, true).get(0);
        testCon = TestUtils.createCOMMContact(acc.id, 'TestContact', true); 
        cat = TestUtils.createCustomAccountTeam(acc.Id,testCon.id,usr.Id,'Navigation', true);
        Account acc1 = TestUtils.createAccount(1, false).get(0);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
        acc1.RecordTypeId = rtByName.getRecordTypeId();
        insert acc1;
        Custom_Account_Team__c cat1 = new Custom_Account_Team__c();
        cat1.Account__c = acc1.Id;
        cat1.User__c = usr.Id;
        cat1.Team_Member_Role__c = 'Navigation';
        insert cat1;
 
        Test.startTest();
        SyncStandardAccountTeamMember sync = new SyncStandardAccountTeamMember();
        database.executeBatch(sync);        
        Test.stopTest();
        }
        //update ref S-414651 starts
        List<AccountContactRelation> strykerCon = [SELECT ID 
                                              FROM AccountContactRelation 
                                              WHERE Contact.id = :testCon.id 
                                              AND Account.id = :acc.id];
        //update ref S-414651 ends
        //List<Stryker_Contact__c> strykerCon = [SELECT ID 
        //                                     FROM Stryker_Contact__c 
        //                                    WHERE Internal_Contact__r.id = :testCon.id 
        //                                      AND Customer_Account__r.id = :acc.id];
    system.assert(strykerCon.size() > 0);
    }
    //*******************************************************************COMM ****************************************************//
    //Name             :     testOppShareRecordsCreation Method
    //Description      :     Test Method created to test Opportunity Share Records Creation
    //Created by       :     Meghna Vijay T-484598 
    //Created Date     :     March 18, 2016
    
    static testMethod void testOppShareRecordsCreation() {
    integer i = 0;
    User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
    Account acc = TestUtils.createAccount(1, true).get(0);
    Contact testCon = TestUtils.createCOMMContact(acc.id, 'TestContact', true);
    Custom_Account_Team__c cat = TestUtils.createCustomAccountTeam(acc.Id, testCon.id, usr.Id, 'Navigation', true);
    Pricebook2 priceBook = TestUtils.createPriceBook(1,'test',false).get(0);
    priceBook.Name = 'COMM Price Book';
    insert priceBook;
    for (Opportunity opp: TestUtils.createOpportunity(5, acc.id, false)) {
      if (i == 0) {
        opp.RecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().
                           get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeID();
      } else if (i == 1) {
          opp.RecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().
                             get(Constants.COMM_OPPTY_RTYPE2).getRecordTypeID();
        } else if (i == 2) {
            opp.RecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().
                               get(Constants.COMM_OPPTY_RTYPE5).getRecordTypeID();
          } else if (i == 3) {
              opp.RecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().
                                 get(Constants.COMM_OPPTY_RTYPE4).getRecordTypeID();
            } 
        i++;
        opportunityList.add(opp);
    }
    insert opportunityList;
    Test.startTest();
    SyncStandardAccountTeamMember sync = new SyncStandardAccountTeamMember();
    database.executeBatch(sync);
    Test.stopTest();
    List < OpportunityShare > oppShareList = new List < OpportunityShare > ();
    oppShareList = [Select Id from OpportunityShare];
    system.assertNotEquals(0, oppShareList.size());
  }
    
    //******************************************************* COMM ***************************************************************//
    
}