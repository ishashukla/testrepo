/*
 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
*/
@istest
public class CasePagingControllerTest {


  //-----------------------------------------------------------------------------------------------
  // Method for test case button functionality
  //-----------------------------------------------------------------------------------------------
   static testMethod void nextButtonTest() {
    String caseNum;
    String caseOwnerId;
    //Profile p = [SELECT Id FROM Profile WHERE Name='Endo - Tech Support'];
    Profile p = TestUtils.fetchProfile('Endo - Tech Support');
    //User u = [select Id, Name,  IsActive, usertype, UserName, ProfileId from User where IsActive = true and ProfileId = :p.id LIMIT 1];
    List<User> userList=TestUtils.createUser(1,p.Name,false);
    userList.get(0).IsActive=true;
    insert userList;
    System.debug ('Here is the user id ' + userList[0].id);
    
    Account acc = new Account(Name = 'Test Account');
    insert acc;
    
    Contact con = new Contact(FirstName = 'TestFName',
                               LastName = 'TestLName',
                                  Email = 'test@test.com',
                              AccountId = acc.Id);
    insert con;
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
    test.startTest();
      List<Case> lstCase = new List<Case>();
      Case cs1 = new Case(Subject = 'Test Subject01',
                        ContactId = con.Id, 
              Oracle_Order_Ref__c = '1234',
                           Status = 'Open',
           Support_Case_Reason__c = 'Other',
                           Origin = 'Email',
                          OwnerId = userList[0].Id);
                          
      //Modified By Chandra Shekhar Sharma to add record types on case Object
  	cs1.recordTypeId = rtByName.getRecordTypeId();
  	
      //insert cs1;                    
      //cs1.OwnerId = userList[0].Id;
      lstCase.add(cs1);
      System.debug('CaseNumber 1: ' + cs1.CaseNumber);
      System.debug('C1: ' + cs1);

      Case cs2 = new Case(Subject = 'Test Subject02',
                        ContactId = con.Id, 
              Oracle_Order_Ref__c = '1234',
                           Status = 'Open',
           Support_Case_Reason__c = 'Other',
                           Origin = 'Email',
                          OwnerId = userList[0].Id);
                          
      //Modified By Chandra Shekhar Sharma to add record types on case Object
  	cs2.recordTypeId = rtByName.getRecordTypeId();
  	
      //insert cs2;                    
      //cs2.OwnerId = userList[0].Id;
      lstCase.add(cs2);
      System.debug('CaseNumber 2: ' + cs2.CaseNumber);
      System.debug('C2: ' + cs2);

      Case cs3 = new Case(Subject = 'Test Subject03', 
                        ContactId = con.Id, 
              Oracle_Order_Ref__c = '1234',
                           Status = 'Open',
           Support_Case_Reason__c = 'Other',
                           Origin = 'Email',
                          OwnerId = userList[0].Id);
                          
      //Modified By Chandra Shekhar Sharma to add record types on case Object
  	cs3.recordTypeId = rtByName.getRecordTypeId();
  	
    //  insert cs3;                    
   //   cs3.OwnerId = userList.get(0).Id;
      
     
      lstCase.add(cs3);
      insert lstCase;
      
      System.debug('CaseNumber 1: ' + cs1.CaseNumber);
      System.debug('ListC1: ' + cs1);
      System.debug('CaseNumber 2: ' + cs2.CaseNumber);
      System.debug('ListC2: ' + cs2);
      System.debug('CaseNumber 3: ' + cs3.CaseNumber);
      System.debug('ListC3: ' + cs3);
      

      //Select Next My open Case
      Case CaseSelectNum = [SELECT Id, CaseNumber, OwnerId, Subject, Status FROM Case WHERE Id = :cs2.Id and Subject = :cs2.Subject LIMIT 1];
      System.debug('CaseSelectNum : ' + CaseSelectNum);
      
      PageReference pg1 = new PageReference('/' + CaseSelectNum.Id);
      Test.setCurrentPage(pg1);
      ApexPages.StandardController sc = new ApexPages.standardController(CaseSelectNum);
      CasePagingController sic = new CasePagingController(sc);
      caseNum = CaseSelectNum.CaseNumber;
      //owner is changed to a sales support
      caseOwnerId = CaseSelectNum.OwnerId;

    test.stopTest();
       
       System.debug('casenum : ' + caseNum );
       System.debug('CaseSelectNum : ' + CaseSelectNum); 
       List<Case> ListCases = [SELECT Id, CaseNumber, OwnerId, Subject FROM Case];
       System.debug('ListCases : ' + ListCases );
       Case myNextCaseList = [SELECT Id, CaseNumber, OwnerId, Subject FROM Case WHERE CaseNumber > :caseNum  AND OwnerId = :caseOwnerId   AND (NOT Status LIKE 'Closed%') ORDER BY CaseNumber ASC LIMIT 1];
       System.debug('next case ' + myNextCaseList);
       PageReference pg2 = new PageReference('/' + myNextCaseList.Id);
       Test.setCurrentPage(pg2);
      
       System.assert(myNextCaseList.Subject == 'Test Subject03','No Next Button');
    
    
    
  }
  
  static testMethod void prevButtonTest() {
    String caseNum;
    String caseOwnerId;
    //Profile p = [SELECT Id FROM Profile WHERE Name='Endo - Tech Support'];
    Profile p = TestUtils.fetchProfile('Endo - Tech Support');
    //User u = [select Id, Name,  IsActive, usertype, UserName, ProfileId from User where IsActive = true and ProfileId = :p.id LIMIT 1];
    List<User> userList=TestUtils.createUser(1,p.Name,false);
    userList.get(0).IsActive=true;
    insert userList;
    System.debug ('Here is the user id ' + userList[0].id);
    Account acc = new Account(Name = 'Test Account');
    insert acc;
    
    Contact con = new Contact(FirstName = 'TestFName',
                               LastName = 'TestLName',
                                  Email = 'test@test.com',
                              AccountId = acc.Id);
    insert con;  
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
    test.startTest();
      List<Case> lstCase = new List<Case>();
      Case cs1 = new Case(Subject = 'Test Subject01',
                        ContactId = con.Id, 
              Oracle_Order_Ref__c = '1234',
                           Status = 'Open',
           Support_Case_Reason__c = 'Other',
                           Origin = 'Email',
                          OwnerId = userList[0].Id);
                          
      //Modified By Chandra Shekhar Sharma to add record types on case Object
  	cs1.recordTypeId = rtByName.getRecordTypeId();
  	
      //insert cs1;                    
      //cs1.OwnerId = userList[0].Id;
      lstCase.add(cs1);
      System.debug('CaseNumber 1: ' + cs1.CaseNumber);
      System.debug('C1: ' + cs1);

      Case cs2 = new Case(Subject = 'Test Subject02',
                        ContactId = con.Id, 
              Oracle_Order_Ref__c = '1234',
                           Status = 'Open',
           Support_Case_Reason__c = 'Other',
                           Origin = 'Email',
                          OwnerId = userList[0].Id);
                          
      //Modified By Chandra Shekhar Sharma to add record types on case Object
  	cs2.recordTypeId = rtByName.getRecordTypeId();
  	
      //insert cs2;                    
      //cs2.OwnerId = userList[0].Id;
      lstCase.add(cs2);
      System.debug('CaseNumber 2: ' + cs2.CaseNumber);
      System.debug('C2: ' + cs2);

      Case cs3 = new Case(Subject = 'Test Subject03', 
                        ContactId = con.Id, 
              Oracle_Order_Ref__c = '1234',
                           Status = 'Open',
           Support_Case_Reason__c = 'Other',
                           Origin = 'Email',
                          OwnerId = userList[0].Id);
                          
      //Modified By Chandra Shekhar Sharma to add record types on case Object
  	cs3.recordTypeId = rtByName.getRecordTypeId();
  	
      //insert cs3;                    
      //cs3.OwnerId = userList[0].Id;
      lstCase.add(cs3);
      
      insert lstCase;
      System.debug('CaseNumber 3: ' + cs3.CaseNumber);
      System.debug('C3: ' + cs3);

      //Select Next My open Case
      Case CaseSelectNum = [SELECT Id, CaseNumber, OwnerId, Subject, Status FROM Case WHERE Id = :cs2.Id and Subject = :cs2.Subject LIMIT 1];
      System.debug('CaseSelectNum : ' + CaseSelectNum);
      
      PageReference pg1 = new PageReference('/' + CaseSelectNum.Id);
      Test.setCurrentPage(pg1);
      ApexPages.StandardController sc = new ApexPages.standardController(CaseSelectNum);
      CasePagingController sic = new CasePagingController(sc);
      caseNum = CaseSelectNum.CaseNumber;
      caseOwnerId = CaseSelectNum.OwnerId;
    test.stopTest();
       
       System.debug('casenum : ' + caseNum );
       System.debug('CaseSelectNum : ' + CaseSelectNum); 
       List<Case> ListCases = [SELECT Id, CaseNumber, OwnerId, Subject FROM Case];
       System.debug('ListCases : ' + ListCases );
       Case myPrevCaseList= [SELECT Id, CaseNumber, OwnerId, Subject FROM Case WHERE CaseNumber < :caseNum  AND OwnerId = :caseOwnerId   AND (NOT Status LIKE 'Closed%') ORDER BY CaseNumber DESC LIMIT 1];
       System.debug('next case ' + myPrevCaseList);
       PageReference pg2 = new PageReference('/' + myPrevCaseList.Id);
       Test.setCurrentPage(pg2);
      
       System.assert(myPrevCaseList.Subject == 'Test Subject01','No Prev Button');
    
    
    
  }
  
  static testMethod void nextSkipClosedTest() {
    String caseNum;
    String caseOwnerId;
    //Profile p = [SELECT Id FROM Profile WHERE Name='Endo - Tech Support'];
    Profile p = TestUtils.fetchProfile('Endo - Tech Support');
    //User u = [select Id, Name,  IsActive, usertype, UserName, ProfileId from User where IsActive = true and ProfileId = :p.id LIMIT 1];
    List<User> userList=TestUtils.createUser(1,p.Name,false);
    userList.get(0).IsActive=true;
    insert userList;
    
    System.debug ('Here is the user id ' + userList[0].id);
    
    Account acc = new Account(Name = 'Test Account');
    insert acc;
    
    Contact con = new Contact(FirstName = 'TestFName',
                               LastName = 'TestLName',
                                  Email = 'test@test.com',
                              AccountId = acc.Id);
    insert con;  
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
	
    List<Case> lstCase = new List<Case>();
    List<Case> lstInsCase = new List<Case>();
    
    Case cs1 = new Case(Subject = 'Test Subject01',
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Open',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs1.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs1;        
    lstInsCase.add(cs1);            
    cs1.OwnerId = userList[0].Id;
    lstCase.add(cs1);
    System.debug('CaseNumber 1: ' + cs1.CaseNumber);
    System.debug('C1: ' + cs1);

    Case cs2 = new Case(Subject = 'Test Subject02',
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Open',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
                        
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs2.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs2;
    lstInsCase.add(cs2);                    
    cs2.OwnerId = userList[0].Id;
    lstCase.add(cs2);
    System.debug('CaseNumber 2: ' + cs2.CaseNumber);
    System.debug('C2: ' + cs2);

    Case cs3 = new Case(Subject = 'Test Subject03', 
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Closed',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
                        
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs3.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs3;                    
    lstInsCase.add(cs3);
    cs3.OwnerId = userList[0].Id;
    lstCase.add(cs3);
    System.debug('CaseNumber 3: ' + cs3.CaseNumber);
    System.debug('C3: ' + cs3);
    
    Case cs4 = new Case(Subject = 'Test Subject04', 
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Open',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
                        
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs4.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs4;                    
    lstInsCase.add(cs4);
    cs4.OwnerId = userList[0].Id;
    lstCase.add(cs4);
    
    insert lstInsCase;
    update lstCase;
    System.debug('CaseNumber 4: ' + cs4.CaseNumber);
    System.debug('C4: ' + cs4);

    //Select Next My open Case
    Case CaseSelectNum = [SELECT Id, CaseNumber, OwnerId, Subject, Status FROM Case WHERE Id = :cs2.Id and Subject = :cs2.Subject LIMIT 1];
    System.debug('CaseSelectNum : ' + CaseSelectNum);
    
    test.startTest();
    PageReference pg1 = new PageReference('/' + CaseSelectNum.Id);
    Test.setCurrentPage(pg1);
    ApexPages.StandardController sc = new ApexPages.standardController(CaseSelectNum);
    CasePagingController sic = new CasePagingController(sc);
    caseNum = CaseSelectNum.CaseNumber;
    caseOwnerId = CaseSelectNum.OwnerId;

    test.stopTest();
       
       System.debug('casenum : ' + caseNum );
       System.debug('CaseSelectNum : ' + CaseSelectNum); 
       List<Case> ListCases = [SELECT Id, CaseNumber, OwnerId, Subject FROM Case];
       System.debug('ListCases : ' + ListCases );
       Case myNextCaseList = [SELECT Id, CaseNumber, OwnerId, Subject FROM Case WHERE CaseNumber > :caseNum  AND OwnerId = :caseOwnerId   AND (NOT Status LIKE 'Closed%') ORDER BY CaseNumber ASC LIMIT 1];
       System.debug('next case ' + myNextCaseList);
       PageReference pg2 = new PageReference('/' + myNextCaseList.Id);
       Test.setCurrentPage(pg2);
      
       System.assert(myNextCaseList.Subject == 'Test Subject04','Next closed page not skipped');
    
    
    
  }
  
  static testMethod void prevSkipClosedTest() {
    String caseNum;
    String caseOwnerId;
    //Profile p = [SELECT Id FROM Profile WHERE Name='Endo - Tech Support'];
    Profile p = TestUtils.fetchProfile('Endo - Tech Support');
    //User u = [select Id, Name,  IsActive, usertype, UserName, ProfileId from User where IsActive = true and ProfileId = :p.id LIMIT 1];
    List<User> userList=TestUtils.createUser(1,p.Name,false);
    userList.get(0).IsActive=true;
    insert userList;
    
    System.debug ('Here is the user id ' + userList[0].id);
    
    Account acc = new Account(Name = 'Test Account');
    insert acc;
    
    Contact con = new Contact(FirstName = 'TestFName',
                               LastName = 'TestLName',
                                  Email = 'test@test.com',
                              AccountId = acc.Id);
    insert con;  
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
	
    List<Case> lstInsCase = new List<Case>();
    List<Case> lstCase = new List<Case>();
    Case cs1 = new Case(Subject = 'Test Subject01',
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Open',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
                        
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs1.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs1;                    
    lstInsCase.add(cs1);
    cs1.OwnerId = userList[0].Id;
    lstCase.add(cs1);
    System.debug('CaseNumber 1: ' + cs1.CaseNumber);
    System.debug('C1: ' + cs1);

    Case cs2 = new Case(Subject = 'Test Subject02',
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Closed',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
                        
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs2.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs2;                    
    lstInsCase.add(cs2);
    cs2.OwnerId = userList[0].Id;
    lstCase.add(cs2);
    System.debug('CaseNumber 2: ' + cs2.CaseNumber);
    System.debug('C2: ' + cs2);

    Case cs3 = new Case(Subject = 'Test Subject03', 
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Open',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
                        
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs3.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs3;                    
    lstInsCase.add(cs3);
    cs3.OwnerId = userList[0].Id;
    lstCase.add(cs3);
    System.debug('CaseNumber 3: ' + cs3.CaseNumber);
    System.debug('C3: ' + cs3);
    
    Case cs4 = new Case(Subject = 'Test Subject04', 
                      ContactId = con.Id, 
            Oracle_Order_Ref__c = '1234',
                         Status = 'Open',
         Support_Case_Reason__c = 'Other',
                         Origin = 'Email',
                        OwnerId = userList[0].Id);
                        
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	cs4.recordTypeId = rtByName.getRecordTypeId();
	
    //insert cs4;                    
    lstInsCase.add(cs4);
    cs4.OwnerId = userList[0].Id;
    lstCase.add(cs4);
    insert lstInsCase;
    update lstCase;
    System.debug('CaseNumber 4: ' + cs4.CaseNumber);
    System.debug('C4: ' + cs4);

    //Select Next My open Case
    Case CaseSelectNum = [SELECT Id, CaseNumber, OwnerId, Subject, Status FROM Case WHERE Id = :cs3.Id and Subject = :cs3.Subject LIMIT 1];
    System.debug('CaseSelectNum : ' + CaseSelectNum);
    
    test.startTest();
    PageReference pg1 = new PageReference('/' + CaseSelectNum.Id);
    Test.setCurrentPage(pg1);
    ApexPages.StandardController sc = new ApexPages.standardController(CaseSelectNum);
    CasePagingController sic = new CasePagingController(sc);
    caseNum = CaseSelectNum.CaseNumber;
    caseOwnerId = CaseSelectNum.OwnerId;

    test.stopTest();
       
       System.debug('casenum : ' + caseNum );
       System.debug('CaseSelectNum : ' + CaseSelectNum); 
       List<Case> ListCases = [SELECT Id, CaseNumber, OwnerId, Subject FROM Case];
       System.debug('ListCases : ' + ListCases );
       Case myPrevCaseList= [SELECT Id, CaseNumber, OwnerId, Subject FROM Case WHERE CaseNumber < :caseNum  AND OwnerId = :caseOwnerId   AND (NOT Status LIKE 'Closed%') ORDER BY CaseNumber DESC LIMIT 1];
       System.debug('next case ' + myPrevCaseList);
       PageReference pg2 = new PageReference('/' + myPrevCaseList.Id);
       Test.setCurrentPage(pg2);
      
       System.assert(myPrevCaseList.Subject == 'Test Subject01','Previous closed page not skipped');
    
    
    
  }
  
}