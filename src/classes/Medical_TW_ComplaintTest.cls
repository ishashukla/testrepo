/*************************************************************************************************
Created By:    Rahul Aeran
Date:          April 23, 2016
Description:   Test class for Medical_TW_Complaint for Medical Division
**************************************************************************************************/
@isTest
private class Medical_TW_ComplaintTest {
  static testMethod void testMedical_TW_Complaint(){
    Test.startTest();
      Test.setMock(WebServiceMock.class, new WebServiceMockClass());
      Medical_TW_Complaint objMedicalComplaint = new Medical_TW_Complaint();
      Medical_TW_Complaint.ComplaintInfoICPort objComplainInfoICPort = new Medical_TW_Complaint.ComplaintInfoICPort();
      Medical_TW_Common.IntegrationHeaderType objIntegrationHeaderType = new Medical_TW_Common.IntegrationHeaderType();
      Medical_TW_ComplaintInfo.ComplaintInfo objComplaintInfo = new Medical_TW_ComplaintInfo.ComplaintInfo();
      Medical_TW_ComplaintInfo.ResultMessage objResultMessage = objComplainInfoICPort.CreateComplaint(objIntegrationHeaderType,objComplaintInfo);
    Test.stopTest();
  }
}