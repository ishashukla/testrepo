// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncCycleCountTransaction_SCHTest.
//
// 28 Oct 2016, Isha Shukla
@isTest
private class COMM_SyncCycleCountTransaction_SCHTest {
    static List<SVMXC__Stock_Adjustment__c> recordList;
    @isTest static void testCycleCount() {
        createData();
        Test.startTest();
        COMM_SyncCycleCountTransaction_SCH obj =new COMM_SyncCycleCountTransaction_SCH();
        obj.execute(null);
        Test.stopTest();
        System.assertEquals('Success', [SELECT Integration_Status__c FROM SVMXC__Stock_Adjustment__c WHERE Id = :recordList[0].Id].Integration_Status__c);
    }
    public static void createData() {
        SVMXC__Installed_Product__c asset = new SVMXC__Installed_Product__c(External_ID__c = '123',SVMXC__Serial_Lot_Number__c='12',Quantity__c = 1);
        insert asset;
        SVMXC__Site__c slocation = new SVMXC__Site__c(Location_ID__c = '1234',SVMXC__Stocking_Location__c = true);
        insert slocation;
        recordList = new List<SVMXC__Stock_Adjustment__c>();
        for(Integer i = 0; i < 4; i++){
            SVMXC__Stock_Adjustment__c stock = new SVMXC__Stock_Adjustment__c(
                Integration_Status__c = 'New',
                SVMXC__Location__c = slocation.Id,
                Asset__c = asset.Id
            );
            recordList.add(stock);
        }
        insert recordList;
        COMMRecordCount__c customSetting = new COMMRecordCount__c(Cycle_Count__c = 2);
        insert customSetting;
    }
}