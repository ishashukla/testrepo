public with sharing class GlobalTestClassUtility {
    public GlobalTestClassUtility() {
        
    }
    public static List<User> genUsers(Integer numUsers, List<ID> profiles){
        List<User> testUsers = new List<User>();
        profiles = (profiles == null) ? new List<ID>() : profiles;
        if(profiles.size() == 0){
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            profiles.add(p.id);
        }

        for(Integer x = 0 ; x < numUsers ; x++){

            testUsers.add( new User(
                Alias = 'standt' + String.valueOf(x), Email = String.valueOf(x) + 'standarduser@testorg.com', 
                Division = 'Surgical;IVS', EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US', ProfileId = profiles[x], TimeZoneSidKey =' America/Los_Angeles', 
                UserName = String.valueOf(randomGenString(8)) + 'standarduser@testorg.com') );
        }
        return testUsers;
    }

    public static List<Forecast_Year__c> genForecastYears(Integer numYears, String startingYear, User forecastUser){
        List<Forecast_Year__c> fYears = new List<Forecast_Year__c>();
        for(Integer x = Integer.valueOf(startingYear) ; x <= (Integer.valueOf(startingYear) ) ; x++){
            fYears.add(new Forecast_Year__c(Division__c = 'IVS', Year__c = String.valueOf(x), Yearly_Quota__c = 2000000, User2__c = forecastUser.id) );
        }
        return fYears;
    }
    private static String randomGenString(Integer len){
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    public static List<Product2> genProducts(Integer numProducts){
        List<Product2> pl2 = new List<Product2>();
        for(Integer x = 0 ; x <= numProducts ; x++){
            pl2.add(new Product2(Name = String.valueOf(randomGenString(8))));
        }
        return pl2;

    }

}