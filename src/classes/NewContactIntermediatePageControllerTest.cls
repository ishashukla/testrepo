// (c) 2015 Appirio, Inc.
//
// Class Name: NewContactIntermediatePageControllerTest 
// Description: Test Class for NewContactIntermediatePageController class.
// 
// April 14 2016, Isha Shukla  Original 
//
@isTest
private class NewContactIntermediatePageControllerTest {
    Static List<Contact> contactList;
    @isTest static void testControllerWithAccountId() {
        createData();
        PageReference pageRef = Page.NewContactIntermediatePage;
        Test.setCurrentPage(pageRef);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        Id idrecordType = rtByName.getRecordTypeId();
        System.currentPageReference().getParameters().put('RecordType',String.valueOf(idrecordType));
        ApexPages.StandardController sc = new ApexPages.StandardController(contactList[0]);
        NewContactIntermediatePageController controller = new NewContactIntermediatePageController(sc);
        controller.con = contactList[0];
        controller.redirectToAccount();
        controller.redirectPage();
        
    }
    @isTest static void testControllerWithoutAccountId() {
        createData();
        PageReference pageRef = Page.NewContactIntermediatePage;
        Test.setCurrentPage(pageRef);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        Id idrecordType = rtByName.getRecordTypeId();
        ApexPages.StandardController sc = new ApexPages.StandardController(contactList[0]);
        NewContactIntermediatePageController controller = new NewContactIntermediatePageController(sc);
        controller.redirectToAccount();
        controller.redirectPage();
    }
    public static void createData() {
        List<Account> accountList = TestUtils.createAccount(1, True);
        contactList = TestUtils.createContact(2, accountList[0].Id,True);
    }
    
}