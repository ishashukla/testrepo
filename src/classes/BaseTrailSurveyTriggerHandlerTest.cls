// (c) 2015 Appirio, Inc.
//
// Class Name: BaseTrailSurveyTriggerHandlerTest
// Description: Test Class for BaseTrailSurveyTriggerHandler   
// April 22, 2016    Meena Shringi    Original
// July 2 , 2016     Isha Shukla     Modified(T-516161)
@isTest 
public with sharing class BaseTrailSurveyTriggerHandlerTest {
    @isTest static void testBasTrailSurveyController() {
        User usr = TestUtils.createUSer(1,'System Administrator',false).get(0);
        usr.Division = 'NSE';
        insert usr;
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
        Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
        Id recordTypeRep = rtByNameRep.getRecordTypeId();
        system.runAs(usr) {
            Test.startTest();
            Account acc = TestUtils.createAccount(1,true).get(0);
            TestUtils.createCommConstantSetting();
            Opportunity opp = TestUtils.createOpportunity(1, acc.Id, false).get(0);
            opp.CloseDate = System.today();
            opp.Business_Unit__c = 'NSE';
            opp.recordTypeId = recordTypeInstrument;
            insert opp;
            Forecast_Year__c forecastYear = TestUtils.createForcastYear(1, usr.Id, recordTypeRep,false).get(0);
            forecastYear.Business_Unit__c = 'NSE';
            forecastYear.Year__c = String.valueOf(System.today().year());
            insert forecastYear;     
            List<Base_Trail_Survey__c> surveyList =  TestUtils.createBaseTrailSurvey(1,opp.Id,true);
            System.debug('surveyList===='+surveyList);
            Database.DeleteResult[] result = Database.delete(surveyList, false);
            System.assertEquals(result[0].getErrors() != null,True);
            Test.stopTest();
        } 
    }
    
}