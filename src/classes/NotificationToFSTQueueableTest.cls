@isTest
private class NotificationToFSTQueueableTest {
    static OrderItem oi;
    @isTest(SeeAllData=true)
    static void testQueue() {
        createData();
        Test.startTest();
        NotificationToFSTQueueable queueObj = new NotificationToFSTQueueable(new Set<Id>{oi.Id});
        ID jobID = System.enqueueJob(queueObj);
        Test.stopTest();
    }
    private static void createData() {
        Id shipmentRT = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
        if(!(RTMap__c.getInstance(shipmentRT).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = shipmentRT,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='Shipment');
            insert rtMap;
        }
        User userRecord = TestUtils.createUser(1, 'Field Service Technician - COMM US', false).get(0);
        userRecord.Email = 'test@test.com';
        insert userRecord;
        User userRecordManager = TestUtils.createUser(1, 'Field Service Technician Manager - COMM US', false).get(0);
        userRecordManager.Email = 'test@test.com';
        insert userRecordManager;
        Id commBPAccountRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Account acc = TestUtils.createCommAccount(1, false).get(0);
        acc.Name = 'testingQueue1243';
        acc.RecordTypeId = commBPAccountRecordTypeId;
        acc.Endo_Active__c = true;
        insert acc;
        Id pricebookId = Test.getStandardPricebookId();
        Product2 prd1 = new Product2(Name = 'Test Product Entry 1', Description = 'Test Product Entry 1', isActive = true);
        insert prd1;
        PricebookEntry pe = new PricebookEntry(UnitPrice = 1, Product2Id = prd1.id, Pricebook2Id = pricebookId, isActive = true);
        insert pe;
        Order orderRecord = TestUtils.createOrder(1, acc.Id, 'Entered', false).get(0);
        orderRecord.PriceBook2Id = pricebookId;
        insert orderRecord;
        oi = new OrderItem(OrderId = orderRecord.Id, Quantity = decimal.valueof('1'), UnitPrice = 1,PricebookEntryId = pe.id);
        oi.Status__c='Entered';
        insert oi;
        SVMXC__Site__c location = new SVMXC__Site__c(Name='testClassLocation',Location_ID__c='testClassLocation');
        insert location;
        SVMXC__RMA_Shipment_Order__c partOrder;
        SVMXC__RMA_Shipment_Order__c partOrder2;
        System.runAs(userRecord) {
            partOrder = new SVMXC__RMA_Shipment_Order__c(
                SVMXC__Destination_Location__c = location.Id,
                RecordTypeId = shipmentRT
            ); 
            insert partOrder;
        }
        System.runAs(userRecordManager) {
            partOrder2 = new SVMXC__RMA_Shipment_Order__c(
                SVMXC__Destination_Location__c = location.Id,
                RecordTypeId = shipmentRT
            ); 
            insert partOrder2;
        }
        List<SVMXC__RMA_Shipment_Line__c> partLineList = new List<SVMXC__RMA_Shipment_Line__c>();
        for(Integer i=0;i<3;i++) {
            SVMXC__RMA_Shipment_Line__c partLine = new SVMXC__RMA_Shipment_Line__c(
                SVMXC__RMA_Shipment_Order__c = partOrder.Id,
                Order_Product__c=oi.Id);  
            partLineList.add(partLine);
        }
        partLineList[1].SVMXC__RMA_Shipment_Order__c = partOrder2.Id;
        insert partLineList;
    }
}