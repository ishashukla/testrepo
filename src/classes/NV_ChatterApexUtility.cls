public with sharing class NV_ChatterApexUtility {
	
	public static ConnectApi.FeedItemInput getTextFeedItemInput(Id contentId, String textContent){
		ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
		input.subjectId = contentId;
		
		ConnectApi.MessageBodyInput body = new ConnectApi.MessageBodyInput();
    	body.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        body.messageSegments.add(getTextSegmentInput(textContent));

	    input.body = body;
	    return input;
	}

	public static ConnectApi.FeedItemInput getTextFeedItemInputWithMentionsAtEnd(Id contentId, String textContent, List<Id> userToMentionIds){
		ConnectApi.FeedItemInput input = getTextFeedItemInput(contentId, textContent);
		for(Id userToMentionId: userToMentionIds){
			input.body.messageSegments.add(getTextSegmentInput(' '));
			input.body.messageSegments.add(getMentionSegment(userToMentionId));
		}
		return input;
	}

	public static ConnectApi.TextSegmentInput getTextSegmentInput(String text){
		ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text = text;
        return textSegment;
	}

	public static ConnectApi.MentionSegmentInput getMentionSegment(Id userToMentionId){
		ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
   		mentionSegment.id = userToMentionId;
   		return mentionSegment;
	}



}