/**================================================================      
 * Appirio, Inc
 * Name: PopulateBusinessUnitController
 * Description: Test class for PopulateBusinessUnitController(T-538445), class to populate Business unit on Value Add Activity record.
 * Created Date: [09/27/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/
@isTest
public class PopulateBusinessUnitControllerTest {
  public static Id accountInstrumentsRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type').getRecordTypeId();
  public static Id opportunityInstrumentsRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type').getRecordTypeId();
  public static Id contactInstrumentsRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact').getRecordTypeId();
  public static List<User> salesRepUserList;
  static User adminUser;
  static Account account;
  static testMethod void testPopulateBU(){
    Test.startTest();
      insertUser();
    Test.stopTest();
    
    //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=salesRepUserList[1].Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
    //insert terr;
    System.runAs(adminUser) {
      Mancode__c mancode = TestUtils.createMancode(1, salesRepUserList[0].Id, null, false).get(0);
      mancode.Name = '000000';
      mancode.Type__c = 'Sales Rep';
      //mancode.Territory__c = terr.Id;
      insert mancode;
      account = TestUtils.createAccount(1, false).get(0);
      account.RecordTypeId = accountInstrumentsRecTypeId;
      insert account;
      AccountTeamMember atm = new AccountTeamMember(UserId = salesRepUserList[0].Id, TeamMemberRole = 'NSE', AccountId = account.Id);
      insert atm;
    }
    Contact contact = TestUtils.createContact(1, account.Id, false).get(0);
    contact.RecordTypeId = contactInstrumentsRecTypeId;
    contact.Allow_Create__c = true;
    Opportunity opportunity = TestUtils.createOpportunity(1, account.Id, false).get(0);
    opportunity.RecordTypeId = opportunityInstrumentsRecTypeId;
    opportunity.Business_Unit__c = 'NSE';
    system.RunAs(salesRepUserList[0]){
        insert contact;
        insert opportunity;
          Value_Add_Activities__c value = new Value_Add_Activities__c();
          value.OwnerId = salesRepUserList[0].Id;
          value.Account__c = account.Id;
          value.Business_Unit__c = 'NSE';
          insert value;
          Value_Add_Activities__c valueCon = new Value_Add_Activities__c();
        valueCon.OwnerId = salesRepUserList[0].Id;
        valueCon.Contact__c = contact.Id;
        valueCon.Business_Unit__c = 'NSE';
        insert valueCon;
        Value_Add_Activities__c valueOpp = new Value_Add_Activities__c();
        valueOpp.OwnerId = salesRepUserList[0].Id;
        valueOpp.Opportunity__c = opportunity.Id;
        valueOpp.Business_Unit__c = 'NSE';
        insert valueOpp;
          PageReference pageRef = Page.PopulateBusinessUnitIntermediatePage;
        Test.setCurrentPage(pageRef);
          System.currentPageReference().getParameters().put('ownerId', String.valueOf(value.OwnerId));
          System.currentPageReference().getParameters().put('accountId', String.valueOf(account.Id));
          System.currentPageReference().getParameters().put('contactId', String.valueOf(contact.Id));
          System.currentPageReference().getParameters().put('oppId', String.valueOf(opportunity.Id));
          PopulateBusinessUnitController pop = new PopulateBusinessUnitController();
          pop.getBUToPopulate();
          system.assertEquals(value.Business_Unit__c, 'NSE');
          system.assertEquals(valueCon.Business_Unit__c, 'NSE');
          system.assertEquals(valueOpp.Business_Unit__c, 'NSE');
     }
    }
     @future
    public static void insertuser() {
      adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
      insert adminUser;
      salesRepUserList = TestUtils.createUser(2, 'Instruments Sales User', false); 
      salesRepUserList[0].Division = 'NSE';
      salesRepUserList[1].Division = 'NSE';
      salesRepUserList[0].Title = 'Sales Rep';
      salesRepUserList[1].Title = 'Sales Rep';
      insert salesRepUserList;
  }
}