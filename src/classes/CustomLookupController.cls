global with sharing class CustomLookupController {
  public Contact newContact {get;set;}
  public ID UserID {get;set;}
  public static boolean showPagination{get;set;}
  public CustomLookupController(ApexPages.StandardController controller) {
    newContact = new Contact();
    UserID = UserInfo.getUserId();
    showPagination = false;
  }
  webService static List<sObject> find(String query){
    system.debug('your query: ' + query);
    List<sObject> so = Database.query(buildQuery(query));
    if(so.size()>0){
      showPagination = true; 
    }
    return so;
  }
  private static String buildQuery(String query){
    //query += ' limit 2000'; 
    return query;
  } 
  public void refreshPage(){
    showPagination = true; 
  }
}