/**================================================================      
 * Appirio, Inc
 * Name: CaseTriggerHandlerInterface
 * Description: Top level interface describing the methods available to all Stryker or division specific functionality
 * Created Date: 10 Aug 2016
 * Created By: Nick Whitney (Appirio)
 * 
 * Date Modified      Modified By      Description of the update
 * 
 ==================================================================*/

public interface CaseTriggerHandlerInterface{
    boolean IsActive();
    
    void OnBeforeInsert(List<Case> NewRecords);
    void OnAfterInsert(List<Case> NewRecords);
    void OnAfterInsertAsync(Set<ID> NewRecordIds);
    
    void OnBeforeUpdate(List<Case> NewRecords, Map<ID, Case> OldRecordMap);
    void OnAfterUpdate(List<Case> NewRecords, Map<ID, Case> OldRecordMap);
    void OnAfterUpdateAsync(Set<ID> UpdatedRecordIDs);
        
    void OnBeforeDelete(List<Case> RecordsToDelete, Map<ID, Case> OldRecordMap);
    void OnAfterDelete(List<Case> RecordsToDelete, Map<ID, Case> OldRecordMap);
    void OnAfterDeleteAsync(Set<ID> DeletedRecordIDs);
    
    void OnUndelete(List<Case> RestoredRecords);
}