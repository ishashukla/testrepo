/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  Oct 5th, 2015
 Purpose       :  Class having common methods for test data creation
*******************************************************************************/
@isTest
public without sharing class Medical_TestUtils {
    //****************************************************************************
  // Method to insert Groups
  //****************************************************************************
    public static Group createGroup(){
        Group grp = new Group(Name='Medical Test');
    insert grp;
    return grp;
  }


  //****************************************************************************
  // Method to insert Queues
  //****************************************************************************
  public static QueuesObject createQueue(){
    Group g1 = new Group(Name='Medical Test', type='Queue');
    insert g1;
    QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
    insert q1;
    return q1;
  }


  //****************************************************************************
  // Method to fetch profile
  //****************************************************************************
  public static Profile fetchProfile(String profileName){
    Profile pr;
    if(profileName == null || profileName == ''){
        pr = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'].get(0);
    }
    else{
        for(Profile p : [SELECT Id, Name FROM Profile WHERE Name = :profileName LIMIT 1]){
            pr = p;
        }
    }
    return pr;
  }

  //****************************************************************************
  // Method to create test user records
  //****************************************************************************
  public static String getRandomString() {
    String rs = String.valueOf(Math.random() * Datetime.now().getTime());
    return rs.substring(rs.length()-10, rs.length()-1);
  }

  public static List<User> createUser(Integer userCount, String profileName, Boolean isInsert ){
    Profile pr = fetchProfile(profileName);
    List<User> userList = new List<User>();
    User u;
    for(Integer i=0; i<userCount; i++){
        u = new User(ProfileId = pr.Id,
                     alias = 'usr' + i,
                     email = 'user' + i + '@test.com',
                   emailencodingkey = 'UTF-8',
                   lastname = 'Testing' + i + getRandomString(),
                   languagelocalekey = 'en_US',
                   localesidkey = 'en_US',
                   timezonesidkey = 'America/Los_Angeles',
                   username = 'user' + getRandomString() + '@test.com' + i);
        userList.add(u);
    }
    if(isInsert){
        insert userList;
    }
    return userList;
  }


  //****************************************************************************
  // Method to create test Account records
  //****************************************************************************
  public static List<Account> createAccount(Integer accCount, Boolean isInsert){
    List<Account> lstAccoount = new List<Account>();
    for(Integer i = 0; i < accCount; i++){
      Account acc = new Account(Name = 'Test Account ' + i,
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States');
      lstAccoount.add(acc);
    }
    if(isInsert){
      insert lstAccoount;
    }
    return lstAccoount;
  }

  //****************************************************************************
  // Method to create test Contact records
  //****************************************************************************
  public static List<Contact> createContact(Integer conCount, Id accountId, Boolean isInsert){
    List<Contact> lstContact = new List<Contact>();
    for(Integer i = 0; i < conCount; i++){
      Contact con = new Contact(FirstName = 'TestFName' + i,
                                LastName = 'TestLName' + i,Email = 'test@appirio.com',
                                AccountId = accountId);
      con.MailingStreet = 'Street';
      con.MailingCity = 'City';
      con.MailingState = 'Alabama';
      con.MailingCountry = 'United States';
      con.MailingPostalCode = '62234';
      lstContact.add(con);
    }
    if(isInsert){
      insert lstContact;
    }
    return lstContact;
  }

  //****************************************************************************
  // Method to Create Case
  //****************************************************************************
  public static List<Case> createCase(Integer accCount, Id accountId, Id contactId, Boolean isInsert){
    //Account acc = createAccount(1, true).get(0);
    List<Case> lstCase = new List<Case>();
    for(Integer i = 0; i < accCount; i++){
      Case cs = new Case(Subject = 'Test Subject', Oracle_Order_Ref__c = '1234',
                AccountId = accountId, ContactId = contactId, status='New');


      cs.Sub_Type__c = 'Shipping Error';
      cs.Origin = 'Phone';
      cs.Description = 'testDescription';
      lstCase.add(cs);
    }
    if(isInsert){
      insert lstCase;
    }
    return lstCase;
  }


  //****************************************************************************
  // Method to Create Asset
  //****************************************************************************
  public static List<Asset> createAsset(Integer count, Id accountId, Boolean isInsert){
    //Account acc = createAccount(1, true).get(0);
    List<Asset> lstAsset = new List<Asset>();
    for(Integer i = 0; i < count; i++){
      Asset assetObj = new Asset(Name='Test Asset',accountId = accountId);
      lstAsset.add(assetObj);
    }
    if(isInsert){
      insert lstAsset;
    }
    return lstAsset;
  }

  //****************************************************************************
  // Method to Create Case Asset Junction Object
  //****************************************************************************
  public static List<Case_Asset_Junction__c> createCaseAssetJunction(Integer count, Id caseId, Id assetId, Boolean isInsert){
    List<Case_Asset_Junction__c> lstCaseAsset = new List<Case_Asset_Junction__c>();
    for(Integer i = 0; i < count; i++){
      Case_Asset_Junction__c caseAssetObj = new Case_Asset_Junction__c(Case__c= caseId, Asset__c = assetId);
      lstCaseAsset.add(caseAssetObj);
    }
    if(isInsert){
      insert lstCaseAsset;
    }
    return lstCaseAsset;
  }



  //****************************************************************************
  // Method to Create Products
  //****************************************************************************
  public static Product2 createProduct(Boolean isInsert){
    Product2 prod = new Product2();
    prod.Name = 'test product';
    prod.Part_Number__c = '12345';
    if(isInsert){
        insert prod;
    }
    return prod;
  }

  //****************************************************************************
  // Method to Get standard Price Book.
  //****************************************************************************
  public static Pricebook2 createPricebook(){
    Pricebook2 priceBook = [SELECT Name FROM Pricebook2 WHERE isStandard = true FOR UPDATE].get(0);
    return pricebook;
  }


  /*
  @method      : create Price book
  @description : This method instantiates/inserts a list of price book records.
  @params      : Integer totalCount, String name, Boolean isInsert
  @returns     : List<Pricebook2> testPriceBooks
  */

  public static List<Pricebook2> createPriceBook(Integer totalCount, String name,Boolean isInsert) {
    List<Pricebook2> testPriceBooks = new List<Pricebook2>();
    for(Integer index = 0; index < totalCount;index++) {
      testPriceBooks.add(new Pricebook2(
        Name = name+index,
        isActive = true
      ));
    }
    if(isInsert) {
      insert testPriceBooks;
    }
    return testPriceBooks;
  }

  //****************************************************************************
  // Method to Create standard Price Book Entries
  //****************************************************************************
  public static PricebookEntry createPricebookEntry(){
    Product2 prod = createProduct(true);
    Pricebook2 priceBook = createPricebook();
    PricebookEntry pbe = new PricebookEntry();
    pbe.Pricebook2Id = priceBook.Id;
    pbe.Product2Id = prod.Id;
    pbe.IsActive = true;
    pbe.UnitPrice = 0.0;
    insert pbe;
    return pbe;
  }


  //****************************************************************************
  // Method to Create price book entry
  //****************************************************************************
  public static PricebookEntry createPricebookEntry(Id productId, Id priceBookId,Boolean isInsert){
    PricebookEntry pbe = new PricebookEntry();
    pbe.Pricebook2Id = priceBookId;
    pbe.Product2Id = productId;
    pbe.IsActive = true;
    pbe.UnitPrice = 0.0;
    if(isInsert)
      insert pbe;
    return pbe;
  }

  //****************************************************************************
  // Method to Create Orders.
  //****************************************************************************
  public static Order createOrder(){
    Account acc = createAccount(10,true).get(0);
    Contact con = createContact(10, acc.Id, true).get(0);
    Pricebook2 pb = createPricebook();
    Order ord = new Order();
    ord.Bill_To_Account__c = acc.Id;
    ord.Ship_To_Account__c = acc.Id;
        ord.PoNumber = '1212';
        ord.Type = 'CO Credit';
        ord.Order_Origin__c = 'Fax';
        ord.EffectiveDate = System.today();
        ord.AccountId = acc.Id;
    ord.Status = 'Draft';
    ord.Pricebook2Id = pb.Id;
    ord.Contact__c = con.Id;
    ord.Is_Scheduled_Order_Mail_Sent__c = false;
    insert ord;
    return ord;
  }

  //****************************************************************************
  // Method to Create Orders.
  //****************************************************************************
  public static Order createOrder(Id accountId, Id contactId, Id priceBookId,Boolean isInsert){

    Order ord = new Order();
    ord.Bill_To_Account__c = accountId;
    ord.Ship_To_Account__c = accountId;
    ord.PoNumber = '1212';
    ord.Type = 'CO Credit';
    ord.Order_Origin__c = 'Fax';
    ord.EffectiveDate = System.today();
    ord.AccountId = accountId;
    ord.Status = 'Draft';
    ord.Pricebook2Id = priceBookId;
    ord.Contact__c = contactId;
    ord.Is_Scheduled_Order_Mail_Sent__c = false;
    if(isInsert)
      insert ord;
    return ord;
  }

  //****************************************************************************
  // Method to Create Order Items.
  //****************************************************************************
  public static OrderItem createOrderItem(){
    PricebookEntry pbEntry = createPricebookEntry();
    Order ord = createOrder();
    OrderItem oi = new OrderItem();
    oi.Quantity = 1;
    oi.Description = 'testDescription';
    oi.Order_Line_Status__c = '531';
    oi.PricebookEntryId = pbEntry.Id;
    oi.UnitPrice = 1.1;
    oi.OrderId = ord.Id;
    oi.Line_Number__c = 1;
    insert oi;
    return oi;
  }

  //****************************************************************************
  // Method to Create Order Items.
  //****************************************************************************
  public static OrderItem createOrderItem(Order ord){
    PricebookEntry pbEntry = createPricebookEntry();
    OrderItem oi = new OrderItem();
    oi.Quantity = 1;
    oi.Description = 'testDescription';
    oi.Order_Line_Status__c = '531';
    oi.PricebookEntryId = pbEntry.Id;
    oi.UnitPrice = 1.1;
    oi.OrderId = ord.Id;
    oi.Line_Number__c = 1.123;
    insert oi;
    return oi;
  }

   //****************************************************************************
  // Method to Create Order Items.
  //****************************************************************************
  public static OrderItem createOrderItem(Id orderId, Id priceBookEntryId,Boolean isInsert){

    OrderItem oi = new OrderItem();
    oi.Quantity = 1;
    oi.Description = 'testDescription';
    oi.Order_Line_Status__c = '531';
    oi.PricebookEntryId = priceBookEntryId;
    oi.UnitPrice = 1.1;
    oi.OrderId = orderId;
    oi.Line_Number__c = 1.123;
    if(isInsert)
      insert oi;
    return oi;
  }

  //****************************************************************************
  // Method to Create Asset_Configuration_Line__c
  //****************************************************************************
  public static List<Asset_Configuration_Line__c> createAssetConfigLine(Integer count, Id assetId, Id productId, Boolean isInsert){
    List<Asset_Configuration_Line__c> lstAssetConfigLine = new List<Asset_Configuration_Line__c>();
    for(Integer i = 0; i < count; i++){
      Asset_Configuration_Line__c assetObj = new Asset_Configuration_Line__c(Asset__c = assetId,
                                      Product__c = productId);
      lstAssetConfigLine.add(assetObj);
    }
    if(isInsert){
      insert lstAssetConfigLine;
    }
    return lstAssetConfigLine;
  }

  //****************************************************************************
  // Method to Create Asset_Configuration_Line__c
  //****************************************************************************
  public static List<Product_Relationship__c> createProductRelationship(Integer count, Id parentProductId, Id childProductId,Id alternateProductId, Boolean isInsert){
    List<Product_Relationship__c> lstProductRelationship = new List<Product_Relationship__c>();
    for(Integer i = 0; i < count; i++){
      Product_Relationship__c assetObj = new Product_Relationship__c(Product__c = parentProductId,Part__c=childProductId,
                                      Alternate_Product__c = alternateProductId,Alternate_Part_Effective_Start_Date__c = Date.today(),
                                      Alternate_Part_Effective_End_Date__c = Date.today(),Effective_Start_Date__c = Date.today(),
                                      Effective_End_Date__c  = Date.today());
      lstProductRelationship.add(assetObj);
    }
    if(isInsert){
      insert lstProductRelationship;
    }
    return lstProductRelationship;
  }

  //****************************************************************************
  // Method to Create Regulatory_Question_Answer__c
  //****************************************************************************
  public static List<Regulatory_Question_Answer__c> createRegulatoryQuesAnswer(Integer count, Id caseId, Boolean isInsert){
    List<Regulatory_Question_Answer__c> lstRegulatoryQuestionAnswer = new List<Regulatory_Question_Answer__c>();
    for(Integer i = 0; i < count; i++){
      Regulatory_Question_Answer__c objRegulatoryQuestion = new Regulatory_Question_Answer__c(Formal_complaint__c = true, Harm_reported__c = 'No',
            Adverse_Consequences__c = 'No', Case__c= caseId,Answer_4__c = 'Answer',Answer_5_New__c= 'Test Answer', Answer_7_New__c = 'No',
              Answer_8__c = 'Not Reported',Answer_11__c = 'Not Reported',Answer_12_New__c = 'No',Answer_13_New__c = 'Test',Answer_3__c = Date.today(),
              Answer_14_New__c = 'No',Answer_15_New__c = ' (USA) Food and Drug Admininstration' ,Answer_16_New__c = '1234',Answer_17_New__c = '  No');
      lstRegulatoryQuestionAnswer.add(objRegulatoryQuestion);
    }
    if(isInsert){
      insert lstRegulatoryQuestionAnswer;
    }
    return lstRegulatoryQuestionAnswer;
  }

  //****************************************************************************
  // Method to Create Event
  //****************************************************************************
  public static List<Event> createEvent(Integer count, Id accountId, Boolean isInsert){
    List<Event> lstEvents = new List<Event>();
    for(Integer i = 0; i < count; i++){
      Event eve = new Event(ShowAs = 'OutOfOffice',DurationInMinutes = 20,ActivityDateTime = system.today());
      lstEvents.add(eve);
    }
    if(isInsert){
      insert lstEvents;
    }
    return lstEvents;
  }

  //****************************************************************************
  // Method to Create Medical_Common_Settings__c
  //****************************************************************************
  public static Medical_Common_Settings__c createMedicalCommonSetting(Boolean isInsert){
    Medical_Common_Settings__c setting = new Medical_Common_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Enable_Send_Email_When_Order_Scheduled__c=true);
    if(isInsert){
      insert setting;
    }
    return setting;
  }
  
  //****************************************************************************
  // Method to Create Medical_Common_Settings__c
  //****************************************************************************
  public static Medical_JDE_Settings__c createMedicalJDESetting(Boolean isInsert){
    Medical_JDE_Settings__c setting = new Medical_JDE_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),JDE_Endpoint_URL__c = 'https://ps1w2.rt.informaticacloud.com:443/active-bpel/services/0010ES/OrderHeader_IC',
                                                                JDE_Password__c = 'Password',JDE_Username__c = 'test.dev@stryker.poc');
    if(isInsert){
      insert setting;
    }
    return setting;
  }
  
  //****************************************************************************
  // Method to Create Medical_QB_Settings__c
  //****************************************************************************
  public static Medical_QB_Settings__c createMedicalQBSetting(Boolean isInsert){
    Medical_QB_Settings__c setting = new Medical_QB_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Endpoint__c = 'https://qbtest.stryker.com/SSOSFDC_WebService/QBService.svc/REST/QBservice',
                                                                QB_Page_URL__c = 'https://qbtest.stryker.com/ERP2Web47_QBSF/e2wSSOReceiver.aspx',Secret_Key__c = 'testsecret');
    if(isInsert){
      insert setting;
    }
    return setting;
  }
  
  
  

}