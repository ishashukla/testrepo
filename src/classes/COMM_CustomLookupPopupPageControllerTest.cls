// 
// (c) 2016 Appirio, Inc.
//
// Test class of COMM_CustomLookupPopupPageController
//
// 22 Mar 2016     Kirti Agarwal   Original(T-483770)
//
@isTest
private class COMM_CustomLookupPopupPageControllerTest{

  @isTest
  static void COMM_CustomLookupPopupPageController_Test () {
    List < Account > accountList = TestUtils.createAccount(1, true);
     List < Contact> conList = TestUtils.createContact(1, accountList[0].id ,true);
    
    PageReference curentPage = Page.COMM_CustomLookupPopupPage;
    Test.setCurrentPage(curentPage);
    ApexPages.currentPage().getParameters().put('Object', 'Contact');
    ApexPages.currentPage().getParameters().put('fieldName', 'Name');
    ApexPages.currentPage().getParameters().put('searchText', '');
    ApexPages.currentPage().getParameters().put('fieldSetName', 'Contact_Search_Result');
    COMM_CustomLookupPopupPageController objOpenLookupPopupCtrl = new COMM_CustomLookupPopupPageController();
    objOpenLookupPopupCtrl.createContact();
    objOpenLookupPopupCtrl.contactRecord = new Contact(FirstName = 'name', LastName='name');
    objOpenLookupPopupCtrl.saveContact();
    System.assert(objOpenLookupPopupCtrl.contactRecord != null );
  }
}