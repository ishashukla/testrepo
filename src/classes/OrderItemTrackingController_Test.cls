// 
// (c) 2016 Appirio, Inc.
//
// I-235766, Test class of Controller Class To show Tracking link on Order Product detail page 
//
// Sept 17, 2016  Shreerath Nair  Original 

@isTest (seeAllData=false)
public class OrderItemTrackingController_Test{

    
     static testMethod void testOrderItemTrackingController() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        
        Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
        insert con;
        
        
        Order ord= Endo_TestUtils.createStandardOrder(con.Id,acc.Id,false);
        ord.Status = 'Entered';
        ord.PriceBook2Id = Test.getStandardPricebookId();
        insert ord;
        Product2 prod = Endo_TestUtils.createProduct(1, true)[0];
        
        
        PricebookEntry priceBookEnt = Endo_TestUtils.getTestPricebookEntry(ord.PriceBook2Id, prod.Id);
        
        OrderItem ordItem1 = Endo_TestUtils.createOrderItem(prod.Id, ord.Id, false);
        ordItem1.shipping_method__c = 'FDX-Parcel-Saturday/8AM';   
        ordItem1.PricebookEntryId = priceBookEnt.Id;
        insert ordItem1;       
        
        Test.startTest();

            ApexPages.StandardController stdOrdItem = new ApexPages.StandardController(ordItem1);
            OrderItemTrackingController trackingController = new OrderItemTrackingController(stdOrdItem);
            
            ordItem1.shipping_method__c = 'MAN-LTL-3 Business Days'; 
            update ordItem1;
            stdOrdItem = new ApexPages.StandardController(ordItem1);
            trackingController = new OrderItemTrackingController(stdOrdItem);
            
            ordItem1.shipping_method__c = 'OCEAN';
            update ordItem1;
            stdOrdItem = new ApexPages.StandardController(ordItem1);
            trackingController = new OrderItemTrackingController(stdOrdItem);
            
            ordItem1.shipping_method__c = 'UPS-Parcel-2nd Day Air'; 
            update ordItem1;
            stdOrdItem = new ApexPages.StandardController(ordItem1);
            trackingController = new OrderItemTrackingController(stdOrdItem);
            System.assertEquals(trackingController.trackingLinkName, 'UPS Website');
            
        Test.stopTest();
        
      }  
}