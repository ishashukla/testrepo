public class DialogBoxPageController{
  public String accId;
  public List<Strategic_Account__c> listOfStrategicAccount;
  public boolean displayPopup {get; set;}    
  
  public DialogBoxPageController(ApexPages.StandardController controller){
    listOfStrategicAccount = new List<Strategic_Account__c>();
    accId = ApexPages.currentPage().getParameters().get('accid');
  }
  
  public PageReference closePopup()
  {   
    PageReference pageRef;    
    displayPopup = false; 
    pageRef = new PageReference('/'+accId);
    return pageRef;  
  }
  
  public PageReference closePopupYes()
  {       
    PageReference pageRef;
    system.debug('accId>>>'+accId);
    listOfStrategicAccount = [SELECT Id, Name, Account__c, User__c FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: UserInfo.getUserId()];
    delete listOfStrategicAccount;
    displayPopup = false; 
    pageRef = new PageReference('/'+accId);
    return pageRef;
  }
      
  public void showPopup()
  {       
    displayPopup = true;   
  }   
}