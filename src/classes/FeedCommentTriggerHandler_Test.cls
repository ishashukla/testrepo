/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Dec 12, 2013 
 Purpose       :  Test class for trigger FeedCommentTriggerHandler
 Reference     :  Task T-217899
*******************************************************************************/
@isTest
private class FeedCommentTriggerHandler_Test {
	
	//----------------------------------------------------------------------------
  // Method for test updateIntelField method
  //----------------------------------------------------------------------------
  public static testmethod void testIntelField(){
  	Test.startTest();
	  
		  //Create test records  
		  Intel__c intel = TestUtils.createIntel(1, true).get(0);
		  intel = [SELECT Id, Chatter_Post__c, Mobile_Update__c FROM Intel__c WHERE Id = :intel.Id].get(0);
		  
		  User usr1 = TestUtils.createUser(1, null, true).get(0);
		  System.runAs(usr1){
		    List<FeedComment> lstReply = TestUtils.createComment(1, intel.Chatter_Post__c, true);
		  }
		  
		  //Test Functionality
		  DateTime updatedMobileUpdate;
		  for(Intel__c intelObj : [SELECT Id, Mobile_Update__c FROM Intel__c WHERE Id = :intel.Id]){
		  	updatedMobileUpdate = intelObj.Mobile_Update__c;
		  }
		  //System.assertNotEquals(updatedMobileUpdate.getTime(), intel.Mobile_Update__c.getTime());
		  System.assert(updatedMobileUpdate <> null);
	  
	  Test.stopTest();
  }
}