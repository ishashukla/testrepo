/*******************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 10, 2014 
 Purpose       :  This will be used for showing map, weather and local time of contact or account
*******************************************************************************/
public with sharing class Endo_MapWeatherComponent {
  public Endo_Address myaddress{get;set;}
  public String objectType{get;set;}
  public String recordId{get;set;}
  public String currentRecordType{get;set;}
    
  private set<Id> setShowMapRecordTypes;
  
  public Boolean isMapRendered{
    get{
      if(setShowMapRecordTypes == null){
        setShowMapRecordTypes = new set<Id>();
        setShowMapRecordTypes.add(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId());
        setShowMapRecordTypes.add(Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Customer Contact').getRecordTypeId());
      }
      if(setShowMapRecordTypes.contains(currentRecordType)){
        return true;
      }
      else{
        return false;
      }
    }
  }
  public String mapAPISrc{
    get{
      if(isMapRendered == true){
        String s = 'http://maps.googleapis.com/maps/api/staticmap?key=' + System.Label.Google_API_Key +
               '&zoom=8&size=500x200&sensor=false&markers=color:red%7C' + myaddress.getAddressForMap();
        System.debug('@@@@' + s);
        return s;
      }
      return '';
    }
  }
  
  public String currentTime{
    get{
      if(currentTime == null){
        currentTime = currentDateTime(timeZoneId);
      }
      return currentTime;
    }
    set;
  }
  
  public Endo_MapWeatherHelper.LatLng lnt{
    get{
      if(lnt == null){
        lnt = Endo_MapWeatherHelper.getLatLng(myaddress.getAddressForGeo());
        if(lnt == null){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Endo_MapWeatherHelper.errorMessage));
        }
        System.debug('value - ' + lnt.lat +',' + lnt.lng);
      }
      return lnt;
    }
    set;
  }
  
  public String timeZoneId{
    get{
      if(timeZoneId == null){        
        timeZoneId = Endo_MapWeatherHelper.getAccountLocalTime(lnt);
        if(timeZoneId == null){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Endo_MapWeatherHelper.errorMessage));
        }
      }      
      return timeZoneId;
    }
    set;
  }
    
  public Endo_MapWeatherComponent(){
  
  }
  
  public String weatherString{
    get{
      if(weatherString == null){
        
        String cityWeather = '';
        if(myaddress.getCityForMap() != null){
          cityWeather = myaddress.getCityForMap().toLowerCase().capitalize().replace(' ', '_');
        }
        
        String stateWeather = '';
        if(myaddress.state != null){
          stateWeather = myaddress.state.toLowerCase().capitalize().replace(' ', '_');
        }
        
        weatherString = Endo_MapWeatherHelper.getWeather(cityWeather, stateWeather);
        
        //weatherString = Endo_MapWeatherHelper.getWeather(myaddress.getCityForMap().toLowerCase().capitalize().replace(' ', '_'), myaddress.state.replace(' ','_'));
        weatherString = weatherString.removeStart('document.write(\'');
        weatherString = weatherString.removeEnd('\');');
        System.debug('webservice Result-' + weatherString);
        weatherString = weatherString.replace('\\"', '"');
        
        weatherString = weatherString.replaceAll('</?a[^>]*>', '' );
        weatherString.trim();
        System.debug('webservice Result-' + weatherString);
        
        
        // In the Heading of Weather map city name is not displaying correctly, so replacing this string with custom code.
        try{
          Integer indexOfHeading =  weatherString.toLowerCase().indexOf('weather forecast for');
          if(indexOfHeading > 0){
            Integer indexOfHeadingEnd = weatherString.toLowerCase().indexOf('</font>', indexOfHeading);
            String heading = weatherString.substring(indexOfHeading, indexOfHeadingEnd);
            String newHeading = 'Weather forecast for '+ myaddress.getCityForMap();
            weatherString = weatherString.replace(heading, newHeading);
          }
        }
        catch(Exception ex){
          
        }
        // end heading replace code.
        
        
        System.debug('webservice Result-' + weatherString);
      }
      return weatherString;
    }
    set;    
  }
  
  public static string currentDateTime(String TimeZome) {
    String result;
    try{
      result = Datetime.Now().format('EEE, hh:mm a', TimeZome);
    }
    catch(exception ex){
    }
    return result;
  }
  
  
}