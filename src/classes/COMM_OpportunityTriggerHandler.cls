// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_OpportunityTriggerHandler - Trigger Handller for opportunity
//
// 03/09/2016, Deepti Maheshwari (T-458596)
// 03/21/2016, Deepti Maheshwari (T-484584)
// 04/14/2016, Shubham Dhupar    (T-492008)
// 05/26/2016, Rahul Aeran       (T-490716), updating project plan on
//                                all the cases belonging to the opportuting of record type COMM Change Request
// 06/08/2016, Deepti Maheshwari (I-221404)
// 07/01/2016, Meghna Vijay      (T-516835), Pricebook field update to COMM Price Book
// 20/09/2016, Isha Shukla       (T-514350), Added logic in the method updatePriceBookToCOMM() if Opp RT is "COMM Service Opportunity" than set price book to "COMM Repair Parts"  
// 26/09/2016, Kanika Mathur     (I-236362), Modified syncStageAndProbablityPercent() method to reverse allocation from standard to custom Stage field.
// 21st Oct 2016     Nitish Bansal       I-240941 // TOO Many SOQL resolution (Proper list checks before SOQL and DML in almost all methods, single RT query)
public class COMM_OpportunityTriggerHandler {
    public static Map<ID, Schema.RecordTypeInfo> opptyRtMap = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosById();
    
    //----------------------------------------------------------------------------
    //  Method called on before insert of records
    //----------------------------------------------------------------------------
    public static void beforeInsert(List<Opportunity> newList) {
        syncPOCloseDateOnOppty(newList, null);
        syncStageAndProbablityPercent(newList, null);
        // Updated for T-492008
        updateProjectPlanOnOpptyUpdate(newList,null);
        updatePriceBookToCOMM(newList,null);
    }
    
     //----------------------------------------------------------------------------
    //  Method called on after insert of records
    //----------------------------------------------------------------------------
    public static void afterInsert(List<Opportunity> newList) {
        copyAccountTeamToOpportunityTeam(newList);
    }
    
    //----------------------------------------------------------------------------
    //  Method called on before update of records
    //----------------------------------------------------------------------------
    public static void beforeUpdate(List<Opportunity> newList, Map<ID, Opportunity> oldMap) {
        syncPOCloseDateOnOppty(newList, oldMap);
        // Updated for T-484584
        syncStageAndProbablityPercent(newList, oldMap);
        // Updated for T-492008
        updateProjectPlanOnOpptyUpdate(newList,oldMap);
        checkFinalApprovalProcess(newList,oldMap);
    }
    
    //----------------------------------------------------------------------------
    //  Method called on After Update of records
    //----------------------------------------------------------------------------
    public static void AfterUpdate(List<Opportunity> newList, Map<ID, Opportunity> oldMap) {
        // DM 06/08 Commented the call to sync pp on docs because the approach for 
        // T-497456 was changed and these updates are no longer required per T-490747
        
        //syncProjectPlanOnOpptyDocument(newList,oldMap);   
        // I-216558-update order with project plan if project plan is changed
        if(!COMM_OrderTriggerHandler.isOrderTrigger){
            updateProjectPlan(newList,oldMap); 
        }
        //T-490716 - update project plan on cases for change request record type
        updateProjectPlanOnCases(newList,oldMap);
        updateCSROwner(newList,oldMap);// SD 9/7: To set CSR Owner to current user on Step 1 Approval or Rejection Action
    }
    
    public static void checkFinalApprovalProcess(List<Opportunity> newList, Map<ID, Opportunity> oldMap) {
   /*    Map<ID, Schema.RecordTypeInfo> opptyRtMap = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosById();
      for(Opportunity opp: newList) {
        if(opptyRTMap.get(opp.RecordtypeID).getName().contains(Constants.COMM)) {
          if(opp.PO_Approval_Status__c == 'Booking' && oldMap.get(opp.Id).PO_Approval_Status__c != opp.PO_Approval_Status__c && opp.Revenue_Recognition__c == NULL ) {
            opp.AddError('Cannot update to booking');
           }
        }
      }*/
    }
    //================================================================      
    // Name         : copyAccountTeamToOpportunityTeam
    // Description  : To copy account team to opportunity team only at Insert
    // Created Date : 26th Sep 2016 
    // Created By   : Shubham Dhupar (Appirio)
    // Issue         : I-236394
    //==================================================================
    public static void copyAccountTeamToOpportunityTeam(List<Opportunity> newOppty) {
      /*Map<ID, Schema.RecordTypeInfo> opptyRtMap = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosById();*/
      Map< Id , Id > opportunityIdToAccountId = new Map< Id , Id >();
      for(Opportunity o : newOppty)  {
        if(opptyRTMap.get(o.RecordtypeID).getName().contains(Constants.COMM)) {
          opportunityIdToAccountId.put(o.Id,  o.AccountId );
          
        }
      }
      Map<id, List<AccountTeamMember > > accountIdToAccountTeamMembers = new    Map<id,  List<AccountTeamMember > > ();
      List<AccountTeamMember> atmList = new List<AccountTeamMember>();
      try{
        if(opportunityIdToAccountId.size() > 0){
          for(AccountTeamMember atm : [SELECT UserId,User.Name,TeamMemberRole, Id, AccountId
                                        FROM AccountTeamMember
                                        WHERE AccountId in :opportunityIdToAccountId.values() ]) {
            if(atm != NULL) {
              atmList.add(atm);
              accountIdToAccountTeamMembers.put(atm.Accountid, atmList);
            }
          }
        }
        
      }catch(System.NullPointerException e) {
        
      }
      List<OpportunityTeamMember> oppTeamMembersList = new List<OpportunityTeamMember>();
      for(Id opp : opportunityIdToAccountId.keySet() ) {
        Id accountId  = opportunityIdToAccountId.get(opp);
        try {
          if(atmList != NULL && atmList.size() > 0) {
            for (AccountTeamMember atm : accountIdToAccountTeamMembers.get(accountId) )  {
              OpportunityTeamMember otm  = new OpportunityTeamMember();
              otm.UserId = atm.UserId;
              otm.TeamMemberRole = atm.TeamMemberRole;
              otm.OpportunityId = opp;
              oppTeamMembersList.add(otm);
            }
          }
        }catch(Exception e){}
        }
      if(!oppTeamMembersList.isEmpty() && oppTeamMembersList.size() > 0) {
        insert oppTeamMembersList;
      }
    }
    //================================================================      
    // Name         : updateCSROwner
    // Description  : To set CSR Owner to current user on Step 1 Approval or Rejection Action
    // Created Date : 9th Sep 2016 
    // Created By   : Shubham Dhupar (Appirio)
    // Task         : I-233827
    //==================================================================
    public static void updateCSROwner(List<Opportunity> newOppty,Map<Id,Opportunity> oldMap) {
       boolean locked = Approval.isLocked(newOppty.get(0).id);
        if(locked) {
          Approval.UnLockResult[] lrList = Approval.unlock(newOppty, false);
        }
        List<Opportunity> oppList = new List<Opportunity>();
        Set<id> opptyId  =  new Set<Id>();
        for(Opportunity opp: newOppty) {
          if(opp.Set_CSR_Owner__c != oldMap.get(opp.id).Set_CSR_Owner__c && opp.Set_CSR_Owner__c == true) {
            Opportunity oppty = new Opportunity(id=opp.id);
            oppty.Assigned_To__c = UserInfo.getUserId();
            oppty.Set_CSR_Owner__c = false;
            //system.debug('user          '+ oppty.Assigned_To__c );
            oppList.add(oppty);
          }
        }
        if(!opplist.isEmpty() && opplist.size() > 0) {
          update oppList;
        }
    }
    //================================================================      
    // Name         : updateProjectPlan
    // Description  : 
    // Created Date : 4th May 2016 
    // Created By   : Kirti Agarwal (Appirio)
    // Update By    : Rahul Aeran I-221404, 101 error (Now we have Project plan update on Order Trigger so 
    //                we do not need to explicitly update order item from here)
    // Task         : I-216558
    //==================================================================
    public static void updateProjectPlan(List<Opportunity> newList, Map<ID, Opportunity> oldMap) {
        Set<Id> oppIdSet = new Set<Id>();
        Map<Id,Id> oppIdAndProjectPlanId = new Map<Id,Id>();
        for(Opportunity opp : newList) {
            if(opp.Project_Plan__c != oldMap.get(opp.id).Project_Plan__c) {
                oppIdAndProjectPlanId.put(opp.id, opp.Project_Plan__c);
                oppIdSet.add(opp.id);
            }
        }
        if(!oppIdSet.isEmpty() && oppIdSet.size() > 0) {
            List<Order> orderList = new List<Order>();
            List<OrderItem> orderItemList = new List<OrderItem>();
            for(Order orderRec : [SELECT ID, Project_Plan__c,opportunityId
                                  //, (SELECT Id, Project_Plan__c 
                                  // FROM orderItems) //Commented by Rahul Aeran as we are now doing this in Order Trigger
                                  FROM Order 
                                  WHERE opportunityId IN :oppIdSet]) {
                                      if(oppIdAndProjectPlanId.containsKey(orderRec.opportunityId) && orderRec.Project_Plan__c != oppIdAndProjectPlanId.get(orderRec.opportunityId)) {                         
                                          orderRec.Project_Plan__c = oppIdAndProjectPlanId.get(orderRec.opportunityId);
                                          orderList.add(orderRec);
                                          /*for(OrderItem oI : orderRec.orderItems) {
                                            oI.Project_Plan__c = oppIdAndProjectPlanId.get(orderRec.opportunityId);
                                            orderItemList.add(oI);
                                            }*///Commented by Rahul Aeran
                                      }
                                  }
            if(!orderList.isEmpty() && orderList.size() > 0) {
                try{
                  update orderList;
                } catch(Exception e) {
                  for(Opportunity opp : newList) {
                    opp.addError(e.getMessage());
                  }
                }
                
            }
            /*if(!orderItemList.isEmpty()) {
            update orderItemList;
            }*///Commented by Rahul Aeran
        }
    }
    
    //================================================================      
    // Name         : updateProjectPlanOnCases 
    // Description  : 
    // Created Date : 25th May 2016 
    // Created By   : Rahul Aeran (Appirio)
    // Task         : T-490716
    //==================================================================
    
    private static void updateProjectPlanOnCases(List<Opportunity> lstNew, map<Id,Opportunity> mapOld){
        map<Id,Id> mapOppIdToPlanId = new map<Id,Id>();
        for(Opportunity opp : lstNew){
            if(opp.Project_Plan__c != mapOld.get(opp.Id).Project_Plan__c){
                mapOppIdToPlanId.put(opp.Id,opp.Project_Plan__c);
            }
        }
        Id rtChangeOrder = Constants.getRecordTypeId(Constants.COMM_CASE_RTYPE, Constants.CASE_OBJECT);
        List<Case> lstCasesToUpdate = new List<Case>();
        if(!mapOppIdToPlanId.keySet().isEmpty() && mapOppIdToPlanId.size() > 0){
            for(Case cs : [SELECT Id,Opportunity__c FROM Case 
                           WHERE Opportunity__c IN :mapOppIdToPlanId.keySet()
                           AND RecordTypeId = :rtChangeOrder]){
                               cs.Project_Plan__c = mapOppIdToPlanId.get(cs.Opportunity__c);
                               lstCasesToUpdate.add(cs);
                           }
            if(!lstCasesToUpdate.isEmpty() && lstCasesToUpdate.size() > 0){
                update lstCasesToUpdate;
            }
        }
        
    }
   /*  //NB - 12/12/2016 - Commented the definition since calling is commented. Impacting code coverage
   
    public static void syncProjectPlanOnOpptyDocument(List<Opportunity> newList, Map<Id,Opportunity> OldMap) {
        Map<Id,Id> newOpptyIdMap = new Map <Id,Id>();
        for(Opportunity opp : newList) {
            if(opp.Project_Plan__c != NULL) {
                if(oldMap != NULL) {  
                    if(OldMap.get(opp.Id).Project_plan__c != opp.Project_Plan__c) {
                        newOpptyIdMap.put(opp.Id, opp.Project_Plan__c);
                    }
                }
                else {
                    if(opp.Project_Plan__c != NULL) {
                        newOpptyIdMap.put(opp.Id, opp.Project_Plan__c);
                    }
                }
            }
        }
        if(newOpptyIdMap != NULL && newOpptyIdMap.size() > 0) {
            List <Document__c> oppDoc = new List<Document__c>();
            Document__c doc;
            for(Document__c opptyDoc : [Select Project_Plan__c, Opportunity__c
                                        FROM Document__c
                                        Where Opportunity__c in : newOpptyIdMap.keySet()]) {
                                            doc = new Document__c(Id = opptyDoc.Id,Project_Plan__c = newOpptyIdMap.get(opptyDoc.Opportunity__c));   
                                            oppDoc.add(doc);                       
                                        }
            if(oppDoc.size() > 0)
              update oppDoc;
        }
    }
    */
    
    
    //------------------------------------------------------------------------------------------------------------------
    //  Method to update Project Plan Stage Field to Cancelled When Oppty's Stage is set to Cancelled. (Ref : T-492008)
    //------------------------------------------------------------------------------------------------------------------
    public static void updateProjectPlanOnOpptyUpdate(List<Opportunity> newList,map<Id,Opportunity> oldMap) {
        Boolean isUpdate = oldMap != null ? true : false;
        Map<id,String> projectIds = new Map<id,String>();
        Map<id,String> stageMap = new Map<id,String>();                                                     
        for(Opportunity oppty : newList) {
            if((!isUpdate || Oppty.Project_Plan__c !=  oldMap.get(oppty.Id).Project_Plan__c || Oppty.Stage__c !=  oldMap.get(oppty.Id).Stage__c) 
               && Oppty.Project_Plan__c != NULL) {
                   projectIds.put(oppty.id,oppty.Project_Plan__c);
                   stageMap.put(oppty.Project_Plan__c,oppty.Stage__c) ; 
               }
        }
        List<Project_Plan__c> proList = new List<Project_Plan__c>();
        if(projectIds.size() > 0){
          for(Project_Plan__c project : [SELECT stage__c 
                                         FROM  Project_Plan__c
                                         WHERE Id in : projectIds.values()] ) {
                                             if(stageMap.get(project.id)==System.Label.COMM_Cancelled && project.stage__c != System.Label.COMM_Cancelled) {
                                                 Project_Plan__c p = new Project_Plan__c (id=project.id,stage__c = System.Label.COMM_Cancelled);
                                                 proList.add(p);
                                             }                   
                                         }
          if(proList.size() > 0)                               
            update proList;
        }
        
    }
    
    //----------------------------------------------------------------------------
    //  Method to sync Stage and Probablity Percent (Ref : T-484584)
    //----------------------------------------------------------------------------
    private static void syncStageAndProbablityPercent(List<Opportunity> newList, Map<ID, Opportunity> oldMap) {
        /*Map<ID, Schema.RecordTypeInfo> opptyRtMap = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosById();*/
        for(Opportunity oppty : newList){     
            if(opptyRTMap.get(oppty.RecordtypeID).getName().contains(Constants.COMM)){
                // In case of update
                if(oldMap != null) {
                    if(oppty.Stage__c != oldMap.get(oppty.id).Stage__c
                       || oppty.StageName != oldMap.get(oppty.id).StageName) {
                           if(oppty.Stage__c != oppty.StageName){
                               //Modified to reverse allocation from standard stage to custom stage -KM 9/26/2016 I-236362
                               oppty.Stage__c = oppty.StageName;
                            }
                       }             
                    //IF probability is updated OR  
                    // When stageName is updated, it automatically updates probablity, so probablity has to be reset in this case          
                    if(oppty.Percent_Probability__c != oldMap.get(oppty.id).Percent_Probability__c
                       || oppty.Probability != oldMap.get(oppty.id).Probability
                       || oppty.Stage__c != oldMap.get(oppty.id).Stage__c
                       || oppty.StageName != oldMap.get(oppty.id).StageName) {
                           if(oppty.Percent_Probability__c != null){
                               if(oppty.Percent_Probability__c.indexof('-') != -1) {
                                   oppty.Probability = Decimal.valueof(oppty.Percent_Probability__c.split('-').get(0));
                               }
                               else {
                                   if(oppty.Percent_Probability__c != null)
                                       oppty.Probability = Decimal.valueof(oppty.Percent_Probability__c);
                               }
                           }
                           else{
                               oppty.Probability = null;
                           }
                       } 
                    // In case of insert         
                }
                else {
                    if(oppty.Stage__c != oppty.StageName) {
                        //Modified to reverse allocation from standard stage to custom stage -KM 9/26/2016 I-236362
                        oppty.Stage__c = oppty.StageName;           
                    }
                    if(oppty.Percent_Probability__c != null) {
                        if(oppty.Percent_Probability__c.indexof('-') != -1) {
                            oppty.Probability = Decimal.valueof(oppty.Percent_Probability__c.split('-').get(0));
                        }
                        else {
                            if(oppty.Percent_Probability__c != null)
                                oppty.Probability = Decimal.valueof(oppty.Percent_Probability__c);
                            
                            else{
                                oppty.Probability = null;
                            }   
                        }
                    }
                }
            }
        }
    }
    //----------------------------------------------------------------------------
    //  Method to sync PO Close Date
    //----------------------------------------------------------------------------
    private static void syncPOCloseDateOnOppty(List<Opportunity> newList, Map<ID, Opportunity> oldMap) {
        /*Map<ID, Schema.RecordTypeInfo> opptyRtMap = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosById();*/
        for(Opportunity oppty : newList) {
            ////system.debug('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'+opptyRTMap.get(oppty.RecordtypeID).getName());
            if(opptyRTMap.get(oppty.RecordtypeID).getName().contains(Constants.COMM)) {
                if(oldMap != null) {
                    if((oppty.PO_Close_Date__c != oldMap.get(oppty.id).PO_Close_Date__c) 
                       && (oppty.PO_Close_Date__c != null)) {
                           oppty.closeDate = oppty.PO_Close_Date__c;
                       }
                }
                else{
                    if(oppty.PO_Close_Date__c != null)
                        oppty.closeDate = oppty.PO_Close_Date__c;
                }
            }
        }
    }
    //================================================================      
    // Name         : updatePriceBookToCOMM 
    // Description  : To update pricebook to COMM Price Book
    // Created Date : 07/01/2016 
    // Created By   : Meghna Vijay (Appirio)
    // Task         : T-516835
    // 20/09/2016   Isha Shukla Modified (T-514350) 
    //==================================================================
    private static void updatePriceBookToCOMM(List<Opportunity>newList, Map<Id, Opportunity>oldMap) {
        
        //COMM_Constant_Setting__c c = [SELECT COMM_Price_Book_Id__c FROM COMM_Constant_Setting__c];
        List<Pricebook2> pricebook = [SELECT Id, Name FROM Pricebook2 WHERE Name =: Constants.COMM_PRICEBOOK];
        //List<Pricebook2> pricebookCOMMBP = [SELECT Id, Name FROM Pricebook2 WHERE Name =: 'COMM Repair Parts']; 
        //T-514350 changes
        COMMBPConfigurations__c cs = COMMBPConfigurations__c.getOrgDefaults();
        //Id pricebookCOMMBPId;
        //if(cs != null) {
        //  pricebookCOMMBPId = Id.valueOf(cs.COMM_Repair_Parts_PricebookId__c);   
        //}
        for(Opportunity oppty : newList) {      
            if(opptyRTMap.get(oppty.RecordtypeID).getName().contains(Constants.COMM)) {
                // T-514350 changes
                //if(pricebookCOMMBPId != null && opptyRTMap.get(oppty.RecordtypeID).getName().contains(Constants.COMM_SERVICE_OPP)) {
                    //oppty.Pricebook2Id = pricebookCOMMBPId;
                //} else if(pricebook != null && pricebook.size() > 0){
                    oppty.Pricebook2Id=pricebook.get(0).Id;
                //}
            } 
        }
    }
}