/**================================================================      
* Appirio, Inc
* Name: COMM_AssetPFARelTriggerHandlerTest
* Description: Test class for COMM_AssetPFARelTriggerHandler
* Created Date: 16 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
* 02Dec16            Jason Carlson    Updated test data on Identification_Number__c from Numeric to String
*                                     due to field type change
==================================================================*/
@isTest
private class COMM_AssetPFARelTriggerHandlerTest {
    static List<Asset_PFA_Relationship__c> assetPFAList;
    static SVMXC__Installed_Product__c asset;
    @isTest static void testTrigger() {
        createData();
        Test.startTest();
        insert assetPFAList;
        Test.stopTest();
        System.assertEquals([SELECT Id FROM SVMXC__Service_Order__c WHERE SVMXC__Component__c = :asset.Id].size() > 0, true);
    }
    private static void createData() {
        Product_Field_Action__c pfa = new Product_Field_Action__c(Identification_Number__c = '11', Date_Initiated__c = System.now() );
        insert pfa;
        assetPFAList = new List<Asset_PFA_Relationship__c>();
        SVMXC__Service_Group__c serviceGroup = new SVMXC__Service_Group__c();
        insert serviceGroup;
        SVMXC__Service_Group_Members__c serviceMember = new SVMXC__Service_Group_Members__c(
            SVMXC__Service_Group__c = serviceGroup.Id,Name = 'Automated'
        );
        insert serviceMember;
        asset = new SVMXC__Installed_Product__c(
            Name = 'testId'
        );
        insert asset;
        for(integer i=0;i<2;i++) {
            Asset_PFA_Relationship__c assetPFARecord = new Asset_PFA_Relationship__c(
                Asset__c = asset.Id,
                PFA__c = pfa.Id,
                Technician__c = serviceMember.Id
            );  
            assetPFAList.add(assetPFARecord);  
        }
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
    }
}