//
// 2016, Appirio Inc.
// 
// Migrated From SRC Org
// 
// 21 April, 2016      Meghna Vijay     T-493108
//
public class RoomTriggerHandler {
  public static void deleteChildRecords(Room__c[] roomsToDel){
    Set<ID> roomToDelID = new Set<ID>();
    List<Equipment__c> equipToDel = new List<Equipment__c>();
    if(roomsToDel.size() > 0) {
      for(Room__c r : roomsToDel) {
        roomToDelID.add(r.Id);
      }
    }
    try {
      if(roomToDelID.size() > 0) {
      delete [SELECT Name, id, Room__c 
              FROM Equipment__c 
              WHERE Room__c  IN : roomToDelID];
      }
    }
    catch(DMLException e) {
     for(Equipment__c equip : equipToDel) {
       equip.addError(Label.EquimentDeleteErrorMessage);
     }
    }
  }
  
  public static void onAfterInsert(List<Room__c> lstNew){
    rollUpRoomOnPhase(lstNew,null);
  }
  public static void onAfterUpdate(List<Room__c> lstNew,map<Id,Room__c> mapOld){
    
  }
  public static void onAfterDelete(List<Room__c> lstOld){
    rollUpRoomOnPhase(lstOld,null);
  }
  
  // Method used to roll up room name on project phase 
  // To be used with Docusign, we don't need this on
  // update as Room has a master detail relationship with Phase
  // @param mapOld - map to hold Trigger.oldMap 
  // @param lstRooms - List to hold Trigger.new in case of insert and
  // Trigger.old in case of delete
  // @return Nothing 
  // @task : T-497607
  private static void rollUpRoomOnPhase(List<Room__c> lstRooms, map<Id,Room__c> mapOld){
    Set<Id> setPhaseIds = new Set<Id>();
    for(Room__c room : lstRooms){
      setPhaseIds.add(room.Project_Phase__c);
    }
    
    List<Project_Phase__c> lstPhases = new List<Project_Phase__c>();
    //Now we will query all the rooms for the phases
    for(Project_Phase__c phase : [SELECT Id,(SELECT Id,Name FROM Rooms__r) 
                                  FROM Project_Phase__c
                                  WHERE Id IN :setPhaseIds]){
      phase.Phase_Room_Numbers__c = '';
      if(phase.Rooms__r != null && !phase.Rooms__r.isEmpty()){
        for(Room__c room : phase.Rooms__r){
          phase.Phase_Room_Numbers__c += room.Name + ', ';
        }
        lstPhases.add(phase);
      }
      
      
    }
    // MV - 10/04 - I-238431 Start
    try {
      Database.update(lstPhases,false);
    }
    catch(DMLException e) {
      for(Room__c room : lstRooms) {
          room.addError(e.getmessage());
        }
    }
    // MV - 10/04 - I-238431 Stop
  }
}