// 
// (c) 2016 Appirio, Inc.
//
// T-507913, This is a batch class for updating Cases for Inactivity
//
// June 2, 2016  Shreerath Nair  Original 

public with sharing class Intr_Related_ListController {

	String objectLabel;
    String objectLabelPlural;  
    Boolean showNewButton;
    public List<String> fieldNames {get; set;}
    public Map<String,String> fieldAlignMap {get; set;}
    public Map<String,String> nameLabelMap {get; set;}
    transient Schema.DescribeSObjectResult objectDescribe;
    public Id deleteRecordId {get; set;}
    public String sortByField {get; set;}
    public Map<String,String> fieldSortDirectionMap {get; set;}
    public Integer pageNumber {get;set;}
    public Integer counter = 0;
    public integer totalsize{get;set;}
    
    //----Variables set from attributes defined in the component----
    public String objectName {get; set;}
    public String fieldsCSV {get; set;}
    public List<String> fieldsList {get; set;}
    public String searchFieldName {get; set;}
    public List<String> searchFieldValue {get; set;}
    public String filter {get; set;}
    public String orderByFieldName {get; set;}
    public String sortDirection {get; set;}
    public Integer pageSize {get; set;}
    public String title {get;set;}
    public String returnUrl {get;set;}
    public String moreLink {get;set;}
    public String showMoreLink {get;set;}
    public Boolean showAsStandardRelatedList {get;set;}
    public String queryString = '';
    private Boolean changeSortDirection;
    
    
    public Intr_Related_ListController() {
    	
        counter = 0;
        changeSortDirection = true;
        
    }
    
    public List<sObject> getRecords() {
    	
        String getCount = 'select count(id) result from '+objectName+' WHERE AccountId IN :searchFieldValue';
        AggregateResult results = database.query(getCount);
        totalsize = Integer.valueOf(results.get('result'));
        
        if(totalsize  > 0 ) {
        	
            Boolean validationPass = true;
            if(fieldsList == null && fieldsCSV == null) {
            	
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'fieldList or fieldsCSV attribute must be defined.'));
                validationPass = false;
                
            }
            //Ensure sortDirection attribute has value of 'asc' or 'desc'
            if(sortDirection != null && sortDirection != 'asc' && sortDirection != 'desc') {
            	
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'sortDirection attribute must have value of "asc" or "desc"'));
                validationPass = false;
                
            }
            if(validationPass == false) {
            	
                return null;
                
            } else {
            	
                	queryString = 'SELECT ';
                	if(fieldsCSV != null) {
                		
	                    queryString += fieldsCSV;       
	                    fieldNames = fieldsCSV.split(',');
	                    
                	} else {
                	
	                    fieldNames = fieldsList.clone();
	                    for(String fld : fieldsList) {
	                    	
	                    	queryString += fld + ',';
                        
                    }
                    
                    queryString = queryString.substring(0,queryString.length() - 1);
                    
                }
                if(searchFieldValue.size() == 0  && String.isBlank(filter)) {
                	
                    queryString += ' FROM ' + objectName + ' LIMIT 0 ';
                    
                }
                else {
                	
                    //add from object and parent criteria
                    queryString += ' FROM ' + objectName + ' WHERE ';
                    
                    String filterCriteria = ''; 
                    if(searchFieldValue.size() > 0) {
                    	
                        filterCriteria += searchFieldName + ' IN :searchFieldValue';
                        //filterCriteria += searchFieldName + ' = \'' + searchFieldValue.trim() + '\'';
                        
                    }
                    
                    //Add any addtional filter criteria to query string if it was defined in component
                    if(filter != null && filter.length() > 0 ) {
                    	
                        filterCriteria += ( String.isBlank(filterCriteria) ? '' : ' AND ' ) + filter;  
                                  
                    }
                    
                    queryString += filterCriteria;
                    
                    //Add order by field to query if defined in component
                    //If sortByField != null then user has clicked a header and sort by this field
                    if(sortByField != null) {
                    	
                        queryString += ' order by ' + sortByField;
                        
                    }else if(orderByFieldName != null) {
                    	
                        queryString += ' order by ' + orderByFieldName;
                        
                    }
                    
                    //If sortByField != null then user has clicked a header, sort based on values stored in map
                    if(sortByField != null) {
                    	
                        /*Use a map to store the sort direction for each field, on first click of header sort asc
                          and then alternate between desc*/
                        if(fieldSortDirectionMap == null) {
                        	
                            fieldSortDirectionMap = new Map<String,String>(); 
                                              
                        }
                        
                        String direction = '';
                        
                        //check to see if field has direction defined, if not or it is asc, order by asc 
                        if(fieldSortDirectionMap.get(sortByField) == null || fieldSortDirectionMap.get(sortByField) == 'desc' ) {
                        	
                            direction = changeSortDirection == true ? 'asc' : 'desc';
                            fieldSortDirectionMap.put(sortByField, direction);
                            
                        } else {
                        	
	                            direction = changeSortDirection == true ? 'desc' : 'asc';
	                            fieldSortDirectionMap.put(sortByField, direction);
                        }
                        
                        queryString += ' ' + direction; 
                    }
                    
                    else if(sortDirection != null) {
                    	
                        //Add sort direction to query if defined in component
                        queryString += ' ' + sortDirection; 
                                    
                    }
                    
                    	//Add limit clause to end of the query
                   		queryString += ' limit :pageSize offset :counter'; 
                }
            }
        }
        /*For the fields that will be displayed identify the field type and set styleClass for
          cell alignment. Numbers, Currency, %, etc should align right in table. put in map FieldName -> class name*/
          
        //Get the meta data info for the fields is the related object
        Map<String, Schema.SObjectField> fieldMap = getObjectDescribe().fields.getMap(); 
        Map<String,String> mapReferenceFields = new Map<String,String>();
        Map<String, Map<String, Schema.SObjectField>> mapReferenceObjectsFieldMap = new Map<String, Map<String, Schema.SObjectField>>(); 
        
        for(Schema.SObjectField field : fieldMap.values()) {
        	
            Schema.DescribeFieldResult fieldResult = field.getDescribe();
            if(fieldResult.getType() == Schema.DisplayType.Reference) {
            	
                mapReferenceFields.put(fieldResult.getRelationshipName(), String.valueOf(fieldResult.getReferenceTo()[0]));
                
            }
        }
        
        //For the fields in the related list populate fieldAlignMap map with the name of the correct style class. Also populate name->label map for header display
        fieldAlignMap = new Map<String,String>();
        nameLabelMap = new Map<String,String>();
        
        if(fieldNames != null) {
        	
	        for(String fld : fieldNames) {
	        	
	            fld = fld.trim();
	            if(fieldMap.containsKey(fld)){
	                populateFieldMaps(fld, fieldMap, fld);
	                
	            }
	            else if(fld.contains('.')) {
	            	
	                populateReferenceFields(fld, fld, mapReferenceFields, mapReferenceObjectsFieldMap);
	                
	            }
	            else {
	            	
	                fieldAlignMap.put(fld,'alignLeft');
	                nameLabelMap.put(fld,fld);
	                
	            }
	            
	        }
        }
        system.debug('>>>>>>query1'+searchFieldValue);
        system.debug('>>>>>>query'+queryString);
        pageNumber = getPageNumber();
        return String.isBlank(queryString) ? null : Database.query(queryString);
    }
    
    public PageReference Beginning() { //user clicked beginning
    	
        counter = 0;
        changeSortDirection = false;
        return null;
        
    }
    
    public PageReference Previous() { //user clicked previous button
    	
        counter -= pageSize;
        changeSortDirection = false;
        return null;
        
    }
    
    public PageReference Next() { //user clicked next button
    	
        counter += pageSize;
        changeSortDirection = false;
        return null;
        
    }
    
    public PageReference End() { //user clicked end
    	
        if( math.mod(totalsize, pageSize) == 0){
            counter = totalsize - pageSize;
            
        }
        else {
        	
            counter = totalsize - math.mod(totalsize, pageSize);
        }
        
        changeSortDirection = false;
        System.debug('>>>>>>>>>1'+totalsize);
        System.debug('>>>>>>>>>2'+pageSize);
        System.debug('>>>>>>>>>3'+math.mod(totalsize, pageSize));
        System.debug('>>>>>>>>>4'+counter);
        return null;
    }
    
    public Boolean getDisablePrevious() { 
        //this will disable the previous and beginning buttons
        if (counter>0) return false; else return true;
    }
    
    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + pageSize < totalsize) return false; else return true;
    }
    
    public Integer getTotal_size() {
        return totalsize;
    }
    
    public Integer getPageNumber() {
        return counter/pageSize + 1;
    }
    
    public Integer getTotalPages() {
        if (math.mod(totalsize, pageSize) > 0) {
            return totalsize/pageSize + 1;
        } else {
            return (totalsize/pageSize);
        }
    }
   
   private void populateReferenceFields(String fld, String fieldName, Map<String,String> mapReferenceFields, Map<String, Map<String, Schema.SObjectField>> mapReferenceObjectsFieldMap) {
        
        List<String> fieldParts = fieldName.split('\\.', 2);
        
        if(mapReferenceFields.containskey(fieldParts[0])) {
        	
            String refObjectName = mapReferenceFields.get(fieldParts[0]);
            Map<String, Schema.SObjectField> refObjFieldMap;
            if(mapReferenceObjectsFieldMap.containskey(refObjectName)) {
            	
                refObjFieldMap = mapReferenceObjectsFieldMap.get(refObjectName);
                }
            else {
            	
                refObjFieldMap = Schema.getGlobalDescribe().get(refObjectName).getDescribe().Fields.getMap();
                mapReferenceObjectsFieldMap.put(refObjectName, refObjFieldMap);
                
            }
            
            if(fieldParts[1].contains('.')) {
            	
                mapReferenceFields = new Map<String,String>();
                for(Schema.SObjectField field : refObjFieldMap.values()) {
                	
                    Schema.DescribeFieldResult fieldResult = field.getDescribe();
                    if(fieldResult.getType() == Schema.DisplayType.Reference) {
                    	
                        mapReferenceFields.put(fieldResult.getRelationshipName(), String.valueOf(fieldResult.getReferenceTo()[0]));
                    }
                }
                populateReferenceFields(fld, fieldParts[1],  mapReferenceFields, mapReferenceObjectsFieldMap);
            }
            else {
            	
                if(refObjFieldMap.containskey(fieldParts[1])) {
                	
                    populateFieldMaps(fld, refObjFieldMap, fieldParts[1]);
                }
                else {
                	
                    fieldAlignMap.put(fld,'alignLeft');
                    nameLabelMap.put(fld,fld);
                }
            }
        }
    } 
    
    private void populateFieldMaps(String fld, Map<String, Schema.SObjectField> field_map, String fieldApi) {
        
        String fieldType = field_map.get(fieldApi).getDescribe().getType().name();    
        
        if(fieldType == 'CURRENCY' || fieldType == 'DOUBLE' || fieldType == 'PERCENT' || fieldType == 'INTEGER') {
        	
            fieldAlignMap.put(fld,'alignRight');
        } else {
        	
            fieldAlignMap.put(fld,'alignLeft');
        }   
        
        //Add to name->label map
        String label = field_map.get(fieldApi).getDescribe().getLabel();
        nameLabelMap.put(fld,label);
    }
    
    
    public Boolean getShowNewButton() {
        //Display new button if user has create permission for related object
        return getObjectDescribe().isCreateable();
        
    }
    
    public DescribeSObjectResult getObjectDescribe() {
        /*Returns object describe for list object. This is used in many places so we are using a dedicated method that only invokes 
          Schema describe calls once as these count against Apex limits. Because this method returns a DescribeSObjectResult all the get 
          methods for this object can be used directly in Visualforce: {!objectDescribe.label}*/
        if(objectDescribe == null) {
        	
            objectDescribe = Schema.getGlobalDescribe().get(objectName).getDescribe();  
        }
        return objectDescribe;
    }
    
    public void sortByFieldAction() {
        
        //Making ssc variable null will cause getRecords method to requery records based on new sort by field clicked by user
        // ssc = null;
        changeSortDirection = true;
    }
    
    public void deleteRecord() {
    	
        for(sObject obj : Database.query(queryString)) {
        	
            if(obj.get('Id') == deleteRecordId){
                delete obj;
                break;  
            }
        }
        getRecords();
    }
    
    public void showMore() {
    	
        calculateMoreRecords(true);
    }
    
    private void calculateMoreRecords(Boolean isSetPageSize) {
        
    }
    
    
    
    
}