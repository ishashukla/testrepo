/**================================================================      
* Appirio, Inc
* Name: COMM_OpptyDocumentsListController   
* Description: T-492209 (Controller for COMM_OpptyQuoteDocumentsListPage)
* Created Date: 04/13/2016
* Created By: Deepti Maheshwari (Appirio)
*
* Date Modified      Modified By      Description of the update
* 5th August 2016    Nitish Bansal    I-227047 - To resolve Inline VF page error in LEX
==================================================================*/
public with sharing class COMM_OpptyDocumentsListController {
  public Opportunity oppty{get;set;} 
  
  public Id selectedID{get;set;}
  public String newDocumentURL{get;set;}
  public COMM_Conga_Setting__c congaSetting{get;set;}
  public Integer listSize {get;set;}
  public List<Document__c> documentList{get;set;} 
  //public List<Document__c> documentListToShow{get;set;}
  public Integer pageSize{get;set;}
  public Integer pageNumber{get;set;}
  
  public String CLOSED_WON{
	  get{
      CLOSED_WON = Constants.CLOSED_WON;
      return CLOSED_WON;
	  }set;
	  
  }
  public String CLOSED_LOST{
	  get{
      CLOSED_LOST = Constants.CLOSED_LOST;
      return CLOSED_LOST;
	  }set;
      
  }
  public String CANCELLED{
	  get{
      CANCELLED = Constants.CANCELLED;
      return CANCELLED;
	  }set;
      
  }

  /*
  public Id CAD_REQUEST_RT_ID {
      get;
      set;
  }
  
  public Id COMM_OPPTY_DOC_OPPORTUNITY_LKID {
      get;
      set;
  }

  public Id COMM_OPPTY_DOC_ACCOUNT_LKID {
      get;
      set;
  }

  public Id COMM_OPPTY_DOC_LISTVIEW_ID {
      get;
      set;
  }
  */

  public COMM_OpptyDocumentsListController(ApexPages.StandardController std){
  /*  CAD_REQUEST_RT_ID = Schema.SObjectType.Document__c.getRecordTypeInfosByName().get('CAD Request').getRecordTypeId();
    COMM_OPPTY_DOC_LISTVIEW_ID = COMM_Constant_Setting__c.getOrgDefaults().Document_List_View_ID__c;
    COMM_OPPTY_DOC_ACCOUNT_LKID = COMM_Constant_Setting__c.getOrgDefaults().Account_Lookup_Id_on_Oppty_Document__c;
    COMM_OPPTY_DOC_OPPORTUNITY_LKID = COMM_Constant_Setting__c.getOrgDefaults().Opportunity_Lookup_Id_on_Oppty_Document__c;*/
    //I-227047 - Nb - 08/05 - Start
    documentList = new List<Document__c>();
    documentListToShow = new List<Document__c>();
    oppty = (Opportunity)std.getRecord();
    pageSize = (Integer)COMM_Constant_Setting__c.getOrgDefaults().Quote_Document_Pagesize__c;
    pageNumber = 1;
    if(oppty != null && oppty .Id != null){
	    documentList = getDocumentList();
	    getDocumentListToShow();
	    newDocumentURL = '/' + Document__c.SObjectType.getDescribe().getKeyPrefix() + '/e?retURL=/';
	    newDocumentURL += oppty.id + '&RecordType=' + COMM_Constant_Setting__c.getOrgDefaults().Opportunity_Document_RT_ID__c;
	    congaSetting = COMM_Conga_Setting__c.getOrgDefaults();
	    isAsc = true;
	    previousSortField = sortField = 'Name';
    } else {
      listSize  = 0;
    }
    //I-227047 - Nb - 08/05 - Start    
  }
  
 //----------------------------------------------------------------------------
 // Name : getDocumentList() Method returns latest version of documents by document type
 // Params : None
 // Returns : list of documents
 //----------------------------------------------------------------------------
  public List<Document__c> getDocumentList(){
    documentList = new List<Document__c>();
    documentListToShow = new List<Document__c>();
    List<Document__c> doclist = [SELECT CreatedDate,ID,Name,Approval_Status__c,  Oracle_Quote__r.BigMachines__Is_Primary__c,
                    Oracle_Quote__r.BigMachines__Opportunity__c,Document_type__c, LastModifiedBy.Name, Opportunity__r.Name,
                    Oracle_Quote__r.Name,View__c, Oracle_Quote__c,LastModifiedDate,
                    Document_Link__c
                    FROM Document__c 
                    WHERE Opportunity__c =: oppty.id OR (Oracle_Quote__r.BigMachines__Opportunity__c =: oppty.id and Oracle_Quote__r.BigMachines__Is_Primary__c = true)
                    ORDER BY CreatedDate DESC];
    Map<String,Document__c> docTypeDocumentListMap = new Map<String, Document__c>();
    Map<String,Document__c> quoteDocTypeDocumentListMap = new Map<String, Document__c>();
    List<Document__c> docsToReturn = new List<Document__c>();
    for(Document__c docs : docList){
      if(docs.Oracle_Quote__c == null){
        if(!docTypeDocumentListMap.containsKey(docs.Document_type__c)){
        /*  docTypeDocumentListMap.get(docs.Document_Type__c).add(docs);
        }else{*/
          docTypeDocumentListMap.put(docs.Document_Type__c, docs);
        }
      }else{
        if(!quoteDocTypeDocumentListMap.containsKey(docs.Document_type__c)){
          quoteDocTypeDocumentListMap.put(docs.Document_Type__c, docs);
        }
      }
    }
    if(docTypeDocumentListMap.size() > 0){
      docsToReturn.addAll(docTypeDocumentListMap.values());
    }
    if(quoteDocTypeDocumentListMap.size() > 0){
      docsToReturn.addAll(quoteDocTypeDocumentListMap.values());
    }
    if(docsToReturn.size() > 0){
      docsToReturn.sort();
      listSize = docsToReturn.size();
    }else{
      listSize = 0;
    }
    system.debug('List Size : ' + listSize);
    /*if(listSize != 0 && listSize <= 5){
      for(Integer i= 0; i < listsize; i++){
        documentListToShow.add(docsToReturn[i]);
      }
    }else if(listSize > 0){
      for(Integer i= 0; i < 5; i++){
        documentListToShow.add(docsToReturn[i]);
      }
    }*/
    return docsToReturn;
  }
    
  //----------------------------------------------------------------------------
  // Name : doDelete() Method to delete selected document
  // Params : None
  // Returns : list of documents
  //----------------------------------------------------------------------------
  public void doDelete(){
    try{
      List<Document__c> docList = [SELECT Id 
                                  FROM Document__c 
                                  WHERE ID = :selectedID];
      if(docList != null && docList.size() > 0){
          delete docList;
      }
      documentList = getDocumentList();
      pageNumber = 1;
      getDocumentListToShow();
    }Catch(DmlException ex){
      //ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Unable to Delete this record!'));
    }
  }
    
 
  //Pagination methods
  
	public Boolean hasPrevious{get{
    if(PageNumber == 1){
        return false;
    }else{
        return true;
    }
	}set;}
    
	public Boolean hasNext{get{
    if(PageNumber == 1){
	    if((listSize / pageSize  > 1)
	    || (((listSize / pageSize ) == 1)
	    && (math.mod(listSize, pageSize) != 0))){
	        system.debug('Return FROM Here');
	        return true;
	    }else{
	        system.debug('Return FROM tHere');
	        return false;
	    }
	    }else{
	    if((pageNumber > (listSize / pageSize))
	    || (pageNumber == (listSize / pageSize)
	    && (math.mod(listSize, pageSize) == 0))){
	        system.debug('Return FROM Hereee');
	        return false;
	    }else{
	        system.debug('Return FROM Here1');
	        return true;
	    }
    }
	}set;}
	
	public void next() 
	{
    pageNumber++;
    getDocumentListToShow();
	}
	
	public void previous() 
	{
    pageNumber--;
    getDocumentListToShow();
	}
	
	public void first() 
	{
    pageNumber = 1;
    getDocumentListToShow();
	}
	
	public void Last() 
	{
	  if(Math.mod(listSize,pageSize) == 0){
	    pageNumber = listSize / pageSize;
	  }else{
	    if(listSize / pageSize >= 1){
	      pageNumber = listSize / pageSize + 1;
	    }else{
	      pageNumber = 1;
	    }
	  }
	  getDocumentListToShow();
	}
  
  public List<Document__c> documentListToShow{get;set;}
  public List<Document__c> getDocumentListToShow(){
    documentListToShow = new List<Document__c>();
    if(documentList != null && documentList.size() > 0){
      if(pageNumber == 1){
        if(documentList.size() < pagesize){
          for(Integer i=0; i<documentList.size();i++){
              documentListToShow.add(documentList.get(i));
          }
        }else{
          for(Integer i=0; i<pagesize;i++){
              documentListToShow.add(documentList.get(i));
          }
        }
        system.debug('in if');
      }else{
        if((pageNumber-1) == (documentList.size() / pageSize)){
          system.debug('I am in second If');
          for(Integer i= ((pageNumber * pageSize) - pageSize); i < documentList.size(); i++){
                  documentListToShow.add(documentList.get(i));
          }
        }else{
          for(Integer i= ((pageNumber * pageSize) - pageSize); i<(pageNumber * pageSize);i++){
              documentListToShow.add(documentList.get(i));
          }
        }
      }
    }
    return documentListToShow;
  }
  
  public String sortField{get;set;}
  public String order{get;set;}
  public Boolean isAsc{get;set;}
  String previousSortField{get;set;}
  public void sortList(){
    system.debug('Sort Field : ' + sortField + ':::order::' + order + 'previousSortField' + previousSortField);
    
    isAsc = previousSortField.equals(sortField)? !isAsc : true; 
    previousSortField = sortField;
    order = isAsc ? 'ASC' : 'DESC';
    List<Document__c> resultList = new List<Document__c>();
    //Create a map that can be used for sorting 
    Map<object, List<Document__c>> objectMap = new Map<object, List<Document__c>>();
    for(Document__c ob : documentListToShow){
      if(sortField == 'LastModifiedBy'){
        if(objectMap.get(ob.LastModifiedBy.Name) == null){
          objectMap.put(ob.LastModifiedBy.Name, new List<Sobject>()); 
        }
        objectMap.get(ob.LastModifiedBy.Name).add(ob);
      }else if(sortField == 'Oracle_Quote__c'){
        if(objectMap.get(ob.Oracle_Quote__r.Name) == null){
          objectMap.put(ob.Oracle_Quote__r.Name, new List<Sobject>()); 
        }
        objectMap.get(ob.Oracle_Quote__r.Name).add(ob);
      }else{
        if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
            objectMap.put(ob.get(sortField), new List<Sobject>()); 
        }
        objectMap.get(ob.get(sortField)).add(ob);
      }
    }       
    //Sort the keys
    List<object> keys = new List<object>(objectMap.keySet());
    keys.sort();
    for(object key : keys){ 
      resultList.addAll(objectMap.get(key)); 
    }
    //Apply the sorted values to the source list
    documentListToShow.clear();
    if(order.toLowerCase() == 'asc'){
      documentListToShow.addAll(resultList);
    }else if(order.toLowerCase() == 'desc'){
      for(integer i = resultList.size()-1; i >= 0; i--){
        documentListToShow.add(resultList[i]);  
      }
    }
    
  }
}