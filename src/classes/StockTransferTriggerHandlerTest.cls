/**================================================================      
* Appirio, Inc
* Name: StockTransferTriggerHandlerTest
* Description: Test class for StockTransferTriggerHandler
* Created Date: 10 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class StockTransferTriggerHandlerTest {
    static SVMXC__Stock_Transfer__c stockTransfer;
    @isTest static void testfirst() {
        createData();
        Test.startTest();
        insert stockTransfer;
        Test.stopTest();
        System.assertEquals([SELECT Integration_Status__c FROM SVMXC__Stock_Transfer__c WHERE Id =:stockTransfer.Id].Integration_Status__c, 'New');
    }
    @isTest static void testSecond() {
        createData();
        insert stockTransfer;
        stockTransfer.Integration_Status__c = 'InProgress';
        Test.startTest();
        update stockTransfer;
        Test.stopTest();
        System.assertEquals([SELECT ReqFromTechEmail__c FROM SVMXC__Stock_Transfer__c WHERE Id =:stockTransfer.Id].ReqFromTechEmail__c, 'test@test.com');
    }
    
    private static void createData() {
        SVMXC__Site__c locationRecord = new SVMXC__Site__c(SVMXC__Stocking_Location__c = true);
        insert locationRecord;
        SVMXC__Service_Group__c sgroup = new SVMXC__Service_Group__c();
        insert sgroup;
        SVMXC__Service_Group_Members__c stechnician = new SVMXC__Service_Group_Members__c(
            SVMXC__Inventory_Location__c = locationRecord.Id,
            SVMXC__Email__c = 'test@test.com',
            SVMXC__Service_Group__c = sgroup.Id);
        insert stechnician;
        SVMXC__Site__c deslocationRecord = new SVMXC__Site__c(SVMXC__Stocking_Location__c = true);
        insert deslocationRecord;
        SVMXC__Service_Group_Members__c dtechnician = new SVMXC__Service_Group_Members__c(
            SVMXC__Inventory_Location__c = deslocationRecord.Id,
            SVMXC__Email__c = 'test@test.com',
            SVMXC__Service_Group__c = sgroup.Id);
        insert dtechnician;        
        stockTransfer = new SVMXC__Stock_Transfer__c(
            SVMXC__Source_Location__c = locationRecord.Id,
            SVMXC__Destination_Location__c = deslocationRecord.Id);
    }
}