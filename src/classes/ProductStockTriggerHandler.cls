/**================================================================      
* Appirio, Inc
* Name: ProductStokeTriggerHandler
* Description: ProductStockTriggerHandler for T-539940
* Created Date: 22 - Sept - 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update

==================================================================*/
public class ProductStockTriggerHandler{
    public static void onAfterUpdate(List<SVMXC__Product_Stock__c> items,Map<Id,SVMXC__Product_Stock__c> oldMap){
        CreatePartRequests(items, oldMap);
        CallApprovalProcess(items, oldMap);
    }
    
    //Create part order and part order line when approved 
    private static void CreatePartRequests(List<SVMXC__Product_Stock__c> items,Map<Id,SVMXC__Product_Stock__c> oldMap){
        List<SVMXC__RMA_Shipment_Order__c> partReqs = new List<SVMXC__RMA_Shipment_Order__c>();
        List<SVMXC__RMA_Shipment_Line__c> partReqLines = new List<SVMXC__RMA_Shipment_Line__c>();
        Id RecordTypeOrder = [Select Id,SobjectType,Name From RecordType where Name ='Shipment' and SobjectType ='SVMXC__RMA_Shipment_Order__c'  limit 1].Id;
        Id RecordTypeOrderLine = [Select Id,SobjectType,Name From RecordType where Name ='Shipment' and SobjectType ='SVMXC__RMA_Shipment_Line__c'  limit 1].Id;
        for(SVMXC__Product_Stock__c item : items){
            SVMXC__Product_Stock__c oldItem;
            if(oldMap != null){
                oldItem = oldMap.get(item.Id);
            }
            if(RecordTypeOrder != null 
                    && RecordTypeOrderLine != null 
                    && (oldItem == null || item.Approval_for_New_Inventory__c != oldItem.Approval_for_New_Inventory__c) 
                    && item.Approval_for_New_Inventory__c == 'Approved' 
                    && item.SVMXC__Location__c != null){
                SVMXC__RMA_Shipment_Order__c newReq = new SVMXC__RMA_Shipment_Order__c();
                newReq.Product_Stock__c = item.Id;
                newReq.SVMXC__Destination_Location__c = item.SVMXC__Location__c;
                newReq.RecordTypeId = RecordTypeOrder;
                newReq.SVMXC__Order_Status__c = 'Open';
                newReq.SVMXC__Company__c = item.SVMXC__Location__r.SVMXC__Account__c;
                //newreq.SVMXC__Contact__c = 
                partReqs.add(newReq);
                SVMXC__RMA_Shipment_Line__c newLine = new SVMXC__RMA_Shipment_Line__c();
                newLine.SVMXC__Line_Status__c = 'Open';
                newLine.SVMXC__Product__c = item.SVMXC__Product__c;
                newLine.SVMXC__Expected_Quantity2__c = item.Maximum_Qty__c;
                newLine.RecordTypeId = RecordTypeOrderLine;
                partReqLines.add(newLine);
            }
        }
        if(partReqs.size() > 0){
            insert partReqs;
        }
        for(integer i = 0; i<partReqs.Size() ; i++){
            partReqLines[i].SVMXC__RMA_Shipment_Order__c = partReqs[i].Id;
        }
        if(partReqLines.size() > 0){
            insert partReqLines;
        }
    }
    
    // Call approval process when create by SFM.T-539620
    private static void CallApprovalProcess(List<SVMXC__Product_Stock__c> items,Map<Id,SVMXC__Product_Stock__c> oldMap){
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        for(SVMXC__Product_Stock__c item : items){
            SVMXC__Product_Stock__c oldItem;
            if(oldMap != null){
                oldItem = oldMap.get(item.Id);
            }
            if((oldItem == null || item.Approval_for_New_Inventory__c != oldItem.Approval_for_New_Inventory__c) 
                    && oldItem.Approval_for_New_Inventory__c == 'New' 
                    && item.Approval_for_New_Inventory__c == 'Requested'){
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval for SFM.');
                req1.setObjectId(item.Id);
                
                // Submit on behalf of a specific submitter
                req1.setSubmitterId(item.CreatedById); 
                
                // Submit the record to specific process and skip the criteria evaluation
                req1.setProcessDefinitionNameOrId('Product_Stock_Record_Approval');
                req1.setSkipEntryCriteria(false);
                requests.add(req1);
            }
        }
        // Submit the approval request for the account
        if(requests.size() > 0)
        {
            Approval.process(requests);
        }
    }
}