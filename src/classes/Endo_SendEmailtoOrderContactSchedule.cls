//
// (c) 2016 Appirio, Inc.
//
// Class Endo_SendEmailtoOrderContactSchedule
//
// June 30,2016  Pratibha Chhimpa (Appirio)  T-497178 (Original)  
global class Endo_SendEmailtoOrderContactSchedule implements Schedulable {
   global void execute(SchedulableContext sc) {
      Endo_SendEmailtoOrderContact obj = new Endo_SendEmailtoOrderContact();
      Id batchInstanceId = Database.executeBatch(obj); 
   }
}