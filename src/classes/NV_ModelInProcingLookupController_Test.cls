//
// (c) 2015 Appirio, Inc.
//
// Apex Test Class Name: NV_ModelInProcingLookupController_Test
// For Apex Class: NV_ModelInProcingLookupController
//
// 08th January 2015   Hemendra Singh Bhati   Original (Task # T-459198)
//
@isTest(seeAllData=false)
private class NV_ModelInProcingLookupController_Test {
  // Private Data Members.

  /*
  @method      : validateControllerFunctionality
  @description : This method validates controller functionality.
  @params      : void
  @returns     : void
  */
  private static testMethod void validateControllerFunctionality() {
    // Inserting Custom Setting Data.
    insert new List<NV_Product_Pricing_Settings__c> {
      new NV_Product_Pricing_Settings__c(
        Name = 'ERPCode',
        Value__c = 'EMPR'
      ),
      new NV_Product_Pricing_Settings__c(
        Name = 'BusSegCode',
        Value__c = 'Test'
      ),
      new NV_Product_Pricing_Settings__c(
        Name = 'OrgUnitID',
        Value__c = 'NV_US'
      ),
      new NV_Product_Pricing_Settings__c(
        Name = 'ConfigName',
        Value__c = 'Global'
      ),
      new NV_Product_Pricing_Settings__c(
        Name = 'CurrencyCode',
        Value__c = 'USD'
      ),
      new NV_Product_Pricing_Settings__c(
        Name = 'Default Effective Date',
        Value__c = '2015-12-15'
      ),
      new NV_Product_Pricing_Settings__c(
        Name = 'Default Model Date',
        Value__c = '2015-12-15'
      )
    };

    // Test Case 1 - Validating Method Named "pricingLookupCallout".
    NV_ModelInProcingLookupController theInstance = new NV_ModelInProcingLookupController();
    theInstance.EffectiveDate = DateTime.now();
    theInstance.ModelDate = DateTime.now();
    theInstance.pricingLookupCallout();
	}
}