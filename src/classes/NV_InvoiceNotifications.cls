//
// (c) 2015 Appirio, Inc.
//
// Apex Class Name: NV_InvoiceNotifications
// Description: Implements the following:
//  1: This apex class method named "notifyOrdersPendingPO" is called from a scheduled apex job class named "NV_NotifyOrdersPendingPO"
//     for processing "Awaiting PO" orders and notifying account owners and users following the order record.
//  2: This apex class method named "notifyOrdersPastDue" is called from a scheduled apex job class named "NV_NotifyOrdersPastDue"
//     for processing orders that has passed due date and notifying account owners and users following the order record.
//
// 16th December 2015   Hemendra Singh Bhati   Original (Task # T-458395)
// 18th December 2015   Hemendra Singh Bhati   Modified (Task # T-458396) - Please see the task description for more details.
// 21st December 2015   Hemendra Singh Bhati   Modified (Task # T-459199) - Please see the task description for more details.
// 05th January 2015    Hemendra Singh Bhati   Modified (Task # T-462574) - Please see the task description for more details.
// Modified By :   Jessica Schilling
// Date        :   5/02/2016
// Case        :   Case 166348
//
public class NV_InvoiceNotifications {
  // Private Data Members.
  private final Set<String> ORDER_TYPES = new Set<String> {
    'Customer Bill Only',
    'RepStock - Sale'
  };
  private final String ORDER_STATUS = 'Awaiting PO';
  private final Id ORDER_NV_BILL_ONLY_RECORD_TYPE_ID = Schema.SObjectType.Order.RecordTypeInfosByName.get('NV Bill Only').RecordTypeId;

  /*
  @method      : notifyOrdersPastDue
  @description : This method determines the order records to process and notifies the account owners and
                 order record followers that the order has passed the due date.
  @params      : void
  @returns     : void
  */
  public void notifyOrdersPastDue() {
    // Processing past due orders.
    determineOrdersToProcess(
      'notifyOrdersPastDue',
      'This order is THE_DATE_DIFFERENCE days past due.\n\n'
    );
  }

  /*
  @method      : notifyOrdersPendingPO
  @description : This method determines the order records to process and notifies the account owners and
                 order record followers about awaiting PO orders.
  @params      : void
  @returns     : void
  */
  public void notifyOrdersPendingPO() {
    // Processing awaiting PO orders.
    determineOrdersToProcess(
      'notifyOrdersPendingPO',
      //START JSchilling Case 166348 5/02/2016
      //Changed notification syntax
      //'This order has been Awaiting PO for THE_DATE_DIFFERENCE days.\n\n'
      'This order for THE_ACCOUNT hospital has been pending a Bill Only PO for THE_DATE_DIFFERENCE days.\n\n'
      //END JSchilling Case 166348 5/02/2016
    );
  }

  /*
  @method      : determineOrdersToProcess
  @description : This method determines the order records to be processed by apex schedular.
  @params      : void
  @returns     : void
  */
  public void determineOrdersToProcess(String theType, String theMessage) {
    // Validating data passed to this method.
    if(String.isBlank(theType) || String.isBlank(theMessage)) {
      return;
    }

    // Determining the SOQL query to execute.
    String theSOQLQuery = '';
    if(theType.equalsIgnoreCase('notifyOrdersPendingPO')) {
      //JSchilling Case 166348 5/02/2016
      //Added Account.name to query below
      theSOQLQuery = 'SELECT Id, Days_Pending_PO__c, Account.OwnerId, Account.Name FROM Order WHERE isDeleted = false AND Type IN :ORDER_TYPES AND Status = \'' + ORDER_STATUS + '\' ';
      theSOQLQuery += 'AND RecordTypeId = \'' + ORDER_NV_BILL_ONLY_RECORD_TYPE_ID + '\'';
    }
    else if(theType.equalsIgnoreCase('notifyOrdersPastDue')) {
      theSOQLQuery = 'SELECT Order__r.Id, Order__r.Account.OwnerId, Order__r.Type, Transaction_Status__c, Invoice_Due_Date__c FROM Order_Invoice__c ';
      theSOQLQuery += 'WHERE isDeleted = false AND Order__c != null AND Order__r.Type IN :ORDER_TYPES AND Invoice_Due_Date__c != null AND Transaction_Status__c != \'Closed\'';
      theSOQLQuery += 'AND Invoice_Due_Date__c < TODAY ORDER BY CreatedDate ASC';
    }
    else {
      return;
    }

    // Extracting order records to be processed.
    Integer theDateDifference = 0;
    Date theDateOrdered = null;
    Map<Id, Integer> ordersToBeProcessed = new Map<Id, Integer>();
    //START JSchilling Case 166348 5/02/2016
    //Added string and map to hold the Order's Account name
    String theAccountName = '';
    Map<Id, String> orderAccount = new Map<Id, String>();
    //END JSchilling Case 166348 5/02/2016
    Map<Id, Set<Id>> theOrderAndItsFollowers = new Map<Id, Set<Id>>();
    if(theType.equalsIgnoreCase('notifyOrdersPendingPO')) {
      for(Order theOrder : (List<Order>)Database.query(theSOQLQuery)) {
        theDateDifference = Integer.valueOf(theOrder.Days_Pending_PO__c);
        //START JSchilling Case 166348 5/02/2016
        //Added to map the Order's Id to its Account name
        if(theOrder.Account.Name != null){
          theAccountName = theOrder.Account.Name;
          orderAccount.put(theOrder.Id, theAccountName);
        }
        //END JSchilling Case 166348 5/02/2016
        if(theDateDifference > 0 && Math.mod(theDateDifference, 10) == 0) {
          ordersToBeProcessed.put(theOrder.Id, theDateDifference);
          theOrderAndItsFollowers.put(
            theOrder.Id,
            new Set<Id> {
              theOrder.Account.OwnerId
            }
          );
        }
      }
    }
    else if(theType.equalsIgnoreCase('notifyOrdersPastDue')) {
      for(Order_Invoice__c theOrderInvoice : (List<Order_Invoice__c>)Database.query(theSOQLQuery)) {
        theDateDifference = theOrderInvoice.Invoice_Due_Date__c.daysBetween(Date.today());
        if(theDateDifference > 0 && Math.mod(theDateDifference, 10) == 0) {
          ordersToBeProcessed.put(theOrderInvoice.Order__r.Id, theDateDifference);
          theOrderAndItsFollowers.put(
            theOrderInvoice.Order__r.Id,
            new Set<Id> {
              theOrderInvoice.Order__r.Account.OwnerId
            }
          );
        }
      }
    }
    system.debug('TRACE: NV_InvoiceNotifications - determineOrdersToProcess - ordersToBeProcessed - ' + ordersToBeProcessed);

    if(ordersToBeProcessed.size() > 0) {
      // Determing order record followers for all the orders filtered above.
      
      for(NV_Follow__c theFollower : [SELECT Record_Id__c, Following_User__c FROM NV_Follow__c WHERE
                                      Record_Id__c IN :ordersToBeProcessed.keySet() AND Following_User__c != null]) {
        theOrderAndItsFollowers.get(theFollower.Record_Id__c).add(theFollower.Following_User__c);
      }
      system.debug('TRACE: NV_InvoiceNotifications - determineOrdersToProcess - theOrderAndItsFollowers - ' + theOrderAndItsFollowers);

      // Notifying the account owners and order record followers about awaiting PO orders.
      if(theOrderAndItsFollowers.size() > 0 && !Test.isRunningTest()) {
        //JSchilling Case 166348 5/02/2016
        //Added new parameter, orderAccount
        notifyUsers(theOrderAndItsFollowers, ordersToBeProcessed, orderAccount, theMessage);
      }
    }
  }

  /*
  @method      : notifyUsers
  @description : This method posts a chatter feed on order record mentioning the order account owner and followers about awaiting PO orders.
  @params      : Map<Id, Set<Id>> theOrderAndItsFollowers, Map<Id, Integer> ordersToBeProcessed, String theMessage
  @returns     : void
  */
  //JSchilling Case 166348 5/02/2016
  //Added new parameter for orderAccount map
  public void notifyUsers(Map<Id, Set<Id>> theOrderAndItsFollowers, Map<Id, Integer> ordersToBeProcessed, Map<Id, String> orderAccount, String theMessage) {
    ConnectApi.FeedItemInput theInput = null;
    ConnectApi.MessageBodyInput theBody = null;
    ConnectApi.TextSegmentInput theTextMessage = null;
    ConnectApi.BatchInput theBatchInput = null;
    ConnectApi.MentionSegmentInput theMentionSegment = null;
    List<ConnectApi.BatchInput> theBatchInputs = new List<ConnectApi.BatchInput>();
    for(Id theOrderId : theOrderAndItsFollowers.keySet()) {
      // The feed input section starts.
      theInput = new ConnectApi.FeedItemInput();
      theInput.SubjectId = theOrderId;

      // The feed body section starts.
      theBody = new ConnectApi.MessageBodyInput();
      theBody.messageSegments = new List<ConnectApi.MessageSegmentInput>();

      // The feed message section starts.
      theTextMessage = new ConnectApi.TextSegmentInput();
      //START JSchilling Case 166348 5/02/2016
      //Added functionality to include the Account name in notification message, in addition to the number of days. Commented out old way of doing this
      String temp = theMessage;
      temp = temp.replace('THE_DATE_DIFFERENCE', String.valueOf(ordersToBeProcessed.get(theOrderId)));
      if(theMessage.contains('THE_ACCOUNT')){
        temp = temp.replace('THE_ACCOUNT', String.valueOf(orderAccount.get(theOrderId)));
      }
      theTextMessage.text = temp;
      /*theTextMessage.text = theMessage.replace(
        'THE_DATE_DIFFERENCE',
        String.valueOf(ordersToBeProcessed.get(theOrderId))
      );*/
      //END JSchilling Case 166348 5/02/2016
      theBody.messageSegments.add(theTextMessage);
      // The feed message section ends.

      // The feed mentioning section starts.
      for(Id theUserId : theOrderAndItsFollowers.get(theOrderId)) {
        theMentionSegment = new ConnectApi.MentionSegmentInput();
        theMentionSegment.Id = theUserId;
        theBody.messageSegments.add(theMentionSegment);

        theTextMessage = new ConnectApi.TextSegmentInput();
        theTextMessage.text = '\n';
        theBody.messageSegments.add(theTextMessage);
      }
      // The feed mentioning section ends.
      // The feed body section ends.

      theInput.body = theBody;
      // The feed input section ends.

      theBatchInput = new ConnectApi.BatchInput(theInput);
      theBatchInputs.add(theBatchInput);
    }

    // Batch posting chatter feeds.
    ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), theBatchInputs);
  }
}