// (c) 2015 Appirio, Inc.
//
// Class Name: StrategicAccountIndicatorExtensionTest
// Description: Test Class for StrategicAccountIndicatorExtension
// July 01 2016, Prakarsh Jain  Original (T-516847)
//
@isTest
public class StrategicAccountIndicatorExtensionTest {
	public static Account acc;
  	public static Strategic_Account__c stratAccount;
    
    //Checks for no strategic accounts
    static testMethod void noStrategic(){
        createTestData();
        PageReference pageRef = Page.StrategicAccountIndicatorPage;
    	Test.setCurrentPage(pageRef);  
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);	
        StrategicAccountIndicatorExtension makeRemoveStratAccount = new StrategicAccountIndicatorExtension(sc);  
    	makeRemoveStratAccount.hasStrategic();
        system.assertEquals(makeRemoveStratAccount.isStrategic, false);
    }
    
    //Checks if account has strategic accounts
    static testMethod void strategic(){
        createTestData();
        stratAccount = new Strategic_Account__c();      
    	stratAccount.Account__c = acc.Id;
    	stratAccount.User__c = UserInfo.getUserId();
    	insert stratAccount;
        PageReference pageRef = Page.StrategicAccountIndicatorPage;
    	Test.setCurrentPage(pageRef);  
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);	
        StrategicAccountIndicatorExtension makeRemoveStratAccount = new StrategicAccountIndicatorExtension(sc);  
    	makeRemoveStratAccount.hasStrategic();
        system.assertEquals(makeRemoveStratAccount.isStrategic, true);
    }
    
    public static void createTestData(){
    	acc = TestUtils.createAccount(1, true).get(0);
  	}
}