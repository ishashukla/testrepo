/**================================================================      
* Appirio, Inc
* Name: PMWorkOrderTemplateController
* Description: Controller for Component (T-506478)
* Created Date: 14 - Sept - 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update

==================================================================*/
public without sharing class PMWorkOrderTemplateController {
    public Id AccountId{get;set;}
    public List<SVMXC__Service_Order__c> RelatedWo{
        get{
            List<string> woStatus = new List<string>{'Closed','Canceled','Rejected'};
            List<SVMXC__Service_Order__c> listServiceOrder = [Select Id,Name,SVMXC__Group_Member__r.SVMXC__Phone__c,OR_Room__c,SVMXC__Component__r.Name,SVMXC__Group_Member__r.Name,Due_Date__c,
            SVMXC__Contact__r.Name from SVMXC__Service_Order__c where SVMXC__Company__c = :AccountId and RecordType.Name = 'Preventive Maintenance' 
            and Due_Date__c >= TODAY and SVMXC__Order_Status__c not in :woStatus];
            return listServiceOrder;
        }
        set;}
    

}