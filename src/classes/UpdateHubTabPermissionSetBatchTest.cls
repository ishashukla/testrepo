/**================================================================      
 * Appirio, Inc
 * Name: UpdateVaultTabPermissionSetBatchTest
 * Description: Test Class for UpdateHubTabPermissionSetBatch(T-516615)
 * Created Date: [07/01/2016]
 * Created By: [Isha Shukla] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/ 
@isTest
private class UpdateHubTabPermissionSetBatchTest {
    static List<User> userList;
    static PermissionSetAssignment oldPermissionSetAccess;
    // Testing for tab access to users in permission set 
    @isTest static void testFirst() {
        createData();
        Test.startTest();
        UpdateHubTabPermissionSetBatch obj = new UpdateHubTabPermissionSetBatch();
        Database.executeBatch(obj);
        Test.stopTest();
        System.assertEquals([SELECT AssigneeId FROM PermissionSetAssignment WHERE AssigneeId =  :userList[1].Id] != Null ,True);
    }
    // creates test data
    @isTest static void createData() {
        Profile pr = TestUtils.fetchProfile('Instruments Sales User');
        userList = TestUtils.createUser(4, pr.Name, false);
        userList[0].Division = 'NSE';
        userList[1].Division = 'IVS';
        insert userList;
        User adminUser = TestUtils.createUser(1,'System Administrator', true).get(0);
        System.runAs(adminUser) {
          myFunc2();
            //Id psaId = [SELECT Id FROM PermissionSet WHERE Name = 'Access_To_The_Hub_Tab_For_Instruments_User_with_IVS_BU' LIMIT 1].Id;
            //oldPermissionSetAccess = new PermissionSetAssignment(PermissionSetId = psaId , AssigneeId = userList[0].Id);
            //insert oldPermissionSetAccess;
            //System.debug(oldPermissionSetAccess+'oldPermissionSetAccess');
        }
        userList[2].Division = 'IVS';
        update userList;
    }
   // @future
    private static void myFunc2()
    {
            Id psaId = [SELECT Id FROM PermissionSet WHERE Name = 'Access_To_The_Hub_Tab_For_Instruments_User_with_IVS_BU' LIMIT 1].Id;
            oldPermissionSetAccess = new PermissionSetAssignment(PermissionSetId = psaId , AssigneeId = userList[0].Id);
            insert oldPermissionSetAccess;
    }
}