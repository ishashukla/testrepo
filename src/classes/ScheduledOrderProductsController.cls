/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : Controller class for ScheduledOrderProducts Component, this component has been
//                  used in VF email template
//
// Nov 26, 2015    Megha Agarwal (Appirio)
//***************************************************************************/
public class ScheduledOrderProductsController {
    public String orderId {get;set;}
    public String receivingConId {get;set;}
    public Contact receivingContact {get{
       if(receivingContact == null && receivingConId != null){
            for(Contact con : [select id, FirstName, Name from Contact where id  =: receivingConId]){
                return con;
            }
            if(receivingContact == null){
                return new Contact();
            }
        }return receivingContact; }set; }

    public Order orderObj {get{
        System.debug('orderObj>>'+orderObj);
        System.debug('orderId>>'+orderId);
       if(orderObj == null && orderId != null){
            for(Order o : [select id, OrderNumber ,Type, EffectiveDate, TotalAmount , PoNumber, OrderReferenceNumber,
                           Ship_To_Account__r.ShippingCity, Ship_To_Account__r.ShippingCountry, Ship_To_Account__r.ShippingCountryCode,
                           Ship_To_Account__r.ShippingPostalCode, Ship_To_Account__r.ShippingState, Ship_To_Account__r.ShippingStreet,
                           Bill_TO_Account__r.ShippingCity, Bill_TO_Account__r.ShippingCountry, Bill_TO_Account__r.ShippingCountryCode,
                            Bill_TO_Account__r.ShippingPostalCode,Bill_TO_Account__r.ShippingState,Bill_TO_Account__r.ShippingStreet,
                            (Select id, Quantity, Description, Request_Date__c from OrderItems)
                            from Order where id = : orderId]){ /*BillingCity, BillingCountry, BillingCountryCode, BillingPostalCode, BillingState, BillingStreet, ShippingCity, ShippingCountry, ShippingCountryCode,
                            ShippingPostalCode,ShippingState,ShippingStreet,*/
                return o;
            }
            if(orderObj == null){
                return new Order();
            }
        }return orderObj; }set;}
}