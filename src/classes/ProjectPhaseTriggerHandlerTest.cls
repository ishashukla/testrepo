// 
// (c) 2016 Appirio, Inc.
//
// Name : ProjectPhaseTriggerHandlerTest 
// Test Class for ProjectPhaseTriggerHandler
//
// 02nd Jun 2016      Rahul Aeran     (T-501829) Original

@isTest
private class ProjectPhaseTriggerHandlerTest {
  static List<Project_Plan__c> lstProjectPlans;
  static testMethod void testRollUpOnProjectPlan(){
    createTestData();
    Test.startTest();
      List<Project_Phase__c> lstPhases1 = TestUtils.createProjectPhase(5,lstProjectPlans.get(0).Id,false);
      List<Project_Phase__c> lstPhases2 = TestUtils.createProjectPhase(4,lstProjectPlans.get(1).Id,false);
      List<Project_Phase__c> lstAll = new List<Project_Phase__c>();
      lstAll.addAll(lstPhases1);
      lstAll.addAll(lstPhases2);
      insert lstAll;
      List<Project_Plan__c> lstPlan = [SELECT Id,Number_of_Phases__c FROM Project_Plan__c 
                                          WHERE Id = :lstProjectPlans.get(0).Id];
      System.assert(lstPlan.get(0).Number_of_Phases__c == lstPhases1.size() , 'The roll has been populated');
      
      lstPlan = [SELECT Id,Number_of_Phases__c FROM Project_Plan__c 
                                          WHERE Id = :lstProjectPlans.get(1).Id]; 
      System.assert(lstPlan.get(0).Number_of_Phases__c == lstPhases2.size() , 'The roll has been populated');
      
      lstPhases2.get(0).Project_Plan__c = lstProjectPlans.get(0).Id;
      update lstPhases2;
      lstPlan = [SELECT Id,Number_of_Phases__c FROM Project_Plan__c 
                                          WHERE Id = :lstProjectPlans.get(0).Id];
      System.assert(lstPlan.get(0).Number_of_Phases__c == (lstPhases1.size() + 1), 'The roll has been updated');
      
      delete (new List<Project_Phase__c>{lstPhases2.get(0)});
      lstPlan = [SELECT Id,Number_of_Phases__c FROM Project_Plan__c 
                                          WHERE Id = :lstProjectPlans.get(0).Id];
      System.assert(lstPlan.get(0).Number_of_Phases__c == (lstPhases1.size()), 'The roll has been updated');
      
    Test.stopTest();
  }
  
  static void createTestData(){
    List<User> userList = TestUtils.createUser(2, Constants.ADMIN_PROFILE , true);
    List<Account> accountList = TestUtils.createAccount(2,true);
    //Insert scenario
    lstProjectPlans =  TestUtils.createProjectPlan(2, accountList[0].Id, false);
    insert lstProjectPlans;
  }
}