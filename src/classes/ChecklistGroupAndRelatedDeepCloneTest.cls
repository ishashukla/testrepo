@isTest
private class ChecklistGroupAndRelatedDeepCloneTest {

	private static testMethod void clonegroup() {
    List<Account> accList = TestUtils.createAccount(1, true);
      Contact con = TestUtils.createCOMMContact(accList.get(0).Id, 'Test', true);
      List<Case> caseList = TestUtils.createCase(1, accList.get(0).Id, false);
      Schema.DescribeSObjectResult caseRT = Schema.SObjectType.Case; 
      Map<String,Schema.RecordTypeInfo> caseRTMap = caseRT.getRecordTypeInfosByName(); 
      Id caseRecordTypeId = caseRTMap.get(Constants.COMM_CASE_RTYPE).getRecordTypeId();
      TestUtils.createRTMapCS(caseRecordTypeId, Constants.COMM_CASE_RTYPE,Constants.COMM);
      insert caseList;
     
      List<ChecklistCUST__c> checkList = TestUtils.createCustomChecklist(1, 'Test',1,true);
      List<Checklist_group__c> checkGroup = TestUtils.createChecklistGroup(1,'Test Group',checkList.get(0).id,1,true);
      List<Checklist_Item__c> checkItem = TestUtils.createChecklistItem(1,'Test Item',checkList.get(0).id,checkGroup.get(0).id,true,true);
      ChecklistGroupAndRelatedDeepClone.cloneChecklist(checkGroup.get(0).id);
	}

}