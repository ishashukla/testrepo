@isTest
private class NV_OrderTriggerHandlerTest {
	
	@testSetup static void setup() {
		Profile sysAdminProfile = NV_Utility.getProfileByName('System Administrator');
		User testUser1 = NV_TestUtility.createUser('testuser_s1@mail.com', 'testuser_s1@mail.com', 
														sysAdminProfile.Id, null, '1', true);		
        Map<String, Id> accountRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');
        Account testAccount = NV_TestUtility.createAccount('TestAccount',accountRecordTypes.get('NV_Customer'), true);
        Contract testContract = NV_TestUtility.createContract(testAccount.Id, true);
		testContract.Status = 'Activated';
		update testContract;
    }

	@isTest static void testOrderTriggerBeforeInsertUpdate() {

		Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
		Contract testContract = [SELECT Id FROM Contract WHERE AccountId =: testAccount.Id LIMIT 1];

		Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
		Order testOrder1 = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, 
														orderRecordTypes.get('NV_Bill_Only'), false);
		testOrder1.Customer_PO__c = 'PO00001';
		testOrder1.EffectiveDate = Date.today();
		testOrder1.Date_Ordered__c = Datetime.now();
		testOrder1.Status = 'Booked';
		insert testOrder1;

		testOrder1 = [SELECT Id, OrderNumber, Closed_Date__c FROM Order WHERE Id =: testOrder1.Id LIMIT 1];
		System.assertEquals(null, testOrder1.Closed_Date__c);
		testOrder1.Status = 'Closed';
		update testOrder1;

		testOrder1 = [SELECT Id, OrderNumber, Closed_Date__c FROM Order WHERE Id =: testOrder1.Id LIMIT 1];
		System.assertEquals(Date.today(), testOrder1.Closed_Date__c);
	}
	
	@isTest static void testOrderTriggerAfterInsertUpdate() {
		Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
		Contract testContract = [SELECT Id FROM Contract WHERE AccountId =: testAccount.Id LIMIT 1];

		Notification_Settings__c userSettings = NV_NotificationSettingsController.getCurrentUserSettings();
		userSettings.Shipped__c = true;
		NV_NotificationSettingsController.setCurrentUserSettings(userSettings);

		Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
		NV_OrderDetail.changeFollowRecord(testAccount.Id, true);
		System.assertEquals(1, [SELECT count() FROM NV_Follow__c]);
		Order testOrder1 = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, 
														orderRecordTypes.get('NV_Standard'), false);
		testOrder1.Customer_PO__c = 'PO00001';
		testOrder1.EffectiveDate = Date.today();
		testOrder1.Date_Ordered__c = Datetime.now();
		testOrder1.Status = 'Booked';
		insert testOrder1;
		System.assertEquals(2, [SELECT count() FROM NV_Follow__c]);

		testOrder1.Status = 'Shipped';
		testOrder1.Hold_Type__c = 'On Hold';
		update testOrder1;

		testOrder1.Hold_Type__c = null;
		update testOrder1;

		delete testOrder1;
		System.assertEquals(1, [SELECT count() FROM NV_Follow__c]);
	}

	@isTest static void testOrderAllActiveOrderCase() {
		Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
		Contract testContract = [SELECT Id FROM Contract WHERE AccountId =: testAccount.Id LIMIT 1];

		Notification_Settings__c userSettings1 = NV_NotificationSettingsController.getCurrentUserSettings();
		userSettings1.Shipped__c = true;
		userSettings1.Booked__c = true;
		NV_NotificationSettingsController.setCurrentUserSettings(userSettings1);

		//Change Account Owner
		User testUser1 = [SELECT Id FROM User WHERE Username = 'testuser_s1@mail.com' LIMIT 1];
		Notification_Settings__c userSettings2 = NV_Utility.getNotificationSettingForUser(testUser1.Id);
		userSettings2.Shipped__c = true;
		userSettings2.Booked__c = true;
		userSettings2.All_Active_Orders__c = true;
		update userSettings2;

		testAccount.OwnerId = testUser1.Id;
		update testAccount;

		Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
		NV_OrderDetail.changeFollowRecord(testAccount.Id, true);
		System.assertEquals(1, [SELECT count() FROM NV_Follow__c]);
		Order testOrder1 = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, 
														orderRecordTypes.get('NV_Standard'), false);
		testOrder1.Customer_PO__c = 'PO00001';
		testOrder1.EffectiveDate = Date.today();
		testOrder1.Date_Ordered__c = Datetime.now();
		testOrder1.Status = 'Booked';
		insert testOrder1;
		System.assertEquals(2, [SELECT count() FROM NV_Follow__c]);

		testOrder1.Status = 'Shipped';
		testOrder1.Hold_Type__c = 'On Hold';
		update testOrder1;

		testOrder1.Hold_Type__c = null;
		update testOrder1;
	}
	
}