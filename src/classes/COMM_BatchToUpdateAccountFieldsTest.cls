/**================================================================      
* Appirio, Inc
* Name: COMM_BatchToUpdateAccountFieldsTest
* Description: Test class of COMM_BatchToUpdateAccountFields
* Created Date: 29th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 
@isTest
private class COMM_BatchToUpdateAccountFieldsTest
{
	@isTest
	static void itShouldUpdateAccountFields()
	{
		Integer i = 0;	
		List<User> userRecList = TestUtils.createUser(5, Constants.ADMIN_PROFILE , false);
		for(User user : userRecList){
			i++;
			user.Sales_Rep_ID__c = '12345' + i;
		}
		insert userRecList;

		List<Account> acc = TestUtils.createAccount(2, false);
        acc[0].RecordTypeId = getRecordTypeId(Label.Endo_COMM_Account_RT_Label,'Account');
        acc[1].RecordTypeId = getRecordTypeId(Label.Endo_COMM_Account_RT_Label,'Account');
        acc[1].OwnerId = userRecList[0].Id;
        acc[0].OwnerId = userRecList[1].Id;
        acc[0].Endo_Active__c = true;
        acc[0].COMM_Sales_Rep__c = userRecList[2].Id;
        acc[1].Level__c = Constants.FACILITY_LEVEL;
        acc[1].Hold_Status__c = 'Credit Hold';
        acc[1].Name = Constants.FACILITY_LEVEL + 'Nick';
        acc[1].Endo_Active__c = true;
        insert acc;

		Test.startTest();    
        	Database.executeBatch(new COMM_BatchToUpdateAccountFields());
        Test.stopTest();
	}

	private static Id getRecordTypeId(String recordType, String objectType) {
        Map<String,Schema.RecordTypeInfo> rtMapByName = null;
        if(rtMapByName == null) {
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
      Schema.SObjectType ctype = gd.get(objectType);
        Schema.DescribeSObjectResult d1 = ctype.getDescribe();
        rtMapByName = d1.getRecordTypeInfosByName();
      }
      Schema.RecordTypeInfo recordTypeDetail = rtMapByName.get(recordType);
      if(recordTypeDetail != null) {
        return recordTypeDetail.getRecordTypeId();
      } else {
        return null;
      }
    } 
}