/*******************************************************************************
 Author        :  Appirio JDC (Dipika Gupta)
 Date          :  Dec 13, 2013
 Purpose       :  Evaluate functionality of IntelListComponentController class.
 Reference     :  Task T-217665, T-218030, T-218031 
*******************************************************************************/
@isTest
private class IntelListComponentController_Test {
	
	// A Method for test saveReply method
	public static testmethod void testSaveReply(){
    Test.startTest();		    
	    //Create test records
	    
	    Account acc = TestUtils.createAccount(1, true).get(0);
	    Contact con = TestUtils.createContact(1, acc.Id, true).get(0);
	    Product2 prod = TestUtils.createProduct(1, true).get(0);
        List<Intel__c> intelList = TestUtils.createIntel(1, true);
	    Tag__c tag1 = new Tag__c(Account__c = acc.Id, Intel__c = intelList.get(0).Id);
	    insert tag1;	    
	    Intel__c intel = [SELECT Id, Chatter_Post__c FROM  Intel__c WHERE Id = :intelList.get(0).Id ];
	    User usr = TestUtils.createUser(1, null, true).get(0);
	    
	   	System.runAs(usr){
				IntelListComponentController controller = new IntelListComponentController();
				controller.sObjAccId = acc.id;
				system.assertEquals(controller.lstIntel.size(),1);
				controller.chatterPostId = intel.Chatter_Post__c;
				controller.commentBody = 'Test Data';
				controller.saveReply();					
				System.assert([SELECT Id FROM FeedComment WHERE FeedItemId = :intel.Chatter_Post__c].size() > 0);
				System.assertEquals(controller.recordNumbering,'1-1 of 1');
				System.assertEquals(controller.pageNumbering,'Page 1 of 1');
				System.assert(!controller.hasPrev);
				System.assert(!controller.hasNext);
				controller.previous();
				controller.first();
				controller.last();
				controller.next();
				
				tag1.Account__c = null;
				tag1.contact__c = con.Id;
				update tag1;
				controller = new IntelListComponentController();
				controller.sObjAccId = con.id;
				system.assertEquals(controller.lstIntel.size(),1);
				
				tag1.Account__c = null;
				tag1.contact__c = con.Id;
				tag1.product__c = prod.Id;
				update tag1;
				controller = new IntelListComponentController();
				controller.sObjAccId = prod.id;
				system.assertEquals(controller.lstIntel.size(),1);
				
		}
		Test.stopTest();
	}
	
	// A Method for test saveFlag method
	public static testmethod void testSaveFlag(){
		Test.startTest();
    
		  //Create test records
	    Account acc = TestUtils.createAccount(1, true).get(0);
	    List<Intel__c> intelList = TestUtils.createIntel(1, true);
	    intelList = [SELECT Id, Chatter_Post__c FROM  Intel__c WHERE Id = :intelList.get(0).Id ];
	    Tag__c tag1 = new Tag__c(Account__c = acc.Id, Intel__c = intelList.get(0).Id);
	    insert tag1;
	    
	    User usr = TestUtils.createUser(1, null, true).get(0);
	    
	   	System.runAs(usr){
				IntelListComponentController controller = new IntelListComponentController();
				controller.sObjAccId = acc.id;
				system.assertEquals(controller.lstIntel.size(),1);
				controller.intelId = intelList.get(0).Id;
				controller.entityId = intelList.get(0).Chatter_Post__c;
				controller.saveFlag();
				
				System.assert([SELECT Id FROM Intel_Flag__c WHERE Intel__c = :controller.intelId].size() > 0);
			}	
		Test.stopTest();	
	}
}