/**================================================================      
* 2016 Appirio, Inc
* Name: COMM_BatchSyncExistingAddress
* Description: Batch Class to Sync up Location records with Address records.
* Created Date: 03 Oct 2016
* Created By: Isha Shukla (Appirio)
==================================================================*/
global class COMM_BatchSyncExistingAddress implements Database.Batchable<sObject> {
    //Start the batch
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id,Name,Address1__c,Address2__c,Status__c,City__c,State_Province__c,Country__c,PostalCode__c,Location_ID__c,Account__c,Phone__c,Type__c FROM Address__c WHERE Location_ID__c != null ');
    }
    
    global void execute(Database.BatchableContext BC, List<Address__c> records){
        Map<String,Address__c> mapAddress = new Map<String,Address__c>();
        for(Address__c addresses : records) {
            mapAddress.put(addresses.Location_ID__c, addresses);
        }
        
        Map<String, Id> mapLocation = new Map<String, Id>();
        for(SVMXC__Site__c location : [SELECT Id, Location_ID__c FROM SVMXC__Site__c WHERE Location_ID__c IN :mapAddress.keySet()]) {
          mapLocation.put(location.Location_ID__c, location.Id);
        }
        
        List<SVMXC__Site__c> lstSitesToUpdate = new List<SVMXC__Site__c>();
        for(String locationId :mapAddress.keySet()){
         SVMXC__Site__c objLocation;
         // Insert Case
         if( !mapLocation.containsKey(locationId) ){
           if(mapAddress.get(locationId).Status__c == 'Active'){
              objLocation = new SVMXC__Site__c();
             copyValues(mapAddress.get(locationId), objLocation);
             lstSitesToUpdate.add(objLocation);  
           }
         }
         // Update Case
         else{
          objLocation = new SVMXC__Site__c(Id = mapLocation.get(locationId));
          copyValues(mapAddress.get(locationId), objLocation);
          lstSitesToUpdate.add(objLocation);  
         }
        }
        
        upsert lstSitesToUpdate;
    }
    
    private void copyValues(Address__c objAddress, SVMXC__Site__c objLocation){
      objLocation.Name = objAddress.Name;
      objLocation.SVMXC__Street__c = objAddress.Address1__c + ' ' + objAddress.Address2__c;
      objLocation.SVMXC__City__c = objAddress.City__c;
      objLocation.SVMXC__State__c = objAddress.State_Province__c;
      objLocation.SVMXC__Country__c = objAddress.Country__c;
      objLocation.SVMXC__Zip__c = objAddress.PostalCode__c;
      objLocation.SVMXC__Account__c = objAddress.Account__c;
      objLocation.SVMXC__Site_Phone__c = objAddress.Phone__c;
      objLocation.SVMXC__Location_Type__c = objAddress.Type__c;
      objLocation.Location_ID__c = objAddress.Location_ID__c;
   }
      
    global void finish(Database.BatchableContext BC){
        
    }
    
}