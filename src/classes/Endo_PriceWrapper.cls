/*************************************************************************************************
Created By:    Sunil Gupta
Date:          Feb 19, 2014
Description  : Wrapper class to display result on price calculation page.
**************************************************************************************************/
global class Endo_PriceWrapper implements Comparable{
    public Decimal priceTemp{get;set;}
    public String productName{get;set;}
    public Decimal discount{get;set;}
    public String majorCode{get;set;}
    public Date startDate{get;set;}
    public Date endDate{get;set;}
    public Pricing_Contract_Line_Item__c PCLI{get;set;}
    public PricebookEntry priceBookEntry{get;set;}
    public static String SORT_DIR = 'ASCENDING';
    public Boolean isLowest{get;set;}
    
    /*
    global Integer compareTo(Object other) {
        Endo_PriceWrapper compareToEmp = (Endo_PriceWrapper)other;
    if(SORT_DIR == 'ASCENDING'){
        if (this.priceTemp == compareToEmp.priceTemp) return 0;
      if (this.priceTemp > compareToEmp.priceTemp) return 1;
      return -1; 
    }
    else{
        if (this.priceTemp == compareToEmp.priceTemp) return 0;
      if (this.priceTemp > compareToEmp.priceTemp) return -1;
      return 1; 
    }
  }*/
  
  global Integer compareTo(Object other) {
        Endo_PriceWrapper compareToEmp = (Endo_PriceWrapper)other;
    if(SORT_DIR == 'ASCENDING'){
        if (this.PCLI.Contract_Price__c == compareToEmp.PCLI.Contract_Price__c) return 0;
      if (this.PCLI.Contract_Price__c > compareToEmp.PCLI.Contract_Price__c) return 1;
      return -1; 
    }
    else{
        if (this.PCLI.Contract_Price__c == compareToEmp.PCLI.Contract_Price__c) return 0;
      if (this.PCLI.Contract_Price__c > compareToEmp.PCLI.Contract_Price__c) return -1;
      return 1; 
    }
  }
  
  
  
}