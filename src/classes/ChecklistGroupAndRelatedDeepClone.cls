/**================================================================      
* Appirio, Inc
* Name: ChecklistGroupAndRelatedDeepClone
* Description: clone Checklist Group and related Checklist Item
* Created Date: 03-Oct-2016 
* Created By: Shubham Dhupar (Appirio,Ref: T-542383)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
global class ChecklistGroupAndRelatedDeepClone {
  //============================================================================================     
  // Name         : cloneChecklist
  // Description  : To Clone Checklist Group and related Checklist Item
  // Created Date : 03-Oct-2016 
  // Created By   : Shubham Dhupar (Appirio)
  // Task         : T-542383
  //==========================================================================================
 webService static String cloneChecklist(ID checklistid) 
  {       
    List<Checklist_Item__c> newcheckList = new list<Checklist_Item__c>();
    Checklist_Group__c check = Database.query(queryAllObjectFields('Checklist_Group__c')  + ' WHERE Id = \'' + checklistid +'\' Limit 1');
    List<Checklist_Item__c> checkList = Database.query(queryAllObjectFields('Checklist_Item__c')  + ' WHERE Checklist_Group__c = \'' + checklistid +'\'');
    Checklist_Group__c Newcheck = check.clone(false, true);
    Insert Newcheck;
    for (Checklist_Item__c woCheck: checkList){
        Checklist_Item__c newWoCheck  = woCheck.clone(false, true);
        newWoCheck.Checklist_Group__c = Newcheck.Id;
        newcheckList.add(newWoCheck);
    }
    Insert newcheckList;
    return Newcheck.Id;
  }
    
  //============================================================================================     
  // Name         : queryAllObjectFields
  // Description  : To query All the fields of an object
  // Created Date : 03-Oct-2016 
  // Created By   : Shubham Dhupar (Appirio)
  // Task         : T-542383
  //==========================================================================================
    Static string queryAllObjectFields(String SobjectApiName){
        String query = '';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
 
        query = 'SELECT ' + commaSepratedFields + ' FROM ' + SobjectApiName;
        return query;
    }
}