/*******************************************************************************
 Name          :  Appirio JDC (Kajal Jalan)
 Date          :  May 05, 2016
 Purpose       :  Class having common methods for test data creation
*******************************************************************************/
@isTest
public without sharing class Intr_TestUtils {
  
  //****************************************************************************
  // Method to Create Case 
  //****************************************************************************
  public static List<Case> createCase(Integer accCount, Id accountId, Id contactId, Boolean isInsert) {
  	
    Account acc = createAccount(1, true).get(0);
    List<Case> lstCase = new List<Case>();
    for(Integer i = 0; i < accCount; i++) {
    	
      Case cs = new Case(Subject = 'Test Subject', Oracle_Order_Ref__c = '1234',
                AccountId = accountId, ContactId = contactId);
     
      lstCase.add(cs);
      
    }
    
    if(isInsert) {
    	
      insert lstCase;
      
    }
    
    return lstCase;
  }
  
  //****************************************************************************
  // Method to Create Junction object Stryker Instrument Contact 
  //****************************************************************************
  public static List<Stryker_Contact__c> createContactJunction(Integer accCount, Id accId, Id contactId, Boolean isInsert) {
  	
    Account acc = createAccount(1, true).get(0);
    Contact con = createContact(1, acc.Id, true).get(0);
    
    List<Stryker_Contact__c> lstSC = new List<Stryker_Contact__c>();
    for(Integer i = 0; i < accCount; i++){
      Stryker_Contact__c sc = new Stryker_Contact__c(Internal_Contact__c = contactId, Customer_Account__c = accId);
     
      lstSC.add(sc);
    }
    if(isInsert) {
    	
      insert lstSC;
    }
    return lstSC;
  }
   
  
  //****************************************************************************
  // Method to fetch profile 
  //****************************************************************************
  public static Profile fetchProfile(String profileName) {
  	
    Profile pr;
    if(profileName == null || profileName == '') {
    	
      pr = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'].get(0);
      
    }
    else {
    	
      for(Profile p : [SELECT Id, Name FROM Profile WHERE Name = :profileName LIMIT 1]){
        pr = p;
        
      } 
    }
    return pr;
  }
  
  //****************************************************************************
  // Method to fetch profile 
  //****************************************************************************
  public static Profile fetchInstrumentProfile(String profileName) {
  	
    Profile pr;
    if(profileName == null || profileName == '') {
    	
      pr = [SELECT Id, Name FROM Profile WHERE Name =: Intr_Constants.INSTRUMENTS_CCKM_PROFILE].get(0);
      
    }
    else {
    	
      for(Profile p : [SELECT Id, Name FROM Profile WHERE Name = :profileName LIMIT 1]){
        pr = p;
        
      } 
    }
    return pr;
  }
  
  
  
  //****************************************************************************
  // Method to create test user records
  //****************************************************************************
  public static List<User> createUser(Integer userCount, String profileName, Boolean isInsert ) {
  	
    Profile pr = fetchProfile(profileName);
    List<User> userList = new List<User>();
    User u;
    for(Integer i=0; i<userCount; i++){
      u = new User(ProfileId = pr.Id,
                   alias = 'usr' + i, 
                   email = 'user' + i + '@test.com', 
                   emailencodingkey = 'UTF-8', 
                   lastname = 'Testing' + i, 
                   languagelocalekey = 'en_US', 
                   localesidkey = 'en_US', 
                   timezonesidkey = 'America/Los_Angeles', 
                   username = 'tester' + i + '@test.com.stryker' + i,
                   Division = 'XYZ');
      userList.add(u);
    }
    if(isInsert) {
    	
      insert userList;
    }
    return userList;
  }
  
  //****************************************************************************
  // Method to create test Account records
  //****************************************************************************  
  public static List<Account> createAccount(Integer accCount, Boolean isInsert) {
  	
    List<Account> lstAccoount = new List<Account>();
    for(Integer i = 0; i < accCount; i++){
      Account acc = new Account(Name = 'Test Account ' + i,
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States');
      lstAccoount.add(acc);
      
    }
    if(isInsert) { 
    	
      insert lstAccoount;
    }
    
    return lstAccoount;
  }
  
  //****************************************************************************
  // Method to create test Account records
  //****************************************************************************  
  public static Account createAccount(String accountName, String recordTypeId, Boolean isInsert) {
     
    Account account = new Account(Name = accountName, RecordTypeId = recordTypeId);
    if(isInsert) {
    	
      insert account;
      
    }
    return account;
  }
  
  //****************************************************************************
  // Method to create test Contact records
  //****************************************************************************  
  public static List<Contact> createContact(Integer conCount, Id accountId, Boolean isInsert) {
  	
    List<Contact> lstContact = new List<Contact>();
    for(Integer i = 0; i < conCount; i++){
      Contact con = new Contact(FirstName = 'TestFName' + i,
                                LastName = 'TestLName' + i,Email = 'test@test.com',
                                Allow_Create__c = true,
                                AccountId = accountId);
      lstContact.add(con);
      
    }
    if(isInsert) {
    	
      insert lstContact;
    }
    return lstContact;
  }
  
  //****************************************************************************
  // Method to create test Opportunity records
  //****************************************************************************  
  public static List<Opportunity> createOpportunity(Integer conCount, Id accountId, Boolean isInsert) {
  	
    List<Opportunity> lstOpp = new List<Opportunity>();
    for(Integer i = 0; i < conCount; i++){
      Opportunity opp = new Opportunity(Name = 'TestFName' + i,
                                	Type = 'Base' + i,CloseDate = date.newInstance(01,11,2016) + i,StageName = 'Closed Won',
                                	ForecastCategoryName = 'Commit', Business_Unit__c = 'IVC', 
                                AccountId = accountId);
      lstOpp.add(opp);
      
    }
    if(isInsert) {
    	
      insert lstOpp;
    }
    return lstOpp;
  }
   //****************************************************************************
  // Method to create test Task records
  //****************************************************************************  
  public static List<Task> createTask(Integer taskCount, Id UserId,boolean isInsert) {
  	
    List<Task> lstTask = new List<Task>();
    for(Integer i = 0; i < taskCount; i++) {
    	
      Task task = new Task(OwnerId = UserId,Subject = 'Log Activity',Status = 'Not Started',Priority = 'High',CallDisposition = 'test');
      lstTask.add(task);
      
    }
    
    if(isInsert) {
    	
      insert lstTask;
      
    }
    
    return lstTask;
  }
  
 
  
  
  
}