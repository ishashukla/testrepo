// (c) 2015 Appirio, Inc.
//
// Class Name: ContactScheduleControllerTest 
// Description: Test Class for ContactScheduleController class.
// 
// February 10 2016, Prakarsh Jain Original 
//
@isTest
private class ContactScheduleControllerTest {
    static Account acc;
    static Contact con;
    static testMethod void testContactScheduleController(){
        createTestData();
        List<Block_Schedule__c> blck = TestUtils.createBlockSchedule(1, acc.id, con.id, false);
        blck[0].dow__c = 'Monday';
        blck[0].starting_Time__c = '12:00AM';
        blck[0].Ending_Time__c = '01:00AM';
        insert blck;
        Test.startTest();
            PageReference pageRef = Page.ContactSchedule;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', String.valueOf(con.Id));
            System.currentPageReference().getParameters().put('selectedContactOrAccount', String.valueOf(acc.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            ContactScheduleController testConSch = new ContactScheduleController(sc);
            testConSch.createBlockScheduleData(blck[0]);
            testConSch.saveDt = '2016-02-16T00:00:00.000Z';
            testConSch.blckRecId = blck[0].id;
            testConSch.inContactTimezone = false;
            List<selectOption> lstAssoContactsOrAccounts = testConSch.getLstAssoContactsOrAccounts();
            system.assertEquals(lstAssoContactsOrAccounts.size()>0,true);
            List<selectOption> LstContactsOrAccounts = testConSch.getLstContactsOrAccounts();
            system.assertEquals(LstContactsOrAccounts.size()>0,true); 
            testConSch.assignToContactOrAccount = acc.id;
            testConSch.showSchAsPerAccounts(acc.Id, con.id);
            testConSch.showSchAccounts();
            testConSch.saveSchedule();
            blck[0].starting_Time__c = '12:00PM';
            blck[0].Ending_Time__c = '01:00PM';
            update blck;
            testConSch.saveDt = '2016-02-16T15:00:00.000Z';     
            system.assertEquals(blck==null,false);
            testConSch.selectedContactOrAccount = 'All'; 
            blck[0].starting_Time__c = '02:00PM';
            blck[0].Ending_Time__c = '03:00PM';
            blck[0].dow__c = 'Monday';
            update blck;
            testConSch.saveSchedule();
            testConSch.deleteSchedule();
        Test.stopTest();
    }
    
    static testMethod void testContactScheduleController1(){
        createTestData();
        Test.startTest();
            PageReference pageRef = Page.ContactSchedule;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', String.valueOf(con.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            ContactScheduleController testConSch = new ContactScheduleController(sc);
            testConSch.con = con;
            testConSch.inContactTimezone = false;
            testConSch.saveDt = String.valueOf(System.now());
            testConSch.inContactTimezone = true;
            
            List<selectOption> lstAssoContactsOrAccounts = testConSch.getLstAssoContactsOrAccounts();
            system.assertEquals(lstAssoContactsOrAccounts.size()>0,true);
            List<selectOption> LstContactsOrAccounts = testConSch.getLstContactsOrAccounts();
            system.assertEquals(LstContactsOrAccounts.size()>0,true);
            testConSch.selectedContactOrAccount = 'All'; 
            testConSch.assignToContactOrAccount = 'All';
            testConSch.showSchAsPerAccounts(acc.Id, con.id);
            testConSch.showSchAccounts();
            
            testConSch.getCorrectedTimeZone('12:30PM', testConSch.saveDt);
            testConSch.strtDate = '12:30PM';
            testConSch.endDt = '01:00PM';
            testConSch.saveSchedule();
            //system.assertEquals(blck==null,false);
            testConSch.deleteSchedule();
        Test.stopTest();
        }
        
        static testMethod void testContactScheduleController2(){
            createTestData();
            Test.startTest();
                PageReference pageRef = Page.ContactSchedule;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id', String.valueOf(con.Id));
                ApexPages.StandardController sc = new ApexPages.StandardController(con);
                ContactScheduleController testConSch = new ContactScheduleController(sc);
                testConSch.inContactTimezone = false;
                testConSch.saveDt = '2016-02-16T00:00:00.000Z';
                testConSch.inContactTimezone = true;
                
                List<selectOption> lstAssoContactsOrAccounts = testConSch.getLstAssoContactsOrAccounts();
                system.assertEquals(lstAssoContactsOrAccounts.size()>0,true);
                List<selectOption> LstContactsOrAccounts = testConSch.getLstContactsOrAccounts();
                system.assertEquals(LstContactsOrAccounts.size()>0,true);
                testConSch.selectedContactOrAccount = 'All'; 
                testConSch.assignToContactOrAccount = 'All';
                testConSch.showSchAsPerAccounts(acc.Id, con.id);
                testConSch.showSchAccounts();
                testConSch.getCorrectedTimeZone('12:30PM', String.valueOf(testConSch.saveDt));
                testConSch.strtDate = '12:30PM';
                testConSch.endDt = '01:00PM';
            Test.stopTest();
    }
    
    static testMethod void testContactScheduleController4(){
        createTestData();
        Test.startTest();
            PageReference pageRef = Page.ContactSchedule;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', String.valueOf(con.Id));
            System.currentPageReference().getParameters().put('selectedContactOrAccount', String.valueOf(acc.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            ContactScheduleController testConSch = new ContactScheduleController(sc);
            testConSch.con = con;
            testConSch.saveDt = '2016-02-16T00:00:00.000Z';
            testConSch.inContactTimezone = false;
            List<selectOption> lstAssoContactsOrAccounts = testConSch.getLstAssoContactsOrAccounts();
            system.assertEquals(lstAssoContactsOrAccounts.size()>0,true);
            List<selectOption> LstContactsOrAccounts = testConSch.getLstContactsOrAccounts();
            system.assertEquals(LstContactsOrAccounts.size()>0,true);
            testConSch.assignToContactOrAccount = acc.id;
            testConSch.showSchAsPerAccounts(acc.Id, con.id);
            testConSch.showSchAccounts();
            testConSch.saveDt = '2016-02-16T15:00:00.000Z';
            testConSch.saveSchedule();
            testConSch.selectedContactOrAccount = 'All'; 
            testConSch.saveSchedule();
            testConSch.deleteSchedule();
        Test.stopTest();
    }
    
    public static void createTestData(){
        acc = TestUtils.createAccount(1, true).get(0); 
        con = TestUtils.createContact(1, acc.Id, false).get(0);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        con.RecordTypeId = rtByName.getRecordTypeId();
        con.timeZone__c = 'GMT–05:00    Peru Time (America/Lima)';
        insert con;
        
    }
}