/**================================================================      
* Appirio, Inc
* Name: COMM_FSITriggerHandlerTest
* Description: Test class for COMM_FSITriggerHandlerTest
* Created Date: 18 Nov 2016
* Created By: Meha Simlote (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class COMM_FSITriggerHandlerTest {
    static List<Field_Service_Investigation__c> FSIList;
    static SVMXC__Service_Order__c workorder;
    @isTest static void testTrigger() {
        createData();
        Test.startTest();
        insert FSIList;
        Test.stopTest();
        System.assertEquals([SELECT Id,Multiple_Visits_Required__c FROM SVMXC__Service_Order__c WHERE Id = :workorder.Id].Multiple_Visits_Required__c,'No');
        
    }
    
    @isTest static void testTrigger2() {
        createData();
        insert FSIList;
        Test.startTest();
        FSIList[0].Return_Visit_Required__c = 'Yes';
        
        Database.SaveResult[] srList = Database.update(FSIList, false);

// Iterate through each returned result
for (Database.SaveResult sr : srList) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted account. Account ID: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.assertEquals('Cannot change the Field in Reopened work order status', err.getMessage());
        }
    }
}
        Test.stopTest();
    } 
    @isTest static void testTrigger3() {
        createData();
        Test.startTest();
        insert FSIList;
        workorder.SVMXC__Order_Status__c ='Open';
        update workorder;
        FSIList[0].Return_Visit_Required__c = 'Yes';
        update FSIList;
        Test.stopTest();
        System.assertEquals([SELECT Id,Multiple_Visits_Required__c FROM SVMXC__Service_Order__c WHERE Id = :workorder.Id].Multiple_Visits_Required__c,'Yes');
    } 
    
private static void createData() {
    string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        workorder = new SVMXC__Service_Order__c(WO_ID__c = '1234',SVMXC__Order_Status__c ='Reopened');
        insert workorder;
        FSIList = new List<Field_Service_Investigation__c>();
        
            
            Field_Service_Investigation__c FSI  = new Field_Service_Investigation__c(Return_Visit_Required__c='No',
                                                                                Work_Order__c = workorder.Id
                                                                               );
            FSIList.add(FSI);
        
        
    }
}