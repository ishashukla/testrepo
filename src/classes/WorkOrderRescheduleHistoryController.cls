/**================================================================      
* Appirio, Inc
* Name: WorkOrderRescheduleHistoryController
* Description: Controller for Component to show Work Order's reschedule history (I-237270)
* Created Date: 2 - Oct - 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update

==================================================================*/
public without sharing class WorkOrderRescheduleHistoryController {
    public Id WorkOrderId{get;set;}
    public List<SVMXC__Service_Order__History> WoHistory
    {
        get
        {
            List<SVMXC__Service_Order__History> listServiceOrder = [SELECT ParentId, OldValue, NewValue, Field, CreatedById, CreatedDate FROM SVMXC__Service_Order__History where parentId = :WorkOrderId and Field = 'Reschedule_Count__c'];
            return listServiceOrder;
        }
        
        set;
        
    }
    

}