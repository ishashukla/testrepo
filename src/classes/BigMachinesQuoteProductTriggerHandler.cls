/**================================================================      
* Appirio, Inc
* Name: BigMachinesQuoteProductTriggerHandler 
* Description: Created this triggerHandler to create a Opportunity product automatically when a Quote line item is created with its Quote marked as 'primary'
               the following fields are not mapped: ServiceDate, Discount, ListPrice, Subtotal, PSR_Contact__c for T-483787
* Created Date:  03/18/2016
* Created By: Mehul Jain (Appirio)
*
* Date Modified      Modified By            Description of the update
* 3rd Nov 2016      Nitish Bansal          I-241409 - Added Proper null & list checks before SOQL and DML
==================================================================*/
public class BigMachinesQuoteProductTriggerHandler {
    
  public static void afterInsert(List<BigMachines__Quote_Product__c> newList) {
    populateQuoteAndPBMap(newList);
  }

  // creates Maps for Quote product with its Quotes and PricebookEntryId and calls cloneBMQProductToOpptyProduct function
  private static void populateQuoteAndPBMap(List<BigMachines__Quote_Product__c> newList){
    List<Id> quoteIdListOfQProduct = new List<Id> ();
    List<Id> productIdListOfQProduct = new List<Id> ();
    Map<Id , BigMachines__Quote__c> quoteMap = new Map<Id , BigMachines__Quote__c> (); // QuoteProductId and its Quote
    Map<Id , Id> pbEntryMap = new Map<Id , Id> ();									                   // QuoteProductId and its PricbookEntryId       
    List<BigMachines__Quote__c> bmQuoteList;
    List<PricebookEntry> pbEntryList;
    
    for(BigMachines__Quote_Product__c tempqProduct : newList) {
      if(tempqProduct.BigMachines__Quote__c != null)                      //NB - 11/03 - I-241409
        quoteIdListOfQProduct.add(tempqProduct.BigMachines__Quote__c);
      if(tempqProduct.BigMachines__Product__c != null)                    //NB - 11/03 - I-241409
        productIdListOfQProduct.add(tempqProduct.BigMachines__Product__c);
    }

    if(quoteIdListOfQProduct.size() > 0){                                 //NB - 11/03 - I-241409
      bmQuoteList = new List<BigMachines__Quote__c>( [SELECT Id, 
                                                      BigMachines__Is_Primary__c,
                                                      BigMachines__Opportunity__c, 
                                                      BigMachines__Opportunity__r.Pricebook2Id, 
                                                      Equipment_Funded_Amount__c, 
                                                      BigMachines__Status__c 
                                                      FROM BigMachines__Quote__c
                                                      WHERE Id IN : quoteIdListOfQProduct] );
    }
    
    if(productIdListOfQProduct.size() > 0){                              //NB - 11/03 - I-241409
      pbEntryList = new List<PricebookEntry>( [SELECT Id, Product2Id
                                                FROM PricebookEntry
                                                WHERE Product2Id IN :productIdListOfQProduct] );
    }    
    
    for(BigMachines__Quote_Product__c tempProduct : newList) {
      for(Integer i = 0; i<bmQuoteList.size(); i++) {
        if( bmQuoteList.get(i).Id.equals(tempProduct.BigMachines__Quote__c) && bmQuoteList.get(i).BigMachines__Is_Primary__c && tempProduct.Id != null) { //NB - 11/03 - I-241409
          quoteMap.put(tempProduct.Id, bmQuoteList.get(i));  
        }
      }  
      for(Integer i =0; i<pbEntryList.size(); i++) {
        if( pbEntryList.get(i).Product2Id.equals(tempProduct.BigMachines__Product__c) && tempProduct.Id != null ) { //NB - 11/03 - I-241409
          pbEntryMap.put(tempProduct.Id, pbEntryList.get(i).Id);  
        }
      }             
    } 

    if( quoteMap.size() > 0 && pbEntryMap.size() > 0 ){
      cloneBMQProductToOpptyProduct(newList,quoteMap,pbEntryMap);
    }  
  } 
  
  
  // Creates Oppty Product for each Quote Product       
  private static void cloneBMQProductToOpptyProduct(List<BigMachines__Quote_Product__c> bmqProductList,
                                                   Map<Id,BigMachines__Quote__c> quoteMap, 
                                                   Map<Id,Id> pbEntryMap ) {
    List<OpportunityLineItem> opptyProductList = new List<OpportunityLineItem> ();
    OpportunityLineItem opptyProduct;
    Boolean containsProduct;
    Id pbEntryId;

    //Map fields of Quote Product to Oppty Product 
    for ( BigMachines__Quote_Product__c bmqProduct : bmqProductList ) { 
      if( quoteMap.containsKey(bmqProduct.Id) && pbEntryMap.containsKey(bmqProduct.Id) ) {
        containsProduct = true;
        opptyProduct = new OpportunityLineItem();  
        BigMachines__Quote__c bmQuote = quoteMap.get(bmqProduct.Id);
        if( pbEntryMap.get(bmqProduct.Id) == null ) {
          bmqProduct.BigMachines__Product__c.addError(Constants.INVALID_PRODUCT_ON_QUOTE);
        }
        else {
          pbEntryId = pbEntryMap.get(bmqProduct.Id);              
          opptyProduct.OpportunityId = bmQuote.BigMachines__Opportunity__c;
          opptyProduct.PricebookEntryId = pbEntryId;
          
          opptyProduct.Location_Account__c = bmqProduct.Location_Account__c;
          opptyProduct.BigMachines__Origin_Quote__c  = bmqProduct.BigMachines__Quote__c;
         
          opptyProduct.Asset_Type__c = bmqProduct.Asset_Type__c;
          opptyProduct.Business_Unit__c = bmqProduct.Business_Unit__c;
          opptyProduct.City_Tax_Amt__c = bmqProduct.City_Tax_Amt__c;
          opptyProduct.City_Tax_Code__c = bmqProduct.City_Tax_Code__c;
          opptyProduct.City_Tax_Pct__c = bmqProduct.City_Tax_Pct__c;
          opptyProduct.County_Tax_Amt__c = bmqProduct.County_Tax_Amt__c;
          opptyProduct.County_Tax_Code__c = bmqProduct.County_Tax_Code__c;
          opptyProduct.County_Tax_Pct__c = bmqProduct.County_Tax_Pct__c;
          opptyProduct.Misc_City_Tax_Code__c = bmqProduct.Misc_City_Tax_Code__c;
          opptyProduct.Misc_County_Tax_Code__c = bmqProduct.Misc_County_Tax_Code__c;
          opptyProduct.Misc_State_Tax_Code__c = bmqProduct.Misc_State_Tax_Code__c;
          opptyProduct.State_Tax_Amt__c = bmqProduct.State_Tax_Amt__c;
          opptyProduct.State_Tax_Code__c = bmqProduct.State_Tax_Code__c;
          opptyProduct.State_Tax_Pct__c = bmqProduct.State_Tax_Pct__c;
          opptyProduct.Residual_Amount__c = bmqProduct.Residual_Amount__c;
          opptyProduct.BigMachines__Synchronization_Id__c = bmqProduct.BigMachines__Synchronization_Id__c;
          opptyProduct.Tax_Basis__c = bmqProduct.Tax_Basis__c;
          opptyProduct.Description = bmqProduct.BigMachines__Description__c;

          opptyProduct.Quantity = bmqProduct.BigMachines__Quantity__c;
          opptyProduct.TotalPrice = bmqProduct.BigMachines__Total_Price__c;
          opptyProduct.Funded_Amt__c = bmQuote.Equipment_Funded_Amount__c;
          opptyProduct.Part_Number__c = bmqProduct.Name;
          opptyProduct.Status__c = bmQuote.BigMachines__Status__c;
          opptyProductList.add(opptyProduct);
        }
      }
    }
    
    if( trigger.isInsert && opptyProductList.size() > 0) { //NB - 11/03 - I-241409
      insert opptyProductList;
    }
    
  }
}