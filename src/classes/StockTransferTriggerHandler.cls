/**================================================================      
* Appirio, Inc
* Name: StockTransferTriggerHandler
* Description: Trigger Handler for StockTransfer_Trigger
* Created Date: 22-Aug-2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update
* 10 Nov 2016        Isha Shukla      I-243425 - Added method setIntegrationStatus
==================================================================*/
public class StockTransferTriggerHandler {
    
    public static void OnBeforeInsert(List<SVMXC__Stock_Transfer__c> transfers)
    {
        PopulateTechnicianEmails(transfers);
        setIntegrationStatus(transfers);
    }
    
    public static void OnBeforeUpdate(List<SVMXC__Stock_Transfer__c> transfers)
    {
        PopulateTechnicianEmails(transfers);
    }
    
    private static void PopulateTechnicianEmails(List<SVMXC__Stock_Transfer__c> transfers)
    {
    	//Set of Locations from Stocktransfer
        Set<Id> sourceLocations = new Set<Id>();
        Set<Id> destiLocations = new Set<Id>();
        for(SVMXC__Stock_Transfer__c item : transfers)
        {
            if(item.SVMXC__Source_Location__c != null)
            {
                sourceLocations.add(item.SVMXC__Source_Location__c);
            }
            if(item.SVMXC__Destination_Location__c != null)
            {
            	destiLocations.add(item.SVMXC__Destination_Location__c);
            }
        }
        
        //Map of Source Location and technician Email
        Map<Id,SVMXC__Service_Group_Members__c> technicians = new Map<Id,SVMXC__Service_Group_Members__c>();
        for(SVMXC__Service_Group_Members__c techs :[select SVMXC__Inventory_Location__c,SVMXC__Email__c from SVMXC__Service_Group_Members__c where SVMXC__Inventory_Location__c in :sourceLocations])
        {
        	if(techs.SVMXC__Inventory_Location__c != null && techs.SVMXC__Email__c != null)
        	{
        		technicians.put(techs.SVMXC__Inventory_Location__c,techs);
        	}
        }
        
        //Map of Destination Location and technician Email
        Map<Id,SVMXC__Service_Group_Members__c> Dtechnicians = new Map<Id,SVMXC__Service_Group_Members__c>();
        for(SVMXC__Service_Group_Members__c techs :[select SVMXC__Inventory_Location__c,SVMXC__Email__c from SVMXC__Service_Group_Members__c where SVMXC__Inventory_Location__c in :destiLocations])
        {
        	if(techs.SVMXC__Inventory_Location__c != null && techs.SVMXC__Email__c != null)
        	{
        		Dtechnicians.put(techs.SVMXC__Inventory_Location__c,techs);
        	}
        }
        
        for(SVMXC__Stock_Transfer__c item : transfers)
        {
        	if(item.SVMXC__Source_Location__c != null)
        	{
        		SVMXC__Service_Group_Members__c tempRecord = technicians.get(item.SVMXC__Source_Location__c);
        		if(tempRecord != null && tempRecord.SVMXC__Email__c != null)
        		{
        			item.ReqFromTechEmail__c = tempRecord.SVMXC__Email__c;
        		}
        	}
        	
        	if(item.SVMXC__Destination_Location__c != null)
        	{
        		SVMXC__Service_Group_Members__c tempRecord = Dtechnicians.get(item.SVMXC__Destination_Location__c);
        		if(tempRecord != null && tempRecord.SVMXC__Email__c != null)
        		{
        			item.ReqByTechEmail__c = tempRecord.SVMXC__Email__c;
        		}
        	}
        	
        	
        }
        
        	
    }
    // Set Integration status to 'New'
    private static void setIntegrationStatus(List<SVMXC__Stock_Transfer__c> records) {
        for(SVMXC__Stock_Transfer__c stock : records) {
            stock.Integration_Status__c = 'New';
        }
    }

}