// (c) 2015 Appirio, Inc.
//
// Class Name: DDMControllerTest  
// Description: Test Class for DDMController class.
// 
// April 15 2016, Isha Shukla  Original 
//
@isTest
Private class DDMControllerTest {
    @isTest static void testDDMController() {
        Profile pr = TestUtils.fetchProfile('System Administrator');
        List<User> user = TestUtils.createUser(1,pr.Name, false);
        user[0].Division = 'NSE';
        System.runAs(user[0]) {
            DDM_Configuration__c ddm = TestUtils.createDDMConfigRecord(true);
        	Test.startTest();
                PageReference pageRef = Page.DDM;
                Test.setCurrentPage(pageRef);
                DDMController controller = new DDMController();
            Test.stopTest();
            System.assertEquals('cmfordering://', controller.ddmurl);
        }
    }
}