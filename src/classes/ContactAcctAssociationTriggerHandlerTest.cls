/*************************************************************************************************
Created By:    Megha Agarwal
Date:          October 15, 2015
Description:  Test class for ContactAcctAssociationTriggerHandler
12 May 2016    Pratibha Chhimpa(Appirio)    Added testUpdateContact method for updateContact method
**************************************************************************************************/
@isTest
private class ContactAcctAssociationTriggerHandlerTest {
    private static List<Account> accounts;
  private static List<Contact> contacts;
  private static Contact_Acct_Association__c primaryContactAcctAssociattion;

  static testMethod void testPrimaryAccountDeletion() {
    accounts = Medical_TestUtils.createAccount(1, true);

    contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);

    List<Contact_Acct_Association__c> contactAcctAssociations = [SELECT Id  FROM Contact_Acct_Association__c
                                                  WHERE Associated_Account__c = :accounts.get(0).id AND Associated_Contact__c = : contacts.get(0).id];

    if(contactAcctAssociations.size() == 0){
      primaryContactAcctAssociattion = new Contact_Acct_Association__c(Associated_Account__c = accounts.get(0).id , Associated_Contact__c = contacts.get(0).id, primary__c = true);
      insert primaryContactAcctAssociattion;
    }
    else{
        primaryContactAcctAssociattion = contactAcctAssociations.get(0);
    }

    try{
      delete primaryContactAcctAssociattion;
    }
    catch(Exception ex){
      System.assert(ex.getMessage().contains(System.Label.Contact_Acct_Association_Primary_Delete_Message) , 'Actual Error : ' + ex.getMessage());
    }
  }
  static testMethod void testUpdateContact() {
  // Test Data
    accounts = Medical_TestUtils.createAccount(1, true);
    contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);
    

    List<Contact_Acct_Association__c> contactAcctAssociations = [SELECT Id  FROM Contact_Acct_Association__c
                                                  WHERE Associated_Account__c = :accounts.get(0).id AND Associated_Contact__c = : contacts.get(0).id];

    if(contactAcctAssociations.size() == 0){
      primaryContactAcctAssociattion = new Contact_Acct_Association__c(Associated_Account__c = accounts.get(0).id , Associated_Contact__c = contacts.get(0).id, primary__c = true);
      insert primaryContactAcctAssociattion;
    }
    else{
        primaryContactAcctAssociattion = contactAcctAssociations.get(0);
    }
    List<Contact> contactList = [SELECT Assoc_w_Multiple_Accounts__c FROM Contact where Id = :contacts.get(0).id];
    
  //  System.assert(contactList.get(0).Assoc_w_Multiple_Accounts__c, true);
    
  }
}