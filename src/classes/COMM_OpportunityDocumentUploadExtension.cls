//
// (c) 2015 Appirio, Inc.
//
// Apex Class Name: COMM_OpportunityDocumentUploadExtension
// For Apex Page: COMM_OpportunityDocumentUpload
// Description: This apex class is used for COMM services. It has the functionality to attach a document to opportunity document record.
//
// 31st Mrach 2015   Hemendra Singh Bhati   Original (Task # T-487784)
//
public class COMM_OpportunityDocumentUploadExtension {
  // Private Data Members.
  private Document__c theRecord;
  private ApexPages.StandardController theController;

  // Public Data Members.
  public String documentName { get; set; }
  public Blob documentBody { get; set; }

  // The Class Constructor.
  public COMM_OpportunityDocumentUploadExtension(ApexPages.StandardController theStandardController) {
    theController = theStandardController;
    theRecord = (Document__c)theStandardController.getRecord();
  }

  /*
  @method      : validateAttachmentId
  @description : This method validates the attachment Id passed as a URL parameter.
  @params      : 
  @returns     : 
  */
  private String validateAttachmentId(String theAttachmentId) {
    String returnedString = null;
    String AttachmentId = String.escapeSingleQuotes(theAttachmentId);
    if(
      theAttachmentId != null &&
      (AttachmentId.length() == 15 || AttachmentId.length() == 18) &&
      Pattern.matches('^[a-zA-Z0-9]*$', AttachmentId) &&
      Id.valueOf(AttachmentId).getSObjectType().getDescribe().getName() == 'Attachment'
    ) {
      returnedString = AttachmentId;
    }
    return returnedString;
  }

  /*
  @method      : processSelectedDocument
  @description : This method uploads the selected document and updates the opportunity document record with attachment Id.
  @params      : void
  @returns     : void
  */
  public PageReference processSelectedDocument() {
    PageReference thePageReference = new PageReference('/' + theRecord.Id);
    try {
      // Deleting Existing Attached Document.
      String theAttachmentId = validateAttachmentId(ApexPages.currentPage().getParameters().get('DocumentId'));
      if(String.isNotBlank(theAttachmentId)) {
        delete new Attachment(
          Id = theAttachmentId
        );
      }
      theRecord.DocumentId__c = null;

      // Inserting Newly Selected Document.
      if(String.isNotBlank(documentName)) {
        Attachment theUploadedDocument = new Attachment(
          ParentId = theRecord.Id,
          Name = documentName,
          Body = documentBody
        );
        insert theUploadedDocument;

        theRecord.DocumentId__c = theUploadedDocument.Id;
        theController.save();
      }
    }
    catch(Exception e) {
      thePageReference = null;
      ApexPages.AddMessage(new ApexPages.Message(
        ApexPages.Severity.ERROR,
        e.getMessage()
      ));

    	system.debug('TRACE: COMM_OpportunityDocumentUploadExtension - processSelectedDocument - Exception Message - ' + e.getMessage());
    	system.debug('TRACE: COMM_OpportunityDocumentUploadExtension - processSelectedDocument - Exception Stack Trace - ' + e.getStackTraceString());
    }
    return thePageReference;
  }
}