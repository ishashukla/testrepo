/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 21, 2013
 Purpose       :  Test Class for AutoCompleteService.cls
 Reference     :  Task T-206113
 Modifications :  Task T-218154    Dec 05, 2013   Sonal Shrivastava
*******************************************************************************/
@isTest 
private class AutoCompleteService_Test {

  static testMethod void myUnitTest() {
    
    //Create test records
    List<Account> accList = TestUtils.createAccount(1, true);
    List<Contact> conList = TestUtils.createContact(1, accList.get(0).Id, true);
    
    String prodNVRecTypeName = Label.Product_RecordType_Stryker_NV;
    List<Product2> prodList = TestUtils.createProduct(1, false);
    prodList.get(0).Taggable__c = true;
    prodList.get(0).IsActive = true;
    prodList.get(0).Nickname__c = 'Test nickname';
    prodList.get(0).RecordTypeId = Schema.SObjectType.Product2.RecordTypeInfosByName.get(prodNVRecTypeName).RecordTypeId;
    insert prodList;
    
    List<User> userList = TestUtils.createUser(1, null, true);
    
    //Test functionality
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    
    req.requestURI = '/services/apexrest/AutoCompleteService/*';
    req.httpMethod = 'GET';
    req.addParameter('searchTerm', 'Test');
    RestContext.request = req;
    RestContext.response = res;
    
    test.startTest();    
    
    //Prepare FixedSearchResults for SOSL query results
    Id [] fixedSearchResults = new Id[10];
    fixedSearchResults[0] = accList.get(0).Id;
    fixedSearchResults[1] = conList.get(0).Id;
    fixedSearchResults[2] = prodList.get(0).Id;
    fixedSearchResults[3] = userList.get(0).Id;
        
    Test.setFixedSearchResults(fixedSearchResults); 
    
    AutoCompleteService.ObjectListWrapper result = AutoCompleteService.doGet();
    System.assertEquals(4, result.objectList.size());
    test.stopTest();
  }
}