/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  May 07, 2014
 Purpose       :  Batch class to update Order Link on Case Object and Case's Contact on matched Order.
 Reference     :  Task T-276769
*******************************************************************************/
global class Endo_LinkCasesWithOrders implements Database.Batchable<sObject>,database.stateful{
  
  // Start block
  global Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator('SELECT Id, Oracle_Order_Ref__c, ContactId FROM Case WHERE Oracle_Order_Ref__c != null AND Current_Case_Order__c = null');
  }
  
  
  // Execute block
  global void execute(Database.BatchableContext BC, List<Case> lstCases){
  	System.debug('#####Execute');
  	Map<String, List<Case>> mapCases = new Map<String, List<Case>>();
    for(Case c :lstCases){
      if(mapCases.containsKey(c.Oracle_Order_Ref__c) == false){
        mapCases.put(c.Oracle_Order_Ref__c, new List<Case>());
      }
      mapCases.get(c.Oracle_Order_Ref__c).add(c);
    }
    System.debug('@@@' + mapCases);
     
     
    Map<String, Order__c> mapOrders = new Map<String, Order__c>();
    for(Order__c order :[SELECT Id, Name FROM Order__c WHERE Name IN :mapCases.keySet()]){
    	mapOrders.put(order.Name, order);
    }
    System.debug('@@@' + mapOrders);
     
    List<Case> caseListToUpdate = new List<Case>();
    Map<Id, Order__c> orderListToUpdate = new Map<Id, Order__c>(); 
    for(String orderNumber :mapCases.keySet()){
    	List<Case> caseList = mapCases.get(orderNumber);
     	if(caseList != null && caseList.size() > 0){
     		Id orderId;
     		if(mapOrders.get(orderNumber) != null){
     	    orderId = mapOrders.get(orderNumber).Id;
     		}
     	 	  if(orderId != null){
     	 	    for(Case c :caseList){
	            c.Current_Case_Order__c = orderId;
	            Order__c objOrder = new Order__c(Id = orderId, Order_Contact__c = c.ContactId);
	            orderListToUpdate.put(objOrder.Id, objOrder);
	            caseListToUpdate.add(c);
	          }
     	 	  }
     	  }
      }
      system.debug('@@@' + caseListToUpdate);
      system.debug('@@@' + orderListToUpdate);
      
      update caseListToUpdate;
      update orderListToUpdate.values();
      
    }
  
  // Finish block
  global void finish(Database.BatchableContext BC){
     
  }
 }