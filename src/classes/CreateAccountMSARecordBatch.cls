/**================================================================      
 * Appirio, Inc
 * Name: CreateAccountMSARecordBatch
 * Description: Created as per T-547784. Batch class to create records for Account MSA object
 * Created Date: [10/17/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/
global class CreateAccountMSARecordBatch implements Database.Batchable<sObject>, System.Schedulable{
  public static Id accountInstrumentsRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type').getRecordTypeId();
  public List<Account_MSA__c> accountMSAList;
  global Database.QueryLocator start(Database.BatchableContext BC){
    String query = '';
    query = 'SELECT Account__c, Id, Agreement_Type__c, Master_Agreement_Number__c, Master_Agreement_Signed__c, Type_of_Terms_and_Conditions__c, Account__r.Instruments_Division__c, Account__r.Active_MSA__c FROM Master_Agreement__c WHERE Account__r.Instruments_Division__c != null';
    return Database.getQueryLocator(query);
  }
  
  global void execute(Database.BatchableContext BC,List<Sobject> scope){
    try{
      	List<String> accountNumberList = new List<String>();
	    Set<String> accountNumberSet = new Set<String>();
	    Set<String> accountNumberFinalSet = new Set<String>();
	    List<Account> accountList = new List<Account>();
	    accountMSAList = new List<Account_MSA__c>();
	    Map<String,Id> accountNumberToAccountIdMap = new Map<String,Id>();
	    Set<Id> accountIdSet = new Set<Id>();
	    Set<Id> masterAgreementIdSet = new Set<Id>();
	    Set<String> accountMSASet = new Set<String>();
	    Set<Id> accountIdToUpdateSet = new Set<Id>();
	    List<Account> accountListToUpdate = new List<Account>();
	    Map<Id,Id> masterAgreementToflexAccountId = new Map<Id,Id>();
	    Map<Id,Set<String>> flexAccountToInstrumentAccountNumber = new Map<Id,Set<String>>();
	    for(Sobject s : scope){
	      Master_Agreement__c masterAgreement = (Master_Agreement__c)s;
	      accountNumberList.add(masterAgreement.Account__r.Instruments_Division__c);
	      accountIdSet.add(masterAgreement.Account__c);
	      masterAgreementIdSet.add(masterAgreement.Id);
	      masterAgreementToflexAccountId.put(masterAgreement.id,masterAgreement.Account__c);
	      if(!flexAccountToInstrumentAccountNumber.containsKey(masterAgreement.Account__c)){
	      		flexAccountToInstrumentAccountNumber.put(masterAgreement.Account__c,new Set<String>());
	      }
	      flexAccountToInstrumentAccountNumber.get(masterAgreement.Account__c).addAll(getAccountNumberSetfromList(masterAgreement.Account__r.Instruments_Division__c));
	    }
	    for(Account_MSA__c accountMSA : [SELECT Id, Account__c, MSA__c 
	                                     FROM Account_MSA__c 
	                                     WHERE MSA__c IN: masterAgreementIdSet]){
	      accountMSASet.add(accountMSA.Account__c + '+' + accountMSA.MSA__c);    
	    }
	    if(accountNumberList.size()>0){
	      for(String actNumber : accountNumberList){
	        List<String> tempList = new List<String>();
	        tempList = actNumber.split(',');
	        accountNumberSet.addAll(tempList);
	      }
	    }
	    if(accountNumberSet.size()>0){
	      for(String actNo : accountNumberSet){
	        actNo = actNo + '-0';
	        accountNumberFinalSet.add(actNo);
	      }
	    }
	    if(accountNumberFinalSet.size()>0){
	      accountList = [SELECT Id, Name, AccountNumber, RecordTypeId 
                       FROM Account 
                       WHERE AccountNumber IN: accountNumberFinalSet AND RecordTypeId =: accountInstrumentsRecTypeId];
	    }
	    if(accountList.size()>0){
	      for(Account account : accountList){
	        if(!accountNumberToAccountIdMap.containsKey(account.AccountNumber)){
	          accountNumberToAccountIdMap.put(account.AccountNumber, account.Id);
	        }
	      }     
	    }
	    Map<Id,Set<Id>> flexAccoundIdtoInstrumentAccountIds = new  Map<Id,Set<Id>>();
	    for(Id flexAccountId : flexAccountToInstrumentAccountNumber.keySet()){
	    	Set<String> accNumberSet = new Set<String>();
	    	accNumberSet = flexAccountToInstrumentAccountNumber.get(flexAccountId);
	    	for(String accNum : accNumberSet){
	    		if(!flexAccoundIdtoInstrumentAccountIds.containsKey(flexAccountId)){
	    			flexAccoundIdtoInstrumentAccountIds.put(flexAccountId,new Set<Id>());
	    		}
	    		if(accountNumberToAccountIdMap.containsKey(accNum)){
	    			flexAccoundIdtoInstrumentAccountIds.get(flexAccountId).add(accountNumberToAccountIdMap.get(accNum));
	    		}
	    	}
	    }
	    for(Id maKey : masterAgreementToflexAccountId.keySet()){
	    	Id flexAccountId = masterAgreementToflexAccountId.get(maKey);
	    	if(flexAccoundIdtoInstrumentAccountIds.containsKey(flexAccountId)){
	    		Set<Id> instrumentAccountIdset = new Set<Id>();
	    		instrumentAccountIdset = flexAccoundIdtoInstrumentAccountIds.get(flexAccountId);
	    		for(Id instrumentAccId : instrumentAccountIdset){
	    			String key = instrumentAccId + '+' + maKey;
	    			if(!accountMSASet.contains(key)){
						Account_MSA__c accountMSA = new Account_MSA__c(Account__c = instrumentAccId, MSA__c = maKey);
						accountMSAList.add(accountMSA);
						Account account = new Account(Id = instrumentAccId,Active_MSA__c = true);
						if(!accountIdToUpdateSet.contains(instrumentAccId)){
							accountIdToUpdateSet.add(instrumentAccId);
							accountListToUpdate.add(account);
						}
	    			}
	    		}
	    	}
	    }
	    if(accountMSAList.size()>0){
	      insert accountMSAList;
	    }  
	    if(accountListToUpdate.size()>0){
	      update accountListToUpdate;
	    }
    }catch(Exception ex){ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'CreateAccountMSARecordBatch', true);}
  }
  
  global void finish(Database.BatchableContext BC){
  
  }
  
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
      CreateAccountMSARecordBatch createRecords = new CreateAccountMSARecordBatch();
      Database.executeBatch(createRecords, 20);
    }
  }
  
  public Set<String> getAccountNumberSetfromList(String accountNumberString){
  	Set<String> accountNumberSet = new Set<String>();
  	Set<String> accountNumberFinalSet = new Set<String>();
	     List<String> tempList = new List<String>();
	     tempList = accountNumberString.split(',');
	     accountNumberSet.addAll(tempList);
	  for(String actNo : accountNumberSet){
	        actNo = actNo + '-0';
	        accountNumberFinalSet.add(actNo);
	  }
	  return accountNumberFinalSet;
  }
}