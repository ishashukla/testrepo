// 
// (c) 2016 Appirio, Inc.
// Endo_SendEmailtoOrderContact
// 27 June 2016    Pratibha Chimppa   Original (T-497178)
// 28th June 2016  Pratibha Chhimpa   T-497203(Added additional contact from Order for mailing OrderItems)
// 27 July 2016    Nathalie Le Guay   Fixed email notification problem: no OrderItem were retrieved.
//                                    Remove OrderItem WHERE clause, and instead added filters in the body of execute()
// 01 Aug 2016   Nathalie Le Guay     Change filter (if statement) so that email is only for OrderItem 'shipped yesterday'
//                                    Recommending to add email to admin in case of batch error.
// 02 Aug 2016  Pratibha Chhimpa      Adding Order.OrderNumber,Order.Account.AccountNumber, Order.Account.Name in eventLogQuery
// 03 Aug 2016  Pratibha Chhimpa      T-523892 | Edit Finish method for AsyncApexJob
// 03 Aug 2016  Daniel Saldana        Updating query to pull order products shipped that day
// 04 Aug 2016  Nathalie Le Guay      Don't send email if there is no matching OrderItem - Added if-statement
// 05 Aug 2016  Parul Gupta           I-227221 | Reviewed code and modified/refactored code
// 06 Oct 2016 Parul Gupta            T-542929 | Refer new field "Tracking_Numbers__c"

global class Endo_SendEmailtoOrderContact implements Database.Batchable<sObject>, Database.Stateful{

  global final String eventLogQuery; 
  global final String LINE_TYPE = Label.LINE_TYPE_SJC_STANDARD_LINE;  
  global final String OLISTATUS = Label.ORDER_LINE_ITEM_STATUS_SHIPPED;
  global Map<Id, List<OrderItem>> mapofOrderItem;
  global Date dtStart;
  global List<String> orderType;

  /****************************************************************************
    // Constructor
  /***************************************************************************/
  global Endo_SendEmailtoOrderContact(){

        dtStart = Date.Today();
      orderType = new List<String> { 'SJC CPT Order','SJC Certified Pre-Owned','SJC Consignment',
      'SJC Credit and Replacement','SJC FOB','SJC FOB Certified Pre Owned','SJC FOB Finance','SJC FOB with Trade In',
      'SJC Finance','SJC Interco Loaner','SJC Intercompany','SJC Loaner','SJC Marketing Sale','SJC Rental',
      'SJC Rep Loaner','SJC Sample Capital for Rep','SJC Sample Disposable Sale','SJC Sample Disposable for Rep',
      'SJC Sample Refurbish Sale','SJC Sample Sale','SJC Standard','SJC Standard with Trade In','SJC Trade In',
      'SPR Safety Stock Transfer' };
      
    mapofOrderItem = new Map<Id, List<OrderItem>>();
      
    // Query on Order to get all Order Products related to it
        eventLogQuery = 'Select Id, Type, Contact__c, Additional_Contact_for_Tracking_Info__c,'+
                      '(Select Shipped_Date__c, PartNo__c, Description, Quantity_Shipped__c, OrderId, Order.PoNumber, Status__c,'+
                      ' Order.OrderNumber, Order.Account.AccountNumber, Order.Account.Name,' +
                      ' Oracle_Ordered_Item__c, Quantity, Unit_of_Measure__c, Shipping_Method__c, Tracking_Numbers__c, PriceBookEntry.Product2.Name, Line_Type__c' +
                      ' From OrderItems where Shipped_Date__c = :dtStart AND Line_Type__c = :LINE_TYPE AND Status__c =:OLISTATUS)'+
                      ' From Order Where Order.Type IN :orderType and Contact__c != null';
  }
  
  /****************************************************************************
    // Start the batch
  /***************************************************************************/
  global Database.QueryLocator start(Database.BatchableContext BC){
    system.debug('Endo_SendEmailtoOrderContact - Query ::' + eventLogQuery);
    return Database.getQueryLocator(eventLogQuery);
  }
  
  /****************************************************************************
    // Execute the batch
  /***************************************************************************/
  global void execute(Database.BatchableContext BC, List<Order> scope){
  
    // Defining mapofOrderItem, Goal to have {ContactID for Mail, List of all Order Products associated }   
    for (Order ord: scope){
              
      // If order line items are associated with order then iterate l
      if (!ord.OrderItems.isEmpty()){
        addOrderLineItems(ord.Contact__c, ord.Additional_Contact_for_Tracking_Info__c, ord.OrderItems);
            }             
    }
    system.debug('Endo_SendEmailtoOrderContact - mapofOrderItem ::' + mapofOrderItem);
    }


  /****************************************************************************
    // Finish the batch
  /***************************************************************************/
  global void finish(Database.BatchableContext BC){
    Endo_EmailUitlity emailUtility = new Endo_EmailUitlity(); 
    
    // Send email to contact and additional contact
    if (!mapofOrderItem.keySet().isEmpty()){
        sendMailToContact(emailUtility);
    }
            
    AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, 
                                        JobItemsProcessed, TotalJobItems, CreatedBy.Email
                      FROM AsyncApexJob WHERE Id =: BC.getJobId()];
    if(a.NumberOfErrors > 0) {
            List<String> toAddresses = new List<String>(); 
        System.debug('\n[BatchAccountType: finish]: [The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.]]');
        
        // Get endo division admins
        for (Division_Admin__c divAdmin : Division_Admin__c.getAll().values()){
          if(divAdmin.Endo__c == true) {
            toAddresses.add(divAdmin.Email__c);
          }
        }
        String userId = UserInfo.getUserId();
        String emailBody = 'The Batch Endo_SendEmailtoOrderContact job processed ' + a.JobItemsProcessed + ' batches out of ' + 
                            a.TotalJobItems + ' and generated ' + a.NumberOfErrors;  
                                               
        Messaging.SingleEmailMessage mail = emailUtility.createEmailMessage('SFDC Batch Error Alert: Endo_SendEmailtoOrderCustomers failed', 
                                                                                emailBody, null, toAddresses, userId, false);
       
        // Send email to admins for batch failure
        if (!Test.isRunningTest()) {            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            return;
        }
        }
  }
  
  /****************************************************************************
    //Add order line items in a map
  /***************************************************************************/
    private void addOrderLineItems(Id contactId, Id additionalContactId, List<OrderItem> orderItems){
        if (mapofOrderItem.get(contactId) == null ){
        mapofOrderItem.put(contactId, new List<OrderItem>());
    } 
    
    // T-497203 | PC
    if (additionalContactId != null){
        if (mapofOrderItem.get(additionalContactId) == null ){
        mapofOrderItem.put(additionalContactId, new List<OrderItem>());
        }  
    }
                
    mapofOrderItem.get(contactId).addAll(orderItems);
    
    // T-497203 | PC        
        if (additionalContactId != null){
            mapofOrderItem.get(additionalContactId).addAll(orderItems);
    }          
    }
    
    
    /****************************************************************************
    // Send email to contacts and additional contact of Order 
    // which includes all order item details
  /***************************************************************************/
    private void sendMailToContact(Endo_EmailUitlity emailUtility){
        Map<Id, Contact> emailAddresses = new Map<Id, Contact>();       
        List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;   
        String body; 
        List<String> emailAdd;
        
        // Fetch contacst with email address   
      for (Contact con : [SELECT Email, Id, Name 
                                            FROM Contact 
                                            WHERE Id IN :mapofOrderItem.keySet()
                                            and Email != null]) {
        emailAddresses.put(con.id, con);
      }
      
      // Get Email Logo From Document
      String imageURL = emailUtility.getEndoEmailLogo();
        
      for (Id contactid : mapofOrderItem.keySet()) {
        
        //Contact's Email Address collection
        emailAdd = new List<String>();
        if (emailAddresses.containsKey(contactid)) {
            emailAdd.add(emailAddresses.get(contactid).Email);          
        }
        system.debug('Endo_SendEmailtoOrderContact - To Email Addresses' + emailAdd);
        
        // Get Email Body           
        body = emailUtility.getEmailBody(imageURL, emailAddresses.get(contactid).Name, mapofOrderItem.get(contactid));
        
        // Get Email Message            
        emailMessages.add(emailUtility.createEmailMessage(Label.Order_Product, null, body, emailAdd, contactid, true));
      }
      
      // Send email messages to contact
      if (!Test.isRunningTest()) {   
        Messaging.sendEmail(emailMessages);
      }
    }
    
}