// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_IPTriggerHandler
// Description: Handler Class for InstalledProductTrigger.
//
// April 6 2016, Prakarsh Jain  Original 
//
public class Instruments_IPTriggerHandler {
  //Method to populate product description field on Installed product(Stryker Ipad Issues workbook)
  public static void populateProductDescriptionOnInstalledProduct(List<InstalledProduct__c> newlist){
    Set<Id> productSet = new Set<Id>();
    Set<Id> ipSet = new Set<Id>();
    List<Product2> lstProducts = new List<Product2>(); 
    Map<Id, String> mapIdToDescription = new Map<Id, String>();
    List<InstalledProduct__c> updatedList = new List<InstalledProduct__c>();
    List<InstalledProduct__c> lstToBeUpdated = new List<InstalledProduct__c>();
    for(InstalledProduct__c ip : newlist){
      if(ip.Product__c != null){
        productSet.add(ip.Product__c);
        ipSet.add(ip.Id);
      }
    }
    updatedList = [SELECT Id, Name, Prod_Description__c, Product__c FROM InstalledProduct__c WHERE Id IN: ipSet];
    lstProducts = [SELECT Id, Name, Description FROM Product2 WHERE Id IN: productSet];
    for(Product2 pro : lstProducts){
      if(!mapIdToDescription.containsKey(pro.Id)){
        mapIdToDescription.put(pro.Id, pro.Description);
      }
    }
    for(InstalledProduct__c ip : updatedList){
      if(mapIdToDescription.containsKey(ip.Product__c)){
        ip.Prod_Description__c = mapIdToDescription.get(ip.Product__c);
        lstToBeUpdated.add(ip);
      }
    }
    if(lstToBeUpdated.size()>0){
      update lstToBeUpdated;
    }
  }
}