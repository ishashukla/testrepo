// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_CreateReplenishmentPOBatch - Batch class which queries Product Stock matching some filter criteria and based on that will create PO & PO Line Items.
//
// 3 Nov 2016, Isha Shukla (T-513975) 
//
global class COMM_CreateReplenishmentPOBatch implements Database.Batchable<sObject>, System.Schedulable{
    Id PORecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
    Id POLineRecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
    
    
    /***********************************************************************************
     Scheduler for batch
    ************************************************************************************/    
    global void execute(SchedulableContext sc){
        if(!Test.isRunningTest()){
            Id batchId = Database.executeBatch(new COMM_CreateReplenishmentPOBatch(), 200);
        }
    }
    
    public Database.QueryLocator start(Database.batchableContext bc) {
        String query = 'SELECT SVMXC__Quantity2__c, Maximum_Qty__c,Is_Replenished__c, SVMXC__Location__r.SVMXC__Service_Engineer__c, SVMXC__Reorder_Level2__c, SVMXC__Product__c, SVMXC__Location__r.Location_ID__c, SVMXC__Location__c '+
            'FROM SVMXC__Product_Stock__c ' +
            'WHERE IsMinThreshold__c = true AND Is_Replenished__c = false';
        system.debug('query'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<SVMXC__Product_Stock__c> productStock) {
        Map<Id,List<SVMXC__Product_Stock__c>> locationToProductStockMap = new Map<Id,List<SVMXC__Product_Stock__c>>();
        Map<Id,String> userToTechMap = new Map<Id,String>();
        Map<Id,Id> locationToUserMap = new Map<Id,Id>();
        for(SVMXC__Product_Stock__c stock : productStock) {
            if(!locationToProductStockMap.containsKey(stock.SVMXC__Location__c)) {
                locationToProductStockMap.put(stock.SVMXC__Location__c,new List<SVMXC__Product_Stock__c>{stock});
            } 
            else {
                locationToProductStockMap.get(stock.SVMXC__Location__c).add(stock);
            }
            if(!userToTechMap.containsKey(stock.SVMXC__Location__r.SVMXC__Service_Engineer__c)) {
                userToTechMap.put(stock.SVMXC__Location__r.SVMXC__Service_Engineer__c,'');
            }
            
            if(!locationToUserMap.containsKey(stock.SVMXC__Location__c)) {
                locationToUserMap.put(stock.SVMXC__Location__c,stock.SVMXC__Location__r.SVMXC__Service_Engineer__c);
            }
            
        }
        List<SVMXC__RMA_Shipment_Order__c> partOrderListToInsert = new List<SVMXC__RMA_Shipment_Order__c>();
        List<SVMXC__RMA_Shipment_Line__c> partLineListToInsert = new List<SVMXC__RMA_Shipment_Line__c>();
        Id accountId = [SELECT Id FROM Account WHERE Name = 'Division VAN' and recordType.name = 'Endo COMM Customer'].get(0).Id;
        if(locationToProductStockMap != null && accountId != null) {
            List<Address__c> addressRecord = new List<Address__c>([SELECT Location_ID__c 
                                                                   FROM Address__c 
                                                                   WHERE Business_Unit_Name__c = 'COMM' 
                                                                   AND Type__c = 'Billing' 
                                                                   AND Primary_Address_Flag__c = true 
                                                                   AND Account__c = :accountId]);            
            List<SVMXC__Site__c> locationRecord = new List<SVMXC__Site__c>([SELECT Id 
                                                                            FROM SVMXC__Site__c 
                                                                            WHERE Location_ID__c = :addressRecord[0].Location_ID__c]);
            List<SVMXC__Service_Group_Members__c> techrecord = new List<SVMXC__Service_Group_Members__c>([SELECT Id ,SVMXC__Salesforce_User__c
                                                                                                          FROM SVMXC__Service_Group_Members__c 
                                                                                                          WHERE SVMXC__Salesforce_User__c IN :userToTechMap.keySet()]);
            if(techrecord.size() > 0) {
                for(SVMXC__Service_Group_Members__c tech : techrecord) {
                    if(userToTechMap.containsKey(tech.SVMXC__Salesforce_User__c)) {
                        userToTechMap.put(tech.SVMXC__Salesforce_User__c,tech.Id);
                    }
                }
            }
            
            for(Id locationId : locationToProductStockMap.keySet()) {
                system.debug('tech>>'+userToTechMap.get(locationToUserMap.get(locationId)) +'  >>  '+locationToUserMap.get(locationId));
                SVMXC__RMA_Shipment_Order__c partOrder = new SVMXC__RMA_Shipment_Order__c();
                partOrder.SVMXC__Company__c = accountId ;
                partOrder.SVMXC__Fulfillment_Type__c = 'Auto Replenishment';
                partOrder.SVMXC__Source_Location__c = locationRecord[0].Id;
                partOrder.RecordTypeId = PORecordTypeId;
                partOrder.SVMXC__Destination_Location__c = locationId;
                partOrder.SVMXC__Order_Status__c = 'Closed';
                partOrder.SVMXC__Order_Type__c = 'Parts Request';
                partOrder.EU_PO__c = locationToProductStockMap.get(locationId).get(0).SVMXC__Location__r.Location_ID__c+date.today().format();
                
                if(userToTechMap != null) {
                    partOrder.Technician_Equipment__c = userToTechMap.get(locationToUserMap.get(locationId)); 
                }
                partOrderListToInsert.add(partOrder);
            }
            if(partOrderListToInsert.size() > 0) {
                insert partOrderListToInsert;
            }
            system.debug('partOrderListToInsert'+partOrderListToInsert);
            for(SVMXC__RMA_Shipment_Order__c partOrder : partOrderListToInsert) {
                for(SVMXC__Product_Stock__c stock : locationToProductStockMap.get(partOrder.SVMXC__Destination_Location__c)) {
                    SVMXC__RMA_Shipment_Line__c partLine = new SVMXC__RMA_Shipment_Line__c(
                        RecordTypeId = POLineRecordTypeId,SVMXC__RMA_Shipment_Order__c = partOrder.Id,
                        SVMXC__Product__c = stock.SVMXC__Product__c,
                        SVMXC__Actual_Quantity2__c = stock.Maximum_Qty__c - stock.SVMXC__Quantity2__c,
                        SVMXC__Expected_Quantity2__c = stock.Maximum_Qty__c - stock.SVMXC__Quantity2__c,
                        SVMXC__Line_Price2__c = 0,
                        SVMXC__Line_Status__c = 'Closed'
                    );
                    partLineListToInsert.add(partLine);
                }
            }
            if(partLineListToInsert.size() > 0) {
                insert partLineListToInsert;
            }  
            System.debug('partLineListToInsert'+partLineListToInsert);
        }
        
        for(SVMXC__Product_Stock__c stock : productStock) {
            stock.Is_Replenished__c = true;
        }        
        update productStock;
    }
    public void finish(Database.BatchableContext BC){
    }
}