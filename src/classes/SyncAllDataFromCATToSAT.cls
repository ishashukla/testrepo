// (c) 2016 Appirio, Inc.
//
// Class Name: SyncAllDataFromCATToSAT - Batch Class to sync ALL Data from Custom Account Team Member object to Standard Account Team members 

global class SyncAllDataFromCATToSAT implements Database.Batchable<sObject>,System.Schedulable {
  global Database.QueryLocator start(Database.BatchableContext BC){
    String query = '';
    query = 'SELECT User__c, Team_Member_Role__c, Name, IsDeleted__c, LastModifiedDate, Id, External_Integration_Id__c, Effective_From_Date__c, Division__c, Account__c, Account_Access_Level__c FROM Custom_Account_Team__c WHERE Team_Member_Role__c != null AND Account_Access_Level__c != null'  ;
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext BC,List<Sobject> scope){
    List<AccountTeamMember> accountMemberList = new List<AccountTeamMember>();
    Set<Id> userIdsSet = new Set<Id>();
    Set<Id> accountIdsSet = new Set<Id>();
    //changes start   T-516154 
    Map<String, Custom_Account_Team__c> mapAccIdUserIdToCustomAccTeam = new Map<String, Custom_Account_Team__c>();
    List<Custom_Account_Team__c> customAccTeamListToBeDeleted = new List<Custom_Account_Team__c>();
    //changes end   T-516154
    String picklistforEditAccess = Label.AccountTeamSyncEditAccessPicklistValues;
    List<AccountTeamMember> accountMemberListToBeInserted = new List<AccountTeamMember>();
    List<AccountTeamMember> accountMemberListToBeUpdated = new List<AccountTeamMember>();
    List<AccountTeamMember> accountMemberListToBeDeleted = new List<AccountTeamMember>();
    List<AccountShare> accountShareList = new List<AccountShare>();
    List<AccountShare> accountShareListtoUpdate = new List<AccountShare>();
    List<AccountShare> accountShareListtoInsert = new List<AccountShare>();
    List<AccountShare> accountShareListtoDelete = new List<AccountShare>();
    for(Sobject s : scope){
      Custom_Account_Team__c customAccountTeam = (Custom_Account_Team__c)s;
      userIdsSet.add(customAccountTeam.User__c);
      accountIdsSet.add(customAccountTeam.Account__c);
    }
    
    Map<String,AccountShare> AccUserRoletoShareRecord = new Map<String,AccountShare>();
    Map<String,AccountTeamMember> AccUserRoletoAccountTeam = new Map<String,AccountTeamMember>();
    for(AccountShare accShare : [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel 
                                 FROM AccountShare
                                 WHERE UserOrGroupId IN:userIdsSet
                                 AND AccountId IN: accountIdsSet
                                 AND RowCause =:Schema.AccountShare.RowCause.Manual]){
      AccUserRoletoShareRecord.put(accShare.AccountId+'+'+accShare.UserOrGroupId,accShare);
    }
    for(AccountTeamMember accMember : [SELECT UserId, TeamMemberRole, Id, AccountId, AccountAccessLevel 
                                       FROM AccountTeamMember 
                                       WHERE UserId IN: userIdsSet 
                                       AND AccountId IN: accountIdsSet]){
      AccUserRoletoAccountTeam.put(accMember.AccountId+'+'+accMember.UserId+'+'+accMember.TeamMemberRole,accMember);
    }
    for(Sobject s : scope){
      Custom_Account_Team__c customAccountTeam = (Custom_Account_Team__c)s;
      //Changes start I-224971
      if(customAccountTeam.IsDeleted__c){
        customAccTeamListToBeDeleted.add(customAccountTeam);
      }
      //Changes End I-224971
      String accTeamkey = customAccountTeam.Account__c+'+'+customAccountTeam.User__c+'+'+customAccountTeam.Team_Member_Role__c;
      String accSharekey = customAccountTeam.Account__c+'+'+customAccountTeam.User__c;
      //changes start   T-516154
      mapAccIdUserIdToCustomAccTeam.put(accTeamkey, customAccountTeam);
      //changes end   T-516154
      // Insert Standard Account team Records
      System.debug('>>>>1'+accTeamkey);
      System.debug('>>>>2'+AccUserRoletoAccountTeam.containsKey(accTeamkey));
      if(AccUserRoletoAccountTeam.containsKey(accTeamkey)){
        System.debug('>>>>3'+customAccountTeam.IsDeleted__c);
        if(customAccountTeam.IsDeleted__c){
          accountMemberListToBeDeleted.add(AccUserRoletoAccountTeam.get(accTeamkey));
        }
        else{
          AccUserRoletoAccountTeam.get(accTeamkey).TeamMemberRole = customAccountTeam.Team_Member_Role__c;
        }
      }
      else{
          if(!customAccountTeam.IsDeleted__c){
          accountMemberListToBeInserted.add(new AccountTeamMember(UserId = customAccountTeam.User__c,
                                        AccountId = customAccountTeam.Account__c,
                                        TeamMemberRole = customAccountTeam.Team_Member_Role__c));
          }
      }
      // Insert sharing records to povide Edit access to account team members and managers in Roles and Hierarchy
      if(AccUserRoletoShareRecord.containsKey(accSharekey)){
        if(customAccountTeam.IsDeleted__c){
          accountShareListtoDelete.add(AccUserRoletoShareRecord.get(accSharekey));
        }
        else{
          AccountShare accShare = AccUserRoletoShareRecord.get(accSharekey);
          if(picklistforEditAccess.containsIgnoreCase(customAccountTeam.Account_Access_Level__c)){
            if(accShare.AccountAccessLevel!='Edit'){
          accShare.AccountAccessLevel = 'Edit';
              accountShareListtoUpdate.add(accShare);
            }
          }
          else{
            if(accShare.AccountAccessLevel=='Edit'){
              accShare.AccountAccessLevel = 'Read';
              accountShareListtoUpdate.add(accShare);
            }
          }
        }
      }
      else{
          if(!customAccountTeam.IsDeleted__c){
          AccountShare accShare = new AccountShare();
          accShare.AccountId = customAccountTeam.Account__c;
          accShare.UserOrGroupId = customAccountTeam.User__c;
          accShare.RowCause = Schema.AccountShare.RowCause.Manual;
          accShare.OpportunityAccessLevel = 'None';
          accShare.ContactAccessLevel = 'None';
          accShare.CaseAccessLevel = 'None';
          if(picklistforEditAccess.containsIgnoreCase(customAccountTeam.Account_Access_Level__c)){
            accShare.AccountAccessLevel = 'Edit';
          }else{
            accShare.AccountAccessLevel = 'Read';
          }
          accountShareListtoInsert.add(accShare);
          }
      }
    }
    if(accountShareListtoInsert.size() > 0){
      Database.insert(accountShareListtoInsert,false);
    }
    if(accountShareListtoUpdate.size() > 0){
      Database.update(accountShareListtoUpdate,false);
    }
    if(accountShareListtoDelete.size() > 0){
      Database.delete(accountShareListtoDelete,false);
    }
    if(accountMemberListToBeInserted.size()>0){
      Database.insert(accountMemberListToBeInserted,false);
    }
    if(accountMemberListToBeUpdated.size()>0){
      Database.update(accountMemberListToBeUpdated,false);
    }
    if(accountMemberListToBeDeleted.size() > 0){
      Database.delete(accountMemberListToBeDeleted,false);
    }
    //changes start   T-516154
    if(customAccTeamListToBeDeleted.size()>0){
      Database.delete(customAccTeamListToBeDeleted,false);
    }
  }
  global void finish(Database.BatchableContext BC){
    Database.executeBatch(new UpdateDeleteSATAndAccountShareBatch());
  }
  global void execute(SchedulableContext sc){
    /*if(!Test.isRunningTest()){
      Id batchId = Database.executeBatch(new NotifyAccountTeamBatch());
      system.debug('@@@batchId = ' + batchId);
    }*/
  }
}