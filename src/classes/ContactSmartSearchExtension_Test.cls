/*******************************************************************
Name  :  ContactSmartSearchExtension_Test 
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 16, 2016

Modified: 
*************************************************************************/
@isTest
private class ContactSmartSearchExtension_Test {
  static testMethod void myUnitTest() {

    List<Instrument_New_Contact_Page_Field_Ids__c> fieldIds = new List<Instrument_New_Contact_Page_Field_Ids__c> ();
    fieldIds.add(new Instrument_New_Contact_Page_Field_Ids__c(First_Name__c = 'abc', Last_Name__c = 'cde', Email__c = 'efg', Phone__c = 'ghi', Account__c = 'ijk'));
    insert fieldIds;
        
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    insert acc;
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.FirstName = 'test';
    con.LastName = 'test';
    con.Email = 'test@test.com';
    con.Phone = '9812345566';
    insert con;
    
    Contact con2 = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con2.FirstName = 'test';
    con2.LastName = 'test';
    con2.Email = 'test@test.com';
    con.Phone = '9812345568';
    insert con2;
    Contact_Acct_Association__c contactAcctAssc = TestUtils.createConAcctAssc(1, acc.Id, con.Id, true).get(0);
    ApexPages.currentPage().getParameters().put('accId', acc.Id);
    ContactSmartSearchExtension controller = new ContactSmartSearchExtension(new ApexPages.StandardController(con));
    controller.contactFirstNameToSearch = 'test';
    controller.contactLastNameToSearch = 'test';
    controller.contactEmail = 'test@test.com';
    //controller.contactPhone = '9912138483';
    
    // Verify Cancel action
    PageReference prCancel = controller.cancel();
    System.assert(prCancel == null);
    
    // Verify Account association with Contact
    controller.selectedContactId = con2.Id;
    //controller.showAssociatedAccounts();
    
    controller.performSearch();
    
    List<Contact_Acct_Association__c> lstAssociation = [SELECT Id FROM Contact_Acct_Association__c 
                                                        WHERE Associated_Contact__c = :con.Id];
    System.assert(lstAssociation.size() > 0);
    
    List<ContactSmartSearchExtension.ContactWrapper> lst = controller.getWrapperList();
     
  }

  static testMethod void myUnitTestTwo() {

    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    insert acc;    
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.FirstName = 'test';
    con.LastName = 'test';
    con.Email = 'test@test.com';
    con.Phone = '9812345566';
    insert con;
    
    Contact con2 = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con2.FirstName = 'test';
    con2.LastName = 'test';
    con2.Email = 'test@test.com';
    con.Phone = '9812345568';
    insert con2;
    Contact_Acct_Association__c contactAcctAssc = TestUtils.createConAcctAssc(1, acc.Id, con.Id, true).get(0);
    ApexPages.currentPage().getParameters().put('accId', acc.Id);
    ContactSmartSearchExtension controller = new ContactSmartSearchExtension(new ApexPages.StandardController(con));

    // Verify Cancel action
    PageReference prCancel = controller.cancel();
    System.assert(prCancel == null);
    
    // Verify Account association with Contact
    controller.selectedContactId = con2.Id;
    
    controller.performSearch();
    
    List<Contact_Acct_Association__c> lstAssociation = [SELECT Id FROM Contact_Acct_Association__c 
                                                        WHERE Associated_Contact__c = :con.Id];
    System.assert(lstAssociation.size() > 0);
    
    List<ContactSmartSearchExtension.ContactWrapper> lst = controller.getWrapperList();
     
  } 
    
  static testMethod void myUnitTest2() {

    List<Instrument_New_Contact_Page_Field_Ids__c> fieldIds = new List<Instrument_New_Contact_Page_Field_Ids__c> ();
    fieldIds.add(new Instrument_New_Contact_Page_Field_Ids__c(First_Name__c = 'abc', Last_Name__c = 'cde', Email__c = 'efg', Phone__c = 'ghi', Account__c = 'ijk'));
    insert fieldIds;
        
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    insert acc;
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.FirstName = 'test';
    con.LastName = 'test';
    con.Email = 'test@test.com';
    con.Phone = '9812345566';
    insert con;
    
    Contact con2 = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con2.FirstName = 'test';
    con2.LastName = 'test';
    con2.Email = 'test@test.com';
    con.Phone = '9812345568';
    insert con2;
    Contact_Acct_Association__c contactAcctAssc = TestUtils.createConAcctAssc(1, acc.Id, con.Id, true).get(0);
    ApexPages.currentPage().getParameters().put('accId', acc.Id);
    ContactSmartSearchExtension controller = new ContactSmartSearchExtension(new ApexPages.StandardController(con));
    controller.contactFirstNameToSearch = 'test';
    controller.contactLastNameToSearch = 'test';
    controller.contactEmail = 'test@test.com';
    controller.contactPhone = '9912138483';
    
    // Verify Cancel action
    PageReference prCancel = controller.cancel();
    System.assert(prCancel == null);
    
    // Verify Account association with Contact
    controller.selectedContactId = con2.Id;
    //controller.showAssociatedAccounts();
    
    controller.performSearch();
    
    List<Contact_Acct_Association__c> lstAssociation = [SELECT Id FROM Contact_Acct_Association__c 
                                                        WHERE Associated_Contact__c = :con.Id];
    System.assert(lstAssociation.size() > 0);
    
    List<ContactSmartSearchExtension.ContactWrapper> lst = controller.getWrapperList();
     
  }
}