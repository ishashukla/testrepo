public class BigMachinesQuoting {
    
    //Product name for Phoenix Quote pricing that should not be deleted by BM Quoting
    private static String phxPrPartNumber = 'PHX_PR_19800805';

    public static void syncQuoteWithOpty(ID quoteId, ID opportunityId) {
        BigMachines__Quote__c[] quotesToUpdate = markOthersAsNonPrimary(quoteId, opportunityId);
        OpportunityLineItem[] oppProdToDelete = getOldOpportunityProducts(opportunityId);

        
        /*
            To update opportunity with values from the primary quote:
                1) type additional quote fields in "quote" select statement
                2) assign the fields to the opportunity (ie. opportunity.FieldA = quote.FieldB__c) 
        */
       /* BigMachines__Quote__c quote = [select Name, Amount__c, BigMachines__Total__c, BigMachines__Pricebook_Id__c from BigMachines__Quote__c where Id = :quoteId];
        Opportunity opportunity = [select Name, Pricebook2Id, CurrencyIsoCode from Opportunity where Id = :opportunityId];
        opportunity.Amount = quote.Amount__c;

        // get opportunity pricebook, if pricebook is not specified use standard pricebook        
        ID optyPricebookId = opportunity.Pricebook2Id;
        String oppCurrencyCode = opportunity.CurrencyIsoCode;
        if (optyPricebookId == null) {
            optyPricebookId = [select Id from Pricebook2 where IsStandard = true].Id;
        }

        // get quote pricebook, pricebook ID on quote is stored as a String
        // if no pricebook Id is specified on quote then just use opportunity pricebook
        // if pricebook Id is specified on quote then it must match opportunity
        ID quotePricebookId = null;
        try {
            quotePricebookId = (ID)quote.BigMachines__Pricebook_Id__c;
        } catch (Exception e) {} 
        if (quotePricebookId != null && quotePricebookId != optyPricebookId) {
            quote.BigMachines__Is_Primary__c = false;
            update quote;
            throw new QuoteSyncException('Could not sync quote [' + quote.Name + '] with opportunity ['
                    + opportunity.Name + '] because they are using different pricebooks.  '
                    + 'In order to sync this quote and opportunity, first change the pricebook on one '
                    + 'of the objects to match the other and then try again.');
        }    
*/
        /*
            To update opportunity products with additional fields from quote products
                1) type additional quote product fields in "quoteProds" select statement
                2) include additional assignments in OpportunityLineItem constructor below
        */
        //OpportunityLineItem[] oppProdToCreate = new OpportunityLineItem[200];
     /*   List<OpportunityLineItem> oppProdToCreate = new List<OpportunityLineItem>();
        BigMachines__Quote_Product__c[] quoteProds = [select Id, Name, BigMachines__Product__c, BigMachines__Quantity__c, BigMachines__Sales_Price__c, BigMachines__Description__c 
                                         from BigMachines__Quote_Product__c where BigMachines__Quote__c = :quoteId];
        if (quoteProds.size() > 0) {
            Set<ID> productIdSet = new Set<ID>();
            for (BigMachines__Quote_Product__c qProd : quoteProds) {
                productIdSet.add(qProd.BigMachines__Product__c);
            }
            PricebookEntry[] pbEntries = [select Id, Product2Id from PricebookEntry 
                                          where Pricebook2Id = :optyPricebookId and CurrencyIsoCode = :oppCurrencyCode and Product2Id in :productIdSet];
            if (productIdSet.size() != pbEntries.size()) {
                quote.BigMachines__Is_Primary__c = false;
                update quote;
                if (productIdSet.size() > pbEntries.size()) {
                    throw new QuoteSyncException('Could not sync quote [' + quote.Name + '] with opportunity ['
                            + opportunity.Name + '] because at least one product did not have a corresponding '
                            + 'pricebook entry in the opportunity\'s pricebook.  This can be corrected by '
                            + 'manually adding the pricebook entry in Salesforce or by having a BigMachines '
                            + 'administrator run a part sync.');
                } else {
                    throw new QuoteSyncException('Could not sync quote [' + quote.Name + '] with opportunity ['
                            + opportunity.Name + '] because there are more pricebook entries than products.  '
                            + 'To correct this, the Apex code that deals with the BigMachines integration '
                            + 'probably needs to be updated to select the correct currency.');
                }
            }
            Map<ID,ID> prodMap = new Map<ID,ID>();
            for (PricebookEntry pbEntry : pbEntries) {
                prodMap.put(pbEntry.Product2Id, pbEntry.Id);
            }
            for (BigMachines__Quote_Product__c qProd : quoteProds) {
                oppProdToCreate.add(new OpportunityLineItem(PriceBookEntryId = prodMap.get(qProd.BigMachines__Product__c), 
                                                            Quantity = qProd.BigMachines__Quantity__c, 
                                                            UnitPrice = qProd.BigMachines__Sales_Price__c, 
                                                            Part_Number__c = qProd.Name,
                                                            Description = qProd.BigMachines__Description__c,
                                                            OpportunityId = opportunity.Id));
            }
        } KS*/
        
        //commit changes
     //KS   update opportunity;
        update quotesToUpdate;

      /*  Integer numberOfRecords = Limits.getDMLRows() + oppProdToDelete.size();
        if (numberOfRecords < Limits.getLimitDMLRows()) {
            delete oppProdToDelete;
            numberOfRecords += oppProdToCreate.size();
        }
        if (numberOfRecords < Limits.getLimitDMLRows()) {
            insert oppProdToCreate;
        }
        if (numberOfRecords >= Limits.getLimitDMLRows()) {
            opportunity.BigMachines_Message__c = 'Syncing large number of opportunity products.  Please refresh page to see updated opportunity values.';
            update opportunity;
            updateProductsLater(quoteId, opportunityId);
        }*/
        
    }
    
    private static BigMachines__Quote__c[] markOthersAsNonPrimary(ID quoteId, ID opportunityId) {
        BigMachines__Quote__c[] otherQuotes = [select Id, BigMachines__Is_Primary__c, BigMachines__Opportunity__c from BigMachines__Quote__c 
                                              where BigMachines__Opportunity__c = :opportunityId and Id != :quoteId];
        for (BigMachines__Quote__c quote : otherQuotes) {
            //set the quote to non-primary 
            quote.BigMachines__Is_Primary__c = false;
        }
        return otherQuotes;
    }
    
   /*
    * XXX 001
    *amind modification
    */
    private static OpportunityLineItem[] getOldOpportunityProducts(ID opportunityId) {
        return [select Id, OpportunityId from OpportunityLineItem
                where OpportunityId = :opportunityId and Part_Number__c <> :phxPrPartNumber];       
    }
    
    /*
    * XXX 001- the old code(without amind modification)
    private static OpportunityLineItem[] getOldOpportunityProducts(ID opportunityId) {
        return [select Id, OpportunityId from OpportunityLineItem
                where OpportunityId = :opportunityId];       
    }
    */
    
    private static OpportunityLineItem[] getNewOpportunityProducts(ID quoteId, ID opportunityId) {
        Opportunity opportunity = [select Name, Pricebook2Id from Opportunity where Id = :opportunityId];
        List<OpportunityLineItem> oppProdToCreate = new List<OpportunityLineItem>();
        BigMachines__Quote_Product__c[] quoteProds = [select Id, Name, BigMachines__Product__c, BigMachines__Quantity__c, BigMachines__Sales_Price__c, BigMachines__Description__c 
                                         from BigMachines__Quote_Product__c where BigMachines__Quote__c = :quoteId];
        if (quoteProds.size() > 0) {
            ID pricebookId = opportunity.Pricebook2Id;
            if (pricebookId == null) {
                pricebookId = [select Id from Pricebook2 where IsStandard = true].Id;
            }
            Set<ID> productIdSet = new Set<ID>();
            for (BigMachines__Quote_Product__c qProd : quoteProds) {
                productIdSet.add(qProd.BigMachines__Product__c);
            }
            PricebookEntry[] pbEntries = [select Id, Product2Id from PricebookEntry 
                                          where Pricebook2Id = :pricebookId and Product2Id in :productIdSet];
            Map<ID,ID> prodMap = new Map<ID,ID>();
            for (PricebookEntry pbEntry : pbEntries) {
                prodMap.put(pbEntry.Product2Id, pbEntry.Id);
            }
            for (BigMachines__Quote_Product__c qProd : quoteProds) {
                oppProdToCreate.add(new OpportunityLineItem(PriceBookEntryId = prodMap.get(qProd.BigMachines__Product__c), 
                                                            Quantity = qProd.BigMachines__Quantity__c, 
                                                            UnitPrice = qProd.BigMachines__Sales_Price__c,
                                                            Part_Number__c = qProd.Name,
                                                            Description = qProd.BigMachines__Description__c,
                                                            OpportunityId = opportunity.Id));
            }
        }
        return oppProdToCreate;
    }

    @future private static void updateProductsLater(ID quoteId, ID opportunityId) {
        Opportunity opportunity = [select Id from Opportunity where Id = :opportunityId];
        OpportunityLineItem[] oppProdToDelete = getOldOpportunityProducts(opportunityId);
        OpportunityLineItem[] oppProdToInsert = getNewOpportunityProducts(quoteId, opportunityId);
        opportunity.BigMachines_Message__c = '';
        delete oppProdToDelete;
        insert oppProdToInsert;
        update opportunity;
    }

    public class QuoteSyncException extends Exception {}    
    
    static testMethod void testSyncQuoteWithOpty() {
        Opportunity opty = new Opportunity();
        opty.Name = 'BigMachines test opportunity for testSyncQuoteWithOpty()';
        opty.StageName = 'Prospecting';
        opty.CloseDate = Date.today();
        insert opty;

        // BigMachines__Quote__c.ensurePrimary - begin test
        BigMachines__Quote__c[] quotes = new BigMachines__Quote__c[3];
        for (Integer i=0; i<quotes.size(); i++) {
            quotes[i] = new BigMachines__Quote__c();
            quotes[i].Name = 'BigMachines test quote ' + (i+1) + ' for testSyncQuoteWithOpty()';
            quotes[i].BigMachines__Opportunity__c = opty.Id;
        }
        insert quotes;  
        // BigMachines__Quote__c.ensurePrimary - end test

        Product2[] prod = new Product2[6];
        for (Integer i=0; i<prod.size(); i++) {
            prod[i] = new Product2();
            prod[i].Name = 'BigMachines test proudct ' + i + ' for testSyncQuoteWithOpty()';
            prod[i].IsActive = true;
            prod[i].BigMachines_Part_Number__c = 'BMITest0' + i;
        }
        insert prod;
        Pricebook2 pbStandard = [select Id from Pricebook2 where IsStandard = true];
        PricebookEntry[] pbookEntry = new PricebookEntry[5];
        for (Integer i=0; i<pbookEntry.size(); i++) {
            pbookEntry[i] = new PricebookEntry();
            pbookEntry[i].IsActive = true;
            pbookEntry[i].Pricebook2Id = pbStandard.id; 
            pbookEntry[i].Product2Id = prod[i].id;
            pbookEntry[i].UnitPrice = i;
        }  
        insert pbookEntry;
		
        opty.Pricebook2Id = pbStandard.Id; //Added by Gagan Brar on 5/5/2015
        Update opty; //Added by Gagan Brar on 5/5/2015
        
        // BigMachinesQuoting.syncQuoteWithOpty - begin positive test
        BigMachines__Quote_Product__c[] qProd = new BigMachines__Quote_Product__c[5];  
        for (Integer i=0; i<qProd.size(); i++) {
            qProd[i] = new BigMachines__Quote_Product__c();
            qProd[i].Name = 'BMITest0' + i;
            qProd[i].BigMachines__Quote__c = quotes[0].id;
            qProd[i].BigMachines__Sales_Price__c = i;
            qProd[i].BigMachines__Quantity__c = i+1;
            qProd[i].BigMachines__Product__c = prod[i].Id; //Added by Gagan Brar on 5/5/2015
        }
        insert qProd;
        quotes[0].BigMachines__Is_Primary__c = true;
        update quotes;      
        updateProductsLater(quotes[0].id, opty.id);
        // BigMachinesQuoting.syncQuoteWithOpty - end positive test
        
        // BigMachines__Quote_Product__c.propogateModifications - begin test
        delete qProd[0];
        // BigMachines__Quote_Product__c.propogateModifications - begin test

        /*// BigMachinesQuoting.syncQuoteWithOpty - begin no product price negative test 
        BigMachines__Quote_Product__c noPriceProd = new BigMachines__Quote_Product__c();
        noPriceProd.Name = 'BMITest05';
        noPriceProd.BigMachines__Quote__c = quotes[0].id;
        noPriceProd.BigMachines__Sales_Price__c = 5;
        noPriceProd.BigMachines__Quantity__c = 6;
        boolean throwsException = false;
        try {
            insert noPriceProd;
        } catch (Exception e) {
            throwsException = true;
        }
        System.assert(throwsException); */
        // BigMachinesQuoting.syncQuoteWithOpty - end no product price negative test

        // BigMachines__Quote_Product__c.propogateModifications - begin error check test
        qProd = new BigMachines__Quote_Product__c[2];
        for (Integer i=0; i<qProd.size(); i++) {
            qProd[i] = new BigMachines__Quote_Product__c();
            qProd[i].Name = 'BMITest0' + i;
            qProd[i].BigMachines__Quote__c = quotes[i+1].id;
            qProd[i].BigMachines__Sales_Price__c = 1;
            qProd[i].BigMachines__Quantity__c = 1;
        }
        insert qProd;
        delete qProd;
        // BigMachines__Quote_Product__c.propogateModifications - end error check test

        // BigMachines__Quote__c.promotePrimaryProdToOppty - begin multi-case test
        quotes[0].BigMachines__Is_Primary__c = false;
        quotes[1].BigMachines__Is_Primary__c = true;
        quotes[2].BigMachines__Is_Primary__c = true;
        update quotes;
        // BigMachines__Quote__c.promotePrimaryProdToOppty - end multi-case test
        
        // BigMachinesQuoting.syncQuoteWithOpty - begin invalid pricebook negative test
        Pricebook2 pbook = new Pricebook2();
        pbook.Name = 'Different pricebook';
        insert pbook;
        quotes[0].BigMachines__Is_Primary__c = true;
        quotes[0].BigMachines__Pricebook_Id__c = pbook.id;
        /* throwsException = false;
        try {
            update quotes;
        } catch (Exception e) {
            throwsException = true;
        }
        System.assert(throwsException); */
        // ignore invalid pricebook Id
        quotes[0].BigMachines__Pricebook_Id__c = 'garbage';
        quotes[0].BigMachines__Is_Primary__c = true;
        update quotes;
        // BigMachinesQuoting.syncQuoteWithOpty - end invalid pricebook negative test
    } 

}