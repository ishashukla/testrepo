// (c) 2015 Appirio, Inc.
//
// Class Name: FlexContractTriggerHandler
//
// March 02 2016, Prakarsh Jain  Original (T-481264)
//
public class FlexContractTriggerHandler{
  public static final String MASTERFINAGRMT = 'MA';
  public static final String MASTERLEASEAGRMT = 'MLA';
  public static final String MASTERRENTALAGRMT = 'MRA';
  //Method to update checkboxes on Account as per the value of Master Agreement Type field on Flex Contract
  public static void updateCheckBoxes(Map<Id, Flex_Contract__c> oldMap, List<Flex_Contract__c> newList, Boolean isDel){
    Set<Id> accountsSetId = new Set<Id>();
    List<Flex_Contract__c> lstContracts = new List<Flex_Contract__c>();
    Map<String,Integer> stringToIntegerMap = new Map<String,Integer>();
    List<Account> lstAccounts = new List<Account>();
    List<Account> accountsToBeUpdated = new List<Account>();
    for(Flex_Contract__c contract : newList){
      if(oldMap == null || (oldMap.get(contract.Id).Account__c != contract.Account__c)){
        accountsSetId.add(contract.Account__c);
        if(oldMap!=null && oldMap.get(contract.Id).Account__c != contract.Account__c){
          accountsSetId.add(oldMap.get(contract.Id).Account__c);
          stringToIntegerMap.put(oldMap.get(contract.Id).Account__c + '+' + MASTERFINAGRMT,0);
          stringToIntegerMap.put(oldMap.get(contract.Id).Account__c + '+' + MASTERLEASEAGRMT,0);
          stringToIntegerMap.put(oldMap.get(contract.Id).Account__c + '+' + MASTERRENTALAGRMT,0);
        }
      }
      else if(isDel){
        accountsSetId.add(contract.Account__c);
        stringToIntegerMap.put(contract.Account__c + '+' + MASTERFINAGRMT, 0);
        stringToIntegerMap.put(contract.Account__c + '+' + MASTERLEASEAGRMT, 0);
        stringToIntegerMap.put(contract.Account__c + '+' + MASTERRENTALAGRMT, 0);
      }else{
        accountsSetId.add(contract.Account__c);
        accountsSetId.add(oldMap.get(contract.Id).Account__c);
        stringToIntegerMap.put(oldMap.get(contract.Id).Account__c + '+' + MASTERFINAGRMT,0);
        stringToIntegerMap.put(oldMap.get(contract.Id).Account__c + '+' + MASTERLEASEAGRMT,0);
        stringToIntegerMap.put(oldMap.get(contract.Id).Account__c + '+' + MASTERRENTALAGRMT,0);
      }
    }
    lstContracts = [SELECT Id, Name, Master_Agreement_Type__c, Account__c FROM Flex_Contract__c WHERE Account__c IN: accountsSetId];
    for(Flex_Contract__c con : lstContracts){
      String key = con.Account__c + '+' + con.Master_Agreement_Type__c;
      if(!stringToIntegerMap.containsKey(key)){
        stringToIntegerMap.put(key,0);
      }
      if(stringToIntegerMap.containsKey(key)){
        Integer tempKey = stringToIntegerMap.get(key);
        tempKey = tempKey + 1;
        stringToIntegerMap.put(key,tempKey);
      }
      
    }
    lstAccounts = [SELECT Id, Name, Active_MRA__c, Active_MLA__c, Active_MFA__c FROM Account WHERE Id IN: accountsSetId];
    for(Account account : lstAccounts){
      if(stringToIntegerMap.containsKey(account.Id + '+' + MASTERFINAGRMT)){
        if(stringToIntegerMap.get(account.Id + '+' + MASTERFINAGRMT) == 0){
          account.Active_MFA__c = false;
        }
        else{
          account.Active_MFA__c = true;
        }
      }
      if(stringToIntegerMap.containsKey(account.Id + '+' + MASTERLEASEAGRMT)){
        if(stringToIntegerMap.get(account.Id + '+' + MASTERLEASEAGRMT) == 0){
          account.Active_MLA__c = false;
        }
        else{
          account.Active_MLA__c = true;
        }
      }
      if(stringToIntegerMap.containsKey(account.Id + '+' + MASTERRENTALAGRMT)){
        if(stringToIntegerMap.get(account.Id + '+' + MASTERRENTALAGRMT) == 0){
          account.Active_MRA__c = false;
        }
        else{
          account.Active_MRA__c = true; 
        }
      }
      accountsToBeUpdated.add(account);
    }
    if(accountsToBeUpdated.size() > 0){
      update accountsToBeUpdated;
    }
  }
  // Added by Nishank for Story S-422734
    public static void PopulateTaxFields(List<Flex_Contract__c> newList){
    List<Id> oppIds = new List<Id>();
    Map<id,Flex_Credit__c> MapOppIdandFlexCredit = new Map<id,Flex_Credit__c>();
    Id flexRTOnlyConventional = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();  
    Id flexRTOnlyRental = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Rental').getRecordTypeId();  
    
    for (Flex_Contract__c contract : newList){
     if(contract.Opportunity__c <> null){
     oppIds.add(contract.Opportunity__c);
     }
    }
     Map<id,Flex_Credit__c> MapFlexCredit = new  Map<id,Flex_Credit__c>([SELECT Id, EOT_Sales_Tax_Treatment__c , EOT_Sales_Tax__c, Total_Sales_Tax_Rate__c,  Opportunity__c, Opportunity__r.RecordTypeId, Opportunity__r.End_Of_Term__c FROM Flex_Credit__c where Opportunity__c IN : oppIds]);
     
     for (Flex_Credit__c credit : MapFlexCredit.values()){
     if(credit.Opportunity__c <> null && !MapOppIdandFlexCredit.ContainsKey(credit.Opportunity__c) && (credit.Opportunity__r.RecordTypeId == flexRTOnlyConventional  || credit.Opportunity__r.RecordTypeId == flexRTOnlyRental )){
     MapOppIdandFlexCredit.put(credit.Opportunity__c,credit);
     }
     }
     
    for (Flex_Contract__c contract : newList){
     if(contract.Opportunity__c <> null && MapOppIdandFlexCredit.ContainsKey(contract.Opportunity__c) && !String.isNotBlank(contract.Tax_Treatment__c)){
      if(MapOppIdandFlexCredit.get(contract.Opportunity__c).Opportunity__r.End_Of_Term__c <> null && MapOppIdandFlexCredit.get(contract.Opportunity__c).Opportunity__r.End_Of_Term__c == 'FMV' && MapOppIdandFlexCredit.get(contract.Opportunity__c).EOT_Sales_Tax_Treatment__c <> null){
       contract.Tax_Treatment__c = MapOppIdandFlexCredit.get(contract.Opportunity__c).EOT_Sales_Tax_Treatment__c;
      }
      if(MapOppIdandFlexCredit.get(contract.Opportunity__c).Opportunity__r.End_Of_Term__c <> null && MapOppIdandFlexCredit.get(contract.Opportunity__c).Opportunity__r.End_Of_Term__c == '$1 Out' && MapOppIdandFlexCredit.get(contract.Opportunity__c).EOT_Sales_Tax__c <> null){
       contract.Tax_Treatment__c = MapOppIdandFlexCredit.get(contract.Opportunity__c).EOT_Sales_Tax__c;
      }
     }
      if(contract.Opportunity__c <> null && MapOppIdandFlexCredit.ContainsKey(contract.Opportunity__c) && contract.Tax_Rate__c == null && MapOppIdandFlexCredit.get(contract.Opportunity__c).Total_Sales_Tax_Rate__c <> null){
       contract.Tax_Rate__c = MapOppIdandFlexCredit.get(contract.Opportunity__c).Total_Sales_Tax_Rate__c;
     }
    }
    
    }
     // End of changes added by Nishank for Story S-422734
}