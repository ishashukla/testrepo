/*
Modified By : Shreerath Nair on 10,June 2016 (T-510461) to redirect for Government User
*/
public class NewContactIntermediatePageController {
    public Contact con{get;set;}
    public String profileName;
    public Id selectedRecordType {get;set;}
    public Boolean selectedRecordTypeB {get;set;}
    public Id selectedAccID {get;set;}
    public Boolean isInstruments {get;set;}
    public Boolean isMedical {get;set;}  
    public Boolean isNV {get;set;}
    public String redirectURL {get;set;}
    //T-510461 (SN)
    public boolean isGovernment {get;set;}
    public String accId{get;set;}
    public List<Account> accList;
    public boolean isGovernmentandMedical{get;set;}
    
    public NewContactIntermediatePageController(ApexPages.StandardController controller){
        selectedRecordType = ApexPages.currentPage().getParameters().get('RecordType');
        system.debug('<<<<????????' + selectedRecordType);
        selectedRecordTypeB = (selectedRecordType != null) ? true : false;
        con = new Contact();
        List<Profile> profileLsit = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        profileName = profileLsit.get(0).Name;
        isInstruments = (profileName.containsIgnoreCase('Instrument')) ? true : false;
        isMedical = (profileName.containsIgnoreCase('Medical')) ? true : false;
        redirectURL = '';
        for(String u : ApexPages.currentPage().getParameters().keySet()){
          if(u != 'save_new'){
              redirectURL += '&'+ u + '=' + ApexPages.currentPage().getParameters().get(u);
          }
        }
        
        /*********T-510461 (SN) Made Changes to work with Government Users as well *****************/ 
        isGovernment = (profileName.containsIgnoreCase('Government')) ? true : false;
       	accId = ApexPages.currentPage().getParameters().get('accId');
       	accList = new List<Account>();
        //Check for the account recordtype for Government user if its for medical
        if(accId!=null){
        	SET<ID> AccountRecordTypeIds = new Set<ID>{Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.MEDICAL_ACCOUNT_RECORD_TYPE_MEDICAL_CALL_CENTER).getRecordTypeId(),
                                                  	   Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.MEDICAL_ACCOUNT_RECORD_TYPE_MEDICAL_PLACEHOLDER).getRecordTypeId()};
        	
        	accList = [SELECT id,Name From Account where id =: accId and recordTypeId IN : AccountRecordTypeIds];
        	
        }
        //check if the profile is Government and is using Instrument Account Records
        isGovernment = (profileName.containsIgnoreCase('Government') && accList.size()==0) ? true : false;
             
        //set the recordtype of Contact when government user create new Contact from Account's related List.
        if(accList.size()>0){
        	
        	if(selectedRecordType == null && profileName.containsIgnoreCase('Government')){
        		
        		selectedRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Intr_Constants.MEDICAL_CONTACT_RECORD_TYPE_MEDICAL_CUSTOMER).getRecordTypeId();
        	}
        	 
        }
        //Check if Government User and is selecting the recordtype of Medical from Contact's 'New' button 
        isGovernmentandMedical = (profileName.containsIgnoreCase('Government') && selectedRecordType == Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Intr_Constants.MEDICAL_CONTACT_RECORD_TYPE_MEDICAL_CUSTOMER).getRecordTypeId()) ? true : false;
        /*********T-510461 (SN) *****************************************************************/ 
    }
    public PageReference redirectToAccount(){
    	
        if(con.accountId!=null){
        	PageReference pg;
        	//T-510461 (SN) Redirect Page if Government User and selecting medical Record type
        	if(isGovernmentandMedical){
        		pg = new PageReference('/apex/Medi_SmartContactSearch?accId='+con.accountId);
        			
        	}
        	else{
            	pg = new PageReference('/apex/ContactSmartSearch?accId='+con.accountId);  
        	}
            return pg;
        }
        else{
            return null;
        }
    }
    public PageReference redirectPage(){
        System.debug('>>>>>>>'+profileName);
        if(profileName.containsIgnoreCase('Instrument')){
            System.debug('>>>>>>>1');
            return null;
        }else{
            System.debug('>>>>>>>2');
            PageReference pg ;
            if(selectedRecordType!=null){
            pg = new PageReference('/003/e?nooverride=1&retURL=%2F003%2Fo&RecordType='+selectedRecordType+'&ent=Contact');
            }
            else{
            pg = new PageReference('/003/e?nooverride=1&retURL=%2F003%2Fo&ent=Contact');
            }
            return pg;
        }
    } 
}