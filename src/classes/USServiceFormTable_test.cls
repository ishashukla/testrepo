/*
 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
*/
@isTest
public class USServiceFormTable_test {
    
    static testMethod void testUSServiceFormTable() {
        
        Test.startTest();
        
        //Case c = [select Id, (select Id from Parts__r), (select Id from AssetUnits__r) from Case limit 1];
        Account acc = TestUtils.createaccount(1,false).get(0);
        insert acc;
        Case c = TestUtils.createCase(1,acc.Id,false).get(0);
        //Modified By Chandra Shekhar Sharma to add record types on case Object
    	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
    
    	c.recordTypeId = rtByName.getRecordTypeId();
        insert c;
        USServiceFormTable cc = new USServiceFormTable();
        cc.thisCase = c;
        cc.getPartSize();
    	
        Test.stopTest();
    }

}