@isTest
private class DeletePrivateContactShares_Test {
    static Contact con;
	private static testMethod void test() {
	    createTestData();
        DeletePrivateContactShares batch = new DeletePrivateContactShares();
        database.executeBatch(batch, 200);
        //List<ContactShare> conShare = [SELECT id from ContactShare WHERE contactID = :con.id and RowCause = 'Manual'];
        //system.assert(conshare == null || conshare.size() == 0);
	}
	
	private static void createTestData(){
	    List<Account> accList = TestUtils.createAccount(1, true);
	    con = TestUtils.createCOMMContact(accList.get(0).id, 'TestContact', false);
	    con.Private__c = true;
	    insert con;
	    list<User> user = TestUtils.createUser(1,'Customer Service - COMM INTL', true);
	    ContactShare conShare = new ContactShare();
	    conShare.contactID = con.id;
	    conShare.ContactAccessLevel  ='Read';
	    conShare.UserOrGroupID = user.get(0).id;
	    insert conShare;
	}

}