/**================================================================      
 * Appirio, Inc
 * Name: UpdateVaultTabPermissionSetBatchTest
 * Description: Test class for UpdateVaultTabPermissionSetBatch(T-516615)
 * Created Date: [07/01/2016]
 * Created By: [Isha Shukla] (Appirio)
 * Date Modified      Modified By      Description of the update
  ==================================================================*/
@isTest
private class UpdateVaultTabPermissionSetBatchTest {
    static List<User> userList;
    static PermissionSetAssignment oldPermissionSetAccess;
    // Testing for tab access to users in permission set 
    @isTest static void testFirst() {
        createData();
        Test.startTest();
        UpdateVaultTabPermissionSetBatch obj = new UpdateVaultTabPermissionSetBatch();
        Database.executeBatch(obj);
        Test.stopTest();
        System.assertEquals([SELECT AssigneeId FROM PermissionSetAssignment WHERE AssigneeId =  :userList[0].Id] != Null ,True);
    }
    // creates test data
    @isTest static void createData() {
       Profile pr = TestUtils.fetchProfile('Instruments Sales User');
       userList = TestUtils.createUser(4, pr.Name, false);
       userList[0].Division = 'Surgical';
       userList[1].Division = 'IVS';
       insert userList;
       User adminUser = TestUtils.createUser(1,'System Administrator', true).get(0);
       System.runAs(adminUser) {
         myFunc2();
       }
       userList[2].Division = 'IVS';
       update userList;       
    }
   // @future
    private static void myFunc2()
    {
      Id psaId = [SELECT Id FROM PermissionSet WHERE Name = 'Access_to_The_Vault_tab_for_Instruments_user_not_IVS' LIMIT 1].Id;
      oldPermissionSetAccess = new PermissionSetAssignment(PermissionSetId = psaId , AssigneeId = userList[1].Id);
      insert oldPermissionSetAccess;
    }
}