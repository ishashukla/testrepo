// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncInventoryTransaction_SCH -  Scheduled Apex to sync new Inventory transfer between Org.
//
// 26 Oct 2016, Isha Shukla (T-550252) 
global class COMM_SyncInventoryTransaction_SCH implements Schedulable{
    global void execute(SchedulableContext  sc){
        List<SVMXC__Stock_Transfer__c> recordList = new List<SVMXC__Stock_Transfer__c>();
        // Querying all records of stock transfer and stock transfer line.
        recordList = [SELECT Acceptance_Status__c,SVMXC__Additional_Information__c,Callout_Result__c,SVMXC__Destination_Location__r.Location_ID__c,
                      Integration_Status__c,SVMXC__IsPartner__c,SVMXC__IsPartnerRecord__c,SVMXC__Not_Posted_Item_Count__c,SVMXC__Partner_Account__c,
                      SVMXC__Partner_Contact__c,Reject_Reason__c,ReqByTechEmail__c,ReqFromTechEmail__c,SVMXC__Source_Location__r.Location_ID__c ,
                      (Select SVMXC__Stock_Transfer__c, SVMXC__Additional_Information__c, SVMXC__Posted_To_Inventory__c, SVMXC__Product__c,
                       SVMXC__Quantity_Transferred2__c,Asset__r.Quantity__c,asset__r.SVMXC__Serial_Lot_Number__c, SVMXC__Select__c, SVMXC__Serial_Number_List__c, SVMXC__Transaction_Results__c, SVMXC__Unit_Price2__c,
                       SVMXC__Use_Price_From_Pricebook__c,SVMXC__Product__r.ProductCode,Asset__r.External_ID__c, Asset__c, SVMX_Serial_Number__r.name From SVMXC__Stock_Transfer_Line__r) 
                      FROM SVMXC__Stock_Transfer__c WHERE Integration_Status__c ='New' and Acceptance_Status__c = 'Accepted'];
        List<SVMXC__Stock_Transfer__c> listOfStockTransfer = new List<SVMXC__Stock_Transfer__c>();
        List<SVMXC__Stock_Transfer__c> listOfStockTransferToUpdate = new List<SVMXC__Stock_Transfer__c>();
        // Number of records to be passed at a time to Queueable class.
        COMMRecordCount__c customSetting = COMMRecordCount__c.getOrgDefaults();
        Integer count = 0;
        if(customSetting != null) {
            count = Integer.valueOf(customSetting.Count__c);   
        }
        Integer i=0;
        // Passing records to Queueable class and updating Integration Status.
        if(recordList.size() > 0 && count > 0) {
            for(SVMXC__Stock_Transfer__c stockTransfer : recordList) {
                if(i != count && i < count) {
                    stockTransfer.Integration_Status__c = 'InProgress';
                    listOfStockTransfer.add(stockTransfer);
                    listOfStockTransferToUpdate.add(stockTransfer);
                    i = i+1;
                    if(listOfStockTransfer.size() == count) {
                        COMM_SyncInventoryTransactionQueue queueObj = new COMM_SyncInventoryTransactionQueue(listOfStockTransfer);
                        ID jobID = System.enqueueJob(queueObj);
                        i = 0 ;
                        listOfStockTransfer.clear();
                    }
                    
                } 
                
            }
            if(listOfStockTransfer.size() > 0){
                COMM_SyncInventoryTransactionQueue queueObj = new COMM_SyncInventoryTransactionQueue(listOfStockTransfer);
                ID jobID = System.enqueueJob(queueObj);
                listOfStockTransfer.clear();
            }
            if(listOfStockTransferToUpdate.size() > 0) {
                update listOfStockTransferToUpdate;
            }
        }
        
    }
}