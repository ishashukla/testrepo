public class USServiceFormTable {

    public Case thisCase {get;set;}
    public Integer partSize;
    public String assetSerialNumbers;
    
    public USServiceFormTable() {
        assetSerialNumbers = '';
        partSize = 0;
    }

    public Integer getPartSize() {
        if (thisCase != null && thisCase.Parts__r != null) {
            partSize = thisCase.Parts__r.size();
        }
        return partSize;
    }

/*
    public Integer getPartSize() {
        if (thisCase==null) {
            return partSize;
        }
        
        for (Case c : [select Id, (select Id from Parts__r) from Case where Id = :thisCase.Id]) {
            return c.Parts__r.size();
        }
        return partSize;
    }
*/
/*
    public String getAssetSerialNumbers() {

        Set<String> caseSerials= new Set<String>();

        if (thisCase==null) {
            return assetSerialNumbers;
        }

        for (Case c : [select Id, Serial_Numbers_of_Products__c, (select Serial_No_1__c, Serial_No_2__c, Serial_No_3__c, Serial_No_4__c from Asset_Connects__r where (Serial_No_1__c != '' or Serial_No_2__c != '' or Serial_No_3__c != '' or Serial_No_4__c != '')) from Case where Id = :thisCase.Id] ) {
            for (Case_Asset__c cac : c.Asset_Connects__r) {
                if(!caseSerials.contains(cac.Serial_No_1__c) && cac.Serial_No_1__c != null) {
                    caseSerials.add(cac.Serial_No_1__c);
                }
                if(!caseSerials.contains(cac.Serial_No_2__c) && cac.Serial_No_2__c != null) {
                    caseSerials.add(cac.Serial_No_2__c);
                }
                if(!caseSerials.contains(cac.Serial_No_3__c) && cac.Serial_No_3__c != null) {
                    caseSerials.add(cac.Serial_No_3__c);
                }
                if(!caseSerials.contains(cac.Serial_No_4__c) && cac.Serial_No_4__c != null) {
                    caseSerials.add(cac.Serial_No_4__c);
                }
            }

            for (String s : caseSerials) {
                if (assetSerialNumbers != null && assetSerialNumbers != '') {
                    assetSerialNumbers += ', ' + s;
                }
                else {
                    assetSerialNumbers = s;
                }
            }
        }

        return assetSerialNumbers;
    }
*/

    static testMethod void testUSServiceFormTable() {
        
        Case c = [select Id, (select Id from Parts__r), (select Id from AssetUnits__r) from Case limit 1];
        USServiceFormTable cController = new USServiceFormTable();
        cController.thisCase = c;
        cController.getPartSize();
    }
}