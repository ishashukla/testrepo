/*******************************************************************************
 Author        :  Appirio JDC (Pitamber Sharma)
 Date          :  May 07, 2014
 Purpose       :  Test class for Endo_LinkCasesWithOrders
 
 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
*******************************************************************************/

@isTest
private class Endo_LinkCasesWithOrdersTest {
	
	static testMethod void myUnitTest() {
		Account acc = new Account(Name = 'Test Account');
    insert acc;
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    insert con;
    
    Case cs = new Case(Subject = 'Test Subject', ContactId = con.Id, Oracle_Order_Ref__c = '1234');
    cs.contactId = con.Id;
     //Modified By Chandra Shekhar Sharma to add record types on case Object
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
    
    cs.recordTypeId = rtByName.getRecordTypeId();
    insert cs;
    
    Order__c ordr = new Order__c(Name='1234', Account__c = acc.Id, Oracle_Order_Number__c = '1234');
    insert ordr;
    
    Test.startTest(); //Start Test
    
    ID batchprocessid = Database.executeBatch(
                           new Endo_LinkCasesWithOrders(), 200);
    AsyncApexJob aaj;
    try{
    	aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, 
              NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid ];
    
    }
    catch(Exception e){
    	// DO Nothing
    }
    
    // Verify the Job submission
    System.assert(aaj != null);
    Test.stopTest();  //End Test
    
    
    Case c = [Select Id, Current_Case_Order__c FROM Case WHERE Id =:cs.Id];
    System.assertEquals(c.Current_Case_Order__c, ordr.Id);
      
    Order__c objOrder = [Select Id, Order_Contact__c FROM Order__c WHERE Id =:ordr.Id];
    System.assertEquals(objOrder.Order_Contact__c, con.Id);
  }
}