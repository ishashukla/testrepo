// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_displayEquipmentWizardCntrlrTest - Test Class for COMM_displayEquipmentWizardController
//
// 06/04/2016, Mehul Jain (T-502677)
//

@isTest
private class COMM_displayEquipmentWizardCntrlrTest{
  @isTest  
  static void testController(){
    PageReference pr;
    Id rtId;  
    COMM_displayEquipmentWizardController cntrlr;  
    Account testAcc = Testutils.createAccount(1,true).get(0);
    Project_Plan__c testPPlan = Testutils.createProjectPlan(1,testAcc.Id, true).get(0);
    Project_Phase__c testPPhase = Testutils.createProjectPhase(1, testPPlan.Id, true).get(0);
    Room__c testRoom = Testutils.createRoomObject(1, testPPhase.Id, true).get(0);
    Equipment__c testEquip = new Equipment__c(Room__c = testRoom.Id);
    Map<Integer, String> mapOfMMTypes = new Map<Integer, String>();
    mapOfMMTypes.put(1, 'Connect Suite');
    mapOfMMTypes.put(2, 'Suite Status');
    mapOfMMTypes.put(3, 'Studio3');
    mapOfMMTypes.put(4, 'Data Mediator');
    Map<Integer, String> mapOfRTs = new Map<Integer, String>();
    mapOfRTs.put(1,'Booms');
    mapOfRTs.put(2,'Doc Station');
    mapOfRTs.put(3,'Exam Light');
    mapOfRTs.put(4,'Lights');
    mapOfRTs.put(5,'ORIS');
    mapOfRTs.put(6,'Media Management');
    mapOfRTs.put(7,'Tables');
    Test.setCurrentPage(Page.COMM_displayEquipmentWizard);   
    for(Integer i=1; i<=7; i++){  
        if(i == 6) {
           for(Integer j=1; j<=4; j++){
                testEquip.Type__c = mapOfMMTypes.get(j);
                rtId = Schema.SObjectType.Equipment__c.getRecordTypeInfosByName().get(mapOfRTs.get(i)).getRecordTypeID();  
                testEquip.RecordTypeID = rtId;
                system.debug(rtId);
                ApexPages.currentPage().getParameters().put('RecordType',rtId);  
                ApexPages.StandardController std = new ApexPages.standardController(testEquip);
                cntrlr = new COMM_displayEquipmentWizardController(std);
                pr = cntrlr.saveEquipment();
                system.debug(pr.getURL());
                system.assertEquals(pr.getURL().contains('COMMEquipment'),true);
           }
        } else {
            rtId = Schema.SObjectType.Equipment__c.getRecordTypeInfosByName().get(mapOfRTs.get(i)).getRecordTypeID();  
            testEquip.RecordTypeID = rtId;
            system.debug(rtId);
            ApexPages.currentPage().getParameters().put('RecordType',rtId);  
            ApexPages.StandardController std = new ApexPages.standardController(testEquip);
            cntrlr = new COMM_displayEquipmentWizardController(std);
            pr = cntrlr.saveEquipment();
            system.debug(pr.getURL());
            system.assertEquals(pr.getURL().contains('COMMEquipment'),true);  
        }
    }    
  }
}