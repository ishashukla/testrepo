public without sharing class FIMSTrainingExt {
    private String userId;
    private String username;
    private List<scormanywhere__Transcript__c> transcripts;
    private List<TranscriptWrapper> transcriptWrappers;
    private String pageMessage;
    public Boolean showTrainingBlock {get; set;}
    public User currentUser {get; set;}

    public FIMSTrainingExt() {
        try {
            initUser();

            currentUser = getUser(userId);
        } catch (Exception e) {
            setErrorOnPage(Label.scormanywhere.My_Courses_Page);
            return;
        }
        showTrainingBlock = true;

        try {
            username = currentUser.Name;
        } catch (Exception e) {

        }

        if (username == NULL) {
            setErrorOnPage(Label.scormanywhere.No_Courses_for_User +' ' + username);
            return;
        }

        transcriptWrappers = getTranscriptWrappers();

        if (transcriptWrappers.size() == 0) {
            setInfoMessageOnPage(Label.scormanywhere.No_Courses_for_User +' ' + currentUser.Name);
            return;
        }
    }

    private void initUser() {
        userId = ApexPages.currentPage().getParameters().get('userId');
        if (userId == NULL) {
            userId = UserInfo.getUserId();
        }
        currentUser = [SELECT Id, Name FROM User WHERE Id = :userId];
        
    }
    public User getUser(String p_userId) {
        List<User> users = [SELECT Name FROM User WHERE Id = :p_userId];

        if ( ! users.isEmpty()) {
            return users.get(0);
        } else {
            return NULL;
        }
    }

    private void setErrorOnPage(String error) {
        showTrainingBlock = false;
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
    }

    private void setInfoMessageOnPage(String message) {
        showTrainingBlock = false;
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.INFO, message));
    }

    public List<TranscriptWrapper> getTranscriptWrappers() {
        List<TranscriptWrapper> wrappers = new List<TranscriptWrapper>();
        
        transcripts = [
            SELECT scormanywhere__Course__r.Name, scormanywhere__Course__c, scormanywhere__Course__r.scormanywhere__Duration__c,
                scormanywhere__Status__c,scormanywhere__Progress__c, scormanywhere__Score__c, scormanywhere__Total_Time__c,
                scormanywhere__Total_Time_Seconds__c, scormanywhere__Session_Time__c, scormanywhere__Completion_Date__c,
                scormanywhere__Attempts__c, scormanywhere__Course__r.scormanywhere__Description__c
            FROM scormanywhere__Transcript__c
            WHERE scormanywhere__User__c = :userId AND scormanywhere__Test_Launch__c = false ORDER by scormanywhere__Course__r.Order__c ASC NULLS LAST
        ];
        
        if (transcripts != null) {
            for (scormanywhere__Transcript__c transcript: transcripts) {
                wrappers.add(new TranscriptWrapper(transcript));
            }
        }
        return wrappers;
    }

    public String getUserNamesCoursesTitle () {
        String title = Label.scormanywhere.My_Courses_Page;
        title = title.replace('{0}', currentUser.Name);
        return title;
    }

    public class TranscriptWrapper {
        public String title {get; set;}
        public String Id {get; set;}
        public String name {get; set;}
        public String type {get; set;}
        public String duration {get; set;}
        public String status {get; set;}
        public String progress {get; set;}
        public String score {get; set;}
        public String action {get; set;}
        public String attempts {get; set;}
        public String totalTime {get; set;}
        public String sessionTime {get; set;}
        public String completionDate {get; set;}
        public String description {get; set;}
        public Boolean hasDescription {get; set;}

        public TranscriptWrapper(scormanywhere__Transcript__c transcript) {
            title = getValue(transcript.scormanywhere__Course__r.Name);
            Id = getValue(transcript.scormanywhere__Course__c);
            duration = getValue(transcript.scormanywhere__Course__r.scormanywhere__Duration__c);
            status = getValue(transcript.scormanywhere__Status__c);
            progress = getValue(String.ValueOf(transcript.scormanywhere__Progress__c));
            progress = progress == '' ? progress : progress + '%';
            score = getValue(String.ValueOf(transcript.scormanywhere__Score__c));
            attempts = getValue(String.ValueOf(transcript.scormanywhere__Attempts__c));
            totalTime = getValue(String.ValueOf(transcript.scormanywhere__Total_Time__c));
            sessionTime = getValue(String.ValueOf(transcript.scormanywhere__Session_Time__c));
            completionDate = getValue(String.ValueOf(transcript.scormanywhere__Completion_Date__c));
            action = transcript.Id;
            description = getValue(String.ValueOf(transcript.scormanywhere__Course__r.scormanywhere__Description__c));
            hasDescription = description.length() > 0;
        }

        private String getValue(String value) {
            return ( ! String.isBlank(value)) ? value : '';
        }
    }
}