/*************************************************************************************************
Created By:    Jitendra Kothari
Date:          March 7, 2014
Description  : This class acts a type for Address which will contain either account or contact address
**************************************************************************************************/
public class Endo_Address {
	public String state {get; set;}
    public String city {get; set;}
    public String country {get; set;}
    public String countryCode {get; set;}
    public String zipCode {get; set;}
    public String street {get; set;}
    
    public String getAddressForMap(){
    	String myaddress;
    	
        //if (street != null) {
        //  myaddress = street;
        //}
        
        if (state != null) {
          if (myaddress == null) {
            myaddress = state;
          }
          else {
            myaddress = myaddress + ',' + state;
          }
        }
        
        if (country != null) {
          if (myaddress == null) {
            myaddress = country;
          }
          else {
            myaddress = myaddress + ',' + country;
          }
        }
        
        if (zipCode != null) {
          if (myaddress == null) {
            myaddress = zipCode;
          }
          else {
            myaddress = myaddress + ',' + zipCode;
          }
        }
        
        if(myaddress != null){
        	return myaddress.replace(' ', '+');
    	}
    	else{
    		return null;
    	}
    }
    
    
    public String getCityForMap(){
    	if(city != null){
        return city;
    	}
    	else{
    	  return null;
    	}
    }
    
    
    public String getAddressForGeo(){
    	String myaddress;
    	if(city != null) {
        myaddress = city;
      }
      if(state != null) {
        if(myaddress == null) {
          myaddress = state;
        }
        else {
          myaddress = myaddress + ', ' + state;
        }
      }
      if (country != null) {
        if (myaddress == null) {
          myaddress = country;
        }
        else {
          myaddress = myaddress + ', ' + country;
        }
      }
      if (zipCode != null) {
        if (myaddress == null) {
          myaddress = zipCode;
        }
        else {
          myaddress = myaddress + ', ' + zipCode;
        }
      }
      return myaddress;
    }
}