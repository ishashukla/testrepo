// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncPartsOrderQueue -  Queueable Apex to sync new PartsOrder to EBS.
//
//
// 31 Oct 2016, Mohan Panneerselvam (T-550285) 
public class COMM_SyncPartsOrderQueue implements Queueable,Database.AllowsCallouts{
    SVMXC__RMA_Shipment_Order__c partsOrderObj = new SVMXC__RMA_Shipment_Order__c();
    Id orderHeaderId;
    List<id> orderLineId= new List<id>();
    String operation;
    List<id> rmaOrderLineId= new List<id>();
    public COMM_SyncPartsOrderQueue(SVMXC__RMA_Shipment_Order__c inPartsOrderObj,Id inOrderHeaderId, List<id> inOrderLineId, String inOperation, List<id> inRMAOrderLineId) {
        this.partsOrderObj = inPartsOrderObj;
        this.orderHeaderId = inOrderHeaderId;
        this.orderLineId = inOrderLineId;
        this.operation = inOperation;
        this.rmaOrderLineId = inRMAOrderLineId;
    }
    public void execute(QueueableContext context) {
        
        map<id,String> accountmapping = new map<id,string>();
        map<id,String> accountmappingInvoice = new map<id,string>();
        String stdOrderId;        
        Set<id> productid= new set<id>();
        SVMXC__RMA_Shipment_Order__c tempPOObj= [select id,name,SVMXC__Company__c, SVMXC__Order_Type__c,SVMXC__Case__r.Type, SVMXC__Company__r.AccountNumber, SVMXC__Case__r.Accountid,Recordtype.name,Reason_Code__c,
                                                   SVMXC__Destination_Location__r.Location_ID__c,SVMXC__Source_Location__r.Location_ID__c,Technician_Equipment__r.Name,
                                                  SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMXC__Start_Date__c, SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMXC__End_Date__c, 
                                                  SVMXC__Service_Order__r.SVMXC__Warranty__r.SVMXC__End_Date__c,SVMXC__Service_Order__r.EU_PO__c,EU_PO__c,SVMXC__Fulfillment_Type__c,
                                                  (Select id,name,SVMXC__Product__c, SVMXC__Product__r.ProductCode ,SVMXC__Line_Type__c,SVMXC__Serial_Number__r.SVMXC__Product__r.ProductCode,
                                                    CreatedDate,SVMXC__Expected_Quantity2__c,SVMXC__Line_Price2__c,Order_Product__c 
                                                   from SVMXC__RMA_Shipment_Line__r)
                                                    from SVMXC__RMA_Shipment_Order__c
                                                    where id =:partsOrderObj.Id
                                                    limit 1
                                                    ];
        
        //accountmapping.put(tempPOObj.SVMXC__Case__r.Accountid,null);
        //accountmappingInvoice.put(tempPOObj.SVMXC__Case__r.Accountid,null);
        accountmapping.put(tempPOObj.SVMXC__Company__c,null);
        accountmappingInvoice.put(tempPOObj.SVMXC__Company__c,null);
        for(Address__c varloop: [select id,account__c,type__C,Location_ID__c from address__c where account__c in:accountMapping.keySet()
                         and Business_Unit_Name__c ='COMM'
                         and Primary_Address_Flag__c =true
                         and ( Type__c = 'Shipping' or Type__c = 'Billing')]){
                             if(Varloop.type__c =='Shipping')
                                 accountmapping.put(varloop.account__c,varloop.Location_ID__c);
                             if(Varloop.type__c =='Billing')      
                                 accountmappingInvoice.put(varloop.Account__c,varloop.Location_ID__c);
                         }
            

                COMM_CreateEBSSalesOrder_REST.informaticaRequest request = new COMM_CreateEBSSalesOrder_REST.informaticaRequest();
                COMM_CreateEBSSalesOrder_REST.salesOrderRequest salesOrderRequest = new COMM_CreateEBSSalesOrder_REST.salesOrderRequest();
                COMM_CreateEBSSalesOrder_REST.lineItems lineItems = new  COMM_CreateEBSSalesOrder_REST.lineItems();
                salesOrderRequest.IntegrationHeader = COMM_CreateEBSSalesOrder_REST.createIntegrationHeader(Label.ORDER_SFDC,Label.order_commerp);
                salesOrderRequest.orderHeader = createOrderHeader(tempPOObj,accountmapping,accountmappingInvoice,orderHeaderId,operation);
                if(operation == 'Create'){
                    lineItems.lineItem = createOrderItems(tempPOObj.svmxc__RMA_Shipment_Line__r,tempPOObj,orderLineId);
                }
                If(operation == 'Update'){
                    lineItems.lineItem = updateOrderItems(rmaOrderLineId,tempPOObj,operation);
                }                
                salesOrderRequest.lineItems = lineItems;
                request.salesOrderRequest = salesOrderRequest;
                // Call Rest utility to create order on oracle SOA
                String response = COMM_CreateEBSSalesOrder_REST.createOrderRestCallout(request); 
                System.debug('response>>'+response);
                COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA=true;
                if(String.isEmpty(response)){
                    if(operation =='Create'){
                        tempPOObj.SVMX_Order__c = orderHeaderId;
                        update tempPOObj;
                        List<Orderitem> tempOrdItemList = [Select id, Description from Orderitem where id in :orderLineId];
                        for(Orderitem tempordItem:tempOrdItemList){
                            for(svmxc__RMA_Shipment_Line__c tmpRMALine:tempPOObj.svmxc__RMA_Shipment_Line__r){
                                if(tempordItem.Description == tmpRMALine.Id){
                                    tmpRMALine.Order_Product__c = tempordItem.id;
                                    break;
                                }
                            }
                        }
                        update tempPOObj.svmxc__RMA_Shipment_Line__r;
                    }
                    //system.debug('tempPOObj==>'+tempPOObj);
                }
                else{
                    tempPOObj.Callout_Result__c = response;
                    update tempPOObj;  
                    if(operation =='Create') {
                        database.delete(orderLineId);  
                        database.delete(orderHeaderId);
                    }
                    //system.debug('tempPOObj==>'+tempPOObj);
                }
        
    }

    // Method to update order Line items
    public static List<COMM_CreateEBSSalesOrder_REST.lineItem> updateOrderItems(List<id> inRMAOrderLine,SVMXC__RMA_Shipment_Order__c orderHeader, String inOperation){
        List<COMM_CreateEBSSalesOrder_REST.lineItem> Liorderitem = new List<COMM_CreateEBSSalesOrder_REST.lineItem>();
        COMM_CreateEBSSalesOrder_REST.lineItem orderitem ;
        integer i=1;        
        List<SVMXC__RMA_Shipment_Line__c> orderLine =[Select id,name,SVMXC__Product__c,SVMXC__Product__r.ProductCode, SVMXC__Line_Type__c,SVMXC__Serial_Number__r.SVMXC__Product__r.ProductCode,
                                                    CreatedDate,SVMXC__Expected_Quantity2__c,SVMXC__Line_Price2__c,Order_Product__c,Order_Product__r.Oracle_Line_Number__c,
                                                    Order_Product__r.Requested_Ship_Date__c, Order_Product__r.Oracle_Order_Line_Number__c,
                                                    Order_Product__r.Order.Oracle_Order_Number__c
                                                      from SVMXC__RMA_Shipment_Line__c 
                                                    where id in :inRMAOrderLine];
        /*
        List<OrderItem> orderItemLst = [select id,description from OrderItem where id in:orderLineId];
        Map<String,String> orderItemKeyMap = new Map<String,String>();
        for(OrderItem temp: orderItemLst){
            orderItemKeyMap.put(temp.description,temp.id);
        }*/
        for(SVMXC__RMA_Shipment_Line__c order : orderLine){
            orderitem = new COMM_CreateEBSSalesOrder_REST.lineItem();
            //orderitem.linetype = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Line_Type__c);
            //orderitem.LineNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.name);
            orderitem.LineNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.Order_Product__r.Oracle_Order_Line_Number__c);
            if(order.SVMXC__Serial_Number__r.SVMXC__Product__r.ProductCode != null){
            	orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Serial_Number__r.SVMXC__Product__r.ProductCode);    
            }
            if(order.SVMXC__Product__r.ProductCode != null){
                orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Product__r.ProductCode);
            }
            
            if(orderHeader.SVMXC__Fulfillment_Type__c !=null && (orderHeader.SVMXC__Fulfillment_Type__c =='Auto Replenishment' || orderHeader.SVMXC__Fulfillment_Type__c =='Manual Replenishment')){
                orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Product__r.ProductCode);
            }
            //if(order.Order_Product__r.Requested_Ship_Date__c!=null)
            //orderitem.requestDate = new COMM_CreateEBSSalesOrder_REST.TagName(order.Order_Product__r.Requested_Ship_Date__c.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'));
            orderitem.lineId = new COMM_CreateEBSSalesOrder_REST.TagName (order.Order_Product__r.Oracle_Line_Number__c);
            //orderitem.lineId = new COMM_CreateEBSSalesOrder_REST.TagName ('1');
            orderitem.oracleOrderNumber = new COMM_CreateEBSSalesOrder_REST.TagName (order.Order_Product__r.Order.Oracle_Order_Number__c);
            orderitem.orderedQuantity = new COMM_CreateEBSSalesOrder_REST.TagName(String.valueOf(order.SVMXC__Expected_Quantity2__c));
            orderitem.unitSellingPrice = new COMM_CreateEBSSalesOrder_REST.TagName(String.valueOf(order.SVMXC__Line_Price2__c));
            //orderitem.unitSellingPrice = new COMM_CreateEBSSalesOrder_REST.TagName('0');
            orderitem.orgId = new COMM_CreateEBSSalesOrder_REST.TagName(label.COMM_Order_BU);
                
            orderitem.origSysLineRef = new COMM_CreateEBSSalesOrder_REST.TagName(order.Order_Product__c);
            orderitem.lineCategoryCode = new COMM_CreateEBSSalesOrder_REST.TagName('RMA');
            orderItem.calculatePriceFlag = new COMM_CreateEBSSalesOrder_REST.TagName(orderHeader.recordtype.name =='RMA'?'TRADEIN': '');
            orderitem.operation = new COMM_CreateEBSSalesOrder_REST.TagName(inOperation);
            //orderitem.orderQuantityUom = new COMM_CreateEBSSalesOrder_REST.TagName('EA');
            Liorderitem.add(orderItem);
        }
        return Liorderitem;
    }
    
    
    
    // Method to create order Line items
    public static List<COMM_CreateEBSSalesOrder_REST.lineItem> createOrderItems(List<SVMXC__RMA_Shipment_Line__c> orderLine,SVMXC__RMA_Shipment_Order__c orderHeader, List<id> orderLineId){
        List<COMM_CreateEBSSalesOrder_REST.lineItem> Liorderitem = new List<COMM_CreateEBSSalesOrder_REST.lineItem>();
        COMM_CreateEBSSalesOrder_REST.lineItem orderitem ;
        integer i=1;
        Map<id,OrderItem> orderItemMap = new Map<id,OrderItem>([select id,description,Oracle_Order_Line_Number__c from OrderItem where id in:orderLineId]);
        Map<String,String> orderItemKeyMap = new Map<String,String>();
        for(Id tempId: orderItemMap.keySet()){
            orderItemKeyMap.put(orderItemMap.get(tempId).description,orderItemMap.get(tempId).id);
        }
        for(SVMXC__RMA_Shipment_Line__c order : orderLine){
            orderitem = new COMM_CreateEBSSalesOrder_REST.lineItem();
            //orderitem.linetype = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Line_Type__c);
            //orderitem.LineNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.name);
            orderitem.LineNumber = new COMM_CreateEBSSalesOrder_REST.TagName(''+orderItemMap.get(orderItemKeyMap.get(order.id)).Oracle_Order_Line_Number__c);
            //orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Serial_Number__r.SVMXC__Product__r.ProductCode);
            if(order.SVMXC__Serial_Number__r.SVMXC__Product__r.ProductCode != null){
            	orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Serial_Number__r.SVMXC__Product__r.ProductCode);    
            }
            if(order.SVMXC__Product__r.ProductCode != null){
                orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Product__r.ProductCode);
            }
            if(orderHeader.SVMXC__Fulfillment_Type__c !=null && (orderHeader.SVMXC__Fulfillment_Type__c =='Auto Replenishment' || orderHeader.SVMXC__Fulfillment_Type__c =='Manual Replenishment')){
                orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Product__r.ProductCode);
            }
            if(order.CreatedDate!=null)
            orderitem.requestDate = new COMM_CreateEBSSalesOrder_REST.TagName(order.CreatedDate.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'));
            
            orderitem.orderedQuantity = new COMM_CreateEBSSalesOrder_REST.TagName(String.valueOf(order.SVMXC__Expected_Quantity2__c));
            orderitem.unitSellingPrice = new COMM_CreateEBSSalesOrder_REST.TagName(String.valueOf(order.SVMXC__Line_Price2__c));
            //orderitem.unitSellingPrice = new COMM_CreateEBSSalesOrder_REST.TagName('0');
            orderitem.orgId = new COMM_CreateEBSSalesOrder_REST.TagName(label.COMM_Order_BU);
                
            orderitem.origSysLineRef = new COMM_CreateEBSSalesOrder_REST.TagName(orderItemKeyMap.get(order.id));
            orderitem.lineCategoryCode = new COMM_CreateEBSSalesOrder_REST.TagName('RMA');
            orderItem.calculatePriceFlag = new COMM_CreateEBSSalesOrder_REST.TagName(orderHeader.recordtype.name =='RMA'?'TRADEIN': '');
            if(orderHeader.Reason_Code__c =='Order Entry Error'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('ORDER ENTRY ERR');        
            }
            else if(orderHeader.Reason_Code__c =='Wrong Part Ordered'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('CS WRNG PN');        
            }
            else if(orderHeader.Reason_Code__c =='Shipping error'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('SHIPPING ERROR');        
            }
            else if(orderHeader.Reason_Code__c =='Customer Changed their mind'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('S23');        
            }
            else if(orderHeader.Reason_Code__c =='Damaged/defective product'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('DEFECTIVE PRODUCT');        
            }
            else if(orderHeader.Reason_Code__c =='Return as part of upgrade incentive program'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('CS PRODUCT');        
            }
            else if(orderHeader.Reason_Code__c =='Out of Box Failure'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('COMM');        
            }
            else if(orderHeader.Reason_Code__c =='Unused Product'){
                orderItem.returnReasonCode = new COMM_CreateEBSSalesOrder_REST.TagName('UNUSED PRODUCT');        
            }           
            orderitem.operation = new COMM_CreateEBSSalesOrder_REST.TagName(Label.Order_Create);
            orderitem.orderQuantityUom = new COMM_CreateEBSSalesOrder_REST.TagName('EA');
            Liorderitem.add(orderItem);
            i++;    
        }
        return Liorderitem;
    }
    
    // Method to create Order Request
    public static COMM_CreateEBSSalesOrder_REST.orderHeader createOrderHeader(SVMXC__RMA_Shipment_Order__c order,Map<id,string> accountmapping,Map<id,string> accountmappingInvoice, Id orderHeaderId,String operation ){
        COMM_CreateEBSSalesOrder_REST.orderHeader orderHeader = new COMM_CreateEBSSalesOrder_REST.orderHeader();
        orderHeader.operation = new COMM_CreateEBSSalesOrder_REST.TagName(operation);
        orderHeader.businessUnit = new COMM_CreateEBSSalesOrder_REST.TagName(label.COMM_Order_BU);
        if(order.SVMXC__Service_Order__r.SVMXC__Warranty__r.SVMXC__End_Date__c != null &&
          order.SVMXC__Service_Order__r.SVMXC__Warranty__r.SVMXC__End_Date__c >= Datetime.now()){
            orderHeader.custPoNumber = new COMM_CreateEBSSalesOrder_REST.TagName('WARRANTY'); 
        }
        if( order.SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMXC__Start_Date__c !=null && 
            order.SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMXC__End_Date__c !=null){
            if(order.SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMXC__Start_Date__c <= Datetime.now() && 
              order.SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMXC__End_Date__c >= Datetime.now()){
                orderHeader.custPoNumber = new COMM_CreateEBSSalesOrder_REST.TagName('SERVICE CONTRACT'); 
            }
        }        
        orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Order_Type__c );
        /*
        If(order.SVMXC__Case__r.Type != null){
            if(order.SVMXC__Case__r.Type  == '120 Day Repair Warranty(Service)'){
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Standard Warranty');
            }
            else if(order.SVMXC__Case__r.Type  == 'Billable repair'){
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Service Purchase Order');
                orderHeader.custPoNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Service_Order__r.EU_PO__c);
            }
            else if(order.SVMXC__Case__r.Type  == 'Factory Warranty Period'){

            }
            else if(order.SVMXC__Case__r.Type  == 'POS Ext Warranty Concessions'){
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Standard Warranty');
            }
            else if(order.SVMXC__Case__r.Type  == 'Service Contract'){
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Standard Service Contract');
            }             
        }*/        
        if(order.SVMXC__Fulfillment_Type__c !=null && (order.SVMXC__Fulfillment_Type__c =='Auto Replenishment'|| order.SVMXC__Fulfillment_Type__c =='Manual Replenishment')){
            orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Consignment Order');
            orderHeader.custPoNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.EU_PO__c);
        }
        
        orderHeader.orderSource = new COMM_CreateEBSSalesOrder_REST.TagName(Label.ORDER_SFDC);
       
        orderHeader.soldFromOrgId = new COMM_CreateEBSSalesOrder_REST.TagName('0');
        orderHeader.soldToOrgId =new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Company__r.AccountNumber);
        
        if(order.SVMXC__Destination_Location__r.Location_ID__c !=null){
            orderHeader.shipToOrgId =  new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Destination_Location__r.Location_ID__c);            
        }
        if(accountmappingInvoice.get(order.SVMXC__Company__c)!=null){
            orderHeader.invoiceToOrgId =  new COMM_CreateEBSSalesOrder_REST.TagName(accountmappingInvoice.get(order.SVMXC__Company__c)); 
        }

        orderHeader.salesrepId = new COMM_CreateEBSSalesOrder_REST.TagName('Communications Sales');
        orderHeader.paymentTerm = new COMM_CreateEBSSalesOrder_REST.TagName('Net 20'); //Hardcoded for Testing -Mohan        
        orderHeader.origSysDocumentRef = new COMM_CreateEBSSalesOrder_REST.TagName(orderHeaderId);
        orderHeader.priceList = new COMM_CreateEBSSalesOrder_REST.TagName(Label.Order_PriceList);        
        orderHeader.projectManager = new COMM_CreateEBSSalesOrder_REST.TagName(order.Technician_Equipment__r.Name);
        orderHeader.projectNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.name);          
        return orderHeader;
    }    
    
}