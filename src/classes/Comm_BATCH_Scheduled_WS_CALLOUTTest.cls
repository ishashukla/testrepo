/**================================================================      
* Appirio, Inc
* Name: Comm_BATCH_Scheduled_WS_CALLOUTTest
* Description: Test class for Comm_BATCH_Scheduled_WS_CALLOUT
* Created Date: 21 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class Comm_BATCH_Scheduled_WS_CALLOUTTest {
    static List<SVMXC__Service_Order_Line__c> wolList;
    static List<Id> wolIds;
    static List<Id> woIds;
    static Account acc;
    static Product2 productRecord;
    static SVMXC__Service_Contract__c service;
    static Case caseRecord;
    static SVMXC__Service_Order__c workOrder;
    static String WOLRecordTypeId;
    @isTest static void testBatch() {
        createData();
        woIds = new List<Id>();
        workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = caseRecord.Id,
                                               Coverage_Type__c = 'Billable repair',SVMXC__Service_Contract__c = service.Id);
        insert workOrder;
        woIds.add(workOrder.Id);
        wolList = new List<SVMXC__Service_Order_Line__c>();
        wolIds = new List<Id>();
        for(integer i=0;i<10;i++) {
            SVMXC__Service_Order_Line__c workOrderLine = new SVMXC__Service_Order_Line__c(
                SVMXC__Service_Order__c = workOrder.Id,SVMXC__Line_Status__c = 'Completed',
                RecordTypeId = WOLRecordTypeId,SVMXC__Product__c = productRecord.Id,SVMXC__Billable_Quantity__c = 10,
                SVMXC__Billable_Line_Price__c = 10,Installation_Date__c = system.today()
            );
            wolList.add(workOrderLine);
        }
        insert wolList;
        for(integer i=0;i<wolList.size();i++) {
            wolIds.add(wolList[i].Id);
        }
        Address__c address = new Address__c(Business_Unit__c='88',account__c=acc.Id,
                                           Primary_Address_Flag__c = true,
                                           Type__c = 'Shipping');
        insert address;
        Test.startTest();
        Comm_BATCH_Scheduled_WS_CALLOUT batch = new Comm_BATCH_Scheduled_WS_CALLOUT(wolIds,woIds);
        Database.executeBatch(batch);
        Test.stopTest();
        System.assertEquals([SELECT Callout_Result__c FROM SVMXC__Service_Order_Line__c WHERE Id = :wolList[0].Id].Callout_Result__c, 'Batch id 59 is processed sucessfully.');
    }
    @isTest static void testBatchSecond() {
        createData();
        woIds = new List<Id>();
        workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = caseRecord.Id,
                                               Coverage_Type__c = '120 Day Repair Warranty(Service)',
                                               SVMXC__Service_Contract__c = service.Id);
        insert workOrder;
        woIds.add(workOrder.Id);
        wolList = new List<SVMXC__Service_Order_Line__c>();
        wolIds = new List<Id>();
        for(integer i=0;i<10;i++) {
            SVMXC__Service_Order_Line__c workOrderLine = new SVMXC__Service_Order_Line__c(
                SVMXC__Service_Order__c = workOrder.Id,SVMXC__Line_Status__c = 'Completed',
                RecordTypeId = WOLRecordTypeId,SVMXC__Product__c = productRecord.Id,SVMXC__Billable_Quantity__c = 10,
                SVMXC__Billable_Line_Price__c = 10
            );
            wolList.add(workOrderLine);
        }
        insert wolList;
        for(integer i=0;i<wolList.size();i++) {
            wolIds.add(wolList[i].Id);
        }
        Address__c address = new Address__c(Business_Unit__c='88',account__c=acc.Id,
                                           Primary_Address_Flag__c = true,
                                           Type__c = 'Billing');
        insert address;
        Test.startTest();
        Comm_BATCH_Scheduled_WS_CALLOUT batch = new Comm_BATCH_Scheduled_WS_CALLOUT(wolIds,woIds);
        Database.executeBatch(batch);
        Test.stopTest();
        System.assertEquals([SELECT Callout_Result__c FROM SVMXC__Service_Order_Line__c WHERE Id = :wolList[0].Id].Callout_Result__c, 'Batch id 59 is processed sucessfully.');
    }
    @isTest static void testBatchThird() {
        createData();
        woIds = new List<Id>();
        workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = caseRecord.Id,
                                               Coverage_Type__c = 'Factory Warranty Period',
                                               SVMXC__Service_Contract__c = service.Id);
        insert workOrder;
        woIds.add(workOrder.Id);
        wolList = new List<SVMXC__Service_Order_Line__c>();
        wolIds = new List<Id>();
        for(integer i=0;i<10;i++) {
            SVMXC__Service_Order_Line__c workOrderLine = new SVMXC__Service_Order_Line__c(
                SVMXC__Service_Order__c = workOrder.Id,SVMXC__Line_Status__c = 'Completed',
                RecordTypeId = WOLRecordTypeId,SVMXC__Product__c = productRecord.Id,SVMXC__Billable_Quantity__c = 10,
                SVMXC__Billable_Line_Price__c = 10
            );
            wolList.add(workOrderLine);
        }
        insert wolList;
        for(integer i=0;i<wolList.size();i++) {
            wolIds.add(wolList[i].Id);
        }
        Address__c address = new Address__c(Business_Unit__c='88',account__c=acc.Id,
                                           Primary_Address_Flag__c = true,
                                           Type__c = 'Billing',Location_ID__c='1234');
        insert address;
        Test.startTest();
        Comm_BATCH_Scheduled_WS_CALLOUT batch = new Comm_BATCH_Scheduled_WS_CALLOUT(wolIds,woIds);
        Database.executeBatch(batch);
        Test.stopTest();
        System.assertEquals([SELECT Callout_Result__c FROM SVMXC__Service_Order_Line__c WHERE Id = :wolList[0].Id].Callout_Result__c, 'Batch id 59 is processed sucessfully.');
    }
    private static void createData() {
        productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        Id standardPBId = Test.getStandardPricebookId();
        PriceBook2 pricebook = new PriceBook2(Name='COMM Price Book');
        insert pricebook;
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM US Stryker Field Service Case').getRecordTypeId();
        Map<String, Schema.RecordTypeInfo> WOLRecordTypeInfo =
            Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName();
        WOLRecordTypeId = WOLRecordTypeInfo.get('Usage/Consumption').getRecordTypeId();
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        acc = TestUtils.createAccount(1, true).get(0);
        caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeId = caseRecordTypeId;
        insert caseRecord;
        service = new SVMXC__Service_Contract__c(SVMXC__Start_Date__c = system.today() - 1,
                                                                           SVMXC__End_Date__c = system.today() + 1);
        insert service;
    }
}