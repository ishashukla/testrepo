/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 07, 2013
 Purpose       :  REST Webservice for getting notification for Intel records.
 Reference     :  Task T-205163
 Modifications :  Issue I-87899     Jan 03, 2014    Sonal Shrivastava
 Modifications :  Issue I-87368     Jan 06, 2014    Sonal Shrivastava
*******************************************************************************/
@RestResource(urlMapping='/IntelNotificationService/*') 
global with sharing class IntelNotificationService {
  
  //****************************************************************************
  // Get Method for REST service
  //****************************************************************************  
  @HttpGet 
  global static IntelNotificationWrapper doGet() {
  try {
      RestRequest req = RestContext.request; 
      IntelNotificationWrapper wrapper = new IntelNotificationWrapper(req);                  
      return wrapper;
    } 
    catch(Exception ex) {
      return new IntelNotificationWrapper();
    }
    return null;
  }
  
  // ***************************************************************************
  // Wrapper class for List of Intel records
  // ***************************************************************************  
  global class IntelNotificationWrapper {
    public List<IntelJson> intelList; 
    
    //-----------------------------------
    // Default Constructor
    //-----------------------------------
    public IntelNotificationWrapper(){}
    
    //-----------------------------------
    // Parameterized Constructor
    //-----------------------------------
    public IntelNotificationWrapper(RestRequest req){
      String timeStamp = null;
      if(req.params.containsKey('timeStamp')){
        timeStamp = req.params.get('timeStamp');
      }
      this.intelList = fetchRecords(timeStamp);
    }
    
    //-----------------------------------
    // Method to fetch all records
    //-----------------------------------
    public List<IntelJson> fetchRecords(String timeStamp){
      List<IntelJson> lstIntelJson = new List<IntelJson>();
      Map<String, Intel__c> mapIntelId_Intel = new Map<String, Intel__c>();
      Set<String> setIntelIdsCreatedByUser = new Set<String>();
      List<Intel__c> lstIntel = new List<Intel__c>(); 
      
      if(timeStamp != null && timeStamp != ''){
        
        //Find all Intels on which comments have been posted after time stamp
        String qry = ' SELECT Id, Details__c, CreatedById, LastModifiedDate, OwnerId, Mobile_Update__c FROM Intel__c ';
              qry += ' WHERE Chatter_Post__c <> null AND Intel_Inactive__c = false ';
              qry += ' AND Mobile_Update__c > ' + timeStamp + ' ';        
        For(Intel__c intel : Database.query(qry)){
          mapIntelId_Intel.put(intel.Id, intel);
        }        
        if(mapIntelId_Intel.size() > 0){          
          //Get Intels from comments
          for(Intel__c intel : WithoutSharingUtility.fetchCommentIntels(mapIntelId_Intel, timeStamp)){
            lstIntel.add(intel);
          }         
          //Add records to JSON list
          addRecordsToJSON(lstIntel, lstIntelJson);
        }        
      }     
      return lstIntelJson; 
    }
    
    //-----------------------------------
    // Method to add records to JSON List
    //-----------------------------------
    public void addRecordsToJSON(List<Intel__c> lstIntel, List<IntelJson> lstIntelJson){
    	List<String> lstIntelOwnerIds = IntelListWrapper.getUserFromIntel(lstIntel);    	
    	
    	// Get additional User data Photo URLs
      Map<String, User> usrId_User = IntelListWrapper.fetchUserInfo(lstIntelOwnerIds);
      for(Intel__c intel : lstIntel){
      	if(usrId_User.containsKey(intel.OwnerId)){
      		User postedByUser = usrId_User.get(intel.OwnerId);
      		lstIntelJson.add(new IntelJson(intel, postedByUser));
      	}        
      } 
    }
  }
  
  // ***************************************************************************
  // Wrapper class for Intel JSON record
  // ***************************************************************************
  public class IntelJson {
    public String id;
    public String text;
    public String createdById;
    public String photoId;
    public String authorSmallPhotoUrl;
    public String authorFullPhotoUrl;
    public DateTime lastModifiedDate;
    
    //-----------------------------------
    // Parameterized Constructor
    //-----------------------------------
    public IntelJson(Intel__c intel, User postedByUser){
      this.id          = intel.Id;
      this.text        = intel.Details__c;
      this.createdById = intel.CreatedById;
      this.photoId             = IntelListWrapper.getContentId(postedByUser.SmallPhotoUrl);              
	    this.authorSmallPhotoUrl = IntelListWrapper.replaceContentURL(postedByUser.SmallPhotoUrl);
	    this.authorFullPhotoUrl  = IntelListWrapper.replaceContentURL(postedByUser.FullPhotoUrl); 
      this.lastModifiedDate    = intel.Mobile_Update__c;      
    }
  }
}