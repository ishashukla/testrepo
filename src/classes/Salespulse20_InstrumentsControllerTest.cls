@isTest
public without sharing class Salespulse20_InstrumentsControllerTest {
	static Mancode__c man;
	static User u;
	static list<Mancode__c> listman;
	static SalespulseURLs__c spu;
	static string userAgent;
	public static testMethod void testingiPadSalespulse20() {
		createTestData();
		test.startTest();
		PageReference pageRef = Page.SalespulseConsTab;
	    Test.setCurrentPage(pageRef);
	    userAgent = 'iPad';
	    ApexPages.currentPage().getHeaders().put('User-Agent',userAgent);
	    Salespulse20_InstrumentsController sic = new Salespulse20_InstrumentsController();
	    test.stopTest();
		
	}
	
	public static testMethod void testingiPhoneSalespulse20() {
		createTestData();
		test.startTest();
		PageReference pageRef = Page.SalespulseConsTab;
	    Test.setCurrentPage(pageRef);
	    userAgent = 'iPhone';
	    ApexPages.currentPage().getHeaders().put('User-Agent',userAgent);
	    Salespulse20_InstrumentsController sic = new Salespulse20_InstrumentsController();
	    test.stopTest();
		
	}
	
	public static testMethod void testingSalespulse20() {
		createTestData();
		test.startTest();
		PageReference pageRef = Page.SalespulseConsTab;
	    Test.setCurrentPage(pageRef);
	    userAgent = 'test';
	    ApexPages.currentPage().getHeaders().put('User-Agent',userAgent);
	    Salespulse20_InstrumentsController sic = new Salespulse20_InstrumentsController();
	    test.stopTest();
		
	}
    
    public static void createTestData() {
    	Profile pr = TestUtils.fetchProfile('System Administrator');
        u = new User();
		u.ProfileId = pr.Id;
		u.lastname = 'Integration User';
		u.alias = 'usr';
		u.email = 'user' + '@test.com';
		u.emailencodingkey = 'UTF-8';
		u.languagelocalekey = 'en_US';
		u.localesidkey = 'en_US';
		u.timezonesidkey = 'America/Los_Angeles';
		u.username = 'kajuuu@test.com'; 
		insert u;
    	man = new Mancode__c();
    	man.Name = 'test';
    	man.Manager__c = u.Id;
    	man.Sales_Rep__c = UserInfo.getUserId();
    	man.Business_Unit__c = 'IVS';
    	insert man;
    	listman = new list<Mancode__c>();
    	listman.add(man);
    	spu = new SalespulseURLs__c();
    	spu.IPAD_Performance__c = '/apex/test?id=01231000001NYBh';
    	spu.IPAD_Service__c = '/apex/test?id=01231000001NYKh';
    	spu.IPAD_Transactions__c = '/apex/test?id=01231000001NYNh';
    	spu.IPAD_Realtime__c = '/apex/test?id=01231000001NYFh';
    	spu.IPAD_Manage__c = '/apex/test?id=01231000001NYWh';
    	spu.IPAD_Order_History__c = '/apex/test?id=01231000001NYSh';
    	spu.IPAD_Insight__c = '/apex/test?id=01231000001NYEh';
    	spu.IPAD_Performance__c = '/apex/test?id=01231000001NYCh';
    	spu.IPHONE_Realtime__c = '/apex/test?id=01231000001NYJh';
    	spu.Desktop_Realtime__c = '/apex/test?id=01231000001NYAh';
    	spu.Desktop_Performance__c = '/apex/test?id=01231000001NYQh';
    	spu.Desktop_Transactions__c = '/apex/test?id=01231000001NYWh';
    	spu.Desktop_Manage__c = '/apex/test?id=01231000001NYSh';
    	spu.Desktop_Order_History__c = '/apex/test?id=01231000001NYLh';
    	spu.Desktop_Insight__c = '/apex/test?id=01231000001NYKh';
    	spu.Desktop_Performance__c = '/apex/test?id=01231000001NYMh';
    	insert spu;
    }
}