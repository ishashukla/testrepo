/*
//
// (c) 2015 Appirio, Inc.
//
// ProductRelatedListControllerTest
//
// 02 Oct 2015 , Megha (Appirio)
// This is test class for ProductRelatedListController
*/
@isTest
public class ProductRelatedListControllerTest {
  static testMethod void testProductRelatedListController(){
    Product2 prod = Medical_TestUtils.createProduct(true);
    
    Product_Relationship__c proRe = new Product_Relationship__c();
    proRe.Alternate_Part_Effective_End_Date__c = System.today();
    proRe.Alternate_Part_Effective_Start_Date__c = System.today();
    proRe.Product__c = prod.Id;
    List<Sobject> testList = new List<Sobject>();
    ProductRelatedListController proRelated = new ProductRelatedListController();
    
    proRelated.objectName = 'Product_Relationship__c';
    proRelated.fieldSetName = 'Product_Hierarchy_Information';
    proRelated.parentField = 'Alternate_Product__c';
    proRelated.parentId = prod.Id;
    proRelated.startDateField = 'Alternate_Part_Effective_Start_Date__c';
    proRelated.endDateField = 'Alternate_Part_Effective_End_Date__c';
    Boolean abc = proRelated.isRecordFound;
    proRelated.getRecords();
    system.debug('proRelated.fieldSetName'+proRelated.fieldSetName);
    system.debug('proRelated.stdSetContrl'+proRelated.stdSetContrl);
    
    abc = proRelated.isRecordFound;
    testList = proRelated.getRecords();
  }
  
  static testMethod void testProductWithOtherParentFields(){
    Product2 prod = Medical_TestUtils.createProduct(true);
    
    Product_Relationship__c proRe = new Product_Relationship__c();
    proRe.Alternate_Part_Effective_End_Date__c = System.today();
    proRe.Alternate_Part_Effective_Start_Date__c = System.today();
    proRe.Product__c = prod.Id;
    List<Sobject> testList = new List<Sobject>();
    ProductRelatedListController proRelated = new ProductRelatedListController();
    
    proRelated.objectName = 'Product_Relationship__c';
    proRelated.fieldSetName = 'Product_Hierarchy_Information';
    proRelated.parentField = 'Part__c';
    proRelated.parentId = prod.Id;
    proRelated.startDateField = 'Alternate_Part_Effective_Start_Date__c';
    proRelated.endDateField = 'Alternate_Part_Effective_End_Date__c';
    
    proRelated.getRecords();
    system.debug('proRelated.fieldSetName'+proRelated.fieldSetName);
    system.debug('proRelated.stdSetContrl'+proRelated.stdSetContrl);
    
    Boolean abc = proRelated.isRecordFound;
    testList = proRelated.getRecords();
    
    proRelated.parentField = 'Alternate_Product__c';
    proRelated.getRecords();
  }
  
  
  
  
}