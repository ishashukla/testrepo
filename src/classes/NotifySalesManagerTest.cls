// (c) 2015 Appirio, Inc.
//
// Class Name: NotifySalesManagerTest
// Description: Test Class for NotifySalesManager   
// April 22, 2016    Meena Shringi    Original 
//
@isTest
private class NotifySalesManagerTest {
    static testMethod void testNotifySalesManager(){
        User usr = TestUtils.createUser(1, 'Instruments Sales Manager', true).get(0);
        System.runAs(usr) {
            Forecast_Year__c foreYears = new Forecast_Year__c();
            foreYears.Manager__c = usr.Id;
            foreYears.User2__c = usr.Id;
            foreYears.Year__c = '2016';
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
            Id recordTypeId = rtByName.getRecordTypeId();
            foreYears.RecordTypeId = recordTypeId;
            insert foreYears;
            NotifySalesManager notify = new NotifySalesManager();
            Date dateField = Date.today();
            Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
            Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
            notify.todaysDate = lastDayOfMonth;
            
            Test.startTest();
            database.executeBatch(notify);    
            Test.stopTest();
        }
    }
}