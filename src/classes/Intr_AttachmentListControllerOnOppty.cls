/****************************************************************************************************************************************
Author        :  Appirio JDC (Shreerath Nair)
Date          :  Aug 3, 2016
Purpose       :  (I-228030)Controller To Create custom attachment Related List Opportunity page 

******************************************************************************************************************************************/

public without sharing class Intr_AttachmentListControllerOnOppty {
    
    public List<Id> oppList {get;set;}
    public List<Opportunity> opportunityList{get;set;}
    
    public Intr_AttachmentListControllerOnOppty(ApexPages.StandardController controller) {
        
        oppList = new List<Id>();
        Opportunity opportunity = (Opportunity)controller.getRecord();    
        opportunityList = [Select id,Name from Opportunity
        			   	   WHERE Id = :opportunity.Id];
		 
		oppList.add(opportunityList[0].Id);
		
        
    }
    
}