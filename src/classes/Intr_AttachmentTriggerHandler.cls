// 
// (c) 2016 Appirio, Inc.
//
// I-227752, This is a controller class for Attachment Trigger Handler
//
// Aug 5, 2016  Shreerath Nair  Original
public  class Intr_AttachmentTriggerHandler {

   //Function to check if an attachment exist Task
   public static void checkAttachmentOnTask(list<Attachment> lstOldAttachment){
   	

  		List<Task> taskToUpdate = new List<Task>();	
  		//Check if attachment related to Task
   		for(Attachment attach : lstOldAttachment){
         //Check if added attachment is related to Task or not
         String parentIdString = String.valueof(attach.parentId);
        	if(parentIdString.substring(0,3) == '00T'){
        		
        		//Add task to List for Updation
        		Task task = new Task(id=attach.parentId,Has_Attachment__c = false);
            	taskToUpdate.add(task);
         	}
        }
        
        
        //Update the Task Records
        if(taskToUpdate.size()>0){
        	update taskToUpdate;	
        }
        
   	
   }
   
   
}