/**================================================================      
 * Appirio, Inc
 * Name: InstallBaseProductMgmtControllerTest
 * Description: Test class for InstallBaseProductMgmtController(Original(T-541293))
 * Created Date: [08/10/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/
@isTest
public class InstallBaseProductMgmtControllerTest {
  Static List<InstalledProduct__c> ip;
  Static List<Install_Base__c> ib;
  Static List<Account> acc;
  Static List<Product2> prod;
  Static List<System__c> sys;
  static List<User> user;
  
  static testMethod void testConstructor(){
    createData();
    System.runAs(user[0]) {
      Test.startTest();
        ib[0].Type__c = 'Stryker';
        ib[0].System__c =sys[0].Id;
        insert ib;
        ip[0].Install_Base__c = ib[0].Id;
        ip[0].Custom_Name__c = 'TestProduct12';
        insert ip;
        ip[0].Enable_Duplicate_Product__c = true;
        ip[0].Duplicate_Quantity__c = 5;
        update ip;
        PageReference pageRef1 = Page.InstallBaseMobilePage;
        Test.setCurrentPage(pageRef1);
        System.currentPageReference().getParameters().put('ibId', String.valueOf(ib[0].Id));
        System.currentPageReference().getParameters().put('BU', 'NSE');
        System.currentPageReference().getParameters().put('type', 'Stryker');
        InstallBaseProductMgmtController ibProdMgmt = new InstallBaseProductMgmtController();
        ibProdMgmt.dupQuantity = '3';
        ibProdMgmt.selectedProduct = prod[1].Id;
        ibProdMgmt.createNewProduct();
        ibProdMgmt.sltProducts = '1';
        ibProdMgmt.keyVal = 1;
        ibProdMgmt.keyValDel = 1;
        ibProdMgmt.showIntermediatePopup();
        ibProdMgmt.showPopup();
        ibProdMgmt.saveAndContinue();
        ibProdMgmt.save();
      Test.stopTest();
    }
  }
    
    static testMethod void testDelete(){
    createData();
    System.runAs(user[0]) {
      Test.startTest();
        ib[0].Type__c = 'Stryker';
        ib[0].System__c =sys[0].Id;
        insert ib;
        ip[0].Install_Base__c = ib[0].Id;
        ip[0].Custom_Name__c = 'TestProduct12';
        insert ip;
        ip[0].Enable_Duplicate_Product__c = true;
        ip[0].Duplicate_Quantity__c = 5;
        update ip;
        PageReference pageRef1 = Page.InstallBaseMobilePage;
        Test.setCurrentPage(pageRef1);
        System.currentPageReference().getParameters().put('ibId', String.valueOf(ib[0].Id));
        System.currentPageReference().getParameters().put('BU', 'NSE');
        System.currentPageReference().getParameters().put('type', 'Stryker');
        InstallBaseProductMgmtController ibProdMgmt = new InstallBaseProductMgmtController();
        ibProdMgmt.dupQuantity = '3';
        ibProdMgmt.selectedProduct = prod[1].Id;
        ibProdMgmt.createNewProduct();
        ibProdMgmt.sltProducts = '1';
        ibProdMgmt.keyVal = 1;
        ibProdMgmt.keyValDel = 1;
        ibProdMgmt.showIntermediatePopup();
        ibProdMgmt.showPopup();
        ibProdMgmt.cancelPopUp();
        ibProdMgmt.deleteProduct();
        System.assertEquals(2,ibProdMgmt.mapIntegerToInstallProduct.size());
      Test.stopTest();
    }
  }
  
  static testMethod void testDel(){
    createData();
    System.runAs(user[0]) {
      Test.startTest();
        ib[0].Type__c = 'Stryker';
        ib[0].System__c =sys[0].Id;
        insert ib;
        ip[0].Install_Base__c = ib[0].Id;
        ip[0].Custom_Name__c = 'TestProduct';
        insert ip;
        ip[0].Enable_Duplicate_Product__c = true;
        ip[0].Duplicate_Quantity__c = 5;
        update ip;
        PageReference pageRef1 = Page.InstallBaseMobilePage;
        Test.setCurrentPage(pageRef1);
        System.currentPageReference().getParameters().put('ibId', String.valueOf(ib[0].Id));
        System.currentPageReference().getParameters().put('BU', 'NSE');
        System.currentPageReference().getParameters().put('type', 'Stryker');
        InstallBaseProductMgmtController ibProdMgmt = new InstallBaseProductMgmtController();
        ibProdMgmt.dupQuantity = '3';
        ibProdMgmt.getSelectedRecord();
        ibProdMgmt.selectedProduct = prod[1].Id;
        ibProdMgmt.createNewProduct();
        ibProdMgmt.sltProducts = '1';
        ibProdMgmt.keyVal = 1;
        ibProdMgmt.keyValDel = 1;
        ibProdMgmt.showIntermediatePopup();
        ibProdMgmt.showPopup();
        ibProdMgmt.cancelPopUp();
        ibProdMgmt.showPopup();
        ibProdMgmt.saveAndContinue();
        ibProdMgmt.save();
        ibProdMgmt.delProduct();
        ibProdMgmt.save();
        System.assertEquals(2,ibProdMgmt.mapIntegerToInstallProduct.size());
      Test.stopTest();
    }
  }
  
  public static List<System__c> createSystem(Integer sysCount,String sysName ,String businessunit ,Boolean isInsert) {
        List<System__c> sysList = new List<System__c>();
        
        for(Integer i = 0; i < sysCount; i++) {
            System__c sysRecord = new System__c(Name = sysName , Business_Unit__c = businessunit);
            sysList.add(sysRecord);
        }
        if(isInsert) {
            insert sysList;
        } 
        return sysList;
    }
    public static List<Install_Base__c> createInstallBase(Integer ibCount,Id accId , Id userId, Id sysId,Boolean isInsert) {
        List<Install_Base__c> ibList = new List<Install_Base__c>();
        
        for(Integer i = 0; i < ibCount; i++) {
            Install_Base__c ibRecord = new Install_Base__c(Account__c = accId , System__c = sysId , ownerId = userId);
            ibList.add(ibRecord);
        }
        if(isInsert) {
            insert ibList;
        } 
        return ibList;
    }
    public static void createData() {
        user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        System.runAs(user[0]) {
            acc = TestUtils.createAccount(1, True);
            sys = createSystem(1,'System1' ,'NSE',True);
            ib = createInstallBase(2,acc[0].Id, user[0].Id, sys[0].Id,false);
            prod = TestUtils.createProduct(2,True);
            ip = TestUtils.createInstalledProduct(1,prod[0].Id, false);
        }
    }
}