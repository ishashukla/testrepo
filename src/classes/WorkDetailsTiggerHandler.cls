/**================================================================      
* Appirio, Inc
* Name: WorkDetailsTiggerHandler
* Description: Handler Class for WrokDetailsTigger T-534091
* Created Date: 13 - Sept - 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update
* 24 Oct 2016       Varun Vasishtha   I-240789 Condition added to check threshold on Billable line price on discount approval
* 02 Dec 2016       Isha Shukla       I-246406 Added method updateQuantityOnProductStock
==================================================================*/
public class WorkDetailsTiggerHandler {
    Static map<string,string> recordtypeDetails = new Map<String,string>();
    
    static{
        Set<String> setDevRTname =new set<String>{'UsageConsumption'};
        for(Recordtype varloop: [SELECT Id, DeveloperName 
                 FROM RecordType 
                 WHERE DeveloperName in :setDevRTname
                 AND SObjectType = 'SVMXC__Service_Order_Line__c']){
            recordtypeDetails.put(varloop.id,varloop.developername);                        
        }
    }
    
    // @ISSUES -    I-239296
    // @DEVELOPER - Gaurav Sinha
    // @DESCRIPTION - SVMXC__Consumed_From_Location__c is defaulted as workorder technician location
    public static void setDefaultLocation(List<SVMXC__Service_Order_Line__c> items){
        set<id> woid = new set<id>();
        map<id,string> mapwolocation = new map<id,string>();
        for(SVMXC__Service_Order_Line__c varloop : items){
            if(varloop.SVMXC__Consumed_From_Location__c == null
               && recordtypeDetails.get(varloop.recordtypeid) == 'UsageConsumption') {
               woid.add(varloop.SVMXC__Service_Order__c);
           }
        }
        
        for(SVMXC__Service_Order__c varloop :[select id,SVMXC__Group_Member__r.SVMXC__Inventory_Location__c 
                                              from SVMXC__Service_Order__c
                                              where id in : woid] ){
            mapwolocation.put(varloop.id,varloop.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c);
        }
        
        for(SVMXC__Service_Order_Line__c varloop : items){
            if(varloop.SVMXC__Consumed_From_Location__c == null
               && recordtypeDetails.get(varloop.recordtypeid) == 'UsageConsumption'){
               varloop.SVMXC__Consumed_From_Location__c = mapwolocation.get(varloop.SVMXC__Service_Order__c);
           }
        }        
    }
    
    // @ISSUES -    I-239296
    // @DEVELOPER - Gaurav Sinha
    public static void onbeforeInsert(List<SVMXC__Service_Order_Line__c> items){
        setDefaultLocation(items);
    }
    public static void onAfterInsert(List<SVMXC__Service_Order_Line__c> items){
        DiscountPendingApproval(items,null);
        updateQuantityOnProductStock(items);
    }
    
    
    public static void onAfterUpdate(List<SVMXC__Service_Order_Line__c> items, Map<Id,SVMXC__Service_Order_Line__c> oldItems){
        DiscountPendingApproval(items,oldItems);        
    }
    
    // Checks for discount or threshold for billable line price on Work Order Details and updates Work order discount approval
    private static void DiscountPendingApproval(List<SVMXC__Service_Order_Line__c> newItems, Map<Id,SVMXC__Service_Order_Line__c> oldItems){
        Map<Id,SVMXC__Service_Order__c> related = new Map<Id,SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order_Line__c entity : newItems){
            SVMXC__Service_Order_Line__c oldEntity;
            if(oldItems != null){
                oldEntity = oldItems.get(entity.Id);
            }
            if(entity.SVMXC__Service_Order__c != null){
                if( (oldEntity == null || (oldEntity.SVMXC__Discount__c != entity.SVMXC__Discount__c || oldEntity.SVMXC__Line_Type__c != entity.SVMXC__Line_Type__c || 
                                           oldEntity.SVMXC__Billable_Line_Price__c != entity.SVMXC__Billable_Line_Price__c)) &&
                   ((entity.SVMXC__Discount__c > 15 && entity.SVMXC__Line_Type__c == 'Parts') || (entity.SVMXC__Discount__c > 0 && entity.SVMXC__Line_Type__c == 'Labor') ||
                    (entity.SVMXC__Line_Type__c == 'Labor' && entity.SVMXC__Billable_Line_Price__c > 2500))
                  ){
                    SVMXC__Service_Order__c relatedEntity = new SVMXC__Service_Order__c(id = entity.SVMXC__Service_Order__c);
                    relatedEntity.Discount_Approval__c = 'Pending Approval';
                    if(!related.containsKey(entity.SVMXC__Service_Order__c))
                    {
                        related.put(entity.SVMXC__Service_Order__c,relatedEntity);
                    }
                }
            }
        }
        
        if(related.size() > 0)        {
            update related.values();
        }
    }
    
    private static void updateQuantityOnProductStock(List<SVMXC__Service_Order_Line__c> workDetails) {
        Map<Id,List<SVMXC__Service_Order_Line__c>> productStockIdToWorkDetailsMap = new Map<Id,List<SVMXC__Service_Order_Line__c>>();
        for(SVMXC__Service_Order_Line__c woLines : workDetails) {
            if(woLines.SVMX_Consumed_Part__c != null) {
                if(!productStockIdToWorkDetailsMap.containsKey(woLines.SVMX_Consumed_Part__c)) {
                    productStockIdToWorkDetailsMap.put(woLines.SVMX_Consumed_Part__c,new List<SVMXC__Service_Order_Line__c>{woLines});
                } else {
                    productStockIdToWorkDetailsMap.get(woLines.SVMX_Consumed_Part__c).add(woLines);
                }
            }
        }
        //system.debug('productStockIdToWorkDetailsMap>>'+productStockIdToWorkDetailsMap);
        String header;
        MAp<Id,SVMXC__Product_Stock__c> productStockMapToUpdate = new Map<Id,SVMXC__Product_Stock__c>();
        List<SVMXC__Service_Order_Line__c> woLinesToUpdate = new List<SVMXC__Service_Order_Line__c>();
        Set<Id> woLineIDset = new Set<Id>();
        Set<Id> stockedSerial = new Set<Id>();
        for(SVMXC__Product_Stock__c productStock : [SELECT Id,SVMXC__Quantity2__c 
                                                    FROM SVMXC__Product_Stock__c 
                                                    WHERE Id IN :productStockIdToWorkDetailsMap.keySet()]) {
            for(SVMXC__Service_Order_Line__c workDetail : productStockIdToWorkDetailsMap.get(productStock.Id)) {
                //System.debug('workdetail>>'+workDetail.SVMXC__Billable_Quantity__c+'product>>'+productStock.SVMXC__Quantity2__c);
                if(workDetail.SVMXC__Billable_Quantity__c != null ){
                    if(workDetail.SVMXC__Billable_Quantity__c >= productStock.SVMXC__Quantity2__c) {
                        productStock.SVMXC__Quantity2__c = 0;
                    } 
                    else {
                        productStock.SVMXC__Quantity2__c = productStock.SVMXC__Quantity2__c - workDetail.SVMXC__Billable_Quantity__c;
                    }  
                    if(workDetail.SVMX_Consumed_Serial_Number__c != null) {
                        stockedSerial.add(workDetail.SVMX_Consumed_Serial_Number__c);
                    }
                    woLineIDset.add(workDetail.Id); 
                    if(!productStockMapToUpdate.containsKey(productStock.Id)) {
                        productStockMapToUpdate.put(productStock.Id,productStock);
                    } 
                }               
            }            
        }
                                                    
        List<SVMXC__Product_Serial__c> stockedSerialListToUpdate = new List<SVMXC__Product_Serial__c>();
        if(stockedSerial.size() > 0) {
            for(Id ss : stockedSerial) {
                SVMXC__Product_Serial__c stockSerialRecord = new SVMXC__Product_Serial__c(
                    Id = ss,SVMXC__Active__c = false
                );
                stockedSerialListToUpdate.add(stockSerialRecord);
            }
        }
        system.debug('stockedSerialListToUpdate>>'+stockedSerialListToUpdate);
        if(stockedSerialListToUpdate.size() > 0) {
            update stockedSerialListToUpdate;
        }
        System.debug('productStockListToUpdate>>'+productStockMapToUpdate);
        if(productStockMapToUpdate.values().size() > 0) {
            update productStockMapToUpdate.values();
        }
        if(woLineIDset.size() > 0 ) {
            for(Id wolineID : woLineIDset) {
                SVMXC__Service_Order_Line__c woLineRecord = new SVMXC__Service_Order_Line__c(
                    Id = wolineID,SVMXC__Line_Status__c = 'Completed'
                );
                woLinesToUpdate.add(woLineRecord);
            }
        }
        System.debug('woLinesToUpdate>>'+woLinesToUpdate);
        if(woLinesToUpdate.size() > 0) {
            update woLinesToUpdate;
        }
    }    
}