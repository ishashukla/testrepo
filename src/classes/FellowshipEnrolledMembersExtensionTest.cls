/****************************************************************
Name  :  FellowshipEnrolledMembersExtensionTest
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 16, 2016

Modified: 
****************************************************************/
@isTest
private class FellowshipEnrolledMembersExtensionTest {
  
  @isTest static void testMethodOne() {
    
    Account acc = Endo_TestUtils.createAccount(1, true).get(0);
    Fellowship__c fs = new Fellowship__c(Account__c = acc.Id);
    insert fs;
    Fellowship_Member__c fsm = new Fellowship_Member__c(Fellowship__c = fs.Id,  Status__c = 'Enrolled');
    insert fsm;
    
    PageReference pageRef = Page.FellowshipEnrolledMembers;
        pageRef.getParameters().put('Id', String.valueOf(fs.Id));
        Test.setCurrentPage(pageRef);

    ApexPages.StandardController sc = new ApexPages.StandardController(fs);

    Test.startTest();
    FellowshipEnrolledMembers_Extension fellowshipEnrolledMembersExt = new FellowshipEnrolledMembers_Extension(sc);
        fellowshipEnrolledMembersExt.first();
        fellowshipEnrolledMembersExt.previous(); 
        fellowshipEnrolledMembersExt.last();
        fellowshipEnrolledMembersExt.next();
        
        fellowshipEnrolledMembersExt.sortedField='Contact__c';
        fellowshipEnrolledMembersExt.ascDesc = ' desc ';
        fellowshipEnrolledMembersExt.sortData();
        Boolean hasnext = fellowshipEnrolledMembersExt.hasNext;
        Boolean hasprevious = fellowshipEnrolledMembersExt.hasPrevious;
        Integer page1 = fellowshipEnrolledMembersExt.pageNumber;
        fellowshipEnrolledMembersExt.isAsc = true;
        
    Test.stopTest();
  }

  @isTest static void testMethodTwo() {
    
    Account acc = Endo_TestUtils.createAccount(1, true).get(0);
    Fellowship__c fs = new Fellowship__c(Account__c = acc.Id);
    insert fs;
    Fellowship_Member__c fsm = new Fellowship_Member__c(Fellowship__c = fs.Id,  Status__c = 'Test');
    insert fsm;
    
    PageReference pageRef = Page.FellowshipEnrolledMembers;
        pageRef.getParameters().put('Id', String.valueOf(fs.Id));
        Test.setCurrentPage(pageRef);

    ApexPages.StandardController sc = new ApexPages.StandardController(fs);

    Test.startTest();
    FellowshipEnrolledMembers_Extension fellowshipEnrolledMembersExt = new FellowshipEnrolledMembers_Extension(sc);
    Test.stopTest(); 
  }
  
}