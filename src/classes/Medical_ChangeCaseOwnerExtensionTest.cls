/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_ChangeCaseOwnerExtensionTest
//
// 27 Nov 2015 , Prakarsh (Appirio JDC)
//
//
*/
@isTest
public class Medical_ChangeCaseOwnerExtensionTest {

	static testMethod void testMedical_ChangeCaseOwnerExtension(){
		List<User> lstMedicaluser = Medical_TestUtils.createUser(10, 'Medical - Call Center', true);

		Account acc = Medical_TestUtils.createAccount(1, true).get(0);

		Contact con = Medical_TestUtils.createContact(1, acc.Id, true).get(0);

		Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
        TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
		List<Case> lstCase = Medical_TestUtils.createCase(5, acc.Id, con.Id, false);
		for(Case c : lstCase) {
			c.RecordTypeId = MedicalrecordTypeId;
		}
		insert lstCase;
    
    List<Event> lstEvents = Medical_TestUtils.createEvent(5,acc.Id,false);
    for(Event e: lstEvents){
    	Datetime d = Datetime.now().addDays(1);
      e.StartDateTime = d;
    	e.EndDateTime = d.addHours(1);
    	e.DurationInMinutes  = 60;
    	e.ActivityDateTime = d;
    }
    insert lstEvents;
		//QueuesObject queu = Medical_TestUtils.createQueue();

		Test.startTest();

	    Test.setCurrentPage(Page.Medical_ChangeCaseOwner);
	    System.currentPageReference().getParameters().put('Id', String.valueOf(lstCase.get(0).Id));
	    ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstCase);
	
	
			// Verify when user select new Owner from User Lookup
			Medical_ChangeCaseOwnerExtension objController = new Medical_ChangeCaseOwnerExtension(sc);
			objController.selectedOwner = 'User';
			objController.selectedUserId = lstMedicaluser.get(0).Id;
			objController.saveOwner();
	
	
	
			// Verify when user select new Queue from User Lookup
	    //Medical_ChangeCaseOwnerExtension objController2 = new Medical_ChangeCaseOwnerExtension(sc);
	    //objController2.selectedOwner = 'Queue';
	    //objController2.selectedQueueId = queu.Id;
	    //objController2.saveOwner();

		Test.stopTest();
	}


}