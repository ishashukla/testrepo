// (c) 2016 Appirio, Inc.
//
// Class Name: OmittedNot_OmittedController
// Description: Controller Class for OmittedNot_Omitted page which is used by Quick Action "Omitted/Not Omitted"
// 
// June 27 2016, Isha Shukla  Original(T-514494) 
//
public with sharing class OmittedNot_OmittedController {
    public Opportunity oppRecord;
    public OmittedNot_OmittedController(ApexPages.StandardController stdController) {
        oppRecord = (Opportunity)stdController.getRecord();
        System.debug((Opportunity)stdController.getRecord());
        // Check forecast category is other than Omitted 
        if(!oppRecord.ForecastCategoryName.containsIgnoreCase('Omitted')) { 
            oppRecord.ForecastCategoryName = 'Omitted'; // assigning Omitted value to forecast category
        // check when stage is not equal to closed won or closed lost and forecast category is other than Omitted 
        } else if(!oppRecord.StageName.containsIgnoreCase('Closed Won') && !oppRecord.StageName.containsIgnoreCase('Closed Lost') && oppRecord.ForecastCategoryName.containsIgnoreCase('Omitted')) {
            oppRecord.ForecastCategoryName = 'Pipeline'; // assigning Pipeline value to forecast category
        } else if(oppRecord.ForecastCategoryName.containsIgnoreCase('Omitted') && oppRecord.StageName.containsIgnoreCase('Closed Won')) {
            oppRecord.ForecastCategoryName = 'Closed';
        } 
    }
    // method to update the opportunity and redirect back to the record
    public PageReference omitNotOmit() {
        System.debug('oppList=='+oppRecord);
        update oppRecord;
        PageReference homePage = new PageReference('/'+oppRecord.Id);  
        homepage.setRedirect(true);
        return homePage;
    }
}