// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncEBSInventory_REST - Apex class to make Inventory sync callout.
//
// 25 Oct 2016, Isha Shukla (T-550235) 
public class COMM_SyncEBSInventory_REST {
  public static final String CONTENT_TYPE = 'application/json';
   public static final String CREATE_ORDER_NAMED_CREDENTIALS = 'callout:IC_Inventory_Sync';
   public static final String BLANK_RESPONSE = 'Response is blank';
    
    // Method to create order on oracle SOA with rest callout
    // @param Order ord 
    // @return String response Message
    public static String createOrderRestCallout(InventorySyncRequest iRequest) {
        
        // Create request objects
        InventorySyncRequest request = iRequest;
        // Convert apex to json string
        String body = JSON.serialize(request);        
        // Replace tag with $t as order informatica service need this special $ symbol
        body = body.replace('tag', '$t');
        
        try { 
            // call sendRequest method
            String responseBody = sendPOSTRequest(CONTENT_TYPE, CREATE_ORDER_NAMED_CREDENTIALS, body);
            
            // Replace $t with tag as apex don't allow special $ symbol in variable name
            responseBody = responseBody.replace('$t', 'tag');
            
            // Return callout response
           return responseBody;
            
        }catch(CalloutException ex){
            
            // Return exception message
            return ex.getMessage();
        }
    }
    
    public static IntegrationHeader createIntegrationHeader(String sender,String receiver){
        COMM_SyncEBSInventory_REST.IntegrationHeader header = new COMM_SyncEBSInventory_REST.IntegrationHeader();
        header.SenderIdentifier = new COMM_SyncEBSInventory_REST.TagName(sender);
        header.ReceiverIdentifier = new COMM_SyncEBSInventory_REST.TagName(receiver);
        header.MessageID = new COMM_SyncEBSInventory_REST.TagName(String.valueOf(System.now().getTime()));
        return header;
    }
    
     
    // Http callout with Named Credentials
    public static String sendPOSTRequest(String contentType, String namedCredentials, String body){
        String responseBody;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(namedCredentials);
        req.setMethod('POST');
        req.setHeader('Content-Type', contentType);
        req.setBody(body);
        System.debug('REST Request :: '+req.getBody());
        req.setTimeout(120000);
        Http http = new Http();
        if (!Test.isRunningTest()){
            HTTPResponse res = http.send(req);
            responseBody = res.getBody();
            //System.debug('Body of Response :: '+responseBody);
        } else{
             responseBody = '{"InventoryResponse":{"xmlns":"http://schemas.stryker.com/LocalInventory/'+
                 'V1","xmlns$env":"http://schemas.xmlsoap.org/soap/envelope/","xmlns$wsa":"http://'+
                 'www.w3.org/2005/08/addressing","responseCode":{"$t":"Success"},"responseMessage":'+
                 '{"$t":"Message submitted successfully to Middleware"}}}';
        }
        System.debug(' REST Response :: '+responseBody);
        return responseBody;
    }
   
    public class InventorySyncRequest {
        public String xmlns = 'http://schemas.stryker.com/LocalInventory/V1';
        public InventoryRequest inventoryRequest;
        
    }
    
    // Class definition - TagName to support namespace - &t in json request
    public class TagName {
        public String tag;
        public TagName(String tag){
            this.tag = tag;
        }
        public TagName(){}
    }

    public class inventoryRequest {
        public IntegrationHeader IntegrationHeader;
        public inventoryDetails inventoryDetails;
    }
    // Class definition - IntegrationHeader
    public class IntegrationHeader{
        public TagName MessageID;
        public TagName CreationDateTime;        
        public TagName SenderIdentifier;
        public TagName ReceiverIdentifier;
     }
    public class inventoryDetails {
        public List<inventory> inventory;        
    }
    public class inventory {
        public TagName headerId;
        public TagName lineId;
        public TagName itemNumber;
        public TagName sourceLocation;
        public TagName transactionQuantity;
        public TagName transactionDate;
        public TagName transactionTypeId;
        public TagName destinationLocation;
        public inventoryLines inventoryLines;
    }
    public class inventoryLines {
        public List<inventoryLine> inventoryLine;        
    }
    
    public class inventoryLine {
        public TagName lotExpirationDate;
        public TagName transactionQuantity;
        public TagName serialLotNumber;
    }

}