/**==================================================================================================================================
 * (c) 2016 Appirio, Inc
 * Name: Batch_SetProductDivision
 * Description: Batch class that will allow to view division information on the COMM Product record directly
 * Created Date: Nov 08, 2016
 * Created By: Isha Shukla (Appirio)
 * 
 * Nov 8th, 2016               Isha Shukla                       T-553847 - created
 * Nov 15th, 2016              Nathalie Le Guay                  Based on assumption/understanding that PricebookEntries are not deleted
 *                                                               by integrations (Devi), adding additional filter criteria to query
 ==========================================================================================================================================*/

global class Batch_SetProductDivision implements Database.Batchable<sObject>, System.Schedulable {


    // Querying products having pricebook 'COMM Price book' from PriceBookEntry standard object
    // If other divisions would like this logic to run on their pricebooks, this query needs to be updated
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT PriceBook2.Name, Product2Id, Product2.COMM_Product__c, Product2.Endo_Product__c,'+
            ' Product2.SPS_Product__c, Product2.NV_Product__c, Product2.Medical_Product__c, Product2.Instruments_Product__c,'+
            ' Product2.Flex_Product__c FROM PriceBookEntry WHERE PriceBook2.Name = \'COMM Price Book\' and Product2.COMM_Product__c = false ORDER BY Product2Id'; 
        return Database.getQueryLocator(query);
    }



    // This method will identify which products have a pricebookEntry for the COMM division, and update the
    // COMM_Product__c Boolean to TRUE if found, otherwise it will be set to FALSE
    global void execute(Database.BatchableContext bc, List<PriceBookEntry> records) {
        Set<Id> setOfProductIds = new Set<Id>();
        List<Product2> productListToUpdate = new List<Product2>();
        Map<Id,List<PriceBookEntry>> mapProductToPricebookEntries = new Map<Id,List<PriceBookEntry>>();
        
        
        
        Map<Id, Product2> productMap = new Map<Id, Product2>();
        
        for (PricebookEntry pbe: records) {
          Product2 productRecordToUpdate;
          if (productMap.get(pbe.Product2Id) == null) {
            productRecordToUpdate = new Product2(
                    Id = pbe.product2Id,
                    COMM_Product__c = false,
                    Endo_Product__c = false,
                    Flex_Product__c = false,
                    Medical_Product__c = false,
                    NV_Product__c = false,
                    SPS_Product__c = false,
                    Instruments_Product__c = false
                );
            productMap.put(pbe.Product2Id, productRecordToUpdate);
          }
          productRecordToUpdate = productMap.get(pbe.Product2Id);
          
          if(pbe.PriceBook2.Name == 'COMM Price Book') {
                        productRecordToUpdate.COMM_Product__c = true;
               /*     } else if(pbe.PriceBook2.Name == 'Endo List Price') {
                        productRecordToUpdate.Endo_Product__c = true;
                    } else if(pbe.PriceBook2.Name == 'Flex List Price') {
                        productRecordToUpdate.Flex_Product__c = true;
                    } else if(pbe.PriceBook2.Name == 'Medical') {
                        productRecordToUpdate.Medical_Product__c = true;
                    } else if(pbe.PriceBook2.Name == 'NV Price Book' || 
                              pbe.PriceBook2.Name == '2014 Stryker NV - Price Book') {
                                  productRecordToUpdate.NV_Product__c = true;
                    } else if(pbe.PriceBook2.Name == 'SPS Products') {
                                  productRecordToUpdate.SPS_Product__c = true;
                    } else if(pbe.PriceBook2.Name == 'Surgical Price Book' || 
                              pbe.PriceBook2.Name == 'Navigation Price Book' || 
                              pbe.PriceBook2.Name == 'IVS Price Book' || 
                              pbe.PriceBook2.Name == 'NSE Price Book') {
                                  productRecordToUpdate.Instruments_Product__c = true;*/
                   } 
                   
          
        }
        
        update productMap.values();

    }



    // If error occurs send email to COMM admin
    // If other divisions would like these notifications, this needs to be updated
    global void finish(Database.BatchableContext BC){
        Endo_EmailUitlity emailUtility = new Endo_EmailUitlity();
        List<String> toAddresses = new List<String>();
        AsyncApexJob jobResult = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                                  FROM AsyncApexJob WHERE Id =: BC.getJobId()].get(0);
        system.debug('jobResult'+jobResult);
        if(jobResult.NumberOfErrors > 0 || Test.isRunningTest()) { 
            for (Division_Admin__c divAdmin : Division_Admin__c.getAll().values()){
                if(divAdmin.COMM__c == true) {
                    toAddresses.add(divAdmin.Email__c);
                }
            }
            String emailBody = 'The Batch Batch_SetProductDivision job processed ' + jobResult.JobItemsProcessed + ' batches out of ' + 
                jobResult.TotalJobItems + ' and generated number of errors ' + jobResult.NumberOfErrors ;
            
            String userId = UserInfo.getUserId();
            Messaging.SingleEmailMessage mail = emailUtility.createEmailMessage('SFDC Batch Error Alert: Batch_SetProductDivision failed', 
                                                                                emailBody, null, toAddresses, userId, false);
            
            // Send email to admins for batch failure
            if (!Test.isRunningTest()) {            
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                return;
            }       
        }
    }

    // Scheduler for batch  
    global void execute(SchedulableContext sc){
        if(!Test.isRunningTest()){
            Id batchId = Database.executeBatch(new Batch_SetProductDivision(), 200);
        }
    }
}