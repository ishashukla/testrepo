//
// (c) 2015 Appirio, Inc.
//
// Apex Test Class Name: nv_DailyOrderModel_Test
// For Apex Class: nv_DailyOrderModel
//
// 07th January 2015   Hemendra Singh Bhati   Original (Task # T-459198)
// 13th January 2015   Hemendra Singh Bhati   Modified (Issue # I-198926) - Please see issue description for more details.
//
@isTest(seeAllData=false)
private class nv_DailyOrderModel_Test {
  // Private Data Members.
  private static final String SYSTEM_ADMINISTRATOR_PROFILE_NAME = 'System Administrator';
  private static final String REGIONAL_MANAGER_CENTRAL_ROLE_NAME = 'Regional Manager - Central';
  private static final String ORDER_NV_CREDIT_RECORD_TYPE_LABEL = 'NV Credit';
  
  private static Order theTestParentOrder;
  private static List<OrderItem> theTestOrderItems;
  private static User theCentralRegionalManager;
  
  static{
    // Instantiating Model Filter Option Inner Class.
    nv_DailyOrderModel.FilterOption theModelFilterOptionInstance = new nv_DailyOrderModel.FilterOption();
    theModelFilterOptionInstance.label = 'Test Label';
    theModelFilterOptionInstance.isEnabled = true;

    // Extracting "System Administrator" Profile Id.
    List<Profile> theProfiles = [SELECT Id FROM Profile WHERE Name = :SYSTEM_ADMINISTRATOR_PROFILE_NAME];
    system.assert(
      theProfiles.size() > 0,
      'Error: The requested user profile does not exist.'
    );

    // Extracting "Regional Manager - Central" Role Id.
    List<UserRole> theUserRoles = [SELECT Id FROM UserRole WHERE Name = :REGIONAL_MANAGER_CENTRAL_ROLE_NAME];
    system.assert(
      theUserRoles.size() > 0,
      'Error: The requested user role does not exist.'
    );

    // Inserting Test User With "Regional Manager - Central" Role.
    theCentralRegionalManager = new User(
      ProfileId = theProfiles.get(0).Id,
      UserRoleId = theUserRoles.get(0).Id,
        Alias = 'hsb',
        Email = 'hsingh@appirio.com',
        EmailEncodingKey = 'UTF-8',
        FirstName = 'Hemendra Singh',
        LastName = 'Bhati',
        LanguageLocaleKey = 'en_US',
        LocaleSidKey = 'en_US',
        TimezoneSidKey = 'Asia/Kolkata',
        Username = 'hsingh@appirio.com.srtyker',
        CommunityNickName = 'hsb',
        IsActive = true
    );
    insert theCentralRegionalManager;

    // Generating Test Data.
    NV_Follow__c theOrderFollower = null;
    Account theTestAccount = null;
    Contract theTestContract = null;
    Id standardPriceBookId = null;
    Product2 theTestProduct = null;
    PricebookEntry theTestPBE = null;
    theTestParentOrder = null;
    Order theTestChildOrder = null;
    theTestOrderItems = null;
    Order_Invoice__c theOrderInvoice = null;
    List<Order_Invoice__c> theTestOrderInvoices = null;
    Id theOrderNVCreditRecordTypeId = Schema.SObjectType.Order.RecordTypeInfosByName.get(ORDER_NV_CREDIT_RECORD_TYPE_LABEL).RecordTypeId;

    // Running Thread As Central Regional Manager.
    system.runAs(theCentralRegionalManager) {
      // Inserting Test NV Follow Record.
      theOrderFollower = NV_TestUtility.createNVFollow(
        UserInfo.getUserId(),
        'User',
        theCentralRegionalManager.Id,
        true
      );

      // Inserting Test Account.
      theTestAccount = new Account(Name = 'Test Account');
      insert theTestAccount;

      // Inserting Test Contract.
      theTestContract = new Contract(
        AccountId = theTestAccount.Id,
        Status = 'Draft',
        StartDate = System.today().addDays(-10),
        ContractTerm = 1,
        External_Integration_Id__c = '123123'
      );
      insert theTestContract;

      // Extracting Standard PriceBook Id.
      standardPriceBookId = Test.getStandardPricebookId();

      // Inserting Test Product.
      theTestProduct = new Product2(
        Name = 'Test Product',
        Catalog_Number__c = '1',
        IsActive = true
      );
      insert theTestProduct;

      // Inserting Test Pricebook Entry.
      theTestPBE = new PricebookEntry(
        Pricebook2Id = standardPriceBookId,
        Product2Id = theTestProduct.Id,
        UnitPrice = 99.00,
        isActive = true
      );
      insert theTestPBE;

      // Instantiating Test Parent Order.
      theTestParentOrder = new Order(
        AccountId = theTestAccount.Id,
        ContractId = theTestContract.Id,
        Status = 'Draft',
        EffectiveDate = Date.today(),
        Date_Ordered__c = Datetime.now(),
        Pricebook2Id = standardPriceBookId,
        Oracle_Order_Number__c = '1234567890',
        Invoice_Related_Sales_Order__c = '1234567891',
        Customer_PO__c = 'PO0000012',
        On_Hold__c = true,
        RecordTypeId = theOrderNVCreditRecordTypeId
      );

      // Instantiating Test Child Order.
      theTestChildOrder = new Order(
        AccountId = theTestAccount.Id,
        ContractId = theTestContract.Id,
        Status = 'Draft',
        EffectiveDate = Date.today(),
        Date_Ordered__c = Datetime.now(),
        Pricebook2Id = standardPriceBookId,
        Oracle_Order_Number__c = '1234567891',
        Invoice_Related_Sales_Order__c = '1234567890',
        Customer_PO__c = 'PO0000013',
        On_Hold__c = true,
        RecordTypeId = theOrderNVCreditRecordTypeId
      );

      // Inserting Test Orders.
      insert new List<Order> {
        theTestParentOrder,
        theTestChildOrder
      };

      // Inserting Test Order Items.
      theTestOrderItems = new List<OrderItem>();
      for(Integer index = 0;index < 3;index++) {
        theTestOrderItems.add(new OrderItem(
          PriceBookEntryId = theTestPBE.Id,
          OrderId = theTestParentOrder.Id,
          Quantity = 1,
          UnitPrice = 99,
          Line_Amount__c = 99,
          Tracking_Number__c = '12345',
          Status__c = 'Delivered',
          Shipping_Method__c = 'FDX-Parcel-Next Day 8AM',
          On_Hold__c = true
        ));
      }
      insert theTestOrderItems;

      // Inserting Test Order Invoices.
      theTestOrderInvoices = new List<Order_Invoice__c>();
      for(Integer index = 0;index < 3;index++) {
        theOrderInvoice = NV_TestUtility.createOrderInvoice(
          theTestParentOrder.Id,
          Date.today().addDays(-10),
          'Credit Memo - CM',
          'AR Transaction',
          '1234567890' + index,
          false
        );
        theOrderInvoice.Transaction_Status__c = 'Open';

        theTestOrderInvoices.add(theOrderInvoice);
      }
      insert theTestOrderInvoices;
    }

    // Instantiating Model Class.
    //nv_DailyOrderModel theModelInstance = new nv_DailyOrderModel();
  }
  

  /*
  @method      : validateModelFunctionality
  @description : This method validates model functionality.
  @params      : void
  @returns     : void
  */
    private static testMethod void validateModelFunctionality() {    

    Test.startTest();
        
    //Re-wrote to have Model calls made via static method calls rather than instance.
    //Change was necessary because of change to make the nv_DailyOrderModel class a Controller.
    nv_DailyOrderModel.getContactRecordTypeId();

    // Running Thread As Central Regional Manager.
    List<NV_Wrapper.UserOrders> theUserOrders = null;
    system.runAs(theCentralRegionalManager) {
      // Setting Up Current Day.
      ControlApexExecutionFlow.theCurrentDay = 'Mon';

      // Test Case 1 - Extracting User Today Orders For Monday.
      theUserOrders = nv_DailyOrderModel.getTodayOrders();

      // Test Case 4 - Extracting User Today Orders For Tuesday.
      ControlApexExecutionFlow.theCurrentDay = 'Tue';
      theUserOrders = nv_DailyOrderModel.getTodayOrders();

      
      // Test Case 8 - Extracting User Yesterday Orders For Saturday.
      ControlApexExecutionFlow.theCurrentDay = 'Sat';
      theUserOrders = nv_DailyOrderModel.getYesterdayOrders();

      // Test Case 9 - Extracting User Two Days Old Orders For Wednesday.
      ControlApexExecutionFlow.theCurrentDay = 'Wed';
      theUserOrders = nv_DailyOrderModel.getTwoDayOldOrders();

      // Test Case 10 - Extracting User Two Days Old Orders For Tuesday.
      ControlApexExecutionFlow.theCurrentDay = 'Tue';
      theUserOrders = nv_DailyOrderModel.getTwoDayOldOrders();

      // Test Case 11 - Extracting User Two Days Old Orders For Sunday.
      ControlApexExecutionFlow.theCurrentDay = 'Sun';
      theUserOrders = nv_DailyOrderModel.getTwoDayOldOrders();

      // Updating Test Parent Order.
      theTestParentOrder.Status = 'Closed';
      update theTestParentOrder;

      // Test Case 12 - Extracting User Two Days Old Orders For Saturday.
      ControlApexExecutionFlow.theCurrentDay = 'Sat';
      theUserOrders = nv_DailyOrderModel.getTwoDayOldOrders();
      
    }

    Test.stopTest();
    }
    /*
  @method      : validateModelFunctionality1
  @description : This method validates model functionality.
  @params      : void
  @returns     : void
  */
    private static testMethod void validateModelFunctionality1() {    

    
        
    //Re-wrote to have Model calls made via static method calls rather than instance.
    //Change was necessary because of change to make the nv_DailyOrderModel class a Controller.
    nv_DailyOrderModel.getContactRecordTypeId();

    // Running Thread As Central Regional Manager.
    List<NV_Wrapper.UserOrders> theUserOrders = null;
    system.runAs(theCentralRegionalManager) {
      // Setting Up Current Day.
      ControlApexExecutionFlow.theCurrentDay = 'Mon';

      // Test Case 1 - Extracting User Today Orders For Monday.
      theUserOrders = nv_DailyOrderModel.getTodayOrders();

      // Test Case 2 - Extracting User Today Orders For Sunday.
      ControlApexExecutionFlow.theCurrentDay = 'Sun';
      theUserOrders = nv_DailyOrderModel.getTodayOrders();

      // Test Case 3 - Extracting User Today Orders For Saturday.
      ControlApexExecutionFlow.theCurrentDay = 'Sat';
      theUserOrders = nv_DailyOrderModel.getTodayOrders();

      

      // Test Case 5 - Extracting User Yesterday Orders For Tuesday.
      ControlApexExecutionFlow.theCurrentDay = 'Tue';
      theUserOrders = nv_DailyOrderModel.getYesterdayOrders();
      Test.startTest();
      // Test Case 6 - Extracting User Yesterday Orders For Monday.
      ControlApexExecutionFlow.theCurrentDay = 'Mon';
      theUserOrders = nv_DailyOrderModel.getYesterdayOrders();
        
       
      // Test Case 7 - Extracting User Yesterday Orders For Sunday.
      ControlApexExecutionFlow.theCurrentDay = 'Sun';
      theUserOrders = nv_DailyOrderModel.getYesterdayOrders();
     
      system.assert(
        theUserOrders.size() > 0,
        'Error: The model class failed to fetch logged-in user active orders.'
      );
      
      // Updating Test Parent Order.
      theTestParentOrder.Status = 'Draft';
      update theTestParentOrder;
      
      // Test Case 13 - Extracting User Active Orders.
      theUserOrders = nv_DailyOrderModel.getActiveOrders();
        
      
  
      // Updating Order Item Status.
      theTestOrderItems[0].Status__c = 'Closed';
      theTestOrderItems[0].On_Hold__c = true;
      update theTestOrderItems[0];

      // Test Case 14 - Extracting User Active Orders.
      theUserOrders = nv_DailyOrderModel.getActiveOrders();
       Test.stopTest();
      system.assert(
        theUserOrders.size() > 0,
        'Error: The model class failed to fetch logged-in user active orders.'
      );
      
          }

    
    }
    
}