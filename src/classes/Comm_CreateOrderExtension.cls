/**================================================================      
* Appirio, Inc
* Name: Comm_CreateOrderExtension
* Description: This page is used to create new order from associated case or contact.
* Created Date: 08/05/2016
* Created By: Sahil Batra
* 
* 05 Aug 2016        Sahil Batra         Class creation. T-499531
* 13 Sep 2016        Isha Shukla         Modified(T-499494) added logic to prepopulate values on Custom Fields on Orders when New Order created from Contact detail page.
* 22 Sep 2016        Isha Shukla         Modified(I-235190) added logic to prepopulate values on Custom Fields on Orders when New Order created from Account detail page.  
* 13 Oct 2016        Isha Shukla         Modified(I-239597) added logic to populate Billing and Shipping address when new order created from contact record, modified method initCurrentOrder()
* 09 Nov 2016        Varun Vasishtha     Modified(I-242109) added logic to populate Billing and Shipping address when new order created from Account record, modified method initCurrentOrder()
==================================================================*/
public with sharing class Comm_CreateOrderExtension {
    // Public variables used on VF page.
    //public Order currentOrder{get;set;}
    public boolean billtoContact;
    public boolean shiptoContact;
    public boolean billtoShiptoContact;
    // Private Variables.
    public Id caseId;
    public Id contactId;
    private Id orderId;
    private Id accountId;
    private Id recordTypeId;
    private Case caseRecord;
    private Contact contactRecord;
    private Account accountRecord;
    private Order orderRec;
    private Id tempRecordType;
    private String entCase;
    
    //-----------------------------------------------------------------------------------------------
    //  Cosntructor
    //-----------------------------------------------------------------------------------------------
    public Comm_CreateOrderExtension(ApexPages.StandardController stdController){
        recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        caseId = ApexPages.currentPage().getParameters().get('case_id');
        contactId = ApexPages.currentPage().getParameters().get('contact_id');
        orderId = ApexPages.currentPage().getParameters().get('order_id');
        accountId = ApexPages.currentPage().getParameters().get('account_id');
        tempRecordType = ApexPages.currentPage().getParameters().get('RecordType');
        System.debug('>>>>'+tempRecordType);
        entCase = ApexPages.currentPage().getParameters().get('entCase');
        caseRecord = new Case();
        orderRec = new Order();
        contactRecord = new Contact();
        accountRecord = new Account();
        if(caseId!=null) {
            caseRecord = [SELECT CaseNumber,AccountId,ContactId,Contact.Name,Account.Billing_Type__c,Account.Name FROM Case WHERE id =:caseId];
        }
        if(orderId != null) {
            orderRec = [SELECT AccountId,Account.Name,Contact__c,OrderNumber,PoNumber FROM Order WHERE Id=:orderId];
        }
        if(contactId != null) {
            contactRecord = [SELECT Name,AccountId,Account.Billing_Type__c,Account.Name,Account.COMM_Shipping_Address__c,
                             Account.COMM_Shipping_City__c,Account.COMM_Shipping_State_Province__c,Account.COMM_Shipping_PostalCode__c,Account.COMM_Shipping_Country__c,
                             Account.COMM_Billing_Address__c,Account.COMM_Billing_City__c,Account.COMM_Billing_State_Province__c,Account.COMM_Billing_PostalCode__c,Account.COMM_Billing_Country__c
                             FROM Contact WHERE Id = :contactId];
        }
        if(accountId != null) {
            accountRecord = [SELECT Id,Billing_Type__c,Name,COMM_Shipping_Address__c,COMM_Shipping_City__c,COMM_Shipping_State_Province__c,
            COMM_Shipping_PostalCode__c,COMM_Shipping_Country__c,COMM_Billing_Address__c,COMM_Billing_City__c,COMM_Billing_State_Province__c,COMM_Billing_PostalCode__c,
            COMM_Billing_Country__c FROM Account WHERE Id = :accountId];
        }
        System.debug('contactRecord+'+contactRecord);
        System.debug('accountRecord+'+accountRecord);
        //currentOrder = new Order();
        
    }
    //-----------------------------------------------------------------------------------------------
    //  Initialize Order record
    //-----------------------------------------------------------------------------------------------	 
    public PageReference initCurrentOrder(){
        PageReference pg = null; 
        Map<String,State_and_Country__c> countryAndState = State_and_Country__c.getAll();
        // create Case when redirected from Order detail page
        if(contactId != null) {     
            billtoContact = false;
            shiptoContact = false;
            billtoShiptoContact = false;
            if(contactRecord.Account.Billing_Type__c !=null && contactRecord.Account.Billing_Type__c !=''){
                if(contactRecord.Account.Billing_Type__c == 'Bill To Address Only'){
                    billtoContact = true;
                }
                if(contactRecord.Account.Billing_Type__c == 'Ship To Address Only'){
                    shiptoContact = true;
                }
                if(contactRecord.Account.Billing_Type__c == 'Bill To and Ship To Address'){
                    billtoShiptoContact = true;
                }
            }
            COMMBPConfigurations__c cs = COMMBPConfigurations__c.getOrgDefaults();
            String shipToLoc = cs.Ship_To_Location__c;
            String billToLoc = cs.Bill_to_Location__c;
            String caseNumber = cs.Case_On_Order__c;
            String contact = cs.Contact_On_Order__c;
            String countryCode = '';
            String stateCode = '';
            String countryCodeBilling = '';
            String stateCodeBilling = '';
            if(countryAndState != null) {
                if(contactRecord.Account.COMM_Shipping_Country__c != '' && contactRecord.Account.COMM_Shipping_Country__c != null && countryAndState.containsKey(contactRecord.Account.COMM_Shipping_Country__c) && countryAndState.get(contactRecord.Account.COMM_Shipping_Country__c).SFDC_Country_Code__c != null) {
                    countryCode = countryAndState.get(contactRecord.Account.COMM_Shipping_Country__c).SFDC_Country_Code__c;
                    
                }
                for(State_and_Country__c state : countryAndState.values()) {
                    system.debug('contactRecord.Account.COMM_Shipping_State_Province__c'+contactRecord.Account.COMM_Shipping_State_Province__c);
                    if(contactRecord.Account.COMM_Shipping_State_Province__c != '' && contactRecord.Account.COMM_Shipping_State_Province__c != null && state.SFDC_State__c != null && state.SFDC_State__c.containsIgnoreCase(contactRecord.Account.COMM_Shipping_State_Province__c)) {
                        stateCode = state.SFDC_State_Code__c;
                        system.debug('countryCode'+stateCode);
                    }
                }
                if(contactRecord.Account.COMM_Billing_Country__c != '' && contactRecord.Account.COMM_Billing_Country__c != null && countryAndState.containsKey(contactRecord.Account.COMM_Billing_Country__c) && countryAndState.get(contactRecord.Account.COMM_Billing_Country__c).SFDC_Country_Code__c != null) {
                    countryCodeBilling = countryAndState.get(contactRecord.Account.COMM_Billing_Country__c).SFDC_Country_Code__c;
                }
                for(State_and_Country__c state : countryAndState.values()) {
                    if(contactRecord.Account.COMM_Billing_State_Province__c != '' && contactRecord.Account.COMM_Billing_State_Province__c != null && state.SFDC_State__c != null && state.SFDC_State__c.containsIgnoreCase(contactRecord.Account.COMM_Billing_State_Province__c)) {
                        stateCodeBilling = state.SFDC_State_Code__c;
                    }
                }
            }
            String url = '/801/e?RecordType='+recordTypeId+'&'+contact+'_lkid='+contactId+'&'+contact+'='+contactRecord.Name+'&accid_lkid='+contactRecord.AccountId;
            if(shiptoContact){
                url = '/801/e?RecordType='+recordTypeId+'&'+contact+'_lkid='+contactId+'&'+contact+'='+contactRecord.Name+'&accid_lkid='+contactRecord.AccountId+'&'+shipToLoc+'_lkid='+contactRecord.AccountId+'&'+shipToLoc+'='+contactRecord.Account.Name;
                
            }
            else if(billtoContact){
                url = '/801/e?RecordType='+recordTypeId+'&'+contact+'_lkid='+contactId+'&'+contact+'='+contactRecord.Name+'&accid_lkid='+contactRecord.AccountId+'&'+billToLoc+'_lkid='+contactRecord.AccountId+'&'+billToLoc+'='+contactRecord.Account.Name;
                
            }
            else if(billtoShiptoContact){
                url = '/801/e?RecordType='+recordTypeId+'&'+contact+'_lkid='+contactId+'&'+contact+'='+contactRecord.Name+'&accid_lkid='+contactRecord.AccountId+'&'+billToLoc+'_lkid='+contactRecord.AccountId+'&'+billToLoc+'='+contactRecord.Account.Name+'&'+shipToLoc+'_lkid='+contactRecord.AccountId+'&'+shipToLoc+'='+contactRecord.Account.Name;
                
            }
            if(stateCode != '' && stateCode != null) {
                url = url +'&ShippingAddressstate='+stateCode;
            }
            if(countryCode != '' && countryCode != null) {
                url = url +'&ShippingAddresscountry='+countryCode;
            }
            if(stateCodeBilling != '' && stateCodeBilling != null) {
                url = url +'&BillingAddressstate='+stateCodeBilling;
            }
            if(countryCodeBilling != '' && countryCodeBilling != null) {
                url = url +'&BillingAddresscountry='+countryCodeBilling;
            }
            if(contactRecord.Account.COMM_Billing_PostalCode__c != null) {
                url = url +'&BillingAddresszip='+contactRecord.Account.COMM_Billing_PostalCode__c;
            }
            if(contactRecord.Account.COMM_Billing_City__c != null) {
                url = url +'&BillingAddresscity='+contactRecord.Account.COMM_Billing_City__c;
            }
            if(contactRecord.Account.COMM_Billing_Address__c != null) {
                url = url +'&BillingAddressstreet='+contactRecord.Account.COMM_Billing_Address__c;
            }
            if(contactRecord.Account.COMM_Shipping_PostalCode__c != null) {
                url = url +'&ShippingAddresszip='+contactRecord.Account.COMM_Shipping_PostalCode__c;
            }
            if(contactRecord.Account.COMM_Shipping_Address__c != null) {
                url = url +'&ShippingAddressstreet='+contactRecord.Account.COMM_Shipping_Address__c;
            }
            if(contactRecord.Account.COMM_Shipping_City__c != null) {
                url = url +'&ShippingAddresscity='+contactRecord.Account.COMM_Shipping_City__c;
            }
            url = url + '&retURL=/'+contactId;
            pg = new PageReference(url);
            pg.setRedirect(true);
            // create Order when redirected from Case detail page     
        } 
        else if(caseId != null) {
            billtoContact = false;
            shiptoContact = false;
            billtoShiptoContact = false;
            //	currentOrder.RecordTypeId = recordTypeId;
            //	currentOrder.EffectiveDate = System.Today();
            //	currentOrder.Status = 'Draft';
            //	currentOrder.ownerId = UserInfo.getUserId();
            //	if(caseRecord.ContactId != null){
            //		 currentOrder.Contact__c = caseRecord.ContactId;
            //	}
            //	if(caseRecord.AccountId != null){
            //		currentOrder.AccountId = caseRecord.AccountId;
            //	}
            if(caseRecord.Account.Billing_Type__c !=null && caseRecord.Account.Billing_Type__c !=''){
                if(caseRecord.Account.Billing_Type__c == 'Bill To Address Only'){
                    billtoContact = true;
                }
                if(caseRecord.Account.Billing_Type__c == 'Ship To Address Only'){
                    shiptoContact = true;
                }
                if(caseRecord.Account.Billing_Type__c == 'Bill To and Ship To Address'){
                    billtoShiptoContact = true;
                }
            }
            COMMBPConfigurations__c cs = COMMBPConfigurations__c.getOrgDefaults();
            String shipToLoc = cs.Ship_To_Location__c;
            String billToLoc = cs.Bill_to_Location__c;
            String caseNumber = cs.Case_On_Order__c;
            String contact = cs.Contact_On_Order__c;
            String url = '/801/e?RecordType='+recordTypeId+'&'+caseNumber+'_lkid='+caseId+'&'+caseNumber+'='+caseRecord.CaseNumber+'&accid_lkid='+caseRecord.AccountId;
            if(shiptoContact){
                url = '/801/e?RecordType='+recordTypeId+'&'+caseNumber+'_lkid='+caseId+'&'+caseNumber+'='+caseRecord.CaseNumber+'&accid_lkid='+caseRecord.AccountId+'&'+shipToLoc+'_lkid='+caseRecord.AccountId+'&'+shipToLoc+'='+caseRecord.Account.Name;
            }
            else if(billtoContact){
                url = '/801/e?RecordType='+recordTypeId+'&'+caseNumber+'_lkid='+caseId+'&'+caseNumber+'='+caseRecord.CaseNumber+'&accid_lkid='+caseRecord.AccountId+'&'+billToLoc+'_lkid='+caseRecord.AccountId+'&'+billToLoc+'='+caseRecord.Account.Name;
            }
            else if(billtoShiptoContact){
                url = '/801/e?RecordType='+recordTypeId+'&'+caseNumber+'_lkid='+caseId+'&'+caseNumber+'='+caseRecord.CaseNumber+'&accid_lkid='+caseRecord.AccountId+'&'+billToLoc+'_lkid='+caseRecord.AccountId+'&'+billToLoc+'='+caseRecord.Account.Name+'&'+shipToLoc+'_lkid='+caseRecord.AccountId+'&'+shipToLoc+'='+caseRecord.Account.Name;
            }
            if(caseRecord.ContactId != null){
                url = url + '&'+contact+'_lkid='+caseRecord.ContactId+'&'+contact+'='+caseRecord.Contact.Name+'&retURL=/'+caseId;
            }
            else{
                url = url + '&retURL=/'+caseId;
            }
            pg = new PageReference(url);
            pg.setRedirect(true);
            // create Case when redirected from Order detail page
        } 
        else if(orderId != null) {
            COMMBPConfigurations__c cs = COMMBPConfigurations__c.getOrgDefaults();
            String orderOncase = cs.Order_On_Case__c;
            String POonCase = cs.PO_On_Case__c;
            //PageReference pg = null;
            String url = '/setup/ui/recordtypeselect.jsp?ent=Case&def_account_id='+orderRec.accountId+'&'+orderOncase+'_lkid='+orderId+'&'+orderOncase+'='+orderRec.orderNumber+'&retURL=/'+orderId+'&save_new_url=/500/e?retURL=/'+orderId+'&def_account_id='+orderRec.accountId+'&'+orderOncase+'_lkid='+orderId+'&'+orderOncase+'='+orderRec.orderNumber;
            if(tempRecordType == null){
                url = '/setup/ui/recordtypeselect.jsp?ent=Case&def_account_id='+orderRec.accountId+'&'+orderOncase+'_lkid='+orderId+'&'+orderOncase+'='+orderRec.orderNumber+'&retURL=/'+orderId+'&save_new_url=/500/e?retURL=/'+orderId+'&def_account_id='+orderRec.accountId+'&'+orderOncase+'_lkid='+orderId+'&'+orderOncase+'='+orderRec.orderNumber;
            }
            else{
                url = '/500/e?def_account_id='+orderRec.accountId+'&'+orderOncase+'_lkid='+orderId+'&'+orderOncase+'='+orderRec.orderNumber+'&retURL=/'+orderId+'&ent=Case&RecordType='+tempRecordType;
            }
            if(orderRec.PoNumber != null) {
                url = url + '&'+POonCase+'='+orderRec.PoNumber;
            }
            if(orderRec.Contact__c != null){
                url = url + '&cas3_lkid='+orderRec.Contact__c;
            }
            else{
                url = url + '&retURL=/'+caseId;
            }
            pg = new PageReference(url);
            pg.setRedirect(true);
            //return pg;
            
            // create Order when redirected from Account detail page  
        } 
        else if(accountId != null) {  
            billtoContact = false;
            shiptoContact = false;
            billtoShiptoContact = false;
            if(accountRecord.Billing_Type__c !=null && accountRecord.Billing_Type__c !=''){
                if(accountRecord.Billing_Type__c == 'Bill To Address Only'){
                    billtoContact = true;
                }
                if(accountRecord.Billing_Type__c == 'Ship To Address Only'){
                    shiptoContact = true;
                }
                if(accountRecord.Billing_Type__c == 'Bill To and Ship To Address'){
                    billtoShiptoContact = true;
                }
            }
            COMMBPConfigurations__c cs = COMMBPConfigurations__c.getOrgDefaults();
            String shipToLoc = cs.Ship_To_Location__c;
            String billToLoc = cs.Bill_to_Location__c;
            //String caseNumber = cs.Case_On_Order__c;
            //String contact = cs.Contact_On_Order__c;
            String url = '/801/e?RecordType='+recordTypeId+'&aid='+accountId;
            if(shiptoContact){
                url = '/801/e?RecordType='+recordTypeId+'&aid='+accountId+'&'+shipToLoc+'_lkid='+accountRecord.Id+'&'+shipToLoc+'='+accountRecord.Name;
            }
            else if(billtoContact){
                url = '/801/e?RecordType='+recordTypeId+'&aid='+accountId+'&'+billToLoc+'_lkid='+accountRecord.Id+'&'+billToLoc+'='+accountRecord.Name;
            }
            else if(billtoShiptoContact){
                url = '/801/e?RecordType='+recordTypeId+'&aid='+accountId+'&'+billToLoc+'_lkid='+accountRecord.Id+'&'+billToLoc+'='+accountRecord.Name+'&'+shipToLoc+'_lkid='+accountRecord.Id+'&'+shipToLoc+'='+accountRecord.Name;
            }
            String countryCode = '';
            String stateCode = '';
            String countryCodeBilling = '';
            String stateCodeBilling = '';
            if(countryAndState != null) {
                if(accountRecord.COMM_Shipping_Country__c != '' && accountRecord.COMM_Shipping_Country__c != null && countryAndState.containsKey(accountRecord.COMM_Shipping_Country__c) && countryAndState.get(accountRecord.COMM_Shipping_Country__c).SFDC_Country_Code__c != null) {
                    countryCode = countryAndState.get(accountRecord.COMM_Shipping_Country__c).SFDC_Country_Code__c;
                    
                }
                for(State_and_Country__c state : countryAndState.values()) {
                    if(accountRecord.COMM_Shipping_State_Province__c != '' && accountRecord.COMM_Shipping_State_Province__c != null && state.SFDC_State__c != null && state.SFDC_State__c.containsIgnoreCase(accountRecord.COMM_Shipping_State_Province__c)) {
                        stateCode = state.SFDC_State_Code__c;
                    }
                }
                if(accountRecord.COMM_Billing_Country__c != '' && accountRecord.COMM_Billing_Country__c != null && countryAndState.containsKey(accountRecord.COMM_Billing_Country__c) && countryAndState.get(accountRecord.COMM_Billing_Country__c).SFDC_Country_Code__c != null) {
                    countryCodeBilling = countryAndState.get(accountRecord.COMM_Billing_Country__c).SFDC_Country_Code__c;
                }
                for(State_and_Country__c state : countryAndState.values()) {
                    if(accountRecord.COMM_Billing_State_Province__c != '' && accountRecord.COMM_Billing_State_Province__c != null && state.SFDC_State__c != null && state.SFDC_State__c.containsIgnoreCase(accountRecord.COMM_Billing_State_Province__c)) {
                        stateCodeBilling = state.SFDC_State_Code__c;
                    }
                }
            }
            if(stateCode != '' && stateCode != null) {
                url = url +'&ShippingAddressstate='+stateCode;
            }
            if(countryCode != '' && countryCode != null) {
                url = url +'&ShippingAddresscountry='+countryCode;
            }
            if(stateCodeBilling != '' && stateCodeBilling != null) {
                url = url +'&BillingAddressstate='+stateCodeBilling;
            }
            if(countryCodeBilling != '' && countryCodeBilling != null) {
                url = url +'&BillingAddresscountry='+countryCodeBilling;
            }
            if(accountRecord.COMM_Billing_PostalCode__c != null) {
                url = url +'&BillingAddresszip='+accountRecord.COMM_Billing_PostalCode__c;
            }
            if(accountRecord.COMM_Billing_City__c != null) {
                url = url +'&BillingAddresscity='+accountRecord.COMM_Billing_City__c;
            }
            if(accountRecord.COMM_Billing_Address__c != null) {
                url = url +'&BillingAddressstreet='+accountRecord.COMM_Billing_Address__c;
            }
            if(accountRecord.COMM_Shipping_PostalCode__c != null) {
                url = url +'&ShippingAddresszip='+accountRecord.COMM_Shipping_PostalCode__c;
            }
            if(accountRecord.COMM_Shipping_Address__c != null) {
                url = url +'&ShippingAddressstreet='+accountRecord.COMM_Shipping_Address__c;
            }
            if(accountRecord.COMM_Shipping_City__c != null) {
                url = url +'&ShippingAddresscity='+accountRecord.COMM_Shipping_City__c;
            }

            url = url + '&retURL=/'+accountId;
            pg = new PageReference(url);
            pg.setRedirect(true);   
        } 
        
        return pg;	
        
    }
    //-----------------------------------------------------------------------------------------------
    //  Save Order : Insert Junction record if order insert is successful
    //-----------------------------------------------------------------------------------------------
    /*   public PageReference save(){
List<Order> orderList = new List<Order>();
orderList.add(currentOrder);
boolean insertSuccess = false;
Database.SaveResult[] results = Database.insert(orderList,false);
if(results != null){
for(Database.SaveResult result : results) {
if(result.isSuccess()){
// Insert in loop as only one record will be inserted at a time
Case_Order_Junction__c obj = new Case_Order_Junction__c();
obj.Case__c = caseId;
obj.Order__c = result.getId();
insert obj;
insertSuccess = true;
}
}
}
if(insertSuccess){
PageReference pg = new PageReference('/' +caseId);
pg.setRedirect(true);
return pg;
}
else{
PageReference pg = new PageReference('/apex/Comm_CreateOrder?case_id='+caseId);
return pg;
}
}
//-----------------------------------------------------------------------------------------------
//  Cancel Order 
//-----------------------------------------------------------------------------------------------
public PageReference cancel(){
PageReference pg = new PageReference('/' +caseId);
pg.setRedirect(true);
return pg;
} */
}