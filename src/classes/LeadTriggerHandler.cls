/*******************************************************************
Name  :  InstrumentSmartContactSearchExtension
Author:  Appirio India (Sahil Batra)
Date  :  Feb 5, 2016

Added: Sahil Batra Feb 05, 2016 (I-201491)
*************************************************************************/

public with sharing class LeadTriggerHandler {
	public static void preventDelete(List<Lead> leadList){
		for(Lead lead : leadList){
			lead.addError('You are not authorized to delete this Lead');
		}
	}
}