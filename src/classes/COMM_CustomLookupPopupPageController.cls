// 
// (c) 2016 Appirio, Inc.
//
// Name : COMM_CustomLookupPopupPageController
// It controls the COMM_CustomLookupPopupPage VF page and showing details for contact
//
// 18th March 2016     Kirti Agarwal      Original(T-483770)
//
// Modified Date        Modified By     Purpose
//25th May 2016         Meghna Vijay    To populate address records of account on look up field on  site visit object T-502656
//
public with sharing class COMM_CustomLookupPopupPageController{

  public List<Sobject> listRecords {get;set;}
  public List<String> listFieldNames {get; set;}
  public String selectedObject;
  public String fieldName;
  public String searchText{get;set;}
  public String fieldSetName;
  public Boolean isContactPage{get;set;}
  public Contact contactRecord{get;set;}  
  public Boolean isAddressPage{get;set;}
  public String oppId{get;set;}
  //constructor
  public COMM_CustomLookupPopupPageController() {
    selectedObject = ApexPages.currentPage().getParameters().get('Object');
    fieldName = ApexPages.currentPage().getParameters().get('fieldName');
    searchText = ApexPages.currentPage().getParameters().get('searchText');
    fieldSetName = ApexPages.currentPage().getParameters().get('fieldSetName');
    oppId = ApexPages.currentPage().getParameters().get('oppRecId');
    
    contactRecord = new Contact();
    contactRecord.OwnerId = UserInfo.getUserId();
    contactRecord.RecordTypeId = Constants.getRecordTypeId('COMM Contact', 'Contact');
    List < SObject > listRecords = new List < SObject > ();
    runSearch();
  }

  /********************************************************************************************************************
   * Method to run the search with parameters passed via Page Parameters
   *********************************************************************************************************************/
  public PageReference runSearch() {
    String fieldsToFetch = getFieldsForSelectedObject();
    String whereClause = '';
    //To populate address records of account on site visit object (T-502656) 
    if(selectedObject.contains('Address__c')) {
      isAddressPage=true;
      Opportunity opp=[Select AccountId from Opportunity Where Id=:oppId];
      String acctId=opp.AccountId;
      String searchString='Select Full_Address__c, Account__c' + ' From '  + selectedObject;
      whereClause=' WHERE Account__c =:acctId';
      //system.assert(false,'searchString '+searchString+'whereClause'+whereClause);
      if (searchText != '' && searchText != null) {
        whereClause = whereClause + ' AND Full_Address__c LIKE \'%' + String.escapeSingleQuotes(searchText) + '%\'';
      }
      
      listRecords = Database.query(searchString + whereClause + ' order by Name  limit 50');
    }
    if(fieldsToFetch != null && fieldsToFetch != '') {
      String searchString = 'Select ' + fieldsToFetch + ' From ' + selectedObject;
      if (searchText != '' && searchText != null) {
        whereClause = ' Where ' + fieldName + ' LIKE \'%' + String.escapeSingleQuotes(searchText) + '%\'';
      }
       listRecords = Database.query(searchString + whereClause + ' order by Name  limit 50');
       
    }
    return null;
  }

  /********************************************************************************************************************
   * Method to Get all Fields of the selected object
   *********************************************************************************************************************/
  public String getFieldsForSelectedObject() {
    listFieldNames = new List < String > ();
    List < Schema.FieldSetMember > fieldSetMemberList = new List < Schema.FieldSetMember > ();
    String fieldsToFetch = '';
    
    try {
      if (selectedObject != null && selectedObject != '' && selectedObject != 'Address__c') {
        Map < String, Schema.SObjectType > GlobalDescribeMap = Schema.getGlobalDescribe();
        
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(selectedObject);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
         if (DescribeSObjectResultObj.FieldSets.getMap().ContainsKey(fieldSetName)) {
          Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
          fieldSetMemberList = fieldSetObj.getFields();
        }

        for (Schema.FieldSetMember fieldSetMemberObj: fieldSetMemberList) {
          listFieldNames.add(string.ValueOf(fieldSetMemberObj.getFieldPath()));
        
        }
      } else {
        listFieldNames.add('Full_Address__c');
      }

      //Building Query with the fields
      Integer i = 0;
      Integer len = listFieldNames.size();
      for (String temp: listFieldNames) {
        if (i == len - 1) {
          fieldsToFetch = fieldsToFetch + temp;
          
        } else {
          fieldsToFetch = fieldsToFetch + temp + ',';
          
        }
        i++;
      }
    } catch (Exception ex) {
      apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,
        'There is no Field for selected Object!'));
    }
    return fieldsToFetch;
  }

  //================================================================      
  // Name: createContact
  // Description: Used to show contact page
  // Created By: Kirti Agarwal (Appirio)
  //==================================================================
  public void createContact() {
    isContactPage = true;
    //System.assert(false,'Iam here in create contact');
  }

  //================================================================      
  // Name: saveContact
  // Description: Used to save contact for comm record type
  // Created By: Kirti Agarwal (Appirio)
  //==================================================================
  public void saveContact() {
    try {
      insert contactRecord;
    } catch (exception e) {
      if(e.getMessage().contains('DUPLICATES_DETECTED')) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Contact_Duplicate_Records_Error));
      }
    }
  }
  
  
}