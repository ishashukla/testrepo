//
// (c) Appirio, Inc.
//
//  Batch class to prepare the String which will be read by Informatica for BOTW integration.
//
// August 13, 2015 Sunil & Naresh Original
// Revision History:
//     2015-08-13   Sunil/Naresh   Original
//     2015-08-13   H.Whitacre     Updates to clear omitted sections
//     2015-08-17   H.Whitacre     Combine strings to String2, add integration timestamp
//     2015-08-18   H.Whitacre     Apply .trim to address lines to remove \r if exist due to CrLf
//     2015-08-19   H.Whitacre     Add more fields mapped from T-428108, T-428109
//     2015-08-24   Sunil          Modify batch to make stateful and calling Contract Line batch from this batch.
//     2015-08-26   H.Whitacre     Remove the final backslash so that Informatica can use slash as delimiter.
//     2015-09-09   H.Whitacre     Many fixes to resolve BOTW testing issues (T-433224).
//     2015-09-11   H.Whitacre     Logic to strip backslash from any data value (T-433745).
//     2015-09-24   H.Whitacre     Logic to compute Term (#63) according to BOTW technique (T-435168).
//     2016-02-09   H.Whitacre     I-201746: Fix several issues found during BOTW testing.
//     2016-02-11   H.Whitacre     I-201746: Fix logic for isPmtInAdvance and BOTW Term calculation.
//
// Use below code to execute this batch one time (for all contracts that meet normal criteria):
//    Database.executeBatch(new BatchBOTWIntegration_FlexContract());
//
// Use below code to execute this batch one time (to process specific set of Contract Ids):
//    List<string> myList = new List<string>{'a0Oe0000005kJs4EAE', 'a0Oe0000005kJs5EAE', ...};
//    Database.executeBatch(new BatchBOTWIntegration_FlexContract(myList));

global class Flex_BatchBOTWIntegrationContract implements Database.Batchable<SObject>, System.Schedulable, Database.Stateful {
  global String query =  ' Select  Term__c,'                           //CONTRACT.TERM
	                          +  ' Payment_Amt__c,'                    //CONTRACT.PYMT
	                          +  ' Misc_Taxable__c,'                   //MISC.TAXABLE
	                          +  ' Misc_Billable_Description__c,'      //MISC.DESC
	                          +  ' Misc_Billable_Amount__c,'           //MISC.AMT
	                          +  ' Lender_Contract_Nbr__c,'            //UM.ALPHA.FIELD5
	                          +  ' Legal_Name__c,'                     //CD.NAME, CD.AR.NAME, AR.NAME, CUST.NAME
	                          +  ' Lease_Type__c,'                     //LEASE.TYPE, INCOME.METHOD, RESID.METHOD
	                          +  ' LPO_Nbr__c,'                        //UM.ALPHA.FIELD6
	                          +  ' LC_Begin_Date__c,'                  //LC.BEGIN.DATE
	                          +  ' Invoice_Description__c,'            //INVOICE.DESC
	                          +  ' Invoice_Code__c,'                   //INVOICE.CODE
                              +  ' Invoice_Lead_Days__c,'              //INV.DAYS
	                          +  ' Flex_Account_Number__c,'            //CONTRACT.NUM
	                          +  ' First_Payment_Due_Date__c,'         //INV.DUE.DAY, FIRST.PYMT.DATE, FIRST.MISC.DATE
	                          +  ' Customer_PO_Nbr__c,'                //PUR.ORDER.NUM
	                          +  ' Commence_Date__c,'                  //BOOKING.DATE, ACTIV.DATE, INCOME.START.DATE
	                          +  ' BOTW_Lender_Code__c,'               //UM.TABLE1
	                          +  ' Account__c,'
	                          +  ' Account__r.Name,'                   //CD.DBA, CUST.SHORT.NAME, CUST.DBA
	                          +  ' Account__r.ShippingStateCode,'      //CD.STATE, CUST.STATE
	                          +  ' Account__r.ShippingPostalCode,'     //CD.ZIP, CUST.ZIP
	                          +  ' Account__r.ShippingCity,'           //CD.CITY, CUST.CITY
	                          +  ' Account__r.ShippingStreet,'         //CD.ADDRESS1, CD.ADDRESS2, CUST.ADDRESS1, CUST.ADDRESS2
	                          +  ' Account__r.BillingStateCode,'       //CD.AR.STATE, AR.STATE
	                          +  ' Account__r.BillingPostalCode,'      //CD.AR.ZIP, AR.ZIP
	                          +  ' Account__r.BillingCity,'            //CD.AR.CITY, AR.CITY
	                          +  ' Account__r.BillingStreet,'          //CD.AR.ADDRESS1, CD.AR.ADDRESS2, AR.ADDRESS1, AR.ADDRESS2
	                          +  ' End_of_Term__c,'                    //PUR.OPTION
                              +  ' Sum_of_Payments__c,'                //GROSS.CONTRACT
                              +  ' Equipment_Funded_Amt__c,'           //GROSS.FINANCE
                              +  ' Freight_Funded_Amt__c,'             //GROSS.FINANCE
                              +  ' Service_Funded_Amt__c,'             //GROSS.FINANCE
                              +  ' First_Payment_Due__c,'              //PYMTS.ADV, PYMTS.ARREARS, BILLING.CYCLE, MISC.CYCLE
                              +  ' Payment_Frequency__c,'              //PYMTS.ADV, PYMTS.ARREARS, BILLING.CYCLE, MISC.CYCLE
                              +  ' Payment_Structure__c,'              //VARIABLE.PYMT.CODE
                              +  ' First_Payment_Amount__c,'           //FIRST.PYMT.AMT
                              +  ' Opportunity_Number__c,'             //UM.ALPHA.FIELD4
                              +  ' BOTW_Irreg_Pmt_Sequence_Date__c,'   //VARIABLE.DATE
                              +  ' BOTW_Irreg_Pmt_Sequence_Rate__c,'   //VARIABLE.RATE
	                          +  '(Select Miscellaneous_Invoiced_Type__c, Other_Misc_Inv_Type_Description__c,'
	                             +  'Miscellaneous_Invoiced_Amount__c, Taxable__c, BOTW_GL_Code__c '
	                             +  'From Flex_Misc_Invoiced_Items__r)' 
	                          +  ' From Flex_Contract__c';

  private final String  SECTIONCU   = 'CU',
                        SECTIONRP   = 'RP',
                        SECTIONCO   = 'CO',
                        SECTIONCBL  = 'CBL',
                        SECTIONCI   = 'CI',
                        SECTIONMB   = 'MB',
                        SECTIONMI   = 'MI',
                        SECTIONRN   = 'RN';
  private final Map<String,Integer> mapPaymentFreqPeriod = createMapPaymentFreqPeriod();

  // This variable will be statefull and used to retain values throughout all chunks of this batch.
  global Set<Id>executedContractIds = new Set<Id>();

  private String result = '';
  private list<String> listFlexContactIds = new list<String>();

  // Variables whose value affects logic across multiple parts of the file
  private boolean billingCycleIsEven = true;
  private String billingCycleString = '';
  
  public Flex_BatchBOTWIntegrationContract(){
  	System.debug('In Blank Consss');
  }

  public Flex_BatchBOTWIntegrationContract(List<String> ids){
  	System.debug('In List Param Consss');
    if(ids != null && ids.size() > 0)
      listFlexContactIds = ids;
  }

  global Database.QueryLocator start(Database.BatchableContext bc){
  	if(listFlexContactIds != null && listFlexContactIds.size() > 0)
      query  +=  ' WHERE id in : listFlexContactIds';
    else
      query  +=  ' WHERE BOTW_Integration_Status__c = null AND Status__c = \'Active\' AND Service_By_BOTW__c = true';
  	System.debug('Queryyyyyyyyyy=====' + query);
  	System.debug('Database.getQueryLocator(query)====='+Database.getQueryLocator(query));
    return Database.getQueryLocator(query);
  }

  // Execute block Query locator will send result to this method.
  global void execute(Database.BatchableContext bc, List<sObject> scope) {
    List<Flex_Contract__c> lstContracts  = (List<Flex_Contract__c>)scope;
    System.debug('@@@size' + lstContracts.size());

    // Build the BOTW Integration strings for each contract in the scope
    for(Flex_Contract__c objContract :lstContracts){
      executedContractIds.add(objContract.Id);
      objContract.BOTW_Integration_String1__c = calculateString1(objContract);
      objContract.BOTW_Integration_String2__c = calculateString2(objContract);
      objContract.BOTW_Integration_Status__c = 'String Ready';
      objContract.BOTW_Integration_Timestamp__c = System.Datetime.now();
    }
    Database.update(lstContracts, false);
  }


  // Finish block - will be executed when batch process is finished.
  // This block is calling another batch to process contract line items for only selected Contracts Ids.
  global void finish(Database.BatchableContext bc) {
  	System.debug('@@@###' + executedContractIds);
    Flex_BatchBOTWIntegrationContractLine objContractLineBatch = new Flex_BatchBOTWIntegrationContractLine();
    objContractLineBatch.executedContractIds = executedContractIds;
    Database.executeBatch(objContractLineBatch);
    System.debug('@@@###end');
  }


  // Schedular for batch.
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
      Id batchId = Database.executeBatch(new Flex_BatchBOTWIntegrationContract(), 1);
      system.debug('@@@batchId = ' + batchId);
    }
  }

  // Helper method to calculate String.
  private String calculateString1(Flex_Contract__c line){
    result = '';

    // Prepare the ShippingStreet for later usage  //(used for #4, #5, #50, #51)
    String[] ShippingStreet = String.isBlank(line.Account__r.ShippingStreet)? new String[] {''}:line.Account__r.ShippingStreet.split('\n');
    if(ShippingStreet.size() < 2){
      ShippingStreet.add('');
    }
    ShippingStreet[0] = ShippingStreet[0].trim();
    ShippingStreet[1] = ShippingStreet[1].trim();
    
    // Prepare the BillingStreet for later usage  //(used for #13, #14, #43, #44)
    String[] BillingStreet = String.isBlank(line.Account__r.BillingStreet)? new String[] {''}:line.Account__r.BillingStreet.split('\n');
    if(BillingStreet.size() < 2){
      BillingStreet.add('');
    }
    BillingStreet[0] = BillingStreet[0].trim();
    BillingStreet[1] = BillingStreet[1].trim();

    // Determine whether payments are in advance or arrears  //(used for #63, #69, #71)      
    boolean pmtsAreInAdvance = (line.First_Payment_Due__c=='In Advance');

    // Prepare Billing Cycle info for later usage  //(used for #75, #82, #146, #147, #253)
    billingCycleIsEven = (line.Payment_Structure__c == 'Even Payments' || line.Payment_Structure__c == null);
    billingCycleString = calculateBillingCycleString(line.First_Payment_Due_Date__c , line.Payment_Frequency__c);


    // Begin writing the string...

    // 1 //CUSTOMER - SECTION-CU
    addValue(SECTIONCU);

    // 2 //CUST.CREDIT.ACCT - Flex_Account_Number__c
    addValue(line.Flex_Account_Number__c);

    // 3 //CD.NAME - Legal_Name__c
    addValue(line.Legal_Name__c);

    // 4 //CD.ADDRESS1 - Account__r.ShippingStreet
    addValue(ShippingStreet[0]);
    // 5 //CD.ADDRESS2 - Account__r.ShippingStreet
    addValue(ShippingStreet[1]);
    // 6 //CD.CITY - Account__r.ShippingCity
    addValue(line.Account__r.ShippingCity);
    // 7 //CD.STATE - Account__r.ShippingStateCode
    addValue(line.Account__r.ShippingStateCode);
    // 8 //CD.ZIP - Account__r.ShippingPostalCode
    addValue(line.Account__r.ShippingPostalCode);
    // 9 //CD.NAICS.CODE - TBD
    addValue('');
    // 10 //CD.SIC.CODE - TBD
    addValue('');
    // 11 //CD.DBA - Account__r.Name
    addValue(line.Account__r.Name);
    // 12 //CD.AR.NAME - Legal_Name__c
    addValue(line.Legal_Name__c);
    // 13 //CD.AR.ADDRESS1 - Account__r.BillingStreet (1st line)
    addValue(BillingStreet[0]);
    // 14 //CD.AR.ADDRESS2 - Account__r.BillingStreet (2nd line)
    addValue(BillingStreet[1]);
    // 15 //CD.AR.CITY - Account__r.BillingCity
    addValue(line.Account__r.BillingCity);
    // 16 //CD.AR.STATE - Account__r.BillingStateCode
    addValue(line.Account__r.BillingStateCode);
    // 17 //CD.AR.ZIP - Account__r.BillingPostalCode
    addValue(line.Account__r.BillingPostalCode);
    // 18 //CD.CNTC.PHONE - BLANK
    addValue('');
    // 19 //CD.CR.SCORING - BLANK
    addValue('');
    // 20 //CU.ALPHA.FIELD6 - BLANK
    addValue('');

    //-----------------------------------------------------------
    // 21 //CUSTOMER MESSAGES SECTION - OMIT THE ENTIRE SECTION
    //addValue(''); // Do not write backslash for omitted sections

    //-----------------------------------------------------------
    // 22 //RELATED PARTY - SECTION-RP
    addValue(SECTIONRP);
    // 23 //CP.PRIN.GUAR.RECORDS - BLANK
    // 24 //CP.PRIN.GUAR.CODES - BLANK
    // 25 //CD.NAME - BLANK
    // 26 //CD.ADDRESS1 - BLANK
    // 27 //CD.ADDRESS2 - BLANK
    // 28 //CD.CITY - BLANK
    // 29 //CD.STATE - BLANK
    // 30 //CD.ZIP - BLANK
    // 31 //CD.FED.ID - BLANK
    for(Integer i=0; i<9; i++){
      addValue('');
    }

    //-----------------------------------------------------------
    // 32 //RELATED PARTY MESSAGES - OMIT THE ENTIRE SECTION
    // 33 //TRADE - OMIT THE ENTIRE SECTION
    // 34 //BANK REFERENCE - OMIT THE ENTIRE SECTION
    // 35 //BANK ACCOUNT - OMIT THE ENTIRE SECTION
    // Do not write backslash for omitted sections

    //-----------------------------------------------------------
    // 36 //CONTRACT - SECTION-CO
    addValue(SECTIONCO);
    // 37 //LESSOR - 820
    addValue('820');
    // 38 //CONTRACT.NUM - Flex_Account_Number__c - format as "0000000+"
    String acctNum = (line.Flex_Account_Number__c != null ? line.Flex_Account_Number__c : '');
    addValue( ('0000000' + acctNum).right(7) + '+');
    // 39 //CUST.ID - Flex_Account_Number__c
    addValue(line.Flex_Account_Number__c);
    // 40 //CUST.SHORT.NAME - Account__r.Name
    addValue(line.Account__r.Name);
    // 41 //CUST.DBA - Account__r.Name
    addValue(line.Account__r.Name);
    // 42 //AR.NAME - Legal_Name__c
    addValue(line.Legal_Name__c);
    // 43 //AR.ADDRESS1 - Account__r.BillingStreet (1st line)
    addValue(BillingStreet[0]);
    // 44 //AR.ADDRESS2 - Account__r.BillingStreet (2nd line)
    addValue(BillingStreet[1]);
    // 45 //AR.CITY - Account__r.BillingCity
    addValue(line.Account__r.BillingCity);
    // 46 //AR.STATE - Account__r.BillingStateCode
    addValue(line.Account__r.BillingStateCode);
    // 47 //AR.ZIP - Account__r.BillingPostalCode
    addValue(line.Account__r.BillingPostalCode);
    // 48 //AR.ATTN - Accounts Payable
    addValue('Accounts Payable');
    // 49 //CUST.NAME - Legal_Name__c
    addValue(line.Legal_Name__c);
    // 50 //CUST.ADDRESS1 - Account__r.ShippingStreet (1st line)
    addValue(ShippingStreet[0]);
    // 51 //CUST.ADDRESS2 - Account__r.ShippingStreet (2nd line)
    addValue(ShippingStreet[1]);
    // 52 //CUST.CITY - Account__r.ShippingCity
    addValue(line.Account__r.ShippingCity);
    // 53 //CUST.STATE - Account__r.ShippingStateCode
    addValue(line.Account__r.ShippingStateCode);
    // 54 //CUST.ZIP - Account__r.ShippingPostalCode
    addValue(line.Account__r.ShippingPostalCode);
    // 55 //REQ.SIGNATURE.PHONE - BLANK
    addValue('');
    // 56 //CONTACT.FAX.PHONE - BLANK
    addValue('');
    // 57 //LEASE.TYPE - Lease_Type__c - When it is "OL" supply "XX" instead (I-201746)
    addValue(line.Lease_Type__c=='OL' ? 'XX' : line.Lease_Type__c);
    // 58 //BOOKING.DATE - Commence_Date__c as mm/dd/yyyy
    string commenceDateString = dateToStringMMDDYYYY(line.Commence_Date__c);
    addValue(commenceDateString);
    // 59 //ACTIV.DATE - Commence_Date__c as mm/dd/yyyy
    addValue(commenceDateString);
    // 60 //FLOAT.RATE - BLANK
    addValue('');
    // 61 //FLOAT.TYPE - BLANK
    addValue('');
    // 62 //INCOME.START.DATE - Commence_Date__c as mm/dd/yyyy
    addValue(commenceDateString);
    // 63 //CONTRACT.TERM - Term__c
    addValue( calculateTermForBotw(line, pmtsAreInAdvance) );
    // 64 //INCOME.METHOD - Lease_Type__c // IF Lease_Type = "OL" then use "E", else "Y"
    addValue(line.Lease_Type__c=='OL' ? 'E' : 'Y');
    // 65 //RESID.METHOD - Lease_Type__c // IF Lease_Type = "OL" then use "E", else "Y"
    addValue(line.Lease_Type__c=='OL' ? 'E' : 'Y');
    // 66 //GROSS.CONTRACT - TBD
    addValue(String.valueOf(line.Sum_of_Payments__c));
    
    // 67 //GROSS.FINANCE - Sum_of_Payments__c less (Equip+Freight+Service funded amts)
    // Per Ray 9/9/15 this should be GROSS.CONTRACT (#66) less sum of A.ORIG.COST (#199).
    // This maps to Sum_of_Payments - SUM(Funded Amt.)
    //addValue(String.valueOf(line.Total_Spread_Amount__c));
    Decimal grossFinance = (line.Sum_of_Payments__c == null ? 0.00 : line.Sum_of_Payments__c)
                         - (line.Equipment_Funded_Amt__c == null ? 0.00 : line.Equipment_Funded_Amt__c)
                         - (line.Service_Funded_Amt__c == null ? 0.00 : line.Service_Funded_Amt__c)
                         - (line.Freight_Funded_Amt__c == null ? 0.00 : line.Freight_Funded_Amt__c);    
    addValue(String.valueOf(grossFinance.setScale(2)));
    
    
    // 68 //ENDING.PYMTS.ADV - BLANK
    addValue('');

    // 69 //PYMTS.ADV - Per Ray@botw leave it BLANK (I-201746)
    addValue('');

    // 70 //SEC.DEPOSIT - BLANK
    addValue('');
    // 71 //PYMTS.ARREARS - binary *opposite* of #69 (PYMTS.ADV)
    addValue( pmtsAreInAdvance ? '0' : '1');
    // 72 //INVOICE.CODE - Invoice_Code__c - only 1st char
    addValue(line.Invoice_Code__c == null ? '' : line.Invoice_Code__c.left(1) );
    // 73 //INVOICE.DESC - Invoice_Description__c
    addValue(line.Invoice_Description__c);
    // 74 //INV.DAYS - Invoice_Lead_Days__c - default 45 if blank
    addValue(line.Invoice_Lead_Days__c == null ? '45' : String.valueOf(line.Invoice_Lead_Days__c) );
    // 75 //VARIABLE.PYMT.CODE - Payment_Structure__c
    addValue( billingCycleIsEven ? '0' : '1' );
    // 76 //PYMT.OPTION - N
    addValue('N');
    // 77 //INV.DUE.DAY - First_Payment_Due_Date__c
    if(line.First_Payment_Due_Date__c != null){
      //System.debug('First_Payment_Due_Date__c.day()===='+line.First_Payment_Due_Date__c.day());
      addValue(String.valueOf(line.First_Payment_Due_Date__c.day()));
    }
    else{
      addValue('');
    }
    // 78 //CONTRACT.PYMT - Payment_Amt__c
    addValue(String.valueOf(line.Payment_Amt__c));
    // 79 //FIRST.PYMT.AMT - First_Payment_Amount__c
    addValue(String.valueOf(line.First_Payment_Amount__c));
    // 80 //FIRST.PYMT.DATE - First_Payment_Due_Date__c   Format as "mm/dd/yyyy"
    addValue(dateToStringMMDDYYYY(line.First_Payment_Due_Date__c));

    // 81 //NON.ACCRUAL.AFTER - 90
    addValue('90');
    // 82 //BILLING.CYCLE - string based on even pmt cycle (I-201746 if irregular just provide all 1's)
    addValue( billingCycleIsEven ? billingCycleString : '1|1|1|1|1|1|1|1|1|1|1|1');
    // 83 //LATE.CHRG.EXMPT - N
    addValue('N');
    // 84 //LATE.CHRG.RATE - BLANK
    addValue('');
    // 85 //LATE.CHARGE.CODE - S
    addValue('S');
    // 86 //MIN.LATE.CHRG - BLANK
    addValue('');
    // 87 //MAX.LATE.CHRG - BLANK
    addValue('');
    // 88 //GRACE.PERIOD - BLANK
    addValue('');
    // 89 //LC.BEGIN.DATE - LC_Begin_Date__c
    addValue(dateToStringMMDDYYYY(line.LC_Begin_Date__c));

    // 90 //PUR.ORDER.NUM - Customer_PO_Nbr__c
    addValue(line.Customer_PO_Nbr__c);
    // 91 //QUOTE.BUYOUT - 01
    addValue('01');
    // 92 //INSURANCE.CODE - BLANK
    // 93 //BANK.CODE - BLANK
    // 94 //AM.ACH.LEAD.DAYS - BLANK
    // 95 //TAPE.BANK.NUM - BLANK
    // 96 //TAPE.ACCOUNT.NUM - BLANK
    // 97 //DEALER - BLANK
    // 98 //MRKTNG.REP - BLANK
    for(Integer i=0; i<7; i++){
      addValue('');
    }
    // 99 //MRKTNG.PCTS - 100
    addValue('100');

    // 100 //PRODUCT.LINE - BLANK
    addValue('');
    // 101 //CR.ATTG.NAME - BLANK
    addValue('');
    // 102 //CR.ATTG.PHONE - BLANK
    addValue('');
    // 103 //OFFICE - 0002
    addValue('0002');
    // 104 //NMF.CONTACT.EMAIL - BLANK
    addValue('');
    // 105 //REMIT.TO - 0001
    addValue('0001');

    // 106 //NMF.GLL.INDEX - BLANK
    // 107 //DAILY.INTEREST - BLANK
    // 108 //ADJUST.BILLING.CYCLE - BLANK
    // 109 //PYMT.CHANGE.CYCLE - BLANK
    // 110 //NEG.ADJUST.BILLING - BLANK
    // 111 //F.DAYS.IN.MONTH - BLANK
    // 112 //F.DAYS.IN.YEAR - BLANK
    // 113 //FIRST.ADJUST.DATE - BLANK
    // 114 //FIRST.INTEREST.DATE - BLANK
    // 115 //RATE.CHANGE.DATE - BLANK
    // 116 //RATE.CHANGE.METHOD - BLANK
    // 117 //BASE.CODE - BLANK
    // 118 //BASE.RATE - BLANK
    // 119 //BASE.CODE.TBL - BLANK
    // 120 //RATE.VARIANCE.CODE - BLANK
    // 121 //RATE.VARIANCE.DATE - BLANK
    // 122 //RATE.VARIANCE - BLANK
    // 123 //RATE.CHANGE.CYCLE - BLANK
    // 124 //RATE.CHANGE.DAY - BLANK
    // 125 //INT.BILLING.CYCLE - BLANK
    // 126 //INTEREST.TO.DAY - BLANK
    // 127 //FIRST.INT.PYMT.DATE - BLANK
    // 128 //UM.USER.CODE - BLANK
    for(Integer i=0; i<23; i++){
      addValue('');
    }
    // 129 //UM.TABLE1 - BOTW_Lender_Code__c
    addValue(line.BOTW_Lender_Code__c);
    // 130 //UM.TABLE2 - BLANK
    // 131 //UM.USER.DATE1 - BLANK
    // 132 //UM.USER.DATE2 - BLANK
    // 133 //UM.USER.DATE3 - BLANK
    // 134 //UM.ALPHA.NUM1 - BLANK
    // 135 //UM.ALPHA.NUM2 - BLANK
    // 136 //UM.ALPHA.FIELD3 - BLANK
    for(Integer i=0; i<7; i++){
      addValue('');
    }
    // 137 //UM.ALPHA.FIELD4 - Opportunity_Number__c
    addValue(line.Opportunity_Number__c);
    // 138 //UM.ALPHA.FIELD5 - Lender_Contract_Nbr__c
    addValue(line.Lender_Contract_Nbr__c);
    // 139 //UM.ALPHA.FIELD6 - LPO_Nbr__c
    addValue(line.LPO_Nbr__c);
    // 140 //UM.ALPHA.FIELD7 - BLANK
    // 141 //UM.ALPHA.FIELD8 - BLANK
    // 142 //UM.ALPHA.FIELD9 - BLANK
    // 143 //UM.ALPHA.FIELD10 - BLANK
    // 144 //UM.USER.AMT1 - BLANK
    // 145 //UM.USER.AMT2 - BLANK
    for(Integer i=0; i<6; i++){
      addValue('');
    }
    // 146 //VARIABLE.DATE - BOTW_Irreg_Pmt_Sequence_Date__c
    addValue( billingCycleIsEven ? '' : line.BOTW_Irreg_Pmt_Sequence_Date__c );
    // 147 //VARIABLE.RATE - BOTW_Irreg_Pmt_Sequence_Rate__c
    addValue( billingCycleIsEven ? '' : line.BOTW_Irreg_Pmt_Sequence_Rate__c );

    //-----------------------------------------------------------
    // 148 //CONTRACT MESSAGES - OMIT ENTIRE SECTION
    // 149 //CONTRACT BASED UCC - OMIT ENTIRE SECTION
    // Do not write backslash for omitted sections


    //-----------------------------------------------------------
    // 150 //CONTRACT BLENDED INCOME - SECTION-CBL
    addValue(SECTIONCBL);
    // 151 //BI.TIMING - BLANK
    // 152 //BI.BEGIN.DATE - BLANK
    // 153 //BI.CODE - BLANK
    // 154 //BI.BOOKING.DATE - BLANK
    // 155 //BI.TERM - BLANK
    // 156 //BI.INCOME.METHOD - BLANK
    // 157 //BI.AMOUNT - BLANK
    // 158 //BI.INC.CONT.RATE - BLANK
    for(Integer i=0; i<8; i++){
      addValue('');
    }
    // 159 //ABI.INC.IN.QUOTE - BLANK
    // ** Leave out the backslash for this final field in this string
    addValueWithoutSlash('');
    
    System.debug('@@@result' + result);
    return result;
  }


  private String calculateString2(Flex_Contract__c line){
  	result = '';

    //-----------------------------------------------------------
    // 230 //CONTRACT INSURANCE - SECTIONCI
    addValue(SECTIONCI);
    // 231 //INSUR.RECORDS - BLANK
    // 232 //INSUR.EFFECTIVE.DATE - BLANK
    // 233 //INSUR.EXPIRE.DATE - BLANK
    // 234 //INSUR.CARRIER - BLANK
    // 235 //INSUR.TYPE - BLANK
    // 236 //INSUR.CARRIER.CNTC - BLANK
    // 237 //INSUR.CARRIER.ADDR1 - BLANK
    // 238 //INSUR.CARRIER.CITY - BLANK
    // 239 //INSUR.CARRIER.STATE - BLANK
    // 240 //INSUR.CARRIER.ZIP - BLANK
    // 241 //INSUR.CARRIER.PHONE - BLANK
    // 242 //INSUR.CARRIER.FAX - BLANK
    for(Integer i=0; i<12; i++){
      addValue('');
    }

    //-----------------------------------------------------------
    // 243 //CONTRACT INSUR COMMENTS - OMIT ENTIRE SECTION
    // 244 //CONTRACT INSUR MISC BILL - OMIT ENTIRE SECTION
    // Do not write backslash for omitted sections


    //-----------------------------------------------------------
    // 245 //MISC BILLABLE - SECTIONMB
    // Only write values if Misc Bill Amount has a value
    // Otherwise write all blanks
    addValue(SECTIONMB);
    if(line.Misc_Billable_Amount__c != null && line.Misc_Billable_Amount__c != 0.00) {
      // 246 //MISC.DESC - Misc_Billable_Description__c
      addValue(line.Misc_Billable_Description__c);
      // 247 //FIRST.MISC.DATE - First_Payment_Due_Date__c  format as "MM/yyyy"
      addValue(dateToStringMMYYYY(line.First_Payment_Due_Date__c));
      // 248 //FINAL.MISC.DATE - BLANK
      addValue('');
      // 249 //INC.IN.RENT - Yes "Y"
      addValue('Y');
      // 250 //MISC.GL.CODE - Hard coded "0240"
      addValue('0240');
      // 251 //MISC.AMT - Misc_Billable_Amount__c
      addValue(String.valueOf(line.Misc_Billable_Amount__c));
      // 252 //MISC.TAXABLE - Misc_Taxable__c
      addValue(line.Misc_Taxable__c ? 'Y' : 'N');
      // 253 //MISC.CYCLE - string based on even pmt cycle (I-201746 if irregular just provide all 1's)
      addValue( billingCycleIsEven ? billingCycleString : '1|1|1|1|1|1|1|1|1|1|1|1' );
      // 254 //MISC.BILL.DISP - N
    addValue('N');
    }
    else {
      for(Integer i=0; i<9; i++){
        addValue('');
      }
    }


    //-----------------------------------------------------------
    // MISC INVOICE    255-263
    // Repeat this section for each child Flex_Misc_Invoiced_Items__c record.
    // If not exist any Flex_Misc_Invoiced_Items, write section one time with blank fields.
    if(line.Flex_Misc_Invoiced_Items__r.size() > 0){
      for(Flex_Misc_Invoiced_Item__c miscInvoiceItem : line.Flex_Misc_Invoiced_Items__r){
        // 255 //MISC INVOICE - SECTIONMI
        addValue(SECTIONMI);
        // 256 //MISC.DESC - Miscellaneous_Invoiced_Type__c   Miscellaneous_Invoiced_Type__c/Other_Misc_Inv_Type_Description__c
        addValue(miscInvoiceItem.Miscellaneous_Invoiced_Type__c == 'Other'
                  ? miscInvoiceItem.Other_Misc_Inv_Type_Description__c
                  : miscInvoiceItem.Miscellaneous_Invoiced_Type__c);
        // 257 //MISC.DATE - First_Payment_Due_Date__c - format "MM/yyyy"
        addValue(dateToStringMMYYYY(line.First_Payment_Due_Date__c));
        // 258 //INC.IN.RENT - N
        addValue('N');
        // 259 //MISC.GL.CODE - BOTW_GL_Code__c
        addValue(miscInvoiceItem.BOTW_GL_Code__c);
        // 260 //MISC.AMT - Miscellaneous_Invoiced_Amount__c
        addValue(String.valueOf(miscInvoiceItem.Miscellaneous_Invoiced_Amount__c));
        // 261 //MISC.TAXABLE - Taxable__c - Y/N
        addValue(miscInvoiceItem.Taxable__c==true ? 'Y' : 'N');
        // 262 //MISC.BILL.DISP - N
        addValue('N');
        // 263 //MISC.PAYABLE - 0
        addValue('0');
      }
    } 
    else {
      // No misc invoice items exist, write one with all blanks
      addValue(SECTIONMI);
      for(Integer i=0; i<8; i++){
        addValue('');
      }
    }


    //-----------------------------------------------------------
    // 264 //CONTRACT FUNDING - OMIT ENTIRE SECTION
    // Do not write backslash for omitted sections


    //-----------------------------------------------------------
  	// 265 //RENEWAL - SECTIONRN
    addValue(SECTIONRN);
    // 266 //R.RENEWAL.TYPE - BLANK
    // 267 //R.FINANCED.RENEWAL - BLANK
    // 268 //R.RESIDUAL.PCT - BLANK
    // 269 //R.ACTIV.DATE - BLANK
    // 270 //R.BOOKING.DATE - BLANK
    // 271 //R.CONTRACT.TERM - BLANK
    // 272 //R.GROSS.CONTRACT - BLANK
    // 273 //R.BILLING.CYCLE - BLANK
    // 274 //R.FIRST.PYMT.DATE - BLANK
    // 275 //R.INCOME.METHOD - BLANK
    for(Integer i=0; i<10; i++){
      addValue('');
    }
      
    // 276 //END HEADER LIST
    // **No value here, final backslash should be just after the field #275 above
    addValueWithoutSlash('');
    

    System.debug('@@@result' + result);
    return result;
  }

    
    public String calculateTermForBotw(Flex_Contract__c line, boolean isPmtInAdvance){
    // Compute the payment TERM according to BOTW-specific logic.
    // The Term (Flex_Contract__c.Term__c) used by Flex is based on info from the lender,
    // and must remain as it is in Salesforce.
    // But BOTW InfoLease import requires the "Term" length to span all months
    // from Commence Date through the Last Payment. Thus BOTW "Term" (#63) may
    // sometimes be greater than (but never less than) Flex's Term.
    // BOTW provided the logic below for computing their "Term" in this fashion.
    // I-201746: Fix logic for non-monthly (annual, qtrly) frequencies.
    
    if(line.First_Payment_Due_Date__c == null || line.Commence_Date__c == null || line.Term__c == null
        || !mapPaymentFreqPeriod.containsKey(line.Payment_Frequency__c)) {
      return String.valueOf(line.Term__c);
    }
    
    Integer iTerm = (Integer)line.Term__c;
    Integer period = mapPaymentFreqPeriod.get(line.Payment_Frequency__c);
    Integer numPmts = (iTerm / period);         //truncate any decimals
    Integer adjMos = (period > 1 ? -1 : (isPmtInAdvance ? 0 : 1));

    Date dtEnd = line.Commence_Date__c.addMonths(iTerm + adjMos);
    Date dtLastPmt = line.First_Payment_Due_Date__c.addMonths(numPmts * period);
    
    // If LastPmt month is beyond End month, add months gap to the Term
    if(dtEnd.monthsBetween(dtLastPmt) > 0){
      iTerm += dtEnd.monthsBetween(dtLastPmt);
      System.debug('@@@ Term was adjusted to: ' + iTerm);
    }
    return String.valueOf(iTerm);
  }
    
    
  public String calculateBillingCycleString(Date firstPaymentDueDate, String paymentFrequency){
    // Compute the billing cycle string for regular/even payments
    // Format "1|0|0|1|0|0|1|0|0|1|0|0"
    // Twelve 1's and 0's represent months January-December; separated by vert pipe.
    // Value = 1 if payment due that month, 0 if no payment that month.
    // Examples:
    //     1|1|1|1|1|1|1|1|1|1|1|1 = Monthly Payments every month.
    //     1|0|0|1|0|0|1|0|0|1|0|0 = Quarterly pmts due in Jan, Apr, Jul, Oct.
    //     0|1|0|0|1|0|0|1|0|0|1|0 = Quarterly pmts due in Feb, May, Aug, Nov.
    //     0|0|1|0|0|0|0|0|1|0|0|0 = Semi-Annual pmts due in Mar, Sep.

    // Fast return for Monthly (most common)
    if(paymentFrequency == 'Monthly'){
      billingCycleString = '1|1|1|1|1|1|1|1|1|1|1|1';
      return billingCycleString;
    }

    billingCycleString = '';
    if(firstPaymentDueDate == null || String.isEmpty(paymentFrequency) || !mapPaymentFreqPeriod.containsKey(paymentFrequency) ){
      return billingCycleString;
    }

    // Determine the cyclic repeat period (#months each payment spans; e.g. Quarterly=3).
    Integer periodMos = mapPaymentFreqPeriod.get(paymentFrequency);

    // Store flags in array, intially all OFF
    String[] seq = new String[] {'0','0','0','0','0','0','0','0','0','0','0','0'};
    // Determine the first month a pmt due in the cycle (use 0-based month to align w/ array indices)
    Integer firstmo = math.mod(firstPaymentDueDate.Month() - 1, periodMos);
    // Set flag ON for each month a pmt is due
    for(Integer i = firstmo; i < 12; i += periodMos){
        seq[i] = '1';
    }
    // Format with pipes
    billingCycleString = String.join(seq, '|');
    //System.debug('@@@ billingCycleString = ' + billingCycleString);
    return billingCycleString;
  }
  
  
  private string dateToStringMMDDYYYY(date dateField){
    // Returns DATE field in "mm/dd/yyyy" format
    // Returns empty string if null
    return ( dateField == null ? '' : datetime.newInstance(dateField, Time.newInstance(0,0,0,0)).format('MM/dd/yyyy') );
  }

  private string dateToStringMMYYYY(date dateField){
    // Returns DATE field in "mm/yyyy" format (month/year only)
    // Returns empty string if null
    return (dateField == null ? '' : datetime.newInstance(dateField, Time.newInstance(0,0,0,0)).format('MM/yyyy'));
  }

  // Helper method to add data values with field terminator "\"
  // 2015-09-11  H.Whitacre  T-433745  Replace "\" with "/" if exist within data value
  private void addValue(String s){
    if(String.isBlank(s)){
      result += '\\';
    }
    else{
      result += s.replace('\\', '/') + '\\';
    }
  }
  
  // Helper method to add data values WITHOUT field terminator
  private void addValueWithoutSlash(String s){
    if(!String.isBlank(s)){
      result += s.replace('\\', '/');
    }
  }

  private Map<String,Integer> createMapPaymentFreqPeriod(){
    // Maps Payment Frequency names to cyclic period in months (#mos ea payment spans; e.g. Quarterly=3).
    // Items come from picklist choices on Flex_Contract__c.Payment_Frequency__c
    // plus some itmes to support legacy/migrated data.
    // Plus copies in all-lowercase to aid in case-insensitive matches.
    // Do NOT include 'Irregular'; map only works for regular pmts cycling monthly to annually.
    Map<string,Integer> mPmtFreq = new map<string,Integer>();
    mPmtFreq.put('Monthly', 1);  // <-- picklist choices
    mPmtFreq.put('Quarterly', 3);
    mPmtFreq.put('Semi-annually', 6);
    mPmtFreq.put('Annually', 12);
    mPmtFreq.put('ANNUAL', 12);    // <-- legacy data
    mPmtFreq.put('SEMI-ANNUAL', 6);
    mPmtFreq.put('monthly', 1);  // <-- picklist choices (lowercase)
    mPmtFreq.put('quarterly', 3);
    mPmtFreq.put('semi-annually', 6);
    mPmtFreq.put('annually', 12);
    mPmtFreq.put('annual', 12);    // <-- legacy data (lowercase)
    mPmtFreq.put('semi-annual', 6);
    return mPmtFreq;
  }

  
  // Method to create Attachment (txt file) for a given Contract Id.
  // *** NOTE: This is NOT a normal part of BOTW integration.  ***
  // *** Normally the TXT file is built & sent by Informatica. ***
  // This method is for AD-HOC usage and "offline" testing only,
  // as an easy way to generate a BOTW text data file without Informatica.
  // The BOTW Integration Strings must already be built (by above main methods) first.
  // Call this method (from anonymous apex), passing in a list of Flex_Contact Id's.
  // It will combine strings into a TXT file, creating as Attachment on the contract(s).
  // Note you may incur error if too many contracts, too many contract lines, or for
  // any other reason that the total text size exceeds capacity limits for Attachment.
  // Example calling procedure:
  //     Flex_BatchBOTWIntegrationContract clazz = new Flex_BatchBOTWIntegrationContract();
  //     Integer qty = clazz.createFileAttachments(new List<string>{'0ab12300004def6','0ab12300005def7'});
  public Integer createFileAttachments(List<string> flexConIds){

    // Fetch contracts & lines data
    List<Flex_Contract__c> cons = [Select Id, BOTW_Integration_Status__c, BOTW_Integration_Timestamp__c,
                                   BOTW_Integration_String1__c, BOTW_Integration_String2__c,
                                     (Select Id, BOTW_Integration_String__c
                                      From Flex_Contract_Lines__r order by Id)
                                   From Flex_Contract__c
                                   Where Id in :flexConIds];
    
    // Generate a TXT file Attachment for each Contract
    List<Attachment> attachs = new List<Attachment>();
    for(Flex_Contract__c con : cons){
      
      Datetime dtNow = System.Datetime.now();
      String filename = 'BOTW_' + con.Id + '_'
                    + dtNow.formatGmt('yyyyMMdd_HHmmss')
                    + '.txt';
      System.debug('@@@ Filename will be ' + filename);
      
      string body = con.BOTW_Integration_String1__c;
      for(Flex_Contract_Line__c line : con.Flex_Contract_Lines__r){
        body += '\\';
        body += line.BOTW_Integration_String__c;
      }
      body += '\\';
      body += con.BOTW_Integration_String2__c;
      
      Attachment atch = new Attachment();
      atch.Name = filename;
      atch.Body = Blob.valueOf(body);
      atch.ContentType = 'text/plain';
      atch.Description = 'Data file for BOTW InfoLease.'
                       + '\nFile Generated ' + dtNow.formatGmt('yyyy-MM-dd HH:mm:ss') + '(GMT)'
                       + '\n*** Integration Reference at time file was generated ***'
                       + '\nBOTW_Integration_Status: ' + con.BOTW_Integration_Status__c
                       + '\nBOTW_Integration_Timestamp: ' 
                            + (con.BOTW_Integration_Timestamp__c == null ? '(null)' : con.BOTW_Integration_Timestamp__c.formatGmt('yyyy-MM-dd HH:mm:ss') )
                            + '(GMT)';
      atch.ParentId = con.Id;
      attachs.add(atch);
    }
    // Insert attachments and return quantity
    if(attachs.size() > 0){
      insert attachs;
    }    
    return attachs.size();
    
  }

}