/*
//
// (c) 2015 Appirio, Inc.
//
// Apex Test class for Visualforce component Controller CustomLookup
//
// This Component is using by Medical_ChangeCaseOwner Page.
//
// Created by : Surabhi Sharma (Appirio)
//
*/
@isTest(seeAllData = true)
public class CustomLookupCtrlTest {
    static testMethod void testCustomLookupCtrl(){
        User usr = Medical_TestUtils.createUser(1, 'Medical - System Admin', true).get(0);
        System.runAs(usr) {
            Test.startTest();
            Group grp = Medical_TestUtils.createGroup();
            CustomLookupCtrl testCustom = new CustomLookupCtrl();
            testCustom.fieldName = 'Name';
            testCustom.objName = 'Group';
            testCustom.selectedId = grp.Id;
            PageReference pageRef = testCustom.getSelectedRecord();
            testCustom.fieldName = 'Name';
            testCustom.objName = 'User';
            testCustom.selectedId = usr.Id;
            pageRef = testCustom.getSelectedRecord();
            Order ord = Medical_TestUtils.createOrder();
            testCustom.fieldName = 'PoNumber';
            testCustom.objName = 'Order';
            testCustom.selectedId = ord.Id;
            pageRef = testCustom.getSelectedRecord(); 
            Test.stopTest();
            System.assertEquals(True ,pageRef == testCustom.getSelectedRecord());
        }
    }
}