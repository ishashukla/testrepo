// (c) 2015 Appirio, Inc.
//
// Class Name: Flex_CreditNotesController
// Description: Contoller Class for Credit NOtes Related list
//
// 29th Jun 2015    Ravindra Shekhawat   Original (Task # T-373931)
//
public with sharing class Flex_CreditNotesController {
   public Id flexCreditId {get; set;}
    //  set controller
 
 	  public Flex_CreditNotesController(ApexPages.StandardController controller){
 	  	flexCreditId = ApexPages.currentPage().getParameters().get('Id');
 	  	System.debug('FLEX CREDIT ID IS :'+flexCreditId);
 	  }
    public CreditNotesWrapper[] creditNotes{
    get{
        List<CreditNotesWrapper> notesList = new List<CreditNotesWrapper>();
        for(Credit_Notes__c notes : [Select c.Opportunity__r.Name, c.Opportunity__c, c.Id, c.Credit_Notes__c, c.LastModifiedBy.Name,
                                     c.LastModifiedDate, c.CreatedBy.Name From Credit_Notes__c c
                                     WHERE  c.Opportunity__c IN
                                     (Select f.Opportunity__c From Flex_Credit__c f WHERE f.Id =: flexCreditId)])
        {
            CreditNotesWrapper tempNotes = new CreditNotesWrapper();
            tempNotes.creditNotes = notes;
            tempNotes.notes = notes.Credit_Notes__c;
            if(notes.Opportunity__r != null) {
            	tempNotes.opportunityName = notes.Opportunity__r.Name;
            }
            
            tempNotes.lastModified = notes.LastModifiedBy.Name + ' - '+notes.LastModifiedDate;

            //Add to list
            notesList.add(tempNotes); 
        }
    return notesList;
    }
    set;
  }
  
  public PageReference createCreditNotes() {
  	Schema.DescribeSObjectResult destination = Credit_Notes__c.sObjectType.getDescribe();
  	PageReference pageRef = new PageReference('/' + destination.getKeyPrefix()+'/e?retURL='+flexCreditId);
  	pageRef.setRedirect(true);
  	return pageRef;
  }
  
  // Wrapper Class For Case Comments
  public class CreditNotesWrapper {

    public Credit_Notes__c creditNotes {get; set;}
    public String opportunityName {get; set;}
  	public String notes {get;set;}
  	public String lastModified {get;set;} 
  }
  
}