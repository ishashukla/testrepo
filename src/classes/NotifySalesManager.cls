global class NotifySalesManager implements Database.Batchable<sObject>{
  global Set<Id> roleSubordinateUserSet;  
  global Map<Id,Set<Id>> managerIdsToUserIdsMap;
  global Map<Id,Monthly_Forecast__c> monthlyForecastMap;
  global EmailTemplate emailTemplate; 
  global Set<Id> allUsers;
  global Map<Id,User> idToUserMap;
  global List<User> lstUsers;
  global List<String> monthStrings = new List<String>{'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'};
  global Date todaysDate = Date.today();
  global Set<Id> profileIdSet;
  global List<Profile> profileList;
  global Database.QueryLocator start(Database.BatchableContext BC){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
    Id recordTypeId = rtByName.getRecordTypeId();
   //Date todaysDate = Date.newInstance(2016,04,19);
   //(Use this code for running debug anonymous:  Database.executeBatch(new NotifySalesManager());)
    system.debug('todaysDate>>>'+todaysDate);
    String query = '';
    query = 'SELECT Name, Year__c, User__c, Month_End__c, Id, Manager__c, Business_Unit__c, Forecast_Year__c, Forecast_Owner__c, Month__c, Forecast_Submitted__c, Forecast_Record_Type_Id__c From Monthly_Forecast__c WHERE  Month_End__c =: todaysDate AND Forecast_Submitted__c = false AND Forecast_Record_Type_Id__c =: recordTypeId';
    system.debug('query>>>'+query);
    return Database.getQueryLocator(query);
  } 
  
  global void execute(Database.BatchableContext BC,List<Sobject> scope){
    allUsers = new Set<Id>();
    roleSubordinateUserSet = new Set<Id>();
    managerIdsToUserIdsMap = new Map<Id,Set<Id>>();
    monthlyForecastMap = new Map<Id,Monthly_Forecast__c>();
    idToUserMap = new Map<Id,User>();
    lstUsers = new List<User>();
    profileIdSet = new Set<Id>();
    for(sObject s: scope){
      Monthly_Forecast__c monthlyForecast = (Monthly_Forecast__c)s;
      if(monthlyForecast.Forecast_Owner__c.length() > 15 && monthlyForecast.Manager__c.length() > 15){
      monthlyForecastMap.put(monthlyForecast.Forecast_Owner__c,monthlyForecast);
      if(!managerIdsToUserIdsMap.containsKey(monthlyForecast.Manager__c)){
        managerIdsToUserIdsMap.put(monthlyForecast.Manager__c,new Set<Id>());
      }
      managerIdsToUserIdsMap.get(monthlyForecast.Manager__c).add(monthlyForecast.Forecast_Owner__c);
     }
    }
    for(String managerId : managerIdsToUserIdsMap.keySet()){
      allUsers.add(managerId);
      allUsers.addAll(managerIdsToUserIdsMap.get(managerId));
    }
    profileList = [SELECT Id, Name FROM Profile WHERE Name = 'Instruments Sales Manager'];
    for(Profile pro : profileList){
      profileIdSet.add(pro.Id);
    }
    lstUsers = [SELECT Id, Name, Email, FirstName, ProfileId FROM User WHERE Id IN: allUsers];
    for(User userIds : lstUsers){
      
      if(!idToUserMap.containsKey(userIds.Id)){
          idToUserMap.put(userIds.id, userIds);
      }
    }
    List<Messaging.SingleEmailMessage> actualMails = new List<Messaging.SingleEmailMessage>();
    String htmlValue;
    
    for(String managerId : managerIdsToUserIdsMap.keySet()){
      User u = idToUserMap.get(managerId);
      if(u.ProfileId != null && profileIdSet.contains(u.ProfileId)){
        htmlValue  = generateEmailTable(monthlyForecastMap, managerId, idToUserMap);
        actualMails.add(sendEmail(htmlValue, managerId));
      }
    }
    try{
      Messaging.sendEmail(actualMails); 
    }
    catch(Exception e){
      System.debug(e);
    }
    
  }
  
  global void finish(Database.BatchableContext BC){
  
  }
  
  private String generateEmailTable(Map<Id,Monthly_Forecast__c> monthlyForecastMap, String managerId, Map<Id, User> idToUserMap) {
    String htmlValue = '';
    // Table Headers
    String tmp = 'Dear '+idToUserMap.get(managerId).FirstName+', <br/>The following Reps haven'+'t submitted their Forecasts for this month.<br/><br/>'+
                 '<table border="1" bordercolor="#FFCC00" style="background-color:#FFFFFF; width:100%"  cellpadding="3" cellspacing="3" >' +   
                 '<tr class="headerRow" style="background-color: #99CCFF; font-weight: bold;">' +
                 '<td align="center">Name</td>' +
                 '<td align="center">Email</td>' +
                 '<td align="center">Direct Link</td>' +
                 //'<td align="center">Mobile Link</td>' +
                 '</tr>';
    htmlValue += tmp;
    tmp = '';
    Set<Id> userSet = new Set<Id>();
    userSet = managerIdsToUserIdsMap.get(managerId);
    List<User> userList = new List<User>();
    for(Id useId : userSet){
      userList.add(idToUserMap.get(useId));
    }
    for(User lstUsers : userList){
      Monthly_Forecast__c monthForecast = monthlyForecastMap.get(lstUsers.Id);
          tmp = '<tr> <td style="font-size: 11px"> <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/_ui/core/userprofile/UserProfilePage?u='+lstUsers.Id+'&tab=sfdc.ProfilePlatformFeed">'+lstUsers.Name +'</a></td>';
          tmp += '<td align="right" style="font-size:11px">' +lstUsers.Email+ '</td>';
          tmp += '<td align="right" style="font-size:11px"> <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/apex/SalesForecast?id='+lstUsers.Id+'&ForecastYR='+Date.Today().year()+'&managerView=true&BU='+monthForecast.Business_Unit__c+'">' + 'Forecast Link</a></td>';
          //tmp += '<td align="right" style="font-size:11px"> <a href="https://ap2.salesforce.com/apex/SalesForecast?id='+lstUsers.Id+'&ForecastYR='+Date.Today().year()+'">Test</a></td>';
          htmlValue += tmp +'</tr>';
          tmp = '';
    }
    
    //End the Table
    htmlValue += '</table>';      
    return htmlValue;
  }
  
  private Messaging.SingleEmailMessage sendEmail(String htmlValue, String managerId){
    emailTemplate = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE DeveloperName = 'Monthly_Forecast_Reminder_Manager']; 
    String htmlBody = emailTemplate.HtmlValue;
    htmlBody = htmlBody.replaceAll('<!\\[CDATA\\[', ''); // replace '<![CDATA['
    htmlBody= htmlBody.replaceAll('\\]\\]>', ''); // replace ']]'
    Messaging.SingleEmailMessage actualMail = new Messaging.SingleEmailMessage();
    Integer month = todaysDate.month();
    actualMail.setSubject('Missing Forecasts for '+monthStrings.get(month-1));
    actualMail.setHtmlBody(htmlBody.replace('[UnsubmittedForecast]', htmlValue));
    actualMail.setToAddresses(new List<String>{idToUserMap.get(managerId).Email});
   // actualMail.setToAddresses(new List<String>{'sbatra@appirio.com'});
    return actualMail;
  }
}