/**================================================================      
 * Appirio, Inc
 * Name: RestCalloutUtility
 * Description: Generic class to perform HTTP callouts 
 * Created Date: 04/08/2016
 * Created By: Parul Gupta (Appirio) for T-524385
 * 
 * Date Modified      Modified By                   Description of the update
 *
 ==================================================================*/
public class RestCalloutUtility {
   
   public static final String CONTENT_TYPE = 'application/json';
   public static final String CREATE_ORDER_NAMED_CREDENTIALS = 'callout:IC_Sales_Order';
   public static final String BLANK_RESPONSE = 'Response is blank';
    
    // Method to create order on oracle SOA with rest callout
    // @param Order ord 
    // @return String response Message
    public static String createOrderRestCallout(Order order) {
        
        // Create request objects
        informaticaRequest request = new informaticaRequest();
        salesOrderRequest salesOrderRequest = new salesOrderRequest();
        salesOrderRequest.IntegrationHeader = createIntegrationHeader();
        salesOrderRequest.orderHeader = createOrderHeader(order);
        request.salesOrderRequest = salesOrderRequest;
        
        // Convert apex to json string
        String body = JSON.serialize(request);
        
        // Replace tag with $t as order informatica service need this special $ symbol
        body = body.replace('tag', '$t');
        
        try { 
            // call sendRequest method
            String responseBody = sendPOSTRequest(CONTENT_TYPE, CREATE_ORDER_NAMED_CREDENTIALS, body);
            
            // Replace $t with tag as apex don't allow special $ symbol in variable name
            responseBody = responseBody.replace('$t', 'tag');
            
            // Return callout response
            return getCreateOrderResponseMessage(responseBody);
            
        }catch(CalloutException ex){
            
            // Return exception message
            return ex.getMessage();
        }
    }
    
    // Method returns Response Message from CreateOrder Rest callout
    public static String getCreateOrderResponseMessage(String responseBody){
        
        // JSON.deserializeUntyped because it was not getting parsed directly in apex class due to $ in some variables
        Map<String, Object> response = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
        Map<String, Object> salesOrderResponseMap = (Map<String, Object>)response.get('salesOrderResponse');
        system.debug('Response - salesOrderResponse ::' +salesOrderResponseMap);
        if (salesOrderResponseMap != null){
            salesOrderResponse salesOrderResponseObj = new salesOrderResponse();
            Map<String, Object> resCode = (Map<String, Object>)salesOrderResponseMap.get('responseCode');
            if (resCode != null){
                salesOrderResponseObj.responseCode = (String)resCode.get('tag');
            }
            Map<String, Object> resMsg = (Map<String, Object>)salesOrderResponseMap.get('responseMessage');
            if (resMsg != null){
                salesOrderResponseObj.responseMessage = (String)resMsg.get('tag');
            }
            system.debug('Response Message ::' + salesOrderResponseObj.responseMessage);
            return salesOrderResponseObj.responseMessage;
        }
        return BLANK_RESPONSE;
    }
    
    // Method to create Integration Header
    public static IntegrationHeader createIntegrationHeader(){
        IntegrationHeader header = new IntegrationHeader();
        header.SenderIdentifier = new TagName(Label.Endo_SFDC);
        header.ReceiverIdentifier = new TagName(Label.Endo_ENDERP);
        return header;
    }
    
    // Method to create Order Request
    public static orderHeader createOrderHeader(Order order){
        orderHeader orderHeader = new orderHeader();
        orderHeader.operation = new TagName(Label.Endo_Create);
        orderHeader.businessUnit = new TagName(Label.Endo_Order_BU);
        orderHeader.orderType = new TagName(order.Type);
        orderHeader.orderSource = new TagName(Label.Endo_SFDC);
        //orderHeader.priceList = new TagName(order.Pricebook2.Name);
        //orderHeader.priceList = new TagName('Endo List Price');
        orderHeader.priceList = new TagName(Label.Endo_Price_List);
        orderHeader.soldFromOrgId = new TagName('0');
        orderHeader.soldToOrgId =new TagName(order.Account.AccountNumber);
        if (order.Ship_To_Address__c != null && order.Ship_To_Address__r.Location_ID__c != null){      
           orderHeader.shipToOrgId = new TagName(order.Ship_To_Address__r.Location_ID__c);
        }
        if (order.Bill_To_Address__c != null && order.Bill_To_Address__r.Location_ID__c != null){
           orderHeader.invoiceToOrgId = new TagName(order.Bill_To_Address__r.Location_ID__c);
        }
        orderHeader.salesrepId = new TagName(order.Salesrep_Name__c);
        orderHeader.paymentTerm = new TagName(order.Payment_Terms__c);
        orderHeader.origSysDocumentRef = new TagName(order.id);
        orderHeader.custPoNumber = new TagName(order.PoNumber);
        orderHeader.creditCheckHold = new TagName(String.valueof(order.Credit_Hold__c));
        return orderHeader;
    }
     
    // Http callout with Named Credentials
    public static String sendPOSTRequest(String contentType, String namedCredentials, String body){
        String responseBody;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(namedCredentials);
        req.setMethod('POST');
        req.setHeader('Content-Type', contentType);
        req.setBody(body);
        System.debug('Create ORDER REST Request :: '+req.getBody());
        req.setTimeout(120000);
        Http http = new Http();
        if (!Test.isRunningTest()){
            HTTPResponse res = http.send(req);
            responseBody = res.getBody();
        } else{
             responseBody = '{"salesOrderResponse":{"xmlns":"http://schemas.stryker.com/SalesOrder/V1",'
                            + '"xmlns$client":"http://xmlns.oracle.com/SalesOrder/CommonSalesOrderSource/ReceiveSalesOrderBPELProcess",'
                            + '"xmlns$env":"http://schemas.xmlsoap.org/soap/envelope/","xmlns$ns0":"http://schemas.stryker.com/SalesOrder/V1",'
                            + '"xmlns$ns1":"http://schemas.stryker.com/common/V1","xmlns$plnk":"http://docs.oasis-open.org/wsbpel/2.0/plnktype",'
                            + '"xmlns$wsa":"http://www.w3.org/2005/08/addressing","xmlns$wsdl":"http://schemas.xmlsoap.org/wsdl/",'
                            + '"IntegrationHeader":{"xmlns":"http://schemas.stryker.com/common/V1","MessageID":{},"CreationDateTime":{},"SenderIdentifier":{"$t":"ENDERP"},"ReceiverIdentifier":{"$t":"SFDC"}},'
                            + '"responseCode":{"$t":"Success"},'
                            + '"responseMessage":{"$t":"Batch id 59 is processed sucessfully."},'
                            + '"orderHeaderResponse":{}}}';
        }
        System.debug('Create ORDER REST Response :: '+responseBody);
        return responseBody;
    }
   
    // Class definition - Informatica Request
    public class informaticaRequest{
        public salesOrderRequest salesOrderRequest;
        public String xmlns = 'http://schemas.stryker.com/SalesOrder/V1';
    }
    
    // Class definition - TagName to support namespace - &t in json request
    public class TagName {
        public String tag;
        public TagName(String tag){
            this.tag = tag;
        }
        public TagName(){}
    }

    // Class definition - salesOrderRequest
    public class salesOrderRequest{
        public IntegrationHeader IntegrationHeader;
        public orderHeader orderHeader;
        public String xmlns = 'http://schemas.stryker.com/common/V1';
     }
     
    // Class definition - IntegrationHeader
    public class IntegrationHeader{
        public TagName MessageID;
        public TagName CreationDateTime;
        
        public TagName SenderIdentifier;
        public TagName ReceiverIdentifier;
     }
   
    // Class definition - orderHeader
    public class orderHeader{
        public String xmlns = 'http://schemas.stryker.com/SalesOrder/V1';
        public TagName operation;
        public TagName businessUnit;
        public TagName orderType;
        public TagName orderSource;
        public TagName origSysDocumentRef;
        public TagName priceList;
        public TagName custPoNumber;
        public TagName paymentTerm;
        public TagName soldToOrg;
        public TagName soldFromOrgId;
        public TagName soldToOrgId;
        public TagName shipToOrgId;
        public TagName invoiceToOrgId;
        public TagName salesrepId;
        public TagName creditCheckHold;
    }
    
    
    // Class definition - salesOrderResponse 
    public class salesOrderResponse {
        public String xmlns;
        public String xmlns_client;
        public String xmlns_env;
        public String xmlns_ns0;
        public String xmlns_ns1;
        public String xmlns_plnk;
        public String xmlns_wsa;
        public String xmlns_wsdl;
        public IntegrationHeader IntegrationHeader;
        public String responseCode;
        public String responseMessage;
        public orderHeaderResponse orderHeaderResponse;
    }
    
    public class orderHeaderResponse{
        
    }
    
}