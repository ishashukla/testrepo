/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Flex_CreditNotesControllerTest {

    static testMethod void myUnitTest() {
         Account testAcc = (TestUtils.createAccount(1,true)).get(0);
         TestUtils.createCommConstantSetting();
         Opportunity testOpp = new Opportunity();
         testOpp.AccountId = testAcc.Id;
         testOpp.Name = 'Test Opportunity';
         testOpp.StageName = 'Qualified';
         testOpp.Term__c = 12;
         testOpp.Payment_Frequency__c = 'Monthly';
         testOpp.Deal_Type__c = 'Lease - standard';
         testOpp.End_Of_Term__c = 'FMV';
         testOpp.Type = 'New Business';
         testOpp.Probability = 90;
         testOpp.CloseDate = Date.today();
         Schema.RecordTypeInfo rtByNameFlex = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional');
         Id recordTypeFlex = rtByNameFlex.getRecordTypeId();
         testOpp.recordTypeId = recordTypeFlex;
         insert testOpp;
         
         Flex_Credit__c flexCredit = new Flex_Credit__c();
         flexCredit.Opportunity__c = testOpp.Id;
         insert flexCredit;
         
         Credit_Notes__c creditNotes = new Credit_Notes__c();
         creditNotes.Opportunity__c = testOpp.Id;
         creditNotes.Credit_Notes__c = 'Test Credit Notes';
         
         insert creditNotes;
         
         Test.startTest();
            ApexPages.currentPage().getParameters().put('Id',flexCredit.Id);
            Flex_CreditNotesController controller = new Flex_CreditNotesController(new ApexPages.StandardController(flexCredit));
            controller.createCreditNotes();
                     System.assertEquals(controller.creditNotes.get(0).opportunityName,'Test Opportunity');
             Test.stopTest();
    }
}