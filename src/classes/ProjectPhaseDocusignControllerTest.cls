@isTest
private class ProjectPhaseDocusignControllerTest {

	private static testMethod void TestController() {
	  List<Account> accountList = TestUtils.createAccount(1,True);
	  List<Project_Plan__c> projects =  TestUtils.createProjectPlan(1, accountList[0].Id, True);
	  List<Project_Phase__c> lstPhases1 = TestUtils.createProjectPhase(1,projects.get(0).Id,true);
	  ContentVersion conv=new ContentVersion();
	  ApexPages.StandardController sc = new ApexPages.StandardController(conv);
      ProjectPhaseDocusignController projectController = new ProjectPhaseDocusignController(sc);
      PageReference pgRef = projectController.redirectToProjectPacket();
      PageReference pageRef = projectController.redirectToStandardDocusignPage();
	}

}