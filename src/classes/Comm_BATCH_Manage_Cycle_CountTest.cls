/**================================================================      
* Appirio, Inc
* Name: [Comm_BATCH_Manage_Cycle_CountTest]
* Description: [Test class for Comm_BATCH_Manage_Cycle_Count]
* Created Date: [22-11-2016]
* Created By: [Meha Simlote] (Appirio)
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class Comm_BATCH_Manage_Cycle_CountTest {
    static SVMXC__Service_Group_Members__c serviceMember;
    static SVMXC__Service_Group_Members__c serviceMember2;
    static List<Product2> productList;
     static User manager;
    static SVMXC__Site__c location;
    static Set<Id> productIdSet;
    static SVMXC__Service_Group__c serviceGroup;
    @isTest static void testControllerFirst() {
        Test.startTest();
         createData();
        Cycle_Count__c varCycleCount = new Cycle_Count__c(name=System.today().format()+' : '+Userinfo.getName(),status__c = 'New',All_Technician__c=true);
        Comm_BATCH_Manage_Cycle_Count batch = new Comm_BATCH_Manage_Cycle_Count(varCycleCount,productIdSet);
        Database.executeBatch(batch);
       Test.stopTest();
        //System.assertEquals([SELECT Id FROM Cycle_Count__c WHERE Location__c = :location.Id].size() > 0,true);
        
    }
     @isTest static void testControllerSecond() {
        Test.startTest();
         createData();
        Cycle_Count__c varCycleCount = new Cycle_Count__c(name=System.today().format()+' : '+Userinfo.getName(),status__c = 'New',
                                                          All_Technician__c=true);
        Comm_BATCH_Manage_Cycle_Count batch = new Comm_BATCH_Manage_Cycle_Count(varCycleCount,null);
        Database.executeBatch(batch);
       Test.stopTest();
          //System.assertEquals([SELECT Id FROM Cycle_Count__c WHERE Location__c = :location.Id].size() > 0,true);
    }
   private static void createData() {
        manager = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User tech = TestUtils.createUser(1, 'System Administrator', false).get(0);
        tech.ManagerId  = manager.Id;
        insert tech;
        productList = new List<Product2>();
        for(integer i = 0 ; i < 2; i++) {
            Product2 productRecod = new Product2(Name='test'+i , IsActive=true,Include_in_cycle_counts__c=true);
            productList.add(productRecod);
        }
        insert productList;
       productIdSet = new Set<Id>();
       for(Product2 prod : productList) {
           productIdSet.add(prod.Id);
       }
       location = new SVMXC__Site__c(SVMXC__Stocking_Location__c=true,
                                                     Location_ID__c='1234',
                                                    Exclude_Location_from_Inventory__c =false);
        insert location;
        serviceGroup = new SVMXC__Service_Group__c(Name='group',SVMXC__Active__c=true);
        insert serviceGroup;
        serviceMember = new SVMXC__Service_Group_Members__c(
            SVMXC__Service_Group__c = serviceGroup.Id,Name = 'testuser',
            SVMXC__Inventory_Location__c = location.Id,SVMXC__Salesforce_User__c = tech.Id,svmxc__Active__c=true
        );
        insert serviceMember;
        serviceMember2 = new SVMXC__Service_Group_Members__c(
            SVMXC__Service_Group__c = serviceGroup.Id,Name = 'testuser2',
            SVMXC__Inventory_Location__c = location.Id,svmxc__Active__c=true,SVMXC__Salesforce_User__c = tech.Id
        );
        insert serviceMember2;
        List<SVMXC__Product_Stock__c> productStockList = new List<SVMXC__Product_Stock__c>();
        for(integer i=0;i<2;i++) {
            SVMXC__Product_Stock__c stockProduct = new SVMXC__Product_Stock__c(
                SVMXC__Location__c = location.Id,
                SVMXC__Product__c = productList[0].Id,
                On_Hand_Qty__c = 5,SVMXC__Quantity2__c=5
            );
            productStockList.add(stockProduct);
        }
        insert productStockList;
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
    }
}