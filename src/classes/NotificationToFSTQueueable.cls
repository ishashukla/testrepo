/**==================================================================================================================================
 * (c) 2016 Appirio, Inc
 * Name: NotificationToFSTQueueable
 * Description: Queueable class that will send email to FST Owner
 * Created Date: Nov 14, 2016
 * Created By: Isha Shukla (Appirio)
 * 
 * Nov 15th, 2016               Isha Shukla                       T-513555 - created
 * Dec 1st , 2016               Isha Shukla                       I-245792 - Modified
 ==========================================================================================================================================*/
public class NotificationToFSTQueueable implements Queueable {
    Set<Id> shippedOrderItemIdsSet = new Set<Id>();
    Id rmaRT = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId();
    Id shipmentRT = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
    Set<Id> rtSet = new Set<Id>{rmaRT,shipmentRT};
    public NotificationToFSTQueueable(Set<Id> shippedOrderItemSet) {
        this.shippedOrderItemIdsSet = shippedOrderItemSet;
    }
    public void execute(QueueableContext context) {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Map<Id,List<SVMXC__RMA_Shipment_Line__c>> partLineToRecords = new Map<Id,List<SVMXC__RMA_Shipment_Line__c>>();
        for(SVMXC__RMA_Shipment_Line__c partOrderLine : [SELECT SVMXC__RMA_Shipment_Order__r.Owner.Profile.Name,Order_Product__r.Tracking_Numbers__c ,
                                                         SVMXC__RMA_Shipment_Order__r.Owner.Email, Order_Product__c,Order_Product__r.OrderItemNumber ,
                                                         SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__r.Name ,
                                                         SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c,SVMXC__RMA_Shipment_Order__c
                                                         FROM SVMXC__RMA_Shipment_Line__c 
                                                         WHERE SVMXC__RMA_Shipment_Order__r.RecordTypeId IN :rtSet 
                                                         AND Order_Product__c IN :shippedOrderItemIdsSet]) {
                                                             System.debug('partOrderLine.SVMXC__RMA_Shipment_Order__r.Owner.Profile.Name>>'+partOrderLine.SVMXC__RMA_Shipment_Order__r.Owner.Profile.Name);
                                                             if(partOrderLine.SVMXC__RMA_Shipment_Order__r.Owner.Profile.Name == 'Field Service Technician - COMM US' || 
                                                                partOrderLine.SVMXC__RMA_Shipment_Order__r.Owner.Profile.Name == 'Field Service Technician Manager - COMM US') {
                                                                    if(!partLineToRecords.containsKey(partOrderLine.SVMXC__RMA_Shipment_Order__c)) {
                                                                        partLineToRecords.put(partOrderLine.SVMXC__RMA_Shipment_Order__c,new List<SVMXC__RMA_Shipment_Line__c>{partOrderLine});
                                                                    } else {
                                                                        partLineToRecords.get(partOrderLine.SVMXC__RMA_Shipment_Order__c).add(partOrderLine);
                                                                    }
                                                                } 
                                                         }
        System.debug('partLineToRecords>>'+partLineToRecords);
        Id vfTemplateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName='FST_Order_Shipped_VF'].Id;
        if(partLineToRecords != null) {
            for(Id pId : partLineToRecords.keySet()) {
                for(SVMXC__RMA_Shipment_Line__c partLine : partLineToRecords.get(pId)) {
                    String emailID = partLine.SVMXC__RMA_Shipment_Order__r.Owner.Email;
                    Messaging.SingleEmailMessage mail = createEmailMessage(vfTemplateId,partLine.SVMXC__RMA_Shipment_Order__r.OwnerId,partLine.Id,new List<String> {emailID});
                	mailList.add(mail);
                }
            }   
        }
        system.debug('mailList>>'+mailList);
        if(!Test.isRunningTest()) {
            if(mailList.size() > 0) {
                Messaging.sendEmail(mailList);
            }
        }
    }
    private Messaging.SingleEmailMessage createEmailMessage(Id templateId,Id receiverId,Id relatedObjectId,List<String> toEmailAddresses) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(templateId);
        mail.setTargetObjectId(receiverId);
        mail.setToAddresses(toEmailAddresses);
        mail.setSaveAsActivity(false);
        mail.setWhatId(relatedObjectId);
        return mail;
    }
}