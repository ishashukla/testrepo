/**================================================================      
* Appirio, Inc
* Name: COMM_RenewWarrantiesControllerTest
* Description: Test class for COMM_RenewWarrantiesController
* Created Date: 18 Nov 2016
* Created By: Meha Simlote (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest(SeeAllData=true)
private class COMM_RenewWarrantiesControllerTest {
    @isTest static void testController() {
        Product2 productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pricebook = [select id,Name  from Pricebook2  where name = 'COMM Price Book'];
        if(pricebook == null) {
            pricebook = new PriceBook2(Name='COMM Price Book');
            insert pricebook;
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Account acc = new Account(Name='TestAccount1',AccountNumber='1234');
        insert acc;
        SVMXC__Installed_Product__c product = new SVMXC__Installed_Product__c(SVMXC__Company__c=acc.id,
                                                                             SVMXC__Product__c = productRecord.Id);
        insert product;
        List<SVMXC__Warranty__c> WarrantyList = new List<SVMXC__Warranty__c>();
        for(integer i=0;i<2;i++) {
            SVMXC__Warranty__c warranty = new SVMXC__Warranty__c(
                SVMXC__End_Date__c=system.today()+1,
                
                SVMXC__Installed_Product__c=product.Id
            );
            WarrantyList.add(warranty);
        } 
        insert WarrantyList;
        Test.startTest();
        PageReference pageRef = Page.COMM_RenewWarranties;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id' , acc.Id);
        COMM_RenewWarrantiesController controller = new COMM_RenewWarrantiesController();
        controller.gv_list_final[0].selected = true;
        controller.CreateOpportuntiy();  
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Opportunity WHERE Name Like 'TestAccount1%'].size() > 0, true);
    }
    
    
    @isTest static void testController2() {
        Product2 productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pricebook = [select id,Name  from Pricebook2  where name = 'COMM Price Book'];
        if(pricebook == null) {
            pricebook = new PriceBook2(Name='COMM Price Book');
            insert pricebook;
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Account acc = new Account(Name='TestAccount1',AccountNumber='1234');
        insert acc;
        SVMXC__Installed_Product__c product = new SVMXC__Installed_Product__c(SVMXC__Company__c=acc.id,
                                                                             SVMXC__Product__c = productRecord.Id);
        insert product;
        List<SVMXC__Warranty__c> WarrantyList = new List<SVMXC__Warranty__c>();
        for(integer i=0;i<2;i++) {
            SVMXC__Warranty__c warranty = new SVMXC__Warranty__c(
                SVMXC__End_Date__c=system.today()+1,
                
                SVMXC__Installed_Product__c=product.Id
            );
            WarrantyList.add(warranty);
        } 
        insert WarrantyList;
        Test.startTest();
        PageReference pageRef = Page.COMM_RenewWarranties;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id' , acc.Id);
        COMM_RenewWarrantiesController controller = new COMM_RenewWarrantiesController();
        controller.CreateOpportuntiy();  
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Opportunity WHERE Name Like 'TestAccount1%'].size() > 0, false);
    }
    @isTest static void testController3() {
        Product2 productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pricebook = [select id,Name  from Pricebook2  where name = 'COMM Price Book'];
        if(pricebook == null) {
            pricebook = new PriceBook2(Name='COMM Price Book');
            insert pricebook;
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Account acc = new Account(Name='TestAccount1',AccountNumber='1234');
        insert acc;
        SVMXC__Installed_Product__c product = new SVMXC__Installed_Product__c(SVMXC__Company__c=acc.id);
        insert product;
        List<SVMXC__Warranty__c> WarrantyList = new List<SVMXC__Warranty__c>();
        for(integer i=0;i<2;i++) {
            SVMXC__Warranty__c warranty = new SVMXC__Warranty__c(
                SVMXC__End_Date__c=system.today()+1,
                
                SVMXC__Installed_Product__c=product.Id
            );
            WarrantyList.add(warranty);
        } 
        insert WarrantyList;
        Test.startTest();
        PageReference pageRef = Page.COMM_RenewWarranties;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id' , acc.Id);
        COMM_RenewWarrantiesController controller = new COMM_RenewWarrantiesController();
        controller.gv_list_final[0].selected = true;
        controller.CreateOpportuntiy();  
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Opportunity WHERE Name Like 'TestAccount1%'].size() > 0, true);
    }
    
    
}