//
// (c) 2014 Appirio, Inc.
// Class SystemLookupPopUpController
// Controller class for SystemLookupPopUp Page
//
// 01 Mar 2016	   Sahil Batra		Original(T-481991)
public class SystemLookupPopUpController {
	public List<System__c> listRecords {get;set;}
	public String businessUnit{get;set;}
	public String systemType{get;set;}
	public List<String> buList = new List<String>();
	public String searchText{get;set;}
	public SystemLookupPopUpController(){
		listRecords = new List<System__c>();
		businessUnit = ApexPages.currentPage().getParameters().get('bu');
		systemType = ApexPages.currentPage().getParameters().get('type');
		buList = businessUnit.split('\\*');
		listRecords = [SELECT Name, Business_Unit__c , Description__c, 	Type__c 
					   FROM System__c
					   WHERE Business_Unit__c IN :buList
					   AND Type__c =:systemType LIMIT 10];
	}
	
	public void runSearch(){
		listRecords = new List<System__c>();
		String temp  = '%'+searchText+'%';
		listRecords = [SELECT Name, Business_Unit__c , Description__c, 	Type__c 
					   FROM System__c
					   WHERE Business_Unit__c IN :buList
					   AND ( Name Like :temp OR Business_Unit__c Like :temp)
					   AND Type__c =:systemType];
	}
}