@isTest
public class EmailCombinedProposalTest {
   
   public static testMethod void testSendEmail() {
        
        Account acct = new Account();
        acct.Name = 'Phoenix test Account for testSendEmail';
        insert acct;
        TestUtils.createCommConstantSetting();
        Opportunity opty = new Opportunity();
        opty.Name = 'Phoenix test Opportunity for testSendEmail';
        opty.StageName = 'Prospecting';
        opty.CloseDate = Date.today();
       	opty.AccountId = acct.Id;
        insert opty;
       
       Document__c dc = new Document__c();
       dc.Name = 'PHX Document_c Name';
       dc.Opportunity__c = opty.Id;
       insert dc;
                
       Attachment att = new Attachment();
       att.Body = Blob.valueOf(' ');
       att.ParentId = dc.Id;
       att.Name = 'PHX Doc Name';
       insert att;
       
        EmailCombinedProposal controller = null;
        
        ApexPages.currentPage().getParameters().put('opptyId', opty.Id);
        ApexPages.currentPage().getParameters().put('accountId', acct.Id);
        ApexPages.currentPage().getParameters().put('keyprefix', '006');
        ApexPages.currentPage().getParameters().put('relatedTo', opty.Name);
       
        try {
            controller = new EmailCombinedProposal();
        } catch (Exception e) {
            System.assert(false);
        }
        
       controller.additionalTo = 'example@stryker.com';
       controller.to = 'example@stryker.com';
       controller.cc = 'example@stryker.com';
       controller.bcc = 'example@stryker.com';
       controller.subject = 'Email Subject';
       controller.message = 'Email Message';
      
       System.assert(controller.getSattachments().size() > 0);
       System.assert(controller.objList.size() > 0);
       System.assert(controller.emailAddresses.size() > 0);
    
       controller.attachmentIds = att.Id;
                
        System.assert(null != controller.sendEmail());       
    }    
    
}