/*
Name            ProjectPlanTriggerHandlerTest
Purpose         T-490786 (Test class for ProjectPlanTriggerHandler)
Created Date    04/26/2016
Created By      Nitish Bansal
*/

@isTest
private class ProjectPlanTriggerHandlerTest
{

    private static final String PM_PROFILE = 'Service Project Manager - COMM US';
    private static List<Custom_Account_Team__c> customAccTeamList;

    private static List<Custom_Account_Team__c> createCustomAccountTeam(Integer customAccTeamCount,Id accId, Id userId,String role,Boolean isDel ,Boolean isInsert) {
        customAccTeamList = new List<Custom_Account_Team__c>();
        for(Integer i = 0; i < customAccTeamCount; i++) {
          Custom_Account_Team__c customAccTeamRecord = new Custom_Account_Team__c(Account__c = accId,User__c = userId,Team_Member_Role__c = role,IsDeleted__c = isDel);
          customAccTeamList.add(customAccTeamRecord);
        }
        if(isInsert){
          insert customAccTeamList;
        }
        return customAccTeamList;
      }

    @isTest
    static void itShould()
    {
        List<User> userList = TestUtils.createUser(2, PM_PROFILE , true);
        List<Account> accountList = TestUtils.createAccount(2,True);
        customAccTeamList = createCustomAccountTeam(3, accountList[0].Id, userList[0].Id, Constants.PROJECT_MANAGER, false, false); 
        insert customAccTeamList;

        //Insert scenario
        List<Project_Plan__c> projects =  TestUtils.createProjectPlan(2, accountList[0].Id, false);
        insert projects;
        System.assertEquals(3,[SELECT Name From Custom_Account_Team__c Where Account__c = :accountList.get(0).Id].size());
        System.assertEquals(2,[SELECT Id From Project_Plan__Share WHERE AccessLevel = 'Edit'].size());

       projects = new List<Project_Plan__c>();
        for(Project_Plan__c proj : [Select Id, Account__c from Project_Plan__c] ){
            if(proj.Account__c != accountList.get(1).Id){
                proj.Account__c = accountList.get(1).Id;
                projects.add(proj);
                break;
            }
        }
        system.assertNotEquals(0, projects.size());
        update projects;
        System.assertEquals(0,[SELECT Name From Custom_Account_Team__c Where Account__c = :accountList.get(1).Id].size());
        System.assertEquals(2,[SELECT Id From Project_Plan__Share WHERE AccessLevel = 'Edit'].size());
    }
}