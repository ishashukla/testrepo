// (c) 2016 Appirio, Inc.
//
// Class Name: StrategicAccountTriggerHandler
// Description: Handler Class for StrategicAccountTrigger.
// 
// June 22 2016, Isha Shukla  Original (T-513125)
//
public class StrategicAccountTriggerHandler {
    // Method to assign currently logged in user id to User field of Strategic Account object
    public static void populateUserOnStrategicAccount(List<Strategic_Account__c> triggerNewList) {
        for(Strategic_Account__c stAccount : triggerNewList) {
            stAccount.User__c = UserInfo.getUserId();
        } 
    }
}