//
// (c) 2015 Appirio, Inc.
//
// Apex Controller test Class for Visualforce component Intr_AutoComplete 
//
// This Component is using by CustomLookUpCtrl Component.
//
// Created by : Shreerath Nair (Appirio)
@isTest
private class Intr_AutoCompleteController_Test {

     static testMethod void testAutocomplete() {    
        
        Profile pro = [SELECT Id,Name FROM Profile WHERE Name='Instruments CCKM']; 
        List<User> us = Intr_TestUtils.createUser(1,pro.Name,false);
        us[0].isActive = true;
        us[0].Division = 'ProCare';
        us[0].Title = 'Mr.';
        insert us;
        
        
        Test.startTest();
        	List<User> listUser = new List<User>();
	        listUser = (List<User>) Intr_AutoCompleteController.findSObjects('User','Testing','Name',pro.Id,pro.Id);
	        //System.assert(listUser[0].Name == 'Testing0',true);
   			listUser = (List<User>) Intr_AutoCompleteController.findSObjects('User','Testing','Name',null,null);
        Test.stopTest();
    }
}