/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_OrderIntermediateExtensionsTest
//
// April 24, 2016 , Rahul Aeran(Appirio JDC)
//
// Test class for Medical_OrderIntermediateExtensions
*/
@isTest
private class Medical_OrderIntermediateExtensionsTest {
  private static List<Account> accounts;
  private static List<Contact> contacts;
  private static Case cs;
  @isTest
  private static void testRelatedTo() {
	  createTestData(4);
	  ApexPages.currentPage().getParameters().put('contact_id' , contacts.get(0).id);
		ApexPages.currentPage().getParameters().put('case_id' , cs.id);
		Medical_OrderIntermediateExtensions orderExtensions = new Medical_OrderIntermediateExtensions();
		orderExtensions.redirectPage();
		orderExtensions.selectedAccId  = accounts.get(0).id;
		orderExtensions.fieldName = 'AccountId';
		orderExtensions.getSelectedRecord();
		orderExtensions.next();
		orderExtensions.cancel();
  }

	@isTest
	private static void testRelatedTo1() {
		createTestData(4);
		accounts.get(0).Billing_Type__c = 'Ship To Address Only';
		upsert accounts;
		ApexPages.currentPage().getParameters().put('contact_id' , contacts.get(0).id);
		ApexPages.currentPage().getParameters().put('case_id' , cs.id);
		Medical_OrderIntermediateExtensions orderExtensions = new Medical_OrderIntermediateExtensions();
		orderExtensions.redirectPage();
		orderExtensions.selectedAccId  = accounts.get(0).id;
		orderExtensions.fieldName = 'AccountId';
		orderExtensions.getSelectedRecord();
		orderExtensions.next();
		orderExtensions.cancel();
  }

	@isTest
	private static void testRelatedTo2() {
    createTestData(4);
    accounts.get(0).Billing_Type__c = 'Bill To and Ship To Address';
    upsert accounts;
    ApexPages.currentPage().getParameters().put('contact_id' , contacts.get(0).id);
    ApexPages.currentPage().getParameters().put('case_id' , cs.id);
    Medical_OrderIntermediateExtensions orderExtensions = new Medical_OrderIntermediateExtensions();
    orderExtensions.redirectPage();
    orderExtensions.selectedAccId  = accounts.get(0).id;
    orderExtensions.fieldName = 'AccountId';
    orderExtensions.getSelectedRecord();
    orderExtensions.next();
    orderExtensions.cancel();
  }

  @isTest
  private static void testNoAssociation() {
    createTestData(0);
    ApexPages.currentPage().getParameters().put('contact_id' , contacts.get(0).id);
    Medical_OrderIntermediateExtensions orderExtensions = new Medical_OrderIntermediateExtensions();
    orderExtensions.redirectPage();
    orderExtensions.selectedAccId  = accounts.get(0).id;
    orderExtensions.fieldName = 'AccountId';
    orderExtensions.getSelectedRecord();
    orderExtensions.next();
    orderExtensions.cancel();

  }

  @isTest
  private static void testBillToFields() {
    createTestData(0);
    ApexPages.currentPage().getParameters().put('contact_id' , contacts.get(0).id);
    Medical_OrderIntermediateExtensions orderExtensions = new Medical_OrderIntermediateExtensions();
    orderExtensions.redirectPage();
    orderExtensions.selectedAccId  = accounts.get(0).id;
    orderExtensions.fieldName = 'Bill_To_Account__c';
    orderExtensions.getSelectedRecord();
    orderExtensions.next();
    orderExtensions.cancel();

    orderExtensions.fieldName = 'Ship_To_Account__c';
    orderExtensions.getSelectedRecord();
    orderExtensions.next();
    orderExtensions.cancel();

    //setting bill to account name as null

    orderExtensions.billToAccount_Name = accounts.get(0).Name;
    orderExtensions.billToAccount_Id = null;
    orderExtensions.getSelectedRecord();
    orderExtensions.next();
    orderExtensions.cancel();

    orderExtensions.billToAccount_Name = accounts.get(0).Name;
    orderExtensions.billToAccount_Id = accounts.get(0).Id;
    orderExtensions.shipToAccount_Name = accounts.get(0).Name;
    orderExtensions.shipToAccount_Id = null;
    orderExtensions.fieldName = 'Bill_To_Account__c';
    orderExtensions.getSelectedRecord();
    orderExtensions.next();
    orderExtensions.cancel();

  }

  private static void createTestData(Integer count){
		accounts = Medical_TestUtils.createAccount(count + 1, false);
		accounts.get(0).Billing_Type__c = 'Bill To Address Only';
		insert accounts;
		contacts = Medical_TestUtils.createContact(2, accounts.get(0).id , true);
		List<Contact_Acct_Association__c> contactAccAssociations = new List<Contact_Acct_Association__c>();
		for(integer i = 1 ; i <= count; i++) {
	    contactAccAssociations.add(new Contact_Acct_Association__c(Associated_Contact__c = contacts.get(0).id, Associated_Account__c = accounts.get(i).id, Account_Contact_Id__c = contacts.get(0).id + '~' + accounts.get(i).id));
		}
		insert contactAccAssociations;

    Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
    TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
		cs = Medical_TestUtils.createCase(10, accounts.get(0).Id, contacts.get(0).Id, false).get(0);
    cs.RecordTypeId = MedicalrecordTypeId;
    insert cs;

  }

}