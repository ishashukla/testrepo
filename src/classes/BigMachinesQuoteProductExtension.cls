public class BigMachinesQuoteProductExtension {

    // URLs for Visualforce pages
    private String bmRedirectURL = '';
    
    private BigMachines__Quote_Product__c bmQuoteProd;

    public BigMachinesQuoteProductExtension(ApexPages.StandardController stdCtrl) {
        bmQuoteProd = (BigMachines__Quote_Product__c)stdCtrl.getRecord();
        bmRedirectURL = '/apex/BM_EditQuote?id=' + bmQuoteProd.BigMachines__Quote__c;
    }

    public String getRedirectURL() {
        return bmRedirectURL;
    } 
}