//
// (c) 2015 Appirio, Inc.
//
//
// Utility Class to send Cases to Trackwise using SOAP Call out
//
// Mar 17, 2016    sunil             T-485162
// Aug 9, 2016     Audrey Carson     C-00172507 - Attachment support
// Sep  18, 2016   Shreerath         I-235837 - Updated the sendToTrackWise() & trackwiseCallout() function to check if case is of Endo or Medical
// Sep 27, 2016    Shreerath         I-237054 - Updated trackwiseCallout() to add quantity from Case_Asset__c object. 
// Sep 27, 2016    Shreerath         I-237053 - Include divisionalAccountNumber from RQA into TW Complaint for Endo Cases only
// Sep 27, 2016    Nathalie Le Guay  I-237049 && I-237050 - update to trackwiseCallout() to keep reprocessedProducts and productsToBeReturned blank for Endo
//                                   I-237051 - update to trackwiseCallout() to populate salesRep value with RQA.Case.Sales_Rep__c.Name
//                                   I-237052 - update to trackwiseCallout() to populate initialReporterFacilityName value with RQA.Account_Name__c
// Oct 07, 2016    Audrey Carson     C-00172279 - update to trackwiseCallout() to pupulate initialReporterFacilityName, salesRep, and divisionalAccountNumber for medical cases
public class Medical_TW_Utility {

  // I-235837(SN) 18 Sept 2016 - List of CaseAsset junction object(Endo)
  public static List<Case_Asset__c> listCaseAsset;
  public static Set<String> recordTypeId_EndoSet = new Set<String>();
  public static Set<String> recordTypeNamesEndo = new Set<String>{'Customer Inquiry & Requests',
                                                  'RMA Order',
                                                  'Repair Order Billing',
                                                  'Sales Order',
                                                  'Sample Refurb Sale / Sample Sale',
                                                  'Samples Request',
                                                  'Tech Support'};
  //----------------------------------------------------------------------------------------------
  // Calling Trackwise service
  //----------------------------------------------------------------------------------------------
  // I-235837(SN) 18 Sept 2016 -  Included Case RecordTYpe field as well to check if its Medical or Endo Case
  // I-237053(SN) 27 Sept 2016 -  Included AccountNumber field 
  public static void invokeTrackWiseWebService(Set<Id> setIds){
    //ACARSON C-00172279 Oct.07.2016 Added Case__r.CreatedBy.Name
    List<Regulatory_Question_Answer__c> lst = [SELECT Id, Account_Number__c, Name, Case__c,case__r.RecordTypeId, SenderIdentifier__c, ReceiverIdentifier__c, CIC__c, Answer_1__c,
                                               Answer_3__c, Answer_4__c, Answer_5_New__c, Answer_8__c, Answer_7_New__c,
                                               Answer_9__c, Answer_10__c, Answer_11__c, Additional_Details__c, Answer_14_New__c,
                                               Answer_17_New__c, Answer_18_New__c, Answer_12_New__c, Answer_15_New__c, Answer_16_New__c,
                                               Intake_source__c, Country_of_event__c, Initial_reporter_name__c, Initial_reporter_address__c,
                                               Initial_reporter_city__c, Initial_reporter_country__c, Initial_reporter_phone__c,
                                               Initial_reporter_type__c, Quantity__c, Serial_number__c, CreatedDate,
                                               Initial_reporter_postal_code__c, Initial_reporter_state__c, Answer_13_New__c, Answer_2_New__c,
                                               Account_Name__c, Sales_Rep__c,Case__r.CreatedBy.Name // NLG - 27 Sept 2016 - Account_Number__c 
                                               FROM Regulatory_Question_Answer__c
                                               Where Id IN :setIds];


    for(Regulatory_Question_Answer__c rqa :lst){
        sendToTrackWise(rqa);
    }
  }
  
  private static void setEndoCaseRT() {
    Map<String, Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Case.getRecordTypeInfosByName();
    for (Schema.RecordTypeInfo rti : rtMapByName.values()) {
      if (recordTypeNamesEndo.contains(rti.getName())) {
        recordTypeId_EndoSet.add(rti.getRecordTypeId());
      }
    }
  }

  //----------------------------------------------------------------------------------------------
  // Calling Trackwise service
  //----------------------------------------------------------------------------------------------
    private static void sendToTrackWise(Regulatory_Question_Answer__c objRQA) {
    
    setEndoCaseRT();
    
    List<Case_Asset_Junction__c> lstAssets;
    // I-235837(SN) 18 Sept 2016 -  get recordtypeid of medical case
    ID medicalRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.MEDICAL_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId();
    // Fetch related Assets
    //CODE MODIFIED BY Vipul Jain 30-08-2016 Case no. 176991  Asset__r.Asset_Description__c,Asset__r.Description added to the query
    
    // I-235837(SN) 18 Sept 2016 - Check if the Regulatory question Associated with Medical Case
    if(medicalRecordTypeId == objRQA.Case__r.RecordTypeID){
    
        lstAssets = [SELECT Asset__r.Quantity, Asset__r.Name, Asset__r.Part_Number__c, Asset__r.Part_Number__r.Name,
                     Asset__r.Part_Number__r.ProductCode,Asset__r.Asset_Description__c,Asset__r.Description
                     FROM Case_Asset_Junction__c
                     WHERE Case__c = :objRQA.Case__c];

    }
    // I-235837(SN) 18 Sept 2016  -  Fetch Related CaseAsset for Endo's Case if Regualatory question is of Endo Case
    else if (recordTypeId_EndoSet.contains(objRQA.Case__r.RecordTypeId)) {
    
        // I-237054(SN) 27 Sept 2016 -  Included Quantity field in the query
        listCaseAsset = [SELECT Asset__r.Name, Asset__r.Product__r.ProductCode, Asset__r.Description__c, Quantity__c
                         FROM Case_Asset__c
                         WHERE Case__c = : objRQA.Case__c];
        
    }
    // I-235837(SN) END
    
    // Fetch related Attachments
    List<Attachment> attachments = [SELECT Id, Body, Name, ContentType FROM Attachment WHERE ParentId = :objRQA.Case__c];


    // Call out
    String resultedId = trackwiseCallout(objRQA, lstAssets, attachments);

    // Update Trackwise Id on related Case record.
    Case c = new Case(Id = objRQA.Case__c);
    c.Trackwise_ID__c = String.valueOf(resultedId);
    c.Trackwise_Status__c = 'Complete';
    update c;
  }


  //----------------------------------------------------------------------------------------------
  // Calling Trackwise service
  //----------------------------------------------------------------------------------------------
  private static String trackwiseCallout(Regulatory_Question_Answer__c objRQA, List<Case_Asset_Junction__c> lstAssets, List<Attachment> attachments) {
    try{
        // prepare attributes to pass on
        String strMessageId                            = encodeValue(objRQA.Name);
        String strSenderIdentifier                     = encodeValue(objRQA.SenderIdentifier__c);
        String strReceiverIdentifier                   = encodeValue(objRQA.ReceiverIdentifier__c);
        String strAnswer4                              = encodeValue(objRQA.Answer_4__c);
        String strAnswer5New                           = encodeValue(objRQA.Answer_5_New__c);
        String strAnswer8                              = encodeValue(objRQA.Answer_8__c);
        String strAnswer7New                           = encodeValue(objRQA.Answer_7_New__c);
        String strAnswer9                              = encodeValue(objRQA.Answer_9__c);
        String strAnswer10                             = encodeValue(objRQA.Answer_10__c);
        String strAnswer11                             = encodeValue(objRQA.Answer_11__c);
        String strAdditionalDetails                    = encodeValue(objRQA.Additional_Details__c);
        String strReasonForProUn                       = encodeValue(objRQA.Answer_13_New__c);
        String strReasonForDelay                       = encodeValue(objRQA.Answer_2_New__c);
        String strAnswer14New                          = encodeValue(objRQA.Answer_14_New__c);
        String strAnswer17New                          = encodeValue(objRQA.Answer_17_New__c);
        String strAnswer18New                          = encodeValue(objRQA.Answer_18_New__c);
        String strAnswer12New                          = encodeValue(objRQA.Answer_12_New__c);
        String strAnswer15New                          = encodeValue(objRQA.Answer_15_New__c);
        String strAnswer16New                          = encodeValue(objRQA.Answer_16_New__c);
        String strIntakeSource                         = encodeValue(objRQA.Intake_source__c);
        String strCountryOfEvent                       = encodeValue(objRQA.Country_of_event__c);
        String strInitial_reporter_name                = encodeValue(objRQA.Initial_reporter_name__c);
        String strInitial_reporter_address             = encodeValue(objRQA.Initial_reporter_address__c);
        String strInitial_reporter_city                = encodeValue(objRQA.Initial_reporter_city__c);
        String strInitial_reporter_country             = encodeValue(objRQA.Initial_reporter_country__c);
        String strInitial_reporter_phone               = encodeValue(objRQA.Initial_reporter_phone__c);

        String strInitial_reporter_state               = encodeValue(objRQA.Initial_reporter_state__c);

        String strInitial_reporter_postalCode               = encodeValue(objRQA.Initial_reporter_postal_code__c);

        String strInitial_reporter_type                = encodeValue(objRQA.Initial_reporter_type__c);
		
        // NLG - 27 Sept 2016 - I-237052
        String strInitialReporterFacilityName          = encodeValue(objRQA.Account_Name__c);

        String strSerial_number                        = encodeValue(objRQA.Serial_number__c);
        String strCIC                                  = encodeValue(objRQA.CIC__c);
        
        // I-237053(SN) 27 Sept 2016  -  Include divisionalAccountNumber from RQA's AccountNumber              
        String strDivisional_AccountNumber             = encodeValue(objRQA.Account_Number__c);

        // NLG - I-237051 - 27 Sept 2016
        String strSalesRep                             = encodeValue(objRQA.Sales_Rep__c);

        Date answer1 = objRQA.Answer_1__c;
        Date answer7 = objRQA.Answer_3__c;
        Date myDate = date.newinstance(objRQA.CreatedDate.year(), objRQA.CreatedDate.month(), objRQA.CreatedDate.day());
        DateTime rqaCreationDate = objRQA.CreatedDate;



        // Intigration header
        Medical_TW_Common.IntegrationHeaderType  objHeader = new Medical_TW_Common.IntegrationHeaderType();
        objHeader.MessageID = strMessageId;
        objHeader.CreationDateTime = rqaCreationDate;
        objHeader.SenderIdentifier = strSenderIdentifier;
        objHeader.ReceiverIdentifier = strReceiverIdentifier;



        // Authentication
        String username = Medical_Trackwise_Settings__c.getInstance().Trackwise_Username__c;
        String password = Medical_Trackwise_Settings__c.getInstance().Trackwise_Password__c;
        String endpoint = Medical_Trackwise_Settings__c.getInstance().Trackwise_Endpoint_URL__c;

        // Create Request
        Medical_TW_Complaint.ComplaintInfoICPort  objRequest = new Medical_TW_Complaint.ComplaintInfoICPort ();
        objRequest.inputHttpHeaders_x = new  Map<String,String>();
        String token = EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password));
        objRequest.inputHttpHeaders_x.put('Authorization', 'Basic ' + token);
        objRequest.timeout_x = 120000;


        objRequest.endpoint_x  = endpoint;
        //objRequest.endpoint_x  = 'http://requestb.in/1jcd67i1';



        // create complaint info
        Medical_TW_ComplaintInfo.ComplaintInfo objComplaintInfo = new Medical_TW_ComplaintInfo.ComplaintInfo();
        objComplaintInfo.cic = strCIC;
        objComplaintInfo.intakeSource = strIntakeSource;
        objComplaintInfo.awarenessDate = answer1;
        objComplaintInfo.countryOfEvent = strCountryOfEvent;

        Medical_TW_ComplaintInfo.ContactGrid objContactInfo = new Medical_TW_ComplaintInfo.ContactGrid();
        Medical_TW_ComplaintInfo.ContactGridRow cgr = new Medical_TW_ComplaintInfo.ContactGridRow();
        objContactInfo.row = new List<Medical_TW_ComplaintInfo.ContactGridRow>();

        /*cgr.contactName = 'con1';
        cgr.contactInfo = '7272727';
        objContactInfo.row.add(cgr);

        cgr.contactName = 'con2';
        cgr.contactInfo = '888288';
        objContactInfo.row.add(cgr);

        objComplaintInfo.contactInfo = objContactInfo;

        objComplaintInfo.healthCareProfessionalOccupation = 'Dentist';
        */
        
        // I-237053(SN) 27 Sept 2016  -  Include divisionalAccountNumber from RQA's Account Number into TW Complaint for Endo case only
        if (recordTypeId_EndoSet.contains(objRQA.Case__r.RecordTypeId)){
            objComplaintInfo.divisionalAccountNumber = strDivisional_AccountNumber;
        }
        
        objComplaintInfo.initialReporterName = strInitial_reporter_name;
        objComplaintInfo.initialReporterAddress = strInitial_reporter_address;
        objComplaintInfo.initialReporterCity = strInitial_reporter_city;
        objComplaintInfo.initialReporterCountry = strInitial_reporter_country;

        objComplaintInfo.initialReporterStateProv = strInitial_reporter_state;

        objComplaintInfo.initialReporterPostalCode = strInitial_reporter_postalCode;


        objComplaintInfo.initialReporterPhone= strInitial_reporter_phone;
        System.debug('@@@initialReporterPhone' + objComplaintInfo.initialReporterPhone);

        objComplaintInfo.initialReporterType= strInitial_reporter_type;
        
        // NLG - 27 Sept 2016 - I-237052
        if (recordTypeId_EndoSet.contains(objRQA.Case__r.RecordTypeId)) {
            objComplaintInfo.initialReporterFacilityName = strInitialReporterFacilityName;
        }

        //Date myeventDate = date.newinstance(answer7.year(), answer7.month(), answer7.day());
        objComplaintInfo.eventDate = answer7;
        //objComplaintInfo.approx= 'No';
        objComplaintInfo.eventDescription= strAnswer4;
        objComplaintInfo.howWasIssueNoticed= strAnswer5New;
        objComplaintInfo.patientInvolvement= strAnswer8;

        objComplaintInfo.procedureCompletedSuccessfully= strAnswer7New;
        System.debug('@@@procedureCompletedSuccessfully' + objComplaintInfo.procedureCompletedSuccessfully);


        objComplaintInfo.medicalIntervention = strAnswer9;
        System.debug('@@@medicalIntervention' + objComplaintInfo.medicalIntervention);

        objComplaintInfo.surgicalDelay= strAnswer10;
        System.debug('@@@surgicalDelay' + objComplaintInfo.surgicalDelay);

        objComplaintInfo.adverseConsequences= strAnswer11;
        objComplaintInfo.adverseConsequencesDetails= strAdditionalDetails;
        //objComplaintInfo.deathDate= mydate;
        objComplaintInfo.userDistributionReported= strAnswer14New;
        objComplaintInfo.notReturnedToStrykerWhyNot = strReasonForProUn;
        objComplaintInfo.reasonForDelay = strReasonForDelay;



        Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGrid regAuthorityNotifiedBody = new Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGrid();
        Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGridRow r = new Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGridRow();
        regAuthorityNotifiedBody.row = new list<Medical_TW_ComplaintInfo.RegAuthorityNotifiedBodyGridRow>();

        r.regAuthorityNotifiedBody = strAnswer15New;
        r.referenceNum = strAnswer16New;
        regAuthorityNotifiedBody.row.add(r);
        //r.regAuthorityNotifiedBody = '(Belgium) Directorate General Public Health Protection';
        //r.referenceNum = '88388';
        //regAuthorityNotifiedBody.row.add(r);
        objComplaintInfo.regAuthorityNotifiedBody = regAuthorityNotifiedBody;

        //objComplaintInfo.notifyLegal = 'No';



        // Assets
        Medical_TW_ComplaintInfo.ProductGrid  lstProductGrid = new Medical_TW_ComplaintInfo.ProductGrid();
        lstProductGrid.row = new list<Medical_TW_ComplaintInfo.ProductGridRow>();
      
        // I-235837(SN) 18 Sept 2016 -  get recordtypeid of medical case
        ID medicalRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.MEDICAL_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId();
      
        // I-235837(SN) 18 Sept 2016 - Check if the Regulatory question Associated with Medical Case
        if(medicalRecordTypeId == objRQA.Case__r.RecordTypeID){
          //START ACARSON C-00172279 Oct.07.2016
          objComplaintInfo.divisionalAccountNumber = strDivisional_AccountNumber;
          objComplaintInfo.initialReporterFacilityName = strInitialReporterFacilityName;
          strSalesRep = encodeValue(objRQA.Case__r.CreatedBy.Name);
          objComplaintInfo.salesRep = strSalesRep;
          //END ACARSON C-00172279 Oct.07.2016
      
          for(Case_Asset_Junction__c objJunction :lstAssets){
              Medical_TW_ComplaintInfo.ProductGridRow  objGrid = new Medical_TW_ComplaintInfo.ProductGridRow();
              //START ACARSON Aug.29.2016 C-00177096 Put in new combination of field values for catalog number as requested
              //objGrid.catalogNumberAndDescription = encodeValue(objJunction.Asset__r.Name) + ' ' + encodeValue(objJunction.Asset__r.Part_Number__r.Name);
              objGrid.catalogNumberAndDescription = encodeValue(objJunction.Asset__r.Part_Number__r.ProductCode) + ' ' + encodeValue(objJunction.Asset__r.Description);
              //END ACARSON Aug.29.2016 C-00177096
              //objGrid.quantity = objJunction.Asset__r.Quantity == null?0:Integer.valueOf(objJunction.Asset__r.Quantity);
              objGrid.quantity = 1; // T-492024 this will be 1 always.
              objGrid.serialLotNum = encodeValue(objJunction.Asset__r.Name);
              //START ACARSON Aug.29.2016 C-00177096
              /* Commented out this field as requested */
              //objGrid.asReportedUDINumber = encodeValue(objJunction.Asset__r.Name) + ' ' + encodeValue(objJunction.Asset__r.Part_Number__r.ProductCode);
              //END ACARSON Aug.29.2016 C-00177096            
              lstProductGrid.row.add(objGrid);
          }  
        }
        // Associate Case Asset if it's an Endo Case
        else if (recordTypeId_EndoSet.contains(objRQA.Case__r.RecordTypeId)) {
           for(Case_Asset__c objCaseAsset :listCaseAsset){
           
               Medical_TW_ComplaintInfo.ProductGridRow  objGrid = new Medical_TW_ComplaintInfo.ProductGridRow();
               objGrid.catalogNumberAndDescription = encodeValue(objCaseAsset.Asset__r.Product__r.ProductCode) + ' ' + encodeValue(objCaseAsset.Asset__r.Description__c);
               objGrid.serialLotNum = encodeValue(objCaseAsset.Asset__r.Name);
               
               // I-237054(SN) 27 Sept 2016 - Add quantity from Case_Asset__c object.  
               objGrid.quantity = objCaseAsset.Quantity__c == null?1:Integer.valueOf(objCaseAsset.Quantity__c);   
              
               lstProductGrid.row.add(objGrid);
           }
        }
       
        // I-235837(SN) END
        
        objComplaintInfo.productDetails = lstProductGrid;
        // end Assets

        // Attachments
        //Medical_TW_ComplaintInfo.IntakeAttachment[] intakeAttachment;
        List<Medical_TW_ComplaintInfo.IntakeAttachment> lstAttachments = new List<Medical_TW_ComplaintInfo.IntakeAttachment>();
        for(Attachment att :attachments) {
          Medical_TW_ComplaintInfo.IntakeAttachment objAttachment = new Medical_TW_ComplaintInfo.IntakeAttachment();
          objAttachment.fileName = att.Name;
          //START ACARSON C-00172507 Aug.9.2016
          objAttachment.binaryData = att.Id;
          //objAttachment.binaryData = EncodingUtil.base64Encode(att.Body);
          //END ACARSON C-00172507 Aug.9.2016
          lstAttachments.add(objAttachment);
        }
        objComplaintInfo.intakeAttachment = lstAttachments;
        // End






        objComplaintInfo.complainantRequireResults = strAnswer17New;
        objComplaintInfo.complainantResultType = strAnswer18New;
        objComplaintInfo.productReturnedToStryker= strAnswer12New;

        //objComplaintInfo.medicalRecordsAvailable= 'No';
        //objComplaintInfo.isProductToBeReturned= 'No';




        Medical_TW_ComplaintInfo.ReprocessedProductsGrid rp = new Medical_TW_ComplaintInfo.ReprocessedProductsGrid();
        rp.reprocessedProduct = new list<String>();
        
        // NLG - Sept 27, 2016 - I-237050
        if (medicalRecordTypeId == objRQA.Case__r.RecordTypeID) {
          rp.reprocessedProduct.add('Rep1');
          rp.reprocessedProduct.add('Rep2');
        }
        system.debug('@@rp.reprocessedProduct' + rp.reprocessedProduct);
        objComplaintInfo.reprocessedProducts = rp;

        
        
        //objComplaintInfo.salesRep = 'Shaun Wernette';
        //system.debug('@@objComplaintInfo.salesRep'+objComplaintInfo.salesRep);
	    // NLG - Sept 27, 2016 - I-237051
        if (recordTypeId_EndoSet.contains(objRQA.Case__r.RecordTypeId)) {
            objComplaintInfo.salesRep = strSalesRep;
        }
        
        
        Medical_TW_ComplaintInfo.ProductsToBeReturnedList ptr = new Medical_TW_ComplaintInfo.ProductsToBeReturnedList();
        ptr.returnedProduct  = new list<String>();

        // NLG - Sept 27, 2016 - I-237049
        if (medicalRecordTypeId == objRQA.Case__r.RecordTypeID) {          
          ptr.returnedProduct.add('Ret1');
          ptr.returnedProduct.add('ret2');
        }

        objComplaintInfo.productsToBeReturned = ptr;

		System.debug('@@@objectComplaint: ' + objComplaintInfo);
        System.debug('@@@before calling web service');
        Medical_TW_ComplaintInfo.ResultMessage resultMessage = objRequest.CreateComplaint(objHeader,objComplaintInfo);
        System.debug('@@@after calling web service');

        System.debug('@@@resultMessageId' + resultMessage.id);
      System.debug('@@@resultMessage' + resultMessage.message);
      return String.valueOf(resultMessage.id);
    }
    catch(Exception ex){
        System.debug('@@@-error(callout)-' + ex.getMessage());
        return '-1';
    }
  }






  private static String encodeValue(String str) {
    if(String.isBlank(str) == true){
        return '';
    }
    return String.valueOf(str);
    // Trackwise is not accepting encoded format.
    //return EncodingUtil.urlEncode(String.valueOf(str), 'UTF-8');
  }
}