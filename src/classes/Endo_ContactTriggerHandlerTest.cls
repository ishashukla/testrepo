/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  Feb 24, 2014 
 Purpose       :  Test Class for Case EEndo_ContactTriggerHandler

 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
*******************************************************************************/
@isTest
private class Endo_ContactTriggerHandlerTest {
  
  //-----------------------------------------------------------------------------------------------
  // Method for test InsertContactAccountAssociation method
  //-----------------------------------------------------------------------------------------------
  static testmethod void testmethod1(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Customer Contact');
    Id recordTypeId = rtByName.getRecordTypeId(); 
    
    
    Test.startTest();
    
    Account acc = Endo_TestUtils.createAccount(1, true).get(0);
      
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.LastName = 'tttest';
    con.Title= 'Sales Rep';
    con.RecordTypeId = recordTypeId;
    insert con; 
    System.debug('@@@' + recordTypeId);
    Test.stopTest();
    
    //verify results.
    List<Contact_Acct_Association__c> junction = [SELECT Id, Primary__c, Associated_Contact__r.LastName, Associated_Account__c, Associated_Contact__c 
                          FROM Contact_Acct_Association__c WHERE Associated_Account__c = :acc.Id];
    
    
    System.assert(!junction.isEmpty());
    System.assertEquals(true, junction[0].Primary__c);
    System.assertEquals(acc.Id, junction[0].Associated_Account__c);
    System.assertEquals(con.Id, junction[0].Associated_Contact__c);
  }
  
  
  //-----------------------------------------------------------------------------------------------
  // Method for test populateSalesSupportAgentOnCase method
  //-----------------------------------------------------------------------------------------------
  static testmethod void testMethod2(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
    Id recordTypeId = rtByName.getRecordTypeId(); 
    
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Endo_Region_Name__c = 'test region';
    insert acc;
    
    List<User> usrList = Endo_TestUtils.createUser(2, 'System Administrator', false);
    
    usrList.get(0).Endo_Support_Regions__c = 'test region';
    
    usrList.get(1).Endo_Support_Regions__c = 'test region';
    usrList.get(1).Top_35_Sales_Agent__c = true;
    insert usrList;
    
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    con.ENDO_Region_Name__c = 'test region';
    con.Top_35_Sales_Agent__c = false;    
    con.RecordTypeId = recordTypeId;
       
    insert con;
    
    Test.startTest();
    // verify results.
    List<Contact> lst = [SELECT Id, Sales_Support_Agent__c FROM Contact WHERE Id = :con.Id];
    
    //System.assertEquals(usrList.get(0).Id, lst.get(0).Sales_Support_Agent__c);
    //Create test records
    //Modified By Chandra Shekhar Sharma to add record types on case Object
    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
      
    Schema.RecordTypeInfo rtByName1 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    TestUtils.createRTMapCS(rtByName1.getRecordTypeId(), 'Tech Support', 'Endo');
    
    objCase.recordTypeId = rtByName1.getRecordTypeId();
    insert objCase;
      
    List<Case> lstCase = [SELECT Id, Top_35_Sales_Rep__c, Sales_Support_Agent__c FROM Case WHERE Id = :objCase.Id];
    //System.assertEquals(false, lstCase.get(0).Top_35_Sales_Rep__c);
    //System.assertEquals(usrList.get(0).Id, lstCase.get(0).Sales_Support_Agent__c);
    
    con.Top_35_Sales_Agent__c = true;
    update con;
    
    Test.stopTest(); 
    // verify results.
    
    lst = [SELECT Id, Sales_Support_Agent__c FROM Contact WHERE Id = :con.Id];
    lstCase = [SELECT Id, Top_35_Sales_Rep__c, Sales_Support_Agent__c FROM Case WHERE Id = :objCase.Id];
    //System.assertEquals(usrList.get(1).Id, lst.get(0).Sales_Support_Agent__c);
    //System.assertEquals(true, lstCase.get(0).Top_35_Sales_Rep__c);
    //System.assertEquals(usrList.get(1).Id, lstCase.get(0).Sales_Support_Agent__c);
    
  }
  
  //-----------------------------------------------------------------------------------------------
  // Method for test UpdateMailingAddress
  //-----------------------------------------------------------------------------------------------
  static testmethod void testMethod3(){
    Test.startTest();
    
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
    Id recordTypeId = rtByName.getRecordTypeId(); 
    
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Endo_Region_Name__c = 'test region';
    acc.ShippingStreet = 'test';
    acc.ShippingStateCode = 'TX';
    acc.ShippingState = 'Texas';
    acc.ShippingPostalCode = '78704';
    acc.ShippingCountryCode = 'US';
    acc.ShippingCountry = 'United States';
    acc.ShippingCity = 'test';
    insert acc;
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    con.RecordTypeId = recordTypeId;
    insert con;
    
    
    Test.stopTest();
    
    //Verify result
    
    List<Contact> lst = [SELECT Id,MailingStateCode, MailingStreet,MailingState, MailingPostalCode, MailingCountryCode, MailingCountry, MailingCity
                          FROM Contact WHERE Id = :con.Id];
    
    System.assertEquals(lst.get(0).MailingStateCode, 'TX');
  }
}