/*
Author - Jyoti Singh (JDC)
Story -  S-389737
Usage - test Class for BatchToSetPartialBackorder
*/
@istest
public class BatchToSetPartialBackorderTest{
    @testSetup static void setup() {   
        Id nvCustomerAccountRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('NV Customer').getRecordTypeId();     
        //Map<String, Id> accountRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');
        Account testAccount = NV_TestUtility.createAccount('TestAccount', nvCustomerAccountRT, true);
        
        Contract testContract = NV_TestUtility.createContract(testAccount.Id, true);
        testContract.Status = 'Activated';
        update testContract;
        
        //Map<String, Id> productRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Product2');
        Id nvProductRT = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Stryker Neurovascular').getRecordTypeId();
        List<Product2> productData = new List<Product2>();
        for(Integer i=1; i<=3;i++){
            productData.add(NV_TestUtility.createProduct('NVProduct'+i, '00000'+i, nvProductRT, false));
        }
        insert productData;
        
        Pricebook2 testPriceBook = NV_TestUtility.createPricebook('NVPriceBook1', true, true);

        //Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
        Id nvStandardOrderRT = Schema.SObjectType.Order.getRecordTypeInfosByName().get('NV Standard').getRecordTypeId();
        Order testOrder1 = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, nvStandardOrderRT, false);
        testOrder1.Customer_PO__c = 'PO00001';
        testOrder1.EffectiveDate = Date.today();
        testOrder1.Date_Ordered__c = Datetime.now();
        testOrder1.Status = 'Booked';
        testOrder1.Pricebook2Id = testPriceBook.Id;
        insert testOrder1;

        Test.startTest();
        
        PricebookEntry pbEntryForP1 = NV_TestUtility.addPricebookEntries(testPriceBook.Id, productData[0].Id, 1000, true, true);
        List<OrderItem> testOrderList = new List<OrderItem>();
        
        OrderItem testOrderItem1 = new OrderItem();
        testOrderItem1.OrderId = testOrder1.Id;
        testOrderItem1.PricebookEntryId = pbEntryForP1.Id;
        testOrderItem1.Quantity = 3;
        testOrderItem1.UnitPrice = pbEntryForP1.UnitPrice;
        //insert testOrderItem1;
        testOrderList.add(testOrderItem1);

        OrderItem testOrderItem2 = new OrderItem();
        testOrderItem2.OrderId = testOrder1.Id;
        testOrderItem2.PricebookEntryId = pbEntryForP1.Id;
        testOrderItem2.Quantity = 3;
        testOrderItem2.UnitPrice = pbEntryForP1.UnitPrice;
        //insert testOrderItem2;
        testOrderList.add(testOrderItem2);
        insert testOrderList;

        testOrderItem1.Status__c = 'Shipped';
        update testOrderItem1;
        
        Test.stopTest();
            
        testOrderItem2.Status__c = 'Backordered';
        update testOrderItem2;
        
        testOrder1.PartialBackOrder__c = '';
        update testOrder1;
    }
    @isTest static void testOrderItemTriggerBeforeInsertUpdate () { 
    	Test.startTest();    
        BatchToSetPartialBackorder orderbatch = new BatchToSetPartialBackorder ();  
        Database.executebatch(orderbatch);
        Test.stopTest();
        Order testOrder1 = [SELECT Id,PartialBackOrder__c  From Order WHERE Customer_PO__c = 'PO00001' LIMIT 1];
        system.assertEquals('Backordered', testOrder1.PartialBackOrder__c);
    }
}