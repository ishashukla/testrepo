/**================================================================      
* Appirio, Inc
* Name: ChecklistDeepCloneController
* Description: clone Checklist Header and related Checklist deatil
* Created Date: 06-Sep-2016 
* Created By: Shubham Dhupar (Appirio,Ref: T-532234)
*
* Date Modified      Modified By      Description of the update
==================================================================*/

global class ChecklistDeepCloneController {
  //============================================================================================     
  // Name         : cloneChecklist
  // Description  : To Clone Checklist Header and related Checklist Details
  // Created Date : 06-Sep-2016 
  // Created By   : Shubham Dhupar (Appirio)
  // Task         : T-532234
  //==========================================================================================
  webService static String cloneChecklist(ID checklistid) 
  {       
    List<CheckList_Detail__c> newcheckList = new list<CheckList_Detail__c>();
    Checklist_Header__c check = Database.query(queryAllObjectFields('Checklist_Header__c')  + ' WHERE Id = \'' + checklistid +'\' Limit 1');
    List<CheckList_Detail__c> checkList = Database.query(queryAllObjectFields('CheckList_Detail__c')  + ' WHERE Checklist_Header__c = \'' + checklistid +'\'');
    check.Status__c = 'New';
    check.Completed_By__c = NULL; 
    check.Completed_On__c = NULL;
    Checklist_Header__c Newcheck = check.clone(false, true);
    Insert Newcheck;
    for (CheckList_Detail__c woCheck: checkList){
        CheckList_Detail__c newWoCheck  = woCheck.clone(false, true);
        newWoCheck.Checklist_Header__c = Newcheck.Id;
        newcheckList.add(newWoCheck);
    }
    Insert newcheckList;
    return Newcheck.Id;
  }
    
    webService static SVMXC.INTF_WebServicesDef.INTF_Response cloneChecklistForSVMX(SVMXC.INTF_WebServicesDef.INTF_Request request) 
    {       
        SVMXC.INTF_WebServicesDef.INTF_Response obj = new SVMXC.INTF_WebServicesDef.INTF_Response(); 
        String recordId; 
        for(SVMXC.INTF_WebServicesDef.SVMXMap objSVXMMap : request.valueMap) 
        { 
            if(objSVXMMap.key == 'SVMX_RECORDID') 
            { 
                 recordId = objSVXMMap.value; 
            } 
        } 
        List<CheckList_Detail__c> newcheckList = new list<CheckList_Detail__c>();
        Checklist_Header__c check = Database.query(queryAllObjectFields('Checklist_Header__c')  + ' WHERE Id = \'' + recordId +'\' Limit 1');
        List<CheckList_Detail__c> checkList = Database.query(queryAllObjectFields('CheckList_Detail__c')  + ' WHERE Checklist_Header__c = \'' + recordId +'\'');
        if(check.isLatest__c = true){
          check.isLatest__c = false;
          update check;
        }
        check.Status__c = 'New';
        check.Completed_By__c = NULL;
        check.Completed_On__c = NULL;
        check.isLatest__c = true;
        Checklist_Header__c Newcheck = check.clone(false, true);
        Insert Newcheck;
        List<Checklist_Header__c> checklistHeader = [SELECT Name FROM Checklist_Header__c WHERE Id = :Newcheck.id];
        for (CheckList_Detail__c woCheck: checkList){
            CheckList_Detail__c newWoCheck  = woCheck.clone(false, true);
            newWoCheck.Checklist_Header__c = Newcheck.Id;
            newcheckList.add(newWoCheck);
        }
        Insert newcheckList;
        
        SVMXC.INTF_WebServicesDef.SVMXMap sObj = new SVMXC.INTF_WebServicesDef.SVMXMap(); 
        sObj.record = Newcheck; 
        obj.valueMap = new List<SVMXC.INTF_WebServicesDef.SVMXMap>(); 
        obj.valueMap.add(sObj); 
        obj.Message = 'Checklist ' + check.name + ' cloned to : ' + checklistHeader.get(0).Name;
        obj.messageType = 'INFO';
        obj.success = true;
        return obj;
    }
  
  //============================================================================================     
  // Name         : queryAllObjectFields
  // Description  : To query All the fields of an object
  // Created Date : 06-Sep-2016 
  // Created By   : Shubham Dhupar (Appirio)
  // Task         : T-532234
  //==========================================================================================
    Static string queryAllObjectFields(String SobjectApiName){
        String query = '';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        List<Checklist_Header__c> RoomsList = new List<Checklist_Header__c>();
            
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
 
        query = 'SELECT ' + commaSepratedFields + ' FROM ' + SobjectApiName;
        return query;
    }
}