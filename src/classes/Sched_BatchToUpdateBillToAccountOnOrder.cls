/******************************************************************************************
 *  Purpose : Scheduler class for BatchToUpdateBillToAccountOnOrder
 *  Author  : Shubham Paboowal
 *  Story	: S-436452
*******************************************************************************************/  
global class  Sched_BatchToUpdateBillToAccountOnOrder implements Schedulable {
  
  // Call a batch class
  global void execute(SchedulableContext sc) {
     BatchToUpdateBillToAccountOnOrder obj = new BatchToUpdateBillToAccountOnOrder(); 
     database.executebatch(obj);
  }
}