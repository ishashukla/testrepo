global class RoomDeepCloneController {

    webService static String cloneRoom(ID roomid) 
    {       
     	List<Equipment__c> NewEquipList = new list<Equipment__c>();
        

        Room__c room = Database.query(queryAllObjectFields('Room__c')  + ' WHERE Id = \'' + roomid +'\' Limit 1');
        List<Equipment__c> EquipList = Database.query(queryAllObjectFields('Equipment__c')  + ' WHERE Room__c = \'' + roomid +'\'');
               
        room.Name = 'Copy '+ room.Name;
        Room__c NewRoom = room.clone(false, true);
        Insert NewRoom;
        
        for (Equipment__c equip: EquipList){
            Equipment__c NewEquip  = equip.clone(false, true);
            NewEquip.Room__c = NewRoom.Id;
            NewEquipList.add(NewEquip);
        }
        Insert NewEquipList;
               
        return NewRoom.Id;
    }
    
    Static string queryAllObjectFields(String SobjectApiName){
        String query = '';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
 		List<Room__c> RoomsList = new List<Room__c>();
            
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
 
        query = 'SELECT ' + commaSepratedFields + ' FROM ' + SobjectApiName;
        return query;
    }

}