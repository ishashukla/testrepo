// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_OpportunityTriggerTest - Test Class for opportunity Trigger
//
// 03/09/2016, Deepti Maheshwari (T-458596)
// 03/21/2016, Deepti Maheshwari (T-484584)
// 09/26/2016, Kanika Mathur (Modified code to use standard Stage field in place of custom and created COMM BP Config custom setting record)
// 12/12/2016, Nitish Bansal (Modified code to use COMM RT and created data to increase code coverage)

@isTest
private class COMM_OpportunityTriggerTest {
    static AccountTeamMember atms; //NB - 12/12/2016 
    static List<Project_Plan__c> projectPlanList = new List<Project_Plan__c> ();//NB - 12/12/2016
    static List<Account> accountList = new List<Account>();
    static List<Opportunity> opportunityList = new List<Opportunity>();
    static Opportunity oppty = new Opportunity();

    static testMethod void testBeforeInsertUpdate(){
    //NB - 12/12/2016 Start    
        User u = TestUtils.createUser(1,'System Administrator', true).get(0);
        system.runAs(u){
            createTestData();
            test.startTest(); 
            List<Opportunity> opptyList = [SELECT Stage__c, Probability, StageName, Set_CSR_Owner__c, Project_Plan__c, PO_Close_Date__c FROM Opportunity WHERE Id = :oppty.id];
            system.assert(opptyList.get(0).Stage__c == 'Prospecting');
            system.assert(opptyList.get(0).Probability == 0); 
            FF_OpportunityTriggerHandler.skipOppTrigger = false;
            opptyList.get(0).StageName = 'Developing';
            opptyList.get(0).Set_CSR_Owner__c = true; 
            opptyList.get(0).Project_Plan__c = projectPlanList.get(1).Id; 
            opptyList.get(0).PO_Close_Date__c = system.today().addDays(10); 
            update opptyList;
            test.stopTest();
        }
    //NB - 12/12/2016 end 
    }
     
    static testMethod void testBeforeUpdate() {
        
        accountList = TestUtils.createAccount(5, true); 
        List<Pricebook2> priceBookList = TestUtils.createpricebook(5,' ',false);
        priceBookList.get(0).Name = 'COMM Price Book';
        insert priceBookList;
        //Added below line to create Comm BP constant setting- KM-9/26/2016
        TestUtils.createCommBPConfigConstantSetting();
        projectPlanList = TestUtils.createProjectPlan(3,accountList.get(1).Id,true);
        opportunityList=TestUtils.createOpportunity(1,accountList[1].id,false);
        oppty = opportunityList.get(0);
        oppty.RecordTypeID = 
        Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeID();
        oppty.PO_Close_Date__c = system.today();
        oppty.Pricebook2Id = priceBookList.get(0).Id;
        oppty.Project_Plan__c = projectPlanList.get(0).Id;
        oppty.StageName = 'Prospecting';
        oppty.Stage__c = System.Label.COMM_Cancelled;//NB - 12/12/2016 
        oppty.Set_CSR_Owner__c = false;//NB - 12/12/2016 
        insert oppty;

        //NB - 12/12/2016 Start
        FF_OpportunityTriggerHandler.skipOppTrigger = false;
        
        List<Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
        for(Order orderRec : orderList ) {
            orderRec.OpportunityId =  oppty.id; 
            orderRec.PONumber = '50';
            orderRec.Project_Plan__c = projectPlanList[1].id;
        }   
        insert orderList;  
        //NB - 12/12/2016 end

        test.startTest();
        oppty.Stage__c = System.Label.COMM_Cancelled;//NB - 12/12/2016
        oppty.Set_CSR_Owner__c = true; //NB - 12/12/2016 
        oppty.Percent_Probability__c = '10- Highly Improbable';//NB - 12/12/2016 
        update oppty;
        List<Opportunity> opptyList = [SELECT StageName, Probability FROM Opportunity WHERE Id = :oppty.id];
        test.stopTest(); 
        
        
    }
    
    static void createTestData() {
        User testUser1 = TestUtils.createUser(1,'Sales - COMM INTL', false)[0];//NB - 12/12/2016 
        testUser1.email = 'testComm@gamil.com';//NB - 12/12/2016 
        insert testUser1;//NB - 12/12/2016 

        accountList = TestUtils.createAccount(5, true); 

        atms = TestUtils.createTeamMembers(accountList.get(1).id, testUser1.id, 'Sales Manager', true);//NB - 12/12/2016 
        
        List<Pricebook2> priceBookList = TestUtils.createpricebook(5,' ',false);
        priceBookList.get(0).Name = 'COMM Price Book';
        insert priceBookList;
        //Added below line to create Comm BP constant setting- KM-9/26/2016
        TestUtils.createCommBPConfigConstantSetting();
        projectPlanList = TestUtils.createProjectPlan(3,accountList.get(1).Id,true);
        opportunityList=TestUtils.createOpportunity(1,accountList[1].id,false);
        oppty = opportunityList.get(0);
        oppty.RecordTypeID = 
        Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeID();
        oppty.PO_Close_Date__c = system.today();
        oppty.Pricebook2Id = priceBookList.get(0).Id;
        oppty.Project_Plan__c = projectPlanList.get(0).Id;
        oppty.StageName = 'Prospecting';
        insert oppty;
    }
    
}