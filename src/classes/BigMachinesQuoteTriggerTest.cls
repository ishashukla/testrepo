// 
// (c) 2016 Appirio, Inc.
// Created this test class to test BigMachinesQuoteTrigger and is related to T-426533
// 
//
// 25 Feb 2016    Meghna Vijay       Original
// 
//

@isTest
private class BigMachinesQuoteTriggerTest {
  static List < Account > accountList = new List < Account > ();
  static List < Opportunity > opportunityList = new List < Opportunity > ();
  static List < BigMachines__Quote__c > bigMachineQuoteList = new List < BigMachines__Quote__c > ();
  static List < BigMachines__Quote__c > bigMachineQuoteList1 = new List < BigMachines__Quote__c > ();
  // method to test After Insert 
  static testMethod void testAfterInsert() {
    accountList = TestUtils.createAccount(5, true); 
    TestUtils.createCommConstantSetting();
    opportunityList = TestUtils.createOpportunity(5, accountList[1].id, true);
    bigMachineQuoteList = TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false);
    bigMachineQuoteList.addAll(TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false));
    test.startTest();
    insert bigMachineQuoteList;
    test.stopTest();
  }
	// method to test After Update
  static testMethod void testAfterUpdate() {
    accountList = TestUtils.createAccount(5, true);
    TestUtils.createCommConstantSetting();
    opportunityList = TestUtils.createOpportunity(5, accountList[1].id, true);
    bigMachineQuoteList = TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false);
    bigMachineQuoteList.addAll(TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false));
    test.startTest();
    insert bigMachineQuoteList;
    BigMachines__Quote__c bm = bigMachineQuoteList.get(0);
    bm.BigMachines__Opportunity__c = opportunityList[0].id;
    update bm;
    test.stopTest();

  }
	// method to test After Delete
  static testMethod void testAfterDelete() {
    accountList = TestUtils.createAccount(5, true);
    TestUtils.createCommConstantSetting();
    opportunityList = TestUtils.createOpportunity(5, accountList[1].id, true);
    bigMachineQuoteList = TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false);
    bigMachineQuoteList.addAll(TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false));
    test.startTest();
    insert bigMachineQuoteList;
    delete bigMachineQuoteList;
    test.stopTest();
  }
	// method to test After Undelete
  static testMethod void testAfterUnDelete() {
    accountList = TestUtils.createAccount(5, true);
    TestUtils.createCommConstantSetting();
    opportunityList = TestUtils.createOpportunity(5, accountList[1].id, true);
    bigMachineQuoteList = TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false);
    bigMachineQuoteList.addAll(TestUtils.createBigMachinesQuote(1, accountList[0].id, opportunityList[1].id, false));
    test.startTest();
    insert bigMachineQuoteList;
    delete bigMachineQuoteList;
    undelete bigMachineQuoteList;
    test.stopTest();
  }

}