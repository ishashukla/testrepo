// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncInventoryTransaction_QueueTest.
//
// 22 Nov 2016, Pragya Awasthi
@isTest
private class COMM_SyncInventoryTransaction_QueueTest {
    static List<SVMXC__Stock_Transfer__c> recordList;
    static set<Id> stockIds;
    @isTest static void testInventorySync() {
        createData();
        List<SVMXC__Stock_Transfer__c> stockList = [SELECT Id ,SVMXC__Source_Location__r.Location_ID__c,SVMXC__Destination_Location__r.Location_ID__c,
                                                    (SELECT Id, SVMXC__Quantity_Transferred2__c, Asset__r.SVMXC__Serial_Lot_Number__c, Asset__r.Quantity__c, 
                                                     SVMXC__Product__r.Inventory_Item_ID__c, SVMX_Serial_Number__r.name,Asset__r.External_ID__c 
                                                     FROM SVMXC__Stock_Transfer_Line__r) 
                                                    FROM SVMXC__Stock_Transfer__c WHERE Id IN :stockIds ];
        Test.startTest();
        COMM_SyncInventoryTransactionQueue queueObj = new COMM_SyncInventoryTransactionQueue(stockList);
        ID jobID = System.enqueueJob(queueObj);
        Test.stopTest();
        System.assertEquals('Success', [SELECT Integration_Status__c FROM SVMXC__Stock_Transfer__c WHERE Id = :recordList[0].Id].Integration_Status__c);
    }
    public static void createData() {
        SVMXC__Site__c slocation = new SVMXC__Site__c(Location_ID__c = '1234',SVMXC__Stocking_Location__c = true);
        insert slocation;
        SVMXC__Site__c dlocation = new SVMXC__Site__c(Location_ID__c = '1235',SVMXC__Stocking_Location__c = true);
        insert dlocation;
        recordList = new List<SVMXC__Stock_Transfer__c>();
        for(Integer i = 0; i < 4; i++){
            SVMXC__Stock_Transfer__c stock = new SVMXC__Stock_Transfer__c(
                     Integration_Status__c = 'New',
                     SVMXC__Source_Location__c = slocation.Id,
                     SVMXC__Destination_Location__c = dlocation.Id
            );
            recordList.add(stock);
        }
       insert recordList;
        stockIds = new Set<Id>();
        for(SVMXC__Stock_Transfer__c stock : recordList) {
            stockIds.add(stock.Id);
        }
       SVMXC__Installed_Product__c asset = new SVMXC__Installed_Product__c(External_ID__c = '123',SVMXC__Serial_Lot_Number__c='12',Quantity__c = 1);
       insert asset;
       List<SVMXC__Stock_Transfer_Line__c> lineList = new List<SVMXC__Stock_Transfer_Line__c>();
       for(Integer i = 0; i < 4; i++){
            SVMXC__Stock_Transfer_Line__c line = new SVMXC__Stock_Transfer_Line__c(
                     SVMXC__Quantity_Transferred2__c = 1,
                     Asset__c = asset.Id,
                     SVMXC__Stock_Transfer__c = recordList[i].Id
            );
            lineList.add(line);
        }
       insert lineList;
       COMMRecordCount__c customSetting = new COMMRecordCount__c(Count__c = 3);
       insert customSetting;
    }
}