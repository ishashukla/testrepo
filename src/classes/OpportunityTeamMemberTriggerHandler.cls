//
// 2016 Appirio Inc.
//
// Created By       :     Rahul Aeran
// Created Date     :     05/03/2016
// Description      :     OpportunityTeamMember Handler Class
//
// 25th Oct 2016     Nitish Bansal  I-241409 // Proper list checks before SOQL and DML in almost all methods, single RT query
public class OpportunityTeamMemberTriggerHandler {
  
    static Id rtcommRecordTypeId = Constants.getRecordTypeId(Constants.COMM_Case_RTYPE , Constants.CASE_OBJECT);
    static Set<String> setCOMMProfiles = new Set<String>{Constants.STRATEGIC_SALES_PROFILE,
                                        Constants.PROFILE_SALES_REGION_MANAGER,
                                        Constants.PROFILE_SALES_REP_US,
                                        Constants.PROFILE_SALES_REP_INTL};


    // Methods to be executed after record is created
    // @param lstNew- List to hold Trigger.new, mapOld - map to hold trigger.oldMap 
    // @return Nothing
    // NB - 05/03- I-208731
    public static void onAfterUpdate(List<OpportunityTeamMember> lstNew, map<Id,OpportunityTeamMember> mapOld){
    
    }
    // Methods to be executed after record is created
    // @param lstNew- List to hold Trigger.new
    // @return Nothing
    // NB - 05/03- I-208731
    public static void onAfterInsert(List<OpportunityTeamMember> lstNew){
        createCaseSharingRecords(lstNew);
    }
    // Methods to be executed after record is created
    // @param lstOld- List to hold Trigger.old
    // @return Nothing
    // Created Date: [05/03/2016]; I-214491
    // Created By: Rahul Aeran (Appirio)
    public static void onAfterDelete(List<OpportunityTeamMember> lstOld){
        deleteSharingRecords(lstOld);
    }
    
    //================================================================      
    // Name: add and remove sharing access for cases for I-214491
    // Description: This class is used to give read access to cases for opportunity team members
    // @param newList - List to hold Trigger.new
    // @return Nothing
    // Created Date: [05/03/2016]; I-214491
    // Created By: Rahul Aeran (Appirio)
  //==================================================================
  
  private static void createCaseSharingRecords(List<OpportunityTeamMember> newList){
      Set<Id> setUserIds = new Set<Id>();
      map<Id,Set<Id>> mapOppIdToMembers = new map<Id,Set<Id>>();
    //  Id rtcommRecordTypeId = Constants.getRecordTypeId(Constants.COMM_Case_RTYPE , Constants.CASE_OBJECT);
      
      for(OpportunityTeamMember om : newList){
         
         if(!mapOppIdToMembers.containsKey(om.OpportunityId)){
             mapOppIdToMembers.put(om.OpportunityId , new Set<id>());
         }
         
         mapOppIdToMembers.get(om.OpportunityId).add(om.UserId);
         setUserIds.add(om.UserId);
      }
      
      //Now we will query the users which fell in those profiles for which we need to share case records 
      /*Set<String> setCOMMProfiles = new Set<String>{Constants.STRATEGIC_SALES_PROFILE,
                                        Constants.PROFILE_SALES_REGION_MANAGER,
                                        Constants.PROFILE_SALES_REP_US,
                                        Constants.PROFILE_SALES_REP_INTL};
      */    
    if(setUserIds.size() > 0){   
      map<Id,User> mapUserIdToUser = new map<Id,User>([SELECT Id,Profile.Name FROM User
                                                        WHERE Id IN :setUserIds
                                                        AND Profile.Name IN :setCOMMProfiles]);
       
      map<Id,Set<Id>> mapOppIdToCases = new map<Id,Set<Id>>();
      if(mapOppIdToMembers.size() > 0){
        for(Case cs : [SELECT Id,Opportunity__c FROM Case
                          WHERE Opportunity__c IN :mapOppIdToMembers.keySet()
                          AND RecordTypeId = :rtcommRecordTypeId]){
           if(!mapOppIdToCases.containsKey(cs.Opportunity__c)){
               mapOppIdToCases.put(cs.Opportunity__c , new Set<id>());
           }
           mapOppIdToCases.get(cs.Opportunity__c).add(cs.Id);
        }
      }
      
      
      System.debug('mapOppIdToCases'+mapOppIdToCases);
      //Now we need to insert the records
      List<CaseShare> lstShareToInsert = new List<CaseShare>();
      for(Id oppId : mapOppIdToCases.keySet()){
          
	      //validating that the opportunity team members has users
	      if(mapOppIdToMembers.containsKey(oppId) && mapOppIdToCases.containsKey(oppId)){
	          for(Id caseId : mapOppIdToCases.get(oppId)){
	              for(Id userId : mapOppIdToMembers.get(oppId)){
	        		      if(mapUserIdToUser.containsKey(userId)){
	                      //including only  the valid profiles
	                      lstShareToInsert.add(new CaseShare( UserOrGroupId = userId, 
                                                            CaseId = caseId,
                                                            CaseAccessLevel = Constants.PERM_READ ));
	                  }
	    		      }
	          }
	      }
      }
      
      if(!lstShareToInsert.isEmpty()){
      	System.debug('lstShareToInsert'+lstShareToInsert.size());
        Database.SaveResult[] srList = Database.insert(lstShareToInsert,false);
        for(Database.SaveResult sr : srList){
        	if(!sr.isSuccess()){
        		//Logging the error
        		System.debug('Error occurred in sharing:'+sr.getErrors().get(0).getMessage());
        	}
        }
      }
    }  
      
  }
  
  //================================================================      
    // Name: add and remove sharing access for cases for I-214491
    // Description: This method is used to delete the manual sharing given to opportunity team members
    // @param newList - List to hold Trigger.old
    // @return Nothing
    // Created Date: [05/03/2016]; I-214491
    // Created By: Rahul Aeran (Appirio)
  //==================================================================
  private static void deleteSharingRecords(List<OpportunityTeamMember> lstOld){
      Set<Id> setUserIds = new Set<Id>();
      map<Id,Set<Id>> mapOppIdToMembers = new map<Id,Set<Id>>();
    //  Id rtcommRecordTypeId = Constants.getRecordTypeId(Constants.COMM_Case_RTYPE , Constants.CASE_OBJECT);
      for(OpportunityTeamMember om : lstOld){
         
         if(!mapOppIdToMembers.containsKey(om.OpportunityId)){
             mapOppIdToMembers.put(om.OpportunityId , new Set<id>());
         }
         mapOppIdToMembers.get(om.OpportunityId).add(om.UserId);
         setUserIds.add(om.UserId);
      }
      
      //Now we will query the users which fell in those profiles for which we need to share case records 
      /*Set<String> setCOMMProfiles = new Set<String>{Constants.STRATEGIC_SALES_PROFILE,
                                        Constants.PROFILE_SALES_REGION_MANAGER,
                                        Constants.PROFILE_SALES_REP_US,
                                        Constants.PROFILE_SALES_REP_INTL};
      */               
    if(setUserIds.size() > 0){                       
      map<Id,User> mapUserIdToUser = new map<Id,User>([SELECT Id,Profile.Name FROM User
                                                        WHERE Id IN :setUserIds
                                                        AND Profile.Name IN :setCOMMProfiles]);
       
       
       
      map<Id,Set<Id>> mapOppIdToCases = new map<Id,Set<Id>>();
      Set<Id> setCasesAffectedBySharing = new Set<Id>();
      if(mapOppIdToMembers.size() > 0){
        for(Case cs : [SELECT Id,Opportunity__c FROM Case
                          WHERE Opportunity__c IN :mapOppIdToMembers.keySet()
                          AND RecordTypeId = :rtcommRecordTypeId]){
           if(!mapOppIdToCases.containsKey(cs.Opportunity__c)){
               mapOppIdToCases.put(cs.Opportunity__c , new Set<id>());
           }
           mapOppIdToCases.get(cs.Opportunity__c).add(cs.Id);
           setCasesAffectedBySharing.add(cs.Id);
        }
      }  
      Set<String> setPermissionsToDelete = new Set<String>();
      for(Id oppId : mapOppIdToCases.keySet()){
          //validating that the opportunity team members has users
          if(mapOppIdToMembers.containsKey(oppId) && mapOppIdToCases.containsKey(oppId)){
              for(Id caseId : mapOppIdToCases.get(oppId)){
                  for(Id userId : mapOppIdToMembers.get(oppId)){
                      //including only  the invalid profiles
        		      if(mapUserIdToUser.containsKey(userId)){
                          //creating a combination of user and cases
                          setPermissionsToDelete.add(caseId + Constants.TILDE_DELIMITER + userId);
                      }
        		  }
              }
          }
		  
          
      }
      
      if(setCasesAffectedBySharing.size() > 0){
        //Now we need to delete the records
        List<CaseShare> lstExistingCaseShareRecords = [SELECT Id, CaseId, UserOrGroupId, RowCause 
                                                FROM CaseShare 
                                                WHERE CaseId IN :setCasesAffectedBySharing 
                                                AND UserOrGroupId IN :setUserIds
                                                AND RowCause = :Constants.ROW_CAUSE_MANUAL
                                                AND CaseAccessLevel = :Constants.PERM_READ];
        String strKey = null;                                        
        List<CaseShare> lstToDelete = new List<CaseShare>(); 
        for(CaseShare cs : lstExistingCaseShareRecords){
            strKey = cs.CaseId + Constants.TILDE_DELIMITER + cs.UserorGroupId;
            
            if(setPermissionsToDelete.contains(strKey)){
                //There is already a sharing existing 
                lstToDelete.add(cs);
            }
        }
        
        if(!lstToDelete.isEmpty())
          delete lstToDelete;
      }    
    }    
  }
}