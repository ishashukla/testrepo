/**================================================================      
* Appirio, Inc
* Name: COMM_displayEquipmentWizardController
* Description: Redirects to the appropriate flow based on record type after creating a new record for Equipment.  Class for T-502677
* Created Date: 22 May 2016
* Created By: Mehul Jain (Appirio)
*
* Date Modified      Modified By      Description of the update
* 11 Aug 2016        Kanika Mathur    Modified to include a Cancel button I-229592
==================================================================*/

public with sharing class COMM_displayEquipmentWizardController{
  public Equipment__c equipment{get;set;}
  public Id rId{get;set;}
  public Boolean flag{get;set;}
  public String recordType{get;set;}
  public String recordTypeName{get;set;}

  // set the RT name in Apex parameter 
  public COMM_displayEquipmentWizardController(ApexPages.StandardController cntrl){
    equipment = (Equipment__c) cntrl.getRecord();
    rId = equipment.Room__c;
    recordType = ApexPages.currentPage().getParameters().get('RecordType');
    system.debug('rt' + recordType);
    equipment.RecordTypeID = recordType;
    recordTypeName = getRTName(recordType);
    //to set the type field required if RT is Media Management
    flag = recordTypeName == Label.COMM_Equipment_Checklist_RT_Media_Management;
  }
  
  // returns the name of RT, takes RT ID as an argument
  public String getRTName(String rtId){
        String rtName ; 
        Schema.DescribeSObjectResult d = Schema.SObjectType.Equipment__c;
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        Schema.RecordTypeInfo rtById =  rtMapById.get(rtId);
        rtName = rtById.getName();
        system.debug('rt name : ' + rtName );
        return rtName;
  }
  
  // inserts a new record for equipment on save and redirects to appropirate flow
  public PageReference saveEquipment() {
    PageReference pr;
   
    if(equipment != null){
      try{
        insert equipment;
      }
      catch(Exception e){
        String error = 'Please resolve the following error:\n' + e.getMessage();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));   
      }        
      if(recordTypeName.equals(Label.COMM_Equipment_Checklist_RT_Booms)){    
          pr = new PageReference('/apex/COMMEquipmentBoomFlowPage'); 
      }    
      if(recordTypeName.equals(Label.COMM_Equipment_Checklist_RT_Doc_Station)){    
          pr = new PageReference('/apex/COMMEquipmentDocStationFlowPage'); 
      }    
      if(recordTypeName.equals(Label.COMM_Equipment_Checklist_RT_Exam_Light)){    
          pr = new PageReference('/apex/COMMEquipmentExamLightFlowPage'); 
      }    
      if(recordTypeName.equals(Label.COMM_Equipment_Checklist_RT_Lights)){    
          pr = new PageReference('/apex/COMMEquipmentLightsFlowPage'); 
      }    
      if(recordTypeName.equals(Label.COMM_Equipment_Checklist_RT_ORIS)){    
          pr = new PageReference('/apex/COMMEquipmentORISFlowPage'); 
      }    
      if(recordTypeName.equals(Label.COMM_Equipment_Checklist_RT_Media_Management)){ 
          if(equipment.Type__c == 'Connect Suite'){
            pr = new PageReference('/apex/COMMEquipmentMMConnectSuite');
          }
          if(equipment.Type__c == 'Suite Status'){
            pr = new PageReference('/apex/COMMEquipmentMMSuiteStatus');
          }
          if(equipment.Type__c == 'Studio3'){
            pr = new PageReference('/apex/COMMEquipmentMMStudio3');
          }
          if(equipment.Type__c == 'Data Mediator'){
            pr = new PageReference('/apex/COMMEquipmentMMDataMediator');
          }
           
      }    
      if(recordTypeName.equals(Label.COMM_Equipment_Checklist_RT_Tables)){    
          pr = new PageReference('/apex/COMMEquipmentTablesFlowPage'); 
      } 
      pr.setRedirect(false);
      return pr;
    }
    else{
     return null;
    }
  }
  
  public PageReference getFinishPage(){
    return new PageReference('/'+equipment.Id);
  }

  public PageReference cancelEquipment() {
    return new PageReference('/'+rId);
  }
  
}