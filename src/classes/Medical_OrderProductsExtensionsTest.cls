/*
//
// (c) 2011 Appirio, Inc.
//
// Test Class for Medical_OrderProductsExtensions used for Medical
//
// This is unit test class for coverage of Medical_OrderProductsExtensions
//
*/
@isTest
private class Medical_OrderProductsExtensionsTest {
  static testMethod void testMedical_OrderProductsExtensions(){
    Account acc = Medical_TestUtils.createAccount(10, true).get(0);
    Contact con = Medical_TestUtils.createContact(10, acc.Id, true).get(0);
    Product2 prod = Medical_TestUtils.createProduct(true);
    Pricebook2 pb = Medical_TestUtils.createPricebook(1,'Test PriceBook', true).get(0);
    PricebookEntry pbe1 = Medical_TestUtils.createPricebookEntry(prod.Id,pb.Id,false);
    PricebookEntry pbe2 = Medical_TestUtils.createPricebookEntry(prod.Id,Test.getStandardPricebookId(), false);
    insert (new List<PricebookEntry>{pbe2,pbe1});
    Order ord = Medical_TestUtils.createOrder(acc.Id,con.Id, pb.Id,true);
    OrderItem oi1 = Medical_TestUtils.createOrderItem(ord.Id,pbe1.Id ,false);
    oi1.Line_Number__c = 1;
    OrderItem oi2 = Medical_TestUtils.createOrderItem(ord.Id,pbe1.Id ,false);
    oi2.Line_Number__c = 1.1;
    insert (new List<OrderItem>{oi1,oi2});
    //oi.OrderId = ord.Id;
    PageReference pageRef = Page.Medical_OrderProducts;
    Test.setCurrentPage(pageRef);
    System.currentPageReference().getParameters().put('Id', String.valueOf(ord.Id));
    ApexPages.StandardController sc = new ApexPages.StandardController(ord);
    Medical_OrderProductsExtensions testOrderProduct = new Medical_OrderProductsExtensions(sc);
    Boolean proFound = testOrderProduct.isProductFound;
  }
  
  
  static testMethod void testMedicalOrderProductsExtension(){
    Account acc = Medical_TestUtils.createAccount(10, true).get(0);
    Contact con = Medical_TestUtils.createContact(10, acc.Id, true).get(0);
    Product2 prod = Medical_TestUtils.createProduct(true);
    Pricebook2 pb = Medical_TestUtils.createPricebook(1,'Test PriceBook', true).get(0);
    Order ord = Medical_TestUtils.createOrder(acc.Id,con.Id, pb.Id,true);
    PricebookEntry pbe1 = Medical_TestUtils.createPricebookEntry(prod.Id,pb.Id,false);
    PricebookEntry pbe2 = Medical_TestUtils.createPricebookEntry(prod.Id,Test.getStandardPricebookId(), false);
    insert (new List<PricebookEntry>{pbe2,pbe1});
    OrderItem oi = Medical_TestUtils.createOrderItem(ord.Id,pbe1.Id ,true);
    //oi.OrderId = ord.Id;
    PageReference pageRef = Page.Medical_OrderProducts;
    Test.setCurrentPage(pageRef);
    System.currentPageReference().getParameters().put('Id', String.valueOf(ord.Id));
    ApexPages.StandardController sc = new ApexPages.StandardController(ord);
    Medical_OrderProductsExtensions testOrderProduct = new Medical_OrderProductsExtensions(sc);
    Boolean proFound = testOrderProduct.isProductFound;
  }
}