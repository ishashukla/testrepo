/*
//
// (c) 2015 Appirio, Inc.
//
// HttpMockResponseClass
//
// April 26, 2016, Sunil (Appirio JDC)
//
// Test class for Mocking http response
*/
@isTest
global class HttpMockResponseClass implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        String xmlresponse = '<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">'+
                              '<?xml version="1.0" encoding="UTF-8"?><e2wSSOResponse><UniqueReqId>0163f9c6-9467-4509-ab9b-66d597874da1'+
                              '</UniqueReqId><UserId>troland</UserId><ErrorCode>0</ErrorCode><ErrorDescription></ErrorDescription></e2wSSOResponse></string>';
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody(xmlresponse);
        res.setStatusCode(200);
        return res;
    }
}