// (c) 2015 Appirio, Inc.
//
// Class Name: OpportunityTeamMultiRepManagerBatchTest 
// Description: Test Class for OpportunityTeamMultiRepManagerBatch class.
// 
// April 11 2016, Isha Shukla  Original 
//
@isTest  
public class OpportunityTeamMultiRepManagerBatchTest {
    static List<Multi_BU_Assignment__c> mbuList;
    static List<User> user;
    static testMethod void testNotifyAccountsBasedOnContracts() {  
          createData();
          System.RunAs(user[0]) {
              Test.startTest();
              mbuList = TestUtils.createMultiBUassign(1,user[1].Id,user[0].Id,'NSE',True);
              OpportunityTeamMultiRepManagerBatch oppTeam = new OpportunityTeamMultiRepManagerBatch();
              Database.executeBatch(oppTeam);
              Test.stopTest();
          }
        System.assertEquals(True, mbuList != Null);
    }
    public static void createData() {
        user = TestUtils.createUser(2, 'System Administrator', false );
        user[0].Division = 'NSE';
        user[1].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, True);
        	Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
        	Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        	List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
        	opp[0].Business_Unit__c = 'NSE';
        	opp[0].Opportunity_Number__c = '50001';
        	opp[0].OwnerId = user[0].Id;
        	opp[0].recordtypeId =  recordTypeInstrument;
        	insert opp; 
        	List<OpportunityTeamMember> memberList = TestUtils.createOpportunityTeamMember(1,opp[0].Id,user[0].Id,False);
        	memberList[0].OpportunityAccessLevel = 'Read';
        	insert memberList;
        }
    }
}