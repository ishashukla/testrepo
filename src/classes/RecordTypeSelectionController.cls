// (c) 2015 Appirio, Inc.
//
// Class Name: RecordTypeSelectionController
// Description: Contoller Class for RecordTypeSelection Page. Controller class for showing the record type selection page 
//
// March 09 2016, Prakarsh Jain  Original (T-454845)
//
public class RecordTypeSelectionController {
public String selectedOppRecordType {get;set;}
  public List<SelectOption> oppRecordTypes {get;set;}
  public Opportunity opp;
  public String oppId;
  public Account acc;
  public List<String> recordTypeNamesList;
  
  public RecordTypeSelectionController(ApexPages.StandardController stdController){
    oppRecordTypes = new List<SelectOption>();
    //Changes done for T-484108. Record Types are visible as per logged in user
    recordTypeNamesList = new List<String>();
    List<RecordTypeInfo> lstRecordTypes = Opportunity.sObjectType.getDescribe().getRecordTypeInfos();
    if (lstRecordTypes.size() > 1) {
      for (RecordTypeInfo recTypes : lstRecordTypes) {
        if (recTypes.isAvailable() && !String.valueOf(recTypes.getRecordTypeId()).endsWith('AAA')){
          recordTypeNamesList.add(recTypes.getName());
        }
      }
    } 
    else{
      recordTypeNamesList.add(lstRecordTypes[0].getName());
    }
    for(String recTypes : recordTypeNamesList){
      oppRecordTypes.add(new SelectOption(recTypes, recTypes));
    }
    String currentURLId  = ApexPages.currentPage().getParameters().get('retURL');
    oppId = currentURLId.substring(currentURLId.indexOf('006'));
    if(oppId!=null){
    	opp = [SELECT Id, Name, AccountId, Parent_Sales_Opportunity__c FROM Opportunity WHERE Id =: oppId].get(0);
   		acc = [SELECT Id, Name FROM Account WHERE Id =: opp.AccountId];
    }
  }
  
  //this function will redirect the user to Create New Opportunity page 
  public PageReference selectRecordType(){
    String recordTypeName;
    for(String recName : recordTypeNamesList){
      if(recName!=null && recName!= '' && recName == selectedOppRecordType){
        recordTypeName = recName;
      }
    }
    //'&RecordType='+selectedOppRecordType+ this causes the page to not load properly. Please investigate its need. ------ investigated and changed the code accordingly
    //Changes done as per chatter by Tom dated 12 March 2016, changed '&RecordType='+selectedOppRecordTypeId+ to '&RecordType='+selectedOppRecordType+ 
    Id selectedOppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(selectedOppRecordType).getRecordTypeId();
    String contentURL = String.valueOf(URL.getSalesforceBaseUrl().toExternalForm())+'/006/e?CF'+Label.Parent_Sales_Opportunity_Id+'='+opp.Name+'&CF'+Label.Parent_Sales_Opportunity_lkid+'='+opp.Id+'&retURL=%2F'+opp.Id+'&RecordType='+selectedOppRecordTypeId+'&ent=Opportunity&opp4_lkid='+opp.AccountId+'&opp4='+acc.Name;
   // String contentURL = String.valueOf(URL.getSalesforceBaseUrl().toExternalForm())+'/006/e?CF'+Label.Parent_Sales_Opportunity_Id+'='+opp.Name+'&CF'+Label.Parent_Sales_Opportunity_lkid+'='+opp.Id+'&retURL=%2F'+opp.Id+'&RecordType='+selectedOppRecordTypeId+'&ent=Opportunity&opp4_lkid='+opp.AccountId+'&opp4='+acc.Name+'&retURL=%2F'+opp.Id;
    return new PageReference(contentURL);
  }
  
  //This function will cancel the action and redirect to Opportunity standard Page
  public PageReference cancel(){
    return new PageReference('/'+opp.Id); 
  }
}