/*******************************************************************************
Author        :  Appirio JDC (Megha Agarwal)
Date          :  Oct 5th, 2015
Description       :  Test class for Medical_OverrideEditCaseController
*******************************************************************************/
@isTest
private class Medical_OverrideEditCaseControllerTest {
    private static List<Account>  accounts;
    private static List<Contact> contacts;
    private static List<Case>  cases;
    //private static List<Regulatory_Question_Answer__c> rqaList;
    
    private static void createTestData(){
        accounts = Medical_TestUtils.createAccount(1, true);
        contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);
        cases = Medical_TestUtils.createCase(1, accounts.get(0).id, contacts.get(0).id , false);
        //rqaList = Medical_TestUtils.createRegulatoryQuesAnswer(1,cases.get(0).Id,true);
    }
    
    static testMethod void redirectToForm(){
        createTestData();  
        Test.startTest();
        Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
        TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
        cases.get(0).recordtypeId = MedicalrecordTypeId;
        insert cases;
        ApexPages.StandardController sc = new ApexPages.StandardController(cases.get(0));
        Medical_OverrideEditCaseController cont = new Medical_OverrideEditCaseController(sc);
        PageReference pageRef = Page.Medical_OverrideEditCase;        
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('recordtype', MedicalrecordTypeId);
        cont.redirectCase();
        Test.stopTest();
    }
    static testMethod void redirectToFormForCOMMBP(){
        Id commBPAccountRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Id commBPCaseRecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        Account acc = TestUtils.createCommAccount(1, false).get(0);
        acc.RecordTypeId = commBPAccountRecordTypeId;
        acc.Endo_Active__c = true;
        insert acc;
        TestUtils.createRTMapCS(commBPCaseRecordTypeId, 'COMM US Stryker Field Service Case', 'COMM');
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeId = commBPCaseRecordTypeId;
        insert caseRecord;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRecord);
        Medical_OverrideEditCaseController cont = new Medical_OverrideEditCaseController(sc);
        PageReference pageRef = Page.Medical_OverrideEditCase;        
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('recordtype', commBPCaseRecordTypeId);
        cont.redirectCase();
        Test.stopTest();
    }
     static testMethod void redirectToEditPageForCOMMBP(){
        Id commBPAccountRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Id commBPCaseRecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        Account acc = TestUtils.createCommAccount(1, false).get(0);
        acc.RecordTypeId = commBPAccountRecordTypeId;
        acc.Endo_Active__c = true;
        insert acc;
        TestUtils.createRTMapCS(commBPCaseRecordTypeId, 'COMM US Stryker Field Service Case', 'COMM');
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeId = commBPCaseRecordTypeId;
        insert caseRecord;
        Regulatory_Question_Answer__c rqa = new Regulatory_Question_Answer__c(Case__c = caseRecord.Id);
        insert rqa;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRecord);
        Medical_OverrideEditCaseController cont = new Medical_OverrideEditCaseController(sc);
        PageReference pageRef = Page.Medical_OverrideEditCase;        
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('recordtype', commBPCaseRecordTypeId);
        cont.redirectCase();
        Test.stopTest();
    }
    static testMethod void redirectToForInstruments(){
        Id commBPAccountRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Id InstrumentrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId();
        Account acc = TestUtils.createCommAccount(1, false).get(0);
        acc.RecordTypeId = commBPAccountRecordTypeId;
        acc.Endo_Active__c = true;
        insert acc;
        TestUtils.createRTMapCS(InstrumentrecordTypeId, Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT, 'Instruments');
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeId = InstrumentrecordTypeId;
        insert caseRecord;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRecord);
        Medical_OverrideEditCaseController cont = new Medical_OverrideEditCaseController(sc);
        PageReference pageRef = Page.Medical_OverrideEditCase;        
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('recordtype', InstrumentrecordTypeId);
        cont.redirectCase();
        Test.stopTest();
    }
    static testMethod void redirectToFormForCOMMBPChangeRequest(){
        Id commBPAccountRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Id commBPCaseRecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('COMM Change Request').getRecordTypeId();
        Account acc = TestUtils.createCommAccount(1, false).get(0);
        acc.RecordTypeId = commBPAccountRecordTypeId;
        acc.Endo_Active__c = true;
        insert acc;
        TestUtils.createRTMapCS(commBPCaseRecordTypeId, 'COMM Change Request', 'COMM');
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeId = commBPCaseRecordTypeId;
        insert caseRecord;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRecord);
        Medical_OverrideEditCaseController cont = new Medical_OverrideEditCaseController(sc);
        PageReference pageRef = Page.Medical_OverrideEditCase;        
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('recordtype', commBPCaseRecordTypeId);
        cont.redirectCase();
        Test.stopTest();
    }
    static testMethod void redirectToObject(){
        createTestData();
        Medical_TestUtils.createRegulatoryQuesAnswer(1,cases.get(0).Id,true);
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(cases.get(0));
        Medical_OverrideEditCaseController cont = new Medical_OverrideEditCaseController(sc);
        PageReference pageRef = Page.Medical_OverrideEditCase;        
        Test.setCurrentPage(pageRef);
        cont.redirectCase();
        Test.stopTest();
    }
}