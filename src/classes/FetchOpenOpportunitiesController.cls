/************************************************************************
Name  : GeneratePDFofFinancialProfilesController 
Author: Naresh K Shiwani(Appirio)
Date  : 12 August, 2015
Description: Controller of FetchOpenOpportunities page.
*************************************************************************/
public with sharing class FetchOpenOpportunitiesController {
	
	//Variables
	public Contact con                     {get;set;}
	public list<Opportunity> openOppList   {get;set;}
	private set<Id> conIdList = new set<id>();
	private set<Id> oppIdList = new set<id>();
	//========================================================================//
  //Constructor
  //========================================================================//
  public FetchOpenOpportunitiesController(ApexPages.StandardController controller) {
    con = (Contact)controller.getRecord();
    openOppList = new list<Opportunity>();
    con = [Select id, title From Contact c where id =: con.id];
    if(con.Title == 'RM'){
    	for(Contact cont : [Select id From Contact c where ReportsToId =: con.id]){
    		conIdList.add(cont.id);
    	}
    	System.debug('conIdList===='+conIdList);
			if(conIdList != null && conIdList.size() > 0){
				for(OpportunityLineItem opp : [Select OpportunityId From OpportunityLineItem where PSR_Contact__c in : conIdList]){
					oppIdList.add(opp.OpportunityId);
				}
				System.debug('oppIdList=========='+oppIdList);
			  if(oppIdList != null && oppIdList.size() > 0){
			    openOppList = [ Select id, Legal_Name__c, stageName, Name, Deal_Type__c, Amount
			                    From Opportunity 
			                    where id in : oppIdList 
			                      and stagename not in ('Closed Won','Closed Lost')];
			  }
	    }
    }
    else
      System.debug('Non====RM===='+con);
  }

}