@isTest
private class BigMachinesNewQuoteCntrl_Test{
    static testMethod void testGetCreateURL() {
        Account acct = new Account();
        acct.Name = 'BigMachines test Account for testGetCreateURL';
        insert acct;
        List<Pricebook2> priceBookList = TestUtils.createpricebook(2,' ',false);
        priceBookList.get(0).Name = 'COMM Price Book';
        insert priceBookList;
        Opportunity opty = new Opportunity();
        opty.Name = 'BigMachines test Opportunity for testGetCreateURL';
        opty.StageName = 'Prospecting';
        opty.CloseDate = Date.today();
        insert opty;
        List<BigMachines__Configuration_Record__c> bigMachinesConfigList = TestUtils.createBigMachineConfig(2,true);
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'BigMachines test Quote for testGetCreateURL';
        quote.BigMachines__Opportunity__c = opty.id;
        quote.BigMachines__Transaction_Id__c = 'transactionId';
        quote.BigMachines__Site__c = bigMachinesConfigList.get(0).Id;
        insert quote;
        
        BigMachinesNewQuoteController controller = null;

        // error case - no account specified
        ApexPages.currentPage().getParameters().put('oppId', opty.id);
        try {
            controller = new BigMachinesNewQuoteController();
            System.assert(false);
        } catch (Exception e) {}
        
        // success case - create new quote
        opty.AccountId = acct.id;
        update opty;
        try {
            controller = new BigMachinesNewQuoteController();
        } catch (Exception e) {
            System.assert(false);
        }
        controller.bmUsesSSL = true;
        controller.buildURL();
        
        // success case - create clone of quote
        ApexPages.currentPage().getParameters().put('actId', acct.id);
        ApexPages.currentPage().getParameters().put('cloneId', quote.id);
        try {
            controller = new BigMachinesNewQuoteController();
        } catch (Exception e) {
            System.assert(false);
        }
        
        controller.bmUsesSSL = false;
        controller.buildURL();
        controller.getCreateURL();
        controller.getSubtitle();

        // error case - invalid clone object type
        ApexPages.currentPage().getParameters().put('cloneId', opty.id);
        try {
            controller = new BigMachinesNewQuoteController();
            System.assert(false);
        } catch (Exception e) {}

        // error case - invalid clone id
        ApexPages.currentPage().getParameters().put('cloneId', 'garbage');
        try {
            controller = new BigMachinesNewQuoteController();
            System.assert(false);
        } catch (Exception e) {}

        // error case - invalid account object type
        ApexPages.currentPage().getParameters().put('actId', opty.Id);
        try {
            controller = new BigMachinesNewQuoteController();
            System.assert(false);
        } catch (Exception e) {}
        
        // error case - invalid account id
        ApexPages.currentPage().getParameters().put('actId', 'garbage');
        try {
            controller = new BigMachinesNewQuoteController();
            System.assert(false);
        } catch (Exception e) {}

        // error case - invalid opportunity id
        ApexPages.currentPage().getParameters().put('oppId', 'garbage');
        try {
            controller = new BigMachinesNewQuoteController();
            System.assert(false);
        } catch (Exception e) {}
        
    }
}