/*
Name            ProjectPhaseDocusignController 
Created Date    06/23/2016
Created By      Rahul Aeran/*
Purpose         T-509669, controller for the page for adding Quick Actions in LEX for Docusign
*/
public class ProjectPhaseDocusignController{
  public String phaseId;
  public ProjectPhaseDocusignController(ApexPages.StandardController std){
    phaseId= std.getRecord().id; 

  }
  public PageReference redirectToProjectPacket(){
    String returnUrl = DocusignUtility.insertDocusignDocsFromPhase(phaseId);
    return new PageReference(returnUrl);
  }
  
 /* public PageReference redirectToBeneficialForm(){
    String returnUrl = DocusignUtility.insertBeneficialPhaseDocument(phaseId);
    return new PageReference(returnUrl);
  }
*/    
  public PageReference redirectToStandardDocusignPage(){
    String returnUrl = DocusignUtility.getStandardDocusignRedirectUrl(phaseId);
    return new PageReference(returnUrl);
  }
}