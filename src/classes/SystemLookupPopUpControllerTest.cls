// (c) 2015 Appirio, Inc.
//
// Class Name: SystemLookupPopUpControllerTest 
// Description: Test Class for SystemLookupPopUpController class.
// 
// April 12 2016, Isha Shukla  Original 
//
@isTest
private class SystemLookupPopUpControllerTest {
    Static List<System__c> sysList;
    @isTest static void testSystemLookupPopUpController() {
        createData();
        PageReference pageRef = Page.SystemLookupPopUp;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('bu','NSE');
        System.currentPageReference().getParameters().put('type','Stryker');
        SystemLookupPopUpController controller = new SystemLookupPopUpController();
        controller.listRecords = sysList;
        controller.runSearch();
        System.assertEquals(True, controller.listRecords != Null);
    }
    public static void createData() {
        sysList = createSystem(1,'test' ,'NSE' ,'Stryker',True);
    }
    public static List<System__c> createSystem(Integer sysCount,String sysName ,String businessunit ,String sysType,Boolean isInsert) {
        sysList = new List<System__c>();
       
        for(Integer i = 0; i < sysCount; i++) {
            System__c sysRecord = new System__c(Name = sysName , Business_Unit__c = businessunit ,Type__c = sysType);
            sysList.add(sysRecord);
        }
        if(isInsert) {
            insert sysList;
        } 
        return sysList;
    }
}