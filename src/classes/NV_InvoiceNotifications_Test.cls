//
// (c) 2015 Appirio, Inc.
//
// Test Class Name: NV_InvoiceNotifications_Test
// For Apex Classes: NV_NotifyOrdersPendingPO, NV_NotifyOrdersPastDue, NV_InvoiceNotifications
//
// 21st December 2015   Hemendra Singh Bhati   Original (Task # T-459199)
// 05th January 2015    Hemendra Singh Bhati   Modified (Task # T-462574) - Please see the task description for more details.
// Modified By :   Jessica Schilling
// Date        :   5/02/2016
// Case        :   Case 166348
// 
@isTest
private class NV_InvoiceNotifications_Test {
  // Private Data Members.
  private static final Id ACCOUNT_NV_CUSTOMER_RECORD_TYPE_ID = Schema.SObjectType.Account.RecordTypeInfosByName.get('NV Customer').RecordTypeId;
  private static final Id ORDER_NV_BILL_ONLY_RECORD_TYPE_ID = Schema.SObjectType.Order.RecordTypeInfosByName.get('NV Bill Only').RecordTypeId;

  /*
  @method      : validateOrderProcessingFunctionality
  @description : This method validates the apex logic that determines the orders and order invoices that will be processed by apex schedular.
  @params      : void
  @returns     : void
  */
  @isTest(seeAllData=false)
  private static void validateOrderProcessingFunctionality() {
    // Inserting Test Account.
    Account theTestAccount = NV_TestUtility.createAccount('Test Account', ACCOUNT_NV_CUSTOMER_RECORD_TYPE_ID, true);

    // Inserting Test Order.
    Order theTestOrder = NV_TestUtility.createOrder(theTestAccount.Id, null, ORDER_NV_BILL_ONLY_RECORD_TYPE_ID, false);
    theTestOrder.Date_Ordered__c = Date.today().addDays(-9);
    theTestOrder.Type = 'Customer Bill Only';
    theTestOrder.Status = 'Awaiting PO';
    theTestOrder.Customer_PO__c = 'PO To Follow';
    insert theTestOrder;

    system.debug([SELECT Status, Type, Days_Pending_PO__c FROM Order]);

    // Inserting Test Order Invoice.
    Order_Invoice__c theTestOrderInvoice = NV_TestUtility.createOrderInvoice(
      theTestOrder.Id,
      Date.today().addDays(-10),
      'Credit Memo - CM',
      'AR Transaction',
      '1234567890',
      true
    );

    // Inserting Test NV Follow Record.
    NV_Follow__c theOrderFollower = NV_TestUtility.createNVFollow(
      theTestOrder.Id,
      'Order',
      UserInfo.getUserId(),
      true
    );

    Test.startTest();

    system.schedule('Test Notify Orders Awaiting PO', '0 0 23 * * ?', new NV_NotifyOrdersPendingPO());
    system.schedule('Test Notify Orders Past Due', '0 0 23 * * ?', new NV_NotifyOrdersPastDue());

    Test.stopTest();
  }

  /*
  @method      : validateNotifyUsersFunctionality
  @description : This method validates the user notification logic that posts a chatter to the order record informing the order account
                 owners and order followers about awaiting PO orders and/or past due orders.
  @params      : void
  @returns     : void
  */
  @isTest(seeAllData=true)
  private static void validateNotifyUsersFunctionality() {
    // Inserting Test Account.
    Account theTestAccount = NV_TestUtility.createAccount('Test Account', ACCOUNT_NV_CUSTOMER_RECORD_TYPE_ID, true);

    // Inserting Test Order.
    Order theTestOrder = NV_TestUtility.createOrder(theTestAccount.Id, null, ORDER_NV_BILL_ONLY_RECORD_TYPE_ID, false);
    theTestOrder.EffectiveDate = Date.today().addDays(-10);
    theTestOrder.Type = 'Customer Bill Only';
    theTestOrder.Status = 'Awaiting PO';
    theTestOrder.Customer_PO__c = 'No PO To Follow';
    insert theTestOrder;

    // Inserting Test Order Invoice.
    Order_Invoice__c theTestOrderInvoice = NV_TestUtility.createOrderInvoice(
      theTestOrder.Id,
      Date.today().addDays(-10),
      'Credit Memo - CM',
      'AR Transaction',
      '1234567890',
      true
    );

    // Inserting Test NV Follow Record.
    NV_Follow__c theOrderFollower = NV_TestUtility.createNVFollow(
      theTestOrder.Id,
      'Order',
      UserInfo.getUserId(),
      true
    );

    Test.startTest();

    NV_InvoiceNotifications theInstance = new NV_InvoiceNotifications();
    theInstance.notifyUsers(
      new Map<Id, Set<Id>> {
        theTestOrder.Id => new Set<Id> {
          UserInfo.getUserId()
        }
      },
      new Map<Id, Integer> {
        theTestOrder.Id => 10
      },
      //START JSchilling Case 166348 5/02/2016
      //Added new parameter to map the Order's Id to its Account name, and replaced old message with the new message
      new Map<Id, String> {
        theTestOrder.Id => 'Test Account'
      },
      'This order for THE_ACCOUNT hospital has been pending a Bill Only PO for THE_DATE_DIFFERENCE days.\n\n'
      //'This order has been Awaiting PO for THE_DATE_DIFFERENCE days.\n\n'
      //END JSchilling Case 166348 5/02/2016
    );

    Test.stopTest();
	}
}