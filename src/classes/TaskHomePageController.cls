/**================================================================      
* Appirio, Inc
* Name: TaskHomePageController
* Description: To display task for logged in user
* Created Date: 27 July 2016
* Created By: Jagdeep Juneja (Appirio)
*
* Date Modified      Modified By      Description of the update
  23rd Aug 2016      Varun Vasishtha  I-231476
==================================================================*/

public class TaskHomePageController{
    public List<SelectOption> options {get;set;}
    public String selectedOption {get;set;}
    public List<sObject> sobjList  {get;set;}
    public List<Task>  taskList {get;set;}
    public String query;
    public Set<Id> idSet;
    public String userId;
    
    public TaskHomePageController(){
        selectedOption = 'All Tasks';  
        options = new List<SelectOption>();
        sobjList  = new List<sObject>();
        query = '';
        taskList = new List<Task>();
        idSet = new Set<Id>();
        userId = UserInfo.getUserId();
        //options.add(new SelectOption('','-None-'));
        options.add(new SelectOption('All Tasks','All Tasks'));
        options.add(new SelectOption('All Open Tasks','All Open Tasks'));
        options.add(new SelectOption('Account','Account'));
        options.add(new SelectOption('Contact','Contact'));
        options.add(new SelectOption('Case','Case'));
        options.add(new SelectOption('Order','Order'));
        options.add(new SelectOption('SVMXC__Service_Order__c','Work Order'));
        onChange();
    }
    
    public void onChange(){
        taskList = null;
        idSet = new Set<Id>();
        System.debug('selectedOption value is '+selectedOption);
        
        if(selectedOption.equals('All Tasks')){
            taskList = [SELECT id,whoId,OwnerId,CallType,Subject,whatId FROM Task WHERE ownerId=:userId];  
            System.debug('one');
        }
        else if(selectedOption.equals('All Open Tasks')){
            taskList = [SELECT id,whoId,OwnerId,CallType,Subject,whatId FROM Task WHERE ownerId=:userId and Status != 'Completed'];  
            System.debug('one');
        }
        else if(selectedOption.equals('Contact')){
            query = 'SELECT Id, WhoId, OwnerId, CallType, Subject, WhatId FROM Task WHERE OwnerId = :userId ' +
                        'AND WhoId IN (SELECT Id FROM ' + selectedOption +')';
            
            taskList = Database.query(query);
        }
        else{
            query = 'SELECT Id, WhoId, OwnerId, CallType, Subject, WhatId FROM Task WHERE OwnerId = :userId ' +
                        'AND WhatId IN (SELECT Id FROM ' + selectedOption +')';
            
            taskList = Database.query(query);
            
        }
    } 
}