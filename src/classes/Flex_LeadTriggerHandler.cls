//
// (c) 2015 Appirio, Inc.
//
// Class Name: AccountTriggerHandler
// Description: Handler Class for LeadTrigger
//
// 11th May 2015  Ravindra Shekhawat Original ( Task # S-326050)
//

public class Flex_LeadTriggerHandler {

  // iPad Quoter automatic lead assignment to correct RFM
  public static void assignLeadsToRFM(List<Lead> newLeads, Map<Id,Lead> oldLeadsMap) {
  	// Fetch Users based on Regions
    Map<String, List<String>> mapRegions = new Map<String, List<String>>();

    for(Flex_Sales_Rep__c objSalesRep :Flex_Sales_Rep__c.getall().values()) {
      if(mapRegions.containsKey(objSalesRep.RFM_Username__c) == false){
        mapRegions.put(objSalesRep.RFM_Username__c, new List<String>());
      }
      mapRegions.get(objSalesRep.RFM_Username__c).add(objSalesRep.Name);
    }
    System.debug('@@@' + mapRegions);


    Map<String, User> mapUsers = new Map<String, User>();
    for(User objUser :[SELECT Id, Username FROM User WHERE Username IN :mapRegions.keySet()]) {
      if(mapRegions.containsKey(objUser.Username) == true){
        List<String> lstRegions = mapRegions.get(objUser.Username);
          for(String region : lstRegions){
            mapUsers.put(region, objUser);
          }
        }
    }
    System.debug('@@@' + mapUsers);

    List<Lead> leadsToUpdate = new List<Lead>();
    for(Lead newLead : newLeads) {
      if(newLead.LeadSource == 'iPad Quoter' && newLead.Flex_Region__c !=null && (oldLeadsMap == null || newLead.Flex_Region__c != oldLeadsMap.get(newLead.Id).Flex_Region__c)){
        if(mapUsers.get(newLead.Flex_Region__c) !=null) {
          Lead existingLead = new Lead();
          existingLead.Id = newLead.Id;
          existingLead.OwnerId = mapUsers.get(newLead.Flex_Region__c).Id;
          leadsToUpdate.add(existingLead);
        }
      }
    }
    if(leadsToUpdate.size() > 0) {
      update leadsToUpdate;
    }
  }

}