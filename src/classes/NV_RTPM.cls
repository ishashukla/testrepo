public class NV_RTPM {
  public class ProductIDs {
    public String[] ProductID;
    private String[] ProductID_type_info = new String[]{'ProductID','http://www.stryker.com/ModelN/RTPM','string','1','-1','false'};
    private String[] apex_schema_type_info = new String[]{'http://www.stryker.com/ModelN/RTPM','true','false'};
    private String[] field_order_type_info = new String[]{'ProductID'};
  }
  public class RealTimePriceMultiRequestType {
    public String ConfigName;
    public String BusSegCode;
    public String CustomerID;
    public DateTime EffectiveDate;
    public DateTime ModelDate;
    public String CurrencyCode;
    public String OrgUnitID;
    public String ERPCode;
    public NV_RTPM.ProductIDs ProductIDs;
    private String[] ConfigName_type_info = new String[]{'ConfigName','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] BusSegCode_type_info = new String[]{'BusSegCode','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] CustomerID_type_info = new String[]{'CustomerID','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] EffectiveDate_type_info = new String[]{'EffectiveDate','http://www.stryker.com/ModelN/RTPM','dateTime','0','1','true'};
    private String[] ModelDate_type_info = new String[]{'ModelDate','http://www.stryker.com/ModelN/RTPM','dateTime','0','1','true'};
    private String[] CurrencyCode_type_info = new String[]{'CurrencyCode','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] OrgUnitID_type_info = new String[]{'OrgUnitID','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ERPCode_type_info = new String[]{'ERPCode','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ProductIDs_type_info = new String[]{'ProductIDs','http://www.stryker.com/ModelN/RTPM','ProductIDs','1','1','false'};
    private String[] apex_schema_type_info = new String[]{'http://www.stryker.com/ModelN/RTPM','true','false'};
    private String[] field_order_type_info = new String[]{'ConfigName','BusSegCode','CustomerID','EffectiveDate','ModelDate','CurrencyCode','OrgUnitID','ERPCode','ProductIDs'};
  }
  public class RealTimePriceMultiResponseType {
    public NV_RTPM.ResolvedPrices ResolvedPrices;
    public String ExecTime;
    public String ResponseStatus;
    public String ResponseMessageType;
    public String ResponseMessage;
    private String[] ResolvedPrices_type_info = new String[]{'ResolvedPrices','http://www.stryker.com/ModelN/RTPM','ResolvedPrices','0','1','true'};
    private String[] ResponseStatus_type_info = new String[]{'ResponseStatus','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ExecTime_type_info = new String[]{'ExecTime','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ResponseMessageType_type_info = new String[]{'ResponseMessageType','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ResponseMessage_type_info = new String[]{'ResponseMessage','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] apex_schema_type_info = new String[]{'http://www.stryker.com/ModelN/RTPM','true','false'};
    private String[] field_order_type_info = new String[]{'ResolvedPrices','ExecTime','ResponseStatus','ResponseMessageType','ResponseMessage'};
  }
  public class ResolvedPrices {
    public NV_RTPM.ResolvedPriceType[] ResolvedPrice;
    private String[] ResolvedPrice_type_info = new String[]{'ResolvedPrice','http://www.stryker.com/ModelN/RTPM','ResolvedPriceType','0','-1','false'};
    private String[] apex_schema_type_info = new String[]{'http://www.stryker.com/ModelN/RTPM','true','false'};
    private String[] field_order_type_info = new String[]{'ResolvedPrice'};
  }
  public class ResolvedPriceType {
    @AuraEnabled
    public String ProductID {get;set;}
    @AuraEnabled
    public String ResolvedPrice {get;set;}
    @AuraEnabled
    public String ResolvedCurrency {get;set;}
    @AuraEnabled
    public String CommitmentID {get;set;}
    @AuraEnabled
    public String ResolvedBasePrice {get;set;}
    @AuraEnabled
    public String ResolvedDiscount {get;set;}
    @AuraEnabled
    public String ResolvedDiscountType {get;set;}
    @AuraEnabled
    public String ResolvedTierIndex {get;set;}
    @AuraEnabled
    public String ContractIDNumber {get;set;}
    @AuraEnabled
    public String ProductGroupID {get;set;}
    @AuraEnabled
    public String ResolvedSubContractType {get;set;}
    @AuraEnabled
    public String ResolvedPremium {get;set;}
    @AuraEnabled
    public String ResolvedPriceBeforePremium {get;set;}
    @AuraEnabled
    public String DiscountableFlag {get;set;}
    @AuraEnabled
    public String TradableUOM {get;set;}
    @AuraEnabled
    public String BuyingGroupID {get;set;}
    @AuraEnabled
    public String AssetMgmtLeasePONumber {get;set;}
    @AuraEnabled
    public String Status {get;set;}
    @AuraEnabled
    public String MessageType {get;set;}
    @AuraEnabled
    public String Message {get;set;}
    private String[] ProductID_type_info = new String[]{'ProductID','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ResolvedPrice_type_info = new String[]{'ResolvedPrice','http://www.stryker.com/ModelN/RTPM','null-decimal','0','1','true'};
    private String[] ResolvedCurrency_type_info = new String[]{'ResolvedCurrency','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] CommitmentID_type_info = new String[]{'CommitmentID','http://www.stryker.com/ModelN/RTPM','null-integer','0','1','true'};
    private String[] ResolvedBasePrice_type_info = new String[]{'ResolvedBasePrice','http://www.stryker.com/ModelN/RTPM','null-decimal','0','1','true'};
    private String[] ResolvedDiscount_type_info = new String[]{'ResolvedDiscount','http://www.stryker.com/ModelN/RTPM','null-decimal','0','1','true'};
    private String[] ResolvedDiscountType_type_info = new String[]{'ResolvedDiscountType','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ResolvedTierIndex_type_info = new String[]{'ResolvedTierIndex','http://www.stryker.com/ModelN/RTPM','null-integer','0','1','true'};
    private String[] ContractIDNumber_type_info = new String[]{'ContractIDNumber','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ProductGroupID_type_info = new String[]{'ProductGroupID','http://www.stryker.com/ModelN/RTPM','null-integer','0','1','true'};
    private String[] ResolvedSubContractType_type_info = new String[]{'ResolvedSubContractType','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ResolvedPremium_type_info = new String[]{'ResolvedPremium','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] ResolvedPriceBeforePremium_type_info = new String[]{'ResolvedPriceBeforePremium','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] DiscountableFlag_type_info = new String[]{'DiscountableFlag','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] TradableUOM_type_info = new String[]{'TradableUOM','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] BuyingGroupID_type_info = new String[]{'BuyingGroupID','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] AssetMgmtLeasePONumber_type_info = new String[]{'AssetMgmtLeasePONumber','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] Status_type_info = new String[]{'Status','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] MessageType_type_info = new String[]{'MessageType','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] Message_type_info = new String[]{'Message','http://www.stryker.com/ModelN/RTPM','string','0','1','true'};
    private String[] apex_schema_type_info = new String[]{'http://www.stryker.com/ModelN/RTPM','true','false'};
    private String[] field_order_type_info = new String[]{'ProductID','ResolvedPrice','ResolvedCurrency','CommitmentID','ResolvedBasePrice','ResolvedDiscount','ResolvedDiscountType','ResolvedTierIndex','ContractIDNumber','ProductGroupID','ResolvedSubContractType','ResolvedPremium', 'ResolvedPriceBeforePremium','DiscountableFlag', 'TradableUOM','BuyingGroupID','AssetMgmtLeasePONumber','Status','MessageType','Message'};
  }
}