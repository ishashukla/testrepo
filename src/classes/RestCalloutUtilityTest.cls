/**================================================================      
 * Appirio, Inc
 * Name: RestCalloutUtilityTest
 * Description: Test class for RestCalloutUtility
 * Created Date: 26/08/2016
 * Created By: Pratibha Chhimpa (Appirio)
 * 
 * Date Modified      Modified By                   Description of the update
 *
 ==================================================================*/

@isTest
private class RestCalloutUtilityTest{
    @isTest static void testEchoString() {              
        
              
        List<Account> acctest = new List<Account>();
        acctest  = Endo_TestUtils.createAccount(1, true);
        List<Contact> contacttest = new List<Contact>();
        contacttest =  Endo_TestUtils.createContact(1,acctest [0].id,true);
        Order ordtest = Endo_TestUtils.createStandardOrder(contacttest[0].id,acctest [0].id ,true);
        
        RestCalloutUtility testrest = new RestCalloutUtility();
        RestCalloutUtility.informaticaRequest infoReq = new RestCalloutUtility.informaticaRequest();
        RestCalloutUtility.TagName tagName  = new RestCalloutUtility.TagName();
        RestCalloutUtility.IntegrationHeader infoHead = new RestCalloutUtility.IntegrationHeader();
        RestCalloutUtility.orderHeader ordHead = new RestCalloutUtility.orderHeader();
        RestCalloutUtility.salesOrderRequest salesOrd = new RestCalloutUtility.salesOrderRequest();
        RestCalloutUtility.salesOrderResponse salesRes = new RestCalloutUtility.salesOrderResponse();
        

        // Call the method that invokes a callout
        String output = RestCalloutUtility.createOrderRestCallout(ordtest );
        
    }
}