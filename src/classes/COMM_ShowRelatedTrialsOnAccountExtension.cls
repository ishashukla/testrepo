/**================================================================      
 * Appirio, Inc
 * Name: COMM_ShowRelatedTrialsOnAccount
 * Description:  Extention class for visualforce page COMM_ShowRelatedTrialsOnAccount
 * Created Date: 08/08/2016
 * Created By: Sahil Batra
 * 
 * 08 Aug 2016        Sahil Batra         Class creation. T-525271
 ==================================================================*/

public class COMM_ShowRelatedTrialsOnAccountExtension {
	// Public variables used on VF page.
	public Id accountId{get;set;}
	public List<Trial__c> trailList{get;set;}
	public List<String> selectedFields{get;set;}
	public Map<String,String> apiToLabel{get;set;}
	public ApexPages.StandardSetController setCon{get;set;}
	public Integer size{get;set;}
	public Integer noOfRecords{get;set;}
	public String ascDesc;
	public String sortedField{get;set;}
	public boolean isAsc{get;set;}
	public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Trial__c.FieldSets.TrailFieldSet.getFields();
    }
    
	// Paginations variables start
	public Boolean hasNext {
  		get {
    		return setCon.getHasNext();
  		}
  		set;
  	}
  	public Boolean hasPrevious {
  		get {
   			return setCon.getHasPrevious();
  		}
  		set;
  	}
    public Integer pageNumber {
  		get {
     		return setCon.getPageNumber();
 		}
  		set;
  	}
	 
	
	
	//-----------------------------------------------------------------------------------------------
    //  Cosntructor
    //-----------------------------------------------------------------------------------------------
    public COMM_ShowRelatedTrialsOnAccountExtension(ApexPages.StandardController stdController){
    	sortedField = '';
    	ascDesc = '';
    	accountId = stdController.getId();
    	trailList = new List<Trial__c>();
    	size = 5;
    	setCon = new ApexPages.StandardSetController(trailList);
    	setCon.setPageSize(size);
    	noOfRecords = setCon.getResultSize();
    	setCon.setPageNumber(1);
    	createQuery();
    }
    
    // Utility method to create query
    public void createQuery(){
      selectedFields = new List<String>();
      apiToLabel = new Map<String,String>();
      if(accountId!=null){
    	String query = 'SELECT Id ';
        for(Schema.FieldSetMember f : this.getFields()){
           	query += ' '+','+f.getFieldPath()+' '; 
           	selectedFields.add(f.getFieldPath());
           	if(!apiToLabel.containsKey(f.getFieldPath())) {
               	apiToLabel.put(f.getFieldPath(),f.getLabel());
       		}
       	}
       	query += 'FROM Trial__c WHERE Opportunity__r.AccountId = :accountId';
       	 if(sortedField!=''){
			query += ' order by '+ sortedField + ' ' + ascDesc +' nulls last';
		}
       	List<Trial__c> tempList = new List<Trial__c>();
      	tempList = Database.query(query);
      	setPagenation(tempList);
      }
    }
    
  	
  	//@method      : first
  	//@description : Pagination method
  	public void first() {
    	setCon.first();
    	getAllRecords();
  	}
  	
  	
  	//@method      : last
  	//@description : Pagination method
  	public void last() {
    	setCon.last();
    	getAllRecords();
  	}
  	
  	//@method      : previous
 	//@description : Pagination method
  	public void previous() {
    	setCon.previous();
    	getAllRecords();
  	}
  	

	//	@method      : next
  	//  @description : Pagination method
  	public void next() {
    	setCon.next();
    	getAllRecords();
  	}
  	
  	//@method      : getAllOpportunity
  	//@description : Pagination method
  	public void getAllRecords(){
   		trailList = new List<Trial__c>();
   		for(Trial__c trial : (List<Trial__c>)setCon.getRecords()){
        	trailList.add(trial);
   		}
  	}
  	
    
  	//@method      : setPagenation
 	//@description : Pagenation Method
    public void setPagenation(List<Trial__c> tempList){
       setCon = new ApexPages.StandardSetController(tempList);
       setCon.setPageSize(size);
       noOfRecords = setCon.getResultSize();
       setCon.setPageNumber(1);
       getAllRecords();
    }
    
    // Method to Sort Data
    public void sortData(){
		if(sortedField!=''){
	   		if(ascDesc == ' desc '){
          		ascDesc = ' asc ';
          		isAsc = true;
       		}
       	else{
    	   		ascDesc = ' desc ';
           		isAsc = false;
       		}
		}
		createQuery();
	}
	
}