// (c) 2015 Appirio, Inc.
//
// Class Name: DialogBoxPageOppControllerTest
// Description: Test Class for DialogBoxPageOpportunityController.
//
// July 11 2016, Prakarsh Jain  Original (T-518521)
@isTest
public class DialogBoxPageOppControllerTest {
    static Account acc;
    static Opportunity opp;
    static OpportunityTeamMember oppMember;
    static List<User> usrList;
    static User userOwner, integration;
    static OpportunityShare oppShare;
    static RecordType oppInstrumentsRecType = [Select SobjectType, Name, Id, DeveloperName From RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Instruments_Opportunity_Record_Type'];
    //Method to check when user other than the owner is trying to delete the opportunity.  
    @isTest static void testDialogBoxPageOppController1(){
        createTestData();  
        PageReference pageRef = Page.DialogBoxPageOpportunity;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('oppId', String.valueOf(opp.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        DialogBoxPageOpportunityController controller = new DialogBoxPageOpportunityController(sc);
        system.runAs(usrList[1]){
            Test.startTest();
            controller.showPopup();
            controller.closePopup();
            controller.closePopupYes();
            Test.stopTest();
            Opportunity oppt = [SELECT Id, Name, Is_Deleted__c FROM Opportunity WHERE Id =: opp.Id];
            System.assertEquals(oppt.Is_Deleted__c, false);
        }  
    } 
    //Method to check when user is trying to delete the opportunity.
    @isTest static void testController() {
        createTestData();
        Test.startTest();
        
        PageReference pageRef = Page.DialogBoxPageOpportunity;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('oppId', String.valueOf(opp.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        DialogBoxPageOpportunityController controller = new DialogBoxPageOpportunityController(sc);
        System.runAs(userOwner) {
            controller.closePopupYes();
        }
        Test.stopTest();
        List<OpportunityShare> opportunityShareList = [SELECT UserOrGroupId, RowCause, OpportunityId, OpportunityAccessLevel, Id 
                                                       FROM OpportunityShare 
                                                       WHERE OpportunityId = :opp.Id AND RowCause != :Schema.OpportunityShare.RowCause.Owner];
        System.assertEquals(opportunityShareList.size(),1); //NB - 12/1 - I-242624
        List<OpportunityTeamMember> oppTeamMemberlst = [SELECT Id FROM OpportunityTeamMember 
                                                        WHERE OpportunityId = :opp.Id];
        System.assertEquals(oppTeamMemberlst.size(), 1);   //NB - 12/1 - I-242624
        Opportunity oppty = [SELECT Id, Name,OwnerId, Is_Deleted__c FROM Opportunity WHERE Id =: opp.Id];
        System.assertEquals(oppty.Is_Deleted__c, true);
    }
    //creates Test Data  
    public static void createTestData(){
        usrList = TestUtils.createUser(2, 'Instruments Sales User', false);
        usrList[0].Division = 'NSE';
        usrList[1].Division = 'NSE';
        insert usrList;
        Profile prfl = TestUtils.fetchProfile('Instruments Sales User');
        userOwner = TestUtils.createUser(1,prfl.Name, false).get(0);
        userOwner.Division = 'NSE';
        insert userOwner;
        User sysAdmin = TestUtils.createUser(1,'System Administrator', false).get(0);
        sysAdmin.Division = 'NSE';
        insert sysAdmin;
        System.runAs(sysAdmin) {
            acc = TestUtils.createAccount(1, true).get(0);
            opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.Opportunity_Number__c = '50001';
            opp.OwnerId = userOwner.Id;
            opp.RecordTypeId = oppInstrumentsRecType.Id;
            opp.StageName = 'Final Quote';
            insert opp;
            oppMember = TestUtils.createOpportunityTeamMember(1, opp.Id, usrList[1].Id, true).get(0);
            oppShare = new OpportunityShare();
            oppShare.UserOrGroupId = usrList[0].Id;
            oppShare.OpportunityId = opp.Id;
            oppShare.OpportunityAccessLevel = 'Edit';
            oppShare.RowCause = Schema.OpportunityShare.RowCause.Manual;  
            insert oppShare;  
            system.debug('oppShare>>>'+oppShare);
        }
    }
}