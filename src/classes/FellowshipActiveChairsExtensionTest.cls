/*******************************************************************
Name  :  FellowshipActiveChairsExtensionTest 
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 16, 2016

Modified: 
*************************************************************************/
@isTest
private class FellowshipActiveChairsExtensionTest {
    
    @isTest static void testMethodOne() {
        Account acc = Endo_TestUtils.createAccount(1, true).get(0);
        List<Contact> contactList = Endo_TestUtils.createContact(1, acc.Id, true);
        Fellowship__c fs = new Fellowship__c(Account__c = acc.Id);
        insert fs;
        Fellowship_Member__c fsm = new Fellowship_Member__c(Fellowship__c = fs.Id,  Status__c = 'Active',Contact__c=contactList[0].Id);
        insert fsm;
        
        PageReference pageRef = Page.FellowshipActiveChairs;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('Id',String.valueOf(fs.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(fs);

        FellowshipActiveChairs_Extension fellowshipEnrolledMembersExt = new FellowshipActiveChairs_Extension(sc);
        fellowshipEnrolledMembersExt.first();
        fellowshipEnrolledMembersExt.last();
        fellowshipEnrolledMembersExt.sortedField = 'Contact__c';
        fellowshipEnrolledMembersExt.ascDesc = ' desc ';
        fellowshipEnrolledMembersExt.sortData();
        Boolean hasnext = fellowshipEnrolledMembersExt.hasNext;
        Boolean hasprevious = fellowshipEnrolledMembersExt.hasPrevious;
        Integer page1 = fellowshipEnrolledMembersExt.pageNumber;
        System.assertEquals(false,fellowshipEnrolledMembersExt.hasNext);
        System.assertEquals(false,fellowshipEnrolledMembersExt.hasPrevious);
        System.assertEquals(1,fellowshipEnrolledMembersExt.pageNumber);
        
    }

    @isTest static void testMethodTwo() {
        
        Account acc = Endo_TestUtils.createAccount(1, true).get(0);
        Fellowship__c fs = new Fellowship__c(Account__c = acc.Id);
        insert fs;
        Fellowship_Member__c fsm = new Fellowship_Member__c(Fellowship__c = fs.Id,  Status__c = 'Test');
        insert fsm;
        
        PageReference pageRef = Page.FellowshipActiveChairs;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('Id',String.valueOf(fs.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(fs);
        FellowshipActiveChairs_Extension fellowshipEnrolledMembersExt = new FellowshipActiveChairs_Extension(sc);
        fellowshipEnrolledMembersExt.next();
        fellowshipEnrolledMembersExt.previous();
        fellowshipEnrolledMembersExt.sortedField = 'Contact__c';
        fellowshipEnrolledMembersExt.ascDesc = ' asc ';
        fellowshipEnrolledMembersExt.sortData();
        System.assertEquals(true, fsm != Null);
    }
    
}