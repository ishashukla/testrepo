/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 13, 2013
 Purpose       :  Wrapper class for Intel records REST services
 Reference     :  Task T-205140, T-205116
 Modifications :  Nov 16, 2013      Task T-205631      Sonal Shrivastava
                  Nov 26, 2013      Task T-205594      Sonal Shrivastava
                  Nov 29, 2013      Task T-216318      Sonal Shrivastava
                  Dec 12, 2013      Task T-219134      Sonal Shrivastava
                  Dec 12, 2013      Task T-217201      Igor Andrtosov 
                  Dec 13, 2013      Task T-217389      Sonal Shrivastava
                  Dec 14, 2013      Task T-215773      Pitamber Sharma
                  Dec 18, 2013      Task T-222508      Dipika Gupta
                  Dec 31, 2013      Task T-226840      Sonal Shrivastava
                  Jan 07, 2014      Issue I-93226      Sonal Shrivastava
                  Jan 13, 2014      Issue I-94893      Sonal Shrivastava
                  Feb 24, 2014      Task T-242140      Sonal Shrivastava
                  Feb 27, 2014      Issue I-103111     Sonal Shrivastava
                  Feb 28, 2014      Issue I-103110     Sonal Shrivastava
                  Mar 04, 2014      Task T-249952      Brandon Jones
                  Mar 04, 2014      Task T-249946      Brandon Jones
*******************************************************************************/
global with sharing class IntelListWrapper {
  
  public List<IntelJson> intelList;
    
    //****************************************************************************
    // Default Constructor
    //****************************************************************************
    public IntelListWrapper(){}
    
    //****************************************************************************
    // Parameterized Constructor
    //****************************************************************************
    public IntelListWrapper(RestRequest req){
      String lastSync     = null;
      Integer batchOffset = 0;
      String searchTerm   = null;
      String userId       = null;   
      List<IntelJson> listFromIntels;
      List<IntelJson> listFromComments;
      Set<String> intelIdSet; 
      
      try{
      //get lastSync, batchOffset and searchTerm from url params
      if(req.params.containsKey('lastSync')){
        lastSync = req.params.get('lastSync');
      }
      if(req.params.containsKey('batchOffset')){
        batchOffset = Integer.valueOf(req.params.get('batchOffset'));
      }
      if(req.params.containsKey('searchTerm')){
        searchTerm = req.params.get('searchTerm');
      }
      if(req.params.containsKey('userId')){
        userId = req.params.get('userId');
      }
      if(req.params.containsKey('userId')){
        userId = req.params.get('userId');
      }
      intelIdSet = new Set<String>();
      if(req.params.containsKey('intelId')){
        String intelIdToQuery = req.params.get('intelId');
        if(intelIdToQuery != null && intelIdToQuery != ''){
            intelIdSet.add(intelIdToQuery);
        }
      }
      system.debug('PARAMS userId: '+userId);
      //Fetch Intel records 
      listFromIntels = queryIntel(lastSync, batchOffset, searchTerm, userId, intelIdSet);
      this.intelList = listFromIntels;
      
      /* Don't do this anymore!  this was causing major issues with pagination.  we'll refactor the relationship later
        if(userId != null && userId != ''){ 
        listFromComments = queryIntelRelatedToComments(userId, listFromIntels);
        if(listFromComments.size() > 0){
          this.intelList.addAll(listFromComments);
        }
      }*/
      
      }catch(Exception ex){
        System.debug('exception --->>> ' + ex.getMessage());
        throw ex;
      }
    }
    
    //****************************************************************************
    // Method for querying Intel Records related to comments for mentioned user 
    //****************************************************************************
    public List<IntelJson> queryIntelRelatedToComments(String userId, List<IntelJson> listFromIntels){              
        
    List<IntelJson> listFromComments = new List<IntelJson>();
    Set<String> fetchedIntelIdsSet = new Set<String>();
    Set<String> commentIntelIdsSet = new Set<String>();
    try{
        //Add already fetched intel Ids in a set
        for(IntelJson inteljson : listFromIntels){
        fetchedIntelIdsSet.add(inteljson.id); 
        }
        //Fetch comment records of the mentioned user
        Map<String, List<FeedComment>> mapIntelId_lstComment = WithoutSharingUtility.fetchMapIntelPostComments(userId);
        
        //Remove duplicate intel ids
        if( mapIntelId_lstComment.size() > 0){
          commentIntelIdsSet = mapIntelId_lstComment.keySet().clone();
          commentIntelIdsSet.removeAll(fetchedIntelIdsSet);
          listFromComments = queryIntel(null, null, null, null, commentIntelIdsSet);
        }
    }catch(Exception ex){
        throw ex;
        return null;
    }
    return listFromComments;
    }
    
    //****************************************************************************
    // Method for querying Intel records
    //****************************************************************************
    public List<IntelJson> queryIntel(String lastSync, Integer batchOffset, String searchTerm, String userId, Set<String> intelIdsSet){
      List<IntelJson> intelList       = new List<IntelJson>();
      String whereClause      = ' WHERE Chatter_Post__c <> null AND Intel_Inactive__c = false ';
      String qryOffSet        = '';
      String tagWhereClause = '';
      String qryFields        = '';
      String qryChildFields   = '';   
      String orderByField     = ' ORDER BY Mobile_Update__c DESC Nulls last';
      String qryLimit         = ' LIMIT 20 '; 
      
      try{
      for(Schema.FieldSetMember f : SObjectType.Intel__c.FieldSets.Intel_Fields.getFields()) {
        qryFields = (qryFields != '') ? (qryFields + ', ' + f.getFieldPath()) : f.getFieldPath();
      }           
      for(Schema.FieldSetMember f : SObjectType.Tag__c.FieldSets.Tag_Fields.getFields()) {
        qryChildFields = (qryChildFields != '') ? (qryChildFields + ', ' + f.getFieldPath()) : f.getFieldPath();
      }
          
      if(searchTerm != null && searchTerm != '' && intelIdsSet.size() == 0){
        system.debug('SearchTerm: '+ searchTerm);
        tagWhereClause = ' WHERE Title__c LIKE \'%' + searchTerm + '%\' ';
        
        //If serach term is provided, execute query on Tag__c and add their Intels in IntelIdSet
        For(Tag__c tg : Database.query('SELECT Id, Intel__c FROM Tag__c ' + tagWhereClause)){
            intelIdsSet.add(tg.Intel__c);
        } 
      }       
      // If batchOffset is sent, pull based on records that have NOT changed/new since last refreshed datetime  [I-93226]
      if(lastSync != null && lastSync != ''){
        if(batchOffset != null && batchOffset > 0){
          whereClause += ' AND Mobile_Update__c != null AND Mobile_Update__c < ' + lastSync + ' ';
          qryOffSet = ' OFFSET ' + batchOffset + ' ';
        }
        else {
          whereClause += ' AND Mobile_Update__c != null AND Mobile_Update__c >= ' + lastSync + ' ';
        }   
      }else {
          whereClause += ' AND Mobile_Update__c != null ';
      } 
      system.debug('PARAMS userId: '+userId);
      if(userId != null && userId != ''){
        String userClause = ' CreatedById = \'' + userId + '\' ';
        whereClause = whereClause + ' AND ' + userClause;
        system.debug('WHERE CLAUSE: '+whereClause); 
      }
      if(intelIdsSet != null && intelIdsSet.size() > 0){
        String idClause = ' Id IN ( ';
        for(String intelId : intelIdsSet){
          idClause += '\'' + intelId + '\',';
        }    
        idClause = idClause.substring(0, idClause.length()-1);         
        idClause += ' ) ';          
        whereClause = whereClause + ' AND ' + idClause;
      }
             
      String qryChildTag = ', (SELECT ' + qryChildFields + ' FROM Tags__r )' ;
      String qryChildFlag = ', (SELECT Id FROM Intel_Flag__r) ';
      String qry = 'SELECT ' + qryFields + qryChildTag + qryChildFlag + ' FROM Intel__c ' + whereClause + orderByField + qryLimit + qryOffSet;
          
      System.debug('query --->>> ' + qry);
      
      List<Intel__c> fetchedIntelList = new List<Intel__c>();      
      For(Intel__c intel : Database.query(qry)){
        fetchedIntelList.add(intel);
      }
          
      // Get chatter replies
      Map<String, String> mapChatterPost_IntelId = new Map<String, String>();
      Map<String, List<ConnectApi.Comment>> mapIntelId_ReplyList = new Map<String, List<ConnectApi.Comment>>();      
      for(Intel__c intel : fetchedIntelList){
        if(intel.Chatter_Post__c != null && intel.Chatter_Post__c != ''){
           mapChatterPost_IntelId.put(intel.Chatter_Post__c, intel.Id);
        }       
      }
        if(mapChatterPost_IntelId.keySet().size() > 0){
          mapIntelId_ReplyList = WithoutSharingUtility.fetchPostReplies(mapChatterPost_IntelId);
        }
            
      Map<String, List<FeedLike>> mapIntelId_LikeList  = fetchLikes(fetchedIntelList);
      // Get additional User data Photo URLs
      Map<String, User> intelUserData   = fetchUserInfo(getUserFromIntel(fetchedIntelList));
      
      For(Intel__c intel : fetchedIntelList){   
        
        List<TagJson> tagList         = new List<TagJson>();
        List<CommentJson> commentList = new List<CommentJson>();
        List<AttachmentJson> attachmentList = new List<AttachmentJson>();
        List<LikeJson> likeList       = new List<LikeJson>();
        
        // Pick only those Intel records where search term is not given OR
        // if given, then only those Intel records which have Tag records matching 
        // the search string.
        if(searchTerm == null || searchTerm == '' || intel.Tags__r.size() > 0){
          for(Tag__c childTag : intel.Tags__r){
            tagList.add(new TagJson(childTag));
          } 
                 
            //Find Comment and attachment List   
            if(mapIntelId_ReplyList.containsKey(intel.Id)){
                for(ConnectApi.Comment comment : mapIntelId_ReplyList.get(intel.Id)){               
                if(String.valueOf(comment.Type) == 'ContentComment'){
                    commentList.add(new CommentJson(comment));
                if(comment.attachment != null && comment.attachment instanceof ConnectApi.ContentAttachment){
                    ConnectApi.ContentAttachment content = (ConnectApi.ContentAttachment)comment.attachment;
                    attachmentList.add(new AttachmentJson(comment, content));
                }                    
                }
                else if(String.valueOf(comment.Type) == 'TextComment' ||
                        Test.isRunningTest() && comment.Type == null){
                commentList.add(new CommentJson(comment));
                }
                }
              } 
              
          //Find Like List
          if(mapIntelId_LikeList.containsKey(intel.Id)){
            for(FeedLike lik : mapIntelId_LikeList.get(intel.Id)){
              likeList.add(new LikeJson(lik));
            }
          }
          User usr = intelUserData.get(intel.OwnerId);
          if (usr != null){
            intelList.add(new IntelJson(intel, usr, tagList, commentList, likeList,attachmentList));
          }
        }
      } 
      } catch(Exception ex){
        throw ex;
        return null;
      }
      return intelList;
    }
  
  //****************************************************************************
  // Method for querying Chatter likes
  //****************************************************************************
  public Map<String, List<FeedLike>> fetchLikes(List<Intel__c> intelList){
    
    Map<String, List<FeedLike>> mapIntelId_LikeList = new Map<String, List<FeedLike>>();
    Map<String, String> mapChatterPost_IntelId = new Map<String, String>();
    
    for(Intel__c intel : intelList){
      if(intel.Chatter_Post__c != null && intel.Chatter_Post__c != ''){
        mapChatterPost_IntelId.put(intel.Chatter_Post__c, intel.Id);
      }     
    }
    
    for(Intel__Feed intelFeed : [SELECT ParentId, LikeCount, 
                                   (SELECT Id, FeedItemId, CreatedById, CreatedBy.Name
                                    FROM FeedLikes 
                                    WHERE FeedItemId IN :mapChatterPost_IntelId.keySet())
                                 FROM Intel__Feed 
                                 WHERE LikeCount > 0 AND ParentId IN :mapChatterPost_IntelId.values()]){
      if(intelFeed.FeedLikes.size() > 0){

          for(FeedLike lik : intelFeed.FeedLikes){
            if(lik.FeedItemId != null){
              
              if(!mapIntelId_LikeList.containsKey(intelFeed.ParentId)){
                mapIntelId_LikeList.put(intelFeed.ParentId, new List<FeedLike>());
              }
              mapIntelId_LikeList.get(intelFeed.ParentId).add(lik);
            }
          }
      }
    }
    return mapIntelId_LikeList; 
  }

  //****************************************************************************
  // Method for querying User Data
  //****************************************************************************
  public static List<String> getUserFromIntel(List<Intel__c> intList) {
      List<String> usrLst = new List<String>();
      for (Intel__c c : intList){
          usrLst.add(c.OwnerId);
      }
      return usrLst;
  }

  //****************************************************************************
  // Method for querying User Data
  //****************************************************************************
  public List<String> getUserFromFeedComment(Map<String, List<FeedComment>> commentMap) {
      List<String> usrLst = new List<String>();
      
      if(commentMap.keySet().size() > 0){
         for( String key : commentMap.keySet()) {
             for(FeedComment comment : commentMap.get(key)){
                 if(comment.FeedItemId != null && comment.CreatedById != null){
                    usrLst.add(comment.CreatedById);
                 }
             }
         }      
      }      
      return usrLst;
  }
  
  //****************************************************************************
  // Method for querying User Data
  //****************************************************************************
  public static Map<String, User> fetchUserInfo(List<String> userIdList) {
      Map<String, User> usrMap = new Map<String, User>();
      List<User> usrLst = [SELECT Id, Name, Phone, SmallPhotoUrl, FullPhotoUrl FROM User WHERE Id IN : userIdList ];
      for (User u : usrLst){
          usrMap.put(u.Id, u);
      }
      return usrMap;
  }
  
  //****************************************************************************  
  // Conetent URL does not work in this case TOO MANY redirects errors, 
  // replace it with WORKING URL format: https://cs8.salesforce.com/profilephoto/
  //****************************************************************************
  public static String replaceContentURL(String url) {
      String filename = url ; //'https://cs8.salesforce.com/profilephoto/729L0000000CcX2/F';
      if (url == null)
          return null;
      List<String> parts = filename.split('/');
      String baseUrl = String.valueof(system.URL.getSalesforceBaseUrl().toExternalForm());
      filename = baseUrl+'/profilephoto/'+parts[parts.size()-2] + '/'+parts[parts.size()-1];
        
      return filename;
  }  
  
  //****************************************************************************  
  // Conetent ID used to make file name, take last part of URL get ID 729L0000000CcX2
  // return the content id
  //****************************************************************************
  public static String getContentId(String url) {
      String content = ''; //'https://cs8.salesforce.com/profilephoto/729L0000000CcX2/F';
      if (url == null)
          return null;
      List<String> parts = url.split('/');
      content = parts[parts.size()-2];
        
      return content;
  }  
  
    //****************************************************************************
    // Wrapper class for Intel__c fields
    //****************************************************************************
    public class IntelJson {
      public String id;
      public String postedById;
      public String postedByName;
      public String authorPhone;
      public String authorSmallPhotoUrl;
      public String authorFullPhotoUrl;
      public String photoId;
      public DateTime posted;
      public String content;
      public Boolean flagged;
      public DateTime lastModifiedDate;
      public String chatterPost;
      public List<TagJson> tags;
      public List<CommentJson> postComments;
      public List<LikeJson> postLikes;
      public List<AttachmentJson> postAttachments;
  

    // New constructor to add User data to Intel record
    public IntelJson(Intel__c intel, User usr, List<TagJson> tagList, List<CommentJson> commentList, List<LikeJson> likeList ,List<AttachmentJson> attachmentList){
        this.id               = intel.Id;
        this.postedById       = intel.OwnerId;
        this.postedByName     = intel.Owner.Name;
        this.authorPhone      = intel.Owner.Phone;
        this.photoId          = IntelListWrapper.getContentId(usr.SmallPhotoUrl);              
        this.authorSmallPhotoUrl = IntelListWrapper.replaceContentURL(usr.SmallPhotoUrl);
        this.authorFullPhotoUrl  = IntelListWrapper.replaceContentURL(usr.FullPhotoUrl);      
        this.posted           = intel.CreatedDate;
        this.content          = intel.Details__c;
        this.flagged          = (intel.Intel_Flags__c > 0) ? true : false; 
        this.lastModifiedDate = intel.Mobile_Update__c;
        this.chatterPost      = intel.Chatter_Post__c;
        this.tags             = tagList;
        this.postComments     = commentList;
        this.postLikes        = likeList;
        this.postAttachments  = attachmentList;
    }
  }
  
  //****************************************************************************
  // Wrapper class for Tag__c fields
  //****************************************************************************
  public class TagJson{
      public String id;
      public String Name;
      public String Title;
      public String Account;
      public String Contact;
      public String Product;
      public String User;
      public DateTime CreatedDate;
        
      public TagJson(Tag__c tag){
        this.Id          = tag.Id;
        this.Name        = tag.Name;
        this.Title       = tag.Title__c;
        this.Account     = tag.Account__c;
        this.Contact     = tag.Contact__c;
        this.Product     = (tag.Product__c != null && tag.Product__r.Taggable__c) ? tag.Product__c : null;  
        this.User        = tag.User__c;
        this.CreatedDate = tag.CreatedDate;
      }
  }
  
  //****************************************************************************
  // Wrapper class for FeedComment fields
  //****************************************************************************
  public class CommentJson{
    public String id;
    public String body;
    public String postedById;
    public String postedBy;
    public DateTime createdDate;
    public String authorPhone;
    public String authorSmallPhotoUrl;
    public String authorFullPhotoUrl;
    public String photoId;
    
    public CommentJson(ConnectApi.Comment comment){
        this.id          = !Test.isRunningTest() ? comment.Id : null;
        this.Body        = !Test.isRunningTest() ? comment.body.text : null;
        this.postedById  = !Test.isRunningTest() ? comment.user.id : null;
        this.postedBy    = !Test.isRunningTest() ? comment.user.name : null;
        this.createdDate = !Test.isRunningTest() ? comment.CreatedDate : null;        
        this.photoId     = !Test.isRunningTest() ? IntelListWrapper.getContentId(comment.user.photo.SmallPhotoUrl) : null;              
        this.authorSmallPhotoUrl = !Test.isRunningTest() ? IntelListWrapper.replaceContentURL(comment.user.photo.SmallPhotoUrl) : null;
        this.authorFullPhotoUrl  = !Test.isRunningTest() ? IntelListWrapper.replaceContentURL(comment.user.photo.largePhotoUrl) : null;
    }
    
    // New Constructor to add User data
    public CommentJson(FeedComment comment, User usr){
        this.id          = comment.Id;
        this.Body        = comment.CommentBody;
        this.postedById  = comment.CreatedById; 
        this.postedBy    = comment.CreatedBy.Name;
        this.createdDate = comment.CreatedDate;
        this.photoId     = IntelListWrapper.getContentId(usr.SmallPhotoUrl);              
        this.authorSmallPhotoUrl = IntelListWrapper.replaceContentURL(usr.SmallPhotoUrl);
        this.authorFullPhotoUrl  = IntelListWrapper.replaceContentURL(usr.FullPhotoUrl);
    }
  }
 
  //****************************************************************************
  // Wrapper class for FeedLike fields
  //**************************************************************************** 
  public class LikeJson{
    public String id;
    public String postedById;
    public String postedBy;
    
    public LikeJson(FeedLike postLike){
      this.id         = postLike.Id;
      this.postedById = postLike.CreatedById; 
      this.postedBy   = postLike.CreatedBy.Name;
    }
  }  
  
  //****************************************************************************
  // Wrapper class for attachments
  //**************************************************************************** 
  public class AttachmentJson{
    public String id;
    public String url;
    public String documentId;
    public String description;
    
    public AttachmentJson(ConnectApi.Comment comment, ConnectApi.ContentAttachment content){
      this.id         = comment.Id;
      this.url        = String.valueof(system.URL.getSalesforceBaseUrl().toExternalForm()) + '/' + content.versionId; 
      this.documentId = content.Id;
      this.description = comment.body.text;
    }
  }
}