/**=====================================================================
 * Appirio, Inc
 * Name: ManageOrderProductsController, created for COMM
 * Description: T-493111
 * Created Date: 04/25/2016
 * Created By: Rahul Aeran
 *
 * Date Modified      Modified By              Description of the update
 * 10 Aug 2016        Kanika Mathur            COMM I-228955: Modified to remove Room assignment to OLI and modify save button
 * 11 Aug 2016		  Ralf Hoffmann            COMM I-229577: added Order_number to SOQL from Order Line item
 * 30 Sep 2016        Kanika Mathur            COMM I-237348: Modified query to include Oracle_Order_Line_Number_Formula__c and sort it by same field
 * 15 Oct 2016		  Ralf Hoffmann			   COMM I-240239: added Order_number-In_Oracle__c to query for page. 
 * 31 Oct 2016        Ralf Hoffmann            COMM I-242229: added Tracked as Software, Track as Asset in SFDC to query
 * 02 Nov 2016	 	  Ralf Hoffmann			   COMM I-242477: changed Initial Where clause to only get Tracked as Asset or Tracked as Software
=====================================================================*/
public with sharing class ManageOrderProductsController{
  public String projectPhaseId{get;set;}
  public String projectPlanId{get;set;}
  public Project_Phase__c selectedProjectPhase{get;set;}
  public List<OrderLineItemAddressController.OrderLineItemWrapper> items{get;set;}
  
  public ManageOrderProductsController(){
    projectPhaseId = ApexPages.currentPage().getParameters().get('id');
    if(projectPhaseId == null){
        return;
    }
    
    List<Project_Phase__c> lstProjectPhases = [SELECT Id,Name,Project_Plan__c,Project_Plan__r.Name,Project_Plan__r.Account__c,
                                                Project_Plan__r.Account__r.Name
                                                FROM Project_Phase__c
                                                WHERE Id = :projectPhaseId];
    if(lstProjectPhases.isEmpty())
        return;
    selectedProjectPhase = lstProjectPhases.get(0);
    projectPlanId = selectedProjectPhase.Project_Plan__c;
    
    initializeItems();
  }
  
  public PageReference backToPhase() {
      return new PageReference('/'+projectPhaseId);
  }
  
  public PageReference updateProjectPhase(){
        //Pagereference pageRef = new PageReference('/apex/OrderProductsPage?id='+projectPhaseId);
        Boolean isErrorExist = false;
        List<OrderItem> lstOrderItemsToUpdate = new List<OrderItem>();
        for(OrderLineItemAddressController.OrderLIneItemWrapper wrap : items){
            if(wrap.isSelected){
                lstOrderItemsToUpdate.add(new OrderItem(Id=wrap.oli.Id,Project_Phase__c = selectedProjectPhase.Id));
            }else{
                if(wrap.oli.Project_Phase__c == selectedProjectPhase.Id){
                    //updating the rooms is only allowed for same project phase records
                    lstOrderItemsToUpdate.add(new OrderItem(Id=wrap.oli.Id));
                }else{
                    // we do not need to do any update here
                    
                }
            }
        }
        
        
        if(items.isEmpty()){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.COMM_OLIAddressUpdate_NO_OLI)); 
        }else{
            if(!lstOrderItemsToUpdate.isEmpty()){
                Database.SaveResult []sr = Database.update(lstOrderItemsToUpdate,false);
                for(Database.SaveResult s : sr){
                    if(!s.isSuccess()){
                        isErrorExist = true;
                        for(Database.Error err : s.getErrors()){ 
                            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage()));
                        }
                    }
                }
            }
            initializeItems();
            if(!isErrorExist)
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.OLI_Update_Project_Phase));
        }
        return null;
    }
  
  
  private void initializeItems(){
    items = new List<OrderLineItemAddressController.OrderLIneItemWrapper>();
    OrderLineItemAddressController.OrderLIneItemWrapper objWrapper;
    //Boolean isCheckBox = false;
    //Modified query to include Oracle_Order_Line_Number_Formula__c and sort it by same field - I-237348; KM 30 Sep, 2016 
    //Modified query to include Order_Number_in_Oracle__c ; I-240239 RH 15 Oct 2016;
    //Modified query to include Tracked as Software, Track as Asset in SFDC; I-242229 RH 31 Oct 2016;
    //Modified query replace Where clause from Tracked_in_IB__c = True to the below; I-242477 RH 2 Nov 2016;
    for(Order ord : [SELECT Id,(SELECT id,OrderItemNumber,Oracle_Order_Line_Number__c, Oracle_Order_Line_Number_To_Sort__c,
                        Description,Pricebookentry.Product2Id,Pricebookentry.Product2.name,Quantity, Quantity__c, Pricebookentry.Product2.Qip_Required__c,
                        PriceBookentry.product2.ProductCode,Project_Phase__c,Project_Plan__c,Order_Number_in_Oracle__c,
                        PriceBookentry.product2.Tracked_as_Software__c,PriceBookentry.product2.Tracked_as_Asset_in_SFDC__c, 
                        PriceBookentry.product2.Description,Project_Plan__r.Name,Project_Phase__r.Name,Hold_Description__c, Order_Number__c
                        FROM OrderItems
                        WHERE PriceBookentry.product2.Tracked_as_Software__c = true OR PriceBookentry.product2.Tracked_as_Asset_in_SFDC__c = true
                        ORDER BY Oracle_Order_Line_Number_To_Sort__c) 
                      FROM Order
                      WHERE Project_Plan__c = :projectPlanId
                      ORDER BY OrderNumber]){
       for(OrderItem oi: ord.OrderItems){
         objWrapper = new OrderLineItemAddressController.OrderLIneItemWrapper(oi,false);
         items.add(objWrapper);
       }                
    }
  }
  
}