/**================================================================      
* Appirio, Inc
* Name: COMM_HomePageAuraComponentControllerTest
* Description: Test class of COMM_HomePageAuraComponentController
* Created Date: 29th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 
@isTest
private class COMM_HomePageAuraComponentControllerTest
{
	@isTest
	static void itShould()
	{
		  Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
          Id recordTypeRep = rtByNameRep.getRecordTypeId();
          Schema.RecordTypeInfo rtByNameManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast');
          Id recordTypeManager = rtByNameManager.getRecordTypeId();
          Profile pr =  TestUtils.fetchProfile('System Administrator');
          User usr = TestUtils.createUser(1, pr.Name, false).get(0);
          usr.Division = 'NSE';
          insert usr;
          system.runAs(usr){
              Test.startTest();
                  Account acc = TestUtils.createAccount(1,true).get(0);
                  List<Opportunity> oppList = new List<Opportunity>();
                  Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
                  opp.Business_Unit__c = 'NSE';
                  //opp.Opportunity_Number__c = '50001';
                  opp.CloseDate = System.today();
                  opp.ForecastCategoryName = 'Pipeline';              
                  opp.OwnerId = usr.Id;
                  oppList.add(opp);
                  Opportunity opp1 = TestUtils.createOpportunity(1,acc.Id,false).get(0);
                  opp1.Business_Unit__c = 'NSE';
                  opp1.Opportunity_Number__c = '50001';
                  opp1.CloseDate = System.today();
                  opp1.ForecastCategoryName = 'Pipeline';              
                  opp1.OwnerId = usr.Id;
                  oppList.add(opp1);
                  insert oppList;
                  List<Forecast_Year__c> lstForcastYear = TestUtils.createForcastYear(1,usr.Id,recordTypeRep,false);
                  lstForcastYear[0].Business_Unit__c = 'NSE';
                  lstForcastYear[0].Manager__c = usr.Id;
                  lstForcastYear[0].Year__c = String.valueOf(System.today().year());
                  lstForcastYear[0].Year_External_Id__c= '1234';

                  lstForcastYear.add(TestUtils.createForcastYear(1,usr.Id,recordTypeManager,false).get(0));
                  lstForcastYear[1].Manager__c = usr.Id;
                  lstForcastYear[1].Business_Unit__c = 'NSE';
                  lstForcastYear[1].Year__c = String.valueOf(System.today().year());
                  lstForcastYear[1].Year_External_Id__c= '12345';
                  insert lstForcastYear;   
                  Monthly_Forecast__c monthlyforcast = [select Id,Quota__c,Forecast_Submitted__c from Monthly_Forecast__c where Forecast_Record_Type_Id__c = :recordTypeRep LIMIT 1];
                  System.assert(monthlyforcast !=null ,'monthly forecasts should be addded related to forecast');
                  monthlyforcast.Forecast_Submitted__c = true;
                  List<String> monthString = new List<String>{'','January','February','March','April','May','June','July','August','September','October','November','December'};
                  monthlyforcast.Month__c = monthString.get(System.today().month());
                  monthlyforcast.Quota__c = 12;            
                  update monthlyforcast;
                  

                  List<String> charDataList = COMM_HomePageAuraComponentController.getTotalSales();
              	  system.assertNotEquals(null, charDataList);

              	  lstForcastYear = [select Id,YTD_Quota__c from Forecast_Year__c];
                  lstForcastYear[0].YTD_Quota__c= 144;
                  lstForcastYear[1].YTD_Quota__c= 140;
                  update lstForcastYear;
                  List<Monthly_Forecast__c> monthlyforcastlst = new List<Monthly_Forecast__c>();
                  for(Monthly_Forecast__c monthlyforcast1 : [select Id,Quota__c,Forecast_Submitted__c, Is_User_And_Current_Year_Data__c from Monthly_Forecast__c]){
                  	monthlyforcast1.Month__c = monthString.get(System.today().month());
                 // 	monthlyforcast1.Is_User_And_Current_Year_Data__c = 1;
                  	monthlyforcast1.Quota__c = 12;
                  	monthlyforcast1.Total_Sales__c = 100;
                  	monthlyforcastlst.add(monthlyforcast1);
                  }
                  update monthlyforcastlst;
              
              	  charDataList = COMM_HomePageAuraComponentController.getTotalSales();
              	  system.assertNotEquals(null, charDataList);
              	  system.assertEquals(System.Label.StrykerPulseiFrame, COMM_HomePageAuraComponentController.getStrykerPulseiUrl());
          	  Test.stopTest(); 	
          }

	}
}