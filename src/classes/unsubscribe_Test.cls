/*************************************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  Sep 08, 2016 
 Purpose       :  Test class for unsubscribe apex class
**************************************************************************************************/
@isTest(seeAllData = true)
private class unsubscribe_Test{

// Test method to ensure you have enough code coverage
  // Have created two methods, one that does the testing
  // with a valid "unsubcribe" in the subject line
  // and one the does not contain "unsubscribe" in the
  // subject line
  
static testMethod void testUnsubscribe() {

// Create a new email and envelope object
   Messaging.InboundEmail email = new Messaging.InboundEmail() ;
   Messaging.InboundEnvelope env   = new Messaging.InboundEnvelope();

// Create a new test Lead and insert it in the Test Method        
   Lead l = new lead(firstName='Rasmus', 
         lastName='Mencke',
         Company='Salesforce', 
         Email='rmencke@salesforce.com', 
         HasOptedOutOfEmail=false);
   insert l;

   Account acc = TestUtils.createAccount(1, true).get(0);
// Create a new test Contact and insert it in the Test Method  
   Contact c = new Contact(firstName='Rasmus', 
           lastName='Mencke', 
           Email='rmencke@salesforce.com', 
           HasOptedOutOfEmail=false,
           AccountId = acc.Id);
   insert c;
   
   // test with subject that matches the unsubscribe statement
   email.subject = 'test unsubscribe test';
   env.fromAddress = 'rmencke@salesforce.com';
   
   // call the class and test it with the data in the testMethod
   unsubscribe unsubscribeObj = new unsubscribe();
   unsubscribeObj.handleInboundEmail(email, env );
            
   }
 
static testMethod void testUnsubscribe2() {

// Create a new email and envelope object
   Messaging.InboundEmail email = new Messaging.InboundEmail();
   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

// Create a new test Lead and insert it in the Test Method        
   Lead l = new lead(firstName='Rasmus', 
         lastName='Mencke',
         Company='Salesforce', 
         Email='rmencke@salesforce.com', 
         HasOptedOutOfEmail=false);
   insert l;

   Account acc = TestUtils.createAccount(1, true).get(0);
// Create a new test Contact and insert it in the Test Method    
   Contact c = new Contact(firstName='Rasmus', 
           lastName='Mencke', 
           Email='rmencke@salesforce.com', 
           HasOptedOutOfEmail=false,
           AccountId = acc.Id);
   insert c;
   
   // Test with a subject that does Not contain unsubscribe
   email.subject = 'test';
   env.fromAddress = 'rmencke@salesforce.com';

   // call the class and test it with the data in the testMethod
   unsubscribe unsubscribeObj = new unsubscribe();
   unsubscribeObj.handleInboundEmail(email, env );            
   }    


}