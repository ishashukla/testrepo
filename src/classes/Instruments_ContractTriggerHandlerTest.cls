// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_ContractTriggerHandlerTest
// Description: Test Class for Instruments_ContractTriggerHandler
// 
@isTest
private class Instruments_ContractTriggerHandlerTest {
    static testMethod void Instruments_ContractTriggerHandlerTest(){
        List<Days_Left_For_Reminder__c> daysLeft = new List<Days_Left_For_Reminder__c>();
        Days_Left_For_Reminder__c dyLeft = new Days_Left_For_Reminder__c();
        dyLeft.Initial_Notification_Days__c = 180;
        dyLeft.Name = 'Notification';
        dyLeft.Next_Notification_Days__c = 30;
        daysLeft.add(dyLeft);
        insert daysLeft;
        Account acc = TestUtils.createAccount(1, true).get(0);
        Contract cron = new Contract();
        cron.AccountId = acc.Id;
        cron.Status = 'Draft';
        cron.External_Integration_Id__c = '123123';
        cron.StartDate = Date.Today();
        cron.ContractTerm = 6;
        insert cron;
    }
    
    static testMethod void Instruments_ContractTriggerHandlerTest1(){
        List<Days_Left_For_Reminder__c> daysLeft = new List<Days_Left_For_Reminder__c>();
        Days_Left_For_Reminder__c dyLeft = new Days_Left_For_Reminder__c();
        dyLeft.Initial_Notification_Days__c = 180;
        dyLeft.Name = 'Notification';
        dyLeft.Next_Notification_Days__c = 90;
        daysLeft.add(dyLeft);
        insert daysLeft;
        Account acc = TestUtils.createAccount(1, true).get(0);
        Contract cron = new Contract();
        cron.AccountId = acc.Id;
        cron.Status = 'Draft';
        cron.External_Integration_Id__c = '123123';
        cron.StartDate = Date.Today();
        cron.ContractTerm = 3;
        insert cron;
    }
}