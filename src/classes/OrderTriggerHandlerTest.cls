//
// (c) 2016 Appirio, Inc.
//
// Test class for trigger OrderTriggerHandler
//
// 20 May 2016     Pratibha Chhimpa       Original
//
@isTest
private class OrderTriggerHandlerTest {
    private static List<Account> accounts;
    private static List<Contact> contacts;
    private static List<Case> cases;
    
    // Get standard price book ID.
    private static Id pricebookId = Test.getStandardPricebookId();
    
    // Id for std Order record type 'Endo Orders'
    private static Id endoOrdersId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Endo Orders').getRecordTypeId();
    
    //  Test Method for insert of order
    static testMethod void testOrderOnSupoortCases() {
        Schema.DescribeSObjectResult caseRT = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> caseRTMap = caseRT.getRecordTypeInfosByName(); 
        Id caseRTId = caseRTMap.get('Sales Order').getRecordTypeId();
        TestUtils.createRTMapCS(caseRTId,'Sales Order','Endo');
        accounts = Medical_TestUtils.createAccount(1, true);
        contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);
        cases = Medical_TestUtils.createCase(1,accounts.get(0).id,contacts.get(0).id,false );
        cases.get(0).RecordTypeId = caseRTId;
        insert cases;
        // Test for Insert of Order Oracle_Order_Number__c should match with Oracle_Order_Ref__c 
        // Record Type should always be Endo Orders
        Test.startTest();
        Order order = Medical_TestUtils.createOrder(accounts.get(0).id, contacts.get(0).id, pricebookId ,false);
        order.Name = 'Test';
        order.Oracle_Order_Number__c = '1234';
        order.RecordTypeId = endoOrdersId;
        insert order;
        Test.stopTest();
        
        // Assert to test Current_Case_StdOrder__c is inserting properly
        Case c = [Select Id, Current_Case_StdOrder__c from Case Where Id =:cases.get(0).id];
        System.assertEquals(c.Current_Case_StdOrder__c , order.id);
                
    }
    
    //  Test Method for update of order
    static testMethod void testOrderOnSupoortCasesUpdate() {
       
        accounts = Medical_TestUtils.createAccount(1, true);
        contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);
        cases = Medical_TestUtils.createCase(1, accounts.get(0).id,contacts.get(0).id, false);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Operations');
        TestUtils.createRTMapCS(rtByName.getRecordTypeId(),'Sales Operations','Endo');
        cases[0].recordTypeId = rtByName.getRecordTypeId(); 
        insert cases;
        
        // Test for Update of Order Oracle_Order_Number__c should match with Oracle_Order_Ref__c
        // Order Name should always be updated
        // Record Type should always be Endo Orders
        Test.startTest();
        Order order = Medical_TestUtils.createOrder(accounts.get(0).id, contacts.get(0).id, pricebookId ,false);
        order.Name = 'Test';
        insert order;
        order.Name = 'UpdateTest';
        order.Oracle_Order_Number__c = '1234';
        order.RecordTypeId = endoOrdersId;
        update order;
        Test.stopTest();
        
        // Assert to test Current_Case_StdOrder__c is inserting properly
        Case c = [Select Id, Current_Case_StdOrder__c from Case Where Id =:cases.get(0).id];
        System.assertEquals(c.Current_Case_StdOrder__c , order.id);
                
    }
    
    //  Test Method for insert  UpdateSalesrepDetailOnOrder 
    static testMethod void testUpdateSalesrepDetailOninsert() {
        accounts = Endo_TestUtils.createAccount(1, true);
        contacts =  Endo_TestUtils.createContact(1, accounts.get(0).id,true);
        contacts[0].Sales_Rep_ID__c = '123.000000000000000';
        update contacts;
        
        // Record Type should always be Endo Orders
        Test.startTest();
        Order order = Medical_TestUtils.createOrder(accounts.get(0).id, contacts.get(0).id, pricebookId ,false);
        order.Name = 'Test';
        order.Oracle_Order_Number__c = '1234';
        order.Salesrep_Id__c = '123';
        order.RecordTypeId = endoOrdersId;
        insert order;
        Test.stopTest();
        
        // Assert to test
        Order ord1 = [Select Id, Salesrep_Name__c, Sales_Director__c, Regional_Manager__c from Order Where Id =:order.id];
        contact c1 = [SELECT ID, Name FROM Contact WHERE id =: contacts.get(0).id];
        System.assertEquals(ord1.Salesrep_Name__c , c1.Name);
        
    }
    
    //  Test Method for insert  UpdateSalesrepDetailOnOrder 
    static testMethod void testUpdateSalesrepDetailOnupdate() {
        accounts = Endo_TestUtils.createAccount(1, true);
        contacts =  Endo_TestUtils.createContact(1, accounts.get(0).id,true);
        contacts[0].Sales_Rep_ID__c = '123.000000000000000';
        update contacts;
        
        // Record Type should always be Endo Orders

        Order order = Medical_TestUtils.createOrder(accounts.get(0).id, contacts.get(0).id, pricebookId ,false);
        order.Name = 'Test';
        order.Oracle_Order_Number__c = '1234';
        order.Salesrep_Id__c = '123';
        order.RecordTypeId = endoOrdersId;
        insert order;
        
        Test.startTest();
        order.Salesrep_Id__c = '124';
        update order;
        Test.stopTest();
        
        // Assert to test
        Order ord1 = [Select Id, Salesrep_Name__c, Sales_Director__c, Regional_Manager__c from Order Where Id =:order.id];
        System.assertEquals(ord1.Salesrep_Name__c , null);
    }
}