// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncCycleCountTransaction_Queue -  Queueable Apex to sync new cycle count records
//
// 28 Oct 2016, Isha Shukla (T-550259) 
public class COMM_SyncCycleCountTransaction_Queue implements Queueable,Database.AllowsCallouts{
    List<SVMXC__Stock_Adjustment__c> listOfStockAdjust = new List<SVMXC__Stock_Adjustment__c>();
    public COMM_SyncCycleCountTransaction_Queue(List<SVMXC__Stock_Adjustment__c> listOfStockAdjust) {
        this.listOfStockAdjust = listOfStockAdjust;  
    }
    public void execute(QueueableContext context) {
        COMM_SyncEBSInventory_REST.InventorySyncRequest inventorySync = new COMM_SyncEBSInventory_REST.InventorySyncRequest();
        COMM_SyncEBSInventory_REST.inventoryDetails inventoryDetails = new COMM_SyncEBSInventory_REST.inventoryDetails();        
        List<COMM_SyncEBSInventory_REST.inventory> listOfinventory = new list<COMM_SyncEBSInventory_REST.inventory>();
        List<COMM_SyncEBSInventory_REST.inventoryLines> listOfinventoryLines = new List<COMM_SyncEBSInventory_REST.inventoryLines>();
        
        for(SVMXC__Stock_Adjustment__c varhead : listOfStockAdjust){
            List<COMM_SyncEBSInventory_REST.inventoryLine> lstInventoryLine = new List<COMM_SyncEBSInventory_REST.inventoryLine>();
            COMM_SyncEBSInventory_REST.inventoryLine tempInventoryLine = new COMM_SyncEBSInventory_REST.inventoryLine();
            tempInventoryLine.serialLotNumber = new COMM_SyncEBSInventory_REST.TagName(varhead.Asset__r.SVMXC__Serial_Lot_Number__c);
            tempInventoryLine.transactionQuantity = new COMM_SyncEBSInventory_REST.TagName(String.valueOf(varhead.Asset__r.Quantity__c));
            lstInventoryLine.add(tempInventoryLine);            
            COMM_SyncEBSInventory_REST.inventoryLines tempinventoryLines = new COMM_SyncEBSInventory_REST.inventoryLines();
            tempinventoryLines.inventoryLine= lstInventoryLine;
            listOfinventoryLines.add(tempinventoryLines);
            COMM_SyncEBSInventory_REST.inventory tempInventory = new COMM_SyncEBSInventory_REST.inventory();
            tempInventory.inventoryLines = tempinventoryLines;
            tempInventory.headerId = new COMM_SyncEBSInventory_REST.TagName(varhead.Id);
            tempInventory.lineId = new COMM_SyncEBSInventory_REST.TagName(varhead.Id);
            if(varhead.SVMXC__Product__r.ProductCode != null) {
                tempInventory.itemNumber = new COMM_SyncEBSInventory_REST.TagName(varhead.SVMXC__Product__r.ProductCode); 
            } else if(varhead.Asset__r.External_ID__c != null) {
                tempInventory.itemNumber = new COMM_SyncEBSInventory_REST.TagName(varhead.Asset__r.External_ID__c);
            }
            tempInventory.sourceLocation = new COMM_SyncEBSInventory_REST.TagName(varhead.SVMXC__Location__r.Location_ID__c);
            tempInventory.transactionQuantity = new COMM_SyncEBSInventory_REST.TagName(String.valueOf(varhead.SVMXC__New_Quantity2__c));
            tempInventory.transactionDate = new COMM_SyncEBSInventory_REST.TagName(Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'));
            If(varhead.SVMXC__New_Quantity2__c != null && varhead.SVMXC__New_Quantity2__c < 0 ){
                tempInventory.transactionTypeId = new COMM_SyncEBSInventory_REST.TagName('32');
            }else{
                tempInventory.transactionTypeId = new COMM_SyncEBSInventory_REST.TagName('42');
            }
            
            listOfinventory.add(tempInventory);
            
        }
        COMM_SyncEBSInventory_REST.inventoryRequest InventoryRequest = new COMM_SyncEBSInventory_REST.inventoryRequest();
        inventoryDetails.inventory = listOfinventory;
        InventoryRequest.inventoryDetails = inventoryDetails;
        InventoryRequest.IntegrationHeader = COMM_SyncEBSInventory_REST.createIntegrationHeader(Label.ORDER_SFDC,Label.order_commerp);
        inventorySync.InventoryRequest = InventoryRequest;
        List<SVMXC__Stock_Adjustment__c> listToUpdateStockAdjust = new List<SVMXC__Stock_Adjustment__c>();
        try {
            COMM_SyncEBSInventory_REST.createOrderRestCallout(inventorySync);
            for(SVMXC__Stock_Adjustment__c varhead : listOfStockAdjust) {
                varhead.Integration_Status__c = 'Success';
                listToUpdateStockAdjust.add(varhead);
            }
            update listToUpdateStockAdjust;
        } catch(Exception e) {
            for(SVMXC__Stock_Adjustment__c varhead : listOfStockAdjust) {
                varhead.Integration_Status__c = 'Failed';
                varhead.Callout_Result__c = 'Error Occured = '+e.getMessage();
                listToUpdateStockAdjust.add(varhead);
            }
            update listToUpdateStockAdjust;
        }
        
    }
}