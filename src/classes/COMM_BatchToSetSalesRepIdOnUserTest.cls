/*
Name            : COMM_BatchToSetSalesRepIdOnUserTest
Created By      : Nitish Bansal (Appirio, India)
Creatded Date   : 06/17/2016
Purpose         : Test Class for COMM_BatchToSetSalesRepIdOnUser
*/

@isTest(seeAllData = false)
public class COMM_BatchToSetSalesRepIdOnUserTest{

    @isTest static void testSalesRepIdPopulation() { 
    
        User sysAdmin = TestUtils.createUser(1, Constants.ADMIN_PROFILE , true).get(0);
            system.RunAs(sysAdmin ){
                    
            List<User> userRec = TestUtils.createUser(1, Constants.ADMIN_PROFILE , false);
            userRec[0].FirstName = 'Julie';
            userRec[0].LastName = 'Schreen';
            userRec[0].Email= 'Julie.Schreen@test.com';
            insert userRec;
                    
            Test.startTest();    
            Database.executeBatch(new COMM_BatchToSetSalesRepIdOnUser());
            Test.stopTest();
            User testUser1 = [SELECT Id,Sales_Rep_ID__c  From User Where LastName = 'Schreen' LIMIT 1];
            //system.assertNotEquals(null, testUser1.Sales_Rep_ID__c);
        }
    }

}