/*
Name            : COMM_BatchToSetSalesRepIdOnUser
Created By      : Nitish Bansal (Appirio, India)
Creatded Date   : 06/17/2016
Purpose         : To set the sales rep Id on the user records

Execute Statment - Database.executeBatch(new COMM_BatchToSetSalesRepIdOnUser(), 50);//run with size 50 only

*/
global class COMM_BatchToSetSalesRepIdOnUser implements Database.Batchable<sObject>{
    global String query;
    global String fname = 'Mark';
    global String lname = 'Brayton';
    global Database.QueryLocator start(Database.batchableContext bc){  
        List<UserLicense> licensesList = [Select Name, Id From UserLicense where Name = 'Salesforce'];
        List<Id> fullLicenseProfileids = new List<Id>();
        for(Profile prof : [Select UserLicenseId, Id From Profile where UserLicenseId = :licensesList.get(0).Id]){
            fullLicenseProfileids.add(prof.Id);
        }
        
        query = 'Select Name, Sales_Rep_ID__c, Id, Email from User' ;
        if(Test.isRunningTest()){
            query = query + ' Where FirstName =: fname and LastName =: lname Limit 1';
        } else {
            query = query + ' Where ProfileId In :fullLicenseProfileids';
        }
        //system.debug('**query**'+query);
        return Database.getQueryLocator(query);                                                                                   
    }

    global void execute(Database.BatchableContext bc, List<User> userList){
        //Declaration
        List<User> updateUserList = new List<User>();
        Map<String, String> userNameToRepIdMap = new Map<String, String>();

        //Map Initialization
        userNameToRepIdMap.put('ADAM.COOK','100803143');
        userNameToRepIdMap.put('ALEC.GRECO','100506141');
        userNameToRepIdMap.put('ALEX.FRITTS','100725160');
        userNameToRepIdMap.put('ALFRED.CHOW','100878094');
        userNameToRepIdMap.put('AMOS.GURA','100380141');
        userNameToRepIdMap.put('ANDREW.FULLER','100419110');
        userNameToRepIdMap.put('ANDY.GORMAN','100549141');
        userNameToRepIdMap.put('ARTIE.PELLER','100713142');
        userNameToRepIdMap.put('ASA.KLEIN','100898093');
        userNameToRepIdMap.put('AUSTIN.MIHALIK','100598141');
        userNameToRepIdMap.put('BEN.BAILEY','100471093');
        userNameToRepIdMap.put('BEN.BLAKE','100192141');
        userNameToRepIdMap.put('BEN.DEISSIG','100833093');
        userNameToRepIdMap.put('BILL.JOLIN','100316141');
        userNameToRepIdMap.put('BRAD.BEARD','100701141');
        userNameToRepIdMap.put('BRAD.SHEARIN','100725144');
        userNameToRepIdMap.put('BRAD.WILSON','100496141');
        userNameToRepIdMap.put('BRENNAN.WOOD','100412093');
        userNameToRepIdMap.put('BRENT.BEGIN','100157142');
        userNameToRepIdMap.put('BRIAN.HUNTER','100724143');
        userNameToRepIdMap.put('BRIAN.KLIPFEL','100120142');
        userNameToRepIdMap.put('BROOKE.GIBSON','100673141');
        userNameToRepIdMap.put('CHRIS.CURIEL','100725156');
        userNameToRepIdMap.put('CHRIS.HICKS','100003081');
        userNameToRepIdMap.put('CHRIS.LEU','100413093');
        userNameToRepIdMap.put('CHRIS.OUHRABKA','100788093');
        userNameToRepIdMap.put('CHRISTINE.SAKANIWA','100485093');
        userNameToRepIdMap.put('CHRISTOPHER.MANCINI','100680141');
        userNameToRepIdMap.put('CHUCK.COGGINS','100725155');
        userNameToRepIdMap.put('CLAY.MAURER','100789141');
        userNameToRepIdMap.put('CLINT.DUNBAR','100735142');
        userNameToRepIdMap.put('CONNOR.CHECK','100740142');
        userNameToRepIdMap.put('DAN.KEMPER','100725142');
        userNameToRepIdMap.put('DARREN.MASINGALE','100002070');
        userNameToRepIdMap.put('DAVID.BIELINSKI','100003078');
        userNameToRepIdMap.put('DAVID.FLORCZYK','100002078');
        userNameToRepIdMap.put('DAVID.HENWOOD','100685143');
        userNameToRepIdMap.put('DEVIN.GONZALEZ','100627141');
        userNameToRepIdMap.put('DOTTIE.BOEHLERT','100678141');
        userNameToRepIdMap.put('ERIC.REECE','100724141');
        userNameToRepIdMap.put('ERIC.ROBINSON','100542093');
        userNameToRepIdMap.put('GUNNAR.SCHMID','100419096');
        userNameToRepIdMap.put('HASSAN.FARID','100364141');
        userNameToRepIdMap.put('HUY.NGUYEN','100471096');
        userNameToRepIdMap.put('IAN.KLUMPP','100628093');
        userNameToRepIdMap.put('JAMIE.SNYDER-FAIR','100771142');
        userNameToRepIdMap.put('JAN-MICHAEL.NOONAN','100676142');
        userNameToRepIdMap.put('JASON.VEGA','100649141');
        userNameToRepIdMap.put('JEFF.ELDRIDGE','100658142');
        userNameToRepIdMap.put('JEFFREY.SPANO','100462141');
        userNameToRepIdMap.put('JENNI.HART','100631141');
        userNameToRepIdMap.put('JEREMY.BELL','100404143');
        userNameToRepIdMap.put('JEREMY.HASLINGER','100366098');
        userNameToRepIdMap.put('JEREMY.MOORE','100725146');
        userNameToRepIdMap.put('JODY.LONG','100725152');
        userNameToRepIdMap.put('JOHN.HIGGINS','100725159');
        userNameToRepIdMap.put('JOHN.LUBISCHER','100780141');
        userNameToRepIdMap.put('JOHN.MICHAELIDES','100003072');
        userNameToRepIdMap.put('JON.WINTON','100739141');
        userNameToRepIdMap.put('JONATHAN.BYERLY','100603093');
        userNameToRepIdMap.put('JOSEPH.MELEK','100920093');
        userNameToRepIdMap.put('JULIE.SCHREEN','100685141');
        userNameToRepIdMap.put('JUSTIN.KAYS','100696145');
        userNameToRepIdMap.put('KATIE.PRUITT','100487141');
        userNameToRepIdMap.put('KEN.AGRELLA','100419101');
        userNameToRepIdMap.put('KEVIN.CROWLEY','100158141');
        userNameToRepIdMap.put('KRISTEN.BERG','100782142');
        userNameToRepIdMap.put('KYLE.CHRISTENSEN','100426141');
        userNameToRepIdMap.put('KYLE.EGLAND','100925093');
        userNameToRepIdMap.put('KYLE.KOCH','100725147');
        userNameToRepIdMap.put('KYLE.UMINGER','100735141');
        userNameToRepIdMap.put('KYLE.WHIMPEY','100741141');
        userNameToRepIdMap.put('MARC.TAYLOR','100003089');
        userNameToRepIdMap.put('MARCUS.PATTON','100725149');
        userNameToRepIdMap.put('MARK.BONFIGLIO','100630141');
        userNameToRepIdMap.put('MARK.BRAYTON','100670141');
        userNameToRepIdMap.put('MARK.MINEAU','100283143');
        userNameToRepIdMap.put('MAX.PAISLEY','100292141');
        userNameToRepIdMap.put('MICHAEL.BROWN3','100725151');
        userNameToRepIdMap.put('MICHAEL.NELSON','100689141');
        userNameToRepIdMap.put('MICHAEL.TUOHY','100133140');
        userNameToRepIdMap.put('MIKE.WEAVER','100419112');
        userNameToRepIdMap.put('MITCHELL.ENGLAND','100883093');
        userNameToRepIdMap.put('MORGAN.ROSSEEL','100938093');
        userNameToRepIdMap.put('NATHAN.BAKER','100655141');
        userNameToRepIdMap.put('TAMARA.SAVAGE','100068140');
        userNameToRepIdMap.put('PAT.REGAN','100702141');
        userNameToRepIdMap.put('RANDY.SCIANNA','100173142');
        userNameToRepIdMap.put('RINO.BEVIS','100467141');
        userNameToRepIdMap.put('ROB.SYNEK','100417093');
        userNameToRepIdMap.put('RUSS.CRAWFORD','100135140');
        userNameToRepIdMap.put('RYAN.COUGHLIN','100587141');
        userNameToRepIdMap.put('RYAN.DUNN','100382093');
        userNameToRepIdMap.put('RYNE.REYNOSO','100564141');
        userNameToRepIdMap.put('SAM.WIDDIFIELD','100470141');
        userNameToRepIdMap.put('SCOTT.BRADLEY','100306141');
        userNameToRepIdMap.put('SCOTT.BURGESS','100698141');
        userNameToRepIdMap.put('SEAN.MURPHY','100809143');
        userNameToRepIdMap.put('SETH.BURKE','100628141');
        userNameToRepIdMap.put('SHEA.REITER','100333141');
        userNameToRepIdMap.put('SHY.DONALDSON','100120140');
        userNameToRepIdMap.put('SONNY.COOK','100202142');
        userNameToRepIdMap.put('STEVE.DANIELSON','100725150');
        userNameToRepIdMap.put('STEVE.MAHONEY','100725154');
        userNameToRepIdMap.put('TADD.FARMER','100672141');
        userNameToRepIdMap.put('TANNER.MATTSON','100725158');
        userNameToRepIdMap.put('TARA.CLOPTON','100392141');
        userNameToRepIdMap.put('TAYLOR.MAY','100740143');
        userNameToRepIdMap.put('THOMAS.VERDI','100786141');
        userNameToRepIdMap.put('TIM.CASEY','100002086');
        userNameToRepIdMap.put('TIM.PAYNE','100412141');
        userNameToRepIdMap.put('TODD.DREHS','100687093');
        userNameToRepIdMap.put('TOM.HURT','100772141');
        userNameToRepIdMap.put('TRACEY.WISTROM','100640141');
        userNameToRepIdMap.put('ZACHARY.SHARY','100803144');

        //for 4 extra values
        userNameToRepIdMap.put('ERIC.ROBINSON','100542093');
        userNameToRepIdMap.put('CHRIS.OUHRABKA','100788093');
        userNameToRepIdMap.put('BEN.DEISSIG ','100833093');
        userNameToRepIdMap.put('ALFRED.CHOW','100878094');

        
        //Quering and updating users
        String upperCaseName;
        for(User u : userList){
            upperCaseName = u.Email.toUpperCase();
            for(String userEmail : userNameToRepIdMap.keyset()){
                if(upperCaseName.contains(userEmail )){
                     u.Sales_Rep_ID__c = userNameToRepIdMap.get(userEmail);
                     //u.Sales_Rep_Id__c = '';
                     updateUserList.add(u);
                     break;
                }
            }
        /*if(userNameToRepIdMap.containsKey(upperCaseName)){
               // u.Sales_Rep_ID__c = userNameToRepIdMap.get(upperCaseName);
               u.Sales_Rep_ID__c = '';
                
            }*/
        }
        if(updateUserList.size() > 0 && !Test.isRunningTest()){
            update updateUserList;
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}