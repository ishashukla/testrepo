// (c) 2015 Appirio, Inc.
//
// Class Name: ForecastYearTrigger_HandlerTest
// Description: Test Class for ForecastYearTrigger_Handler and MonthlyForecastTriggerHandler class.
// 
// April 5 2016, Meena Shringi  Original 
//
@isTest 
public class ForecastYearTrigger_HandlerTest {
      static testMethod void testForecastYearTrigger_Handler(){
          Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
          Id recordTypeRep = rtByNameRep.getRecordTypeId();
          Schema.RecordTypeInfo rtByNameManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast');
          Id recordTypeManager = rtByNameManager.getRecordTypeId();
          Profile pr =  TestUtils.fetchProfile('System Administrator');
          User usr = TestUtils.createUser(1, pr.Name, false).get(0);
          usr.Division = 'NSE';
          insert usr;
          system.runAs(usr){
              Test.startTest();
                  Account acc = TestUtils.createAccount(1,true).get(0);
                  List<Opportunity> oppList = new List<Opportunity>();
                  Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
                  opp.Business_Unit__c = 'NSE';
                  //opp.Opportunity_Number__c = '50001';
                  opp.CloseDate = System.today();
                  opp.ForecastCategoryName = 'Pipeline';              
                  opp.OwnerId = usr.Id;
                  oppList.add(opp);
                  Opportunity opp1 = TestUtils.createOpportunity(1,acc.Id,false).get(0);
                  opp1.Business_Unit__c = 'NSE';
                  opp1.Opportunity_Number__c = '50001';
                  opp1.CloseDate = System.today();
                  opp1.ForecastCategoryName = 'Pipeline';              
                  opp1.OwnerId = usr.Id;
                  oppList.add(opp1);
                  insert oppList;
                  List<Forecast_Year__c> lstForcastYear = TestUtils.createForcastYear(1,usr.Id,recordTypeRep,false);
                  lstForcastYear[0].Business_Unit__c = 'NSE';
                  lstForcastYear[0].Manager__c = usr.Id;
                  lstForcastYear[0].Year__c = String.valueOf(System.today().year());
                  lstForcastYear[0].Year_External_Id__c = '1234';
                  lstForcastYear.add(TestUtils.createForcastYear(1,usr.Id,recordTypeManager,false).get(0));
                  lstForcastYear[1].Manager__c = usr.Id;
                  lstForcastYear[1].Business_Unit__c = 'NSE';
                  lstForcastYear[1].Year__c = String.valueOf(System.today().year());
                  lstForcastYear[1].Year_External_Id__c = '12345';
                  insert lstForcastYear;   
                  Monthly_Forecast__c monthlyforcast = [select Id,Quota__c,Forecast_Submitted__c from Monthly_Forecast__c where Forecast_Record_Type_Id__c = :recordTypeRep LIMIT 1];
                  System.assert(monthlyforcast !=null ,'monthly forecasts should be addded related to forecast');
                  monthlyforcast.Forecast_Submitted__c = true;
                  List<String> monthString = new List<String>{'','January','February','March','April','May','June','July','August','September','October','November','December'};
                  monthlyforcast.Month__c = monthString.get(System.today().month());
                  monthlyforcast.Quota__c = 12;            
                  update monthlyforcast;
                  List<Monthly_Forecast__c> monthlyforcastlst = new List<Monthly_Forecast__c>();
                  monthlyforcast.Forecast_Submitted__c = false;
                  monthlyforcastlst.add(monthlyforcast);
                  Monthly_Forecast__c monthlyforcast1 = [select Id,Quota__c,Forecast_Submitted__c from Monthly_Forecast__c where Forecast_Record_Type_Id__c = :recordTypeManager LIMIT 1];
                  monthlyforcast1.Month__c = monthString.get(System.today().month());
                  monthlyforcastlst.add(monthlyforcast1);
                  update monthlyforcastlst;
              Test.stopTest(); 
          }
          
      }
}