/**================================================================      
* Appirio, Inc
* Name: COMM_EquipmentTriggerHandlerTest
* Description: Test class of COMM_EquipmentTriggerHandler
* Created Date: 29th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 
@isTest
private class COMM_EquipmentTriggerHandlerTest
{
	@isTest
	static void itShould()
	{
		Map<Integer, String> mapOfMMTypes = new Map<Integer, String>();
	    mapOfMMTypes.put(1, 'Connect Suite');
	    mapOfMMTypes.put(2, 'Suite Status');
	    mapOfMMTypes.put(3, 'Studio3');
	    mapOfMMTypes.put(4, 'Data Mediator');
	    Map<Integer, String> mapOfRTs = new Map<Integer, String>();
	    mapOfRTs.put(1,'Booms');
	    mapOfRTs.put(2,'Doc Station');
	    mapOfRTs.put(3,'Exam Light');
	    mapOfRTs.put(4,'Lights');
	    mapOfRTs.put(5,'ORIS');
	    mapOfRTs.put(6,'Media Management');
	    mapOfRTs.put(7,'Tables');

		Account testAcc = Testutils.createAccount(1,true).get(0);
	    Project_Plan__c testPPlan = Testutils.createProjectPlan(1,testAcc.Id, true).get(0);
	    Project_Phase__c testPPhase = Testutils.createProjectPhase(1, testPPlan.Id, true).get(0);
	    Room__c testRoom = Testutils.createRoomObject(1, testPPhase.Id, true).get(0);

	    Equipment__c testEquip;
	    List<Equipment__c> equipmentList = new List<Equipment__c>();

	    for(Integer i = 1; i <= 7; i ++){
	    	testEquip = new Equipment__c(Room__c = testRoom.Id);
	    	for(Integer j = 1; j <= 4; j++){
                testEquip.Type__c = mapOfMMTypes.get(j);
            }    
            testEquip.RecordTypeID = Schema.SObjectType.Equipment__c.getRecordTypeInfosByName().get(mapOfRTs.get(i)).getRecordTypeID();  
            equipmentList.add(testEquip);
	    }
	    insert testEquip;
	    
	    Test.startTest();

		    Integer i = 0;
		    equipmentList = new List<Equipment__c>();
		    for(Equipment__c equp : [Select Id, Description__c from Equipment__c]){
		    	equp.Description__c = 'Action Required';
		    	equipmentList.add(equp);
		    	i++;
		    	if(i == 4){
		    		break;
		    	}
		    }

		    update equipmentList;
	    Test.stopTest();
	    for(Equipment__c equp : [Select Id, Description__c, Action_Required__c from Equipment__c where Description__c = 'Action Required']){
			system.assertEquals(true, equp.Action_Required__c);
		}
	}
}