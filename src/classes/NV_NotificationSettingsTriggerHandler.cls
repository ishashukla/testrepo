public with sharing class NV_NotificationSettingsTriggerHandler {

	public static final String UserRequiredErrorMsg = 'User is required';
	public static final String UserSettingUniqueErrorMsg = 'User\'s Setting record already exist. User can only have one Settings record.';

	public static void validateUser(Map<Id,Notification_Settings__c> oldRecordsMap, List<Notification_Settings__c> newRecords){
		
		Notification_Settings__c oldRecord = null;
		List<Notification_Settings__c> filteredRecords = new List<Notification_Settings__c>();
		Set<Id> userIds = new Set<Id>();
		for(Notification_Settings__c newRecord : newRecords){
			if(newRecord.User__c == null){
				newRecord.addError(UserRequiredErrorMsg);
				continue;
			}
			if(oldRecordsMap!= null && oldRecordsMap.containsKey(newRecord.Id)){
				oldRecord = oldRecordsMap.get(newRecord.Id);
			}

			if(oldRecord == null || (oldRecord.User__c != newRecord.User__c)){
				filteredRecords.add(newRecord);
				userIds.add(newRecord.User__c);
			}
		}

		Map<Id, Notification_Settings__c> settingsMap = getNotificationSettingsByUserIds(userIds);

		for(Notification_Settings__c record: filteredRecords){
			if(settingsMap.containsKey(record.User__c)){
				record.addError(UserSettingUniqueErrorMsg);
			}
		}
	}

	public static Map<Id, Notification_Settings__c> getNotificationSettingsByUserIds(Set<Id> userIds){
		Map<Id, Notification_Settings__c> dataMap = new Map<Id, Notification_Settings__c>();
		for(Notification_Settings__c settings: [Select Id, User__c From Notification_Settings__c Where User__c in: userIds]){
			dataMap.put(settings.User__c, settings);
		}
		return dataMap;
	}
}