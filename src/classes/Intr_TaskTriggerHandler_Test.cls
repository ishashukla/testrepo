/*******************************************************************************
 Author        :  Appirio JDC (Kajal Jalan)
 Date          :  June 06, 2016
 Purpose       :  Test Class For Intr_TaskTriggerHandler (T-508997)
*******************************************************************************/
@isTest
public class Intr_TaskTriggerHandler_Test {
	
	static User assignTo;
	
	public static testMethod void updateCallReason() {
		
		CreateTestData();
		
		Test.startTest();
			//Create New Task
			Task testTask = Intr_TestUtils.createTask(1,assignTo.Id,false).get(0);
			testTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments CCKM Task Record').getRecordTypeId();
			testTask.CallDisposition = 'Order';
			insert testTask;
		
			List<Task> TaskList = [SELECT Id, Call_Reason__c FROM Task WHERE Id =: testTask.Id LIMIT 1];
	        // Verify call Reason Field is populating
	        System.assertEquals(TaskList[0].Call_Reason__c, 'Order');

	        testTask.CallDisposition = 'test';
	        update testTask;
	        List<Task> TaskList2 = [SELECT Id, Call_Reason__c FROM Task WHERE Id =: testTask.Id LIMIT 1];
	        System.assertEquals(TaskList2[0].Call_Reason__c, 'test');

       Test.stopTest();
	} 
	
	// Create Test Data
	public static void CreateTestData() {
		
		assignTo = Intr_TestUtils.createUser(1,'Instruments CCKM Admin',true).get(0);
		
	} 
	
	
	public static testMethod void forCheckAttachmentOnTask() {
		
		CreateTestData();
		
		Test.startTest();
			//Create New Task
			Task task = Intr_TestUtils.createTask(1,assignTo.Id,true).get(0);
			Attachment attach=new Attachment();    
        	attach.Name='Unit Test Attachment';
        	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        	attach.body=bodyBlob;
        	attach.parentId=task.id;
        	attach.OwnerId = assignTo.Id;
        	insert attach;
		    task.Subject = 'Hello';
			update task;
			
			
		Test.stopTest();
		
		List<Attachment> AttachList = [SELECT Id,ParentId FROM Attachment WHERE ParentId = :task.id];
		
        // Verify call Reason Field is populating
        System.assertEquals(AttachList.size(),1);
	} 
    
}