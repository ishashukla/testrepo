/**================================================================      
* Appirio, Inc
* Name: ProjectTeamAddMemberExtension 
* Description: T-500923 (To add project team members)
* Created Date: 05/06/2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      		Description of the update
*     							 Rahul Aeran     			(T-501829)
*     							 Deepti Maheshwari    I-228021 - Make sure PM is able to create project plan record with correct values 
*     							 Deepti Maheshwari    I-228107, Setting default PM if no PM exists
*	08/16/2016				 Deepti Maheshwari		Updating the changes to remove phase teams (I-229890)
==================================================================*/
public class ProjectTeamAddMemberExtension {

    String PROJECT_TEAM_RECORDTYPE_ID;
    public List<ProjectTeamWrapper> projectTeamMembers {get;set;}
    public List<ProjectTeamWrapper> newProjectTeamMembers {get;set;}
    public Project_Plan__c project {get;set;}
    public Boolean showAddMemberSection {get;set;}
    public Boolean displayAccess {get;set;}
    public List<selectoption> phaseAccessLevels{get;set;} 
    public List<selectoption> projectAccessLevels{get;set;} 
    
    public Id selectedId {get;set;}
    public Integer listSize {get;set;}
    public List<ProjectTeamWrapper> projectTeamMembersToShow {get;set;}
    public Integer currentSizeShown {get;set;}
    public Boolean reachedMax {get;set;}
		
		// DM 08/16 Changes for I-229890 start
    //Added by Rahul Aeran as part of T-501829
    //public Project_Phase__c projectPhase{get;set;}
    
    
    //==========================================================================
    // Method to save team members and show the blank list again for the user to enter more records for project phase
    //==========================================================================
    /*public void savePhasesTeamMembersAndMore() {
        saveNewPhaseTeamMembers();
        addTeamMembers(); 
    }*/
  
    //==========================================================================
    // Method to save the newly created phase team members
    //==========================================================================
    /*public Pagereference saveNewPhaseTeamMembers () {
        List<Custom_Account_Team__c> phaseMemToInsert = new List<Custom_Account_Team__c>();
        try {
          for ( ProjectTeamWrapper atmWrap : newProjectTeamMembers) {
            if ( atmWrap.member.User__c != null ) {
              phaseMemToInsert.add(atmWrap.member);
            }
          }
       
          if (!phaseMemToInsert.isEmpty()) {
            insert phaseMemToInsert;
          }
          
          return new pagereference('/'+projectPhase.Id);
        }
        catch ( exception ex) {
          if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, 'Installers can only be Integration Specialist or Field Service Technicians.'));
          }
          return null;
        }
    }*/
    // DM 08/16 Changes for I-229890 end
    public ProjectTeamAddMemberExtension(ApexPages.Standardcontroller std) {
            String phaseId = ApexPages.currentPage().getParameters().get('phaseId');
        init();
        
        Id projId = std.getRecord().Id;
        project = null;
        if(projId != null) {
            PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
          project = [SELECT Id,Name, OwnerId FROM Project_Plan__c WHERE Id = :projId];
        }
        // DM 08/16 Changes for I-229890 start
        /*else if(phaseId != null){
            //Added by Rahul Aeran to include phase team member addition as well  
          List<Project_Phase__c> lstPhases = [SELECT Id,Name FROM Project_Phase__c 
                                            WHERE Id = :phaseId];
          if(!lstPhases.isEmpty()){
            projectPhase = lstPhases.get(0);
            PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get(Constants.INSTALLER_RECORDTYPE).getRecordTypeId(); 
            addTeamMembers();
          }
            
        }*/
      	// DM 08/16 Changes for I-229890 end
        addTeamMembers();
    }
                                                  
                                                                
       private void init(){
            showAddMemberSection = false;
      displayAccess = false;
        currentSizeShown = 5;
        reachedMax = false;
        projectTeamMembersToShow = new List<ProjectTeamWrapper>();
        projectAccessLevels= new List<SelectOption>();
        projectAccessLevels.add(new selectOption('Read','Read Only'));
        projectAccessLevels.add(new selectOption('Edit','Read/Write'));
      projectTeamMembers = new List<ProjectTeamWrapper>();
        listSize = projectTeamMembers.size();
        if( listSize == 5) {
        reachedMax = true;
        }
        }
    //==========================================================================
    // Wrapper class to hold the Team Member record as well as their projectess levels
    //==========================================================================
    public class ProjectTeamWrapper {
        public Custom_Account_Team__c member {get;set;}
        public String projectAccess {get;set;}
        public User user{get;set;}
        public List<SObject> userList{get;set;}
        
        public ProjectTeamWrapper() {
          member = new Custom_Account_Team__c();
          projectAccess = '';
          user= new User(); 
          userList = new sObject[]{user};
        }
    }

    //==========================================================================
    // Method to create new members list and display the add team members section
    //==========================================================================
    public void addTeamMembers () {
        newProjectTeamMembers = new List<ProjectTeamWrapper>();
        
        for ( Integer i=0; i<5; i++) {
          ProjectTeamWrapper projectMem = new ProjectTeamWrapper();
          
          //Added by Rahul Aeran for phase team
          if(project != null){
            projectMem.member.Project__c = project.Id;
                        
          }
          // DM 08/16 Changes for I-229890 start
          /*else if(projectPhase != null){
            projectMem.member.Project_Phase__c = projectPhase.Id;
          }*/
          // DM 08/16 Changes for I-229890 end
          system.debug('Team Role : ' + projectMem.member.Team_Member_Role__c);
          /*projectAccessLevels = new List<selectOption>();

          if(projectMem.member.Team_Member_Role__c=='Sales Rep'|| projectMem.member.Team_Member_Role__c=='Field Service Technician'|| projectMem.member.Team_Member_Role__c=='3rd Party Team Member') {
              
              projectAccessLevels.add(new selectOption('Read','Read Only'));
          } 
          
          else {
              projectAccessLevels.add(new selectOption('Edit','Read/Write'));
          }*/
          projectMem.member.RecordTypeId = PROJECT_TEAM_RECORDTYPE_ID;
          newProjectTeamMembers.add(projectMem);
        }
        showAddMemberSection = true;
    }

    //==========================================================================
    // Method to save team members and show the blank list again for the user to enter more records
    //==========================================================================
    public void saveAndMore() {
        saveNewTeamMembers();
        addTeamMembers(); 
    }
  
    //==========================================================================
    // Method to cancel and return back to Project page
    //==========================================================================
    public pagereference doCancel () {
        newProjectTeamMembers = null;
        if(project != null){
            return new pagereference('/'+project.Id);
        }
        // DM 08/16 Changes for I-229890 start
        /*else if(projectPhase != null){
            return new pagereference('/'+projectPhase.Id);
        }*/
        // DM 08/16 Changes for I-229890 end
        return new pagereference('/');
    }

    //==========================================================================
    // Method to save the newly created team members
    //==========================================================================
    public pagereference saveNewTeamMembers () {
        List<Custom_Account_Team__c> projMemToInsert = new List<Custom_Account_Team__c>();
        List<Project_Plan__Share> projShareToInsert = new List<Project_Plan__Share>();
        //List<ContactShare> contactSharesToInsert = new List<ContactShare>();
        Map<ID,Set<ID>> projIdToUserMap = new Map<ID,Set<ID>>();
        Map<Id, String> userIdAccessLevel = new Map<Id, String>();
        try {
          for ( ProjectTeamWrapper atmWrap : newProjectTeamMembers) {
            if ( atmWrap.member.User__c != null ) {
              // DM 07/22 Setting project access level in custom account team
              atmWrap.member.Project_Access_Level__c = atmWrap.projectAccess;
              if(!userIdAccessLevel.containsKey(atmWrap.member.User__c)){
                userIdAccessLevel.put(atmWrap.member.User__c, atmWrap.projectAccess);
              }
              projMemToInsert.add(atmWrap.member);
              System.debug('---atmWrap.member.Account_Access_Level__c---'+atmWrap.projectAccess);
              if ( atmWrap.projectAccess == 'Edit' || atmWrap.projectAccess == 'Read') {
                Project_Plan__Share projectShare = new Project_Plan__Share();
                projectShare.AccessLevel = atmWrap.projectAccess;
                projectShare.ParentId = atmWrap.member.Project__c;
                projectShare.UserOrGroupId = atmWrap.member.User__c;
                projShareToInsert.add(projectShare);
                if(projIdToUserMap.containsKey(atmWrap.member.Project__c)){
                    projIdToUserMap.get(atmWrap.member.Project__c).add(atmWrap.member.User__c);
                }else{
                    projIdToUserMap.put(atmWrap.member.Project__c,new Set<ID>{atmWrap.member.User__c});
                }
              }
            }
          }
        /*  for(Contact contacts : [SELECT Private__c, AccountID, ID from Contact WHERE AccountId in :projIdToUserMap.keyset()]){
            if(!contacts.private__c){
                for(ID User__c : projIdToUserMap.get(contacts.AccountID)){
                    ContactShare conShare = new ContactShare();
                    conShare.UserOrGroupId = User__c;
                    conShare.ContactAccessLevel = 'Read';
                    conShare.contactID = contacts.ID;
                    contactSharesToInsert.add(conShare);
                }
            }
          }
        */  
          if (!projMemToInsert.isEmpty()) {
            insert projMemToInsert;
          }
          System.debug('projShareToInsert'+projShareToInsert);
          if (!projShareToInsert.isEmpty()) {
            Database.insert(projShareToInsert);
          }
        /*  if(contactSharesToInsert != null){
              insert contactSharesToInsert;
          }*/
          //provideOppDocAccess(userIdAccessLevel); //NB - 05/09 - T-500928
          return new pagereference('/'+project.Id);
        }
        catch ( exception ex) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, ex.getMessage()));
          return null;
        }
    }

    //NB - 05/09 - T-500928
    public void provideOppDocAccess(Map<Id, String> userIdAccessLevel){
      Map<String, Document__Share> docShareMapToInsert = new Map<String, Document__Share>();
      
      // Updated by Deepti Maheshwari (I-226474)
      // Documents will never have project look up populated and 
      //access has to be provided by querying oppty and oracle quotes individually
      Map<ID, Opportunity> opportunityMap = new Map<ID, Opportunity>([SELECT ID 
                                        FROM Opportunity 
                                        WHERE Project_Plan__r.id = :project.Id]);
      Map<ID, BigMAchines__Quote__c> oracleQuotes = new Map<ID, BigMAchines__Quote__c>([SELECT ID 
                                            FROM BigMAchines__Quote__c 
                                            WHERE BigMachines__Opportunity__r.id in : opportunityMap.keyset()
                                            AND BigMAchines__is_primary__c = true]); 
      List<Document__c> doclist = [SELECT Id
                              FROM Document__c 
                              WHERE (Opportunity__r.id =: opportunityMap.keyset()
                              OR Oracle_Quote__r.id =: oracleQuotes.keyset())];
      for(Document__c oppDoc : doclist){
        for(Id userId: userIdAccessLevel.keyset()){
          if(!docShareMapToInsert.containsKey(oppDoc.id + '_' + userId)) {
            // DM 07/22 access to documents will be according to access on project teams
            docShareMapToInsert.put(oppDoc.id + '_' + userId, new Document__Share(ParentID = oppDoc.id, UserOrGroupID = userId,
            AccessLevel = userIdAccessLevel.get(userId),
            rowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c));
          }
        }
      }

      if(docShareMapToInsert.size() > 0){
        insert docShareMapToInsert.values();
      } 
    }
}