/*
Name                    - bmQuotePricingTriggerHandler
Created By              - Deepti Maheshwari
Created Date            - 03/04/2016
Purpose                 - T-459649 ; Trigger handler for bmQuoteTriggerHandler
Comments                - This trigger needs to be bulkified

* 26th Oct 2016     Nitish Bansal  I-241409 // Added proper null checks before SOQL statements
*/
public class bmQuotePricingTriggerHandler {
    static BigMachines__Quote__c bmQuote = null;
    static final String partNumber = 'Quote_Total_Ext_Net';
    static final String lineItemName = 'BigMachines Line Item';
    
    public static void afterInsertOrUpdate(){
        bmQuote = (BigMachines__Quote__c)Trigger.new[0];
        ID OpportunityId = bmQuote.BigMachines__Opportunity__c;
        if(OpportunityId != null){
            Opportunity opportunity = [select Name, Pricebook2Id, CurrencyIsoCode from Opportunity where Id = :opportunityId];
            String oppCurrencyCode = opportunity.CurrencyIsoCode;
            final BigMachines__Quote__c[] bmQuoteprimary = [select Id, BigMachines__Total__c from BigMachines__Quote__c
                                                  where BigMachines__Opportunity__c = :OpportunityId
                                                  and BigMachines__Is_Primary__c = true];
                                                  
                                                  
            //if found any primary bm quote
            if (bmQuoteprimary.size() > 0) {
                
                OpportunityLineItem opptyLineItem = null;
                
                try{
                    opptyLineItem = [select Id, OpportunityId, Part_Number__c, CurrencyIsoCode from OpportunityLineItem
                                     where OpportunityId = :opportunityId and Part_Number__c = :partNumber];
                }catch(QueryException ex){
                    //Ignore
                }
                //check if a specific oppty line item exists
                if(null != opptyLineItem && null != opptyLineItem.Part_Number__c){
                    opptyLineItem.UnitPrice = null != bmQuoteprimary[0].BigMachines__Total__c ? bmQuoteprimary[0].BigMachines__Total__c : 0;                    
                    update opptyLineItem;
                }else{
                    // retrieve Pricebook
                    ID opptyPricebookId = bmQuote.BigMachines__Opportunity__r.Pricebook2Id;
                    if (null == opptyPricebookId) {
                        opptyPricebookId = [select Id from Pricebook2 where IsStandard = true].Id;
                    }
                    
                    // Retrieve a specific product. If does not exist then create it as well as create a pricebook entry
                    Product2 product = null;
                    try{
                         product = [select Id from Product2 where Name=:lineItemName and IsActive=true];
                        //Nothing to do yet, ignoring....
                    }catch(QueryException ex){
                        //Ignore and create a new specific product
                        product = new Product2();
                        product.IsActive = true;
                        product.Name = lineItemName;
                        insert product;
                       
                        final PriceBookEntry stdPriceBookEntry = new PriceBookEntry();
                        stdPriceBookEntry.Product2Id = [select Id from Product2 where Name=:lineItemName and IsActive=true].Id;
                        stdPriceBookEntry.Pricebook2Id = opptyPricebookId;
                        stdPriceBookEntry.UnitPrice = 0;
                        stdPriceBookEntry.IsActive = true;
                        stdPriceBookEntry.CurrencyIsoCode = oppCurrencyCode;
                        insert stdPriceBookEntry;
                    }

                    // retrieve PriceBookEntry
                    final ID priceBookEntryId = [select Id from PriceBookEntry where CurrencyIsoCode = :oppCurrencyCode and Product2Id=:product.Id and IsActive=true and Pricebook2Id=:opptyPricebookId].Id;
                    
                    // Add product and Pricebook to the opportunity using OpportunityLineItem 
                    final OpportunityLineItem oppLineItem = new OpportunityLineItem();
                    oppLineItem.Part_Number__c = partNumber;
                    oppLineItem.OpportunityId = OpportunityId;
                    oppLineItem.PricebookEntryId = priceBookEntryId;
                    oppLineItem.UnitPrice = null != bmQuoteprimary[0].BigMachines__Total__c ? bmQuoteprimary[0].BigMachines__Total__c : 0;
                    oppLineItem.Quantity = 1;
                    insert oppLineItem;                
                }
            }else{
                  try{
                    if([select Id from BigMachines__Quote__c where BigMachines__Opportunity__c = :OpportunityId].size() > 0){
                        delete [select Id from OpportunityLineItem where OpportunityId = :opportunityId and Part_Number__c = :partNumber];
                    }
                  }catch(QueryException ex){
                        //Ignore
                  }
            }
        }
    }
    
    public static void afterDelete(){
        bmQuote = (BigMachines__Quote__c)Trigger.old[0];
        ID OpportunityId = bmQuote.BigMachines__Opportunity__c;
        if(OpportunityId != null){
            Opportunity opportunity = [select Name, Pricebook2Id, CurrencyIsoCode from Opportunity where Id = :opportunityId];
            String oppCurrencyCode = opportunity.CurrencyIsoCode;
            if(bmQuote.BigMachines__Is_Primary__c){
                try{
                   delete [select Id from OpportunityLineItem where OpportunityId = :opportunityId and Part_Number__c = :partNumber];
                }catch(QueryException ex){
                    //Ignore
                }
            }
        }
    }
}