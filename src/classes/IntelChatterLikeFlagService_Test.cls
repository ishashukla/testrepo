/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 20, 2013
 Purpose       :  Test Class for IntelChatterLikeFlagService.cls
 Reference     :  Task T-205633
 Modifications :  Task T-207120    Dec 23, 2013    Sonal Shrivastava
*******************************************************************************/
@isTest
private class IntelChatterLikeFlagService_Test {

	static testMethod void myUnitTest() {
    //Create test records
    List<Intel__c> intelList = TestUtils.createIntel(1, true);
    User usr = TestUtils.createUser(1, null, true).get(0);    
    intelList = [SELECT Id, Flagged_As_Inappropriate__c, Chatter_Post__c, CreatedDate
                 FROM Intel__c WHERE Id IN :intelList];                 
    FeedComment comment = TestUtils.createComment(1, intelList.get(0).Chatter_Post__c, true).get(0);
    
    // Test POST method
	  List<IntelChatterLikeFlagService.IntelLikeFlagJSON> intelJsonList = new List<IntelChatterLikeFlagService.IntelLikeFlagJSON>();       
	  intelJsonList.add(new IntelChatterLikeFlagService.IntelLikeFlagJSON(intelList.get(0).Id, 'intel', 'like', usr.Id, usr.lastname, System.now(), 'Test body', intelList.get(0).Chatter_Post__c));
	  intelJsonList.add(new IntelChatterLikeFlagService.IntelLikeFlagJSON(intelList.get(0).Id, 'intel', 'flag', usr.Id, usr.lastname, System.now(), 'Test body', intelList.get(0).Chatter_Post__c));
	  intelJsonList.add(new IntelChatterLikeFlagService.IntelLikeFlagJSON(intelList.get(0).Id, 'reply', 'like', usr.Id, usr.lastname, System.now(), 'Test comment', comment.Id));
	  intelJsonList.add(new IntelChatterLikeFlagService.IntelLikeFlagJSON(intelList.get(0).Id, 'reply', 'flag', usr.Id, usr.lastname, System.now(), 'Test comment', comment.Id));
	  
	  IntelChatterLikeFlagService.IntelLikeFlagWrapper wrap = new IntelChatterLikeFlagService.IntelLikeFlagWrapper();
	  wrap.intelLikeFlagList = intelJsonList;
	  
	  RestRequest req = new RestRequest();
	  RestResponse res = new RestResponse();
	  
	  req.requestURI = '/services/apexrest/IntelChatterLikeFlagService/*';
	  req.httpMethod = 'POST';
	  req.requestBody = Blob.valueOf(JSON.serialize(wrap)); 
	  
	  RestContext.request = req;
	  RestContext.response = res;
	  
	  test.startTest();
	  Map<String, String> result = IntelChatterLikeFlagService.doPost();    
	  System.assertEquals(2, [SELECT Id FROM Intel_Flag__c WHERE Intel__c = :intelList.get(0).Id].size());        
	  test.stopTest();
    
    // Test GET method
    
    req = new RestRequest();
    res = new RestResponse();

    req.requestURI = '/services/apexrest/IntelChatterLikeFlagService/*';
    req.httpMethod = 'GET';
    req.addParameter('intelId', intelList.get(0).Id);
    req.addParameter('replyId', comment.Id);
    RestContext.request = req;
    RestContext.response = res;

    IntelChatterLikeFlagService.IntelLikeFlagWrapper result1 = IntelChatterLikeFlagService.doGet();
    System.assertNotEquals(null, result1.intelLikeFlagList);
    System.assertEquals(3, result1.intelLikeFlagList.size()); 
	}
}