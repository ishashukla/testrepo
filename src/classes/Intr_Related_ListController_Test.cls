// 
// (c) 2016 Appirio, Inc.
//
// T-507913, This is a batch class for updating Cases for Inactivity
//
// June 2, 2016  Shreerath Nair  Original

@isTest
private class Intr_Related_ListController_Test {

    static Account account;
    static list<Case> CaseList;
    static List<String> example = new List<String>();
    static User user;
    
    // Method to test search functionality
    static testMethod void testRelatedListFunctionality() {
        
        createTestData();
        
        Test.startTest();
        
	        Intr_Related_ListController controller = new Intr_Related_ListController();
	        // Setting attributes for searching
	        controller.fieldsCSV = 'UserId,TeamMemberRole,User.Name';
	        controller.sortDirection = 'asc'; 
	        controller.objectName = 'AccountTeamMember';
	        controller.pageSize = 5;
	        controller.pageNumber = 0;
	        controller.searchFieldName = 'AccountId';
	        example.add(account.Id);
	        controller.searchFieldValue = example;
	        controller.orderByFieldName = 'User.Name';
	        controller.sortByField	= 'User.Name';
	        controller.filter = 'CreatedById != null';
	        controller.title = 'Account Team Member';
	        controller.moreLink = 'true';
	        controller.showmoreLink = 'true';
	        controller.showAsStandardRelatedList = true;
	        controller.returnUrl = '/003/0';
	        
	        // Calling search method
	        //controller.getRecords();
	        List<sObject> records = controller.getRecords();
	        system.assertEquals(records.size(), 1, 'Search method should return 1 records.');
	        
	        controller.Beginning();
	        controller.Previous();
	        controller.Next();
	        controller.End();
	        controller.getDisablePrevious();
	        controller.getDisableNext();
	        controller.getTotal_size();
	        controller.getPageNumber();
	        controller.getTotalPages();
	        controller.getShowNewButton();
	        controller.sortByFieldAction();
	        controller.showMore();
	        
	        
	        controller.deleteRecordId = controller.getRecords().get(0).id;
	        controller.deleteRecord();
	        //List<sObject> records1 = controller.getRecords();
	        //system.assertEquals(records.size(), 5, 'After delete, size of the record list should be 4.');
        
        Test.stopTest();
        
    }
   
    // Method to create test data
    private static void createTestData() {
        
        Profile pro = [SELECT Id,Name FROM Profile WHERE Name='Instruments CCKM']; 
    	List<User> us = Intr_TestUtils.createUser(1,pro.Name,true);
        
        account = Intr_TestUtils.createAccount(1, false).get(0);
        account.Name = 'test';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
        insert account;
        
        Contact contact = Intr_TestUtils.createContact(1, account.Id, false).get(0);
		contact.RecordTypeId =   Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CONTACT_RECORD_TYPE).getRecordTypeId();
		contact.FirstName = 'test';
		contact.LastName = 'Jalan';
        contact.Timezone__c = '(GMT–04:00) Eastern Daylight Time (America/New_York)';
		insert contact;
			
		Intr_TestUtils.createContactJunction(1, account.Id, contact.Id, true).get(0);
			
		CaseList = new list<Case>();
		Id repairDepotCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_DEPOT).getRecordTypeId();	
		TestUtils.createRTMapCS(repairDepotCaseRT, 'Instruments - Repair (Depot)', 'Instruments');
		// Creating list of cases for testing bulk insert
		for(integer i=0;i<2;i++){
				
			Case objCase = Intr_TestUtils.createCase(1, account.Id, contact.Id, false).get(0);
			objCase.RecordTypeId = repairDepotCaseRT;
			objCase.Origin = 'Phone'+i;
        	objCase.Sub_Type__c = 'other'+i;
        	objCase.Description = 'test'+i;
        	objCase.Status = 'new'+i;
				
			CaseList.add(objCase);
				
			}
		
		insert CaseList;
		
		 AccountTeamMember accTeamMember  = new AccountTeamMember();
         accTeamMember.UserId = us[0].Id;
         accTeamMember.TeamMemberRole = 'Account Manager';
         accTeamMember.AccountId = account.ID;       
         insert accTeamMember;
        
    }
}