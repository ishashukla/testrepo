//
// (c) 2014 Appirio, Inc.
// Class COMM_IntegrationErrorProcessingBatch
// Class to process error records generated from integration
//
// 27 May 2016     Deepti Maheshwari       Original(T-469786)
global class COMM_IntegrationErrorProcessingBatch implements Database.Batchable<sObject>,Database.Stateful, System.Schedulable{
  //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query='';
        query = 'SELECT Id, Retry_Counter__c, TargetObjectID__c,Operation__c FROM COMM_Callout_Error__c';
        query += ' WHERE Retry_Counter__c < 3';
        //system.debug('>>>>>query'+query);
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC,List<Sobject> scope){
      Set<String> pplToCreate = new Set<String>();
      Set<String> pplToAssign = new Set<String>();
      Set<String> oliToUpdate = new Set<String>();
        for(COMM_Callout_Error__c errorRec : (List<COMM_Callout_Error__c>)scope){
          
            if(errorRec.Operation__c == Constants.INT_OP_PRJCT_CREATE){
              pplToCreate.add(errorRec.TargetObjectID__c);
            }else if(errorRec.Operation__c == Constants.INT_OP_PRJCT_ASSIGN){
              pplToAssign.add(errorRec.TargetObjectID__c);
            }
          else if(errorRec.Operation__c == 'OrderItem'){
            oliToUpdate.add(errorRec.TargetObjectID__c);
          }
        }
        if(pplToCreate.size() > 0){
          List<Project_Plan__c> projectsToSync = [SELECT Project_Plan_Number__c, Start_Date__c,
                                            Completion_Date__c, Id, Long_Name__c,Name,
                                            Engineering_Approval_Status__c, 
                                            Project_Manager__r.Sales_Rep_Id__c,
                                            Oracle_Project_Status__c
                                            FROM Project_Plan__c
                                            WHERE Name in :pplToCreate];
      WebServiceHelper.executeProjectCreationService(projectsToSync);
        }
        if(pplToAssign.size() > 0){
          Set<ID> orderIdsSet = new Set<ID>();
          for(String orderId : pplToAssign){
            String[] orderPPArray = orderId.split(':');
            orderIdsSet.add(orderPPArray.get(1));
          }
          List<Order> orders = [SELECT Project_Plan__r.id, Project_Plan__r.Project_Plan_Number__c,
                          Oracle_Order_Number__c  
                          FROM Order 
                          WHERE ID in : orderIdsSet];
            if(orders.size() > 0){
                WebServiceHelper.executeProjectPlanAssignmentToOrder(orders);
            }                          
          
        }
        Map<String,String> oldValueMap = new Map<String,String>();
        String OldRequestedDate;
        if(oliToUpdate.size() > 0){
          List<OrderItem> orderItems = [SELECT Id,Requested_Ship_Date__c, Shipping_Address__r.Location_Id__c,
                          Order.Oracle_Order_Number__c, Line_Number__c, Line_Type__c, Approval_Status__c,
                          Oracle_Line_Number__c, Oracle_Order_Line_Number__c, Oracle_Ordered_Item__c
                          FROM OrderItem
                          WHERE ID in : oliToUpdate];
      oldValueMap.put(OrderItems.get(0).id + '-' + 'Requested_Ship_Date__c', String.valueof(OrderItems.get(0).Requested_Ship_Date__c));
      WebServiceHelper.executeSalesOrderItemUpdateHTTP(orderItems,oldValueMap);
        }
    }
    global void finish(Database.BatchableContext BC){
    }

  // Schedular for batch.
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
      COMM_IntegrationErrorProcessingBatch errorBatch = new COMM_IntegrationErrorProcessingBatch();
      Database.executeBatch(errorBatch);
    }
  }
}