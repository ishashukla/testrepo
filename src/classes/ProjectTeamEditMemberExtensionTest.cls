@isTest
private class ProjectTeamEditMemberExtensionTest
{
	@isTest
    static void itShould()
    {
        //Creating a System Admin User
        List<User> userList = TestUtils.createUser(2, 'System Administrator', true);
        List<Document__c> oppDocList = new List<Document__c>() ;
        List<Document__c> delOppDocList = new List<Document__c>() ;
        Document__c oppDoc = new Document__c();
        Custom_Account_Team__c projectMem = new Custom_Account_Team__c();
        List<Custom_Account_Team__c> projectMemList = new List<Custom_Account_Team__c>();
        //Creating an account, opprtunity and multile documents as the Admin user
        Test.startTest();
        system.runAs(userList.get(0)){
            List<Account> accountList = TestUtils.createAccount(1, true);
            TestUtils.createCommConstantSetting();
            List<Opportunity> opportunityList = TestUtils.createOpportunity(1, accountList.get(0).Id, true);
            List<Project_Plan__c> projectList = TestUtils.createProjectPlan(1, accountList.get(0).Id, false);
            projectList[0].Start_Date__c = system.today();
            projectList[0].Completion_Date__c = system.today().addDays(20);
            insert projectList;
        
            for(Integer i = 0; i <= 5; i++){
                if(i == 0 || i == 1){
                    projectMem = new Custom_Account_Team__c();
                    projectMem.Project__c = projectList.get(0).Id;
                    projectMem.Account_Access_Level__c = 'Read';
                    projectMem.User__c = userList.get(i).Id;
                    projectMem.RecordTypeId = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
                    projectMemList.add(projectMem);
                }
                
                oppDoc = new Document__c();
                oppDoc.Name = 'Test Opportunity Document' + String.valueOf(i);
                oppDoc.Opportunity__c = opportunityList.get(0).Id;
                oppDoc.Account__c = accountList.get(0).Id;
                oppDoc.Document_type__c = 'Other';
                oppDoc.Project_Plan__c = projectList.get(0).Id;
                oppDocList.add(oppDoc);
            }
            insert projectMemList;
            insert oppDocList;
           
            //List<Document__Share> docSharerecords = [Select Id from Document__Share where 
                                                    //rowCause =:Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c];
            //system.assertNotEquals(null, docSharerecords);
            //system.assertNotEquals(0, docSharerecords.size());
            
            Test.setCurrentPageReference(new PageReference('ProjectTeamEditMember.myPage')); 
			System.currentPageReference().getParameters().put('id', projectMemList.get(0).Id);
            ProjectTeamEditMemberExtension instance = new ProjectTeamEditMemberExtension();
            instance.saveNewTeamMembers(); 
            PageReference pRef = instance.doCancel(); 
            system.assertNotEquals(null, pRef);
        }
        Test.stopTest();
    }
}