//
//(c) 2016 Appirio, Inc.
// Created By:     Sahil Batra
// Date:           June 28, 2016
// Description:    This is controller for ASRForecast
// Sahil Batra      June 28,2016    Original T-513407
// Sahil Batra      Sep 12,2016 I-227519(Modified) added null checkes
//
public class ASRForecastController {
    public List<SelectOption> BUList {get;set;}
    public String selectedBu {get;set;}
    public List<SelectOption> roleList {get;set;}
    public String selectedRole {get;set;}
    public List<SelectOption> userList {get;set;}
    public String selectedUser {get;set;}
    public String createURL {get;set;}
    public boolean hasURL {get;set;}
    public boolean hasAllInstrumentPermission{get;set;}
    public boolean showASRUsers{get;set;}
    public List<SelectOption> ASRuserList {get;set;}
    public String ASRselectedUser {get;set;}
    public List<SelectOption> ASRBUList {get;set;}
    public String ASRselectedBu {get;set;}
    public Map<String,String> userToNameMap = new Map<String,String>();
    public Map<String,String> userToDivisionMap = new Map<String,String>();
    public Map<String,String> userToProfileMap = new Map<String,String>();
    public Id recordTypeRep;
    public Id recordTypeRepManager;
    public Set<Id> userId = new Set<Id>();
    public Set<Id> addedUserId = new Set<Id>();
    //Constructor
    public ASRForecastController(){
        Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
        recordTypeRep = rtByNameRep.getRecordTypeId();
        Schema.RecordTypeInfo rtByNameRepManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast');
        recordTypeRepManager = rtByNameRepManager.getRecordTypeId();
        Id defaultUserId = null;
        userToNameMap = new Map<String,String>();
        userToDivisionMap = new Map<String,String>();
        userToProfileMap = new Map<String,String>();
        Set<Id> userIdforASR = new Set<Id>();
        hasURL = false;
        hasAllInstrumentPermission = false;
        showASRUsers = false;
        Id currentUser = UserInfo.getUserId();
        List<PermissionSetAssignment> recordList = new List<PermissionSetAssignment>();
        recordList = [select PermissionSet.Name From PermissionSetAssignment where Assignee.Id =:currentUser AND PermissionSet.Name = 'Instrument_Forecasts'];
        if(recordList.size() > 0){
            hasAllInstrumentPermission = true;
            BUList = new List<SelectOption>();
            BUList.add(new SelectOption('Instruments','Instruments'));
            Schema.DescribeFieldResult fieldResult = Forecast_Year__c.Business_Unit__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple){
                BUList.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        roleList = new List<SelectOption>();
        roleList.add(new SelectOption('VPI','VPI'));
        }
        System.debug('>>>>>>>'+hasAllInstrumentPermission);
        if(!hasAllInstrumentPermission){
            for(ASR_Relationship__c rec : [SELECT ASR_User__c,ASR_User__r.Name,ASR_User__r.Division,ASR_User__r.Profile.Name FROM ASR_Relationship__c 
                                           WHERE Mancode__r.Sales_Rep__c =:currentUser 
                                           AND ( Mancode__r.Type__c = 'ASR'
                                           OR  Mancode__r.Type__c = 'Sales Ops' )]){
                userIdforASR.add(rec.ASR_User__c);
                userToNameMap.put(rec.ASR_User__c,rec.ASR_User__r.Name);
                userToDivisionMap.put(rec.ASR_User__c,rec.ASR_User__r.Division);
                userToProfileMap.put(rec.ASR_User__c,rec.ASR_User__r.Profile.Name);
            }
            for(Mancode__c mancode : [SELECT Sales_Rep__c,DefaultViewAs__c,DefaultViewAs__r.Name,DefaultViewAs__r.Division,DefaultViewAs__r.Profile.Name
                                      FROM Mancode__c 
                                      WHERE Sales_Rep__c =:currentUser
                                      AND (Type__c = 'ASR' OR Type__c = 'Sales Ops')
                                      AND DefaultViewAs__c!=null]){
                defaultUserId = mancode.DefaultViewAs__c;
                userIdforASR.add(mancode.DefaultViewAs__c);
                userToNameMap.put(mancode.DefaultViewAs__c,mancode.DefaultViewAs__r.Name);
                userToDivisionMap.put(mancode.DefaultViewAs__c,mancode.DefaultViewAs__r.Division);
                userToProfileMap.put(mancode.DefaultViewAs__c,mancode.DefaultViewAs__r.Profile.Name);                   
            }
        if(userIdforASR.size() > 0){
            showASRUsers = true;
            ASRuserList = new List<SelectOption>();
            for(Id userID : userIdforASR){
                ASRuserList.add(new SelectOption(userID,userToNameMap.get(userID)));
            }
            if(defaultUserId != null){
                ASRselectedUser = defaultUserId;
            }else{
                ASRselectedUser = ASRuserList.get(0).getValue();
            }
            ASRBUList = new  List<SelectOption>();
            List<String> tempbuList = new List<String>();
            tempbuList = userToDivisionMap.get(ASRselectedUser).split(';');
            for(String bu : tempbuList){
                    ASRBUList.add(new SelectOption(bu,bu));
                }
        if(UserInfo.getUiTheme() != 'Theme4t'){
            ASRselectedBu = ASRBUList.get(0).getValue();
            createASRUrl();
        }
        }
        else{
            if(!hasAllInstrumentPermission){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Contact your Service administrator'));
            }
        } 
    }else{
        userList = new List<SelectOption>();
        userId = new Set<Id>();
        addedUserId = new Set<Id>();
        userList.add(new SelectOption('','None'));
        Mancode__c currentUserMancode = new Mancode__c();
        List<Mancode__c> mancoodeRecordList = [SELECT Type__c,Sales_Rep__c,Business_Unit__c,Sales_Rep__r.Name,DefaultViewAs__c FROM Mancode__c WHERE Sales_Rep__c =:UserInfo.getUserId() AND DefaultViewAs__c!=null];
        System.debug('>>>>>>>1'+mancoodeRecordList);
        if(mancoodeRecordList.size() > 0){
            currentUserMancode = mancoodeRecordList.get(0);
            Id viewAsUserId = currentUserMancode.DefaultViewAs__c;
            System.debug('>>>>>>>2'+viewAsUserId);
            Mancode__c mancode = new Mancode__c();
            // I-227519 Start
            if(viewAsUserId != null){
                mancode = [SELECT Type__c FROM Mancode__c WHERE Sales_Rep__c=:viewAsUserId LIMIT 1];
            }
            // I-227519 End
            List<Forecast_Year__c> forecastList = [SELECT RecordTypeId,Id,Business_Unit__c,User2__c FROM Forecast_Year__c WHERE User2__c =:viewAsUserId];
            // I-227519
            if(forecastList.size() > 0 && UserInfo.getUiTheme() != 'Theme4t' && mancode != null){
                if(viewAsUserId!=null && mancode.Type__c =='Sales Rep' && mancode.Type__c!='VPI'){
                    hasURL = true;
                    createURL = '/apex/SalesForecast?id='+viewAsUserId+'&managerView=true&fromASR=true';
                }else if(viewAsUserId!=null && mancode.Type__c != 'Sales Rep' && mancode.Type__c!='VPI'){
                    hasURL = true;
                    createURL = '/apex/ManagerSalesForecast?id='+viewAsUserId+'&managerView=true&fromASR=true';
                }else{
                    hasURL = true;
                    createURL = '/apex/VPIForecast?id='+viewAsUserId+'&fromASR=true&managerView=true';
                }
            }
        }
        for(Mancode__c rec : [SELECT Type__c,Sales_Rep__c,Sales_Rep__r.Name FROM Mancode__c WHERE Type__c='VPI']){
            userId.add(rec.Sales_Rep__c);
        }
        for(Forecast_Year__c rec :   [SELECT User2__c,User2__r.Name FROM Forecast_Year__c WHERE User2__c IN:userId ORDER BY User2__r.Name]){
            if(!addedUserId.contains(rec.User2__c)){
                userList.add(new SelectOption(rec.User2__c,rec.User2__r.Name));
                addedUserId.add(rec.User2__c);
            }
        }
        if(userList.size() > 0 && userList.get(0)!=null){
            selectedUser = userList.get(0).getValue();
        }
        System.debug('>>>>>>>5'+createURL); 
    }
}
    public void loadASRSelectedBU(){
        hasURL = false;
        ASRBUList = new  List<SelectOption>();
        List<String> tempbuList = new List<String>();
        tempbuList = userToDivisionMap.get(ASRselectedUser).split(';');
        for(String bu : tempbuList){
            ASRBUList.add(new SelectOption(bu,bu));
        }
        ASRselectedBu = ASRBUList.get(0).getValue();
    }
    public void getRoles(){
        hasURL = false;
        selectedUser = null;
        roleList = new List<SelectOption>();
    //  roleList.add(new SelectOption('','None'));
        if(selectedBu == 'Instruments'){
            roleList.add(new SelectOption('VPI','VPI'));
        }
        else{
            roleList.add(new SelectOption('Sales Rep','Sales Rep'));
            roleList.add(new SelectOption('Regional Manager', 'Regional Manager'));
        }
        if(selectedBu == 'IVS' || selectedBu == 'Navigation'){
            roleList.add(new SelectOption('VP','VP'));
        }
        else if(selectedBu == 'Surgical' || selectedBu == 'NSE'){
            roleList.add(new SelectOption('Director','Director'));
            roleList.add(new SelectOption('VP','VP'));
        }
    //  selectedRole = 'VPI';
        selectedRole = roleList.get(0).getValue();
        getAllUsers();
    }
    public void getAllUsers(){
        hasURL = false;
        userList = new List<SelectOption>();
        userId = new Set<Id>();
        addedUserId = new Set<Id>();
        userList.add(new SelectOption('','None'));
        selectedUser = null;
        if(selectedRole == 'VPI'){
        for(Mancode__c rec : [SELECT Type__c,Sales_Rep__c,Sales_Rep__r.Name FROM Mancode__c WHERE Type__c=:selectedRole]){
            userId.add(rec.Sales_Rep__c);
        }
        }
        for(Mancode__c rec : [SELECT Type__c,Sales_Rep__c,Sales_Rep__r.Name FROM Mancode__c WHERE Type__c=:selectedRole AND Business_Unit__c  =: selectedBu]){
            userId.add(rec.Sales_Rep__c);
        //  userList.add(new SelectOption(rec.Sales_Rep__c,rec.Sales_Rep__r.Name));
        }
        for(Forecast_Year__c rec :   [SELECT User2__c,User2__r.Name FROM Forecast_Year__c WHERE User2__c IN:userId ORDER BY User2__r.Name]){
            if(!addedUserId.contains(rec.User2__c)){
                userList.add(new SelectOption(rec.User2__c,rec.User2__r.Name));
                addedUserId.add(rec.User2__c);
            }
        }
    }
    public void createURL(){
        if( selectedUser == null || selectedUser == ''){
            hasURL = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select User from Picklist'));
        }else{
        hasURL = false;
        if(selectedUser!='' && selectedRole!='' && selectedRole!='Sales Rep' && selectedRole!='VPI'){
            hasURL = true;
            createURL = '/apex/ManagerSalesForecast?id='+selectedUser+'&managerView=true&fromASR=true';
        }
        else if(selectedUser!='' && selectedRole!='' && selectedRole=='Sales Rep' && selectedRole!='VPI'){
                hasURL = true;
                createURL = '/apex/SalesForecast?id='+selectedUser+'&managerView=true&fromASR=true&BU='+selectedBu;
        }
        else{
            hasURL = true;
            createURL = '/apex/VPIForecast?id='+selectedUser+'&fromASR=true&managerView=true';
        }
        }
        System.debug('>>>>'+createURL);
    }
    public PageReference createURLSF1(){
        if( selectedUser == null || selectedUser == ''){
            hasURL = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select User from Picklist'));
            return null;
        }else{
        hasURL = false;
        if(selectedUser!='' && selectedRole!='' && selectedRole!='Sales Rep' && selectedRole!='VPI'){
            hasURL = true;
            createURL = '/apex/ManagerSalesForecast?id='+selectedUser+'&managerView=true&fromASR=true';
        }
        else if(selectedUser!='' && selectedRole!='' && selectedRole=='Sales Rep' && selectedRole!='VPI'){
                hasURL = true;
                createURL = '/apex/SalesForecast?id='+selectedUser+'&managerView=true&fromASR=true&BU='+selectedBu;
        }
        else{
            hasURL = true;
            createURL = '/apex/VPIForecast?id='+selectedUser+'&fromASR=true&managerView=true';
        }
        }
        PageReference pg = new PageReference(createURL); 
        pg.setRedirect(true); 
        return pg;
    }
    public PageReference createASRUrlSF1(){
        Mancode__c mancode = new Mancode__c();
        // I-227519 Start
        if(ASRselectedUser != null){
            mancode = [SELECT Type__c FROM Mancode__c WHERE Sales_Rep__c=:ASRselectedUser LIMIT 1];
        }
        // I-227519 End
        hasURL = false;
        String profileName = userToProfileMap.get(ASRselectedUser);
        // I-227519 Start
        if(ASRselectedUser!=null && mancode != null && mancode.Type__c !='Sales Rep' && mancode.Type__c!='VPI'){
            hasURL = true;
            createURL = '/apex/ManagerSalesForecast?id='+ASRselectedUser+'&managerView=true&fromASR=true';
        }// I-227519 Start
        else if(ASRselectedUser!=null && mancode != null && mancode.Type__c == 'Sales Rep' && mancode.Type__c!='VPI'){
            hasURL = true;
            createURL = '/apex/SalesForecast?id='+ASRselectedUser+'&managerView=true&fromASR=true&BU='+ASRselectedBu;
        }else{
            hasURL = true;
            createURL = '/apex/VPIForecast?id='+ASRselectedUser+'&fromASR=true&managerView=true';
        }
        PageReference pg = new PageReference(createURL); 
        pg.setRedirect(true); 
        return pg;
    }
    public void createASRUrl(){
        Mancode__c mancode = new Mancode__c();
        mancode = [SELECT Type__c FROM Mancode__c WHERE Sales_Rep__c=:ASRselectedUser LIMIT 1];
        hasURL = false;
        String profileName = userToProfileMap.get(ASRselectedUser);
        if(ASRselectedUser!=null && mancode.Type__c !='Sales Rep' && mancode.Type__c!='VPI'){
            hasURL = true;
            createURL = '/apex/ManagerSalesForecast?id='+ASRselectedUser+'&managerView=true&fromASR=true';
        }
        else if(ASRselectedUser!=null && mancode.Type__c == 'Sales Rep' && mancode.Type__c!='VPI'){
            hasURL = true;
            createURL = '/apex/SalesForecast?id='+ASRselectedUser+'&managerView=true&fromASR=true&BU='+ASRselectedBu;
        }else{
            hasURL = true;
            createURL = '/apex/VPIForecast?id='+ASRselectedUser+'&fromASR=true&managerView=true';
        }
    }
}