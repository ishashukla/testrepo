// 
// (c) 2016 Appirio, Inc.
//
// T-507913, This is a test class for Intr_AccountTeamRelatedListController
//
// June 2, 2016  Shreerath Nair  Original

@isTest
private class Intr_AccountTeamRelatedListControl_Test {

    static Account account;
    static list<Case> CaseList;
    static List<String> example = new List<String>();
    static User user;
    
    
    static testMethod void testAccountTeamRelatedList() {
        //createTestData();
        Profile pro = [SELECT Id,Name FROM Profile WHERE Name = 'Instruments CCKM']; 
        List<User> us = Intr_TestUtils.createUser(1, pro.Name, true);
        
        account = Intr_TestUtils.createAccount(1, false).get(0);
        account.Name = 'test';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
        insert account;
        
        Contact contact = Intr_TestUtils.createContact(1, account.Id, false).get(0);
        contact.RecordTypeId =   Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CONTACT_RECORD_TYPE).getRecordTypeId();
        contact.FirstName = 'test';
        contact.LastName = 'Jalan';
        contact.Timezone__c = '(GMT–04:00) Eastern Daylight Time (America/New_York)';
        insert contact;
            
        Intr_TestUtils.createContactJunction(1, account.Id, contact.Id, true).get(0);
        Test.startTest();   
            CaseList = new list<Case>();
            Id repairDepotCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_DEPOT).getRecordTypeId();

            TestUtils.createRTMapCS(repairDepotCaseRT, 'Instruments - Repair (Depot)', 'Instruments');
                // Creating list of cases for testing bulk insert
            for(integer i=0;i<1;i++){
                    
                Case objCase = Intr_TestUtils.createCase(1, account.Id, contact.Id, false).get(0);
                objCase.RecordTypeId = repairDepotCaseRT;
                objCase.Origin = 'Phone'+i;
                objCase.Sub_Type__c = 'other'+i;
                objCase.Description = 'test'+i;
                objCase.Status = 'new'+i;
                    
                CaseList.add(objCase);
                    
                }
            
            insert CaseList;
            
             AccountTeamMember accTeamMember  = new AccountTeamMember();
             accTeamMember.UserId = us[0].Id;
             accTeamMember.TeamMemberRole = 'Account Manager';
             accTeamMember.AccountId = account.ID;       
             insert accTeamMember;
            Intr_AccountTeamRelatedListController Intr = new Intr_AccountTeamRelatedListController(new ApexPages.StandardController(CaseList[0]));
            //system.assertEquals(OpportunityList[0].AccountId,account.Id);
        Test.stopTest(); 
    }
    
    // Method to create test data
   /* private static void createTestData() {
        
        Profile pro = [SELECT Id,Name FROM Profile WHERE Name = 'Instruments CCKM']; 
        List<User> us = Intr_TestUtils.createUser(1, pro.Name, true);
        
        account = Intr_TestUtils.createAccount(1, false).get(0);
        account.Name = 'test';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
        insert account;
        
        Contact contact = Intr_TestUtils.createContact(1, account.Id, false).get(0);
        contact.RecordTypeId =   Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CONTACT_RECORD_TYPE).getRecordTypeId();
        contact.FirstName = 'test';
        contact.LastName = 'Jalan';
        contact.Timezone__c = '(GMT–04:00) Eastern Daylight Time (America/New_York)';
        insert contact;
            
        Intr_TestUtils.createContactJunction(1, account.Id, contact.Id, true).get(0);
            
        CaseList = new list<Case>();
            
            // Creating list of cases for testing bulk insert
        for(integer i=0;i<10;i++){
                
            Case objCase = Intr_TestUtils.createCase(1, account.Id, contact.Id, false).get(0);
            objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_DEPOT).getRecordTypeId();
            objCase.Origin = 'Phone'+i;
            objCase.Sub_Type__c = 'other'+i;
            objCase.Description = 'test'+i;
            objCase.Status = 'new'+i;
                
            CaseList.add(objCase);
                
            }
        
        insert CaseList;
        
         AccountTeamMember accTeamMember  = new AccountTeamMember();
         accTeamMember.UserId = us[0].Id;
         accTeamMember.TeamMemberRole = 'Account Manager';
         accTeamMember.AccountId = account.ID;       
         insert accTeamMember;
        
    }*/
    
}