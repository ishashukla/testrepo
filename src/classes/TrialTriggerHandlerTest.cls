/**================================================================      
* Appirio, Inc
* Name: TrialTriggerHandlerTest
* Description: Test class of TrialTriggerHandler
* Created Date: 05/31/2016
* Created By: Rahul Aeran (Appirio)
*
* Date Modified      Modified By      Description of the update
* 29th Nov 2016      Nitish Bansal    I-242624
==================================================================*/ 

@isTest
private class TrialTriggerHandlerTest {
  static testMethod void testTrialTriggerHandler(){
    List < Account > accountList = TestUtils.createAccount(1, true);
    TestUtils.createCommConstantSetting();
    List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
    String rtTrial = Constants.getRecordTypeId(Constants.TRIAL_RT_TRIAL,Constants.SOBJECT_TRIAL);
    String rtRequest = Constants.getRecordTypeId(Constants.TRIAL_RT_REQUEST,Constants.SOBJECT_TRIAL);
    List < Trial__c > lstTrials = TestUtils.createTestTrials(2, rtTrial, oppList.get(0).Id,null,false);
    lstTrials.get(0).Kenco_Trial_Number__c = '456';
    lstTrials.get(1).Kenco_Trial_Number__c = '567';
    insert lstTrials;
    List < Trial__c > lstRequests = TestUtils.createTestTrials(50, rtRequest, oppList.get(0).Id,lstTrials.get(0).Id,false);
    
    Test.startTest();
      insert lstRequests;
      for(Trial__c trial : lstRequests){
        trial.Trial__c = lstTrials.get(1).Id;
      }
      update lstRequests;
      
      List<Trial__c> lstQueried = [SELECT Id FROM Trial__c WHERE Opportunity__c = null];
      System.assert(lstQueried.size() == lstRequests.size(),'The requests have a null opportunity set');

      delete lstTrials;
    Test.stopTest();
  }
}