// (c) 2015 Appirio, Inc.
//
// Class Name: ContactHomePageControllerTest 
// Description: Test Class for ContactHomePageController class.
// 
// April 6 2016, Isha Shukla  Original 
//
@isTest
private class ContactHomePageControllerTest {
    Static List<Contact> contactList;
    Static List<EntitySubscription> entityList;
    Static List<AccountTeamMember> accountTeamMemberList;
    Static List <Contact_Acct_Association__c> contactAcctAssociationList;
    @isTest static void testContactHomePageController() {
        createData();
        Test.startTest();
        PageReference pageRef = Page.ContactHomePage;        
        Test.setCurrentPage(pageRef);
        ContactHomePageController contactHomePageController = new ContactHomePageController();
        contactAcctAssociationList = [SELECT Id, Name, Associated_Account__c, Associated_Contact__c FROM Contact_Acct_Association__c WHERE Associated_Contact__c =: contactList[0].Id];
        contactHomePageController.contactList = contactAcctAssociationList;
        contactHomePageController.entityList = entityList;
        contactHomePageController.sortedField = 'Name';
        contactHomePageController.sortData();
        contactHomePageController.getFields();
        Boolean hasNext = contactHomePageController.hasNext;
        Boolean hasPrevious = contactHomePageController.hasPrevious;
        Integer pageNumber = contactHomePageController.pageNumber;
        contactHomePageController.currentContact = contactHomePageController.contactList[0].Id;
        contactHomePageController.first();
        contactHomePageController.last();
        contactHomePageController.unfollowContact();
        contactHomePageController.profileName = 'Chatter Free User';
        contactHomePageController.redirectPage();
        System.assertEquals(1, contactHomePageController.contactList.size());
        System.assertEquals(1,contactHomePageController.entityList.size());
        Test.stopTest();
    }
    @isTest static void testWhenSelectedViewMyAccountContact() {
        createData();
        Test.startTest();
        PageReference pageRef = Page.BaseTrailSurvey;
        Test.setCurrentPage(pageRef);
        ContactHomePageController contactHomePageController = new ContactHomePageController();
        contactAcctAssociationList = [SELECT Id, Name, Associated_Account__c, Associated_Contact__c FROM Contact_Acct_Association__c WHERE Associated_Contact__c =: contactList[0].Id];
        contactHomePageController.contactList = contactAcctAssociationList;
        contactHomePageController.selectedView ='My Account Contact';
        contactHomePageController.selectedRole = 'Surgeon';
        contactHomePageController.sortedField = 'Name';
        contactHomePageController.ascDesc = ' desc ';
        contactHomePageController.sortData();
        contactHomePageController.previous();
        contactHomePageController.next();
        contactHomePageController.getFields();
        contactHomePageController.redirectPage();
        contactHomePageController.followContact();
        contactHomePageController.unfollowContact();
        System.assertEquals(1, contactHomePageController.contactList.size());
        System.assertEquals('My Account Contact',contactHomePageController.selectedView);
        Test.stopTest();
    }
    public static void createData() {
        List<Account> accountList = TestUtils.createAccount(1, True);
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        contactList = TestUtils.createContact(1, accountList[0].Id, false);
        contactList[0].Personnel__c = 'Surgeon';
        contactList[0].RecordTypeId = recordTypeInstrument;
        insert contactList;
        accountTeamMemberList = createAccountTeamMember(1, accountList[0].Id,UserInfo.getUserId(),True);
   }
    public static List<EntitySubscription> createEntity(Integer entityCount,Id currentUserId ,Id recordId,Boolean isInsert) {
        entityList = new List<EntitySubscription>();
        for(Integer i = 0; i < entityCount; i++) {
            EntitySubscription subscription = new EntitySubscription(SubscriberId = currentUserId, ParentId = recordId);
            entityList.add(subscription);
        }
        if(isInsert) {
            insert entityList;
        } 
        return entityList;
    }
    public static List<AccountTeamMember> createAccountTeamMember(Integer atmCount,Id accountId ,Id userId,Boolean isInsert) {
        accountTeamMemberList = new List<AccountTeamMember>();
        for(Integer i = 0; i < atmCount; i++) {
            AccountTeamMember accountTeamMemberRecord = new AccountTeamMember(UserId = userId, AccountId = accountId);
            accountTeamMemberList.add(accountTeamMemberRecord);
        }
        if(isInsert) {
            insert accountTeamMemberList;
        } 
        return accountTeamMemberList;
    }
    
}