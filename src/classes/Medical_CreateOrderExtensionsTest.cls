/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_CreateOrderExtensionsTest
//
// April 26, 2016, Sunil (Appirio JDC)
//
// Test class for Medical_CreateOrderExtensions
*/
@isTest
private class Medical_CreateOrderExtensionsTest {
  private static List<Account> accounts;
  private static List<Contact> contacts;
  private static Order ord;
  private static Case cs;
  static testMethod void testMedical_CreateOrderExtensions() {
    createTestData(4);

    Test.startTest();
      Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('JDE Order').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('RecordType' , rtOrder);
      ApexPages.currentPage().getParameters().put('case_id',cs.Id);
      ApexPages.currentPage().getParameters().put('accid',accounts.get(0).Id);
      ApexPages.currentPage().getParameters().put('contact_id',contacts.get(0).Id);
      ApexPages.StandardController sc = new ApexPages.StandardController(ord);
      Medical_CreateOrderExtensions orderExtensions = new Medical_CreateOrderExtensions(sc);
      PageReference pageRef = Page.Medical_CreateOrder;
      Test.setCurrentPage(pageRef);
        orderExtensions.selectedAccId  = accounts.get(0).id;
        orderExtensions.fieldName = 'AccountId';
        orderExtensions.getSelectedRecord();
        orderExtensions.cancel();
        orderExtensions.saveOrder();
        orderExtensions.callExternalServices();
    Test.stopTest();

  }
  static testMethod void testMedical_CreateOrderExtensionsWithCallout() {
    createTestData(4);

    Test.startTest();
      Test.setMock(WebServiceMock.class, new WebServiceMockClass());
      Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('JDE Order').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('RecordType' , rtOrder);
      ApexPages.currentPage().getParameters().put('case_id',cs.Id);
      ApexPages.currentPage().getParameters().put('accid',accounts.get(0).Id);
      ApexPages.currentPage().getParameters().put('contact_id',contacts.get(0).Id);
      ApexPages.StandardController sc = new ApexPages.StandardController(ord);
      Medical_CreateOrderExtensions orderExtensions = new Medical_CreateOrderExtensions(sc);
      PageReference pageRef = Page.Medical_CreateOrder;
      Test.setCurrentPage(pageRef);
      orderExtensions.selectedAccId  = accounts.get(0).id;
      orderExtensions.fieldName = 'AccountId';
      orderExtensions.getSelectedRecord();
      orderExtensions.cancel();
      orderExtensions.saveOrder();
      orderExtensions.callExternalServices();
    Test.stopTest();

  }
  static testMethod void testMedical_Quote_Builder_Order(){
    User objMedicalProfileUser = Medical_TestUtils.createUser(1,'Medical - System Admin',false).get(0);
    objMedicalProfileUser.QB_User_Id__c = 'pathik_csr';
    insert objMedicalProfileUser;
    System.runAs(objMedicalProfileUser){
        createTestData(4);
        Test.startTest();

          Test.setMock(HttpCalloutMock.class, new HttpMockResponseClass());
          Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Quote Builder Order').getRecordTypeId();
          ApexPages.currentPage().getParameters().put('RecordType' , rtOrder);
          ApexPages.currentPage().getParameters().put('case_id',cs.Id);
          ApexPages.currentPage().getParameters().put('accid',accounts.get(0).Id);
          ApexPages.currentPage().getParameters().put('contact_id',contacts.get(0).Id);
          ApexPages.currentPage().getParameters().put('BillTo',accounts.get(0).Id);
          ApexPages.currentPage().getParameters().put('BillToName',accounts.get(0).Name);
          ApexPages.currentPage().getParameters().put('ShipTo',accounts.get(0).Id);
          ApexPages.currentPage().getParameters().put('ShipToName',accounts.get(0).Name);
          ApexPages.StandardController sc = new ApexPages.StandardController(ord);
          Medical_CreateOrderExtensions orderExtensions = new Medical_CreateOrderExtensions(sc);
          PageReference pageRef = Page.Medical_CreateOrder;
          Test.setCurrentPage(pageRef);
          orderExtensions.selectedAccId  = accounts.get(0).id;
          orderExtensions.fieldName = 'AccountId';
          orderExtensions.getSelectedRecord();
          orderExtensions.cancel();
          orderExtensions.saveOrder();
          orderExtensions.callExternalServices();
        Test.stopTest();
    }

  }
  static testMethod void redirectingToHomePage(){
    createTestData(4);
    Test.startTest();

      Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Quote Builder Order').getRecordTypeId();
      ApexPages.currentPage().getParameters().put('contact_id',contacts.get(0).Id);
      ApexPages.currentPage().getParameters().put('RecordType' , rtOrder);
      ApexPages.currentPage().getParameters().put('BillToName',accounts.get(0).Name);
      ApexPages.currentPage().getParameters().put('ShipToName',accounts.get(0).Name);
      ApexPages.StandardController sc = new ApexPages.StandardController(ord);
      Medical_CreateOrderExtensions orderExtensions = new Medical_CreateOrderExtensions(sc);
      PageReference pageRef = Page.Medical_CreateOrder;
      Test.setCurrentPage(pageRef);
        orderExtensions.billToAccount_Name = accounts.get(0).Name;
        orderExtensions.shipToAccount_Name = accounts.get(0).Name;
        orderExtensions.billToAccount_Id = null;
        orderExtensions.shipToAccount_Id = null;
        orderExtensions.saveOrder();
        orderExtensions.billToAccount_Name = accounts.get(0).Name;
        orderExtensions.billToAccount_Name = accounts.get(0).Name;
        orderExtensions.billToAccount_Id = null;
        orderExtensions.shipToAccount_Id = accounts.get(0).Id;
        orderExtensions.saveOrder();
    Test.stopTest();
  }
  private static void createTestData(Integer count){
    accounts = Medical_TestUtils.createAccount(count + 1, false);
    accounts.get(0).Billing_Type__c = 'Bill To Address Only';
    insert accounts;
    contacts = Medical_TestUtils.createContact(2, accounts.get(0).id , false);
    contacts.get(0).Phone = '(999) 999-9999';
    insert contacts;
    List<Contact_Acct_Association__c> contactAccAssociations = new List<Contact_Acct_Association__c>();
    for(integer i = 1 ; i <= count; i++) {
      contactAccAssociations.add(new Contact_Acct_Association__c(Associated_Contact__c = contacts.get(0).id, Associated_Account__c = accounts.get(i).id, Account_Contact_Id__c = contacts.get(0).id + '~' + accounts.get(i).id));
    }
    insert contactAccAssociations;

    Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
    TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
    cs = Medical_TestUtils.createCase(10, accounts.get(0).Id, contacts.get(0).Id, false).get(0);
    cs.RecordTypeId = MedicalrecordTypeId;
    insert cs;

    Product2 prod = Medical_TestUtils.createProduct(true);
    Pricebook2 pb = Medical_TestUtils.createPricebook(1,'Test PriceBook', true).get(0);
    PricebookEntry pbe1 = Medical_TestUtils.createPricebookEntry(prod.Id,pb.Id,false);
    PricebookEntry pbe2 = Medical_TestUtils.createPricebookEntry(prod.Id,Test.getStandardPricebookId(), false);
    insert (new List<PricebookEntry>{pbe2,pbe1});
    Medical_Common_Settings__c setting = Medical_TestUtils.createMedicalCommonSetting(true);
    ord = Medical_TestUtils.createOrder(accounts.get(0).Id,contacts.get(0).Id, pb.Id,false);
    ord.Is_Scheduled_Order_Mail_Sent__c = false;
    ord.Contact_PhoneNumber__c = '(999) 999-9999';
    ord.PoNumber = '123456';
    insert ord;

    Medical_JDE_Settings__c jdeSetting = Medical_TestUtils.createMedicalJDESetting(true);
    Medical_QB_Settings__c qbSetting = Medical_TestUtils.createMedicalQBSetting(true);
  }

}