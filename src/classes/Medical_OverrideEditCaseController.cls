/*
//
// (c) 2015 Appirio, Inc.
//
// Controller class for Medical_OverrideEditCase Page
// This Pagae is used redirect Case on RQA page is not already exist.
// Custom button is using this page.
// Custom button is placed only Medical Potential Case layout.
// 08 March 2016 , Sunil (Appirio)
//
// 25 May 2016, Shreerath Nair - T-505716 (Redirect if the recordtype changed to Instrument Potential Product) 
* 25th Nov 2016       Nitish Bansal       I-244957 //LEX case edit, infinite loop issue
* 30th Nov 2016       Isha Shukla         I-243794 // Added Record Type check for COMM BP
*/
public with sharing class Medical_OverrideEditCaseController {
	// Class Variables
	public Case currentCase {get;set;}
  public Boolean openStandardpage = false; //NB - 11/25 - I-244957

	// Controller
	public Medical_OverrideEditCaseController(ApexPages.StandardController sc){
		currentCase = (Case)sc.getRecord();
  }

	//-----------------------------------------------------------------------------------------------
  //  Redirect user to custom Regulatory form page if not already.
  //  This is the case when Case is created from Outlook.
  //-----------------------------------------------------------------------------------------------
  public PageReference redirectCase(){
     
  	String redirectURL ='';
  	//T-505716 (SN)
  	//get recordtypeid from parameter if recordtype is changed or new record is created
  	Id caseRecordType = ApexPages.currentPage().getParameters().get('RecordType');
  	Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
  	Id InstrumentrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId();
  	Id COMMBPrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
  	
  	//check if the recordtype equals Potential product for medical & Intrument.
  	if(caseRecordType == MedicalrecordTypeId || caseRecordType == InstrumentrecordTypeId || currentCase.recordtypeId==MedicalrecordTypeId || currentCase.recordtypeId==InstrumentrecordTypeId || currentCase.recordtypeId==COMMBPrecordTypeId){
    List<Regulatory_Question_Answer__c> lstRQ = [SELECT Id FROM Regulatory_Question_Answer__c WHERE Case__c = :currentCase.Id];
		//If ROA already exist for the case record.
    	if(lstRQ.size() > 0){
    		//when user changes recordtype using change recordtype link
    		if(caseRecordType!=null)	
      			redirectURL = '/' + currentCase.Id + '/e?retURL=%2F' + currentCase.Id+ '&RecordType=' + caseRecordType + '&nooverride=1';
      		else
      			redirectURL = '/' + currentCase.Id + '/e?retURL=%2F' + currentCase.Id + '&nooverride=1';
    	}
    	else{
     	 // When no RQA exist then redirect user to RQA page.
      		//if recordtype is for medical
            if(currentCase.RecordTypeId == MedicalrecordTypeId) {
      			redirectURL = '/apex/Medical_RegulatoryForm?Id=' + currentCase.Id + '&retURL=%2F500&RecordType=' + MedicalrecordTypeId +'&ent=Case&save_new=1&sfdc.override=1';
            } else if(currentCase.RecordTypeId == COMMBPrecordTypeId) {
                redirectURL = '/apex/Medical_RegulatoryForm?Id=' + currentCase.Id + '&retURL=%2F500&RecordType=' + COMMBPrecordTypeId +'&ent=Case&save_new=1&sfdc.override=1';
            } else {
      			redirectURL = '/apex/Medical_RegulatoryForm?Id=' + currentCase.Id + '&retURL=%2F500&RecordType=' + InstrumentrecordTypeId +'&ent=Case&save_new=1&sfdc.override=1';
            }
    	}
  	}
  	else{
      openStandardpage = true; //NB - 11/25 - I-244957
  		//when user changes recordtype using change recordtype link
      if(caseRecordType!=null)	
      			redirectURL = '/' + currentCase.Id + '/e?retURL=%2F' + currentCase.Id+ '&RecordType=' + caseRecordType + '&nooverride=1';
      		else
      			redirectURL = '/' + currentCase.Id + '/e?retURL=%2F' + currentCase.Id + '&nooverride=1';
      
  	}	
    System.debug('@@@' + redirectURL);
    //NB - 11/25 - I-244957 Start
    if(UserInfo.getUiTheme() == 'Theme4d' && openStandardpage){
      String url = '/500/e?nooverride=0';
      for(String u : ApexPages.currentPage().getParameters().keySet()){
        if(u == COMM_Constant_Setting__c.getOrgDefaults().Case_Project_Plan_Field_Id__c || 
             u == COMM_Constant_Setting__c.getOrgDefaults().Case_Description_Field_Id__c){
            url += '&'+ u + '=' + EncodingUtil.URLEncode(ApexPages.currentPage().getParameters().get(u),'UTF-8');
          } else {
            url += '&'+ u + '=' + ApexPages.currentPage().getParameters().get(u);
          }
      }
      system.debug('URL : ' + url);
      redirectURL = url;
    }
    //NB - 11/25 - I-244957 End
    PageReference pg = new PageReference(redirectURL);
    pg.setRedirect(true);
    return pg;
   }
}