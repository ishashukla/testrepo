/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  June 09, 2014
 Purpose       :  Test Class for FF_FlexCreditTriggerHandlerTest
*******************************************************************************/
@isTest(seeAllData = true)
private class FF_FlexCreditTriggerHandlerTest{
  //-----------------------------------------------------------------------------------------------
  // Method for test notifySalesRepIfCaseAlreadyExists method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testMethod1(){
    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Type = 'Hospital';
    acc.Legal_Name__c = 'Test';
    insert acc;

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;

    Product2 prod = new Product2();
    prod.Name = 'test product';
    prod.Part_Number__c = '12345';
    insert prod;

    Pricebook2 priceBook = [SELECT Name FROM Pricebook2 WHERE isStandard = true FOR UPDATE].get(0);

    /*
    Pricebook2 priceBook = new Pricebook2();
    priceBook.Name = 'test value';
    //priceBook.IsStandard = true;
    priceBook.IsActive = true;
    insert priceBook;*/

    PricebookEntry pbe = new PricebookEntry();
    pbe.Pricebook2Id = priceBook.Id;
    pbe.Product2Id = prod.Id;
    pbe.IsActive = true;
    pbe.UnitPrice = 0.0;
    insert pbe;
	
    Id flexRTOnlyConventional = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();  
    Opportunity opp = new Opportunity();
    opp.StageName = 'Quoting';
    opp.AccountId = acc.Id;
    opp.CloseDate = Date.newinstance(2014,5,29);
    opp.Name = 'test value';
    opp.RecordTypeId = flexRTOnlyConventional;
    insert opp;

    Flex_Credit__c fc = new Flex_Credit__c();
    fc.Credit_Amount__c = 10;
    fc.Credit_Approval_Duration__c = 1;
    fc.Credit_Status__c = 'New';
    fc.Opportunity__c = opp.Id;
    insert fc;

    // Verifing Results.
    List<Opportunity> lstOpps = [SELECT Id, StageName FROM Opportunity WHERE Id = :opp.Id];
    System.assertEquals(lstOpps.get(0).StageName, 'Quoting');


    fc.Credit_Status__c = 'Declined';
    update fc;

    // Verifing Results.
    List<Opportunity> lstOpps2 = [SELECT Id,Closed_Lost_Reason__c, StageName FROM Opportunity WHERE Id = :opp.Id];
    /* tom muse commented it out for r2 deployment */
    //System.assertEquals(lstOpps2.get(0).StageName, 'Closed Lost');
    //System.assertEquals(lstOpps2.get(0).Closed_Lost_Reason__c, 'Credit Declined');

    Test.stopTest();
  }

  public static testmethod void testMethod2(){
    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Type = 'Hospital';
    acc.Legal_Name__c = 'Test';
    insert acc;

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;

    Product2 prod = new Product2();
    prod.Name = 'test product';
    prod.Part_Number__c = '12345';
    insert prod;

    Pricebook2 priceBook = [SELECT Name FROM Pricebook2 WHERE isStandard = true].get(0);

    /*
    Pricebook2 priceBook = new Pricebook2();
    priceBook.Name = 'test value';
    //priceBook.IsStandard = true;
    priceBook.IsActive = true;
    insert priceBook;*/

    PricebookEntry pbe = new PricebookEntry();
    pbe.Pricebook2Id = priceBook.Id;
    pbe.Product2Id = prod.Id;
    pbe.IsActive = true;
    pbe.UnitPrice = 0.0;
    insert pbe;
	
    Id flexRTOnlyConventional = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();  
    Opportunity opp = new Opportunity();
    opp.StageName = 'Quoting';
    opp.AccountId = acc.Id;
    opp.CloseDate = Date.newinstance(2014,5,29);
    opp.Name = 'test value';
    opp.RecordTypeId = flexRTOnlyConventional;
    insert opp;

    Flex_Credit__c fc = new Flex_Credit__c();
    fc.Credit_Amount__c = 10;
    fc.Credit_Approval_Duration__c = 1;
    fc.Credit_Status__c = 'New';
    fc.Opportunity__c = opp.Id;
    insert fc;


    fc.Credit_Status__c = 'Pending Lender Decision';
    update fc;

    fc.Credit_Status__c = 'New';
    update fc;

    // Verifing Results.
    //List<Opportunity> lstOpps2 = [SELECT Id,Closed_Lost_Reason__c, StageName FROM Opportunity WHERE Id = :opp.Id];
    //System.assertEquals(lstOpps2.get(0).StageName, 'Closed Lost');
    //System.assertEquals(lstOpps2.get(0).Closed_Lost_Reason__c, 'Credit Declined');

    Test.stopTest();
  }
}