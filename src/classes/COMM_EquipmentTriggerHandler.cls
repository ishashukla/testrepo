/**================================================================      
* Appirio, Inc
* Name: COMM_EquipmentTriggerHandler 
* Description: COMM_EquipmentTriggerHandler  Class for T-505445
* Created Date: 05/25/2016 
* Created By: Mehul Jain (Appirio)
*
* Date Modified      Modified By            Description of the update
* 12 Oct 2016        Deepti Maheshwari      I-239719 updated to evaluate even if field didnt change ; Checks the 'Action Required ' checkbox field on Equipment if any field on the record contains the value 'Action Required'
* 3rd Nov 2016       Nitish Bansal          I-241409 - Removed unnecessary statements.
==================================================================*/
public  class COMM_EquipmentTriggerHandler {
  
  public static void beforeUpdate(List<Equipment__c> newEquipments, Map<Id, Equipment__c> oldMapOfEquipments){
    if(trigger.isUpdate){
      checkActionRequired(newEquipments, oldMapOfEquipments);
    }  
  }
  
  // Checks for all the fields of currect record, whether they contain "Action Required" or not
  public static void checkActionRequired(List<Equipment__c> newEquipments, Map<Id, Equipment__c> oldMapOfEquipments) {
    Map<String, Schema.SObjectField> eqObjectFieldMap = Schema.SObjectType.Equipment__c.fields.getMap();
    
    for(Equipment__c eq : newEquipments){
      if(eq != null ){
        eq.Action_Required__c = false;
        for(String eqField :  eqObjectFieldMap.keySet()){
            //if(eq.get(eqField) != oldMapOfEquipments.get(eq.Id).get(eqField) ){
            if( eq.get(eqField) != null && eq.get(eqField).equals('Action Required') ){
              eq.Action_Required__c = true;
              break;
            } 
         // }  
        }
      }  
    }
  }
}