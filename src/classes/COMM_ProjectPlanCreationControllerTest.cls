/**================================================================      
* Appirio, Inc
* Name: COMM_ProjectPlanCreationControllerTest 
* Description: Test Class of COMM_ProjectPlanCreationController apex class
* Created Date: 12 April 2016
* Created By: Kirti Agarwal  (Appirio)
*
* Date Modified      Modified By      Description of the update
* 26 Aug 2016        Kanika Mathur    Modified to fix test failure
==================================================================*/

@isTest
private class COMM_ProjectPlanCreationControllerTest {

  @isTest
  static void COMM_ProjectPlanCreationControllerTest () {
    List < Account > accountList = TestUtils.createAccount(1, true);
    TestUtils.createCommConstantSetting();
    List < Contact > contactList = TestUtils.createContact(1, accountList[0].id, true);
    List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
    //Created user and OppTeamMember to fix failure
    List<User> userList = TestUtils.createUser(1, 'Service Project Manager - COMM US', true);
    List<OpportunityTeamMember> oppTeamMemberList = TestUtils.createOpportunityTeamMember(1, oppList[0].Id, userList[0].Id, false);
    oppTeamMemberList[0].TeamMemberRole = Constants.PROJECT_MANAGER;
    insert oppTeamMemberList;
    
    PageReference curentPage= Page.COMM_ProjectPlanCreationPage;
    Test.setCurrentPage(curentPage);
    ApexPages.currentPage().getParameters().put('accId', accountList[0].id);
    ApexPages.currentPage().getParameters().put('oppId', oppList[0].id);
    system.assert(oppList[0].id != null);
    
    ApexPages.StandardController cntrl = new ApexPages.StandardController(new Project_Plan__c());
    COMM_ProjectPlanCreationController obj = new COMM_ProjectPlanCreationController(cntrl);
    obj.projectPlan = new Project_Plan__c();
    obj.projectPlan.Account__c = accountList[0].id;
    obj.saveProjectPlan();
    system.assert(obj.projectPlan.id != null);
    
  }
}