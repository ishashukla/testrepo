/**================================================================      
 * Appirio, Inc
 * Name: QualityQuestionnaire
 * Description: T-521673 (QualityQuestionnaire wrapper class 
 *              for Endo Quality questionnaire to be displayed on Regulatory form,  
 *              Specific to Endo - Tech Support profile)
 * Created Date: 02/08/2016
 * Created By: Parul Gupta (Appirio)
 * 
 * Date Modified      Modified By                   Description of the update
 ==================================================================*/
public class QualityQuestionnaire{
    public Question__c question {get;set;}
    public Answer__c answer {get;set;}
    public QualityQuestionnaire(Question__c question){
        this.question = question;
        this.answer = new Answer__c(Question__c = question.id);
    }
}