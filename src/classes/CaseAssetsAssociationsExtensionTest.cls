/**================================================================      
* Appirio, Inc
* Name: CaseAssetsAssociationsExtensionTest
* Description: Test Class for apex class CaseAssetsAssociationsExtension
* Created Date: 08-Aug-2016
* Created By: Raghu Rankawat (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/

@isTest
public class CaseAssetsAssociationsExtensionTest {
    
    @isTest
    public static void caseAssetsAssociationsTestMethod(){

        Test.startTest();
        
        Id changeRequestCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.COMM_CASE_RTYPE).getRecordTypeId();
        TestUtils.createRTMapCS(changeRequestCaseRT, Constants.COMM_CASE_RTYPE, 'COMM');
        Case caseObj = new Case();
        caseObj.RecordTypeId = changeRequestCaseRT;
        insert caseObj;
        
        //Passing Id as GET Parameter to page CaseAssetsAssociations
        PageReference pgRf = Page.CaseAssetsAssociations;
        Test.setCurrentPageReference(pgRf); 
        ApexPages.currentPage().getParameters().put('Id', caseObj.id);

        SVMXC__Installed_Product__c installProduct = new SVMXC__Installed_Product__c();
        installProduct.Product_Description__c = 'Test Description';
        installProduct.SVMXC__Serial_Lot_Number__c = '1234';
        insert installProduct;
        
        CaseAssetsAssociationsExtension objCaseAssets = new CaseAssetsAssociationsExtension();
        objCaseAssets.selectedInstalledProduct = installProduct.id;
        
        objCaseAssets.addInstalledProduct();
        System.assertEquals(objCaseAssets.caseAssetList.get(0).Asset__c , installProduct.id , 'Install Product Id is not equal');
        
        objCaseAssets.save();
        System.assertEquals(objCaseAssets.caseAssetList.size() , 1 , 'Case asset list null');

        //when is_Delete__c is fasle
        objCaseAssets.deleteProduct();
        objCaseAssets.caseAssetList.get(0).is_Delete__c = true;
        //when is_Delete__c is true
        objCaseAssets.deleteProduct();
        System.assertEquals(objCaseAssets.caseAssetList.size() , 0 , 'Case asset list item not deleted');

        objCaseAssets.cancel();

        Test.stopTest();
    }

}