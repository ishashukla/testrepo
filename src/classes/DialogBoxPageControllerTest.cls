/******************************************************************************************
 *  Purpose : Test Class for DialogBoxPageController
 *  Author  : Shubham Paboowal
 *  Date    : September 13, 2016
 *  Story	: S-437706
*******************************************************************************************/  
@isTest
private class DialogBoxPageControllerTest {

    static testMethod void myUnitTest() {
        List<Account> accList = Medical_TestUtils.createAccount(1, true);
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(accList[0]);
        DialogBoxPageController con = new DialogBoxPageController(controller);
        con.showPopup();
        con.closePopup();
        con.closePopupYes();
    }
}