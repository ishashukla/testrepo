/*******************************************************************
Name  :  ContactSmartSearchExtension 
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 4, 2016

Modified: Feb 8, 2016 Gaurav Nawal (Appirio India) T-474041 Added custom setting to pull field Ids.
*************************************************************************/
//June 8, 2016    Prakarsh Jain   T-510069(Modified)
public class ContactSmartSearchExtension {
    //Search criteria fields
  public String contactFirstNameToSearch {set;get;}
  public String contactLastNameToSearch {set;get;}
  public String contactEmail {set;get;}
  public String contactPhone {set;get;}
  public String allowCreate {set;get;}
  public String instrumentRTId{get;set;}
  public boolean isAsc{set; get;}
  public Integer showingFrom{get;set;}
  public Integer showingTo{get;set;}
  public boolean hasNext{get;set;}
  public boolean hasPrevious{get;set;}
  public String requestedPage {get;set;}
  public integer totalResults {set; get;}
  public Integer totalPage {set; get;}
  public string query;
  public integer searchCount{set; get;}
  public string searchStatus{set; get;}
  private string sortOrder;
  private static final Integer DEFAULT_RESULTS_PER_PAGE = 20;  
  public String selectedContactId {get;set;}  
  public ApexPages.StandardSetController contactResults;
  private List<Contact_Acct_Association__c> lstAssociations;
  private Set<Id> associatedContactIds;
  //GN 02/08] T-474041 changes start here.
  public String firstNameFldId {get; set;}
  public String lastNameFldId {get; set;}
  public String phoneFldId {get; set;}
  public String emailFldId {get; set;}
  public String accountFldId {get; set;}
  public String accountId {get;set;}
  //T-503333
  public boolean createPermission{get;set;}
  public String allowCreateId {get; set;}
  //GN 02/08] T-474041 changes end here.
  public String fromAccount;
  
   public boolean isInstrumentsProfile{get;set;}
  
  
  //-----------------------------------------------------------------------------------------------
  // Constructor
  //-----------------------------------------------------------------------------------------------
  public ContactSmartSearchExtension(ApexPages.StandardController controller) {
    //T-510069 changes start
    fromAccount = 'Contact';
    if(ApexPages.currentPage().getParameters().get('fromAccount')!=null){
      fromAccount = ApexPages.currentPage().getParameters().get('fromAccount'); 
    }
    
    // I-227162(SN) start
    List<String> ProfileNameList = new List<String>{Intr_Constants.INSTRUMENTS_CCKM_PROFILE
    											    ,Intr_Constants.INSTRUMENTS_CCKM_ADMIN_PROFILE
    											    ,Intr_Constants.INSTRUMENTS_PROCARE_SALES_PROFILE
    											    ,Intr_Constants.GOVERNMENT_USER_PROFILE
    											    ,Intr_Constants.SYSTEM_ADMIN_PROFILE};
    Map<id,Profile> profileMap = new Map<id,Profile>([SELECT Id, Name FROM Profile WHERE Name IN : ProfileNameList]);
    if(profileMap.containsKey(userinfo.getProfileId())){
    	isInstrumentsProfile =true;	
    }
    else{
    	isInstrumentsProfile =false;	
    }  
    // I-227162(SN) End
    
    //T-510069 changes end
    Schema.RecordTypeInfo rtByNameIntrument = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
    instrumentRTId = (String)rtByNameIntrument.getRecordTypeId();
    resetSearchStatus();
    fillAssociatedContactIds();
    //GN 02/08] T-474041 changes start here.
    populateFieldIds();
    //GN 02/08] T-474041 changes end here.
  }
  
  public Account currentAccount{
    get{
        //T-503333
        createPermission = Schema.sObjectType.Case.isCreateable();
        
        Account currentAc; 
        accountId = ApexPages.currentPage().getParameters().get('accId');
      if(String.isBlank(accountId) == false){
        List<Account> accList = [Select Id, Name,AccountNumber, Oracle_Account_Number__c FROM Account Where Id = :accountId];
        if(accList != null && accList.size() > 0){
          currentAc = accList.get(0);
        }
      }
      return currentAc;
    }
  }
  
  private void fillAssociatedContactIds(){
    associatedContactIds = new Set<Id>();
    for(Contact_Acct_Association__c ca :[SELECT Id, Associated_Contact__c 
                                          FROM Contact_Acct_Association__c 
                                          WHERE Associated_Account__c = :currentAccount.Id 
                                          AND Associated_Contact__r.Is_Archived__c = FALSE]){
      associatedContactIds.add(ca.Associated_Contact__c);   
    }
  }
  
  //-----------------------------------------------------------------------------------------------
  // Action method for reset search
  //-----------------------------------------------------------------------------------------------
  public void resetSearchStatus(){
    searchCount = 0;
    searchStatus = '';
    contactFirstNameToSearch = '';
    contactLastNameToSearch = '';
    contactEmail = '';
    contactPhone = '';
    isAsc = true;
    hasPrevious = false;
    hasNext = false;
  }
  
  public void performSearch(){
    searchContact();
  }
  
  //-----------------------------------------------------------------------------------------------
  // Action method for show Associated Accounts
  //-----------------------------------------------------------------------------------------------
  
  public PageReference showAssociatedAccounts(){
    PageReference pgRef;
    if(currentAccount == null || String.isBlank(selectedContactId)){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid input.'));
        return null;
    }
    //Create new junction entry;
    Contact_Acct_Association__c ca = new Contact_Acct_Association__c();
    ca.Associated_Account__c = currentAccount.Id;
    ca.Associated_Contact__c = selectedContactId;
    insert ca;
    Account account = [SELECT Id, Name FROM Account WHERE Id =: currentAccount.Id].get(0);
    fillAssociatedContactIds();
    
    //Prakarsh Jain T-510069 changes start
    if(fromAccount.contains('Account')){
      pgRef = new PageReference('/'+account.Id);
    }
    else{
      pgRef = new PageReference('/apex/ContactHomePage');
    }
    return pgRef; 
    //Prakarsh Jain T-510069 changes end
   
  }
  
  
   // I-227162(SN) start
   public void showAssociatedAccountsAndCreateCase(){
   
    //Create new junction entry;
    Contact_Acct_Association__c ca = new Contact_Acct_Association__c();
    ca.Associated_Account__c = currentAccount.Id;
    ca.Associated_Contact__c = selectedContactId;
    insert ca;
    Account account = [SELECT Id, Name FROM Account WHERE Id =: currentAccount.Id].get(0);
    fillAssociatedContactIds();
   }
   // I-227162(SN) End
  
  
  
  //-----------------------------------------------------------------------------------------------
  // Action method for Cancel
  //-----------------------------------------------------------------------------------------------
  public Pagereference cancel(){
    Pagereference pg = null;
    return pg;  
  }
  
  private Boolean isAnyFilterExist{
    get{
        String firstName = String.escapeSingleQuotes(contactFirstNameToSearch.trim());
      String lastName = String.escapeSingleQuotes(contactLastNameToSearch.trim());
      String email = String.escapeSingleQuotes(contactEmail.trim());
      String phone = String.escapeSingleQuotes(contactPhone.trim());
      if(String.isBlank(firstName) && String.isBlank(lastName) && String.isBlank(email) && String.isBlank(phone)) {
        return false;
      }
      return true;
    }
  } 
  
  //-----------------------------------------------------------------------------------------------
  // Helper method to search Contact and make list according to pagesize
  //-----------------------------------------------------------------------------------------------
  private void searchContact(){
    //showContactButton = true;
    Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
    Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
    query = 'SELECT Id, Primary__c, Associated_Contact__r.Title, Associated_Contact__r.Phone, Associated_Contact__r.Owner.Name, '+
            'Associated_Contact__r.Name,Associated_Contact__r.FirstName, Associated_Contact__r.LastName, Associated_Contact__r.Email,  '+
            'Associated_Account__r.Oracle_Account_Number__c, Associated_Account__r.Name '+
            'FROM Contact_Acct_Association__c '+
            'WHERE Associated_Contact__r.RecordTypeId =:recordTypeInstrument ' +
            'AND Associated_Contact__r.Is_Archived__c = FALSE';
            //'WHERE Associated_Contact__r.RecordType.Name = \'Instruments Contact\'';
            
    query = findSearchCondition(query);
    
    query += ' order by Primary__c desc'; // Do not change this condition, we are filling a map on based of this
    system.debug('query<<<'+query);
    try{
      lstAssociations = new List<Contact_Acct_Association__c>();
      contactResults = new ApexPages.StandardSetController(Database.query(query));
      contactResults.setPageSize(DEFAULT_RESULTS_PER_PAGE);
      lstAssociations = contactResults.getRecords();
      searchCount = contactResults.getResultSize();
    }
    catch(Exception e){
      searchCount = 0;
    }
    
    if(searchCount  == 0){
      searchStatus = 'No matching results found.';
    }
    system.debug('contactResults.getPageNumber()>>>'+contactResults.getPageNumber());
    requestedPage = String.valueOf(contactResults.getPageNumber());
    showingFrom = 1;
    
    totalResults = searchCount;  
    totalPage = 0;
    totalPage = totalResults / contactResults.getPageSize() ; 
    
    if(totalPage * contactResults.getPageSize() < totalResults){
      totalPage++;
    }
    
    if(searchCount < contactResults.getPageSize()) {
      showingTo = searchCount;
    }
    else {
        showingTo = contactResults.getPageSize();
    }
    
    if(contactResults.getHasNext()) {
      hasNext = true;
    }
    else {
      hasNext = false;
    }
    hasPrevious = false;
  }
  
  
  //-----------------------------------------------------------------------------------------------
  // Helper method for search condition
  //-----------------------------------------------------------------------------------------------
  private String findSearchCondition(String query){
    String firstName = String.escapeSingleQuotes(contactFirstNameToSearch.Trim());
    String lastName = String.escapeSingleQuotes(contactLastNameToSearch.Trim());
    String email = String.escapeSingleQuotes(contactEmail.trim());
    String phone = String.escapeSingleQuotes(contactPhone.trim());
    
    if(String.isBlank(firstName) == false){
      if(query.toUpperCase().contains('WHERE')){
        query += ' and Associated_Contact__r.FirstName like \'%' + firstName + '%\'';
        
      }
      else{
        query += ' where Associated_Contact__r.FirstName like \'%' + firstName + '%\'';
      }
    }
    
    if(String.isBlank(lastName) == false){
      if(query.toUpperCase().contains('WHERE')){
        query += ' and Associated_Contact__r.LastName like \'%' + lastName.Trim() + '%\'';
      }
      else{
        query += ' where Associated_Contact__r.LastName like \'%' + lastName.Trim() +  '%\'';
      }
    }
    
    if(String.isBlank(email) == false){
      if(query.toUpperCase().contains('WHERE')){
        query += ' and Associated_Contact__r.Email like \'%' + email.Trim() + '%\'';
      }
      else{
        query += ' where Associated_Contact__r.Email like \'%' + email.Trim() +  '%\'';
      }
    }

    if(String.isBlank(contactPhone) == false){
      if(query.toUpperCase().contains('WHERE')){
        query += ' and Associated_Contact__r.Phone like \'%' + phone.Trim() + '%\'';
      }
      else{
        query += ' where Associated_Contact__r.Phone like \'%' + phone.Trim() +  '%\'';
      }
    }
    
    // On first load or no filter exist.
    if(isAnyFilterExist == false){
      if(query.toUpperCase().contains('WHERE')){
        query += ' AND Associated_Account__c = \'' + currentAccount.Id + '\'';
      }
      else{
        query += ' WHERE Associated_Account__c = \'' + currentAccount.Id + '\'';
      }
    }
    system.debug('query+++'+query);
    return query;
  }
  
  //-----------------------------------------------------------------------------------------------
  // Helper method for getWrapperList
  //-----------------------------------------------------------------------------------------------
  public List<ContactWrapper> getWrapperList() {
    searchContact();
    List<ContactWrapper> lst = new List<ContactWrapper>();
    Map<Id, List<String>> mapPrimarAccount = new Map<Id, List<String>>();
    for(Contact_Acct_Association__c ca: lstAssociations){
      ContactWrapper obj = new ContactWrapper();
      obj.ca = ca;
      Boolean addInList = false;
      if(isAnyFilterExist == false){
        obj.showOnlyCurrentAccountContacts = true;
        obj.showAssociateAccountLink = false;
        addInList = true;
      }
      else{
        obj.showOnlyCurrentAccountContacts = false;
        if(associatedContactIds.contains(ca.Associated_Contact__c) == false){
          obj.showAssociateAccountLink = true;
        }
        else{
          obj.showAssociateAccountLink = false;
        }
        
     //   if(ca.Primary__c == true){
          if(mapPrimarAccount.containsKey(ca.Associated_Contact__r.Id) == false){
            mapPrimarAccount.put(ca.Associated_Contact__r.Id, new List<String>());
            addInList = true;
          }
          
    //    }
        else{
          if(mapPrimarAccount.get(ca.Associated_Contact__r.Id) != null){
            mapPrimarAccount.get(ca.Associated_Contact__r.Id).add(ca.Associated_Account__r.Name);
          }
        }
        
      }
      
      
      if(addInList == true){
        lst.add(obj);
      }
      
      searchCount = lst.size();
    }
    
    for(ContactWrapper obj :lst) {
        if(mapPrimarAccount.get(obj.ca.Associated_Contact__r.Id) != null && mapPrimarAccount.get(obj.ca.Associated_Contact__r.Id).size() > 0) {
            obj.lstSecondaryAccounts = mapPrimarAccount.get(obj.ca.Associated_Contact__r.Id);
        }
    }
    return lst;
  }
  
  public class ContactWrapper{
    public Contact_Acct_Association__c ca{get;set;}
    public Boolean showAssociateAccountLink{get;set;}
    public Boolean showOnlyCurrentAccountContacts{get;set;}
    public List<String> lstSecondaryAccounts{get;set;}
  }
  
  //GN 02/08] T-474041 changes start here.
  private void populateFieldIds() {
    firstNameFldId = '';
    lastNameFldId = '';
    phoneFldId = '';
    emailFldId = '';
    accountFldId = '';
    List<Instrument_New_Contact_Page_Field_Ids__c> lstFieldIds = [SELECT First_Name__c, Last_Name__c, Email__c, Phone__c, Account__c, Allow_Create__c
                                                                  FROM Instrument_New_Contact_Page_Field_Ids__c
                                                                  LIMIT 1];
    if(!lstFieldIds.isEmpty()) {
      firstNameFldId = lstFieldIds[0].First_Name__c;
      lastNameFldId = lstFieldIds[0].Last_Name__c;
      phoneFldId = lstFieldIds[0].Phone__c;
      emailFldId = lstFieldIds[0].Email__c;
      accountFldId = lstFieldIds[0].Account__c;
      allowCreateId = lstFieldIds[0].Allow_Create__c;
    }
  }
  //GN 02/08] T-474041 changes end here.
}