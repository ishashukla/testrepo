@isTest
public class BMQPTriggerHandlerTest {
	public static testmethod void TestOpptyPrdctOnQPrdctCreation(){
		Account testAcc = TestUtils.createAccount(1,true).get(0);
		Product2 testProd = TestUtils.createProduct(1,true).get(0);
		TestUtils.createCommConstantSetting();
		Opportunity testOppty = TestUtils.createOpportunity(1,testAcc.Id,true).get(0);

		Id pricebookId = Test.getStandardPricebookId();
    //System.debug('**********'+)
		System.debug('<<<<<'+pricebookId);
		PricebookEntry testPriceBook = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = testProd.Id,
        UnitPrice = 10000, IsActive = true);
        
        insert testPriceBook;

        TestUtils.createBigMachineConfig(1, true);
/*		Opportunity testOppty1 = [SELECT Id
									FROM Opportunity
									WHERE Id =:testOppty.Id];
*/
		/*BigMachines__Configuration_Record__c testBMConfig = new BigMachines__Configuration_Record__c(); 

		testBMConfig.BigMachines__action_id_copy__c =  'action1';
		testBMConfig.BigMachines__action_id_open__c =  'action2';
		testBMConfig.BigMachines__bm_site__c =  'test Site';
		testBMConfig.BigMachines__document_id__c = 'test doc';
		testBMConfig.BigMachines__process__c =  'test process';
		testBMConfig.BigMachines__process_id__c =  'process1';
		testBMConfig.BigMachines__version_id__c = 'version1';

		testBMConfig.BigMachines__Is_Active__c = true;
		testBMConfig.BigMachines__Primary_Commerce_Process__c = true;
		insert testBMConfig;*/

		BigMachines__Configuration_Record__c testBMConfig	= TestUtils.createBigMachineConfig(1, false).get(0);
		testBMConfig.BigMachines__Is_Active__c = true;
		testBMConfig.BigMachines__Primary_Commerce_Process__c = false;
		insert 	testBMConfig;

		BigMachines__Configuration_Record__c testBMConfig1 = [SELECT Id 
																FROM BigMachines__Configuration_Record__c
																WHERE Id =: testBMConfig.Id];

		System.debug('quote site - '+testBMConfig1);														

		BigMachines__Quote__c testBMQuote = TestUtils.createBigMachinesQuote(1,testAcc.Id, testOppty.Id, false).get(0);
		//BigMachines__Quote__c testBMQuote = [SELECT Id
		//									 FROM BigMachines__Quote__c
		//									 WHERE BigMachines__Opportunity__c = :testOppty.Id];

		testBMQuote.Equipment_Funded_Amount__c = 100;
		testBMQuote.BigMachines__Status__c = 'test status';

    testBMQuote.BigMachines__Pricebook_Id__c=pricebookId;
		testBMQuote.BigMachines__Site__c = testBMConfig1.Id;
		testBMQuote.BigMachines__Is_Primary__c = true;
		testBMQuote.BigMachines__Opportunity__c = testOppty.Id;
		insert testBMQuote;
		BigMachines__Quote_Product__c testBMQProduct= new BigMachines__Quote_Product__c();


          testBMQProduct.BigMachines__Quote__c = testBMQuote.Id;
       	  //testBMQProduct.BigMachines__Quote__r.BigMachines__Opportunity__c = testOppty.Id;
          testBMQProduct.Location_Account__c = testAcc.Id;

           //testBMQProduct.BigMachines__Quote__r.BigMachines__Pricebook_Id__c=testPriceBook.Pricebook2Id;
           //System.debug('***********'+testPriceBook.Pricebook2Id + '<<<<<<<<<<<<'+testBMQProduct.BigMachines__Quote__r.BigMachines__Pricebook_Id__c);
           testBMQProduct.Asset_Type__c = 'test asset' ;
           testBMQProduct.Business_Unit__c = 'test BU';
           testBMQProduct.City_Tax_Amt__c = 20;
           testBMQProduct.City_Tax_Code__c = '123';
           testBMQProduct.City_Tax_Pct__c = 10; 
           testBMQProduct.County_Tax_Amt__c = 25;
           testBMQProduct.County_Tax_Code__c = '124';
           testBMQProduct.County_Tax_Pct__c = 15;
           testBMQProduct.Misc_City_Tax_Code__c = '125';
           testBMQProduct.Misc_County_Tax_Code__c = '126';
           testBMQProduct.Misc_State_Tax_Code__c = '127';
           testBMQProduct.State_Tax_Amt__c = 30;
           testBMQProduct.State_Tax_Code__c = '128';
           testBMQProduct.State_Tax_Pct__c = 35;
           testBMQProduct.Residual_Amount__c = 300;
           testBMQProduct.BigMachines__Synchronization_Id__c = 'test sync id';
           testBMQProduct.Tax_Basis__c = 40;
           testBMQProduct.BigMachines__Description__c = 'test description';
           testBMQProduct.BigMachines__Sales_Price__c = 500;
           testBMQProduct.BigMachines__Quantity__c = 50;

           testBMQProduct.Name = 'P123';
           testBMQProduct.BigMachines__Product__c = testProd.Id;

           insert testBMQProduct ;

        /*   List<OpportunityLineItem> opptyCreated = [SELECT Id 
           										FROM OpportunityLineItem
           										WHERE BigMachines__Origin_Quote__c = :testBMQuote.Id];
     
           System.assertEquals(true,opptyCreated.size() == 1);
         */  
	}


}