/*******************************************************************************
 Author        :  Appirio (Deepti Maheshwari)
 Date          :  March 25, 2016
 Purpose       :  Test Class for COMM_TaskTriggerHandler
*******************************************************************************/
@isTest
private class COMM_TaskTriggerHandlerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
      Test.startTest();
        Account acc = TestUtils.createAccount(1, true).get(0);
        Contact con = TestUtils.createCOMMContact(acc.id, 'Testcon', true);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Change Request');
        Id recordTypeId = rtByName.getRecordTypeId();
        TestUtils.createRTMapCS(recordTypeId, 'COMM Change Request', 'COMM');
        Case cas = TestUtils.createCase(1,acc.Id,true).get(0);
        Id rTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        TestUtils.createRTMapCS(rTId, 'COMM US Stryker Field Service Case', 'COMM');
        Id devRecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Preventive Maintenance').getRecordTypeId();
	        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(SVMXC__Company__c = acc.Id,Due_Date__c=Date.today(),
	        RecordTypeId = devRecordTypeId,SVMXC__Order_Status__c = 'Open');
	        insert wo;
        //TestUtils.createCommConstantSetting();
        Pricebook2 priceBook = TestUtils.createPriceBook(1,'test',false).get(0);
        priceBook.Name = 'COMM Price Book';
        insert priceBook;
        Opportunity oppty = TestUtils.createOpportunity(1,acc.id, false).get(0);
        oppty.RecordTypeID = 
      Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeID();
      oppty.PO_Close_Date__c = system.today()+30;
      oppty.Stage__c = 'Prospecting';
      insert oppty;     
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Subject = Constants.GROUND_BREAKING;
        t.Status ='Not Started';
        t.Priority='Normal';
        t.WhatId = oppty.Id;
        t.ActivityDate = system.today();
        t.RecordTypeID = 
        Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.COMM_OPPTY_TASK).getRecordTypeID();
        //t.WhoId = UserInfo.getUserId();
        insert t;
        Task t2 = new Task();
        t2.OwnerId = UserInfo.getUserId();
        t2.Subject = Constants.GROUND_BREAKING;
        t2.Status ='Not Started';
        t2.Priority='Normal';
        t2.WhatId = oppty.Id;
        t2.ActivityDate = system.today();
        t2.Work_Order__c = wo.Id;
        t2.Salesperson_to_Email__c = con.Id;
        t2.Case__c = cas.Id;
        t2.WhoId = con.Id;
        t2.RecordTypeID = 
        Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.COMM_TASK).getRecordTypeID();
        //t.WhoId = UserInfo.getUserId();
        insert t2;
        t2.Status = 'Completed';
        update t2;
        //system.debug('<<<<<'+t.WhatId+'<<<<<<'+t.Subject+'<<<<<'+t.ActivityDate);
        List<Opportunity> opptyAfterTaskInsert = [SELECT Ground_Breaking__c, ID
                                                  FROM Opportunity 
                                                  WHERE Id = :oppty.id];
        //system.debug('<<<<<<'+oppty.Id+'<<<<<'+oppty.Ground_Breaking__c+'<<<<<'+opptyAfterTaskInsert[0].Id+'<<<<<'+opptyAfterTaskInsert[0].Ground_Breaking__c);
      system.assertEquals(opptyAfterTaskInsert.get(0).Ground_Breaking__c, System.today());                                                
      t.Subject = Constants.PRESENTATION;
      t.ActivityDate = system.today() - 1;
      update t;
      List<Opportunity> opptyAfterTaskUpdate = [SELECT Presentation_Date__c, ID
                                                FROM Opportunity 
                                                WHERE Id = :oppty.id];
      system.assert(opptyAfterTaskUpdate.get(0).Presentation_Date__c == (system.today()-1));
      Test.StopTest();
    }
    
    //Name             :     leaseExpirationDateTest Method
      //Description      :     Test Method created to test check the field values of Task 
      //                       after the activation of COMM Flex Contract Expiration Date 
      //                       Workflow rule
      //Created by       :     Meghna Vijay I-215112 
      //Created Date     :     May 4 , 2016
    
    static testMethod void leaseExpirationDateTest() {
     List<Task> taskList = TestUtils.createTask(5,false);
     taskList.get(0).Subject = Constants.FLEX_CONTRACT;
     insert taskList;
    }
}