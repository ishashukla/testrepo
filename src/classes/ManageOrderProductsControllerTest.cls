/**=====================================================================
 * Appirio, Inc
 * Name: ManageOrderProductsControllerTest 
 * Description: Test class for ManageOrderProductsController (T-493111)
 * Created Date: 28th April 2016
 * Created By: Rahul Aeran(Appirio)
 * 
 * Date Modified      Modified By              Description of the update
 * 11 Aug 2016        Kanika Mathur            COMM I-228955: Modified to remove dependency on Room__c field
=======================================================================*/
@isTest
private class ManageOrderProductsControllerTest { 
    @isTest
  static void testManageOrderProductsControllerTest(){
    User sysAdmin =  TestUtils.createUser(1,'System Administrator', true).get(0);
    System.runAs(sysAdmin){
	    List < Account > accountList = TestUtils.createAccount(1, false);
	    accountList[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.COMM_Intl_Accounts_RT_Label).getRecordTypeId();
	    insert accountList;
	    //TestUtils.createCommConstantSetting();
	    PriceBook2 priceBook = TestUtils.createPriceBook(1,'test',false).get(0);
      priceBook.name = 'COMM Price Book';
      insert priceBook;
	    List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, false);
	    oppList[0].RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeId();
	    insert oppList;

	    List < Project_Plan__c > projectPlanList = TestUtils.createProjectPlan(1, accountList[0].id, true);
	    List<Project_Phase__c> projectPhaseList = TestUtils.createProjectPhase(1,projectPlanList.get(0).Id,true);
	    
	    List<product2> productList = TestUtils.createProduct(1,false);
	    productList.get(0).Track_In_IB__c = true;
	    productList.get(0).RecordTypeId = [select id from recordtype where SobjectType = 'Product2' and developername = 'Master'].id;
	    insert productList; 

	    List<Pricebook2> priceBookList = TestUtils.createPriceBook(1,'Test Price Book',true);
	    //standard price book
	    TestUtils.createPriceBookEntry(1,Test.getStandardPricebookId(),productList.get(0).Id,true);
	    
	    List<PriceBookEntry> priceBookEntryList = new List<PriceBookEntry>();
	    //custom price book
	    priceBookEntryList.addAll(TestUtils.createPriceBookEntry(1,priceBooklist.get(0).Id,productList.get(0).Id,false));
	    insert priceBookEntryList;
	    
	    List<Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
	    orderList.get(0).opportunityid  =  oppList[0].id;
	    orderList.get(0).Pricebook2id = priceBookList.get(0).Id;
	    orderList.get(0).Project_Plan__c = projectPlanList.get(0).Id;
	    orderList.get(0).Oracle_Order_Number__c = '12345';
	    orderList.get(0).RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(Constants.COMM_ORDER_RECORD_TYPE_NAME).getRecordTypeId();
	    insert orderList;  
	
	    Test.startTest();
		    List<OrderItem> orderItemList = TestUtils.createOrderItem(10,projectPhaseList.get(0).Id,orderList.get(0).Id,priceBookEntryList.get(0).Id,false);
		    /*for(OrderItem oi : orderItemList){
		        oi.Project_Plan__c = projectPlanList.get(0).Id;
		    }*/
		    insert orderItemList;
	      	PageReference pageRef = Page.OrderLineItemAddressPage;
    	    Test.setCurrentPage(pageRef);
    	    ManageOrderProductsController cont;// = new ManageOrderProductsController();
    	    System.currentPageReference().getParameters().put('id',projectPhaseList.get(0).Id);
    	    cont  = new ManageOrderProductsController();
    	    Integer i = 0;
    	    System.debug('Items size:'+cont.items.size());

    	    for(OrderLineItemAddressController.OrderLIneItemWrapper oli : cont.items){
    	        i++;
    	        if(Math.mod(i,2) == 0){
    	            oli.isSelected = true;
    	        }
    	    }
    	    
    	    cont.updateProjectPhase();
    	 Test.stopTest();
    }
  }
}