/**================================================================      
* Appirio, Inc
* Name: RegulatoryQuestionAnswerTriggerHandler
* Description: Created for Handling the trigger events on RQA object
* Created Date: 9th Nov 2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
public class RegulatoryQuestionAnswerTriggerHandler {
    public static void OnBeforeInsert(List<Regulatory_Question_Answer__c> newList){
        SetRqaOptionforOffline(newList);
    }

    //I-239595 Methods created to set the options for RQA in offline mode.
    private static void SetRqaOptionforOffline(List<Regulatory_Question_Answer__c> newList){
        for(Regulatory_Question_Answer__c item :newList){
            if(item.Patient_involvement_Medical_delay__c == 'Yes'
                    && item.Formal_complaint__c == false 
                    && item.Adverse_Consequences__c == 'No'){
                item.Answer_8__c = 'Yes - No Impact';
            }
            
            if(item.Patient_involvement_Medical_delay__c == 'No'){
                item.Answer_8__c = 'No - No Impact';
            }
            
            if(item.Patient_involvement_Medical_delay__c == 'Complainant Not Aware' 
                    && item.Formal_complaint__c == false 
                    && item.Adverse_Consequences__c == 'No'){
                item.Answer_8__c = 'Complainant Not Aware';
            }
            if((item.Adverse_Consequences__c != null && item.Adverse_Consequences__c == 'No')
                    || String.isBlank(item.Answer_11__c) == true 
                    || item.Answer_11__c == '--Please Select--'){
                item.Answer_11__c = 'No';
            }
            if(String.isBlank(item.Answer_7_New__c) == true || item.Answer_7_New__c == '--Please Select--'){
                item.Answer_7_New__c = 'No Associated Procedure';
            }
            
            if(String.isBlank(item.Answer_9__c) == true || item.Answer_9__c == '--Please Select--'){
                item.Answer_9__c = 'No Associated Procedure';
            }
            
            if(String.isBlank(item.Answer_10__c) == true || item.Answer_10__c == '--Please Select--'){
                item.Answer_10__c = 'No';
            }
            
            if(String.isBlank(item.Adverse_Consequences__c) == true || item.Adverse_Consequences__c == '--Please Select--'){
                item.Adverse_Consequences__c = 'No';
            }
        }
    }
}