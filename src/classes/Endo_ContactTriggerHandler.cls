/*************************************************************************************************
Created By:    Sunil Gupta
Date:          Feb 06, 2014
Description  : Handler class for Contact Trigger
Feb 04,2016    Sahil Batra   Update(T-472925)

Updated      : Shreerath Nair (I-220603) Added logic to check if Changed account already exist on Contact Account Association
**************************************************************************************************/
public class Endo_ContactTriggerHandler {
    
    //-----------------------------------------------------------------------------------------------
  //  Populate Mailing Address on Contact from Account
  //-----------------------------------------------------------------------------------------------
  public static void UpdateMailingAddress(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts) {
    Schema.RecordTypeInfo rtByNameCC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
    Id recordTypeCC = rtByNameCC.getRecordTypeId();
    
    // prepare map for AccountId versue list of contacts.
    map<Id, List<Contact>> mapContacts = new map<Id, List<Contact>>();
    for(Contact c :lstNewContacts){
      if(c.RecordTypeId == recordTypeCC && ((mapOldContacts == null) || mapOldContacts.get(c.Id).AccountId != c.AccountId )){
        if(mapContacts.containsKey(c.AccountId) == false){
            mapContacts.put(c.AccountId, new List<Contact>());
          }
          mapContacts.get(c.AccountId).add(c);
        }
      }      
    System.debug('@@@' + mapContacts);
    
    List<Account> lstAccounts = [SELECT ShippingStreet, ShippingStateCode, ShippingState, ShippingPostalCode, ShippingCountryCode,
                  ShippingCountry, ShippingCity FROM Account WHERE Id IN :mapContacts.keySet()];
                  
    for(Account acc :lstAccounts){
        for(Contact con :mapContacts.get(acc.id)){
            con.MailingStreet = acc.ShippingStreet;
            con.MailingStateCode = acc.ShippingStateCode;
            con.MailingState = acc.ShippingState;
            con.MailingPostalCode = acc.ShippingPostalCode;
            con.MailingCountryCode = acc.ShippingCountryCode;
            con.MailingCountry = acc.ShippingCountry;
            con.MailingCity = acc.ShippingCity;
        }
    }
  } 
    
    
    
  //-----------------------------------------------------------------------------------------------
  //  Populate Sales Support Agent on Contact
  //-----------------------------------------------------------------------------------------------
  public static void populateSalesSupportAgent(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts) {
  Schema.RecordTypeInfo rtByNameCC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
    Id recordTypeCC = rtByNameCC.getRecordTypeId();
    set<Id> setTopSalesRep = new set<Id>();
    //Prepare map for Region with List of Contacts
    Map<String, List<Contact>> mapContacts = new Map<String, List<Contact>>();
    System.debug('@@@' + mapOldContacts);
    for(Contact c :lstNewContacts){
      if(c.RecordTypeId == recordTypeCC){
        
        if((mapOldContacts == null || mapOldContacts.get(c.Id).ENDO_Region_Name__c != c.ENDO_Region_Name__c || mapOldContacts.get(c.Id).Top_35_Sales_Agent__c != c.Top_35_Sales_Agent__c) && c.ENDO_Region_Name__c != null ){
          if(mapContacts.containsKey(c.ENDO_Region_Name__c.toLowerCase()) == false){
            mapContacts.put(c.ENDO_Region_Name__c.toLowerCase(), new List<Contact>());
          }
          mapContacts.get(c.ENDO_Region_Name__c.toLowerCase()).add(c);
        }
      }      
    }
  System.debug('@@@' + mapContacts);
    
    String strRegions = '';
    for(String s :mapContacts.keySet()){
        strRegions = strRegions + '\'' + s + + '\'' + ',';
    }
    if(strRegions.endsWith(',')){
        strRegions = strRegions.subString(0, strRegions.length() - 1);
    }
    if(strRegions.length() > 0){
        strRegions = '(' + strRegions + ')';
    }
    System.debug('@@@' + strRegions);
    
    // Query Users on base of Regions.
    List<User> lstUsers = new List<User>();
    if(String.isBlank(strRegions) == false){
        //String qry = 'SELECT Id, Endo_Support_Regions__c FROM User WHERE Endo_Support_Regions__c INCLUDES' + strRegions;
        String qry = 'SELECT Id,Title, Endo_Support_Regions__c, Top_35_Sales_Agent__c FROM User WHERE IsActive = true AND (Top_35_Sales_Agent__c = true OR Endo_Support_Regions__c INCLUDES' + strRegions + ')';
        System.debug('@@@' + qry);
        lstUsers = Database.query(qry);
    }/*else{
      String qry = 'SELECT Id, Endo_Support_Regions__c, Top_35_Sales_Agent__c FROM User WHERE IsActive = true AND (Top_35_Sales_Agent__c = true)';
      lstUsers = Database.query(qry);
    }*/
    System.debug('@@@' + lstUsers);
    Id top35SalesAgentId = null;   
    // Fetch users which contains the Regions as a value in u.Endo_Support_Regions__c
    // Fetch users which contains the Regions as a value in u.Endo_Support_Regions__c
    map<String, String> mapSalesUsers = new map<String, String>(); 
    map<String, String> mapSampleUsers = new map<String, String>();   
      
    for(User u :lstUsers){
        if(u.Top_35_Sales_Agent__c == true){
        top35SalesAgentId = u.Id;
      }
      else{      
        for(String region :u.Endo_Support_Regions__c.split(';')){
          if(String.isBlank(region) == false){
              if(u.title!=null && u.title.indexof('Sales')!=-1){
            mapSalesUsers.put(region.toLowerCase(), u.Id);
              }else if(u.title!=null && (u.title.indexof('Sample')!=-1 || u.title.indexof('Samples')!=-1 )){
                  mapSampleUsers.put(region.toLowerCase(), u.Id);
              }
          }
        }    
      } 
    }
    System.debug('@@@ Sales Users' + mapSalesUsers);
    System.debug('@@@ Sample Users' + mapSampleUsers);
    System.debug('@@@' + top35SalesAgentId);
    
    //List<Contact> toBeUpdate = new List<Contact>();
    for(String rgn :mapContacts.keySet()){ 
        if(mapSampleUsers.get(rgn) != null){
          Id samplesupportAgentId = mapSampleUsers.get(rgn.toLowerCase());
            system.debug('@@@samplesupportAgentId ' + samplesupportAgentId);
             for(Contact c : mapContacts.get(rgn.toLowerCase())){
                 c.samples_agent__c = samplesupportAgentId;
             }
        } else{
            for(Contact c : mapContacts.get(rgn.toLowerCase())){
                  c.samples_agent__c=null;
             }
           
        }
        
        if(mapSalesUsers.get(rgn) != null){
          Id supportAgentId = mapSalesUsers.get(rgn.toLowerCase());
              system.debug('@@@supportAgentId ' + supportAgentId);
          for(Contact c : mapContacts.get(rgn.toLowerCase())){
            System.debug('@@@' + c);
            if(c.Top_35_Sales_Agent__c){
              c.Sales_Support_Agent__c = top35SalesAgentId;
            }else{
              c.Sales_Support_Agent__c = supportAgentId;
            }
          } 
        }else{
           for(Contact c : mapContacts.get(rgn.toLowerCase())){
              if(c.Top_35_Sales_Agent__c){
              c.Sales_Support_Agent__c = top35SalesAgentId;
            }else{
              c.Sales_Support_Agent__c = null;
            }
               
               
          }  
        }
    }    
  }
  
  //-----------------------------------------------------------------------------------------------
  //  Populate Sales Support Agent on Case for This contact - code depricated
  //-----------------------------------------------------------------------------------------------
  
  /*
  public static void populateSalesSupportAgentOnCase(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts) {
    map<Id, boolean> contactIds = new map<Id, boolean>();
    for(Contact c : lstNewContacts){
        if(mapOldContacts != null && mapOldContacts.get(c.Id).Top_35_Sales_Agent__c != c.Top_35_Sales_Agent__c){
            contactIds.put(c.Id, c.Top_35_Sales_Agent__c);
        }
    }
    List<Case> caseList = [select Id, Top_35_Sales_Rep__c, ContactId, Sales_Rep__c from Case Where ContactId IN :contactIds.keySet() OR Sales_Rep__c IN :contactIds.keySet()];
    if(caseList.isEmpty()) return;
    for(Case c : caseList){
        if(contactIds.containsKey(c.ContactId)){
            c.Top_35_Sales_Rep__c = contactIds.get(c.ContactId);
        } else if(contactIds.containsKey(c.Sales_Rep__c)){
            c.Top_35_Sales_Rep__c = contactIds.get(c.Sales_Rep__c);
        }
    }
    update caseList;
  }
  */
  
  //-----------------------------------------------------------------------------------------------
  // Insert record in contact account junction object.
  //-----------------------------------------------------------------------------------------------
  public static void InsertContactAccountAssociation(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts) {
    Schema.RecordTypeInfo rtByNameCC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Customer Contact');
    Id recordTypeCC = rtByNameCC.getRecordTypeId();
    //T-472925 Update Start - Get reocrd type Id for Instruments Contact
    Schema.RecordTypeInfo rtByNameIntrument = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
    Id recordTypeInstrument = rtByNameIntrument.getRecordTypeId();
    //T-472925 Update End
    List<Id> contactsToUpdate = new List<Id>();
    //Schema.RecordTypeInfo rtByNameSC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
    //Id recordTypeSC = rtByNameSC.getRecordTypeId();
    
   //I-220603(SN) - Map to associate contact with its Account
    Map<Id,Id> contactToAssociationMap = new Map<Id,Id>();
    
    List<Contact_Acct_Association__c> lstCustomerContacts = new List<Contact_Acct_Association__c>();
    //List<Stryker_Contact__c> lstStrykerContacts = new List<Stryker_Contact__c>();
    System.debug('@@@' + mapOldContacts);    
    for(Contact c :lstNewContacts){
        if(mapOldContacts == null || mapOldContacts.get(c.Id).AccountId != c.AccountId){
        //System.debug('@@@' + c.RecordTypeId == recordTypeCC);
        System.debug('@@@' + c.RecordTypeId);
        System.debug('@@@' + recordTypeCC);
        //T-472925 Added Condition to check if it is instrument record type
          if((c.RecordTypeId == recordTypeCC || c.RecordTypeId == recordTypeInstrument)&& c.accountId != null){
            
            Contact_Acct_Association__c obj = new Contact_Acct_Association__c();
            obj.Associated_Account__c = c.AccountId;
            obj.Associated_Contact__c = c.Id;
            obj.Primary__c = true;
            lstCustomerContacts.add(obj);
            contactsToUpdate.add(c.Id);
            
            //I-220603(SN)- Put the contact with Account its Associated
            contactToAssociationMap.put(c.id,c.AccountId);
          }
          /*
          else if(c.RecordTypeId == recordTypeSC && c.accountId != null){
            Stryker_Contact__c obj = new Stryker_Contact__c();
            obj.Customer_Account__c = c.AccountId;
            obj.Internal_Contact__c = c.Id;
            lstStrykerContacts.add(obj);
          }*/
        }
    }
    System.debug('@@@' + lstCustomerContacts);
    //System.debug('@@@' + lstStrykerContacts);

    List<Contact_Acct_Association__c> listCAA= [SELECT Id, Associated_Account__c, Associated_Contact__c, Primary__c FROM Contact_Acct_Association__c WHERE Associated_Contact__c IN :contactsToUpdate];
    for(Contact_Acct_Association__c caa : listCAA){
      
      
      //I-220603(SN)- Check if the changed account already exist on Contact Account Association	
      if(contactToAssociationMap.containsKey(caa.Associated_Contact__c) &&
      	 contactToAssociationMap.get(caa.Associated_Contact__c)== caa.Associated_Account__c){
      	caa.Primary__c = true;
      }
      else{
      	caa.Primary__c = false;
      }
    }
    
    //update existing Contact_Acct_Association__c records to primary=false to maintain a singular primary record
    Database.update(listCAA);
    
    Database.insert(lstCustomerContacts,false);
    //insert lstStrykerContacts; We are not sure for Internal contact as of now so commented this.    
  }
  //-----------------------------------------------------------------------------------------------
  //  Update the Employee Contact and Product Junction Table if Product line value has been changed
  //-----------------------------------------------------------------------------------------------
  /*
  public static void UpdateEmployeeContactProductJunction(Map<Id, Contact> oldMap, List<Contact> newList){
    System.debug('eeeeeeeeeeeeeeeee');
    Id employeeRecordTypeId = Schema.sObjectType.Contact.getRecordTypeInfosByName().get('Endo Employee').getRecordTypeId();
    
    // Prepare map for eligible Contacts
    Map<Id, Contact> mapEligibleContacts = new Map<Id, Contact>();
    for(Contact c :newList){
        if(c.recordTypeId == employeeRecordTypeId && c.Product_Line__c != oldMap.get(c.Id).Product_Line__c){
            mapEligibleContacts.put(c.id, c);
        }
    }
    System.debug('ssssssssssss@@@' + mapEligibleContacts);
    
    // Prepare Map for Contacts
    map<String, List<Id>> mapContacts = new map<String, List<Id>>(); 
    for(Contact c :mapEligibleContacts.values()){
      for(String productLine :c.Product_Line__c.split(';')){
        if(String.isBlank(productLine) == false){
          if(mapContacts.containsKey(productLine) == false){
            mapContacts.put(productLine, new List<Id>());
          }
          mapContacts.get(productLine).add(c.Id);
        }
      }     
    }
    System.debug('xxxxxxxxxx@@@' + mapContacts);
    
    
    // Query Products for new product line values.
    map<String, List<Id>> mapProducts = new map<String, List<Id>>();
    for(Product2 product :[SELECT Id, ODP_Category__c FROM Product2 WHERE ODP_Category__c IN :mapContacts.keySet()]){
      if(String.isBlank(product.ODP_Category__c) == false){
        if(mapProducts.containsKey(product.ODP_Category__c) == false){
            mapProducts.put(product.ODP_Category__c, new List<Id>());
        }
        mapProducts.get(product.ODP_Category__c).add(product.Id);
      }
    }
    System.debug('ccccccccc@@@' + mapProducts);
    
    
    // Fetch all junction records related to these contacts.
    List<Employee_Contact_Product__c> lstToDelete = [SELECT Id FROM Employee_Contact_Product__c WHERE Contact__c 
                                               IN :mapEligibleContacts.keySet()];
    System.debug('@@@' + lstToDelete);
    delete lstToDelete;
    
    
    // Insert again the junction values.
    List<Employee_Contact_Product__c> juctionList = new List<Employee_Contact_Product__c>();
    for(String productLine :mapContacts.keySet()){
        if(mapProducts.get(productLine) != null){
          for(Id productId :mapProducts.get(productLine)){
            for(Id contactId :mapContacts.get(productLine)){
              Employee_Contact_Product__c objJunction = new Employee_Contact_Product__c();
              objJunction.Contact__c = contactId;
              objJunction.Product__c = productId;
              juctionList.add(objJunction);
            }
          }
        }
    }
    System.debug('@@@' + juctionList);
    insert juctionList;
    
  }*/
  
  
  /*public static void populateSalesSupportAgent(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts) {
    Schema.RecordTypeInfo rtByNameCC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
    Id recordTypeCC = rtByNameCC.getRecordTypeId();
    
    //Prepare map for Region with List of Contacts
    Map<String, List<Contact>> mapContacts = new Map<String, List<Contact>>();
    System.debug('@@@' + mapOldContacts);
    for(Contact c :lstNewContacts){
      if(c.RecordTypeId == recordTypeCC && (mapOldContacts == null || mapOldContacts.get(c.Id).ENDO_Region_Name__c != c.ENDO_Region_Name__c) && c.ENDO_Region_Name__c != null){
        if(mapContacts.containsKey(c.ENDO_Region_Name__c) == false){
          mapContacts.put(c.ENDO_Region_Name__c, new List<Contact>());
        }
        mapContacts.get(c.ENDO_Region_Name__c).add(c);
      }      
    }
    System.debug('@@@' + mapContacts);
    
    String strRegions = '';
    for(String s :mapContacts.keySet()){
        strRegions = strRegions + '\'' + s + + '\'' + ',';
    }
    if(strRegions.endsWith(',')){
        strRegions = strRegions.subString(0, strRegions.length() - 1);
    }
    if(strRegions.length() > 0){
        strRegions = '(' + strRegions + ')';
    }
    System.debug('@@@' + strRegions);
    
    // Query Users on base of Regions.
    List<User> lstUsers = new List<User>();
    if(String.isBlank(strRegions) == false){
        String qry = 'SELECT Id, Endo_Support_Regions__c FROM User WHERE Endo_Support_Regions__c INCLUDES' + strRegions;
        System.debug('@@@' + qry);
        lstUsers = Database.query(qry);
    }
    System.debug('@@@' + lstUsers);
       
    // Fetch users which contains the Regions as a value in u.Endo_Support_Regions__c
    map<String, Id> mapUsers = new map<String, String>(); 
    for(User u :lstUsers){
      for(String region :u.Endo_Support_Regions__c.split(';')){
        System.debug('@@@' + region);
        if(String.isBlank(region) == false){
          mapUsers.put(region, u.Id);
        }
      }     
    }
    System.debug('@@@' + mapUsers);
    
    List<Contact> toBeUpdate = new List<Contact>();
    for(String rgn :mapContacts.keySet()){
        if(mapUsers.get(rgn) != null){
          Id supportAgentId = mapUsers.get(rgn);
          for(Contact c :mapContacts.get(rgn)){
            toBeUpdate.add(new Contact(Id = c.Id, Sales_Support_Agent__c = supportAgentId));
          } 
        }
    }
    
    System.debug('@@@' + toBeUpdate);
    update toBeUpdate;
    
  }*/
  
}