public class BigMachinesEditQuoteExtension {
    
    // URLs for Visualforce pages
    private String bmEditURL = '';
    
    private BigMachines__Quote__c bmQuote;
    private Boolean bmUsesSSL;

    public BigMachinesEditQuoteExtension(ApexPages.StandardController stdCtrl) {
        this(stdCtrl, BigMachinesParameters.usesSSL);
    }
    
    public BigMachinesEditQuoteExtension(ApexPages.StandardController stdCtrl, Boolean ssl) {
        bmQuote = (BigMachines__Quote__c)stdCtrl.getRecord();
        bmUsesSSL = ssl;
        buildURL();
    }

    private void buildURL() {
        bmEditURL = 'http' + ((bmUsesSSL) ? 's' : '') + '://'
                  + BigMachinesParameters.bm_site + '.bigmachines.com/commerce/buyside/'
                  + 'document.jsp?formaction=performAction'
                  + '&action_id=' + BigMachinesParameters.action_id_open
                  + '&bm_url_capture_cookie=' + BigMachinesParameters.bm_site
                  + '&bs_id=' + bmQuote.BigMachines__Transaction_Id__c 
                  + '&_partnerOpportunityId=' + bmQuote.BigMachines__Opportunity__c
                  + '&_partnerAccountId=' + bmQuote.BigMachines__Opportunity__r.AccountId
                  + '&bm_cm_process_id=' + BigMachinesParameters.process_id
                  + '&document_id=' + BigMachinesParameters.document_id;
    }
    
    public String getEditURL() {
        return bmEditURL;
    }
}