/*************************************************************************************************
Created By:    Gagandeep Brar (Stryket IS)
Date:          April 4th , 2016
Description  : Test class to test Trigger Stryker_EmailtoCaseTrigger and Apex Code in 
                Stryker_EmailtoCaseTriggerHandler Class

April.05.2016; Gagan; Initial Test Class created
Aug.25.2016; Gagan; Updated to include logic for Instrument
**************************************************************************************************/
@isTest
public class Stryker_EmailtoCaseTriggerHandlerTest {

    private static List<Account>  accounts;
    private static Contact testContact;
    private static List<Case>  cases;
    private static Asset objAsset;
    private static testMethod void testNewEmailtoCase() {
        createTestData();
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = testContact.email;
        newEmail.ToAddress = testContact.email;
        newEmail.ParentId = cases[0].id;
        newEmail.Subject = 'Test Email-to-Case';
     	newEmail.Status = '0';
        INSERT newEmail;
        
        Test.startTest();
            Case Testcase = [SELECT id, contactId FROM Case WHERE Id =: cases[0].id];
            System.assertEquals(testContact.Id, Testcase.ContactId);
        Test.stopTest();
    }
    
    private static void createTestData(){
        accounts = Medical_TestUtils.createAccount(1, true);
        
        RecordType contactRecordType = [SELECT DeveloperName, Id FROM RecordType WHERE DeveloperName =: 'Medical_Customer_Contact'];
        testContact = new Contact(FirstName = 'TestFName', LastName = 'TestLName',Email = 'testemail123@stryker.com',AccountId = accounts[0].id,RecordTypeId = contactRecordType.Id, 
                                  MailingStreet = '123 Main st', MailingCity = 'San Jose', MailingCountry = 'United States', MailingState = 'California', MailingPostalCode = '95001');
        INSERT testContact;
        objAsset = Medical_TestUtils.createAsset(1,accounts.get(0).id,true).get(0);
        cases = Medical_TestUtils.createCase(1, accounts.get(0).id, null , false);
        Schema.DescribeSObjectResult caseRT = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> caseRecordTypeMap = caseRT.getRecordTypeInfosByName(); 
        Id rtId = caseRecordTypeMap.get('Medical - Potential Product Complaint').getRecordTypeId();
        cases[0].RecordTypeId = rtId;
        TestUtils.createRTMapCS(cases[0].RecordTypeId,'Medical - Potential Product Complaint','Medical');
        insert cases;
        
        List<Case_Asset_Junction__c> lstCaseAssets = new List<Case_Asset_Junction__c>();
        Case_Asset_Junction__c objJunction1 = Medical_TestUtils.createCaseAssetJunction(1, cases[0].id, objAsset.Id, true).get(0);
        RecordType caseRecordType = [SELECT DeveloperName, Id FROM RecordType WHERE DeveloperName =: 'Medical_Call_Center_Case_Type'];
        cases[0].RecordTypeId = caseRecordType.Id;
        cases[0].SuppliedEmail = testContact.email;
        Cases[0].Origin = 'Email';
        if(cases[0].AssetId == null){
            cases[0].AssetId = objAsset.Id;
        }
        if(cases[0].Related_Asset__c == null){
            cases[0].AssetId = objAsset.Id;
        }
        UPDATE cases;
    }
}