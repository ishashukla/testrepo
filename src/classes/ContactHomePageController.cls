// 
// 
//
// ContactHomePageController for ContactHomePage Page
//
// 03  Feb 2016  Sahil Batra    Original(T-476369)
// 
// 
//
public class ContactHomePageController {
	public Integer size{get;set;}
	public Integer noOfRecords{get;set;}
	public String selectedView {get;set;}
	public String selectedRole {get;set;}
	public String selectedAccount {get;set;}
	public List<SelectOption> viewOptions {get;set;}
	public List<SelectOption> rolesOptions {get;set;}
	public List<SelectOption> accountOptions {get;set;}
	public ApexPages.StandardSetController setCon{get;set;}
	public String profileName;
	public List<EntitySubscription> entityList;
	public List<Contact_Acct_Association__c> contactList{get;set;}
	public List<String> selectedFields{get;set;}
	public String followingUserString{get;set;}
	public Id chatterUser{get;set;}
	public Id currentContact{get;set;}
	public String sortedField{get;set;}
	public boolean isAsc{get;set;}
	public Map<String,String> apiNameToLabelMap{get;set;}
	public String ascDesc;
	public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Contact.FieldSets.HomePageFields.getFields();
    }
	Set<Id> contactIds = new Set<Id>();
		  public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
  
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
	public void getAllContacts(){
		contactList = new List<Contact_Acct_Association__c>();
		for(Contact_Acct_Association__c c : (List<Contact_Acct_Association__c>)setCon.getRecords()){
			contactList.add(c);
		}
	}
	public ContactHomePageController(){
		rolesOptions = new List<SelectOption>();
		accountOptions = new List<SelectOption>();
		apiNameToLabelMap = new Map<String,String>();
		ascDesc = '';
		sortedField = '';
		chatterUser = null;
		selectedFields = new List<String>();
		size = Integer.valueOf(Label.My_Contact_List_Size);
		contactList = new List<Contact_Acct_Association__c>();
		setCon = new ApexPages.StandardSetController(contactList);
		setCon.setPageSize(size);
		noOfRecords = setCon.getResultSize();
		setCon.setPageNumber(1);
		viewOptions = new List<SelectOption>();
		selectedView = 'My Contacts';
		viewOptions.add(new SelectOption( 'My Contacts', 'My Contacts'));
  		viewOptions.add(new SelectOption( 'My Account Contact', 'My Account Contacts'));
		contactList = new List<Contact_Acct_Association__c>();
		List<Profile> profileLsit = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
		profileName = profileLsit.get(0).Name;
		selectedRole = 'All';
		selectedAccount = 'All';
		switchView();
	}
	public void switchView(){
		contactList.clear();
		followingUserString = '';
		sortedField = '';
		ascDesc = '';
		selectedFields = new List<String>();
		entityList = new List<EntitySubscription>();
		entityList = [SELECT ParentId,Id,SubscriberId
			     	  FROM EntitySubscription
				      WHERE SubscriberId =:UserInfo.getUserId()];
		if(entityList.size() > 0){
			for(EntitySubscription es : entityList){	
				String conId = es.ParentId;
				if(conId.substring(0,3) == '003'){
					followingUserString = followingUserString +';'+conId;
				}
			}
		}
		System.debug('>>>>>>>my accountttttt'+selectedView);
		createContactList(entityList);      
	}
	public void createContactList(List<EntitySubscription> entityList){
		if(selectedView == 'My Contacts'){
		//	selectedRole = 'All';
			if(entityList.size() > 0){
				contactIds = new Set<Id>();
				for(EntitySubscription es : entityList){
					String conId = es.ParentId;
					if(conId.substring(0,3) == '003'){
						contactIds.add(es.ParentId);
					}
				}
				if(contactIds.size() > 0){
					getContactRecords(contactIds);
				}
			}
		}
		else if(selectedView == 'My Account Contact'){
			System.debug('>>>>>>>my account');
			List<AccountTeamMember> accountTeamList = new List<AccountTeamMember>();
			accountTeamList = [SELECT UserId, AccountId FROM AccountTeamMember WHERE UserId =:UserInfo.getUserId()];
			Set<Id> accountIdSet = new Set<Id>(); 
			if(accountTeamList.size() > 0){
				for(AccountTeamMember member : accountTeamList){
					accountIdSet.add(member.AccountId);
				}
				System.debug('>>>>>>>my account1'+accountIdSet);
				List<Contact_Acct_Association__c> associationList = new List<Contact_Acct_Association__c>();
				associationList = [Select Associated_Contact__c,Associated_Contact__r.Personnel__c, Associated_Account__c 
								   From Contact_Acct_Association__c
								   WHERE Associated_Account__c IN:accountIdSet];
				System.debug('>>>>>>>my account2'+associationList);
				if(associationList.size() > 0){
					contactIds = new Set<Id>();
					for(Contact_Acct_Association__c acc : associationList){
						contactIds.add(acc.Associated_Contact__c);
					}
					System.debug('>>>>>>>my account3'+contactIds);
					if(contactIds.size() > 0){
						getContactRecords(contactIds);
					}
				}
			}
		}
	}
/*	public void getContactRecords(Set<Id> contactIds){
		for(Schema.FieldSetMember f : this.getFields()) {
			if(f.getFieldPath().equalsIgnoreCase('accountId')){
				apiNameToLabelMap.put('Account.Name' , 'Account Name');
			}
			else{
				apiNameToLabelMap.put(f.getFieldPath(),f.getLabel());
			}
		}
		List<Contact> tempList = new List<Contact>();
		String query ='SELECT Id ';
		for(Schema.FieldSetMember f : this.getFields()) {
           	query += ', ' + f.getFieldPath();
           	if(f.getFieldPath().equalsIgnoreCase('accountId')){
           		selectedFields.add('Account.Name');
           	}
           	else{
           		selectedFields.add(f.getFieldPath());
           	}
        }
        if(query.containsIgnoreCase('AccountId')){
        		query +=', '+'Account.Name';
        }
        query += ' FROM Contact WHERE ID IN :contactIds';
		if(selectedRole!='All'){
        	if(query.containsIgnoreCase('Personnel__c') && query.containsIgnoreCase('Name')){
        		query += ' AND Personnel__c=:selectedRole';
        	}
		}
		if(selectedAccount!='All'){
			if(query.containsIgnoreCase('Account.Name')){
        		query += ' AND AccountId=:selectedAccount';
        	}
		}
	//	else{
    //    	query += ' FROM Contact WHERE Id IN :contactIds';
	//	}
		if(sortedField!=''){ //order by '+ sortField + ' ' + ascDesc +' nulls last
			query += ' order by '+ sortedField + ' ' + ascDesc +' nulls last';
		}
				System.debug('>>>>>>>'+query);
				tempList = Database.query(query);
				if(query.containsIgnoreCase('Personnel__c') && selectedRole == 'All'){
					rolesOptions = new List<SelectOption>();
					rolesOptions.add(new SelectOption( 'All', 'All'));
					Set<String> roles = new  Set<String>();
					for(Contact con : tempList){
						if(con.Personnel__c!=null){
							roles.add(con.Personnel__c);
						}
					}
					if(roles.size() > 0){
						for(String role : roles){
							rolesOptions.add(new SelectOption(role,role));
						}
					}
				}
				if(query.containsIgnoreCase('Account.Name') && selectedAccount == 'All'){
					accountOptions = new List<SelectOption>();
					accountOptions.add(new SelectOption( 'All', 'All'));
					Map<String,String> accMap = new Map<String,String>();
					for(Contact con : tempList){
						accMap.put(con.AccountId,con.Account.Name);
					}
					if(accMap.size() > 0){
						for(String acc : accMap.keySet()){
							accountOptions.add(new SelectOption(acc,accMap.get(acc)));
						}
					}
				}
				setCon = new ApexPages.StandardSetController(tempList);
				setCon.setPageSize(size);
				noOfRecords = setCon.getResultSize();
				setCon.setPageNumber(1);
				getAllContacts();
	} */
		public void getContactRecords(Set<Id> contactIds){
		for(Schema.FieldSetMember f : this.getFields()) {
			System.debug('>>>>>>>'+f.getFieldPath());
			if(f.getFieldPath().equalsIgnoreCase('accountId')){
				apiNameToLabelMap.put('Associated_Account__r.Name' , 'Account Name');
			}
			else if(f.getFieldPath().equalsIgnoreCase('Name')){
				apiNameToLabelMap.put('Associated_Contact__r.Name' , 'Name');
			}
			else{
				apiNameToLabelMap.put('Associated_Contact__r.'+f.getFieldPath(),f.getLabel());
			}
		}
		List<Contact_Acct_Association__c> tempList = new List<Contact_Acct_Association__c>();
		List<Contact_Acct_Association__c> finalList = new List<Contact_Acct_Association__c>();
		String query ='SELECT Id,Associated_Contact__c,Associated_Account__c ';
		for(Schema.FieldSetMember f : this.getFields()) {
           	query += ', ' + 'Associated_Contact__r.'+f.getFieldPath();
           	if(f.getFieldPath().equalsIgnoreCase('accountId')){
           		selectedFields.add('Associated_Account__r.Name');
           	}
           	else{
           		selectedFields.add('Associated_Contact__r.'+f.getFieldPath());
           	}
        }
        if(query.containsIgnoreCase('accountId')){
        		query +=', '+'Associated_Account__r.Name';
        }
        query += ' FROM Contact_Acct_Association__c WHERE Associated_Contact__c IN :contactIds ';
		if(selectedRole!='All'){
        	if(query.containsIgnoreCase('Associated_Contact__r.Personnel__c') && query.containsIgnoreCase('Name')){
        		query += ' AND Associated_Contact__r.Personnel__c=:selectedRole';
        	}
		}
		if(selectedAccount!='All'){
			if(query.containsIgnoreCase('Associated_Account__r.Name')){
        		query += ' AND Associated_Account__c=:selectedAccount';
        	}
		}
	//	else{
    //    	query += ' FROM Contact WHERE Id IN :contactIds';
	//	}
		if(sortedField!=''){ //order by '+ sortField + ' ' + ascDesc +' nulls last
			query += ' order by '+ sortedField + ' ' + ascDesc +' nulls last';
		}
				System.debug('>>>>>>>'+query);
				tempList = Database.query(query);
		List<Contact> tempContactList = new List<Contact>();
		tempContactList = [SELECT Id,AccountId FROM Contact WHERE Id IN:contactIds];
		Set<String> contactAccount = new Set<String>();
		for(Contact c : tempContactList){
			contactAccount.add(c.Id+'+'+c.AccountId);
		}
		if(selectedAccount == 'All'){
			for(Contact_Acct_Association__c accCon : tempList){
				String key = accCon.Associated_Contact__c+'+'+accCon.Associated_Account__c;
				if(contactAccount.contains(key)){
					finalList.add(accCon);
				}
			}
		}
		else{
			finalList = tempList;
		}
				System.debug('>>>>>>>'+tempList);
				if(query.containsIgnoreCase('Personnel__c') && selectedRole == 'All'){
					rolesOptions = new List<SelectOption>();
					rolesOptions.add(new SelectOption( 'All', 'All'));
					Set<String> roles = new  Set<String>();
					for(Contact_Acct_Association__c con : tempList){
						if(con.Associated_Contact__r.Personnel__c!=null){
							roles.add(con.Associated_Contact__r.Personnel__c);
						}
					}
					if(roles.size() > 0){
						for(String role : roles){
							rolesOptions.add(new SelectOption(role,role));
						}
					}
				}
				if(query.containsIgnoreCase('Associated_Account__r.Name') && selectedAccount == 'All'){
					accountOptions = new List<SelectOption>();
					accountOptions.add(new SelectOption( 'All', 'All'));
					Map<String,String> accMap = new Map<String,String>();
					for(Contact_Acct_Association__c con : tempList){
						accMap.put(con.Associated_Account__c,con.Associated_Account__r.Name);
					}
					if(accMap.size() > 0){
						for(String acc : accMap.keySet()){
							accountOptions.add(new SelectOption(acc,accMap.get(acc)));
						}
					}
				} 
				setCon = new ApexPages.StandardSetController(finalList);
				setCon.setPageSize(size);
				noOfRecords = setCon.getResultSize();
				setCon.setPageNumber(1);
				getAllContacts();
	}
	public void sortData(){
		if(sortedField!=''){
		   if(ascDesc == ' desc '){
             ascDesc = ' asc ';
             isAsc = true;
           }
           else{
           ascDesc = ' desc ';
           isAsc = false;
           }
		  contactList.clear();
		  followingUserString = '';
		  rolesOptions = new List<SelectOption>();
		  selectedFields = new List<String>();
		  entityList = new List<EntitySubscription>();
		  entityList = [SELECT ParentId,Id,SubscriberId
			       	    FROM EntitySubscription
				        WHERE SubscriberId =:UserInfo.getUserId()];
		  if(entityList.size() > 0){
			for(EntitySubscription es : entityList){	
				String conId = es.ParentId;
				if(conId.substring(0,3) == '003'){
					followingUserString = followingUserString +';'+conId;
				}
			}
		  }
		  createContactList(entityList);      
		}
	}
	public PageReference redirectPage(){
		if(profileName.containsIgnoreCase('Admin') || profileName.containsIgnoreCase('Instrument')){
			return null;
		}else{
			PageReference pg = new PageReference('/003/o');
			return pg;
		}
	}
    public void first() {
        setCon.first();
        getAllContacts();
    }
  
    public void last() {
        setCon.last();
        getAllContacts();
    }
  
    public void previous() {
        setCon.previous();
        getAllContacts();
    }
  
    public void next() {
        setCon.next();
        getAllContacts();
    }
    public void followContact(){
    	EntitySubscription entity = new EntitySubscription();
    	if(chatterUser!=null){
    		entity.ParentId =chatterUser;
    		entity.SubscriberId = UserInfo.getUserId();
    		insert entity;
    	}
    	switchView();
    }
    public void unfollowContact(){
    	if(chatterUser!=null){
    		List<EntitySubscription> tempEntityList = new List<EntitySubscription>();
			tempEntityList = [SELECT ParentId,Id,SubscriberId
			     	  	  	  FROM EntitySubscription
				      	      WHERE SubscriberId =:UserInfo.getUserId() AND ParentId =:chatterUser];
			if(tempEntityList.size() > 0){
				delete tempEntityList.get(0);
			}
    	}
    	switchView();
    }
}