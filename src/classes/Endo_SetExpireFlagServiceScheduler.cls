/******************************************************************************************
 *  Purpose : Schedular class Endo_SetExpireFlagServiceContract
 *  Author  : Sunil Gupta
 *  Date    : April 23, 2014
*******************************************************************************************/  
global class  Endo_SetExpireFlagServiceScheduler implements Schedulable {
  
  // Call a batch class
  global void execute(SchedulableContext sc) {
     Endo_SetExpireFlagServiceContract obj = new Endo_SetExpireFlagServiceContract(); 
     database.executebatch(obj);
  }
}