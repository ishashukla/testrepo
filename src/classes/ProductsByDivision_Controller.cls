/****************************************************************
Name  :  ProductsByDivision_Controller
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 17, 2016

Modified: 
*****************************************************************/
public class ProductsByDivision_Controller
{
    public String selectedDivision {get; set;}
    public String sortChar {get; set;}
    public Set<String> setOfDistinctSections;
    public List<SelectOption> items {get; set;}
    public Map<String, String> mapOfBUAndPriceBookName {get; set;}
    public Set<String> setOfColumnsToDisplay {get; set;}
    public String fieldQuery {get; set;}
    public List<String> searchAlphabet {get;set;}
    public List<Product2> allSearchResults {
        get {
            if(con != null)
                return (List<Product2>)con.getRecords();
            else
                return new List<Product2>();
        } set;
    }
    
    //Controller
    public  ProductsByDivision_Controller () {
        searchAlphabet = new List<String> {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','All'};
        sortChar = 'All';
        mapOfBUAndPriceBookName = new Map<String, String> ();
        items = new List<SelectOption> ();
        allSearchResults = new List<Product2>();
        setOfDistinctSections = new Set<String> ();
        for(Quick_Link_Section__c qls : [SELECT Id, Section__c
                                        FROM Quick_Link_Section__c]) {
            if(!setOfDistinctSections.contains(qls.Section__c)) {
                setOfDistinctSections.add(qls.Section__c);    
            }
        }
        for(String s : setOfDistinctSections) {
            items.add(new SelectOption(s, s));    
        }
        for(String s : setOfDistinctSections) {
            selectedDivision = s;
            break;  
        }
        for(BusinessUnitAndPricebookName__c custSet : [SELECT Id, Name, Price_Book_Name__c
                                                        FROM BusinessUnitAndPricebookName__c
                                                        WHERE Name IN :setOfDistinctSections]) {
            mapOfBUAndPriceBookName.put(custSet.Name, custSet.Price_Book_Name__c);
        }
        buildQueryString();
        search();
    }
    
     public PageReference search() {
        if(!String.isBlank(selectedDivision) && !mapOfBUAndPriceBookName.isEmpty()) {
            Set<Id> setOfPriceBookIds = new Set<Id> ();
            for(PriceBook2 pb : [SELECT Id 
                                FROM PriceBook2
                                WHERE Name = :mapOfBUAndPriceBookName.get(selectedDivision)]) {
                setOfPriceBookIds.add(pb.Id);
            }
            if(!setOfPriceBookIds.isEmpty()) {
                Set<Id> setOfProductIds = new Set<Id> ();
                for(PriceBookEntry pbe : [SELECT Id, Product2Id
                                            FROM PriceBookEntry
                                            WHERE Pricebook2Id IN :setOfPriceBookIds]) {
                    if(!setOfProductIds.contains(pbe.Product2Id)) {
                        setOfProductIds.add(pbe.Product2Id);    
                    }
                }
                String query = 'SELECT Id, ' + fieldQuery + ' ' +
                                'FROM Product2 ' + 
                                'WHERE Id IN :setOfProductIds ';
                if(sortChar != 'All') {
                    query += 'AND Name LIKE \'' + String.escapeSingleQuotes(sortChar)+ '%' + '\' ';
                }
                query += 'ORDER BY Name LIMIT 10000';
                con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            }
            // sets the number of records in each page set
            con.setPageSize(20);
        } else {
            con = null;
        }
        return null;
    }
    
    //Instantiate the StandardSetController
    public ApexPages.StandardSetController con{get; set;}
    
    //Boolean to check if there are more records after the present displaying records
    public Boolean hasNext {
        get {
            return con.getHasNext();
        } set;
    }
 
    //Boolean to check if there are more records before the present displaying records
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        } set;
    }
 
    //Page number of the current displaying records
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        } set;
    }

    //Returns the previous page of records
    public void previous() {
        con.previous();
    }
 
    //Returns the next page of records
    public void next() {
        con.next();
    }

    private void buildQueryString() {
        setOfColumnsToDisplay = new Set<String> ();
        List<Schema.FieldSetMember> fieldSetMemberList =  readFieldSet('FieldsToDisplayOnDynamicList','Product2');
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList) {
            setOfColumnsToDisplay.add(fieldSetMemberObj.getFieldPath());
        }
        fieldQuery = '';
        for(String s : setOfColumnsToDisplay) {
            fieldQuery += s + ', ';
        }
        fieldQuery = fieldQuery.removeEnd(', ');
    }

    private List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    }

}