@isTest
private class NV_NotificationSettingsControllerTest {
	
	@testSetup static void setup(){
		List<User> testUsers = new List<User>();
		Profile sysAdminProfile = NV_Utility.getProfileByName('System Administrator');
		for (Integer i=1;i<=4;i++){
			testUsers.add(NV_TestUtility.createUser('testuser_ncs'+i+'@mail.com', 'testuser_ncs'+i+'@mail.com',
															sysAdminProfile.Id, null, 'nc'+i+'', false));
		}
		insert testUsers;

		Map<String, Id> accountRecordTypes1 = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');
        Account testAccount1 = NV_TestUtility.createAccount('TestAccount1',accountRecordTypes1.get('NV_Customer'), true);
        Contract testContract1 = NV_TestUtility.createContract(testAccount1.Id, true);
		testContract1.Status = 'Activated';
		update testContract1;

		Map<String, Id> orderRecordTypes1 = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
		Order testOrder1 = NV_TestUtility.createOrder(testAccount1.Id, testContract1.Id, 
														orderRecordTypes1.get('NV_Standard'), false);
		testOrder1.Customer_PO__c = 'PO00001';
		testOrder1.EffectiveDate = Date.today();
		testOrder1.Date_Ordered__c = Datetime.now();
		testOrder1.Status = 'Booked';
		insert testOrder1;
	}

	@isTest static void testNotificationSettings() {

		User testUser = [SELECT Id, Username FROM User WHERE Username like '%testuser_ncs%' LIMIT 1];
		
		System.runAs(testUser){
			//Test Scenario 1 where record is not available yet
			System.assertEquals(0, [SELECT count() FROM Notification_Settings__c WHERE User__c =: testUser.Id]);
			Notification_Settings__c setting1 = NV_NotificationSettingsController.getCurrentUserSettings();
			System.assertEquals(1, [SELECT count() FROM Notification_Settings__c WHERE User__c =: testUser.Id]);
			setting1.Booked__c = true;
			NV_NotificationSettingsController.setCurrentUserSettings(setting1);
			setting1 = NV_NotificationSettingsController.getCurrentUserSettings();
			System.debug('<<<>>>'+setting1);
			System.assertEquals(true, setting1.Settings__c.contains('Creation'));
		}
	}

	@isTest static void testFollowUsers() {
		List<User> testUsers = [SELECT Id, Username FROM User WHERE Username like '%testuser_ncs%'];

		List<UserRole> userRoles = new List<UserRole>();
		userRoles.add(NV_Utility.getRoleByName('Regional Manager - Mountain West'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - AZ Phoenix'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - WA Seattle'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - CO Denver'));

		//Update 1st user role
		testUsers[0].UserRoleId = userRoles[0].Id;
		update testUsers[0];
		Integer beforeCount = NV_Utility.getSubordinateUsers(testUsers[0].Id,userRoles[0].Id).size();

		for(Integer i=1; i<4 ; i++){
			testUsers[i].UserRoleId = userRoles[i].Id;
		}
		update testUsers;
        
		System.runAs(testUsers[0]){
			System.assertEquals(0, [SELECT count() FROM NV_Follow__c]);
			
			//Follow First 2 Users
			List<NV_Follow__c> followRecords = new List<NV_Follow__c>();
			followRecords.add(new NV_Follow__c(Following_User__c = testUsers[0].Id,
														Record_Id__c = testUsers[1].Id));
			followRecords.add(new NV_Follow__c(Following_User__c = testUsers[0].Id,
														Record_Id__c = testUsers[2].Id));
			insert followRecords;
			
			System.assertEquals(2, [SELECT count() FROM NV_Follow__c]);

			List<NV_NotificationSettingsController.FollowUser> followUsers = NV_NotificationSettingsController.getCurrentUserFollowUsers();

            System.assertEquals(true, followUsers.size()>=3);
			for(NV_NotificationSettingsController.FollowUser followUser : followUsers){
				if(followUser.RecordId == testUsers[1].Id || followUser.RecordId == testUsers[2].Id){
					followUser.IsFollowed = false;
				}
				else if(followUser.RecordId == testUsers[3].Id ){
					followUser.IsFollowed = true;
				}
			}
			NV_NotificationSettingsController.setCurrentUserFollowUsersJSON(JSON.serialize(followUsers));

			System.assertEquals(1, [SELECT count() FROM NV_Follow__c]);
		}
	}
	
}