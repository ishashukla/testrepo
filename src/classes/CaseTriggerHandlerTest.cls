/*******************************************************************************
 Name          :  CaseTriggerHandlerTest
 Author        :  Appirio JDC (Sunil)
 Date          :  Jan 27, 2014 
 Purpose       :  Test Class for Case TriggerHandler

 Updated By    : Nitish Bansal
 Updated Date  : 02/28/2016
 Purpose       : T-459650
 
 Updated By    : Jagdeep Juneja
 Updated Date  : 07/07/2016
 Purpose       : T-517828
*******************************************************************************/
@isTest
private class CaseTriggerHandlerTest {
  
  //-----------------------------------------------------------------------------------------------
  // Method for test notifySalesRepIfCaseAlreadyExists method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void notifySalesRepIfCaseAlreadyExists(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    Id recordTypeId = rtByName.getRecordTypeId(); 
    
    Test.startTest();
    /*Account acc = Endo_TestUtils.createAccount(1, true).get(0);
      
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;
    
    Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0); 
    */
    //Create test records  
    TestUtils.createRTMapCS(recordTypeId, 'Tech Support', 'Endo');
    
    Case objCase = new Case();
    objCase.Symptom_Type__c = 'Physical Damage'; 
    objCase.recordTypeId = recordTypeId;
    insert objCase;
    
    //Case objCase2 = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    Case objCase2 = new Case();
    objCase2.recordTypeId = recordTypeId;
    insert objCase2;
    
    
    
    Test.stopTest();
  }
  
  
  //-----------------------------------------------------------------------------------------------
  // Method for test notifySalesRepIfCaseAlreadyExists method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void autoPopulateSalesRep(){
    
    Test.startTest();
   /* Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.RecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo Customer').getRecordTypeId();
    insert acc;
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    con.RecordTypeId =   Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact').getRecordTypeId();
    insert con;
    
    
   //Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0); 
    
   
    
    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0); 
    */
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
    
    Case objCase = new Case();
    objCase.Sales_Rep__c = null;
    objCase.recordTypeId = rtByName.getRecordTypeId();
    insert objCase;
    Test.stopTest();
    
    objCase = [SELECT Id, Sales_Rep__c FROM Case WHERE Id = :objCase.Id].get(0);
    // Verify Sales Rep is populating
   // System.assertEquals(objCase.Sales_Rep__c, con.Id);
    
    //Case objCase2 = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0);
    //objCase2.recordTypeId = recordTypeId;
    //insert objCase2;
    
  }
  //-----------------------------------------------------------------------------------------------
  // Method for test populateAssignedStateForEmailToCase method
  //-----------------------------------------------------------------------------------------------
  /*public static testmethod void testPopulateAssignedStateForEmailToCase(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case');
    Id recordTypeId = rtByName.getRecordTypeId();    
    
    /*Account acc = Endo_TestUtils.createAccount(1, true).get(0);
      
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;
    
    Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0); 
    
   
    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0); 
    
    Case objCase = new Case();
    objCase.recordTypeId = recordTypeId;
    objCase.Origin = 'Email-to-Case';
    objCase.Description = 'Assigned State<MI>Account Number<123045>';
    objCase.Region_Comm__c = 'SE';
    
    Test.startTest();
    TestUtils.createCommConstantSetting();
    insert objCase;
    Case objCase2 = [SELECT Id, Assigned_State__c FROM Case WHERE Id = :objCase.Id].get(0);
    System.assertEquals(objCase2.Assigned_State__c, 'SE');
    Test.stopTest();
    
    
  }*/
  //-----------------------------------------------------------------------------------------------
  // Method for test populateAssignedStateForEmailToCase method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testUpdateAccountMoodValue(){
    
    /*Account acc = Endo_TestUtils.createAccount(1, true).get(0);
      
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;
    
    Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0); 
    
    
    Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0); 
    */
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
    TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
    
    Case objCase = new Case();
    objCase.recordTypeId = rtByName.getRecordTypeId();
    insert objCase;
    
    Test.startTest();
    
    objCase.Status = 'Closed';
    objCase.Mood_of_Customer__c = 'Very Satisfied';
    update objCase;
    
    Test.stopTest();
    
   // acc = [SELECT Id, Mood_of_Customer__c FROM Account WHERE Id = :acc.Id].get(0);
   // System.assert(acc.Mood_of_Customer__c == 'Very Satisfied');
    
  }

  static testMethod void copyCaseOwnerEmployeeNumberTest(){
     Profile  p = [Select Id From Profile Where Name='System Administrator' or Name='Systemadministrator' Limit 1];
     User  u = new User(Username='testTriggerController@test.com', LastName='testTriggerController', 
               Email='testTriggerController@test.com', Alias='test', CommunityNickname='testTriggerController',
               TimeZoneSidKey='Europe/Berlin', LocaleSidKey='en_US', EmailEncodingKey='UTF-8',
               ProfileId=p.Id, LanguageLocaleKey='en_US'); 
     insert u;
     System.runAs(u) {

      User owner = [select id from user where username=:UserInfo.getUserName()];
      /*Account acc = Endo_TestUtils.createAccount(1, true).get(0);
      
      Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
      con.Title= 'Sales Rep';
      insert con;
      Endo_TestUtils.createContactJunction(1, acc.Id, con.Id, true).get(0); 
      Case objCase = Endo_TestUtils.createCase(1, acc.Id, con.Id, false).get(0); 
      */
      Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case');
      TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'COMM US Stryker Field Service Case', 'COMM');
      
      Case objCase = new Case();
      objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
      insert objCase;
      Case c = new Case();
      c = [select id, Employee_Number__c from Case where Id=:objCase.Id limit 1];
      System.debug('*** after insert: Case Employee Number = ' + c.Employee_Number__c);
      c.ownerId = owner.Id;
      update c;
      c = [select id, Employee_Number__c from Case where Id=:c.Id limit 1];
      System.debug('*** after update: Case Employee Number = ' + c.Employee_Number__c);
    }   
   }
   
   static testMethod void DeleteCaseSharing(){
     List<Account> accList = TestUtils.createAccount(1,true);
     List<Opportunity> opptyList = new List<Opportunity>();
     List<Pricebook2> priceBookList = TestUtils.createpricebook(5,' ',false);
        priceBookList.get(0).Name = 'COMM Price Book';
        insert priceBookList;
        //Added below line to create Comm BP constant setting- KM-9/26/2016
        TestUtils.createCommBPConfigConstantSetting();
        List<Project_Plan__c> projectPlanList = TestUtils.createProjectPlan(3,accList.get(0).Id,true);
        opptyList = TestUtils.createOpportunity(2,accList.get(0).id,false);
        List<Opportunity> oppList = new List<Opportunity>();
        for(Opportunity oppty: opptyList) {
          oppty.RecordTypeID = 
          Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeID();
          //Schema.DescribeSObjectResult opptyRT = Schema.SObjectType.Opportunity; 
          // Map<String,Schema.RecordTypeInfo> opptyRTMap = opptyRT.getRecordTypeInfosByName(); 
          // oppty.RecordTypeID = opptyRTMap.get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeId();
          oppty.PO_Close_Date__c = system.today();
          oppty.Pricebook2Id = priceBookList.get(0).Id;
          oppty.Project_Plan__c = projectPlanList.get(0).Id;
          oppty.StageName = 'Prospecting';
          oppList.add(oppty);
        }
        insert oppList;
     List<User> userList = TestUtils.createUser(1,'System Administrator',true);
     List<User> userLst = TestUtils.createUser(1,Constants.PROFILE_SALES_REP_US,true);
     List<OpportunityTeamMember> otm = TestUtils.createOpportunityTeamMember(2,oppList.get(0).id,userLst.get(0).id, false);
     otm.get(1).OpportunityId = oppList.get(1).id;
     Insert otm;
     List<Case> caseList = TestUtils.createCase(1,accList.get(0).id,false);
     Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Change Request');
     TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'COMM Change Request', 'COMM');
     caseList.get(0).ownerId = userList.get(0).id;
     caseList.get(0).Opportunity__c = opptyList.get(0).id;
     caseList.get(0).RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Change Request').getRecordTypeId();
     insert caseList;
     caseList.get(0).Opportunity__c = opptyList.get(1).id;
     update caseList;
   }
   
   /*static testMethod void insertCaseAssetConnectorAfterCaseAssetInsertTestMethod(){
     Account acc = Endo_TestUtils.createAccount(1, true).get(0);
     Contact con = Endo_TestUtils.createContact(1, acc.Id, true).get(0);
     Product2 pro = Endo_TestUtils.createProduct(1,false).get(0); 
     pro.Tracked_as_Software__c = true;
     insert pro;
     System.debug('checkbox value is '+pro.Tracked_as_Software__c);
     SVMXC__Installed_Product__c installedProductVar1 = new SVMXC__Installed_Product__c();
     installedProductVar1.Name = 'Demo1';
     installedProductVar1.SVMXC__Product__c = pro.Id;
     
     SVMXC__Installed_Product__c installedProductVar2 = new SVMXC__Installed_Product__c();
     installedProductVar2.Name = 'Demo2';
     installedProductVar2.SVMXC__Product__c = pro.Id;    
     List<SVMXC__Installed_Product__c>  assetList = new List<SVMXC__Installed_Product__c>();
     assetList.add(installedProductVar1);
     assetList.add(installedProductVar2);
     insert assetList;
     System.debug('asset list is'+assetList);
     System.debug('asset list is'+assetList);
     
     Case c1 = new Case();
     //c1.RecordTypeId = caseRecordTypeId; 
     c1.ContactId = con.Id;
     c1.AccountId = con.AccountId;
     c1.Region_Comm__c = 'NONE';
     c1.Type = 'Authorized Upgrade';
     c1.Status = 'On Hold'; 
     c1.Subject = 'Test';
     c1.Asset_Installed_Product__r = assetList[0];
     
     insert c1;
     
     List<Case_Asset__c> caseAssetConnectList = [select Installed_Product__c from Case_Asset__c ];
     System.debug('caseAssetConnectList is'+caseAssetConnectList);
     System.assertEquals(c1.Asset_Installed_Product__c, caseAssetConnectList[0].Installed_Product__c);
     
     c1.Asset_Installed_Product__c = null;
     update c1;
     List<Case_Asset__c> caseAssetConnectList1 = [select Installed_Product__c from Case_Asset__c];
     System.assertEquals(null, caseAssetConnectList1[0].Installed_Product__c, 'Asset value of Case and related list are not equals');
     
     
   }*/
   
   
}