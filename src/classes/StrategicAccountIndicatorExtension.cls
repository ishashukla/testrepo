// (c) 2015 Appirio, Inc.
//
// Class Name: StrategicAccountIndicatorExtension
// Description: Contoller Class for StrategicAccountIndicatorPage. Class assigns a value to a boolean variable to show on page. If boolean variable is true than
//              checkbox is checked else unchecked.
// July 01 2016, Prakarsh Jain  Original (T-516847/S-419408)
//
public class StrategicAccountIndicatorExtension {
  public String accId;
  public Boolean isStrategic{get;set;}
  public List<Strategic_Account__c> listOfStrategicAccount;
  //Constructor
  public StrategicAccountIndicatorExtension(ApexPages.StandardController controller){
    isStrategic = false;
    listOfStrategicAccount = new List<Strategic_Account__c>();
    Account account = (Account)controller.getRecord();
    accId = account.id;
  }
  //Assigns true or false to the boolean variable
  public void hasStrategic(){
    for(Strategic_Account__c sa:[SELECT Id FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: UserInfo.getUserId()]){
        listOfStrategicAccount.add(sa);
    }
    //listOfStrategicAccount = [SELECT Id FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: UserInfo.getUserId()];
    if(listOfStrategicAccount.size()>=1){
      isStrategic = true;
    }
    else{
      isStrategic = false;
    }
  }
}