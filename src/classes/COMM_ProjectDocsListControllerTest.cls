// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_ProjectDocsListControllerTest - Test Class for COMM_ProjectDocsListController
//
// 03/09/2016, Deepti Maheshwari (T-458596)
// 03/21/2016, Deepti Maheshwari (T-484584)
//

@isTest
private class COMM_ProjectDocsListControllerTest {
  static testMethod void testBeforeInsert(){
  	
  	List < Account > accountList = TestUtils.createAccount(1, true);
    List < Contact > contactList = TestUtils.createContact(1, accountList[0].id, true);
    //TestUtils.createCommConstantSetting();
    PriceBook2 priceBook = TestUtils.createPriceBook(1,'test',false).get(0);
    priceBook.Name = 'COMM Price Book';
    insert priceBook;
    List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, false);
    Opportunity oppty = oppList.get(0);
  	oppty.RecordTypeID = 
  	Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.COMM_OPPTY_RTYPE1).getRecordTypeID();
  	insert oppty;
  	List<Document__c> opportunityDocs = TestUtils.createOpportunityDocument(5,accountList.get(0).Id, 
  	                                                          oppty.Id, true);
  	List<Project_Plan__c> projects = TestUtils.createProjectPlan(1, accountList.get(0).id, true);
  	oppty.Project_Plan__c = projects.get(0).id;
  	update oppty;
  	test.startTest(); 
  	
  	PageReference curentPage= Page.COMM_ProjectDocListPage;
    Test.setCurrentPage(curentPage);
    ApexPages.currentPage().getParameters().put('Id', projects.get(0).Id);
    
    ApexPages.StandardController cntrl = new ApexPages.StandardController(new Project_Plan__c());
    COMM_ProjectDocsListController obj = new COMM_ProjectDocsListController(cntrl);
    
  	test.stopTest(); 
  }
}