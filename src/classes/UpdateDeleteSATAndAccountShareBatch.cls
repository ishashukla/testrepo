/**================================================================      
 * Appirio, Inc
 * Name: UpdateDeleteSATAndAccountShareBatch 
 * Description: Batch class to Update/Delete AccountShare & AccountTeamMember 
 * records based on the data in Custom_Account_Team__c object
 * Created Date: 18 Aug,2016
 * Created By: Prakarsh Jain (Appirio)
 * 
 * Date Modified      Modified By      Description of the update
 * 18 Aug 2016        Prakarsh Jain         Class creation.T-528109
 ==================================================================*/
global class UpdateDeleteSATAndAccountShareBatch implements Database.Batchable<sObject>{
  public static Id accountInstrumentsRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type').getRecordTypeId();
  global Database.QueryLocator start(Database.BatchableContext BC){
    String query;
    query = 'Select UserId, TeamMemberRole, IsDeleted, Id, AccountId, AccountAccessLevel From AccountTeamMember';
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext BC,List<Sobject> scope){
    Set<Id> accountIds = new Set<Id>();
    Set<Id> userIds = new Set<Id>();
    Set<Id> accountIdsToConsider = new Set<Id>();
    Map<String, Custom_Account_Team__c> mapAccIdUserIdToCustomAccTeam = new Map<String, Custom_Account_Team__c>();
    Map<String,AccountShare> accUserRoletoShareRecord = new Map<String,AccountShare>();
    Map<String,AccountTeamMember> accUserRoletoAccountTeam = new Map<String,AccountTeamMember>();
    List<AccountTeamMember> listOfAccountTeamMemberToUpdate = new List<AccountTeamMember>();
    List<AccountShare> listOfAccountShareToUpdate = new List<AccountShare>();
    List<AccountTeamMember> listOfAccountTeamMemberToDelete = new List<AccountTeamMember>();
    List<AccountShare> listOfAccountShareToBeDeleted = new List<AccountShare>();
    for(Sobject s : scope){
      AccountTeamMember accountTeamMember = (AccountTeamMember)s;
      accountIds.add(accountTeamMember.AccountId);
      userIds.add(accountTeamMember.UserId);
    }
    system.debug('accountIds>>>'+accountIds); 
    system.debug('userIds>>>'+userIds);
    for(Account account : [SELECT Id, RecordTypeId, Name FROM Account WHERE Id IN: accountIds]){
      if(account.RecordTypeId == accountInstrumentsRecTypeId){
        accountIdsToConsider.add(account.Id);
      }
    }
    system.debug('accountIdsToConsider>>>'+accountIdsToConsider);
    //List<Custom_Account_Team__c> listOfCustomAccountTeam = [SELECT User__c, Team_Member_Role__c, Name, IsDeleted__c, Id, External_Integration_Id__c, Effective_From_Date__c, Division__c, Account__c, Account_Access_Level__c 
    //                                                        FROM Custom_Account_Team__c WHERE Account__c IN: accountIds];
                                                            
    for(Custom_Account_Team__c customAccountTeam : [SELECT User__c, Team_Member_Role__c, Name, IsDeleted__c, Id, External_Integration_Id__c, Effective_From_Date__c, Division__c, Account__c, Account_Access_Level__c 
                                      FROM Custom_Account_Team__c WHERE Account__c IN: accountIdsToConsider AND User__c IN: userIds]){
      String accTeamkey = customAccountTeam.Account__c+'+'+customAccountTeam.User__c+'+'+customAccountTeam.Team_Member_Role__c;
      mapAccIdUserIdToCustomAccTeam.put(accTeamkey, customAccountTeam);
      
    } 
    system.debug('mapAccIdUserIdToCustomAccTeam>>>'+mapAccIdUserIdToCustomAccTeam);  
    for(AccountTeamMember accMember : [SELECT UserId, TeamMemberRole, Id, AccountId, AccountAccessLevel 
                                       FROM AccountTeamMember 
                                       WHERE AccountId IN: accountIdsToConsider
                                       AND UserId IN: userIds]){
      AccUserRoletoAccountTeam.put(accMember.AccountId+'+'+accMember.UserId+'+'+accMember.TeamMemberRole,accMember);
    }
    system.debug('AccUserRoletoAccountTeam>>>'+AccUserRoletoAccountTeam);
    for(AccountShare accShare : [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel 
                                 FROM AccountShare
                                 WHERE UserOrGroupId IN:userIds
                                 AND AccountId IN: accountIdsToConsider
                                 AND (RowCause =:Schema.AccountShare.RowCause.Team
                                 OR RowCause =:Schema.AccountShare.RowCause.Manual)]){
      AccUserRoletoShareRecord.put(accShare.AccountId+'+'+accShare.UserOrGroupId,accShare);
    }
    system.debug('AccUserRoletoShareRecord>>>'+AccUserRoletoShareRecord);
    for(Sobject s : scope){
      AccountTeamMember accountTeamMember = (AccountTeamMember)s;
      String accTeamkey = accountTeamMember.AccountId+'+'+accountTeamMember.UserId+'+'+accountTeamMember.TeamMemberRole;
      String accSharekey = accountTeamMember.AccountId+'+'+accountTeamMember.UserId;
      if(mapAccIdUserIdToCustomAccTeam.containsKey(accTeamkey)){
          if(AccUserRoletoShareRecord.containsKey(accSharekey)){
                AccountShare accShare = AccUserRoletoShareRecord.get(accSharekey);
                accShare.AccountAccessLevel = 'Edit';
                //accountTeamMember.AccountAccessLevel = 'Edit';
                listOfAccountShareToUpdate.add(accShare);
          }
      }
      else{
        listOfAccountTeamMemberToDelete.add(accountTeamMember);
        if(AccUserRoletoShareRecord.containsKey(accSharekey)){
            listOfAccountShareToBeDeleted.add(AccUserRoletoShareRecord.get(accSharekey));
        }
      }
      
    }    
    system.debug('listOfAccountShareToUpdate>>>'+listOfAccountShareToUpdate);
    system.debug('listOfAccountTeamMemberToDelete>>>'+listOfAccountTeamMemberToDelete);
    system.debug('listOfAccountShareToBeDeleted>>>'+listOfAccountShareToBeDeleted);
    if(listOfAccountShareToUpdate.size()>0){
      Database.update(listOfAccountShareToUpdate, false);
    }        
    
    if(listOfAccountTeamMemberToDelete.size()>0){
      Database.delete(listOfAccountTeamMemberToDelete, false);
    }                                        
    
    if(listOfAccountShareToBeDeleted.size()>0){
      Database.delete(listOfAccountShareToBeDeleted, false);
    }
  }
  global void finish(Database.BatchableContext BC){
  
  }
}