/**================================================================      
* Appirio, Inc
* Name: PMWorkOrderTemplateControllerTest
* Description: Test class for PMWorkOrderTemplateController
* Created Date: 22 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class PMWorkOrderTemplateControllerTest {
    private static testMethod void test() {
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Account acc = new Account(Name = 'Test');
        insert acc;
        Id devRecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Preventive Maintenance').getRecordTypeId();
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(SVMXC__Company__c = acc.Id,Due_Date__c=Date.today(),
                                                                 RecordTypeId = devRecordTypeId,SVMXC__Order_Status__c = 'Open');
        insert wo;
        Test.startTest();
        PMWorkOrderTemplateController cntrl = new PMWorkOrderTemplateController();
        cntrl.AccountId = acc.Id;
        List<SVMXC__Service_Order__c> orders = cntrl.RelatedWo;
        Test.stopTest();
        System.assertEquals(orders != null, true);
    }
    
}