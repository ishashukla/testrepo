/**================================================================      
 * Appirio, Inc
 * Name: ValueAddActivitiesTriggerHandler
 * Description: Handler class for ValueAddActivitiesTrigger(T-538445), added Method shareASRUser()
 * Created Date: [09/20/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 * Oct 12, 2016       Shreerath Nair   Implemented new Trigger framework(T-542899)
 * Nov 04, 2016       Prakarsh Jain    Added method updateBusinessUnitValue(I-241684)
 * Nov 08, 2016       Prakarsh Jain    Added Try-Catch Block(I-242917)
 * Nov 10, 2016       Prakarsh Jain    Added method populateAccountForContactOppValAddRecords(T-554454)
 ==================================================================*/
public class ValueAddActivitiesTriggerHandler { 
  
  public static Id oppInstrumentsRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type').getRecordTypeId();
  public static Id oppInstrumentsClosedWonRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Closed Won Opportunity').getRecordTypeId();
  public static Id conInstrumentsRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact').getRecordTypeId();
    
  /**================================================================      
  * Standard Methods from the ValueAddActivitiesTriggerHandler 
  ==================================================================*/  
  
  public static boolean IsActive(){
      return TriggerState.isActive('ValueAddActivitiesTrigger');
      
  }
  
  public void OnBeforeInsert(List<Value_Add_Activities__c> newList){
      //Call to Method which restricts user to save Value Add Activity records for incorrect BU.
      updateBusinessUnitValue(newList);
      //Call to Method populateAccountForContactOppValAddRecords()
      populateAccountForContactOppValAddRecords(newList);
  }
  public void OnAfterInsert(List<Value_Add_Activities__c> newList){
      //Call to Method which shares the Value Add Activity record with the ASR User
      shareASRUser(newList,null);    
  }
    
  public void OnBeforeUpdate(List<Value_Add_Activities__c> newList, Map<ID, Value_Add_Activities__c> oldMap){
      //Call to Method populateAccountForContactOppValAddRecords()
      populateAccountForContactOppValAddRecords(newList);
  }
  public void OnAfterUpdate(List<Value_Add_Activities__c> newList, Map<ID, Value_Add_Activities__c> oldMap){
      //Call to Method which shares the Value Add Activity record with the ASR User
      shareASRUser(newList,oldMap);
      
  }
    
  public void OnBeforeDelete(List<Value_Add_Activities__c> oldList, Map<ID, Value_Add_Activities__c> oldMap){}
  /**================================================================      
  * End of Standard Methods from the ValueAddActivitiesTriggerHandler 
  ==================================================================*/
  
  /**================================================================      
  * Trigger Call Methods from the ValueAddActivitiesTrigger
  ==================================================================*/
  
  public void beforeInsert(List<Value_Add_Activities__c> newList){
      // Org Wide functionality
      OnBeforeInsert(newList);
  }
    
  public void afterInsert(List<Value_Add_Activities__c> newList){
      // Org Wide functionality
      OnAfterInsert(newList);
  }
    
  public void beforeUpdate(List<Value_Add_Activities__c> newList, Map<ID, Value_Add_Activities__c> oldMap){
      // Org Wide functionality
      OnBeforeUpdate(newList, oldMap);
  }
    
  public void afterUpdate(List<Value_Add_Activities__c> newList, Map<ID, Value_Add_Activities__c> oldMap){
      // Org Wide functionality
      OnAfterUpdate(newList, oldMap);
  }
    
  public void beforeDelete(List<Value_Add_Activities__c> oldList, Map<ID, Value_Add_Activities__c> oldMap){
      // Org Wide functionality
      OnbeforeDelete(oldList, oldMap);

  }
  /**================================================================      
  * End of Trigger Call Methods from the ValueAddActivitiesTrigger
  ==================================================================*/
  
  
  //Method adds ASR User to sharing record when a sales rep creates a Value_Add_Activities__c record. The ASR user is the user who is related
  //to the sales rep from mancode and view as relationship record.
  public static void shareASRUser(List<Value_Add_Activities__c> newList, Map<Id, Value_Add_Activities__c> oldMap){
    try{
      Set<Id> ownerIdSet = new Set<Id>();
        Set<Id> ASRUserIdSet = new Set<Id>();
        List<Value_Add_Activities__Share> valShareRecordsToBeShared = new List<Value_Add_Activities__Share>();
        List<ASR_Relationship__c> viewAsRelationshipList = new List<ASR_Relationship__c>();
        Map<Id,Id> ownerIdToASRUserIdMap = new Map<Id,Id>();
        for(Value_Add_Activities__c value : newList){
          if(oldMap == null || (oldMap != null && oldMap.get(value.Id).OwnerId != value.OwnerId)){
            ownerIdSet.add(value.OwnerId);
          }
        }
        if(ownerIdSet.size()>0){
          for(ASR_Relationship__c asrUsers : [SELECT Name, Mancode__c, ASR_User__c, Mancode__r.Sales_Rep__c, Mancode__r.Type__c 
                                              FROM ASR_Relationship__c 
                                              WHERE ASR_User__c IN: ownerIdSet AND Mancode__r.Type__c = 'ASR']){
            ASRUserIdSet.add(asrUsers.Mancode__r.Sales_Rep__c);
            ownerIdToASRUserIdMap.put(asrUsers.ASR_User__c, asrUsers.Mancode__r.Sales_Rep__c);
          }
        }
        for(Value_Add_Activities__c val : newList){
          if(ownerIdToASRUserIdMap.containsKey(val.OwnerId)){
            Value_Add_Activities__Share valShare = new Value_Add_Activities__Share(UserOrGroupId = ownerIdToASRUserIdMap.get(val.OwnerId), 
            RowCause = Schema.Value_Add_Activities__Share.RowCause.Manual, ParentId = val.Id, AccessLevel = 'Edit');
            valShareRecordsToBeShared.add(valShare);
          }
        }
        if(valShareRecordsToBeShared.size()>0){
          insert valShareRecordsToBeShared;
        }
    }catch(Exception ex){
      ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'ValueAddActivitiesTriggerHandler', true);
    }
  }
  
  //Method to restrict user to save Value Add Activities record if the sales rep user does not to belongs to that business unit in the mancode record
  public static void updateBusinessUnitValue(List<Value_Add_Activities__c> newList){
    try{
      Set<Id> ownerIdSet = new Set<Id>();
	    List<Mancode__c> mancodeList = new List<Mancode__c>();
	    Map<Id,List<String>> salesRepIdToBUMap = new Map<Id,List<String>>();
	    for(Value_Add_Activities__c value : newList){
	      //if(value.Business_Unit__c == null && value.Business_Unit__c == ''){
	        ownerIdSet.add(value.OwnerId);
	      //}
	    }
	    if(ownerIdSet.size()>0){
	      mancodeList = [SELECT Id, Sales_Rep__c, Business_Unit__c FROM Mancode__c WHERE Sales_Rep__c IN: ownerIdSet];
	    }
	    if(mancodeList.size()>0){
	      for(Mancode__c mancode : mancodeList){
	        if(salesRepIdToBUMap.containsKey(mancode.Sales_Rep__c)){
	          salesRepIdToBUMap.get(mancode.Sales_Rep__c).add(mancode.Business_Unit__c);    
	        } 
	        else{
	          salesRepIdToBUMap.put(mancode.Sales_Rep__c,new List<String>{mancode.Business_Unit__c});
	        }
	      }
	    }
	    for(Value_Add_Activities__c value : newList){
	      if(salesRepIdToBUMap.containsKey(value.OwnerId)){
	        List<String> tempList = salesRepIdToBUMap.get(value.OwnerId);
	        Set<String> tempSet = new Set<String>();
	        tempSet.addAll(tempList);
	        //if(tempList.size() == 1){
	        //  value.Business_Unit__c = tempList.get(0);
	        //}
	        if(!tempSet.contains(value.Business_Unit__c)){
	          value.addError('Please select a correct Business Unit');
	        }
	      }
	    }
    }catch(Exception ex){ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'ValueAddActivitiesTriggerHandler', true);}
  }
  
  //Nov 10, 2016  Prakarsh Jain   Method to populate Account on the Value Add Activity record if the value add activity is related to Contact
  //                              or Opportunity  
  public static void populateAccountForContactOppValAddRecords(List<Value_Add_Activities__c> newList){
    try{
      Set<Id> contactIdSet = new Set<Id>();
	    Set<Id> opportunityIdSet = new Set<Id>();
	    Map<Id,Id> contactIdToAccountIdMap = new Map<Id,Id>();
	    Map<Id,Id> opportunityIdToAccountIdMap = new Map<Id,Id>();
	    for(Value_Add_Activities__c value : newList){
	      if(value.Contact__c != null){
	        contactIdSet.add(value.Contact__c);
	      }
	      else if(value.Opportunity__c != null){
	        opportunityIdSet.add(value.Opportunity__c);
	      }
	    }
	    if(contactIdSet.size()>0){
	      for(Contact contact : [SELECT Id, AccountId FROM Contact 
	                             WHERE Id 
	                             IN: contactIdSet AND RecordTypeId =: conInstrumentsRecTypeId]){
	        if(!contactIdToAccountIdMap.containsKey(contact.Id)){
	          contactIdToAccountIdMap.put(contact.Id, contact.AccountId);
	        }
	      }
	    }
	    if(opportunityIdSet.size()>0){
	      for(Opportunity opportunity : [SELECT Id, AccountId FROM Opportunity 
	                                     WHERE Id 
	                                     IN: opportunityIdSet AND 
	                                     (RecordTypeId =: oppInstrumentsRecTypeId OR RecordTypeId =: oppInstrumentsClosedWonRecTypeId)]){
	        if(!opportunityIdToAccountIdMap.containsKey(opportunity.Id)){
	          opportunityIdToAccountIdMap.put(opportunity.Id, opportunity.AccountId);
	        }
	      }
	    }
	    for(Value_Add_Activities__c value : newList){
	      if(contactIdToAccountIdMap.containsKey(value.Contact__c)){
	        value.Account__c = contactIdToAccountIdMap.get(value.Contact__c);
	      }
	      else if(opportunityIdToAccountIdMap.containsKey(value.Opportunity__c)){
	        value.Account__c = opportunityIdToAccountIdMap.get(value.Opportunity__c);
	      }
	    }
    }catch(Exception ex){ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'ValueAddActivitiesTriggerHandler', true);}
  }
}