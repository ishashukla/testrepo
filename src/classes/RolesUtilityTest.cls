// (c) 2015 Appirio, Inc.
//
// Class Name: RolesUtilityTest 
// Description: Test Class for RolesUtility class.
// 
// April 14 2016, Isha Shukla  Original 
//
@isTest
public class RolesUtilityTest {
    static testMethod void testRolesUtility() {
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<UserRole> UserRoleList = new List<UserRole>();
        UserRole parentRole = createParentUserRole('Manager');
        insert parentRole;
        UserRole childRole = createChildUserRole('User');
        childRole.parentRoleId = parentRole.Id;
        insert childRole;
        List<User> managerList = new List<User>();
        User Manager1 = createUser('Manager1','Manager1','M1',pf.Id,parentRole.Id);
        User Manager2 = createUser('Manager2','Manager2','M2',pf.Id,parentRole.Id);
        managerList.add(Manager1);
        managerList.add(Manager2);
        insert managerList;
        List<User> userList = new List<User>();
        User User1 = createUser('User1','User1','U1',pf.Id,childRole.Id);
        User User2 = createUser('User2','User2','U2',pf.Id,childRole.Id);
        User User3 = createUser('User3','User3','U3',pf.Id,childRole.Id);
        userList.add(User1);
        userList.add(User2);
        userList.add(User3);
        insert userList;
        
        System.debug('All Managers'+managerList);
        System.debug('All Users'+userList);
        System.debug('Manager1 Reporting Users'+RolesUtility.getManagertoUsersMap(new Set<Id>{managerList.get(0).Id}));
        System.debug('Manager2 Reporting Users'+RolesUtility.getManagertoUsersMap(new Set<Id>{managerList.get(1).Id}));
        System.debug('User 1 Manager'+RolesUtility.getUsertoManagerMap(new Set<Id>{userList.get(0).Id}));
        RolesUtility.getManagerId(User1.Id);
        RolesUtility.getManagerId(new Set<Id>{userList[0].Id});
        RolesUtility.getRoleSubordinateUsers(User1.Id);
        System.assertEquals(2,RolesUtility.getManagerId(User1.Id).size());
        System.assertEquals(2,RolesUtility.getManagerId(new Set<Id>{userList[0].Id}).size());
        System.assertEquals(3,RolesUtility.getRoleSubordinateUsers(Manager1.Id).size());
    }
    
    public static UserRole createParentUserRole(String name){
        UserRole role = new UserRole(Name = name);
        return role;
    }
    public static UserRole createChildUserRole(String name){
        UserRole role = new UserRole(Name = name);
        return role;
    }
    public static User createUser(String fName,String lName,String uniqueName,Id profileId,Id roleId){
        User tuser = new User(firstname = fName,
                              lastName = lName,
                              email = uniqueName + '@test' + 'testOrg' + '.org',
                              Username = uniqueName + '@test' + 'testOrg' + '.org',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = (fName+lName).substring(0,8),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = profileId,
                              UserRoleId = roleId);
        return tuser;
    }
}