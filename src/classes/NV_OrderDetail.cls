//
// (c) 2015 Appirio, Inc.
//
// Apex Class Name: NV_OrderDetail
// For Aura Definition Bundles: nv_OrderDetailInfo, nv_OrderView
//
// 22nd December 2015   Hemendra Singh Bhati    Modified (Task # T-458996) - Please see the task description for more details.
// 24th December 2015   Hemendra Singh Bhati    Modified (Task # T-458968) - Please see the task description for more details.
// 7th 	October 2016	Padmesh Soni(Appirio Offshore)	Modified (S-443533) - Please see the task description for more details.
//    
public class NV_OrderDetail {
    @AuraEnabled
    public Id OrderId {get;set;}

    @AuraEnabled
    public String AccountName {get;set;}

    @AuraEnabled
    public String AccountNumber {get;set;}

    @AuraEnabled
    public String OrderType {get;set;}

    @AuraEnabled
    public String OrderRecordType {get;set;}

    @AuraEnabled
    public Datetime OrderDate {get;set;}

    @AuraEnabled
    public Decimal TotalAmount {get;set;}
    
    @AuraEnabled
    public Decimal InvoiceAmount {get;set;}

    @AuraEnabled
    public String PONumber {get;set;}

    @AuraEnabled
    public String Status {get;set;}

    @AuraEnabled
    public String StatusClass {get;set;}

    @AuraEnabled
    public String OrderNumber {get;set;}

    @AuraEnabled
    public Id AccountId {get;set;}

    @AuraEnabled
    public Integer ShippedItemCount {get;set;}

    @AuraEnabled
    public List<NV_OrderItemsWithTrackingDetail> NotShippedOrderItems {get;set;}

    @AuraEnabled
    public List<NV_OrderItemsWithTrackingDetail> ShippedOrderItems {get;set;}

    @AuraEnabled
    public List<NV_OrderInvoiceDetails> OrderInvoices { get; set; }

    @AuraEnabled
    public List<String> OrderInvoicesNumbers { get; set; }

    @AuraEnabled
    public String invoiceRelatedSalesOrder { get; private set; }
    
    @AuraEnabled
    public List<NV_ContactDetail> Contacts { get;set;}

    @AuraEnabled
    public Boolean AccountFollowed {get;set;}

    @AuraEnabled
    public Boolean OrderFollowed {get;set;}

    @AuraEnabled
    public String BillingAddress {get;set;}

    @AuraEnabled
    public String ShippingAddress {get;set;}

    @AuraEnabled
    public Integer DaysPendingPO {get;set;}

    @AuraEnabled
    public Integer DaysSalesOutstanding {get;set;}

    @AuraEnabled
    public Datetime InvoiceDate {get;set;}
    @AuraEnabled
    public Datetime InvoiceDueDate {get;set;}
    @AuraEnabled
    public String InvoiceNumber {get;set;}

    @AuraEnabled
    public String ShippedTitle {get;set;}
    @AuraEnabled
    public String NotShippedTitle {get;set;}
    @AuraEnabled
    public String HoldType {get;set;}

    @AuraEnabled
    public Boolean Expedited {get;set;}
    
    @AuraEnabled
    public Boolean OnHold {get;set;}
    
    @AuraEnabled
    public List<String> TrackingNumbers {get;set;}

    @AuraEnabled
    public String oracleOrderNumber { get; private set; }
    
    /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - Start **/
    //Constants
    public final String ORDER_TYPE_NV_RECALL = 'NV Recall';
    public final String ORDER_ITEM_STATUS_RETURNED = 'Returned';
    /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - End **/
		
    public NV_OrderDetail(Order orderRecord) {
            system.debug('TRACE: NV_OrderDetail - NV_OrderDetail - orderRecord - ' + orderRecord);
            system.debug('TRACE: NV_OrderDetail - NV_OrderDetail - orderRecord.OrderItems - ' + orderRecord.OrderItems);
            system.debug('TRACE: NV_OrderDetail - NV_OrderDetail - orderRecord.Order_Invoices__r - ' + orderRecord.Order_Invoices__r);

        OrderId = orderRecord.Id;
        OrderNumber = orderRecord.Oracle_Order_Number__c;
        AccountId = orderRecord.AccountId;
        AccountName = orderRecord.Account.Name;
        if(!String.isBlank(orderRecord.Account.AccountNumber))
            AccountNumber = orderRecord.Account.AccountNumber.replace('NV-', '');
        OrderRecordType = orderRecord.RecordType.DeveloperName;
        HoldType = orderRecord.Hold_Type__c;
        OnHold = false;
        if(orderRecord.On_Hold__c){
            OnHold = true;
        } else if(orderRecord.Line_Hold_Count__c > 0){
            OnHold = true;
        }
        /*
        BillingAddress = NV_Utility.getString(orderRecord.BillingStreet) + ' '
                            + NV_Utility.getString(orderRecord.BillingCity, ', ')
                            + NV_Utility.getString(orderRecord.BillingState) + ' '
                            + NV_Utility.getString(orderRecord.BillingCountry)+ ' '
                            + NV_Utility.getString(orderRecord.BillingPostalCode) +'';
        
        ShippingAddress = NV_Utility.getString(orderRecord.ShippingStreet) + ' '
                            + NV_Utility.getString(orderRecord.ShippingCity, ', ')
                            + NV_Utility.getString(orderRecord.ShippingState) + ' '
                            + NV_Utility.getString(orderRecord.ShippingCountry)+ ' '
                            + NV_Utility.getString(orderRecord.ShippingPostalCode) +'';
        */
        /*BillingAddress = NV_Utility.getString(orderRecord.Bill_To_Location__r.BillingStreet) + ' '
                            + NV_Utility.getString(orderRecord.Bill_To_Location__r.BillingCity, ', ')
                            + NV_Utility.getString(orderRecord.Bill_To_Location__r.BillingState) + ' '
                            + NV_Utility.getString(orderRecord.Bill_To_Location__r.BillingCountry)+ ' '
                            + NV_Utility.getString(orderRecord.Bill_To_Location__r.BillingPostalCode) +'';
        ShippingAddress = NV_Utility.getString(orderRecord.Ship_To_Location__r.ShippingStreet) + ' '
                            + NV_Utility.getString(orderRecord.Ship_To_Location__r.ShippingCity, ', ')
                            + NV_Utility.getString(orderRecord.Ship_To_Location__r.ShippingState) + ' '
                            + NV_Utility.getString(orderRecord.Ship_To_Location__r.ShippingCountry)+ ' '
                            + NV_Utility.getString(orderRecord.Ship_To_Location__r.ShippingPostalCode) +'';*/

        BillingAddress = NV_Utility.getString(orderRecord.Oracle_Order_Billing_Address__c);
        ShippingAddress = NV_Utility.getString(orderRecord.Oracle_Order_Shipping_Address__c);

        DaysPendingPO = Integer.valueOf(orderRecord.Days_Pending_PO__c);
        DaysSalesOutstanding = 0;
        if(orderRecord.Days_Outstanding__c != null){
            DaysSalesOutstanding = Integer.valueOf(orderRecord.Days_Outstanding__c);
        }
        InvoiceDueDate = orderRecord.Invoice_Due_Date__c;
        InvoiceDate = orderRecord.Invoice_Date__c;
        InvoiceNumber = orderRecord.Invoice_Number__c;
        InvoiceAmount = orderRecord.Invoice_Subtotal__c;
        
        TotalAmount = orderRecord.Order_Total__c;
        PONumber = orderRecord.Customer_PO__c;
        Status = orderRecord.Status;

        OrderType = orderRecord.Type;
        //OrderType = 'Sales';
        ShippedTitle = 'Shipped';
        NotShippedTitle = 'Not Shipped';
        if(OrderRecordType == 'NV_Bill_Only'){
            //OrderType = 'Bill Only';
            ShippedTitle = 'Closed Line Items';
            NotShippedTitle = 'Order Line Items';
        } else if(OrderRecordType == 'NV_RMA'){
            //OrderType = 'RMA';
            
	    /** Code commented - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - Start **/
            //ShippedTitle = 'Returned Items';
            //NotShippedTitle = 'Awaiting Return';
            /** Code commented - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - Start **/

            if(TotalAmount >= 0){
                TotalAmount *= -1;
            }
        } else if(OrderRecordType == 'NV_Credit'){
            //OrderType = 'Credit';
            ShippedTitle = 'Credit Memos';
            NotShippedTitle = 'Credit Memos';
        }

        StatusClass = 'default';
        if(Status == 'Shipped' || Status == 'Delivered' || Status == 'Closed'){
            StatusClass = 'shipped';
        } else if(Status == 'Partial Ship'){
            StatusClass = 'partial';
        } else if(Status == 'Partial Shipping' || Status == 'Backordered'){
            StatusClass = 'hold';
        }
        if(OnHold){
            StatusClass = 'hold';
        }

        //OrderDate = orderRecord.EffectiveDate;
        OrderDate = orderRecord.Date_Ordered__c;
        if(OrderDate == null){
            OrderDate = orderRecord.EffectiveDate;
        }
        
        Boolean afterCutoff = false;
        if(OrderDate.hour() > 16 || (OrderDate.hour() == 16 && OrderDate.minute() >= 30)){//Order came in after 7:30PM EST
            afterCutoff = true;
        }
        //Set to actual date for displaying:
        OrderDate = orderRecord.EffectiveDate;
        Contacts = new List<NV_ContactDetail>();
        
        NotShippedOrderItems = new List<NV_OrderItemsWithTrackingDetail>();
        NotShippedOrderItems.add(new NV_OrderItemsWithTrackingDetail());
        ShippedOrderItems = new List<NV_OrderItemsWithTrackingDetail>();
        ShippedItemCount = 0;

        AccountFollowed = false;
        OrderFollowed = false;

        // Temp Map for sorting details.
        Map<String, NV_OrderItemsWithTrackingDetail> tempSOItems = new Map<String, NV_OrderItemsWithTrackingDetail>();

        // Added For Task # T-458968.
        invoiceRelatedSalesOrder = '';
        if(String.isNotBlank(orderRecord.Invoice_Related_Sales_Order__c)) {
          invoiceRelatedSalesOrder = orderRecord.Invoice_Related_Sales_Order__c;
        }

        Boolean isExternalOrder = false;
        Order theExternalOrder = null;
        List<OrderItem> theOrderItems = orderRecord.OrderItems;
        Map<Id, List<Order_Invoice__c>> orderItemInvoicesMapping = new Map<Id, List<Order_Invoice__c>>();
        if(OrderRecordType.equalsIgnoreCase('NV_Credit')) {
          // Determining if the order is an external order.
          isExternalOrder = true;
          OrderNumber = '';
          oracleOrderNumber = '';

          // Determining the external order.
          for(Order theRecord : [SELECT Id, OrderNumber, Oracle_Order_Number__c, Customer_PO__c FROM Order
                                 WHERE Oracle_Order_Number__c = :invoiceRelatedSalesOrder ORDER BY CreatedDate DESC LIMIT 1]) {
            theExternalOrder = theRecord;
          }

          if(theExternalOrder != null) {
            OrderNumber = theExternalOrder.OrderNumber;
            PONumber = theExternalOrder.Customer_PO__c;
            oracleOrderNumber = theExternalOrder.Oracle_Order_Number__c;

            // Extracting order invoice records of the external order.
            theOrderItems = [SELECT Id FROM OrderItem WHERE OrderId = :theExternalOrder.Id];

            Set<Id> orderItemIds = new Set<Id>();
            for(OrderItem orderItemRecord : theOrderItems) {
              orderItemIds.add(orderItemRecord.Id);
              orderItemInvoicesMapping.put(orderItemRecord.Id, new List<Order_Invoice__c>());
            }

            if(orderItemIds.size() > 0) {
              for(Order_Invoice__c theRecord : [SELECT Id, Name, Order_Item__c, Invoice_Date__c, Invoice_Due_Date__c
                                                FROM Order_Invoice__c WHERE Order_Item__c IN :orderItemIds]) {
                if(!orderItemInvoicesMapping.containsKey(theRecord.Order_Item__c)) {
                  orderItemInvoicesMapping.put(theRecord.Order_Item__c, new List<Order_Invoice__c>());
                }
                orderItemInvoicesMapping.get(theRecord.Order_Item__c).add(theRecord);
              }
            }
          }
        }
		
		/** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - Start **/
		//Set to hold hardcoded values
        Set<String> notShippedRMAStatus = new Set<String>{'Entered','Booked','Awaiting Shipping','Backordered'};
		Set<String> shippedRMAStatus = new Set<String>{'Shipped' , 'Delivered', 'Closed'};
		Set<String> awaitingRMAStatus = new Set<String>{'Awaiting Return'};
		Set<String> returnedRMAStatus = new Set<String>{'Returned', 'Closed'};
		/** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - End **/
		
        for(OrderItem orderItemRecord : orderRecord.OrderItems){
            //if(String.isBlank(orderItemRecord.Tracking_Number__c)) {
            Set<String> expeditedShippingMethods = new Set<String>{'FDX-Parcel-Next Day 8AM','FDX-Parcel-P1 Overnight 10.30', 'FDX-Parcel-Saturday/8AM'};
            Set<String> returnShippedTypes = new Set<String>{'US NV Complaint Ship Only L', 'US NV Ship Only Line'};
            if(!String.isBlank(orderItemRecord.Shipping_Method__c) && expeditedShippingMethods.contains(orderItemRecord.Shipping_Method__c)){
                Expedited = true;
            }
            
            /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - Start **/
            //Check for Order Type for NV Recall and its status
            if(OrderRecordType == 'NV_RMA' && String.isNotBlank(orderItemRecord.Status__c) && orderItemRecord.Line_Type__c == 'US NV Ship Only Line') {
            	
            	if(String.isNotBlank(orderItemRecord.Status__c) && shippedRMAStatus.contains(orderItemRecord.Status__c)) {
            		
            		//incremented count
	    			ShippedItemCount++;
	                
	                //Hold tracking number
	                String tracking = orderItemRecord.Tracked_Number__c;
	                
	                //Check for blank
	                if(String.isBlank(tracking)){
	                    tracking = '';
	                }
	                
	                //Check for already contained key
	                if(!tempSOItems.containsKey(tracking)){
	                	
	                	//populate map
	                    tempSOItems.put(tracking, new NV_OrderItemsWithTrackingDetail(orderItemRecord.Tracked_Number__c, orderItemRecord.Tracking_Number__c, 
	                    																	orderItemRecord.Tracking_Information__c));
	                }
		                
	                //Adding element on map's existing key
	                tempSOItems.get(tracking).OrderItems.add(new NV_OrderItemDetail(orderItemRecord, OrderRecordType, null, theExternalOrder));
	                
	                //Incremented value by 1
	                tempSOItems.get(tracking).itemCount++;
	            } else if((String.isNotBlank(orderItemRecord.Status__c) && notShippedRMAStatus.contains(orderItemRecord.Status__c)) || orderItemRecord.On_Hold__c){
            		
            		NV_OrderItemDetail temp = new NV_OrderItemDetail(
	                  orderItemRecord,
	                  OrderRecordType,
	                  OrderRecordType.equalsIgnoreCase('NV_Credit') ? orderItemInvoicesMapping.get(orderItemRecord.Id) : null,
	                  theExternalOrder
	                );
	                temp.PastCutoff = afterCutoff;
	                if(returnShippedTypes.contains(orderItemRecord.Line_Type__c)){
	                    if(NotShippedOrderItems.size() == 1){
	                        NotShippedOrderItems.add(new NV_OrderItemsWithTrackingDetail());
	                    }
	                    NotShippedOrderItems[1].OrderItems.add(temp);
	                } else{
	                    NotShippedOrderItems[0].OrderItems.add(temp);
	                }
            	}
            } else if(OrderRecordType == 'NV_RMA' && String.isNotBlank(orderItemRecord.Status__c) && orderItemRecord.Line_Type__c == 'US NV Cust Recall Line' 
		&& returnedRMAStatus.contains(orderItemRecord.Status__c)) {
				
				ShippedTitle = 'Returned Items';
	            NotShippedTitle = 'Awaiting Return';
	            
				//incremented count
    			ShippedItemCount++;
                
                //Hold tracking number
                String tracking = orderItemRecord.Tracked_Number__c;
                
                //Check for blank
                if(String.isBlank(tracking)){
                    tracking = '';
                }
                
                //Check for already contained key
                if(!tempSOItems.containsKey(tracking)){
                	
                	//populate map
                    tempSOItems.put(tracking, new NV_OrderItemsWithTrackingDetail(orderItemRecord.Tracked_Number__c, orderItemRecord.Tracking_Number__c, 
                    																	orderItemRecord.Tracking_Information__c));
                }
	                
                //Adding element on map's existing key
                tempSOItems.get(tracking).OrderItems.add(new NV_OrderItemDetail(orderItemRecord, OrderRecordType, null, theExternalOrder));
                
                //Incremented value by 1
                tempSOItems.get(tracking).itemCount++;
          	} else if(OrderRecordType == 'NV_RMA' && String.isNotBlank(orderItemRecord.Status__c) && orderItemRecord.Line_Type__c == 'US NV Cust Recall Line' 
			&& String.isNotBlank(orderItemRecord.Status__c) && (awaitingRMAStatus.contains(orderItemRecord.Status__c) 
          		|| (orderItemRecord.Status__c == 'Booked' && orderItemRecord.On_Hold__c))) {
        		
        		ShippedTitle = 'Returned Items';
	            NotShippedTitle = 'Awaiting Return';
	            
        		NV_OrderItemDetail temp = new NV_OrderItemDetail(
                  orderItemRecord,
                  OrderRecordType,
                  null,
                  theExternalOrder
                );
                temp.PastCutoff = afterCutoff;
                if(returnShippedTypes.contains(orderItemRecord.Line_Type__c)){
                    if(NotShippedOrderItems.size() == 1){
                        NotShippedOrderItems.add(new NV_OrderItemsWithTrackingDetail());
                    }
                    NotShippedOrderItems[1].OrderItems.add(temp);
                } else{
                    NotShippedOrderItems[0].OrderItems.add(temp);
                }
        	} else /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - End **/
            
            if(orderItemRecord.Status__c != 'Delivered' && orderItemRecord.Status__c != 'Shipped' && orderItemRecord.Status__c != 'Closed' && orderItemRecord.Status__c != 'Returned'){
                NV_OrderItemDetail temp = new NV_OrderItemDetail(
                  orderItemRecord,
                  OrderRecordType,
                  OrderRecordType.equalsIgnoreCase('NV_Credit') ? orderItemInvoicesMapping.get(orderItemRecord.Id) : null,
                  theExternalOrder
                );
                temp.PastCutoff = afterCutoff;
                if(returnShippedTypes.contains(orderItemRecord.Line_Type__c)){
                    if(NotShippedOrderItems.size() == 1){
                        NotShippedOrderItems.add(new NV_OrderItemsWithTrackingDetail());
                    }
                    NotShippedOrderItems[1].OrderItems.add(temp);
                } else{
                    NotShippedOrderItems[0].OrderItems.add(temp);
                }
            }
            else{
                ShippedItemCount++;
                //Logic for S-371704 : Replacing Tracking Number with Tracked Number
                String tracking = orderItemRecord.Tracked_Number__c;
                if(String.isBlank(tracking)){
                    tracking = '';
                }
                if(!tempSOItems.containsKey(tracking)){
                    tempSOItems.put(tracking,
                        new NV_OrderItemsWithTrackingDetail(orderItemRecord.Tracked_Number__c, orderItemRecord.Tracking_Number__c, orderItemRecord.Tracking_Information__c));
                }
                tempSOItems.get(tracking).OrderItems.add(new NV_OrderItemDetail(
                  orderItemRecord,
                  OrderRecordType,
                  OrderRecordType.equalsIgnoreCase('NV_Credit') ? orderItemInvoicesMapping.get(orderItemRecord.Id) : null,
                  theExternalOrder
                 ));
                tempSOItems.get(tracking).itemCount++;
            }            
        }

        if(tempSOItems.size()>0){
            for(String tNo : tempSOItems.keySet()){
                ShippedOrderItems.add(tempSOItems.get(tNo));
            }
        }

        TrackingNumbers = new List<String>();
        if(!String.isBlank(orderRecord.Tracking_Number__c)){
            TrackingNumbers = orderRecord.Tracking_Number__c.split(',');
        }

            // Binding Order Invoice Data With Order Instance.
            Integer counter = 1;
            OrderInvoices = new List<NV_OrderInvoiceDetails>();
      OrderInvoicesNumbers = new List<String>();
      List<Order_Invoice__c> theOrderInvoices = orderRecord.Order_Invoices__r;

      if(isExternalOrder) {
        theOrderInvoices = new List<Order_Invoice__c>();

        if(theExternalOrder != null) {
          theOrderInvoices = [SELECT Order__c, Id, Name, Transaction_Status__c, Invoice_Date__c, Invoice_Due_Date__c, Sales_Order_Number__c,
                              Oracle_Invoice_Number__c, No_of_Days_Outstanding__c, Display_Days_Sales_Outstanding__c FROM Order_Invoice__c WHERE
                              Order__c = :theExternalOrder.Id ORDER BY CreatedDate DESC];
        }
      }

      for(Order_Invoice__c record : theOrderInvoices) {
              OrderInvoicesNumbers.add(record.Oracle_Invoice_Number__c);
                OrderInvoices.add(new NV_OrderInvoiceDetails(record, OrderRecordType, counter));
                counter++;
            }
            system.debug('TRACE: NV_OrderDetail - NV_OrderDetail - OrderInvoices - ' + OrderInvoices);
    }

    public class NV_OrderItemDetail{
        @AuraEnabled
        public Id OrderItemId {get;set;}

        @AuraEnabled
        public Id OrderId {get;set;}

        @AuraEnabled
        public String Description {get;set;}

        @AuraEnabled
        public Decimal Quantity {get;set;}

        @AuraEnabled
        public Decimal UnitPrice {get;set;}
        
        @AuraEnabled
        public Decimal TotalPrice {get;set;}

        @AuraEnabled
        public String HoldDescription {get;set;}
        
        @AuraEnabled
        public String ReturnReason {get;set;}

        @AuraEnabled
        public Id ProductId {get;set;}

        @AuraEnabled
        public String ProductCatalogNumber {get;set;}

        @AuraEnabled
        public Date ExpectedShipDate {get;set;}

        @AuraEnabled
        public String FedExTrackingNumber{get;set;}

        @AuraEnabled
        public String Status {get;set;}

        @AuraEnabled
        public String TrackingStatus {get;set;}

        @AuraEnabled
        public Datetime LastShipAction {get;set;}
        
        //Start - Added by Jyoti for Story S-389798
        @AuraEnabled
        public Datetime DeliveredDate {get;set;}
        
        @AuraEnabled
        public Datetime ShippedDate {get;set;}
        //End - Story S-389798
        
        @AuraEnabled
        public Boolean PastCutoff {get;set;}
        
        @AuraEnabled
        public Boolean Expedited {get;set;}
        
        @AuraEnabled
        public Boolean OnHold {get;set;}
        
        // Added For Task # T-458968.
        @AuraEnabled public String creditReason { get; set; }
        @AuraEnabled public String orderNumber { get; set; }
        @AuraEnabled public String oracleOrderNumber { get; set; }
        @AuraEnabled public String poNumber { get; set; }
        @AuraEnabled public Decimal lineAmount { get; set; }
        @AuraEnabled public List<String> invoiceNumbers { get; set; }

        public NV_OrderItemDetail(OrderItem orderItemRecord, String OrderRecordType, List<Order_Invoice__c> orderItemInvoices, Order externalOrder) {
            OrderItemId = orderItemRecord.Id;
            OrderId = orderItemRecord.OrderId;
            ProductId = orderItemRecord.PricebookEntry.Product2Id;
            ProductCatalogNumber = orderItemRecord.PricebookEntry.Product2.ProductCode;
            Description = orderItemRecord.Description;
            Quantity = orderItemRecord.Quantity;
            UnitPrice = orderItemRecord.UnitPrice;
            TotalPrice = orderItemRecord.Sub_Total__c;
            if(OrderRecordType == 'NV_RMA'){
                if(UnitPrice >= 0){
                    UnitPrice *= -1;
                }
                if(TotalPrice >= 0){
                    TotalPrice *= -1;
                }
            }
            Set<String> expeditedShippingMethods = new Set<String>{'FDX-Parcel-Next Day 8AM','FDX-Parcel-P1 Overnight 10.30', 'FDX-Parcel-Saturday/8AM'};
            if(!String.isBlank(orderItemRecord.Shipping_Method__c) && expeditedShippingMethods.contains(orderItemRecord.Shipping_Method__c)){
                Expedited = true;
            }
            OnHold = orderItemRecord.On_Hold__c;
            HoldDescription = orderItemRecord.Hold_Description__c;
            ReturnReason = orderItemRecord.Return_Reason__c;
            //ExpectedShipDate = orderItemRecord.Scheduled_Ship_Date__c;
            FedExTrackingNumber = orderItemRecord.Tracking_Number__c;
            TrackingStatus = orderItemRecord.Tracking_Information__c;
            Status = orderItemRecord.Status__c;
            if(orderItemRecord.Status__c == 'Delivered')
                LastShipAction = orderItemRecord.Delivered_Date__c;                
            else if(orderItemRecord.Status__c == 'Shipped' || orderItemRecord.Status__c == 'Closed')
                LastShipAction = orderItemRecord.Shipped_Date__c;
            //Start - Added by Jyoti For Story S-389798     
            if(orderItemRecord.Status__c == 'Delivered' || orderItemRecord.Status__c == 'Shipped' || orderItemRecord.Status__c == 'Closed'){
                DeliveredDate =  orderItemRecord.Delivered_Date__c;    
                ShippedDate = orderItemRecord.Shipped_Date__c; 
            }   
            //End - Story S-389798
            // Added For Task # T-458968.
            creditReason = orderItemRecord.Credit_Reason_Description__c;
            lineAmount = orderItemRecord.Line_Amount__c;

            if(externalOrder != null) {
              orderNumber = externalOrder.OrderNumber;
              oracleOrderNumber = externalOrder.Oracle_Order_Number__c;
              poNumber = externalOrder.Customer_PO__c;
            }

            invoiceNumbers = new List<String>();
            if(orderItemInvoices != null) {
              for(Order_Invoice__c theRecord : orderItemInvoices) {
                invoiceNumbers.add(theRecord.Name);
              }
            }
        }
    }

    public class NV_OrderItemsWithTrackingDetail{

        @AuraEnabled
        public String FedExTrackedNumber{get;set;}

        @AuraEnabled
        public String FedExTrackingNumber{get;set;}

        @AuraEnabled
        public String TrackingStatus {get;set;}

        @AuraEnabled
        public List<NV_OrderItemDetail> OrderItems {get;set;}

        @AuraEnabled
        public Integer itemCount {get;set;}

        public NV_OrderItemsWithTrackingDetail(){
            this.FedExTrackingNumber = '';
            this.FedExTrackedNumber = '';
            this.TrackingStatus = '';
            this.OrderItems = new List<NV_OrderItemDetail>();
            this.itemCount = 0;
        }

        public NV_OrderItemsWithTrackingDetail(String FedExTrackedNumber, String FedExTrackingNumber, String TrackingStatus){
            this.FedExTrackedNumber = FedExTrackedNumber;
            this.FedExTrackingNumber = FedExTrackingNumber;
            this.TrackingStatus = TrackingStatus;
            this.OrderItems = new List<NV_OrderItemDetail>();
            this.itemCount = 0;
        }
    }

  /**
  * Apex Inner Class Name: NV_OrderInvoiceDetails
  * Description: This class is used to encapsulate order invoice data.
  * 
  *
  * @author   Hemendra Singh Bhati (JDC) Appirio Inc.
  * @version  1.0
  * @since    2015-11-22 
  */
  public class NV_OrderInvoiceDetails {
    // Public Data Members.
    @AuraEnabled public Id theOrderId { get; set; }
    @AuraEnabled public Id theOrderInvoiceId { get; set; }
    @AuraEnabled public String theTransactionStatus { get; set; }
    @AuraEnabled public Date theInvoiceDate { get; set; }
    @AuraEnabled public Date theInvoiceDueDate { get; set; }
    @AuraEnabled public String theSalesOrderNumber { get; set; }
    @AuraEnabled public Decimal theTotalOutstandingDays { get; set; }
        @AuraEnabled public Boolean displayDaysSalesOutstanding { get; set; }
        @AuraEnabled public Integer theRecordNumber { get; set; }

    // The Class Default Constructor.
    public NV_OrderInvoiceDetails(Order_Invoice__c theOrderInvoice, String theOrderRecordTypeName, Integer theRowNumber) {
      theOrderId = theOrderInvoice.Order__c;
      theOrderInvoiceId = theOrderInvoice.Id;
      theTransactionStatus = theOrderInvoice.Transaction_Status__c;
      theInvoiceDate = theOrderInvoice.Invoice_Date__c;
      theInvoiceDueDate = theOrderInvoice.Invoice_Due_Date__c;
      theSalesOrderNumber = theOrderInvoice.Oracle_Invoice_Number__c;
      theTotalOutstandingDays = theOrderInvoice.No_of_Days_Outstanding__c;
            displayDaysSalesOutstanding = theOrderInvoice.Display_Days_Sales_Outstanding__c;
            theRecordNumber = theRowNumber;
    }
  }

    //BJ - Broke NV_ContactDetail out so that it could have static methods.

    @AuraEnabled
    public static NV_OrderDetail getOrderDetailByOrderId(Id orderId){
        NV_OrderDetail orderDetail = null;
        List<Order> orderList = [SELECT Id, Name, Oracle_Order_Number__c, AccountId, Account.Name, Account.AccountNumber,
                                             RecordType.Name, RecordType.DeveloperName, Type, Customer_PO__c, Status, Tracking_Number__c,
                               // BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                               // ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
                               Ship_To_Location__r.ShippingStreet, Ship_To_Location__r.ShippingCity, Ship_To_Location__r.ShippingState,
                               Ship_To_Location__r.ShippingPostalCode, Ship_To_Location__r.ShippingCountry, Bill_To_Location__r.BillingStreet,
                               Bill_To_Location__r.BillingCity, Bill_To_Location__r.BillingState, Bill_To_Location__r.BillingPostalCode,
                               Bill_To_Location__r.BillingCountry, Days_Outstanding__c, Days_Pending_PO__c, Oracle_Order_Billing_Address__c,
                               Oracle_Order_Shipping_Address__c, Invoice_Subtotal__c, Invoice_Date__c, Invoice_Number__c, Invoice_Due_Date__c, Hold_Type__c, 
                               EffectiveDate, TotalAmount, Order_Total__c, Date_Ordered__c, On_Hold__c, Line_Hold_Count__c, Invoice_Related_Sales_Order__c,
                                     (SELECT Id, OrderItemNumber, OrderId, Scheduled_Ship_Date__c, Line_Type__c, Description, UnitPrice, Quantity,
                                     Sub_Total__c, Hold_Description__c, Status__c, PricebookEntryId, PricebookEntry.Product2Id,
                                     PricebookEntry.Product2.ProductCode, Tracked_Number__c, Tracking_Number__c, Tracking_Status__c,
                                     Tracking_Information__c, On_Hold__c, Shipped_Date__c, Delivered_Date__c, Return_Reason__c, Shipping_Method__c,
                               Credit_Reason_Description__c, Line_Amount__c, Order.OrderNumber, Order.Oracle_Order_Number__c, Order.PoNumber
                               FROM OrderItems 
                               /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - Start **/
                               //To handle data display
                               ORDER BY Line_Type__c DESC 
                               /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - End **/),
                               (SELECT Order__c, Id, Name, Transaction_Status__c, Invoice_Date__c, Invoice_Due_Date__c, Sales_Order_Number__c,
                               Oracle_Invoice_Number__c, No_of_Days_Outstanding__c, Display_Days_Sales_Outstanding__c FROM Order_Invoices__r
                               ORDER BY CreatedDate DESC) FROM Order WHERE Id = :orderId LIMIT 1];
        if(orderList.size()>0){
            orderDetail  = new NV_OrderDetail(orderList[0]);

            Set<Id> contactIds = new Set<Id>();
            //Collect all associated contacts
            for(Contact_Acct_Association__c cAssociation : [SELECT Id, Associated_Contact__c 
                                                                FROM Contact_Acct_Association__c 
                                                                WHERE Associated_Account__c =: orderList[0].AccountId
                                                                AND Associated_Contact__c != null]){

                contactIds.add(cAssociation.Associated_Contact__c);
            }

            for(Contact record: [SELECT Id, AccountId, Name, Title, Title__c, Email, Phone,
                                    MobilePhone, Contact_Type__c, Inactive__c, Is_Primary__c, Is_Visible_In_Detail__c
                                    FROM Contact
                                    WHERE Is_Visible_In_Detail__c = true
                                        AND Inactive__c = false
                                        AND (AccountId =: orderList[0].AccountId OR Id in: contactIds)
                                        ORDER BY Is_Primary__c DESC, NAME ASC ]){
                orderDetail.Contacts.add(new NV_ContactDetail(record));
            }



            List<NV_Follow__c> followRecords = [SELECT Id, Record_Id__c, Record_Type__c FROM NV_Follow__c 
                                                WHERE Following_User__c =: UserInfo.getUserId()
                                                AND Record_Id__c in (:orderList[0].Id, :orderList[0].AccountId)];
            if(followRecords.size()>0){
                for(NV_Follow__c follow : followRecords){
                    if(follow.Record_Type__c == 'Account'){
                        orderDetail.AccountFollowed = true;
                    }
                    else if(follow.Record_Type__c == 'Order'){
                        orderDetail.OrderFollowed = true;
                    }
                }
            } 
        }
        return orderDetail;
    }

    @AuraEnabled
    public static void changeFollowRecord(String recordIdS, Boolean toFollow){
        Id recordId = Id.valueOf(recordIdS);
        Id userId = UserInfo.getUserId();
        if(toFollow){
            NV_Follow__c record =new NV_Follow__c(Following_User__c = userId, 
                                                    Record_Id__c = recordId,
                                                    External_Id__c = userId+'_'+recordId);
            upsert record External_Id__c;
        }
        else{
            List<NV_Follow__c> followRecordsToDelete = [SELECT Id FROM NV_Follow__c 
                                                        WHERE Record_Id__c =: recordId
                                                        AND Following_User__c =: userId];
            if(followRecordsToDelete.size()>0){
                delete followRecordsToDelete;
            }
        }
    }
}