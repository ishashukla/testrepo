/**================================================================      
* Appirio, Inc
* Name: NV_ProductPricingImplTest.cls
* Description: Test class for the interface implementing class
* Created Date: 03-AUG-2016
* Created By: Ray Dehler (Appirio)
*
* Date Modified      Modified By      Description of the update
* 03 AUG 2016        Ray Dehler       Created class from deprecated old class and added support for Instruments S-423983
==================================================================*/
@isTest
private class NV_ProductPricingImplTest {
  // Private Data Members.
  private static final String SYSTEM_ADMINISTRATOR_PROFILE_NAME = 'System Administrator';
  private static final String REGIONAL_MANAGER_CENTRAL_ROLE_NAME = 'Regional Manager - Central';
  private static final String ORDER_NV_CREDIT_RECORD_TYPE_LABEL = 'NV Credit';

  /*
  @method      : validateControllerFunctionality
  @description : This method validates controller functionality.
  @params      : void
  @returns     : void
  */
  private static testMethod void validateControllerFunctionality() {
    // Extracting "System Administrator" Profile Id.
    List<Profile> theProfiles = [SELECT Id FROM Profile WHERE Name = :SYSTEM_ADMINISTRATOR_PROFILE_NAME];
    system.assert(
      theProfiles.size() > 0,
      'Error: The requested user profile does not exist.'
    );

    // Extracting "Regional Manager - Central" Role Id.
    List<UserRole> theUserRoles = [SELECT Id FROM UserRole WHERE Name = :REGIONAL_MANAGER_CENTRAL_ROLE_NAME];
    system.assert(
      theUserRoles.size() > 0,
      'Error: The requested user role does not exist.'
    );

    // Inserting Test User With "Regional Manager - Central" Role.
    User theCentralRegionalManager = new User(
      ProfileId = theProfiles.get(0).Id,
      UserRoleId = theUserRoles.get(0).Id,
	    Alias = 'hsb',
	    Email = 'hsingh@appirio.com',
	    EmailEncodingKey = 'UTF-8',
	    FirstName = 'Hemendra Singh',
	    LastName = 'Bhati',
	    LanguageLocaleKey = 'en_US',
	    LocaleSidKey = 'en_US',
	    TimezoneSidKey = 'Asia/Kolkata',
	    Username = 'hsingh@appirio.com.srtyker',
	    CommunityNickName = 'hsb',
	    IsActive = true
    );
    insert theCentralRegionalManager;

    // Generating Test Data.
    NV_Follow__c theOrderFollower = null;
    Account theTestAccount = null;
    Contract theTestContract = null;
    Pricebook2 theTestPriceBook = null;
    Product2 theTestProduct = null;
    PricebookEntry theTestPBE = null;
    PricebookEntry theStandardPBE = null;
    Id standardPriceBookId = null;

    Test.startTest();

    // Running Thread As Central Regional Manager.
    system.runAs(theCentralRegionalManager) {
      // Inserting Test NV Follow Record.
      theOrderFollower = NV_TestUtility.createNVFollow(
        UserInfo.getUserId(),
        'User',
        theCentralRegionalManager.Id,
        true
      );

      // Inserting Test Account.
      theTestAccount = new Account(
        Name = 'Test Account',
        AccountNumber = '9876543210',
        ShippingCity = 'PLATTSBURGH',
        ShippingState = 'New York',
        Purpose__c = 'Main',
        OwnerId = UserInfo.getUserId()
      );
      insert theTestAccount;

      // Test Case 1 - Validating Method Named "getMyCustomerAccounts".
      List<Account> theAccounts = NV_ProductPricingController.getMyCustomerAccounts();
      system.assert(
        theAccounts.size() > 0,
        'Error: The controller class failed to fetch account records for followed users.'
      );

      // Inserting Test Contract.
      theTestContract = new Contract(
        AccountId = theTestAccount.Id,
        Status = 'Draft',
        ContractTerm = 1,
        StartDate = system.today(),
        External_Integration_Id__c = '123123'
      );
      insert theTestContract;

      // Extracting Standard PriceBook Id.
      standardPriceBookId = Test.getStandardPricebookId();

      // Inserting Test Pricebook.
      theTestPriceBook = new Pricebook2(
        Name = 'NV Price Book',
        IsActive = true
      );
      insert theTestPriceBook;

      // Inserting Test Product.
      theTestProduct = new Product2(
        Name = 'Test Product',
        Catalog_Number__c = '1',
        Product_Division__c = 'NV',
        Segment__c = 'Test Segment',
          ODP_Category__c = 'Test Category',
        Life_Cycle_Code__c = 'Test Life Cycle Code',
        IsActive = true
      );
      insert theTestProduct;

      // Instantiating Standard Pricebook Entry.
      theStandardPBE = new PricebookEntry(
        Pricebook2Id = standardPriceBookId,
        Product2Id = theTestProduct.Id,
        UnitPrice = 99.00,
        UseStandardPrice = false,
        isActive = true
      );

      // Instantiating Test Pricebook Entry.
      theTestPBE = new PricebookEntry(
        Pricebook2Id = theTestPriceBook.Id,
        Product2Id = theTestProduct.Id,
        UnitPrice = 99.00,
        UseStandardPrice = false,
        isActive = true
      );

      // Inserting Test Pricebook Entries.
      insert new List<PricebookEntry> {
        theStandardPBE,
        theTestPBE
      };

      // Test Case 2 - Validating Method Named "getAllProducts".
      List<NV_Wrapper.ProductLineData> theProductLineData = NV_ProductPricingController.getAllProducts();
      System.debug('THE PRODUCT LINE DATA: ' + theProductLineData);
      system.assert(
        theProductLineData.size() > 0,
        'Error: The controller class failed to fetch product line data.'
      );

      // Inserting Custom Setting Data.
      insert new List<NV_Product_Pricing_Settings__c> {
        new NV_Product_Pricing_Settings__c(
          Name = 'ERPCode',
          Value__c = 'EMPR'
        ),
        new NV_Product_Pricing_Settings__c(
          Name = 'BusSegCode',
          Value__c = 'Test'
        ),
        new NV_Product_Pricing_Settings__c(
          Name = 'OrgUnitID',
          Value__c = 'NV_US'
        ),
        new NV_Product_Pricing_Settings__c(
          Name = 'ConfigName',
          Value__c = 'Global'
        ),
        new NV_Product_Pricing_Settings__c(
          Name = 'CurrencyCode',
          Value__c = 'USD'
        )
      };

      // Test Case 3 - Validating Methods Named "getFollowableUsers" And "retrievePricing".
      NV_ProductPricingController.getFollowableUsers();
      NV_ProductPricingController.retrievePricing(
        UserInfo.getUserId(),
        'Test,Test'
      );
      NV_ProductPricingController.isBigDataMode();
      try {
	    NV_ProductPricingController.getProductsForSegment('Test Segment', 'Test Category');
	    System.assert(false, 'Should not be able to run unimplimented method.');
	  } catch(Mobile_Utility.NotImplementedException e) {}
      try {
	    NV_ProductPricingController.searchForProducts('Test');
	    System.assert(false, 'Should not be able to run unimplimented method.');
	  } catch(Mobile_Utility.NotImplementedException e) {}
    }

    Test.stopTest();
  }

}