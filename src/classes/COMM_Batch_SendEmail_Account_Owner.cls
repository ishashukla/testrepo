/**================================================================      
* Appirio, Inc
* Name: [COMM_Batch_SendEmail_Account_Owner]
* Description: [batch Class for Sending email notification to Accoutn Owner]
* Created Date: [14-09-2016]
* Created By: [Gaurav Sinha] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 14 Sep 2016        Gaurav Sinha      T-509402
==================================================================*/
public class COMM_Batch_SendEmail_Account_Owner implements Database.Batchable<SObject>, Database.Stateful, Schedulable{
    
    public String query = ' select id,SVMXC__End_Date__c,SVMXC__Installed_Product__c, SVMXC__Installed_Product__r.SVMXC__Company__c,' +
        ' SVMXC__Installed_Product__r.SVMXC__Company__r.name,SVMXC__Installed_Product__r.name, ' + 
        ' Name,SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c,SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__c, SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__r.Name' +
        ' from SVMXC__Warranty__c where ( ( SVMXC__End_Date__c >= NEXT_N_DAYS:30 and SVMXC__End_Date__c = NEXT_N_DAYS:37 ) or '+
        ' ( SVMXC__End_Date__c >= NEXT_N_DAYS:90 and SVMXC__End_Date__c = NEXT_N_DAYS:97 ) '+
        ' or ( SVMXC__End_Date__c >= NEXT_N_DAYS:120 and SVMXC__End_Date__c = NEXT_N_DAYS:127 ) ) and SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__c != null ';
    
    public Map<String,List<SVMXC__Warranty__c>> accountOwnerGrouping = new Map<String,List<SVMXC__Warranty__c>>();
    public Map<String,List<SVMXC__Warranty__c>> accountGrouping = new Map<String,List<SVMXC__Warranty__c>>();
    /*
    * @method:  COMM_Batch_SendEmail_Account_Owner
    * @param : query : to reinitalize the query string
    * @Description: Constructor for reintialze the Query.
    */
    public COMM_Batch_SendEmail_Account_Owner(String query){
        this.query = query;
    }
    
    /*
    * @method:  COMM_Batch_SendEmail_Account_Owner
    * @param : query : to reinitalize the query string
    * @Description: Constructor for reintialze the Query.
    */
    public COMM_Batch_SendEmail_Account_Owner(){
    }
    /*
    * @method:  execute
    * @param : sc : SchedulableContext
    * @Description: Method implemented for the interface Schedulable. It calls the batch class for implementing the desired functionality.
    */
    public void execute(SchedulableContext sc) {
        
        if(!Test.isRunningTest()){
            Id batchId = Database.executeBatch(new COMM_Batch_SendEmail_Account_Owner(query), 2000);        
            System.debug('\n\n==> batchId = ' + batchId); 
        }
    }
    /*
    * @method:  finish
    * @param : context : Database.BatchableContext
    * @Description: Method implemented for the interface Schedulable
    */
    public void finish(Database.BatchableContext context) {
        if(accountOwnerGrouping.size() > 0){
            createEmail();
        }
    }
    /*
    * @method:  start
    * @param : context : Database.BatchableContext
    * @Description: Method implemented for the interface Batchable to get the records on which the batch processing needs to be applied.
    */
    public Database.QueryLocator start(Database.BatchableContext context){
        system.debug(query);
        return  Database.getQueryLocator(query);
    }
    /*
    * @method:  execute
    * @param : context : Database.BatchableContext : Batch Context
    * @param : objects : List<SVMXC__Warranty__c> : List of obejct returned in the batch context.
    * @Description: Method implemented for the interface Batchable to apply the processing of copying the records from Opportunity_Period__c to Opportunity_Forecast_Snapshot__c
    */
    public void execute(Database.BatchableContext context, List<SVMXC__Warranty__c> objects){
        for(SVMXC__Warranty__c varloop: objects){
            if(accountGrouping.get(varloop.SVMXC__Installed_Product__r.SVMXC__Company__c)==null){
                accountGrouping.put(varloop.SVMXC__Installed_Product__r.SVMXC__Company__c,new list<SVMXC__Warranty__c>{varloop});
            }
            else{
                accountGrouping.get(varloop.SVMXC__Installed_Product__r.SVMXC__Company__c).add(varloop);
            }
            if(accountOwnerGrouping.get(varloop.SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__c)==null){    
                accountOwnerGrouping.put(varloop.SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__c,new list<SVMXC__Warranty__c>{varloop});
            }
            else{
                //accountOwnerGrouping.get(varloop.SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__c).add(varloop);
            }
        }
    }
    
    /*
    * @method:  createEmail
    * @Description: Method implemented to genrate the email for the error Records
    */    
    private void createEmail(){        
        // First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        if(!test.isRunningTest())
            Messaging.reserveSingleEmailCapacity(2);
        
        // Processes and actions involved in the Apex transaction occur next,
        // which conclude with sending a single email.
        
        // Now create a new single email message object
        // that will send out a single email to the addresses in the To, CC & BCC list.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<Messaging.SingleEmailMessage> limail = New list<Messaging.SingleEmailMessage>();
        String htmlBody = '';
        String plainBody='';
        System.debug('accountOwnergrouping.keyset() ==> '+accountOwnergrouping.keyset());
        System.debug('accountOwnergrouping.size() ==> '+accountOwnergrouping.size());
         String baseURL = URL.getSalesforceBaseUrl().toExternalForm(); // it will return: < https://cs14.salesforce.com >
        String PageURL = page.COMM_RenewWarranties.getUrl()+'?id=';   // it will return: < /apex/myVFpage?id=906F00000008w9wIAA >

        for(String varloop : accountOwnergrouping.keyset()){
            mail = new Messaging.SingleEmailMessage();
            htmlBody = '';
            plainBody='';
            mail.setsaveAsActivity(false);
            mail.setTargetObjectId(varloop);
            // Specify the name used as the display name.
            mail.setSenderDisplayName('Salesforce Support');
            // Specify the subject line for your email address.
            mail.setSubject(System.label.Warranty_Expiration_Subject);
            // Set to True if you want to BCC yourself on the email.
            mail.setBccSender(false);
            //mail.setCcAddresses(new List<String>{'gsinha@appirio.com','grv.sinha@gmail.com'});
            // Optionally append the salesforce.com email signature to the email.
            // The email address of the user executing the Apex Code will be used.
            mail.setUseSignature(false);
            htmlBody = 'Hi '+accountOwnergrouping.get(varloop)[0].SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__r.name +',';
            htmlBody += '<br/> '+ System.label.Warranty_Expiration_Body + '<br/>';
            //htmlBody += 'Email to <b>'+accountOwnergrouping.get(varloop)[0].SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__r.name+'</b> <br/>';
            plainBody = 'Email to '+accountOwnergrouping.get(varloop)[0].SVMXC__Installed_Product__r.SVMXC__Company__r.COMM_Sales_Rep__r.name;
            htmlBody+= '<ul>';
            for(SVMXC__Warranty__c var : accountOwnergrouping.get(varloop)){                
                htmlBody+= '<li>' + var.SVMXC__Installed_Product__r.SVMXC__Company__r.name ;
                htmlBody+= '<ul>';
                for(SVMXC__Warranty__c varacc : accountgrouping.get(var.SVMXC__Installed_Product__r.SVMXC__Company__c)){
                    htmlBody+= '<li> Warranty Name : '+varacc.name;
                    htmlBody+= '<ul>';
                    htmlBody+= '<li>Asset :'+varacc.SVMXC__Installed_Product__r.name + '</li>';
                    htmlBody+= '<li>Serial Number :'+( varacc.SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c!=null ?varacc.SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c:'') + '</li>';
                    htmlBody+= '<li>End Date :'+varacc.SVMXC__End_Date__c.format() + '</li>';
                    htmlBody+= '<li>Renew opportunity :<a href="'+baseUrl + pageUrl + var.SVMXC__Installed_Product__r.SVMXC__Company__c + '">Link</a></li>';
                    htmlBody+= '</ul>';
                    htmlBody+= '</li>';
                }
                htmlBody+= '</ul>';
                htmlBody+= '</li>';
            }
            htmlBody+= '</ul>';
            mail.sethtmlBody(htmlBody);
            limail.add(mail);
        }
        
        // Specify the text content of the email.
        // mail.setPlainTextBody(plainBody);
        
        // Specify the html content of the email.
        
        // Send the email you have created.
        if(!test.isRunningTest())
            Messaging.sendEmail( limail );
    }
}