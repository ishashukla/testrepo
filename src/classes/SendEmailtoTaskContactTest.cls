/**================================================================      
 * Appirio, Inc
 * Name: SendEmailtoTaskContactTest
 * Description: Test Class for SendEmailtoTaskContact
 * Created Date: 18/08/2016
 * Created By: Pratibha Chhimpa (Appirio)
 * 
*******************************************************************************/
@isTest
private class SendEmailtoTaskContactTest{

  static testMethod void testEmailtoContact()
  {
      Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Internal Contact');
      List<Account> listAccount  = Endo_TestUtils.createAccount(1,true);
      List<Contact> listContact  = Endo_TestUtils.createContact(1,listAccount[0].id,false);
      listContact[0].RecordTypeId = rtByName.getRecordTypeId();
      insert listContact ;
      
      List<Task> tasks = new List<Task>();
      tasks.add(new Task(
      ActivityDate = Date.today().addDays(7),
      Subject='Sample Task',
      WhatId = listAccount[0].Id,
      Salesperson_to_Email__c = listContact[0].Id,
      OwnerId = UserInfo.getUserId(),
      Status='In Progress'));
      insert tasks; 
      
      Test.startTest();
      SendEmailtoTaskContact sendtestEmail = new SendEmailtoTaskContact();
      SendEmailtoTaskContact.sendEmail(tasks[0].id);
      Test.stopTest();
  }
}