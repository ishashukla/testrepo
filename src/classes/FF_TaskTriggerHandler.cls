/*************************************************************************************************
Created By:    Sunil Gupta
Date:          June 16, 2014
Description:   Handler class for Task Trigger
**************************************************************************************************/
public class FF_TaskTriggerHandler {
    //-----------------------------------------------------------------------------------------------
  //  Set Task Owner to RFM
  //-----------------------------------------------------------------------------------------------
  public static void setTaskOwner(List<Task> lstNew){

    // Task related to Flex Contracts.
    Set<Id> setContractIds = new Set<Id>();
    String contractPrefix = Flex_Contract__c.sObjectType.getDescribe().getKeyPrefix();
    System.debug('@@@' + contractPrefix);

    // Consider only Task created for Flex Contract Object.
    for(Task objTask: lstNew){
    	if(objTask.WhatId != null){
	      if(String.valueOf(objTask.WhatId).subString(0,3) == contractPrefix){
	        System.debug('@@@' + objTask.Id);
	        setContractIds.add(objTask.WhatId);
	      }
    	}
    }

    if(setContractIds.size() < 1){
    	return ;
    }

    // Fetch Related Opportunities Regions.
    Map<Id, Flex_Contract__c> mapContracts = new Map<Id, Flex_Contract__c>([SELECT Id, Opportunity__r.Flex_Region__c
                                              FROM Flex_Contract__c WHERE Id IN :setContractIds]);
    System.debug('@@@' + mapContracts);

    // Sales Rep Map based on Regions
    Map<String, String> mapSalesReps = new Map<String, String>();
    for(Flex_Sales_Rep__c objSalesRep :Flex_Sales_Rep__c.getall().values()){
         mapSalesReps.put(objSalesRep.Name, objSalesRep.RFM_Username__c);
    }
    System.debug('@@@' + mapSalesReps);

    // Fetch users
    Map<String, User> mapUsers = new Map<String, User>();
    for(User objUser :[SELECT Id, Username FROM User WHERE Username IN :mapSalesReps.values()]){
    	mapUsers.put(objUser.UserName, objUser);
    }
    System.debug('@@@' + mapUsers);


    // Update Owner of Task
    for(Task objTask: lstNew){
        if(setContractIds.contains(objTask.WhatId) == true){
            String region;
            System.debug('@@@' + objTask.WhatId);
            if(mapContracts.get(objTask.Whatid) != null){
              region = mapContracts.get(objTask.Whatid).Opportunity__r.Flex_Region__c;
              System.debug('@@@' + region);
            }
            if(region != null){
                if(mapSalesReps.get(region) != null){
                    String userName = mapSalesReps.get(region);
                    System.debug('@@@' + userName);
                    if(mapUsers.get(userName) != null){
                    	objTask.OwnerId = mapUsers.get(userName).Id;
                    }

                }
            }
        }
    }
  }
}