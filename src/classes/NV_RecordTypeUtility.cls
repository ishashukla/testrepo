public with sharing class NV_RecordTypeUtility {
	public static String getIdQuery = 'SELECT Id, Name, DeveloperName, Description, ' + 
										'BusinessProcessId, SobjectType, IsActive ' + 
										'FROM RecordType ' + 
										'WHERE IsActive = true ' + 
										'AND SobjectType =: objectName ' + 
										'AND Name =: recordTypeName'; 

	public static String getIdDNQuery = 'SELECT Id, Name, DeveloperName, Description, ' + 
										'BusinessProcessId, SobjectType, IsActive ' + 
										'FROM RecordType ' + 
										'WHERE IsActive = true ' + 
										'AND SobjectType =: objectName ' + 
										'AND DeveloperName =: recordTypeName'; 

	public static String getNameQuery= 'SELECT Id, Name, DeveloperName, Description, ' + 
										'BusinessProcessId, SobjectType, IsActive ' + 
										'FROM RecordType ' + 
										'WHERE IsActive = true ' + 
										'AND SobjectType =: objectName ' + 
										'AND Id =: recordTypeId'; 

	public static String getForObject= 'SELECT Id, Name, DeveloperName, Description, ' + 
										'BusinessProcessId, SobjectType, IsActive ' + 
										'FROM RecordType ' + 
										'WHERE IsActive = true ' + 
										'AND SobjectType =: objectName '; 

	public static Id getId(String objectName, String recordTypeName) { 
		Id recordTypeId; 
		List<SObject> r = Database.query(getIdQuery); 
		if(!r.isEmpty()) { recordTypeId = r[0].Id; } 
		return recordTypeId; 
	} 

	public static Id getIdDN(String objectName, String recordTypeName) { 
		Id recordTypeId; 
		List<SObject> r = Database.query(getIdDNQuery); 
		if(!r.isEmpty()) { recordTypeId = r[0].Id; } 
		return recordTypeId; 
	} 

	public static String getName(String objectName, String recordTypeId) { 
		String recordTypeName; 
		List<SObject> rt = Database.query(getNameQuery); 
		for(SObject r : rt) { 
			recordTypeName = String.valueOf(r.get('Name')); 
		} 
		return recordTypeName; 
	} 

	public static Map<String, Id> getRecordTypeIds(String objectName) { 
		Map<String, Id> t = new Map<String, Id>(); 
		List<SObject> rt = Database.query(getForObject); 
		for(SObject r : rt) { 
			t.put(String.valueOf(r.get('Name')), r.Id); 
		}
		return t; 
	} 

	public static Map<String, Id> getDeveloperNameRecordTypeIds(String objectName) { 
		Map<String, Id> t = new Map<String, Id>(); 
		List<SObject> rt = Database.query(getForObject); 
		for(SObject r : rt) { 
			t.put(String.valueOf(r.get('DeveloperName')), r.Id); 
		} 
		return t; 
	} 

	public static Map<Id, String> getRecordTypeNames(String objectName) { 
		Map<Id, String> t = new Map<Id, String>(); 
		List<SObject> rt = Database.query(getForObject); 
		for(SObject r : rt) { 
			t.put(r.Id, String.valueOf(r.get('Name'))); 
		}
		return t; 
	} 
}