/**================================================================      
* Appirio, Inc
* Name: INS_ProductPricingImplTest.cls
* Description: Test class for the interface implementing class
* Created Date: 03-AUG-2016
* Created By: Ray Dehler (Appirio)
*
* Date Modified      Modified By      Description of the update
* 03 AUG 2016        Ray Dehler       Created class similar to NV_ProductPricingImplTest S-423983
==================================================================*/
@isTest
private class INS_ProductPricingImplTest {
  private static final String SYSTEM_ADMINISTRATOR_PROFILE_NAME = 'System Administrator';

  private static testMethod void validateControllerFunctionality() {
    // Extracting "System Administrator" Profile Id.
    List<Profile> theProfiles = [SELECT Id FROM Profile WHERE Name = :SYSTEM_ADMINISTRATOR_PROFILE_NAME];
    system.assert(
      theProfiles.size() > 0,
      'Error: The requested user profile does not exist.'
    );

    // Inserting Test User
    User sysAdminuser = new User(
      ProfileId = theProfiles.get(0).Id,
      Alias = 'rtsf23',
      Email = 'raytest@appirio.com',
      EmailEncodingKey = 'UTF-8',
      FirstName = 'Ray',
      LastName = 'Test',
      LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US',
      TimezoneSidKey = 'Asia/Kolkata',
      Username = 'raytest234@stryker.432.com',
      CommunityNickName = 'rtsf23', 
      Division = 'Test Div',
      IsActive = true
    );
    insert sysAdminuser;

    // Generating Test Data.
    Account theTestAccount = null;
    AccountTeamMember atm = null;
    Pricebook2 theTestPriceBook = null;
    Product2 theTestProduct = null;
    PricebookEntry theTestPBE = null;
    PricebookEntry theStandardPBE = null;
    Id standardPriceBookId = null;

    Test.startTest();

    // Running Thread As Central Regional Manager.
    system.runAs(sysAdminuser) {
      // Inserting Test Account.
      theTestAccount = new Account(
        Name = 'Test Account',
        AccountNumber = '9876543210-0',
        ShippingCity = 'PLATTSBURGH',
        ShippingState = 'New York'
      );
      insert theTestAccount;

      atm = new AccountTeamMember(
        AccountId = theTestAccount.Id,
        UserId = sysAdminuser.Id,
        TeamMemberRole = 'Does not matter'
      );
      insert atm;

      Product_Pricing_Settings__c settings = Product_Pricing_Settings__c.getInstance();
      settings.Controller_Class__c = 'INS_ProductPricingImpl';
      insert settings;

      // Test Case 1 - Validating Method Named "getMyCustomerAccounts".
      List<Account> theAccounts = NV_ProductPricingController.getMyCustomerAccounts();
      system.assert(
        theAccounts.size() > 0,
        'Error: The controller class failed to fetch account records.'
      );

      // Extracting Standard PriceBook Id.
      standardPriceBookId = Test.getStandardPricebookId();

      // Inserting Test Pricebook.
      theTestPriceBook = new Pricebook2(
        Name = 'Test Div Price Book',
        IsActive = true
      );
      insert theTestPriceBook;

      // Inserting Test Product.
      theTestProduct = new Product2(
        Name = 'Test Product',
        Segment__c = 'Test Segment',
        ODP_Category__c = 'Test Category',
        IsActive = true
      );
      insert theTestProduct;

      // Instantiating Standard Pricebook Entry.
      theStandardPBE = new PricebookEntry(
        Pricebook2Id = standardPriceBookId,
        Product2Id = theTestProduct.Id,
        UnitPrice = 99.00,
        UseStandardPrice = false,
        isActive = true
      );

      // Instantiating Test Pricebook Entry.
      theTestPBE = new PricebookEntry(
        Pricebook2Id = theTestPriceBook.Id,
        Product2Id = theTestProduct.Id,
        UnitPrice = 99.00,
        UseStandardPrice = false,
        isActive = true
      );

      // Inserting Test Pricebook Entries.
      insert new List<PricebookEntry> {
        theStandardPBE,
        theTestPBE
      };

      // Test Case 2 - Validating Method Named "getAllProducts".
      List<NV_Wrapper.ProductLineData> theProductLineData = NV_ProductPricingController.getAllProducts();
      System.debug('THE PRODUCT LINE DATA: ' + theProductLineData);
      system.assert(
        theProductLineData.size() > 0,
        'Error: The controller class failed to fetch product line data.'
      );


      // Inserting Custom Setting Data.
      insert new List<Instruments_Product_Pricing_Settings__c> {
        new Instruments_Product_Pricing_Settings__c(
          Name = 'ERPCode',
          Value__c = 'EMPR'
        ),
        new Instruments_Product_Pricing_Settings__c(
          Name = 'BusSegCode',
          Value__c = 'Test'
        ),
        new Instruments_Product_Pricing_Settings__c(
          Name = 'OrgUnitID',
          Value__c = 'NV_US'
        ),
        new Instruments_Product_Pricing_Settings__c(
          Name = 'ConfigName',
          Value__c = 'Global'
        ),
        new Instruments_Product_Pricing_Settings__c(
          Name = 'CurrencyCode',
          Value__c = 'USD'
        )
      };

      List<User> users = NV_ProductPricingController.getFollowableUsers();
      System.assert(users != null);
      NV_ProductPricingController.retrievePricing(
        UserInfo.getUserId(),
        'Test,Test'
      );
      NV_ProductPricingController.isBigDataMode();
      NV_ProductPricingController.getProductsForSegment('Test Segment', 'Test Category');
      NV_ProductPricingController.getProductsForSegment('', '');
      NV_ProductPricingController.searchForProducts('Test');
      NV_ProductPricingController.searchForProducts('');
    }

    Test.stopTest();
  }
}