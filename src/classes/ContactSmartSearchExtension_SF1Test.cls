@isTest
public without sharing class ContactSmartSearchExtension_SF1Test {
	static Contact nContact;
	static Account acc;
	
	public static testMethod void testContactSmartSearch() {
    createTestData();
    test.startTest();
	Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.FirstName = 'test';
    con.LastName = 'test';
    con.Email = 'test@test.com';
    con.Phone = '9812345566';
    insert con;
    PageReference pageRef = Page.ContactSmartSearch_NewContact_SF1;
	Test.setCurrentPage(pageRef);
	ApexPages.currentPage().getParameters().put('firstName',con.FirstName);
	ApexPages.currentPage().getParameters().put('lastName',con.LastName);
	ApexPages.currentPage().getParameters().put('email','testclass@appirio.com');
	ApexPages.currentPage().getParameters().put('phone',con.Phone);
	ApexPages.currentPage().getParameters().put('accId', acc.Id);
	ContactSmartSearchExtension_SF1 css = new ContactSmartSearchExtension_SF1(new ApexPages.StandardController(con));
	css.save();
	css.getRecordName();
	test.stopTest();
	}
    
    public static void createTestData() {
    	acc = TestUtils.createAccount(1, true).get(0);
    	
    }
}