/**================================================================      
 * Appirio, Inc
 * Name: PrivateNotesControllerTest 
 * Description: Test Class for PrivateNotesController class.
 * Created Date: [04/13/2016]
 * Created By: [Isha Shukla] (Appirio)
 * Date Modified      Modified By      Description of the update
 * Apr 12,2016       Isha Shukla       Original
 * Jun 13,2016       Isha Shukla       Modified(T-510753)
 ==================================================================*/
@IsTest
private class PrivateNotesControllerTest {
    Static List<Account> accountList;
    Static List<Contact> contactList;
    Static List<Opportunity> oppList;
    Static List<Notes__c> noteList;
    Static List<User> user;
    Static List<RecordType> recordtypeListforNotes = new List<RecordType>([SELECT SobjectType,IsActive,Id,DeveloperName FROM RecordType 
                                                                           WHERE SobjectType = 'Notes__c' 
                                                                           AND DeveloperName = 'Notes']);
    Static List<RecordType> recordtypeListforPreNotes = new List<RecordType>([SELECT SobjectType,IsActive,Id,DeveloperName FROM RecordType 
                                                                              WHERE SobjectType = 'Notes__c' 
                                                                              AND DeveloperName = 'Preference_Cards']);
    // Testing when record type is 'Notes', isNotes Parameter is True, Id of notes is passed and relatedId parameter is not passed
    @isTest static void testFirstCase() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforNotes[0].Id;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id',String.valueOf(noteList[0].Id));
            System.currentPageReference().getParameters().put('isNotes','true');
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            controller.save();
            controller.cancel();
            controller.makeRecordPublic();
            controller.makeRecordPrivate();
            controller.postChatter();
            controller.deleteRecord();
            Test.stopTest();
            System.assertEquals(true,recordtypeListforNotes != Null);
            System.assertEquals(true,controller.notes != Null);
        }
    }
    
    @isTest static void testMkPrivate() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforNotes[0].Id;
            noteList[0].Private__c = true;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id',String.valueOf(noteList[0].Id));
            System.currentPageReference().getParameters().put('isNotes','true');
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            controller.save();
            controller.cancel();
            controller.makeRecordPublic();
            controller.makeRecordPrivate();
            controller.postChatter();
            controller.deleteRecord();
            Test.stopTest();
            System.assertEquals(true,recordtypeListforNotes != Null);
            System.assertEquals(true,controller.notes != Null);
        }
    }
    
    @isTest static void testDelete() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforPreNotes[0].Id;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id',String.valueOf(noteList[0].Id));
            System.currentPageReference().getParameters().put('isNotes','false');
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            controller.save();
            controller.cancel();
            controller.makeRecordPublic();
            controller.makeRecordPrivate();
            controller.postChatter();
            controller.deleteRecord();
            Test.stopTest();
            System.assertEquals(true,recordtypeListforNotes != Null);
            System.assertEquals(true,controller.notes != Null);
        }
    }
    
    // Testing when notes id is passed, isNotes parameter is set to true, relatedId parameter is not passed and recordtype of notes is Preference Cards
    @isTest static void testSecondCase() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforPreNotes[0].Id;
            noteList[0].Account__c = Null;
            noteList[0].Opportunity__c = Null;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id',String.valueOf(noteList[0].Id));
            System.currentPageReference().getParameters().put('isNotes','true');
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            Test.stopTest();
            System.assertEquals(true,recordtypeListforPreNotes != Null);
            System.assertEquals(true,controller.notes != Null);
        }
    }
    // Testing when notes id is not passed, relatedId parameter has account id, isNotes parameter is set to false and recordtype for notes is Preference Cards
    @isTest static void testThirdCase() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforPreNotes[0].Id;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('relatedId',String.valueOf(accountList[1].Id));
            System.currentPageReference().getParameters().put('isNotes','false');
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            Test.stopTest();
            System.assertEquals(true,recordtypeListforPreNotes != Null);
            System.assertEquals(true,ApexPages.currentPage().getParameters().get('id')== Null);
        }
    }
    // Testing scenario when isNotes parameter is set to false, relatedId parameter has contact id, notes id is not passed.
    @isTest static void testFourthCase() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforNotes[0].Id;
            noteList[0].Account__c = Null;
            noteList[0].Contact__c = Null;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('isNotes','false');
            System.currentPageReference().getParameters().put('relatedId',String.valueOf(contactList[0].Id));
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            Test.stopTest();
            System.assertEquals(true,recordtypeListforNotes != Null);
            System.assertEquals(true,ApexPages.currentPage().getParameters().get('id')== Null);
        }
    }
    // Testing scenario when relatedId has opportunity id, isNotes is set to true, notes id is not passed and recordtype for notes is Preference Cards. 
    @isTest static void testFifthCase() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforPreNotes[0].Id;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('relatedId',String.valueOf(oppList[0].Id));
            System.currentPageReference().getParameters().put('isNotes','true');
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            controller.save();
            controller.cancel();
            controller.makeRecordPublic();
            controller.makeRecordPrivate();
            controller.postChatter();
            Test.stopTest();
            System.assertEquals(true,recordtypeListforPreNotes != Null);
            System.assertEquals(true,controller.notes != Null); 
        }
    }
    // Testing when Notes has lookup only for Opportunity, isNotes Parameter is True, Id of notes is passed and relatedId parameter is not passed
    @isTest static void testSixthCase() {
        createData();
        System.runAs(user[0]) {
            noteList[0].recordtypeId = recordtypeListforNotes[0].Id;
            noteList[0].Account__c = Null;
            noteList[0].Contact__c = Null;
            insert noteList;
            PageReference pageRef = Page.PrivateNotes;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id',String.valueOf(noteList[0].Id));
            System.currentPageReference().getParameters().put('isNotes','true');
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(noteList[0]);
            PrivateNotesController controller = new PrivateNotesController(sc);
            controller.notes = noteList[0];
            controller.deleteRecord();
            Test.stopTest();
            System.assertEquals(true,recordtypeListforNotes != Null);
            System.assertEquals(true,controller.notes != Null);
        }
    }
    // Method to setup the test data 
    public static void createData() {
        Schema.RecordTypeInfo rtByNameContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        Id recordTypeContact = rtByNameContact.getRecordTypeId();
        user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        System.runAs(user[0]) {
            accountList = TestUtils.createAccount(2, True);
            contactList = TestUtils.createContact(1,accountList[0].Id, false);
            contactList[0].RecordTypeId = recordTypeContact;
            insert contactList;
            oppList = TestUtils.createOpportunity(1,accountList[0].Id, false);
            oppList[0].Business_Unit__c = 'NSE';
            oppList[0].Opportunity_Number__c = '50001';
            oppList[0].OwnerId = user[0].Id;
            insert oppList[0];
            noteList = createNote(2,'title',accountList[0].Id,contactList[0].Id,oppList[0].Id,false);
        }
    }
    // method to create Notes custom object records list
    public static List<Notes__c> createNote(Integer noteCount,String title,Id accountId ,Id contactId ,Id oppId,Boolean isInsert) {
        noteList = new List<Notes__c>();
        
        for(Integer i = 0; i < noteCount; i++) {
            Notes__c noteRecord = new Notes__c(Name = title ,Account__c = accountId, Contact__c = contactId , Opportunity__c = oppId);
            noteList.add(noteRecord);
        }
        if(isInsert) {
            insert noteList;
        } 
        return noteList;
    }
}