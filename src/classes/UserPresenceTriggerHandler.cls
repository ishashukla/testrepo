/**================================================================      
 * Appirio, Inc
 * Name: UserPresenceTriggerHandler
 * Description: Handler Class for UserPresenceTrigger(T-505263)
 * Created Date: 05/26/2016
 * Created By: Ashu Gupta (Appirio)
 ==================================================================*/
public without sharing class UserPresenceTriggerHandler {
    
    // Method to be called on AfterInsert event of trigger
    //  @param List of new after insert UserServicePresence, Map of new after insert UserServicePresence
  	//  @return void.
    public static void onAfterInsert(List<UserServicePresence> newList, Map<Id, UserServicePresence> newMap){
        Set<String> statusId = new Set<String>(); 
        List<User> usersToUpdate = new List<User>();
        
      for(UserServicePresence userServicePresence : newMap.values()){           
            if(!statusId.contains(userServicePresence.ServicePresenceStatusId))
                statusId.add(userServicePresence.ServicePresenceStatusId);
            }
        
        Map<Id, ServicePresenceStatus> servicePresenseStatusMap = new Map<Id, ServicePresenceStatus>([SELECT id, masterLabel 
                                                                   FROM ServicePresenceStatus  
                                                                   WHERE id in :statusId]);
        
        for(UserServicePresence userServicePresence : newMap.values()){   
            
            if(servicePresenseStatusMap.containsKey(userServicePresence.ServicePresenceStatusId)){
                usersToUpdate.add(new User(id = userServicePresence.userId, 
                                 Status__c = servicePresenseStatusMap.get(userServicePresence.ServicePresenceStatusId).masterLabel));
            }
        }
        
        if(usersToUpdate.size() > 0){
            update usersToUpdate;
        }       
	}       
}