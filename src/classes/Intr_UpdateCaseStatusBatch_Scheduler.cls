// 
// (c) 2016 Appirio, Inc.
//
// T-500180, This is a test class for Intr_UpdateCaseStatusBatch class
//
// 12 May, 2016  Shreerath Nair Original 

public class Intr_UpdateCaseStatusBatch_Scheduler implements schedulable{
    
     //Execute scheduler batch Intr_UpdateCaseStatusBatch
     public void execute(SchedulableContext sc){
        Intr_UpdateCaseStatusBatch batch = new Intr_UpdateCaseStatusBatch(); // Batch class
        database.executebatch(batch);
    }

}