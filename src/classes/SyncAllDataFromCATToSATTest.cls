// (c) 2016 Appirio, Inc.
//
// Class Name: SyncAllDataFromCATToSATTest 
// Description: Test Class for SyncAllDataFromCATToSAT
// August 18, 2016 Pratz Joshi Original

@isTest
private class SyncAllDataFromCATToSATTest {
    // Method to test insert of Custom Account Team
static testMethod void testSyncAllDataFromCATToSAT(){
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User usr1 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
      User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        System.runAs(usr) {
            Test.startTest();
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            List<Custom_Account_Team__c> recList = new List<Custom_Account_Team__c>();
            Custom_Account_Team__c cat = new Custom_Account_Team__c();
            cat.Account__c = acc.Id;
            cat.User__c = usr1.Id;
            cat.Team_Member_Role__c = 'Navigation';
            cat.Account_Access_Level__c ='Read Only';
            recList.add(cat);
            Custom_Account_Team__c cat2 = new Custom_Account_Team__c();
            cat2.Account__c = acc.Id;
            cat2.User__c = usr2.Id;
            cat2.Team_Member_Role__c = 'Surgical';
            cat2.Account_Access_Level__c ='Read/Write';
            recList.add(cat2);
            insert recList;
            SyncAllDataFromCATToSAT sync = new SyncAllDataFromCATToSAT();
            database.executeBatch(sync);
            Test.stopTest();
        }
    }
        // Method to test delete of Custom Account Team
    static testMethod void testSyncAllDataFromCATToSAT1(){
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User usr1 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
      User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        System.runAs(usr) {
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            Test.startTest();
            AccountTeamMember atm = new AccountTeamMember();
            atm.AccountId = acc.Id;
            atm.TeamMemberRole = 'Navigation';
            atm.UserId = usr1.Id;
            insert atm;
            AccountShare accShare = new AccountShare();
          accShare.AccountId = acc.Id;
          accShare.UserOrGroupId = usr1.Id;
          accShare.RowCause = Schema.AccountShare.RowCause.Manual;
          accShare.OpportunityAccessLevel = 'None';
          accShare.ContactAccessLevel = 'None';
          accShare.CaseAccessLevel = 'None';
          accShare.AccountAccessLevel = 'Edit';
            insert accShare;
            List<Custom_Account_Team__c> recList = new List<Custom_Account_Team__c>();
            Custom_Account_Team__c cat = new Custom_Account_Team__c();
            cat.Account__c = acc.Id;
            cat.User__c = usr1.Id;
            cat.Team_Member_Role__c = 'Navigation';
            cat.Account_Access_Level__c ='Private';
            cat.IsDeleted__c = true;
             insert cat;
            SyncAllDataFromCATToSAT sync = new SyncAllDataFromCATToSAT();
            database.executeBatch(sync);
            List<AccountShare> shareList= [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel 
                        FROM AccountShare
                        WHERE UserOrGroupId =:usr1.Id
                        AND AccountId =:acc.Id
                        AND RowCause =:Schema.AccountShare.RowCause.Manual];
             system.assertEquals(shareList.size(),1);
            Test.stopTest();
        }
    }
     // Method to test update of Access Level in Custom Account Team
     static testMethod void testSyncAllDataFromCATToSAT2(){
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User usr1 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
      User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        System.runAs(usr) {
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            Test.startTest();
            AccountTeamMember atm = new AccountTeamMember();
            atm.AccountId = acc.Id;
            atm.TeamMemberRole = 'Navigation';
            atm.UserId = usr1.Id;
            insert atm;
            AccountShare accShare = new AccountShare();
          accShare.AccountId = acc.Id;
          accShare.UserOrGroupId = usr1.Id;
          accShare.RowCause = Schema.AccountShare.RowCause.Manual;
          accShare.OpportunityAccessLevel = 'None';
          accShare.ContactAccessLevel = 'None';
          accShare.CaseAccessLevel = 'None';
          accShare.AccountAccessLevel = 'Edit';
            insert accShare;
            List<Custom_Account_Team__c> recList = new List<Custom_Account_Team__c>();
            Custom_Account_Team__c cat = new Custom_Account_Team__c();
            cat.Account__c = acc.Id;
            cat.User__c = usr1.Id;
            cat.Team_Member_Role__c = 'Navigation';
            cat.Account_Access_Level__c ='Private';
             insert cat;
            SyncAllDataFromCATToSAT sync = new SyncAllDataFromCATToSAT();
            database.executeBatch(sync);
            List<AccountShare> shareList= [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel 
                        FROM AccountShare
                        WHERE UserOrGroupId =:usr1.Id
                        AND AccountId =:acc.Id
                        AND RowCause =:Schema.AccountShare.RowCause.Manual];
             system.assertEquals(shareList.size(),1);
            Test.stopTest();
        }
    }
}