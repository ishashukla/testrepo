/**=====================================================================
 * Appirio, Inc
 * Name: OrderLineItemAddressController
 * Description: T-482421 Update OLI Address used for COMM
                based on the available address and user action
                Controller class for COMM_OrderLineItemAddressPage
 * Created Date: 04/13/2016
 * Created By: Rahul Aeran
 * 05/02/2016   Rahul Aeran     T-497412 - Updating the controller for the new additions of the task, same controller 
 * for UpdateOLIShippingDetails page
 * 09/09/2016    Meghna Vijay    I-234373 - To restrict the address to Shipping And Business Unit Name = COMM
 * 10/14/2016    Kanika Mathur   I-239411 - Modified code to let the items be set as true
 * 10/31/2016    Ralf Hoffmann   I-242229 - added Tracked as Software, Track as Asset in SFDC to query for product
 * 01/11/206     Ralf Hoffmann   I-242315 - updated initial sort order to Order number and line item. 
 * 											added Link-to-line-id to query where clause to limit to only top level order lines
=====================================================================*/
public with sharing class OrderLineItemAddressController{
    public String projectPhaseId{get;set;}
    public Project_Phase__c selectedProjectPhase{get;set;}
    public Project_Plan__c selectedProject{get;set;}
    public List<SelectOption> lstAvailableAddress{get;set;}
    public String selectedAddress{get;set;}
    public List<Address__c> lstSelectedAddressObject{get;set;}
    public List<OrderLineItemWrapper> items{get;set;}
    map<Id,Address__c> mapAddressIdToAddressObject;
    String accountId;
    public Boolean isFieldEditable{get;set;} // used for T-497412, to verify that only profile that
    public String sortExpression{get;set;}
    public String sortDirection{get;set;}
    String projectId;
    //Dummy object to map Field to show date and selected status for the complete list
    public OrderItem dummyOrderItem{get;set;}
    
    public OrderLineItemAddressController(){
        sortExpression = 'Order_Number_in_Oracle__c,Oracle_Order_Line_Number_To_Sort__c'; //rh 11/1/2016 I-242315 update from ID to sort by order and order line item
        sortDirection = 'ASC';
        isFieldEditable = false;
        dummyOrderItem = new OrderItem();
        projectPhaseId = ApexPages.currentPage().getParameters().get('id');
        projectId = ApexPages.currentPage().getParameters().get('ppid');
        lstAvailableAddress = new List<SelectOption>();
        lstSelectedAddressObject = new List<Address__c>();
        items = new List<OrderLIneItemWrapper>();
        if(projectPhaseId == null && projectId == null){
            return;
        }
        List<Project_Phase__c> lstProjectPhases;
        List<Project_Plan__c> lstProjectPlan;
        if(!String.isEmpty(projectPhaseId)){
          lstProjectPhases = [SELECT Id,Name,Project_Plan__c,Project_Plan__r.Name,Project_Plan__r.Account__c, 
                                                      Project_Plan__r.Account__r.Name
                                                      FROM Project_Phase__c
                                                      WHERE Id = :projectPhaseId];
          if(!lstProjectPhases.isEmpty())
          {
            
            selectedProjectPhase = lstProjectPhases.get(0);
            accountId = selectedProjectPhase.Project_Plan__r.Account__c;
            selectedProject = selectedProjectPhase.Project_Plan__r;
          }
        }else if(!String.isEmpty(projectId)){
          
          lstProjectPlan = [SELECT Id,Name,Account__c,Account__r.Name
                             FROM Project_Plan__c WHERE Id = :projectId];
          if(!lstProjectPlan.isEmpty())
          {
            selectedProject = lstProjectPlan.get(0);
            accountId = lstProjectPlan.get(0).Account__c;
          }
          /*
          //NB - 10/04 - I-238013 - Start
          lstProjectPhases = [SELECT Id,Name,Project_Plan__c,Project_Plan__r.Name,Project_Plan__r.Account__c, 
                                                      Project_Plan__r.Account__r.Name
                                                      FROM Project_Phase__c
                                                      WHERE Project_Plan__c= :projectId];
          if(lstProjectPhases != null && lstProjectPhases.size() > 0)
          {
            selectedProjectPhase = lstProjectPhases.get(0);
          }
          //NB - 10/04 - I-238013 - Start    
          */                                        
        }
        
        String strCompletedAddress = '';
        if(accountId != null){
            mapAddressIdToAddressObject = new map<Id,Address__c>();
            for(Address__c add : [SELECT Id,Primary_Address_Flag__c,Type__c,Account__c,Address1__c,Address2__c,
                                     City__c,State_Province__c,PostalCode__c,Country__c,Full_Address__c,Status__c,
                                     Start_Date__c,End_Date__c,Business_Unit_Name__c
                                     FROM Address__c 
                                     WHERE Account__c =:accountId
                                     AND Type__c = :Constants.SHIPPING
                                     AND Business_Unit_Name__c = :Constants.COMM]){
                strCompletedAddress = add.Full_Address__c;
                if(strCompletedAddress != null){
                    strCompletedAddress = strCompletedAddress.replace('<br>',' ');
                    lstAvailableAddress.add(new SelectOption(add.Id,strCompletedAddress));                             
                }
                mapAddressIdToAddressObject.put(add.Id,add);
            }
            if(!lstAvailableAddress.isEmpty()){
                selectedAddress = lstAvailableAddress.get(0).getValue();
                lstSelectedAddressObject.add(mapAddressIdToAddressObject.get(selectedAddress));
            }
            
        }
        
        
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:UserInfo.getProfileId() LIMIT 1];
        String strLoggedInUserProfileName = PROFILE[0].Name;
        if(strLoggedInUserProfileName.contains(Constants.PROJECT_MANAGER) ||
            strLoggedInUserProfileName.contains(Constants.ADMIN_PROFILE)){
            isFieldEditable = true;
        } 
        initializeItems(null);
       
        
    }
    
    public void setOrderBy(){
      sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
      initializeItems(null);
    }
    public void initializeItems(Set<Id> selectedItemIds){
        
        items = new List<OrderLIneItemWrapper>();
        OrderItem oi;
        //rh 15 Oct 2016; I-240239 ; added Oracle Order fields for order number, line number, line number to sort;
        //rh 31 oct 2016; I-242229; added Tracked as software, Track as Asset in SFDC to query;
        //rh 01 nov 2016; I-242315; added Link-to-line-id to query to limit to only top level order lines;
        String finalQuery = 'SELECT Id,OrderItemNumber,Oracle_Order_Line_Number__c,Locked__c,'+
                            'Description,Pricebookentry.Product2Id,Pricebookentry.Product2.name,Pricebookentry.Product2.QIP_Required__c,'+
            				'Pricebookentry.Product2.Tracked_as_Software__c,Pricebookentry.Product2.Tracked_as_Asset_in_SFDC__c,'+
            				'Pricebookentry.Product2.Description,Hold_Description__c,Requested_Ship_Date__c, '+ 
                            'PriceBookentry.product2.ProductCode,Product_Shipping_Address__c,ServiceDate,EndDate,'+
                            'Status__c,Shipping_Address__c,Shipping_Address__r.Name,Order.PoNumber,order.SAP_Order_Number__c,'+
                            'Order.COMM_SAP_Order_Number__c ,Shipped_Date__c,Approval_Status__c,Project_Phase__c,Promise_Date__c,'+
            				'Order_Number_in_Oracle__c,Oracle_Order_Line_Number_To_Sort__c, Callout_Result__c' +
                            ' FROM OrderItem'+
            				' WHERE Link_To_Line_Id__c = Null';
        if(!String.isBlank(projectPhaseId)){
          finalQuery+= ' AND Project_Phase__c = :projectPhaseId';
        }else if(!String.isBlank(projectId)){
          finalQuery+= ' AND Project_Plan__c = :projectId';
        }
        
        finalQuery+= ' ORDER BY ' +sortExpression  + ' ' + sortDirection; 
        System.debug('Final Query:'+finalQuery);
        for(Sobject obj: Database.query(finalQuery)){
            oi = (OrderItem)obj;
            if(selectedItemIds != null && selectedItemIds.contains(oi.Id)) {
               items.add(new OrderLIneItemWrapper(oi,true));   
            } else {
                items.add(new OrderLIneItemWrapper(oi,false)); 
            }
        }
    }
    public PageReference updateAddress(){
        System.debug('Selected address:'+selectedAddress);
        Boolean itemSelected = false;
        Boolean isErrorExist = false;
        List<OrderItem> lstOrderItemsToUpdate = new List<OrderItem>();
        Set<Id> selectedOLIIds = new Set<Id>();
        for(OrderLIneItemWrapper wrap : items){
            if(wrap.isSelected){
                itemSelected = true;
                lstOrderItemsToUpdate.add(new OrderItem(Id=wrap.oli.Id,Shipping_Address__c = selectedAddress));
                selectedOLIIds.add(wrap.oli.Id);
            }
        }
        if(selectedAddress == null){
          ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.COMM_OLIAddressUpdate_No_Address));
        }else{
          if(!itemSelected){
              if(items.isEmpty()){
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.COMM_OLIAddressUpdate_NO_OLI)); 
              }else{
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.COMM_OLIAddressUpdate_No_OLI_Selected));
              }
          }else{
            if(!lstOrderItemsToUpdate.isEmpty()){
              try{
                Database.SaveResult []sr = Database.update(lstOrderItemsToUpdate,false);
                for(Database.SaveResult s : sr){
                  System.debug('Success:'+s.isSuccess());
                  if(!s.isSuccess()){
                      isErrorExist = true;
                      for(Database.Error err : s.getErrors()){
                          ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage()));
                      }
                  }
                }
              }catch(Exception ex){
                system.debug('exception : ' + ex.getMessage());
              }
            }
            initializeItems(selectedOLIIds);
            if(!isErrorExist)
              ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.COMM_OLIAddressUpdate_Address_Updated));
          }
        }
        
        return null;
    }
    public PageReference updateStatus(){
      List<OrderItem> lstOrderItemsToUpdate = new List<OrderItem>();
      Set<Id> selectedOLIIds = new Set<Id>();
      Boolean isErrorExist = false;
      for(OrderLineItemAddressController.OrderLIneItemWrapper wrap : items){
        if(wrap.isSelected && dummyOrderItem.Approval_Status__c != NULL){
            lstOrderItemsToUpdate.add(new OrderItem(Id=wrap.oli.Id,Approval_Status__c = dummyOrderItem.Approval_Status__c));
            selectedOLIIds.add(wrap.oli.Id);
        }
        else if(wrap.isSelected && dummyOrderItem.Approval_Status__c == NULL) {
          ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot update Status to None from Update Information Section '));
          selectedOLIIds.add(wrap.oli.Id);
        }
        else{
            lstOrderItemsToUpdate.add(new OrderItem(Id=wrap.oli.Id,Approval_Status__c = wrap.oli.Approval_Status__c));
        }
      }
      if(items.isEmpty()){
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.COMM_OLIAddressUpdate_NO_OLI)); 
      }else if(dummyOrderItem.Approval_Status__c != NULL){ 
        Database.SaveResult []sr = Database.update(lstOrderItemsToUpdate,false);
        //sendEmailNotificationOnRequested(lstOrderItemsToUpdate);
        for(Database.SaveResult s : sr){
            if(!s.isSuccess()){
                isErrorExist = true;
                for(Database.Error err : s.getErrors()){ 
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage()));
                }
            }
        }
        
        initializeItems(selectedOLIIds);
        if(!isErrorExist && dummyOrderItem.Approval_Status__c != NULL )
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.OLI_Update_Project_Phase));
      }
      return null;
      
    }
    
    public PageReference updateDate(){
      List<OrderItem> lstOrderItemsToUpdate = new List<OrderItem>();
      Set<Id> selectedOLIIds = new Set<Id>();
      Boolean isErrorExist = false;
      for(OrderLineItemAddressController.OrderLIneItemWrapper wrap : items){
        if(wrap.isSelected && dummyOrderItem.Requested_Ship_Date__c != NULL){
            lstOrderItemsToUpdate.add(new OrderItem(Id=wrap.oli.Id,Requested_Ship_Date__c = dummyOrderItem.Requested_Ship_Date__c));
            selectedOLIIds.add(wrap.oli.Id);
        }
        else if(wrap.isSelected && dummyOrderItem.Requested_Ship_Date__c == NULL) {
          ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot update Date to NULL from Update Information Section '));
          selectedOLIIds.add(wrap.oli.Id);
        }
        else{
            lstOrderItemsToUpdate.add(new OrderItem(Id=wrap.oli.Id,Requested_Ship_Date__c = wrap.oli.Requested_Ship_Date__c));
        }
      }
      if(items.isEmpty()){
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.COMM_OLIAddressUpdate_NO_OLI)); 
      }else{
        Database.SaveResult []sr = Database.update(lstOrderItemsToUpdate,false);
        
        for(Database.SaveResult s : sr){
            if(!s.isSuccess()){
                isErrorExist = true;
                for(Database.Error err : s.getErrors()){ 
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage()));
                }
            }
        }
        
        initializeItems(selectedOLIIds);
        if(!isErrorExist && dummyOrderItem.Requested_Ship_Date__c != NULL)
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.OLI_Update_Project_Phase));
      }
      return null;
    }
    public void setAddress(){
      lstSelectedAddressObject = new List<Address__c>();
      lstSelectedAddressObject.add(mapAddressIdToAddressObject.get(selectedAddress));  
    }
    
    public class OrderLIneItemWrapper{
        public OrderItem oli{get;set;}
        public Boolean isCheckBox = false;
        public Boolean isSelected{get;set;}
        public String selectedRoom{get;set;} //used for T-493111 as we are using the same wrapper
        public String strDescription{get;set;} //this is for holding Hold_Description__c
        public OrderLineItemWrapper(OrderItem oli, Boolean isSelected){
            this.oli = oli;
            this.isSelected = isSelected;
            if(oli != null){
                if(oli.Hold_Description__c != null && oli.Hold_Description__c.length() > 30){
                    this.strDescription = oli.Hold_Description__c.subString(0,30) + '...';
                }else{
                    this.strDescription = oli.Hold_Description__c;
                }
            }
        }
    }
   
}