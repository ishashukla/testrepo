/****************************************************************************************************************************************
Author        :  Appirio JDC (Kajal Jalan)
Date          :  May 09, 2016
Purpose       :  Controller To Create custom attachment list view on Account page Used in Intr_attachmentListViewOnAccount Vf (T-499890)

Update        :  Shreerath(T-514433), 24th June, 2016 - Add child account's opportunity attachment into list.
******************************************************************************************************************************************/
// T-514433, 24th June, 2016 - Add child account's opportunity attachment into list.
public without sharing class Intr_AttachmentListController {
    
    public List<Id> oppList {get;set;}
    
    public Intr_AttachmentListController(ApexPages.StandardController controller) {
        
        oppList = new List<Id>();
        Account account = (Account)controller.getRecord();
        
        Set<String> setAccountIds = new Set<String>(); 
		setAccountIds.add(account.Id); 
		
        //T-514433 (SN) - Check if the Account has any child account
        for(Account childAccount : [SELECT Id FROM Account WHERE Parent.Id =: account.Id]){ 
			setAccountIds.add(childAccount.Id); 
		} 
		
		//T-514433 (SN) -  add the attachment of Opportunity into List
		Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>([SELECT Id 
																		FROM Opportunity 
																		WHERE AccountId IN: setAccountIds 
																		AND StageName = 'Closed Won']); 
		oppList.addAll(mapOpportunity.keySet()); 
		
        
    }
}