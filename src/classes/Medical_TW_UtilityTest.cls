/*******************************************************************************
 Author        :  Appirio JDC (Megha Agarwal)
 Date          :  Oct 5th, 2015
 Description       :  Test class for Medical_TW_Utility
 
  Modified Date			Modified By				Purpose
  06-09-2016			Chandra Shekhar			To Fix Test Class
*******************************************************************************/
@isTest
private class Medical_TW_UtilityTest {
  private static List<Account>  accounts;
  private static List<Contact> contacts;
  private static List<Case>  cases;
  private static List<Asset__c> assets;
  private static List<Case_Asset__c> caseAssets;
  
  private static void createTestData(){
    accounts = Medical_TestUtils.createAccount(1, true);
    contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);
    cases = Medical_TestUtils.createCase(1, accounts.get(0).id, contacts.get(0).id , false);
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Non Product Issue');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Medical - Non Product Issue', 'Medical');

	cases[0].recordTypeId = rtByName.getRecordTypeId();
	insert cases;
 	assets = Endo_TestUtils.createAsset(1, accounts.get(0).id, true);
    caseAssets = Endo_TestUtils.createCaseAssetJunction(1, cases.get(0).id, assets.get(0).id, true);
  }
  
  static testMethod void testSendCasesToTrackWise(){
    createTestData();
    
    Medical_TestUtils.createRegulatoryQuesAnswer(1,cases.get(0).Id,true);
    Case cs = cases.get(0);
    cs.Status = 'Closed';
    Test.startTest();
    update cs;
    Test.stopTest();
  }
}