/*******************************************************************************
Author        :  Appirio JDC (Megha Agarwal)
Date          :  Oct 5th, 2015
Description       :  Handler Class of Case functionality for Medical Division
1.) Prevent Case closure if any open activities

Feb.03, 2016      Sunil                 S-371516 Add method for insertCaseAssetJunctionRecord
March 07, 2016    Sunil                 I-204597
March.17, 2016    Sunil                 Add method for sendCasesToTrackwise
June 07, 2016     Shreerath Nair        I-221437
June.17, 2016     Shreerath             Add if-else condition to call future method seperatly for batch 
June 29, 2016     Shreerath             I-224409
June 29, 2016     Shreerath             Added recordtype check in sendCasesToTrackwise function to run for 
                                        medical Potential Product RecordTYpe only.
15 Sept 2016      Nathalie Le Guay      I-235086 - Add Endo RT to Trackwise logic
19 Sept 2016      Shreerath Nair        I-235106 - Add Endo RT to validateAssetExist function
*******************************************************************************/
public class Medical_CaseTriggerHandler implements CaseTriggerHandlerInterface{
    /**================================================================      
    * Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/
    public static boolean IsActive(){
        return TriggerState.isActive(Constants.TRIGGER_MEDICAL_CASETRIGGER);
    }
    
    public static void OnBeforeInsert(List<Case> newCases){}
    public static void OnAfterInsert(List<Case> newCases){
        insertCaseAssetJunctionRecord(newCases, null);
        sendCasesToTrackwise(newCases, null);
    }
    public static void OnAfterInsertAsync(Set<ID> newCaseIDs){}
    
    public static void OnBeforeUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){
        checkNoActivityAssociated(newCases, oldCaseMap);
        validateRQAExistOnPotentialProductCases(newCases, oldCaseMap);
        validateAssetExist(newCases, oldCaseMap);
    }
    public static void OnAfterUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){
        insertCaseAssetJunctionRecord(newCases, oldCaseMap);
        sendCasesToTrackwise(newCases, oldCaseMap);

    }
    public static void OnAfterUpdateAsync(Set<ID> updatedCaseIDs){}
    
    public static void OnBeforeDelete(List<Case> CasesToDelete, Map<ID, Case> caseMap){}
    public static void OnAfterDelete(List<Case> deletedCases, Map<ID, Case> caseMap){}
    public static void OnAfterDeleteAsync(Set<ID> deletedCaseIDs){}
    
    public void OnUndelete(List<Case> restoredCases){}
    /**================================================================      
    * End of Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/
    
    
    //----------------------------------------------------------------------------------------------
    //  Insert CaseAssetJunction Record When Case is created
    //----------------------------------------------------------------------------------------------
    public static void insertCaseAssetJunctionRecord(List<Case> lstNewCases, Map<Id, Case> mapOldCases){
        List<Case_Asset_Junction__c> lstCAJunction = new List<Case_Asset_Junction__c>();
        
        for(case c : lstNewCases){
            if(c.AssetId != null){
                if(mapOldCases == null || c.AssetId != mapOldCases.get(c.Id).AssetId){
                    Case_Asset_Junction__c objJunction = new Case_Asset_Junction__c(Case__c = c.Id, Asset__c = c.AssetId);
                    lstCAJunction.add(objJunction);
                }
            }
        }
        /* Inserting Case Asset Junction Records */
        if(!lstCAJunction.isEmpty()){
            upsert lstCAJunction;
        }
        
        // I-204597 Set Asset lookup field to NULL on Case record.
        List<Case> lstCasesToUpdate = new List<Case>();
        for(Case c : lstNewCases){
            if(c.AssetId != null){
                Case newCase = new Case();
                newCase.Id = c.Id;
                newCase.AssetId = null;
                lstCasesToUpdate.add(newCase);
                
            }
        }
        update lstCasesToUpdate;        
    }
    
    //----------------------------------------------------------------------------------------------
    //  Send Eligible Case to Trackwise
    //----------------------------------------------------------------------------------------------
    public static void sendCasesToTrackwise(List<Case> lstNewCases, Map<Id, Case> mapOldCases){
        Set<Id> setCaseIds = new Set<Id>();
        // I-224409(SN)
        ID medicalRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.MEDICAL_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId();
        Set<String> recordTypeNames = new Set<String>{'Customer Inquiry & Requests',
            'RMA Order',
            'Repair Order Billing',
            'Sales Order',
            'Sample Refurb Sale / Sample Sale',
            'Samples Request',
            'Tech Support',
            Intr_Constants.MEDICAL_CASE_RECORD_TYPE_POTENTIAL_PRODUCT};
                
        //Map<String, String> recordTypeId_Map = new Map<String, String>();
        Set<String> recordTypeId_Set = new Set<String>();
        Map<String, Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Case.getRecordTypeInfosByName();
        for (Schema.RecordTypeInfo rti : rtMapByName.values()) {
            if (recordTypeNames.contains(rti.getName())) {
                //recordTypeId_Map.put(rti.getName(), rti.getRecordTypeId());
                recordTypeId_Set.add(rti.getRecordTypeId());
            }
        }       
        
        // Filter eligible Cases - with Close status
        System.debug('@@@###' + lstNewCases);
        
        for(Case cs : lstNewCases){
            System.debug('@@@###' + cs.Trackwise_Status__c);
            System.debug('@@@###' + String.isBlank(cs.Trackwise_Status__c));
            System.debug('@@@###' + cs.status);
            System.debug('@@@###' + mapOldCases);
            system.debug('NLG: set of rt ids: '+recordTypeId_Set);
            
            if(mapOldCases != null){
                System.debug('@@@last' + mapOldCases.get(cs.id).status);
                system.debug('NLG: my case RT ID ' + mapOldCases.get(cs.id).RecordTypeId);
                system.debug('NLG: isClosed? New:' + cs.isClosed + ' old: ' + mapOldCases.get(cs.id).isClosed);
                system.debug('NLG: status? New:' + cs.status + ' old: ' + mapOldCases.get(cs.id).status);
            }
            //System.debug('@@@###' + mapOldCases.get(cs.id).status);
            // I-224409(SN)
            //if(cs.status == 'Closed' && (mapOldCases == null || cs.status != mapOldCases.get(cs.id).status) && (String.isBlank(cs.Trackwise_Status__c) || cs.Trackwise_Status__c == 'Planned')  && cs.RecordTypeId == medicalRecordTypeId){
            if(cs.isClosed && (mapOldCases == null || cs.status != mapOldCases.get(cs.id).status) && (String.isBlank(cs.Trackwise_Status__c) || cs.Trackwise_Status__c == 'Planned') &&  recordTypeId_Set.contains(cs.RecordTypeId) ) {
                setCaseIds.add(cs.id);
            }
        }
        System.debug('@@@### setCaseIds ' + setCaseIds);
        
        // Filter eligible Case - having RQA associated
        Set<Id> eligibleRQIds = new Set<Id>();
        List<Case> casesToUpdate = new List<Case>();
        for(Regulatory_Question_Answer__c rqa :[SELECT Id, Formal_complaint__c, Harm_reported__c, Adverse_Consequences__c, Case__c FROM Regulatory_Question_Answer__c WHERE Case__c IN :setCaseIds]){
            if(rqa.Formal_complaint__c == true || rqa.Harm_reported__c == 'Yes' || rqa.Adverse_Consequences__c == 'Yes' || rqa.Adverse_Consequences__c == 'Complainant Not Aware') {
                eligibleRQIds.add(rqa.Id);
                // Update status of Cases being sent to Trackwise.
                Case c = new Case(Id = rqa.Case__c);
                c.Trackwise_Status__c = 'Sent for Trackwise';
                casesToUpdate.add(c);
            }
        }
        System.debug('@@@eligibleRQIds' + eligibleRQIds);
        
        if(casesToUpdate.size() > 0){
            update casesToUpdate;
        }
        
        system.debug('\nRunning Trackwise eligibility check: '+ eligibleRQIds.isEmpty());
        
        if(eligibleRQIds.isEmpty() == false){            
            //invokeTrackWiseWebService(eligibleRQIds);    
            //---------------------------------------------------------------------------------------------
            // I-221437 - if-else condition to call future method seperatly for batch 
            if(System.isBatch()){                
                Medical_TW_Utility.invokeTrackWiseWebService(eligibleRQIds);
            }
            else{    
                invokeTrackWiseWebService(eligibleRQIds);
            }
        }
    }
    
    //----------------------------------------------------------------------------------------------
    // Calling Trackwise service
    //----------------------------------------------------------------------------------------------
    @future(callout=true)
    public static void invokeTrackWiseWebService(Set<Id> setIds) {
        System.debug('@@@ start invokeTrackWiseWebService' + setIds);
        Medical_TW_Utility.invokeTrackWiseWebService(setIds);
        System.debug('@@@ end invokeTrackWiseWebService');
    }
    
    //----------------------------------------------------------------------------------------------
    //  check NoActivity Associated before closing a Case
    //----------------------------------------------------------------------------------------------
    public static void checkNoActivityAssociated(List<Case> lstNewCases, Map<Id, Case> oldCaseMap){
        Set<Id> closedCaseIds = new Set<Id>();
        Set<Id> errCaseIds = new Set<Id>();
        
        for(Case cs : lstNewCases){
            if(cs.status == 'Closed' && cs.status != oldCaseMap.get(cs.id).status){
                closedCaseIds.add(cs.id);
            }
        }
        
        if(!closedCaseIds.isEmpty()){
            for(Case cs : [select id , (select id  from OpenActivities limit 1) from Case where id in : closedCaseIds]){
                // check Open Activity
                System.debug('::cs.OpenActivities'+cs.OpenActivities);
                if(!cs.OpenActivities.isEmpty()){
                    //newCaseMap.get(cs.id).addError(System.Label.Case_Closure_VR_Message);
                    errCaseIds.add(cs.id);
                }
            }
        }

        for(Case cs : lstNewCases){
            if(errCaseIds.contains(cs.id) ){
                cs.addError(System.Label.Case_Closure_VR_Message);
            }
        }
    }    
    
    //----------------------------------------------------------------------------------------------
    //  Validate that before closing the Case, RQA record is associated when type is Potential Product Case
    //----------------------------------------------------------------------------------------------
    public static void validateRQAExistOnPotentialProductCases(List<Case> lstNewCases, Map<Id, Case> oldCaseMap){
        Set<Id> closedCaseIds = new Set<Id>();
        
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
        Profile ProfileName = [SELECT Name FROM profile WHERE id = :UserInfo.getProfileId()];
        // I-219050 we are showing an error message if User is editing a Case which does not associate with any RQA record.
        for(Case cs : lstNewCases){
            if(cs.RecordTypeId == recordTypeId){
                if(ProfileName.Name.containsIgnoreCase('medical')){
                    //if(cs.status == 'Closed' && cs.status != oldCaseMap.get(cs.id).status){
                    closedCaseIds.add(cs.id);
                    //}
                }
            }
        }
        
        Set<ID> idCaseWithRQA = new Set<ID>();
        for(Regulatory_Question_Answer__c rqa :[SELECT Id, Case__c FROM Regulatory_Question_Answer__c WHERE Case__c IN :closedCaseIds]){
            idCaseWithRQA.add(rqa.Case__c);
        }
        
        for(Case cs : lstNewCases){
            if(closedCaseIds.contains(cs.Id) == true && idCaseWithRQA.contains(cs.Id) == false){
                cs.addError('Please click on "Edit Support Case" button to associate RQA on this Case.');
            }
        }
    }   
    
    /* START ACARSON C-00173805 07.15.2016
    // Added method for validating an asset is added to a medical case before it is closed. Method was in SYKFULL2 sandbox */
    //----------------------------------------------------------------------------------------------
    //  In this method we will check if Junction object is not having any reocrd and Asset Lookup is blank than throw an error.
    //  We can not do this using Validation rule as Asset Lookup is removed and copied to Junction object as soon as Case is Saved.
    //----------------------------------------------------------------------------------------------
    public static void validateAssetExist(List<Case> lstNewCases, Map<Id, Case> oldCaseMap){
        Set<Id> eligibleCaseIds = new Set<Id>();
        
        // I-235837(SN) 19 Sept 2016 - START - Set to include all Case Recordtype for Endo & Medical
        Set<String> recordTypeNames = new Set<String>{'Customer Inquiry & Requests',
            'RMA Order',
            'Repair Order Billing',
            'Sales Order',
            'Sample Refurb Sale / Sample Sale',
            'Samples Request',
            'Tech Support',
            Intr_Constants.MEDICAL_CASE_RECORD_TYPE_POTENTIAL_PRODUCT};

        Set<String> recordTypeId_Set = new Set<String>();
        Map<String, Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Case.getRecordTypeInfosByName();
        // I-235837(SN) - Add all RecordtypeId into a Set
        for (Schema.RecordTypeInfo rti : rtMapByName.values()) {
            if (recordTypeNames.contains(rti.getName())) {
                recordTypeId_Set.add(rti.getRecordTypeId());
            }
        }
        //Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
        for(Case cs : lstNewCases){
            String status = cs.status;
            if(recordTypeId_Set.contains(cs.RecordTypeId) 
                    && cs.AssetId == null 
                    && status.Contains('Closed') 
                    && cs.status != oldCaseMap.get(cs.id).status){
                eligibleCaseIds.add(cs.id);
            }
        }
        
        for(Case_Asset_Junction__c junction :[SELECT Case__c FROM Case_Asset_Junction__c Where Case__c = :eligibleCaseIds]){
            eligibleCaseIds.remove(junction.Case__c);
        }
        
        //I-235837(SN) 19 Sept 2016 -  Check if Case Asset Exist for the Case
        
        for(Case_Asset__c caseAsset : [SELECT Case__c 
                                       FROM Case_Asset__c
                                       WHERE Case__c = :eligibleCaseIds]){
            //Remove case from set if asset exist for the case
            eligibleCaseIds.remove(caseAsset.Case__c);                               
        }
        
        //check if Regulatory question is available on the case.
        SET<id> castWithRQA_Set = new SET<id>();
        for(Regulatory_Question_Answer__c rqa : [SELECT Id, Case__c 
                                                 FROM Regulatory_Question_Answer__c 
                                                 WHERE Case__c IN : eligibleCaseIds]){
            castWithRQA_Set.add(rqa.case__c);
        } 
        
        //Remove caseId from Set if no regulatory question exist.
        for(ID id : eligibleCaseIds){
            if(!castWithRQA_Set.contains(id)){
                eligibleCaseIds.remove(id);
            }
        }
        
        //I-235837(SN) - END
        for(Case cs : lstNewCases){
            if(eligibleCaseIds.contains(cs.Id) == true){
                cs.addError('Please Associate an Asset to this Case before saving.');
            }
        }
    }
    //END ACARSON C-00173805 07.15.2016
}