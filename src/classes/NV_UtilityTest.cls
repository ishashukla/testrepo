@isTest
private class NV_UtilityTest {

    @testSetup static void setup(){
        List<User> testUsers = new List<User>();
        Profile sysAdminProfile = NV_Utility.getProfileByName('System Administrator');
        for (Integer i=1;i<=4;i++){
            testUsers.add(NV_TestUtility.createUser('testuser_uts'+i+'@mail.com', 'testuser_uts'+i+'@mail.com',
                                                            sysAdminProfile.Id, null, 'ut'+i+'', false));
        }
        insert testUsers;

        Map<String, Id> accountRecordTypes1 = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');
        Account testAccount1 = NV_TestUtility.createAccount('TestAccount1',accountRecordTypes1.get('NV_Customer'), true);
        Contract testContract1 = NV_TestUtility.createContract(testAccount1.Id, true);
        testContract1.Status = 'Activated';
        update testContract1;

        Map<String, Id> orderRecordTypes1 = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
        Order testOrder1 = NV_TestUtility.createOrder(testAccount1.Id, testContract1.Id,
                                                        orderRecordTypes1.get('NV_Standard'), false);
        testOrder1.Customer_PO__c = 'PO00001';
        testOrder1.EffectiveDate = Date.today();
        testOrder1.Date_Ordered__c = Datetime.now();
        testOrder1.Status = 'Booked';
        insert testOrder1;
    }

    @isTest static void testGetNotificationSettingForUser(){
        //Replaced current user with new created user
        Id currentUserId = [SELECT Id, Username FROM User WHERE Username like '%testuser_uts%' LIMIT 1].Id;
        //Test Scenario 1 where record is not available yet
        System.assertEquals(0, [SELECT count() FROM Notification_Settings__c WHERE User__c =: currentUserId]);
        //This method will create new record
        Notification_Settings__c newUserSettings = NV_Utility.getNotificationSettingForUser(currentUserId);
        //New Record get created
        System.assertEquals(1, [SELECT count() FROM Notification_Settings__c WHERE User__c =: currentUserId]);

        //Update settings
        newUserSettings.Booked__c = true;
        update newUserSettings;

        ////Test Scenario 1 where record is available
        newUserSettings = NV_Utility.getNotificationSettingForUser(currentUserId);
        System.assertEquals(true, newUserSettings.Settings__c.contains('Creation'));
    }

    @isTest static void testGetNotificationSettingsForUsers(){

        Set<Id> testUsersIds = new Set<Id>();
        List<User> testUsers = [SELECT Id, Username FROM User WHERE Username like '%testuser_uts%'];
        for(User record : testUsers){
            testUsersIds.add(record.Id);
        }

        System.assertEquals(0, [SELECT count() FROM Notification_Settings__c WHERE User__c in : testUsersIds]);

        //Test Scenario 1 where records are not available yet
        Map<Id,Notification_Settings__c> userSettingMap = NV_Utility.getNotificationSettingsForUsers(testUsersIds);

        System.assertEquals(4, [SELECT count() FROM Notification_Settings__c WHERE User__c in : testUsersIds]);

        for(Id userId : userSettingMap.keySet()){
            userSettingMap.get(userId).Booked__c = true;
        }
        update userSettingMap.values();

        //Test Scenario 2 where records are available
        userSettingMap = NV_Utility.getNotificationSettingsForUsers(testUsersIds);
        for(Id userId : userSettingMap.keySet()){
            System.assertEquals(true, userSettingMap.get(userId).Settings__c.contains('Creation'));
        }
    }

    @isTest static void testIsUserIsOptedForStatus(){
        Id currentUserId = UserInfo.getUserId();
        Notification_Settings__c currentUserSettigns = NV_Utility.getNotificationSettingForUser(currentUserId);
        currentUserSettigns.All_Active_Orders__c = false;
        currentUserSettigns.All_Booked__c = true;
        currentUserSettigns.Backordered__c = false;
        update currentUserSettigns;

        // Start modified for S-399670
        Test.startTest();
        currentUserSettigns = NV_Utility.getNotificationSettingForUser(currentUserId);

        //Test Scenario 1  All Open order "Booked" is true
        System.assertEquals(true, NV_Utility.isUserIsOptedForStatus(currentUserSettigns,'Creation',true));

        //Test Scenario 2 Those I Follow "Booked" is false
        //System.assertEquals(false, NV_Utility.isUserIsOptedForStatus(currentUserSettigns,'Creation',false));

        currentUserSettigns.All_Booked__c = false;
        currentUserSettigns.Booked__c = true;
        update currentUserSettigns;
        Test.stopTest();

        currentUserSettigns = [Select All_Settings__c, Settings__c from Notification_Settings__c where id=:currentUserSettigns.id];

        //Test Scenario 3 All Open order "Booked" is false
        System.assertEquals(false, NV_Utility.isUserIsOptedForStatus(currentUserSettigns,'Creation',true));

        //Test Scenario 4 Those I Follow "Booked" is true
        System.assertEquals(true, NV_Utility.isUserIsOptedForStatus(currentUserSettigns,'Creation',false));
        //End S-399670

        //Test Scenario 4 with AllActiveOrder true & Status for Backordered
        System.assertEquals(false, NV_Utility.isUserIsOptedForStatus(currentUserSettigns,'Backordered',true));
    }

    @isTest static void testGetDayOfWeek() {
        String dt =  NV_Utility.getDayOfWeek(DateTime.newInstance(2015, 10, 3,0,0,0));
        //System.assertEquals(null, NV_Utility.getDayOfWeek(null));
        System.assertEquals('Sat', NV_Utility.getDayOfWeek(Date.newInstance(2015, 10, 3)));
        System.assertEquals('Sat', NV_Utility.getDayOfWeek(DateTime.newInstance(2015, 10, 3,0,0,0)));

        System.assertEquals(-1, NV_Utility.getDayOfWeekInInt(null));
        System.assertEquals(6, NV_Utility.getDayOfWeekInInt(Date.newInstance(2015, 10, 3)));

    }

    @isTest static void testGetSubordinateUsers() {

        List<User> testUsers = [SELECT Id, Username FROM User WHERE Username like '%testuser_uts%'];

        List<UserRole> userRoles = new List<UserRole>();
        userRoles.add(NV_Utility.getRoleByName('Regional Manager - Mountain West'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - AZ Phoenix'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - WA Seattle'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - CO Denver'));

        //Update 1st user role
        testUsers[0].UserRoleId = userRoles[0].Id;
        update testUsers[0];
        Integer beforeCount = NV_Utility.getSubordinateUsers(testUsers[0].Id,userRoles[0].Id).size();

        for(Integer i=1; i<4 ; i++){
            testUsers[i].UserRoleId = userRoles[i].Id;
        }
        update testUsers;

        List<User> getUserSubordinates = NV_Utility.getSubordinateUsers(testUsers[0].Id,userRoles[0].Id);
        System.debug(getUserSubordinates);
        System.assertEquals(beforeCount+3,getUserSubordinates.size());
    }

    @isTest static void testGetSubordinateUsersOfSelectedRole() {

        List<User> testUsers = [SELECT Id, Username FROM User WHERE Username like '%testuser_uts%'];

        List<UserRole> userRoles = new List<UserRole>();
        userRoles.add(NV_Utility.getRoleByName('Regional Manager - Mountain West'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - AZ Phoenix'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - WA Seattle'));
        userRoles.add(NV_Utility.getRoleByName('Territory Manager - CO Denver'));

        //Update 1st user role
        testUsers[0].UserRoleId = userRoles[0].Id;
        update testUsers[0];
        Integer beforeCount = NV_Utility.getSubordinateUsersOfSelectedRole(testUsers[0].Id,userRoles[0].Id,'Territory Manager - AZ Phoenix').size();

        for(Integer i=1; i<4 ; i++){
            testUsers[i].UserRoleId = userRoles[i].Id;
        }
        update testUsers;

        List<User> getUserSubordinates = NV_Utility.getSubordinateUsersOfSelectedRole(testUsers[0].Id,userRoles[0].Id, 'Territory Manager - AZ Phoenix');
        System.debug(getUserSubordinates);
        System.assertEquals(beforeCount+1,getUserSubordinates.size());
    }

    @isTest static void testAddNewLine() {

        String orgText = 'Stryker';
        String newText = 'Order';

        String expectedValueAddNewLine = 'Stryker'+'\n'+'Order';

        String testAddNewLine1 = NV_Utility.addNewLine(orgText,newText);
        System.assertEquals(expectedValueAddNewLine, testAddNewLine1 );

        orgText = '';
        String testAddNewLine2 = NV_Utility.addNewLine(orgText,newText);
        System.assertEquals('Order',testAddNewLine2);

    }

    @isTest static void testGetJoinStringFromSet() {
        Set<String> dataSet = new Set<String>{'abc','def','xyz'};
        String separator = '||';
        String expectedOutput1 = 'abc||def||xyz';
        String testJoinString1 = NV_Utility.getJoinStringFromSet(dataSet, separator);
        System.assertEquals(expectedOutput1,testJoinString1);

        Set<String> emptyDataSet = new Set<String>();

        String expectedOutput2 = null;
        String testJoinString2 = NV_Utility.getJoinStringFromSet(emptyDataSet, separator);
        System.assertEquals(expectedOutput2,testJoinString2);

    }


    @isTest static void testGetString() {

        String inp1 = 'Stryker';

        String getstring1 = NV_Utility.getString(inp1);
        System.assertEquals(inp1,getString1);

        String dependent = 'NV';
        String getstring2 = NV_Utility.getString(inp1,dependent);
        String expectedOutput1 = 'Stryker'+'NV';
        System.assertEquals(expectedOutput1,getString2);

        String getstring3 = NV_Utility.getString(null,dependent);
        System.assertEquals('', getstring3);

    }

    @isTest static void testGetFollowUsersForRecords(){
        Order testOrder1 = [SELECT Id FROM Order LIMIT 1];
        List<User> testUsers = [SELECT Id, Username FROM User WHERE Username like '%testuser_uts%'];

        List<NV_Follow__c> testRecords = new List<NV_Follow__c>();
        for(Integer i=0;i<3;i++){
            testRecords.add(new NV_Follow__c(Record_Id__c= testOrder1.Id, Following_User__c = testUsers[i].Id));
        }
        insert testRecords;

        Set<Id> recordIds = new Set<Id>();
        recordIds.add(testOrder1.Id);

        Set<Id> userIds = new Set<Id>();
        Map<Id, Set<Id>> testGetFollowUserForRecords = NV_Utility.getFollowUsersForRecords(recordIds, userIds);

        //There would be 3 followers
        System.assertEquals(3, userIds.size());
        System.assertEquals(3, testGetFollowUserForRecords.get(testOrder1.Id).size());
    }

}