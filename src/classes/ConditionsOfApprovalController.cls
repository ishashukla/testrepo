// (c) 2015 Appirio, Inc.
//
// Class Name: ConditionsOfApprovalController
// Description: Contoller Class for ConditionsOfApproval Related list on Opportunity
//
// July 24 2015, Sunil  Original (S-326226)
// July 27 2015, Naresh Removed with sharing attribute of class(I-173388)
//
public with sharing class ConditionsOfApprovalController {
  //variable
  private final Opportunity opp = null;

  public List<Conditions_of_Approval__c> lstConditionsOfApproval {get;set;}

  //========================================================================//
  //Constructor
  //========================================================================//
  public ConditionsOfApprovalController(ApexPages.StandardController controller) {
    opp = (Opportunity)controller.getRecord();
    lstConditionsOfApproval = loadRecords();
  }

  //=========================================================================
  //Load child records
  //=========================================================================//
  private List<Conditions_of_Approval__c> loadRecords() {
    return [SELECT Status__c, Name, LastModifiedDate, LastModifiedById, Id,
               Description__c, Date_Completed__c, Flex_Credit__r.Name, Completed_by__c, Flex_Credit__c
               FROM Conditions_of_Approval__c
               WHERE Flex_Credit__r.Opportunity__c = :opp.Id];
  }
}