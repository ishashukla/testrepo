/**================================================================      
* Appirio, Inc
* Name: Instruments_OppTriggerHandlerTest 
* Description: Test Class for Instruments_OpportunityTriggerHandler class.
* Created Date: 11 April,2016
* Created By: Isha Shukla (Appirio)
* 
* Date Modified      Modified By      Description of the update
* 11 April 2016      Isha Shukla         Class creation.
* 19 July  2016      Isha Shukla         Modified(T-520730)
* 27 Aug   2016      Isha Shukla         Modified(T-525938)
* 19 Sept  2016      Prakarsh Jain       Modified(T-538449)
* 12 Oct   2016      Shreerath Nair      Modified(T-542917)
* 28 Oct   2016      Prakarsh Jain       Modified(T-551433) Added method testPopulateBUOnFlexContract
==================================================================*/
@isTest
private class Instruments_OppTriggerHandlerTest {
    static Account acc;
    static Opportunity opp;
    static Opportunity opp1;
    static Opportunity opp2;
    static List<Opportunity> oppList;
    static List<Opportunity> oppyList;
    static Id tempUserId ;
    static List<User> usrList;
    static User usrASR;
    static OpportunityTeamMember member;
    static String FLEXFIN;
    static BigMachines__Quote__c quote;
    static BigMachines__Configuration_Record__c site;
    public static RecordType oppInstrumentsRecType = [Select SobjectType, Name, Id, DeveloperName From RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Instruments_Opportunity_Record_Type'];
    public static Id oppFlexConventionalRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();
    public static Account accASR;
    static User adminUser;
    public static List<Opportunity> opptASRList;
    static User usrSalesRep;
    /*@isTest static void testOwnerUpdate1() { 
        Test.startTest();
        createData();
        
        System.runAs(usrList[0]) {
            //Test.startTest();
            //Pricebook2 customPB = new Pricebook2(Name='NSE Price Book', isActive=true);
            //insert customPB;
            Id pricebookId = Test.getStandardPricebookId();
            User_Pricebook_Override__c priceBookOverride = new User_Pricebook_Override__c(Name =usrList.get(0).Id , Is_Active__c = true,Pricebook_ID__c = pricebookId);
            insert  priceBookOverride;
            //Test.stopTest();
        }
        System.runAs(usrList.get(1)) {
            
            opp.Business_Unit__c = 'NSE';
            opp.StageName = 'Submitted To Flex';
            opp.OwnerId = usrList.get(0).Id;
            insert opp;
            quote = TestUtils.createQuoteRecord(acc.Id, opp.Id, site, false);
            quote.BigMachines__Status__c = 'Flex Quoting';
            quote.Financing_Requested__c = True;
            quote.BigMachines__Is_Primary__c = true;
            insert quote;
            member = TestUtils.createOpportunityTeamMember(1,opp.Id,usrList.get(0).Id,false).get(0);
            member.TeamMemberRole = FLEXFIN;
            insert member;
            opp.CloseDate = System.today();
            opp.StageName = 'Final Quote';
            opp.OwnerId = usrList.get(1).Id;
            
            update opp;
            
            
        } 
        Test.stopTest(); 
        System.assertEquals(True ,[SELECT BU_Manager__c FROM Opportunity WHERE OwnerId =:usrList.get(1).Id LIMIT 1].BU_Manager__c == usrList.get(0).Id);
        
    }*/
    @isTest static void testResubmit() { 
        createData();
        System.runAs(usrList.get(1)) {
            opp.Business_Unit__c = 'NSE';
            opp.StageName = 'Submitted to Flex';
            opp.OwnerId = usrList.get(0).Id;
            opp.Submitted_for_Forecast__c = True;
            Test.startTest();
            insert opp;
            member = TestUtils.createOpportunityTeamMember(1,opp.Id,usrList.get(0).Id,false).get(0);
            member.TeamMemberRole = FLEXFIN;
            Test.stopTest();
            insert member;
           
            opp.Parent_Sales_Opportunity__c = Null;
            opp.StageName = 'Closed Won';
            update opp;
            
        }
        System.assertEquals(True ,[SELECT Name FROM Opportunity].size() > 0);
        
    }
    //Testing when StageName Updates to Upside
    @isTest static void testStageNameUpdate() {
        createData();
        opp.Business_Unit__c = 'IVS';
        opp.StageName = 'Final Quote';
        opp.Parent_Sales_Opportunity__c = opp1.Id;
        System.runAs(usrList.get(0)) {
            Test.startTest();
            insert opp;
            member = TestUtils.createOpportunityTeamMember(1,opp.Id,usrList.get(0).Id,false).get(0);
            member.TeamMemberRole = FLEXFIN;
            insert member;
            opp.StageName = 'Upside';
            update opp;
            Test.stopTest(); 
        }
        System.assertEquals('IVS', opp.Business_Unit__c);
    }
    // Test when Opportunity when Business Unit Updates
    @isTest static void testBUUpdate() {
        createData();
        System.runAs(usrList.get(1)) {
            Test.startTest();
            opp.Business_Unit__c = 'NSE';
            opp.StageName = 'Final Quote';
            opp.OwnerId = usrList.get(0).Id;
            opp.Parent_Sales_Opportunity__c = opp1.Id;
            insert opp;
            member = TestUtils.createOpportunityTeamMember(1,opp.Id,usrList.get(0).Id,false).get(0);
            member.TeamMemberRole = FLEXFIN;
            insert member;
            opp.StageName = 'Closed Won';
            opp.Business_Unit__c = 'IVS';
            opp.OwnerId = usrList.get(0).Id;
            update opp;
            Test.stopTest();
        }
        System.assertEquals('IVS', opp.Business_Unit__c);
        System.assertEquals(usrList.get(0).Id, opp.OwnerId);
    }
    
    //Pratz Joshi   August 18, 2016   T-528105
    //This Test method tests that when a new Instrument's Opportunity is created with Stage = 'Closed Won', its Forecast Category is set to Closed
    @isTest static void testInstOppForecastCategoryForStageClosedWon(){
        createData();     
        System.runAs(usrList.get(0)) {
            Test.startTest();
            opp.Business_Unit__c = 'Surgical';
            opp.StageName = 'Closed Won';
            insert opp;           
            Test.stopTest();
            Opportunity oppt = [SELECT Id, Name, StageName, ForecastCategoryName FROM Opportunity WHERE Id =: opp.Id];
            system.assertEquals('Closed', oppt.ForecastCategoryName);    
        }
        
    }  
    
    //Prakarsh Jain   June 16, 2016   T-512007
    //This Test method tests that when a new Opportunity with Forecast Category = 'Omitted' is created
    //the Opportunity.ForecastCategoryName = 'Omitted' AND Opportunity.Freeze_Forecast_Category__c = 'TRUE'
    @isTest static void testOppForecastStageNameInsertOmitted(){
        //createData();
        List<User> usr = TestUtils.createUser(1,null,false);
        usr.get(0).Division = 'NSE;IVS;Navigation;Surgical';
        insert usr;
        System.runAs(usr.get(0)) {
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            Test.startTest();
            Account acct = TestUtils.createAccount(1,true).get(0);
            opp = TestUtils.createOpportunity(1, acct.Id, false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.ForecastCategoryName = 'Omitted';
            opp.StageName = 'In Development';
            opp.recordTypeId = recordTypeInstrument;
            insert opp;
            System.debug('>>>>>>'+opp.ForecastCategoryName);
            Test.stopTest();
            List<Opportunity> oppt = [SELECT Id, Name, StageName, ForecastCategoryName, Freeze_Forecast_Category__c FROM Opportunity WHERE Id =: opp.Id];
            for(Opportunity op : oppt){
                system.assertEquals('Omitted', op.ForecastCategoryName);
                system.assertEquals(TRUE, op.Freeze_Forecast_Category__c);   
            }
            
        }
        
    }
    @isTest static void testOppForecastStageNameInsertNonOmitted(){
        //createData();
        List<User> usr = TestUtils.createUser(1,null,false);
        usr.get(0).Division = 'NSE;IVS;Navigation;Surgical';
        insert usr;
        System.runAs(usr.get(0)) {
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            Account acct = TestUtils.createAccount(1,true).get(0);
            opp = TestUtils.createOpportunity(1, acct.Id, false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.ForecastCategoryName = 'Pipeline';
            opp.StageName = 'In Development';
            opp.recordTypeId = recordTypeInstrument;
            insert opp;
            List<Opportunity> oppt = [SELECT Id, Name, StageName, ForecastCategoryName FROM Opportunity WHERE Id =: opp.Id];
            for(Opportunity op : oppt){
                system.assertNotEquals('Omitted', op.ForecastCategoryName);  
            }
        }
        
    }
    @isTest static void testOppForecastStageNameUpdateOmittedUpdate(){
        //createData();
        List<User> usr = TestUtils.createUser(1,null,false);
        usr.get(0).Division = 'NSE;IVS;Navigation;Surgical';
        insert usr;
        System.runAs(usr.get(0)) {
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            Account acct = TestUtils.createAccount(1,true).get(0);
            opp = TestUtils.createOpportunity(1, acct.Id, false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.ForecastCategoryName = 'Omitted';
            opp.StageName = 'In Development';
            opp.recordTypeId = recordTypeInstrument;
            insert opp;
            opp.StageName = 'Upside';
            update opp;
            List<Opportunity> oppt = [SELECT Id, Name, StageName, ForecastCategoryName, Freeze_Forecast_Category__c FROM Opportunity WHERE Id =: opp.Id];
            for(Opportunity op : oppt){
                system.assertEquals('Omitted', op.ForecastCategoryName);
                system.assertEquals(TRUE, op.Freeze_Forecast_Category__c);    
            }
        }
        
    }
    //This Test method tests that when even if an Opportunity's Stage is updated to 'Closed Won', its Forecast Category changes to 'Closed'
    @isTest static void testOppForecastStageNameUpdateClosedWon(){
        createData();
        System.runAs(usrList.get(0)) {
            Test.startTest();
            opp.Business_Unit__c = 'NSE';
            opp.StageName = 'Closed Lost';
            insert opp;
            opp.StageName = 'Closed Won';
            update opp;
            Test.stopTest(); 
            Opportunity oppt = [SELECT Id, Name, StageName, ForecastCategoryName FROM Opportunity WHERE Id =: opp.Id];
            
            System.assertEquals('Closed', oppt.ForecastCategoryName);    
        }
    }
    //T-512007  Changes End
    
    // Test when Owner and Business Unit Updates 
    @isTest static void testWhenBUandOwnerUpdate() {
        createData();
        User usr = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr.Division = 'NSE;IVS';
        insert usr;
        List<Mancode__c> mancodeList2 = TestUtils.createMancode(2, usrList.get(0).Id, usrList.get(1).Id, false);
        mancodeList2[1].Sales_Rep__c = usrList.get(1).Id;
        mancodeList2[1].Manager__c = usr.Id;
        mancodeList2[1].Business_Unit__c = 'NSE';
        opp.Business_Unit__c = 'IVS';
        opp.StageName = 'Closed Won';
        opp.Parent_Sales_Opportunity__c = opp1.Id;
        opp.OwnerId = usrList.get(0).Id;
        System.runAs(usrList.get(0)) {
            Test.startTest();
            insert opp;
            member = TestUtils.createOpportunityTeamMember(1,opp.Id,usrList.get(0).Id,false).get(0);
            member.TeamMemberRole = FLEXFIN;
            insert member;
            opp.StageName = 'Final Quote';
            opp.Business_Unit__c = 'NSE';
            opp.OwnerId = usrList.get(1).Id;
            update opp;
            Test.stopTest();  
            System.assertEquals([SELECT userOrGroupId 
                                 FROM OpportunityShare 
                                 WHERE userOrGroupId = :usrList.get(1).Id 
                                 AND OpportunityId = :opp.Id].userOrGroupId,usrList.get(1).Id );
        }
    }
    @isTest static void testWhenOppDelete() {
        createData();
        opp.Business_Unit__c = 'IVS';
        opp.StageName = 'Closed Won';
        opp.Parent_Sales_Opportunity__c = opp1.Id;
        opp.OwnerId = usrList.get(0).Id;
        System.runAs(usrList.get(0)) {
            Test.startTest();
            insert opp;
            member = TestUtils.createOpportunityTeamMember(1,opp.Id,usrList.get(0).Id,false).get(0);
            member.TeamMemberRole = FLEXFIN;
            insert member;
            opp.StageName = 'Final Quote';
            opp.Business_Unit__c = 'Surgical';
            opp.OwnerId = usrList.get(1).Id;
            update opp;
            delete opp;
            Test.stopTest();   
        }
        
    }
    @isTest(SeeAllData=true)
    static void testLast() {
        User usr = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr.Division = 'IVS';
        insert usr;
        system.runAs(usr) {
            // creating test data
            Test.startTest();
            Account acct = TestUtils.createAccount(1,true).get(0);
            Opportunity oppy = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            oppy.Business_Unit__c = 'IVS';
            //oppy.Opportunity_Number__c = '501002';
            oppy.OwnerId = usr.Id;
            oppy.RecordTypeId = oppInstrumentsRecType.Id;
            oppy.StageName = 'Final Quote';
            insert oppy;
            OpportunityTeamMember otm  = TestUtils.createOpportunityTeamMember(1,oppy.Id,usr.Id,false).get(0);
            otm.TeamMemberRole = FLEXFIN;
            insert otm;
            AccountTeamMember atm = TestUtils.createAccountTeamMember(1, acct.Id, usr.Id, false).get(0);
            atm.TeamMemberRole = FLEXFIN;
            insert atm;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(acct.Id, oppy.Id, sites, false);
            quotes.BigMachines__Is_Primary__c = true;
            //quotes.Financing_Requested__c = true;
            //quotes.BigMachines__Status__c = 'Flex';
            insert quotes;
            oppy.StageName = 'Submitted to Flex';
            oppy.Parent_Sales_Opportunity__c = null;
            update oppy;
            Test.stopTest();
            System.assertEquals(oppy.StageName, 'Submitted to Flex');   
        }   
    }
    @isTest static void testsetInstrumentsOwnerAsOppTeamMember() {
        User insUser = TestUtils.createUser(1, 'Instruments Sales User', false).get(0);
        insUser.Division = 'Surgical';
        insert insUser;
        User flexUser = TestUtils.createUser(1, 'Flex - Sales', false).get(0);
        flexUser.Division = 'Surgical';
        insert flexUser;
        adminUser = TestUtils.createUser(1, 'System Administrator', true).get(0);
        Account acct;
        Opportunity flexOpp;
        Test.startTest();
        System.runAs(adminUser) {
            acct = TestUtils.createAccount(1,true).get(0);
            Opportunity oppy = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            oppy.Business_Unit__c = 'Surgical';
            oppy.Opportunity_Number__c = '50001';
            oppy.OwnerId = insUser.Id;
            oppy.RecordTypeId = oppInstrumentsRecType.Id;
            oppy.StageName = 'Final Quote';
            insert oppy;
            OpportunityTeamMember otm  = TestUtils.createOpportunityTeamMember(1,oppy.Id,flexUser.Id,false).get(0);
            otm.TeamMemberRole = FLEXFIN;
            insert otm;
            flexOpp = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            flexOpp.Business_Unit__c = 'Surgical';
            flexOpp.Opportunity_Number__c = '50003';
            flexOpp.OwnerId = flexUser.Id;
            flexOpp.RecordTypeId = oppFlexConventionalRecordTypeId;
            flexOpp.StageName = 'Final Quote';
            flexOpp.Parent_Sales_Opportunity__c = oppy.Id;
            insert flexOpp;
        }
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Opportunity WHERE Id = :flexOpp.Id].size() > 0 , true);
        
    }
    @isTest static void testupdateSharingonOpportunity() {
        User usr = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr.Division = 'IVS';
        insert usr;
        User usr1 = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr1.Division = 'IVS;NSE';
        insert usr1;
        system.runAs(usr) {
            // creating test data
            Account acct = TestUtils.createAccount(1,true).get(0);
            Opportunity oppy = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            oppy.Business_Unit__c = 'IVS';
            oppy.Opportunity_Number__c = '501002';
            oppy.OwnerId = usr.Id;
            oppy.RecordTypeId = oppInstrumentsRecType.Id;
            oppy.StageName = 'Final Quote';
            oppy.Is_Deleted__c = true;
            insert oppy;
            OpportunityTeamMember otm  = TestUtils.createOpportunityTeamMember(1,oppy.Id,usr.Id,false).get(0);
            otm.TeamMemberRole = FLEXFIN;
            insert otm;
            AccountTeamMember atm = TestUtils.createAccountTeamMember(1, acct.Id, usr.Id, false).get(0);
            atm.TeamMemberRole = FLEXFIN;
            insert atm;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(acct.Id, oppy.Id, sites, false);
            quotes.BigMachines__Is_Primary__c = true;
            quotes.Financing_Requested__c = true;
            quotes.BigMachines__Status__c = 'Flex';
            insert quotes;
            oppy.StageName = 'Submitted to Flex';
            oppy.OwnerId = usr1.Id;
            //oppy.Parent_Sales_Opportunity__c = null;
            oppy.Is_Deleted__c = false;
            Test.startTest();
            update oppy;
            Test.stopTest();
            System.assertEquals(oppy.StageName, 'Submitted to Flex'); 
        }
    }
    @isTest static void testUpdateSharingonOppty() {
        User usr = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr.Division = 'IVS';
        insert usr;
        User usr1 = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr1.Division = 'IVS;NSE';
        insert usr1;
        system.runAs(usr) {
            // creating test data
            Account acct = TestUtils.createAccount(1,true).get(0);
            Opportunity oppy = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            oppy.Business_Unit__c = 'IVS';
            oppy.Opportunity_Number__c = '501002';
            oppy.OwnerId = usr.Id;
            oppy.RecordTypeId = oppInstrumentsRecType.Id;
            oppy.StageName = 'Final Quote';
            oppy.Is_Deleted__c = true;
            insert oppy;
            OpportunityTeamMember otm  = TestUtils.createOpportunityTeamMember(1,oppy.Id,usr.Id,false).get(0);
            otm.TeamMemberRole = FLEXFIN;
            insert otm;
            AccountTeamMember atm = TestUtils.createAccountTeamMember(1, acct.Id, usr.Id, false).get(0);
            atm.TeamMemberRole = FLEXFIN;
            insert atm;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(acct.Id, oppy.Id, sites, false);
            quotes.BigMachines__Is_Primary__c = true;
            quotes.Financing_Requested__c = true;
            quotes.BigMachines__Status__c = 'Flex';
            insert quotes;
            oppy.StageName = 'Submitted to Flex';
            oppy.OwnerId = usr1.Id;
            oppy.Business_Unit__c = 'NSE';
            //oppy.Parent_Sales_Opportunity__c = null;
            oppy.Is_Deleted__c = false;
            Test.startTest();
            update oppy;
            Test.stopTest();
            System.assertEquals(oppy.StageName, 'Submitted to Flex');
            
        }
    }
    @isTest static void testupdateSharingonOpp() {
        User usr = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr;
        system.runAs(usr) {
            // creating test data
            Test.startTest();
            Account acct = TestUtils.createAccount(1,true).get(0);
            Opportunity oppy = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            oppy.Business_Unit__c = 'IVS';
            oppy.Opportunity_Number__c = '501002';
            oppy.OwnerId = usr.Id;
            oppy.RecordTypeId = oppInstrumentsRecType.Id;
            oppy.StageName = 'Final Quote';
            oppy.Is_Deleted__c = true;
            insert oppy;
            OpportunityTeamMember otm  = TestUtils.createOpportunityTeamMember(1,oppy.Id,usr.Id,false).get(0);
            otm.TeamMemberRole = FLEXFIN;
            insert otm;
            AccountTeamMember atm = TestUtils.createAccountTeamMember(1, acct.Id, usr.Id, false).get(0);
            atm.TeamMemberRole = FLEXFIN;
            insert atm;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(acct.Id, oppy.Id, sites, false);
            quotes.BigMachines__Is_Primary__c = true;
            quotes.Financing_Requested__c = true;
            quotes.BigMachines__Status__c = 'Flex';
            insert quotes;
            oppy.StageName = 'Submitted to Flex';
            oppy.OwnerId = usr.Id;
            oppy.Business_Unit__c = 'NSE';
            //oppy.Parent_Sales_Opportunity__c = null;
            oppy.Is_Deleted__c = false;
            update oppy;
            System.assertEquals(oppy.StageName, 'Submitted to Flex');
            Test.stopTest();
        }
    }
    
    //Prakarsh Jain  19 Sep, 2016  Modified (T-538449) - Test Method to check ASR User is added to Opportunity Team Member with Read/Write Access on Opportunity if the Opportunity is created by ASR User.
    static testMethod void testAddASRUserToOppTeam(){
      opptASRList = new List<Opportunity>();
      Set<Id> oppIdsSet = new Set<Id>();
      Test.startTest();
        insertUser(false);
      Test.stopTest();
      System.runAs(adminUser) {
        accASR = TestUtils.createAccount(1, true).get(0);
        AccountTeamMember accountMemberASR = TestUtils.createaccountTeamMember(1,accASR.Id,usrASR.Id,false).get(0);
        accountMemberASR.TeamMemberRole = 'IVS';
        insert accountMemberASR;
      }
      
      system.runAs(usrASR){
        opptASRList = TestUtils.createOpportunity(10, accASR.Id, false);
        for(Opportunity opptASR : opptASRList){
          opptASR.RecordTypeId = oppInstrumentsRecType.Id;
            opptASR.Business_Unit__c = 'IVS';
            opptASR.StageName = 'In Development';
            opptASR.OwnerId = usrASR.Id;
        }
        insert opptASRList;
      }
      for(Opportunity oppIds : opptASRList){
        oppIdsSet.add(oppIds.Id);
      }
      List<OpportunityTeamMember> oppTeamMemberList = [SELECT UserId,OpportunityAccessLevel FROM OpportunityTeamMember WHERE 
                                                       OpportunityId IN: oppIdsSet];
      for(OpportunityTeamMember otm : oppTeamMemberList){
        system.assertEquals(otm.OpportunityAccessLevel,'Edit');
      }
      
    }
    
    static testMethod void testAddASRUserToOppTeamIsAddedAlready(){
      opptASRList = new List<Opportunity>();
      Set<Id> oppIdsSet = new Set<Id>();
      Test.startTest();
        insertUser(true);
      Test.stopTest();
      System.runAs(adminUser) {
        accASR = TestUtils.createAccount(1, true).get(0);
        AccountTeamMember accountMemberASR = TestUtils.createaccountTeamMember(1,accASR.Id,usrASR.Id,false).get(0);
        accountMemberASR.TeamMemberRole = 'IVS';
        insert accountMemberASR;
        AccountTeamMember accountMemberSalesRep = TestUtils.createaccountTeamMember(1,accASR.Id,usrSalesRep.Id,false).get(0);
        accountMemberSalesRep.TeamMemberRole = 'IVS';
        insert accountMemberSalesRep;
      }
      
      system.runAs(usrASR){
        opptASRList = TestUtils.createOpportunity(10, accASR.Id, false);
        for(Opportunity opptASR : opptASRList){
          opptASR.RecordTypeId = oppInstrumentsRecType.Id;
          opptASR.Business_Unit__c = 'IVS';
          opptASR.StageName = 'In Development';
          opptASR.OwnerId = usrASR.Id;
        }
        insert opptASRList;
        for(Opportunity opptt : opptASRList){
          opptt.OwnerId = usrSalesRep.Id;
        }
        update opptASRList;
        for(Opportunity oppIds : opptASRList){
          oppIdsSet.add(oppIds.Id);
        }
        List<OpportunityTeamMember> oppTeamMemberList = [SELECT UserId,OpportunityAccessLevel FROM OpportunityTeamMember WHERE 
                                                         OpportunityId IN: oppIdsSet AND UserId =: usrASR.Id];
        system.assertEquals(oppTeamMemberList.size()>0,true);                                                  
      }
    }
    
    //Method to populate busines unit field on flex contract with the business unit value of the parent sales opportunity on the 
    //opportunity to which the flex contract is related to. 
    static testMethod void testPopulateBUOnFlexContract(){
      User usrSysAdmin = TestUtils.createUser(1,null,false).get(0);
      usrSysAdmin.Division = 'NSE;IVS;Navigation;Surgical';
      insert usrSysAdmin;
      system.RunAs(usrSysAdmin){
        Test.startTest();
            Account acct = TestUtils.createAccount(1, true).get(0);
              List<Opportunity> oppListInstruments = TestUtils.createOpportunity(1, acct.Id, false);
              List<Opportunity> oppListFlex = TestUtils.createOpportunity(10, acct.Id, false);
              oppListInstruments[0].RecordTypeId = oppInstrumentsRecType.Id;
              oppListInstruments[0].Type = 'Standard';
              oppListInstruments[0].CloseDate = Date.Today();
              oppListInstruments[0].StageName = 'In Development';
              oppListInstruments[0].ForecastCategoryName = 'Pipeline';
              oppListInstruments[0].Business_Unit__c = 'IVS';
              insert oppListInstruments;
              List<Opportunity> oppListFlexToInsert = new List<Opportunity>();
              List<Opportunity> oppListFlexToUpdate = new List<Opportunity>();
              for(Opportunity tempOpp : oppListFlex){
                tempOpp.RecordTypeId = oppFlexConventionalRecordTypeId;
                tempOpp.Parent_Sales_Opportunity__c = oppListInstruments[0].Id;
                tempOpp.Type = 'Rollover Business';
                tempOpp.StageName = 'Quoting';
                tempOpp.CloseDate = Date.Today();
                tempOpp.Deal_Type__c = 'Usage Rental';
                oppListFlexToInsert.add(tempOpp);
              }
              insert oppListFlexToInsert;
              Set<Id> flexOppIds = new Set<Id>();
              for(Opportunity tempOppFlex : oppListFlexToInsert){
                flexOppIds.add(tempOppFlex.Id);
                tempOppFlex.StageName = 'Closed Won';
                oppListFlexToUpdate.add(tempOppFlex);
              }
              update oppListFlexToUpdate;
              List<Flex_Contract__c> flexContractList = [SELECT Id, Name, Opportunity__c, Business_Unit__c FROM Flex_Contract__c WHERE Opportunity__c IN: flexOppIds];
              for(Flex_Contract__c flexContract : flexContractList){
                system.assertEquals(flexContract.Business_Unit__c, oppListInstruments[0].Business_Unit__c);
              }
            Test.stopTest();
      }
    }
    
    //T-552902(SN) test method positive to restrict update of Opportunity's Amount if one or more Oracle quote is related to Opportunity
    @isTest static void testRestrictUpdateOfOpportunityAmountPositive() {
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr;
        system.runAs(usr) {
            // creating test data
            
            insert new Messages__c(Name='RestrictOpportunityAmount',Message_Text__c= 'You cannot update Opportunity Amount if CPQ Quote is associated with the Opportunity');
            Account acct = TestUtils.createAccount(1,true).get(0);
            Opportunity oppy = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            oppy.Business_Unit__c = 'IVS';
            oppy.Opportunity_Number__c = '501002';
            oppy.OwnerId = usr.Id;
            oppy.RecordTypeId = oppInstrumentsRecType.Id;
            oppy.StageName = 'Final Quote';
            oppy.Is_Deleted__c = true;
            insert oppy;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(acct.Id, oppy.Id, sites, false);
            quotes.BigMachines__Is_Primary__c = true;
            insert quotes;
            oppy.StageName = 'Submitted to Flex';
            oppy.OwnerId = usr.Id;
            oppy.Business_Unit__c = 'NSE';
            oppy.Amount = 12;
            oppy.Is_Deleted__c = false;
            Test.startTest();
            
            try{
                update oppy;
            }
            catch(Exception ex){
                System.AssertEquals(ex.getMessage().contains('You cannot update Opportunity Amount'),true);  
            }
            Test.stopTest();
        }
    }
    
    
    //T-552902(SN) test method Negative to restrict update of Opportunity's Amount if one or more Oracle quote is related to Opportunity
    @isTest static void testRestrictUpdateOfOpportunityAmountNegative() {
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr;
        system.runAs(usr) {
            // creating test data
            
            insert new Messages__c(Name='RestrictOpportunityAmount',Message_Text__c= 'You cannot update Opportunity Amount if CPQ Quote is associated with the Opportunity');
            Account acct = TestUtils.createAccount(1,true).get(0);
            Opportunity oppy = TestUtils.createOpportunity(1,acct.Id,false).get(0);
            oppy.Business_Unit__c = 'IVS';
            oppy.Opportunity_Number__c = '501002';
            oppy.OwnerId = usr.Id;
            oppy.RecordTypeId = oppInstrumentsRecType.Id;
            oppy.StageName = 'Final Quote';
            oppy.Is_Deleted__c = true;
            insert oppy;
           
            oppy.StageName = 'Submitted to Flex';
            oppy.OwnerId = usr.Id;
            oppy.Business_Unit__c = 'NSE';
            oppy.Amount = 12;
            oppy.Is_Deleted__c = false;
            
            Test.startTest();
            
            update oppy;
            Opportunity oppty = [SELECT Amount FROM Opportunity WHERE ID =: oppy.Id][0];
            System.AssertEquals(oppty.Amount,12);
           
            Test.stopTest();
        }
    }
    
    //T-552902(SN) test method bulk to restrict update of Opportunity's Amount if one or more Oracle quote is related to Opportunity
    @isTest static void testRestrictUpdateOfOpportunityAmountForBulk() {
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr;
        system.runAs(usr) {
            // creating test data
            
            insert new Messages__c(Name='RestrictOpportunityAmount',Message_Text__c= 'You cannot update Opportunity Amount if CPQ Quote is associated with the Opportunity');
            Account acct = TestUtils.createAccount(1,true).get(0);
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            List<Opportunity> opptyList = TestUtils.createOpportunity(10,acct.Id,false);
            
            List<BigMachines__Quote__c> quoteList = new List<BigMachines__Quote__c>();
            for(Opportunity oppt : opptyList){
                oppt.RecordTypeId = oppInstrumentsRecType.Id;
                oppt.Business_Unit__c = 'IVS';
                oppt.OwnerId = usr.Id;
                oppt.RecordTypeId = oppInstrumentsRecType.Id;
                oppt.StageName = 'Final Quote';
                oppt.Is_Deleted__c = true;
                
                
            }
            insert opptyList;
            
            for(Opportunity oppt : opptyList){
                BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(acct.Id, oppt.Id, sites, false);
                quotes.BigMachines__Is_Primary__c = true;
                quotes.Financing_Requested__c = true;
                quotes.BigMachines__Status__c = 'Flex';
                quoteList.add(quotes);
            }
            insert quoteList;
            
            for(Opportunity oppt : opptyList){
                oppt.Amount = 12;
                oppt.Is_Deleted__c = false;
            }
            Test.startTest();
            try{
                update opptyList;
            }
            catch(Exception ex){
                System.AssertEquals(ex.getMessage().contains('You cannot update Opportunity Amount'),true);  
            }
            
            Test.stopTest();
        }
    }
    
    
    
    public static void createData() {
        usrList = TestUtils.createUser(2,null,false);
        usrList.get(0).Division = 'NSE;IVS;Navigation;Surgical';
        usrList.get(1).Division = 'NSE;IVS;Navigation;Surgical';
        insert usrList;
        usrASR = TestUtils.createUser(1,'Instruments ASR User',false).get(0);
        usrASR.Division = 'IVS';
        usrASR.Title = 'ASR';
        insert usrASR;
          system.debug('usrASR>>>'+usrASR.ProfileId);
          System.runAs(usrList[0]) {
            FLEXFIN = Label.Flex_Financial_Role;
            tempUserId = system.UserInfo.getUserId();
            System.debug('usrList>>'+usrList);
            acc = TestUtils.createAccount(1, true).get(0);
            
            AccountTeamMember accountMember = TestUtils.createaccountTeamMember(1,acc.Id,usrList.get(0).Id,false).get(0);
            accountMember.TeamMemberRole = FLEXFIN;
            insert accountMember;
            AccountTeamMember accountMemberASR = TestUtils.createaccountTeamMember(1,acc.Id,usrASR.Id,false).get(0);
                accountMemberASR.TeamMemberRole = 'IVS';
                insert accountMemberASR;
            tempUserId = usrList.get(1).Id;
            
            //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=usrASR.Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
            //insert terr;
            //  List<Mancode__c> BUassignment =  TestUtils.createMultiBUassignment(2,usrList.get(0).Id,tempUserId,false);
            List<Mancode__c> mancodeList = TestUtils.createMancode(2, usrList.get(0).Id, tempUserId, false);
            //mancodeList.get(0).Territory__c = terr.Id;
            mancodeList.get(0).Manager__c = usrList.get(0).Id;
            mancodeList.get(0).Business_Unit__c = 'NSE';
            mancodeList.get(0).Sales_Rep__c = usrList.get(1).Id;
            mancodeList.get(1).Business_Unit__c = 'NSE';
            //mancodeList.get(1).Territory__c = terr.Id;
            insert mancodeList;
            
            opp = TestUtils.createOpportunity(1, acc.Id, false).get(0);
            opp.RecordTypeId = oppInstrumentsRecType.Id;
            opp1 = TestUtils.createOpportunity(1, acc.Id, false).get(0);
            oppList = TestUtils.createOpportunity(10, acc.Id, false);
            oppyList = TestUtils.createOpportunity(10, acc.Id, false);
            opp1.Business_Unit__c = 'NSE';
            opp1.StageName = 'Final Quote';
            opp1.OwnerId = usrList.get(0).Id;
            opp1.Parent_Sales_Opportunity__c = opp1.Id;
            opp1.RecordTypeId = oppInstrumentsRecType.Id;
            insert opp1;
            
            for(Opportunity oppt : oppList){
                oppt.Business_Unit__c = 'NSE';
                oppt.StageName = 'Closed Lost';
                oppt.OwnerId = usrList.get(0).Id;
                oppt.RecordTypeId = oppInstrumentsRecType.Id;
            }
            
            insert oppList;
            
            for(Opportunity oppt : oppyList){
                oppt.Business_Unit__c = 'NSE';
                oppt.StageName = 'In Development';
                oppt.OwnerId = usrList.get(0).Id;
                oppt.RecordTypeId = oppInstrumentsRecType.Id;
            }
            
            insert oppyList;
            Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
            Id recordTypeRep = rtByNameRep.getRecordTypeId();
            Forecast_Year__c fyear1 = new Forecast_Year__c(Year__c='2016',Yearly_Quota__c=12000,Division__c='Instrument',RecordTypeId = recordTypeRep,User2__c=usrList.get(1).Id,Business_Unit__c='NSE',Manager__c=usrList.get(0).Id);
            insert fyear1;
            Monthly_Forecast__c monthlyf = new Monthly_Forecast__c(Month__c= String.valueOf(System.today().month()),Forecast_Year__c = fyear1.Id,Bill_Only_Forecast__c=-12000 );
            insert monthlyf;
            site = new BigMachines__Configuration_Record__c(BigMachines__action_id_copy__c = '18793335',
                                                            BigMachines__action_id_open__c = '0393544',
                                                            BigMachines__bm_site__c = '4345453',
                                                            BigMachines__document_id__c = '5334322',
                                                            BigMachines__process__c = '90293233',
                                                            BigMachines__process_id__c = '483212949',
                                                            BigMachines__version_id__c = '73421844',
                                                            BigMachines__Is_Active__c = true                                                                                           
                                                           );
            insert site;
        }
        }
         @future
        public static void insertUser(Boolean method1){
          adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
          insert adminUser;
          usrASR = TestUtils.createUser(1,'Instruments ASR User',false).get(0);
          usrSalesRep = TestUtils.createUser(1,'Instruments Sales User',false).get(0);
          usrASR.Division = 'IVS';
          usrASR.Title = 'ASR';
          insert usrASR;
          if(method1) {
            usrSalesRep.Division = 'IVS';
            usrSalesRep.Title = 'Sales Rep';
            insert usrSalesRep;
          }
        }       
    }