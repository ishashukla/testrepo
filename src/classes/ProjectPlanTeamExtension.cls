/*
Name            ProjectPlanTeamExtension
Purpose         T-500923 (To show project team records)
Created Date    05/06/2016
Created By      Nitish Bansal
Updated By      Rahul Aeran T-500952 (Adding some default opportunity memebers as per role)
Updated By      Deepti Maheshwari (REf : I-223272)
Updated By      Deepti Maheshwari (ref: I-226753 Setting project access level in variable on custom account team)
*/

public class ProjectPlanTeamExtension {
    
    String PROJECT_TEAM_RECORDTYPE_ID;
    public List<ProjectTeamWrapper> projectTeamMembers {get;set;}
    public List<ProjectTeamWrapper> newProjectTeamMembers {get;set;}
    public Project_Plan__c project {get;set;}
    public Boolean showAddMemberSection {get;set;}
    public Boolean displayAccess {get;set;}
    public List<selectOption> projectAccessLevels {get;set;}
    public Id selectedId {get;set;}
    public Integer listSize {get;set;}
    public List<ProjectTeamWrapper> projectTeamMembersToShow {get;set;}
    public Integer currentSizeShown {get;set;}
    public Boolean reachedMax {get;set;}

    public ProjectPlanTeamExtension(ApexPages.Standardcontroller std) {
        PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
        showAddMemberSection = false;
        displayAccess = false;
        currentSizeShown = 5;
        reachedMax = false;
        projectTeamMembersToShow = new List<ProjectTeamWrapper>();
        projectAccessLevels = new List<selectOption>();
        projectAccessLevels.add(new selectOption('Read','Read Only'));
        projectAccessLevels.add(new selectOption('Edit','Read/Write'));

        projectTeamMembers = new List<ProjectTeamWrapper>();
        Id projId = std.getRecord().Id;
        project = null;
        if(projId != null) {
          project = [SELECT Id,Name, OwnerId FROM Project_Plan__c WHERE Id = :projId];
          //system.assert(false, project);
          fetchExistingProjectTeamMembers();
          //system.assert(false, projectTeamMembersToShow);
        }

        listSize = projectTeamMembers.size();
        if( listSize == 5) {
            reachedMax = true;
        }
        /*
        if( ApexPages.currentPage().getParameters().get('showAddMemberSection') != null && 
         ApexPages.currentPage().getParameters().get('showAddMemberSection') != '' ) {
          addTeamMembers();
        }*/
    }

    //==========================================================================
    // Wrapper class to hold the Team Member record as well as their projectess levels
    //==========================================================================
    public class ProjectTeamWrapper {
        public Custom_Account_Team__c member {get;set;}
        public String projectAccess {get;set;}
        
        public ProjectTeamWrapper() {
          member = new Custom_Account_Team__c();
          projectAccess = '';
        }
    }

    //==========================================================================
    // Method to fetch the existing Project Team Members and their access levels
    //==========================================================================
    public void fetchExistingProjectTeamMembers () {
        projectTeamMembers = new List<ProjectTeamWrapper> ();
        projectTeamMembersToShow = new List<ProjectTeamWrapper>();
        Integer i = 0;
        map<Id,Project_Plan__Share> projectShareMap = new map<Id,Project_Plan__Share>();
        for ( Project_Plan__Share projectShare : [SELECT Id, AccessLevel, UserOrGroupId, ParentId
                                       FROM Project_Plan__Share
                                       WHERE ParentId = :project.Id]) {
          projectShareMap.put(projectShare.UserOrGroupId, projectShare);
        }
        for ( Custom_Account_Team__c projectMem : [SELECT Id, Team_Member_Role__c, Project__c, User__c,
                                              Account_Access_Level__c, Project__r.OwnerID, 
                                              User__r.Name, Project_Access_Level__c
                                       FROM Custom_Account_Team__c 
                                       WHERE Project__c = :project.Id AND RecordTypeId =:PROJECT_TEAM_RECORDTYPE_ID
                                       order by User__r.Name ASC]) {
          ProjectTeamWrapper projectWrap = new ProjectTeamWrapper();
          projectWrap.member = projectMem;
          if(projectShareMap != null && projectShareMap.containsKey(projectMem.User__c)) {
            Project_Plan__Share projectShare = projectShareMap.get(projectMem.User__c);
            projectWrap.projectAccess = projectShare.AccessLevel;
          }
          projectTeamMembers.add(projectWrap);
          if( i < 5) {
            projectTeamMembersToShow.add(projectWrap);
            i++;
          }
          
        }
    }

    //==========================================================================
    // Method to show the access levels on the list view
    //==========================================================================
    public void showAccess (){
        displayAccess = true;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Project team members may have greater access than defined by their project team membership.'));
    }

    //==========================================================================
    // Method to show next set of records in the detail page
    //==========================================================================
    public void showMoreRecords() {
        Integer loopEndVariable;
        if( currentSizeShown+5 < projectTeamMembers.size() ) {
          loopEndVariable = currentSizeShown+5;
        }
        else {
          loopEndVariable = projectTeamMembers.size();
          reachedMax = true;
        }
        for( Integer i=currentSizeShown; i < loopEndVariable; i++) {
          projectTeamMembersToShow.add(projectTeamMembers[i]);
        }
        currentSizeShown = projectTeamMembersToShow.size();
    }

    //==========================================================================
    // Method to delete the account team member records
    //==========================================================================
    public PageReference doDelete() {
        try{
          if ( selectedId != null ) {
            integer indexToDel = -1;
            for (ProjectTeamWrapper atWrap :projectTeamMembers) {
              indexToDel++;
              if ( atWrap.member.Id == selectedId ) {
                break;
              }
            }
            if( indextoDel != -1) {
              projectTeamMembers.remove(indextoDel);
            }
            Custom_Account_Team__c atmToDel = [SELECT Id, User__c, Project__c FROM Custom_Account_Team__c WHERE Id = :selectedId];
            delete atmToDel;
            //removeContactShare(atmToDel.User__c, atmToDel.AccountID);
            removeDocAccess(atmToDel.User__c, atmToDel.Project__c); //NB - 05/09 - T-500928
            //OpportunityTeamMember opptyTeam = [SELECT Id, UserId,OpportunityId FROM OpportunityTeamMember WHERE Id =: selectedId];
            //removeDocumentAccess(opptyTeam.UserId, opptyTeam.OpportunityId);
            fetchExistingProjectTeamMembers();
          }
        }
        catch ( exception ex) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
        }
        return null;
    }

    //NB - 05/09 - T-500928
    //Delete document share object for the project team member being deleted
    public void removeDocAccess(Id userId, Id projId) {
        List<Id> docIdlist = new List<Id>();

        for(Document__c oppDoc : [Select Id from Document__c where Project_Plan__c = :projId]) {
            docIdlist.add(oppDoc.Id);
        }

        List<Document__Share> removeDocShareRecords = [Select Id, ParentID from Document__Share 
                                                        where rowCause = :Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c
                                                        And UserOrGroupID = :userId And ParentID in :docIdlist];
        if(removeDocShareRecords != null && removeDocShareRecords.size() > 0){
            delete removeDocShareRecords;
        }                                               
    }
    
    /*public void removeDocumentAccess(Id userId, Id opptyId) {
      Set<Id> docIdSet = new Set<Id> ();
      for(Document__c optyDoc : [SELECT Id, Opportunity__c, Oracle_Quote__r.BigMachines__Opportunity__r.id FROM Document__c WHERE (Opportunity__c =: opptyId OR 
                                Oracle_Quote__r.BigMachines__Opportunity__r.id =: opptyId) ]) {
                                  docIdSet.add(optyDoc.Id);
                                }
      List<Document__Share> removeDocShareRecords = [Select Id, ParentID from Document__Share 
                                                        where rowCause = :Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c
                                                        And UserOrGroupID = :userId And ParentID in :docIdSet];
        if(removeDocShareRecords.size() > 0){
            delete removeDocShareRecords;
        }                               
    }*/
    
    //==========================================================================
    // Method to create default clone of opportunity team member as per T-500952
    //==========================================================================
    public PageReference addOppTeamMemberWithSpecificRoles(){
      Set<String> setRoles = new Set<String>{Constants.PROJECT_ENGINEER,Constants.ROLE_PM,Constants.ROLE_RSM,Constants.ROLE_SALES_REP};
      Set<String> setExistingUserWithRoles = new Set<String>();
      List<Custom_Account_Team__c> lstExistingTeamMembers = [SELECT Id,Account__c,Project__c,User__c,Team_Member_Role__c
                                                              FROM Custom_Account_Team__c WHERE Project__c =:project.Id
                                                              AND Team_Member_Role__c IN :setRoles];
      for(Custom_Account_Team__c cat : lstExistingTeamMembers){
        setExistingUserWithRoles.add(cat.User__c + Constants.TILDE_DELIMITER + cat.Team_Member_Role__c);
      }
      List<Opportunity> lstOpportunities = [SELECT Id,AccountId,(SELECT Id,UserId,TeamMemberRole FROM OpportunityTeamMembers WHERE TeamMemberRole IN: setRoles)
                                              FROM Opportunity WHERE Project_Plan__c = :project.Id];
      
     Map<Id, Set<Id>> opptyUserIdMap = new Map<Id, Set<Id>>();
     Set<Id> userIdSet = new Set<Id>();
     Set<Id> docIdSet = new Set<Id>();
      //We need to create all the new memebers which are not there with that role
      String key =  null;
      List<Custom_Account_Team__c> lstNewMembersToInsert = new List<Custom_Account_Team__c>(); 
      List<Project_Plan__Share> projectShareList = new List<Project_Plan__Share>(); // NB - 05/10 - T-500927
      List<RoleAccessProjectSetting__c> roleAccessSettings = [Select Name, Project_Access__c from RoleAccessProjectSetting__c]; // NB - 05/10 - T-500927
      Map<Id, Set<Id>> opptyDocIdSetMap = new Map<Id, Set<Id>>();
      //Map<Id, List<BigMachines__Quote__c>> opptyQuoteDocList = new Map<Id, List<BigMachines__Quote__c>>();
      Id rtProjectTeam = Constants.getRecordTypeId(Constants.RT_CUSTOM_ACC_TEAM_PROJECT_TEAM,Constants.CUSTOM_ACCOUNT_TEAM);
      for(Opportunity opp : lstOpportunities){
        for(OpportunityTeamMember otm : opp.OpportunityTeamMembers){
          key = otm.UserId + Constants.TILDE_DELIMITER + otm.TeamMemberRole;
          userIdSet.add(otm.UserId);
        opptyUserIdMap.put(opp.Id,userIdSet);
          if(!setExistingUserWithRoles.contains(key)){
            //Adding it to the set so that the same user record doesn't gets created from other opportunity
            setExistingUserWithRoles.add(key);
            //we will create the new custom team records only if there isn't one already existing
            
            
            // NB - 05/10 - T-500927 - Start
            for(RoleAccessProjectSetting__c roleAccess : roleAccessSettings){
                if(roleAccess.Name == otm.TeamMemberRole) {
                    //DM 07/22 Updated for I-226753 Setting project access level in variable on custom account team
                    lstNewMembersToInsert.add(new Custom_Account_Team__c(User__c = otm.UserId,Team_Member_Role__c = otm.TeamMemberRole,Project__c = project.Id,
                                      Account__c = opp.AccountId,Project_Access_Level__c = roleAccess.Project_Access__c, RecordTypeID = rtProjectTeam));
                    projectShareList.add(new Project_Plan__Share(
                        AccessLevel = roleAccess.Project_Access__c, ParentId = project.Id, UserOrGroupId = otm.UserId));
                    break;
                }
            }
            // NB - 05/10 - T-500927 - End                          
          }
        }
      }
      System.debug('lstNewMembersToInsert:'+lstNewMembersToInsert);
      if(lstNewMembersToInsert.size() > 0)
      {
        insert lstNewMembersToInsert;
        if(projectShareList.size() > 0){
          Database.insert(projectShareList, false);
        }
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Members_Added_Successfully));
        //(Ref : I-223272)
        fetchExistingProjectTeamMembers();
      }
      
      /*if(opptyUserIdMap.size()>0) {
        for(Document__c doc : [SELECT Id, Opportunity__c,Oracle_Quote__r.BigMachines__Opportunity__r.id FROM Document__c  WHERE  
                        (Opportunity__c IN: opptyUserIdMap.keyset() OR Oracle_Quote__r.BigMachines__Opportunity__r.id IN:opptyUserIdMap.keyset())]) {
          docIdSet.add(doc.Id);
          if(doc.Opportunity__c!=null) {
            if(opptyDocIdSetMap.containsKey(doc.Opportunity__c)) {
            opptyDocIdSetMap.get(doc.Opportunity__c).add(doc.Id);
            }
            else {
              opptyDocIdSetMap.put(doc.Opportunity__c,docIdSet);
            }
          }
            else if(doc.Oracle_Quote__r.BigMachines__Opportunity__r.id!=null) {
              if(opptyDocIdSetMap.containsKey(doc.Oracle_Quote__r.BigMachines__Opportunity__r.id)) {
                opptyDocIdSetMap.get(doc.Oracle_Quote__r.BigMachines__Opportunity__r.id).add(doc.Id);
              }
              else {
                opptyDocIdSetMap.put(doc.Oracle_Quote__r.BigMachines__Opportunity__r.id,docIdSet);
              }
            }
          }
        }
        List<Document__Share> lstDocShareRecord = new List<Document__Share>();
        List<String> key1 = new List<String>();
        for(String s : setExistingUserWithRoles) {
           key1.addALL(s.split(Constants.TILDE_DELIMITER));
        }
        System.debug('key'+key1);
        if(key1.size()>0) {
        for(Id oppId : opptyUserIdMap.keyset()) {
          for(Id docId : opptyDocIdSetMap.get(oppId)) {
            for(Id userId : opptyUserIdMap.get(oppId)) {
             if(key1[1]==Constants.PROJECT_ENGINEER || key1[1]==Constants.ROLE_PM) {
               lstDocShareRecord.add(new Document__Share(UserOrGroupId= userId,
                                                       ParentId = docId,
                                                       AccessLevel= CONSTANTS.PERM_EDIT,
                                                      RowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c));
             } else {
               lstDocShareRecord.add(new Document__Share(UserOrGroupId= userId,
                                                       ParentId = docId,
                                                       AccessLevel= CONSTANTS.PERM_READ,
                                                      RowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c));
             }
             
            }
          }
        }
        if(lstDocShareRecord.size()>0) {
               Database.insert(lstDocShareRecord,false);
             }
            
        }*/
     return null;
    } 
}