//
// (c) 2014 Appirio, Inc.
// Class NotifyAccountTeamBatch
// Class to put Chatter Feed on Account for New Account team members added withing 24 Hours
//
// 28 Jan 2016     Sahil Batra       Original(T-469786)
global class NotifyAccountTeamBatch implements Database.Batchable<sObject>,Database.Stateful ,System.Schedulable{
	public List<ConnectApi.BatchInput> batchInputs1 = new List<ConnectApi.BatchInput>();
	public List<ConnectApi.BatchInput> batchInputs2 = new List<ConnectApi.BatchInput>();
	public Map<String,String> accountIdtoName = new Map<String,String>();
	global NotifyAccountTeamBatch(){
		
	}
	//Start Method
	global Database.QueryLocator start(Database.BatchableContext BC){
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
        Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
		String query='';
		query = 'SELECT Id,UserId,AccountId FROM AccountTeamMember WHERE CreatedDate >=LAST_N_DAYS:1 AND Account.recordtypeid=:recordTypeInstrument';
		System.debug('>>>>>query'+query);
      //  System.debug('??'+Database.getQueryLocator(query).size());
		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC,List<Sobject> scope){
		 System.debug('>>>>>>>>size'+scope);
		Set<String>  accIds = new Set<String>();
		for(Sobject s : scope){
		 	AccountTeamMember accountMember = (AccountTeamMember)s;
		 	accIds.add(accountMember.AccountId);
		 }
		 if(accIds.size() > 0){
		 	for(Account a : [Select Id,Name FROM Account where Id IN :accIds]){
		 		accountIdtoName.put(a.Id,a.Name);
		 	}
		 }
		for(Sobject s : scope){
			AccountTeamMember accountMember = (AccountTeamMember)s;
			ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(returnInputFeed(accountMember));
				batchInputs1.add(batchInput);		
		}
		System.debug('>>>>>>>>111'+batchInputs1);
		if(batchInputs1.size() > 0){
              if(!Test.isRunningTest()){
			  	ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs1);
              }
		}
	}
	global void finish(Database.BatchableContext BC){
	}
	public ConnectApi.FeedItemInput returnInputFeed(AccountTeamMember member){
		String communityId = null;
		ConnectApi.FeedType feedType = ConnectApi.FeedType.UserProfile;
		ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
		ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegment;
		ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
		messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		textSegment = new ConnectApi.TextSegmentInput();
		textSegment.text = 'Welcome ';
		messageInput.messageSegments.add(textSegment);
		mentionSegment.id = member.UserId;
		messageInput.messageSegments.add(mentionSegment);
		textSegment = new ConnectApi.TextSegmentInput();
		textSegment.text = ', You have been added to '+accountIdtoName.get(member.AccountId)+' account.';
		messageInput.messageSegments.add(textSegment);
		input.body = messageInput;
		input.subjectId = member.AccountId;
		input.feedElementType = ConnectApi.FeedElementType.FeedItem;
		
		return input;
	}
  // Schedular for batch.
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
      Id batchId = Database.executeBatch(new NotifyAccountTeamBatch(), 200);
      system.debug('@@@batchId = ' + batchId);
    }
  }
}