/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
/**================================================================      
* Appirio, Inc
* Name: AddressTriggerHandlerTest
* Description: T-482844
* Created Date: 12 Apr 2016
* Created By: Rahul Aeran (Appirio)
*
* Date Modified      Modified By            Description of the update
* 16 Sep 2016        Kanika Mathur          Ref : I-235328
* 28 Sep 2016		 Isha Shukla    		Ref : T-541552 Added Test Method testsyncAddressWithSVMXSite
==================================================================*/
@isTest
private class AddressTriggerHandlerTest {
    static List<Account> accList1;
    static testMethod void testBillingAddress(){
        User sysAdmin =  TestUtils.createUser(1,'System Administrator', true).get(0);
        System.runAs(sysAdmin){
            
            createTestData();
            //List<Account> accounts = [SELECT Id FROM Account];
            List<Address__c> addresses = new List<Address__c>();
            List<Address__c> addresses1 = new List<Address__c>();
            List<Address__c> addresses2 = new List<Address__c>();
            List<Address__c> addresses3 = new List<Address__c>();
            Test.startTest();
            //Creating a list of addresses for 3 different accounts that we have inserted
            /*for(Integer i=0 ; i<21; i++){
                if(Math.mod(i,3) == 0){
                    addresses1.addAll(TestUtils.createAddressObject(accList1.get(0).Id,1,true,'Aruba',null,Constants.BILLING, '88', false));
                    addresses1.addAll(TestUtils.createAddressObject(accList1.get(0).Id,1,true,'Aruba',null,Constants.SHIPPING, '88', false));
                    //For first account
                    System.debug('addresses1>>'+addresses1);
                }else if(Math.mod(i,2) == 0){
                    //For second account
                    addresses2.addAll(TestUtils.createAddressObject(accList1.get(1).Id,1,false,'Aruba',null,Constants.BILLING, '88', false));
                    addresses2.addAll(TestUtils.createAddressObject(accList1.get(1).Id,1,false,'Aruba',null,Constants.SHIPPING, '88',  false));
                    
                }else{
                    addresses3.addAll(TestUtils.createAddressObject(accList1.get(2).Id,1,false,'Aruba',null,Constants.BILLING, '88', false));
                    addresses3.addAll(TestUtils.createAddressObject(accList1.get(2).Id,1,false,'Aruba',null,Constants.SHIPPING, '88', false));
                    
                }
            }*/
            Address__c address = TestUtils.createAddressObject(accList1.get(0).Id,1,true,'Aruba',null,Constants.BILLING, '88', false).get(0);
            Address__c address1 = TestUtils.createAddressObject(accList1.get(0).Id,1,true,'Aruba',null,Constants.SHIPPING, '88', false).get(0);
            //addresses.addAll(addresses1);
            //addresses.addAll(addresses2);
            //addresses.addAll(addresses3);
            addresses.add(address);
            addresses.add(address1);
            insert addresses;
            //We need to assert that the accounts do not have there address populated as none of the above were primary
            List<Account> newAccounts = [SELECT Id FROM Account WHERE COMM_Billing_Country__c = 'United States'];
            System.assert(newAccounts.isEmpty(),'There would not be any account');
            addresses.get(0).City__c = 'Test City';
            addresses.get(1).City__c = 'Test City'; 
            update addresses;
            newAccounts = [SELECT Id,COMM_Billing_City__c FROM Account WHERE COMM_Billing_City__c = :addresses.get(0).City__c AND Id=:accList1.get(0).Id];
            System.assertEquals(newAccounts[0].COMM_Billing_City__c, addresses.get(0).City__c);
            newAccounts = [SELECT Id,COMM_Shipping_City__c FROM Account WHERE COMM_Shipping_City__c = :addresses.get(1).City__c AND Id=:accList1.get(0).Id];
            System.assert(newAccounts.size() == 1,'One of the account should be updated');
            
            //setting two primary flag true for billing type
            addresses.get(0).Primary_Address_Flag__c = false;
            addresses.get(1).Primary_Address_Flag__c = false;
            update addresses;
            //List<Address__c> newAddressesAcc2 = [SELECT Id, Primary_Address_Flag__c FROM Address__c WHERE Id IN:addresses AND Primary_Address_Flag__c = true];
            List<Address__c> newAddressesAcc2 = [SELECT Id, Primary_Address_Flag__c FROM Address__c WHERE Id =: addresses.get(0).Id AND Primary_Address_Flag__c = true];
            //System.assert(newAddressesAcc2.size() == 1, 'At a time only one billing address can be primary');
            System.assert(newAddressesAcc2.size() == 0, 'Now there should not be any primary address for this account');
            
            //Testing with already having a billing address as primary
            addresses.get(0).Primary_Address_Flag__c = true; 
            update addresses;
            newAddressesAcc2 = [SELECT Id,Primary_Address_Flag__c FROM Address__c WHERE Id =:addresses.get(0).Id AND Primary_Address_Flag__c = true];
            System.assert(newAddressesAcc2.size() == 1, 'At a time only one billing address can be primary');
            
            
            //setting the shipping address flag as true
            addresses.get(1).Primary_Address_Flag__c = true;
            update addresses;
            
            List<Address__c> newAddressesAcc3 = [SELECT Id,Primary_Address_Flag__c FROM Address__c WHERE Id =:addresses.get(1).Id AND Primary_Address_Flag__c = true];
            System.assert(newAddressesAcc3.size() == 1, 'At a time only one shipping address can be primary');
            
            Test.stopTest();
        }
    }
    static testMethod void testsyncAddressWithSVMXSite() {
        User sysAdmin =  TestUtils.createUser(1,'System Administrator', true).get(0);
        System.runAs(sysAdmin){
            List<Account> accList = TestUtils.createAccount(1, false);
            insert accList;
            Test.startTest();
            List<Address__c> addresses = new List<Address__c>();
            addresses = TestUtils.createAddressObject(accList[0].Id, 1, true, 'Aruba', 'state', Constants.BILLING, 'test', false);
            addresses[0].Location_ID__c = '121';
            addresses[0].Account__c = accList[0].Id;
            insert addresses;
            Test.stopTest();
            
            List<SVMXC__Site__c> locationList = new List<SVMXC__Site__c>([SELECT Id,SVMXC__Account__c 
                                                                          FROM SVMXC__Site__c 
                                                                          WHERE Location_ID__c = '121']);
            System.assertEquals(locationList.size(), 1);
            System.assertEquals(locationList.get(0).SVMXC__Account__c, addresses[0].Account__c);    
            
        }
        
    }
    static void createTestData() {
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        accList1 = TestUtils.createCommAccount(3, false);
        accList1[0].RecordTypeId = accountRTId;
        accList1[1].RecordTypeId = accountRTId;
        accList1[2].RecordTypeId = accountRTId;
        accList1[1].Endo_Active__c = true;
        accList1[2].Endo_Active__c = true;
        accList1[0].Endo_Active__c = true;
        insert accList1;
    }
}