/**================================================================      
* Appirio, Inc
* Name: COMM_Batch_SendEmail_Account_OwnerTest
* Description: Test class for COMM_Batch_SendEmail_Account_Owner
* Created Date: 21 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest(SeeAllData=true)
private class COMM_Batch_SendEmail_Account_OwnerTest {
    @isTest static void testBatch() {
        createData();
        Test.startTest();
        COMM_Batch_SendEmail_Account_Owner batch = new COMM_Batch_SendEmail_Account_Owner();
        Database.executeBatch(batch);
        Test.stopTest();
    }
    private static void createData() {
        User salesRep = TestUtils.createUser(1, 'System Administrator', true).get(0);
        Account acc = TestUtils.createAccount(1, false).get(0);
        acc.COMM_Sales_Rep__c = salesRep.Id;
        insert acc;
        SVMXC__Installed_Product__c asset = new SVMXC__Installed_Product__c(
            SVMXC__Company__c = acc.Id,Name = 'test1',SVMXC__Serial_Lot_Number__c = '0101'
        );
        insert asset;
        List<SVMXC__Warranty__c> warrantyList = new List<SVMXC__Warranty__c>();
        for(integer i = 0 ; i < 100;i++) {
            SVMXC__Warranty__c warranty = new SVMXC__Warranty__c(SVMXC__End_Date__c = System.today() + 37,
                                                                 SVMXC__Installed_Product__c = asset.Id);
            warrantyList.add(warranty);
        }        
        insert warrantyList;
    }
}