public with sharing class Salespulse20_InstrumentsController {
    public uParams u {get;set;}

    public Salespulse20_InstrumentsController(){
        u = new uParams();
    }
        public class uParams {
        public String pf {get; set;}
        public String mc {get;set;}
        public String div {get;set;}
        public String id {get;set;}
        public String performanceURL {get;set;}
        public String serviceURL {get;set;}
        public String transactionsURL {get;set;}
        public String real_timeURL {get;set;}
        public String manageURL {get;set;}
        public String orderHistoryURL {get;set;}
        public String insightURL {get;set;}
        public String defaultURL {get;set;}
        
        //Modified 14 Sep 2016 by Sneha Gupta on case#00177915
        public String serialNumberInquiryURL {get;set;}
        public String sampleInventoryURL {get;set;}
        public String pricingAnalysisURL {get;set;}
        //end
        //ICCKM 9/30 Changes
        public String oeCustomerSearchURL {get;set;}
        public String desktopOrderEntryURL {get;set;}
        //end
        //10/18 changes - S-448320 new tab requests for ICCKM and Empower
        public String loanersURL {get;set;}
        public String repairsURL {get;set;}
        public String proCareAnalysisURL {get;set;}
        public String backorderURL {get;set;}
        public String shipmentsURL {get;set;}
        public String otherURL {get;set;}
        //end
        public String alertURL {get;set;}
        public String iframeTDclass {get;set;}
        public String district {get;set;}
        public Boolean performanceTab {get;set;}
        public Boolean serviceTab {get;set;}
        public Boolean transactionsTab {get;set;}
        public Boolean real_timeTab {get;set;}
        public Boolean manageTab {get;set;}
        public Boolean orderHistoryTab {get;set;}
        public Boolean insightTab {get;set;}
        public Boolean alertTab {get;set;}
        // Modified 14 Sep 2016 by Sneha Gupta on case#00177915
        public Boolean serialNumberInquiryTab {get;set;}
        public Boolean sampleInventoryTab {get;set;}
        public Boolean pricingAnalysisTab {get;set;}
        //end 
        //ICCKM 9/30 Changes
        public Boolean oeCustomerSearchTab {get;set;}
        public Boolean desktopOrderEntryTab {get;set;}
        //end
        //10/18 changes - S-448320 new tab requests for ICCKM and Empower
        public Boolean loanersTab {get;set;}
        public Boolean repairsTab {get;set;}
        public Boolean proCareAnalysisTab {get;set;}
        public Boolean backorderTab {get;set;}
        public Boolean shipmentsTab {get;set;}
        public Boolean otherTab {get;set;}
        //end
        
    
        public Boolean isIPHONE {get;set;}
        public Boolean isError {get;set;}

        public uParams() {
            List<Mancode__c> mcs = [SELECT Id, Disctrict__c, Name, Manager__c, Sales_Rep__c, Business_Unit__c, Business_Unit_Code__c 
                                    from Mancode__c where Sales_Rep__c =: UserInfo.getUserId()];
            mc = '';
            district = '';
            for(Mancode__c m : mcs){
                if(mc == ''){
                    mc = m.Name;
                }else{
                    mc += ',' + m.Name;
                }
                if(district == ''){
                    district = m.Disctrict__c;
                }else{
                    district += ',' + m.Disctrict__c;
                }
            }
            //Get real values for these
            div = '';
            //Get real values for these

            id = UserInfo.getUserId();
            isIPHONE = false;
            String userAgent = System.currentPageReference().getHeaders().get('User-Agent');
            
            system.debug('$$$$$UserAgent : ' + userAgent);
            SalespulseURLs__c spurl = SalespulseURLs__c.getInstance();
            isError = false;
            serviceURL = '';
            performanceURL = '';
            transactionsURL = '';
            manageURL = '';
            orderHistoryURL = '';
            insightURL = '';
            real_timeURL = '';
            alertURL = '';
            // Modified 14 Sep 2016 by Sneha Gupta on case#00177915
            serialNumberInquiryURL='';
            sampleInventoryURL='';
            pricingAnalysisURL='';
            //end
            //ICCKM 9/30 Changes
            oeCustomerSearchURL='';
            desktopOrderEntryURL='';
            //end
            //10/18 changes - S-448320 new tab requests for ICCKM and Empower
            loanersURL='';
            repairsURL='';
            proCareAnalysisURL='';
            backorderURL='';
            shipmentsURL='';
            otherURL='';
            //end

            if (userAgent.contains('iPad')){
                pf = 'SalesPulse_ipad';
                performanceURL = spurl.IPAD_Performance__c;
                serviceURL = spurl.IPAD_Service__c;
                transactionsURL = spurl.IPAD_Transactions__c;
                
                // Modified 14 Sep 2016 by Sneha Gupta on case#00177915
                serialNumberInquiryURL= spurl.IPAD_Serial_Number_Inquiry__c;
                sampleInventoryURL= spurl.IPAD_Sample_Inventory__c;
                pricingAnalysisURL=  spurl.IPAD_Pricing_Analysis__c;
                //end
                //10/18 changes - S-448320 new tab requests for ICCKM and Empower
                backorderURL=spurl.IPAD_Backorder__c;
                shipmentsURL=spurl.IPAD_Shipments__c;
                otherURL=spurl.IPAD_Other__c;
                //end
                
                real_timeURL = spurl.IPAD_Realtime__c;
                real_timeURL = real_timeURL.replace('Mancode=', 'Mancode=' + mc);
                real_timeURL = real_timeURL.replace('User_id=', 'User_id=' + id);
                real_timeURL = real_timeURL.replace('District=', 'District=' + district);
                manageURL = spurl.IPAD_Manage__c;
                orderHistoryURL = spurl.IPAD_Order_History__c;
                insightURL = spurl.IPAD_Insight__c;
                defaultURL = spurl.IPAD_Performance__c;
        alertURL = spurl.IPAD_Alerts__c;                
        iframeTDclass = 'ipad';
            }
            else if (userAgent.contains('iPhone')){
                pf = 'SalesPulse_mobile';
                
                // Modified 14 Sep 2016 by Sneha Gupta on case#00177915
                serialNumberInquiryURL= spurl.IPHONE_Serial_Number_Inquiry__c;
                sampleInventoryURL= spurl.IPHONE_Sample_Inventory__c;
                pricingAnalysisURL=  spurl.IPHONE_Pricing_Analysis__c;
                //end
                //10/18 changes - S-448320 new tab requests for ICCKM and Empower
                backorderURL=spurl.IPHONE_Backorder__c;
                shipmentsURL=spurl.IPHONE_Shipments__c;
                otherURL=spurl.IPHONE_Other__c;
                //end
                
                real_timeURL = spurl.IPHONE_Realtime__c;
                real_timeURL = real_timeURL.replace('Mancode=', 'Mancode=' + mc);
                real_timeURL = real_timeURL.replace('User_id=', 'User_id=' + id);
                real_timeURL = real_timeURL.replace('District=', 'District=' + district);
                defaultURL = real_timeURL;
                iframeTDclass = 'mobile';
                isIPHONE = true;
            }else{
                pf = 'SalesPulse_Desktop';
                
                // Modified 14 Sep 2016 by Sneha Gupta on case#00177915
                serialNumberInquiryURL= spurl.Desktop_Serial_Number_Inquiry__c;
                sampleInventoryURL= spurl.Desktop_Sample_Inventory__c;
                pricingAnalysisURL= spurl.Desktop_Pricing_Analysis__c;
                //end 
                //ICCKM 9/30 Changes
                oeCustomerSearchURL = spurl.Desktop_OE_and_Customer_PO_Search__c;
                desktopOrderEntryURL = spurl.Desktop_Order_Entry__c;
                //end
                //10/18 changes - S-448320 new tab requests for ICCKM and Empower
                loanersURL=spurl.Desktop_Loaners__c;
                repairsURL=spurl.Desktop_Repairs__c;
                proCareAnalysisURL=spurl.Desktop_ProCare_Analysis__c;
                backorderURL=spurl.Desktop_Backorder__c;
                shipmentsURL=spurl.Desktop_Shipments__c;
                otherURL=spurl.Desktop_Other__c;
                //end
                
                serviceURL = spurl.Desktop_Service__c;
                performanceURL = spurl.Desktop_Performance__c;
                transactionsURL = spurl.Desktop_Transactions__c;
                real_timeURL = spurl.Desktop_Realtime__c;
                real_timeURL = real_timeURL.replace('Mancode=', 'Mancode=' + mc);
                real_timeURL = real_timeURL.replace('User_id=', 'User_id=' + id);
                real_timeURL = real_timeURL.replace('District=', 'District=' + district);
                manageURL = spurl.Desktop_Manage__c;
                orderHistoryURL = spurl.Desktop_Order_History__c;
                insightURL = spurl.Desktop_Insight__c;
                defaultURL = spurl.Desktop_Performance__c;
        alertURL = spurl.Desktop_Alerts__c;                
        iframeTDclass = 'desktop';
            }
            performanceTab = (performanceURL == '' || performanceURL == 'false' || performanceURL == '/apex/Salespulse_2_0_help') ? false : true;
            serviceTab = (serviceURL == '' || serviceURL == 'false' || serviceURL == '/apex/Salespulse_2_0_help') ? false : true;
            transactionsTab = (transactionsURL == '' || transactionsURL == 'false' || transactionsURL == '/apex/Salespulse_2_0_help') ? false : true;
            real_timeTab = (real_timeURL == '' || real_timeURL == 'false' || real_timeURL == '/apex/Salespulse_2_0_help') ? false : true;
            manageTab = (manageURL == '' || manageURL == 'false' || manageURL == '/apex/Salespulse_2_0_help') ? false : true;
            orderHistoryTab = (orderHistoryURL == '' || orderHistoryURL == 'false' || orderHistoryURL == '/apex/Salespulse_2_0_help') ? false : true;
            insightTab = (insightURL == '' || insightURL == 'false' || insightURL == '/apex/Salespulse_2_0_help') ? false : true;
            defaultURL = ( defaultURL == null || defaultURL == '' || defaultURL == 'false') ? spurl.Error_URL__c : defaultURL;
             alertTab = (alertURL == '' || alertURL == 'false' || alertURL == '/apex/Salespulse_2_0_help') ? false : true;
            //ICCKM 9/30 Changes
            oeCustomerSearchTab=(oeCustomerSearchURL == '' || oeCustomerSearchURL == 'false' || oeCustomerSearchURL == '/apex/Salespulse_2_0_help') ? false : true;
            desktopOrderEntryTab=(desktopOrderEntryURL == '' || desktopOrderEntryURL == 'false' || desktopOrderEntryURL == '/apex/Salespulse_2_0_help') ? false : true;
            //end
            // Modified 14 Sep 2016 by Sneha Gupta on case#00177915
            serialNumberInquiryTab=(serialNumberInquiryURL == '' || serialNumberInquiryURL == 'false' || serialNumberInquiryURL == '/apex/Salespulse_2_0_help') ? false : true;
            sampleInventoryTab=(sampleInventoryURL == '' || sampleInventoryURL == 'false' || sampleInventoryURL == '/apex/Salespulse_2_0_help') ? false : true;
            pricingAnalysisTab=(pricingAnalysisURL == '' || pricingAnalysisURL == 'false' || pricingAnalysisURL == '/apex/Salespulse_2_0_help') ? false : true;
            //end
            //10/18 changes - S-448320 new tab requests for ICCKM and Empower
            loanersTab=(loanersURL == '' || loanersURL == 'false' || loanersURL == '/apex/Salespulse_2_0_help') ? false : true;
            repairsTab=(repairsURL == '' || repairsURL == 'false' || repairsURL == '/apex/Salespulse_2_0_help') ? false : true;
            proCareAnalysisTab=(proCareAnalysisURL == '' || proCareAnalysisURL == 'false' || proCareAnalysisURL == '/apex/Salespulse_2_0_help') ? false : true;
            backorderTab=(backorderURL == '' || backorderURL == 'false' || backorderURL == '/apex/Salespulse_2_0_help') ? false : true;
            shipmentsTab=(shipmentsURL == '' || shipmentsURL == 'false' || shipmentsURL == '/apex/Salespulse_2_0_help') ? false : true;
            otherTab=(otherURL == '' || otherURL == 'false' || otherURL == '/apex/Salespulse_2_0_help') ? false : true;
            //end
            
            if(spurl.is_Error_URL__c){
                iframeTDclass += ' error';
                isError = true;
            }
        }
    }


}