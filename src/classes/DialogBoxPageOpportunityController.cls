// (c) 2015 Appirio, Inc.
//
// Class Name: DialogBoxPageOpportunityController
// Description: Contoller Class for DialogBoxPageOpportunity Page. Class to show popup when delete button is clicked on opportunity.
//
// July 11 2016, Prakarsh Jain  Original (T-518521)
//
global class DialogBoxPageOpportunityController {
  public boolean displayPopup {get; set;}  
  public String oppId;
  public Opportunity opp;
  
  public DialogBoxPageOpportunityController(ApexPages.StandardController controller){
    opp = new Opportunity();
    oppId =  ApexPages.currentPage().getParameters().get('oppId');
    if(oppId != null){
      opp = [SELECT Id, Name, Is_Deleted__c, OwnerId FROM Opportunity WHERE Id =: oppId];
    }
  }
   
  public void showPopup()
  {       
    displayPopup = true;   
  }
  
  public PageReference closePopupYes(){
    PageReference pageRef = clsPopup(oppId);
    return pageRef;
  } 
  
  global PageReference clsPopup(String oppId){
    List<OpportunityTeamMember> opportunityTeamMemberList = new List<OpportunityTeamMember>();
    List<OpportunityShare> opportunityShareList = new List<OpportunityShare>();
    List<OpportunityShare> opportunityShareListToDelete = new List<OpportunityShare>();
    system.debug('opp.OwnerId>>>'+opp.OwnerId);
    system.debug('UserInfo.getUserId()'+UserInfo.getUserId());
    PageReference pageRef;
    if(opp.OwnerId == UserInfo.getUserId()){
      opp.Is_Deleted__c = true;
      
      opp.OwnerId = [SELECT Id, Name FROM User WHERE Name = 'Integration User'].get(0).Id;
      
      //update opp;
      
      opportunityTeamMemberList = [SELECT OpportunityId, UserId, TeamMemberRole 
                                   FROM OpportunityTeamMember 
                                   WHERE OpportunityId =: opp.Id];
      system.debug('opportunityTeamMemberList>>>'+opportunityTeamMemberList);
      if(opportunityTeamMemberList.size()>0){
        delete opportunityTeamMemberList;
      } 
        
      opportunityShareList = [SELECT UserOrGroupId, RowCause, OpportunityId, OpportunityAccessLevel, Id 
                              FROM OpportunityShare 
                              WHERE OpportunityId =: opp.Id 
                              AND RowCause != :Schema.OpportunityShare.RowCause.Owner
                              AND RowCause != :Schema.OpportunityShare.RowCause.Rule
                              AND RowCause != :Schema.OpportunityShare.RowCause.ImplicitChild];
      System.debug('opportunityShareList'+opportunityShareList+'opp'+opp.Id);
        for(OpportunityShare oppShare : opportunityShareList){
          System.debug('oppShare.UserOrGroupId='+oppShare.UserOrGroupId+'opp.OwnerId='+opp.OwnerId);
        if(oppShare.UserOrGroupId != opp.OwnerId){
          opportunityShareListToDelete.add(oppShare);
        }
      }
      
      system.debug('opportunityShareListToDelete>>>'+opportunityShareListToDelete);
      if(opportunityShareListToDelete.size()>0){
        delete opportunityShareListToDelete;
      }
        update opp;
      pageRef = new PageReference('/006/o');
      return pageRef;
    }
    else{
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Cannot_Delete_Opportunity_Label));
      return null;
    }
  }
  
  public PageReference closePopup(){
    PageReference pageRef;
    pageRef = new PageReference('/'+oppId);
    return pageRef;
  }  
}