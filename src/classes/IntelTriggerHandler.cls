/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 22, 2013
 Purpose       :  Handler class for trigger IntelTrigger
 Reference     :  Task T-214509
 Modifications :  Task T-217899    Dec 12, 2013    Sonal Shrivastava
                  Task T-214509    Dec 13, 2013    Sonal Shrivastava
*******************************************************************************/
public class IntelTriggerHandler {
  
  //****************************************************************************
  // Method called on before insert
  //****************************************************************************
  public static void onBeforeInsert(List<Intel__c> newList){
    setMobileUpdate(newList);
  }
  
  //****************************************************************************
  // Method called on after insert
  //****************************************************************************
  public static void onAfterInsert(List<Intel__c> newList){
    if(!Test.isRunningTest()) {
      createChatterPostOnIntel(newList);
    } else {
      TestUtils.createChatterPostOnIntel(newList);
    }
    updateIntelChatterPost(newList); 
  }
  
  //****************************************************************************
  // Method to set Mobile Update field on Intel records
  //****************************************************************************
  public static void setMobileUpdate(List<Intel__c> newList){
  	for(Intel__c intel : newList){
  		intel.Mobile_Update__c = System.now();
  	}
  }
  
  //****************************************************************************
  // Method to insert chatter posts 
  //****************************************************************************
  private static void createChatterPostOnIntel(List<Intel__c> newList){    
    String NV_INTEL_GROUP_ID = getChatterGroupId();
    
    //We are assuming that Intel records are never loaded in bulk, so create 
    //create chatter posts on all Intel records in a loop.
    for(Intel__c intel : newList){      
      ConnectApi.FeedType feedType = ConnectApi.FeedType.Record;
      ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
      ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
      ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
      ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();    
      messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
      if(NV_INTEL_GROUP_ID != null){
        mentionSegment.id = NV_INTEL_GROUP_ID;    
        messageInput.messageSegments.add(mentionSegment);  
      } 
      textSegment.text = (intel.Related_Tags__c != null && intel.Related_Tags__c != '') 
                          ? ' ' + intel.Details__c + '\n\n ' + Label.RELATED_TAGS + ' ' + intel.Related_Tags__c
                          : ' ' + intel.Details__c;
      messageInput.messageSegments.add(textSegment);    
      input.body = messageInput;
      
      ConnectApi.FeedItem feedItemRep = ConnectApi.ChatterFeeds.postFeedItem(null, feedType, intel.Id, input, null);
    }
  }
  
  //****************************************************************************
  // Method to get Chatter group Id
  //****************************************************************************
  public static String getChatterGroupId(){
  	String profileName = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()].get(0).Name;
    String groupId;
        
    //Get Chatter post Id based on profile name    
    for(Chatter_Group_Ids__c cs : Chatter_Group_Ids__c.getAll().values()){
      if(cs.Profile_Name__c.equalsIgnoreCase(profileName)){
        groupId = cs.Group_Id__c;
        break;
      }
    }
    if(groupId == null){
      groupId = Chatter_Group_Ids__c.getInstance('System Admin Group').Group_Id__c;
    }
    return groupId;
  } 
  
  //****************************************************************************
  // Method to update Intel Chatter Post field
  //****************************************************************************
  private static void updateIntelChatterPost(List<Intel__c> newList){
    Map<String, String> mapIntelId_ChatterPostId = new Map<String, String>();
    List<Intel__c> lstIntelToUpdate = new List<Intel__c>();
    
    for(FeedItem fi : [SELECT Id, ParentId FROM FeedItem WHERE ParentId IN :newList]){
      mapIntelId_ChatterPostId.put(fi.ParentId, fi.Id);     
    } 
    for(String intelId : mapIntelId_ChatterPostId.keySet()){
      lstIntelToUpdate.add(new Intel__c(Id = intelId,
                                        Chatter_Post__c = mapIntelId_ChatterPostId.get(intelId)));
    }
    if(lstIntelToUpdate.size() > 0){
      update lstIntelToUpdate;
    }
  }
  
}