/**================================================================      
* Appirio, Inc
* Name: BigMachinesQuoteProductExt_Test
* Description: Test class for BigMachinesQuoteProductExt Trigger Handler
* Created Date: 13-Apr-2016
* Created By: Meghna Vijay (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/

@isTest
private class BigMachinesQuoteProductExt_Test{

        static testMethod void testQuoteProdExt() {
            //TestUtils.createCommConstantSetting();
            List<Pricebook2> priceBookList = TestUtils.createpricebook(5,' ',false);
            priceBookList.get(0).Name = 'COMM Price Book';
            insert priceBookList;
            List<BigMachines__Configuration_Record__c> bigMachineConfigList = TestUtils.createBigMachineConfig(2,true);
            Opportunity opty = new Opportunity();
            opty.Name = 'BigMachines test opportunity for testQuoteProdExt';
            opty.StageName = 'Prospecting';
            opty.CloseDate = Date.today();
            insert opty;
        
            BigMachines__Quote__c quote = new BigMachines__Quote__c();
            quote.Name = 'BigMachines test quote for testQuoteProdExt';
            quote.BigMachines__Opportunity__c = opty.Id;
            quote.BigMachines__Site__c=bigMachineConfigList.get(0).Id;
            insert quote;
            
            BigMachines__Quote_Product__c qProd = new BigMachines__Quote_Product__c();
            qProd.Name = 'BigMachines test quote product for testQuoteProdExt';
            qProd.BigMachines__Quote__c = quote.id;
            qProd.BigMachines__Quantity__c=1000;
            qProd.BigMachines__Sales_Price__c=2500;
            insert qProd;
            
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(qProd);
            BigMachinesQuoteProductExtension controller = new BigMachinesQuoteProductExtension(stdCtrl);
            controller.getRedirectURL();
    }
}