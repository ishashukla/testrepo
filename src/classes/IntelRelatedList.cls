/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Dec 6, 2013
 Purpose       :  Class for displaying related Intel list on Account/Contact/Product.
 Reference     :  Task T-217665, T-218030, T-218031
 Modifications :  Task T-217665, T-218030, T-218031    Dec 13, 2013    Dipika Gupta
*******************************************************************************/ 
public with sharing class IntelRelatedList {
	
  //----------------------------------------------------------------------------
	// Constructor
	//----------------------------------------------------------------------------
  public IntelRelatedList(){}
  
  //----------------------------------------------------------------------------
  // Constructor
  //----------------------------------------------------------------------------
  public static List<IntelWrapper> populateIntelList(String relatedTo, String relatedToId){
  	Map<String,Intel__c> intelMap = new Map<String, Intel__c>();
	  Set<String> setChatterPostIds = new Set<String>();  	
  	List<IntelWrapper> lstIntelWrapper = new List<IntelWrapper>();
  	String qry;
  	String parent;
  	
  	if(relatedTo == Constants.ACCOUNT_TYPE){
  		parent = 'Account__c';
  	}
  	else if(relatedTo == Constants.CONTACT_TYPE){
  		parent = 'Contact__c';
  	}
  	else if(relatedTo == Constants.PRODUCT_TYPE){
  		parent = 'Product__c';
  	}
  	else if(relatedTo == Constants.INTEL_TYPE){
  		return getIntelWrapperList(relatedToId);
  	}
  	qry = ' SELECT Id, Intel__c, ';
  	qry +=        'Intel__r.Chatter_Post__c, Intel__r.OwnerId, Intel__r.Owner.Name, Intel__r.CreatedDate, Intel__r.Details__c ' ;
  	qry += ' FROM Tag__c WHERE Intel__r.Chatter_Post__c <> NULL AND Intel__r.Intel_Inactive__c = false ';
  	qry += ' AND ' + parent + ' = \'' + relatedToId + '\'';
  	
  	for(Tag__c tag : Database.query(qry)){
  		if(!intelMap.containsKey(tag.Intel__c)){
  		  intelMap.put(tag.Intel__c, tag.Intel__r);
  		} 
	    if(tag.Intel__r.Chatter_Post__c != null){
	      setChatterPostIds.add(tag.Intel__r.Chatter_Post__c); 
	    } 
  	}
  	set<String> intelIds = intelMap.keySet();
  	map<String, list<Tag__c>> mapIntelToTag = new map<String, list<Tag__c>>();
    for(Intel__c intel : Database.query('SELECT Id, (SELECT ID, Title__c,Account__c,Contact__c,Product__c FROM Tags__r) From Intel__c  WHERE Chatter_Post__c <> null AND Intel_Inactive__c = false AND ID IN : intelIds')){
    	mapIntelToTag.put(intel.id,intel.Tags__r);
    }
  	
  	if(intelMap.size() > 0){
      lstIntelWrapper = fetchPostReplies(setChatterPostIds, intelMap, mapIntelToTag);	
  	}  	
  	return lstIntelWrapper;
  } 
  
  private static List<IntelWrapper> getIntelWrapperList(String intelId){
  	Set<String> setChatterPostIds = new Set<String>();
  	Map<String,Intel__c> intelMap = new Map<String, Intel__c>();
  	map<String, list<Tag__c>> mapIntelToTag = new map<String, list<Tag__c>>();
  	System.debug('intelId=====      '+intelId);
  	String qry = ' SELECT Id, Chatter_Post__c, OwnerId, Owner.Name, CreatedDate, Details__c, (SELECT ID, Title__c,Account__c,Contact__c,Product__c FROM Tags__r) From Intel__c ' 
  	           + ' WHERE Chatter_Post__c <> null AND Intel_Inactive__c = false AND Id = \'' + intelId + '\'';
  	for(Intel__c intel : Database.query(qry)){
  		mapIntelToTag.put(intel.id,intel.Tags__r);
  		intelMap.put(intelId, intel);
  		setChatterPostIds.add(intel.Chatter_Post__c);
  	}
  	return fetchPostReplies(setChatterPostIds, intelMap, mapIntelToTag);
  }
  
  //----------------------------------------------------------------------------
  // Method to get chatter post replies
  //----------------------------------------------------------------------------
  public static List<IntelWrapper> fetchPostReplies(Set<String> setChatterPostIds,
                                                    Map<String,Intel__c> intelMap,
                                                    map<String, list<Tag__c>> mapIntelToTag ){
    List<IntelWrapper> lstIntelWrapper = new List<IntelWrapper>();
    Map<String, List<PostReplyWrapper>> mapIntelId_PostReply = new Map<String, List<PostReplyWrapper>>();
    
    //Get all reply comments
  	List<FeedComment> lstPostReplies = WithoutSharingUtility.fetchPostComments(setChatterPostIds);
      
    // Iterate all feed comments 
    for(FeedComment reply : lstPostReplies){
    	//Add Feed Comments with related Intel into map
      if(!mapIntelId_PostReply.containsKey(reply.ParentId)){
        mapIntelId_PostReply.put(reply.ParentId, new List<PostReplyWrapper>());
      }
      mapIntelId_PostReply.get(reply.ParentId).add(new PostReplyWrapper(reply, false));
    } 
    // Iterate intel records and create IntelWrapper instance
    for(String intelId : intelMap.keySet()){  
      List<PostReplyWrapper> commentList;
      if(mapIntelId_PostReply.containsKey(intelId)){
        commentList = mapIntelId_PostReply.get(intelId);
      }else{
        commentList = new List<PostReplyWrapper>();
      }  
      
      //Prepare map for tag names 
      map<String,Id> mapTagTitleToReference  = new map<String,Id>();
      if(mapIntelToTag.containsKey(intelId)){
      	list<Tag__c> listTag  = mapIntelToTag.get(intelId);
      	for(Tag__c tag : listTag){
      		String referenceId = (tag.Account__c != null) ? tag.Account__c : (tag.Contact__c!= null ? tag.Contact__c : (tag.Product__c!= null ? tag.Product__c : null));
      		if(referenceId != null && tag.Title__c != null){
      			mapTagTitleToReference.put(tag.Title__c, referenceId);
      		}
      	}
      } 
      lstIntelWrapper.add(new IntelWrapper(intelMap.get(intelId), commentList, false, mapTagTitleToReference));        
    }
    return lstIntelWrapper;
  }
  
  //----------------------------------------------------------------------------
	// A Method Which Save FeedComments 
	//----------------------------------------------------------------------------
	public static PageReference saveReply(String chatterPostId, String commentBody){
	  if(chatterPostId != '' && commentBody != ''){
	    FeedComment sObjFeedComment = new FeedComment(FeedItemId = chatterPostId, CommentBody = commentBody);
	    try {
	      insert sObjFeedComment;
	    } catch(Exception ex){
	    	throw ex;
	      return null;  
	    }
	  } 
	  return null;      
	}
    
	//----------------------------------------------------------------------------
	// A Method Which Save Intel_Flag 
	//----------------------------------------------------------------------------
	public static PageReference saveFlag(String intelId, String entityId){
			      
	  if(intelId != '' && intelId != null && entityId != '' && entityId != null){
	  	String postingUserId;
	  	String body;
	  	DateTime occDate;
	  	String entityType;
	  	
	  	if(entityId.startsWith(Constants.CHATTERPOST_PREFIX)){
	  		for(Intel__c intel : [SELECT Id, CreatedById, CreatedDate, Details__c, Chatter_Post__c
	  		                      FROM Intel__c 
	  		                      WHERE Chatter_Post__c = :entityId]){
	  			postingUserId = intel.CreatedById;
	        body          = intel.Details__c;
	        entityType    = Constants.TYPE_POST;
	        occDate       = intel.CreatedDate;
	  		}
	  	}
	  	else if(entityId.startsWith(Constants.COMMENT_PREFIX)){
	  		for(FeedComment comment : WithoutSharingUtility.fetchComment(entityId)){
	        postingUserId = comment.CreatedById;
	        body          = comment.CommentBody;
	        entityType    = Constants.TYPE_COMMENT;
	        occDate       = comment.CreatedDate;
	  		}
	  	}
	  	System.debug('occdate --->>> ' + occDate);
      Intel_Flag__c cObjIntelFlag = new Intel_Flag__c( Posting_User__c    = postingUserId,
		                                                   Post_Comment__c    = body,
		                                                   Intel__c           = intelId,
		                                                   Occurrence_Date__c = occDate,
		                                                   Entity_Id__c       = entityId,
		                                                   Entity_Type__c     = entityType ); 
      try {                                              
        insert cObjIntelFlag;
      } catch(Exception ex){
        throw ex;
        return null;  
      }
	  } 
	  return null;          
	}
  
  //----------------------------------------------------------------------------
	// Wrapper Class having Intel Details and Feed Comment list of Intel
	//----------------------------------------------------------------------------
	public class IntelWrapper{
	  public String id            {get; set;}
	  public String chatterPostId {get; set;}
	  public String detail        {get; set;}
    public Intel__c intelObj    {get; set;}
    public String postedDateTime {get; set;}
	  public Boolean postFlag     {get; set;}
	  public List<PostReplyWrapper> lstFeedComment {get; set;}
	  public List<PostReplyWrapper> lstLastFiveComment {get; set;}
    public map<String,Id> mapTagTitleToReference {get;set;}        
	  
	  //--------------------------------------------------------------------------
	  // IntelWrapper Class Constructor
	  //--------------------------------------------------------------------------
	  public IntelWrapper(Intel__c intel, List<PostReplyWrapper> fcomment, Boolean flag, map<String,Id> mapTagTitleToReference){
	    id = intel.Id;
	    chatterPostId = intel.Chatter_Post__c;
	    detail = intel.Details__c; 
	    
	    intelObj = intel;
	    postedDateTime = intel.CreatedDate.format('MM/dd/YYYY HH:mm aaa');
	    postFlag = flag;
	    lstFeedComment = fcomment;
	    this.mapTagTitleToReference = mapTagTitleToReference;
	    lstLastFiveComment = new List<PostReplyWrapper>();
	    if(lstFeedComment.size() <= 5){
	    	lstLastFiveComment = lstFeedComment;
	    }else{
	    	for(Integer i=0; i<5; i++){
	    		lstLastFiveComment.add(lstFeedComment.get(lstFeedComment.size() - 5 + i)); 
	    	}
	    }	    
	  }   
	}
    
	//--------------------------------------------------------------------------
	// Wrapper Class having FeedComments  
	//--------------------------------------------------------------------------
	public class PostReplyWrapper{
	  public FeedComment reply {get; set;}
	  public Boolean replyFlag {get; set;}
	  public String postedDateTime {get; set;}
      
	  //------------------------------------------------------------------------
    // PostReplyWrapper Class Constructor
    //------------------------------------------------------------------------
	  public PostReplyWrapper(FeedComment comment, Boolean flag){
	    reply = comment;
	    postedDateTime = comment.CreatedDate.format('MM/dd/YYYY HH:mm aaa');
	    replyFlag = flag;
	  }
  } 
  
}