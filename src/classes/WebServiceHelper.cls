// 
// (c) 2016 Appirio, Inc.
//
// Name : WebServiceHelper 
// Used to make web service callouts
//
// 15th May 2016     Deepti Maheshwari      Original(T-492006)
// 19th Aug 2016     ralf hoffmann            changed start_date__c to project_start_date__c
// 15th Oct 2016 + 6th Dec 2016	ralf hoffmann			       I-240204 updated project name to truncate after 30 char on EBS request
// 19th October 2016     Nitish Bansal    Update (I-240725) // Added null checks to avoid null pointer exceptions.
// 24th October 2016     Nitish Bansal    Update (I-241253) // Resolved list must not have two identically equal elements issue 
// 7th November 2016     Deepti Maheshwari Update(I-242660) // Update Parsing of sales order update to get request message and source identifier
public class WebServiceHelper {
	
	public static final String CONTENT_TYPE = 'application/json';
	public static final String AUTHORIZATION = 'Authorization';
	public static final String BASIC = 'Basic ';
   // do callout
  
  //================================================================      
  // Name         : executeProjectCreationService
  // Description  : Method called on project creation/updation
  // Created Date : 15th May 2016 
  // Created By   : Deepti Maheshwari (Appirio)
  // Task         : T-492006
  //==================================================================  
  @future(callout=true)
  public static void asyncExecuteProjectCreationService(Set<ID> projectIDs){
    List<Project_Plan__c> projectsToSync = [SELECT Project_Plan_Number__c, Project_Start_Date__c,
                                            Completion_Date__c, Id, Long_Name__c,Name,
                                            Engineering_Approval_Status__c, 
                                            Project_Manager__r.Sales_Rep_Id__c,
                                            Oracle_Project_Status__c
                                            FROM Project_Plan__c
                                            WHERE id in :projectIDs];
    if(projectsToSync.size() > 0){
      executeProjectCreationService(projectsToSync);
    }
  }
  
  public static void executeProjectCreationService(List<Project_Plan__c> projectsToSync){
    SchemasStrykerCOMMproject.ProjectType ebsProject;
    SchemasStrykerCOMMproject.ProjectsType ebsProjects = new SchemasStrykerCOMMproject.ProjectsType();
    ebsProjects.project = new List<SchemasStrykerCOMMproject.ProjectType>();
    SchemasStrykerCOMMproject.ProjectResponseType response = new SchemasStrykerCOMMproject.ProjectResponseType();
    
    SchemasStrykerCOMMproject.IntegrationHeaderType integrationHeader = new SchemasStrykerCOMMproject.IntegrationHeaderType();
    integrationHeader.SenderIdentifier = Constants.SENDER_ID;
    integrationHeader.ReceiverIdentifier = Constants.RECIEVER_ID;
    List<String> errorTargetObjectIDs = new List<String>();
    List<String> sourceIdentifiers = new List<String>();
    for(Project_Plan__c projects : projectsToSync){
      ebsProject = new SchemasStrykerCOMMproject.ProjectType();
      ebsProject.businessUnit = Constants.BUSINESS_UNIT;
      ebsProject.projectNumber = projects.Project_Plan_Number__c;
      sourceIdentifiers.add(projects.Project_Plan_Number__c);
      ebsProject.projectDescription = projects.Name.substring(0,projects.Name.length() < 30 ? projects.Name.length() : 30);  //RH 10/15/16, 12/6/16 I-240204 (truncate per EBS request)
      Datetime dt, dt1 ;
      if(projects.Project_Start_Date__c != null){ //NB - 05/23 - Added null check
          //dt = datetime.newInstance(projects.Start_Date__c.year(), 
          //        projects.Start_Date__c.month(),projects.Start_Date__c.day());

          ebsProject.startDate = projects.Project_Start_Date__c;
      }
      
      if(projects.Completion_Date__c!= null){//NB - 05/23 - Added null check
          //dt1 = datetime.newInstance(projects.Completion_Date__c.year(), 
          //        projects.Completion_Date__c.month(),projects.Completion_Date__c.day());
          ebsProject.completionDate = projects.Completion_Date__c;
      }
      //TODO : remove hardcoding for salesrepID after testing
      //ebsProject.resourceId = projects.Project_Manager__r.Sales_Rep_Id__c != null ? projects.Project_Manager__r.Sales_Rep_Id__c : '100603093';
      ebsProject.resourceId = projects.Project_Manager__r.Sales_Rep_Id__c;
      ebsProject.projectId = projects.id;
      //ebsProject.salesOrderId;
      ebsProject.longName = projects.Long_Name__c;
      ebsProject.projectOrganization = Constants.COMM_Marketing;
      ebsProject.projectStatus = projects.Oracle_Project_Status__c;
      ebsProjects.project.add(ebsProject);
    }
    if(ebsProjects.project.size() > 0){
      
      COMM_SFDCProjectWSDLtoApexController.SFDCProject_ICPort service = 
                    new COMM_SFDCProjectWSDLtoApexController.SFDCProject_ICPort();
      try{
        service.inputHttpHeaders_x = new Map<String, String>();
        String token = EncodingUtil.base64Encode(Blob.valueOf(Label.Oracle_Integration_user_name + ':' + Label.Oracle_Integration_password));
        service.inputHttpHeaders_x.put(AUTHORIZATION, BASIC + token);
        service.timeout_x = 120000;
        response = service.ProcessSFDCProject_IC(integrationHeader, ebsProjects);
        system.debug(response);
        
        for(SchemasStrykerCOMMproject.ResponseType responseT : response.Projects.Project){
          //system.debug('Response code : ' + responseT.responseCode);
          //system.debug('Response code : ' + responseT.responseMessage);
          if(responseT.responseCode == 'ERROR'){
            errorTargetObjectIDs.add(responseT.sourceIdentifier);
          }
        }
        if(errorTargetObjectIDs.size() > 0)
        createErrorRecord('Project_Plan__c', errorTargetObjectIDs, Constants.INT_OP_PRJCT_CREATE);
      }Catch(CalloutException ex){
        system.debug('Exception : ' + ex);
        createErrorRecord('Project_Plan__c', sourceIdentifiers, Constants.INT_OP_PRJCT_CREATE);
      }
    }
  }
  
  //================================================================      
  // Name         : createErrorRecord
  // Description  : Method to create error record in error object
  // Created Date : 15th May 2016 
  // Created By   : Deepti Maheshwari (Appirio)
  // Task         : T-492006
  //================================================================== 
  private static void createErrorRecord(String objectName, List<String> targetObjectIDs,
                                        		String operation){
    COMM_Callout_Error__c error;
    List<COMM_Callout_Error__c> errorListToUpdate = new List<COMM_Callout_Error__c>();
    List<COMM_Callout_Error__c> errorList = [SELECT ID,TargetObjectID__c,
                                        Retry_Counter__c,Operation__c
                                        FROM COMM_Callout_Error__c 
                                        WHERE TargetObjectID__c IN :targetObjectIDs
                                        AND Object_Name__c = :objectName
                                        AND Operation__c = :operation];
    Map<String, COMM_Callout_Error__c> targetObjectNameMap = new Map<String, COMM_Callout_Error__c>();                                 
    for(COMM_Callout_Error__c errorObj : errorList){
      targetObjectNameMap.put(errorObj.TargetObjectID__c, errorObj);
    }
    // To do Add Response message
    for(String targetObjectID : targetObjectIDs){
      if(targetObjectNameMap.containsKey(targetObjectID)){
        error = targetObjectNameMap.get(targetObjectID);
        error.Retry_Counter__c++;
      }else{
        error = new COMM_Callout_Error__c();
        error.Object_Name__c = objectName;
        error.Retry_Counter__c = 1;
        error.TargetObjectID__c = targetObjectID;
        error.Operation__c = operation;
      }
      errorListToUpdate.add(error);
    }
    //NB - 10/24 - I-241253 - Start - removing duplicate elements from the list
    Set<COMM_Callout_Error__c> errorRecordsSet = new Set<COMM_Callout_Error__c>();
    errorRecordsSet.addAll(errorListToUpdate);  
    errorListToUpdate = new List<COMM_Callout_Error__c>();
    errorListToUpdate.addAll(errorRecordsSet);
    //NB - 10/24 - I-241253 - End
    if(errorListToUpdate.size() > 0){
      Database.upsert(errorListToUpdate, false);
      system.debug('errorListToUpdate: ' + errorListToUpdate);
    }
  }
  
  //================================================================      
  // Name         : executeProjectPlanAssignmentToOrder
  // Description  : Method called from Order trigger to assign 
  //                a project plan to sales order
  // Created Date : 15th May 2016 
  // Created By   : Deepti Maheshwari (Appirio)
  // Task         : T-492006
  //================================================================== 
  
  @future(callout=true)
  public static void asyncExecuteProjectPlanAssignmentToOrder(Set<ID> orderIds){
    List<Order> orders = [SELECT Project_Plan__r.id, Project_Plan__r.Project_Plan_Number__c,
                          Oracle_Order_Number__c  
                          FROM Order 
                          WHERE ID in : orderIds];
    if(orders.size() > 0){
      executeProjectPlanAssignmentToOrder(orders);
    }
  }
  
  public static void executeProjectPlanAssignmentToOrder(List<Order> orders){
    SchemasStrykerCOMMproject.ProjectType ebsProject;
    SchemasStrykerCOMMproject.ProjectsType ebsProjects = new SchemasStrykerCOMMproject.ProjectsType();
    ebsProjects.project = new List<SchemasStrykerCOMMproject.ProjectType>();
    SchemasStrykerCOMMproject.ProjectResponseType response = new SchemasStrykerCOMMproject.ProjectResponseType();
    
    SchemasStrykerCOMMproject.IntegrationHeaderType integrationHeader = new SchemasStrykerCOMMproject.IntegrationHeaderType();
    integrationHeader.SenderIdentifier = Constants.SENDER_ID;
    integrationHeader.ReceiverIdentifier = Constants.RECIEVER_ID;
    List<String> sourceIdentifiers = new List<String>();
    Map<String, Set<String>> projectPlanOrderIDMap = new Map<String, Set<String>>();
    List<String> errorTargetObjectIDs = new List<String>();
    for(Order ord : orders){
      ebsProject = new SchemasStrykerCOMMproject.ProjectType();
      ebsProject.businessUnit = Constants.BUSINESS_UNIT;
      ebsProject.projectNumber = ord.Project_Plan__r.Project_Plan_Number__c;
      ebsProject.projectId = ord.Project_Plan__r.id;
      if(ord.Oracle_Order_Number__c != null)//NB - 10/19 - I-240725
        ebsProject.salesOrderId = Integer.valueof(ord.Oracle_Order_Number__c);
      if(!projectPlanOrderIDMap.containsKey(ord.Project_Plan__r.Project_Plan_Number__c)){
      	projectPlanOrderIDMap.put(ord.Project_Plan__r.Project_Plan_Number__c, new Set<String>{ord.id});
      }else{
      	projectPlanOrderIDMap.get(ord.Project_Plan__r.Project_Plan_Number__c).add(ord.id);
      }
      sourceIdentifiers.add(ord.Project_Plan__r.Project_Plan_Number__c);
      
      ebsProjects.project.add(ebsProject);
    }
    if(ebsProjects.project.size() > 0){
      
      COMM_SFDCProjectWSDLtoApexController.SFDCProject_ICPort service = 
                    new COMM_SFDCProjectWSDLtoApexController.SFDCProject_ICPort();
      
      try{
        service.inputHttpHeaders_x = new Map<String, String>();
        String token = EncodingUtil.base64Encode(Blob.valueOf(Label.Oracle_Integration_user_name + ':' + Label.Oracle_Integration_password));
        service.inputHttpHeaders_x.put(AUTHORIZATION, BASIC + token);
        if(!Test.isRunningTest()) {
        response = service.ProcessSFDCProject_IC(integrationHeader, ebsProjects);
        system.debug(response);
        //system.debug(response);        
        for(SchemasStrykerCOMMproject.ResponseType responseT : response.Projects.Project){
          //system.debug('Response code : ' + responseT.responseCode);
          //system.debug('Response code : ' + responseT.responseMessage);
          if(responseT.responseCode == 'ERROR'){
          	if(projectPlanOrderIDMap.containsKey(responseT.sourceIdentifier)){
              for(String orderID : projectPlanOrderIDMap.get(responseT.sourceIdentifier)){
            	errorTargetObjectIDs.add(responseT.sourceIdentifier + ':' + orderID);
              }
          	}
          }
        }
        if(errorTargetObjectIDs.size() > 0)
          createErrorRecord('Project_Plan__c', errorTargetObjectIDs, Constants.INT_OP_PRJCT_ASSIGN);
      	}
      }Catch(CalloutException ex){
        for(String ppID : projectPlanOrderIDMap.keyset()){
          for(String OrderID : projectPlanOrderIDMap.get(ppID)){
            errorTargetObjectIDs.add(ppID + ':' + orderID);
          }          
        }
        system.debug('Exception : ' + ex);
        createErrorRecord('Project_Plan__c', errorTargetObjectIDs, Constants.INT_OP_PRJCT_ASSIGN);
      }      
    }
  }
  
  //================================================================      
  // Name         : executeSalesOrderItemUpdate
  // Description  : Method called on sales order item updation, to 
  //                send Requested_Ship_Date__c and address information
  // Created Date : 15th May 2016 
  // Created By   : Deepti Maheshwari (Appirio)
  // Task         : T-492006
  //================================================================== 
  
  @future(callout=true)
  public static void asyncExecuteSalesOrderItemUpdate(Set<ID> orderItemIds, Map<String, String> oldValueMap){
    List<OrderItem> orderItems = [SELECT Requested_Ship_Date__c, Shipping_Address__r.Location_Id__c,
                          Order.Oracle_Order_Number__c, Line_Number__c, Line_Type__c, Approval_Status__c,
                          Oracle_Line_Number__c, Oracle_Order_Line_Number__c, Oracle_Ordered_Item__c
                          FROM OrderItem
                          WHERE ID in : orderItemIds];
    if(orderItems.size() > 0){
      executeSalesOrderItemUpdateHTTP(orderItems, oldValueMap);
      //createOrderLineCallout(orderItems);
    }
  }
 
  public static void executeSalesOrderItemUpdateHTTP(List<OrderItem> orderItems, Map<String, String> oldValueMap){
    if(orderItems.size () > 0){
      String reqStr = '<?xml version="1.0" encoding="UTF-8"?><env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" ';
      reqStr += ' xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"';
      reqStr += ' xmlns:ns1="http://schemas.stryker.com/common/V1" xmlns:v1="http://schemas.stryker.com/SalesOrder/V1">';
      reqStr += '<env:Header/><env:Body><v1:salesOrderRequest xmlns="http://schemas.stryker.com/SalesOrder/V1">';
      reqStr += '<ns1:IntegrationHeader><ns1:MessageID/><ns1:CreationDateTime/>';
      reqStr += '<ns1:SenderIdentifier>SFDC</ns1:SenderIdentifier><ns1:ReceiverIdentifier>ENDERP</ns1:ReceiverIdentifier></ns1:IntegrationHeader>';
      reqStr += '<v1:orderHeader /><v1:lineItems>';
      String dataStr = '';
      for(OrderItem ord : orderItems){
        dataStr += '<v1:lineItem><v1:operation /><v1:orgId>';
        if(ord.Oracle_Order_Line_Number__c != null) //NB - 10/19 - I-240725
          dataStr += 88 + '</v1:orgId><v1:lineNumber>' + String.valueof(ord.Oracle_Order_Line_Number__c);
        if(ord.Oracle_Line_Number__c != null)//NB - 10/19 - I-240725
          dataStr += '</v1:lineNumber><v1:lineId>' + Integer.valueOf(ord.Oracle_Line_Number__c);
        dataStr += '</v1:lineId><v1:orderedItem>' + ord.Oracle_Ordered_Item__c;
        if(ord.Requested_Ship_Date__c != null)//NB - 10/19 - I-240725
          dataStr += '</v1:orderedItem><v1:requestDate>' + String.valueof(ord.Requested_Ship_Date__c);
        if(ord.Order.Oracle_Order_Number__c != null)//NB - 10/19 - I-240725
          dataStr += '</v1:requestDate><v1:oracleOrderNumber>' + Integer.valueof(ord.Order.Oracle_Order_Number__c) + '</v1:oracleOrderNumber>';
        dataStr += '<v1:flowStatusCode>' + ord.Approval_Status__c + '</v1:flowStatusCode>';
        if(ord.Shipping_Address__c != null && ord.Shipping_Address__r.Location_Id__c != null){
          //if(ord.Shipping_Address__r.Location_Id__c.contains('_')){
          //  String[] addressSplit = ord.Shipping_Address__r.Location_Id__c.split('_');
          //  dataStr += '<v1:shipToId>' + Integer.valueof(addressSplit.get(addressSplit.size() - 1)) + '</v1:shipToId>';
          //}
          dataStr += '<v1:shipToId>' + ord.Shipping_Address__r.Location_Id__c + '</v1:shipToId>';
        }
        dataStr += '</v1:lineItem>';
      }
      reqStr += dataStr + '</v1:lineItems></v1:salesOrderRequest></env:Body></env:Envelope>';
      system.debug('Request String : ' + reqStr);
      HttpRequest request = new HttpRequest();
      request.setBody(reqStr);
      request.setMethod('POST');
      String token = EncodingUtil.base64Encode(Blob.valueOf(Label.Oracle_Integration_user_name + ':' + Label.Oracle_Integration_password));
      request.setHeader(AUTHORIZATION, BASIC + token);
      request.setHeader('SOAPAction','');
      request.setHeader('Content-type', 'text/xml');
      request.setTimeout(120000);
      request.setEndpoint(Label.Sales_Order_EP);
      Map<String, String> lineItemErrorMessageMap = new Map<String, String>();
      Http httpClient = new Http();
      try{
        HTTPResponse httpResponse = httpClient.send(request);
        String responseBody = httpResponse.getBody();
        XmlStreamReader xsr = new XmlStreamReader(responseBody);
        system.debug('Response Body : ' + responseBody);
        String errorMsg = '';
        while(xsr.hasNext()) {
          if (xsr.getEventType() == XmlTag.START_ELEMENT) {
            if(xsr.getLocalName() == 'responseCode'){
              xsr.next();
              if(xsr.getEventType() == XmlTag.START_ELEMENT){
                break;
              }
              /*else if(xsr.getEventType() == XmlTag.CHARACTERS){
              }*/
            }
            //Fix for I-242660 Starts
            if(xsr.getLocalName() == 'orderLineResponse'){
              xsr.next();
              if(xsr.getEventType() == XmlTag.START_ELEMENT){
                if(xsr.getLocalName() == 'responseMessage'){
                  xsr.next();
                  if(xsr.getEventType() == XmlTag.CHARACTERS){
                    errorMsg = xsr.getText();
                  } 
                  xsr.next();
                  xsr.next();
                }
                if(xsr.getEventType() == XmlTag.START_ELEMENT){
                    if(xsr.getLocalName() == 'sourceIdentifier'){
                      xsr.next();
                      if(xsr.getEventType() == XmlTag.CHARACTERS){
                        lineItemErrorMessageMap.put(xsr.getText(), errorMsg);
                      }  
                    }
                }
              }
            }
            //Fix for I-242660 ends
          }
          xsr.next();
        }
        //system.debug('Error Map : ' + lineItemErrorMessageMap);
        if(lineItemErrorMessageMap.size() > 0 && (oldValueMap != null && oldValueMap.size() > 0)){
            revertErrorRecords(lineItemErrorMessageMap,oldValueMap);
        }
      }catch(Exception ex){
        system.debug('Exception : ' + ex);
        
      }
      
    }
  }
  
  //Fix for I-242660
  private static void revertErrorRecords(Map<String, String> lineItemErrorMessageMap , Map<String, String> oldValueMap){
     Set<ID> orderIdSet = new Set<Id>();
     Map<String, OrderItem> orderProductMapByLineNumber = new Map<String, OrderItem>();
     try{
         for(String recordId : oldValueMap.keyset()){
            orderIdSet.add(recordId.split('-').get(0));
         }
         for(OrderItem ord : [SELECT Requested_Ship_Date__c, Approval_Status__c, Shipping_Address__c , 
                         Callout_Result__c, id, Oracle_Line_Number__c
                         FROM OrderItem WHERE id in :orderIDSet]){
             orderProductMapByLineNumber.put(ord.Oracle_Line_Number__c, ord);
         }
         String Key;
         if(orderProductMapByLineNumber.size() > 0){
             for(String orderNumber : orderProductMapByLineNumber.keyset()){
                 if(lineItemErrorMessageMap.containsKey(orderNumber)){
                     orderProductMapByLineNumber.get(orderNumber).Callout_Result__c = lineItemErrorMessageMap.get(orderNumber);
                 }
                 key = orderProductMapByLineNumber.get(orderNumber).id + '-' + 'Requested_Ship_Date__c';
                 if(oldValueMap.containsKey(key)){
                    orderProductMapByLineNumber.get(orderNumber).Requested_Ship_Date__c = Date.valueof(oldValueMap.get(key));
                 }
                 key = orderProductMapByLineNumber.get(orderNumber).id + '-' + 'Shipping_Address__c';
                 if(oldValueMap.containsKey(key)){
                     orderProductMapByLineNumber.get(orderNumber).Shipping_Address__c = oldValueMap.get(key);
                 }
             }
             Constants.isTriggerExecuted = true;
             update orderProductMapByLineNumber.values();
         }
     }Catch(Exception ex){
         system.debug('exception : ' + ex);
     }
     
  }
}