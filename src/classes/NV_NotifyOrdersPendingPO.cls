//
// (c) 2015 Appirio, Inc.
//
// Apex Class Name: NV_NotifyOrdersPendingPO
// Description: This is a scheduled apex job class for processing awaiting PO orders.
//
// 16th December 2015   Hemendra Singh Bhati   Original (Task # T-458395)
//
global class NV_NotifyOrdersPendingPO implements Schedulable {
  /*
  @method      : execute
  @description : This method executes apex scheduler logic.
  @params      : SchedulableContext theSchedulableContext
  @returns     : void
  */
  global void execute(SchedulableContext theSchedulableContext) {
    // Initiating invoice notifications process.
    NV_InvoiceNotifications theInstance = new NV_InvoiceNotifications();
		theInstance.notifyOrdersPendingPO();
  }
}