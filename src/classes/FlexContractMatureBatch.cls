//
// (c) Appirio, Inc.
//
//  Batch class to check the scheduled maturity date on Flex Contract.
//
// July 21, 2015 Sunil    Original
// Aug  19, 2015 Naresh   Changed new Opportunity Probability to 10% from old Opportunity Probability
global class FlexContractMatureBatch implements Database.Batchable<SObject>, System.Schedulable {
    Date CRITERIA_DATE = System.Today().addDays(180);

  global final String query = 'SELECT Scheduled_Maturity_Date__c, Opportunity__c, name, Term__c, Equipment_Funded_Amt__c, End_of_Term__c FROM Flex_Contract__c' +
                                ' WHERE Is_Batch_Processed__c = false AND ' +
                                ' Scheduled_Maturity_Date__c <= :CRITERIA_DATE'; //updated for the story #S-404578

  global Database.QueryLocator start(Database.BatchableContext bc){
    return Database.getQueryLocator(query);
  }

  global void execute(Database.BatchableContext bc, List<sObject> scope) {
    List<Flex_Contract__c> lstContacts  = (List<Flex_Contract__c>)scope;
    System.debug('@@@' +lstContacts);

    // find set of Opportunity eligible for cloning.
    //Map<Id, Id> mapOppIds = new Map<Id, Id>(); // commented for the story #S-404578
    
    //added for the story #S-404578
    Map<Id, Flex_Contract__c> mapOppToContract = new Map<Id, Flex_Contract__c>();
    
    for(Flex_Contract__c contract :lstContacts){
        mapOppToContract.put(contract.Opportunity__c, contract); //updated for the story #S-404578
    }
    
    //added for the story #S-404578 #START
    
    Map<Id, Flex_Contract_Line__c> mapFCToFCL = new Map<Id, Flex_Contract_Line__c>();
    List<Id> listAccountId = new List<Id>();
    
    // pick any Flex_Contract_Line__c record associated Flex_Contract__c record
    for(Flex_Contract_Line__c fcl : [select id, Location_Account__c, Business_Unit__c, PSR_Contact__c, PSR_Contact__r.name, Flex_Sales_Rep__c, Flex_Contract__c from Flex_Contract_Line__c where Flex_Contract__c = :lstContacts]) {
    	if(!mapFCToFCL.containsKey(fcl.Flex_Contract__c)) {
    		mapFCToFCL.put(fcl.Flex_Contract__c, fcl);
    		listAccountId.add(fcl.Location_Account__c);
    	}
    }
    
    //fetch account fields associated to the flex contract line record
    Map<Id, Account> mapAccount = new Map<Id, Account>([select id, Website, Type, BillingCity, BillingStreet, BillingState, BillingCountry, BillingPostalCode  from account where id in :listAccountId]);
    
    //added for the story #S-404578 #END

    List<Opportunity> lstOpps = new List<Opportunity>([SELECT RecordTypeId, Name, OwnerId, Deal_Type__c, Document_Type__c, AccountId, StageName, Probability, CloseDate
                                 FROM Opportunity
                                 WHERE Id IN :mapOppToContract.keySet()]); //updated for the story #S-404578

    //System.debug('mapOppIds========='+mapOppIds); //commented for the story #S-404578
    
    /* Commented out for the story #S-404578 #START
    // insert new Opportunities
    List<Opportunity> lstNewOpps = new List<Opportunity>();
    for(Opportunity opp :lstOpps){
      Opportunity newOpp = new Opportunity();
      newOpp.RecordTypeId = opp.RecordTypeId;
      newOpp.Name = opp.Name + ' - Renewal';
      newOpp.AccountId = opp.AccountId;
      newOpp.StageName = 'Quoting';
      newOpp.Deal_Type__c = opp.Deal_Type__c;
      newOpp.Document_Type__c = opp.Document_Type__c;
      newOpp.Probability = 10;
      newOpp.CloseDate = System.today();
      newOpp.OwnerId = opp.OwnerId;
      System.debug('mapOppIds.get(opp.Id)========='+mapOppIds.get(opp.Id));
      newOpp.Existing_Contract__c = mapOppIds.get(opp.Id);
      lstNewOpps.add(newOpp);
      
    }
    System.debug('@@@lstNewOpps' + lstNewOpps);
    Database.insert(lstNewOpps);
    Commented out for the story #S-404578 #STOP */
    
    // added for the story #S-404578 #START
    
    //fetch the record type id of lead record 'Flex Lead Record Type'
    Id leadRecordTypeId;
    List<RecordType> lstRT = new List<RecordType>([select id from RecordType where name = 'Flex Lead Record Type' and sobjecttype = 'Lead']);
    if(lstRT.size() > 0) {
    	leadRecordTypeId = lstRT[0].id;
    }
    
    //mapping for the Business unit field from Flex Contract Line record to Lead record
    Map<String, String> mapFCLToLead = new Map<String, String>();
    mapFCLToLead.put(Constants.ENDO, Constants.ENDO_FullForm);
    mapFCLToLead.put(Constants.IVS, Constants.IVS_FullForm);
    mapFCLToLead.put(Constants.PC, Constants.PC_FullForm);
    mapFCLToLead.put(Constants.PH, Constants.PH_FullForm);
    mapFCLToLead.put(Constants.Surg, Constants.Surg_FullForm);
    
    List<Lead> lstNewLead = new List<Lead>();
    
    if(leadRecordTypeId != null) {
    	
    	//insert new leads
    	for(Opportunity opp :lstOpps){
    	
	    	Lead newLead = new Lead();
	    	newLead.recordTypeId = leadRecordTypeId;
	    	newLead.status = 'Unqualified';
	    	newLead.Description = 'Renewal Lead';
	    	newLead.LeadSource = 'Expiring Contract';
	    	
	    	if(mapOppToContract.containsKey(opp.id)) {
	    		
	    		newLead.Company = mapOppToContract.get(opp.id).name != null ? mapOppToContract.get(opp.id).name : ''; 
	    		newLead.Term_Length__c = mapOppToContract.get(opp.id).Term__c != null ? mapOppToContract.get(opp.id).Term__c : 0;	
	    		newLead.Equipment_Total__c = mapOppToContract.get(opp.id).Equipment_Funded_Amt__c != null ? mapOppToContract.get(opp.id).Equipment_Funded_Amt__c : 0;
	    		newLead.End_Of_Term__c = mapOppToContract.get(opp.id).End_of_Term__c != null ? mapOppToContract.get(opp.id).End_of_Term__c : '';
	    		
	    		//Get Flex Contract Line record
	    		if(mapFCToFCL.containsKey(mapOppToContract.get(opp.id).id) && mapFCToFCL.get(mapOppToContract.get(opp.id).id).PSR_Contact__c != null 
	    				&& mapFCToFCL.get(mapOppToContract.get(opp.id).id).Flex_Sales_Rep__c != null) {
	    			
	    			if(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Business_Unit__c != null && mapFCLToLead.containsKey(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Business_Unit__c)) {
	    				newLead.Business_Unit__c = mapFCLToLead.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Business_Unit__c);
	    			
	    			} else if(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Business_Unit__c == null) {
	    				newLead.Business_Unit__c = Constants.NAV;
	    			
	    			} else if(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Business_Unit__c != null) {
	    				newLead.Business_Unit__c = mapFCToFCL.get(mapOppToContract.get(opp.id).id).Business_Unit__c;
	    			}
	    			
	    			newLead.LastName = mapFCToFCL.get(mapOppToContract.get(opp.id).id).PSR_Contact__r.name;
	    			newLead.ownerId = mapFCToFCL.get(mapOppToContract.get(opp.id).id).Flex_Sales_Rep__c;
	    			
	    			if(mapFCToFCL.get(mapOppToContract.get(opp.id).id).PSR_Contact__r.name.length() >= 40) {
	    				newLead.Rep_Name__c = mapFCToFCL.get(mapOppToContract.get(opp.id).id).PSR_Contact__r.name.substring(0,39);
	    			} else {
	    				newLead.Rep_Name__c = mapFCToFCL.get(mapOppToContract.get(opp.id).id).PSR_Contact__r.name;
	    			}
	    			
	    			//Get Location Account record on the Flex Contract Line record
	    			if(mapAccount.containsKey(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c)) {
	    				
	    				newLead.Website = mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).Website != null ? mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).Website : '';
	    				
	    				if(mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).Type != null) {
	    					newLead.Customer_Type_of_Facility__c = mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).Type;
	    				}
	    				
	    				if(mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingStreet != null) {
	    					newLead.Street = mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingStreet;
	    				}
	    				
	    				if(mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingCity != null ) {
	    					newLead.City = mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingCity;
	    				}
	    				
	    				if(mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingState != null) {
	    					newLead.State = mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingState;
	    				}
	    				
	    				if(mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingCountry != null) {
	    					newLead.Country = mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingCountry;
	    				}
	    				
	    				if(mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingPostalCode != null) {
	    					newLead.PostalCode = mapAccount.get(mapFCToFCL.get(mapOppToContract.get(opp.id).id).Location_Account__c).BillingPostalCode;
	    				}
	    			}
	    			
	    			//adding to the list to insert
					lstNewLead.add(newLead);
	    		}
	    	}
	    }
    } 
    
    
    Database.insert(lstNewLead);
    
    // added for the story #S-404578 #STOP

    // Update Flex Contracts.
    List<Flex_Contract__c> lstContractsToUpdate = new List<Flex_Contract__c>();
    for(Flex_Contract__c contract :lstContacts){
      contract.Is_Batch_Processed__c = true;
      lstContractsToUpdate.add(contract);
    }
    System.debug('@@@lstContractsToUpdate' + lstContractsToUpdate);
    Database.update(lstContractsToUpdate);

  }

  global void finish(Database.BatchableContext bc) {
    //if(currentIndex != null) Database.executeBatch(new BatchDeleteData(null,currentIndex - 1));
  }

  // Schedular for batch.
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
      Id batchId = Database.executeBatch(new FlexContractMatureBatch(), 200);
      system.debug('@@@batchId = ' + batchId);
    }
  }

}