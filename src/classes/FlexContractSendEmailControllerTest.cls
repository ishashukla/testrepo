//
// (c) 2012 Appirio, Inc.
//
//  Test class for FlexContractSendEmailController
//
// 19 Aug 2015     Sunil
//
@isTest
private class FlexContractSendEmailControllerTest {
  
  static testmethod void testMethodSendEmail(){
    
    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Type = 'Hospital';
    acc.Legal_Name__c = 'Test';
    acc.Flex_Region__c = 'test';
    insert acc;

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;
    TestUtils.createCommConstantSetting();
    Opportunity opp = new Opportunity();
    opp.StageName = 'Quoting';
    opp.AccountId = acc.Id;
    opp.CloseDate = Date.newinstance(2014,5,29);
    opp.Name = 'test value';
    Schema.RecordTypeInfo rtByNameFlex = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional');
    Id recordTypeFlex = rtByNameFlex.getRecordTypeId();
    opp.recordTypeId = recordTypeFlex;
    insert opp;

    Flex_Contract__c contract = new Flex_Contract__c();
    contract.Account__c = acc.Id;
    contract.Opportunity__c = opp.Id;
    contract.Name = opp.Name;
    contract.Scheduled_Maturity_Date__c = System.today();
    insert contract;

    

    Test.stopTest();
      
      FlexContractSendEmailController objController = new FlexContractSendEmailController(new ApexPages.StandardController(contract));
      Test.setCurrentPageReference(new PageReference('Page.myPage'));
      System.currentPageReference().getParameters().put('test', 'test');      
      objController.redirectPage();
      }
      }