public with sharing class AutocompleteSearchController {
 	private sObject sObj;
	public String field{get;set;}
	public String require{get;set;}
 	public List<sObject> sObjL {
 		get;
 		set{
 			sObjL = value;
	 		setSObj(sObjL.get(0));
 		}
 	}
    public sObject getSObj(){
        return sObj;
    }
    public void setSObj(sObject value){
    	sObj = value;
        try{
            DescribeSObjectResult r = sObj.getsObjectType().getDescribe();
            sObjLabel = r.getLabel();
            sObjName = r.getName();
            System.debug(r.getName());
        }catch(Exception e){ 
            System.debug('error getting sobject'); 
        }
       
    }
    public String sObjId{get;set;}
	public String sObjLabel{get; set;}
	public String sObjName{get; set;}
	public PageReference selected(){
		sObj = sObj.getsObjectType().newSObject(sObjId);
		sObjL.clear();
		sObjL.add(sObj);
		return null;
	}
	/*@isTest static void search() {
		AutocompleteSearchController auto = new AutocompleteSearchController();
		List<Account> al = new Account[]{new Account(name = 'acc')};
		
		auto.SObjL = al;
		sObject s = auto.getsObj();
		System.assertEquals('Account', auto.sObjName);
		System.assertEquals(s.get('Name'), 'acc');
		
		Account a = new Account(name = 'acc2');
		insert a;
		
		auto.sObjId = a.id; 
		
		auto.selected();
		
		system.assertEquals(al.get(0).id, a.id);
	}*/
	
}