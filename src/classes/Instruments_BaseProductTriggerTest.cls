/**================================================================      
* Appirio, Inc
* Name: Instruments_BaseProductTriggerTest 
* Description: Test Class for Instruments_BaseProductTrigger
* Created Date: April 15, 2016
* Created By: Meena Shringi (Appirio)
*
* Date Modified      Modified By            Description of the update
* June 24, 2016      Isha Shukla            Modified(T-513054)
* July 2, 2016       Isha Shukla            Modified(T-516161)
* Nov 21 2016        Nitish Bansal          I-244792 - Empower prod sync (Replaced with prod copy since all changes are from Empower only)
==================================================================*/
@isTest 
public class Instruments_BaseProductTriggerTest {
   public static Id oppInstrumentsRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type').getRecordTypeId();
   public static User usr;
   /* @isTest static void testInstruments_BaseTriggerHandler() { 
        User usr = TestUtils.createUser(1, 'System Administrator', false ).get(0);
        usr.Division = 'NSE';
        insert usr;
        system.runAs(usr){
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
            Account acc = TestUtils.createAccount(1,true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.Opportunity_Number__c = '50001';
            opp.OwnerId = usr.Id;
            insert opp;
            Base_Trail_Survey__c survey = TestUtils.createBaseTrailSurvey(1, opp.Id, true).get(0);
            BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc.Id, opp.Id, site, True);
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(10, product.Id,survey.Id,'12',quote.Id,false);
            for(Integer i = 0; i < 10; i++) {
                baseProdlst[i].External_Id__c = '12'+i;
            }
            Test.startTest();
            insert baseProdlst;
            Test.stopTest();
            List<Base_Product__c> baseProductlst = [select Prod_Description__c from Base_Product__c];
            for(Base_Product__c baseProd : baseProductlst) {
                System.assertEquals(product.Description, baseProd.Prod_Description__c, 'description on base product must be populated from product');
            }
            
        } 
    }*/
    
    // This test method testing when parent Opportunity does not have child base trail record
    @isTest static void testFirst() {
        
        createTestData();
        system.runAs(usr) {
            // creating test data
             
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            
            insert product;
            
            Account acc = TestUtils.createAccount(1,true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.Opportunity_Number__c = '50001';
            opp.OwnerId = usr.Id;
            opp.RecordTypeId = oppInstrumentsRecTypeId;
            Test.startTest();
            insert opp;
             
            BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc.Id, opp.Id, site, True);
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(10, product.Id,null,'12',quote.Id,false);
            for(Integer i = 0; i < 10; i++) {
                baseProdlst[i].External_Id__c = '12'+i;
            }
            Test.stopTest();
            insert baseProdlst;
            
            System.assertEquals([SELECT Id,(SELECT Id FROM Base_Trail_Surveys__r) FROM Opportunity WHERE Id = :opp.Id].Base_Trail_Surveys__r[0].Id == [SELECT Base_Trail_Survey__c FROM Base_Product__c WHERE Oracle_Quote__c = :quote.Id LIMIT 1].Base_Trail_Survey__c, True);
            System.assertEquals([SELECT Oracle_Quote__c FROM Base_Trail_Survey__c WHERE Opportunity__c = :opp.Id LIMIT 1].Oracle_Quote__c == [SELECT Oracle_Quote__c FROM Base_Product__c WHERE Oracle_Quote__c = :quote.Id LIMIT 1].Oracle_Quote__c, True);
          }
       
    }
    
    // Test method testing when parent Opportunity does have child base trail record 
    @isTest static void testSecond() {
      
        createTestData();
        system.runAs(usr) {
            // creating test data
            
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
           
            Account acc = TestUtils.createAccount(1,true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.Opportunity_Number__c = '50001';
            opp.OwnerId = usr.Id;
            opp.RecordTypeId = oppInstrumentsRecTypeId;
            Test.startTest();
            insert opp;
            
            //Base_Trail_Survey__c survey2 = TestUtils.createBaseTrailSurvey(1, opp.Id, true).get(0);
            BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc.Id, opp.Id, site, True);
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(10, product.Id,null,'12',quote.Id,false);
            for(Integer i = 0; i < 10; i++) {
                baseProdlst[i].External_Id__c = '12'+i;
            }
            Test.stopTest();
            insert baseProdlst;
            
            System.assertEquals([SELECT Id,(SELECT Id FROM Base_Trail_Surveys__r) FROM Opportunity WHERE Id = :opp.Id].Base_Trail_Surveys__r[0].Id == [SELECT Base_Trail_Survey__c FROM Base_Product__c WHERE Oracle_Quote__c = :quote.Id LIMIT 1].Base_Trail_Survey__c, True);
          }
         
    }
    // Testing when Base product price and quantity updates
    @isTest static void testThird() {
      
        createTestData();
        system.runAs(usr) {
            // creating test data
            
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
            Account acc = TestUtils.createAccount(1,true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.Opportunity_Number__c = '50001';
            opp.OwnerId = usr.Id;
            opp.RecordTypeId = oppInstrumentsRecTypeId;
            Test.startTest();
            insert opp;
            
            //Base_Trail_Survey__c survey2 = TestUtils.createBaseTrailSurvey(1, opp.Id, true).get(0);
            BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc.Id, opp.Id, site, True);
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(2, product.Id,null,'12',quote.Id,false);
            baseProdlst[0].External_Id__c = '1242';
            baseProdlst[0].Price__c = 10;
            baseProdlst[0].Quantity__c = 1;
            baseProdlst[1].External_Id__c = '1233';
            baseProdlst[1].Price__c = 10;
            baseProdlst[1].Quantity__c = 1;
            Test.stopTest();
            insert baseProdlst;
            baseProdlst[1].Price__c = 20;
            baseProdlst[1].Quantity__c = 10;
            
            update baseProdlst;
            
            //System.assertEquals([SELECT Roll_Up_Total__c FROM Base_Trail_Survey__c WHERE Opportunity__c = :opp.Id LIMIT 1].Roll_Up_Total__c != null,true);
            }
       
    }
    // Testing when base product deletes 
    @isTest static void testFourth() {
      
        createTestData();
        system.runAs(usr) {
            // creating test data
            
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
            Account acc = TestUtils.createAccount(1,true).get(0);
            
            Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp.Business_Unit__c = 'NSE';
            opp.Opportunity_Number__c = '50001';
            opp.OwnerId = usr.Id;
            opp.RecordTypeId = oppInstrumentsRecTypeId;
             Test.startTest();
            insert opp;
            
            //Base_Trail_Survey__c survey2 = TestUtils.createBaseTrailSurvey(1, opp.Id, true).get(0);
            BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
            BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc.Id, opp.Id, site, True);
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(2, product.Id,null,'12',quote.Id,false);
            baseProdlst[0].External_Id__c = '1242';
            baseProdlst[0].Price__c = 10;
            baseProdlst[0].Quantity__c = 1;
            baseProdlst[1].External_Id__c = '1233';
            baseProdlst[1].Price__c = 10;
            baseProdlst[1].Quantity__c = 1;
            Test.stopTest(); 
            insert baseProdlst;
            delete baseProdlst;            
           
            //System.assertEquals(survey2.Roll_Up_Total__c == null,true);
        }
      
    }
    
    public static void createTestData(){
      usr = TestUtils.createUser(1, null, false ).get(0);
      usr.Division = 'NSE;IVS';
      insert usr;
    }
}