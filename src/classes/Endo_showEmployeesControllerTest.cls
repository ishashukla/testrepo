/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  March 14, 2014 
 Purpose       :  Test Class for Case Endo_showEmployeesController
*******************************************************************************/
@isTest
private class Endo_showEmployeesControllerTest {
  
  //-----------------------------------------------------------------------------------------------
  // Method for test employeess inline vf page at product
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testMethod1(){
    Id employeeId = Schema.sObjectType.Contact.getRecordTypeInfosByName().get('Endo Employee').getRecordTypeId();
    
    Account acc = Endo_TestUtils.createAccount(1, true).get(0);
    
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Product_Line__c = 'test';
    con.RecordTypeId = employeeId;
    insert con;
    
    Product2 product = Endo_TestUtils.createProduct(1, false).get(0);
    product.ODP_Category__c = 'test';
    product.Part_Number__c = '123';
    insert product;
    
    Test.startTest();
    apexPages.standardController sc = new ApexPages.StandardController(product);
    Endo_showEmployeesController obj = new Endo_showEmployeesController(sc);
    List<Contact> lstContact = obj.getContactList();
    
    Test.stopTest();
    
    //Verify Results
    System.assert(lstContact.size() > 0);

  }
  
}