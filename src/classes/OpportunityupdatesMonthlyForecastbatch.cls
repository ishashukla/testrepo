global class OpportunityupdatesMonthlyForecastbatch implements Database.Batchable<SObject> {

 // @description: start of batch class 
 //               
 // @param: none
 // @return: void
 global Database.QueryLocator start(Database.BatchableContext BC){ 
    List <string> stageValue = new  List <String>{Constants.Forecasted, Constants.POsubmitted,Constants.ClosedWon};
    String query = 'Select CloseDate, Amount , Owner.Name , Ownerid ,StageName , RecordType.DeveloperName from Opportunity where StageName in: stageValue and LastModifiedDate > YESTERDAY';
    //String query= 'SELECT Id, recordType.name, Name, rC_Giving__Calculated_Giving_Type__c, CloseDate, AccountID FROM Opportunity where  (recordType.name in ( \'Donation\', \'Purchase\') or (rC_Giving__Parent__c!=null and recordType.name in ( \'Transaction\'))) order by AccountID,  CloseDate asc, rC_Giving__Parent__c ';     
   return Database.getQueryLocator(query);
 }
 // @description: Update all Opportunity
 // @param: none
 // @return: void
global void execute(Database.BatchableContext BC, List<sObject> scope){       
  try{  
   system.debug('***scope>>'+scope);
   Integer num = 0;
   String month;
   Integer year;
   Set<String> users = new Set<String>();
   Map<String,List<opportunity>> OpptyMap = new Map<String,List<opportunity>>();
   for(Opportunity opp : (List<Opportunity>)scope) {
     if(opp.RecordType.DeveloperName.contains(Constants.COMM)) {  
       num = opp.CloseDate.month();
       year = opp.CloseDate.Year();
       if(num == 1)  month = 'January';
       else if(num == 2)  month = 'February';
       else if(num == 3)  month = 'March';
       else if(num == 4)  month = 'April';
       else if(num == 5)  month = 'May';
       else if(num == 6)  month = 'June';
       else if(num == 7)  month = 'July';
       else if(num == 8)  month = 'August';
       else if(num == 9)  month = 'September';
       else if(num == 10)  month = 'October';
       else if(num == 11)  month = 'November';
       else if(num == 12)  month = 'December';
       system.debug('***update Lifetime Summary>>');
       List<Opportunity> oppty = OpptyMap.get(opp.Owner.Name) == null ?
                                                       new  List<Opportunity>() :
                                                       OpptyMap.get(opp.Owner.Name);
       oppty.add(opp);
       OpptyMap.put(opp.Owner.Name + '~' + month + '~' + year , oppty);
       users.add(opp.Owner.Name);
       System.debug('$$$$$$$$$$$$$$$$$$$$$' + opptyMap);
     }
   }
   List<Monthly_Forecast__c> forecastList = new List<Monthly_Forecast__c>();
   for(Monthly_Forecast__c forecast: [Select User__c,Year__c,Month__c, Capital_Forecast__c From Monthly_Forecast__c Where User__c in: users]) {
   System.Debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!'+ forecast.Capital_Forecast__c );
    if(OpptyMap.containsKey(forecast.User__c+ '~' + forecast.Month__c + '~' + forecast.Year__c)) {
      forecast.Capital_Forecast__c = 0;
      for(Opportunity opp: OpptyMap.get(forecast.User__c + '~' + forecast.Month__c + '~' + forecast.Year__c)) {
      System.Debug('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!'+ OpptyMap.get(forecast.User__c+ '~' + forecast.Month__c + '~' + forecast.Year__c) );
        if(opp.Amount == null){
          opp.Amount = 0;
        }
        forecast.Capital_Forecast__c += opp.Amount ;
      }
      forecastList.add(forecast);
    }
  }  
   if(forecastList.size() > 0) {
     System.Debug('############################# IN Update' );
     update forecastList;   
   }
  }
   catch(Exception ex) {
    System.debug('Batch update failed AccountUpdateSummarybatch : Error > '+ex);
   }
  
}
 // @description: Finish method of batch job
 // @param: none
 // @return: void
 // 05 Aug, 2015  Puneet Sardana  Original 
 global void finish(Database.BatchableContext BC){  
    
 }

}