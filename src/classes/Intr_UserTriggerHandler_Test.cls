// (c) 2016 Appirio, Inc.
//
// Class Name: SyncStandardAccountTeamMemberTest 
// Description: Test Class for Intr_UserTriggerHandler
// June 29, 2016    Shreerath Nair    Original
@isTest
private class Intr_UserTriggerHandler_Test {

    static testMethod void testAccountTeamMember() {
        // TO DO: implement unit test
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User usr1 = TestUtils.createUser(1, 'Instruments Sales User',true).get(0);
     	User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
      	
      	System.runAs(usr) {
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            Test.startTest();
            AccountTeamMember atm = new AccountTeamMember();
            atm.AccountId = acc.Id;
            atm.TeamMemberRole = 'Navigation';
            atm.UserId = usr1.Id;
            insert atm;
            AccountShare accShare = new AccountShare();
            accShare.AccountId = acc.Id;
            accShare.UserOrGroupId = usr1.Id;
            accShare.RowCause = Schema.AccountShare.RowCause.Manual;
            accShare.OpportunityAccessLevel = 'None';
            accShare.ContactAccessLevel = 'None';
            accShare.CaseAccessLevel = 'None';
            accShare.AccountAccessLevel = 'Edit';
            insert accShare;
            usr1.ProCare_Region_Rep__c = usr2.id;
      	    update usr1;
      	}
      	
    }
    
     static testMethod void testAccountTeamMember1() {
        // TO DO: implement unit test
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User usr1 = TestUtils.createUser(1, 'Instruments Sales User',true).get(0);
     	User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
      	
      	System.runAs(usr) {
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            Test.startTest();
            AccountTeamMember atm = new AccountTeamMember();
            atm.AccountId = acc.Id;
            atm.TeamMemberRole = 'Navigation';
            atm.UserId = usr1.Id;
            insert atm;
            AccountTeamMember atm1 = new AccountTeamMember();
            atm1.AccountId = acc.Id;
            atm1.TeamMemberRole = 'ProCare Rep';
            atm1.UserId = usr.Id;
            insert atm1;
            AccountShare accShare = new AccountShare();
            accShare.AccountId = acc.Id;
            accShare.UserOrGroupId = usr1.Id;
            accShare.RowCause = Schema.AccountShare.RowCause.Manual;
            accShare.OpportunityAccessLevel = 'None';
            accShare.ContactAccessLevel = 'None';
            accShare.CaseAccessLevel = 'None';
            accShare.AccountAccessLevel = 'Edit';
            insert accShare;
            usr1.ProCare_Region_Rep__c = usr2.id;
      	    update usr1;
      	}
      	
    }
    
    
    
}