// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_RMAShipmentOrderLineTriggerTest - Test class for COMM_RMAShipmentOrderLineTriggerHandler
//
// 7 Nov 2016, Isha Shukla (T-553574) 
//
@isTest(SeeAllData=true)
private class COMM_RMAShipmentOrderLineTriggerTest {
    static Map<String, Schema.RecordTypeInfo> partOrderInfo = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
    static String PORecordTypeId = partOrderInfo.get('RMA').getRecordTypeId();
    static SVMXC__RMA_Shipment_Order__c partOrderRecord;
    static SVMXC__RMA_Shipment_Order__c partOrderRecord2;
    @isTest static void testFirstCase() {
        createData();
        List<SVMXC__RMA_Shipment_Line__c> lineList = new List<SVMXC__RMA_Shipment_Line__c>();
        SVMXC__RMA_Shipment_Line__c line1 = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__RMA_Shipment_Order__c = partOrderRecord.Id,
            SVMXC__Expected_Quantity2__c = 1
        );
        lineList.add(line1);
        SVMXC__RMA_Shipment_Line__c line2 = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__RMA_Shipment_Order__c = partOrderRecord2.Id,
            SVMXC__Expected_Quantity2__c = 1
        );
        lineList.add(line2);
        SVMXC__RMA_Shipment_Line__c line3 = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__RMA_Shipment_Order__c = partOrderRecord.Id,
            SVMXC__Expected_Quantity2__c = 1
        );
        lineList.add(line3);
        insert lineList;
        Test.startTest();
        lineList[0].SVMXC__Expected_Quantity2__c = 2;
        lineList[1].SVMXC__Expected_Quantity2__c = 2;
        lineList[2].SVMXC__Expected_Quantity2__c = 2;
        update lineList;
        Test.stopTest();
        System.assertEquals([SELECT Callout_Result__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id = :partOrderRecord.Id].Callout_Result__c,'Batch id 59 is processed sucessfully.');
    }
    private static void createData() {
        User integrationUser = TestUtils.createUser(1, 'DataMigrationIntegration', true).get(0);
        Account acc = TestUtils.createAccount(1, true).get(0);
        Product2 productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pb = [SELECT Id from Pricebook2 where Name = 'COMM Price Book'];
        if(pb == null) {
            pb = new Pricebook2(name = 'COMM Price Book',isActive = true);
            insert pb; 
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Order orderRecord = new Order(
            AccountId = acc.Id,EffectiveDate = system.today(),Status= 'Entered',Pricebook2id = pb.id,
            RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId()
        );
        COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA=true;
        insert orderRecord;
        Orderitem orderLines = new Orderitem(
            OrderId = orderRecord.Id,PriceBookEntryId = pbe.Id,Status__c = 'Entered',
            Requested_Ship_Date__c = Date.today(),Quantity = 1,UnitPrice  = 0,Description = 'test'
        );
        insert orderLines;
        if(!(RTMap__c.getInstance(PORecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = PORecordTypeId,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='RMA');
            insert rtMap;
        }
        partOrderRecord = new SVMXC__RMA_Shipment_Order__c(
            RecordTypeId = PORecordTypeId,SVMX_Order__c = orderRecord.Id
        );
        insert partOrderRecord;
        partOrderRecord2 = new SVMXC__RMA_Shipment_Order__c(
            RecordTypeId = PORecordTypeId,SVMX_Order__c = orderRecord.Id
        );
        insert partOrderRecord2;
        partOrderRecord2 = new SVMXC__RMA_Shipment_Order__c(
            RecordTypeId = PORecordTypeId
        );
        insert partOrderRecord2;
    }
}