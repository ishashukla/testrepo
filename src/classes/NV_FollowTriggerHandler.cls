public with sharing class NV_FollowTriggerHandler {
    public static void processFollowBeforeInsert(Map<Id, NV_Follow__c> oldRecordMap, List<NV_Follow__c> newRecords){
        for(NV_Follow__c newRecord : newRecords){
            if(!String.isBlank(newRecord.Record_Id__c)){
                if(newRecord.Record_Id__c.length()<18){
                    newRecord.Record_Id__c = String.valueOf(Id.valueOf(newRecord.Record_Id__c));
                }
                if(String.isBlank(newRecord.External_Id__c)){
                    newRecord.External_Id__c = newRecord.Following_User__c + '_' + newRecord.Record_Id__c;
                }
                if(String.isBlank(newRecord.Record_Type__c)){
                    newRecord.Record_Type__c = Id.valueOf(newRecord.Record_Id__c).getSobjectType().getDescribe().getName();
                }
            }
        }
    }

    public static void processFollowAfterInsert(Map<Id, NV_Follow__c> oldRecordMap, List<NV_Follow__c> newRecords){

        Map<Id, Set<Id>> accountToUserIds = new Map<Id, Set<Id>>();
        Id recordId;
        for(NV_Follow__c newRecord : newRecords){
            recordId = Id.valueOf(newRecord.Record_Id__c);
            if(recordId.getSobjectType() == Schema.Account.SObjectType){
                if(accountToUserIds.containsKey(recordId))
                    accountToUserIds.get(recordId).add(newRecord.Following_User__c);
                else
                    accountToUserIds.put(recordId, new Set<Id>{newRecord.Following_User__c});
            }   
        }

        List<NV_Follow__c> followRecordsToCreate = new List<NV_Follow__c>();
        if(accountToUserIds.size()>0){
            for(Order orderRecord : [SELECT Id, AccountId FROM Order 
                                        WHERE AccountId in: accountToUserIds.keySet() 
                                            AND Status NOT IN ('Cancelled', 'Shipped', 'Closed', 'Delivered')]){
                if(accountToUserIds.containsKey(orderRecord.AccountId)){
                    for(Id userId : accountToUserIds.get(orderRecord.AccountId)){
                        followRecordsToCreate.add(new NV_Follow__c(Following_User__c = userId, 
                                                                    Record_Id__c = orderRecord.Id,
                                                                    External_Id__c = userId+'_'+orderRecord.Id));
                    }
                }
            }

            if(followRecordsToCreate.size()>0){
                upsert followRecordsToCreate External_Id__c;
            }
        }
    }
    //Uncommented by Jyoti for Story S-389745
    public static void processFollowAfterDelete(List<NV_Follow__c> oldRecords){

        Map<Id, Set<Id>> accountToUserIds = new Map<Id, Set<Id>>();
        Id recordId;
        for(NV_Follow__c oldRecord : oldRecords){
            recordId = Id.valueOf(oldRecord.Record_Id__c);
            if(recordId.getSobjectType() == Schema.Account.SObjectType){
                if(accountToUserIds.containsKey(recordId))
                    accountToUserIds.get(recordId).add(oldRecord.Following_User__c);
                else
                    accountToUserIds.put(recordId, new Set<Id>{oldRecord.Following_User__c});
            }   
        }

        Set<String> externalIds = new Set<String>();
        //removed status condition for Story S-389745
        for(Order orderRecord : [SELECT Id, AccountId FROM Order 
                                        WHERE AccountId in: accountToUserIds.keySet() 
                                            ]){
            if(accountToUserIds.containsKey(orderRecord.AccountId)){
                for(Id userId : accountToUserIds.get(orderRecord.AccountId)){
                    externalIds.add(userId+'_'+orderRecord.Id);
                }
            }
        }

        List<NV_Follow__c> followRecordsToDelete = [SELECT Id From NV_Follow__c Where External_Id__c in: externalIds];
        if(followRecordsToDelete.size()>0){
            delete followRecordsToDelete;
        }
    }
    
}