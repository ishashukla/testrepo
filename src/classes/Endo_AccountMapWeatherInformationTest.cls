/*******************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 10, 2014 
 Purpose       :  Test Class for Endo_AccountMapWeatherInformation
*******************************************************************************/
@isTest
private class Endo_AccountMapWeatherInformationTest {
	static testmethod void testEndo_AccountMapWeatherInformation(){
	    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
	    acc.ShippingCountryCode = 'US';
	    acc.ShippingState = 'California';
	    insert acc;
        
	    Test.startTest();
	    
	    ApexPages.StandardController stdController = new ApexPages.StandardController(acc);
	    Endo_AccountMapWeatherInformation comp = new Endo_AccountMapWeatherInformation(stdController);
	    
	    System.assertEquals(acc.ShippingState, comp.myaddress.state);
	    System.assertEquals(acc.ShippingCountryCode, comp.myaddress.countryCode);
	    System.assertEquals(acc.ShippingCountry, comp.myaddress.country);
	    System.assertEquals(acc.ShippingCity, comp.myaddress.city);
	    
	    comp.accountFields();
	    
	    Test.stopTest();
	}
}