// 
// (c) 2016 Appirio, Inc.
//
// T-500180, This is a test class for Intr_UpdateCaseStatusBatch class
//
// 12 May, 2016  Shreerath Nair Original 

/*
 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
*/

@isTest
private class Intr_UpdateCaseStatusBatch_SchedulerTest {

        public static List<Case> caseList  = new List<Case>();	
        
        
	    public static testMethod void testschedule() {
	        Profile pro = [SELECT Id,Name FROM Profile WHERE Name='Instruments CCKM']; 
            List<User> us = Intr_TestUtils.createUser(1,pro.Name,true);
            
            System.runAs(us[0]) {
                createTestdata();
                Test.StartTest();
            
                    //Create object of scheduler class
                    Intr_UpdateCaseStatusBatch_Scheduler sh1 = new Intr_UpdateCaseStatusBatch_Scheduler();
                    String sch = '0 0 23 * * ?';
            
                    //schedule the scheduler class
                    system.schedule('Test Territory Check', sch, sh1);
                    
                  
                Test.stopTest();
            }
        }
        
        public static void createTestData(){
      
            Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
            Account acc = Intr_TestUtils.createAccount('test',RecordTypeId,true);
            List<Contact> lstContact = Intr_TestUtils.createContact(1,acc.id,true);
            caseList = Intr_TestUtils.createCase(1,acc.id,lstContact[0].id,false);
            
            //Modified By Chandra Shekhar Sharma to add record types on case Object
	        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Instruments - Employee Complaint');
	        TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Instruments - Employee Complaint', 'Instruments');
        	caseList[0].recordTypeId = rtByName.getRecordTypeId();
	        insert caseList;
            insert new Instruments_CCKM_Common_Setting__c(Auto_Case_Closure_Delay_Days__c = -2, Enable_Auto_Case_Closure__c = true);

        }

    	

}