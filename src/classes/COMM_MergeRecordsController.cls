/**================================================================      
* Appirio, Inc
* Name: COMM_MergeRecordsController
* Description: To merge the project plan reocrds (Original    Appirio Asset) - T-489645
* Created By :  Kirti Agarwal
* Created Date : 27th April 2016
*
* Date Modified         Modified By      Description of the update
* 11th August 2016      Nitish Bansal    I-228981 - To allow search by project number also.
* 19th October 2016     Nitish Bansal    Update (I-239981) // To add project forecasting values on project.
==================================================================*/
public without sharing class COMM_MergeRecordsController{
 
    //this guides the number of merged records
    public Integer getMaxRecordsCount(){return COMM_MergeRecordsUtil.MERGED_RECORDS_COUNT;}
    
    //choosen sobject 
    public String sObjectType{get;Set;}
    //sobject has changed
    public String sObjectTypeOld = null;
    public List<SObject> objList;
    //search text
    public String searchText{get;Set;}
    
    public Boolean isAllowedToMerge{get;Set;}
    
    //results of the search SOSL query
    public List<SObject> foundRecordList{get;set;}
   //totalsize of search list  
   public Integer total_size = 0;
   // no of records on one page of table
   public Integer list_size= 5 ;
   //getting the counter offset value of the page selected in the table 
   public integer counter = 0;
  // field selected to show in the table
   public String fieldSelected{get { return fieldSelected; }
      set { fieldSelected = value; }}  
   public Set<Decimal> amountSet = new Set<Decimal>();
   // selected page in the table
   public string selectedPage{
   get;
   set{selectedPage=value;}  //set from the parameter in actionFunction
   }
    
    //SObject list
    public List<SelectOption> sobjectsSlctOpt{get;Set;}
    
    public String soslFields;
    
    //merging objects
    public List<SObject> merginObjects{get;set;}
    public List<String> mergingIds{get;set;}
    
    /**
        list of selected fields of all object
        stores:
            {OBJ-ID1, 
                    {FIELD1, TRUE}
                    {FIELD2, TRUE}
                    ...
                    {FIELDN, FALSE}
            OBJ-ID2, 
                    {FIELD1, FALSE}
                    {FIELD2, FALSE}
                    ...
                    {FIELDN, TRUE}
            }
    */  
    public Map<ID,Map<String,COMM_MergeRecordsUtil.InputHiddenValue>>selectedObjFields {get;set;}
    
    //Describe information for the selected sobject
    public COMM_MergeRecordsUtil.DescribeResult describe{get;set;}
   
    //Move an object form the search list to the "merge table"
    public String selectMergetObjectId{get;set;}
    public Integer selectMergetObjectPosition{get;set;}
    //selected master object
    public String masterObjectId{get;set;}
    public boolean isSearch;
    
    public COMM_MergeRecordsController() 
    { this.objList = new List<sObject>();
        this.describe = new COMM_MergeRecordsUtil.DescribeResult();   
        this.foundRecordList = new List<Sobject>();
        this.sobjectsSlctOpt = new List<SelectOption>();
        //this.uniqueFields = new Map<String,String>();
        this.merginObjects = new List<SObject>();
        //this.allFields = new Map<String,COMM_MergeRecordsUtil.Field>();
        this.selectedObjFields = new  Map<ID,Map<String,COMM_MergeRecordsUtil.InputHiddenValue>>();
        
        //set ot store an ordered list of sobjects (by name)
        Set<String> objsOrderName = new Set<String>();
        
        //search for the "searchable" sobjects and stores the describes 
        //T-489645 - Added by Kirti
        Map<String,String> sobjNamesLabels = new Map<String,String>();
        sobjNamesLabels.put(COMM_MergeRecordsUtil.PROJECT_PLAN, COMM_MergeRecordsUtil.PROJECT_PLAN_LABEL);
        objsOrderName.add(COMM_MergeRecordsUtil.PROJECT_PLAN);
        //Commented : T-489645
        /*for(Schema.Sobjecttype sot : Schema.getGlobalDescribe().values())
        {
            Schema.Describesobjectresult dr = sot.getDescribe();
            if(dr.isSearchable() && dr.isAccessible() && dr.isCustomSetting()==false  && dr.isDeprecatedAndHidden()==false
                && dr.isQueryable() && dr.isUpdateable() && dr.isDeletable())
            {
                sobjNamesLabels.put(dr.getName(), dr.getlabel());
                objsOrderName.add(dr.getName());
            }
        }*/
        
        System.assert(objsOrderName.size()> 0, 'No object found.');
        
        //creates a list to make it order its elements
        
        List<String> nameList = new List<String>(objsOrderName);
        nameList.sort();
        // List of Merge object Ids
        mergingIds = new List<String>{'0','0','0'};        
        for(String name : nameList)
        {
            this.sobjectsSlctOpt.add(new SelectOption(name,sobjNamesLabels.get(name)));
        }
        //T-489645 - Added for "Project Plan Merge" button
        selectedPage = '0' ;
        isAllowedToMerge = false;
        currentUserAllowedToMergeRecord();
        selectMergetObjectPosition = 0;
        sObjectType = 'Project_Plan__c';
        String sobjectIdVal = ApexPages.currentPage().getParameters().get('sObjectId');
        if(sobjectIdVal  != '' && sobjectIdVal != null) {
          for(Project_Plan__c rec : [SELECT id, Project_Plan_Number__c FROM Project_Plan__c WHERE Id =:sobjectIdVal]) {
            selectMergetObjectId = rec.id;
            masterObjectId = rec.id;
            searchText =  String.valueOf(rec.Project_Plan_Number__c);
            isSearch = false;
            searchRecords();
            selectMergetObject();
            isSearch = true;
            this.foundRecordList = new List<Sobject>();
            searchText = '';
          }
        }
        isSearch = false; //NB - 08/11- I-228981
    }
    
    private void currentUserAllowedToMergeRecord() {
      String commSystemAdmin = Constants.COMM + Constants.ADMIN_PROFILE;
      for(Profile profileRec : [SELECT Id 
                                  FROM Profile 
                                 WHERE Name = :Constants.ADMIN_PROFILE 
                                    OR Name = :commSystemAdmin ]) {
        if(UserInfo.getProfileId() == profileRec.Id) {
          isAllowedToMerge = true;
        }
      }
      
    }
    
    public Boolean getFieldRendering() {
    
    
     if( foundRecordList.size() > 0)
     { //system.debug('Hello ji 1');
     return true;
     } 
        return false;
    }

    public list<SelectOption> getFieldOptions(){
        List<SelectOption> options = new List<SelectOption>();
      
      for(String des :describe.allFieldsKeySet)
      {
        if(! des.equalsIgnoreCase('name'))
          options.add(new SelectOption(des,des));

      }
        
            return options;
    }
  
        
    public void initDescribe()
    {   
        this.masterObjectId = null;
        this.foundRecordList = new List<Sobject>();
        this.selectedObjFields = new  Map<ID,Map<String,COMM_MergeRecordsUtil.InputHiddenValue>>();
        this.merginObjects = new List<Sobject>();
        this.objlist = new List<sObject>();    
        this.describe = COMM_MergeRecordsUtil.initDescribes(this.sObjectType);
        
        //fills with blank objects
        for(Integer i = this.merginObjects.size(); i < getMaxRecordsCount(); i++)
        {
            this.merginObjects.add(this.describe.sOType.newsobject());
        }
                    
        System.assert(this.merginObjects.size()== getMaxRecordsCount() ,'Initialization of mergeing object incorrect. Must be exactly '+getMaxRecordsCount()+' void objects');
    }
    
    /**
        Record search
    **/
    public void searchRecords()
    {        selectedPage = '0';

        if(this.searchText==null || this.searchText.trim().length()==0)
        { 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Search non void text.'));
            return;
        }
        
        System.assert(sObjectType!=null && sObjectType.length()>0,'Object Type should not be blank.');
        
        //if the sobject changes, need for refresh
        if(sObjectTypeOld != sObjectType)
        {
            sObjectTypeOld = this.sObjectType;
            initDescribe();
        }
        
        soslFields = '';
        if(this.describe.allFields.size()>0)
        {
            for(String s : this.describe.allFields.keySet()) soslFields+=s+',';
        }
        soslFields = soslFields.substring(0,soslFields.length()-1);
        selectedPage='0';
        String searchquery;
        if(!isSearch) {
         searchquery='FIND \''+this.searchText.trim()+'\' IN ALL FIELDS RETURNING '+sObjectType+' ('+soslFields+' where Id not in :mergingIds ORDER BY '+this.describe.nameField+' limit 200)';        //system.debug('Search query: '+searchquery);
        }else {
          searchquery='FIND \''+this.searchText.trim()+'\' IN Name FIELDS RETURNING '+sObjectType+' ('+soslFields+' where Id not in :mergingIds ORDER BY '+this.describe.nameField+' limit 200)';        //system.debug('Search query: '+searchquery);
        }
        
        try
        {  
             List<List<SObject>> result = search.query(searchquery);
            objList =new List<SObject>(); 
            for(List<SObject> lst : result) 
            objList = lst;
            
            if(objList.size()==0) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'No record found.'));
            else if(isSearch) {
              selectedPage = '1';
            }
            
        }
        catch(Exception e)
        {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        if(isSearch)
          total_size = objList.size();
        setTableList(0);
      }
    
    public void setTableList(Integer i) {
      //system.debug(objList.size() + '====setTableList==' + i);
      
      if (objList.size() > i * list_size && objList.size() < (i + 1) * list_size) {
        //system.debug('First Case');
        Integer j;
        foundRecordList = new List < sObject > ();
        for (j = i * list_size; j < objList.size(); j++)
          foundRecordList.add(objlist.get(j));
      }

      if (objList.size() >= (i + 1) * list_size) {
        //system.debug('Second Case');
        Integer j;
        foundRecordList = new List < sObject > ();
        for (j = i * list_size; j < (i + 1) * list_size; j++)
          foundRecordList.add(objList.get(j));

      }

      if (objlist.size() <= i * list_size) {
        //system.debug('Third Case');
        //system.debug('oo size is ' + objList.size() + 'i is' + i);
        foundRecordList = new List < sObject > ();
      }

    }
    
    //Moves an object from the search list to the marging list
    public void selectMergetObject()
    {
        if(selectMergetObjectId == null || selectMergetObjectPosition == null 
            || selectMergetObjectPosition<0 || selectMergetObjectPosition >= getMaxRecordsCount())
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No object selected or error in position.'));
            return;
        }
        //system.debug('select Merget Object Id'+selectMergetObjectId +'selectMergetObjectPosition'+selectMergetObjectPosition);
        for(SObject obj : this.foundRecordList)
        {
            if((String)obj.get('ID') == this.selectMergetObjectId)
            {
                String prevObjId = (String)this.merginObjects[this.selectMergetObjectPosition].get('Id');
                
                //if current position object is the master object, it is "deselected"
                if(prevObjId == this.masterObjectId) this.masterObjectId = null;
                
                //remove previous "field selection" informations
                if(prevObjId != null) this.selectedObjFields.remove(prevObjId);
                
                //previous object replaced by current selected object
                this.merginObjects[this.selectMergetObjectPosition] = obj;
                //mergingIds.add(obj.Id); 
                mergingIds[this.selectMergetObjectPosition] = obj.Id;
                //init the map of "selected" fields
                Map<String, COMM_MergeRecordsUtil.InputHiddenValue> flist =  new Map<String,COMM_MergeRecordsUtil.InputHiddenValue>();
                selectedObjFields.put((ID)selectMergetObjectId, flist);
                for( COMM_MergeRecordsUtil.Field f : this.describe.allFields.values()) flist.put(f.name,new COMM_MergeRecordsUtil.InputHiddenValue(f.name,false,f.isWritable));
                break;
            }
        }
        
        if(isSearch) 
        searchRecords();
    }
    
    //merge records into one
    public PageReference mergeRecords()
    {
        if(this.masterObjectId == null || this.masterObjectId.trim().length()==0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Select a Survivor record.'));
            return null;
        }
 
        COMM_MergeRecordsUtil.SelectMasterResult smr = COMM_MergeRecordsUtil.mergeRecords(this.merginObjects, this.masterObjectId);
        if(smr.masterObject == null || smr.loserObjs.size()==0)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You need more than one record to merge.'));
            return null;
        }
        
        for(Sobject objRec : smr.loserObjs)
        {   
         String planId = String.valueOf(objRec.get('Id'));
         for(Project_Plan__c p : [SELECT Id, Boom_Amount__c,Lighting_Systems_Amount__c,Oris_Amount__c,
                                  Pro_Care_Amount__c,Integration_Services_Amount__c,Orreno_Amount__c,
                                  Tables_Amount__c,Other_Amount__c,Stage__c FROM Project_Plan__c WHERE Id =: planId ]) {
             if(p.Stage__c != Constants.INITIATION) {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Merged record should be in initiation stage'));
               return null;
              } 
            } 
            
        }  
        
        //copy looser fields into master
        COMM_MergeRecordsUtil.copyDataIntoMasterRecord(smr,this.selectedObjFields,this.sObjectType);
        
        Savepoint sp = Database.setSavepoint();
        try
        {
            //NB - 10/19 - I-239981 - Start
            if(getObjectApiName(smr.masterObject.Id).Contains('Project_Plan__c')){
                String oldId = smr.loserObjs[0].Id;
                String newId = smr.masterObject.Id;
                Project_Plan__c oldPP = new Project_Plan__c();
                Project_Plan__c newPP = new Project_Plan__c();
                for(Project_Plan__c pp : [Select Id, Boom_Amount__c,Integration_Services_Amount__c,Lighting_Systems_Amount__c,
                                                    Oris_Amount__c, Orreno_Amount__c,Other_Amount__c,Pro_Care_Amount__c,Tables_Amount__c
                                                    from Project_Plan__c where Id= :oldId or Id= :newId]){
                    if(pp.Id == oldId){
                       oldPP = pp; 
                    } else {
                        newPP = pp;
                    }
                }
                //NB - 10/21 - I-240940 - Start - Bad Data Null Pointer Exception handling
                newPP.Boom_Amount__c = (newPP.Boom_Amount__c != null ? newPP.Boom_Amount__c : 0) + (oldPP.Boom_Amount__c != null ? oldPP.Boom_Amount__c : 0);
                newPP.Integration_Services_Amount__c = (newPP.Integration_Services_Amount__c != null ? newPP.Integration_Services_Amount__c : 0) + (oldPP.Integration_Services_Amount__c != null ? oldPP.Integration_Services_Amount__c : 0);
                newPP.Lighting_Systems_Amount__c = (newPP.Lighting_Systems_Amount__c != null ? newPP.Lighting_Systems_Amount__c : 0) + (oldPP.Lighting_Systems_Amount__c != null ? oldPP.Lighting_Systems_Amount__c : 0);
                newPP.Oris_Amount__c = (newPP.Oris_Amount__c != null ? newPP.Oris_Amount__c : 0) + (oldPP.Oris_Amount__c != null ? oldPP.Oris_Amount__c : 0);
                newPP.Orreno_Amount__c = (newPP.Orreno_Amount__c != null ? newPP.Orreno_Amount__c : 0) + (oldPP.Orreno_Amount__c != null ? oldPP.Orreno_Amount__c : 0);
                newPP.Other_Amount__c = (newPP.Other_Amount__c != null ? newPP.Other_Amount__c : 0) + (oldPP.Other_Amount__c != null ? oldPP.Other_Amount__c : 0);
                newPP.Pro_Care_Amount__c = (newPP.Pro_Care_Amount__c != null ? newPP.Pro_Care_Amount__c : 0) + (oldPP.Pro_Care_Amount__c != null ? oldPP.Pro_Care_Amount__c : 0);
                newPP.Tables_Amount__c = (newPP.Tables_Amount__c != null ? newPP.Tables_Amount__c : 0) + (oldPP.Tables_Amount__c != null ? oldPP.Tables_Amount__c : 0);
                //NB - 10/21 - I-240940 - End

                delete oldPP;
                update newPP;

                PageReference page = new PageReference('/'+newPP.Id);
                page.setRedirect(true);
                return page;
            }
            //NB - 10/19 - I-239981 - End
            delete smr.loserObjs;
            update smr.masterObject;
            
            PageReference page = new PageReference('/'+smr.masterObject.Id);
            page.setRedirect(true);
            return page;
        }
        catch(DMLException e)
        {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDMLMessage(0)));
        }
        catch(Exception e)
        {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred: '+e.getMessage()));
        }
       initDescribe();
        return null;
    }
    
    //NB - 10/19 - I-239981 - Start
    private String getObjectApiName(String objId){
        String keyCode  = objId.subString(0,3);
        String objName;
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType objectInstance : gd.values())
        {
            if(objectInstance.getDescribe().getKeyPrefix() == keyCode)
            {//do your processing with the API name what you want
                objName = objectInstance.getDescribe().getName();
            }
        }
        return objName;
    } 
    //NB - 10/19 - I-239981 - Start   
   
     public Component.Apex.pageBlockButtons getMyCommandButtons() {
        
        //the reRender attribute is a set NOT a string
        Set<string> theSet = new Set<string>();
        theSet.add('myPanel');
        theSet.add('myButtons');
        theSet.add('srcResultPanel');
        integer totalPages;
        if (math.mod(total_size, list_size) > 0) {
            totalPages = total_size/list_size + 1;
        } else {
            totalPages = (total_size/list_size);
        }
        
        //system.debug('Total pages are'+ totalPages +'Total size is '+total_size + 'list size is' + list_size);
        integer currentPage;        
        if (selectedPage == '0') {
            currentPage = counter/list_size + 1;
        } else {
            currentPage = integer.valueOf(selectedPage);
        }
     
        Component.Apex.pageBlockButtons pbButtons = new Component.Apex.pageBlockButtons();        
        pbButtons.location = 'top';
        pbButtons.id = 'myPBButtons';       
        Component.Apex.outputPanel opPanel = new Component.Apex.outputPanel();
        opPanel.id = 'myButtons';
                                
        //the Previous button will alway be displayed
        Component.Apex.commandButton b1 = new Component.Apex.commandButton();
        b1.expressions.action = '{!Previous}';
        b1.title = 'Previous';
        b1.value = 'Previous';
        b1.expressions.disabled = '{!disablePrevious}';        
        b1.reRender = theSet;
        opPanel.childComponents.add(b1);        
        
        //the Next button will alway be displayed
        Component.Apex.commandButton b2 = new Component.Apex.commandButton();
        b2.expressions.action = '{!Next}';
        b2.title = 'Next';
        b2.value = 'Next';
        b2.expressions.disabled = '{!disableNext}';        
        b2.reRender = theSet;
        opPanel.childComponents.add(b2);
                
        //add all buttons as children of the outputPanel                
        pbButtons.childComponents.add(opPanel);  
  
        return pbButtons;

    }    
    
    public PageReference refreshGrid() { //user clicked a page number        
        setTableList(Integer.valueOf(selectedPage)-1);
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
        Integer pageVal = Integer.valueOf(selectedPage) - 1;
        selectedPage = String.valueOf(pageVal);
        refreshGrid();
        return null;
    }

    public PageReference Next() { //user clicked next button
        Integer pageVal = Integer.valueOf(selectedPage) + 1;
        selectedPage = String.valueOf(pageVal);
        refreshGrid();
        return null;
    }

    public PageReference End() { //user clicked end
        selectedPage = '0';
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }
    
    public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
        if(Integer.valueOf(selectedPage) > 1) {
          return false;
        } else {
          return true;
        }
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
        if( getTotalPages() > 1  && Integer.valueOf(selectedPage) < getTotalPages()) {
          return false;
        } else {
          return true;
        }
    }

    public Integer getTotal_size() {
     return total_size;
    }
    
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
       
       }
    }
    public PageReference find(){
    List<String> fieldList;
       describe.uniqueFields= new Map<String,String>();
        fieldList  = fieldSelected.split(',');
       //system.debug('all fields are '+ describe.allFieldsKeySet);
       
   for(String field :  fieldList)
      { 
       //system.debug('s is'+field);
       field=field.trim();
       field = field.removeStart('[');
       field = field.removeEnd(']');
       describe.uniqueFields.put(field,describe.allFields.get(field).label);
       }
        return null;}
    
}