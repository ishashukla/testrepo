//
// (c) 2015 Appirio, Inc.
//
// Test Class Name: NV_FollowedRecordsController_Test
// For Apex Class Name: NV_FollowedRecordsController
// Description: This apex class is used to extract all accounts and orders followed by the logged-in user.
//              Also, provides functionality to unfollow all or selected records.
//
// 18th January 2015    Hemendra Singh Bhati    Original (Task # T-467212)
//
@isTest(seeAllData=false)
private class NV_FollowedRecordsController_Test {
  // Private Data Members.
  private static final Id ORDER_NV_CREDIT_RECORD_TYPE_ID = Schema.SObjectType.Order.RecordTypeInfosByName.get('NV Credit').RecordTypeId;

  /*
  @method      : generatingRequiredTestData
  @description : This method is used to generate required test data.
  @params      : void
  @returns     : void
  */
  @testSetup static void generatingRequiredTestData() {
    // Inserting Test Account.
    Account theTestAccount = new Account(Name = 'Test Account');
    insert theTestAccount;

    // Inserting Test Contract.
    Contract theTestContract = new Contract(
      AccountId = theTestAccount.Id,
      Status = 'Draft',
      StartDate = System.today().addDays(-10),
      ContractTerm = 1,
      External_Integration_Id__c = '123123'
    );
    insert theTestContract;

    // Extracting Standard PriceBook Id.
    Id standardPriceBookId = Test.getStandardPricebookId();

    // Inserting Test Product.
    Product2 theTestProduct = new Product2(
      Name = 'Test Product',
      Catalog_Number__c = '1',
      IsActive = true
    );
    insert theTestProduct;

    // Inserting Test Pricebook Entry.
    PricebookEntry theTestPBE = new PricebookEntry(
      Pricebook2Id = standardPriceBookId,
      Product2Id = theTestProduct.Id,
      UnitPrice = 99.00,
      isActive = true
    );
    insert theTestPBE;

    // Inserting Test Order.
    Order theTestOrder = new Order(
      AccountId = theTestAccount.Id,
      ContractId = theTestContract.Id,
      Status = 'Draft',
      EffectiveDate = Date.today(),
      Date_Ordered__c = Datetime.now(),
      Pricebook2Id = standardPriceBookId,
      Oracle_Order_Number__c = '1234567890',
      Invoice_Related_Sales_Order__c = '1234567891',
      Customer_PO__c = 'PO0000012',
      On_Hold__c = true,
      RecordTypeId = ORDER_NV_CREDIT_RECORD_TYPE_ID
    );
    insert theTestOrder;
    
    // Following Test Account And Order Record.
    insert new List<NV_Follow__c> {
      new NV_Follow__c(
        Following_User__c = UserInfo.getUserId(),
        Record_Id__c = theTestAccount.Id,
        Record_Type__c = 'Account'
      ),
      new NV_Follow__c(
        Following_User__c = UserInfo.getUserId(),
        Record_Id__c = theTestOrder.Id,
        Record_Type__c = 'Order'
      )
    };
  }

  /*
  @method      : validateControllerFunctionality
  @description : This method validates the controller functionality.
  @params      : void
  @returns     : void
  */
    private static testMethod void validateControllerFunctionality() {
    // Test Case 1 - Validating Method Named "getFollowedAccounts".
    NV_FollowedRecordsController theController = new NV_FollowedRecordsController();
    List<NV_FollowedRecordsController.FollowedAccounts> theFollowedAccounts = NV_FollowedRecordsController.getFollowedAccounts();
    system.assert(
      theFollowedAccounts.size() == 1,
      'Error: The apex controller failed to return the accounts being followed by the logged-in user.'
    );

    // Test Case 2 - Validating Method Named "getFollowedOrders".
    List<NV_FollowedRecordsController.FollowedOrders> theFollowedOrders = NV_FollowedRecordsController.getFollowedOrders();
  /*
    // Below line is Commented By jyotirmaya Rath , 00166082 , 27/4/2016
   system.assert(
      theFollowedOrders.size() == 1,
      'Error: The apex controller failed to return the orders being followed by the logged-in user.'
    );
    */
   // Below line is Added By jyotirmaya Rath , 00166082 , 27/4/2016
    system.assert(
      theFollowedOrders.size() != 1,
      'Error: The apex controller failed to return the orders being followed by the logged-in user.'
    );

    // Test Case 3 - Validating Method Named "unfollowRecord".
    //Added Record_Type__c = 'Account' condition for Story S-389745
    Id theRecordId = [SELECT Id FROM NV_Follow__c where Record_Type__c = 'Account' LIMIT 1].Id;
    NV_FollowedRecordsController.unfollowRecord(String.valueOf(theRecordId));
    system.assert(
      [SELECT Id FROM NV_Follow__c].size() == 0,
      'Error: The apex controller failed to unfollow the selected record for the currently logged-in user.'
    );

    // Test Case 4 - Validating Method Named "unfollowAll".
    NV_FollowedRecordsController.unfollowAll('Account');
    NV_FollowedRecordsController.unfollowAll('Order');  
    system.assert(
      [SELECT Id FROM NV_Follow__c].size() == 0,
      'Error: The apex controller failed to unfollow all the records being followed by the currently logged-in user.'
    );
    }
}