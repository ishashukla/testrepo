@isTest(SeeAllData='true')
public class COMMReportEXTTest {
  static testMethod void runReport() {
 //   Id ReportId = [Select id from Report where DeveloperName = 'Installed_Products_Ready_to_Install'].Id;
    List<Account> accountList = TestUtils.createAccount(1,True);
    List<Project_Plan__c> projects =  TestUtils.createProjectPlan(1, accountList[0].Id, false);
    insert projects;
    List<Project_Phase__c> lstPhases1 = TestUtils.createProjectPhase(1,projects[0].Id,true);
    COMMReportEXT.runReportFunction('Installed_Products_Ready_to_Install',projects[0].id,'');
    COMMReportEXT.runReportFunction('Installed_Products_Ready_to_Install','',lstPhases1[0].id);
  }
}