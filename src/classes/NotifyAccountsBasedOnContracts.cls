// (c) 2015 Appirio, Inc.
//
// Class Name: NotifyAccountsBasedOnContracts
// Description: Batch Class for posting chatter on Account if Contract is reaching its end date
//
// February 25 2016, Prakarsh Jain  Original (T-479824)
//
global class NotifyAccountsBasedOnContracts implements Database.Batchable<sObject>, Database.Stateful{
  global Map<Id,Contract> idToContractMap;
  //global Date todaysDate = Date.newInstance(2016,04,06);
  //Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Instruments');
  //Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
  RecordType recType = [Select Id From RecordType  Where SobjectType = 'Contract' and DeveloperName = 'Instruments'];
  Id recordTypeInstrument = recType.Id;
  global Date todaysDate = Date.Today();
  global List<ConnectApi.BatchInput> accountPost = new List<ConnectApi.BatchInput>();
  global List<Contract> lstContracts;
  global List<Contract> localRepContracts;
  global List<Contract> serviceContractRepContracts;
  global List<ConnectApi.BatchInput> chatterPostList = new List<ConnectApi.BatchInput>();
  global Database.QueryLocator start(Database.BatchableContext BC){
    String query = '';
    query = 'SELECT Id, EndDate,Contract_Type__c, AccountId, Reminder_Date__c, OwnerId FROM Contract WHERE Reminder_Date__c =: todaysDate AND RecordTypeId =:recordTypeInstrument';
    system.debug('query>>>'+query);
    return Database.getQueryLocator(query);
  }
  
  global void execute(Database.BatchableContext BC,List<Sobject> scope){
    String salutation = Label.Contract_Chatter_Salutation;
    String localRepBeforeMention = Label.Local_Rep_Before_Mentioned;
    String localRepAfterMention = Label.Local_Rep_After_Mentioned;
    String serviceContractBeforeMention = Label.Service_Contract_Before_Mention;
    String serviceContractAfterMention = Label.Local_Rep_After_Mentioned;
    String contractBeforeMention = Label.Contract_Chatter_Before_Mention;
    String contractAfterMention = Label.Contract_Chatter_After_Mention;
    idToContractMap = new Map<Id,Contract>();
    lstContracts = new List<Contract>();
    localRepContracts = new List<Contract>();
    serviceContractRepContracts = new List<Contract>();
    String integratedUserName = Label.Integration_User;
    User integratedUser;
    List<User> userList = [SELECT Id,Name FROM User WHERE Name = :integratedUserName];
    integratedUser = userList.get(0);
    System.debug('>>>>>integrated user'+integratedUser);
    for(sObject s: scope){
      Contract contract = (Contract)s;
      if(contract.Contract_Type__c!=null && contract.Contract_Type__c == 'Local Rep Contract' && contract.ownerId == integratedUser.Id){
        localRepContracts.add(contract);
      }
      else if(contract.Contract_Type__c!=null && contract.Contract_Type__c == 'Service Contract' && contract.ownerId == integratedUser.Id){
        serviceContractRepContracts.add(contract);
      }
      else if(contract.ownerId != integratedUser.Id){
        lstContracts.add(contract); 
      }
      if(!idToContractMap.containsKey(contract.Id)){
        idToContractMap.put(contract.Id, contract);
      }
    }
    system.debug('idToContractMap???'+idToContractMap);
    if(lstContracts.size()>0){
      for(Contract con : lstContracts){
        ConnectApi.BatchInput accPost = new ConnectApi.BatchInput(returnInputFeed(con,integratedUser,salutation,contractBeforeMention,contractAfterMention));
        accountPost.add(accPost);
      } 
    }
   if(localRepContracts.size()>0){
      for(Contract con : localRepContracts){
        ConnectApi.BatchInput accPost = new ConnectApi.BatchInput(returnInputFeed(con,integratedUser,'',localRepBeforeMention,localRepAfterMention));
        accountPost.add(accPost);
      } 
    }
   if(serviceContractRepContracts.size()>0){
      for(Contract con : serviceContractRepContracts){
        ConnectApi.BatchInput accPost = new ConnectApi.BatchInput(returnInputFeed(con,integratedUser,'',serviceContractBeforeMention,serviceContractAfterMention));
        accountPost.add(accPost);
      } 
    }
    system.debug('accountPost+++'+accountPost);
    if(accountPost.size()>0){
      chatterPostList.addAll(accountPost);
    }
    if(chatterPostList.size()>0){
      ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), chatterPostList);
    }
  }
  
  global void finish(Database.BatchableContext BC){
    List<Days_Left_For_Reminder__c> daysLeft = Days_Left_For_Reminder__c.getAll().values();
    List<Contract> contractList = new List<Contract>();
    system.debug('idToContractMap>>>'+idToContractMap);
    for(String idContract : idToContractMap.keySet()){
      contractList.add(idToContractMap.get(idContract));
    }
    for(Contract con : contractList){
      con.Reminder_Date__c = con.Reminder_Date__c.addDays(Integer.valueOf(daysLeft.get(0).Next_Notification_Days__c));
    }
    update contractList;
  }
  
  public static ConnectApi.FeedItemInput returnInputFeed(Contract con,User integratedUser, String salutationMessage,String beforeMention,String afterMention){
    String communityId = null;
    ConnectApi.FeedType feedType = ConnectApi.FeedType.UserProfile;
    ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
    ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
    ConnectApi.TextSegmentInput textSegment;
    ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
    ConnectApi.LinkSegmentInput linksegment = new ConnectApi.LinkSegmentInput();
    messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
    String baseUrl = String.valueOf(URL.getSalesforceBaseUrl().toExternalForm());
    if(con.ownerId != integratedUser.Id){
      textSegment = new ConnectApi.TextSegmentInput();
      textSegment.text = salutationMessage; //'Hey ';
      messageInput.messageSegments.add(textSegment);
      mentionSegment.id = con.OwnerId;
      messageInput.messageSegments.add(mentionSegment);
    }
    textSegment = new ConnectApi.TextSegmentInput();
  //  textSegment.text = ' one of your Contracts is '+con.Reminder_Date__c.daysBetween(con.EndDate)+' days out from expiring.  Here is a link to the Contract: '+baseUrl+'/'+con.Id;
    textSegment.text = beforeMention+' '+con.Reminder_Date__c.daysBetween(con.EndDate)+' '+afterMention+' '+baseUrl+'/'+con.Id;
    messageInput.messageSegments.add(textSegment);
    input.body = messageInput;
    input.subjectId = con.AccountId;
    input.feedElementType = ConnectApi.FeedElementType.FeedItem;
    system.debug('input>>>'+input);
    return input;
  }
}