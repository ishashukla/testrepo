// 
// (c) 2016 Appirio, Inc.
//
// ObjectHierarchy_Test: Consolidated Test class for Object Hierarchy Classes
//							1. ObjectHierarchy_ComponentController
//							2. ObjectHierarchy_Utility
//							3. ObjectHierarchy_TestUtility
//
// 18 Apr 2016     Durgesh Dhoot       Original
//
@isTest
private class ObjectHierarchy_Test {
	
	static testMethod void testGetObjectHierarchyDataBasics() {

		List<Account> hierarchyAccountData  = ObjectHierarchy_TestUtility.getSampleHierarchyAccounts(3);
		Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData(hierarchyAccountData[0].Id,
																		'Name', 
																		'ParentId', 
																		'AccountNumber,NumberOfEmployees', 
																		'full');
		//Validate response map size
		System.assertEquals(2, hierarchyWrapper.size());
		//Validate map contains data
		System.assertEquals(true, hierarchyWrapper.containsKey('data'));
		List<ObjectHierarchy_Utility.ObjectWrapper> data = (List<ObjectHierarchy_Utility.ObjectWrapper>) hierarchyWrapper.get('data');
		//Validate data size
		System.assertEquals(13, data.size());

		//Validate map contains columns
		System.assertEquals(true, hierarchyWrapper.containsKey('columns'));
		List<ObjectHierarchy_Utility.ColumnWrapper> columns = (List<ObjectHierarchy_Utility.ColumnWrapper>) hierarchyWrapper.get('columns');
		//Validate data size
		System.assertEquals(3, columns.size());
	}

	static testMethod void testGetObjectHierarchyDataForInnerNodeWithFull() {

		List<Account> hierarchyAccountData  = ObjectHierarchy_TestUtility.getSampleHierarchyAccounts(3);
		Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData
																		(hierarchyAccountData[4].Id,
																		'Name', 
																		'ParentId', 
																		'AccountNumber,NumberOfEmployees', 
																		'full');
		//Validate response map size
		System.assertEquals(2, hierarchyWrapper.size());
		//Validate map contains data
		System.assertEquals(true, hierarchyWrapper.containsKey('data'));
		List<ObjectHierarchy_Utility.ObjectWrapper> data = (List<ObjectHierarchy_Utility.ObjectWrapper>) hierarchyWrapper.get('data');
		//Validate data size
		System.assertEquals(13, data.size());

		//Validate map contains columns
		System.assertEquals(true, hierarchyWrapper.containsKey('columns'));
		List<ObjectHierarchy_Utility.ColumnWrapper> columns = (List<ObjectHierarchy_Utility.ColumnWrapper>) hierarchyWrapper.get('columns');
		//Validate data size
		System.assertEquals(3, columns.size());
	}

	static testMethod void testGetObjectHierarchyDataForInnerNodeWithCurrent() {

		List<Account> hierarchyAccountData  = ObjectHierarchy_TestUtility.getSampleHierarchyAccounts(3);
		Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData(hierarchyAccountData[1].Id,
																		'Name', 
																		'ParentId', 
																		'AccountNumber,NumberOfEmployees', 
																		'current');
		//Validate response map size
		System.assertEquals(2, hierarchyWrapper.size());
		//Validate map contains data
		System.assertEquals(true, hierarchyWrapper.containsKey('data'));
		List<ObjectHierarchy_Utility.ObjectWrapper> data = (List<ObjectHierarchy_Utility.ObjectWrapper>) hierarchyWrapper.get('data');
		//Validate data size
		System.assertEquals(4, data.size());

		//Validate map contains columns
		System.assertEquals(true, hierarchyWrapper.containsKey('columns'));
		List<ObjectHierarchy_Utility.ColumnWrapper> columns = (List<ObjectHierarchy_Utility.ColumnWrapper>) hierarchyWrapper.get('columns');
		//Validate data size
		System.assertEquals(3, columns.size());
	}

	static testMethod void testGetObjectHierarchyDataWithRecordNotFound(){
		Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData(
																		'00100000000aa00AAA',
																		'Name', 
																		'ParentId', 
																		'AccountNumber,NumberOfEmployees', 
																		'full');
        //Validate response map size, should be 2 as for columns and error
		System.assertEquals(2, hierarchyWrapper.size());
		//Validate map contains data
		System.assertEquals(true, hierarchyWrapper.containsKey('error'));
        System.assertEquals(ObjectHierarchy_Utility.RECORD_NOT_FOUND, hierarchyWrapper.get('error'));
	}
	
    static testMethod void testGetObjectHierarchyDataWithInvalidId(){
		try{
			Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData(
																		'INVALID_ID',
																		'Name', 
																		'ParentId', 
																		'AccountNumber,NumberOfEmployees', 
																		'full');
		}
		catch(Exception e){
			System.assertNotEquals(null, e.getMessage());
		}
	}
    
	static testMethod void testGetObjectHierarchyDataWithSOQLLimitReached() {

		List<Account> hierarchyAccountData  = ObjectHierarchy_TestUtility.getSampleHierarchyAccounts(3);
		//Expicitly Mark SOQL Limit Reached as true
		ObjectHierarchy_Utility.SOQLLimitReached = true;
		Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData(hierarchyAccountData[0].Id,
																		'Name', 
																		'ParentId', 
																		'AccountNumber,NumberOfEmployees', 
																		'full');
		//Validate response map size
		System.assertEquals(3, hierarchyWrapper.size());
		//Validate map contains data
		System.assertEquals(true, hierarchyWrapper.containsKey('data'));
		List<ObjectHierarchy_Utility.ObjectWrapper> data = (List<ObjectHierarchy_Utility.ObjectWrapper>) hierarchyWrapper.get('data');
		//Validate data size
		System.assertEquals(1, data.size());

		//Validate map contains columns
		System.assertEquals(true, hierarchyWrapper.containsKey('columns'));
		List<ObjectHierarchy_Utility.ColumnWrapper> columns = (List<ObjectHierarchy_Utility.ColumnWrapper>) hierarchyWrapper.get('columns');
		//Validate data size
		System.assertEquals(3, columns.size());

		//Validate map contains warning
		System.assertEquals(true, hierarchyWrapper.containsKey('warning'));
		//Validate warning message
		System.assertEquals(ObjectHierarchy_Utility.SOQL_LIMIT_REACHED_MSG, String.valueOf(hierarchyWrapper.get('warning')));
	}

	static testMethod void testGetObjectHierarchyDataWithInvalidTitleField(){
        List<Account> hierarchyAccountData  = ObjectHierarchy_TestUtility.getSampleHierarchyAccounts(3);
		Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData(
																		hierarchyAccountData[0].Id,
																		'InvalidNameField', 
																		'ParentId', 
																		'AccountNumber,NumberOfEmployees', 
																		'full');
		//Validate response map size
		System.assertEquals(1, hierarchyWrapper.size());
		//Validate map contains data
		System.assertEquals(true, hierarchyWrapper.containsKey('error'));
        System.assertEquals(ObjectHierarchy_Utility.INVALID_TITLE_FIELD + 'InvalidNameField', hierarchyWrapper.get('error'));
	}

	static testMethod void testGetObjectHierarchyDataWithInvalidParentField(){
        List<Account> hierarchyAccountData  = ObjectHierarchy_TestUtility.getSampleHierarchyAccounts(3);
		Map<String, Object> hierarchyWrapper = ObjectHierarchy_ComponentController.getObjectHierarchyData(
																		hierarchyAccountData[0].Id,
																		'Name', 
																		'InvalidParentField', 
																		'AccountNumber,NumberOfEmployees', 
																		'full');
		//Validate response map size
		System.assertEquals(1, hierarchyWrapper.size());
		//Validate map contains data
		System.assertEquals(true, hierarchyWrapper.containsKey('error'));
        System.assertEquals(ObjectHierarchy_Utility.INVALID_PARENT_FIELD + 'InvalidParentField', hierarchyWrapper.get('error'));
	}
}