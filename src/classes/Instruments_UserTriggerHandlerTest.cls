// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_UserTriggerHandlerTest
// Description: Test Class for Instruments_UserTriggerHandler    
// April 06, 2016    Isha Shukla    Original
@isTest (SeeAllData=true)
private class Instruments_UserTriggerHandlerTest { 
    static testMethod void UserTriggerHandlerTest(){

        Test.startTest();
        //FlexChatterProfilesUserGroup__c chatterProfile = new FlexChatterProfilesUserGroup__c(Name = 'Flex - Business Admin');
        //insert chatterProfile;
        //CollaborationGroup grp = new CollaborationGroup(Name = 'Flex Chatter Group',CollaborationType = 'Public');
        //insert grp;
        List<User> userList = TestUtils.createUser(2,'Flex - Business Admin',false);
        userList[0].Division='IVS';
        userList[1].Division='IVS';
        insert userList;
        Id profileId = TestUtils.fetchProfile('Flex - Business Admin').Id;       
        Test.stopTest();
        
        System.assertEquals(2, [SELECT Id FROM User WHERE Division='IVS' AND ProfileId = :profileId].size());
    }
}