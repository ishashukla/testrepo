// (c) 2015 Appirio, Inc.
//
// Class Name: MonthlyForecastTriggerHandlerTest
// Description: Test Class for ForecastYearTrigger_Handler and MonthlyForecastTriggerHandler class.
// 
// April 5 2016, Meena Shringi  Original 
//
@isTest 
public class MonthlyForecastTriggerHandlerTest {
    static testMethod void testForecastYearTrigger_Handler(){
        Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
        Id recordTypeRep = rtByNameRep.getRecordTypeId();
        Schema.RecordTypeInfo rtByNameManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast');
        Id recordTypeManager = rtByNameManager.getRecordTypeId();
        Profile pr =  TestUtils.fetchProfile('System Administrator');
        User usr = TestUtils.createUser(1, pr.Name, false).get(0);
        usr.Division = 'NSE';
        insert usr;
        TriggerRunSetting__c triggerRunSetting = new TriggerRunSetting__c(Name = 'MonthlyForecastTrigger',Trigger_Run__c = True);
        insert triggerRunSetting;
        Profile pr2 =  TestUtils.fetchProfile('Instruments Sales User');
        List<User> instUsr = TestUtils.createUser(3, pr2.Name, false);
        instUsr[0].Division = 'NSE';
        instUsr[1].Division = 'NSE';
        instUsr[2].Division = 'NSE';
        insert instUsr;
        system.runAs(usr){
            Account acc = TestUtils.createAccount(1,true).get(0);
            TestUtils.createCommConstantSetting();
            List<Opportunity> oppList = new List<Opportunity>();
            Opportunity opp = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp.Business_Unit__c = 'NSE';
            //opp.Opportunity_Number__c = '50001';
            opp.CloseDate = System.today();
            opp.ForecastCategoryName = 'Pipeline';              
            opp.OwnerId = usr.Id;
            opp.Submitted_for_Forecast__c = true;
            oppList.add(opp);
            Opportunity opp1 = TestUtils.createOpportunity(1,acc.Id,false).get(0);
            opp1.Business_Unit__c = 'NSE';
            //opp1.Opportunity_Number__c = '50001';
            opp1.CloseDate = System.today();
            opp1.ForecastCategoryName = 'Pipeline';              
            opp1.OwnerId = usr.Id;
            opp1.Submitted_for_Forecast__c = true;
            oppList.add(opp1);
            insert oppList;
            List<Mancode__c> mancodeList1 = TestUtils.createMancode(1, usr.Id, instUsr[0].Id, false);
            mancodeList1[0].Business_Unit__c = 'NSE';
            insert mancodeList1;
            List<Mancode__c> mancodeList2 = TestUtils.createMancode(1, instUsr[0].Id, instUsr[1].Id, false);
            mancodeList2[0].Business_Unit__c = 'NSE';
            insert mancodeList2;
            List<Mancode__c> mancodeList3 = TestUtils.createMancode(1, instUsr[1].Id, instUsr[2].Id, false);
            mancodeList3[0].Business_Unit__c = 'NSE';
            insert mancodeList3;
            Test.startTest();
                List<Forecast_Year__c> lstForcastYear = TestUtils.createForcastYear(1,usr.Id,recordTypeRep,false);
                lstForcastYear[0].Business_Unit__c = 'NSE';
                lstForcastYear[0].Manager__c = instUsr[0].Id;
                lstForcastYear[0].Year__c = String.valueOf(System.today().year());
                lstForcastYear[0].Year_External_Id__c= '1234';
                lstForcastYear.add(TestUtils.createForcastYear(1,instUsr[0].Id,recordTypeManager,false).get(0));
                lstForcastYear[1].Manager__c = instUsr[1].Id;
                lstForcastYear[1].Business_Unit__c = 'NSE';
                lstForcastYear[1].Year__c = String.valueOf(System.today().year());
                lstForcastYear[1].Year_External_Id__c= '12345';
                insert lstForcastYear;   
                Monthly_Forecast__c monthlyforcast = [select Id,Quota__c,Total_Sales__c,Trail_Forecast_Total__c,Manual_Adjustments__c,Bill_Only_Forecast__c,Forecast_Submitted__c from Monthly_Forecast__c where Forecast_Record_Type_Id__c = :recordTypeRep LIMIT 1];
                System.assert(monthlyforcast !=null ,'monthly forecasts should be addded related to forecast');
                monthlyforcast.Forecast_Submitted__c = true;
                List<String> monthString = new List<String>{'','January','February','March','April','May','June','July','August','September','October','November','December'};
                monthlyforcast.Month__c = monthString.get(System.today().month());
                monthlyforcast.Quota__c = 12; 
                monthlyforcast.Total_Sales__c = 100;
                monthlyforcast.Trail_Forecast_Total__c = 100;
                monthlyforcast.Manual_Adjustments__c = 100;
                monthlyforcast.Bill_Only_Forecast__c = 100;
                update monthlyforcast;
                List<Monthly_Forecast__c> monthlyforcastlst = new List<Monthly_Forecast__c>();
                monthlyforcast.Forecast_Submitted__c = false;
                monthlyforcastlst.add(monthlyforcast);
                Monthly_Forecast__c monthlyforcast1 = [SELECT Id,Quota__c,Forecast_Submitted__c FROM Monthly_Forecast__c WHERE Forecast_Record_Type_Id__c = :recordTypeManager LIMIT 1];
                monthlyforcast1.Month__c = monthString.get(System.today().month());
                monthlyforcastlst.add(monthlyforcast1);
                update monthlyforcastlst;
            Test.stopTest();
            System.assertEquals([SELECT Quota__c FROM Monthly_Forecast__c WHERE Forecast_Record_Type_Id__c = :recordTypeRep LIMIT 1].Quota__c, 12);
        }
    }
}