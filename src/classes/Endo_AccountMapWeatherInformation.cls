/*******************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 10, 2014 
 Purpose       :  This will be used for showing map, weather and local time of account
*******************************************************************************/
public with sharing class Endo_AccountMapWeatherInformation {
    public Account acc{get;set;}
    public Endo_Address myaddress{get; set;}
    public String error{get; set;}
    public Endo_AccountMapWeatherInformation(ApexPages.StandardController stdController){
        if(!Test.isRunningTest()){ // for test classes - addFields are not working with test classes
            stdController.addFields(accountFields());
        }
        acc = (Account)stdController.getRecord();
        myaddress = new Endo_Address();
        myaddress.city = acc.ShippingCity;
        myaddress.state = acc.ShippingState;
        myaddress.country = acc.ShippingCountry;
        myaddress.countryCode = acc.ShippingCountryCode;
        myaddress.zipCode = acc.ShippingPostalCode;
        myaddress.street = acc.ShippingStreet;
        
        if(myaddress.zipCode == null && myaddress.city == null && myaddress.state == null && myaddress.country == null && myaddress.countryCode == null){
        	error = 'Cannot display location information for this Customer Account: No Shipping Address is listed.';
        }        
    }
    //To get account fields
    @TestVisible
    private List<String> accountFields(){
        List<string> fields = new List<String>();
        SObjectType objToken = Schema.getGlobalDescribe().get('Account');
        Map<String, SObjectField> fieldsDesc = objToken.getDescribe().fields.getMap();
        for(String f : fieldsDesc.keySet()){
            fields.add(f);
        }   
        return fields;
    }
}