/********************************************
* Appirio, Inc 
* Name                    Endo_Serial_Numbers_on_Order_Test
* Created By              Gagan Brar
* Created Date            Sept 12, 2016
* Description             I-234908
* Test class for Endo_Serial_Numbers_on_Order.cls
********************************************/

@isTest
public class Endo_Serial_Numbers_on_Order_Test {

    public static testMethod void Endo_Serial_Number_on_Order_Test() {
        
        Account acc = new Account(Name = 'Test Account');
        insert acc;

        Order ordr = new Order(Name='1234', AccountId = acc.Id, Oracle_Order_Number__c = '1234', EffectiveDate=system.today());
        insert ordr;
        
        Test.startTest();
        
        PageReference pageRef = new ApexPages.StandardController(ordr).view();
        pageRef.setRedirect(true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ordr);
        // Initialising controller
        Endo_Serial_Number_on_Order_Handler controller = new Endo_Serial_Number_on_Order_Handler(sc); 
        
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getparameters().put('id', ordr.id);

        Test.stopTest();
   }
}