//
// (c) 2012 Appirio, Inc.
//
//  Test class of  WebserviceAmendOpportunity
//
// 26 Aug 2015     Naresh K Shiwani      Original
//
@isTest
private class FileImportControllerTest {
	
  static testmethod void testUploadCSV(){
  	Test.startTest();
  	TestUtils.createCommConstantSetting();
  	Account acnt                        = TestUtils.createAccount(1, true).get(0);
    Opportunity opp                     = TestUtils.createOpportunity(1, acnt.id, false).get(0);
    Schema.RecordTypeInfo rtByNameFlex = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional');
    Id recordTypeFlex = rtByNameFlex.getRecordTypeId();
	opp.recordTypeId = recordTypeFlex;
	insert opp;
    System.assertNotEquals(opp, null);
    Flex_Credit__c fc                   = new Flex_Credit__c();
    fc.Credit_Amount__c                 = 10;
    fc.Credit_Approval_Duration__c      = 1;
    fc.Credit_Status__c                 = 'New';
    fc.Opportunity__c                   = opp.Id;
    insert fc;
    System.assertNotEquals(fc, null);
  	Attachment attach                   = new Attachment();
		String dummyData                    = 'Salesforce_com_ID__c,Lessee_Legal_Name__c,Proposed_Address__c,Secretary_of_State__c,Finance_Amount_Up_To__c,Status__c,Term_Up_To_Months__c,Risk_Rating__c,Payment_Frequency__c,Interest_Rate__c,Residual_Percentage__c,Guarantor__c,Additional_Comments_Document_Requirement__c,Sales_Tax_Type__c,Expiry_Date__c,FMV_Exemption__c,Sales_Tax_Comments__c,Additional_Tax_Comments__c,Application_Number__c,Applicant_Type__c,Ownership_Structure__c,State_Sales_Tax_Rate__c,County_Sales_Tax_Rate__c,City_Sales_Tax_Rate__c,Misc_Sales_Tax_Rate__c,Total_Sales_Tax_Rate__c,EOT_Sales_Tax_Treatment__c,Sales_Tax_Exempt_Form__c,Entity_Type__c,X1_Out_Exemption__c' +
											                    '\n' +
											                    fc.Id+ ',' +
											                    '"THE MCDOWELL HOSPITAL, INC.","430 RANKIN DRIVE, MARION NC 28752",Active-In Good Standing,564577.89,Approval,60,N/A,Monthly,RATE PER CURRENT RATE SHEET,PERCENTAGE PER APPROVED RESIDUAL MATRX,Mr. Joe Guarantor,"Standard Documents apply including tax exempt certificate, certificates of insurance. If Powerload equipment, Property Waiver document required. If entity is a municipality and structure a FMV please email USB to get property tax amounts that may need to be included in customers payment.",Sales Tax,07-09-2015,No,None,If equipment located in Texas or Massachusetts finance charges must be disclosed in documents. If equipment located in Florida there will be a Florida Doc Stamp Fee (not charged if structure is FMV). If customer located in Tennessee there will be a Tennessee indebtedness tax when the UCC is filed.,1979104,Hospital,Corporation,0.0475,0.02,0,0,0.0675,FMV - Rental Stream,Sales Tax Exempt Form,Medical For-Profit,No';
  	attach.Name                         ='Test.csv';
    Blob value                          = Blob.valueOf(dummyData);
    attach.body                         = value;
  	FileImportController fileImportConObj = new FileImportController(new ApexPages.StandardController(fc));
  	fileImportConObj.attachment = attach;
  	fileImportConObj.upload();
  	fileImportConObj.close();
  	Test.stopTest();
  }
  
  static testmethod void testUploadCSVWithErrors(){
  	Test.startTest();
  	TestUtils.createCommConstantSetting();
    Account acnt                          = TestUtils.createAccount(1, true).get(0);
    Opportunity opp                       = TestUtils.createOpportunity(1, acnt.id, false).get(0);
    Schema.RecordTypeInfo rtByNameFlex = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional');
    Id recordTypeFlex = rtByNameFlex.getRecordTypeId();
	opp.recordTypeId = recordTypeFlex;
	insert opp;
    System.assertNotEquals(opp, null);
    Flex_Credit__c fc                     = new Flex_Credit__c();
    fc.Credit_Amount__c                   = 10;
    fc.Credit_Approval_Duration__c        = 1;
    fc.Credit_Status__c                   = 'New';
    fc.Opportunity__c                     = opp.Id;
    insert fc;
    FileImportController fileImportConObj = new FileImportController(new ApexPages.StandardController(fc));
    fileImportConObj.upload();
    
    Attachment attach                     = new Attachment();
    String dummyData                      = 'Single Line Data';
    attach.Name                           ='Test.csv';
    Blob value                            = Blob.valueOf(dummyData);
    attach.body                           = value;
    fileImportConObj.attachment           = attach;
    fileImportConObj.upload();
    
    Attachment attach1                    = new Attachment();
    dummyData                             = 'Salesforce_com_ID__c,Lessee_Legal_Name__c,Finance_Amount_Up_To__c,Term_Up_To_Months__c,Expiry_Date__c' +
	                                          '\n' +
	                                          '"Test",123,564577.89,"Test",07-09';
    attach1.Name                          = 'Test1.csv';
    value                                 = Blob.valueOf(dummyData);
    attach1.body                          = value;
    fileImportConObj.attachment           = attach1;
    fileImportConObj.upload();
    Test.stopTest();
  }
  
}