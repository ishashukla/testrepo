/**================================================================      
* Appirio, Inc
* Name: MancodeTriggerHandlerTest 
* Description: Test Class for MancodeTriggerHandler
* Created Date: 26 August,2016
* Created By: Isha Shukla (Appirio)
* 
* Date Modified      Modified By      Description of the update
* 26 Aug 2016        Isha Shukla         Class creation.
* October 23,2016	 Sahil Batra		T-549184 - Added code to populate Mancode record Name test
==================================================================*/
@isTest
private class MancodeTriggerHandlerTest {
    static Id oppInstrumentsRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type').getRecordTypeId();
    static List<Mancode__c> mancodeList;
    static User adminUser;
    static List<User> usrList;
    static User usr;
    static  List<Opportunity> oppList;
    @isTest static void testHandler() {
        createData();
        System.runAs(adminUser) {
            Test.startTest();
            insert mancodeList;
            mancodeList[0].Manager__c = usrList[2].Id;
            update mancodeList;
            Test.stopTest();
            System.assertEquals([SELECT userOrGroupId FROM OpportunityShare WHERE userOrGroupId = :usrList[2].Id AND OpportunityId = :oppList[0].Id ].size() > 0 ,true);
            List<Mancode__c> mancodeListtemp = new List<Mancode__c>();
            mancodeListtemp = [SELECT Name FROM Mancode__c WHERE id =:mancodeList[0].Id];
            if(mancodeListtemp.size() > 0){
            	//System.assertEquals(mancodeListtemp.get(0).Name,'1234');
            }
        }
        
    }
    public static void createData() {
        adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
        insert adminUser;
        usrList = TestUtils.createUser(4,'Instruments Sales User', false);
        usrList[0].Division = 'Surgical';
        usrList[1].Division = 'Surgical';
        usrList[2].Division = 'Surgical';
        insert usrList;
        usr = TestUtils.createUser(1,'Instruments Sales Manager', false).get(0);
        usr.Division = 'Surgical';
        insert usr;
        System.runAs(adminUser) {
        //    Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=adminUser.Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
       //     insert terr;
            mancodeList = TestUtils.createMancode(1, usrList[0].Id, usrList[1].Id, false);
            mancodeList[0].Business_Unit__c = 'Surgical';
       //     mancodeList[0].Territory__c = terr.Id;
            List<Account> accList = TestUtils.createAccount(1, True);
            oppList = TestUtils.createOpportunity(1, accList[0].Id, false);
            oppList[0].Business_Unit__c = 'Surgical';
            oppList[0].Opportunity_Number__c = '50001';
            oppList[0].OwnerId = usrList[0].Id;
            oppList[0].RecordTypeId = oppInstrumentsRecTypeId;
            insert oppList;
            OpportunityShare oppShare = new OpportunityShare();
            oppShare.userOrGroupId = usrList[1].Id;
            oppShare.OpportunityAccessLevel = 'Edit';
            oppShare.RowCause = Schema.OpportunityShare.RowCause.Manual;
            oppShare.OpportunityId = oppList[0].Id;
            insert oppShare;
        }
        
    }
}