/*
//
// (c) 2015 Appirio, Inc.
//
// Apex Controller for Visualforce component OpenLookupPopupUser
//
// This Component is using by Medical_ChangeCaseOwner Page.
//
// Created by : Surabhi Sharma (Appirio)
// 02 Feb 2016 , Sunil (Appirio) I-203808
// 16 Feb 2016 , Sunil (Appirio) I-203808
// 14 Jun 2016 , Shreerath (Appirio) I-222400 Search active users only
*/
public without sharing class OpenLookupPopupUserCtrl {

	public List<Sobject> listRecords {get;set;}
	public List<String> listFieldNames {get; set;}
	public String selectedObject;
	public String fieldName;
	public String searchText{get;set;}
	public String fieldSetName;
	private String targetObject;
  private String targetProfile;
  
  //I-227281(SN)
  private Id InstrumentProcareProfile;
    


  //-----------------------------------------------------------------------------------------------
  //  Cosntructor
  //-----------------------------------------------------------------------------------------------
  public OpenLookupPopupUserCtrl(){
  	selectedObject = ApexPages.currentPage().getParameters().get('Object');
    fieldName = ApexPages.currentPage().getParameters().get('fieldName');
    searchText = ApexPages.currentPage().getParameters().get('searchText');
    fieldSetName = ApexPages.currentPage().getParameters().get('fieldSetName');
    targetObject = ApexPages.currentPage().getParameters().get('targetObject');
    targetProfile = ApexPages.currentPage().getParameters().get('targetProfile');
    InstrumentProcareProfile = ApexPages.currentPage().getParameters().get('InstrumentProcareProfile');
    System.debug('@@@shree' + InstrumentProcareProfile);
    List<SObject> listRecords = new List<SObject>();
    System.debug('@@@' + listRecords);
    runSearch();
    System.debug('@@@' + listRecords);
  }


  //-----------------------------------------------------------------------------------------------
  //  Method to run the search with parameters passed via Page Parameters
  //-----------------------------------------------------------------------------------------------
  public PageReference runSearch() {
    Id profileId;
    if(!String.isBlank(targetProfile)){
      profileId = targetProfile;
    }else{
      Profile medicalProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Medical - Call Center'].get(0);
      profileId = medicalProfile.Id;
    }

    if(selectedObject ==  'User') {
    	Set<Id> setOwnerId =new Set<Id>();
	    for(Event e :[SELECT Id, OwnerId FROM Event WHERE ShowAs = 'OutOfOffice' AND EndDateTime >= :DateTime.Now()]) {
	    	System.debug('@@@@###' + e.OwnerId);
	      setOwnerId.add(e.OwnerId);
	    }

	    System.debug( '===setOwnerId==' + setOwnerId);

	    String fieldsToFetch = getFieldsForSelectedObject();
	    String whereClause = '';
	    if(fieldsToFetch != null && fieldsToFetch != ''){
	    	String searchString = 'Select ' + fieldsToFetch + ' From ' + selectedObject;
	      if(searchText != '' && searchText != null){
	        whereClause = ' Where ' + fieldName  + ' LIKE \'%' + String.escapeSingleQuotes(searchText)+'%\'';
	      }

	      if (searchText == '' || searchText == null) {
	      	/***** I-222400,I-227281 (SN) *************/ 
	      	whereClause = ' Where Id NOT IN : setOwnerId AND (ProfileId = :InstrumentProcareProfile OR ProfileId = :profileId)  AND IsActive = true';
	      }
	      else {
	      	/***** I-222400,I-227281 (SN) *************/
	      	whereClause += ' AND Id NOT IN : setOwnerId AND (ProfileId = :InstrumentProcareProfile OR ProfileId = :profileId)   AND IsActive = true';
	      }

	      System.debug( '===query==' + searchString + whereClause + ' order by Name  limit 50');
	      listRecords = Database.query(searchString + whereClause + ' order by Name  limit 50');
	    }
    }
    else if(selectedObject == 'Group') {
    	String fieldsToFetch = getFieldsForSelectedObject();
    	listRecords = new List<Sobject>();

    	if(fieldsToFetch != null && fieldsToFetch != ''){
    		String searchString =  'SELECT Queue.Id, QueueId , Queue.Name, Queue.Type FROM QueueSobject Where Queue.Type = \'Queue\'';

       	if(targetObject != null){
       	  searchString += ' AND SobjectType = :  targetObject ';
       	}
       	if(searchText != '' && searchText != null){
       		searchString += ' AND Queue.Name  LIKE \'%' + String.escapeSingleQuotes(searchText)+'%\'';
        }

        for(QueueSobject sobj : Database.query(searchString + ' order by Queue.Name limit 50')){
        	listRecords.add(sobj.Queue);
       	}

       	System.debug('================listRecords==================' +listRecords);
    	}
    }
    return null;
  }

  /*************************************************************************************************
  * Method to Get all Fields of the selected object
  *************************************************************************************************/
  public String getFieldsForSelectedObject(){
   listFieldNames = new List<String>();
   List<Schema.FieldSetMember> fieldSetMemberList = new List<Schema.FieldSetMember>();
   String fieldsToFetch = '';
   System.debug('==========selectedObject=========' +selectedObject);
       try{
       		//System.assert(false,selectedObject);

           if(selectedObject != null && selectedObject != '' && selectedObject == 'User'){

                Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
                Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(selectedObject);
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                //System.debug('=====================Name+++++++++++++++++++++++++++++++++' +listFieldNames);
                if(DescribeSObjectResultObj.FieldSets.getMap().ContainsKey(fieldSetName)){
                    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
                    fieldSetMemberList =  fieldSetObj.getFields();
                }

                for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                {
                    listFieldNames.add(string.ValueOf(fieldSetMemberObj.getFieldPath()));
                    System.debug('+++++++++++++++++Name+++++++++++++++++++++++++++++++++' +listFieldNames);
                }
           } else if(selectedObject != null && selectedObject != '' && selectedObject == 'Group'){
            	String field = 'Name';
            	listFieldNames.add(field);
            	System.debug('Name======================================+' +listFieldNames);
            }
            else{
            	String field1 = 'Name';
            	listFieldNames.add(field1);
            	System.debug('::::::::::::::::::::::Name======================================+' +listFieldNames);
            }

           //Building Query with the fields
            Integer i = 0;
            Integer len = listFieldNames.size();
            for(String temp:listFieldNames){
                if(i==len-1){
                    fieldsToFetch = fieldsToFetch + temp;
                } else {
                    fieldsToFetch = fieldsToFetch + temp + ',';
                }
                i++;
            }
        }catch(Exception ex){
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,
                                'There is no Field for selected Object!'));
        }
        return fieldsToFetch;
    }
}