/**================================================================
* Author        :  Appirio JDC
* Date          :  
* Description   :  
* 
* Update Date      Author            Description
* Jun 6, 2016      Padmesh Soni      S-421189
 ==================================================================*/
public  class NV_OrderItemTriggerHandler { //Commented by Rahul Aeran with sharing

    public static String TrackingNumerDelimeter = ';';

    //Added by Jyoti for Story S-389737
    public static final String BACKORDERED  = 'Backordered';

    public static void processOrderItemsBeforeInsertUpdate(Map<Id, OrderItem> oldRecordMap, List<OrderItem> newRecords, Boolean isUpdate){

        Set<String> statusToConsider = new Set<String>{'Shipped', 'Delivered', 'Closed'};
        Map<Id, List<OrderItem>> orders = new Map<Id, List<OrderItem>>(); //Comment when deployed.
        OrderItem oldRecord;
        for(OrderItem newRecord : newRecords){
            oldRecord = null;
            if(isUpdate && oldRecordMap != null && oldRecordMap.containsKey(newRecord.Id))
                oldRecord = oldRecordMap.get(newRecord.Id);

            if((oldRecord == null || oldRecord.Status__c != newRecord.Status__c)
                    && statusToConsider.contains(newRecord.Status__c)){
                if(newRecord.Status__c == 'Shipped') newRecord.Shipped_Date__c = Date.today();
    
                //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - Start
                //Code commented
                //else if(newRecord.Status__c == 'Delivered') newRecord.Delivered_Date__c = Date.today();
                //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - End
    
                else if(newRecord.Status__c == 'Closed' && newRecord.Shipped_Date__c == null) newRecord.Shipped_Date__c = Date.today();
                else if(newRecord.Status__c == 'Cancelled') newRecord.Cancelled_Date__c = Datetime.now(); //Uncomment when deployed

                //Comment the following when deployed
                if(newRecord.Status__c == 'Shipped' || newRecord.Status__c == 'Delivered' || newRecord.Status__c == 'Closed'){
                    if(!orders.containsKey(newRecord.OrderId)){
                        orders.put(newRecord.OrderId, new List<OrderItem>());
                    }
                    orders.get(newRecord.OrderId).add(newRecord);
                } else if(newRecord.Status__c == 'Cancelled'){
                    newRecord.Cancelled_Date__c = Datetime.now();
                }
            }

            //Logic for S-371704, Populate Tracked_Number__c with the first FedEx tracking number that comes in from Oracle.
            if(oldRecord == null || oldRecord.Tracking_Number__c != newRecord.Tracking_Number__c){
                if(!String.isEmpty(newRecord.Tracking_Number__c) && newRecord.Tracking_Number__c.contains(NV_OrderItemTriggerHandler.TrackingNumerDelimeter)){
                    newRecord.Tracked_Number__c = newRecord.Tracking_Number__c.split(NV_OrderItemTriggerHandler.TrackingNumerDelimeter)[0].trim();
                }
                else{
                    newRecord.Tracked_Number__c = newRecord.Tracking_Number__c;
                }
                newRecord.Order_Delivered__c = false;
                newRecord.Tracking_Information__c = '';
            }

            newRecord.On_Hold__c = !String.isEmpty(newRecord.Hold_Description__c);
        }

        //Comment the following when deployed
        /*Map<Id,Order> orderData = getOrdersByIds(orders.keySet());
        for(Order o : orderData.values()){
           if(o.Date_Ordered__c != null){
                String dateInPST =  o.Date_Ordered__c.format('yyyy-MM-dd', 'America/Los_Angeles');
                for(OrderItem oi : orders.get(o.Id)){
                    oi.Shipped_Date__c = Date.valueOf(dateInPST);
                }
            }
        }*/
    }

    public static void processOrderItemsAfterInsertUpdate(Map<Id, OrderItem> oldRecords, Map<Id, OrderItem> newRecords){

        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

        Set<Id> orderIds = new Set<Id>();
        Set<Id> orderIdsToConsiderForStatus = new Set<Id>();

        Map<Id, Map<String, List<OrderItem>>> orderToStatusItemMap = new Map<Id, Map<String, List<OrderItem>>>();
        Map<Id, Set<String>> orderIdToOrderItemsWithHold = new Map<Id,Set<String>>();
        Set<Id> orderIdsWithNewItems = new Set<Id>();

        //Added by Jyoti for Story S-389737
        setOrderPBOonInsertUpdate(oldRecords,newRecords);

        for(OrderItem newRecord: newRecords.values()){
            OrderItem oldRecord = new OrderItem();
            if(oldRecords!= null && oldRecords.containsKey(newRecord.Id))
                oldRecord = oldRecords.get(newRecord.Id);
            System.debug('TRACE: oldRecord:  ' + oldRecord);
            System.debug('TRACE: newRecord:  ' + newRecord);
            if(oldRecord!= null && oldRecord.Status__c != newRecord.Status__c){
                orderIds.add(newRecord.OrderId);
                orderIdsToConsiderForStatus.add(newRecord.OrderId);

                String toAdd;
                if(newRecord.Status__c == 'Delivered') toAdd = 'DELIVERED';
                else if(newRecord.Status__c == 'Shipped') toAdd = 'SHIPPED';
                else if(oldRecord.Status__c == 'Shipped' && newRecord.Status__c != 'Delivered') toAdd = 'SHIPPED_NOT';
                else if(newRecord.Status__c == 'Backordered') toAdd = 'BACKORDERED';
                else if(oldRecord.Status__c == 'Backordered' && newRecord.Status__c != 'Shipped' && newRecord.Status__c != 'Delivered') toAdd = 'NOT_BACKORDERED';
                else toAdd = newRecord.Status__c +'_OTHERS';

                if(toAdd!=null){
                    if(orderToStatusItemMap.containsKey(newRecord.OrderId)){
                        if(orderToStatusItemMap.get(newRecord.OrderId).containsKey(toAdd)){
                            orderToStatusItemMap.get(newRecord.OrderId).get(toAdd).add(newRecord);
                        }
                    }
                    else{
                        orderToStatusItemMap.put(newRecord.OrderId, new Map<String, List<OrderItem>>());
                        orderToStatusItemMap.get(newRecord.OrderId).put(toAdd, new List<OrderItem>{newRecord});
                    }
                }
            }

            if(oldRecord!= null && oldRecord.Hold_Description__c != newRecord.Hold_Description__c){
                if((String.isBlank(oldRecord.Hold_Description__c) && !String.isBlank(newRecord.Hold_Description__c))
                    ||(!String.isBlank(oldRecord.Hold_Description__c) && String.isBlank(newRecord.Hold_Description__c))){

                    orderIds.add(newRecord.OrderId);

                    String toAdd = '';
                    if(String.isBlank(newRecord.Hold_Description__c)) toAdd = 'NOT_ON_HOLD';
                    else toAdd = 'ON_HOLD';

                    if(!orderIdToOrderItemsWithHold.containsKey(newRecord.OrderId))
                        orderIdToOrderItemsWithHold.put(newRecord.OrderId, new Set<String>());
                    orderIdToOrderItemsWithHold.get(newRecord.OrderId).add(toAdd);
                }
            }

            if(oldRecord != null && oldRecord.Id == null){
                //NEW RECORD. CHECK IT.
                orderIds.add(newRecord.OrderId);
                orderIdsWithNewItems.add(newRecord.OrderId);
            }
        }

        System.debug('[DD] : orderToStatusItemMap : '+ orderToStatusItemMap);
        System.debug('[DD] : orderIdToOrderItemsWithHold : '+ orderIdToOrderItemsWithHold);
        Set<Id> userForSettings = new Set<Id>();
        Map<Id,Order> orderData = new Map<Id,Order>();
        Map<Id, Set<Id>> orderToUserMap = new Map<Id, Set<Id>>();
        Map<Id,Notification_Settings__c> userSettingsMap = new Map<Id,Notification_Settings__c>();
        if(orderIds.size() > 0){
          orderData = getOrdersByIds(orderIds);
        }
        
        for(Order record : orderData.values()){
            userForSettings.add(record.Account.OwnerId);
        }

        //Collect all users for each order
        if(userForSettings.size() > 0 && orderIds.size() > 0){
            orderToUserMap = NV_Utility.getFollowUsersForRecords(orderIds, userForSettings);
        }
        
        //Add Current User In Set
        userForSettings.add(UserInfo.getUserId());

        //Get Notification Settings for all users
        if(orderData.size() > 0){
            userSettingsMap = NV_Utility.getNotificationSettingsForUsers(userForSettings);
        }
        Notification_Settings__c userSettings;
        System.debug('[DD] : userSettingsMap : '+ userSettingsMap);

        List<Order> ordersToUpdate = new List<Order>();

        for(Order orderRecord: orderData.values()){
            if(orderToStatusItemMap.containsKey(orderRecord.Id)
                || orderIdToOrderItemsWithHold.containsKey(orderRecord.Id)
                || orderIdsWithNewItems.contains(orderRecord.Id)){

                Set<Id> usersToMentionForStatusChange = new Set<Id>();
                Set<Id> usersToMentionForCreation = new Set<Id>();
                Set<Id> usersToMentionForHoldChange = new Set<Id>();

                //Check If Order Exist in orderToUserMap & collect users for both feeds
                if(orderToUserMap.containsKey(orderRecord.Id)){
                    for(Id userId : orderToUserMap.get(orderRecord.Id)){
                        //Check If User Settings Exist in userSettingsMap
                        if(userSettingsMap.size() > 0 && userSettingsMap.containsKey(userId)){
                            if(orderToStatusItemMap.containsKey(orderRecord.Id)){
                                userSettings = userSettingsMap.get(userId);
                                //Get ValidStatus Set from Settings
                                Set<String> validStatus = new Set<String>();
                                if(!String.isBlank(userSettings.Settings__c))
                                    validStatus.addAll(userSettings.Settings__c.split(';'));

                                //Iterate over all status to collected users
                                for(String orderItemStatus : orderToStatusItemMap.get(orderRecord.Id).keySet()){
                                    Boolean toAdd = false;
                                    if(orderItemStatus == 'SHIPPED' && validStatus.contains('Shipped')) toAdd = true;
                                    else if(orderItemStatus == 'SHIPPED_NOT' && orderRecord.Status == 'Shipped' && validStatus.contains('Shipped')) toAdd = true;
                                    else if(orderItemStatus == 'DELIVERED' && validStatus.contains('Delivered')) toAdd = true;
                                    else if(orderItemStatus.contains('BACKORDERED') && validStatus.contains('Backordered')) toAdd = true;
                                    else if(orderItemStatus.contains('_OTHERS') && validStatus.contains(orderItemStatus.remove('_OTHERS'))) toAdd = true;
                                    if(toAdd) usersToMentionForStatusChange.add(userId);
                                }

                                if(!orderRecord.Creation_Notified__c && validStatus.contains('Creation')){
                                    //usersToMentionForCreation.add(userId);
                                    orderIdsWithNewItems.add(orderRecord.Id);
                                }
                            }

                            //Filtering Users for Status Hold Type Notification
                            if((String.isBlank(orderRecord.Hold_Type__c) || orderRecord.Hold_Type__c == 'Credit Check Failure')
                                && orderIdToOrderItemsWithHold.containsKey(orderRecord.Id)
                                && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userId), 'OnHold', false)){
                                usersToMentionForHoldChange.add(userId);
                            }

                            //Filtering Users for Creation Notification
                            if(orderIdsWithNewItems.contains(orderRecord.Id)
                                && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userId), 'Creation', false)
                                 ){
                                usersToMentionForCreation.add(userId);
                            }
                        }
                    }
                }

                if(orderRecord.Account.OwnerId!=null){
                    Id userIdForAllActive = orderRecord.Account.OwnerId;
                    System.debug('[DD] userIdForAllActive  : '+ userIdForAllActive);
                    if(orderToStatusItemMap.containsKey(orderRecord.Id)){
                        //Logic for Context User for Status Notification
                        if(!usersToMentionForStatusChange.contains(userIdForAllActive)
                                && userSettingsMap.containsKey(userIdForAllActive)){
                            userSettings = userSettingsMap.get(userIdForAllActive);
                            System.debug('[DD] Current user Settings: '+ userSettings);

                            //If User has opted for all Active Order, than only add him in mention
                            //Modified for S-399670
                            //if(userSettings.All_Active_Orders__c){
                             if(!String.isBlank(userSettings.All_Settings__c)){
                                Set<String> validStatus = new Set<String>();
                                //if(!String.isBlank(userSettings.All_Settings__c))
                                    validStatus.addAll(userSettings.All_Settings__c.split(';'));

                                //Iterate over all status to collected users
                                for(String orderItemStatus : orderToStatusItemMap.get(orderRecord.Id).keySet()){
                                    System.debug('[DD] orderItemStatus: '+ orderItemStatus);
                                    System.debug('[DD] ValidStatus: '+ validStatus);
                                    Boolean toAdd = false;
                                    if(orderItemStatus == 'SHIPPED' && validStatus.contains('Shipped')) toAdd = true;
                                    else if(orderItemStatus == 'SHIPPED_NOT' && orderRecord.Status == 'Shipped' && validStatus.contains('Shipped')) toAdd = true;
                                    else if(orderItemStatus == 'DELIVERED' && validStatus.contains('Delivered')) toAdd = true;
                                    else if(orderItemStatus.contains('BACKORDERED') && validStatus.contains('Backordered')) toAdd = true;
                                    else if(orderItemStatus.contains('_OTHERS') && validStatus.contains(orderItemStatus.remove('_OTHERS'))) toAdd = true;
                                    if(toAdd) usersToMentionForStatusChange.add(userIdForAllActive);
                                }

                                if(!orderRecord.Creation_Notified__c && validStatus.contains('Creation')){
                                    usersToMentionForCreation.add(userIdForAllActive);
                                    orderIdsWithNewItems.add(orderRecord.Id);
                                }
                              }
                           // }
                           //End S-399670
                        }
                    }

                    //Logic for Context User for Hold Type Notification
                    if(!usersToMentionForHoldChange.contains(userIdForAllActive)
                            && userSettingsMap.containsKey(userIdForAllActive)){
                        userSettings = userSettingsMap.get(userIdForAllActive);
                        //If User has opted for all Active Order, than only add him in mention
                        //Modified for S-399670
                        //if(userSettings.All_Active_Orders__c && NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userIdForAllActive), 'OnHold', false))
                        if(NV_Utility.isUserIsOptedForStatus(userSettingsMap.get(userIdForAllActive), 'OnHold', true)){
                            usersToMentionForHoldChange.add(userIdForAllActive);
                        }//end S-399670
                    }
                }

                System.debug('[DD] : usersToMentionForStatusChange : '+ usersToMentionForStatusChange);
                System.debug('[DD] : usersToMentionForHoldChange : '+ usersToMentionForHoldChange);
                System.debug('[BJ] : usersToMentionForCreation: ' + usersToMentionForCreation);

                //Logic to collect status feeds
                if(orderToStatusItemMap.containsKey(orderRecord.Id)){
                    String feedText = '';
                    String newStatus = '';
                    Boolean isPartialShipped = false ;

                    for(String currentStatus: orderToStatusItemMap.get(orderRecord.Id).keySet()){
                        if(currentStatus == 'BACKORDERED'){
                            feedText = NV_Utility.addNewLine(feedText, 'Some items are Backordered on Order for '+ orderRecord.Account.Name +'.');
                        }
                        else if(currentStatus == 'NOT_BACKORDERED'){
                            feedText = NV_Utility.addNewLine(feedText, 'Some items are no longer Backordered on Order for '+ orderRecord.Account.Name +'.');
                        }
                        else if(currentStatus == 'SHIPPED_NOT' && orderRecord.Status == 'Shipped'){
                            feedText = NV_Utility.addNewLine(feedText, 'Partial shipment of Order for '+ orderRecord.Account.Name +'.');
                            isPartialShipped = true;
                            //newStatus = 'Partial Ship';
                        }
                        else if(currentStatus == 'SHIPPED'){
                            //Collect all shipped items
                            Set<Id> orderItemIds = new Set<Id>();
                            for(OrderItem oItem : orderToStatusItemMap.get(orderRecord.Id).get('SHIPPED')){
                                orderItemIds.add(oItem.Id);
                            }
                            Boolean isAllShipped = checkIfAllOtherItemsHaveStatus(orderRecord, new Set<String>{'Shipped','Delivered'}, orderItemIds);
                            if(isAllShipped){
                                //newStatus = 'Shipped';
                                feedText = NV_Utility.addNewLine(feedText, 'Order for '+ orderRecord.Account.Name +' is Shipped.');
                            }
                            else if(!isPartialShipped){
                                //newStatus = 'Partial Ship';
                                feedText = NV_Utility.addNewLine(feedText, 'Partial shipment of Order for '+ orderRecord.Account.Name +'.');
                            }
                        }
                        //Is no item yet with status not delivered
                        else if(currentStatus == 'DELIVERED'){
                            //Collect all shipped items
                            Set<Id> orderItemIds = new Set<Id>();
                            Set<String> trackingNumbers = new Set<String>();
                            for(OrderItem oItem : orderToStatusItemMap.get(orderRecord.Id).get('DELIVERED')){
                                orderItemIds.add(oItem.Id);
                                //Collecte unique Tracking Numbers

                                //Commenting the Tracking Number and changing it to Tracked Number
                                //if(!String.isBlank(oItem.Tracking_Number__c)) trackingNumbers.add(oItem.Tracking_Number__c);
                                if(!String.isBlank(oItem.Tracked_Number__c)) trackingNumbers.add(oItem.Tracked_Number__c);
                            }
                            String trackingNumbersString = NV_Utility.getJoinStringFromSet(trackingNumbers, ', ');
                            Boolean isAllDelivered = checkIfAllOtherItemsHaveStatus(orderRecord, new Set<String>{'Delivered'}, orderItemIds);
                            String text = '';
                            if(isAllDelivered){
                                //newStatus = 'Delivered';
                                text = 'Order for '+ orderRecord.Account.Name +'';
                                if(trackingNumbersString!=null) text += ' having Tracking No(s): '+ trackingNumbersString;
                                text += ' is Delivered.';
                            }
                            else{
                                text = 'Some items of Order for '+ orderRecord.Account.Name +'';
                                if(trackingNumbersString!=null) text += ' having Tracking No(s): '+ trackingNumbersString;
                                text += ' is Delivered.';
                            }
                            feedText = NV_Utility.addNewLine(feedText, text);
                        }
                        else if(currentStatus.contains('_OTHERS')){
                            feedText = NV_Utility.addNewLine(feedText, 'Some items are moved to '+currentStatus.remove('_OTHERS')+' on Order for '+ orderRecord.Account.Name +'.');
                        }

                    }



                    System.debug('[DD] : ['+ orderRecord.Id +'] feedText : '+ feedText);
                    if(!String.isBlank(feedText) && usersToMentionForStatusChange.size()>0){
                        ConnectApi.FeedItemInput input = NV_ChatterApexUtility.getTextFeedItemInputWithMentionsAtEnd(orderRecord.Id,feedText,new List<Id>(usersToMentionForStatusChange));
                        batchInputs.add(new ConnectApi.BatchInput(input));
                    }



                    //if(!String.isBlank(newStatus)){
                    //  orderRecord.Status = newStatus;
                    //  ordersToUpdate.add(orderRecord);
                    //}
                }

                //Logic to collect hold type feeds
                if((String.isBlank(orderRecord.Hold_Type__c) || orderRecord.Hold_Type__c == 'Credit Check Failure') && orderIdToOrderItemsWithHold.containsKey(orderRecord.Id)){
                    String feedText = '';

                    for(String holdStatus : orderIdToOrderItemsWithHold.get(orderRecord.Id)){
                        if(holdStatus == 'ON_HOLD'){
                            feedText += 'Some items are On Hold in Order for '+ orderRecord.Account.Name +'.';
                        }
                        else if(holdStatus == 'NOT_ON_HOLD'){
                            feedText += 'Some items are no longer On Hold in Order for '+ orderRecord.Account.Name +'.';
                        }

                        if(!String.isBlank(feedText)){
                            feedText += '\n';
                        }
                    }
                    System.debug('[DD] : ['+ orderRecord.Id +'] feedText : '+ feedText);
                    if(!String.isBlank(feedText) && usersToMentionForHoldChange.size()>0){
                        ConnectApi.FeedItemInput input = NV_ChatterApexUtility.getTextFeedItemInputWithMentionsAtEnd(orderRecord.Id,feedText,new List<Id>(usersToMentionForHoldChange));
                        batchInputs.add(new ConnectApi.BatchInput(input));
                    }
                }

                Set<String> creationStatus = new Set<String>{'Entered', 'Booked', 'Awaiting Shipping', 'Backordered'};
                System.debug('Creation Notified? ' + orderRecord.Creation_Notified__c);
                System.debug('orderIdsWithNewItems: ' + orderIdsWithNewItems);
                if(!orderRecord.Creation_Notified__c && orderIdsWithNewItems.contains(orderRecord.Id)){
                    String feedText = '';
                    getOrderNewStatus(orderRecord);
                    if(creationStatus.contains(orderRecord.Status)){
                        feedText = feedText + orderRecord.Account.Name + ' has placed a new order.\n';
                    }

                    if(!String.isBlank(feedText) && usersToMentionForCreation.size()>0){
                        ConnectApi.FeedItemInput input = NV_ChatterApexUtility.getTextFeedItemInputWithMentionsAtEnd(orderRecord.Id,feedText,new List<Id>(usersToMentionForCreation));
                        batchInputs.add(new ConnectApi.BatchInput(input));
                    }
                }

            }

            if(orderIdsToConsiderForStatus.contains(orderRecord.Id)){
                if(getOrderNewStatus(orderRecord) || !orderRecord.Creation_Notified__c){
                    orderRecord.Creation_Notified__c = true;
                    ordersToUpdate.add(orderRecord);
                }
            } else if(!orderRecord.Creation_Notified__c){
                orderRecord.Creation_Notified__c = true;
                ordersToUpdate.add(orderRecord);
            }
        }

        if(!Test.isRunningTest() && batchInputs.size()>0){
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
        } //Uncomment When Deployed
        if(ordersToUpdate.size()>0){
            System.debug('[DD] : ordersToUpdate : '+ ordersToUpdate);
            NV_OrderTriggerHandler.IsToProcessAfterUpdate = false;
            update ordersToUpdate;
            NV_OrderTriggerHandler.IsToProcessAfterUpdate = true;
        }
    }

    public static Boolean checkIfAllOtherItemsHaveStatus(Order orderRecord, Set<String> status, Set<Id> itemIds){
        if(itemIds.size()>0){
            Boolean isSame = true;
            for(OrderItem record: orderRecord.OrderItems){
                if(itemIds.contains(record.Id)) continue;
                if(!status.contains(record.Status__c)){
                    isSame = false;
                    break;
                }
            }
            return isSame;
        }
        else{
            return false;
        }
    }

    public static Map<Id, Order> getOrdersByIds(Set<Id> recordIds){
        Map<Id, Order> data  = new Map<Id, Order>([Select Id, AccountId, Account.Name, Account.OwnerId, Status, Hold_Type__c, Contract.OwnerId,
                                                        RecordType.DeveloperName, Date_Ordered__c, Creation_Notified__c,
                                                    (Select Id, Status__c, Hold_Description__c, Order_Delivered__c  From OrderItems
                                                    Where OrderId In :recordIds)
                                                    From Order Where Id in: recordIds]);
        return data;
    }

    public static Boolean getOrderNewStatus(Order orderRecord){

        Integer value1, value2;
        String newStatus = orderRecord.Status;
        Integer isAtleastOneOrderShipped = 0;
        Integer isAtleastOneOrderBackordered = 0;
        Integer isOrderDelivered = 1;

        value1 = -1;
        for(OrderItem record: orderRecord.OrderItems){
            if(!String.isBlank(record.Status__c)){
                if(statusMapping.containsKey(orderRecord.RecordType.DeveloperName)
                && statusMapping.get(orderRecord.RecordType.DeveloperName).containsKey(record.Status__c)){
                    value2 = statusMapping.get(orderRecord.RecordType.DeveloperName).get(record.Status__c);
                    if(value2 > value1){
                        value1 = value2;
                        newStatus = record.Status__c;
                    }
                }
                if(record.Status__c == 'Backordered' || record.Status__c == 'Partial Shipping'){
                    isAtleastOneOrderBackordered = 1;
                }
            }

            System.debug('[DD] Status : '+ record.Status__c);
            System.debug('[DD] value1 : '+ value1);
            System.debug('[DD] value2 : '+ value2);
            System.debug('[DD] isAtleastOneOrderShipped : '+ isAtleastOneOrderShipped);

            //1= Delivered, 2 = Closed, 3 = Shipped
            if(orderRecord.RecordType.DeveloperName == 'NV_Standard'
                && value1 > 0 && value2 < 4 && value2 != 0
                && isAtleastOneOrderShipped != 1){
                isAtleastOneOrderShipped = 1;
            }

            //Check for Delivered
            if(isOrderDelivered != 0 && record.Order_Delivered__c) isOrderDelivered = 1;
            else isOrderDelivered = 0;
        }

        if(isOrderDelivered == 1 && newStatus != 'Cancelled') newStatus = 'Delivered';
        if(newStatus == 'Delivered' && isOrderDelivered != 1 ) newStatus = 'Shipped';

        System.debug('[DD] value1 : '+ value1);
        System.debug('[DD] value2 : '+ value2);
        System.debug('[DD] isAtleastOneOrderShipped : '+ isAtleastOneOrderShipped);

        if(value1 > 3 && isAtleastOneOrderShipped == 1) newStatus = 'Partial Ship';
        if(isAtleastOneOrderShipped == 0 && isAtleastOneOrderBackordered == 1) newStatus = 'Backordered';

        if(newStatus != orderRecord.Status) {
            orderRecord.Status = newStatus;
            return true;
        }
        else return false;
    }

    static Map<String, Map<String, Integer>> statusMapping;
    static{
        Map<String, String> statusPriorityMap = new Map<String, String>();
        statusPriorityMap.put('NV_Standard', 'Cancelled,Delivered,Closed,Shipped,Backordered,Awaiting Shipping,Booked,Entered');
        statusPriorityMap.put('NV_RMA', 'Cancelled,Closed,Returned,Awaiting Return,Booked,Entered');
        statusPriorityMap.put('NV_Bill_Only', 'Cancelled,Closed,Awaiting PO,Booked,Entered');
        statusMapping = new Map<String, Map<String, Integer>>();
        Integer index;
        for(String type : statusPriorityMap.keySet()){
            index = 0;
            statusMapping.put(type, new Map<String, Integer>());
            for(String status : statusPriorityMap.get(type).split(',')){
                statusMapping.get(type).put(status, index++);
            }
        }
    }

    //Start - Story S-389737
    //For getting Order Ids to set PartialBackOrder__c on after Delete

    public static void processOrderItemsAfterDelete(List<OrderItem> oldRecordList, Map<Id, OrderItem> oldRecords){
        Set<Id> orderIds = new Set<Id>();
        for(OrderItem oldRecord: oldRecordList){
            if(oldRecord.Status__c != null){
                orderIds.add(oldRecord.OrderId);
            }
        }
        if(orderIds.size() > 0)
            updateOrderPartialBackorder(orderIds);

    }
    //For getting Order Ids to set PartialBackOrder__c on after Undelete
    public static void processOrderItemsAfterUndelete(List<OrderItem> newRecordList){
        Set<Id> orderIds = new Set<Id>();
        for(OrderItem newRecord: newRecordList){
            if(newRecord.Status__c != null){
                orderIds.add(newRecord.OrderId);
            }
        }
        if(orderIds.size() > 0)
            updateOrderPartialBackorder(orderIds);
    }


    //For getting Order Ids to set PartialBackOrder__c on after Insert and update
    public static void setOrderPBOonInsertUpdate(Map<Id, OrderItem> oldRecords, Map<Id, OrderItem> newRecords){
      
        //Updates made by Rahul Aeran to stop recursion
        Boolean isUpdate = oldRecords != null ? true : false;
        Set<Id> orderIds = new Set<Id>();
        for(OrderItem newRecord: newRecords.values()){
         //NB - 09/20 - Direct UAT code changes start
            if((!isUpdate || oldRecords.get(newRecord.Id).Status__c != newRecord.Status__c) && newRecord.Status__c != null){
              orderIds.add(newRecord.OrderId);
            }
            
          /*  
             OrderItem oldRecord = new OrderItem();
            // - Since it's faulty if condition which makes no sense
            if((oldRecord == null && newRecord.Status__c != null) || (oldRecord != null && oldRecord.Status__c != newRecord.Status__c)){
                orderIds.add(newRecord.OrderId);
            }
          */
          
        //NB - 09/20 - Direct UAT code changes end

        }
        if(orderIds.size() > 0)
            updateOrderPartialBackorder(orderIds);
    }

    // For updating order PartialBackOrder__c field if any order item having Backordered status
    public static void updateOrderPartialBackorder(Set<Id> orderIds){
        List<Order> orderToUpdate = new List<Order>();
        for(Order order: [Select Id, Status, PartialBackOrder__c, (Select Id, Status__c From OrderItems where Status__c =: BACKORDERED  ) From Order where Id in: orderIds]) {
            if(order.OrderItems.size() > 0){
                order.PartialBackOrder__c = BACKORDERED  ;
                orderToUpdate.add(order);
            }
            else{
            //NB - 05/20 - Adding If condition to reduce un-necessary DML operation & trigger calling
                if(order.PartialBackOrder__c != null && order.PartialBackOrder__c != ''){ //NB - 11/10 - I-241409
                    order.PartialBackOrder__c = '';
                    orderToUpdate.add(order);
                }
           //NB - 05/20 - Recursive Trigger Issue - End     
            }
        }
        if(!orderToUpdate.isEmpty() && orderToUpdate.size() > 0){
            update orderToUpdate;
        }
    }

    //End - Story S-389737

}