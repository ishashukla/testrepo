//Modified By Kanika Mathur to fix test failure on 28 Aug 2016

@isTest
public class OpptyContactTriggrHandlerTest { 
  @isTest
  static void ContactOpptyAssociationTriggrHandlerTest () {
    List < Account > accountList = TestUtils.createAccount(1, true);
    List <Contact> contactList = TestUtils.createContact(2, accountList[0].id, true);
    List < Project_Plan__c > projectPlanList = TestUtils.createProjectPlan(1, accountList[0].id, true);
    List <Related_Contact__c> contactAssocList = TestUtils.createOpptyContact(1,contactList[0].id,projectPlanList[0].id, false);
    contactAssocList[0].Primary__c = true;
    contactAssocList[0].RecordTypeId = Schema.SObjectType.Related_Contact__c.getRecordTypeInfosByName().get('Contact Project Association').getRecordTypeId();
    insert contactAssocList;
    Project_Plan__c pp = [SELECT Id, Customer_Signer__c FROM Project_Plan__c WHERE Id =: projectPlanList[0].Id];
    System.assertEquals(pp.Customer_Signer__c , contactAssocList[0].Contact__c);
    List <Related_Contact__c> conAssocList =  TestUtils.createOpptyContact(1,contactList[1].id,projectPlanList[0].id, false);
    conAssocList[0].Primary__c = true;
    insert conAssocList;
    contactAssocList[0].Primary__c = false;
    update contactAssocList;
    contactAssocList[0].Primary__c = true;
    delete contactAssocList;
  }
}