//
// (c) 2015 Appirio, Inc.
//
// Test Class Name: COMM_OpportunityDocumentUploadExtTest
// For Apex Class: COMM_OpportunityDocumentUploadExtension
// For Apex Page: COMM_OpportunityDocumentUpload
// Description: This apex class is used for COMM services. It has the functionality to attach a document to opportunity document record.
//
// 31st Mrach 2015   Hemendra Singh Bhati   Original (Task # T-487784)
//
@isTest(seeAllData=false)
private class COMM_OpportunityDocumentUploadExtTest {
  /*
  @method      : validateExtensionFunctionality
  @description : This method validates the extension functionality.
  @params      : void
  @returns     : void
  */
	private static testMethod void validateExtensionFunctionality() {
    // Inserting Test Account Record.
    List<Account> theTestAccounts = TestUtils.createAccount(1, true);
    TestUtils.createCommConstantSetting();
    // Inserting Test Opportunity Record.
    List<Opportunity> theTestOpportunities = TestUtils.createOpportunity(1, theTestAccounts.get(0).Id, true);

    // Inserting Test Opportunity Document Record.
    List<Document__c> theTestOpportunityDocument = TestUtils.createOpportunityDocument(1, theTestAccounts.get(0).Id, theTestOpportunities.get(0).Id, true);

    // Setting The Current Page.
    PageReference thePage = Page.COMM_OpportunityDocumentUpload;
    Test.setCurrentPage(thePage);

    // Inserting Test Attachment Record.
    List<Attachment> theTestAttachments = TestUtils.createAttachment(1, theTestOpportunityDocument.get(0).Id, true);

    // Setting Required Page Parameters.
		ApexPages.currentPage().getParameters().put('DocumentId', String.valueOf(theTestAttachments.get(0).Id));

    Test.startTest();

    // Instantiating Opportunity Document Extension.
    COMM_OpportunityDocumentUploadExtension theExtension = new COMM_OpportunityDocumentUploadExtension(new ApexPages.StandardController(
      theTestOpportunityDocument.get(0)
    ));
    theExtension.documentName = 'Test Attachment Document';
    theExtension.documentBody = Blob.valueOf('This is a test attachment body.');
    theExtension.processSelectedDocument();

    Test.stopTest();

    // Validating Test Case Results.
    system.assert(
      [SELECT DocumentId__c FROM Document__c WHERE Id = :theTestOpportunityDocument.get(0).Id].DocumentId__c != null,
      'Error: The opportunity document extension failed to update field API named "DocumentId__c".'
    );
	}
}