//
// (c) 2015 Appirio, Inc.
//
// Apex Test Class Name: NV_OrderHistoryController_Test
// For Apex Class: NV_OrderHistoryController
//
// 07th January 2015   Hemendra Singh Bhati   Original (Task # T-459198)
//
@isTest(seeAllData=false)
private class NV_OrderHistoryController_Test {
  // Private Data Members.
  private static final String SYSTEM_ADMINISTRATOR_PROFILE_NAME = 'System Administrator';
  private static final String TERRITORY_MANAGER_WASHINGTON_DC_ROLE_NAME = 'Territory Manager - WASHINGTON DC';
  private static final String ACCOUNT_NV_CUSTOMER_RECORD_TYPE_LABEL = 'NV Customer';

  /*
  @method      : validateControllerFunctionality
  @description : This method validates controller functionality.
  @params      : void
  @returns     : void
  */
	private static testMethod void validateControllerFunctionality() {
    // Extracting "System Administrator" Profile Id.
    List<Profile> theProfiles = [SELECT Id FROM Profile WHERE Name = :SYSTEM_ADMINISTRATOR_PROFILE_NAME];
    system.assert(
      theProfiles.size() > 0,
      'Error: The requested user profile does not exist.'
    );

    // Extracting "Territory Manager - WASHINGTON DC" Role Id.
    List<UserRole> theUserRoles = [SELECT Id FROM UserRole WHERE Name = :TERRITORY_MANAGER_WASHINGTON_DC_ROLE_NAME];
    system.assert(
      theUserRoles.size() > 0,
      'Error: The requested user role does not exist.'
    );

    // Inserting Test User With "Territory Manager - WASHINGTON DC" Role.
    User theTerritoryManager = new User(
      ProfileId = theProfiles.get(0).Id,
      UserRoleId = theUserRoles.get(0).Id,
	    Alias = 'hsb',
	    Email = 'hsingh@appirio.com',
	    EmailEncodingKey = 'UTF-8',
	    FirstName = 'Hemendra Singh',
	    LastName = 'Bhati',
	    LanguageLocaleKey = 'en_US',
	    LocaleSidKey = 'en_US',
	    TimezoneSidKey = 'Asia/Kolkata',
	    Username = 'tm.hsingh@appirio.com.srtyker',
	    CommunityNickName = 'hsb',
	    IsActive = true
    );
    insert theTerritoryManager;

    // Generating Test Data.
    Account theTestAccount = null;
    Order theTestOrder = null;
    Contract theTestContract = null;
    PricebookEntry theTestPBE = null;
    Id standardPriceBookId = null;
    Product2 theTestProduct = null;
    List<OrderItem> theTestOrderItems = null;
    Id theAccountNVCustomerRecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get(ACCOUNT_NV_CUSTOMER_RECORD_TYPE_LABEL).RecordTypeId;

    // Running Thread As Territory Manager.
    system.runAs(theTerritoryManager) {
      // Inserting Test Account.
      theTestAccount = new Account(
        Name = 'Test Account',
        Purpose__c = 'Main',
        RecordTypeId = theAccountNVCustomerRecordTypeId,
        OwnerId = theTerritoryManager.Id
      );
      insert theTestAccount;

      // Inserting Test Contract.
      theTestContract = new Contract(
        AccountId = theTestAccount.Id,
        Status = 'Draft',
        ContractTerm = 1,
        StartDate = Date.today().addDays(-5),
        External_Integration_Id__c = '123123'
      );
      insert theTestContract;

      // Extrating Standard PriceBook Id.
      standardPriceBookId = Test.getStandardPricebookId();

      // Inserting Test Product.
      theTestProduct = new Product2(
        Name = 'Test Product',
        Catalog_Number__c = '1',
        Product_Division__c = 'NV',
        Life_Cycle_Code__c = 'Test Life Cycle Code',
        IsActive = true
      );
      insert theTestProduct;

      // Inserting Test Pricebook Entry.
      theTestPBE = new PricebookEntry(
        Pricebook2Id = standardPriceBookId,
        Product2Id = theTestProduct.Id,
        UnitPrice = 99.00,
        isActive = true
      );
      insert theTestPBE;

      // Inserting Test Order.
      theTestOrder = new Order(
        AccountId = theTestAccount.Id,
        ContractId = theTestContract.Id,
        Status = 'Draft',
        EffectiveDate = Date.today().addDays(-1),
        Date_Ordered__c = Datetime.now().addDays(-1),
        Pricebook2Id = standardPriceBookId,
        Oracle_Order_Number__c = '1234567890',
        Invoice_Related_Sales_Order__c = '1234567891',
        Customer_PO__c = 'PO0000012',
        On_Hold__c = true,
        Shipping_Method__c = 'FDX-Parcel-Next Day 8AM'
      );
      insert theTestOrder;

      // Inserting Test Order Items.
      theTestOrderItems = new List<OrderItem>();
      for(Integer index = 0;index < 3;index++) {
        theTestOrderItems.add(new OrderItem(
          PriceBookEntryId = theTestPBE.Id,
          OrderId = theTestOrder.Id,
          Quantity = 1,
          UnitPrice = 99,
          Line_Amount__c = 99,
          Tracking_Number__c = '12345',
          Status__c = 'Delivered',
          Shipping_Method__c = 'FDX-Parcel-Next Day 8AM',
          On_Hold__c = true
        ));
      }
      insert theTestOrderItems;
    }

    Test.startTest();

    // Running Thread As Territory Manager.
    system.runAs(theTerritoryManager) {
      // Test Case 1 - Fetching Recent Order Activity.
      List<Account> theAccountData = NV_OrderHistoryController.getRecentOrderActivity();
      system.assert(
        theAccountData.size() > 0,
        'Error: The controller class failed to fetch most recent accounts for which the order was placed.'
      );

      // Test Case 2 - Fetching Filtered Account And Order Data.
      List<List<SObject>> theFilteredData = NV_OrderHistoryController.getFilteredData('Test', '10');
      system.assert(
        theFilteredData.size() > 0,
        'Error: The controller class failed to fetch filtered account and order data.'
      );

      // Updating Test Order.
      theTestOrder.Hold_Type__c = 'Test Case';
      theTestOrder.On_Hold__c = true;
      update theTestOrder;

      // Test Case 3 - Fetching Filtered Order Data - No Search Term Provided.
      List<NV_Wrapper.OrderWrapper> theOrderWrapper = NV_OrderHistoryController.getFilteredOrders(
        theTestAccount.Id,
        theTestProduct.Id,
        2,
        'On Hold;Expedited;Draft',
        'Name',
        null,
        'Test',
        '0'
      );
      system.assert(
        theOrderWrapper.size() > 0,
        'Error: The controller class failed to filter order data.'
      );

      // Test Case 4 - Fetching Filtered Order Data - No Product Id Provided.
      theOrderWrapper = NV_OrderHistoryController.getFilteredOrders(
        theTestAccount.Id,
        null,
        2,
        'On Hold;Expedited;Draft',
        'Name',
        null,
        'Test',
        '0'
      );
      system.assert(
        theOrderWrapper.size() > 0,
        'Error: The controller class failed to filter order data.'
      );

      // Test Case 5 - Fetch Followable Users.
      List<User> theFollowableUsers = NV_OrderHistoryController.getFollowableUsers();
    }

    Test.stopTest();
	}
}