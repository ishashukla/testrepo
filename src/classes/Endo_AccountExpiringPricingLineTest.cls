/*************************************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 19, 2014 
 Purpose       :  Test class for Endo_AccountExpiringPricingLine
**************************************************************************************************/
@isTest(seeAllData = true)
private class Endo_AccountExpiringPricingLineTest {
    static testMethod void testEndo_AccountExpiringPricingLine() {
        // Load the Data
        Account acc = Endo_TestUtils.createAccount(1, true).get(0);
            
        List<Product2> products = Endo_TestUtils.createProduct(2, true);
        
        Pricebook2 pb = Endo_TestUtils.getStandardPricebook();
        
        PricebookEntry standardPrice1 = Endo_TestUtils.getTestPricebookEntry(pb.Id, products.get(0).Id);
        PricebookEntry standardPrice2 = Endo_TestUtils.getTestPricebookEntry(pb.Id, products.get(1).Id);
        
        Pricing_Contract__c pc = Endo_TestUtils.createPC(1, true).get(0);
        
        Pricing_Contract_Account__c pca = Endo_TestUtils.createPricingContractAccount(acc.Id, pc.Id, true);     
        List<Pricing_Contract_Line_Item__c> pclis = Endo_TestUtils.createPCLI(2, false);
        pclis.get(0).Product__c = products.get(0).Id;
        pclis.get(0).Pricing_Contract__c = pc.Id;
        pclis.get(0).Application_Method__c = '%';
        pclis.get(0).Value__c = 20.0;
        
        pclis.get(1).Product__c = products.get(1).Id;
        pclis.get(1).Pricing_Contract__c = pc.Id;
        pclis.get(1).Application_Method__c = 'NEWPRICE';
        pclis.get(1).Value__c = 500.0;
        
        Test.startTest();         
        //inline VF page
        Endo_AccountExpiringPricingLine controller = new Endo_AccountExpiringPricingLine(new ApexPages.StandardController(acc));
        System.assertEquals(false, controller.showPricingLineItemCounter);
        System.assertEquals(false, controller.showPricingLineItems);
        System.assertEquals(0, controller.counter);
        System.assertEquals(null, controller.expiringPricingLineItems);
        
        insert pclis;
        controller = new Endo_AccountExpiringPricingLine(new ApexPages.StandardController(acc));
        System.assertEquals(true, controller.showPricingLineItemCounter);
        System.assertEquals(false, controller.showPricingLineItems);
        System.assertEquals(2, controller.counter);
        System.assertEquals(null, controller.expiringPricingLineItems);
        
        //show exipiring price line items
        ApexPages.currentPage().getParameters().put('showPLI', 'true');
        controller = new Endo_AccountExpiringPricingLine(new ApexPages.StandardController(acc));
        System.assertEquals(false, controller.showPricingLineItemCounter);
        System.assertEquals(true, controller.showPricingLineItems);
        System.assertEquals(false, controller.expiringPricingLineItems.isEmpty());
        System.assertEquals(2, controller.expiringPricingLineItems.size());
        
        //coverage only
        controller.accountFields();
        Test.stopTest();
    }
}