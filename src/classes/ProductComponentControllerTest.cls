/**================================================================      
 * Appirio, Inc
 * Name: ProductComponentControllerTest
 * Description: Test Class for ProductComponentController class.
 * Created Date: [08/04/2016]
 * Created By: [Isha Shukla] (Appirio)
 * Date Modified      Modified By      Description of the update
   07 Oct 2016        Prakarsh Jain    Added test Methods to check the split product functionality  
 ==================================================================*/
@isTest
public class ProductComponentControllerTest {
    Static List<InstalledProduct__c> ip;
    Static List<Install_Base__c> ib;
    Static List<Account> acc;
    Static List<Product2> prod;
    Static List<System__c> sys;
    static List<User> user;
    @isTest static void testProductComponentControllerWithAccId() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            Test.stopTest();
            System.assertEquals(String.valueOf(acc[0].Id),productComponent.recordId);
        }
    }
    @isTest static void testProductComponentControllerWithBaseIdwithSystemNull() {
        createData();
        System.runAs(user[0]) {
            ib[0].Type__c = 'Stryker';
            ib[0].System__c =Null;
            insert ib;
            ip[0].Install_Base__c = ib[0].Id;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(ib[0].Id);
            productComponent.getrecordId();
            productComponent.asset = ib[0];
            productComponent.changeInput();
            productComponent.addProductSF1();
            Test.stopTest();
            System.assertEquals(String.valueOf(ib[0].Id),productComponent.recordId);
        }
    }
    @isTest static void testProductComponentControllerWithBaseIdwithSystemNull1() {
        createData();
        System.runAs(user[0]) {
            ib[0].Type__c = 'Stryker';
            ib[0].System__c =Null;
            insert ib;
            ip[0].Install_Base__c = ib[0].Id;
            ip[0].Custom_Name__c = 'TestProduct';
            insert ip;
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.dupQuantity = '3';
            productComponent.setrecordId(ib[0].Id);
            productComponent.getrecordId();
            productComponent.asset = ib[0];
            productComponent.changeInput();
            productComponent.createNewProduct();
            productComponent.showPopup();
            productComponent.showIntermediatePopup();
            productComponent.saveAndContinue();
            productComponent.createNewProduct();
            productComponent.save();
            Test.stopTest();
            System.assertEquals(String.valueOf(ib[0].Id),productComponent.recordId);
        }
    }
    @isTest static void testProductComponentControllerWithBaseIdwithSystemNotNull() {
        createData();
        System.runAs(user[0]) {
            ib[0].Type__c ='Stryker';
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.dupQuantity = '3';
            productComponent.setrecordId(ib[0].Id);
            productComponent.getrecordId();
            productComponent.asset = ib[0];
            productComponent.changeInput();
            productComponent.installedProductList = ip;
            productComponent.refreshPage();
            productComponent.showIntermediatePopup();
            productComponent.cancelPopUp();
            Test.stopTest();
            System.assertEquals(String.valueOf(ib[0].Id),productComponent.recordId);
        }
    }
    //Method to check duplicate Products are added
    @isTest static void testCreateNewProduct() {
        createData();
        System.runAs(user[0]) {
            ib[0].Type__c = 'Stryker';
            ib[0].System__c =sys[0].Id;
            insert ib;
            ip[0].Install_Base__c = ib[0].Id;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            ip[0].Enable_Duplicate_Product__c = true;
            ip[0].Duplicate_Quantity__c = 5;
            update ip;
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.dupQuantity = '3';
            productComponent.setrecordId(ib[0].Id);
            productComponent.getrecordId();
            productComponent.asset = ib[0];
            productComponent.selectedProduct = prod[1].Id;
            productComponent.asset.System__c = sys[0].Id;
            productComponent.createNewProduct();
            productComponent.addProduct();
            productComponent.sltProducts = '1';
            productComponent.keyVal = 1;
            productComponent.keyValDel = 1;
            productComponent.showIntermediatePopup();
            productComponent.showPopup();
            productComponent.saveAndContinue();
            productComponent.addProductSF1();
            productComponent.save();
            Test.stopTest();
            System.assertEquals(2,productComponent.mapIntegerToInstallProduct.size());
        }
    }
    //Method to check cancel pop up
    @isTest static void testCancelPopup() {
        createData();
        System.runAs(user[0]) {
            ib[0].Type__c = 'Stryker';
            ib[0].System__c =sys[0].Id;
            insert ib;
            ip[0].Install_Base__c = ib[0].Id;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            ip[0].Enable_Duplicate_Product__c = true;
            ip[0].Duplicate_Quantity__c = 5;
            update ip;
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.dupQuantity = '3';
            productComponent.setrecordId(ib[0].Id);
            productComponent.getrecordId();
            productComponent.asset = ib[0];
            productComponent.selectedProduct = prod[1].Id;
            productComponent.asset.System__c = sys[0].Id;
            productComponent.createNewProduct();
            productComponent.addProduct();
            productComponent.sltProducts = '1';
            productComponent.keyVal = 1;
            productComponent.keyValDel = 1;
            productComponent.showIntermediatePopup();
            productComponent.showPopup();
            productComponent.saveAndContinue();
            productComponent.addProductSF1();
            productComponent.save();
            Test.stopTest();
            System.assertEquals(2,productComponent.mapIntegerToInstallProduct.size());
        }
    }
    //Method to check installed products are deleted
    @isTest static void testCreateNewProductDeleteProduct() {
        createData();
        System.runAs(user[0]) {
            ib[0].Type__c = 'Stryker';
            ib[0].System__c =sys[0].Id;
            insert ib;
            ip[0].Install_Base__c = ib[0].Id;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            ip[0].Enable_Duplicate_Product__c = true;
            update ip;
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.dupQuantity = '3';
            productComponent.setrecordId(ib[0].Id);
            productComponent.getrecordId();
            productComponent.asset = ib[0];
            productComponent.selectedProduct = prod[1].Id;
            productComponent.asset.System__c = sys[0].Id;
            productComponent.createNewProduct();
            productComponent.addProduct();
            productComponent.sltProducts = '1';
            productComponent.keyVal = 1;
            productComponent.keyValDel = 1;
            productComponent.showIntermediatePopup();
            productComponent.showPopup();
            productComponent.cancelPopup();
            productComponent.showPopup();
            productComponent.saveAndContinue();
            productComponent.save();
            productComponent.deleteProduct();
            productComponent.save();
            Test.stopTest();
            System.assertEquals(2,productComponent.mapIntegerToInstallProduct.size());
        }
    }
    //Method to check installed products are deleted
    @isTest static void testCreateNewProductDelProduct() {
        createData();
        System.runAs(user[0]) {
            ib[0].Type__c = 'Stryker';
            ib[0].System__c =sys[0].Id;
            insert ib;
            ip[0].Install_Base__c = ib[0].Id;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            ip[0].Enable_Duplicate_Product__c = true;
            update ip;
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.dupQuantity = '3';
            productComponent.setrecordId(ib[0].Id);
            productComponent.getrecordId();
            productComponent.asset = ib[0];
            productComponent.selectedProduct = prod[1].Id;
            productComponent.asset.System__c = sys[0].Id;
            productComponent.createNewProduct();
            productComponent.addProduct();
            productComponent.sltProducts = '1';
            productComponent.keyVal = 1;
            productComponent.keyValDel = 1;
            productComponent.showIntermediatePopup();
            productComponent.showPopup();
            productComponent.saveAndContinue();
            productComponent.save();
            productComponent.delProduct();
            productComponent.save();
            Test.stopTest();
            System.assertEquals(2,productComponent.mapIntegerToInstallProduct.size());
        }
    }
    @isTest static void testAddProductsSF1(){
      createData();
      System.runAs(user[0]) {
        Test.startTest();
          ProductComponentController productComponent = new ProductComponentController();
          productComponent.addProductSF1();
        Test.stopTest();
      }
    }
    @isTest static void testSavewithSystemNotNull() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            productComponent.dupQuantity = '3';
            productComponent.asset.System__c = sys[0].Id;
            productComponent.installedProductList = ip;
            productComponent.delList = ip;
            productComponent.addProduct();
            productComponent.showIntermediatePopup();
            productComponent.showPopup();
            productComponent.save();
            Test.stopTest();
            System.assertEquals(True, productComponent.asset != Null);
        }
    }

    @isTest static void testSavewithSystemNull() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            ib[0].Type__c = 'Stryker';
            ib[0].System__c = Null;
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            productComponent.dupQuantity = '3';
            productComponent.asset.System__c = Null;
            productComponent.installedProductList = ip;
            productComponent.addProduct();
            productComponent.showIntermediatePopup();
            productComponent.showPopup();
            productComponent.saveAndContinue();
            productComponent.createNewProduct();
            productComponent.save();
            Test.stopTest();
            System.assertEquals(True, productComponent.asset != Null);
        }
    }
    @isTest static void testCancel() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            productComponent.dupQuantity = '3';
            productComponent.cancel();
            Test.stopTest();
            System.assertEquals(True, productComponent.asset != Null);
        }
    }
    @isTest static void testRefreshProductList() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            productComponent.dupQuantity = '3';
            productComponent.refreshProdList();
            Test.stopTest();
            System.assertEquals(True, productComponent.asset != Null);
        }
    }
    @isTest static void testAddProductwithSystemNotNull() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            Id recordId = productComponent.getrecordId();
            productComponent.asset.System__c = sys[0].Id;
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            productComponent.dupQuantity = '3';
            productComponent.addProduct();
            Test.stopTest();
            System.assertEquals(productComponent.asset.System__c == sys[0].Id, True);
        }
    }
    @isTest static void testAddProductwithSystemNull() {
        createData();
        System.runAs(user[0]) {
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            productComponent.asset.System__c = Null;
            productComponent.addProduct();
        }
    }
    @isTest static void testGetSelectedRecords() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            insert ib;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            productComponent.selectedId = sys[0].Id; 
            productComponent.getSelectedRecord();
            productComponent.dupQuantity = '3';
            Test.stopTest();
            System.assertEquals(True,productComponent.selectedId == sys[0].Id) ;
        }
    }
    @isTest static void testDeleteProductWhenDeleteTrue() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            insert ib;
            ip[0].Delete__c = True;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            productComponent.installedProductList = ip;
            productComponent.deleteProduct();
            productComponent.changeType();
            productComponent.dupQuantity = '3';
            productComponent.validateValues();
            Test.stopTest();
            System.assertEquals(True,  productComponent.installedProductList != Null);
        }
    }
    @isTest static void testDeleteProductWhenDeleteFalse() {
        createData();
        System.runAs(user[0]) {
            Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            ProductComponentController productComponent = new ProductComponentController();
            productComponent.setrecordId(acc[0].Id);
            productComponent.getrecordId();
            insert ib;
            ip[0].Delete__c = False;
            ip[0].Custom_Name__c = 'TestProduct12';
            insert ip;
            productComponent.installedProductList = ip;
            productComponent.deleteProduct();
            productComponent.asset.Type__c = 'Stryker';
            productComponent.changeType();
            productComponent.validateValues();
            productComponent.dupQuantity = '3';
            Test.stopTest();
            System.assertEquals(True,  productComponent.asset != Null);
        }
    }
    public static List<System__c> createSystem(Integer sysCount,String sysName ,String businessunit ,Boolean isInsert) {
        List<System__c> sysList = new List<System__c>();
        
        for(Integer i = 0; i < sysCount; i++) {
            System__c sysRecord = new System__c(Name = sysName , Business_Unit__c = businessunit);
            sysList.add(sysRecord);
        }
        if(isInsert) {
            insert sysList;
        } 
        return sysList;
    }
    public static List<Install_Base__c> createInstallBase(Integer ibCount,Id accId , Id userId, Id sysId,Boolean isInsert) {
        List<Install_Base__c> ibList = new List<Install_Base__c>();
        
        for(Integer i = 0; i < ibCount; i++) {
            Install_Base__c ibRecord = new Install_Base__c(Account__c = accId , System__c = sysId , ownerId = userId);
            ibList.add(ibRecord);
        }
        if(isInsert) {
            insert ibList;
        } 
        return ibList;
    }
    public static void createData() {
        user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        System.runAs(user[0]) {
            acc = TestUtils.createAccount(1, True);
            sys = createSystem(1,'System1' ,'NSE',True);
            ib = createInstallBase(2,acc[0].Id, user[0].Id, sys[0].Id,false);
            prod = TestUtils.createProduct(2,True);
            ip = TestUtils.createInstalledProduct(1,prod[0].Id, false);
        }
    }
}