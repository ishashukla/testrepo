// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_RenewWarrantiesController - Landing page to create opportunity
//
// 08/09/2016, Gaurav Sinha      (T-5331862) Create landing page from Report to create Opportunities
public with sharing class COMM_RenewWarrantiesController {
    public static final String CONST_PriceBOOK = 'COMM Price Book';
    PUBLIC static final String CONST_OpportunityName_suffix = ' Warranty Renewal';
    public static final String const_initial_opp_Status = 'Initial Meeting';
    public static final String CONST_RT_Name = 'COMM_Service_Contract_Opportunity';
    public String gv_accountid{
        get{
            return apexpages.currentPage().getParameters().get('id');
        }
        private set;
    }
    
    public COMM_RenewWarrantiesController(){
        retrieveData();
    }
    
    public list<productWrapper> gv_list_final {get;set;}
    // @method :retrieveData
    // @Description : method used to retrieve the data from the warrant based on the accoutn id 
    //                and the expiration day within 180 days
    // @authod : Gaurav Sinha
    public void retrieveData(){
        productWrapper lv_Productwrapper  = new productWrapper();
        gv_list_final =  new list<productWrapper>();
        Date lv_Date = System.today() + 180;
        for(SVMXC__Warranty__c varloop:[SELECT id ,name,SVMXC__Installed_Product__r.SVMXC__Product__c,
                                        	SVMXC__Installed_Product__r.SVMXC__Product__r.name,
                                        	SVMXC__Installed_Product__c,SVMXC__Installed_Product__r.name,
                                        	SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c,
                                        	SVMXC__start_Date__c,SVMXC__End_Date__c
                                        FROM SVMXC__Warranty__c
                                        WHERE SVMXC__Installed_Product__r.SVMXC__Company__c =: gv_accountid
                                            AND SVMXC__End_Date__c >= today
                                            AND SVMXC__End_Date__c <=: lv_Date])      
        { 
            lv_Productwrapper.selected=false;
            lv_Productwrapper.objWarranty = varloop;
            gv_list_final.add(lv_Productwrapper);
            lv_Productwrapper  = new productWrapper();
        }
    }
    // @method :CreateOpportuntiy
    // @Description : Method used to create the opportuntiy with selected account and Selected product
    // @authod : Gaurav Sinha
    public pageReference CreateOpportuntiy(){
        Pricebook2 lv_pb;
        Opportunity opp;
        Savepoint sp = Database.setSavepoint();
        try{
            Account lv_account = [select id,name from account where id=: gv_accountid];
            lv_pb = [select id,Name  from Pricebook2  where name =: CONST_PriceBOOK ];
            RecordType rt = [select id from Recordtype where developername = : CONST_RT_Name];
            opp = new Opportunity(
                name = lv_account.name +CONST_OpportunityName_suffix, 
                closedate = system.today() + 30,
                Pricebook2id = lv_pb.id,
                Stagename=const_initial_opp_Status,
                stage__c = 'Prospecting',
                recordtypeid= rt.id
            );
            
            set<id> lv_set = new Set<id>();
            
            for(ProductWrapper varloop:gv_list_final){
                if(!varloop.selected) continue;
                lv_set.add(varloop.objWarranty.SVMXC__Installed_Product__r.SVMXC__Product__c);
            }
            if(lv_set.size() == 0){
                return null;
            }
            insert opp; 
            Map<String,PricebookEntry> mapProductPriceBook = new Map<String,PricebookEntry>();
            String str = [select pricebook2id from opportunity where id = :opp.id][0].pricebook2id;
            for(PricebookEntry record:[SELECT id,unitprice,product2id,product2.name 
                                       FROM pricebookentry 
                                       WHERE product2.id in :lv_set
                                       AND pricebook2id = :str]){
                mapProductPriceBook.put(record.product2id, record);
            }
            if(mapProductPriceBook.size() == 0){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'You do not have access to create Opportunity for this product. KIndly contact your admin'+str);
            	ApexPages.addMessage(myMsg);
                return null;
            }
            List<OpportunityLineItem> lv_List_oppLineItem = new List<OpportunityLineItem>();
            OpportunityLineItem lv_oplineitem;
            for(ProductWrapper varloop:gv_list_final){
                if(!varloop.selected) continue;
                
                lv_oplineitem = new OpportunityLineItem();
                lv_oplineitem.opportunityid  = opp.id;
                lv_oplineitem.quantity = 1;
                
                if( mapProductPriceBook.get(varloop.objWarranty.SVMXC__Installed_Product__r.SVMXC__Product__c) !=null){
                    lv_oplineitem.pricebookentryid = mapProductPriceBook.get(varloop.objWarranty.SVMXC__Installed_Product__r.SVMXC__Product__c).id;
                    lv_oplineitem.unitprice = mapProductPriceBook.get(varloop.objWarranty.SVMXC__Installed_Product__r.SVMXC__Product__c).unitprice;
                    lv_List_oppLineItem.add(lv_oplineitem);
                }
            }
            
            insert lv_List_oppLineItem;
            return new PageReference('/'+opp.id+'/e');
        }
        catch(Exception e){
            database.rollback(sp);
            String errormessage = lv_pb==null ? ('Price Book '+ CONST_PriceBOOK +' Not found') :  e.getMessage();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,errormessage);
            ApexPages.addMessage(myMsg);
            return null;
        }
        return null;
    }
    
    public class ProductWrapper{
        public SVMXC__Warranty__c objWarranty{Get;set;}
        public boolean selected{Get;set;}
        public ProductWrapper(){}
    }
}