/*******************************************************************************
 Author        :  Appirio JDC (Pitamber Sharma)
 Date          :  Jan 27, 2014
 Purpose       :  Test class for trigger Endo_OrderTriggerHandler
 
 Modified Date          Modified By             Purpose
 05-09-2016             Chandra Shekhar         To Fix Test Class
 06-10-2016             Parul Gupta   T-542805 (Endo is using Stardard Order now, so commenting whole code)
*******************************************************************************/

@isTest
private class Endo_OrderTriggerHandlerTest {

    /*static testMethod void myUnitTest() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        
        Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
        insert con;
        
        
        Case cs = new Case(Subject = 'Test Subject', Oracle_Order_Ref__c = '1234');
        cs.contactId = con.Id;
        
        //Modified By Chandra Shekhar Sharma to add record types on case Object
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support');
        TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Tech Support', 'Endo');
        
        cs.recordTypeId = rtByName.getRecordTypeId();
        insert cs;
        
        Test.startTest();
        
        Order__c ordr = new Order__c(Name='1234', Account__c = acc.Id, Oracle_Order_Number__c = '1234');
        insert ordr;
        Test.stopTest();
        
        Case c = [Select Id, Current_Case_Order__c from Case Where Id =:cs.Id];
        
        System.assertEquals(c.Current_Case_Order__c, ordr.Id);
                
    }*/
}