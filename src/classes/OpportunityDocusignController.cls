/*
Name            OpportunityDocusignController 
Created Date    06/23/2016
Created By      Rahul Aeran
Purpose         T-509669, controller for the page for adding Quick Actions in LEX for Docusign
*/
public class OpportunityDocusignController {
  public String opptyId;
  public OpportunityDocusignController(ApexPages.StandardController std){
    opptyId = std.getRecord().id; 

  }
  public PageReference redirectToStandardDocusignPage(){
    String returnUrl = DocusignUtility.getStandardDocusignRedirectUrl(opptyId);
    return new PageReference(returnUrl);
  }
}