// (c) 2016 Appirio, Inc.
//
// Description - Trigger Handler for COMM_AttachmentTriggerHandler 
//
// 02/26/2016   Nitish Bansal Reference : T-459651
// 03/29/2016   Deepti Maheshwari Reference : T-485167
// 03/31/2016   Deepti Maheshwari Updated class name (Prefixed with COMM_)
// 04/19/2016   Deepti Maheshwari changes for T-485167
// 06/07/2016   Deepti Maheshwari (Issue Fix I-216824)
public class COMM_AttachmentTriggerHandler {

    // Methods to be executed after record is inserted
    // @param newRecords - List to hold Trigger.new 
    // @return Nothing
    public static void afterRecordInsert(List<Attachment> newRecords){
      //Added New method for task T-485167
    deleteAttachmentsFromQuotes(newRecords);
    updateParentDocument(newRecords);       
    }

    // Methods to be executed before record is Deleted
    // @param oldRecords - List to hold Trigger.old 
    // @return Nothing
    public static void beforeRecordDelete(List<Attachment> oldRecords){
      preventRecordDelete(oldRecords);
    }
  
  // Methods to be executed after a record is inserted to delete attachments 
  // on quotes and create on opportunity documents (Ref : T-485167)
  // @param newRecords - List to hold Trigger.new 
  // @return Nothing
  private static void deleteAttachmentsFromQuotes(List<Attachment> newRecords) {
    Map<Id,List<Attachment>> attachmentsMapByQuote = new Map<Id,List<Attachment>>();
    List<Attachment> attachmentsToInsert = new List<Attachment>(); 
    List<Document__c> quoteDocs = new List<Document__c>();  
    Map<String, COMM_Prefix_Doc_Type_Mapping__c> commPrefixDocTypeSetting = 
                                      COMM_Prefix_Doc_Type_Mapping__c.getAll();
    Set<ID> attachmentIDs = new Set<ID>(); 
    for(Attachment att : newRecords){
      if(att.ParentID.getSobjectType().getDescribe().getName() == Constants.QUOTE_OBJECT_NAME) {
        if(attachmentsMapByQuote.containsKey(att.ParentID)) {
          attachmentsMapByQuote.get(att.ParentID).add(att);
        }else {
          attachmentsMapByQuote.put(att.ParentID,new List<Attachment>{att}); 
        }        
        Document__c quoteDoc = new Document__c();
        quoteDoc.RecordTypeID = Schema.SObjectType.Document__c.getRecordTypeInfosByName().
                                get(Constants.OPPTY_DOCUMENT_RT).getRecordTypeID();
        if(att.Name.contains('_') 
          && commPrefixDocTypeSetting.containsKey(att.Name.split('_').get(0))) {
          quoteDoc.Document_type__c = 
            commPrefixDocTypeSetting.get(att.Name.split('_').get(0)).Document_Type_Value__c;
        }
        quoteDoc.Oracle_Quote__c = att.ParentID;   
        system.debug('Setting name here!!!');
        quoteDoc.Name = att.Name;     
        quoteDocs.add(quoteDoc);        
      }
    }
    
    if(quoteDocs.size() > 0) {
      //This code was responsible to set opportunity id whenever a new quote attachment 
      //was created. This requirement was changed hence commenting the code
      // Ref : I-216824
      
      /*Map<ID, BigMachines__Quote__c> quoteMap = new Map<ID, BigMachines__Quote__c>
                                               ([SELECT BigMachines__Opportunity__c, ID,BigMachines__Is_Primary__c
                                               FROM BigMachines__Quote__c
                                               WHERE ID in :attachmentsMapByQuote.keyset()]);
      for(Document__c doc : quoteDocs){
        if(quoteMap.containsKey(doc.Oracle_Quote__c) && quoteMap.get(doc.Oracle_Quote__c).BigMachines__Is_Primary__c)
        doc.Opportunity__c = quoteMap.get(doc.Oracle_Quote__c).BigMachines__Opportunity__c;
      }*/
      insert quoteDocs;
      for(Document__c doc : quoteDocs) {
        if(attachmentsMapByQuote.containsKey(doc.Oracle_Quote__c)) {
            for(Attachment attachment : attachmentsMapByQuote.get(doc.Oracle_Quote__c)) {
              attachmentsToInsert.add(new Attachment(ParentId = doc.ID, Description = attachment.Description,
              body = attachment.body, contentType = attachment.contentType, Name = attachment.Name));
              attachmentIDs.add(attachment.id);
            } 
        }
      }
      if(attachmentsToInsert.size() > 0) {
        insert attachmentsToInsert;
      }
      if(attachmentIDs.size() > 0) {
        List<Attachment> attList = [SELECT Id 
                                   FROM Attachment 
                                   WHERE Id in :attachmentIDs];
        delete attList;
      }
    }
  }
  
    // Prevents a user from deleting combined proposal if it doesn't falls in RolesCustomSetting__c custom setting
    // @param oldRecords - List to hold Trigger.old 
    // @return Nothing
    private static void preventRecordDelete(List<Attachment> oldRecords){
        //Delete allowed if user role is in RolesCustomSetting__c 
        Boolean deleteAllowed = CombinedProposalUtil.isAllowedDeleteCP();
        if(!deleteAllowed){
            Set<Id> parentOppDocumentsSet = new Set<Id>();
            for(Attachment att : oldRecords){
                if(att.ParentId != null){
                    // Checking parent object type is Opportunity Document
                    if(att.ParentId.getSobjectType().getDescribe().getName() == 'Document__c'){ 
                        //Adding all Opportunity Document Id's to a set
                        parentOppDocumentsSet.add(att.ParentId); 
                    }
                }
            }

            if(parentOppDocumentsSet.size() > 0){
                //Quering Opportunity Document for the Id's present in the above created set 'parentOppDocumentsSet' for which delete is restricted
                Map<Id, Document__c> parentOppDocsMap = new Map<Id, Document__c> ([Select Id, Document_type__c From Document__c 
                                                        Where Document_type__c = :Constants.RESTRICTED_DOCUMENT_TYPE
                                                              And Id In :parentOppDocumentsSet]);
                //Checking if we have any restricted document deletion is being processed via attachment delete
                if(parentOppDocsMap.size() > 0){
                    for(Attachment att : oldRecords){
                        if(att.ParentId != null){
                            // Checking parent object type is Opportunity Document
                            if(att.ParentId.getSobjectType().getDescribe().getName() == 'Document__c'){ 
                                //Letting the user know that the specific attachment can't be deleted.
                                if(parentOppDocsMap.containsKey(att.ParentId)){
                                    att.addError(System.Label.Please_contact_an_Administrator_to_delete_Combined_Proposals);
                                }
                            }
                        }
                    }
                }
            }
        }
  }

    // Assigns an attechment ID to Document object's documentId if Parent is Opportunity Document of type 'Combined Proposal'
    // @param newRecords - List to hold Trigger.new 
    // @return Nothing
    private static void updateParentDocument(List<Attachment> newRecords){
        Map<Id, Attachment> parentOppDocToAttachmentMap = new Map<Id, Attachment>();
        for(Attachment att : newRecords){
            if(att.ParentId != null){
                // Checking parent object type is Opportunity Document
                if(att.ParentId.getSobjectType().getDescribe().getName() == 'Document__c'){ 
                    //Adding all Opportunity Document Id's to a set
                    parentOppDocToAttachmentMap.put(att.ParentId, att); 
                }
            }
        }
        List<Document__c> updateParentOppDocList = new List<Document__c>();
        //T-485167- Kirti - Removed condition Document_type__c = :Constants.RESTRICTED_DOCUMENT_TYPE
        for(Document__c oppDoc : [Select Id, Document_type__c, DocumentId__c, Name From Document__c 
                                    Where Id In :parentOppDocToAttachmentMap.keyset()]){
                                      system.debug('Diocument : ' + oppDoc);
            if(parentOppDocToAttachmentMap.containsKey(oppDoc.Id)){
                oppDoc.DocumentId__c = parentOppDocToAttachmentMap.get(oppDoc.Id).Id;
                // Ref T-485167 - Populate doc name only if doc name is blank
                if(oppDoc.Name ==  null){
                  oppDoc.Name = parentOppDocToAttachmentMap.get(oppDoc.Id).Name;
                }
                updateParentOppDocList.add(oppDoc);
            }           
        }

        if(updateParentOppDocList.size() > 0){
            update updateParentOppDocList;
        }
    }
}