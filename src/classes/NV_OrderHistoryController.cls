public with sharing class NV_OrderHistoryController {
    //Date -> 
    public static Integer recordCount = 20;

    @AuraEnabled
    public static List<Account> getRecentOrderActivity(){
        List<Account> accountData = new List<Account>();
        Set<Id> accountIds = new Set<Id>();
        List<AggregateResult> aggResult = [SELECT AccountId, Max(Date_Ordered__c) MaxDateOrdered 
                                            FROM Order
                                            WHERE Date_Ordered__c != null AND AccountId != null
                                            GROUP BY AccountId
                                            ORDER BY Max(Date_Ordered__c) DESC
                                            LIMIT :recordCount];
        for(AggregateResult record:aggResult){
            accountIds.add((Id)record.get('AccountId'));
        }
        system.debug('TRACE: NV_OrderHistoryController - getRecentOrderActivity - accountIds - ' + accountIds);

        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Name, AccountNumber, ShippingCity, ShippingState, Owner.Name 
                                                                FROM Account WHERE Id IN :accountIds ORDER BY CreatedDate DESC]);
    system.debug('TRACE: NV_OrderHistoryController - getRecentOrderActivity - accountMap - ' + accountMap);

        for(AggregateResult record:aggResult){
            Id accountId = (Id)record.get('AccountId');
            if(accountMap.containsKey(accountId)){
                accountData.add(accountMap.get(accountId));     
            }
        }
        system.debug('TRACE: NV_OrderHistoryController - getRecentOrderActivity - accountData - ' + accountData);
        return accountData;
    }   


    @AuraEnabled
    public static List<List<SObject>> getFilteredData(String searchString, String offSetPage){
        if(offSetPage == null) offSetPage = '0';
        Integer page = Integer.valueOf(offSetPage);
        Integer offSet = page * recordCount; 
        searchString = '%'+searchString+'%';
        List<Account> accountData = new List<Account>();    
        accountData = [SELECT Id, Name, AccountNumber, ShippingCity, ShippingState, Nickname__c, Owner.Name 
                        FROM Account
                        WHERE RecordType.DeveloperName = 'NV_Customer'
                        AND Purpose__c = 'Main'
                        AND ((Name LIKE :searchString) OR (Nickname__c LIKE :searchString) OR (AccountNumber LIKE :searchString))
                        ORDER BY Name
                        LIMIT :recordCount
                        OFFSET :offSet];
        System.debug('Account Filter Size: '+ accountData.size());              
        
        List<Product2> productData = new List<Product2>();
        productData = [SELECT Id, Name, ProductCode, Part_Number__c, Segment__c, Description__c, Long_Description__c,
                        ODP_Category__c, Catalog_Number__c, GTIN__c, IsActive, Life_Cycle_Code__c
                        FROM Product2
                        WHERE Product_Division__c = 'NV'
                        AND IsActive = true
                        AND Life_Cycle_Code__c != ''
                        AND ((Name LIKE :searchString) OR (ODP_Category__c LIKE :searchString) OR (Part_Number__c LIKE :searchString) OR (Catalog_Number__c LIKE :searchString)
                            OR (Description__c LIKE :searchString) OR (GTIN__c LIKE :searchString) OR (Segment__c LIKE :searchString))
                        ORDER BY Name
                        LIMIT :recordCount
                        OFFSET :offSet];
        System.debug('Product Filter Size: '+ productData.size());
        
        List<List<SObject>> filteredData = new List<List<SObject>>();
        filteredData.add(accountData);
        filteredData.add(productData);
        return filteredData;

        /*
        //Account Fields:
        //Name, Nickname__c, Oracle_Account_Number__c
        //Product Fields
        //ODP_Category__c, Part_Number__c, Catalog_Number__c, 
        //Description__c, GTIN__c, Segment__c
        searchString = '*'+searchString+'*';
        List<List< SObject>> searchResults = [FIND :searchString
                                                RETURNING Account(Id, Name, Nickname__c
                                                                    ORDER BY Name 
                                                                    LIMIT :recordCount 
                                                                    OFFSET :offSet),
                                                        Product2(Id, Name, Part_Number__c
                                                                    ORDER BY Name 
                                                                    LIMIT :recordCount 
                                                                    OFFSET :offSet)];

        return accountData;
        */
    }

    @AuraEnabled
    public static List<NV_Wrapper.OrderWrapper> getFilteredOrders(Id accountId, Id productId, Integer dateRange,
                                                    String statusFilterString, String orderByString, String ownerIds,
                                                    String searchTerm, String offSetPage){
        
        if(searchTerm != null) searchTerm = '%'+searchTerm+'%';
        if(offSetPage == null) offSetPage = '0';
        Integer offSet = Integer.valueof(offSetPage) * recordCount; 
        
        Set<String> expeditedShippingMethods = new Set<String>{'FDX-Parcel-Next Day 8AM','FDX-Parcel-P1 Overnight 10.30', 'FDX-Parcel-Saturday/8AM'};
        //Added for Story S-389737
        String BACKORDERED    = 'Backordered';        
                                                        
                                                        
        List<Order> orderData = new List<Order>();

        Set<String> statusToFilter = new Set<String>();
        List<String> whereClause = new List<String>();
        Set<Id> orderIdToConsider = null;
        Set<Id> ownerIdsSet = new Set<Id>();
        //Added PartialBackOrder__c field in Query for Story S-389737
        String query = 'SELECT Id, OrderNumber, AccountId, Account.Name, Account.Owner.Name, Date_Ordered__c, Type, EffectiveDate, Customer_PO__c, Order_Total__c, On_Hold__c, PartialBackOrder__c, Status FROM Order';

         if(accountId != null){ 
            whereClause.add(' AccountId =: accountId');
        } else if(searchTerm != null){
            //BJ - Missed a requirement. Adding Search Term Functionality.
            System.debug('Search Term: ' + searchTerm);
            whereClause.add(' ((Account.Name LIKE :searchTerm) OR (Account.Nickname__c LIKE :searchTerm) OR (Account.AccountNumber LIKE :searchTerm))');
        }
                                        
        if(dateRange != null) whereClause.add(' Date_Ordered__c = LAST_N_DAYS:'+dateRange);

        if(ownerIds == null){
            ownerIdsSet.add(UserInfo.getUserId());
        }
        else{
            for(String oId : ownerIds.split(';')){
                ownerIdsSet.add(Id.valueOf(oId));
            }
        }
        whereClause.add(' Account.OwnerId in: ownerIdsSet');
        System.debug('TRACE - OwnerIdsSet:');
       System.debug(ownerIdsSet);
        if(!String.isBlank(statusFilterString)){
            for(String statusString : statusFilterString.split(';')){
                if(statusString == 'On Hold'){
                    whereClause.add( ' On_Hold__c = true');
                }
                else if(statusString == 'Expedited'){
                    whereClause.add( ' Shipping_Method__c in: expeditedShippingMethods');
                }
                //Start - Added for Story S-389737
                else if(statusString == BACKORDERED){
                    whereClause.add( ' PartialBackOrder__c =:'+BACKORDERED+'');
                }
                //End - Story S-389737
                else{
                    statusToFilter.add(statusString);
                }
            }

            if(statusToFilter.size()>0){
                whereClause.add( ' Status in: statusToFilter');
            }       
        }

        if(productId != null){
            System.debug('TRACE: ProductId - ' + productId);
            orderIdToConsider = new Set<Id>();
            String orderItemQuery = 'SELECT OrderId FROM OrderItem WHERE Pricebookentry.Product2Id =:productId ';
            //if(accountId != null) orderItemQuery += ' AND Order.AccountId =:accountId'; //Current implementation makes this impossible. - BJ
            orderItemQuery += ' GROUP BY OrderId';
            List<AggregateResult> aggregateList = Database.query(orderItemQuery);
            for(AggregateResult ar : aggregateList){
                orderIdToConsider.add((Id)ar.get('OrderId'));
            }
            if(orderIdToConsider.size()>0){
                whereClause.add( ' Id in: orderIdToConsider');
            } else{
                return new List<NV_Wrapper.OrderWrapper>();
            }
        } else if(searchTerm != null){
            orderIdToConsider = new Set<Id>();
            String orderItemQuery = 'SELECT OrderId FROM OrderItem WHERE ((Pricebookentry.Product2.Name LIKE :searchTerm) OR (Pricebookentry.Product2.ODP_Category__c LIKE :searchTerm) OR (Pricebookentry.Product2.Part_Number__c LIKE :searchTerm) OR (Pricebookentry.Product2.Catalog_Number__c LIKE :searchTerm)OR (Pricebookentry.Product2.Description__c LIKE :searchTerm) OR (Pricebookentry.Product2.GTIN__c LIKE :searchTerm) OR (Pricebookentry.Product2.Segment__c LIKE :searchTerm))';
            if(accountId != null) orderItemQuery += ' AND Order.AccountId =:accountId';
            orderItemQuery += ' GROUP BY OrderId';
            List<AggregateResult> aggregateList = Database.query(orderItemQuery);
            for(AggregateResult ar : aggregateList){
                orderIdToConsider.add((Id)ar.get('OrderId'));
            }
            if(orderIdToConsider.size()>0){
                whereClause.add( ' Id in: orderIdToConsider');
            }
        }

        if(whereClause.size()>0){
            String whereClauseString = String.join(whereClause, ' AND');
            query += ' WHERE ' + whereClauseString;
        }

        if(!String.isBlank(orderByString)) query += ' ORDER BY ' + orderByString;
        query += ' LIMIT ' + recordCount;
        query += ' OFFSET ' + offSetPage;
        System.debug('TRACE: Query - ' + query);
        System.debug('TRACE: OrderIdsToConsider - ');
        System.debug(orderIdToConsider);
        orderData = Database.query(query);
        system.debug('Order Filter Size: '+ orderData.size());

        List<NV_Wrapper.OrderWrapper> owData = new List<NV_Wrapper.OrderWrapper>();
        if(orderData.size() > 0){
            for(Order record: orderData) {
                owData.add(new NV_Wrapper.OrderWrapper(record));
            }
        }
        return owData;
    }


    @AuraEnabled
    public static List<User> getFollowableUsers(){
        List<User> followableUsers = new List<User>();
        Set<Id> ownersToConsider = new Set<Id>();
        User currentUser = [SELECT Id, Name, UserRole.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

        /*if(currentUser!= null && currentUser.UserRole != null && currentUser.UserRole.Name != null && 
            (currentUser.UserRole.Name.contains('ATM') || currentUser.UserRole.Name.contains('Regional Manager'))){
            for(NV_Follow__c record: [SELECT Record_Id__c FROM NV_Follow__c 
                                            WHERE Following_User__c =: UserInfo.getUserId()
                                            AND Record_Type__c = 'User']){
                ownersToConsider.add(Id.valueOf(record.Record_Id__c));
            }
        }*/ 
        List<User> subordinates = NV_Utility.getSubordinateUsersOfSelectedRole(currentUser.Id, currentUser.UserRoleId, '%Territory Manager%');
        if(subordinates.size() > 0){
            subordinates.add(currentUser);
        }
        return subordinates;
    }




}