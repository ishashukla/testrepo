// (c) 2015 Appirio, Inc.
//
// Class Name: SalesForecastTest 
// Description: Test Class for SalesForecast class.
// 
// April 20 2016, Isha Shukla  Original 
//* 22 Nov   2016      Nitish Bansal     I-244792 - Empower prod sync
//
@isTest
private class SalesForecastTest {
    static List<User> testUsers;
    static List<Opportunity> oppList;
    static Forecast_Year__c fyear1;
    static Forecast_Year__c fyear2;
    static Monthly_Forecast__c monthlyf;
    @isTest static void testSf() {
        Profile profile = TestUtils.fetchProfile('System Administrator');
        testUsers = TestUtils.createUser(2, profile.Name, false);
        testUsers[0].Division = 'IVS';
        testUsers[1].Division = 'IVS';
        insert testUsers;
        Test.startTest();
        System.runAs(testUsers.get(0)) {
            createData();
            oppList[0].Business_Unit__c = 'IVS';
            oppList[0].Opportunity_Number__c = '50001';
            oppList[0].OwnerId = testUsers[0].Id;
            oppList[0].CloseDate = date.parse('1/1/2017');
            oppList[0].ForecastCategoryName='Pipeline';
            oppList[0].StageName = 'Prospecting';
            oppList[0].Amount= 1200;
            insert oppList;
            PageReference pageRef = Page.SalesForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('managerView','true');
            System.currentPageReference().getParameters().put('BU','IVS');
            System.currentPageReference().getParameters().put('id',String.valueOf(testUsers[0].Id));
            SalesForecastCtrl controller = new SalesForecastCtrl();
            controller.nfy = fyear1;
            controller.oppMonth = 'January';
            controller.rowId=String.valueOf(monthlyf.Id);
            System.assertEquals('2016',controller.nfy.Year__c);
            controller.getOpportunityData('2017');
            controller.doSort = True;
            controller.oppMonth = 'May';
            controller.getOpportunityList();
            controller.getOpportunityListUpside();
            controller.getInTheWorksOpp();
            controller.getInTheOmittedOpp();
            controller.getOpportunityListwithBaseTrailRecords();
            controller.ForecastYr = '2017';
            controller.updateMonthlyQuota();
            controller.createNewForecastYear();
            controller.save();
            controller.submitRow();
            controller.resubmit();
            String getProfile = controller.UserProf;
        }
        Test.stopTest();
        System.assertEquals(true,oppList != null);
    }
    @isTest static void testSfWithoutId() {
        Profile profile = TestUtils.fetchProfile('System Administrator');
        testUsers = TestUtils.createUser(2, profile.Name, false);
        testUsers[0].Division = 'IVS';
        testUsers[1].Division = 'IVS';
        insert testUsers;
        Test.startTest();
        System.runAs(testUsers.get(0)) {
            createData();
            oppList[0].Business_Unit__c = 'IVS';
            oppList[0].Opportunity_Number__c = '50001';
            oppList[0].OwnerId = testUsers[0].Id;
            oppList[0].CloseDate = date.parse('2/1/2016');
            oppList[0].ForecastCategoryName='Omitted';
            oppList[0].StageName = 'Prospecting';
            oppList[0].Amount= 1200;
            insert oppList;
            PageReference pageRef = Page.SalesForecast;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('managerView','true');
            System.currentPageReference().getParameters().put('BU','IVS');
            System.currentPageReference().getParameters().put('id',String.valueOf(testUsers[1].Id));
            SalesForecastCtrl controller = new SalesForecastCtrl();  
            controller.setYear();
            controller.save();
            controller.cancelNew();
            controller.first();
            Integer pageNumber = controller.pageNumber;
            controller.last();
            Boolean hasPrevious = controller.hasPrevious;
            controller.previous();
            Boolean hasNext = controller.hasNext;
            controller.next();
            controller.forecastYR = '2016';
            controller.refreshPage();
            controller.sortData();
            controller.getOpportunityListUpside();
            controller.sortData();
            controller.redirectPage();
            controller.switchViewRefresh();
            controller.backToASR();
            
        }
        Test.stopTest();
        System.assertEquals(true, oppList != Null);
        
    }
    
    public static void createData() {
        Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
        Id recordTypeRep = rtByNameRep.getRecordTypeId();
        fyear1 = new Forecast_Year__c(Year__c='2016',Yearly_Quota__c=12000,Division__c='Instrument',RecordTypeId = recordTypeRep,User2__c=testUsers[0].Id,Business_Unit__c='IVS',Manager__c=testUsers[1].Id,Yearly_Base__c = 1200);
        fyear1.Year_External_Id__c= '12345';
        insert fyear1;
        fyear2 = new Forecast_Year__c(Year__c='2016',Yearly_Quota__c=12000,Division__c='Instrument',RecordTypeId = recordTypeRep,User2__c=testUsers[0].Id,Business_Unit__c='IVS',Manager__c=testUsers[1].Id,Yearly_Base__c = 1200);
        fyear2.Year_External_Id__c= '1234';
        insert fyear2;
        monthlyf = new Monthly_Forecast__c(Month__c= 'January',Forecast_Year__c = fyear1.Id,Bill_Only_Forecast__c=-12000 );
        insert monthlyf;
        List<Account> accountList = TestUtils.createAccount(1, True);
        TestUtils.createCommConstantSetting();
        oppList = TestUtils.createOpportunity(1,accountList[0].Id, false);    
    }
}