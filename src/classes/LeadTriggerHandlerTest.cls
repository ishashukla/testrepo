// (c) 2015 Appirio, Inc.
//
// Class Name: LeadTriggerHandlerTest
// Description: Test Class for LeadTriggerHandler    
// April 12, 2016    Meena Shringi    Original
@isTest 
public class LeadTriggerHandlerTest {
    @isTest
    static void testRepRecordSubmittedview()
    {   Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Instruments Lead Record Type');
           Id recordTypelead = rtByNameRep.getRecordTypeId();
        Lead ld = TestUtils.createLead(1,recordTypelead , true).get(0);
     try {
         delete ld;
         
     }   
     catch(Exception ex) {
         System.assert(true,'Lead must not be deleted');
     }
    }
}