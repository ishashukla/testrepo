@isTest

private class BigMachinesEditQuoteExt_Test{
    static testMethod void testGetEditURL() {
    
        // Create an opportunity record.
        List<Pricebook2> priceBookList = TestUtils.createpricebook(2,' ',false);
        priceBookList.get(0).Name = 'COMM Price Book';
        insert priceBookList;
        Opportunity opty = new Opportunity();
        opty.Name = 'BigMachines test Opportunity for testGetEditURL';
        opty.StageName = 'Prospecting';
        opty.CloseDate = Date.today();
        insert opty;
        
        // Create Oracle Quote record.
        List<BigMachines__Configuration_Record__c> bigMachinesConfigList = TestUtils.createBigMachineConfig(2,true);
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'BigMachines test quote for testGetEditURL';
        quote.BigMachines__Opportunity__c = opty.id;
        quote.BigMachines__Transaction_Id__c = 'BMI123';
        quote.BigMachines__Site__c = bigMachinesConfigList.get(0).Id;
        insert quote;
        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(quote);
        BigMachinesEditQuoteExtension controller = new BigMachinesEditQuoteExtension(stdCtrl);
        controller = new BigMachinesEditQuoteExtension(stdCtrl, true);
        controller = new BigMachinesEditQuoteExtension(stdCtrl, false);
        String testEditURL = controller.getEditURL();
        System.assertNotEquals(testEditURL, '');
    }
}