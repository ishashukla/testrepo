/*================================================================      
 * Appirio, Inc
 * Name: Instruments_AccountTriggerHandler
 * Description: Handler Class for AccountTrigger
 * Created Date: March 23 2016
 * Created By: Prakarsh Jain  Original (T-486819)
 * 
 * Date Modified      Modified By                   Description of the update
 * March 24 2016      Tom Muse                      Removed commented out code and system debugs. Reviewed the rest of the approach and prepped it for handoff to Norman.
 * May 05,2016        Sahil Batra                   Added method populateGPONames to populate GPO Name field
 * August 24,2016     Nitish Bansal                 To assign Flex rep to account team for COMM RT and make sure multiple users for same state can be added (I-231769)
 * Sept 23, 2016      Nitish Bansal                 PR-006129 - Used new address feilds for COMM records.
==================================================================*/
public class Instruments_AccountTriggerHandler {
    
    
  public static void populateGPONames(List<Account> newList){
    for(Account acc : newList){
            acc.Instruments_GPO_Names__c = acc.GPO_Names__c;
    }
  } 
  //Method to add AccountTeamMember when Account is inserted Billing state/Shipping State is inserted/updated
  public static void insertFlexRepToAccountTeam(Map<Id, Account> oldMap, List<Account> newList){
    List<AccountTeamMember> lstToAddAccountTeamMember = new List<AccountTeamMember>();
    List<Flex_Rep_and_States__c> flexRepsList = Flex_Rep_and_States__c.getAll().values();
    Map<Id, String> mapIdToStringAccount = new Map<Id, String>();
    
    Map<String, List<Id>> mapStringToIdFlexReps = new Map<String, List<Id>>(); //NB - 08/24 - I-231769
    Map<Id, List<Id>> mapAccIdToUserId = new Map<Id, List<Id>>(); //NB - 08/24 - I-231769
    
    List<Account> lstAccounts = new List<Account>();
    Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
    Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId(); 
    
    //NB - 08/24 - I-231769 - Start - COMM RT inclusion
    String COMM_INTL_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.COMM_Intl_Accounts_RT_Label).getRecordTypeId();   
    String COMM_ENT_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.COMM_Intl_Entity_RT_Label).getRecordTypeId();   
    String COMM_ENDO_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Endo_COMM_Account_RT_Label).getRecordTypeId();   
    for(Account acc : newList){
      //NB - 09/23 - PR-006129 - Start
      if(oldMap == null ||(oldMap.get(acc.Id).BillingStateCode != acc.BillingStateCode) || (oldMap.get(acc.Id).ShippingStateCode != acc.ShippingStateCode)) {
          if(acc.RecordTypeId == recordTypeInstrument) {
              lstAccounts.add(acc);
          }
      }
      if(oldMap == null ||(oldMap.get(acc.Id).COMM_Billing_State_Province__c != acc.COMM_Billing_State_Province__c) || (oldMap.get(acc.Id).COMM_Shipping_State_Province__c != acc.COMM_Shipping_State_Province__c)) {
          if(acc.RecordTypeId == COMM_INTL_RT_ID || acc.RecordTypeId == COMM_ENT_RT_ID || acc.RecordTypeId == COMM_ENDO_RT_ID) {
              lstAccounts.add(acc);
          }
      }
      //NB - 09/23 - PR-006129 - End
    }
    //NB - 08/24 - I-231769 - End
    
    for(Account acc : lstAccounts){
      //NB - 09/23 - PR-006129 - Start
      if(acc.RecordTypeId == recordTypeInstrument) {
        if(!mapIdToStringAccount.containsKey(acc.Id) && acc.BillingState!=null && acc.ShippingState!=null){
          mapIdToStringAccount.put(acc.Id, acc.BillingState);
        }
        else if(!mapIdToStringAccount.containsKey(acc.Id) && acc.ShippingState!=null && acc.BillingState == null){
          mapIdToStringAccount.put(acc.Id, acc.ShippingState);
        }
        else if(!mapIdToStringAccount.containsKey(acc.Id) && acc.BillingState!=null && acc.ShippingState==null){
          mapIdToStringAccount.put(acc.Id, acc.BillingState);
        }
      } else if(acc.RecordTypeId == COMM_INTL_RT_ID || acc.RecordTypeId == COMM_ENT_RT_ID || acc.RecordTypeId == COMM_ENDO_RT_ID) {   
          if(!mapIdToStringAccount.containsKey(acc.Id) && acc.COMM_Billing_State_Province__c!=null && acc.COMM_Shipping_State_Province__c!=null){
            mapIdToStringAccount.put(acc.Id, acc.COMM_Billing_State_Province__c);
          }
          else if(!mapIdToStringAccount.containsKey(acc.Id) && acc.COMM_Shipping_State_Province__c!=null && acc.COMM_Billing_State_Province__c == null){
            mapIdToStringAccount.put(acc.Id, acc.COMM_Shipping_State_Province__c);
          }
          else if(!mapIdToStringAccount.containsKey(acc.Id) && acc.COMM_Billing_State_Province__c!=null && acc.COMM_Shipping_State_Province__c==null){
            mapIdToStringAccount.put(acc.Id, acc.COMM_Billing_State_Province__c);
          }
      } 
      //NB - 09/23 - PR-006129 - End   
    }
    
    //NB - 08/24 - I-231769 - Start - To make sure multiple users with same state can be added to account team
    List<Id> tempUserIds;
    for(Flex_Rep_and_States__c flexReps : flexRepsList){
      tempUserIds = new List<Id>();  
      if(mapStringToIdFlexReps.containsKey(flexReps.State__c)){
        tempUserIds.addAll(mapStringToIdFlexReps.get(flexReps.State__c));
      } 
      tempUserIds.add(flexReps.Name);   
      mapStringToIdFlexReps.put(flexReps.State__c, tempUserIds);
    }
    for(Account acc : lstAccounts){
      if(mapIdToStringAccount.containsKey(acc.Id)){
        for(String state : mapStringToIdFlexReps.keySet()){
          if(state.contains(mapIdToStringAccount.get(acc.Id))){
            mapAccIdToUserId.put(acc.Id,mapStringToIdFlexReps.get(state));
          }
        }  
      }
    }
    for(Account acc : lstAccounts){
      if(mapAccIdToUserId.containsKey(acc.Id)){
        for(Id userId : mapAccIdToUserId.get(acc.Id)){
          AccountTeamMember accTeamMember = new AccountTeamMember();
          accTeamMember.UserId = userId;
          accTeamMember.AccountId = acc.Id;
          accTeamMember.TeamMemberRole = Label.Flex_Financial;
          lstToAddAccountTeamMember.add(accTeamMember);
        }      
      }
    }
    //NB - 08/24 - I-231769 end
    
    if(lstToAddAccountTeamMember.size()>0){
    //NB - 08/24 - I-231769 Start - replacing insert via database.insert since inactive users are not filtered and can't be processed.
      try {
          Database.SaveResult[] results = Database.insert(lstToAddAccountTeamMember,false);
          if(results != null) {
            Integer index = 0;
            for(Database.SaveResult result : results) {
              if(!result.isSuccess()) {
                system.debug('RECORD : ' + lstToAddAccountTeamMember.get(index) + 'ERROR::' + result.getErrors());
              }
              index++;
            }
          }
        }catch(Exception e) {
            System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
        }  
    //NB - 08/24 - I-231769 end
    }
  }
    public static void shareInstallBaseRecords(List<Account> newList ,Map<Id,Account> oldMap) {
        Set<Id> accountId = new Set<Id>();
        Map<Id,Id> accToOwnerMap = new Map<Id,Id>();
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
        Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        for(Account accNew : newList) {
            if(oldMap != Null && accNew.OwnerId != oldMap.get(accNew.Id).OwnerId && accNew.RecordTypeId == recordTypeInstrument) {
                accountId.add(accNew.Id);
                accToOwnerMap.put(accNew.Id,accNew.OwnerId);
            }
        }
        List<Install_Base__c> installBase = new List<Install_Base__c>([SELECT Id,Account__c FROM Install_Base__c WHERE Account__c IN :accountId]);
        List<String> newRecord = new List<String>();
        List<Install_Base__Share> installBaseShareList = new List<Install_Base__Share>();
        for(Install_Base__c ibList : installBase) {
            if(accToOwnerMap.containsKey(ibList.Account__c)) {
                Install_Base__Share ibShare = new Install_Base__Share();
                ibShare.UserOrGroupId = accToOwnerMap.get(ibList.Account__c);
                ibShare.ParentId = ibList.Id;
                ibShare.AccessLevel = 'Read';
                ibShare.RowCause = Schema.AccountShare.RowCause.Manual;
                installBaseShareList.add(ibShare);
            }
        }
        insert installBaseShareList;
        List<Install_Base__Share> installBaseDeleteList = new List<Install_Base__Share>();
        Map<Id,Id> ibToAccOwnerMap = new Map<Id,Id>();
        for(Install_Base__c ibList : installBase) {
            if(oldMap.containsKey(ibList.Account__c)) {
                ibToAccOwnerMap.put(ibList.Id,oldMap.get(ibList.Account__c).OwnerId);
            }
        }
        installBaseDeleteList =[SELECT Id,ParentId,UserOrGroupId,RowCause 
                                    FROM Install_Base__Share 
                                    WHERE ParentId IN :ibToAccOwnerMap.keySet() 
                                    AND UserOrGroupId IN : ibToAccOwnerMap.values()
                                    AND RowCause =:Schema.AccountShare.RowCause.Manual];
        if(installBaseDeleteList.size() > 0) {
            delete installBaseDeleteList;
        } 
    }
}