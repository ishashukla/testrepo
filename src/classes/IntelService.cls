/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 07, 2013
 Purpose       :  REST Webservice for reading data from Intel records.
 Reference     :  Task T-205140
*******************************************************************************/
@RestResource(urlMapping='/IntelService/*') 
global with sharing class IntelService {
  
  //****************************************************************************
  // Get Method for REST service
  //****************************************************************************  
	@HttpGet 
	global static IntelListWrapper doGet() {
	try {
	    RestRequest req = RestContext.request;      
	    IntelListWrapper intelListWrapper = new IntelListWrapper(req);              
	    return intelListWrapper;
	  }
	  catch(Exception ex) {
	    return new IntelListWrapper();
	  }
	  return null;
	}
  
}