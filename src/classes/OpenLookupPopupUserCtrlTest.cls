/*
//
// (c) 2015 Appirio, Inc.
//
// Test class for OpenLookupPopupUserCtrl created for MEDICAL
//
//
// Created by : Surabhi Sharma (Appirio)
//
*/
@isTest
public class OpenLookupPopupUserCtrlTest {
	private static List<Account> accounts;
  private static List<Contact> contacts;
	static testMethod void testOpenLookupPopupUserCtrl(){
		createTestData();
    Account acc = accounts.get(0);
    Contact con = contacts.get(0);
    System.currentPageReference().getParameters().put('Object', 'User');
    System.currentPageReference().getParameters().put('fieldName', 'Name');
    System.currentPageReference().getParameters().put('searchText', 'testFirstName');
    System.currentPageReference().getParameters().put('fieldSetName', 'user_search_result');
    System.currentPageReference().getParameters().put('targetObject', 'User');  	
    OpenLookupPopupUserCtrl olpop = new OpenLookupPopupUserCtrl();
    olpop.selectedObject = 'User';
    Event e = Medical_TestUtils.createEvent(1,acc.Id,true).get(0);
	    
	}
	
	static testMethod void testOpenLookupPopupUserCtrl1(){
		createTestData();
    Account acc = accounts.get(0);
  	Contact con = contacts.get(0);
    System.currentPageReference().getParameters().put('Object', 'Group');
    OpenLookupPopupUserCtrl olpop = new OpenLookupPopupUserCtrl();
    olpop.selectedObject = 'Group';
    Event e = Medical_TestUtils.createEvent(1,acc.Id,true).get(0);
	}
	
	private static void createTestData(){
    accounts = Medical_TestUtils.createAccount(4, true);
    contacts = Medical_TestUtils.createContact(2, accounts.get(0).id , true);
    
  }
}