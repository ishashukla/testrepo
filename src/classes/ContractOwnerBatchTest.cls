@isTest
public class ContractOwnerBatchTest {
	static Contract con;
	static Account acc;
	static list<Contract> listCon;
	static list<AccountTeamMember> listaccTeam;
	static list<Contract_Batch_Role__c> listContractRole; 
	static testMethod void testContractOwnerBatch() {
		createTestData();
		test.startTest();
		Contract_Batch_Role__c cbc = new Contract_Batch_Role__c();
		cbc.Name = 'IVS';
		insert cbc;
		listContractRole = new list<Contract_Batch_Role__c>();
		listContractRole.add(cbc);
		Profile pr = TestUtils.fetchProfile('System Administrator');
		User u = new User();
		u.ProfileId = pr.Id;
		u.lastname = 'Integration User';
		u.alias = 'usr';
		u.email = 'user' + '@test.com';
		u.emailencodingkey = 'UTF-8';
		u.languagelocalekey = 'en_US';
		u.localesidkey = 'en_US';
		u.timezonesidkey = 'America/Los_Angeles';
		u.username = 'kajuuu@test.com'; 
		insert u;
		//TestUtils.createUser(1,'System Administrator',true).get(0);
		AccountTeamMember atm = new AccountTeamMember();
		atm.AccountId = acc.Id;
		atm.UserId = u.Id;
		atm.TeamMemberRole = 'IVS';
		insert atm;
		listaccTeam = new list<AccountTeamMember>();
		listaccTeam.add(atm);
		ContractOwnerBatch cob = new ContractOwnerBatch();
		listCon = new list<Contract>();
		listCon.add(con);
		cob.start(null);
		cob.execute(null,listCon);
		cob.finish(null);
		test.stopTest();
		
	}
	public static void createTestData(){
		acc = TestUtils.createAccount(1, true).get(0);
		Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Instruments');
		Id recordTypeInstrument = rtByName.getRecordTypeId();
		con = TestUtils.createContracts(1,acc.Id,'Service Contract',recordTypeInstrument,true).get(0);
	}
}