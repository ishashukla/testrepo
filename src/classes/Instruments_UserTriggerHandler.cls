/*************************************************************************************************
Created By:     Sahil Batra
Date:           Mar 17, 2016
Description:    Handler class for User Trigger to Add user to Flex Chatter Group
Sahil Batra     Mar 17,2016 Original (T-486050)
Prakarsh Jain   Aug 26,2016 Modified (I-227715)
**************************************************************************************************/
public class Instruments_UserTriggerHandler {
    public static void addToFlexChatterGroup(List<User> newList, Map<Id, User> oldMap){
        Set<String> profileNames = new Set<String>();
        Id chatterGroupId = null;
        for(FlexChatterProfilesUserGroup__c chatterProfile : [SELECT Id,Name FROM FlexChatterProfilesUserGroup__c]){
            profileNames.add(chatterProfile.Name);
        }
        System.debug('profileNames\\\\\\\\'+profileNames);
        Set<Id> profileIdSet = new Set<Id>();
        for(Profile profile : [SELECT Name, Id FROM Profile WHERE Name IN:profileNames]){
            profileIdSet.add(profile.Id);
        }
        List<CollaborationGroup> groupList = [SELECT Id, Name From CollaborationGroup WHERE Name = 'Flex Chatter Group' LIMIT 1];
        
        if(groupList.size() > 0){
            chatterGroupId = groupList.get(0).Id;
        } 
        List<CollaborationGroupMember> memberListtoInsert = new List<CollaborationGroupMember>();
        Set<Id> userIdsToDelete = new Set<Id>();
        Set<Id> userIdsToInsert = new Set<Id>();
        for(User user : newList){
            if(oldMap == null || (user.profileId != oldMap.get(user.Id).profileId)){
                if(oldMap == null && profileIdSet.contains(user.profileId) && chatterGroupId!=null){
                    userIdsToInsert.add(user.Id);
                }
            }
            if(oldMap != null && user.profileId != null){
                if(user.profileId != oldMap.get(user.Id).profileId){
                    if(profileIdSet.contains(user.profileId) && !profileIdSet.contains(oldMap.get(user.Id).profileId)){
                        userIdsToInsert.add(user.Id);
                    } else if ((!profileIdSet.contains(user.profileId) && profileIdSet.contains(oldMap.get(user.Id).profileId))){
                        userIdsToDelete.add(user.id);
                    }
                }
            }
        }
        if(!Test.isRunningTest()) {
         updateGroupMembers(userIdsToInsert,userIdsToDelete,chatterGroupId);
        }
    }
    
    @future
    public static void updateGroupMembers(Set<Id> userIdsToInsert,Set<Id> userIdsToDelete,Id chatterGroupId){
        List<CollaborationGroupMember> insertList = new List<CollaborationGroupMember>();
        List<CollaborationGroupMember> deleteList = new List<CollaborationGroupMember>();
        if(userIdsToInsert.size() > 0){
            for(Id user : userIdsToInsert){
                CollaborationGroupMember member = new CollaborationGroupMember();
                member.MemberId = user;
                member.CollaborationGroupId = chatterGroupId;
                member.CollaborationRole = 'Standard';
                insertList.add(member);
            }
        }
        if(userIdsToDelete.size() > 0){
            deleteList = [SELECT Id FROM CollaborationGroupMember WHERE MemberId IN:userIdsToDelete AND CollaborationGroupId =:chatterGroupId];
        }
        if(deleteList.size() > 0){
             if(!Test.isRunningTest()){
                delete deleteList;
             }
        }
        if(insertList.size() > 0){
            if(!Test.isRunningTest()){
                insert insertList;
            }
        }
        
    }
    
    //Method to restrict duplication of Division value for a user
    //Prakarsh Jain    29 March 2016     (I-208788)
    public static void restrictDuplicateDivision(List<User> newList, Map<Id, User> oldMap){
      List<User> insertUsers = new List<User>();
      List<String> lstDivisions = new List<String>();
      Set<Id> profileIdsSet = new Set<Id>();
      List<Profile> profileIdsList = [SELECT Id, Name FROM Profile WHERE Name =: Label.Instruments_Sales_Rep OR Name =: Label.Sales_Manager_Profile];
      if(profileIdsList.size()>0){
        for(Profile pro :profileIdsList){
          profileIdsSet.add(pro.Id);
        }
      }
      for(User user : newList){
      if(oldMap == null || (user.Division != oldMap.get(user.Id).Division)){
        insertUsers.add(user);
      }
    }
    for(User usr : insertUsers){
      List<String> lstDiv = new List<String>();
      Set<String> setDiv = new Set<String>();
      if(usr.Division != null && usr.Division != ''){
        lstDiv.addAll(usr.Division.split(';'));
        setDiv.addAll(usr.Division.split(';'));
      }
      if(lstDiv.size()!=setDiv.size() && (usr.Division != null || usr.Division != '') && profileIdsSet.contains(usr.ProfileId)){
        usr.addError(Label.Duplicate_Division_Error_Message);
      }
    }
    }        
}