/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_CreateOrderExtensions
//
// 27 Nov 2015 , Prakarsh (Appirio JDC)
//
// Order creation custom page controller
// 14 Jan, 2016, Sunil implement JDE web service
*/
public with sharing class Medical_CreateOrderExtensions {
  // Public variables used on VF page.
  public Order currentOrder{get;set;}
  public Boolean showJDENumberScreen{get;set;}
  public String responseJDEOrderNumber{get;set;}
  public String isOrderSavedSuccess{get;set;}
  public String billToAccount_Name{get;set;}
  public String billToAccount_Id{get;set;}
  public String shipToAccount_Name{get;set;}
  public String shipToAccount_Id{get;set;}
  public String selectedAccId {get;set;}
  public String fieldName {get;set;}


  // Private Variables.
  private Contact relatedContact;
  public String recordTypeName{get;set;}
  private User currentUserInfo;
  private String billingAccountNumber;
  private String shippingAccountNumber;
  private String caseId;
  private Id recordTypeId;


  //-----------------------------------------------------------------------------------------------
  //  Cosntructor
  //-----------------------------------------------------------------------------------------------
  public Medical_CreateOrderExtensions(ApexPages.StandardController stdController){
    System.debug('@@@constructro');
    currentOrder = (Order)stdController.getRecord();

    isOrderSavedSuccess = 'no';

    responseJDEOrderNumber = '';
    showJDENumberScreen = false;


    // Fill Record Type Map for Order Object
    Map<String, String> mapOrderRecordTypes = new Map<String, String>();
    for(RecordType rt :[SELECT Id, Name, DeveloperName FROM RecordType WHERE sObjectType = 'Order']){
      mapOrderRecordTypes.put(rt.Id, rt.DeveloperName);
    }
    recordTypeId = Id.valueOf(ApexPages.currentPage().getParameters().get('RecordType'));
    recordTypeName =  mapOrderRecordTypes.get(recordTypeId);

    currentUserInfo = [SELECT Id, QB_User_Id__c FROM USER WHERE Id = :UserInfo.getUserId()].get(0);
    if(validateCurrentUser() == false){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your user profile is not associated with a QuoteBuilder user ID, until this is corrected, you will not be able to create orders within QuoteBuilder.'));
      return;
    }

    String accId = ApexPages.currentPage().getParameters().get('accid');
    String contactId = ApexPages.currentPage().getParameters().get('contact_id');
    caseId = ApexPages.currentPage().getParameters().get('case_id');

    if(String.isBlank(ApexPages.currentPage().getParameters().get('BillTo')) == false){
      billToAccount_Name =  ApexPages.currentPage().getParameters().get('BillToName');
      billToAccount_Id =  ApexPages.currentPage().getParameters().get('BillTo');
    }

    if(String.isBlank(ApexPages.currentPage().getParameters().get('ShipTo')) == false){
      shipToAccount_Name = ApexPages.currentPage().getParameters().get('ShipToName');
      shipToAccount_Id = ApexPages.currentPage().getParameters().get('ShipTo');
    }

    if(String.isBlank(contactId) == false){
      relatedContact = [SELECT ID, AccountId, Phone, FirstName, LastName FROM Contact WHERE ID =: contactId LIMIT 1];
    }

    initCurrentOrder();



  }

  //-----------------------------------------------------------------------------------------------
  //  Hepler method to initialize current Order
  //-----------------------------------------------------------------------------------------------
  private void initCurrentOrder(){
    if(relatedContact != null){
      currentOrder.Contact__c = relatedContact.Id;
    }

    if(String.isBlank(caseId) == false){
      currentOrder.Case__c = caseId;
    }

    currentOrder.Contact_PhoneNumber__c = relatedContact.Phone; //Added by Gagan
    currentOrder.RecordTypeId = recordTypeId;

    currentOrder.EffectiveDate = System.Today();
    currentOrder.Status = 'Draft';

  }



  //-----------------------------------------------------------------------------------------------
  //  Hepler method to validate current user
  //-----------------------------------------------------------------------------------------------
  public Boolean validateCurrentUser(){
    if(String.isBlank(currentUserInfo.QB_User_Id__c) == true && recordTypeName == 'Quote_Builder_Order'){
      return false;
    }
    return true;
  }



  //-----------------------------------------------------------------------------------------------
  //  Cancel action method called from visual force
  //-----------------------------------------------------------------------------------------------
  public PageReference cancel(){
    PageReference pg;
    if(relatedContact.Id != null){
        pg = new PageReference('/' + relatedContact.Id);
    }
    else{
      pg = new PageReference('/');
    }
    pg.setRedirect(true);
    return pg;

  }

  //-----------------------------------------------------------------------------------------------
  //  Save Order record
  //-----------------------------------------------------------------------------------------------
  public PageReference saveOrder(){
    try{

      // If User remove the Account names from text box then update Id field to null
      if(String.isBlank(billToAccount_Name) == true){
        billToAccount_Name = billToAccount_Id = null;
      }
      if(String.isBlank(shipToAccount_Name) == true){
        shipToAccount_Name = shipToAccount_Id = null;
      }

      // Validate if Account name is not selected from Lookup field (User just typed in text box without selecting from lookup)
      if(String.isBlank(billToAccount_Name) != true && String.isBlank(billToAccount_Id) == true){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Bill To Account from lookup window.');
        ApexPages.addMessage(myMsg);
        return null;
      }
      if(String.isBlank(shipToAccount_Name) != true && String.isBlank(shipToAccount_Id) == true){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Ship To Account from lookup window.');
        ApexPages.addMessage(myMsg);
        return null;
      }


      // Make sure Shipping Account is populated.
      if(String.isBlank(shipToAccount_Id) == true){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Ship To Account before create this Order.'));
        return null;
      }

      // Make sure Billing Account is populated.
      if(String.isBlank(billToAccount_Id) == true){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Bill To Account before create this Order.'));
        return null;
      }

      if(validateCurrentUser() == false){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your user profile is not associated with a QuoteBuilder user ID, until this is corrected, you will not be able to create orders within QuoteBuilder.'));
        return null;
      }

      Account accBill = [SELECT Id, Name, AccountNumber FROM Account WHERE Id =:billToAccount_Id LIMIT 1];

      Account accShip = [SELECT Id, Name, AccountNumber FROM Account WHERE Id =:shipToAccount_Id LIMIT 1];

      currentOrder.Bill_To_Account__c = accBill.Id;
      currentOrder.Ship_To_Account__c = accShip.Id;
      shippingAccountNumber = accShip.AccountNumber;
      billingAccountNumber = accBill.AccountNumber;

      currentOrder.AccountId = accBill.Id;
      System.debug('@@@' + currentOrder);
      insert currentOrder;
      System.debug('@@@###' + currentOrder.Id);
      isOrderSavedSuccess = 'yes';
    }
    catch (Exception ex) {
      isOrderSavedSuccess = 'no';
      // do not show complete error message in case phone number validation failed.
      if(ex.getMessage().contains('(999) 999-9999')){
        return null;
      }
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
    }
    return null;
  }


  //-----------------------------------------------------------------------------------------------
  //  Call webservices
  //-----------------------------------------------------------------------------------------------
  public PageReference callExternalServices(){
    System.debug('@@@' + currentOrder.Id);

    // JDE
    if(recordTypeName == 'JDE_Order'){
      String callServiceResult = callJDEWebService();

      if(callServiceResult == 'success'){
        showJDENumberScreen = true;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Web service callout success, your JDE Order Number is : ' + responseJDEOrderNumber));
        // Update current Order.
        currentOrder.JDE_Order_Number__c = responseJDEOrderNumber;
        currentOrder.Oracle_Order_Number__c = responseJDEOrderNumber + '.' + convertOrderType();

        update currentOrder;
        PageReference pg = new PageReference('/' + currentOrder.Id);
        pg.setRedirect(true);
        return pg;

      }
      // in case of failing web service delete the newly created Order record and reset.
      else{
      //S-415473 - Praful Gupta - code update starts
        String originalorder1 = currentOrder.Original_Order_Number__c;
        String poNumbr1 = currentOrder.PoNumber;
        String orderOrigin1 = currentOrder.Order_Origin__c;   
     //S-415473 - Praful Gupta - code update Ends       
        delete currentOrder;
        DataBase.emptyRecycleBin(currentOrder);

        currentOrder = new Order();
      // S-415473 - Praful Gupta - code update starts  
        currentOrder.Original_Order_Number__c = originalorder1;
        currentOrder.PoNumber = poNumbr1;
        currentOrder.Order_Origin__c = orderOrigin1;
     // S-415473 - Praful Gupta - code update Ends    
        
        initCurrentOrder();
      }
    }

    // QB
    else if(recordTypeName == 'Quote_Builder_Order'){
      // Call QB Web Service
      PageReference pgQBcallout = callQBWebService();

      // IF QB call out is failed.
      if(pgQBcallout == null){
        // in case of failing web service delete the newly created Order record and reset.
         // S-415473 - Praful Gupta - code update starts  
        String originalorder = currentOrder.Original_Order_Number__c;
        String poNumbr = currentOrder.PoNumber;
        String orderOrigin = currentOrder.Order_Origin__c;
         // S-415473 - Praful Gupta - code update Ends  
        delete currentOrder;
        DataBase.emptyRecycleBin(currentOrder);

        currentOrder = new Order();
         // S-415473 - Praful Gupta - code update starts  
        currentOrder.Original_Order_Number__c = originalorder;
        currentOrder.PoNumber = poNumbr;
        currentOrder.Order_Origin__c = orderOrigin;
         // S-415473 - Praful Gupta - code update Ends  
        initCurrentOrder();
      }
      else {
        return pgQBcallout;
      }
    }
    return null;
  }

  //-----------------------------------------------------------------------------------------------
  //  Helper method to call QB Web service.
  //-----------------------------------------------------------------------------------------------
  private PageReference callQBWebService(){
    HttpRequest req = new HttpRequest();
    HttpResponse res = new HttpResponse();
    Http http = new Http();
    //String webServiceURL = 'https://modelndev.stryker.com/SSOSFDC-QB_webservice/qbservice.svc/REST/QBService?UserId=pathik_csr&UniqueReqId=123456&SFDCOrderId=303&SecretKey=qwer&ShipToNumber=2133123&BillToNumber=213123&CustomerPO=dkrPO&ContactName=devendra&ContactPhoneNumber=2191234567';
    String secretKey = Medical_QB_Settings__c.getInstance().Secret_Key__c;
    String endpointURL = Medical_QB_Settings__c.getInstance().Endpoint__c;
    String webServiceURL = endpointURL;

    // Phone number validation expected format by QB is: 999999-9999 while user input format is: (999) 999-9999
    String qbAllowedPhoneNumber = currentOrder.Contact_PhoneNumber__c;
    qbAllowedPhoneNumber = qbAllowedPhoneNumber.replace('(', '');
    qbAllowedPhoneNumber = qbAllowedPhoneNumber.replace(')', '');
    qbAllowedPhoneNumber = qbAllowedPhoneNumber.replaceAll(' ', '');

    // "-" should be allowed from QB System ideally but as of now they don't allowing -
    //qbAllowedPhoneNumber = qbAllowedPhoneNumber.replaceAll('-', '');

    webServiceURL = webServiceURL + '?UserId=' + currentUserInfo.QB_User_Id__c;
    webServiceURL = webServiceURL + '&UniqueReqId=' + uniqueNumber();
    webServiceURL = webServiceURL + '&SFDCOrderId=' + currentOrder.Id;
    webServiceURL = webServiceURL + '&SecretKey=' + secretKey;
    webServiceURL = webServiceURL + '&ShipToNumber=' + shippingAccountNumber;
    webServiceURL = webServiceURL + '&BillToNumber=' + billingAccountNumber;
    webServiceURL = webServiceURL + '&CustomerPO=' + EncodingUtil.urlEncode(String.valueOf(currentOrder.PoNumber), 'UTF-8');
    webServiceURL = webServiceURL + '&ContactName=' + EncodingUtil.urlEncode(relatedContact.FirstName + ' ' + relatedContact.LastName, 'UTF-8');
    webServiceURL = webServiceURL + '&ContactPhoneNumber=' + qbAllowedPhoneNumber;

    System.debug('@@@webServiceURL: '+ webServiceURL);
    req.setEndpoint(webServiceURL);
    req.setMethod('GET');
    try {
      res = http.send(req);
      System.debug('@@@res: ' + res);

      String resultedBody = res.getBody();
      System.debug('@@@response: ' + resultedBody);

      Medical_QBUtility.QBWrapper objQBWrapper = Medical_QBUtility.getFormattedResult(resultedBody);
      // To Do: parse res.getBody() into xml but XML is not proper formatted.
      // 0. Valid Request
      // 1. Secret Key is not correct.
      // 2. Unique Request Id already exists
      // 3. User ID is not active in QB or User Type is not configured for SSO login.
      // 4. Ship-to or sold-to are not in QB
      // 5. One of the input parameters is empty or does not exist in QB.
      // 6. A JDE Order is created using the currentSFDCOrderId

      if(objQBWrapper.errorCode == '0'){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Web service callout Successfull: ' + objQBWrapper.errrorDescription));
        PageReference pg = new PageReference('/apex/Medical_QuoteBuilder?UniqueReqId=' + objQBWrapper.uniqueReqId);
        pg.setRedirect(true);
        return pg;
        //https://qb.stryker.com/e2wSSOReceiver.aspx
      }
      else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Web service callout failed: ' + objQBWrapper.errorCode));
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Web service callout failed: ' + objQBWrapper.errrorDescription));
        return null;
      }
    }
    catch(System.CalloutException e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Web service callout failed: ' + e.getMessage()));
      System.debug('@@@Exception: '+ e.getMessage());
      return null;
    }
  }

  //-----------------------------------------------------------------------------------------------
  //  Helper method to call JDE Web service.
  //-----------------------------------------------------------------------------------------------
  private String callJDEWebService(){
    // Fetch Integration server credentials
    String username = Medical_JDE_Settings__c.getInstance().JDE_Username__c;
    String password = Medical_JDE_Settings__c.getInstance().JDE_Password__c;

    Medical_JDE_OrderHeader.OrderHeader_ICPort objOrderHeader = new Medical_JDE_OrderHeader.OrderHeader_ICPort();
    objOrderHeader.inputHttpHeaders_x = new  Map<String,String>();
    String token = EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password));
    System.debug('@@@token' + token);

    objOrderHeader.inputHttpHeaders_x.put('Authorization', 'Basic ' + token);
    objOrderHeader.timeout_x = 120000;
    objOrderHeader.endpoint_x  = Medical_JDE_Settings__c.getInstance().JDE_Endpoint_URL__c;
    //objOrderHeader.endpoint_x  = 'http://requestb.in/1llqtng1';

    try {
      /*
        '802919',     // PO Number
        '1',         // tracking Id
        '1259508', // address number
        '1259508', // ship to address number
        '0',     //JDEOrderNumber
        'SO',     // Order Type
        '1250214',    // OrderTakenBy
        'Sunil',      //ContactFirstName
        'Gupta',      //ContactLastName
        '1234567890'        //ContactNumber
      */
      String poNumber = currentOrder.PoNumber;
      String trackingId = currentOrder.Id;
      String orderType = convertOrderType();
      String orderTakenBy = currentOrder.Address_Book_Id__c;
      String contactFirstName = relatedContact.FirstName;
      String contactLastName = relatedContact.LastName;
      //String contactPhoneNumber = currentOrder.Contact_Phone_Number__c;
      String contactPhoneNumber = currentOrder.Contact_PhoneNumber__c.substring(1, 4)+currentOrder.Contact_PhoneNumber__c.substring(6, 14); // Added by Gagan

      // Debug parameters.
      System.debug('@@@ PO Number' + poNumber);
      System.debug('@@@ trackingId' + trackingId);
      System.debug('@@@ addressNumber' + billingAccountNumber);
      System.debug('@@@ shipToAddressNumber' + shippingAccountNumber);
      System.debug('@@@ orderType' + orderType);
      System.debug('@@@ orderTakenBy' + orderTakenBy);
      System.debug('@@@ contactFirstName' + contactFirstName);
      System.debug('@@@ contactLastName' + contactLastName);
      System.debug('@@@ contactPhoneNumber' + contactPhoneNumber);

      Medical_JDE_OrderHeaderSchema.OrderHeader response = objOrderHeader.CreateOrderHeader(
                                poNumber, trackingId, billingAccountNumber, shippingAccountNumber, '0', orderType, orderTakenBy, contactFirstName, contactLastName, contactPhoneNumber);

      System.debug('@@@response' + response);
      System.debug('@@@JDEOrderNumber' + response.JDEOrderNumber);
      //System.debug('@@@JDEOrderNumber_type_info' + response.JDEOrderNumber_type_info);
      //System.debug('@@@ErrorCode' + response.OrderHeaderFault.ErrorCode);
      //System.debug('@@@ErrorDescription' + response.OrderHeaderFault.ErrorDescription);
      //System.debug('@@@ErrorCode_type_info' + response.OrderHeaderFault.ErrorCode_type_info);
      responseJDEOrderNumber = response.JDEOrderNumber;
      return 'success';
    }
    catch (CalloutException ce) {
      System.debug( '@@@SOAP Fault Details: ' + ce.getMessage());
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Web service call to JDE failed.  Please contact your system administrator for more information.'));
      return ce.getMessage();
    }
    catch (Exception e) {
      System.debug( '@@@Something else happened ' + e.getMessage());
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Web service call to JDE failed.  Please contact your system administrator for more information.'));
      return e.getMessage();
    }
  }

  //-----------------------------------------------------------------------------------------------
  //  Visualforce action method when Account is selected from custom lookup
  //-----------------------------------------------------------------------------------------------
  public void getSelectedRecord(){

    if(String.isBlank(selectedAccId) == false){
      List<Account> lstAccounts = [SELECT Id, Name, AccountNumber  FROM Account WHERE ID = : selectedAccId];
      if(lstAccounts.size() == 0){
        return;
      }

      if(fieldName == 'Bill_To_Account__c'){
        billToAccount_Id = lstAccounts.get(0).Id;
        billToAccount_Name = lstAccounts.get(0).Name;
      }

      else if(fieldName == 'Ship_To_Account__c'){
        shipToAccount_Id = lstAccounts.get(0).Id;
        shipToAccount_Name = lstAccounts.get(0).Name;
      }
    }


  }

  //-----------------------------------------------------------------------------------------------
  //  Helper method to generate unique number
  //-----------------------------------------------------------------------------------------------
  private String uniqueNumber(){
    String kHexChars = '0123456789abcdef';
    String returnValue = '';
    Integer nextByte = 0;
    for (Integer i=0; i<16; i++) {
      if (i==4 || i==6 || i==8 || i==10){
        returnValue += '-';
      }
      nextByte = (Math.round(Math.random() * 255)-128) & 255;
      if (i==6) {
        nextByte = nextByte & 15;
        nextByte = nextByte | (4 << 4);
      }
      if (i==8) {
        nextByte = nextByte & 63;
        nextByte = nextByte | 128;
      }
      returnValue += getCharAtIndex(kHexChars, nextByte >> 4);
      returnValue += getCharAtIndex(kHexChars, nextByte & 15);
    }
    return returnValue;
  }
  private String getCharAtIndex(String str, Integer index) {
    if (str == null) return null;
    if (str.length() <= 0) return str;
    if (index == str.length()) return null;
    return str.substring(index, index+1);
  }

  //-----------------------------------------------------------------------------------------------
  //  Helper method to convert JDE friendly Order Type codes
  //-----------------------------------------------------------------------------------------------
  private String convertOrderType(){
    if(String.isBlank(currentOrder.Type) == true){
        return '';
    }
    else{
        if(String.valueOf(currentOrder.Type).containsIgnoreCase(':')){
            return currentOrder.Type.split(':')[0];
        }
    }
    return currentOrder.Type;
    /*
    String result = 'SO';
      if (currentOrder.Type == 'SW Warranty') {
        result = 'SW';
      }

      else if (currentOrder.Type == 'SO Domestic Sales Order') {
        result = 'SO';
      }

      else if (currentOrder.Type == 'SQ Sales Quote') {
        result = 'SQ';
      }

      else if (currentOrder.Type == 'SX Field Service Rep') {
        result = 'SX';
      }

      else if (currentOrder.Type == 'SA Sample Order') {
        result = 'SA';
      }

      else if (currentOrder.Type == 'ST Trial Order') {
        result = 'ST';
      }

      else if (currentOrder.Type == 'SI International') {
        result = 'SI';
      }

      else if (currentOrder.Type == 'SM Misc') {
        result = 'SM';
      }

      else if (currentOrder.Type == 'CO Credit') {
        result = 'CO';
      }

      else if (currentOrder.Type == 'RMA Return') {
        result = 'RA';
      }

      else if (currentOrder.Type == 'SR Rental Order') {
        result = 'SR';
      }

      else if (currentOrder.Type == 'SL Secondary Market Sales') {
        result = 'SL';
      }

      else if (currentOrder.Type == 'SN International Adj') {
        result = 'SN';
      }

      else if (currentOrder.Type == 'CI International Credit') {
        result = 'CI';
      }

      else if (currentOrder.Type == 'SZ Service Part Order') {
        result = 'SZ';
      }

      else if (currentOrder.Type == 'CZ Service Part Credit') {
        result = 'CZ';
      }
      return result; */
  }
}