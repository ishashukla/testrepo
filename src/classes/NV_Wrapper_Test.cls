//
// (c) 2015 Appirio, Inc.
//
// Apex Test Class Name: NV_Wrapper_Test
// For Apex Class: NV_Wrapper, NV_RTPM, NV_CloudRTPB
//
// 08th January 2015   Hemendra Singh Bhati   Original (Task # T-459198)
//
@isTest(seeAllData=false)
private class NV_Wrapper_Test {
  // Private Data Members.

  /*
  @method      : validateNVWrapperFunctionality
  @description : This method validates functionality built in apex wrapper class named "NV_Wrapper".
  @params      : void
  @returns     : void
  */
  private static testMethod void validateNVWrapperFunctionality() {
    // Instantiating Inner Class - NV_Wrapper.OrderWrapper
    NV_Wrapper.OrderWrapper theOrderWrapper = new NV_Wrapper.OrderWrapper(new Order());
    theOrderWrapper = new NV_Wrapper.OrderWrapper(new Order(), Date.today().addDays(-10), Date.today());
    theOrderWrapper.Expedited = true;

    // Instantiating Inner Class - NV_Wrapper.UserOrders
    NV_Wrapper.UserOrders theUserOrder = new NV_Wrapper.UserOrders(UserInfo.getUserId(), UserInfo.getUserName());

    // Instantiating Inner Class - NV_Wrapper.ProductLineData
    NV_Wrapper.ProductLineData theProductLineData = new NV_Wrapper.ProductLineData('Test Product Name');
    theProductLineData.addNewProduct(new Product2(
      Segment__c = 'Test Segment 1'
    ));
    theProductLineData.addNewProduct(new Product2(
      Segment__c = 'Test Segment 2'
    ));
    theProductLineData.addNewSegment('Test Segment 3');
    theProductLineData.getSegments();

    // Instantiating Inner Class - NV_Wrapper.SegmentData
    NV_Wrapper.SegmentData theSegmentData = new NV_Wrapper.SegmentData('Test Segment');
    theSegmentData.getProducts();

    // Instantiating Inner Class - NV_Wrapper.ModelNPricing
    NV_Wrapper.ModelNPricing theModelNPricing = new NV_Wrapper.ModelNPricing(new NV_RTPM.ResolvedPriceType());

    // Instantiating Inner Class - NV_Wrapper.RegionalDataWrapper
    NV_Wrapper.RegionalDataWrapper theRegionalDataWrapper = new NV_Wrapper.RegionalDataWrapper();
    theRegionalDataWrapper.getUserWrapperMap();

    // Instantiating Inner Class - NV_Wrapper.UserWrapper
    NV_Wrapper.UserWrapper theUserWrapper = new NV_Wrapper.UserWrapper(new User(Id = UserInfo.getUserId()));
	}

  /*
  @method      : validateNVRTPMFunctionality
  @description : This method validates functionality built in apex class named "NV_RTPM".
  @params      : void
  @returns     : void
  */
  private static testMethod void validateNVRTPMFunctionality() {
    // Instantiating Inner Class - NV_RTPM.ProductIDs
    NV_RTPM.ProductIDs theProductIDs = new NV_RTPM.ProductIDs();

    // Instantiating Inner Class - NV_RTPM.RealTimePriceMultiRequestType
    NV_RTPM.RealTimePriceMultiRequestType theRealTimePriceMultiRequestType = new NV_RTPM.RealTimePriceMultiRequestType();

    // Instantiating Inner Class - NV_RTPM.RealTimePriceMultiResponseType
    NV_RTPM.RealTimePriceMultiResponseType theRealTimePriceMultiResponseType = new NV_RTPM.RealTimePriceMultiResponseType();

    // Instantiating Inner Class - NV_RTPM.ResolvedPrices
    NV_RTPM.ResolvedPrices theResolvedPrices = new NV_RTPM.ResolvedPrices();

    // Instantiating Inner Class - NV_RTPM.ResolvedPriceType
    NV_RTPM.ResolvedPriceType theResolvedPriceType = new NV_RTPM.ResolvedPriceType();
    theResolvedPriceType.ProductID = 'Test';
    theResolvedPriceType.ResolvedPrice = 'Test';
    theResolvedPriceType.ResolvedCurrency = 'Test';
    theResolvedPriceType.CommitmentID = 'Test';
    theResolvedPriceType.ResolvedBasePrice = 'Test';
    theResolvedPriceType.ResolvedDiscount = 'Test';
    theResolvedPriceType.ResolvedDiscountType = 'Test';
    theResolvedPriceType.ResolvedTierIndex = 'Test';
    theResolvedPriceType.ContractIDNumber = 'Test';
    theResolvedPriceType.ProductGroupID = 'Test';
    theResolvedPriceType.Status = 'Test';
    theResolvedPriceType.MessageType = 'Test';
    theResolvedPriceType.Message = 'Test';
  }

  /*
  @method      : validateNVCloudRTPBFunctionality
  @description : This method validates functionality built in apex class named "NV_CloudRTPB".
  @params      : void
  @returns     : void
  */
  private static testMethod void validateNVCloudRTPBFunctionality() {
    // Inserting Custom Setting Data.
    insert new List<NV_Product_Pricing_Settings__c> {
      new NV_Product_Pricing_Settings__c(
        Name = 'Username',
        Value__c = 'hsingh@appirio.com'
      ),
      new NV_Product_Pricing_Settings__c(
        Name = 'Password',
        Value__c = 'password'
      )
    };

    // Instantiating Inner Class - NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort
    NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort theInstance = new NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort();
    NV_RTPM.RealTimePriceMultiResponseType theResponseType = theInstance.ModelNProductRealTimePriceLookup(
      new NV_RTPM.RealTimePriceMultiRequestType()
    );
  }
}