/**=====================================================================
   * Appirio, Inc
   * Name: COMMReportEXT.cls
   * Description: To Pass Project Id As a Parameter to Install Product Ready to install Report
   * Created Date: 9th May 2016
   * Created By: Shubham Dhupar(T-501087)
   * =====================================================================*/
global with sharing class COMMReportEXT {
    webservice static String runReportFunction(String devName, String projectId, String phaseId) {
    Id ReportId = [Select id from Report where DeveloperName =: devName].Id;
    System.debug('projectId ' + projectId );
    PageReference pageRef;
    if(projectId != '') {
        pageRef = new PageReference('/'+ReportId +'?pv0='+projectId);
    }
    else if(phaseId != '') {
        pageRef = new PageReference('/'+ReportId +'?pv1='+phaseId);
    }
    pageRef.setRedirect(true);
    return String.valueOf((pageRef).getUrl());
  }

}