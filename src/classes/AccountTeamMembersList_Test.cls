/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMembersList_Test
 * Description: Test class for AccountTeamMembersList_Test (T-477657)
 * Created Date: 3rd March 2016
 * Created By: Deepti Maheshwari(Appirio)
 -----------------------------------------------------------------------
 * Purpose        :  To Add DefaultAcctTeam Test Method
 * Modified by    :  Meghna Vijay
 * Modified Date  :  April 06, 2016
 * =====================================================================*/
@isTest
private class AccountTeamMembersList_Test {

// Method to verify the functionality of AccountTeamMembersList controller
static testMethod void basicTest() {
createTestData();
//fetch the created Account and User
Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account 0'];
User usr = [SELECT Id FROM User WHERE Email = 'user0@test.com' limit 1];

// Set the standard controller and call the constructor.
ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(acc);
AccountTeamMembersListEdit editController = new AccountTeamMembersListEdit(std);
AccountTeamMembersList viewController = new AccountTeamMembersList(std);

// Call the methods and create new team member
editController.addTeamMembers();
editController.newAccountTeamMembers[0].member.UserId = usr.Id;
editController.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
editController.newAccountTeamMembers[0].accountAccess = 'Edit';
editController.newAccountTeamMembers[0].opportunityAccess = 'Read';
editController.newAccountTeamMembers[0].caseAccess = 'Read';
System.debug('&&&&&&&&&&&&&&&&&&&&&&&'+editController);
// Cancel (removes ATM from the list)
editController.doCancel();

List<AccountTeamMember> atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
System.assertEquals(0, atm.size());

// Try again, and then save
editController.addTeamMembers();
editController.newAccountTeamMembers[0].member.UserId = usr.Id;
editController.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
editController.newAccountTeamMembers[0].accountAccess = 'Edit';
editController.newAccountTeamMembers[0].opportunityAccess = 'Read';
editController.newAccountTeamMembers[0].caseAccess = 'Read';

editController.saveAndMore();
viewController.showAccess();
viewController.showMoreRecords();

// Verify that the new team member is created and has proper role as given by user
atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
System.assertEquals(1, atm.size());
System.assertEquals(atm.get(0).TeamMemberRole, 'Sales Manager');


// delete the Team Member created
viewController = new AccountTeamMembersList(std);
viewController.selectedId = atm.get(0).Id;
viewController.doDelete();

}

// Method to verify the functionality of AccountTeamMembersList controller
static testMethod void basicTest2() {
createTestData();
//fetch the created Account and User
Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account 0'];
User usr = [SELECT Id FROM User WHERE Email = 'user0@test.com' limit 1];

// Set the standard controller and call the constructor.
ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(acc);
AccountTeamMembersList controller = new AccountTeamMembersList(std);

// Call the methods and create new team member
controller.addTeamMembers();
controller.newAccountTeamMembers[0].member.UserId = usr.Id;
controller.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
controller.newAccountTeamMembers[0].accountAccess = 'Edit';
controller.newAccountTeamMembers[0].opportunityAccess = 'Read';
controller.newAccountTeamMembers[0].caseAccess = 'Read';

// Cancel (removes ATM from the list)
controller.doCancel();
System.debug('&&&&&&&&&&&&&&&&&&&&&&&'+controller);
List<AccountTeamMember> atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
System.assertEquals(0, atm.size());

// Try again, and then save
controller.addTeamMembers();
controller.newAccountTeamMembers[0].member.UserId = usr.Id;
controller.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
controller.newAccountTeamMembers[0].accountAccess = 'Edit';
controller.newAccountTeamMembers[0].opportunityAccess = 'Read';
controller.newAccountTeamMembers[0].caseAccess = 'Read';

controller.saveAndMore();
controller.showAccess();
controller.showMoreRecords();

// Verify that the new team member is created and has proper role as given by user
atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
System.assertEquals(1, atm.size());
System.assertEquals(atm.get(0).TeamMemberRole, 'Sales Manager');

// delete the Team Member created
controller.selectedId = atm.get(0).Id;
controller.doDelete();


}
  //----------------------------------------------COMM I-210282 Start-----------------------------------------//
  //Name             :     DefaultAcctTeam Method
  //Description      :     Test Method created to test insertion of Default Account Team in 
  //                       Account Team Member Related List
  //Created by       :     Meghna Vijay I-210282 
  //Created Date     :     April 06, 2016
  static testMethod void DefaultAcctTeam() {
    createTestData();
    Account acc = [SELECT Id 
                   FROM Account 
                   WHERE Name = 'Test Account 0'];
                   System.debug('>>>>>>>>>>>>>>>>>'+acc.Id);
    User usr = [SELECT Id 
                FROM User 
                WHERE Email = 'user0@test.com' limit 1];
         System.debug('<<<<<<<<'+usr);       
         
    
    System.runAs(usr) {
                    
            
    ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(acc);
    AccountTeamMembersList defaultAcctTeamMember = new AccountTeamMembersList(std);
    defaultAcctTeamMember.addDefaultAcctMember();
    List<AccountTeamMember> acctTeamMemberList = [SELECT Id
                                                  FROM AccountTeamMember
                                                  WHERE AccountId =: acc.Id];
                                                  System.debug('(((((((((('+acctTeamMemberList);
    System.assertEquals(acctTeamMemberList.size(), 1);
    }
  }
  //----------------------------------------------COMM I-210282 End-------------------------------------------//
  //=======================================================================
  // Create Test Data
  //=======================================================================

  public static void createTestData() {
    List<Account> accList = TestUtils.createAccount(1, true);
    List<User> testUser = TestUtils.createUser(2,'System Administrator', true);
    List<AccountTeamMember> accntTeamMemberList= TestUtils.createAccountTeamMember(1,accList.get(0).Id, testUser.get(1).Id, true);
    List<UserAccountTeamMember> userAccountTeamMem = [SELECT Id 
                                                      FROM UserAccountTeamMember 
                                                      WHERE ownerId = :testUser[0].Id];
    List<Contact> conList = new List<Contact>();
    conList.add(TestUtils.createCOMMContact(accList.get(0).Id,'xyz', false));
    conList.get(0).Private__c = false;
    insert conList;
  }
}