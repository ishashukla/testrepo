//
// (c) 2014 Appirio, Inc.
// Class autoCompleteController
// Controller class for Auto_Complete component
//
// 01 Mar 2016     Sahil Batra      Original(T-481991)
global class autoCompleteController {
    @RemoteAction
    global static SObject[] findSObjects(string obj, string qry, string addFields,String systemType,String bu) {
        // more than one field can be passed in the addFields parameter
        // split it into an array for later use
       Map<String,Integer> objectToSizeMap = new Map<String,Integer>();
       List<AutoCompleteResultSize__c> recordSizeList = new List<AutoCompleteResultSize__c>();
       recordSizeList = [SELECT Name,Size_of_Records__c FROM AutoCompleteResultSize__c];
       if(recordSizeList.size() > 0){
           for(AutoCompleteResultSize__c rec : recordSizeList){
               objectToSizeMap.put(rec.Name,Integer.valueOf(rec.Size_of_Records__c));
           }
        }
        List<String> buList = new List<String>();
        buList = bu.split('\\*');
        Set<String> pbNameSet = new Set<String>();
        System.debug('>>>>>1'+obj);
        System.debug('>>>>>1'+qry);
        System.debug('>>>>>1'+addFields);
        System.debug('>>>>>1'+systemType);
        System.debug('>>>>>1'+bu);
        if(obj == 'Product2'){
        Set<Id> setOfPriceBookIds = new Set<Id> ();
        if(!systemType.contains('baseTrail')){
        List<BusinessUnitAndPricebookName__c> pbList = [SELECT Id, Name, Price_Book_Name__c
                                                        FROM BusinessUnitAndPricebookName__c
                                                        WHERE Name IN :buList AND System_Type__c =:systemType];
         for(BusinessUnitAndPricebookName__c pbName : pbList) {
            pbNameSet.add(pbName.Price_Book_Name__c);
         }
         System.debug('>>>>pbNames'+pbNameSet);               
            for(PriceBook2 pb : [SELECT Id 
                                FROM PriceBook2
                                WHERE Name IN :pbNameSet]) {
                setOfPriceBookIds.add(pb.Id);
        }}
        else if(systemType.contains('baseTrail')){
            String temp = systemType.substring(9,systemType.length());
             System.debug('temp>>>>>1'+temp);
            setOfPriceBookIds.add(temp);
        }
        System.debug('>>>>pbIds'+setOfPriceBookIds); 
          if(setOfPriceBookIds.size() > 0) {
             Set<String> setOfProductIds = new Set<String> ();
               for(PriceBookEntry pbe : [SELECT Id, Product2Id
                                            FROM PriceBookEntry
                                            WHERE Pricebook2Id IN :setOfPriceBookIds AND Product2.isActive = true]) {
                    if(!setOfProductIds.contains(pbe.Product2Id)) {
                        setOfProductIds.add(pbe.Product2Id);    
                    }
              }
           System.debug('>>>>prodIds'+setOfProductIds);
        List<String> fieldList;
        if (String.isNotBlank(addFields)) fieldList = addFields.split(',');
       // check to see if the object passed is valid
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) {
            // Object name not valid
            return null;
        }
        // create the filter text
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        //begin building the dynamic soql query
        String soql = 'select id, Name';
        // if an additional field was passed in add it to the soql
        if (fieldList != null) {
            for (String s : fieldList) {
                soql += ', ' + s;
            }
        }
        // add the object and filter by name to the soql
        soql += ' from ' + obj + ' where Id IN:setOfProductIds AND IsActive = true AND  ( name' + filter;
        // add the filter by additional fields to the soql
        if (fieldList != null) {
            for (String s : fieldList) {
                soql += ' or ' + s + filter;
            }
        }
        soql += ' ) order by Name';
        if(objectToSizeMap.size() > 0 && objectToSizeMap.containsKey(obj)){
            soql+= ' limit '+objectToSizeMap.get(obj);
        }
        else{
            soql+= ' limit 50';
        }
        List<sObject> L = new List<sObject>();
        try {
            System.debug('>>>>soql'+soql);
            L = Database.query(soql);
        }
        catch (QueryException e) {
            return null;
        }
        System.debug('>>>>soql'+L);
        return L;
     }
     else{
        return null;
     }
   }
   else{
        List<String> fieldList;
        if (String.isNotBlank(addFields)) fieldList = addFields.split(',');
       // check to see if the object passed is valid
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) {
            // Object name not valid
            return null;
        }
        // create the filter text
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        //begin building the dynamic soql query
        String soql = 'select id, Name';
        // if an additional field was passed in add it to the soql
        if (fieldList != null) {
            for (String s : fieldList) {
                soql += ', ' + s;
            }
        }
        // add the object and filter by name to the soql
        soql += ' from ' + obj + ' where Business_Unit__c IN:buList AND Type__c =:systemType AND ( name' + filter;
        // add the filter by additional fields to the soql
        if (fieldList != null) {
            for (String s : fieldList) {
                soql += ' or ' + s + filter;
            }
        }
        soql += ' ) order by Name';
        if(objectToSizeMap.size() > 0 && objectToSizeMap.containsKey(obj)){
            soql+= ' limit '+objectToSizeMap.get(obj);
        }
        else{
            soql+= ' limit 50';
        }
        List<sObject> L = new List<sObject>();
        try {
            System.debug('>>>>soql'+soql);
            L = Database.query(soql);
        }
        catch (QueryException e) {
            return null;
        }
        System.debug('>>>>soql'+L);
        return L;
   }
  }
}