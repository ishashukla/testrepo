/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Dec 12, 2013 
 Purpose       :  Handler class for trigger FeedCommentTrigger
 Reference     :  Task T-217899
*******************************************************************************/
public without sharing class FeedCommentTriggerHandler {
  
  //****************************************************************************
  // Method called on after insert
  //****************************************************************************
  public static void onAfterInsert(List<FeedComment> newList){
    updateIntelField(newList);
  }
  
  //****************************************************************************
  // Method to update Mobile Update field on Intel object
  //****************************************************************************
  private static void updateIntelField(List<FeedComment> newList){
  	Map<String, Intel__c> mapIntelId_Intel = new Map<String, Intel__c>();
  	
  	for(FeedComment reply : newList){
  		//If it is Intel chatter post reply, add the intel record in a map with
  		//key as intel Id and Intel record with updated Mobile Update field
  		if(String.valueOf(reply.ParentId).startsWith(Constants.INTEL_PREFIX)){
  			if(!mapIntelId_Intel.containsKey(reply.ParentId)){
  				mapIntelId_Intel.put(reply.ParentId, new Intel__c(Id = reply.ParentId,
  				                                                  Mobile_Update__c = System.now()));
  			}
  		}
  	}
  	//Update Intel Records
  	if(mapIntelId_Intel.size() > 0){
  		update mapIntelId_Intel.values();
  	}
  }
  
}