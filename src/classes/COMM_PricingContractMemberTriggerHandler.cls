// 
// (c) 2016 Appirio, Inc.
//
// Name : COMM_PricingContractMemberTriggerHandler 
// 
// 3rd May 2016     Original     T-499313
// Used to populate Account.Primary_Contract field based on Primary flag on Pricing_Contract_Account__c
//
public class COMM_PricingContractMemberTriggerHandler {

  //================================================================      
  // Name         : afterInsertAndUpdate
  // Description  : 
  // Created Date : 3rd May 2016 
  // Created By   : Kirti Agarwal (Appirio)
  // Task         : T-499313
  //==================================================================
  public void afterInsertAndUpdate(List<Pricing_Contract_Account__c> newList, Map<Id, Pricing_Contract_Account__c> oldMap) {
    
    Set<Id> accountIdSet = new Set<Id>();
    Set<Id> nonUpdatedAccount = new Set<Id>();
    Map<Id, String> contractIdAndNameMap = new Map<Id, String>();
    
    for(Pricing_Contract_Account__c contractRec : newList) {
      contractIdAndNameMap.put(contractRec.Contract__c, '');
    }
    
    for(Contract contract : [SELECT Id, Name FROM Contract WHERE ID IN: contractIdAndNameMap.keyset()]) {
      contractIdAndNameMap.put(contract.id, contract.Name);
    }

    List<Account> updateAccountList = new List<Account>();
    //this loop populating the Primary_Contract__c with current contract with primary = true
    for(Pricing_Contract_Account__c contractRec : newList) {
      if(oldMap == null || (oldMap.get(contractRec.id).Primary__c != contractRec.Primary__c)) {
        if(contractRec.Primary__c) {
          if(!accountIdSet.contains(contractRec.Account__c)) {
            updateAccountList.add(new Account(Id = contractRec.Account__c, 
                 Primary_Contract__c = String.valueOf(contractRec.Contract__c)  + '=' + contractIdAndNameMap.get(contractRec.Contract__c)));
          }
          accountIdSet.add(contractRec.Account__c);
        }else {
          nonUpdatedAccount.add(contractRec.Account__c);
        }
      }
    } 
    
    //remove account element from nonUpdatedAccount if contains by accountIdSet
    for(Id accId : nonUpdatedAccount) {
      if(accountIdSet.contains(accId)) {
        nonUpdatedAccount.remove(accId);
      }
    }

    
    if(!nonUpdatedAccount.isEmpty()) {
      accountIdSet = new set<Id>();
      //this loop populating the Primary_Contract__c with anyother contract with primary = true
      for(Pricing_Contract_Account__c contractRec: [SELECT Id , Account__c, Contract__c, Contract__r.Name
                                                      FROM Pricing_Contract_Account__c 
                                                     WHERE Primary__c = true 
                                                       AND Account__c IN :nonUpdatedAccount]) {
         updateAccountList.add(new Account(Id = contractRec.Account__c, 
              Primary_Contract__c =   String.valueOf(contractRec.Contract__c)  + '=' + contractRec.Contract__r.Name));
         accountIdSet.add(contractRec.Account__c);
      }
      
      //this loop populating the Primary_Contract__c with blank value
      for(Id accId : nonUpdatedAccount) {
        if(!accountIdSet.contains(accId)) {
          updateAccountList.add(new Account(Id = accId, 
              Primary_Contract__c = ''));
        }
      }
    }

    //updated the account if Primary__c = true for Contract
    if(!updateAccountList.isEmpty()) {
      upsert updateAccountList;
    }
  }
  
}