@isTest
private class COMM_CheckListItemTriggerHandlerTest {

	private static testMethod void populateChecklistAndSequence() {
	  List<ChecklistCUST__c> checklist = TestUtils.createCustomChecklist(1,'Test checklist',1,true);
	  List<Checklist_Group__c> checklistGroup = TestUtils.createChecklistGroup(1,'Test Group',checklist.get(0).id,1,true);
	  List<CheckList_Item__c> checklistItem = TestUtils.createChecklistItem(3,'Test Item',checklist.get(0).id,checklistGroup.get(0).id,true,false);
	  checklistItem.get(0).Sequence__c = 1;
	  checklistItem.get(2).Sequence__c = 4;
	  Insert checklistItem;
	  checklistItem.get(1).Sequence__c = 1;
	  Update checklistItem.get(1);
	  List<CheckList_Item__c> checklistnewItem = TestUtils.createChecklistItem(1,'Test new Item',checklist.get(0).id,checklistGroup.get(0).id,true,true);
	  List<CheckList_Item__c> checklistItems = TestUtils.createChecklistItem(1,'Test new Items',checklist.get(0).id,checklistGroup.get(0).id,true,false);
	  checklistItems.get(0).sequence__c = 5;
	  insert checklistItems;
	}

}