// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncInventoryTransaction_SCHTest.
//
// 28 Oct 2016, Isha Shukla
@isTest
private class COMM_SyncInventoryTransaction_SCHTest {
    static List<SVMXC__Stock_Transfer__c> recordList;
    @isTest static void testInventorySync() {
        createData();
        Test.startTest();
        COMM_SyncInventoryTransaction_SCH obj =new COMM_SyncInventoryTransaction_SCH();
		obj.execute(null);
        Test.stopTest();
        System.assertEquals('Success', [SELECT Integration_Status__c FROM SVMXC__Stock_Transfer__c WHERE Id = :recordList[0].Id].Integration_Status__c);
    }
    public static void createData() {
        SVMXC__Site__c slocation = new SVMXC__Site__c(Location_ID__c = '1234',SVMXC__Stocking_Location__c = true);
        insert slocation;
        SVMXC__Site__c dlocation = new SVMXC__Site__c(Location_ID__c = '1235',SVMXC__Stocking_Location__c = true);
        insert dlocation;
        recordList = new List<SVMXC__Stock_Transfer__c>();
        for(Integer i = 0; i < 4; i++){
            SVMXC__Stock_Transfer__c stock = new SVMXC__Stock_Transfer__c(
                     Integration_Status__c = 'New',
                     SVMXC__Source_Location__c = slocation.Id,
                     SVMXC__Destination_Location__c = dlocation.Id,
                	 Acceptance_Status__c = 'Accepted'
            );
            recordList.add(stock);
        }
       insert recordList;
       SVMXC__Installed_Product__c asset = new SVMXC__Installed_Product__c(External_ID__c = '123',SVMXC__Serial_Lot_Number__c='12',Quantity__c = 1);
       insert asset;
       List<SVMXC__Stock_Transfer_Line__c> lineList = new List<SVMXC__Stock_Transfer_Line__c>();
       for(Integer i = 0; i < 4; i++){
            SVMXC__Stock_Transfer_Line__c line = new SVMXC__Stock_Transfer_Line__c(
                     SVMXC__Quantity_Transferred2__c = 1,
                	 Asset__c = asset.Id,
                     SVMXC__Stock_Transfer__c = recordList[i].Id
            );
            lineList.add(line);
        }
       insert lineList;
       COMMRecordCount__c customSetting = new COMMRecordCount__c(Count__c = 3);
       insert customSetting;
    }
}