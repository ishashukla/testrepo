/*************************************************************************************************
Created By:    Used Asset(Show Filtered Accounts on a Map) modified by Sunil Gupta
Date:          Feb 14, 2014
Description  : Test class for Endo_showSalesRepTerritoryController
**************************************************************************************************/
@isTest
private class Endo_ShowSalesRepTerritoryControllerTest {
  static testMethod void testCallReportMapConMethod() {
        // Load the Data
        Account acc = Endo_TestUtils.createAccount(1, false).get(0);
        //acc.ShippingCity = 'Test';
        //acc.ShippingCountry = 'Test';
        //acc.ShippingPostalCode = '12345'; 
        insert acc;
          
        Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
        con.Title= 'Sales Rep';
        insert con;
        
        Stryker_Contact__c junction = Endo_TestUtils.createContactJunction(1, acc.Id,con.Id, true).get(0);
        
        Test.startTest();
        
        Endo_ShowSalesRepTerritoryController obj = new Endo_ShowSalesRepTerritoryController(new ApexPages.StandardController(con));
        List<Account> returnedAccts = Endo_showSalesRepTerritoryController.returnAccountsforDateRange(con.Id);
        
        System.assertEquals(1,returnedAccts.size());
        Test.stopTest();
    }
}