/**================================================================      
* Appirio, Inc
* Name: Mobile_Utility.cls
* Description: Utility class for Mobile projects
* Created Date: 03-AUG-2016
* Created By: Ray Dehler (Appirio)
*
* Date Modified      Modified By      Description of the update
* 03 AUG 2016        Ray Dehler       Created and added support for Big Data S-423983
==================================================================*/
public with sharing class Mobile_Utility {
	public static final String DEFAULT_CONTROLLER_CLASS = 'NV_ProductPricingImpl';

    public class NotImplementedException extends Exception {}
}