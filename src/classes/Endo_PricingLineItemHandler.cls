/*************************************************************************************************
Created By:    Jitendra Kothari
Date:          March 20, 2014
Description  : Handler class for PricingLineItem Trigger
**************************************************************************************************/
public class Endo_PricingLineItemHandler {
	 public static void populateContractPrice(List<Pricing_Contract_Line_Item__c> lstNewpli, Map<Id, Pricing_Contract_Line_Item__c> mapOldPli) {
	 	set<Id> productIds = new set<Id>();
        for(Pricing_Contract_Line_Item__c pricingLine : lstNewpli){
        	//if(pricingLine.Excluder__c == true){
        	//	pricingLine.Contract_Price__c = 0.0;
        	//}
        	//else
        	 if((mapOldPli == null) 
        			|| (mapOldPli.get(pricingLine.Id).Application_Method__c != pricingLine.Application_Method__c)) {
        			//|| (mapOldPli.get(pricingLine.Id).Excluder__c != pricingLine.Excluder__c)){
	            if(pricingLine.Application_Method__c != null){// && pricingLine.Excluder__c == false){
	            	if(pricingLine.Application_Method__c == '%' || pricingLine.Application_Method__c == 'Percent'){ 
		                productIds.add(pricingLine.Product__c);
		            }else if(pricingLine.Application_Method__c.replace(' ', '').toUpperCase() == 'NEWPRICE'){
		            	pricingLine.Contract_Price__c = pricingLine.Value__c;
		            }else{
                		pricingLine.Contract_Price__c = pricingLine.Value__c;
                	}
	            }
            }
        }
        if(productIds.isEmpty()) return;
        Map<Id, PricebookEntry> priceBookByProduct = getPriceBookRecords(productIds);
        Decimal discountAmount = 0.0;
        for(Pricing_Contract_Line_Item__c pricingLine : lstNewpli){
            if(pricingLine.Application_Method__c != null 
            	&& (pricingLine.Application_Method__c == '%'  || pricingLine.Application_Method__c == 'Percent' ) && priceBookByProduct.containsKey(pricingLine.Product__c)){
                Decimal lstPrice = priceBookByProduct.get(pricingLine.Product__c).UnitPrice;
                if(pricingLine.Value__c != null){
                    discountAmount = (lstPrice * pricingLine.Value__c) / 100 ;
                }
                pricingLine.Contract_Price__c = lstPrice - discountAmount;
            }
        }
	 }
	 // Helper method to get unit price by product
    @TestVisible
    private static Map<Id, PricebookEntry> getPriceBookRecords(set<Id> productIds){
        List<PricebookEntry> priceBooks = [SELECT UnitPrice, Product2Id 
                                                FROM PricebookEntry 
                                                WHERE Pricebook2.Name = 'Standard Price Book' AND IsActive = true AND Product2Id IN :productIds];
        Map<Id, PricebookEntry> priceBookByProduct = new Map<Id, PricebookEntry>();
        for(PricebookEntry pe : priceBooks){
            priceBookByProduct.put(pe.Product2Id, pe);
        }
        return priceBookByProduct;
    }
}