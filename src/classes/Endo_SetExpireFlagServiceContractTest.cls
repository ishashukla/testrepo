/*************************************************************************************************** 
Created By   : Sunil
Created Date : Apr 23, 2014
Usage        : Unit test coverage of Endo_SetExpireFlagServiceContract
***************************************************************************************************/
@isTest 
private class Endo_SetExpireFlagServiceContractTest{
  
  //------------------------------------------------------------------------------------------------
  // Unit Test Method 1
  //------------------------------------------------------------------------------------------------
  static testMethod void  testUnit(){
  	Account acc = Endo_TestUtils.createAccount(1, true).get(0);
  	
    Service_Contract__c obj = new Service_Contract__c();
    obj.Account__c = acc.Id;
    obj.Start_Date__c = System.Today();
    obj.End_Date__c = System.Today();
    insert obj;
    
    Service_Contract__c obj2 = new Service_Contract__c();
    obj2.Account__c = acc.Id;
    obj2.Start_Date__c = System.Today();
    obj2.End_Date__c = System.Today().addDays(20);
    insert obj2;
    
    Service_Contract__c obj3 = new Service_Contract__c();
    obj3.Account__c = acc.Id;
    obj3.Start_Date__c = System.Today();
    obj3.End_Date__c = System.Today().addDays(40);
    insert obj3;
    
    Service_Contract__c obj4 = new Service_Contract__c();
    obj4.Account__c = acc.Id;
    obj4.Start_Date__c = System.Today();
    obj4.End_Date__c = System.Today().addDays(1);
    insert obj4;
    
    Service_Contract__c obj5 = new Service_Contract__c();
    obj5.Account__c = acc.Id;
    obj5.Start_Date__c = System.Today();
    obj5.End_Date__c = System.Today().addDays(-3);
    obj5.Is_Expiring__c = true;
    insert obj5;
    
    Service_Contract__c obj6 = new Service_Contract__c();
    obj6.Account__c = acc.Id;
    obj6.Start_Date__c = System.Today();
    obj6.End_Date__c = System.Today().addDays(300);
    obj6.Is_Expiring__c = true;
    insert obj6;
    
    Test.startTest();
    
    Id batchprocessid = Database.executeBatch(new Endo_SetExpireFlagServiceContract());
    AsyncApexJob aaj;
    try{
      aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob 
             WHERE ID =: batchprocessid];
    }
    catch(Exception e){
      // DO Nothing
    }
    
    // Verify the Job submission
    System.assert(aaj != null);             
    Test.stopTest();
    set<Id> setSelected = new set<Id>();
    setSelected.add(obj.Id);
    setSelected.add(obj2.Id);
    setSelected.add(obj4.Id);
    
    set<Id> setFalse = new set<Id>();
    setFalse.add(obj3.Id);
    setFalse.add(obj5.Id);
    setFalse.add(obj6.Id);
    
    for(Service_Contract__c sc :[SELECT Id, End_Date__c, Is_Expiring__c FROM Service_Contract__c WHERE Is_Expiring__c = true]){
      System.debug('@@@' + sc.End_Date__c);
      System.assertEquals(setSelected.contains(sc.Id), true);
    }
    
    for(Service_Contract__c sc :[SELECT Id, End_Date__c, Is_Expiring__c FROM Service_Contract__c WHERE Is_Expiring__c = false]){
       System.debug('@@@' + sc.End_Date__c);
      System.assertEquals(setFalse.contains(sc.Id), true);
    }
  }
}