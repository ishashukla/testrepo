/**================================================================      
* Appirio, Inc
* Name: COMM_ProductTriggerHandler
* Description: T-522767 (To create Price book entries on the COMM GIM+ products)
* Created Date: 07/27/2016
* Created By: Nitish Bansal (Appirio)
*
* 1st September 2016     Nitish Bansal      Update (I-233001)  //To create PBE on COMM products on update of records.
* 12th September 2016    Nitish Bansal      Update //To resolve the non-sequential bulk API load errors.
* 15th September 2016    Nitish Bansal      Update (I-235331) // Restrict trigger to create only standard PBE. (Commented COMM PB code)
* 28th September 2016    Ralf hoffmann      Update (I-237535) // Adding back trigger code for COMM PBE, previously commented in I-235331
* 4th October 2016       Nitish Bansal      Update (I-238338)  //To populate external id on COMM PBE.
* 29th November 2016     Nitish Bansal      I-242624
==================================================================*/

public class COMM_ProductTriggerHandler {

    public static void onAfterInsertUpdate(List<Product2> newList){
        Id  stdPriceBookId; //NB - 11/29 - I-242624
        Boolean stdPBEexists, commPBEexists;
        Pricebook2 commPriceBook; //NB - 11/29 - I-242624
        Pricebookentry tempPricebookentry;
        List<Pricebookentry> stdPriceBookEntryList = new List<Pricebookentry>();
        List<Pricebookentry> commPriceBookList = new List<Pricebookentry>();  //I-237535 uncommented this
        List<Product2> createStandardPBEForProductsList = new List<Product2>();
        List<Product2> createCommPBEForProductsList = new List<Product2>(); //I-237535 uncommented this
        List<PriceBookEntry> updatePBEForProductList = new List<PriceBookEntry>();
        //Quering Pricebooks
        for(Pricebook2 priceBook : [Select Id, name, isStandard from Pricebook2 
                                    where name = :Constants.COMM_PRICEBOOK or isStandard = true]){
            //std PB check
            if(priceBook.isStandard){
                stdPriceBookId = priceBook.Id; //NB - 11/29 - I-242624
            } else if(priceBook.name == Constants.COMM_PRICEBOOK){//COMM PB check
                commPriceBook = priceBook;
            }
        }

        //NB - 11/29 - I-242624 start
        if(Test.isRunningTest())   
            stdPriceBookId = Test.getStandardPricebookId();
        //NB - 11/29 - I-242624 end    
     
        for(Product2 p : [SELECT Id,CurrencyIsoCode,Item_Owner__c,Floor_Price__c, Inventory_Item_ID__c, 
                                  (Select Id, CurrencyISOCode, PriceBook2Id, UnitPrice, External_id__c from PriceBookEntries) 
                                  FROM Product2  
                                  WHERE Id in :trigger.newMap.keyset()]){ //NB - 10/04 - I-238338 - Modified query
            stdPBEexists = commPBEexists= false;
            for(PriceBookEntry pbe : p.priceBookEntries){
                //I-237535 uncommented following if clause    
                if(pbe.currencyISOCode == p.currencyISOCode && pbe.PriceBook2Id == commPriceBook.Id){
                        commPBEexists = true;    
                        //NB - 10/04 - I-238338 - Start
                        if((pbe.UnitPrice != p.Floor_Price__c && p.Floor_Price__c > 1.00) || pbe.External_id__c == null){
                            if(pbe.External_id__c == null){
                                pbe.External_id__c = Constants.COMM_PB_STRING + p.Inventory_Item_ID__c; 
                            }
                            if(pbe.UnitPrice != p.Floor_Price__c && p.Floor_Price__c > 1.00){
                                pbe.UnitPrice = p.Floor_Price__c;
                            }
                            updatePBEForProductList.add(pbe);
                        } 
                        //NB - 10/04 - I-238338 - End   
                    }else 
                    if(stdPriceBookId != null && pbe.currencyISOCode == p.currencyISOCode && pbe.PriceBook2Id == stdPriceBookId){//NB - 11/29 - I-242624
                        stdPBEexists = true;                   
                        if(pbe.UnitPrice != p.Floor_Price__c && p.Floor_Price__c > 1.00){
                            pbe.UnitPrice = p.Floor_Price__c;
                            updatePBEForProductList.add(pbe); 
                        }                          
                    }
            }
            if(!stdPBEexists){
                createStandardPBEForProductsList.add(p);
            }
            //I-237535 uncommented following if statement
            if(!commPBEexists){
                createCommPBEForProductsList.add(p);
            } 
        }
        
        if(stdPriceBookId != null && createStandardPBEForProductsList.size() > 0){//NB - 11/29 - I-242624
            for(Product2 prod : createStandardPBEForProductsList){
                //Checking for COMM products
                if(prod.Item_Owner__c == 'SCC' ){
                    //Creating entry in standard price book            
                    tempPricebookentry = new PricebookEntry();
                    tempPricebookentry.UnitPrice = 1.00 ;
                    tempPricebookentry.UseStandardPrice = false;
                    tempPricebookentry.IsActive = true;
                    tempPricebookentry.Pricebook2Id = stdPriceBookId;//NB - 11/29 - I-242624
                    tempPricebookentry.PRODUCT2ID = prod.Id;
                    tempPricebookentry.CurrencyIsoCode = prod.CurrencyIsoCode; 
                    stdPriceBookEntryList.add(tempPricebookentry);
                }
            }
        }

        //I-237535 uncommenting following if statement block
        if(commPriceBook != null && createCommPBEForProductsList.size() > 0){
            for(Product2 prod : createCommPBEForProductsList){
                //Checking for COMM products
                if(prod.Item_Owner__c == 'SCC' ){
                    //creating entry in COMM Price Book
                    tempPricebookentry = new PricebookEntry();
                    tempPricebookentry.UnitPrice = 1.00 ;
                    tempPricebookentry.UseStandardPrice = false;
                    tempPricebookentry.IsActive = true;
                    tempPricebookentry.Pricebook2Id = commPriceBook.Id;
                    tempPricebookentry.PRODUCT2ID = prod.Id;
                    tempPricebookentry.CurrencyIsoCode = prod.CurrencyIsoCode; 
                    tempPricebookentry.External_id__c = Constants.COMM_PB_STRING + prod.Inventory_Item_ID__c; //NB - 10/04 - I-238338
                    commPriceBookList.add(tempPricebookentry);
                }
            }
        }        
                
        //inserting standard price book entry records
        if(stdPriceBookEntryList.size() > 0){
            try {
                insert stdPriceBookEntryList;
            } catch (DMLException e) {
                //system.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }
        
        //inserting comm price book entry records (2 separate DML statements are used since custom PBE records can be entered only once we already have the std PBE records in system.)
        //I-237535 - uncommented Comm PBE logic (following if statement)
        //system.debug('commPriceBookList : ' + commPriceBookList.size());
        if(commPriceBookList.size() > 0){
            try {
                insert commPriceBookList;
            } catch (DMLException e) {
                //system.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }
        
        //system.debug('updatePBEForProductList : ' + updatePBEForProductList.size());
        if(updatePBEForProductList.size() > 0){
            try {
                update updatePBEForProductList;
            } catch (DMLException e) {
                //system.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }
      
    }
}