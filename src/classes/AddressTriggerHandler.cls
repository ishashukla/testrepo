/**================================================================      
* Appirio, Inc
* Name: AddressTriggerHandler
* Description: T-482844
* Created Date: 11 Apr 2016
* Created By: Rahul Aeran (Appirio)
*
* Date Modified      Modified By            Description of the update
* 4 Jul 2016         Deepti Maheshwari      Ref : I-224956
* 12 July 2016       Rahul Aeran            Ref : I-225660
* 15 Sep 2016        Kanika Mathur          Ref : I-235328 To replace standard address fields by custom address fields.
* 22 Sep 2016        Ralf Hoffmann          Ref : I-236601 to also replace the Location_id on account with custom Billto/Sipto for Comm
* 28 Sep 2016        Isha Shukla            Ref : T-541552 Added method syncAddressWithSVMXSite to Sync Up Address fields with Location fields.
* 08 Nov 2016        Ralf Hoffmann			Ref : I-243410 added to method createAccountHierarchyOnAddress additional condition: address.Business_Unit_Name__c == 'COMM'
==================================================================*/

public class AddressTriggerHandler {
    
    public static void onAfterInsert(List<Address__c> lstNew){
        updateAddressonAccount(lstNew, null);
        createAccountHierarchyOnAddress(lstNew, null);
        syncAddressWithSVMXSite(lstNew, null);
    }
    
    
    public static void onAfterUpdate(List<Address__c> lstNew, map<Id,Address__c> oldMap){
        updateAddressonAccount(lstNew, oldMap);
        createAccountHierarchyOnAddress(lstNew, oldMap);
        syncAddressWithSVMXSite(lstNew, oldMap);
    }
    
    
    // Methods to be called after record insert and after update
    // @pupose : T-482844
    // @description : Used to update billing and shipping address on Account based on Address primary object
    // @param mapOld - map to hold Trigger.oldMap 
    // @param lstNew - List to hold Trigger.new 
    // @return Nothing 
    public static void updateAddressonAccount(List<Address__c> lstNew, map<Id,Address__c> mapOld){
        Boolean isUpdate = mapOld != null ? true : false;
        //creating two sets one for billing address and another for shipping address
        map<Id,Id> mapAccIdToBillingAddId = new map<Id,Id>();
        map<Id,Id> mapAccIdToShippinngAddId = new map<Id,Id>();
        set<Id> setAllAccounts = new Set<Id>();
        for(Address__c add : lstNew){
            //checking if the record is for insert or the primary is getting updated i.e. getting checked and account & type should be populated as well
            //In case we have the requirement we can add the check to the account change as well and add both previous and current account in setAllAccounts
            if((!isUpdate || 
                mapOld.get(add.Id).Primary_Address_Flag__c != add.Primary_Address_Flag__c ||
                mapOld.get(add.Id).Address1__c != add.Address1__c ||
                mapOld.get(add.Id).Address2__c != add.Address2__c ||
                mapOld.get(add.Id).City__c != add.City__c ||
                mapOld.get(add.Id).State_Province__c != add.State_Province__c ||
                mapOld.get(add.Id).PostalCode__c != add.PostalCode__c ||
                mapOld.get(add.Id).Country__c != add.Country__c || 
                mapOld.get(add.Id).Type__c != add.Type__c ||
                mapOld.get(add.Id).Tax_Exemption_Status__c != add.Tax_Exemption_Status__c || //RH 9/8/16 I-234345
                mapOld.get(add.Id).Location_ID__c != add.Location_ID__c) //NB - 09/01 - I-232639
               && add.Account__c != null && add.Type__c != null 
               && add.Business_Unit_Name__c == 'COMM'){ //KM 9/15/16 Added business unit for COMM
                setAllAccounts.add(add.Account__c);
                //Checking for the billing type
                if(add.Type__c == Constants.BILLING && add.Primary_Address_Flag__c){    
                    mapAccIdToBillingAddId.put(add.Account__c,add.Id);
                }
                else if(add.Type__c == Constants.SHIPPING && add.Primary_Address_Flag__c){
                    mapAccIdToShippinngAddId.put(add.Account__c,add.Id);
                }  
            }
        }
        system.debug('>>>> after loop, list of accounts>>>>'+setAllAccounts);
        //Check if there are any accounts corresponding to which the address have been updated, if not return
        if(setAllAccounts.isEmpty() && setAllAccounts.size() > 0) //NB - 09/01 - I-232639
            return;
        
        //We need to update the address which is already there and is set as primary
        List<Address__c> lstAddressToUpdate = new List<Address__c>();
        map<Id,Account> mapAccountsToUpdate = new map<Id,Account>();
        map<Id,List<Address__c>> mapAccountIdToAddresses = new map<Id,List<Address__c>>();
        Account acc;
        //rh 9/8/16  I-234345: added tax excemption status below
        //rh 11/1/16 I-tbd: added where clause for BU = COMM;
        for(Address__c add : [SELECT Id,Primary_Address_Flag__c,Type__c,Account__c,Address1__c,Address2__c,
                              City__c,State_Province__c,PostalCode__c,Country__c,Location_ID__c, 
                              Tax_Exemption_Status__c
                              FROM Address__c
                              WHERE Account__c IN:setAllAccounts
                              AND (Type__c =:Constants.BILLING 
                                   OR Type__c =:Constants.SHIPPING)
                              AND Primary_Address_Flag__c = true
                              AND Business_Unit_Name__c = 'COMM'
                              ORDER BY LastModifiedDate DESC]){
            if(!mapAccountIdToAddresses.containsKey(add.Account__c)){
                mapAccountIdToAddresses.put(add.Account__c,new List<Address__c>()); 
            }
            mapAccountIdToAddresses.get(add.Account__c).add(add);
        }
        system.debug('>> addresses to update>'+mapAccountIdToAddresses);
        //This boolean variables will ensure that the addresses have been updated, if not we need to set the corresponding address to null
        Boolean isShippingUpdated = false, isBillingUpdated = false;
        for(Id accId : setAllAccounts){
            //Resetting the variables
            isShippingUpdated = false;
            isBillingUpdated = false;
            if(!mapAccountsToUpdate.containsKey(accId)){
                //Adding the account key for billing or shipping address
                mapAccountsToUpdate.put(accId,new Account(Id=accId));
            }
            //Fetching the object from the map
            acc = mapAccountsToUpdate.get(accId);
            //If there isn't a key for account in mapAccountIdToAddresses this indicates that there are no primary billing or shipping address
            if(mapAccountIdToAddresses.containsKey(accId)){
                for(Address__c add : mapAccountIdToAddresses.get(accId)){
                    //First validate that we have a new primary address for the type that the current address is fetched
                    if(add.Type__c == Constants.BILLING){
                        isBillingUpdated = true;
                        if(mapAccIdToBillingAddId.containsKey(add.Account__c)){
                            if(mapAccIdToBillingAddId.get(add.Account__c) != add.Id){
                                lstAddressToUpdate.add(new Address__c(Id= add.Id,Primary_Address_Flag__c = false));
                            }
                            else{
                                updateBillingAddress(add,acc);
                            }
                        }
                        else{
                            //putting the first record for billing as the address
                            mapAccIdToBillingAddId.put(add.Account__c,add.Id);
                            updateBillingAddress(add,acc);
                        }
                    }
                    else if(add.Type__c == Constants.SHIPPING){
                        isShippingUpdated = true;
                        if(mapAccIdToShippinngAddId.containsKey(add.Account__c)){
                            if(mapAccIdToShippinngAddId.get(add.Account__c) != add.Id){
                                lstAddressToUpdate.add(new Address__c(Id= add.Id,Primary_Address_Flag__c = false));
                            }
                            else{
                                updateshippingAddress(add,acc);
                            }
                        }
                        else{
                            //putting the first record for billing as the address
                            mapAccIdToShippinngAddId.put(add.Account__c,add.Id);
                            updateshippingAddress(add,acc);
                        }
                    }
                }
                
                if(!isShippingUpdated){
                    updateshippingAddress(null,acc);
                }
                if(!isBillingUpdated){
                    updateBillingAddress(null,acc);
                }
            }
            else{
                //This indicates that we need to set the billing and shipping address to null
                updateBillingAddress(null,acc);
                updateshippingAddress(null,acc);
            }
        }
        
        //After validating we need to update the address and account records
        List<Account> lstAccountWithAddressErrors = new List<Account>();
        List<Database.SaveResult> sr = null;
        if(!mapAccountsToUpdate.keySet().isEmpty()){
            sr = Database.update(mapAccountsToUpdate.values(),false);
            for(Integer i=0;i<sr.size();i++){
                if(!sr.get(i).isSuccess()){
                    lstAccountWithAddressErrors.add(new Account(id = mapAccountsToUpdate.values().get(i).Id,Has_Wrong_Primary_Checked__c = true));
                }
            }
            
            //update mapAccountsToUpdate.values();
            Database.update(lstAddressToUpdate,false);
        }
        
        if(!lstAccountWithAddressErrors.isEmpty()){
            //updating the flag for system admin reporting
            update lstAccountWithAddressErrors;
        }
    }
    
    //Modified by Kanika Mathur to replace standard address fields by custom address fields for COMM: I-235328
    private static void updateBillingAddress(Address__c add, Account acc){
        if(add != null){
            acc.COMM_Billing_Address__c = (add.Address1__c != null ? add.Address1__c + ' ' : '') +  (add.Address2__c != null ? add.Address2__c : '');
            acc.COMM_Billing_City__c = add.City__c;
            acc.COMM_Billing_State_Province__c = add.State_Province__c;
            acc.COMM_Billing_PostalCode__c = add.PostalCode__c;
            acc.COMM_Billing_Country__c = add.Country__c;
            //Modified By - Deepti Maheshwari (07/04/2016 Ref : I-224956)
            acc.COMM_BillTo_Location_ID__c  = add.Location_ID__c;
        }
        else{
            acc.COMM_Billing_Address__c =  null;
            acc.COMM_Billing_City__c = null;
            acc.COMM_Billing_State_Province__c = null;
            acc.COMM_Billing_PostalCode__c = null;
            acc.COMM_Billing_Country__c = null;
            acc.COMM_BillTo_Location_ID__c  = null;
        }
    }
    
    //Modified by Kanika Mathur to replace standard address fields by custom address fields for COMM: I-235328
    private static void updateshippingAddress(Address__c add, Account acc){
        system.debug('>> shipping addres to update>>'+add+acc);
        if(add != null){
            acc.COMM_Shipping_Address__c = (add.Address1__c != null ? add.Address1__c + ' ' : '') +  (add.Address2__c != null ? add.Address2__c : '');
            acc.COMM_Shipping_City__c = add.City__c;
            acc.COMM_Shipping_State_Province__c = add.State_Province__c;
            acc.COMM_Shipping_PostalCode__c = add.PostalCode__c;
            acc.COMM_Shipping_Country__c = add.Country__c;
            //Modified By - Deepti Maheshwari (07/04/2016 Ref : I-224956)
            acc.COMM_ShipTo_Location_ID__c = add.Location_ID__c;
            acc.Tax_Exemption_Status__c = add.Tax_Exemption_Status__c;  //RH 9/8/16 I-234345
        }
        else{
            acc.COMM_Shipping_Address__c =  null;
            acc.COMM_Shipping_City__c = null;
            acc.COMM_Shipping_State_Province__c = null;
            acc.COMM_Shipping_PostalCode__c = null;
            acc.COMM_Shipping_Country__c = null; 
            acc.COMM_ShipTo_Location_ID__c = null;
        }
    }
    
    // Methods to be called after record insert and after update
    // @pupose : I-225660
    // @description : Used to update parent account on billing address type
    // @param mapOld - map to hold Trigger.oldMap 
    // @param lstNew - List to hold Trigger.new 
    // @return Nothing 
    // Update 08 Nov 2016 RH I-243410; restricted to only COMM addresses.
    private static void createAccountHierarchyOnAddress(List<Address__c> lstNew, map<Id,Address__c> mapOld){
        Boolean isUpdate = mapOld != null ? true : false;
        Set<Id> setAccountIds = new Set<Id>();
        Set<String> setSharedAccountIds = new Set<String>();
        for(Address__c address : lstNew){
            if((!isUpdate || 
            (address.Primary_Address_Flag__c != mapOld.get(address.Id).Primary_Address_Flag__c)) 
            && address.Account__c != null 
            && address.Shared_from_Account__c != null 
            && address.Type__c ==  Constants.BILLING 
            && address.Business_Unit_Name__c == 'COMM'
            && address.Primary_Address_Flag__c){
                setSharedAccountIds.add(address.Shared_from_Account__c);
                setAccountIds.add(address.Account__c);
            }
        }
        
        if(setSharedAccountIds.isEmpty() || setAccountIds.isEmpty()){
            return;
        }
        
        map<String,Account> mapExternalIdToAccount = new map<String,Account>();
        map<Id,Account> mapAccountIdToAccount = new map<Id,Account>();
        for(Account acc : [SELECT Id,Level__c,ExternalId__c,RecordType.Name,parentId FROM Account
                           WHERE ((ExternalId__c IN : setSharedAccountIds AND Level__c = :Constants.SYSTEM_LEVEL)
                                  OR (Id IN : setAccountIds AND Level__c = :Constants.FACILITY_LEVEL))
                           AND RecordType.Name LIKE '%COMM%']){
            //We will check if it's an external Id account by checking our set
            if(setSharedAccountIds.contains(acc.ExternalId__c)){ 
                if(acc.Level__c == Constants.SYSTEM_LEVEL){
                    mapExternalIdToAccount.put(acc.ExternalId__c,acc);
                }
            }
            
            if(setAccountIds.contains(acc.Id)){
                if(acc.Level__c == Constants.FACILITY_LEVEL){
                    mapAccountIdToAccount.put(acc.Id,acc);
                }
            }
        }
        List<Account> lstAccountsToUpdate = new List<Account>();
        for(Address__c address : lstNew){
            if((!isUpdate || (address.Primary_Address_Flag__c != mapOld.get(address.Id).Primary_Address_Flag__c)) 
               && address.Account__c != null 
               && address.Shared_from_Account__c != null 
               && address.Type__c ==  Constants.BILLING
               && address.Business_Unit_Name__c == 'COMM'
               && address.Primary_Address_Flag__c){
                if(mapExternalIdToAccount.containsKey(address.Shared_from_Account__c) &&  mapAccountIdToAccount.containsKey(address.Account__c)){
                    //Excluding the circular account hierarchy because of wrong integration data
                    if(mapAccountIdToAccount.get(address.Account__c).Id != mapExternalIdToAccount.get(address.Shared_from_Account__c).ParentId){ 
                        lstAccountsToUpdate.add(new Account(Id = address.Account__c,ParentId = mapExternalIdToAccount.get(address.Shared_from_Account__c).Id));
                    }
                }
            }
        }
        
        if(!lstAccountsToUpdate.isEmpty()){
            update lstAccountsToUpdate;
        }
    }
    // 28 Sep 2016 , Isha Shukla , T-541552 - Added method syncAddressWithSVMXSite to Sync Up Address fields with Location fields.
    private static void syncAddressWithSVMXSite(List<Address__c> lstNew, map<Id,Address__c> mapOld){
        Map<String, SVMXC__Site__c> existingSites = new Map<String, SVMXC__Site__c>();
        Set<String> setLocationIds = new Set<String>();
        
        for(Address__c a : lstNew){
           if(a.Location_ID__c != null){
             setLocationIds.add(a.Location_ID__c);
           }
        }
        
        if(setLocationIds.size() > 0){
            for(SVMXC__Site__c s : [SELECT Id, Location_ID__c 
                                    FROM SVMXC__Site__c
                                    WHERE Location_ID__c IN :setLocationIds]){
                existingSites.put(s.Location_ID__c, s);
            }
        }
        
        List<SVMXC__Site__c> lstNewSites = new List<SVMXC__Site__c>();
        List<SVMXC__Site__c> lstUpdatedSites = new List<SVMXC__Site__c>();
        for(Address__c a : lstNew){
            SVMXC__Site__c s = new SVMXC__Site__c();
            if(existingSites.containsKey(a.Location_ID__c)){
                s = existingSites.get(a.Location_ID__c);
            }
            else if (a.Status__c != 'Active'){
                // If the Address is not active, do not create a new one. As per above, it would still be updated
                continue;
            }
            s.Name = a.Name;
            s.SVMXC__Street__c = a.Address1__c + ' ' + a.Address2__c;
            s.SVMXC__City__c = a.City__c;
            s.SVMXC__State__c = a.State_Province__c;
            s.SVMXC__Country__c = a.Country__c;
            s.SVMXC__Zip__c = a.PostalCode__c;
            s.Location_ID__c = a.Location_ID__c;
            s.SVMXC__Account__c = a.Account__c;
            s.SVMXC__Site_Phone__c = a.Phone__c;
            s.SVMXC__Location_Type__c = a.Type__c;
            
            if(existingSites.containsKey(a.Location_ID__c)){
                lstUpdatedSites.add(s);
                System.debug('lstUpdatedSites--if'+lstUpdatedSites);
            }
            else{
                lstNewSites.add(s);
                System.debug('lstNewSites--if'+lstNewSites);
            }
        }
         System.debug('@@##' + lstNewSites);
        if(!lstNewSites.isEmpty()){
         System.debug('@@##' + lstNewSites);
            insert lstNewSites;
        }
        
        if(!lstUpdatedSites.isEmpty()){
                 System.debug('@@##' + lstUpdatedSites);
            update lstUpdatedSites;
        }
    }
}