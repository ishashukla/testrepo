/*************************************************************************************************
* Created By:    Gagandeep Brar (Stryket IS)
* Date:          April 4th , 2016
* Description  : Email to Case Trigger - after insert that updates case contact on a case with devision
*                 specific contact
*                 Handler classs for Trigger Stryker_EmailtoCaseTrigger
* 
* 04 Apr 2016    Gagan        Class created with logic for Endo and Medical  [Original]
* 09 Jun 2016    Pratibha     Added method to Alert a customer when they send an email to an agent who is not available
* 25 Aug 2016    Gagan        Updated to include locig for Instrument
**************************************************************************************************/

  public class Stryker_EmailtoCaseTriggerHandler {
  //----------------------------------------------------------------------------------------------
  //  Method to call After Insert  
  //----------------------------------------------------------------------------------------------
  public static void onAfterInsert(List<EmailMessage> ListNewEmail){
      updateNewEmailToCaseContact(ListNewEmail);
      sendEmailtoContact(ListNewEmail);
   }
   
   //----------------------------------------------------------------------------------------------
  //  Alert a customer when they send an email to an agent who is not available 
  //---------------------------------------------------------------------------------------------- 
   public static void sendEmailtoContact(List<EmailMessage> ListNewEmail){ 
      
       List<Id> casesToUpdate = new List<Id>();
       
       for(EmailMessage newEmail : ListNewEmail){
           casesToUpdate.add(newEmail.ParentId);
       }
       
       system.debug('casesToUpdate' + casesToUpdate);
       if(casesToUpdate.size() > 0){
           Set<String> endoRecordTypes = new Set<String>{'Repair_Order_Billing', 'RMA_Order_Entry', 'Sales_Operations', 'Sales_Order', 
                   'Sample_Refurb_Sale_Sample_Sale', 'Samples_Audit', 'Samples_Request', 'Tech_Support', 'Customer_Inquiry_Requests'}; 
           
           // Add label for email template name
           EmailTemplate templateId = [Select id,name from EmailTemplate where name = :Label.Auto_Response_if_Customer_Responds_While_Agent_Unavailable];
           
           // OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress where DisplayName= ''];
           List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
           
           For(Case caseFirst : [Select Id, status, contactId, Contact.Email, Owner_Status__c, RecordType.DeveloperName 
                                From Case 
                                where id IN :casesToUpdate and RecordType.DeveloperName IN :endoRecordTypes]){
               if(caseFirst.ContactId != null && (caseFirst.Owner_Status__c == 'Logged Off')){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateID(templateId.Id); 
                    mail.setSaveAsActivity(false);
                    //mail.setOrgWideEmailAddressId(owa.id);
                    mail.setTargetObjectId(caseFirst.contactId);
                    mail.setWhatId(caseFirst.Id);
                    allmsg.add(mail);
               }       
           }
           
           if(!allmsg.isEmpty()){
             Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg,false);
             if (results[0].success) { 
                System.debug('The email was sent successfully.'); 
             } 
             else 
             { 
                 System.debug('The email failed to send: ' + results[0].errors[0].message); 
             }
           }
       }
              
   }
  //----------------------------------------------------------------------------------------------
  //  Update Contact on new cases created vai EmailtoCase   
  //----------------------------------------------------------------------------------------------
  public static void updateNewEmailToCaseContact(List<EmailMessage> ListNewEmail){
      
      List<Case> casesToUpdate = new List<Case>();
      
      for(EmailMessage newEmail : ListNewEmail){
        List<EmailMessage> CaseEmailCount = [SELECT Id, MessageDate, ParentId, Status FROM EmailMessage WHERE ParentId =: newEmail.ParentId];

        // Check if e-mail was an auto-response sent from Endo CustomerCare team with Status of "Sent" (3) or if it is an e-mail with Status of "New" (0) 
        if ((!String.isBlank(newEmail.Subject) && newEmail.Subject.containsIgnoreCase(Constants.EMAIL_SUBJECT_ENDO_CASE_OPENED) && newEmail.Status == '3') || (newEmail.Status == '0' && CaseEmailCount.size() == 1)){
          case c = new case();  
          List<Case> caselist = [SELECT id, AccountId, Description, caseNumber, RecordTypeId, ContactId, Origin, SuppliedEmail FROM Case WHERE Id =: newEmail.ParentId AND Status = 'New' AND Origin IN ('Email','Email-to-Case')];
          if (caselist.size() > 0){
            c = caselist[0];
          } 
          
          if (c.id != null  && c.SuppliedEmail != null){
                // Set contact e-mail to the "To Address" if the last e-mail sent was from the Endo CustomerCare Team
                String ContactEmail;
                if (newEmail.Status == '3') {
                    ContactEmail = newEmail.ToAddress;              
                } else {
                    ContactEmail = newEmail.FromAddress;
                }

                List<Contact> ContactList = [SELECT Id, RecordTypeId, AccountId FROM Contact WHERE RecordTypeId IN :ContactRecordTypes(c.RecordTypeId) AND Email =: ContactEmail];
                if(ContactList.size() == 1 && c.ContactId != ContactList[0].id){
                    c.ContactId = ContactList[0].id;
                    c.AccountId = ContactList[0].AccountId;
                    casesToUpdate.add(c);
                }
                if (c.id != null && ContactList.size() != 1 && c.ContactId != null)
                {
                    c.ContactId = null;
                    c.AccountId = null;
                    casesToUpdate.add(c);
                }
            }
            
          if(casesToUpdate.size() > 0){
            UPDATE casesToUpdate;  
          }
        }
      }
    }
    
    //----------------------------------------------------------------------------------------------
    //  Find devision specific contacts Record type per the case record type
    //----------------------------------------------------------------------------------------------
    public static Set<Id> ContactRecordTypes(Id CaseRecordTypeId){      
        
        RecordType NewCaseRecordType = [SELECT DeveloperName, Id FROM RecordType WHERE Id =: CaseRecordTypeId];
        System.debug('***GAGAN*** NewCaseRecordType: \n'+NewCaseRecordType);
        
        Map<String, Set<String>> CaseTypesbyDivision = new Map<String, Set<String>>();
        CaseTypesbyDivision.put('Endo', new Set<String>{'Repair_Order_Billing', 'RMA_Order_Entry', 'Sales_Operations', 'Sales_Order', 'Sample_Refurb_Sale_Sample_Sale', 'Samples_Audit', 'Samples_Request', 'Tech_Support', 'Customer_Inquiry_Requests'});
        CaseTypesbyDivision.put('Medical', new Set<String>{'Medical_Call_Center', 'Medical_Call_Center_Case_Type', 'Medical_Employee_Complaint'});
        CaseTypesbyDivision.put('Instrument', new Set<String>{'Instruments_Employee_Complaint', 'Instruments_Non_Product_Issue', 'Instruments_Potential_Product_Complaint', 'Instruments_Repair_Depot', 'Instruments_Repair_Field_Service'});
        CaseTypesbyDivision.put('COMM', new Set<String>{'COMM_Change_Request','COMM_Customer_Service','COMM_Employee_Complaint','Sales_Operations_Support','COMM_System_Support_Case','COMM_US_Complaint_Form','COMM_US_Stryker_Field_Service_Case','Customer_Inquiry_Requests'});
        
        String SelectDivision = null;
        for (String div : CaseTypesbyDivision.keySet()){
            Set<String> DivTest1 = CaseTypesbyDivision.get(div);
            System.debug('***GAGAN*** DivTest1: \n'+DivTest1);
            
            if(DivTest1.contains(NewCaseRecordType.DeveloperName) == TRUE){
                SelectDivision = div;
            }
        }
        System.debug('***GAGAN*** SelectDivision: \n'+SelectDivision);
        
        Map<String, Set<String>> ContactTypesbyDivision = new Map<String, Set<String>>();
        ContactTypesbyDivision.put('Endo', new Set<String>{'Endo_Customer_Contact', 'Endo_Employee', 'Endo_Internal_Contact'});
        ContactTypesbyDivision.put('Medical', new Set<String>{'Medical_Customer_Contact', 'Medical_Employee_Contact'});
        ContactTypesbyDivision.put('Instrument', new Set<String>{'Instruments_Contact'});
        ContactTypesbyDivision.put('COMM', new Set<String>{'COMM_Contact'});
        ContactTypesbyDivision.put('COMM', new Set<String>{'COMM_Private_Contact'});
        
        Set<Id> relatedRecordIds = new Set<Id>();
        if (SelectDivision != null){
            for(RecordType crid :[SELECT DeveloperName, Id FROM RecordType WHERE DeveloperName IN :ContactTypesbyDivision.get(SelectDivision)]){
                relatedRecordIds.add(crid.Id);
            }    
        }
        System.debug('***GAGAN*** relatedRecordIds: \n'+relatedRecordIds);
        Return relatedRecordIds;
    }
}