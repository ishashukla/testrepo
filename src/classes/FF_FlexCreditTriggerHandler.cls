/*************************************************************************************************
Created By:    Sunil Gupta
Date:          June 09, 2014
Description:   Handler class for Flex Credit Trigger

// Revision History:
// 2015-08-25   Sunil;  Update opportunity winning bank field when credit status is approved
**************************************************************************************************/
public class FF_FlexCreditTriggerHandler {

  /* update Last Recorded Time, Last_Recorded_Time__c is using in validation rule so setting in after trigger */
  public static void updateSatusChangeTime(Map<Id, Flex_Credit__c> mapOld, List<Flex_Credit__c> lstNew) {
    // Update Last Recorded Time whenever Status is Changed.
    List<Flex_Credit__c> lstToUpdate = new List<Flex_Credit__c>();
    for(Flex_Credit__c fc :lstNew){
      if(mapOld == null  || (fc.Credit_Status__c != mapOld.get(fc.Id).Credit_Status__c)){
        Flex_Credit__c fcNew = new Flex_Credit__c(Id = fc.Id);
        fcNew.Last_Recorded_Time__c = System.now();
        System.debug('@@@');
        lstToUpdate.add(fcNew);
      }
    }
    update lstToUpdate;
  }


  /* Update Business Hours when Credit Status is Changed */
  public static void updateBusinessHours(Map<Id, Flex_Credit__c> mapOld, List<Flex_Credit__c> lstNew) {

    for(Flex_Credit__c fc :lstNew){
        if(mapOld != null  && (fc.Credit_Status__c != mapOld.get(fc.Id).Credit_Status__c)){
            // When record is edited and Status changed from New to something else
            if(mapOld != null && mapOld.get(fc.Id).Credit_Status__c == 'New'){
                System.debug('@@@');
                calculateBusinessHrs(fc);
            }
            // When record is edited and Status changed from Pending Lender Decision to something else
            if(mapOld != null && mapOld.get(fc.Id).Credit_Status__c == 'Pending Lender Decision'){
              System.debug('@@@');
              calculateBusinessHrs(fc);
            }
        }
    }

    // Update Last Recorded Time whenever Status is Changed.
    //for(Flex_Credit__c fc :lstNew){
    //  if(mapOld == null  || (fc.Credit_Status__c != mapOld.get(fc.Id).Credit_Status__c)){
    //    fc.Last_Recorded_Time__c = System.now();
    //  }
    //}
  }

  private static BusinessHours flexBusinessHours{
    get{
        if(flexBusinessHours == null){
            flexBusinessHours = [SELECT Id FROM BusinessHours WHERE name = 'Flex Business Hours'];
        }
        return flexBusinessHours;
    }
  }

  private static void calculateBusinessHrs(Flex_Credit__c fc){
    //8:00 AM to 5:00 PM EST
    // Monday - Friday only
    System.debug('@@@' + fc.Business_Hours_Aging__c);
    Datetime pastTime = fc.Last_Recorded_Time__c;//Datetime.newInstance(2014, 6, 10, 9, 0, 0);
    DateTime currentTime = System.now();//Datetime.newInstance(2014, 6, 10, 10, 0, 0);
    System.debug('@@@' + pastTime);
    System.debug('@@@' + currentTime);

    Long millSeconds = BusinessHours.diff(flexBusinessHours.Id, pastTime, currentTime);
    System.debug('@@@' + millSeconds);
    Integer secondDiff = (Integer)(millSeconds/1000);
    System.debug('@@@' + secondDiff);
    System.debug('@@@' + pastTime);
    System.debug('@@@' + currentTime);
    if(fc.Business_Hours_Aging__c == null){
      fc.Business_Hours_Aging__c = 0;
    }
    System.debug('@@@' + fc.Business_Hours_Aging__c);
    fc.Business_Hours_Aging__c = fc.Business_Hours_Aging__c + secondDiff;
    System.debug('@@@' + fc.Business_Hours_Aging__c);
  }


  /* Update opportunity stage when credit status is declined */
  public static void updateOpportunityStage(Map<Id, Flex_Credit__c> mapOld, List<Flex_Credit__c> lstNew) {
    // set of eligible Opportunities.
    Set<Id> setOpsClosed = new Set<Id>();
    Set<Id> setOpsDealReview = new Set<Id>();
    for(Flex_Credit__c fc :lstNew){
      if(mapOld == null || (fc.Credit_Status__c != mapOld.get(fc.Id).Credit_Status__c)){
        if(fc.Credit_Status__c == 'Declined'){
          setOpsClosed.add(fc.Opportunity__c);
        }

        else if(fc.Credit_Status__c == 'Approved' || fc.Credit_Status__c == 'Contingent Approval'){
          setOpsDealReview.add(fc.Opportunity__c);
        }
      }
    }
    system.debug('@@@' + setOpsClosed);

    Set<Id> setOps = new Set<Id>();
    setOps.addAll(setOpsDealReview);
    setOps.addAll(setOpsClosed);

    List<Opportunity> lstOpps = [SELECT Id, StageName FROM Opportunity WHERE Id IN :setOps];
    for(Opportunity opp :lstOpps){
    	if(setOpsClosed.contains(opp.Id)){
    	  opp.StageName = 'Closed Lost';
        opp.Closed_Lost_Reason__c = 'Credit Declined';
    	}
    	else if(setOpsDealReview.contains(opp.Id)){
    		opp.StageName = 'Deal Review';
    	}
    }
    update lstOpps;
  }





  /* Update opportunity winning bank field when credit status is approved */
  public static void updateOpportunityWinningBank(Map<Id, Flex_Credit__c> mapOld, List<Flex_Credit__c> lstNew) {
    // set of eligible Opportunities.
    Map<Id, String> mapWinningBank = new Map<Id, String>();
    for(Flex_Credit__c fc :lstNew){
      if(mapOld == null || (fc.Credit_Status__c != mapOld.get(fc.Id).Credit_Status__c)){
        if(fc.Credit_Status__c == 'Approved'){
          mapWinningBank.put(fc.Opportunity__c, fc.Winning_Bank__c);
        }
      }
    }
    system.debug('@@@' + mapWinningBank);

    List<Opportunity> lstOpps = [SELECT Id, StageName FROM Opportunity WHERE Id IN :mapWinningBank.keySet()];
    for(Opportunity opp :lstOpps){
      opp.Winning_Bank__c = mapWinningBank.get(opp.Id);
    }
    system.debug('@@@' + lstOpps);
    update lstOpps;
  }
}