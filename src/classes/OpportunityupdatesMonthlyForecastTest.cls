//
// (c) 2016, Appirio Inc.
//
// Test Call for HE_BatchPopulateAccountShare
//
// 19 Feb 2016     Gagandeep Kaur       Original - T-490523 : Batch process to calculate PFP Volume Points
//
@isTest(seealldata = false)
private class OpportunityupdatesMonthlyForecastTest {
  static List<Opportunity> opp;
  static List<Account> acc;
  static List<Forecast_Year__c> forYear;
  static List<Monthly_Forecast__c> monthfor;
  static User adminUser;
  static testMethod void testBatch() {
    createUser();
    system.runAs(adminUser) {         
        createTestData();
        Test.startTest();
        OpportunityupdatesMonthlyForecastbatch oppty = new OpportunityupdatesMonthlyForecastbatch();
        Database.executeBatch(oppty,1);
        Test.stopTest();
    }
  }
  static void createUser() {
    adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
    insert adminUser;
  }
  static void createTestData() {
    acc = TestUtils.createAccount(1,True);
    opp = TestUtils.createOpportunity(1,acc[0].id,false);
    opp[0].StageName = 'Forcasted';
    opp[0].Ownerid = adminUser.id;
    opp[0].Amount = 50;
    opp[0].CloseDate = Date.newinstance(2016,12,25);
    insert opp[0];
    Id devRecordTypeId = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast').getRecordTypeId();
    forYear = TestUtils.createForcastYear(1,adminUser.id,devRecordTypeId,true);
    monthfor = TestUtils.createMonthlyForecast(1,forYear[0].id,true);
  }
}