/**================================================================      
* Appirio, Inc
* Name: ChecklistDeepCloneController
* Description: clone Checklist Header and related Checklist deatil
* Created Date: 06-09-2016 
* Created By: Shubham Dhupar (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/

global class ChecklistItemAndGroupDeepCloneController {
    //============================================================================================     
    // Name         : cloneChecklist
    // Description  : To Clone Checklist and related groups and items, and update items with
    //                newly created groups
    // Created Date : 06 Sep 2016 
    // Created By   : Shubham Dhupar (Appirio)
    // Task         : T-536950
    //============================================================================================
    webService static String cloneChecklist(ID checklistid) 
    {       
        System.debug('------------------        '+ checklistid);
        ChecklistCUST__c Newcheck = deepClone(checklistid);
        return Newcheck.Id;
    }
    
    webService static SVMXC.INTF_WebServicesDef.INTF_Response cloneChecklistForSVMX(SVMXC.INTF_WebServicesDef.INTF_Request request) 
    {       
        SVMXC.INTF_WebServicesDef.INTF_Response obj = new SVMXC.INTF_WebServicesDef.INTF_Response(); 
        String recordId; 
        for(SVMXC.INTF_WebServicesDef.SVMXMap objSVXMMap : request.valueMap) 
        { 
            if(objSVXMMap.key == 'SVMX_RECORDID') 
            { 
                 recordId = objSVXMMap.value; 
            } 
        } 
        ChecklistCUST__c check = Database.query(queryAllObjectFields('ChecklistCUST__c')  + ' WHERE Id = \'' + recordId +'\' Limit 1');
        ChecklistCUST__c Newcheck = deepClone(recordId);
        List<ChecklistCUST__c> checklistHeader = [SELECT Name FROM ChecklistCUST__c WHERE Id = :Newcheck.id];
        SVMXC.INTF_WebServicesDef.SVMXMap sObj = new SVMXC.INTF_WebServicesDef.SVMXMap(); 
        sObj.record = Newcheck; 
        obj.valueMap = new List<SVMXC.INTF_WebServicesDef.SVMXMap>(); 
        obj.valueMap.add(sObj); 
        obj.Message = 'Checklist ' + check.name + ' cloned to : ' + checklistHeader.get(0).Name;
        obj.messageType = 'INFO';
        obj.success = true;
        return obj;
    }
    
    public Static ChecklistCUST__c deepClone(Id recordId) {
        List<Checklist_Group__c	> newcheckList = new list<Checklist_Group__c>();
        List<Checklist_item__c> newItemList = new List<Checklist_Item__c>();
        ChecklistCUST__c check = Database.query(queryAllObjectFields('ChecklistCUST__c')  + ' WHERE Id = \'' + recordId +'\' Limit 1');
        List<Checklist_Group__c	> checkList = Database.query(queryAllObjectFields('Checklist_Group__c')  + ' WHERE Checklist__c = \'' + recordId +'\'');
        List<Checklist_item__C> checkItem = Database.query(queryAllObjectFields('Checklist_item__c')  + ' WHERE Checklist__c = \'' + recordId +'\'');  
               
        ChecklistCUST__c Newcheck = check.clone(false, true);
        Insert Newcheck;
        List<ChecklistCUST__c> checklistHeader = [SELECT Name FROM ChecklistCUST__c WHERE Id = :Newcheck.id];
        for (Checklist_Group__c	 woCheck: checkList){
            Checklist_Group__c	 newWoCheck  = woCheck.clone(false, true);
            newWoCheck.Checklist__c = Newcheck.Id;
            newcheckList.add(newWoCheck);
        }
        Insert newcheckList;
        Map<id,id> oldNewGroupmap = new Map<id,id>();
        for(Checklist_Group__c oldGroup: [Select id,name from Checklist_Group__c where checklist__c =:recordId]) {
          for(Checklist_Group__c newGroup: newcheckList) {
            if(oldgroup.Name == newGroup.name) {
              oldNewGroupmap.put(oldGroup.id,newGroup.id);
            }
          }
        }
        
        Id groupname;
        for(Checklist_item__C checkI: checkItem){
          Checklist_item__C	iCheck  = checkI.clone(false, true);
          iCheck.Checklist__c = Newcheck.Id;
          groupname = iCheck.Checklist_Group__c;
          iCheck.Checklist_Group__c = oldNewGroupmap.get(groupname);
           System.debug('ID in Item   ' + iCheck.Checklist__c);
          newItemList.add(iCheck);
        }
        Insert newItemList;     
        return Newcheck;
    }
    Static string queryAllObjectFields(String SobjectApiName){
        String query = '';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
      
            
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
 
        query = 'SELECT ' + commaSepratedFields + ' FROM ' + SobjectApiName;
        return query;
    }
}