/**================================================================      
* 2016 Appirio, Inc
* Name: COMM_CaseTriggerHandlerTest
* Description: Test class for COMM_CaseTriggerHandler Class
* Created Date: 23 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class COMM_CaseTriggerHandlerTest {
    static List<Case> caseList;
    static Order orderRecord;
    static Order orderRecord2;
    static Id rtCase2;
    static User userRecord;
    static User OppMem1;
    static User OppMem2;
    static List<Opportunity> oppList2;
    @isTest static void testTrigger() {
        createData();
        Test.startTest();
        insert caseList;
        Test.stopTest();
        String check = [SELECT Ship_To_Address_for_pickup__c FROM Case WHERE Id = :caseList[0].Id].Ship_To_Address_for_pickup__c;
        System.assertEquals(check.contains(orderRecord.ShippingStreet), true);
    }
    @isTest static void testTriggerSecond() {
        createData();
        Test.startTest();
        insert caseList;
        Id workOrderRTId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Estimate').getRecordTypeId();
        SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Order_Status__c = 'Open',
                                                                        SVMXC__Case__c = caseList[0].Id,
                                                                        RecordTypeId = workOrderRTId);
        insert workOrder;
        caseList[0].Status = 'Closed';
        caseList[1].Status = 'New';
        caseList[2].Type = 'No Charge Add';
        caseList[2].POC_NCA_only__c = 'test';
        caseList[2].Shipping_Method__c = 'FW-LTL-Standard';
        caseList[3].RecordTypeId = rtCase2;
        Database.SaveResult[] srlist = Database.update(caseList,false);
        Test.stopTest();
        for(Database.SaveResult sr : srlist) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    System.assertEquals('Open Work Orders must be completed before Case can be closed.', err.getMessage());
                }
            }
        }
    }
    @isTest static void testTriggerActivity() {
        createData();
        EmailTemplate template;
        System.runas(userRecord){
            template = new EmailTemplate (IsActive=true,developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test'); // plus any other fields that you want to set
            insert template;
        }
        COMM_Constant_Setting__c templateId = new COMM_Constant_Setting__c(Case_ReOpen_Template__c = template.Id);
        
        
        for(integer i=0;i<caseList.size();i++) {
            caseList[i].Order__c = orderRecord.Id;
            caseList[i].Status = 'New';
        }
        Test.startTest();
        System.runas(userRecord) {
          caseList[4].Status = 'Closed';
        insert caseList;
        caseList[4].Status = 'New';
        update caseList;  
        }
        
        Test.stopTest();
    } 
    @isTest static void testTriggerThird() {
        createData();
        Test.startTest();
        insert caseList;
        caseList[0].Order__c = orderRecord2.Id;
        caseList[0].Opportunity__c = oppList2[0].Id;
        caseList[0].OwnerId = OppMem1.Id;
        update caseList;
        Test.stopTest();
    }
    private static void createData() {
        userRecord = TestUtils.createUser(1, 'System Administrator', false).get(0);
        OppMem1 = TestUtils.createUser(1, 'Strategic Sales Manager - COMM US', false).get(0);
        OppMem1.Email = 'test@test.com';
        insert OppMem1;
        userRecord.ManagerId = OppMem1.Id;
        insert userRecord;
        OppMem2 = TestUtils.createUser(1, 'Strategic Sales Manager - COMM US', true).get(0);
        Pricebook2 pricebook = new Pricebook2(Name = 'COMM Price Book');
        insert pricebook;
        String rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Change Request').getRecordTypeId();
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('COMM Service Opportunity').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM Change Request');
        insert rtMap;
        rtCase2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Commercial Operations Support').getRecordTypeId();
        RTMap__c rtMap2 = new RTMap__c(Name = rtCase2,
                                       Object_API_Name__c='Case',
                                       Division__c = 'COMM',
                                       RT_Name__c='COMM Commercial Operations Support');
        insert rtMap2;
        Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Account acc = TestUtils.createAccount(1, false).get(0);
        acc.RecordTypeId = accRT;
        acc.Endo_Active__c = true;
        acc.COMM_Shipping_Country__c = 'United States';
        acc.COMM_Shipping_Address__c = 'test';
        acc.COMM_Shipping_State_Province__c  = 'Arizona';
        acc.COMM_Shipping_PostalCode__c = '123';
        acc.COMM_Shipping_City__c = 'testcity';
        insert acc;
        List<Opportunity> oppList = TestUtils.createOpportunity(1, acc.Id, false);
        oppList[0].RecordTypeId = rtOpp;
        insert oppList;
        OpportunityTeamMember newOTM = new OpportunityTeamMember();
        newOTM.OpportunityId = oppList[0].Id;
        newOTM.UserId = OppMem1.Id;
        insert newOTM;
        oppList2 = TestUtils.createOpportunity(1, acc.Id, false);
        oppList2[0].RecordTypeId = rtOpp;
        insert oppList2;
        OpportunityTeamMember newOTM2 = new OpportunityTeamMember();
        newOTM2.OpportunityId = oppList2[0].Id;
        newOTM2.UserId = OppMem2.Id;
        insert newOTM2;
        orderRecord = new Order(Status='Entered',
                                AccountId = acc.Id,
                                EffectiveDate=system.today(),OpportunityId=oppList[0].Id,ShippingStreet='test');
        insert orderRecord;
        orderRecord2 = new Order(Status='Entered',
                                 AccountId = acc.Id,
                                 EffectiveDate=system.today(),OpportunityId=oppList2[0].Id,ShippingStreet='test');
        insert orderRecord2;
        caseList = TestUtils.createCase(10, acc.id, false);
        for(integer i=0;i<caseList.size();i++) {
            caseList[i].Order__c = orderRecord.Id;
            caseList[i].Opportunity__c = oppList[0].Id;
            caseList[i].Status = 'New';
            caseList[i].RecordTypeId = rtCase;
        }
        caseList[1].Related_Orders__c = orderRecord2.Id;
        caseList[1].Status = 'Closed';
        caseList[5].RecordTypeId = rtCase2;
    }
}