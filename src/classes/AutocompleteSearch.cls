global with sharing class AutocompleteSearch {
	webService static List<sObject> find(String sObjName, String field, String q){
		List<sObject> so = Database.query(buildQuery(sObjName, field, q));
		return so;
	}
	private static String buildQuery(String sObjName, String field, String q){
		String query = '';
		query += 'select id, ';
		query += field + ' ';
		query += 'from ';
		query += sObjName + ' ';
		query += 'where ';
		query += field + ' ';
		query += 'like ';
		query +=  '\'' + q + '%\'';
		query += 'limit 10';
		System.debug(query);
		return query;
	}
	   
   /*@isTest static void search() {
   		//insert test data
   		Opportunity o = new Opportunity(name='test', stagename='Qualified', closedate = Date.today());
   		insert o;
   		
   		List<sObject> sobjs = find('Opportunity', 'Name', 'te');
   		Opportunity o2 = (Opportunity)sobjs.get(0);
   		
   		System.assertEquals(o2.name, o.name);
      // Implement test code 
    
   }*/
}