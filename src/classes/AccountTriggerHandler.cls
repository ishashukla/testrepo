/**================================================================      
* Appirio, Inc
* Name: AccountTriggerHandler
* Description: To create new opportunity and close open task. (T-459647)
* Created By :  Nitish Bansal
* Created Date : 02/26/2016
*
* Date Modified      Modified By      Description of the update
*                    Kirti Agarwal    T-477949 (Used to insert account team member for parent account)
*                    Rahul Goyal      T-477692 (used to merge accounts)
*                    Shubham Dhupar   T-477692 (Used to Merge Account with Deleted Accounts owner Emails, Account Name/s) 
*                    Kirti Agarwal    I-210448
* 10 May 2016        Mehul Jain       I-216712 (used to update standard type field based on values from custom type field)
* 11 August 2016     Nitish Bansal    I-229907 - Syncing Type__c and Type for Accounts coming via integration
* 12 August 2016     Kanika Mathur    T-523264 - To update Account FirstName, Last Name, OwnerManager fields according to RT
* 19 August 2016     Kanika Mathur    T-523264 - Added check for Endo COMM Customer RT
* 24 August 2016     Kanika Mathur    T-529374 - To update COMM_Sales_Rep__c field and create manual share record for Sales Rep
* 13 Sept 2016       Varun Vasishtha  T-514084 - To Send a mail for Hold Release
** 17 November 2016   Shubham Dhupar     T-555497 - Added before Delete to restrict Comm System Admin to delete Accounts.
==================================================================*/
public class AccountTriggerHandler {
  Static Id profileId=userinfo.getProfileId();
  Static String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
  public static final string holdTemplate = 'Hold_Released_for_Account_2';
  public static void onBeforeInsert(List<Account> newList) {
    updateStandardType(newList);
    updateCOMMSalesRep(newList, null);
    updateAccountOwner(newList, null);
  }

  public static void onBeforeUpdate(List<Account> newList, Map<Id, Account> oldMap) {
    if(!Constants.isTriggerExecuted) {
        beforeRecordUpdate(oldMap, newList);
        Constants.isTriggerExecuted = true;
      }
    updateStandardType(newList);
    updateCOMMSalesRep(newList, oldMap);
    updateAccountOwner(newList, oldMap);
  }

  public static void onAfterInsert(List<Account> newList, Map<Id, Account> oldMap) {
    beforeRecordInsert(oldMap, newList);
    manualShareForSalesRep(newList, null);
    manualSharesForSalesRepOnParent(newList, null);
  }

  public static void onAfterUpdate(List<Account> newList, Map<Id, Account> oldMap) {
    afterRecordUpdateCOMM(oldMap, newList);
    manualShareForSalesRep(newList, oldMap);
    manualSharesForSalesRepOnParent(newList, oldMap);
    HoldRelesedNotification(newList,oldMap);
    updatePhysicalAddressOnProject(newList,oldMap);
  }
  //================================================================      
    // Name         : onBeforeDelete
    // Description  : To execute Before Delete Methods
    // Created Date : 17th November 2016 
    // Created By   : Shubham Dhupar (Appirio)
    // Task         : T-555497
  //==================================================================
  public static void onBeforeDelete(List<Account> oldList,Map<Id,Account> oldMap) {
    BlockedAccountDeletion(oldList,oldMap);
  }
  public static void onAfterDelete(List<Account> oldList) {
    updateMasterAccount(oldList);
  }
  //================================================================      
    // Name         : BlockedAccountDeletion
    // Description  : To Restrict Comm System Admin from Deleting Account Records
    // Created Date : 17th November 2016 
    // Created By   : Shubham Dhupar (Appirio)
    // Task         : T-555497
  //==================================================================
  public static void BlockedAccountDeletion(List<Account> oldList,Map<Id,Account> oldMap) {
    Map<String, BlockedAccountDeletionProfiles__c> blockedAccountProfiles = BlockedAccountDeletionProfiles__c.getAll();
    for(Account acc: oldList) {
      //System.Debug('Proile is ' +  BlockedAccountDeletionProfiles__c.getOrgDefaults().COMM_System_Admin_Profile__c);
      if(blockedAccountProfiles.containsKey(profileName) && getRecordTypeName(acc.RecordTypeId, 'Account').contains('COMM'))  {
        oldMap.get(acc.id).addError(System.Label.Insufficient_Priveldge);
      }
    }
  }
  public static void updatePhysicalAddressOnProject(List<Account> newList,Map<Id, Account> oldMap) {
    Map<Id,String> accountIdMap = new Map<Id,String>();
    String accountAddress = '';
    for(Account acc: newList) {
      if(getRecordTypeName(acc.RecordTypeId, 'Account').contains('COMM')) {
        if(acc.COMM_Shipping_Address__c !=  oldMap.get(acc.Id).COMM_Shipping_Address__c 
        || acc.COMM_Shipping_City__c !=  oldMap.get(acc.Id).COMM_Shipping_City__c
        || acc.COMM_Shipping_State_Province__c !=  oldMap.get(acc.Id).COMM_Shipping_State_Province__c
        || acc.COMM_Shipping_PostalCode__c !=  oldMap.get(acc.Id).COMM_Shipping_PostalCode__c
        || acc.COMM_Shipping_Country__c !=  oldMap.get(acc.Id).COMM_Shipping_Country__c) {
          accountAddress = '';
          if(acc.COMM_Shipping_Address__c != null){
            accountAddress = acc.COMM_Shipping_Address__c;
          }
          if(acc.COMM_Shipping_City__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_City__c;
          }
          if(acc.COMM_Shipping_State_Province__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_State_Province__c;
          }
          if(acc.COMM_Shipping_PostalCode__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_PostalCode__c; 
          }
          if(acc.COMM_Shipping_Country__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_Country__c; 
          }
          accountIdMap.put(acc.Id,accountAddress);
        }
      }
    }
    if(!accountIdMap.isEmpty()) {
      List<Project_plan__C> planList = new List<Project_Plan__c>();
      for(Project_plan__c plan : [Select id,Account__c,Physical_address__c
                                    from Project_Plan__c
                                    where Account__c in : accountIdMap.keyset()]) {
        plan.Physical_address__c = accountIdMap.get(plan.Account__c);
        planList.add(plan);
      }
      if(!planList.isEmpty()) {
        update planList;
      }
    }
  }
  // Added by Rahul - T-477692
  // Modified by Shubham Dhupar T-477692
  public static void updateMasterAccount(List<Account> loserAccountList) {
    list<Account> loserAccList  = new list<Account>();
    list<String> loserAccNameList  = new list<String>();
    list<String> loserAccEmailList  = new list<String>();
    list<Id> loserUserIdLIst  = new list<Id>();
    list<String> loserAccLevelList  = new list<String>();
    list<String> loserOracleAccNumberList = new list<String>();
    Id masterAccountId;
    // Lists Storing details of the Loser Account
    for( Account a :loserAccountList) {
      if(a.MasterRecordId  != null) {
        loserAccList.add(a);
        loserAccNameList.add(a.Name);
        loserUserIdLIst.add(a.OwnerId);
        loserAccLevelList.add(a.Type);  
        loserOracleAccNumberList.add(a.Oracle_Account_Number_Comm__c);
        masterAccountId= a.MasterRecordId;
      }
    }
    // Compares the Owner of the Migrated Accounts
    String firstOwner = '';
    String secondOwner = '';
    for(Id owner : loserUserIdLIst) {
      if(firstOwner == '') {
        firstOwner = owner;
      }
      else {
        secondOwner = owner; 
      }
    }
    for(User us : [Select id,Email From User where Id in : loserUserIdLIst ] ) {
     loserAccEmailList.add(us.Email);
      if(firstOwner == secondOwner) { 
       loserAccEmailList.add(us.Email); // Stores Email Twice in the List if Owners are same.
      }
    }
      
    //code for merging if one or more then one accounts are merging into another Master Account.
    if(loserAccList.size() > 0) {
      updateMasterAccountFuture(masterAccountId, loserAccNameList, loserAccLevelList, loserAccEmailList, loserOracleAccNumberList );
    }
  }
  // Updates the Master Account with Details of the Loser Account
  @future
  public static void updateMasterAccountFuture(Id masterAccId,List<String> loserAccNameList, List<String> loserAccLevelList, 
                                                List<String> loserAccEmailList, List<String> loserOracleAccNumberList) {
    Account masterAcc = new Account();
    masterAcc  = [SELECT Id,Merged_Account_Name__c , Merged_Account_Name_2__c, Merged_Account_Email_1__c, Merged_Account_Email_2__c,
                        Merged_Account_Level__c, Is_Master_In_Merge__c, Oracle_Account_Number_Comm__c
                        FROM Account 
                        WHERE Id = :masterAccId];
    String merged_acc_level= '';
    String mergedAccName1 = '';
    String mergedAccName2 = '';
    String mergedAccEmail1 = '';
    String mergedAccEmail2 = '';
    String oracleAccNumber = '';
    // Stores Account Names of the Loser Account in Master Account
    for(String accName :loserAccNameList) {
      if(mergedAccName1 == '') {
        mergedAccName1 = accName;
        masterAcc.Merged_Account_Name__c = mergedAccName1; 
      }
      else if(mergedAccName1 != '' && mergedAccName2 == '' ) {
        mergedAccName2 = accName;
        masterAcc.Merged_Account_Name_2__c = mergedAccName2; 
      }
    }
    // Stores Account Owner's Email of the Loser Account in Master Account
    for(String accEmail :loserAccEmailList){
      if(mergedAccEmail1 == '') {
        mergedAccEmail1 = accEmail;
        masterAcc.Merged_Account_Email_1__c= mergedAccEmail1; 
      }
      else if(mergedAccEmail1 != '' && mergedAccEmail2 == ''){
        mergedAccEmail2 = accEmail;
        masterAcc.Merged_Account_Email_2__c= mergedAccEmail2;
      }
    }
    // Stores Account Level Of Loser account
    for(String accType :loserAccLevelList ){
      if(merged_acc_level != ''){
        merged_acc_level += ',';
      }
      merged_acc_level += accType ;
    }
    // Stores Oracle Account Number Of Loser account                                             
    for(String oracleAccountNumber :  loserOracleAccNumberList){
      if(oracleAccNumber != ''){
        oracleAccNumber += ',';
      }
      oracleAccNumber += oracleAccountNumber ;
    }
    masterAcc.Merged_Account_Level__c = merged_acc_level;
    masterAcc.Oracle_Account_Number_Comm__c = oracleAccNumber;
    masterAcc.Is_Master_In_Merge__c = true;
    update masterAcc ;
  }
   
  // Methods to be executed before record is Updated
  // @param oldMap - map to hold Trigger.oldMap 
  // @param newRecords - List to hold Trigger.new 
  // @return Nothing 
  // @task : T-477949
  public static void beforeRecordInsert(Map < Id, Account > oldMap, List < Account > newRecords) {
    //Populate Strategic account and Negotiated Contract checkbox based on parent account specified on a account
    SPS_AccountTriggerHandler.PopulateValuesToChildAccounts(oldMap, newRecords);
  }

  // Methods to be executed before record is Updated
  // @param oldMap - map to hold Trigger.oldMap 
  // @param newRecords - List to hold Trigger.new 
  // @return Nothing 
  public static void beforeRecordUpdate(Map < Id, Account > oldMap, List < Account > newRecords) {
    //Populate Strategic account and Negotiated Contract checkbox based on parent account specified on a account
    SPS_AccountTriggerHandler.PopulateValuesToChildAccounts(oldMap, newRecords);
  }
  
  // Methods to be executed after record update
  // @pupose : Used for COMM purpose
  // @description : Used to create the account team member on Parent Account with account owner
  // @param oldMap - map to hold Trigger.oldMap 
  // @param newRecords - List to hold Trigger.new 
  // @return Nothing 
  
  public static void afterRecordUpdateCOMM(Map < Id, Account > oldMap, List < Account > newRecords) {
    List < AccountTeamMember > accTeamMemList = new List < AccountTeamMember > ();
    List < AccountShare > accountShareList = new List < AccountShare > ();
    Map < Id, Id > parentrecordAndOwnerIds = new Map < Id, Id > ();
    Set < Id > parentAccountIds = new Set < Id > ();

    //User to collect parent account IDs
    for (Account acc: newRecords) {
      parentAccountIds.add(acc.ParentId);
    }

    //Used to populate the parent account id and owner id 
    for (Account acc: [SELECT id, 
                              OwnerId 
                       FROM Account 
                       WHERE Id IN: parentAccountIds]) {
      parentrecordAndOwnerIds.put(acc.id, acc.OwnerId);
    }
    
    //I-210448 - Modified the if condition
    for (Account acc: newRecords) {
     if ( getRecordTypeName(acc.RecordTypeId, 'Account') != null 
          && getRecordTypeName(acc.RecordTypeId, 'Account').contains('COMM') 
          && acc.ParentId != null 
          && parentrecordAndOwnerIds.containsKey(acc.ParentId)
          && acc.OwnerId != parentrecordAndOwnerIds.get(acc.ParentId) 
          && (oldMap.get(acc.id).ParentId != acc.ParentId)) {
        accTeamMemList.add(new AccountTeamMember(UserId = acc.OwnerId, AccountId = acc.ParentId));
      }   
    }

    if (!accTeamMemList.isEmpty()) {
      insert accTeamMemList;
    }
  }
  
  // @pupose : Used for COMM purpose
  // @description : Used to get the record type name for account object
  // @param oldMap - map to hold Trigger.oldMap 
  // @param newRecords - List to hold Trigger.new 
  // @return Nothing 
  // @task : T-477949
  public static String getRecordTypeName(Id recordType, String objectType) {
    Map < Id, Schema.RecordTypeInfo > rtMapById = null;
    
    if (rtMapById == null) {
      Map < String, Schema.SObjectType > gd = Schema.getGlobalDescribe();
      Schema.SObjectType ctype = gd.get(objectType);
      Schema.DescribeSObjectResult d1 = ctype.getDescribe();
      rtMapById = d1.getRecordTypeInfosById();

    }
    
    Schema.RecordTypeInfo recordTypeDetail = rtMapById.get(recordType);
    if (recordTypeDetail != null) {
      return recordTypeDetail.getName();
    } else {
      return null;
    }
  }
  
  // Added by Mehul - I-216712
  // NB - Skipping the functionality for Integration user  - 05/30 
  public static void updateStandardType (List<Account> newAccounts){
    
    Boolean skipCode = false;

    for(Profile migrationProfile : [Select id from Profile where Name = :Label.DataMigrationIntegration]){
      if(migrationProfile.Id == UserInfo.getProfileId()){ //Logged-in user is DM user
        skipCode = true;
      }
    }
    if(!skipCode){
      for(User intuser : [Select Id from User Where Name = :Label.Integration_User]){
        if(intuser.Id == UserInfo.getUserId()){ //Logged-in user is Integration user
          skipCode = true;
        }
      }
    }
    
    if(!skipCode){ //Non - Integration Users
      for(Account acc : newAccounts){
        //String getRecordTypeName=Schema.sObject.Account.getRecordTypeInfosByName().get('');
        system.debug('acc RT' + getRecordTypeName(acc.RecordTypeId,Constants.ACCOUNT_TYPE) + acc.Level__c );
        if(acc.RecordTypeId!=NULL) {
          if(getRecordTypeName(acc.RecordTypeId,Constants.ACCOUNT_TYPE).equals(Label.Endo_COMM_Account_RT_Label) 
            && (Constants.FACILITY_LEVEL).equals(acc.Level__c) ){
            acc.Type = acc.Type__c ;
          }
        }
      }
    } 
    //NB - 08/11 - I-229907 - Syncing Type__c and Type for Accounts coming via integration
    else {// Integration users
      for(Account acc : newAccounts){
        if(acc.RecordTypeId!= NULL) {
          if(getRecordTypeName(acc.RecordTypeId,Constants.ACCOUNT_TYPE).contains(Label.COMM) ){
            acc.Type = acc.Type__c ;
          }
        }
      }
    }
    //NB - 08/11 - I-229907 End
  }

  //Added by Kanika Mathur on 8/12/2016 to update Account FirstName, Last Name, OwnerManager fields according to RT
  public static void updateAccountOwner(List<Account> newList, Map<Id, Account> oldMap) {
    Set<Id> userIdsSalesRep = new Set<Id>();
    Set<Id> userIdsOwner = new Set<Id>();
    Map<Id, User> userMap = new Map<Id, User>();
    if(oldMap == null) {
      for(Account a : newList) {
        if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')) {
            if(getRecordTypeName(a.RecordTypeId, 'Account').contains('Endo')
                && a.COMM_Sales_Rep__c == null) {
                    userIdsOwner.add(a.OwnerId); 
            }
            if(a.COMM_Sales_Rep__c != null) {
                userIdsSalesRep.add(a.COMM_Sales_Rep__c);
            }
        } else {
            userIdsOwner.add(a.OwnerId);
            }
      }

      if(userIdsOwner.size() > 0 || userIdsSalesRep.size() > 0) {
        for(User u : [SELECT Id, FirstName, LastName, ManagerId, Manager.Name
                      FROM User 
                      WHERE Id IN : userIdsSalesRep 
                      OR Id IN : userIdsOwner]) {
          userMap.put(u.Id, u);
        }
      }

      for(Account a : newList) {
        if(userIdsSalesRep.contains(a.COMM_Sales_Rep__c)) {
          if(userMap.containsKey(a.COMM_Sales_Rep__c)) {
            a.Owner_First_Name__c = userMap.get(a.COMM_Sales_Rep__c).FirstName;
            a.Owner_Last_Name__c = userMap.get(a.COMM_Sales_Rep__c).LastName;
            a.Owner_Manager_Id__c = userMap.get(a.COMM_Sales_Rep__c).ManagerId;
            a.Owner_Manager_Name__c = userMap.get(a.COMM_Sales_Rep__c).Manager.Name;
          }
        } else if(userIdsOwner.contains(a.OwnerId)) {
            if(userMap.containsKey(a.OwnerId)) {
              a.Owner_First_Name__c = userMap.get(a.OwnerId).FirstName;
              a.Owner_Last_Name__c = userMap.get(a.OwnerId).LastName;
              a.Owner_Manager_Id__c = userMap.get(a.OwnerId).ManagerId;
              a.Owner_Manager_Name__c = userMap.get(a.OwnerId).Manager.Name;
            }
          }
      }
    } else {
        for(Account a : newList) {
          if(a.OwnerId != oldMap.get(a.Id).OwnerId 
            || a.COMM_Sales_Rep__c != oldMap.get(a.Id).COMM_Sales_Rep__c) {
            if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')) {
                if(getRecordTypeName(a.RecordTypeId, 'Account').contains('Endo')
                && a.COMM_Sales_Rep__c == null) {
                    userIdsOwner.add(a.OwnerId); 
                }
                if(a.COMM_Sales_Rep__c != null) {
                    userIdsSalesRep.add(a.COMM_Sales_Rep__c);
                }
            } else {
                userIdsOwner.add(a.OwnerId);
              }
          }
        }

        if(userIdsOwner.size() > 0 || userIdsSalesRep.size() > 0) {
          for(User u : [SELECT Id, FirstName, LastName, ManagerId, Manager.Name 
                        FROM User 
                        WHERE Id IN : userIdsSalesRep 
                        OR Id IN : userIdsOwner]) {
            userMap.put(u.Id, u);
          }
        }

        for(Account a : newList) {
          if(userIdsSalesRep.contains(a.COMM_Sales_Rep__c)) {
            if(userMap.containsKey(a.COMM_Sales_Rep__c)) {
              a.Owner_First_Name__c = userMap.get(a.COMM_Sales_Rep__c).FirstName;
              a.Owner_Last_Name__c = userMap.get(a.COMM_Sales_Rep__c).LastName;
              a.Owner_Manager_Id__c = userMap.get(a.COMM_Sales_Rep__c).ManagerId;
              a.Owner_Manager_Name__c = userMap.get(a.COMM_Sales_Rep__c).Manager.Name;
            }
          } else if(userIdsOwner.contains(a.OwnerId)) {
              if(userMap.containsKey(a.OwnerId)) {
                a.Owner_First_Name__c = userMap.get(a.OwnerId).FirstName;
                a.Owner_Last_Name__c = userMap.get(a.OwnerId).LastName;
                a.Owner_Manager_Id__c = userMap.get(a.OwnerId).ManagerId;
                a.Owner_Manager_Name__c = userMap.get(a.OwnerId).Manager.Name;
              }
            }
        }
    }
  }
  
  //Added by Kanika Mathur on 8/24/2016 to update field COMM_Sales_Rep__c according to RT and profile
  public static void updateCOMMSalesRep(List<Account> newList, Map<Id, Account> oldMap) {
      String pName = [SELECT Name FROM Profile WHERE Id =: userinfo.getProfileId()].Name;
      if(oldMap == null) {
         for(Account a : newList) {
          if ((a.RecordTypeId != null)
          && getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
          && pName.contains('COMM')
          && (!pName.equals(System.Label.DataMigrationIntegration))) {
              a.COMM_Sales_Rep__c = a.OwnerId;
          }
        } 
      } else {
          for(Account a : newList) {
              if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
                && pName.contains('COMM')
                && (!pName.equals(System.Label.DataMigrationIntegration))
                && a.OwnerId != oldMap.get(a.Id).OwnerId
                && a.COMM_Sales_Rep__c == null) {
                    a.COMM_Sales_Rep__c = a.OwnerId;
                }
          }
      }
      
  }
  
  //Added by Kanika Mathur on 8/24/2016 to provide access to Sales Rep on Account
  public static void manualShareForSalesRep(List<Account> newList, Map<Id, Account> oldMap) {
      Set<Id> accountIds = new Set<Id>();
      List<AccountShare> accShareAddList = new List<AccountShare>();
      List<AccountShare> accShareRemoveList = new List<AccountShare>();
      Set<Id> accShareIds = new Set<Id>();
      if(oldMap == null) {
          for(Account a : newList) {
            if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
                && a.COMM_Sales_Rep__c != null
                && a.COMM_Sales_Rep__c != a.OwnerId) {
                    AccountShare accShareRecord = new AccountShare();
                    accShareRecord.AccountId = a.Id;
                    accShareRecord.UserOrGroupId = a.COMM_Sales_Rep__c;
                    accShareRecord.AccountAccessLevel = 'Edit';
                    accShareRecord.OpportunityAccessLevel = 'Edit'; // MV - 10/06 - I-238733 Start
                    accShareRecord.CaseAccessLevel = 'Read';        // MV - 10/06 - I-238733 End
                    accShareAddList.add(accShareRecord);
            } 
        }
      } else {
          for(Account a : newList) {
              if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
                && a.COMM_Sales_Rep__c != oldMap.get(a.Id).COMM_Sales_Rep__c) {
                    if(a.COMM_Sales_Rep__c == null) {
                        accountIds.add(a.Id);
                    } else {
                        if(oldMap.get(a.Id).COMM_Sales_Rep__c != null) {
                            accountIds.add(a.Id);
                        }
                        if(a.COMM_Sales_Rep__c != a.OwnerId) {
                            AccountShare accShareRecord = new AccountShare();
                            accShareRecord.AccountId = a.Id;
                            accShareRecord.UserOrGroupId = a.COMM_Sales_Rep__c;
                            accShareRecord.AccountAccessLevel = 'Edit';
                            accShareRecord.OpportunityAccessLevel = 'Edit'; // MV - 10/06 - I-238733 Start
                            accShareRecord.CaseAccessLevel = 'Read'; // MV - 10/06 - I-238733 End
                            accShareAddList.add(accShareRecord);
                        }
                      }
                }
          }
      }
      if(!accountIds.isEmpty()) {
          List<AccountShare> accShareDeleteList = new List<AccountShare>([SELECT Id 
                                                                            FROM AccountShare 
                                                                            WHERE AccountId IN : accountIds 
                                                                            AND RowCause =: 'Manual']);
        if(!accShareDeleteList.isEmpty()) {
            database.delete(accShareDeleteList, false);
        }
      }
      if(!accShareAddList.isEmpty()) {
          database.insert(accShareAddList, false);
      }
      
  }
  //Added by Kanika Mathur on 8/24/2016 to provide access to Sales Rep on Parent Account
  public static void manualSharesForSalesRepOnParent (List<Account> newList, Map<Id, Account> oldMap) {
      List<AccountShare> accShareAddList = new List<AccountShare>();
      List<AccountShare> accShareDeleteList = new List<AccountShare>();
      Set<Id> parentAccountIdsSet = new Set<Id>();
      Set<Id> parentAccountIdsToDeleteShares = new Set<Id>();
      Map<Id, Id> parentToOwnerIdMap = new Map<Id, Id>();
      if(oldMap == null) {
          for(Account a : newList) {
              if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
                && a.COMM_Sales_Rep__c != null
                && a.COMM_Sales_Rep__c != a.OwnerId
                && a.ParentId != null) {
                  parentAccountIdsSet.add(a.ParentId);
              }
          }
          
          if(!parentAccountIdsSet.isEmpty()) {
             for(Account a : [SELECT Id, OwnerId FROM Account WHERE Id IN : parentAccountIdsSet]) {
                parentToOwnerIdMap.put(a.Id, a.OwnerId);
             } 
          }
          
          for(Account a : newList) {
             if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
                && a.COMM_Sales_Rep__c != null
                && a.COMM_Sales_Rep__c != a.OwnerId
                && a.ParentId != null
                && parentToOwnerIdMap.get(a.ParentId) != a.COMM_Sales_Rep__c) {
                    AccountShare accShareRecord = new AccountShare();
                    accShareRecord.AccountId = a.ParentId;
                    accShareRecord.UserOrGroupId = a.COMM_Sales_Rep__c;
                    accShareRecord.AccountAccessLevel = 'Read';
                    accShareRecord.OpportunityAccessLevel = 'Read';
                    accShareAddList.add(accShareRecord);
                }
          }
          
      } else {
          for(Account a : newList) {
              if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
                && a.ParentId != oldMap.get(a.Id).ParentId
                && a.COMM_Sales_Rep__c != null
                && a.COMM_Sales_Rep__c != a.OwnerId) {
                    if(a.ParentId == null) {
                        parentAccountIdsToDeleteShares.add(a.ParentId);
                    } else {
                        if(oldMap.get(a.Id).ParentId != null) {
                            parentAccountIdsToDeleteShares.add(a.ParentId);
                        }
                        parentAccountIdsSet.add(a.ParentId);
                    }
                }
          }
          if(!parentAccountIdsSet.isEmpty()) {
             for(Account a : [SELECT Id, OwnerId FROM Account WHERE Id IN : parentAccountIdsSet]) {
                parentToOwnerIdMap.put(a.Id, a.OwnerId);
             } 
          }
          
          for(Account a : newList) {
              if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')
                && a.ParentId != oldMap.get(a.Id).ParentId
                && a.COMM_Sales_Rep__c != null
                && a.COMM_Sales_Rep__c != a.OwnerId
                && a.ParentId != null
                && parentToOwnerIdMap.get(a.ParentId) != a.COMM_Sales_Rep__c) {
                    AccountShare accShareRecord = new AccountShare();
                    accShareRecord.AccountId = a.ParentId;
                    accShareRecord.UserOrGroupId = a.COMM_Sales_Rep__c;
                    accShareRecord.AccountAccessLevel = 'Read';
                    accShareRecord.OpportunityAccessLevel = 'Read';
                    accShareAddList.add(accShareRecord);
                }
          }
          
          if(!parentAccountIdsToDeleteShares.isEmpty()) {
            List<AccountShare> parentAccShareDeleteList = new List<AccountShare>([SELECT Id 
                                                                            FROM AccountShare 
                                                                            WHERE AccountId IN : parentAccountIdsToDeleteShares 
                                                                            AND RowCause =: 'Manual']);
            if(!parentAccShareDeleteList.isEmpty()) {
              database.delete(parentAccShareDeleteList, false);
            }
          }
          if(!accShareAddList.isEmpty()) {
              database.insert(accShareAddList, false);
          }
      }
  }
  //Send a mail to Related case owner and technician if there a hold on account(Varun Vasishtha T-514084)
  private static void HoldRelesedNotification(List<Account> newItems,Map<Id,Account> oldItems)
  {
    Set<Id> accountList = new Set<Id>();
    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    EmailTemplate template = [select id from EmailTemplate where DeveloperName = :holdTemplate];
    for(Account entity : newItems)
    {
        Account oldEntity = oldItems.get(entity.Id);
        if(oldEntity.Hold_Status__c !=  entity.Hold_Status__c && string.isBlank(entity.Hold_Status__c))
        {
            accountList.add(entity.Id);
        }
    }
    List<string> woStatus = new List<string>{'Closed','Canceled','Rejected'};
    List<SVMXC__Service_Order__c> relatedWorkOrders = [select SVMXC__Company__c,SVMXC__Company__r.Name,SVMXC__Case__r.OwnerId,SVMXC__Member_Email__c from  SVMXC__Service_Order__c 
    where SVMXC__Company__c IN: accountList and SVMXC__Order_Status__c not in :woStatus];
    for(SVMXC__Service_Order__c related : relatedWorkOrders)
    {
        boolean isUser = COMM_CaseUtil.IsUser(related.SVMXC__Case__r.OwnerId);
        if(related.SVMXC__Case__r.OwnerId != null && isUser)
        {
            Messaging.SingleEmailMessage caseMail = new Messaging.SingleEmailMessage();
            caseMail.setTemplateId(template.Id);
            caseMail.setSaveAsActivity(false);
            caseMail.setTargetObjectId(related.SVMXC__Case__r.OwnerId);
            caseMail.setWhatId(related.SVMXC__Company__c);
            emails.add(caseMail);
        }
        if(string.isNotBlank(related.SVMXC__Member_Email__c))
        {
            string body = '<p>A hold has been released on the following record:</p>';
            body += '<p>Account Name :'+ related.SVMXC__Company__r.Name+'</p>';
            body += '<p>For detail please follow this link : <a href='+ System.Label.COMMBaseURL +'/'+ related.SVMXC__Company__c +'>'+related.SVMXC__Company__r.Name+'</a></p>';
            string subject = 'A Hold has been released';
            Messaging.SingleEmailMessage caseMail = new Messaging.SingleEmailMessage();
            caseMail.setSaveAsActivity(false);
            caseMail.setToAddresses(new List<string>{related.SVMXC__Member_Email__c});
            caseMail.setHtmlBody(body);
            caseMail.setSubject(subject);
            emails.add(caseMail);
            
        }
    }
    System.debug('varun enter mail'+emails);
    Messaging.sendEmail(emails); 
  }
}