/**=====================================================================
 * Appirio, Inc
 * Name: OrderLineItemAddressControllerTest 
 * Description: Test class for OrderLineItemAddressController (T-482421)
 * Created Date: 14th April 2016
 * Created By: Rahul Aeran(Appirio)
=======================================================================*/
@isTest
private class OrderLineItemAddressControllerTest {
    @isTest 
    static void testOrderLineItemAddressUpdateController(){
        User sysAdmin =  TestUtils.createUser(1,'System Administrator', true).get(0);
        System.runAs(sysAdmin){
            List < Account > accountList = TestUtils.createAccount(1, true);
            List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
            List < Project_Plan__c > projectPlanList = TestUtils.createProjectPlan(1, accountList[0].id,true);
            
            
            
            List<Project_Phase__c> projectPhaseList = TEstUtils.createProjectPhase(1,projectPlanList.get(0).Id,true);
            List<product2> productList = TestUtils.createProduct(1,true);
            
            List<Pricebook2> priceBookList = TestUtils.createPriceBook(1,'Test Price Book',true);
            
            
            //standard price book
            TestUtils.createPriceBookEntry(1,Test.getStandardPricebookId(),productList.get(0).Id,true);
            List<PriceBookEntry> priceBookEntryList = new List<PriceBookEntry>();
            //custom price book
            priceBookEntryList.addAll(TestUtils.createPriceBookEntry(1,priceBooklist.get(0).Id,productList.get(0).Id,true));
            
            //insert projectPlanList;
            
            //insert priceBookEntryList;
            
            
            List<Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
            orderList.get(0).opportunityid  =  oppList[0].id;
            orderList.get(0).Pricebook2id = priceBookList.get(0).Id;
            //orderList.get(0).Oracle_Number__c = '12';
            orderList.get(0).Oracle_Order_Number__c='1234';
            insert orderList;
            
            //insert projectPhaseList;
              
        
            
            List<OrderItem> orderItemList = TestUtils.createOrderItem(10,projectPhaseList.get(0).Id,orderList.get(0).Id,priceBookEntryList.get(0).Id,false);
              Integer i=0;
            for(OrderItem o: orderItemList)
            {   
                o.Hold_Description__c = 'This is hold description text for testing';
                o.Oracle_Line_Number__c='12'+i;
                o.Oracle_Order_Line_Number__c='123';
                o.Status__c='Entered'; //NB - 11/9 - I-242624
                i++;
            }
            orderItemList[0].Hold_Description__c = 'test';//NB - 11/9 - I-242624
            orderItemList[0].Requested_Ship_Date__c = system.today().addDays(20);//NB - 11/9 - I-242624
            orderItemList[0].Approval_Status__c = 'Requested';//NB - 11/9 - I-242624
            Test.StartTest();
            insert orderItemList;
            
                PageReference pageRef = Page.OrderLineItemAddressPage;
                Test.setCurrentPage(pageRef);
                OrderLineItemAddressController cont = new OrderLineItemAddressController();
                System.currentPageReference().getParameters().put('id',projectPhaseList.get(0).Id);
                cont  = new OrderLineItemAddressController();
                
                
                //There were no address inserted till now
                System.assert(cont.lstAvailableAddress.isEmpty(),'The address list should be empty');
                List<Address__c> addresses = new List<Address__c>();
                addresses.addAll(TestUtils.createAddressObject(accountList.get(0).Id,1,false,'United States',null,Constants.SHIPPING,Constants.BUSINESSUNIT,false));
                insert addresses;
                
                cont  = new OrderLineItemAddressController();
                System.assert(!cont.lstAvailableAddress.isEmpty(),'The list should return one object');
                
                
                
                //calling the on change event for the select list
                cont.selectedAddress = addresses.get(0).Id;
                cont.setAddress();
                
                cont.updateAddress(); 
                
                
                //Selecting one of the items for updating
                for(OrderLineItemAddressController.OrderLIneItemWrapper ow : cont.items){
                    ow.isSelected = true;
                    ow.oli.Requested_Ship_Date__c = system.today().addDays(20);//NB - 11/9 - I-242624
                    ow.oli.Approval_Status__c = 'Requested';//NB - 11/9 - I-242624
                }
                
                cont.updateAddress();
                //Selecting one of the items for updating
                for(OrderLineItemAddressController.OrderLIneItemWrapper ow : cont.items){
                    ow.isSelected = true;
                    ow.oli.Requested_Ship_Date__c = system.today().addDays(20);//NB - 11/9 - I-242624
                    ow.oli.Approval_Status__c = 'Requested';//NB - 11/9 - I-242624
                }
                //setting one of the items as not selected
                cont.items.get(0).isSelected = false;
                cont.dummyOrderItem = orderItemList[0];//NB - 11/9 - I-242624
                cont.updateStatus();
                cont.updateDate();
            
            
            System.currentPageReference().getParameters().put('id','');
            System.currentPageReference().getParameters().put('ppid',projectPlanList.get(0).Id);
            cont.setOrderBy(); //NB - 11/9 - I-242624
            cont=new OrderLineItemAddressController();
            Test.StopTest();
        }
    }
}