/**================================================================      
* Appirio, Inc
* Name: COMM_Manage_CycleCount_Controller
* Description: Controller for page Cycel Count S-399757
* Created Date: 29 - Sept - 2016
* Created By: Gaurav Sinha (Appirio)
*
* Date Modified      Modified By      Description of the update
* 07 - Nov - 2016    Varun Vasistha   Commented createWorkorder function due to change in AC (I-242550)
* 14 - Nov - 2016    Isha Shukla      S-399758 Modified the logic which will create cycle count lines depending on Products.
==================================================================*/
public class COMM_Manage_CycleCount_Controller {    
    public list<ProductWrapper> liProductWrapeer{get;set;}
    Public Cycle_Count__c varCycleCount{get;set;}
    PUBLIC STATIC STRING  WO_ReocrdTYPE = 'Administrative_Time';
    
    public COMM_Manage_CycleCount_Controller(){
        displaySectionSearch = true;
        varCycleCount = new Cycle_Count__c(name=System.today().format()+' : '+Userinfo.getName(),status__c = 'New');
        PopulateProductWrapper();
        
    }
    
    public void PopulateProductWrapper(){
        liProductWrapeer = new list<ProductWrapper>();
        for(integer i =0;i<20;i++){
            liProductWrapeer.add(new ProductWrapper());
        }
        
    }
    
    public boolean displaySectionSearch{get;set;}
    
    public void saveRecords(){
        Integer count = 0;
        if(varCycleCount.Technician__c !=null)
            count++;
        if(varCycleCount.Service_Team__c !=null)
            count++;
        if(varCycleCount.User_Manager__c !=null)
            count++;
        if(varCycleCount.All_Technician__c)
            count++;
        //if(varCycleCount.division__c !=null)
        //    count++;
        
        if(count>1 || count==0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select only one Category for cycle count generation.');
            ApexPages.addMessage(myMsg);
            return ;
        }
        Savepoint sp = Database.setSavepoint();
        try{
            List<Cycle_Count__c> listHeaderToinsert = new List<Cycle_Count__c>();
            set<id> TempLocation = new Set<Id>();
            Map<Id,Id> techToLocationMap = new Map<Id,Id>();
            System.debug('displaySectionSearch===>'+displaySectionSearch);
            // if(displaySectionSearch){
            varCycleCount.IsMonthly__c = displaySectionSearch;
            if(varCycleCount.Technician__c !=null){
                SVMXC__Service_Group_Members__c vartechnicanEmail = [select SVMXC__Inventory_Location__c,SVMXC__Email__c,SVMXC__Salesforce_User__c,SVMXC__Salesforce_User__r.Managerid from SVMXC__Service_Group_Members__c where id =: varCycleCount.Technician__c][0];
                varCycleCount.Location__c = vartechnicanEmail.SVMXC__Inventory_Location__c;
                varCycleCount.Technician_Email__c = vartechnicanEmail.SVMXC__Email__c;
                varCycleCount.Technician_Salesforce_user__c = vartechnicanEmail.SVMXC__Salesforce_User__c;
                varCycleCount.Technician_Salesforce_user_Manger__c = vartechnicanEmail.SVMXC__Salesforce_User__r.Managerid;
                
                if(varCycleCount.Location__c!=null) {
                    TempLocation.add(varCycleCount.Location__c);
                    if(!techToLocationMap.containsKey(vartechnicanEmail.Id)) {
                        techToLocationMap.put(vartechnicanEmail.Id,vartechnicanEmail.SVMXC__Inventory_Location__c);
                    }
                }
                listHeaderToinsert.add(varCycleCount);
            }
            else if(varCycleCount.Service_Team__c !=null){
                Map<String,SVMXC__Service_Group_Members__c> localLocation = new Map<string,SVMXC__Service_Group_Members__c>();
                List<SVMXC__Service_Group_Members__c> holder = new List<SVMXC__Service_Group_Members__c>();
                
                for(SVMXC__Service_Group_Members__c varloop: [select SVMXC__Inventory_Location__c ,SVMXC__Email__c,SVMXC__Salesforce_User__c,SVMXC__Salesforce_User__r.Managerid
                                                              from SVMXC__Service_Group_Members__c
                                                              where SVMXC__Service_Group__c  =: varCycleCount.Service_Team__c
                                                              and svmxc__Active__c= true]){
                                                                  localLocation.put(varloop.id,varloop);
                                                                  holder.add(varloop);
                                                                  if(varloop.SVMXC__Inventory_Location__c!=null) {
                                                                      TempLocation.add(varloop.SVMXC__Inventory_Location__c);
                                                                      if(!techToLocationMap.containsKey(varloop.Id)) {
                                                                          techToLocationMap.put(varloop.Id,varloop.SVMXC__Inventory_Location__c);
                                                                      }
                                                                  }	  
                                                              }
                Cycle_Count__c temCycleCount = new Cycle_Count__c();
                for(SVMXC__Service_Group_Members__c varLoop : holder){
                    temCycleCount = new Cycle_Count__c();
                    temCycleCount.Technician__c = varloop.id;
                    temCycleCount.Location__c =localLocation.get(varLoop.id).SVMXC__Inventory_Location__c;
                    temCycleCount.name = varCycleCount.name;
                    temCycleCount.Technician_Email__c = localLocation.get(varLoop.id).SVMXC__Email__c;
                    temCycleCount.Technician_Salesforce_user__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__c;
                    temCycleCount.Technician_Salesforce_user_Manger__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__r.Managerid;                     
                    temCycleCount.status__c = varCycleCount.status__c;
                    temCycleCount.Division__c = varCycleCount.Division__c;
                    temCycleCount.Due_Date__c = varCycleCount.Due_Date__c;
                    temCycleCount.Service_Team__c = varCycleCount.Service_Team__c;
                    listHeaderToinsert.add(temCycleCount);
                }
            }
            else if(varCycleCount.All_Technician__c){
                if(displaySectionSearch){
                    set<id> productId = new set<id>();
                    for(ProductWrapper pr : liProductWrapeer){
                        if(pr.product.product__C!=null)
                            productId.add(pr.product.product__C);
                    }
                    Database.executeBatch(new Comm_BATCH_Manage_Cycle_Count(varCycleCount,productId),10);
                }else{
                    Database.executeBatch(new Comm_BATCH_Manage_Cycle_Count(varCycleCount,null),10);
                }
            }
            else if(varCycleCount.User_Manager__c!=null){
                Map<String,SVMXC__Service_Group_Members__c> localLocation = new Map<string,SVMXC__Service_Group_Members__c>();
                List<SVMXC__Service_Group_Members__c> holder = new List<SVMXC__Service_Group_Members__c>();
                
                for(SVMXC__Service_Group_Members__c varloop: [select SVMXC__Inventory_Location__c ,SVMXC__Email__c,SVMXC__Salesforce_User__c,SVMXC__Salesforce_User__r.Managerid
                                                              from SVMXC__Service_Group_Members__c
                                                              where SVMXC__Salesforce_User__r.managerid  =: varCycleCount.User_Manager__c
                                                              and svmxc__Active__c= true]){
                                                                  localLocation.put(varloop.id,varloop);
                                                                  holder.add(varloop);
                                                                  if(varloop.SVMXC__Inventory_Location__c!=null) {
                                                                      TempLocation.add(varloop.SVMXC__Inventory_Location__c);
                                                                      if(!techToLocationMap.containsKey(varloop.Id)) {
                                                                          techToLocationMap.put(varloop.Id,varloop.SVMXC__Inventory_Location__c);
                                                                      }
                                                                  }
                                                              }
                Cycle_Count__c temCycleCount = new Cycle_Count__c();
                for(SVMXC__Service_Group_Members__c varLoop : holder){
                    temCycleCount = new Cycle_Count__c();
                    temCycleCount.Technician__c = varloop.id;
                    temCycleCount.Location__c =localLocation.get(varLoop.id).SVMXC__Inventory_Location__c;
                    temCycleCount.name = varCycleCount.name;
                    temCycleCount.Technician_Email__c = localLocation.get(varLoop.id).SVMXC__Email__c;
                    temCycleCount.Technician_Salesforce_user__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__c;
                    temCycleCount.Technician_Salesforce_user_Manger__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__r.Managerid;                     
                    temCycleCount.status__c = varCycleCount.status__c;
                    temCycleCount.Division__c = varCycleCount.Division__c;
                    temCycleCount.Due_Date__c = varCycleCount.Due_Date__c;
                    temCycleCount.Service_Team__c = varCycleCount.Service_Team__c;
                    listHeaderToinsert.add(temCycleCount);
                }
                
            }
            /*else if(varCycleCount.User_Manager__c!=null){
Map<String,SVMXC__Service_Group_Members__c> localLocation = new Map<string,SVMXC__Service_Group_Members__c>();
List<SVMXC__Service_Group_Members__c> holder = new List<SVMXC__Service_Group_Members__c>();

for(SVMXC__Service_Group_Members__c varloop: [select SVMXC__Inventory_Location__c ,SVMXC__Email__c,SVMXC__Salesforce_User__c,SVMXC__Salesforce_User__r.Managerid
from SVMXC__Service_Group_Members__c
where SVMXC__Salesforce_User__r.Division  =: varCycleCount.Division__c
and svmxc__Active__c= true]){
localLocation.put(varloop.id,varloop);
holder.add(varloop);
if(varloop.SVMXC__Inventory_Location__c!=null)
TempLocation.add(varloop.SVMXC__Inventory_Location__c);
}
Cycle_Count__c temCycleCount = new Cycle_Count__c();
for(SVMXC__Service_Group_Members__c varLoop : holder){
temCycleCount = new Cycle_Count__c();
temCycleCount.Technician__c = varloop.id;
temCycleCount.Location__c =localLocation.get(varLoop.id).SVMXC__Inventory_Location__c;
temCycleCount.name = varCycleCount.name;
temCycleCount.Technician_Email__c = localLocation.get(varLoop.id).SVMXC__Email__c;
temCycleCount.Technician_Salesforce_user__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__c;
temCycleCount.Technician_Salesforce_user_Manger__c = localLocation.get(varLoop.id).SVMXC__Salesforce_User__r.Managerid;                     
temCycleCount.status__c = varCycleCount.status__c;
temCycleCount.Division__c = varCycleCount.Division__c;
temCycleCount.Due_Date__c = varCycleCount.Due_Date__c;
temCycleCount.Service_Team__c = varCycleCount.Service_Team__c;
listHeaderToinsert.add(temCycleCount);
}

} */
            if(!listHeaderToinsert.isEmpty()){
                insert listHeaderToinsert;
            }
            Set<id> productId = new Set<id>();
            list<Cycle_Count_Line__c> lineItemsTOinserted = new List<Cycle_Count_Line__c>();
            for(ProductWrapper pr : liProductWrapeer){
                if(pr.product.product__C!=null)
                    productId.add(pr.product.product__C);
            }
            System.debug('TempLocation===>'+TempLocation);
            System.debug('productId===>'+productId);
            // if the quaterly Cyccle count is sent
            if(!displaySectionSearch){               
                for(SVMXC__Product_Stock__c varLoop : [select id,SVMXC__Product__c,SVMXC__Location__c,On_Hand_Qty__c 
                                                       from SVMXC__Product_Stock__c
                                                       where SVMXC__Location__r.Exclude_Location_from_Inventory__c = false
                                                       and SVMXC__Location__c in : TempLocation
                                                       and  On_Hand_Qty__c  > 1
                                                       and SVMXC__Product__r.include_in_cycle_counts__c = true
                                                      ]){
                                                          for(Cycle_Count__c var: listHeaderToinsert) {
                                                              Cycle_Count_Line__c cycleLine = new Cycle_Count_Line__c();
                                                              cycleLine.Cycle_Count__c = var.Id;
                                                              cycleLine.Product__c = varloop.SVMXC__Product__c;
                                                              if(techToLocationMap.containsKey(var.Technician__c) && techToLocationMap.get(var.Technician__c) == varloop.SVMXC__Location__c) {
                                                                  cycleLine.Product_Stock__c = varloop.Id;
                                                                  cycleLine.Expected_Quantity__c = varloop.On_Hand_Qty__c;
                                                              }
                                                              lineItemsTOinserted.add(cycleLine);
                                                          }
                                                      }
            }
            else{
                Map<String,SVMXC__Product_Stock__c> dictionaryProductStock = new Map<String,SVMXC__Product_Stock__c>();
                for(SVMXC__Product_Stock__c varLoop : [select id,SVMXC__Product__c,SVMXC__Location__c,On_Hand_Qty__c  
                                                       from SVMXC__Product_Stock__c
                                                       where SVMXC__Location__r.Exclude_Location_from_Inventory__c = false
                                                       and SVMXC__Location__c in : TempLocation
                                                       and  SVMXC__Product__c in: productId
                                                       and SVMXC__Product__r.include_in_cycle_counts__c = true
                                                      ]){
                                                          if(!dictionaryProductStock.containsKey(varLoop.SVMXC__Location__c+'+'+varLoop.SVMXC__Product__c)) {
                                                              dictionaryProductStock.put(varLoop.SVMXC__Location__c+'+'+varLoop.SVMXC__Product__c,varLoop);                            
                                                          }
                                                          
                                                      }
                for(ProductWrapper prod : liProductWrapeer) {
                    if(prod.product.product__c != null) {
                        for(Cycle_Count__c cycleCount: listHeaderToinsert){
                            Cycle_Count_Line__c cycleLine = new Cycle_Count_Line__c();
                            cycleLine.Cycle_Count__c = cycleCount.id;
                            cycleLine.Product__c = prod.product.product__c;
                            if(dictionaryProductStock.containsKey(cycleCount.Location__c+'+'+prod.product.product__c)) {
                                cycleLine.Product_Stock__c = dictionaryProductStock.get(cycleCount.Location__c+'+'+prod.product.product__c).Id;
                                cycleLine.Expected_Quantity__c = dictionaryProductStock.get(cycleCount.Location__c+'+'+prod.product.product__c).On_Hand_Qty__c;
                            }
                            lineItemsTOinserted.add(cycleLine);
                        }  
                    }                              
                }
                
            }
            if(!lineItemsTOinserted.isEmpty()){
                insert lineItemsTOinserted;
            }
            if(varCycleCount.All_Technician__c) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Cycle Count Records are inserted ');
                ApexPages.addMessage(myMsg);
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,listHeaderToinsert.size() + ' Records inserted ');
                ApexPages.addMessage(myMsg);
            }
            
            /* }else{
massProcessing(false);
return ;
}*/
            
            //Comented due to change in AC (Varun Vasistha I-242550)
            // createWorkorder(listHeaderToinsert);
            
            varCycleCount = new Cycle_Count__c(name=System.today().format()+' : '+Userinfo.getName(),status__c = 'New');
            PopulateProductWrapper();
        }
        catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
            Database.rollback(sp);
            
            return;
        }
    }
    //Comented due to change in AC (Varun Vasistha I-242550)
    /*public void createWorkorder(List<Cycle_Count__c> listHeaderToinser){
String rtID ='';
for(recordtype var : [select id from recordtype where developername = 'Administrative_Time' limit 1]){
rtid = var.id;
}
List<SVMXC__Service_Order__c> LiWO = new list<SVMXC__Service_Order__c>();
SVMXC__Service_Order__c varWO = new SVMXC__Service_Order__c();
for(Cycle_Count__c varLoop:listHeaderToinser ){
varWO = new SVMXC__Service_Order__c();
varWO.RecordTypeid= rtid;
varwo.Cycle_Count__c = varCycleCount.id; 
LiWO.add(varwo);
}
if(!LiWO.isEmpty()){
insert liWO;    
}

}*/
    /*
* @method:  changeMode
* @param : None
* @Description: Change the Displayemode
*/
    public void changeMode(){
        displaySectionSearch = !displaySectionSearch;
    }
    
    public class ProductWrapper{
        public Cycle_Count_Line__c product{Get;set;}
        
        public ProductWrapper(){
            product = new Cycle_Count_Line__c();
        }    
    }   
}