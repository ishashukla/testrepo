public with sharing class NV_Utility {

	public static Notification_Settings__c getNotificationSettingForUser(Id userId){
	//Modified for S-397645
		List<Notification_Settings__c> settings = [SELECT Id, Entered__c, All_Entered__c, Booked__c, All_Booked__c, Shipped__c, All_Shipped__c, Cancelled__c, All_Cancelled__c,
														Awaiting_Shipping__c, All_Awaiting_Shipping__c, Backordered__c, All_Backordered__c,
														Delivered__c, All_Delivered__c,
														Closed__c, All_Closed__c,
														Awaiting_PO__c, All_Awaiting_PO__c, Awaiting_Return__c,All_Awaiting_Return__c, Returned__c, All_Returned__c, On_Hold__c,All_On_Hold__c,
														Settings__c, All_Settings__c,
														//Added for S-389739
														Bill_Closed__c, All_Bill_Closed__c,
														//end S-389739
														User__c, All_Active_Orders__c
														FROM Notification_Settings__c
														WHERE User__c =: userId
														LIMIT 1];
	 //End S-397645
		if(settings.size()>0){
			return settings[0];
		}
		else{
			return createAndGetNotificationSettingForUser(userId);
		}
	}

	public static Map<Id,Notification_Settings__c> getNotificationSettingsForUsers(Set<Id> userIds){
		Map<Id,Notification_Settings__c> notificationSettingMap = new Map<Id,Notification_Settings__c>();
		//Modified for S-397645
		for(Notification_Settings__c setting : [SELECT Id, Entered__c, All_Entered__c, Booked__c, All_Booked__c, Shipped__c, All_Shipped__c, Cancelled__c, All_Cancelled__c,
														Awaiting_Shipping__c, All_Awaiting_Shipping__c, Backordered__c, All_Backordered__c,
														Delivered__c, All_Delivered__c,
														Closed__c, All_Closed__c,
														Awaiting_PO__c, All_Awaiting_PO__c, Awaiting_Return__c,All_Awaiting_Return__c, Returned__c, All_Returned__c, On_Hold__c,All_On_Hold__c,
														Settings__c, All_Settings__c,
														//Added for S-389739
														Bill_Closed__c, All_Bill_Closed__c,
														//end S-389739
														User__c, All_Active_Orders__c
													FROM Notification_Settings__c
													WHERE User__c in : userIds]){
		//End S-397645
			notificationSettingMap.put(setting.User__c, setting);
		}
		Set<Id> newUserIds = new Set<Id>();
		for(Id userId : userIds){
			if(!notificationSettingMap.containsKey(userId)){
				newUserIds.add(userId);
			}
		}
		for(Notification_Settings__c setting: createAndGetNotificationSettingsForUsers(newUserIds)){
			notificationSettingMap.put(setting.User__c, setting);
		}
		return notificationSettingMap;
	}

	public static Notification_Settings__c createAndGetNotificationSettingForUser(Id userId){
		Notification_Settings__c setting = new Notification_Settings__c(User__c = userId);
		insert setting;
		return getNotificationSettingForUser(userId);
	}

	public static List<Notification_Settings__c> createAndGetNotificationSettingsForUsers(Set<Id> userIds){

		List<Notification_Settings__c> settings = new List<Notification_Settings__c>();
		for(Id userId : userIds){
			settings.add(new Notification_Settings__c(User__c = userId));
		}
		insert settings;
		//Modified for S-397645
		settings = [SELECT Id, Entered__c, All_Entered__c, Booked__c, All_Booked__c, Shipped__c, All_Shipped__c, Cancelled__c, All_Cancelled__c,
														Awaiting_Shipping__c, All_Awaiting_Shipping__c, Backordered__c, All_Backordered__c,
														Delivered__c, All_Delivered__c,
														Closed__c, All_Closed__c,
														Awaiting_PO__c, All_Awaiting_PO__c, Awaiting_Return__c,All_Awaiting_Return__c, Returned__c, All_Returned__c, On_Hold__c,All_On_Hold__c,
														Settings__c, All_Settings__c,
														//Added for S-389739
														Bill_Closed__c, All_Bill_Closed__c,
														//end S-389739
														User__c, All_Active_Orders__c
						FROM Notification_Settings__c
						WHERE User__c in : userIds];
		//End S-397645
		return settings;
	}

	public static String getDayOfWeek(Datetime myDate){
		String dayOfWeek = null;
		if(myDate!=null){
			DateTime myDateTime =  Datetime.newInstance(myDate.year(), myDate.month(), myDate.day(),0,0,0);
			dayOfWeek = myDateTime.format('E');
		}
		return dayOfWeek;
	}

	public static String getDayOfWeek(Date myDate){
		String dayOfWeek = null;
		if(myDate!=null){
			DateTime myDateTime =  Datetime.newInstance(myDate.year(), myDate.month(), myDate.day(),0,0,0);
			dayOfWeek = myDateTime.format('E');
		}
		return dayOfWeek;
	}

	public static Integer getDayOfWeekInInt(Date myDate){
		Map<String,Integer> weekDay = new Map<String,Integer> {'Sun'=>0, 'Mon'=>1, 'Tue'=>2, 'Wed'=>3, 'Thu'=>4, 'Fri'=>5, 'Sat'=>6};

		Integer dayOfWeek = -1;
		if(myDate!=null){
			DateTime myDateTime =  Datetime.newInstance(myDate.year(), myDate.month(), myDate.day(),0,0,0);
			dayOfWeek = weekDay.get(myDateTime.format('E'));
		}
		return dayOfWeek;
	}

	public static List<User> getSubordinateUsers(Id userToGetSubordinates, Id roleId){
		Set<Id> subordinateRoles = getSubordinateRoles(userToGetSubordinates,roleId);
		return [select Id, Name, Email, UserRoleId from User where UserRoleId in :subordinateRoles];
	}

	public static List<User> getSubordinateUsersOfSelectedRole(Id userToGetSubordinates, Id roleId, String roleName){
		Set<Id> subordinateRoles = getSubordinateRoles(userToGetSubordinates,roleId);
		return [SELECT Id, Name, FirstName, LastName, Email, UserRoleId FROM User
				WHERE UserRoleId IN :subordinateRoles
				AND UserRole.Name LIKE :roleName];
	}


	public static Set<Id> getSubordinateRoles(Id userId, Id roleId){

        if(roleId == null){
	        // get requested user's role
		    roleId = [select UserRoleId from User where Id = :userId].UserRoleId;
		}

		// get all of the roles underneath the user
	    Set<Id> allSubRoleIds = getAllSubRoleIds(new Set<ID>{roleId});
		return allSubRoleIds;
    }

    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
	    Set<ID> currentRoleIds = new Set<ID>();

		// get all of the roles underneath the passed roles
		for(UserRole userRole :[select Id from UserRole where ParentRoleId IN :roleIds AND ParentRoleID != null]){
			currentRoleIds.add(userRole.Id);
		}

		// we need to get more rolls by recursion
		if(currentRoleIds.size() > 0){
			currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
		}
		return currentRoleIds;
	}

	public static Boolean isUserIsOptedForStatus(Notification_Settings__c userSettings, String status, Boolean checkForAllActive){
		Boolean isOpted = false;

		//Modified for S-399670
		/*if(checkForAllActive && userSettings.All_Active_Orders__c == false){
			return false;
		}*/
		Set<String> validStatus = new Set<String>();
		if(checkForAllActive){
			if(!String.isBlank(userSettings.All_Settings__c)){
			validStatus.addAll(userSettings.All_Settings__c.split(';'));
		}

		}else{
			if(!String.isBlank(userSettings.Settings__c)){
			validStatus.addAll(userSettings.Settings__c.split(';'));
			}

		}
		//end S-399670

		if(validStatus.contains(status)){
			isOpted = true;
		}
		return isOpted;
	}

	public static Map<Id, Set<Id>> getFollowUsersForRecords(Set<Id> recordIds, Set<Id> userIds){
		Map<Id, Set<Id>> recordToUserMap = new Map<Id,Set<Id>>();
		for(NV_Follow__c follow: [SELECT Id, Following_User__c, Record_Id__c
									FROM NV_Follow__c
									WHERE Record_Id__c in: recordIds ]){
			Id recordId = Id.valueOf(follow.Record_Id__c);
			if(recordToUserMap.containskey(recordId)){
				recordToUserMap.get(recordId).add(follow.Following_User__c);
			}
			else{
				recordToUserMap.put(recordId, new Set<Id>{follow.Following_User__c});
			}
			userIds.add(follow.Following_User__c);
		}
		return recordToUserMap;
	}

	public static String addNewLine(String orgText , String newText){
		if(String.isBlank(orgText)){
			return newText;
		}
		else{
			return orgText + '\n' + newText;
		}
	}

	public static String getJoinStringFromSet(Set<String> dataSet, String separator){
		String returnString = null;
		if(dataSet.size()>0){
			returnString = String.join(new List<String>(dataSet), separator);
		}
		return returnString;
	}

	public static String getString(String input){
		return getString(input, '');
	}

	public static String getString(String input , String dependent){
		if(input == null) return '';
		else return input + dependent ;
	}

	public static Profile getProfileByName(String profileName){
		Profile p = null;
		List<Profile> profileData = [SELECT Id, Name From Profile WHERE Name =: profileName LIMIT 1];
		if(profileData.size() > 0){
			p = profileData[0];
		}
		return p;
	}

	public static UserRole getRoleByName(String recordName){
		UserRole record = null;
		List<UserRole> data = [SELECT Id, Name, DeveloperName, ParentRoleId
								From UserRole WHERE Name =: recordName LIMIT 1];
		if(data.size() > 0){
			record = data[0];
		}
		return record;
	}

	public static Boolean isInRange(Datetime dateToCompare, Date startDate, Date endDate){
        if(dateToCompare>= startDate && dateToCompare < endDate){
            return true;
        }
        else{
            return false;
        }
    }
}