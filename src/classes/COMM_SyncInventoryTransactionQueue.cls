// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncInventoryTransactionQueue -  Queueable Apex to sync new Inventory transfer between Org.
//
// 26 Oct 2016, Isha Shukla (T-550252) 
public class COMM_SyncInventoryTransactionQueue implements Queueable,Database.AllowsCallouts{
    List<SVMXC__Stock_Transfer__c> listOfStockTransfer = new List<SVMXC__Stock_Transfer__c>();
    public COMM_SyncInventoryTransactionQueue(List<SVMXC__Stock_Transfer__c> listOfStockTransfer) {
        this.listOfStockTransfer = listOfStockTransfer;  
    }
    public void execute(QueueableContext context) {
        COMM_SyncEBSInventory_REST.InventorySyncRequest inventorySync = new COMM_SyncEBSInventory_REST.InventorySyncRequest();
        COMM_SyncEBSInventory_REST.inventoryDetails inventoryDetails = new COMM_SyncEBSInventory_REST.inventoryDetails();
        List<COMM_SyncEBSInventory_REST.inventoryLine> lstInventoryLine = new List<COMM_SyncEBSInventory_REST.inventoryLine>();
        List<COMM_SyncEBSInventory_REST.inventory> listOfinventory = new list<COMM_SyncEBSInventory_REST.inventory>();
        List<COMM_SyncEBSInventory_REST.inventoryLines> listOfinventoryLines = new List<COMM_SyncEBSInventory_REST.inventoryLines>();
        
        for(SVMXC__Stock_Transfer__c varhead : listOfStockTransfer){
            for(SVMXC__Stock_Transfer_Line__c varLine : varhead.SVMXC__Stock_Transfer_Line__r){
                COMM_SyncEBSInventory_REST.inventoryLine tempInventoryLine = new COMM_SyncEBSInventory_REST.inventoryLine();
                tempInventoryLine.serialLotNumber = new COMM_SyncEBSInventory_REST.TagName(varLine.SVMX_Serial_Number__r.name);
                tempInventoryLine.transactionQuantity = new COMM_SyncEBSInventory_REST.TagName(String.valueOf(varLine.SVMXC__Quantity_Transferred2__c));
                lstInventoryLine.add(tempInventoryLine);            
                COMM_SyncEBSInventory_REST.inventoryLines tempinventoryLines = new COMM_SyncEBSInventory_REST.inventoryLines();
                tempinventoryLines.inventoryLine= lstInventoryLine;
                listOfinventoryLines.add(tempinventoryLines);
                COMM_SyncEBSInventory_REST.inventory tempInventory = new COMM_SyncEBSInventory_REST.inventory();
                tempInventory.inventoryLines = tempinventoryLines;
                tempInventory.headerId = new COMM_SyncEBSInventory_REST.TagName(varhead.Id);
                tempInventory.lineId = new COMM_SyncEBSInventory_REST.TagName(varLine.Id);
                if(varLine.SVMXC__Product__r.ProductCode != null) {
                    tempInventory.itemNumber = new COMM_SyncEBSInventory_REST.TagName(varLine.SVMXC__Product__r.ProductCode); 
                } else if(varLine.Asset__r.External_ID__c != null) {
                    tempInventory.itemNumber = new COMM_SyncEBSInventory_REST.TagName(varLine.Asset__r.External_ID__c);
                }
                tempInventory.sourceLocation = new COMM_SyncEBSInventory_REST.TagName(varhead.SVMXC__Source_Location__r.Location_ID__c);
                tempInventory.transactionQuantity = new COMM_SyncEBSInventory_REST.TagName(String.valueOf(varLine.SVMXC__Quantity_Transferred2__c));
                tempInventory.transactionDate = new COMM_SyncEBSInventory_REST.TagName(Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'));
                tempInventory.transactionTypeId = new COMM_SyncEBSInventory_REST.TagName('02');
                tempInventory.destinationLocation = new COMM_SyncEBSInventory_REST.TagName(varhead.SVMXC__Destination_Location__r.Location_ID__c);
                listOfinventory.add(tempInventory);                
            }
        }
        COMM_SyncEBSInventory_REST.inventoryRequest InventoryRequest = new COMM_SyncEBSInventory_REST.inventoryRequest();
        inventoryDetails.inventory = listOfinventory;
        InventoryRequest.inventoryDetails = inventoryDetails;
        InventoryRequest.IntegrationHeader = COMM_SyncEBSInventory_REST.createIntegrationHeader(Label.ORDER_SFDC,Label.order_commerp);
        inventorySync.InventoryRequest = InventoryRequest;
        List<SVMXC__Stock_Transfer__c> listToUpdateStockTransfer = new List<SVMXC__Stock_Transfer__c>();
        try {
            COMM_SyncEBSInventory_REST.createOrderRestCallout(inventorySync);
            for(SVMXC__Stock_Transfer__c varhead : listOfStockTransfer) {
                varhead.Integration_Status__c = 'Success';
                listToUpdateStockTransfer.add(varhead);
            }
            update listToUpdateStockTransfer;
        } catch(Exception e) {
            for(SVMXC__Stock_Transfer__c varhead : listOfStockTransfer) {
                varhead.Integration_Status__c = 'Failed';
                varhead.Callout_Result__c = 'Error Occured = '+e.getMessage();
                listToUpdateStockTransfer.add(varhead);
            }
            update listToUpdateStockTransfer;
        }
        
    }
}