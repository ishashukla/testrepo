/*******************************************************************************
 Author        :  Appirio JDC (Dipika Gupta)
 Date          :  Dec 13, 2013
 Purpose       :  Display related Intel tag for Account,Contact,Product.
 Reference     :  Task T-217665, T-218030, T-218031 
*******************************************************************************/
public with sharing class IntelListComponentController { 
    
    public String objType {get; set;}
    public String intelId { get; set; }
    public String chatterPostId { get; set; }
    public String commentBody { get; set; }
    public string entityId {get; set;}
    public boolean isUseForIntel {get{
    	if(sObjAccId.startsWith(Constants.INTEL_PREFIX)){
    		return true;
    	}else{
    		return false;
    	}
    } set;}
    
    public String sObjAccId{get;set;}
    private ObjectPaginator paginator {get;set;}
    private Integer DEFAULT_RESULTS_PER_PAGE =Integer.ValueOf('5'); 
    private List<Object> listReturnedObject {get;set;}
    
    
    private List<IntelRelatedList.IntelWrapper> m_lstIntel;
    
    public List<IntelRelatedList.IntelWrapper> lstIntel { 
        get{
            if(m_lstIntel == null){
                List<IntelRelatedList.IntelWrapper> listAllIntel;
                if(sObjAccId.startsWith(Constants.ACCOUNT_PREFIX)){
                	  objType = Constants.ACCOUNT_TYPE;
                    listAllIntel = IntelRelatedList.populateIntelList(Constants.ACCOUNT_TYPE, sObjAccId);
                }
                else if(sObjAccId.startsWith(Constants.CONTACT_PREFIX)){
                	   objType = Constants.CONTACT_TYPE;
                     listAllIntel = IntelRelatedList.populateIntelList(Constants.CONTACT_TYPE, sObjAccId);
                }
                else if(sObjAccId.startsWith(Constants.PRODUCT_PREFIX)){
                	  objType = Constants.PRODUCT_TYPE;
                    listAllIntel =IntelRelatedList.populateIntelList(Constants.PRODUCT_TYPE, sObjAccId);
                }
                else if(sObjAccId.startsWith(Constants.INTEL_PREFIX)){
                	objType = Constants.INTEL_TYPE;
                    listAllIntel =IntelRelatedList.populateIntelList(Constants.INTEL_TYPE, sObjAccId);
                }
                populateListIntel(listAllIntel);
           }
            return m_lstIntel;
        }
        set;
    }
     
    public IntelListComponentController() {}
    
    //----------------------------------------------------------------------------
    // A Method Which Save FeedComments 
    //----------------------------------------------------------------------------
    public PageReference saveReply(){
            PageReference pg;       
            if(chatterPostId != '' && commentBody != ''){
                try {
                    pg = IntelRelatedList.saveReply(chatterPostId, commentBody);
                    List<IntelRelatedList.IntelWrapper> listAllIntel = IntelRelatedList.populateIntelList(objType, sObjAccId);
                    populateListIntel(listAllIntel);
                    commentBody = '';
                    chatterPostId = '';
                } catch(Exception ex){
                    apexpages.addMessages(ex);
                    return null;
                }
            }   
        return pg; 
    }
    
    //----------------------------------------------------------------------------
    // A Method Which Save Intel_Flag 
    //----------------------------------------------------------------------------
    public PageReference saveFlag(){
      PageReference pg; 
                 
      if(intelId != '' && entityId != ''){
        try {
          pg = IntelRelatedList.saveFlag(intelId, entityId);
          List<IntelRelatedList.IntelWrapper> listAllIntel = IntelRelatedList.populateIntelList(objType, sObjAccId);
          populateListIntel(listAllIntel);
          entityId = '';
          chatterPostId = '';
        } catch(Exception ex){
          apexpages.addMessages(ex);
          return null;
        }
      }
      return pg;                
    }
    
    private void populateListIntel(List<IntelRelatedList.IntelWrapper> listAllIntel){
        if(!isUseForIntel){
	        setPagination(listAllIntel);
	        listReturnedObject = paginator.getFirstPage();
	        typeCastObjectList();
        }else{
        	m_lstIntel = new List<IntelRelatedList.IntelWrapper>();
        	m_lstIntel.addAll(listAllIntel);
        }
    }
    
    private void typeCastObjectList(){
        m_lstIntel = new list<IntelRelatedList.IntelWrapper>();
        for(Object obj : listReturnedObject) {
            m_lstIntel.add((IntelRelatedList.IntelWrapper)obj);
        }
    }
    
    /************************************************
    Method to Define ObjectPaginator Object for Pagination for a list of task templates
    **************************************************/
    private void setPagination(List<IntelRelatedList.IntelWrapper> resContents){
      paginator = new ObjectPaginator(DEFAULT_RESULTS_PER_PAGE, resContents);
    }
    
    /************************************************
    method that is used to numbering the record on the page (exa : 1-10 of 100)
    **************************************************/
    public String recordNumbering {
        get {
          if(listReturnedObject != null && listReturnedObject.size() != 0 ) {
            return Paginator.getShowingFrom() + '-' + Paginator.getShowingTo() + ' of ' + Paginator.totalResults;
          }
          return '';
        }set;
    }
      
    /************************************************
    A method that is used to return the total no of pages 
    **************************************************/
    public String pageNumbering {
        get {
          if(listReturnedObject != null && listReturnedObject.size() != 0) {
              return 'Page '+ Paginator.currentPage + ' of ' + Paginator.totalPage;  
          }
          return '';
        }set;
    }
        
    /************************************************
     Returns true if previous page is available, otherwise false 
    **************************************************/
    public boolean hasPrev{
        get{
            return paginator.isPreviousPageAvailable;
        }set;
    }
     
    /************************************************
     Returns true if next page is available, otherwise false 
    **************************************************/ 
    public boolean hasNext{
        get{
           return paginator.isNextPageAvailable;
        }set;
    }
      
   /************************************************
     Returns the previous page of records 
    **************************************************/
    public void previous() {      
        listReturnedObject = Paginator.getPreviousPage();  
        typeCastObjectList();
    }  
        
    
    /************************************************
     Returns the next page of records   
    **************************************************/  
    public void next() {       
       listReturnedObject = Paginator.getNextPage(); 
        typeCastObjectList();
    }
      
    /************************************************
     Return the first page of records   
    **************************************************/     
    public void first() {      
       listReturnedObject = Paginator.getFirstPage();
       typeCastObjectList();
    }
      
    /************************************************
     Return the last page of the records   
    **************************************************/  
    public void last() {      
       listReturnedObject = Paginator.getLastPage();
        typeCastObjectList();
    }
}