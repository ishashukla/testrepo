// 
// (c) 2016 Appirio, Inc.
//
// 
// 19 July 2016    Parul Gupta    Original(T-516600)
//

@isTest(SeeAllData=false)
private class Endo_QualityQuestionnaireControllerTest {

    
    // Test method to test functionality for quality questions
    static testMethod void testQualityQuestions() {
        List<Product2> products = [Select id from Product2];
        createTestData();
        List<QualityQuestionnaire> quaQuestions = new List<QualityQuestionnaire>();
        Quality_Questionnaire__c QualityQuestionnaire = new Quality_Questionnaire__c();
        ApexPages.StandardController sc = new ApexPages.standardController(QualityQuestionnaire);
        Endo_QualityQuestionnaireController controller1 = new Endo_QualityQuestionnaireController();
        Endo_QualityQuestionnaireController controller = new Endo_QualityQuestionnaireController(sc);
        controller.QualityQuestionnaire.Product__c = products[0].id;
        controller.productLine = 'Monitors';
        controller.quaQuestions = new List<QualityQuestionnaire>();
        quaQuestions = controller.quaQuestions ;
        controller.showNextQuestions();
        System.assert(controller.qualityQuestions.size() > 0);
        controller.qualityQuestions[0].answer.answer__c = 'Test Answer';
        controller.save();
        
        //after saving answer for answer will be inserted so assertion for checking answer size.
        System.assert([Select id from Answer__c].size() > 0);
        controller.cancel();
        
    }//End test()
    
    // Test method to test functionality for Regulatory questions
    static testMethod void testRegulatoryQuestions() {
        List<Product2> products = [Select id from Product2];
        Quality_Questionnaire__c QualityQuestionnaire = new Quality_Questionnaire__c();
        ApexPages.StandardController sc = new ApexPages.standardController(QualityQuestionnaire);
        Endo_QualityQuestionnaireController controller = new Endo_QualityQuestionnaireController(sc);
        controller.QualityQuestionnaire.Product__c = products[0].id;
        controller.showNextQuestions();
        System.assert(controller.regulatoryQuestions.size() > 0);
        controller.regulatoryQuestions[0].answer.answer__c = 'Test Answer';
        controller.save();
        
        //after saving answer for answer will be inserted so assertion for checking answer size.
        System.assert([Select id from Answer__c].size() > 0);
        controller.cancel();
        
    }//End testTypeRegulatory()
    
    // Set up test data
    @testSetup static void createTestData(){
       
        // Question List
        List<Question__c> questionList = new List<Question__c>();
        questionList.add(Endo_TestUtils.createQuestion('iSuite or Cart?', 'Quality', false));
        questionList[0].Product_Line__c= 'Monitors';
        questionList.add(Endo_TestUtils.createQuestion('iSuite or Cart1?', 'Regulatory', false));
        
        insert questionList;
        
        //Product List
        List<Product2> products = Endo_TestUtils.createProduct(2, true);
        System.assert(products.size() > 0);
    }//End createTestData1()
}//End Class Endo_QualityQuestionnaireController_Test