//
// (c) 2014 Appirio, Inc.
// Class RolesUtility
// class to get manager from user id and team from Manager ID.
//
// 09 Feb 2016     Sahil Batra    Original(S-374034)
// 08-March-2016   Sahil Batra    Modified(I-206589) // Added method getManagertoUsersMap and getUsertoManagerMap to bulkify the code to be used in Trigger
public with sharing class RolesUtility {

 public static Set<Id> getRoleSubordinateUsers(Id userId) {

    // get requested user's role
    Id roleId = [select UserRoleId from User where Id = :userId].UserRoleId;
    // get all of the roles underneath the user
    Set<Id> allSubRoleIds = getAllSubRoleIds(new Set<ID>{roleId});
    System.debug('Sub Roles'+allSubRoleIds);
    // get all of the ids for the users in those roles
    Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where 
      UserRoleId IN :allSubRoleIds]);
    // return the ids as a set so you can do what you want with them
    System.debug(users.keySet());
  return users.keySet();
  }

  private static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {

    Set<ID> currentRoleIds = new Set<ID>();

    // get all of the roles underneath the passed roles
    for(UserRole userRole :[select Id from UserRole where ParentRoleId 
      IN :roleIds AND ParentRoleID != null])
    currentRoleIds.add(userRole.Id);

    // go fetch some more rolls!
    if(currentRoleIds.size() > 0)
      currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));

    return currentRoleIds;

  }
  
  public static Set<Id> getManagerId(Id userId){
    Set<ID> currentRoleIds = new Set<ID>();
    Id roleId = [select UserRoleId from User where Id = :userId].UserRoleId;
    for(UserRole userRole :[select ParentRoleId,Id from UserRole where Id =:roleId AND ParentRoleID != null]){
        currentRoleIds.add(userRole.ParentRoleId);
    }
    Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :currentRoleIds]);
    // returns the Managers Id for a user
    return users.keySet();
  }
  
  public static Map<Id,set<Id>> getManagertoUsersMap(Set<Id> managerIdSet){
  	Map<Id,Set<Id>> managerIdtoUSerId = new Map<Id,Set<Id>>();
  	Map<Id,Id> managerIdtoRoleId = new Map<Id,Id>();
  	Map<Id,Set<Id>> parentRoletoChildRole = new Map<Id,Set<Id>>();
  	Map<Id,Set<Id>> childRoletoUserIds = new Map<Id,Set<Id>>();
  	Set<Id> allManagerRoles = new Set<Id>();
  	Set<Id> allChildRoles = new Set<Id>();
  	List<User> userList = new List<User>();
  	userList = [SELECT UserRoleId FROM User WHERE Id IN :managerIdSet];
  	for(User user : userList){
  		managerIdtoRoleId.put(user.Id,user.UserRoleId);
  		allManagerRoles.add(user.UserRoleId);
  	}
  	List<UserRole> userRoleList = new List<UserRole>();
  	userRoleList = [SELECT Id,ParentRoleId FROM UserRole WHERE ParentRoleId IN :allManagerRoles AND ParentRoleID != null];
  	for(UserRole role : userRoleList){
  		if(!parentRoletoChildRole.containsKey(role.ParentRoleId)){
  			parentRoletoChildRole.put(role.ParentRoleId,new Set<Id>());
  		}
  		parentRoletoChildRole.get(role.ParentRoleId).add(role.Id);
  		allChildRoles.add(role.Id);
  	}
  	userList = new List<User>();
  	userList = [SELECT Id,UserRoleId FROM User WHERE UserRoleId IN :allChildRoles];
  	for(User user : userList){
  		if(!childRoletoUserIds.containsKey(user.UserRoleId)){
  			childRoletoUserIds.put(user.UserRoleId,new Set<Id>());
  		}
  		childRoletoUserIds.get(user.UserRoleId).add(user.Id);
  	}
  	for(Id managerId : managerIdtoRoleId.keySet()){
  		Id managerRole = managerIdtoRoleId.get(managerId);
  		if(parentRoletoChildRole.containsKey(managerRole)){
  			Set<Id> childRoles = new Set<Id>();
  			childRoles = parentRoletoChildRole.get(managerRole);
  			for(Id childRoleId : childRoles){
  				if(childRoletoUserIds.containsKey(childRoleId)){
  					if(!managerIdtoUSerId.containsKey(managerId)){
  						managerIdtoUSerId.put(managerId,new Set<Id>());
  					}
  					managerIdtoUSerId.get(managerId).addAll(childRoletoUserIds.get(childRoleId));
  				}
  			}
  		}
  	}
  	return managerIdtoUSerId;
  }
  public static Map<Id,Set<Id>> getUsertoManagerMap(Set<Id> userIdSet){
  	Map<Id,Set<Id>> userIdtoManagerId = new Map<Id,Set<Id>>();
  	Map<Id,Id> userIdtoRoleId = new Map<Id,Id>();
  	Map<Id,Set<Id>> roleIdtoParentRoleId = new Map<Id,Set<Id>>();
  	Map<Id,Set<Id>> parentRoleIdtoUserId = new Map<Id,Set<Id>>();
  	List<User> userList = new List<User>();
  	userList = [SELECT UserRoleId FROM User WHERE Id IN :userIdSet];
  	Set<Id> allUserRoleSet = new Set<Id>();
  	Set<Id> allUserParentRoleSet = new Set<Id>();
  	for(User user : userList){
  		userIdtoRoleId.put(user.Id,user.UserRoleId);
  		allUserRoleSet.add(user.UserRoleId);
  	}
  	List<UserRole> userRoleList = new List<UserRole>();
  	userRoleList = [SELECT ParentRoleId,Id from UserRole where Id IN :allUserRoleSet AND ParentRoleID != null];
  	for(UserRole role : userRoleList){
  		if(!roleIdtoParentRoleId.containsKey(role.Id)){
  			roleIdtoParentRoleId.put(role.Id,new Set<Id>());
  		}
  		roleIdtoParentRoleId.get(role.Id).add(role.ParentRoleId);
  		allUserParentRoleSet.add(role.ParentRoleId);
  	}
  	userList = new List<User>();
  	userList = [SELECT Id,UserRoleId FROM User WHERE UserRoleId IN :allUserParentRoleSet];
  	for(User user : userList){
  		if(!parentRoleIdtoUserId.containsKey(user.UserRoleId)){
  			parentRoleIdtoUserId.put(user.UserRoleId,new Set<Id>());
  		}
  		parentRoleIdtoUserId.get(user.UserRoleId).add(user.Id);
  	}
  	for(Id userId : userIdtoRoleId.keySet()){
  		Id roleId = userIdtoRoleId.get(userId);
  		if(roleIdtoParentRoleId.containsKey(roleId)){
  			Set<Id> parentRoleIds = new Set<Id>();
  			parentRoleIds = roleIdtoParentRoleId.get(roleId);
  			for(Id parentRoleId : parentRoleIds){
  				if(parentRoleIdtoUserId.containsKey(parentRoleId)){
  					Set<Id> users = new Set<Id>();
  					users = parentRoleIdtoUserId.get(parentRoleId);
  					userIdtoManagerId.put(userId,users);
  				}
  			}
  		}
  	}
  	return userIdtoManagerId;
  }
  
  public static Map<Id,Set<Id>> getManagerId(Set<Id> userId){
    //Set<Id> testSet = new Set<Id>{'005c0000002Ib3C'};
    List<User> userIdList = [SELECT Id, Name FROM User WHERE Id IN: userId];
    Set<Id> userIDsSet = new Set<Id>();
    for(User ids : userIdList){
      userIDsSet.add(ids.Id);
    }
    Map<Id, Set<Id>> roleIdToUserIdMap = new Map<Id, Set<Id>>();
    Set<ID> currentRoleIds = new Set<ID>();
    List<User> roleIdList = [SELECT UserRoleId from User where Id IN :userIDsSet AND UserRoleId!= null];
    for(User usr : roleIdList){
      if(!roleIdToUserIdMap.containsKey(usr.UserRoleId)){
        roleIdToUserIdMap.put(usr.UserRoleId, new Set<Id>());
      }
      roleIdToUserIdMap.get(usr.UserRoleId).add(usr.Id);
    }
    system.debug('roleIdToUserIdMap>>>'+roleIdToUserIdMap);
    Map<Id,Set<Id>> parentRoleToRoleId = new Map<Id,Set<Id>>();
    for(UserRole userRole :[select ParentRoleId,Id from UserRole where Id IN :roleIdToUserIdMap.keySet() AND ParentRoleID != null]){
      currentRoleIds.add(userRole.Id);
      if(!parentRoleToRoleId.containsKey(userRole.ParentRoleId)){
        parentRoleToRoleId.put(userRole.ParentRoleId, new Set<Id>());
      }
      parentRoleToRoleId.get(userRole.ParentRoleId).add(userRole.Id);
    }
    system.debug('parentRoleToRoleId>>>'+parentRoleToRoleId);
    system.debug('parentRoleToRoleId.keySet()>>>'+parentRoleToRoleId.keySet());
    //Map<Id,User> mapToGetManagerIds = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :parentRoleToRoleId.keySet()]);
    List<User> lstUsers = new List<User>([Select Id, Name, UserRoleId From User where UserRoleId IN :parentRoleToRoleId.keySet()]);
    Map<Id,User> mapToGetManagerIds = new Map<Id, User>();
    /*for(User usr : lstUsers){
      if(!mapToGetManagerIds.containsKey(usr.Id)){
        mapToGetManagerIds.put(usr.Id, usr);
      }
    }*/
    Map<Id,Set<Id>> parentRoleIdToManagerId = new Map<Id,Set<Id>>();
    /*for(String parentRoleIds : parentRoleToRoleId.keySet()){
      if(!parentRoleIdToManagerId.containsKey(parentRoleIds)){
        parentRoleIdToManagerId.put(parentRoleIds, new Set<Id>());
      }
      parentRoleIdToManagerId.get(parentRoleIds).addAll(mapToGetManagerIds.keySet());
    }*/
    for(User usrId : lstUsers){
      if(!parentRoleIdToManagerId.containsKey(usrId.UserRoleId)){
        parentRoleIdToManagerId.put(usrId.UserRoleId, new Set<Id>());
      }
      parentRoleIdToManagerId.get(usrId.UserRoleId).add(usrId.Id);
    }
    system.debug('parentRoleIdToManagerId>>>'+parentRoleIdToManagerId);
    Map<Id,Set<Id>> parentIdToUsers = new Map<Id,Set<Id>>();
    for(String pmId : parentRoleToRoleId.keySet()){
      for(String roleId : parentRoleToRoleId.get(pmId)){
        if(roleIdToUserIdMap.containsKey(roleId)){
          for(Id userIds :roleIdToUserIdMap.get(roleId)){
            if(!parentIdToUsers.containsKey(pmId)){
             parentIdToUsers.put(pmId, new Set<Id>());
            } 
            parentIdToUsers.get(pmId).add(userIds);
          }
        }
      }  
    }
    system.debug('parentIdToUsers>>>'+parentIdToUsers);
    Map<Id,Set<Id>> managerToUserId = new Map<Id,Set<Id>>();
    for(String prId : parentRoleIdToManagerId.keyset()){
      if(parentIdToUsers.containsKey(prId)){
        for(String manId : parentRoleIdToManagerId.get(prId)){
           system.debug('prId>>>'+prId+'manId>>>'+manId);
           system.debug('manId>>>'+manId+'parentIdToUsers.get(prId)>>>'+parentIdToUsers.get(prId));
           managerToUserId.put(manId, parentIdToUsers.get(prId));
        }
      }
    }
    system.debug('managerToUserId>>>'+managerToUserId);
    return managerToUserId;
  }

}