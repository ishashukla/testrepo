// (c) 2015 Appirio, Inc.
//
// Class Name: AutoCompleteControllerTest 
// Description: Test Class for AutoCompleteController class.
// 
// April 22 2016, Isha Shukla  Original 
//
@isTest 
public class AutoCompleteControllerTest {
    static List<AutoCompleteResultSize__c> recordSizeList;
    static BusinessUnitAndPricebookName__c bupRecord;
   static BusinessUnitAndPricebookName__c bupRecord2;
    static List<Product2> productList;
    static testMethod void testAutoCompleteController() {
      List<User> user = TestUtils.createUser(2, 'System Administrator', false );
      user[0].Division = 'IVS';
      user[1].Division = 'IVS';
      insert user;
        System.runAs(user[0]) {
       createData();
       insert recordSizeList;
       insert bupRecord;
       
       Test.startTest();
       autoCompleteController.findSObjects('Product2','','Name','Stryker','IVS');
       Test.stopTest();
        }
       System.assertEquals(True,productList != Null);
       System.assertEquals('IVS',bupRecord.Name);
    }
    static testMethod void testAutoCompleteControllerWithNullObject() {
        List<User> user = TestUtils.createUser(2, 'System Administrator', false );
      user[0].Division = 'IVS';
      user[1].Division = 'IVS';
      insert user;
        System.runAs(user[0]) {
       createData();
       insert bupRecord;
       Test.startTest();
       autoCompleteController.findSObjects('Opportunity','','Name','Stryker','IVS');
       Test.stopTest();
        }
       System.assertEquals('IVS',bupRecord.Name);
    }
    static testMethod void testAutoCompleteControllerWithoutRecordSizeList() {
        List<User> user = TestUtils.createUser(2, 'System Administrator', false );
      user[0].Division = 'IVS';
      user[1].Division = 'IVS';
      insert user;
        System.runAs(user[0]) {
       createData();
       insert bupRecord;
       
       Test.startTest();
       autoCompleteController.findSObjects('Product2','','Name','Stryker','IVS');
       Test.stopTest();
        }
       System.assertEquals(True,productList != Null);
       System.assertEquals('IVS',bupRecord.Name);
    }
    public static void createData() {
      recordSizeList = new List<AutoCompleteResultSize__c>();
      recordSizeList.add(new AutoCompleteResultSize__c(Name = 'Product2' ,Size_of_Records__c = 49));
      recordSizeList.add(new AutoCompleteResultSize__c(Name = 'System__c' ,Size_of_Records__c = 50));
     
      bupRecord = new BusinessUnitAndPricebookName__c(Name='IVS',System_Type__c='Stryker',Price_Book_Name__c='IVS Price Book');
      
      bupRecord2 = new BusinessUnitAndPricebookName__c(Name='BaseTrail',System_Type__c='baseTrail',Price_Book_Name__c='IVS Price Book');
     
      productList = TestUtils.createProduct(1, True);
      Id pricebookId = Test.getStandardPricebookId();
      PriceBook2 pricebook2 = new PriceBook2(Name='IVS Price Book',isActive = True);
      insert pricebook2;
      PriceBookEntry entrystd = new PriceBookEntry(Pricebook2Id = pricebookId,Product2Id = productList[0].Id,UnitPrice = 1000,isActive = True);
      insert entrystd;
      PriceBookEntry entrycus = new PriceBookEntry(Pricebook2Id = pricebook2.Id,Product2Id = productList[0].Id,UnitPrice = 1000,isActive = True);
      insert entrycus;
      
      List<Account> acc = TestUtils.createAccount(1, true);
      TestUtils.createCommConstantSetting();
      Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
      Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
      List<Opportunity> oppList = TestUtils.createOpportunity(1,acc[0].Id,false);
      oppList[0].Business_Unit__c = 'IVS';
      oppList[0].Opportunity_Number__c = '50001';
      oppList[0].recordtypeId =  recordTypeInstrument;
      insert oppList;
    }
}