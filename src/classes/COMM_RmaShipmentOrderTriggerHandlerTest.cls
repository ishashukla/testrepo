/*******************************************************************************
Name          :  CommPartsOrderTriggerHandlerTest
Author        :  Appirio JDC (Jagdeep Juneja)
Date          :  July 12, 2016 
Purpose       :  Test Class for Comm PartsOrderTriggerHandler

*******************************************************************************/
@isTest(seeAllData=True)
private class COMM_RmaShipmentOrderTriggerHandlerTest{
    public static testmethod void populateOpportunityFromOrder(){

        Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware');
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,UnitPrice = 12000, IsActive = true);
        insert customPrice; 
        COMM_Constant_Setting__c check = COMM_Constant_Setting__c.getInstance();
        if(check.COMM_Price_Book_Id__c == null) {
            COMM_Constant_Setting__c c = new COMM_Constant_Setting__c();
            c.COMM_Price_Book_Id__c = customPB.Id;
            insert c;
        }
        Account acc = TestUtils.createAccount(1, false).get(0);
        insert acc;
        
        Contact con = TestUtils.createContact(1, acc.Id, false).get(0);
        insert con;
        
       /* List<Pricebook2> priceBookList = TestUtils.createPriceBook(5,' ',false);
        priceBookList.get(0).Name = 'COMM Price Book';
        insert priceBookList; */
        List<Opportunity> oppList = TestUtils.createOpportunity(2, acc.Id, true);
        List<Order> orderList = TestUtils.createOrder(2, acc.id, 'Entered',  false);
        orderList[0].OpportunityId = oppList[0].Id;
        orderList[1].OpportunityId = oppList[1].Id;
        insert orderList;
        
        Id changeRequestCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.COMM_CASE_RTYPE).getRecordTypeId();
        RTMap__c rtmap = RTMap__c.getvalues(changeRequestCaseRT);
        if(rtmap == null) {
            TestUtils.createRTMapCS(changeRequestCaseRT, Constants.COMM_CASE_RTYPE, 'COMM');
        }
        
        List<Case> caseList = TestUtils.createCase(1, acc.Id, false);
        caseList[0].RecordTypeId = changeRequestCaseRT;
        insert caseList;

       // User u = TestUtils.createUser(1, 'System Administrator', true).get(0);

       // List<SVMXC__ServiceMax_Processes__c> svmxcProcesses = TestUtils.createServiceMaxProcess(1, u.Id, true);
        //TestUtils.createServiceMaxConfig(1, svmxcProcesses[0].Id, true);

        List<SVMXC__RMA_Shipment_Order__c> partOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
        partOrderList.add(new SVMXC__RMA_Shipment_Order__c());
        partOrderList[0].SVMX_Order__c = orderList[0].Id;
        partOrderList[0].SVMXC__Case__c = caseList[0].Id;
        partOrderList[0].SVMXC__Order_Status__c = 'Open';
        partOrderList[0].SVMXC__Company__c = acc.Id;
        partOrderList[0].SVMXC__Contact__c = con.Id;
        //partOrderList[0].SVMX_RMA_Number__c = '1234';
        partOrderList[0].RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId();
        //system.debug('######RT Id of Parts Order'+partOrderList[0].RecordTypeId);
        //system.debug('######Inside Test Class'+partOrderList);
        Test.startTest();
        insert partOrderList;
        Test.stopTest();
        //System.assertEquals(partOrderList.get(0).SVMX_Opportunity__c,oppList[0].Id ,'Both Opportunity Id are not equals');
        partOrderList.get(0).SVMX_Order__c = orderList[1].Id;
        update partOrderList;
       // System.assertEquals(partOrderList.get(0).SVMX_Opportunity__c,oppList[1].Id ,'Both Opportunity Id are not equals');
    }
}