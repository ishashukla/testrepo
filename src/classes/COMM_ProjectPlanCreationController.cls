/**================================================================      
* Appirio, Inc
* Name: COMM_ProjectPlanCreationController 
* Description: Used to create a record of new project plan record using COMM_ProjectPlanCreationPage. (T-492006)
* Created Date: 12th April 2016
* Created By: Kirti Agarwal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 15th April 2016    Kirti Agarwal    T-491567 - populate OwnerId and Project_Manager__c fields
* 4th August 2016    Nitish Bansal    I-228021 - Make sure PM is able to create project plan record with correct values 
* 6th August 2016    Nitish Bansal    I-228107, Setting default PM if no PM exists
* 12th October 2016  Deepti Maheshwari I-239537 - Defaulting business unit to COMM
==================================================================*/

public class COMM_ProjectPlanCreationController {
  public Project_Plan__c projectPlan {get; set;}
  private String opportunityId;
  private String accountId;
  
  
  //Added by Rahul Aeran for SF1 compatibility
  public Account acc{get;set;}
  public List<sObject> accList{get;set;}
  
  public User projectManager{get;set;}
  public List<sObject> userList{get;set;}
  
  public User projectEngineer{get;set;}
  public List<sObject> projectEngineerList{get;set;}
  public  String s;
 
 
  //constructor
  public COMM_ProjectPlanCreationController(ApexPages.standardController ctrl) {
    projectPlan = (Project_Plan__c)ctrl.getRecord(); // NB - 08/04 - I-228021 - To make sure values are binded to controller in classic view
    opportunityId  = ApexPages.currentPage().getParameters().get('oppId');
    accountId = ApexPages.currentPage().getParameters().get('accId');
    projectPlan.Account__c = accountId;
    projectPlan.Order_Type__c = Constants.SINGLE_PO;
    //DM Fix for I-239537 - Defaulting business unit to COMM
    //projectPlan.Business_Unit__c = Constants.COMM;
    //Added by Rahul Aeran for SF1 compatibility
    if(accountId!=null) {
        acc = new Account(Id=accountId);
    }else {
        acc=new Account();
    }
     
    accList = new sObject[]{acc};
    
    projectManager = new User(); 
    userList = new sObject[]{projectManager};
    
    projectEngineer = new User(); 
    projectEngineerList = new sObject[]{projectEngineer};
        
  }

  //================================================================      
  // Name         : saveProjectPlan
  // Description  : Functionality for save button to save the record and 
  // redirect the page to the record created
  // Created Date : 12th April 2016
  // Created By   : Kirti Agarwal (Appirio)
  //================================================================== 
  public PageReference saveProjectPlan() {
   
    system.debug('UserInfo.getUiThemeDisplayed() : ' + UserInfo.getUiThemeDisplayed());
    //System.debug('*****************************'+accList);
    //We need to handle salesforce1 scenario here as we are providing custom lookups here
      if(!String.isBlank(UserInfo.getUiThemeDisplayed()) && UserInfo.getUiThemeDisplayed().toLowerCase() == Constants.THEME_SF1 ){
        String accID = accList.get(0).Id;
        if(accID.length() != 0){
            projectPlan.Account__c = accList.get(0).Id;
        }else{
            //System.debug('((((((((((((((((((('+accList.get(0).Id);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Account : You must enter a value'));
            return null;
        }
        if(userList != null && userList.size() > 0 && userList.get(0).Id != null){
           s=userList.get(0).Id;
           System.debug('>>>>>>>>>>>'+userList.get(0).Id);
           projectPlan.Project_Manager__c = userList.get(0).Id;
        }else{
           s= '';
        }
        if(projectEngineerList.get(0).Id != null){
            projectPlan.Project_Engineer__c = projectEngineerList.get(0).Id;
        }
        
      }
      
      
    Savepoint sp = Database.setSavepoint();
    try {
      String projectManagerId;
      String projectEngineerId;
      if(opportunityId  != '' && opportunityId  != null) {
        //T-491567 - populate OwnerId and Project_Manager__c fields
        //T-492006 - populate the Project_Manager__c, Project_Engineer__c , Receptionist__c 
        for(OpportunityTeamMember atm : [SELECT Id, 
                                                UserId, 
                                                TeamMemberRole 
                                         FROM OpportunityTeamMember 
                                         WHERE OpportunityId =: opportunityId 
                                         AND(TeamMemberRole = :Constants.PROJECT_MANAGER
                                            OR TeamMemberRole = :Constants.PROJECT_SERVICES_MANAGER
                                            OR TeamMemberRole = :Constants.PROJECT_ENGINEER
                                            ) 
                                         Order by TeamMemberRole DESC]) {
            
            if(atm.TeamMemberRole == Constants.PROJECT_MANAGER || atm.TeamMemberRole == Constants.PROJECT_SERVICES_MANAGER) {
              projectManagerId = atm.UserId;
            }
            else if(atm.TeamMemberRole == Constants.PROJECT_ENGINEER) {
              projectEngineerId =  atm.UserId;
            }         
        }
        
        //I-228107 NB - 08/06 - Start - Setting default PM if no PM exists
        COMM_Constant_Setting__c constantsSetting = COMM_Constant_Setting__c.getOrgDefaults();
        system.debug('PM : ' + constantsSetting.Default_PM_Sales_Rep_ID__c);
        if(projectManagerId == null) {
          List<User> defaultPMList = new List<User>([Select Id from User where IsActive = true AND 
                          Sales_Rep_ID__c = :constantsSetting.Default_PM_Sales_Rep_ID__c]);
          system.debug('OM List : ' + defaultPMList);
          if(defaultPMList != null && defaultPMList.size() > 0){
              projectManagerId = defaultPMList.get(0).Id;        
          }   
        }
        system.debug('Project Manager ID : ' + projectManagerId);
        system.debug('Project Manager ID sss : ' + s);
        if(projectManagerId == null) {
            projectManagerId = Userinfo.getUserId();
        }
        //I-228107 NB - 08/06 - End
        
        //NB - 08/04 - I-228021 Start- To make sure user entered values are set on the record
        if(projectPlan.Project_Manager__c == null || (s != null && !s.contains('005'))){
          projectPlan.Project_Manager__c = projectManagerId;
        }
        if(projectPlan.Project_Engineer__c == null){
          projectPlan.Project_Engineer__c = projectEngineerId;
        }

        projectPlan.OwnerId = projectPlan.Project_Manager__c;
        //NB - 08/04 - I-228021 - END
        try {
            insert projectPlan;
            
        }catch(Exception e) {
            System.debug('*********************'+e.getMessage());
            
        }
    
    
        Opportunity opport = new Opportunity (id = opportunityId, Project_Plan__c = projectPlan.id);
        upsert opport;
        
        //NB - 06/28 - I-224217 - Start
        //If project plan has a PE
        if(projectPlan.Project_Engineer__c != null){
            String key =  null;
            List<Project_Phase__c> projPhaseList = new List<Project_Phase__c>();
            Map<Id,List<Project_Phase__c>> ppIdToPhaseListMap = new Map<Id,List<Project_Phase__c>>();
            Set<String> setExistingUserWithRoles = new Set<String>();
            List<Custom_Account_Team__c> lstNewMembersToInsert = new List<Custom_Account_Team__c>(); 
            List<Project_Plan__Share> projectShareList = new List<Project_Plan__Share>(); 
            //List<Project_Phase__Share> lstSharingRecordsToInsert = new List<Project_Phase__Share>();
            
            //Quering the opportunity along with opp team associated with this project plan
            List<Opportunity> lstOpportunities = [SELECT Id,AccountId, Project_Plan__c, 
                                                  (SELECT Id,UserId,TeamMemberRole FROM OpportunityTeamMembers WHERE UserId =: projectPlan.Project_Engineer__c)
                                                  FROM Opportunity WHERE Project_Plan__c = :projectPlan.Id];
                                                  
            //Quering all project phases for the inserted project plans if any exists
            /*for(Project_Phase__c projectPhase : [Select Id, Project_Plan__c from Project_Phase__c 
                                                WHERE Project_Plan__c = :projectPlan.Id]){
                projPhaseList.add(projectPhase);
            }*/
            
            String PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
            List<RoleAccessProjectSetting__c> roleAccessSettings = [Select Name, Project_Access__c from RoleAccessProjectSetting__c];
            
            for(Opportunity opp : lstOpportunities){
                for(OpportunityTeamMember otm : opp.OpportunityTeamMembers){
                  key = otm.UserId + Constants.TILDE_DELIMITER + otm.TeamMemberRole + Constants.TILDE_DELIMITER + opp.Project_Plan__c;
                  
                  if(!setExistingUserWithRoles.contains(key)){
                        //Adding it to the set so that the same user record doesn't gets created from other opportunity on same project
                        setExistingUserWithRoles.add(key);
                        //we will create the new custom team records only if there isn't one already existing
                        
                        lstNewMembersToInsert.add(new Custom_Account_Team__c(User__c = otm.UserId,Team_Member_Role__c = otm.TeamMemberRole,Project__c = opp.Project_Plan__c,
                                                  Account__c = opp.AccountId,RecordTypeID = PROJECT_TEAM_RECORDTYPE_ID));
                        projectShareList.add(new Project_Plan__Share(
                                    AccessLevel = Constants.PERM_EDIT, ParentId = opp.Project_Plan__c, UserOrGroupId = otm.UserId));
                    }
                    //Giving edit access on the project phase records
                    /*for(Project_Phase__c pPhase : projPhaseList){
                        lstSharingRecordsToInsert.add(new Project_Phase__Share(
                                AccessLevel = Constants.PERM_EDIT, ParentId = pPhase.Id, UserOrGroupId = otm.UserId));
                    }*/
                                          
                  }
                }
                
                if(lstNewMembersToInsert.size() > 0){
                
                    insert lstNewMembersToInsert;
                    
                    if(projectShareList.size() > 0){
                        try {
                            Database.SaveResult[] results = Database.insert(projectShareList,false);
                            if(results != null){
                                Integer index = 0;
                                for(Database.SaveResult result : results) {
                                  if(!result.isSuccess()) {
                                    system.debug('RECORD : ' + projectShareList.get(index) + 'ERROR::' + result.getErrors());
                                  }
                                  index++;
                                }
                            }
                        } catch (Exception e) {
                            System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
                        }
                    }
                    
                    //Providing access on project phase
                    /*if(lstSharingRecordsToInsert.size() > 0){
                        try {
                            Database.SaveResult[] results = Database.insert(lstSharingRecordsToInsert,false);
                            if(results != null){
                                Integer index = 0;
                                for(Database.SaveResult result : results) {
                                  if(!result.isSuccess()) {
                                    system.debug('RECORD : ' + lstSharingRecordsToInsert.get(index) + 'ERROR::' + result.getErrors());
                                  }
                                  index++;
                                }
                            }
                        } catch (Exception e) {
                            System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
                        }
                    }*/
                }
                 
            }
          
       } 
        //NB - 06/28 - I-224217 - ENd       
        
        //Commented by Rahul Aeran as now we have a trigger on Opportunity to achieve this functionality
        /*List<Order> orderList = new List<Order>();
        List<OrderItem> orderItemList = new List<OrderItem>();

        for(Order orderRec : [SELECT ID, Project_Plan__c,
                                     (SELECT Id, Project_Plan__c 
                                        FROM orderItems) 
                                FROM Order 
                               WHERE opportunityId =: opportunityId]) {
          orderRec.Project_Plan__c = projectPlan.id;
          orderList.add(orderRec);
          for(OrderItem oI : orderRec.orderItems) {
             oI.Project_Plan__c = projectPlan.id;
             orderItemList.add(oI);
          }
        }
        
        if(!orderList.isEmpty()) {
          update orderList;
        }
        
        if(!orderItemList.isEmpty()) {
          update orderItemList;
        }*/
        
        PageReference recordPage = new PageReference('/' + projectPlan.id);
        recordPage.setRedirect(true);
        return recordPage;
     }catch(exception e) {
      system.debug('Exception : ' + e.getMessage());
      //Rolling back the transaction 
      Database.rollback(sp);
      if (e.getMessage().contains('REQUIRED_FIELD_MISSING')) {
        //apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.error, 'you must enter required fields');
        //apexpages.addmessage(msg);
        
      }else{
        
        //Showing the user what the error is and rolling back the transaction
        apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.error, e.getMessage());
        apexpages.addmessage(msg);
      }
      return null;
      system.debug('exception : ' + e.getMessage());
    }
    return null;
  }
  
  public pageReference cancelRecord() {
      PageReference recordPage;
      system.debug('OpptyID : ' + opportunityId);
      recordPage = new PageReference('/' + opportunityId);
      recordPage.setRedirect(true);
      return recordPage;
  }
}