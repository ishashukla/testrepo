/*************************************************************************************************
Created By:    Megha Ag (Appirio)
Date:          Nov 28, 2015
Description  : Trigger handler

Modification History:
* Feb 18, 2016; sunil; // T-476922 Send Email only when custom setting flag is true.
* May 19, 2016: PC // I-218941 added method updateOrderTotal
* 21st Oct 2016     Nitish Bansal       I-240941 // TOO Many SOQL resolution (Proper list checks before SOQL and DML, Constansts check)
* Dec 1, 2016		Gagan Brar			I-234370 | Added logic for update Capital Order Checkbox on Order header
* Dec 1, 2016		Gagan Brar			I-246396 | Added logic to update Requested Date on Order header based on Requested Ship Date on Order Lines
**************************************************************************************************/
public class OrderProductsTriggerHandler {
    private final static String ORDER_STATUS_SCHEDULED = 'Scheduled';
    private final static String X531_IN_MANUFACTURING ='531';
    private final static String X530_IN_MANUFACTURING ='530';
    private final static String X574_IN_MANUFACTURING ='574';
    public static void afterInsert(List<OrderItem> oItems){
        if(Constants.isTriggerExecuted){
            updateOrders(oItems, null);
            Constants.isTriggerExecuted = true; 
        }
        updateOrderTotal(oItems, null);
    }

    public static void afterUpdate(List<OrderItem> oItems , Map<Id, OrderItem> oldOItems){
        if(Constants.isTriggerExecuted){
            updateOrders(oItems, oldOItems);
            Constants.isTriggerExecuted = true; 
        }
        updateOrderTotal(oItems, oldOItems);
    }
    //  Update Capital Order CheckBox on Order(Std) Object
    //  @param New List of OrderItem, Old Map of OrderItem
    //  @return void
    // Added by PC on 19th May for I-218941

    private static void updateOrderTotal(List<OrderItem> oItems , Map<Id, OrderItem> oldOItems){
        List<Order> updatedOrder = new List<Order>();
        List<Id> orderID = new List<Id>();
        Map<Id,List<Product2>> productID = new Map<Id, List<Product2>>();
        Map<Id, Order> mapOrder = new Map<Id, Order>();
        Map<Id,Decimal> mapOrderItems = new Map<Id, Decimal>();
        Date RequestedDate = null;
        
        for(OrderItem oi : [Select Id,OrderId,PricebookEntry.Product2.Base_or_Capital__c from OrderItem where Id IN :oItems ]){
            orderID.add(oi.OrderId);
            if(!productID.containsKey(oi.OrderId)){
                productID.put(oi.OrderId,new List<Product2>());
            }
                productID.get(oi.OrderId).add(oi.PricebookEntry.Product2);
        }
        Decimal orderTotal = 0 ; 
        for(Order orderFirst : [Select Id, Order_Total__c, CapitalOrder__c, Requested_Date__c, (Select Quantity, UnitPrice, Sub_Total__c, Requested_Ship_Date__c from OrderItems) from Order where Id IN :orderID]){ //NB - 11/11 - I-243413
            mapOrder.put(orderFirst.id, orderFirst);
            for(OrderItem orderitemFirst : orderFirst.OrderItems){
                //orderTotal = orderTotal + orderitemFirst.Quantity * orderitemFirst.UnitPrice ; //NB - 11/11 - I-243413
                orderTotal = orderTotal + orderitemFirst.Sub_Total__c; //NB - 11/11 - I-243413
                if (RequestedDate == null || (RequestedDate > orderitemFirst.Requested_Ship_Date__c)){
                    RequestedDate = orderitemFirst.Requested_Ship_Date__c;
                }
            }
            mapOrder.get(orderFirst.id).Requested_Date__c = RequestedDate; 
            mapOrderItems.put(orderFirst.id, orderTotal);
        }
        system.debug('mapOrder #############' + mapOrder);
        Boolean flag = false;
        for(Id orderFirst : productID.keySet()){
            for(Product2 prod : productID.get(orderFirst)){
                if(prod.Base_or_Capital__c  == 'Base' || prod.Base_or_Capital__c == ''){
                    flag = true;
                }
            }
            system.debug('updatedOrder #############' + mapOrderItems.get(orderFirst));
            if(!flag && mapOrder.containsKey(orderFirst) && mapOrderItems.get(orderFirst) > 25000){
                mapOrder.get(orderFirst).CapitalOrder__c = true;  
                system.debug('check ###########' + mapOrder.get(orderFirst).CapitalOrder__c);
            }
            else{
                mapOrder.get(orderFirst).CapitalOrder__c = false;
            }
            updatedOrder.add(mapOrder.get(orderFirst));
        }
        try{system.debug('updatedOrder #############' + updatedOrder);
            if(!updatedOrder.isEmpty()){
                update updatedOrder;
            }
        }
        catch(Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            }
    }

    private static void updateOrders(List<OrderItem> oItems , Map<Id, OrderItem> oldOItems){
        List<Order> orderToBeUpdate = new List<Order>();
        Set<Id> OrderIds = new Set<Id>();
        Map<Id, OrderItem> orderItemMap = new Map<Id, OrderItem>();
        Map<Id, Messaging.SingleEmailMessage> msgMap = new Map<Id, Messaging.SingleEmailMessage>();
        for(OrderItem oi : oItems){
        system.debug('OrderItem oi'+oi.Order_Line_Status__c);
            if(oldOItems != null ? (oldOItems.get(oi.id).Order_Line_Status__c != oi.Order_Line_Status__c && (oi.Order_Line_Status__c == X531_IN_MANUFACTURING || oi.Order_Line_Status__c == X530_IN_MANUFACTURING || oi.Order_Line_Status__c == X574_IN_MANUFACTURING)) : (oi.Order_Line_Status__c == X531_IN_MANUFACTURING || oi.Order_Line_Status__c == X530_IN_MANUFACTURING || oi.Order_Line_Status__c == X574_IN_MANUFACTURING)){
                OrderIds.add(oi.OrderId);
                if(!orderItemMap.containsKey(oi.OrderId)){
                    orderItemMap.put(oi.orderId, oi);
                }
            }
        }
        system.debug('orderItemMap'+orderItemMap);
        if(!OrderIds.isEmpty()){
            Id emailTemplateId;
            for(EmailTemplate et : [select id from EmailTemplate where DeveloperName='Scheduled_Order_Notification']){
                emailTemplateId = et.id;
            }
            for(Order o : [select id, Status , ShipToContactId, Contact__c, Is_Scheduled_Order_Mail_Sent__c, (Select id, Order_Line_Status__c from OrderItems) from Order where id in : orderIds ]){
                Integer scheduledOIs = 0;
                for(OrderItem oi : o.OrderItems){
                    if(oi.Order_Line_Status__c == X531_IN_MANUFACTURING || oi.Order_Line_Status__c == X530_IN_MANUFACTURING || oi.Order_Line_Status__c == X574_IN_MANUFACTURING){
                        scheduledOIs++;
                    }
                }
                system.debug('o.OrderItems.size()'+o.OrderItems.size());
                system.debug('scheduledOIs'+scheduledOIs);
                if(scheduledOIs != 0 && o.OrderItems.size() != 0 && scheduledOIs == o.OrderItems.size()){
                    //o.status = X531_IN_MANUFACTURING; // I-188988 - commented - we do not require to change the Order status
                    system.debug('scheduledOIsIF');

                    if(!o.Is_Scheduled_Order_Mail_Sent__c && emailTemplateId != null){
                        msgMap.put(o.id, prepareMailMessage(emailTemplateId, o));
                    }
                    o.Is_Scheduled_Order_Mail_Sent__c = true;
                    orderToBeUpdate.add(o);
                }
            }
            if(!orderToBeUpdate.isEmpty()){
            system.debug('orderToBeUpdateIF');
                Set<Id> failureOrderIds = new Set<Id>();
                List<Database.SaveResult> saveResults = Database.update(orderToBeUpdate, false);
                for(Integer i =0; i < saveResults.size(); i++){
                    if(!saveResults[i].isSuccess()){
                          // DML operation failed
                        Database.Error error = saveResults.get(i).getErrors().get(0);
                        failureOrderIds.add(orderToBeUpdate[i].id);
                        orderItemMap.get(orderToBeUpdate[i].id).addError(error.getMessage());
                    }
                }

                if(!failureOrderIds.isEmpty() && !msgMap.isEmpty()){
                system.debug('INSIDEFAILUREIF');
                    for(Id oId : failureOrderIds){
                        msgMap.remove(oId);
                    }
                }
                if(!msgMap.isEmpty()){
                system.debug('INSIDELASTIF');

                  // T-476922 Send Email only when custom setting flag is true.
                  if(Medical_Common_Settings__c.getOrgDefaults().Enable_Send_Email_When_Order_Scheduled__c){
                    Messaging.sendEmail(msgMap.values());
                  }

                }
            }
        }
    }

    private static Messaging.SingleEmailMessage prepareMailMessage(Id templateId, Order o){
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(templateId);
        msg.setWhatId(o.id);
        msg.setBccSender(false);
        //msg.setCcSender(false);
        system.debug('o.Contact__c'+o.Contact__c);
        msg.setTargetObjectId(o.Contact__c);
        msg.setSaveAsActivity(true);
        return msg;
    }
}