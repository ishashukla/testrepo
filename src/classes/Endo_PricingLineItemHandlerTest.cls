/*************************************************************************************************
Created By:    Jitendra Kothari
Date:          March 20, 2014
Description  : Test class for Endo_PricingLineItemHandlerTest
**************************************************************************************************/
@isTest(seeAllData = true)
private class Endo_PricingLineItemHandlerTest {
	static testMethod void testEndo_PricingLineItemTrigger() {
		// Load the Data
		Account acc = Endo_TestUtils.createAccount(1, true).get(0);
		    
	    List<Product2> products = Endo_TestUtils.createProduct(2, true);
	    
	    Pricebook2 pb = Endo_TestUtils.getStandardPricebook();
	    
	    PricebookEntry standardPrice1 = Endo_TestUtils.getTestPricebookEntry(pb.Id, products.get(0).Id);
	    standardPrice1.UnitPrice = 100.0;
	    
	    upsert standardPrice1;
	    PricebookEntry standardPrice2 = Endo_TestUtils.getTestPricebookEntry(pb.Id, products.get(1).Id);
	    
	    
	    Pricing_Contract__c pc = Endo_TestUtils.createPC(1, true).get(0);
	    
	    Pricing_Contract_Account__c pca = Endo_TestUtils.createPricingContractAccount(acc.Id, pc.Id, true);	    
	    List<Pricing_Contract_Line_Item__c> pclis = Endo_TestUtils.createPCLI(4, false);
	    pclis.get(0).Product__c = products.get(0).Id;
	    pclis.get(0).Pricing_Contract__c = pc.Id;
	    pclis.get(0).Application_Method__c = '%';
	    pclis.get(0).Value__c = 20.0;
	    
	    pclis.get(1).Product__c = products.get(1).Id;
	    pclis.get(1).Pricing_Contract__c = pc.Id;
	    pclis.get(1).Application_Method__c = 'NEWPRICE';
	    pclis.get(1).Value__c = 500.0;
	    
	    pclis.get(2).Product__c = products.get(1).Id;
	    pclis.get(2).Pricing_Contract__c = pc.Id;
	    //pclis.get(2).Excluder__c = true;
	    
	    pclis.get(3).Product__c = products.get(1).Id;
	    pclis.get(3).Pricing_Contract__c = pc.Id;
	    pclis.get(3).Application_Method__c = 'XYZ';
	    pclis.get(3).Value__c = 100.0;
	    
	    Test.startTest();
	    insert pclis;
	    Test.stopTest();
	    
	    pclis = [Select Id, Contract_Price__c, Value__c from Pricing_Contract_Line_Item__c Where Id in :pclis];
	    /* tom muse commented out for production push */
	    //System.assertEquals(80.0, pclis.get(0).Contract_Price__c);
	    //System.assertEquals(500.0, pclis.get(1).Contract_Price__c);
	    //System.assertEquals(0.0, pclis.get(2).Contract_Price__c);
	    //System.assertEquals(100.0, pclis.get(3).Contract_Price__c);
	}
}