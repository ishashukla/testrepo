/*******************************************************************
Name  :  QuickLinksController
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 05, 2016

Modified:
*************************************************************************/
public class QuickLinksController {
  
    public Map<String, List<Quick_Link_Section__c>> mapOfSectionAndItsLinks {get; set;}
    public Boolean mapSizeCheck {get; set;}


    public QuickLinksController() {
        mapSizeCheck = false;
        mapOfSectionAndItsLinks = new Map<String, List<Quick_Link_Section__c>> ();

        User userDetails = [SELECT Division 
                            FROM User
                            WHERE Id = :UserInfo.getUserId()];
        
        Set<String> setOfUserDivisions = new Set<String> ();
        
        if(userDetails != null && !String.isBlank(userDetails.Division)) {
            for(String division : userDetails.Division.split(';')) {
                setOfUserDivisions.add(division.trim());
            }
        }
        
        if(!setOfUserDivisions.isEmpty()) {
            for(Quick_Link_Section__c link : [SELECT System_Name__c, URL__c, Section__c
                                            FROM Quick_Link_Section__c
                                            WHERE Section__c IN :setOfUserDivisions
                                            ORDER BY System_Name__c]) {
                if(!mapOfSectionAndItsLinks.containsKey(link.Section__c)) {
                    mapOfSectionAndItsLinks.put(link.Section__c, new List<Quick_Link_Section__c> ());
                }
                mapOfSectionAndItsLinks.get(link.Section__c).add(link);
            }

        }
        if(mapOfSectionAndItsLinks.size() > 0) {
            mapSizeCheck = true;
        }
    }
}