// (c) 2015 Appirio, Inc.
//
// Class Name: DoubleBaggerUtilityTest 
// Description: Test Class for DoubleBaggerUtility class.
// 
// April 11 2016, Isha Shukla  Original 
//
@isTest
Private class DoubleBaggerUtilityTest {
    static Set<Id> userSet;
    //static List<Multi_BU_Assignment__c> mbuList;
    static User user1;
    static User usr;
    static User manager;
    static UserRole role1;
    static UserRole role2;
    static List<User> instUsr;
    @isTest static void testDoubleBaggerUtility() {
        createData();
        Map<Id,String> map1 = DoubleBaggerUtility.getUsersBusinessUnits(userSet);
        String s = usr.Id+'+'+'NSE';
        System.debug('>>>>'+s);
        Map<String,Id> map2 = DoubleBaggerUtility.getManagerforUser(new Set<String>{s});
        Map<String,String> getDirectorOrVPFromManager = DoubleBaggerUtility.getDirectorOrVPFromManager(new Set<Id>{instUsr[1].Id});
        System.assertEquals(True, map1 != Null);
        System.assertEquals(True, map2 != Null);
    }
    @isTest static void testDoubleBaggerUtilityWithRole() {
        createData();
        System.RunAs(user1) {
            Test.startTest();
           // mbuList = createMultiBUassign(1,manager.Id,user1.Id,'NSE',true);
            Map<Id,String> map1 = DoubleBaggerUtility.getUsersBusinessUnits(userSet);
            String s = user1.Id+'+'+'NSE';
            String s1 = usr.Id+'+'+'NSE';
            Map<String,Id> map2 = DoubleBaggerUtility.getManagerforUser(new Set<String>{s});
            Map<Id,Set<Id>> getRepforManagerFromMancode = DoubleBaggerUtility.getRepforManagerFromMancode(new Set<Id>{instUsr[0].Id});
            Map<String,String> getManagerandBUforUser = DoubleBaggerUtility.getManagerandBUforUser(new Set<String>{s1});
            Test.stopTest();
            System.assertEquals(True, map1 != Null);
            System.assertEquals(True, map2 != Null);
        }
        //system.debug('mbuList=='+mbuList);
        
    }
    
    public static void createData() {
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        Profile pfman = [SELECT Id FROM Profile WHERE Name = :Label.Sales_Manager_Profile];
        UserRole parentRole = createParentUserRole('Manager');
        insert parentRole;
        UserRole childRole = createChildUserRole('User');
        childRole.parentRoleId = parentRole.Id;
        insert childRole;
        manager = createUser('Manager1','Manager1','M1',pfman.Id,parentRole.Id);
        insert manager;
        user1 = createUser('testfirst','testlast','testuser',pf.Id,childRole.Id);
        user1.Division='NSE';
        insert user1;
        userSet = new Set<Id>();
        userSet.add(user1.Id);
        Profile pr =  TestUtils.fetchProfile('System Administrator');
        usr = TestUtils.createUser(1, pr.Name, false).get(0);
        usr.Division = 'NSE';
        insert usr;
        Profile pr2 =  TestUtils.fetchProfile('Instruments Sales User');
        instUsr = TestUtils.createUser(3, pr2.Name, false);
        instUsr[0].Division = 'NSE';
        instUsr[1].Division = 'NSE';
        instUsr[2].Division = 'NSE';
        insert instUsr;
        System.runAs(user1) {
        List<Mancode__c> mancodeList1 = TestUtils.createMancode(1, usr.Id, instUsr[0].Id, false);
        mancodeList1[0].Business_Unit__c = 'NSE';
        insert mancodeList1;
        List<Mancode__c> mancodeList2 = TestUtils.createMancode(1, instUsr[0].Id, instUsr[1].Id, false);
        mancodeList2[0].Business_Unit__c = 'NSE';
        insert mancodeList2;
        List<Mancode__c> mancodeList3 = TestUtils.createMancode(1, instUsr[1].Id, instUsr[2].Id, false);
        mancodeList3[0].Business_Unit__c = 'NSE';
        insert mancodeList3;
        }
    }
    public static UserRole createParentUserRole(String name) {
        role1 = new UserRole(Name = name);
        return role1;
    }
    public static UserRole createChildUserRole(String name){
        role2 = new UserRole(Name = name);
        return role2;
    }
    public static User createUser(String fName,String lName,String uniqueName,Id profileId,Id roleId){
        User tuser = new User(firstname = fName,
                              lastName = lName,
                              email = uniqueName + '@test' + 'testOrg' + '.org',
                              Username = uniqueName + '@test' + 'testOrg' + '.org',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = (fName+lName).substring(0,8),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = profileId,
                              UserRoleId = roleId);
        return tuser;
    }
  /*  public static List<Multi_BU_Assignment__c> createMultiBUassign(Integer mbuCount,Id managerId,Id repId,String bu,Boolean isInsert) {
        mbuList = new List<Multi_BU_Assignment__c>();
        
        for(Integer i = 0; i < mbuCount; i++) {
            Multi_BU_Assignment__c mbuRecord = new Multi_BU_Assignment__c(Business_Unit__c=bu,Manager__c=managerId,Rep__c=repId);
            mbuList.add(mbuRecord);
        }
        if(isInsert) {
            insert mbuList;
        } 
        return mbuList;
    } */
}