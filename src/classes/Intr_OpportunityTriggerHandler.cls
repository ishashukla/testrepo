/***********************************************************************************
 Author        :  Appirio JDC (Shreerath Nair)
 Date          :  July 26, 2016
 Purpose       :  Handler Class to create Scheduled Revenue for Procare Opportunity(T-500173)
************************************************************************************/
public  class Intr_OpportunityTriggerHandler {
    
    
    
    public static void handleOpportunityForScheduledRevenue(Map<Id, Opportunity> oldMap,List<Opportunity> newList){
    	
    	//get the record type Id for Instruments Opportunity
    	Map<String,Schema.RecordTypeInfo> mapOpportunityInfo = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
    	SET<ID> InstrumentRecordTypeIds = new Set<ID>{mapOpportunityInfo.get(Intr_Constants.INSTRUMENTS_Opportunity_RECORD_TYPE).getRecordTypeId(),
                                                      mapOpportunityInfo.get(Intr_Constants.INSTRUMENTS_OPPORTUNITY_RECORD_TYPE_CLOSED_WON).getRecordTypeId()};
    	Map<ID,Opportunity> opportunityToConsiderMap = new Map<ID,Opportunity>();
    	
    	//Loop through all Opportunity
        for(Opportunity opp : newList){
        	//Check if any following fields are changed
            if((opp.StageName == 'Closed Won'
               && opp.Contract_Start_Date__c !=null
               && (opp.IVS_Value__c>0 || 
                   opp.Navigation_Value__c>0 || 
                   opp.Neptune_Value__c>0 || 
                   opp.NSE_Value__c>0 || 
                   opp.Surgical_Value__c>0 || 
                   opp.Infection_Control_Value__c>0 || 
                   opp.Software_Value__c>0)
               && opp.Term__c>0 &&
               InstrumentRecordTypeIds.contains(opp.recordTypeId))  
                                  && ((oldMap.get(opp.Id).IVS_Value__c != opp.IVS_Value__c)
                                  || (oldMap.get(opp.Id).Navigation_Value__c  != opp.Navigation_Value__c)
                                  || (oldMap.get(opp.Id).Neptune_Value__c  != opp.Neptune_Value__c)
                                  || (oldMap.get(opp.Id).NSE_Value__c  != opp.NSE_Value__c)
                                  || (oldMap.get(opp.Id).Surgical_Value__c  != opp.Surgical_Value__c)
                                  || (oldMap.get(opp.Id).Infection_Control_Value__c  != opp.Infection_Control_Value__c)
                                  || (oldMap.get(opp.Id).Software_Value__c  != opp.Software_Value__c)
                                  || (oldMap.get(opp.Id).Term__c  != opp.Term__c)
                                  || (oldMap.get(opp.Id).Contract_Start_Date__c  != opp.Contract_Start_Date__c)
                                  || (oldMap.get(opp.Id).StageName != opp.StageName))){                   	            
              	//add the opportunity into map
                opportunityToConsiderMap.put(opp.Id,opp);
             
            }
       }
       
       
       List<Scheduled_Revenue__c> ScheduledRevenueList = new List<Scheduled_Revenue__c>();
     
       
       //delete all previous existing scheduled revenue 
       if(opportunityToConsiderMap.size()>0){
       	delete [SELECT Id FROM Scheduled_Revenue__c WHERE Opportunity__c IN : opportunityToConsiderMap.KeySet()];
       }


       
       // Loop through all Opportunity added in Map to create new Scheduled Revenue
       for(Opportunity opportunity : opportunityToConsiderMap.Values()){

          Map<String,Double> businessUnitsToCalculate = new Map<String,Double>();
          If(opportunity.IVS_Value__c > 0) businessUnitsToCalculate.put('IVS',opportunity.IVS_Value__c);
          If(opportunity.Navigation_Value__c > 0) businessUnitsToCalculate.put('Navigation',opportunity.Navigation_Value__c);
          If(opportunity.Neptune_Value__c > 0) businessUnitsToCalculate.put('Neptune',opportunity.Neptune_Value__c);
          If(opportunity.NSE_Value__c > 0) businessUnitsToCalculate.put('NSE',opportunity.NSE_Value__c);
          If(opportunity.Surgical_Value__c > 0) businessUnitsToCalculate.put('Surgical',opportunity.Surgical_Value__c);
          If(opportunity.Infection_Control_Value__c > 0) businessUnitsToCalculate.put('Infection Control',opportunity.Infection_Control_Value__c);
          If(opportunity.Software_Value__c > 0) businessUnitsToCalculate.put('Software',opportunity.Software_Value__c);
       	  
          for(String businessUnit : businessUnitsToCalculate.KeySet()){
            Double buValue = businessUnitsToCalculate.get(businessUnit);
            Double Amount = buValue/opportunity.Term__c;
            //Create new Scheduled Revenue based on the Term(in Months)
            for(Integer i =0;i<opportunity.Term__c;i++){
              Scheduled_Revenue__c scheduleRev = new Scheduled_Revenue__c();
              scheduleRev.Business_Unit__c = businessUnit; 
              scheduleRev.Amount__c = Amount;
              scheduleRev.Opportunity__c = opportunity.Id;            
     
              //Set next Scheduled Revenue Payment Date by adding one month in Contract Signed Date evertime 
              scheduleRev.Payment_Date__c = opportunity.Contract_Start_Date__c.addMonths(i);

              ScheduledRevenueList.add(scheduleRev);
            }
          }
       }
       
       //insert newly created Scheduled Revenues of Opportunities.
       if(ScheduledRevenueList.size()>0){
       	insert ScheduledRevenueList;
       }
             
       
    }  
}