// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_CreateStdOrderForPartsOrderQueue -  Creates Standard Order for every new PartsOrder.
//
// 31 Oct 2016, Mohan Panneerselvam (T-550285) 
public class COMM_CreateStdOrderForPartsOrderQueue implements Queueable{
    SVMXC__RMA_Shipment_Order__c partsOrderObj = new SVMXC__RMA_Shipment_Order__c();
    public COMM_CreateStdOrderForPartsOrderQueue(SVMXC__RMA_Shipment_Order__c inPartsOrderObj) {
        this.partsOrderObj = inPartsOrderObj;  
    }
    public void execute(QueueableContext context) {
        Pricebook2 pb = [select id from Pricebook2 where name = 'COMM Price Book'];
        Order varorder = new Order();
        Set<id> productid= new set<id>();
        OrderItem varorderItems = new OrderItem();
        List<OrderItem> lstOrderItems = new List<OrderItem>();            
        SVMXC__RMA_Shipment_Order__c objRMAHeader = [select SVMXC__Case__r.Accountid,SVMXC__Company__c,SVMXC__Destination_Location__r.Location_Id__c,
                                                                (Select id,name,SVMXC__Product__c,SVMXC__Expected_Quantity2__c,SVMXC__Line_Price2__c 
                                                                 from SVMXC__RMA_Shipment_Line__r) from  SVMXC__RMA_Shipment_Order__c where id =:partsOrderObj.Id];
        
        
        Address__c billToAddObj = [select id,account__c,type__C,Location_ID__c from address__c where Business_Unit_Name__c ='COMM'
                         and Primary_Address_Flag__c =true and Type__c = 'Billing' 
                         and account__c =: objRMAHeader.SVMXC__Company__c limit 1];
        Address__c shipToAddObj = [select id,account__c,type__C,Location_ID__c from address__c where Business_Unit_Name__c ='COMM'
                         and Type__c = 'Shipping' and Location_Id__c =: objRMAHeader.SVMXC__Destination_Location__r.Location_Id__c limit 1];
        /*if(objRMAHeader.SVMXC__Case__r.Accountid != null){
            varorder.Accountid = objRMAHeader.SVMXC__Case__r.Accountid;
        }else{
                    
        }*/
        varorder.Accountid = objRMAHeader.SVMXC__Company__c;
        varorder.EffectiveDate = system.today();
        varorder.Status= 'Entered';
        varOrder.Pricebook2id = pb.id;
        varOrder.Ship_To_Address__c = shipToAddObj.id;
        varOrder.Bill_To_Address__c = billToAddObj.id;  
        varOrder.recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        for(SVMXC__RMA_Shipment_Line__c shipmentLine : objRMAHeader.svmxc__RMA_Shipment_Line__r){
            productid.add(shipmentLine.SVMXC__Product__c);
        }
        Map<Id,PricebookEntry> pbeMap = new Map<Id,pricebookentry>();
        for(PricebookEntry varloop : [select id,product2id,unitprice from PricebookEntry where product2id in :productid and pricebook2id = : pb.id] ){
            pbeMap.put(varloop.product2id, varloop);
        }
        COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA=true;
        insert varorder;
        integer i=1;
        for(SVMXC__RMA_Shipment_Line__c shipmentLine : objRMAHeader.svmxc__RMA_Shipment_Line__r){
            varorderitems = new Orderitem();
            varorderitems.orderid = varorder.id;
            varorderitems.pricebookentryid = pbeMap.get(shipmentLine.svmxc__product__c).id;
            varorderItems.Quantity   = shipmentLine.SVMXC__Expected_Quantity2__c;
            //varorderItems.UnitPrice    = shipmentLine.SVMXC__Line_Price2__c;
            varorderItems.UnitPrice  = 0;
            varorderItems.Description = shipmentLine.id;
            varorderItems.Status__c = 'Entered';
            varorderItems.Requested_Ship_Date__c = Date.today();
            varorderItems.Oracle_Order_Line_Number__c = ''+i;
            lstOrderItems.add(varorderItems);
            i++;
        }                    
        insert lstOrderItems;
            List <Id>orderLineId = new List<Id>();
            for(OrderItem tempOrdLine:lstOrderItems){
                orderLineId.add(tempOrdLine.id);
            }
        if( !Test.isRunningTest() ) {
           System.enqueueJob(new COMM_SyncPartsOrderQueue(partsOrderObj,varorder.id,orderLineId,'Create',null));  
        }      
    }
}