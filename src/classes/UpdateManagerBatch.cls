// (c) 2016 Appirio, Inc.
//
// Class Name: UpdateManagerBatch
// Description: Batch Class to update forecast records and opportunity records for changes in managers 
//
// Isha Shukla  June 3,2016  Original (T-508003)
// Pratz Joshi  June 18,2016 Modified (S-420435) - Update the SOQL Query to incorporate the new 
//              Instruments Closed Won Opportunity Record Type in the WHERE Clause
// Isha Shukla  June 27,2016 Modified (I-224137) - Changed logic from OR to AND in the line 47
global with sharing class UpdateManagerBatch implements Database.Batchable<sObject>,Database.Stateful {
    Id oldId;
    Id newId;
    global boolean hasError = false;
    global String errorMsg = '';
    global EmailTemplate emailTemplate;
    global UpdateManagerBatch( Id oldManager, Id newManager ) {
        oldId = oldManager;
        newId = newManager;
    }
    // Query records which requires new manager update
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Manager__c FROM Mancode__c WHERE Manager__c = :oldId AND Sales_Rep__c != Null AND Business_Unit__c != Null';
        return Database.getQueryLocator(query);
    }
    // Updates records from old manager to new manager 
    global void execute(Database.BatchableContext bc, List<Mancode__c> records) {
        List<Mancode__c> mancodeListToUpdate = new List<Mancode__c>();
        for(Mancode__c mancodeList : records) {
            mancodeList.Manager__c = newId;
            mancodeListToUpdate.add(mancodeList);
        }
        try{
            update mancodeListToUpdate;
        }
        catch(Exception e){
            hasError = true;
            errorMsg = e.getMessage();
        }
    }
    // Updates Instruments Opportunity Manager field 
    global void finish(Database.BatchableContext bc) {
        if(!hasError){
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        
            //Pratz Joshi Jun 18th, 2016 Story # - S-420435
            Id rt_OppInstrumentClosedWon_Id = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Closed Won Opportunity').getRecordTypeId();

            Set<String> yearSet = new Set<String>();
            for( Forecast_Year__c forecastyr : [SELECT Id,Year__c,User2__c,ownerId FROM Forecast_Year__c WHERE User2__c = :oldId] ) {
                yearSet.add(forecastyr.Year__c);
            }
            
             List<Forecast_Year__c> deleteForecast = new List<Forecast_Year__c>();
             for( Forecast_Year__c forecastyr : [SELECT Id,Year__c,User2__c,ownerId FROM Forecast_Year__c WHERE User2__c = :newId AND Year__c IN:yearSet] ) {
                    deleteForecast.add(forecastyr);
                }
            if(deleteForecast.size() > 0) {
                delete deleteForecast;
            }
            List<Opportunity> oppListToUpdate = new List<Opportunity>();
        
            //Pratz Joshi Jun 18th, 2016 Story # S-420435
            //Update the SOQL Query to incorporate the new 
            //Instruments Closed Won Opportunity Record Type in the WHERE Clause
            List<Opportunity> oppList = new List<Opportunity>([SELECT BU_Manager__c 
                                                               FROM Opportunity 
                                                               WHERE (StageName != 'Closed Won' 
                                                               AND StageName != 'Closed Lost') // Modified (I-224137)
                                                               AND (BU_Manager__c = :oldId) 
                                                               AND (RecordTypeId = :recordTypeInstrument
                                                               OR RecordTypeId = :rt_OppInstrumentClosedWon_Id)]);
        for(Opportunity opp : oppList) {
            opp.BU_Manager__c = newId;
            oppListToUpdate.add(opp);
        }
        update oppListToUpdate;
        // When a manager is changed for a user, Current Years forecast as well as future forecasts should be deleted of old manager.
        List<Forecast_Year__c> forecastYearListToUpdate = new List<Forecast_Year__c>();
        for( Forecast_Year__c forecastyr : [SELECT Id,Year__c,User2__c,ownerId FROM Forecast_Year__c WHERE User2__c = :oldId] ) {
            forecastyr.ownerId = newId;
            forecastyr.User2__c = newId;
            forecastYearListToUpdate.add(forecastyr);
        }
        if(forecastYearListToUpdate.size() > 0) {
            update forecastYearListToUpdate;
        }
    }
    List<Messaging.SingleEmailMessage> actualMails = new List<Messaging.SingleEmailMessage>();
    String htmlValue;
    htmlValue = 'Hi,<br/><br/>';
    if(hasError){
        String subject = 'Manager Update Failed.';
        htmlValue = htmlValue + Label.BatchUpdateFailure;
        htmlValue = htmlValue + errorMsg;
        actualMails.add(sendEmail(htmlValue, UserInfo.getUserId(),subject));
    }
    else{
        String subject = 'Manager Update Successful.';
        htmlValue = htmlValue + Label.BatchUpdateSuccess;
        actualMails.add(sendEmail(htmlValue, UserInfo.getUserId(),subject));
    }
    try{
        Messaging.sendEmail(actualMails); 
    }
    catch(Exception e){
        System.debug(e);
    }
    
    }
    private Messaging.SingleEmailMessage sendEmail(String htmlValue, String managerId,String subject){
    emailTemplate = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE DeveloperName = 'Monthly_Forecast_Reminder_Manager']; 
    String htmlBody = emailTemplate.HtmlValue;
    htmlBody = htmlBody.replaceAll('<!\\[CDATA\\[', ''); // replace '<![CDATA['
    htmlBody= htmlBody.replaceAll('\\]\\]>', ''); // replace ']]'
    Messaging.SingleEmailMessage actualMail = new Messaging.SingleEmailMessage();
    User activeUser = [Select Email From User where Id =:UserInfo.getUserId()];
    actualMail.setSubject(subject);
    actualMail.setHtmlBody(htmlBody.replace('[UnsubmittedForecast]', htmlValue));
    actualMail.setToAddresses(new List<String>{activeUser.Email});
   // actualMail.setToAddresses(new List<String>{'sbatra@appirio.com'});
    return actualMail;
  }
}