/*************************************************************************************************
Created By:    Rahul Aeran
Date:          April 23, 2016
Description:   Mock class for mocking Webservice callouts in Test classes
**************************************************************************************************/
public class WebServiceMockClass implements WebServiceMock  {     
  // Implement this interface method
  public void doInvoke(
    Object stub,
    Object request,
    Map<String, Object> response,
    String endpoint,
    String soapAction,
    String requestName, 
    String responseNS,
    String responseName,
    String responseType) {
    	if(stub instanceof Medical_JDE_OrderHeader.OrderHeader_ICPort){
    		Medical_JDE_OrderHeaderSchema.OrderHeader objMedicalOrderHeader = new Medical_JDE_OrderHeaderSchema.OrderHeader();
    		objMedicalOrderHeader.JDEOrderNumber = 'JDE';
    		response.put('response_x',objMedicalOrderHeader );
    	}else if(stub instanceof Medical_TW_Complaint.ComplaintInfoICPort){
    		response.put('response_x', new Medical_TW_ComplaintInfo.CreateComplaintResponse_element());
    	}else{
    		System.debug('Instance of:'+stub);
    	}
     
    } 
  }