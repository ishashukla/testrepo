/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
Name                    - AccountTriggerHandlerTest
Updated By              - Nitish Bansal
Updated Date            - 02/26/2016
Purpose                 - T-459647
*/ 
/*
Name                    - AccountTriggerHandlerTest
Updated By              - Kanika Mathur
Updated Date            - 08/04/2016
Purpose                 - Updated method SPS_AccountTestMethod1() to include recordtype for fixing assert failure
*/ 
/*
Name                    - AccountTriggerHandlerTest
Updated By              - Kanika Mathur
Updated Date            - 08/23/2016
Purpose                 - Updated method AccountTestMethodCOMM() to use Endo COMM Customer RT instead of US Accounts
*/
 
@isTest
private class AccountTriggerHandlerTest {
  static testMethod void AccountMergeTestMethod(){
    // create User for finance profile
    List<User> users = TestUtils.createUser(1,'Finance - COMM US',false);
    if(users.size() > 0){
      users[0].Email = 'rahul10cs@gmail.com';
      insert users;
      System.runAs(users[0]){
      List<Account> accs = TestUtils.createAccount(2,false);
        accs[0].Country_Code__c = 'India';
        accs[0].CurrencyIsoCode = 'USD';
        accs[0].AccountNumber = '1234';
        accs[0].OwnerId = users[0].id;
        accs[0].COMM_Shipping_Address__c = 'Test Ship Address';
        accs[0].COMM_Shipping_City__c  = 'Test City';
        accs[0].COMM_Shipping_State_Province__c = 'California';
        accs[0].COMM_Shipping_Country__c = 'United States';
        accs[0].COMM_Shipping_PostalCode__c = '12';
        accs[0].RecordTypeId = getRecordTypeId('COMM Corporate Accounts','Account');
        accs[1].Country_Code__c = 'India';
        accs[1].CurrencyIsoCode = 'USD';
        accs[1].AccountNumber = '1234';
        accs[1].OwnerId = users[0].id;
        accs[1].COMM_Shipping_Address__c = 'Test Ship Address';
        accs[1].COMM_Shipping_City__c  = 'Test City';
        accs[1].COMM_Shipping_State_Province__c = 'California';
        accs[1].COMM_Shipping_Country__c = 'United States';
        accs[1].COMM_Shipping_PostalCode__c = '12';
        accs[1].RecordTypeId = getRecordTypeId('COMM Corporate Accounts','Account');
       // accs[1].Type = 'Competitor';
        insert accs;
        merge accs[0] accs[1];
      }
    }
  }
      //To check that Strategic account and Negotiated Contract checkbox is populated properly
      //Modified by Kanika Mathur to include record type when creating account to fix test class failure
  static testMethod void SPS_AccountTestMethod1() {
        Test.startTest();

        String profileName = [SELECT Id, Name FROM Profile WHERE Id =:userinfo.getProfileId()].Name;
        System.debug('<--- profileName --->' + profileName);
        Id SPSrecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('SPS Customer').getRecordTypeId();
        Account parentAccount = new Account(Name = 'Parent Test Account',
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States', recordtypeid = SPSrecordTypeId);
        parentAccount.Strategic_Account__c = true;
        insert parentAccount;
        
        Account childAccount = new Account(Name = 'Child Test Account 01',
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States', recordtypeid = SPSrecordTypeId);
        insert childAccount;
        
        childAccount.ParentId = parentAccount.Id;
        update childAccount;
        
        parentAccount.Negotiated_Contract__c = true;
        update parentAccount;
        
        Test.stopTest();
        
        Account testAccount = [SELECT Id, ParentId, Strategic_Account__c, Negotiated_Contract__c,  Name FROM Account WHERE Id = :childAccount.Id];
                                
        System.assertEquals(testAccount.Strategic_Account__c, true);
        System.assertEquals(testAccount.Negotiated_Contract__c, true);

    }
    //modified by Kanika Mathur to fix test failure
    //NB - 11/17 - I-242624 Start
    static testMethod void AccountTestMethodCOMM() {
        Test.startTest();

        String profileName = [SELECT Id, Name FROM Profile WHERE Id =:userinfo.getProfileId()].Name;
        List<User> u  = TestUtils.createUser(1, Label.COMM_System_Administrator, true);
        List<User> users = TestUtils.createUser(1,'COMM System Administrator',true);
        List<Account> acc = TestUtils.createAccount(2, false);
        acc[0].RecordTypeId = getRecordTypeId('COMM Corporate Accounts','Account');
        acc[1].RecordTypeId = getRecordTypeId('COMM Corporate Accounts','Account');
        acc[1].OwnerId = u[0].Id;
        acc[0].OwnerId = users[0].Id;
        //acc[0].COMM_Sales_Rep__c = users[0].Id;
        acc[1].Level__c = Constants.FACILITY_LEVEL;
        acc[1].Hold_Status__c = 'Credit Hold';
        acc[1].Name = Constants.FACILITY_LEVEL + 'Nick';
        insert acc;

        List<Project_Plan__c> projects =  TestUtils.createProjectPlan(2, acc[0].Id, false);
        projects[0].Account__c = acc[1].Id;
        insert projects;
        /*SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c ();
        workOrder.SVMXC__Company__c = acc[0].Id;
        workOrder.SVMXC__Order_Status__c = 'Open';
        workOrder.SVMXC__Member_Email__c = 'asdad@adad.in';
        insert workOrder;*/
        Account updateAcc = new Account();
        updateAcc.Id = [Select Id from Account where Id =:acc[1].Id].Id;
        updateAcc.COMM_Shipping_Address__c = 'nick test address';
        updateAcc.COMM_Shipping_City__c = 'nick test City';
        updateAcc.COMM_Shipping_State_Province__c = 'Okahama';
        updateAcc.COMM_Shipping_PostalCode__c = '24401';
        updateAcc.COMM_Shipping_Country__c = 'United States';
        updateAcc.ParentId = acc[0].Id;
        updateAcc.COMM_Sales_Rep__c = users[0].Id;
        updateAcc.Hold_Status__c = '';
        update updateAcc;
        System.assertEquals([SELECT Id FROM AccountTeamMember WHERE UserId =: u[0].Id AND AccountId =: acc[0].Id].size(), 1);

        BlockedAccountDeletionProfiles__c csInstance = new BlockedAccountDeletionProfiles__c();
        csInstance.Name = 'COMM System Administrator';
        insert csInstance;

        system.runAs(users.get(0)){
            acc = TestUtils.createAccount(2, false);
            acc[0].RecordTypeId = getRecordTypeId('COMM Corporate Accounts','Account');
            acc[0].Name = Constants.FACILITY_LEVEL + 'test';
            acc[1].RecordTypeId = getRecordTypeId('COMM Corporate Accounts','Account');
            acc[1].Name = Constants.FACILITY_LEVEL + 'Duplciate';
            //acc[1].OwnerId = u[0].Id;
            insert acc;
            delete acc;
        }
        Test.stopTest(); 
        users = TestUtils.createUser(1,Constants.INTEGRATION_PROFILE,true);
        system.runAs(users.get(0)){
            acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = getRecordTypeId('COMM Corporate Accounts','Account');
            acc[0].Name = Constants.FACILITY_LEVEL + 'testing duplicate';
            insert acc;
            update acc;
        }
        
           
    }
    //NB - 11/17 - I-242624  End
    /**
    * This method is used to create the recordtype id
    */
   public static Id getRecordTypeId(String recordType, String objectType) {
        Map<String,Schema.RecordTypeInfo> rtMapByName = null;
        if(rtMapByName == null) {
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
      Schema.SObjectType ctype = gd.get(objectType);
        Schema.DescribeSObjectResult d1 = ctype.getDescribe();
        rtMapByName = d1.getRecordTypeInfosByName();
      }
      Schema.RecordTypeInfo recordTypeDetail = rtMapByName.get(recordType);
      if(recordTypeDetail != null) {
        return recordTypeDetail.getRecordTypeId();
      } else {
        return null;
      }
    } 
}