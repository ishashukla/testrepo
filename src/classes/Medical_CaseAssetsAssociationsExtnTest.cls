/*************************************************************************************************
Created By:    Sunil
Date:          April 26, 2016
Description  : Test class for Medical_CaseAssetsAssociationsExtensions

 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
**************************************************************************************************/
@isTest
private class Medical_CaseAssetsAssociationsExtnTest {

  static testMethod void testAddAssetRow(){
    Account objAccount = Medical_TestUtils.createAccount(1, true).get(0);

    Asset objAsset = Medical_TestUtils.createAsset(1,objAccount.Id,true).get(0);

    Contact objContact = Medical_TestUtils.createContact(1, objAccount.id, true).get(0);

    List<Case> lstCases = Medical_TestUtils.createCase(1,objAccount.Id, objContact.Id,false);
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical - Potential Product Complaint');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Medical - Potential Product Complaint', 'Medical');
	lstCases[0].recordTypeId = rtByName.getRecordTypeId();
	insert lstCases;
	
    Case objCase = lstCases.get(0);

    List<Case_Asset_Junction__c> lstCaseAssets = new List<Case_Asset_Junction__c>();
    Case_Asset_Junction__c objJunction1 = Medical_TestUtils.createCaseAssetJunction(1, objCase.Id, objAsset.Id, true).get(0);
    lstCaseAssets.add(objJunction1);

    System.currentPageReference().getParameters().put('Id', objCase.Id);

    Test.startTest();
      ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(lstCaseAssets);
      ApexPages.currentPage().getParameters().put('Id' , objCase.Id);
      Medical_CaseAssetsAssociationsExtension ext = new Medical_CaseAssetsAssociationsExtension(ssc);
      PageReference pageRef = Page.medical_CaseAssetsAssociations;        
      Test.setCurrentPage(pageRef);
      ext.caseObj = objCase;
      ext.rowIndex = 1;
      ext.getSelectedRecord();
      ext.addRow();
      ext.populateSerialNumber();
      ext.populateAccount();
      ext.addRow();
      ext.saveCaseAndForm();
      ext.removeRow();
      //setting caseAssets as 1
      ext.caseAssets = new List<Case_Asset_Junction__c>();
      ext.caseAssets.add(new Case_Asset_Junction__c());
      ext.rowIndex = 0;
      ext.removeRow();
      ext.csAssetId = objJunction1.Id;
      ext.populateSerialNumber();
      Case_Asset_Junction__c objQueriedJunction = [SELECT Id,Name FROM Case_Asset_Junction__c LIMIT 1];
      ext.csAssetId = null; 
      ext.caSerialNumber  = objQueriedJunction.Name;
      ext.populateSerialNumber();
      
      //setting account as null for the case
      objCase.AccountId = null;
      update objCase;
      ext.caseObj = objCase;
      ext.populateAccount();
      
    Test.stopTest();
    PageReference pg = ext.saveCaseAndForm();
    System.assert(pg != null);



  }
}