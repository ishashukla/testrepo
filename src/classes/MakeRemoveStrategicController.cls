// (c) 2015 Appirio, Inc.
//
// Class Name: MakeRemoveStrategicController
// Description: Contoller Class for MakeRemoveStrategic. Class creates an account a strategic account for logged in user. If account is 
//              already strategic for the logged in user, user is prompted to either remove the strategic or remain as it is.
// June 27 2016, Prakarsh Jain  Original (T-514072)
//
public class MakeRemoveStrategicController {
  public String accId{get;set;}
  public Boolean showPanel{get;set;}
  public Strategic_Account__c stratAcc{get;set;}
  public Map<String,String> mapNameToMsg;
  public Map<String, Messages__c> customSettingMap;
  public List<Messages__c> listCustomSetting;
  public MakeRemoveStrategicController(){
    stratAcc = new Strategic_Account__c();
    showPanel = false;
    mapNameToMsg = new Map<String,String>();
    customSettingMap = new Map<String, Messages__c>();
    listCustomSetting = new List<Messages__c>();
    listCustomSetting = Messages__c.getAll().values();
    accId = ApexPages.currentPage().getParameters().get('accId');
    //customSettingMap = Messages__c.getAll();
    for(Messages__c csData : listCustomSetting){
      customSettingMap.put(csData.Id, csData);
    }
    for(Messages__c csData : customSettingMap.values()){
      mapNameToMsg.put(csData.Name, csData.Message_Text__c);
    }
  }
  
  /*public void insertRemove(){
    Integer strategicAcc = [SELECT COUNT() FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: UserInfo.getUserId()];
    system.debug('strategicAcc>>>'+strategicAcc);
      if(strategicAcc == 1){
      showPanel = true;
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,mapNameToMsg.get('PreviouslyMarkedAsStrategic'));
      ApexPages.addMessage(myMsg);
    }
    else{
      stratAcc.Account__c = accId;
      stratAcc.User__c = UserInfo.getUserId();
      insert stratAcc;
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,mapNameToMsg.get('AccountMadeStrategicSuccess'));
      ApexPages.addMessage(myMsg);
    }
  }*/
  
  public PageReference insertRemove(){
    Integer strategicAcc = [SELECT COUNT() FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: UserInfo.getUserId()];
    system.debug('strategicAcc>>>'+strategicAcc);
      if(strategicAcc == 1){
      /*showPanel = true;
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,mapNameToMsg.get('PreviouslyMarkedAsStrategic'));
      ApexPages.addMessage(myMsg);*/
      PageReference pageRef;
      Strategic_Account__c stratAccdelete = [SELECT Id, Account__c, User__c FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: UserInfo.getUserId()];
      delete stratAccdelete;
      pageRef = new PageReference('/'+accId);
      return pageRef;
    }
    else{
      PageReference pageRef;
      stratAcc.Account__c = accId;
      stratAcc.User__c = UserInfo.getUserId();
      insert stratAcc;
      pageRef = new PageReference('/'+accId);
      return pageRef;
    }
  }
  
  public PageReference removeStrategic(){
    PageReference pageRef;
    Strategic_Account__c stratAccdelete = [SELECT Id, Account__c, User__c FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: UserInfo.getUserId()];
    delete stratAccdelete;
    pageRef = new PageReference('/'+accId);
    return pageRef;
  }
  
  public PageReference refreshPage(){
    PageReference pageRef = new PageReference('/'+accId);
    return pageRef;
  }
}