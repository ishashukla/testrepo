/*******************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 10, 2014 
 Purpose       :  Test Class for Endo_MapWeatherComponent
*******************************************************************************/
@isTest
private class Endo_MapWeatherComponentTest {
	static testmethod void testEndo_MapWeatherComponent(){
	    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
	    acc.ShippingCountryCode = 'US';
	    acc.ShippingState = 'California';
	    insert acc;
	    
	    Endo_Address myaddress = new Endo_Address();
        myaddress.city = acc.ShippingCity;
        myaddress.state = acc.ShippingState;
        myaddress.country = acc.ShippingCountry;
        myaddress.countryCode = acc.ShippingCountryCode;  
        
	    Test.startTest();
	    
	    Endo_MapWeatherComponent comp = new Endo_MapWeatherComponent();
	    comp.myaddress = myaddress; 
	    //Endo_MapWeatherHelper.LatLng lnt = comp.lnt;
	    String currentTime  = comp.currentTime;
	    System.assertNotEquals(null , currentTime);
	    String weatherString = comp.weatherString;
	    System.assertNotEquals(null , weatherString);
	    String address = comp.myaddress.getAddressForMap();
	    //System.assertEquals('California,US' , address);
	    
	    String result = ' {"location" : {"lat" : 39.6034810,"lng" : -119.6822510}}';
	    Endo_MapWeatherHelper.logError(JSON.createParser(result));
	    
	    Endo_MapWeatherHelper.TimeZoneResultWrapper tzrw = new Endo_MapWeatherHelper.TimeZoneResultWrapper();
	    tzrw.dstOffset = 0.0;
	    tzrw.rawOffset = 0.0;
	    tzrw.timeZoneName = 'Pacific Standard Time';
	    tzrw.status = 'OK';
	    
	    HttpRequest req = new HttpRequest();
	    req.setEndpoint('https://maps.googleapis.com/');
		req.setMethod('GET');
	    try{
	    	Endo_MapWeatherHelper.fetchResult(req);
	    }catch(Exception e){}
	    
	    Test.stopTest();
	}
}