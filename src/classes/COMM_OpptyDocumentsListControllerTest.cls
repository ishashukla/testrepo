/**================================================================      
* Appirio, Inc
* Name: COMM_OpptyDocumentsListControllerTest
* Description: Test class of COMM_OpptyDocumentsListController
* Created Date: 30th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 

@isTest
private class COMM_OpptyDocumentsListControllerTest
{
	static List<Opportunity> opptyList = new List<Opportunity>();
	static List<Document__c> docList;
	static void createTestData() {
	    TestUtils.createCommBPConfigConstantSetting();
	    TestUtils.createCommConstantSetting();
	    List<Account> accList = TestUtils.createAccount(1,true);
	    opptyList = TestUtils.createOpportunity(20,accList.get(0).Id,true);
	    docList = TestUtils.createOpportunityDocument(20,accList.get(0).Id,opptyList.get(0).Id,false);
	    List<BigMachines__Configuration_Record__c> bigMachineConfigList = TestUtils.createBigMachineConfig(1,true);
	    List<BigMachines__Quote__c> bigMachineQuoteList = TestUtils.createBigMachinesQuote(1,accList.get(0).Id, opptyList.get(0).Id,false);
	    bigMachineQuoteList.get(0).BigMachines__Site__c = bigMachineConfigList.get(0).Id;
	    insert bigMachineQuoteList;
	    for(Integer i = 0; i < 10; i++){
	    	docList.get(i).Document_Type__c = 'CAD Drawing';	
	    }
	    docList.get(0).Oracle_Quote__c = bigMachineQuoteList.get(0).Id;
	    docList.get(2).Oracle_Quote__c = bigMachineQuoteList.get(0).Id;
	    docList.get(2).Opportunity__c = null;
	    insert docList;
	}

	@isTest
	static void itShouldTestController()
	{
		createTestData();
		Test.startTest();
		    ApexPages.StandardController std = new ApexPages.StandardController(opptyList.get(0));
		    COMM_OpptyDocumentsListController controller = new COMM_OpptyDocumentsListController(std);
		    System.assertEquals(controller.documentList.size()>0,true);
			System.assertEquals(false,controller.hasPrevious);
		    System.assertEquals(true,controller.hasNext);
		    controller.next();
		    System.assertEquals(true,controller.hasPrevious);
		    controller.previous();
		    controller.first();
		    System.assertEquals(true,controller.hasNext);
		    System.assertEquals(false,controller.hasPrevious);
		    controller.last();
		    System.assertEquals(true,controller.hasPrevious);
		    System.assertEquals(false,controller.hasNext);
		    controller.sortList();
		    controller.sortField = 'LastModifiedBy';
		    controller.sortList();
		    controller.sortField = 'Oracle_Quote__c';
		    controller.sortList();
		    controller.selectedId = controller.documentList.get(0).Id;
		    controller.doDelete();
		    System.assertEquals(false,controller.hasPrevious);
		    System.assertEquals(true,controller.hasNext);
		    System.assertEquals(true, controller.documentList.size()>0);
		    System.assertEquals(Constants.CANCELLED,controller.CANCELLED);	
		    System.assertEquals(Constants.CLOSED_WON,controller.CLOSED_WON);	
		    System.assertEquals(Constants.CLOSED_LOST,controller.CLOSED_LOST);		    
		Test.stopTest();    
	}
}