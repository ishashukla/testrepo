@isTest
private class ChatterUploaderCtrlTest {
    @isTest
    static void testUploader() {

        scormanywhere__Course__c Course = new scormanywhere__Course__c();
        Course.scormanywhere__SCORM_Training_Path__c = 'testCourse';
        insert Course;
        
        test.setCurrentPage(Page.ChatterUploader);
        ApexPages.currentPage().getParameters().put('uploadId', 'testetset');

        ChatterUploaderCtrl chup = new ChatterUploaderCtrl();
        String uploadId = chup.uploadId;
        String uploaderUrl = chup.getUploadCourseUrl();
        PageReference p = chup.redirectWithParams();
        system.assertNotEquals(p, null);
    }
}