//
// (c) 2012 Appirio, Inc.
//
//  Test class of  WebserviceAmendOpportunity
//
// 19 Aug 2015     Naresh K Shiwani      Original
//
@isTest
private class OpportunitySendAnEmailControllerTest {
  
  static testmethod void testMethodSendEmail(){
    
    Test.startTest();
    TestUtils.createCommConstantSetting();
    User usr   = TestUtils.createUser(1, null, true).get(0);
    System.runAs(usr){
    
	    Account accnt               = new Account();
	    accnt.Name                  = 'AccntName';
	    accnt.Type                  = 'Hospital';
	    accnt.Legal_Name__c         = 'Test';
	    accnt.Flex_Region__c        = 'test';
	    insert accnt;
	    
        Id flexRTOnlyConventional 	= Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();
	    Opportunity opp             = new Opportunity();
	    opp.StageName               = 'Quoting';
	    opp.AccountId               = accnt.Id;
	    opp.CloseDate               = Date.newinstance(2014,5,29);
	    opp.Name                    = 'test value';
	    opp.StageName               = 'Closed Won';
	    opp.Contract_Sign_Date__c   = System.today();
	    opp.Opportunity_Number__c   = '101';
        opp.RecordTypeId 			= flexRTOnlyConventional;
	    insert opp;
	    
	    Product2 prod               = TestUtils.createProduct(1, true).get(0);
	    
	    PricebookEntry pbe2         = new PricebookEntry(unitprice=1,Product2Id=prod.Id,
	                                       Pricebook2Id=Test.getStandardPricebookId(),
                                         isActive=true,UseStandardPrice = false);
      insert pbe2;
        
        OpportunityLineItem oppItem = new OpportunityLineItem();
        oppItem.Quantity            = 1.00;
        oppItem.OpportunityId       = opp.Id;
        oppItem.TotalPrice          = 100;
        oppItem.Asset_Type__c       = 'Equipment';
        oppItem.PriceBookEntryId    = pbe2.Id;
        insert oppItem;
        
        System.assertNotEquals(oppItem, null);
        OpportunitySendAnEmailController oppSendEmailContObj = new OpportunitySendAnEmailController(new ApexPages.StandardController(opp));
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
      System.currentPageReference().getParameters().put('test', 'test');        
        oppSendEmailContObj.redirectPage();
    }
    Test.stopTest();
  }
}