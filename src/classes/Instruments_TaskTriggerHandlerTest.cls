// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_TaskTriggerHandlerTest 
// Description: Test Class for Instruments_TaskTriggerHandler class.
// 
// March 03 2016, Prakarsh Jain Original 
//
@isTest //(SeeAllData = true)
private class Instruments_TaskTriggerHandlerTest {
    // Testing when Whatid is having opportunity id
    static testMethod void testInstruments_TaskTriggerHandlerOpp(){
        User usr = TestUtils.createUser(1, 'System Administrator', false).get(0);
        usr.Division = 'NSE';
        insert usr;
        System.runAs(usr) {
            Test.startTest();
            TriggerRunSetting__c triggerRunSetting = new TriggerRunSetting__c(Name = 'MarketingActivityTaskTrigger',Trigger_Run__c = True);
            insert triggerRunSetting;
            Account acc = TestUtils.createAccount(1, true).get(0);
            Opportunity opp = TestUtils.createOpportunity(1, acc.Id, false).get(0);
            opp.Business_Unit__c = 'NSE';
            insert opp;
            Task tsk = new Task();
            tsk.Subject = 'testSubject';
            tsk.Status = 'Not Started';
            tsk.Priority = 'Normal';
            tsk.Marketing_Activity__c = 'Other'; 
            tsk.WhatId = opp.Id;
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Marketing Task Record');
            Id recordTypeId = rtByName.getRecordTypeId();
            tsk.Marketing_Activity_Detail__c = 'test';
            tsk.RecordTypeId = recordTypeId;
            insert tsk;
            Test.stopTest();
        }
    }
    // Testing when Whatid is having account id
    static testMethod void testInstruments_TaskTriggerHandlerAcc(){
        Account acc = TestUtils.createAccount(1, true).get(0);
        TriggerRunSetting__c triggerRunSetting = new TriggerRunSetting__c(Name = 'MarketingActivityTaskTrigger',Trigger_Run__c = True);
        insert triggerRunSetting;
        Task tsk = new Task();
        tsk.Subject = 'testSubject';
        tsk.Status = 'Not Started';
        tsk.Priority = 'Normal';
        tsk.Marketing_Activity__c = 'Other';
        tsk.WhatId = acc.Id;
        tsk.Marketing_Activity_Detail__c = 'test detail';
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Marketing Task Record');
        Id recordTypeId = rtByName.getRecordTypeId();
        tsk.RecordTypeId = recordTypeId;
        insert tsk;
    }
    // Testing when Whatid is having account id and whoid is having contact id
    static testMethod void testInstruments_TaskTriggerHandlerCon(){
        Account acc = TestUtils.createAccount(1, true).get(0);
        Contact con = TestUtils.createContact(1, acc.Id, true).get(0);
        TriggerRunSetting__c triggerRunSetting = new TriggerRunSetting__c(Name = 'MarketingActivityTaskTrigger',Trigger_Run__c = True);
        insert triggerRunSetting;
        Task tsk = new Task();
        tsk.Subject = 'testSubject';
        tsk.Status = 'Not Started';
        tsk.Priority = 'Normal';
        tsk.Marketing_Activity__c = 'Other';
        tsk.WhoId = con.Id;
        tsk.WhatId = acc.Id; 
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Marketing Task Record');
        Id recordTypeId = rtByName.getRecordTypeId();
        tsk.RecordTypeId = recordTypeId;
        tsk.Marketing_Activity_Detail__c = 'test detail';
        insert tsk;
    }
    // Testing on task delete
    static testmethod void testDeleteTask() {
        User adminuser = TestUtils.createUser(1, 'System Administrator', true).get(0);
        Profile profile = TestUtils.fetchProfile('Standard User');
        List<User> user = TestUtils.createUser(1, profile.Name, true);
        System.runAs(adminuser) {
        Schema.RecordTypeInfo rtByNameInstrumentMarketing = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Marketing Task Record');
        Id recordTypeMarketingInstrument = rtByNameInstrumentMarketing.getRecordTypeId();
        Task task = new Task(Subject='new task',OwnerId=user[0].Id,RecordTypeId=recordTypeMarketingInstrument);
        insert task;
        Test.startTest();
        try {
            delete task;
        } catch(Exception e) {
            e.setMessage('Deletion Failed');
        }
        Test.stopTest();
        System.assertEquals(true,task != Null);
        }
    }
    // Testing on task edit
    static testmethod void testEditTask() {
        Profile profile = TestUtils.fetchProfile('Standard User');
        List<User> user = TestUtils.createUser(1, profile.Name, true);
        Schema.RecordTypeInfo rtByNameInstrumentMarketing = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Marketing Task Record');
        Id recordTypeMarketingInstrument = rtByNameInstrumentMarketing.getRecordTypeId();
        Task task = new Task(Subject='new task',OwnerId=user[0].Id,RecordTypeId=recordTypeMarketingInstrument);
        task.Subject='new task2';
        Test.startTest();
        try {
            update task;
        } catch(Exception e) {
            e.setMessage('Updation Failed');
        }
        Test.stopTest();
        System.assertEquals(true,task != Null);
    }
    
}