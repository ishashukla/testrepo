/**================================================================      
* Appirio, Inc
* Name: BigMachinesQuoteTriggerHandler 
* Description: Created this class to handle BigMachinesQuoteTrigger (T-426533)
* Created Date: 22 Feb 2016 
* Created By: Meghna Vijay (Appirio)
*
* Date Modified      Modified By            Description of the update
* 4 March 2016      Deepti Maheshwari       Updated for T-459649
* 28 March 2016     Mehul Jain              Updated function afterUpdate() for T-483787
* 3rd Nov 2016      Nitish Bansal          I-241409 - Added Proper null & list checks before SOQL and DML
==================================================================*/
public class BigMachinesQuoteTriggerHandler {
    // method for After Insert Trigger
  public static void afterInsert(list < BigMachines__Quote__c > newQuotes) {
    Set < String > opptyIdSet = new Set < String > ();
    // Used to populate Opportunity Id set 
    for (BigMachines__Quote__c bm: newQuotes) {
      if(bm.BigMachines__Opportunity__c != null)
        opptyIdSet.add(bm.BigMachines__Opportunity__c); 
    }
    recalculate(opptyIdSet);
  }
  // method for After Delete Trigger
  public static void afterDelete(list < BigMachines__Quote__c > oldQuotes) {
    Set < String > opptyId = new Set < String > ();
    // Used to populate Opportunity Id set 
    for (BigMachines__Quote__c bm: oldQuotes) {
      if(bm.BigMachines__Opportunity__c != null)
        opptyId.add(bm.BigMachines__Opportunity__c);
    }
    recalculate(opptyId);
  }
    // method for After Update Trigger
  public static void afterUpdate(list < BigMachines__Quote__c > newQuotes, Map < Id, BigMachines__Quote__c > oldQuoteMap) {
    Set < String > opptyId = new Set < String > ();
    // Used to populate Opportunity Id set
    // To check if current opportunity Id is different from previous one
    for (BigMachines__Quote__c b: newQuotes) {
      if (b.BigMachines__Opportunity__c != oldQuoteMap.get(b.Id).BigMachines__Opportunity__c || 
          b.BigMachines__Is_Primary__c != oldQuoteMap.get(b.Id).BigMachines__Is_Primary__c ) {//NB - 06/27 - I-224056
        if(b.BigMachines__Opportunity__c != null)
          opptyId.add(b.BigMachines__Opportunity__c);
        if(oldQuoteMap.get(b.Id).BigMachines__Opportunity__c != null)  
          opptyId.add(oldQuoteMap.get(b.Id).BigMachines__Opportunity__c);
      }
    }
   // BigMachinesQuoteProductTriggerHandler.createOpptyProducts(newQuotes,oldQuoteMap);  //check if quote is set primary and create oppty products - MJ
    recalculate(opptyId);
  }
    // method for After Undelete Trigger
  public static void afterUndelete(list < BigMachines__Quote__c > newQuotes) {
    // Used to populate Opportunity Id set
    Set < String > opptyId = new Set < String > ();
    for (BigMachines__Quote__c bm: newQuotes) {
      opptyId.add(bm.BigMachines__Opportunity__c);
    }
    recalculate(opptyId);
  }
  
  // 4 March 2016   Deepti Maheshwari  Updated for T-459649
  public static void beforeInsert(List<BigMachines__Quote__c> newQuotes){
    setPrimaryQuote(newQuotes);
  }
  // 4 March 2016   Deepti Maheshwari  Updated for T-459649
  //Migrated From CommSrc and triggers are not bulkified in CommSource so not changing it
  private static void setPrimaryQuote(List<BigMachines__Quote__c> newQuotes){
    ID oppId = newQuotes.get(0).BigMachines__Opportunity__c;
    if (Trigger.size == 1 && oppId != null) {
        BigMachines__Quote__c[] primary = [select Id from BigMachines__Quote__c
                                          where BigMachines__Opportunity__c = :oppId
                                          and BigMachines__Is_Primary__c = true];    
        if (primary.size() == 0) {
            newQuotes.get(0).BigMachines__Is_Primary__c = true; 
        }
    }
  }
  
  // method to calculate Roll-Up Summary Values
  public static void recalculate(Set < String > opptyId) {
    list < Opportunity > opportunityList = new list < Opportunity > ();
    opportunityList = [SELECT Id, Hidden_Discounted_Equipment_Total__c, Hidden_Tax_Total__c,
                                    Hidden_Total_Contract_Price__c, Hidden_Total_Discount__c,
                                    Hidden_Total_Discount_Amount__c, Hidden_Total_Estimated_Freigh__c,
                                    Hidden_Total_GPO_Discount__c, Hidden_Total_List_Price__c,
                                    Hidden_Total_Product_Line_Discounts__c, Hidden_Total_Special_Discounts__c,
                                    Primary_Quote_Number__c, 
                                    (SELECT Name, BigMachines__Is_Primary__c, BM_Discounted_Equipment_Total__c, BM_Tax_Total__c,
                                        BM_Total_Contract_Price__c, BM_Total_Discount_Percent__c,
                                        BM_Total_Discount_Amount__c, BM_Total_Estimated_Freight__c,
                                        BM_Total_GPO_Discount__c, BM_Total_List_Price__c,
                                        BM_Total_Product_Line_Discounts__c, BM_Total_Special_Discounts__c FROM BigMachines__BigMachines_Quotes__r)
                                        FROM Opportunity WHERE Id in : opptyId]; //NB - 06/27 - I-224056 - Added few more fields to query
    
    // Used to initialize all the Roll-Up Summary Fields To zero of the opportunityList
    for(Opportunity opp: opportunityList) {
      opp.Hidden_Discounted_Equipment_Total__c = 0;
      opp.Hidden_Tax_Total__c = 0;
      opp.Hidden_Total_Contract_Price__c = 0;
      opp.Hidden_Total_Discount__c = 0;
      opp.Hidden_Total_Discount_Amount__c = 0;
      opp.Hidden_Total_Estimated_Freigh__c = 0;
      opp.Hidden_Total_GPO_Discount__c = 0;
      opp.Hidden_Total_List_Price__c = 0;
      opp.Hidden_Total_Product_Line_Discounts__c = 0;
      opp.Hidden_Total_Special_Discounts__c = 0;
      //opp.Amount = 0;
        // Used To calculate and store the Sum of all the child of Opportunity to Roll-Up summary fields
      for (BigMachines__Quote__c b: opp.BigMachines__BigMachines_Quotes__r) {
        opp.Hidden_Discounted_Equipment_Total__c += b.BM_Discounted_Equipment_Total__c;
        opp.Hidden_Tax_Total__c += b.BM_Tax_Total__c;
        opp.Hidden_Total_Contract_Price__c += b.BM_Total_Contract_Price__c;
        opp.Hidden_Total_Discount__c += b.BM_Total_Discount_Percent__c;
        opp.Hidden_Total_Discount_Amount__c += b.BM_Total_Discount_Amount__c;
        opp.Hidden_Total_Estimated_Freigh__c += b.BM_Total_Estimated_Freight__c;
        opp.Hidden_Total_GPO_Discount__c += b.BM_Total_GPO_Discount__c;
        opp.Hidden_Total_List_Price__c += b.BM_Total_List_Price__c;
        opp.Hidden_Total_Product_Line_Discounts__c += b.BM_Total_Product_Line_Discounts__c;
        opp.Hidden_Total_Special_Discounts__c += b.BM_Total_Special_Discounts__c;
        
        //NB - 06/27 - I-224056 - Start
        if(b.BigMachines__Is_Primary__c){
            opp.Primary_Quote_Number__c = b.Name;    
        }
        //NB - 06/27 - I-224056 End
      }
    }
    if(opportunityList.size() > 0)
      update opportunityList;
  }
}