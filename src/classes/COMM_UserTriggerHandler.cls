// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_UserTriggerHandler - Trigger Handller for User
//
// 02/26/2016, Shubham Dhupar (I-222184)
// Purpose - Will be Used for Integration.

public class COMM_UserTriggerHandler {
  
//=======================================================================
// Method called on before insert
//=======================================================================
  public static void beforeInsert(List<User> newList) {
    updateCustomAccountTeam(newlist,null);
    updateContactUser(newlist,null);
  }
//=======================================================================
// Method called on before Update
//=======================================================================
  public static void beforeUpdate(List<User> newList,Map<Id, User> oldMap) {
    updateCustomAccountTeam(newlist,oldMap);
    updateContactUser(newlist,oldMap);
  }
  
  //============================================================================     
 // Name         : updateContactUser
 // Description  : updates Contact's Sales_Support_Agent__c when Resource_ID__c 
 //                of contact matches with Sales_Rep_Id__c of user
 // Created Date : 15th May 2016 
 // Created By   : Shubham Dhupar (Appirio)
 
 // Task         : I-222184
 //=============================================================================
  public static void updateContactUser(List<User> newList,Map<Id, User> oldMap) {
    Boolean isUpdate = oldMap != null ? true : false;
    Map<String,Id> userIDMap = new Map<String, Id>();
    Map<String,Id> removeIDMap = new Map<String, Id>();
    for(User us : newList) {
      if((!isUpdate|| oldMap.get(us.id).Sales_Rep_ID__c != us.Sales_Rep_ID__c)
        && us.Sales_Rep_ID__c != NULL ) {
        //system.debug('value is ++++++++++++++++++++++++'+ oldMap);
        userIDMap.put(us.Sales_Rep_ID__c,us.Id);
      }
      if(isUpdate) {
        if(oldMap.get(us.id).Sales_Rep_ID__c != us.Sales_Rep_ID__c && oldMap.get(us.id).Sales_Rep_ID__c != NULL) {
          //system.debug('value is ******************* '+ oldMap.get(us.id).Sales_Rep_ID__c);
          removeIDMap.put(oldMap.get(us.id).Sales_Rep_ID__c,us.Id);
        }
      }
    }
    
    List<Contact> contactList = new List<Contact>();
    
    if(userIDMap != NULL) {
      for(Contact con: [Select id,Resource_ID__c,Sales_Support_Agent__c
                         FROM Contact
                         WHERE Resource_ID__c in : userIDMap.keyset() ]) {               
        if(con != NULL) {
          con.Sales_Support_Agent__c = userIDMap.get(con.Resource_ID__c);
          contactList.add(con);
        }                  
      }
      update contactList;
    }
  
    List<Contact> contactRemoveList = new List<Contact>();
    
    if(removeIDMap != NULL) {
      for(Contact con: [Select id,Resource_ID__c,Sales_Support_Agent__c
                         FROM Contact
                         WHERE Resource_ID__c in : removeIDMap.keyset() ]) {               
        if(con != NULL) {
          //system.debug('cat value ##########' + con);
          con.Sales_Support_Agent__c = NULL;
          
          contactRemoveList.add(con);
        }                  
      }
      update contactRemoveList;
    }
  }

  //======================================================================================      
 // Name         : updateCustomAccountTeam
 // Description  : updates Custom Account Team's Sales_Support_Agent__c when ResourceID__c
  //                of Custom Account Team matches with Sales_Rep_Id__c of user
 // Created Date : 15th May 2016 
 // Created By   : Shubham Dhupar (Appirio)
 
 // Task         : I-222184
 //========================================================================================
  
  public static void updateCustomAccountTeam(List<User> newList,Map<Id, User> oldMap) {
    Boolean isUpdate = oldMap != null ? true : false;
    Map<String,Id> userIDMap = new Map<String, Id>();
    Map<String,Id> removeIDMap = new Map<String, Id>();
    for(User us : newList) {
      if((!isUpdate|| oldMap.get(us.id).Sales_Rep_ID__c != us.Sales_Rep_ID__c)
        && us.Sales_Rep_ID__c != NULL ) {
        userIDMap.put(us.Sales_Rep_ID__c,us.Id);
      }
      else if(isUpdate && oldMap.get(us.id).Sales_Rep_ID__c != us.Sales_Rep_ID__c && oldMap.get(us.id).Sales_Rep_ID__c != NULL) {
        //system.debug('value is ******************* '+ oldMap.get(us.id).Sales_Rep_ID__c);
        removeIDMap.put(oldMap.get(us.id).Sales_Rep_ID__c,us.Id);
      }
    }
    
    List<Custom_Account_Team__c> customAccountTeamList = new List<Custom_Account_Team__c>();
    
    if(userIDMap != NULL) {
      for(Custom_Account_Team__c cat: [Select id,ResourceID__c,User__c
                                         FROM Custom_Account_Team__c
                                         WHERE ResourceID__c in : userIDMap.keyset() ]) {               
        if(cat != NULL) {
          cat.User__c = userIDMap.get(cat.ResourceID__c);
          customAccountTeamList.add(cat);
        }                  
      }
      update customAccountTeamList;
    }
  
    List<Custom_Account_Team__c> customAccountTeamRemoveList = new List<Custom_Account_Team__c>();
    
    if(removeIDMap != NULL) {
      for(Custom_Account_Team__c cat: [Select id,ResourceID__c,User__c
                                         FROM Custom_Account_Team__c
                                         WHERE ResourceID__c in : removeIDMap.keyset() ]) {               
        if(cat != NULL) {
          //system.debug('cat value ##########' + cat);
          cat.User__c = NULL;
          
          customAccountTeamRemoveList.add(cat);
        }                  
      }
      update customAccountTeamRemoveList;
    }
  }
 // SD 11/29: No reference found in Trigger or Handler.  
/*
  public static void syncEmailOnCases(Map<Id, User> newMap, Map<Id, User> oldMap) {
    List<User> filteredUsers = new List<User>();
    for(User u : newMap.values()) {
      if(u.Email != oldMap.get(u.Id).Email || u.FirstName != oldMap.get(u.Id).FirstName || u.LastName != oldMap.get(u.Id).LastName) {
       //system.debug('in handler'+u);
        filteredUsers.add(u);
      }
    }
    
    if(filteredUsers.size() > 0) {
      Map<Id, List<Case>> userCaseMap = new Map<Id, List<Case>>();
      List<Case> updateableCases = new List<Case>();
      for(Case c: [Select Id, CreatedById, Creator_Email__c, Owner_First_Name__c, Owner_Last_Name__c, CreatedDate
                   from Case
                   where CreatedById IN: filteredUsers]) {
                  
        if(!userCaseMap.containsKey(c.CreatedById)) {
          userCaseMap.put(c.CreatedById, new List<Case>());
        }
        userCaseMap.get(c.CreatedById).add(c);
      }
      
      if(userCaseMap.size() > 0) {
        for(Id userId : userCaseMap.keySet()) {
          for(Case c : userCaseMap.get(userId)) {
             c.Creator_Email__c = newMap.get(userId).Email;
             if(newMap.get(userId).FirstName != null){
                 c.Owner_First_Name__c= newMap.get(userId).FirstName ;
             }    
             c.Owner_Last_Name__c= newMap.get(userId).LastName;
            updateableCases.add(c);
        
           }
       }
        update updateableCases;
      }
    }
  }  */
}