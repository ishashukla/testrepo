public with sharing class TaskIntermediateController{
    public Task newTask{get;set;}
    public String parentId{get;set;}
    public String selectedAccId {get;set;}
    public String selectedRecordName{get;set;}
    private Map<String, String> urlParameters;
    private String accId;
    
    public TaskIntermediateController(){
        parentId = ApexPages.currentPage().getParameters().get('who_id');
        accId = ApexPages.currentPage().getParameters().get('AccId');
        newTask = new Task();
        if(String.isBlank(parentId) == false){
          newTask.whoId = parentId;
        }
        urlParameters = ApexPages.currentPage().getParameters();
    }
    
    public PageReference redirectPage(){
      if(String.isBlank(accId) == false){
        newTask.whatId = accId;
      }
      else if(!String.isBlank(parentId)){
        List<Contact_Acct_Association__c> contactAcctAssociations = [select id ,Associated_Account__c from Contact_Acct_Association__c where Associated_Contact__c = : parentId limit 2];
        if(contactAcctAssociations.size() > 1){
            return null;
        }else if(contactAcctAssociations.size() == 1){
            newTask.whatId = contactAcctAssociations.get(0).Associated_Account__c;
            return next();
        }
      }
      return next();
      }
      
      
    public PageReference getSelectedRecord(){
        if(selectedAccId != null && selectedAccId != ''){                 
            string currentRecordId = this.selectedAccId;       
            if(currentRecordId <> null){
                List<Sobject> sobjectList = Database.query('SELECT Id, Name  FROM Account WHERE ID = : selectedAccId');
                
                if(!sobjectList.isEmpty()){
                    newTask.whatID = sobjectList[0].id;
                    selectedRecordName = (String) sobjectList[0].get('Name');
                }
            }
        }
        return null;
    }  
    public PageReference next(){
        String url = '/00T/e?isdtp=vw';
        
        if(String.isBlank(parentID) == false){
          url += '&who_id=' + parentID;
        }
        
        
        for(String param : urlParameters.keySet()){
            url += '&' + param + '=' + urlParameters.get(param);
        }
        
        if(newTask.whatId != null){
           url += '&what_id=' + newTask.whatID;
        }
        return new PageReference(url);
    }
    
    public PageReference cancel(){
        return new PageReference('/'+ParentId);
    }
    
}