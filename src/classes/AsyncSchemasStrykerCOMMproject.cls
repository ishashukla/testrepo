/**================================================================      
* Appirio, Inc
* Name: AsyncSchemasStrykerCOMMproject  
* Description: T-492006 (WSDL Generated class for project interface)
* Created Date: 05/15/2016
* Created By: Deepti Maheshwari (Appirio)
*
* Date Modified      Modified By      Description of the update
 
==================================================================*/
public class AsyncSchemasStrykerCOMMproject {
    public class ProjectResponseTypeFuture extends System.WebServiceCalloutFuture {
        public SchemasStrykerCOMMproject.ProjectResponseType getValue() {
            SchemasStrykerCOMMproject.ProjectResponseType response = (SchemasStrykerCOMMproject.ProjectResponseType)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
}