@isTest
private class Intr_AttachmentTriggerHandler_Test {

	static User assignTo;
	
    static testMethod void forCheckAttachmentOnTask() {
        
       CreateTestData();
		
		Test.startTest();
			//Create New Task
			Task task = Intr_TestUtils.createTask(1,assignTo.Id,true).get(0);
			Attachment attach=new Attachment();    
        	attach.Name='Unit Test Attachment';
        	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        	attach.body=bodyBlob;
        	attach.parentId=task.id;
        	attach.OwnerId = assignTo.Id;
        	insert attach;       		
		
		List<Attachment> AttachList = [SELECT Id,ParentId FROM Attachment WHERE ParentId = :task.id];
		
		delete AttachList;
	Test.stopTest();	

    }
    
    public static void CreateTestData() {
		
		assignTo = Intr_TestUtils.createUser(1,Intr_Constants.INSTRUMENTS_PROCARE_SALES_PROFILE,true).get(0);
		
	} 
	
}