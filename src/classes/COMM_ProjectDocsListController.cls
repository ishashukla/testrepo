// (c) 2016 Appirio, Inc.
//
// Description Controller for COMM_ProjectDocsListController
//
// Created On 04/26/2016   Deepti Maheshwari Reference (T-497456) 
//
// Modified Date          Modified By        Purpose
// 09/02/2016            Meghna Vijay        To remove Document Type != PO check from the query(I-232677)
public with sharing class COMM_ProjectDocsListController {
  public Project_Plan__c project{get;set;} 
  public Id selectedID{get;set;}
  public String newDocumentURL{get;set;}
  public COMM_Conga_Setting__c congaSetting{get;set;}
  public Integer listSize {get;set;}
  public List<Document__c> documentList{get;set;} 
  //public List<Document__c> documentListToShow{get;set;}
  public Integer pageSize{get;set;}
  public Integer pageNumber{get;set;}
  public Id docSignOffCheckId{get;set;}
  Id profileId=userinfo.getProfileId();
  String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
  public List<Document__c> opptyDocList = new List<Document__c>();
  Map<Id, Document__c> opptyDocMap = new Map<Id, Document__c>();
  public COMM_ProjectDocsListController(ApexPages.StandardController std){
    
    documentList = new List<Document__c>();
    documentListToShow = new List<Document__c>();
    project = (Project_Plan__c)std.getRecord();
    documentList = getDocumentList();
    pageSize = (Integer)COMM_Constant_Setting__c.getOrgDefaults().Quote_Document_Pagesize__c;
    pageNumber = 1;
    getDocumentListToShow();
    newDocumentURL = '/' + Document__c.SObjectType.getDescribe().getKeyPrefix() + '/e?retURL=/';
    newDocumentURL += project.id + '&RecordType=' + COMM_Constant_Setting__c.getOrgDefaults().Opportunity_Document_RT_ID__c;
    congaSetting = COMM_Conga_Setting__c.getOrgDefaults();
    isAsc = true;
    previousSortField = sortField = 'Name';
  }
  
 //----------------------------------------------------------------------------
 // Name : getDocumentList() Method returns latest version of documents by document type
 // Params : None
 // Returns : list of documents
 //----------------------------------------------------------------------------
  public List<Document__c> getDocumentList(){
    documentList = new List<Document__c>();
    documentListToShow = new List<Document__c>();
     List<Document__c> doclist = new List<Document__c>();
     List<Document__c> quoteDocList = new List<Document__c>();
    Map<ID, Opportunity> opportunityMap = new Map<ID, Opportunity>([SELECT ID 
                                          FROM Opportunity 
                                          WHERE Project_Plan__r.id = :project.Id]);
    Map<ID, BigMAchines__Quote__c> oracleQuotes = new Map<ID, BigMAchines__Quote__c>([SELECT ID 
                                          FROM BigMAchines__Quote__c 
                                          WHERE BigMachines__Opportunity__r.id in : opportunityMap.keyset()
                                          AND BigMAchines__is_primary__c = true]);          
    if(!profileName.contains('Integration')) {
      doclist = [SELECT CreatedDate,ID,Name,Approval_Status__c, 
                                Document_type__c, LastModifiedBy.Name, Opportunity__r.Name,
                                Oracle_Quote__r.Name,View__c, Oracle_Quote__c,LastModifiedDate,
                                Document_Link__c, Opportunity__c, Use_for_Sign_Off__c
                                FROM Document__c 
                                WHERE Opportunity__r.id =: opportunityMap.keyset() 
                                ORDER BY CreatedDate DESC];
      System.debug('*******'+'doclist.size='+doclist.size());
      quoteDocList = [SELECT CreatedDate,ID,Name,Approval_Status__c, 
                                Document_type__c, LastModifiedBy.Name, Opportunity__r.Name,
                                Oracle_Quote__r.Name,View__c, Oracle_Quote__c,LastModifiedDate,
                                Document_Link__c, Opportunity__c, Use_for_Sign_Off__c
                                FROM Document__c 
                                WHERE Oracle_Quote__r.id =: oracleQuotes.keyset() 
                                ORDER BY CreatedDate DESC];
    }
    else {
       doclist = [SELECT CreatedDate,ID,Name,Approval_Status__c, 
                                Document_type__c, LastModifiedBy.Name, Opportunity__r.Name,
                                Oracle_Quote__r.Name,View__c, Oracle_Quote__c,LastModifiedDate,
                                Document_Link__c, Opportunity__c, Use_for_Sign_Off__c
                                FROM Document__c 
                                WHERE Opportunity__r.id =: opportunityMap.keyset() and Document_type__c != 'Purchase Order' 
                                ORDER BY CreatedDate DESC];
      System.debug('*******'+'doclist.size='+doclist.size());
      quoteDocList = [SELECT CreatedDate,ID,Name,Approval_Status__c, 
                                Document_type__c, LastModifiedBy.Name, Opportunity__r.Name,
                                Oracle_Quote__r.Name,View__c, Oracle_Quote__c,LastModifiedDate,
                                Document_Link__c, Opportunity__c, Use_for_Sign_Off__c
                                FROM Document__c 
                                WHERE Oracle_Quote__r.id =: oracleQuotes.keyset() and Document_type__c != 'Purchase Order'
                                ORDER BY CreatedDate DESC];
    }
    System.debug('&&&&&&&&&&'+'quoteDocList.size'+quoteDocList.size());
    Map<String,Document__c> docTypeDocumentListMap = new Map<String, Document__c>();
    Map<String,Document__c> quoteDocTypeDocumentListMap = new Map<String, Document__c>();
    List<Document__c> docsToReturn = new List<Document__c>();
    for(Document__c docs : docList){
      if(!docTypeDocumentListMap.containsKey(docs.Document_type__c + '-' + docs.Opportunity__c)){
        docTypeDocumentListMap.put(docs.Document_type__c + '-' + docs.Opportunity__c, docs);
      }
    }
    if(docTypeDocumentListMap.size() > 0){
      docsToReturn.addAll(docTypeDocumentListMap.values());
    }
    for(Document__c docs : quoteDocList){
      if(!quoteDocTypeDocumentListMap.containsKey(docs.Document_type__c + '-' + docs.Oracle_Quote__c)){
        quoteDocTypeDocumentListMap.put(docs.Document_type__c + '-' + docs.Oracle_Quote__c, docs);
      }
    }
    if(quoteDocTypeDocumentListMap.size() > 0){
      docsToReturn.addAll(quoteDocTypeDocumentListMap.values());
    }
    if(docsToReturn.size() > 0){
      docsToReturn.sort();
      listSize = docsToReturn.size();
    }else{
      listSize = 0;
    }
    return docsToReturn;
  }
    
  //----------------------------------------------------------------------------
  // Name : doDelete() Method to delete selected document
  // Params : None
  // Returns : list of documents
  //----------------------------------------------------------------------------
  public void doDelete(){
    try{
      List<Document__c> docList = [SELECT Id 
                                  FROM Document__c 
                                  WHERE ID = :selectedID];
      if(docList != null && docList.size() > 0){
          delete docList;
      }
      documentList = getDocumentList();
      pageNumber = 1;
      getDocumentListToShow();
    }Catch(DmlException ex){
      //ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Unable to Delete this record!'));
    }
  }
    
 
  //Pagination methods
  
    public Boolean hasPrevious{get{
        if(PageNumber == 1){
            return false;
        }else{
            return true;
        }
    }set;}
    
    public Boolean hasNext{get{
        if(PageNumber == 1){
            if((listSize / pageSize  > 1)
            || (((listSize / pageSize ) == 1)
            && (math.mod(listSize, pageSize) != 0))){
                system.debug('Return FROM Here');
                return true;
            }else{
                system.debug('Return FROM tHere');
                return false;
            }
        }else{
            if((pageNumber > (listSize / pageSize))
            || (pageNumber == (listSize / pageSize)
            && (math.mod(listSize, pageSize) == 0))){
                system.debug('Return FROM Hereee');
                return false;
            }else{
                system.debug('Return FROM Here1');
                return true;
            }
        }
    }set;}
    
    public void next() 
    {
        pageNumber++;
        getDocumentListToShow();
    }
    
    public void previous() 
    {
        pageNumber--;
        getDocumentListToShow();
    }
    
    public void first() 
    {
        pageNumber = 1;
        getDocumentListToShow();
    }
    
    public void Last() 
    {
      if(Math.mod(listSize,pageSize) == 0){
        pageNumber = listSize / pageSize;
      }else{
        if(listSize / pageSize >= 1){
          pageNumber = listSize / pageSize + 1;
        }else{
          pageNumber = 1;
        }
      }
      getDocumentListToShow();
    }
  
  public List<Document__c> documentListToShow{get;set;}
  public List<Document__c> getDocumentListToShow(){
    documentListToShow = new List<Document__c>();
    if(documentList != null && documentList.size() > 0){
      if(pageNumber == 1){
        if(documentList.size() < pagesize){
          for(Integer i=0; i<documentList.size();i++){
              documentListToShow.add(documentList.get(i));
          }
        }else{
          for(Integer i=0; i<pagesize;i++){
              documentListToShow.add(documentList.get(i));
          }
        }
        system.debug('in if');
      }else{
        if((pageNumber-1) == (documentList.size() / pageSize)){
          system.debug('I am in second If');
          for(Integer i= ((pageNumber * pageSize) - pageSize); i < documentList.size(); i++){
                  documentListToShow.add(documentList.get(i));
          }
        }else{
          for(Integer i= ((pageNumber * pageSize) - pageSize); i<(pageNumber * pageSize);i++){
              documentListToShow.add(documentList.get(i));
          }
        }
      }
    }
    return documentListToShow;
  }
  public String sortField{get;set;}
  public String order{get;set;}
  public Boolean isAsc{get;set;}
  String previousSortField{get;set;}
  public void sortList(){
    system.debug('Sort Field : ' + sortField + ':::order::' + order + 'previousSortField' + previousSortField);
    
    isAsc = previousSortField.equals(sortField)? !isAsc : true; 
    previousSortField = sortField;
    order = isAsc ? 'ASC' : 'DESC';
    List<Document__c> resultList = new List<Document__c>();
    //Create a map that can be used for sorting 
    Map<object, List<Document__c>> objectMap = new Map<object, List<Document__c>>();
    for(Document__c ob : documentListToShow){
      if(sortField == 'LastModifiedBy'){
        if(objectMap.get(ob.LastModifiedBy.Name) == null){
          objectMap.put(ob.LastModifiedBy.Name, new List<Sobject>()); 
        }
        objectMap.get(ob.LastModifiedBy.Name).add(ob);
      }else if(sortField == 'Oracle_Quote__c'){
        if(objectMap.get(ob.Oracle_Quote__r.Name) == null){
          objectMap.put(ob.Oracle_Quote__r.Name, new List<Sobject>()); 
        }
        objectMap.get(ob.Oracle_Quote__r.Name).add(ob);
      }else if(sortField == 'Opportunity__c'){
        if(objectMap.get(ob.Opportunity__r.Name) == null){
          objectMap.put(ob.Opportunity__r.Name, new List<Sobject>()); 
        }
        objectMap.get(ob.Opportunity__r.Name).add(ob);
      }else{
        if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
            objectMap.put(ob.get(sortField), new List<Sobject>()); 
        }
        objectMap.get(ob.get(sortField)).add(ob);
      }
    }       
    //Sort the keys
    List<object> keys = new List<object>(objectMap.keySet());
    keys.sort();
    for(object key : keys){ 
      resultList.addAll(objectMap.get(key)); 
    }
    //Apply the sorted values to the source list
    documentListToShow.clear();
    if(order.toLowerCase() == 'asc'){
      documentListToShow.addAll(resultList);
    }else if(order.toLowerCase() == 'desc'){
      for(integer i = resultList.size()-1; i >= 0; i--){
        documentListToShow.add(resultList[i]);  
      }
    }
    
  }
  public void doSave() {
      if(opptyDocMap.size()>0) {
          //System.assert(false,opptyDocList);
          update opptyDocMap.values();
      }
      
      //System.assert(false,opptyDocList);
  }
  public void checkDoc() {
      for(Document__c doc : documentList) {
          if(doc.Id == docSignOffCheckId) {
              opptyDocMap.put(doc.Id,doc);
          }
      }
      
      //System.assert(false,opptyDocList);
  }
}