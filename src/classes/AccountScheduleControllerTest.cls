// (c) 2015 Appirio, Inc.
//
// Class Name: AccountScheduleController
// Description: Test Class for AccountScheduleController class.
// 
// February 15 2016, Prakarsh Jain  Original 
//
@isTest
public class AccountScheduleControllerTest {
    static Account acc;
    static Contact con;
    static testMethod void testAccountScheduleController(){
        createTestData();
        List<Block_Schedule__c> blck = TestUtils.createBlockSchedule(1, acc.id, con.id, false);
        blck[0].dow__c = 'Monday';
        blck[0].starting_Time__c = '12:00AM';
        blck[0].Ending_Time__c = '01:00AM';
        insert blck;
        Test.startTest();
            PageReference pageRef = Page.AccountSchedule;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', String.valueOf(con.Id));
            System.currentPageReference().getParameters().put('selectedContactOrAccount', String.valueOf(con.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            AccountScheduleController testConSch = new AccountScheduleController(sc);
            testConSch.blckRecId = blck[0].Id;
            testConSch.saveDt = '2016-02-16T00:00:00.000Z';
            List<selectOption> lstAssoContacts = testConSch.getLstAssoContacts();
            system.assertEquals(lstAssoContacts.size()>0,true);
            testConSch.showSchAsPerAccounts(con.Id, acc.id);
            testConSch.showSchAccounts();
            system.assertEquals(blck==null,false);
            blck[0].starting_Time__c = '12:00PM';
            blck[0].Ending_Time__c = '01:00PM';
            update blck;
            testConSch.saveDt = '2016-02-16T15:00:00.000Z';
            testConSch.selectedContactOrAccount = 'All'; 
            blck[0].starting_Time__c = '02:00PM';
            blck[0].Ending_Time__c = '03:00PM';
            blck[0].dow__c = 'Monday';
            update blck;
            testConSch.deleteSchedule();
        Test.stopTest();
    }
    
    static testMethod void testAccountScheduleController1(){
        createTestData();
        List<Block_Schedule__c> blck = TestUtils.createBlockSchedule(1, acc.id, con.id, false);
        blck[0].dow__c = 'Monday';
        blck[0].starting_Time__c = '12:00AM';
        blck[0].Ending_Time__c = '01:00AM';
        insert blck;
        Test.startTest();
            PageReference pageRef = Page.AccountSchedule;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', String.valueOf(acc.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(acc);
            AccountScheduleController testConSch = new AccountScheduleController(sc);
            testConSch.saveDt = '2016-02-16T00:00:00.000Z';
            List<selectOption> lstAssoContacts = testConSch.getLstAssoContacts();
            system.assertEquals(lstAssoContacts.size()>0,true);
            testConSch.getLstContacts();
            testConSch.selectedContactOrAccount = 'All'; 
            testConSch.assignToContactOrAccount = 'All';
            testConSch.showSchAsPerAccounts(con.Id, acc.id);
            testConSch.showSchAccounts();
            testConSch.saveSchedule();
            testConSch.createBlockScheduleData(blck[0]);
            system.assertEquals(blck==null,false);
            testConSch.deleteSchedule();
        Test.stopTest();
    }
    
    public static void createTestData(){
        acc = TestUtils.createAccount(1, true).get(0); 
        con = TestUtils.createContact(1, acc.Id, false).get(0);
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        con.RecordTypeId = rtByName.getRecordTypeId();
        con.timeZone__c = 'GMT–05:00    Peru Time (America/Lima)';
        insert con;
        
    }
}