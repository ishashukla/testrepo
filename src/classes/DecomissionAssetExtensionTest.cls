/**================================================================      
* Appirio, Inc
* Name: DecomissionAssetExtensionTest
* Description: Test class for DecomissionAssetExtension
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class DecomissionAssetExtensionTest {

	private static testMethod void testwithInactiveChildAsset() {
	    Test.startTest();
	    SVMXC__Installed_Product__c item = new SVMXC__Installed_Product__c(Inactivate_Child_Assets__c = 'Yes',Name='Test');
	    insert item;
        ApexPages.StandardController sc = new ApexPages.StandardController(item);  
        DecomissionAssetExtension controller = new DecomissionAssetExtension(sc);
        controller.getItems();
        controller.SaveCustom();
        Test.stopTest();
        System.assertEquals(controller.IsSuccess, true);
	}
	
	private static testMethod void testwithoutInactiveChildAsset() {
	    Test.startTest();
	    SVMXC__Installed_Product__c item = new SVMXC__Installed_Product__c(Name='Test');
	    insert item;
        ApexPages.StandardController sc = new ApexPages.StandardController(item);  
        DecomissionAssetExtension controller = new DecomissionAssetExtension(sc);
        controller.SaveCustom();
        Test.stopTest();
        System.assertEquals(controller.IsSuccess, false);
	}

}