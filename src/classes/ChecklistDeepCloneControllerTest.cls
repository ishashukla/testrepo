@isTest
private class ChecklistDeepCloneControllerTest {

    private static testMethod void test() {
      List<Account> accList = TestUtils.createAccount(1, true);
      Contact con = TestUtils.createCOMMContact(accList.get(0).Id, 'Test', true);
      List<Case> caseList = TestUtils.createCase(1, accList.get(0).Id, false);
      Schema.DescribeSObjectResult caseRT = Schema.SObjectType.Case; 
      Map<String,Schema.RecordTypeInfo> caseRTMap = caseRT.getRecordTypeInfosByName(); 
      Id caseRecordTypeId = caseRTMap.get(Constants.COMM_CASE_RTYPE).getRecordTypeId();
      //System.debug('NNNNNNNNNNNNNNNNNNNNN'+caseRecordTypeId);
      TestUtils.createRTMapCS(caseRecordTypeId, Constants.COMM_CASE_RTYPE,Constants.COMM);
      insert caseList;
      Checklist_Header__c checklist = new checklist_Header__c(status__c='New');
      insert checklist;
      CheckList_Detail__c woChecklist = new CheckList_Detail__c(Name = 'test1', Checklist_Header__c =checklist.id );
      insert woChecklist;
      ChecklistDeepCloneController.cloneChecklist(checklist.id);
      
    }

}