public with sharing class NV_SalesTotalsController {

    @AuraEnabled
    public static NV_Wrapper.RegionalDataWrapper getSalesTotalsAndUsersForToday(){
        Date currentDate = Date.today();
        String todaysDay = NV_Utility.getDayOfWeek(currentDate);

        if(Test.isRunningTest()) {
          todaysDay = ControlApexExecutionFlow.theCurrentDay;
        }

        Date startDate, endDate;

        if(todaysDay == 'Mon'){
            startDate = currentDate.addDays(-2);
            endDate = currentDate.addDays(1);
        }
        else if(todaysDay == 'Sun'){
            startDate = currentDate.addDays(-1);
            endDate = currentDate.addDays(2);
        }
        else if(todaysDay == 'Sat'){
            startDate = currentDate;
            endDate = currentDate.addDays(3);
        }
        else{
            startDate = currentDate;
            endDate = currentDate.addDays(1);
        }
        return getSalesTotalsAndUsers(startDate, endDate);
    }

    @AuraEnabled
    public static List<User> getFollowableUsers(){
        List<User> followableUsers = new List<User>();
        
        User currentUser = [SELECT Id, Name, LastName, FirstName, UserRole.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

        List<User> subordinates = NV_Utility.getSubordinateUsersOfSelectedRole(currentUser.Id, currentUser.UserRoleId, '%Territory Manager%');
        //CF S-402754 Problem if user had no subordinates then it would not be added to the list
        if(subordinates != null && subordinates.size() > 0){
            subordinates.add(currentUser);
        } else {
            subordinates = new List<User>();
            subordinates.add(currentUser); 
        }
        //CF S-402754 End

        return subordinates;
    }

    private static Boolean isInRange(Datetime dateToCompare, Date startDate, Date endDate){
        return (dateToCompare>= startDate && dateToCompare < endDate) ? true : false ;
    }


    private static NV_Wrapper.RegionalDataWrapper getSalesTotalsAndUsers(Date startDate, Date endDate){
        NV_Wrapper.RegionalDataWrapper dataWrapper = new NV_Wrapper.RegionalDataWrapper();

        List<User> followableUsers = getFollowableUsers();
        for(User user : followableUsers){
            dataWrapper.userWrapperMap.put(user.Id, new NV_Wrapper.UserWrapper(user));
        }

        Set<String> recordTypeToConsiderForClosed = new Set<String>{'NV_RMA', 'NV_Credit'};
        Set<String> expeditedShippingMethods = new Set<String>{'FDX-Parcel-Next Day 8AM','FDX-Parcel-P1 Overnight 10.30', 'FDX-Parcel-Saturday/8AM'};
        Set<String> statusToConsider = new Set<String>{'Delivered', 'Shipped', 'Closed', 'Open'}; //Open is a status on Credit orders, only.
        Set<String> statusToSkip = new Set<String>{'Delivered', 'Shipped', 'Closed', 'Cancelled', 'Credit Applied', 'Entered'};

        NV_Wrapper.UserWrapper userWrapperRecord ;  
        Set<Id> orderAlreadyCoveredForBookedTotal = new Set<Id>();

        for(OrderItem record: [SELECT
                                    Id, OrderId, Sub_Total__c, Status__c,
                                    Order.Id, Order.Account.Name, Order.Status, Order.Type,
                                    Order.EffectiveDate, Order.TotalAmount, Order.Date_Ordered__c, Order.Order_Total__c,
                                    Order.Hold_Type__c, Order.Customer_PO__c, Order.RecordType.DeveloperName, Order.LastModifiedDate,
                                    Order.Closed_Date__c, Order.On_Hold__c, Order.Line_Hold_Count__c, On_Hold__c, Shipped_Date__c,
                                    Hold_Description__c, Shipping_Method__c, Line_Amount__c,
                                    Order.Account.OwnerId, Order.Account.Owner.FirstName, Order.Account.Owner.LastName
                                FROM 
                                    OrderItem
                                WHERE 
                                    Order.Account.OwnerId in : dataWrapper.userWrapperMap.keySet() AND
                                    ((Order.EffectiveDate >= :startDate AND Order.EffectiveDate < :endDate)
                                        OR (Shipped_Date__c >= :startDate AND Shipped_Date__c < :endDate))
                                    
                                ORDER BY 
                                    Order.LastModifiedDate DESC]){
            
            if(dataWrapper.userWrapperMap.containsKey(record.Order.Account.OwnerId)){
                userWrapperRecord = dataWrapper.userWrapperMap.get(record.Order.Account.OwnerId);
                //Booked Total
                
                //An Order should contribute the Total Amount to the Book Total if its Order Start Date is the date of that tab.
                if(record.Order.Status != 'Cancelled' && record.Order.Status != 'Entered' && isInRange(record.Order.EffectiveDate, startDate, endDate) && !orderAlreadyCoveredForBookedTotal.contains(record.OrderId)){
                    //Connor Flynn S-402754 Changing so that RMA's and Credit remove from total instead of add
                    if(record.Order.RecordType.DeveloperName != 'NV_RMA' && record.Order.RecordType.DeveloperName != 'NV_Credit'){
                        dataWrapper.RegionalBookedTotal += record.Order.Order_Total__c;
                        userWrapperRecord.BookedTotal += record.Order.Order_Total__c;
                    } else{
                        if(record.Order.RecordType.DeveloperName == 'NV_RMA'){
                            dataWrapper.RegionalBookedTotal -= record.Order.Order_Total__c;
                            userWrapperRecord.BookedTotal -= record.Order.Order_Total__c;
                        } else{
                            dataWrapper.RegionalShippedTotal += record.Line_Amount__c;
                            userWrapperRecord.ShippedTotal += record.Line_Amount__c;
                        }
                    }
                    //Connor Flynn S-402754 END
                    orderAlreadyCoveredForBookedTotal.add(record.OrderId);

                }

                //Shipped Total
                //For each Order Product that has a Shipped Date that matches the Tab Date, the SubTotal of that Order Product record should be added to the Order's Shipped Total contribution.
                ////Per Issue I-185593 - all orders should be calculated on Closed status.
                if(record.Status__c != 'Cancelled' && isInRange(record.Shipped_Date__c, startDate, endDate) 
                        && statusToConsider.contains(record.Status__c)){
                    if(record.Order.RecordType.DeveloperName != 'NV_RMA' && record.Order.RecordType.DeveloperName != 'NV_Credit'){ 
                        dataWrapper.RegionalShippedTotal += record.Sub_Total__c; 
                        userWrapperRecord.ShippedTotal += record.Sub_Total__c; 
                    } else{
                        if(record.Order.RecordType.DeveloperName == 'NV_RMA'){
                            dataWrapper.RegionalShippedTotal -= record.Sub_Total__c; 
                            userWrapperRecord.ShippedTotal -= record.Sub_Total__c;
                        } else{
                            dataWrapper.RegionalShippedTotal += record.Line_Amount__c; 
                            userWrapperRecord.ShippedTotal += record.Line_Amount__c;
                        }
                    }
                }

                //For Credit Total
                if(recordTypeToConsiderForClosed.contains(record.Order.RecordType.DeveloperName) 
                    && statusToConsider.contains(record.Status__c)){
                        if(record.Order.RecordType.DeveloperName == 'NV_Credit'){
                            //Line Amount is a negative value
                            dataWrapper.RegionalCreditTotal += record.Line_Amount__c; 
                            userWrapperRecord.CreditTotal += record.Line_Amount__c;
                        } else{
                            dataWrapper.RegionalCreditTotal -= record.Sub_Total__c; 
                            userWrapperRecord.CreditTotal -= record.Sub_Total__c;
                        }
                }


            }

        }

        Set<Id> orderAlreadyCoveredForOpenTotal = new Set<Id>();
        Set<Id> orderAlreadyCoveredForPendingPO = new Set<Id>();
         
        // For Open Orders
        for(OrderItem record: [SELECT
                                    Id, OrderId, Sub_Total__c, Status__c,
                                    Order.Id, Order.Account.Name, Order.Status, Order.Type,
                                    Order.EffectiveDate, Order.TotalAmount, Order.Date_Ordered__c, Order.Order_Total__c,
                                    Order.Hold_Type__c, Order.Customer_PO__c, Order.RecordType.DeveloperName, Order.LastModifiedDate,
                                    Order.Closed_Date__c, Order.On_Hold__c, Order.Line_Hold_Count__c, On_Hold__c, Shipped_Date__c,
                                    Hold_Description__c, Shipping_Method__c,
                                    Order.Account.OwnerId, Order.Account.Owner.FirstName, Order.Account.Owner.LastName
                                FROM 
                                    OrderItem
                                WHERE 
                                    Order.Account.OwnerId in : dataWrapper.userWrapperMap.keySet()
                               		AND Order.RecordType.DeveloperName <> 'NV_Credit'
                                    AND Order.Status NOT in : statusToSkip
                                    AND Order.Date_Ordered__c <= TODAY
                                    AND (Order.Type != 'LTC Transfer' 
                                         OR (Order.Type = 'LTC Transfer' 
                                             AND Order.EffectiveDate = LAST_N_DAYS:3))
                                ORDER BY 
                                    Order.LastModifiedDate DESC]){
            
            if(dataWrapper.userWrapperMap.containsKey(record.Order.Account.OwnerId)){
                userWrapperRecord = dataWrapper.userWrapperMap.get(record.Order.Account.OwnerId);
                
                //Add Order Total @ First Iteration of that Order
                if(!orderAlreadyCoveredForOpenTotal.contains(record.OrderId)){
                    if(record.Order.RecordType.DeveloperName == 'NV_RMA'){
                        dataWrapper.RegionalOpenTotal -= record.Order.Order_Total__c;
                        userWrapperRecord.OpenTotal -= record.Order.Order_Total__c;
                    } else {
                        dataWrapper.RegionalOpenTotal += record.Order.Order_Total__c;
                        userWrapperRecord.OpenTotal += record.Order.Order_Total__c;
                    }
                    orderAlreadyCoveredForOpenTotal.add(record.OrderId);
                }

                //Subtract values of OrderItem having status as "Shipped", "Closed", or "Delivered"
                if(statusToConsider.contains(record.Status__c)){
                    if(record.Order.RecordType.DeveloperName == 'NV_RMA'){
                        dataWrapper.RegionalOpenTotal += record.Sub_Total__c;
                        userWrapperRecord.OpenTotal += record.Sub_Total__c;
                    } else{
                        dataWrapper.RegionalOpenTotal -= record.Sub_Total__c;   
                        userWrapperRecord.OpenTotal -= record.Sub_Total__c; 
                    }
                }

                if(record.Order.RecordType.DeveloperName == 'NV_Bill_Only' && 
                    !orderAlreadyCoveredForPendingPO.contains(record.OrderId) && 
                        (String.isBlank(record.Order.Customer_PO__c) 
                        	|| record.Order.Customer_PO__c == 'PO To Follow' )
                    ){
                        dataWrapper.RegionalPendingPOTotal += record.Order.Order_Total__c; 
                        userWrapperRecord.PendingPOTotal += record.Order.Order_Total__c;
                        orderAlreadyCoveredForPendingPO.add(record.OrderId);
                }
            }
        }
        dataWrapper.finalize();//Force the UserWrapper to be populated.
        return dataWrapper;

    }
}