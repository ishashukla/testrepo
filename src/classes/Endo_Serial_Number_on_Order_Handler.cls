public class Endo_Serial_Number_on_Order_Handler {
 
    Public static List<Asset__c> SerialNumbers {get; set;}
	
    public Endo_Serial_Number_on_Order_Handler(ApexPages.StandardController controller)
    {
         
        Order Record = (Order) controller.getRecord();    //Get Order from controller
		system.debug('****GAGAN**** Record :'+Record);
        
        Order ord1 = [SELECT Id, OrderNumber, Order_Number_Oracle__c FROM ORDER WHERE id =: Record.Id];
        
        SerialNumbers = [SELECT Id, Name, Shipped_Date__c, Product__c, Product__r.Description, Status__c, Sales_Order__c, Part_Number__c, Asset_ID__c FROM Asset__c WHERE Sales_Order__c = :ord1.Order_Number_Oracle__c]; 
		system.debug('****GAGAN*** SerialNumbers:'+SerialNumbers);
    }
}