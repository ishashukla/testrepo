/**================================================================      
* Appirio, Inc
* Name: COMM_ContactTriggerHandler
* Description: Trigger Handller for Contact(T-477658)
* Created By : Deepti Maheshwari
* Created Date : 26 Feb 2016
*
* Date Modified      Modified By      Description of the update
* 22 Apr 2016        Meghna Vijay     To restrict Contact Sharing Records only for Active Users (I-212592)
* 25 Apr 2016        Kirti Agarwal    I-214801 - Update DML statement(Database.insert instead of Insert)
* 21 Aug 2016        Kanika Mathur    T-523264 - To update sharing for COMM Sales Rep on Account if RT contains COMM
==================================================================*/

public class COMM_ContactTriggerHandler {

static List<ContactShare> contactSharesToRestore = new List<ContactShare>();
//=======================================================================
// Method called on after insert
//=======================================================================
    public static void afterInsert(List<Contact> newList) {
        system.debug('This is new Insert List'  + newList);
        openAccessForAccountOwner(newList, null);
        provideReadAccessToAccountTeam(newList, null);
    }
    public static void beforeInsert(List<Contact> newList) {
        linkUserOnSalesRepMatch(newList, null);
    }

//=======================================================================
// Method called on after update
//=======================================================================    
    public static void afterUpdate(List<Contact> newList,Map<Id,Contact> oldMap) {
        system.debug('This is new update List'  + newList);
        restoreShares();
        openAccessForAccountOwner(newList, oldMap);
        provideReadAccessToAccountTeam(newList, oldMap);
        revokeAccessOnContactIfPrivate(newList, oldMap);
        syncSalesRepOnCases(newList, oldMap);
        
    }

//=======================================================================
// Method called on before Update
//=======================================================================
    public static void beforeUpdate(List<Contact> newList,Map<Id,Contact> oldMap) {
      linkUserOnSalesRepMatch(newList, oldMap);
      cacheManualShares(newList, oldMap);
      
    }
    private static void linkUserOnSalesRepMatch(List<Contact> contactList, Map<Id, Contact> oldMap) {
      Boolean isUpdate = oldMap != null ? true : false;
      Id devRecordTypeId = 
      Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.COMM_CONTACT_RTYPE).getRecordTypeId();
      Map<String,Id> contactIDMap = new Map<String, Id>();
      for(Contact con : contactList) {
        if((!isUpdate|| oldMap.get(con.id).Resource_ID__c != con.Resource_ID__c)
          && con.recordTypeID == devRecordTypeId) {
          contactIDMap.put(con.Resource_ID__c,con.Id);
        }
      }
      Map<String,Id> userIDMap = new Map<String, Id>();
      List<String> salesRepID  = new List<String>();
      if(contactIDMap != NULL) {
        for(User us: [Select id,Sales_Rep_ID__c
                       FROM user
                       WHERE Sales_Rep_ID__c in : contactIDMap.keyset() ]) {
                  
          if(us != NULL) {
            userIDMap.put(us.Sales_Rep_ID__c, us.id); 
          }                  
        }
      }
      for(Contact con : contactList) {
          if(userIDMap.containsKey(con.Resource_ID__c) && con.recordTypeID == devRecordTypeId && con.Resource_ID__c != NULL ) {
            System.debug('########### Resource Id ' + con.Resource_ID__c);
            con.Sales_Support_Agent__c  = userIDMap.get(con.Resource_ID__c);
          }
          else if(con.Resource_ID__c == NULL && con.recordTypeID == devRecordTypeId ){
            con.Sales_Support_Agent__c  = NULL;
          }    
      }
    }
//=======================================================================
// Method to cache manual shares on owner change
//=======================================================================
  private static void cacheManualShares(List<Contact> contactList, Map<Id, Contact> oldMap) {
    System.debug('%%%%%%%%%%%% update ka Contact' + contactList );
    Id devRecordTypeId = 
    Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.COMM_CONTACT_RTYPE).getRecordTypeId();
    Set<ID> contactIDSet = new Set<ID>();
    for(Contact con : contactList) {
      if(oldMap.get(con.id).OwnerID != con.OwnerID
        && con.recordTypeID == devRecordTypeId){
        contactIDSet.add(con.id);
      }
    }
    contactSharesToRestore = [SELECT ID, ContactID, ContactAccessLevel, UserOrGroupID 
                              FROM ContactShare 
                              WHERE rowcause = :Constants.ROW_CAUSE_MANUAL
                              AND ContactID  in :contactIDSet];
  }

//=======================================================================
// Method to restore manual shares on owner change
//=======================================================================
  private static void restoreShares(){
    List<ContactShare> contactShares = new List<ContactShare>();
    
    if(contactSharesToRestore != null && contactSharesToRestore.size() > 0){
      for(ContactShare conShare : contactSharesToRestore){
        ContactShare shre = new ContactShare();
        shre.ContactID = conShare.ContactID;
        shre.ContactAccessLevel = conShare.ContactAccessLevel;
        shre.UserOrGroupID = conShare.UserOrGroupID;
        contactShares.add(shre);
      }
      //I-214801 - updated 
      Database.insert(contactShares,false);
    }
  }

//=======================================================================
// Method to open contact access for account owner 
// Modified by Kanika Mathur to include Sales Rep for COMM Accounts
//=======================================================================
    private static void openAccessForAccountOwner(List<Contact> contactList, Map<Id, Contact> oldMap){
        List<ContactShare> contactShares = new List<ContactShare>();
        Set<ID> contactIDs = new Set<ID>();
        Id devRecordTypeId = 
        Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.COMM_CONTACT_RTYPE).getRecordTypeId();
        for(Contact con : contactList) {
          if(!con.Private__c && con.recordTypeID == devRecordTypeId) {
            if(oldMap != null) {
              if(oldMap.get(con.id).Private__c != con.Private__c) {
                contactIDs.add(con.id);
              }
            }else {
              contactIDs.add(con.id);
            }
          }
        }
        
        if(contactIDs.size() > 0){ 
          for(Contact contactRec : [SELECT account.OwnerID, account.recordTypeID, account.COMM_Sales_Rep__c, accountId, ID 
                                    FROM Contact 
                                    WHERE Id in: contactIDs]) {
            if(getRecordTypeName(contactRec.account.recordTypeID,'Account').contains('COMM')
                && contactRec.account.COMM_Sales_Rep__c != null) {
                    if(contactRec.account.COMM_Sales_Rep__c != userinfo.getUserID()) {
                        ContactShare conShare = new ContactShare();
                        conShare.contactId = contactRec.Id;
                        conShare.UserOrGroupId = contactRec.account.COMM_Sales_Rep__c;
                        System.debug('117 Line pr@@@@@@@@' + conShare.UserOrGroupId);
                        conShare.rowcause = Constants.ROW_CAUSE_MANUAL;
                        conShare.ContactAccessLevel = Constants.PERM_EDIT;
                        contactShares.add(conShare);
                    }
            } else if(contactRec.account.OwnerID != userinfo.getUserID()) {
                ContactShare conShare = new ContactShare();
                conShare.contactId = contactRec.Id;
                conShare.UserOrGroupId = contactRec.account.OwnerID;
                System.debug('117 Line pr@@@@@@@@' + conShare.UserOrGroupId);
                conShare.rowcause = Constants.ROW_CAUSE_MANUAL;
                conShare.ContactAccessLevel = Constants.PERM_EDIT;
                contactShares.add(conShare);
             }
          }
          //I-214801 - updated 
          Database.insert(contactShares, false);
        }
    }

//=======================================================================
// Method to open access of account teams on contact
//=======================================================================  
    private static void provideReadAccessToAccountTeam(List<Contact> contactList, Map<Id, Contact> oldMap) {
        List<ContactShare> contactShares = new List<ContactShare>();
        Map<ID, ID> contactAccountMap = new Map<Id,Id>();
        Map<ID, List<AccountTeamMember>> accountTeamMembers = new Map<ID, List<AccountTeamMember>>();
        Map<ID, User> activeUserMap = new Map<ID, User>();
      Set<ID> userIDs = new Set<ID>();
      
        Id devRecordTypeId = 
          Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.COMM_CONTACT_RTYPE).getRecordTypeId();
        for(Contact con : contactList){
          if(!con.Private__c && con.recordTypeID == devRecordTypeId) {
            if(oldMap != null) {
              if(oldMap.get(con.id).Private__c != con.Private__c) {
                contactAccountMap.put(con.id,con.AccountID);
              }
            }else{
              contactAccountMap.put(con.id,con.AccountID);
            }
          }
        }
        System.debug('COMM MAp$$$$$' + contactAccountMap);
        for(AccountTeamMember atms : [SELECT AccountId, TeamMemberRole, UserId,AccountAccessLevel 
                                      FROM AccountTeamMember 
                                      WHERE AccountID in :contactAccountMap.values()]) {
          if(accountTeamMembers.containsKey(atms.AccountId)) {
            accountTeamMembers.get(atms.AccountID).add(atms);
          }else {
            accountTeamMembers.put(atms.AccountID, new List<AccountTeamMember>{atms});
          }
          userIDs.add(atms.UserID);
        }
        // Active User Map to store only active users whose Id lies in Account Team Member User Id(I-212592)
        if(userIDs.size() > 0){
          activeUserMap = new Map<ID, User>([SELECT ID, 
                                                    isActive 
                                             FROM User
                                             WHERE Id in :userIDs 
                                             AND isActive = true]);
        }
        //Contact share records created only for active users (I-212592)
        for(Id contactID : contactAccountMap.keyset()) {
          if(accountTeamMembers.containsKey(contactAccountMap.get(contactID))) {
            for(AccountTeamMember atm : accountTeamMembers.get(contactAccountMap.get(contactID))) {
              if(atm.UserId != userinfo.getUserID()
              && activeUserMap.containsKey(atm.UserId)) {
                ContactShare conShare = new ContactShare();
                conShare.contactId = contactID;
                conShare.UserOrGroupId = atm.UserId;
                conShare.rowcause = Constants.ROW_CAUSE_MANUAL;
                conShare.ContactAccessLevel = Constants.PERM_READ;
                contactShares.add(conShare);
              }
            }
          }
        }
        //I-214801 - updated 
        Database.insert(contactShares,false);
  }

//=======================================================================
// Method to remove access if contact in private
//=======================================================================
    private static void revokeAccessOnContactIfPrivate(List<Contact> contactList, Map<Id, Contact> oldMap) {
        Set<ID> contactIDs = new Set<ID>();
        Id devRecordTypeId = 
        Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.COMM_CONTACT_RTYPE).getRecordTypeId();
        for(Contact con : contactList) {
          if(con.Private__c && con.recordTypeID == devRecordTypeId) {
            if(oldMap != null) {
              if(oldMap.get(con.id).Private__c != con.Private__c) {
                contactIDs.add(con.id);
              }
            }else {
              contactIDs.add(con.id);
            }
          }
        }
        
        List<ContactShare> contactShares = [SELECT Id 
                                            FROM ContactShare 
                                            WHERE rowcause = :Constants.ROW_CAUSE_MANUAL
                                            AND contactId in :contactIDs];
        if(contactShares != null && contactShares.size() > 0){
          delete contactShares;
        }
    }
    
    public static void syncSalesRepOnCases(List<Contact> newList, Map<Id, Contact> oldMap) {
    Map<Id, Contact> newMap = new Map<Id, Contact>();
    for(Contact c: newList){
        newMap.put(c.Id,c);
    }
    List<Contact> filteredContacts = new List<Contact>();
    for(Contact c : newMap.values()) {
      if(c.Top_35_Sales_Agent__c != oldMap.get(c.Id).Top_35_Sales_Agent__c) {
       system.debug('in handler'+c);
        filteredContacts.add(c);
      }
    }
    
    if(filteredContacts.size() > 0) {
      Map<Id, List<Case>> contactCaseMap = new Map<Id, List<Case>>();
      List<Case> updateableCases = new List<Case>();
      for(Case c: [Select Id,Sales_Rep__c, Sales_Rep_Top_35_Sales_Agent__c
                   from Case
                   where Sales_Rep__c IN: filteredContacts]) {
                  
        if(!contactCaseMap.containsKey(c.Sales_Rep__c)) {
          contactCaseMap.put(c.Sales_Rep__c, new List<Case>());
        }
        contactCaseMap.get(c.Sales_Rep__c).add(c);
      }
      
      if(contactCaseMap.size() > 0) {
        for(Id contactId : contactCaseMap.keySet()) {
          for(Case c : contactCaseMap.get(contactId)) {   
             c.Sales_Rep_Top_35_Sales_Agent__c = newMap.get(contactId).Top_35_Sales_Agent__c;
            updateableCases.add(c);
          }  
       }
        update updateableCases;
      }
    }
  }
  
//=======================================================================
// Method to get record type name of any sObject
//=======================================================================
  public static String getRecordTypeName(Id recordType, String objectType) {
    Map < Id, Schema.RecordTypeInfo > rtMapById = null;
    
    if (rtMapById == null) {
      Map < String, Schema.SObjectType > gd = Schema.getGlobalDescribe();
      Schema.SObjectType ctype = gd.get(objectType);
      Schema.DescribeSObjectResult d1 = ctype.getDescribe();
      rtMapById = d1.getRecordTypeInfosById();

    }
    
    Schema.RecordTypeInfo recordTypeDetail = rtMapById.get(recordType);
    if (recordTypeDetail != null) {
      return recordTypeDetail.getName();
    } else {
      return null;
    }
  }
    
    
}