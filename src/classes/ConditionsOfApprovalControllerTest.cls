//
// (c) 2012 Appirio, Inc.
//
//  Test class of  WebserviceAmendOpportunity
//
// 19 Aug 2015     Naresh K Shiwani      Original
//
@isTest
private class ConditionsOfApprovalControllerTest {
  static testmethod void testMethodSendEmail(){
    Schema.RecordTypeInfo rtByNameFlex = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional');
    Id recordTypeFlex = rtByNameFlex.getRecordTypeId();
    Test.startTest();
    TestUtils.createCommConstantSetting();
    Account acnt                        = TestUtils.createAccount(1, true).get(0);
    Opportunity opp                     = TestUtils.createOpportunity(1, acnt.id, false).get(0);
    opp.recordTypeId 					= recordTypeFlex; 
    System.assertNotEquals(opp, null);
    Flex_Credit__c fc                   = new Flex_Credit__c();
    fc.Credit_Amount__c                 = 10;
    fc.Credit_Approval_Duration__c      = 1;
    fc.Credit_Status__c                 = 'New';
    fc.Opportunity__c                   = opp.Id;
    insert fc;
    Conditions_of_Approval__c conOfApp  = new Conditions_of_Approval__c();
    conOfApp.Flex_Credit__c             = fc.id;
    insert conOfApp;
    System.assertNotEquals(conOfApp, null);
    ConditionsOfApprovalController oppSendEmailContObj = new ConditionsOfApprovalController(new ApexPages.StandardController(opp));
    Test.stopTest();
  }
}