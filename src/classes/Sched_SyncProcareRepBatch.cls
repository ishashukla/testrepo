/**
 *  Purpose         :   This scheduler is used to schedule SyncProcareRepBatch class.
 *
 *	Created By		:	Padmesh Soni (Appirio Offshore)
 *
 *	Created Date	:	10/06/2016
 *
 *	Current Version	:	V_1.0
 *
 *	Revision Log	:	V_1.0 - Created - S-441563
 **/
global class Sched_SyncProcareRepBatch implements Schedulable {
    
    //execute method to execute the logic of batch processing 
    global void execute(SchedulableContext ctx) {
        
        //Batch executes here
        Database.executeBatch(new SyncProcareRepBatch(), 200);
    }
}