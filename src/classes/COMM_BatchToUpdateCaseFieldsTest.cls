/**================================================================      
* Appirio, Inc
* Name: COMM_BatchToUpdateCaseFieldsTest
* Description: Test class of COMM_BatchToUpdateCaseFields
* Created Date: 29th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 
@isTest
private class COMM_BatchToUpdateCaseFieldsTest
{
	static List<Case> caseList;
   	static Id rtCase2;
    static User userRecord;
    static User OppMem1;

	@isTest
	static void itShouldPopulateCaseFields()
	{
		createData();
        Test.startTest();
        	insert caseList;
        	Database.executeBatch(new COMM_BatchToUpdateCaseFields());
        Test.stopTest();

	}
	
	private static void createData() {
        userRecord = TestUtils.createUser(1, 'System Administrator', false).get(0);
        OppMem1 = TestUtils.createUser(1, 'Strategic Sales Manager - COMM US', false).get(0);
        OppMem1.Email = 'test@test.com';
        insert OppMem1;
        userRecord.ManagerId = OppMem1.Id;
        insert userRecord;
        
        String rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Change Request').getRecordTypeId();
        
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM Change Request');
        insert rtMap;
        rtCase2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Commercial Operations Support').getRecordTypeId();
        RTMap__c rtMap2 = new RTMap__c(Name = rtCase2,
                                       Object_API_Name__c='Case',
                                       Division__c = 'COMM',
                                       RT_Name__c='COMM Commercial Operations Support');
        insert rtMap2;
        Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Account acc = TestUtils.createAccount(1, false).get(0);
        acc.RecordTypeId = accRT;
        acc.Endo_Active__c = true;
        acc.COMM_Shipping_Country__c = 'United States';
        acc.COMM_Shipping_Address__c = 'test';
        acc.COMM_Shipping_State_Province__c  = 'Arizona';
        acc.COMM_Shipping_PostalCode__c = '123';
        acc.COMM_Shipping_City__c = 'testcity';
        insert acc;
        
        Contact con1 = TestUtils.createCOMMContact(acc.id, 'TestContact', false);
        con1.Top_35_Sales_Agent__c = false; 
        insert con1;

        caseList = TestUtils.createCase(10, acc.id, false);
        for(integer i=0;i<caseList.size();i++) {
           
            caseList[i].Status = 'New';
            caseList[i].RecordTypeId = rtCase;
        }
        caseList[0].Sales_Rep__c = con1.Id;
        caseList[1].Status = 'Closed';
        caseList[5].RecordTypeId = rtCase2;
    }
}