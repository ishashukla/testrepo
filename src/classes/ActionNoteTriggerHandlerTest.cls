/*
Name 					- ActionNoteTriggerHandlerTest
Created By 				- Nitish Bansal
Created Date 			- 02/23/2016
Purpose 				- T-477246 
Updated                 - T-477244
*/

@isTest
private class ActionNoteTriggerHandlerTest
{
	@isTest
	static void itShouldRestrictUpdate()
	{
		//Creating multiple users
		List<User> userList = TestUtils.createUser(2, 'System Administrator', true);
		List<Action_Note__c> noteList = new List<Action_Note__c>() ;
		Action_Note__c note = new Action_Note__c();

		//Creating an account and multile action notes as one user
		system.runAs(userList.get(0)){
			List<Account> accountList = TestUtils.createAccount(2, false);
			accountList[0].Level__c = Constants.SYSTEM_LEVEL;
			accountList[1].Level__c = Constants.SYSTEM_LEVEL;
			insert accountList;

			for(Integer i = 0; i <= 5; i++){
				note = new Action_Note__c();
				note.AccountName__c = accountList[0].Id;
				noteList.add(note);
			}
			
			insert noteList;
			noteList.get(0).AccountName__c = accountList[1].Id;
			
		}

		//Updating multiple notes as second user
		system.runAs(userList.get(1)){
			for(Action_Note__c tempNote : noteList){
				//tempNote.Type__c = 'System';
				tempNote.Notes__c = 'abstract';
			}
			
			try{
				update noteList;
			} catch(Exception e){
				Boolean messagefound = false; 
				//Checking that user gets excepted error message
				if(e.getMessage().contains(System.Label.Action_Note_Edit_Validation_Message)){
					messagefound = true;
				}
				system.assertEquals(true, messagefound);
			}
		}

		
	}

	@isTest
	static void itShouldUpdateShortNotes()
	{
		//Creating a System Admin User
		List<User> userList = TestUtils.createUser(1, 'System Administrator', true);
		List<Action_Note__c> noteList = new List<Action_Note__c>() ;
		Action_Note__c note = new Action_Note__c();

		//Creating an account and multile action notes as the Admin user
		system.runAs(userList.get(0)){
			List<Account> accountList = TestUtils.createAccount(2, false);
			accountList[0].Level__c = Constants.SYSTEM_LEVEL;
			accountList[1].Level__c = Constants.SYSTEM_LEVEL;
			insert accountList;
			for(Integer i = 0; i <= 5; i++){
				note = new Action_Note__c();
				//Value > 255 char
				note.Notes__c = 'nick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testing';
				note.AccountName__c = accountList[0].Id;
				noteList.add(note);
			}
			//value less thn 255 char
			noteList.get(0).Notes__c = 'Nick testing';
			insert noteList;

			Set<Id> recordIds = new Set<Id>();
			for(Action_Note__c insertedNote : noteList){
				recordIds.add(insertedNote.Id);
			}

			//Checking that short notes field is populated correctly via Insert
			for(Action_Note__c updatedNote : [Select Id, Notes_short__c from Action_Note__c Where Id In : recordIds]){
				system.assertNotEquals(null, updatedNote.Notes_short__c);
			}

			//Updatin the Notes__c field value for the inserted records
			noteList = new List<Action_Note__c>() ;
			for(Action_Note__c updatedNote : [Select Id, Notes_short__c, Notes__c from Action_Note__c Where Id In : recordIds]){
				updatedNote.Notes__c = 'Modified Value of Main Notes Field';
				noteList.add(updatedNote);
			}
			//Value > 255 characters
			noteList.get(0).Notes__c = 'nick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testing';
			Id tempId = noteList.get(0).Id;
			update noteList;

			//Checking that short notes field is populated correctly via Update
			for(Action_Note__c updatedNote : [Select Id, Notes_short__c from Action_Note__c Where Id In : recordIds]){
				system.assertNotEquals(null, updatedNote.Notes_short__c);
				if(tempId != updatedNote.Id){
					system.assertEquals('Modified Value of Main Notes Field', updatedNote.Notes_short__c);
				}
			}

			delete noteList[3];
		}
		
		userList = TestUtils.createUser(1, Label.Sales_COMM_INTL, true);
		system.runAs(userList.get(0)){
			try{
				List<Account> accountList = TestUtils.createAccount(1, false);
				accountList[0].Level__c = Constants.SYSTEM_LEVEL;
				insert accountList;
				for(Integer i = 0; i <= 5; i++){
					note = new Action_Note__c();
					//Value > 255 char
					note.Notes__c = 'nick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testing';
					note.AccountName__c = accountList[0].Id;
					noteList.add(note);
				}
				//value less thn 255 char
				noteList.get(0).Notes__c = 'Nick testing';
				insert noteList;
				delete noteList[0];
			} catch(Exception e){
				Boolean messagefound = false; 
				//Checking that user gets excepted error message
				if(e.getMessage().contains(System.Label.Action_Note_Delete_Validation_Message)){
					messagefound = true;
				}
			}
		}
		userList = TestUtils.createUser(1, Label.Sales_Rep_COMM_US, true);
		system.runAs(userList.get(0)){
			try{
				List<Account> accountList = TestUtils.createAccount(1, false);
				accountList[0].Level__c = Constants.SYSTEM_LEVEL;
				insert accountList;
				for(Integer i = 0; i <= 5; i++){
					note = new Action_Note__c();
					//Value > 255 char
					note.Notes__c = 'nick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testingnick testing';
					note.AccountName__c = accountList[0].Id;
					noteList.add(note);
				}
				//value less thn 255 char
				noteList.get(0).Notes__c = 'Nick testing';
				insert noteList;
				delete noteList[1];
			} catch(Exception e){
				Boolean messagefound = false; 
				//Checking that user gets excepted error message
				if(e.getMessage().contains(System.Label.Action_Note_Delete_Validation_Message)){
					messagefound = true;
				}
			}
		}
	}
}