/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMemberEdit
 * Description: 
 * Created Date: 3/2/2016
 * Created By: Deepti Maheshwari
 *
=====================================================================*/
public without sharing class AccountTeamMemberEdit {
  
  public Id accId{get;set;}
  public List<selectOption> accountOppAccessLevels {get;set;}
  public List<selectOption> caseAccessLevels {get;set;}
  public AccountTeamMember teamMemberToEdit{get;set;}
  public Id teamMemberID;
  public String memberName{get;set;}
  public accountTeamWrapper wrapper{get;set;}
  //==========================================================================
  // Constructor
  //==========================================================================
  public AccountTeamMemberEdit () {
    Id teamMemberID = ApexPages.currentPage().getParameters().get('Id');
    teamMemberToEdit = [SELECT AccountID,userId,TeamMemberRole FROM AccountTeamMember WHERE ID =: teamMemberID];
    accountOppAccessLevels = new List<selectOption>();
    accountOppAccessLevels.add(new selectOption('Read','Read Only'));
    accountOppAccessLevels.add(new selectOption('Edit','Read/Write'));
    caseAccessLevels = new List<selectOption>();
    caseAccessLevels.add(new selectOption('None','Private'));
    caseAccessLevels.add(new selectOption('Read','Read Only'));
    caseAccessLevels.add(new selectOption('Edit','Read/Write'));
    if(teamMemberToEdit != null){
        accId = teamMemberToEdit.accountId;
        List<AccountShare> accShare = [SELECT CaseAccessLevel, OpportunityAccessLevel,AccountAccessLevel FROM AccountShare WHERE AccountID = :teamMemberToEdit.AccountID 
                                      AND userOrGroupId = :teamMemberToEdit.UserID];
        List<User> memberUser = [SELECT Name FROM User where ID = :teamMemberToEdit.userID];
        if(memberUser != null){
            memberName = memberUser.get(0).name;
        }
        if(accShare != null){
           wrapper = new accountTeamWrapper();
           wrapper.member=teamMemberToEdit;
           wrapper.accountAccess = accShare.get(0).AccountAccessLevel;
           wrapper.opportunityAccess = accShare.get(0).OpportunityAccessLevel;
           wrapper.caseAccess = accShare.get(0).CaseAccessLevel;
           wrapper.accShare= accShare.get(0); 
        }
    }
  }
  
  //==========================================================================
  // Method to called on save button
  //==========================================================================
  public pagereference saveNewTeamMembers () {
    List<AccountTeamMember> atmToUpdate = new List<AccountTeamMember>();
    List<AccountShare> accshareToUpdate = new List<AccountShare>();
    try {
        if ( wrapper.member.userId != null ) {
          atmToUpdate.add(wrapper.member);
          
          if ( wrapper.accShare != null) {
            wrapper.accshare.AccountAccessLevel = wrapper.accountAccess;
            wrapper.accshare.OpportunityAccessLevel = wrapper.opportunityAccess;
            wrapper.accshare.CaseAccessLevel = wrapper.caseAccess;
            accshareToUpdate.add(wrapper.accshare);
            system.debug(wrapper.accshare);
        }
      }

      
      if (!atmToUpdate.isEmpty()) {
        update atmToUpdate;
      }
      if (!accshareToUpdate.isEmpty()) {
        update accshareToUpdate;
      }

      return new pagereference('/'+accId);
    }
    catch ( exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, ex.getMessage()));
      return null;
    }
  }
  
  //==========================================================================
  // Method to cancel and return back to Account page
  //==========================================================================
  public pagereference doCancel () {
    wrapper = null;
    return new pagereference('/'+accId);
  }


  //==========================================================================
  // Wrapper class to hold the Team Member record as well as their access levels
  //==========================================================================
  public class accountTeamWrapper {
    public AccountTeamMember member {get;set;}
    public String accountAccess {get;set;}
    public String opportunityAccess {get;set;}
    public String caseAccess {get;set;}
    public AccountShare accShare{get;set;}
    public accountTeamWrapper() {
      member = new AccountTeamMember();
      opportunityAccess = '';
      caseAccess = '';
    }
  }
  
}