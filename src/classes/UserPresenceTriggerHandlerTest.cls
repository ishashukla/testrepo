/******************************************************************************************
 *  Name    : UserPresenceTriggerHandlerTest
 *  Purpose : Test Class for UserPresenceTriggerHandler
 *  Author  : Ashu Gupta
 *  Date    : 27 May 2016                             
 ********************************************************************************************/

@isTest
private class   UserPresenceTriggerHandlerTest {

    private static testMethod void createTestData() {
        List<ServicePresenceStatus> servicePresenseStatus = [SELECT id, MasterLabel 
                                                                   FROM ServicePresenceStatus                                                                                                       
                                                                   limit 1];
       UserServicePresence usr = new UserServicePresence();
       usr.userId = UserInfo.getUserId();
       if(servicePresenseStatus.size() > 0){
          usr.ServicePresenceStatusId = servicePresenseStatus[0].id;          
       }
       insert usr;     
       List<User> user= [SELECT id, status__c from user WHERE id =:usr.userId];
       if(servicePresenseStatus.size() > 0){
           System.assertEquals(user[0].status__c, servicePresenseStatus[0].MasterLabel);
       }
    }

}