/**================================================================
* Author        :  Appirio JDC
* Date          :  
* Description   :  
* 
* Update Date      Author            Description
* Jun 6, 2016      Padmesh Soni      S-421189
 ==================================================================*/
@isTest
private class NV_OrderItemTriggerHandlerTest {
    
        static Profile sysAdminProfile;
        static User testUser1;
        static Account testAccount;
        static Contract testContract;
        static Pricebook2 testPriceBook;
        static Order testOrder1, testOrder2;
    
	/*@testSetup*/public static void setup() {
		sysAdminProfile = NV_Utility.getProfileByName('System Administrator');
		testUser1 = NV_TestUtility.createUser('testuser_s1@mail.com', 'testuser_s1@mail.com',
														sysAdminProfile.Id, null, '1', true);
		Id accountRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('NV Customer').getRecordTypeId();
        //Map<String, Id> accountRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');
        testAccount = NV_TestUtility.createAccount('TestAccount',accountRecordTypeId, true);

        testContract = NV_TestUtility.createContract(testAccount.Id, true);
		testContract.Status = 'Activated';
		update testContract;

    	//Map<String, Id> productRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Product2');
    	Id productRecordTypeId = Schema.sObjectType.Product2.getRecordTypeInfosByName().get('Stryker Neurovascular').getRecordTypeId();
    	List<Product2> productData = new List<Product2>();
    	for(Integer i=1; i<=9;i++){
    		productData.add(NV_TestUtility.createProduct('NVProduct'+i, '00000'+i, productRecordTypeId, false));
    	}
    	insert productData;

    	testPriceBook = NV_TestUtility.createPricebook('NVPriceBook1', true, true);

    	//Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
    	Id orderRecordTypeId = Schema.sObjectType.Order.getRecordTypeInfosByName().get('NV Standard').getRecordTypeId();
		testOrder1 = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, orderRecordTypeId, false);
		testOrder1.Customer_PO__c = 'PO00001';
		testOrder1.EffectiveDate = Date.today();
		testOrder1.Date_Ordered__c = Datetime.now();
		testOrder1.Status = 'Booked';
		testOrder1.Pricebook2Id = testPriceBook.Id;
		insert testOrder1;

		testOrder2 = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, orderRecordTypeId, false);
		testOrder2.Customer_PO__c = 'PO00002';
		testOrder2.EffectiveDate = Date.today();
		testOrder2.Date_Ordered__c = Datetime.now();
		testOrder2.Status = 'Booked';
		testOrder2.Pricebook2Id = testPriceBook.Id;
		insert testOrder2;
    }

	@isTest static void testOrderItemTriggerBeforeInsertUpdate () {
	    setup();
		//Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
		//Contract testContract = [SELECT Id FROM Contract WHERE AccountId =: testAccount.Id LIMIT 1];
		Product2 testProduct1 = [SELECT Id FROM Product2 WHERE Name = 'NVProduct1' LIMIT 1];
		//Pricebook2 testPricebook1 = [SELECT Id From Pricebook2 WHERE Name = 'NVPriceBook1' LIMIT 1];
		//Order testOrder1 = [SELECT Id From Order WHERE Customer_PO__c = 'PO00001' LIMIT 1];

		PricebookEntry pbEntryForP1 = NV_TestUtility.addPricebookEntries(testPricebook.Id, testProduct1.Id, 1000, true, true);
		OrderItem testOrderItem1 = new OrderItem();
		testOrderItem1.OrderId = testOrder1.Id;
		testOrderItem1.PricebookEntryId = pbEntryForP1.Id;
		testOrderItem1.Quantity = 3;
		testOrderItem1.UnitPrice = pbEntryForP1.UnitPrice;
		Test.startTest();
		insert testOrderItem1;

		testOrderItem1 = [SELECT Id, Shipped_Date__c FROM OrderItem WHERE Id =: testOrderItem1.Id LIMIT 1];
		System.assertEquals(null, testOrderItem1.Shipped_Date__c);
		testOrderItem1.Status__c = 'Shipped';
		update testOrderItem1;
		testOrderItem1 = [SELECT Id, Shipped_Date__c FROM OrderItem WHERE Id =: testOrderItem1.Id LIMIT 1];
		System.assertEquals(Date.today(), testOrderItem1.Shipped_Date__c);

		testOrderItem1 = [SELECT Id, Delivered_Date__c FROM OrderItem WHERE Id =: testOrderItem1.Id LIMIT 1];
		System.assertEquals(null, testOrderItem1.Delivered_Date__c);
		testOrderItem1.Status__c = 'Delivered';
		update testOrderItem1;
		testOrderItem1 = [SELECT Id, Delivered_Date__c FROM OrderItem WHERE Id =: testOrderItem1.Id LIMIT 1];
		//System.assertEquals(Date.today(), testOrderItem1.Delivered_Date__c);
    Test.stopTest();

	}

	//Start modified for S-389737
	@isTest static void testOrderItemTriggerAfterInsertUpdate() {
	    setup();
		//Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
		//Contract testContract = [SELECT Id FROM Contract WHERE AccountId =: testAccount.Id LIMIT 1];
		Pricebook2 testPricebook1 = [SELECT Id From Pricebook2 WHERE Name = 'NVPriceBook1' LIMIT 1];
		//Order testOrder1 = [SELECT Id From Order WHERE Customer_PO__c = 'PO00001' LIMIT 1];
		//Order testOrder2 = [SELECT Id From Order WHERE Customer_PO__c = 'PO00002' LIMIT 1];

		//Change Account Owner
		//User testUser1 = [SELECT Id FROM User WHERE Username = 'testuser_s1@mail.com' LIMIT 1];
		Notification_Settings__c userSettings2 = NV_Utility.getNotificationSettingForUser(testUser1.Id);
		userSettings2.Shipped__c = true;
		userSettings2.Booked__c = true;
		userSettings2.Delivered__c = true;
		userSettings2.Backordered__c = true;
		userSettings2.All_Active_Orders__c = true;
		update userSettings2;

		testAccount.OwnerId = testUser1.Id;
		update testAccount;

		List<Product2> productData = [SELECT Id, Name From Product2 WHERE Name like 'NVProduct%' ORDER BY NAME ASC];
		Id stdPricebookId = Test.getStandardPricebookId();

		//Create Pricebook Entries
		List<PricebookEntry> stdPricebookEntryData = new List<PricebookEntry>();
		List<PricebookEntry> customPricebookEntryData = new List<PricebookEntry>();
		for(Integer i=0; i<9;i++){
			stdPricebookEntryData.add(NV_TestUtility.createPricebookEntry(stdPricebookId, productData[i].Id,
																			1000, true, false));
			customPricebookEntryData.add(NV_TestUtility.createPricebookEntry(testPricebook1.Id, productData[i].Id,
																			1000, true, false));
		}

		insert stdPricebookEntryData;
		insert customPricebookEntryData;

		//Create Notification Settings
		Notification_Settings__c userSettings = NV_NotificationSettingsController.getCurrentUserSettings();
		userSettings.Shipped__c = true;
		userSettings.Booked__c = true;
		userSettings.Delivered__c = true;
		userSettings.Backordered__c = true;
		NV_NotificationSettingsController.setCurrentUserSettings(userSettings);

		//Follow Order Record
		NV_OrderDetail.changeFollowRecord(testOrder1.Id, true);

		//Create new OrderItem
		Test.startTest();
			//Covering Case: Delivered
			OrderItem testOrderItem1 = new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Delivered',
														Order_Delivered__c = true,
														PricebookEntryId = customPricebookEntryData[0].Id,
														UnitPrice = customPricebookEntryData[0].UnitPrice,
														Tracking_Number__c = '000001');
			insert testOrderItem1;
			System.assertEquals('Shipped', [SELECT Id, Status From Order WHERE Id =: testOrder1.Id].Status);

			//Updating Item to cover Shipped Status
			testOrderItem1.Status__c = 'Shipped';
			testOrderItem1.Order_Delivered__c = false;
			update testOrderItem1;
			System.assertEquals('Shipped', [SELECT Id, Status From Order WHERE Id =: testOrder1.Id].Status);

			/*OrderItem testOrderItem2 = new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Booked',
														PricebookEntryId = customPricebookEntryData[0].Id,
														UnitPrice = customPricebookEntryData[0].UnitPrice);
			insert testOrderItem2;
			System.assertEquals('Partial Ship', [SELECT Id, Status From Order WHERE Id =: testOrder1.Id].Status);*/

			//Updating Item to cover Backordered Status
			testOrderItem1.Status__c = 'Backordered';
			testOrderItem1.Hold_Description__c = null;
			update testOrderItem1;

			//Updating Item to cover Not Backordered Status
			testOrderItem1.Status__c = 'Booked';
			update testOrderItem1;
		Test.stopTest();
		/*List<OrderItem> orderItems = new List<OrderItem>();
		orderItems.add(new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Delivered',
										PricebookEntryId = customPricebookEntryData[1].Id,
										UnitPrice = customPricebookEntryData[1].UnitPrice,
										Tracking_Number__c = '000001'));
		orderItems.add(new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Shipped',
										PricebookEntryId = customPricebookEntryData[2].Id,
										UnitPrice = customPricebookEntryData[2].UnitPrice));
		orderItems.add(new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Shipped',
										PricebookEntryId = customPricebookEntryData[2].Id,
										UnitPrice = customPricebookEntryData[2].UnitPrice));
		orderItems.add(new OrderItem(OrderId = testOrder2.Id, Quantity = 3, Status__c = 'On Hold',
										PricebookEntryId = customPricebookEntryData[3].Id,
										UnitPrice = customPricebookEntryData[3].UnitPrice,
										Hold_Description__c = 'On Hold'));

		insert orderItems;
		System.assertEquals('Partial Ship', [SELECT Id, Status From Order WHERE Id =: testOrder1.Id].Status);*/
	} // end S-389737


	//Start added for S-389737
	@isTest static void testOrderItemTriggerAfterInsertUpdate2() {
	    setup();
		//Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
		//Contract testContract = [SELECT Id FROM Contract WHERE AccountId =: testAccount.Id LIMIT 1];
		//Pricebook2 testPricebook1 = [SELECT Id From Pricebook2 WHERE Name = 'NVPriceBook1' LIMIT 1];
		//Order testOrder1 = [SELECT Id From Order WHERE Customer_PO__c = 'PO00001' LIMIT 1];
		//Order testOrder2 = [SELECT Id From Order WHERE Customer_PO__c = 'PO00002' LIMIT 1];

		//Change Account Owner
		//User testUser1 = [SELECT Id FROM User WHERE Username = 'testuser_s1@mail.com' LIMIT 1];
		Notification_Settings__c userSettings2 = NV_Utility.getNotificationSettingForUser(testUser1.Id);
		userSettings2.Shipped__c = true;
		userSettings2.Booked__c = true;
		userSettings2.Delivered__c = true;
		userSettings2.Backordered__c = true;
		userSettings2.All_Active_Orders__c = true;
		update userSettings2;

		testAccount.OwnerId = testUser1.Id;
		update testAccount;

		List<Product2> productData = [SELECT Id, Name From Product2 WHERE Name like 'NVProduct%' ORDER BY NAME ASC];
		Id stdPricebookId = Test.getStandardPricebookId();

		//Create Pricebook Entries
		List<PricebookEntry> stdPricebookEntryData = new List<PricebookEntry>();
		List<PricebookEntry> customPricebookEntryData = new List<PricebookEntry>();
		for(Integer i=0; i<9;i++){
			stdPricebookEntryData.add(NV_TestUtility.createPricebookEntry(stdPricebookId, productData[i].Id,
																			1000, true, false));
			customPricebookEntryData.add(NV_TestUtility.createPricebookEntry(testPricebook.Id, productData[i].Id,
																			1000, true, false));
		}

		insert stdPricebookEntryData;
		insert customPricebookEntryData;

		//Create Notification Settings
		Notification_Settings__c userSettings = NV_NotificationSettingsController.getCurrentUserSettings();
		userSettings.Shipped__c = true;
		userSettings.Booked__c = true;
		userSettings.Delivered__c = true;
		userSettings.Backordered__c = true;
		NV_NotificationSettingsController.setCurrentUserSettings(userSettings);

		//Follow Order Record
		NV_OrderDetail.changeFollowRecord(testOrder1.Id, true);

		//Create new OrderItem

		Test.startTest();
			OrderItem testOrderItem2 = new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Booked',
														PricebookEntryId = customPricebookEntryData[0].Id,
														UnitPrice = customPricebookEntryData[0].UnitPrice);
			insert testOrderItem2;
			System.assertEquals('Booked', [SELECT Id, Status From Order WHERE Id =: testOrder1.Id].Status);


			List<OrderItem> orderItems = new List<OrderItem>();
			orderItems.add(new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Delivered',
											PricebookEntryId = customPricebookEntryData[1].Id,
											UnitPrice = customPricebookEntryData[1].UnitPrice,
											Tracking_Number__c = '000001'));
			orderItems.add(new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Shipped',
											PricebookEntryId = customPricebookEntryData[2].Id,
											UnitPrice = customPricebookEntryData[2].UnitPrice));
			orderItems.add(new OrderItem(OrderId = testOrder1.Id, Quantity = 3, Status__c = 'Shipped',
											PricebookEntryId = customPricebookEntryData[2].Id,
											UnitPrice = customPricebookEntryData[2].UnitPrice));
			orderItems.add(new OrderItem(OrderId = testOrder2.Id, Quantity = 3, Status__c = 'On Hold',
											PricebookEntryId = customPricebookEntryData[3].Id,
											UnitPrice = customPricebookEntryData[3].UnitPrice,
											Hold_Description__c = 'On Hold'));

			insert orderItems;
		
		System.assertEquals('Partial Ship', [SELECT Id, Status From Order WHERE Id =: testOrder1.Id].Status);
		  //Added by Shubham for Story S-447620 #Start
        if(orderItems.size() > 0){
            delete orderItems;
        }
        //Added by Shubham for Story S-447620 #End
	Test.stopTest();
	} //end S-389737

}