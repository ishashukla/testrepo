//
// (c) Appirio, Inc.
//
//  Batch class to prepare the String which will be read by Informatica for BOTW integration.
//
//  This Batch class will calculate the string for Contract Line Item
//
// August 13, 2015 Sunil Original
// Revision History:
//     2015-08-13   Sunil       Original
//     2015-08-13   H.Whitacre  Updates to clear omitted sections
//     2015-08-17   H.Whitacre  Add integration timestamp, botw field numbering
//     2015-08-18   H.Whitacre  Apply .trim to address lines to remove \r if exist due to CrLf
//     2015-08-19   H.Whitacre  Add more mapped fields for "Depr" fields based on today decisions.
//     2015-08-20   H.Whitacre  Add more Tax fields based on today decisions.
//     2015-08-21   H.Whitacre  Add Residual_Amount__c (#217) and Equip Cost (#202).
//     2015-08-26   H.Whitacre  Remove the final backslash so that Informatica can use slash as delimiter. 
//     2015-08-27   H.Whitacre  Revise mapping for Tax Code fields (T-430400).
//     2015-09-09   H.Whitacre  Revise logic for blanking certain sections conditionally (T-433224).
//     2015-09-11   H.Whitacre  Logic to strip backslash from any data value (T-433745).
//     2016-02-08   H.Whitacre  I-201746: Fix several issues found during BOTW testing.
//     2016-02-19   H.Whitacre  I-203460: Revise mapping of OPER.LS depreciation fields #163-#165.
//     2016-03-03   H.Whitacre  I-206338: Only add Upfront Tax to fld#202 when Upfront Tax Financed true.
//     
// Use below code to execute this batch one time:
//Database.executeBatch(new Flex_BatchBOTWIntegrationContractLine());

global class Flex_BatchBOTWIntegrationContractLine implements Database.Batchable<SObject> {
  private String result = '';
  private Boolean isOperatingLease = false;

  // This variable will be used to keep selected Contract Ids for which Contract Lines will be processed.
  global Set<Id>executedContractIds = new Set<Id>();

  global final String query = 'SELECT Id,'
                           + ' Name,'
                           + ' Location_Account__r.ShippingStreet,'
                           + ' Location_Account__r.ShippingCity,'
                           + ' Location_Account__r.ShippingStateCode,'
                           + ' Location_Account__r.ShippingPostalCode,'
                           + ' Flex_Contract__r.End_Of_Term__c,'
                           + ' Flex_Contract__r.Commence_Date__c,'
                           + ' Flex_Contract__r.Upfront_Tax_Financed__c,'
                           + ' Flex_Contract__r.BOTW_Tax_Code__c,'
                           + ' Flex_Contract__r.Lease_Type__c,'
                           + ' Equipment_Sell_Price__c,'
                           + ' Service_Sell_Price__c,'
                           + ' Freight_Sell_Price__c,'
                           + ' Equipment_Funded_Amt__c,'
                           + ' Service_Funded_Amt__c,'
                           + ' Freight_Funded_Amt__c,'
                           + ' Blind_Discount_Amt__c,'
                           + ' City_Tax_Amt__c,'
                           + ' City_Tax_Pct__c,'
                           + ' County_Tax_Amt__c,'
                           + ' County_Tax_Pct__c,'
                           + ' State_Tax_Amt__c,'
                           + ' State_Tax_Pct__c,'
                           + ' Tax_Basis__c,'
                           + ' Residual_Amount__c,'
                           + ' Product_Name__c,'
                           + ' Product_Catalog_Number__c'
                           + ' FROM Flex_Contract_line__c'
                           + ' WHERE Flex_Contract__c IN :executedContractIds';
                           //City_Tax_Code__c, County_Tax_Code__c, State_Tax_Code__c,
                           //Misc_City_Tax_Code__c, Misc_County_Tax_Code__c, Misc_State_Tax_Code__c,'


  global Database.QueryLocator start(Database.BatchableContext bc){
    return Database.getQueryLocator(query);
  }


  // Execute block Query locator will send result to this method.
  global void execute(Database.BatchableContext bc, List<sObject> scope) {
    List<Flex_Contract_Line__c> lstContractLines  = (List<Flex_Contract_Line__c>)scope;
    System.debug('@@@size' + lstContractLines.size());

    for(Flex_Contract_Line__c line :lstContractLines){
        line.BOTW_Integration_String__c = calculateString(line);
        line.BOTW_Integration_Status__c = 'String Ready';
        line.BOTW_Integration_Timestamp__c = System.Datetime.now();
    }

    Database.update(lstContractLines, false);
  }

  global void finish(Database.BatchableContext bc) {

  }

  // Helper method to calculate String.
  private String calculateString(Flex_Contract_Line__c line){
    
    // Initialize vars
    result = '';
    isOperatingLease = (line.Flex_Contract__r.Lease_Type__c != null && line.Flex_Contract__r.Lease_Type__c == 'OL');

    // sellPrice is used for field #164 (OPER.LS.BASIS) and #200 (EQUIP.LIST.PRICE)
    Decimal sellprice = (line.Equipment_Sell_Price__c == null ? 0.00 : line.Equipment_Sell_Price__c)
                        + (line.Service_Sell_Price__c == null ? 0.00 : line.Service_Sell_Price__c)
                        + (line.Freight_Sell_Price__c == null ? 0.00 : line.Freight_Sell_Price__c);
    // residualAmt is used for field #165 (OPER.LS.SALVAGE) and #217 (RESID.AMT)
    Decimal residualAmt = line.Residual_Amount__c == null ? 0.00 : line.Residual_Amount__c;
    
    // 160 // ASSET SECTION - begin section
    addValue('AS');
    
    // 161-165 // Operating Lease fields
    // Write the OPER.LS fields (161-165) only for Operating Leases (lease type is OL)
    // otherwise leave all 5 fields blank
    if(isOperatingLease==true){
      // Lease type is OL, write the 5 fields
      //161 //OPER.LS.METHOD - S
      addValue('S');
      //162//OPER.LS.BEGIN.DATE - Flex_Contract__r.Commence_Date__c as "MM/yyyy"
      addValue( line.Flex_Contract__r.Commence_Date__c == null ? '' : datetime.newInstance(line.Flex_Contract__r.Commence_Date__c, Time.newInstance(0,0,0,0)).format('MM/yyyy') );
      //163 //OPER.LS.LIM - Life in months always "60" (I-203460)
      addValue('60');
      //164 //OPER.LS.BASIS - sum of Sell Prices (I-203460)
      addValue(String.valueOf(sellprice.setScale(2)));
      //165 //OPER.LS.SALVAGE - Residual_Amount__c (I-203460)
      addValue(line.Residual_Amount__c == null ? '' : String.valueOf(residualAmt.setScale(2)));
    }
    else {
      // Lease Type is not OL, write 5 blanks
      for(Integer i = 0; i<5; i++){
        addValue('');
      }
    }
    
    //166 //EQUIP.CODE - BLANK
    addValue('');
    //167 //EQUIP.DESC - Product_Name__c trim to 50 chars
    String productName = (line.Product_Name__c == null ? '' : line.Product_Name__c);
    if(productName.length() > 50) productName = productName.left(50);
    addValue(productName);
    //168 //MODEL - Product_Catalog_Number__c
    addValue(line.Product_Catalog_Number__c);
    //169 //SERIAL.NUMBER - BLANK
    addValue('');

    //170 //EQUIP.ADDR1 - Location_Account__r.ShippingStreet [line 1]
    String[] street = String.isBlank(line.Location_Account__r.ShippingStreet)? new String[] {''}:line.Location_Account__r.ShippingStreet.split('\n');
    if(street.size() < 2) street.add('');
    street[0] = street[0].trim();
    street[1] = street[1].trim();
    addValue(street[0]);

    //171 //EQUIP.ADDR2 - ShippingStreet [line 2]
    addValue(street[1]);

    //172 //EQUIP.CITY - Location_Account__r.ShippingCity
    addValue(line.Location_Account__r.ShippingCity);
    //173 //EQUIP.STATE - Location_Account__r.ShippingStateCode
    addValue(line.Location_Account__r.ShippingStateCode);
    //174 //EQUIP.ZIP - Location_Account__r.ShippingPostalCode
    addValue(line.Location_Account__r.ShippingPostalCode);


    //175 //STATE.TAX.CODE - Flex_Contract__r.BOTW_Tax_Code__c
    addValue(line.Flex_Contract__r.BOTW_Tax_Code__c);
    //176 //CNTY.TAX.CODE - Flex_Contract__r.BOTW_Tax_Code__c
    addValue(line.Flex_Contract__r.BOTW_Tax_Code__c);
    //177 //CITY.TAX.CODE - Flex_Contract__r.BOTW_Tax_Code__c
    addValue(line.Flex_Contract__r.BOTW_Tax_Code__c);
    //178 //A.MISC.STATE.TAX.CODE - Flex_Contract__r.BOTW_Tax_Code__c
    addValue(line.Flex_Contract__r.BOTW_Tax_Code__c);
    //179 //A.MISC.CNTY.TAX.CODE - Flex_Contract__r.BOTW_Tax_Code__c
    addValue(line.Flex_Contract__r.BOTW_Tax_Code__c);
    //180 //A.MISC.CITY.TAX.CODE - Flex_Contract__r.BOTW_Tax_Code__c
    addValue(line.Flex_Contract__r.BOTW_Tax_Code__c);

    // 181-185 // *.TAX.PCT  - Tax Percent fields
    addTaxPctValues(line);
    
    // 186-198 //UT.* - Upfront Tax fields
    addUpfrontTaxValues(line);

    //199 //A.ORIG.COST - Equipment_Funded_Amt__c + Service_Funded_Amt__c + Freight_Funded_Amt__c
    Decimal fundedAmt = (line.Equipment_Funded_Amt__c == null ? 0.00 : line.Equipment_Funded_Amt__c)
                        + (line.Service_Funded_Amt__c == null ? 0.00 : line.Service_Funded_Amt__c)
                        + (line.Freight_Funded_Amt__c == null ? 0.00 : line.Freight_Funded_Amt__c);
    addValue(String.valueOf(fundedAmt.setScale(2)));

    //200 //EQUIP.LIST.PRICE - Equipment_Sell_Price__c + Service_Sell_Price__c + Freight_Sell_Price__c
    addValue(String.valueOf(sellprice.setScale(2)));

    //201 //EQUIP.DISCOUNT - Blind_Discount_Amt__c
    Decimal blindDiscount = line.Blind_Discount_Amt__c == null ? 0 : line.Blind_Discount_Amt__c;
    addValue(String.valueOf(blindDiscount.setScale(2)));

    //202 //EQUIP.COST = ORIG.COST + UpfrontTax = #199 + #193 + #194 + #196
    //  I-206338 only add upfront tax if UT financed
    Decimal upfrontTax = (line.State_Tax_Amt__c == null ? 0.00 : line.State_Tax_Amt__c)
                         + (line.County_Tax_Amt__c == null ? 0.00 : line.County_Tax_Amt__c)
                         + (line.City_Tax_Amt__c == null ? 0.00 : line.City_Tax_Amt__c);
    Decimal equipCost = fundedAmt + (line.Flex_Contract__r.Upfront_Tax_Financed__c == true ? upfrontTax : 0.00);
    addValue(String.valueOf((equipCost).setScale(2)));


    //203 //BEG.DEPR.DATE - Depreciation fields - leave blank
    //204 //FED.CONVENTION - blank
    //205 //SAFEHAR.FEDMTHD - blank
    //206 //DEPR.BASIS - blank
    //207 //FED.LIM - blank
    //208 //FED.CALC.BASIS - blank
    //209 //CALC.AMT - blank
    //210 //ADS.CLASS.LIM - blank
    //211 //AMT.DEPR.BASIS - blank
    //212 //ST.BEG.DEPR.DATE - blank
    //213 //ST.METHOD - blank
    //214 //ST.DEPR.BASIS - blank
    //215 //ST.LIM - blank
    //216 //ST.CALC.BASIS - blank
    for(Integer i=0; i<14; i++){
      addValue('');
    }
    //217 //RESID.AMT - Residual_Amount__c - provide value only if FMV EndOfTerm and non-OL lease types
    if(isOperatingLease==false && line.Flex_Contract__r.End_Of_Term__c=='FMV'){
      addValue(String.valueOf(residualAmt.setScale(2)));
    }
    else{
      addValue('');
    }

    //218//PUR.OPTION - Flex_Contract__r.End_Of_Term__c $1 out - 01, FMV - 61, No Purchase Option - 00 else blank.
    Map<String,String> mapEotToPurOption = new Map<String,String>();
    mapEotToPurOption.put('$1 out', '01');
    mapEotToPurOption.put('FMV', '61');
    mapEotToPurOption.put('No Purchase Option', '00');
    addValue(mapEotToPurOption.get(line.Flex_Contract__r.End_Of_Term__c));

    //219 //AUM.TABLE2 - (unused,leave blank)
    addValue('');

    //220 //AS.RECOURSE.CODE - (unused,leave blank)
    // ** Leave out the backslash for this final field in this string
    // ** so that Informatica can add a backslash as a delimiter
    addValueWithoutSlash('');
      

    System.debug('@@@result' + result);
    return result;
  }
    
    
  // Helper for TaxPct fields (#181-185)
  private void addTaxPctValues(Flex_Contract_Line__c line){
    // Write the *.TAX.PCT fields only if tax code is NOT 00 or 31-99
    Set<String> exclusionCodes = new Set<String> {'0', '00', '000'
          , '31', '96', '97', '99', '031', '096', '097', '099'};
    
    if(String.isBlank(line.Flex_Contract__r.BOTW_Tax_Code__c)
           || exclusionCodes.contains(line.Flex_Contract__r.BOTW_Tax_Code__c) ) {
       // Leave the TaxPct fields blank for the listed codes
       for(Integer i=0; i<5; i++){
         addValue('');
       }
    }
    else {
      //181 //STATE.TAX.PCT - State_Tax_Pct__c
      addValue(String.valueOf(line.State_Tax_Pct__c));
      //182 //CNTY.TAX.PCT - County_Tax_Pct__c
      addValue(String.valueOf(line.County_Tax_Pct__c));
      //183 //CITY.TAX.PCT - City_Tax_Pct__c
      addValue(String.valueOf(line.City_Tax_Pct__c));
      //184 //TCNTY.TAX.PCT - (unused,leave blank)
      addValue('');
      //185 //TCITY.TAX.PCT - (unused,leave blank)
      addValue('');
    }
  }


  // Helper for UT Upfront Tax fields (#168-198)
  private void addUpfrontTaxValues(Flex_Contract_Line__c line){
    // Write the UT.* fields (186-198) only if Tax Code is 99 and Tax Amounts are non-zero
    // otherwise leave all 13 fields blank
    Boolean hasTaxAmt = (line.State_Tax_Amt__c != null  && line.State_Tax_Amt__c > 0.00)
                     || (line.County_Tax_Amt__c != null && line.County_Tax_Amt__c > 0.00)
                     || (line.City_Tax_Amt__c != null   && line.City_Tax_Amt__c > 0.00);
    Boolean hasCodeForUT = ( (line.Flex_Contract__r.BOTW_Tax_Code__c == '99') || (line.Flex_Contract__r.BOTW_Tax_Code__c == '099') );
    if(hasTaxAmt && hasCodeForUT){
      //186 //UT.ZIP.CODE - Location_Account__r.ShippingPostalCode
      addValue(line.Location_Account__r.ShippingPostalCode);
      //187 //UT.STATE.CODE - geo code - BLANK
      addValue('');
      //188 //UT.CNTY.CODE - geo code - BLANK
      addValue('');
      //189 //UT.CITY.CODE - geo code - BLANK
      addValue('');
      //190 //UT.FINANCED - Flex_Contract__r.Upfront_Tax_Financed__c - bool as "0" or "1"
      addValue(line.Flex_Contract__r.Upfront_Tax_Financed__c ? '1' : '0');
      //191 //UT.RESP.CODE - BLANK
      addValue('');
      //192 //UT.BASIS - Tax_Basis__c
      addValue(String.valueOf(line.Tax_Basis__c));
      //193 //UT.STATE.TAX.AMT - State_Tax_Amt__c
      addValue(String.valueOf(line.State_Tax_Amt__c));
      //194 //UT.CNTY.TAX.AMT - County_Tax_Amt__c
      addValue(String.valueOf(line.County_Tax_Amt__c));
      //195 //UT.TCNTY.TAX.AMT - BLANK
      addValue('');
      //196 //UT.CITY.TAX.AMT - City_Tax_Amt__c
      addValue(String.valueOf(line.City_Tax_Amt__c));
      //197 //UT.TCITY.TAX.AMT - BLANK
      addValue('');
      //198 //UT.TAX.CODE - Constant "100"
      addValue('100');
    }
    else{
      // 186-198 // When has no upfront tax, leave all blank
      for(Integer i=0; i<13; i++){
        addValue('');
      }
    }
  }

  
  // Helper method to add data values with field terminator "\"
  // 2015-09-11  H.Whitacre  T-433745  Replace "\" with "/" if exist within data value
  private void addValue(String s){
    if(String.isBlank(s)){
      result += '\\';
    }
    else{
      result += s.replace('\\', '/') + '\\';
    }
  }
  
  // Helper method to add data values WITHOUT field terminator
  private void addValueWithoutSlash(String s){
    result += (String.isBlank(s) ? '' : s.replace('\\', '/'));
  }
    
}