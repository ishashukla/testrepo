// 
// (c) 2016 Appirio, Inc.
//
// Name : COMM_ContactOpptyCreationController 
// Used to create new junction object between contact and opportunity
// Adding some search and create contact functionality.
//
// 15th March 2016     Kirti Agarwal      Original(T-477520)
//
//Modified By         Modified Date       Purpose
//Meghna Vijay        23 May, 2016        To pre-populate Site visit Attendees and site visit travelers
//                                        and added validation error for airline ticket required.
//                                        
public class COMM_ContactOpptyCreationController {
  public Related_Contact__c junctionContactOpp {get;set;}
  public String oppId {get;set;}
  public String  contactName{get;set;}
  public String recordType{get;set;}
  public RecordType recordTypeName{get;set;}
  public Contact con {get;set;}
  public List<sObject> conList {get;set;}
  public String txtContact{get;set;}
  public Opportunity opp{get;set;}
  public List<sObject> oppList{get;set;}
  
  public Site_Visit__c site{get;set;}
  public List<sObject> siteList{get;set;}
  
  public Project_plan__c plan{get;set;}
  public List<sObject> planList{get;set;}
  
  
  public COMM_ContactOpptyCreationController(ApexPages.StandardController cntrl) {
    junctionContactOpp = (Related_Contact__c) cntrl.getRecord();

    System.debug('First Debug             '+ junctionContactOpp);
    oppId = ApexPages.currentPage().getParameters().get('retURL');
    recordType = ApexPages.currentPage().getParameters().get('RecordType');
    recordTypeName=[Select Id, Name FROM recordType Where Id =: recordType];
    List<COMM_Constant_Setting__c> custmSettingList = [SELECT Site_Visit_Attendee_Field_Id__c, Site_Visit_Traveler_Field_Id__c, Project_Look_up_ID_on_Contact_Ass__c FROM COMM_Constant_Setting__c];
    junctionContactOpp.Site_Visit_Attendees__c = ApexPages.currentPage().getParameters().get(custmSettingList.get(0).Site_Visit_Attendee_Field_Id__c);
    junctionContactOpp.Project_Plan__c = ApexPages.currentPage().getParameters().get('CF'+custmSettingList.get(0).Project_Look_up_ID_on_Contact_Ass__c+'_lkid');
    //Added by Rahul Aeran for SF1
  // if(!String.isBlank(UserInfo.getUiThemeDisplayed()) && UserInfo.getUiThemeDisplayed().toLowerCase() == Constants.THEME_SF1 ){
      con = new Contact(); 
        conList = new sObject[]{con};
       
        if(junctionContactOpp.Site_Visit_Attendees__c !=null) {
          site = new Site_Visit__c(Id = junctionContactOpp.Site_Visit_Attendees__c);
        } else {
          site = new Site_Visit__c();
        }
         siteList = new sObject[]{site};
         opp = new Opportunity();
         
        oppList = new sObject[]{opp};
        if(junctionContactOpp.Project_Plan__c !=null) {
          plan = new Project_plan__c(Id = junctionContactOpp.Project_Plan__c); 
        } else {
          plan = new Project_plan__c();
        }
        planList = new sObject[]{plan};
     
   //}
   
  }

  //================================================================      
  // Name: saveJunctionObject
  // Description: Used to insert junction record
  // Created Date: 15th March 2016 
  // Created By: Kirti Agarwal (Appirio)
  //==================================================================
  public PageReference saveJunctionObject() {
    system.debug('Enter save                 ' + junctionContactOpp);
   
    if (contactName != '') {
      system.debug('Enter save                 ' + junctionContactOpp);
      junctionContactOpp.RecordTypeID = recordType;
      junctionContactOpp.Contact__c = contactName;
      
      
      //We need to handle salesforce1 scenario here as we are providing custom lookups here
      if(!String.isBlank(UserInfo.getUiThemeDisplayed()) && UserInfo.getUiThemeDisplayed().toLowerCase() == Constants.THEME_SF1 ){
        if(conList.get(0).Id != null){
            junctionContactOpp.Contact__c = conList.get(0).Id;
        } 
        if(oppList.get(0).Id != null){
            junctionContactOpp.Opportunity__c = oppList.get(0).Id;
        }
        if(siteList.get(0).Id != null){
            junctionContactOpp.Site_Visit_Attendees__c = siteList.get(0).Id;
        }
        if(planList.get(0).Id != null){
            junctionContactOpp.Project_plan__c = planList.get(0).Id;
        }
        System.debug('Junction object plan:'+junctionContactOpp.Project_plan__c);
        System.debug('Junction object Contact__c:'+junctionContactOpp.Contact__c);
        System.debug('Junction object Opportunity__c:'+junctionContactOpp.Opportunity__c);
        System.debug('Junction object Site_Visit_Attendees__c:'+junctionContactOpp.Site_Visit_Attendees__c);
      }
      
    }
  try {
      if(junctionContactOpp.Contact__c != NULL) {
      /*    Contact con = [Select Phone,firstname,lastname,email,Account.Name, MobilePhone from Contact where id=:  junctionContactOpp.Contact__c];
          junctionContactOpp.Contact_Phone__c = con.MobilePhone;
          junctionContactOpp.first_name__c = con.firstname;
          junctionContactOpp.last_name__c = con.lastname;
          junctionContactOpp.email_Id__c = con.email;
          junctionContactOpp.Company_name__c= con.Account.Name;
          junctionContactOpp.Work_Phone__c = con.Phone; */
      } 
      insert junctionContactOpp;
      PageReference redirectPage = new PageReference('/' + junctionContactOpp.id);
      redirectPage.setRedirect(true);
      return redirectPage;
    } catch (exception e) {
      System.debug(e);
      if (e.getMessage().contains('REQUIRED_FIELD_MISSING') || e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
        return null;
      }
    }
    return cancel();
  }

  //================================================================      
  // Name: cancel
  // Description: Used to redirect user to opportunity
  // for sales manager profiles
  // Created Date: 15th March 2016 
  // Created By: Kirti Agarwal (Appirio)
  //==================================================================
  public PageReference cancel() {
    PageReference redirectPage;
    if (oppId != '' && oppId != null) {
      redirectPage = new PageReference(oppId);
    } else if (junctionContactOpp.id != null) {
      redirectPage = new PageReference('/' + junctionContactOpp.id);
    } else if(junctionContactOpp.Site_Visit_Attendees__c != null) {
      redirectPage = new PageReference('/'+junctionContactOpp.Site_Visit_Attendees__c);
    }
    else {
      redirectPage = new PageReference('/home/home.jsp');
    }
    redirectPage.setRedirect(true);
    return redirectPage;
  }
  
  
}