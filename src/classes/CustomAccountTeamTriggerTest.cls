/**================================================================      
 * Appirio, Inc
 * Name: CustomAccountTeamTriggerTest
 * Description: Test Class for CustomAccountTeamTrigger class.
 * Created Date: [04/11/2016]
 * Created By: [Isha Shukla] (Appirio)
 * Date Modified      Modified By      Description of the update
 * Apr 11,2016        Isha Shukla      Original
 ==================================================================*/
@isTest
private class CustomAccountTeamTriggerTest {
    Static List<Custom_Account_Team__c> customAccTeamList;
    //Static List<Multi_BU_Assignment__c> MultiBUAssignmentList;
    Static List<Mancode__c> mancodeList;
    Static List<User> userList;
    Static List<System__c> sys;
    Static List<Install_Base__c> ibList;
    @isTest static void testNewRecord() {
        createData();
        Test.startTest();
        system.runAs(userList[0]){
            insert customAccTeamList;
        }
        
        Test.stopTest();
        System.assertEquals(3,[SELECT Name From Custom_Account_Team__c].size());
    }
    @isTest static void testUpdateIsDeletedTrue() {
        createData();
        system.runAs(userList[0]){
            insert customAccTeamList;
            customAccTeamList[0].IsDeleted__c = True;
            Test.startTest();
            update customAccTeamList;
            Test.stopTest();
        }
        System.assertEquals(2,[SELECT Name From Custom_Account_Team__c WHERE IsDeleted__c = False].size());
    }
    @isTest static void testUpdateIsDeletedFalse() {
        createData();
        system.runAs(userList[0]){
            customAccTeamList[0].IsDeleted__c = True;
            insert customAccTeamList;
            customAccTeamList[0].IsDeleted__c = False;
            Test.startTest();
            update customAccTeamList;
            Test.stopTest();
        }
        System.assertEquals(3,[SELECT Name From Custom_Account_Team__c WHERE IsDeleted__c = False].size());
    }
    @isTest static void testUpdateTeamMemberRole() {
        createData();
        system.runAs(userList[0]){
            insert customAccTeamList;
            customAccTeamList[0].Team_Member_Role__c = 'IVS';
            Test.startTest();
            update customAccTeamList;
            Test.stopTest();
        }
        System.assertEquals(1,[SELECT Name From Custom_Account_Team__c WHERE Team_Member_Role__c = 'IVS'].size());
    }
 
    @isTest static void testAccountAccessLevel() {
        createData();
        system.runAs(userList[0]){
            insert customAccTeamList;
            customAccTeamList[0].Account_Access_Level__c = 'Private';
            Test.startTest();
            update customAccTeamList;
            Test.stopTest();
        }
        System.assertEquals(1,[SELECT Name From Custom_Account_Team__c WHERE Account_Access_Level__c = 'Private'].size());
    }
    
    public static void createData() {
        userList = TestUtils.createUser(2, 'System Administrator',false);
        userList[0].Division = 'NSE';
        userList[1].Division = 'NSE';
        insert userList;
        system.debug('userList>>>'+userList);
       System.runAs(userList[0]){
            List<Account> accountList = TestUtils.createAccount(1,True);
            customAccTeamList = TestUtils.createCustomAccountTeam(3,accountList[0].Id,userList[0].Id,'NSE',false,false);
            sys = TestUtils.createSystem(1, 'systemName', 'NSE', true);
            ibList = TestUtils.createInstallBase(1, accountList[0].Id, userList[1].Id, sys[0].Id, true);
            mancodeList = TestUtils.createMancode(1, userList[0].Id, userList[1].Id, false);
            mancodeList[0].Business_Unit__c = 'NSE';
            
            //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=userList[0].Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
            //insert terr;
            //mancodeList[0].Territory__c = terr.Id;
            insert mancodeList;
            List<Multi_BU_Assignment__c > multiBuList = TestUtils.createMultiBUassignment(1,userList[0].Id,userList[1].Id,true);
        }
        system.debug('customAccTeamList>>>'+customAccTeamList);
        system.debug('MultiBUAssignmentList>>>'+mancodeList);
    }
}