/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 29, 2013
 Purpose       :  Without Sharing Class having methods called from With Sharing
                  Classes. 
 Reference     :  Task T-216318
 Modifications :  Jan 02, 2014      Task T-226840      Sonal Shrivastava                
*******************************************************************************/
public without sharing class WithoutSharingUtility {	
	
	public static List<FeedComment> fetchPostComments(Set<String> feedItemIdSet){
		List<FeedComment> lstFeedComment = new List<FeedComment>();
		if(feedItemIdSet != null && feedItemIdSet.size() > 0){
			for(FeedComment comment : [SELECT Id, CreatedById, CreatedBy.Name, CreatedDate, CommentBody, FeedItemId, ParentId, CommentType,  
			                                  RelatedRecordId
	                               FROM FeedComment
	                               WHERE FeedItemId IN :feedItemIdSet]){
				lstFeedComment.add(comment);
			} 
		}
		return lstFeedComment;
	}
	
	
	public static Map<String, List<ConnectApi.Comment>> fetchPostReplies(Map<String, String> mapChatterPost_IntelId){
		Map<String, List<ConnectApi.Comment>> mapIntelId_commentList = new Map<String, List<ConnectApi.Comment>>();
		try{
			//Loop for each intel
			for(String feedItemId : mapChatterPost_IntelId.keySet()){
				
				ConnectApi.CommentPage comPage;
				if(!Test.isRunningTest()){
					comPage = ConnectApi.ChatterFeeds.getCommentsForFeedItem(null, feedItemId);					
				}	
				else {
					comPage = new ConnectApi.CommentPage();
				  ConnectApi.Comment com = new ConnectApi.Comment();
					comPage.comments = new list<ConnectApi.Comment>();
					comPage.comments.add(com);
					comPage.total = 1;									
				}				
				if(comPage.total > 0){
	        for(ConnectApi.Comment comment : comPage.comments){
	          String intelId = mapChatterPost_IntelId.get(feedItemId); 
	          if(!mapIntelId_commentList.containsKey(intelId)){
	            mapIntelId_commentList.put(intelId, new List<ConnectApi.Comment>());
	          }
	          mapIntelId_commentList.get(intelId).add(comment);
	        }         
	      }
			}
		}catch(Exception ex){
      throw ex;
      return null;
    }
		return mapIntelId_commentList;
	}
	
	
	
	public static Map<String, List<FeedComment>> fetchMapIntelPostComments(String userId){
		Map<String, List<FeedComment>> mapIntelId_lstComment = new Map<String, List<FeedComment>>();
		for(FeedComment comment : [SELECT Id, CreatedById, CreatedBy.Name, CreatedDate, CommentBody, FeedItemId, ParentId, CommentType  
                               FROM FeedComment
                               WHERE CreatedById = :userId]){
			//Check if comment is on Intel record
			if(String.valueOf(comment.ParentId).startsWith(Constants.INTEL_PREFIX)){				
				//Add comment to a map with key as IntelId and value as list of comment
				if(!mapIntelId_lstComment.containsKey(comment.ParentId)){
					mapIntelId_lstComment.put(comment.ParentId, new List<FeedComment>());
				}
				mapIntelId_lstComment.get(comment.ParentId).add(comment);
			}
		}
		return mapIntelId_lstComment;
	}
	
	public static List<FeedComment> fetchComment(String commentId){
		return [ SELECT Id, CommentBody, CreatedById, CreatedDate, CommentType 
	           FROM FeedComment 
	           WHERE Id = :commentId];
	}
  
  public static List<Intel__c> fetchCommentIntels(Map<String, Intel__c> mapIntelId_Intel, String timeStamp){
    List<Intel__c> intelList = new List<Intel__c>();
    Set<String> intelSet = new Set<String>(); 
    String currentUserId = UserInfo.getUserId();
    String timestampStr = timeStamp.substring(0, 4) + '-' + 
                          timeStamp.substring(5, 7) + '-' +
                          timeStamp.substring(8, 10) + ' ' + 
                          timeStamp.substring(11, 13) + ':' + 
                          timeStamp.substring(14, 16) + ':' + 
                          timeStamp.substring(17, 19) ;
    DateTime timestampDTGMT = Datetime.valueOfGmt(timestampStr);
    
    System.debug('timeStamp from  service --->>> ' + timeStamp);
    System.debug('currentUserId --->>> ' + currentUserId);
    System.debug('timestampDTGMT --->>> ' + timestampDTGMT);
     
    for(FeedComment comment : [SELECT Id, ParentId, CreatedById, CreatedDate FROM FeedComment  
                               WHERE ParentId IN :mapIntelId_Intel.keySet()]){
      
      Intel__c parentIntel = mapIntelId_Intel.get(comment.parentId);
      
      System.debug('comment --->>> ' + comment);
      System.debug('parentIntel --->>> ' + parentIntel);
      
      if(!intelSet.contains(parentIntel.Id)){
        //Case 1 : If someone else commented on Intel created by current user after timestamp           
	      if(parentIntel.CreatedById == currentUserId && comment.createdDate > timestampDTGMT && comment.createdById != currentUserId){
	      	System.debug('if --->>> ');
	      	intelSet.add(parentIntel.Id);
	        intelList.add(parentIntel);
	      }	      
	      //Case 2 : If someone else commented after timestamp on Intel on which current user had commented in the past
	      else if(parentIntel.CreatedById != currentUserId && comment.createdById == currentUserId && comment.CreatedDate < timestampDTGMT){
	      	System.debug('else --->>> ');
	      	intelSet.add(parentIntel.Id);
	        intelList.add(parentIntel);
	      }
      }
    }
    return intelList;
  }
}