@isTest
public class TestFIMSTrainingExt {
	
    private static scormanywhere__Course__c course;
	
	private static void init() {
		course = new scormanywhere__Course__c();
		insert course;
		
		PageReference pr = Page.MyFIMSTraining; 
		pr.getParameters().put('userId', UserInfo.getUserId());
		Test.setCurrentPageReference(pr);
	}
    
    @isTest
    static void testCtrl() {
        init();
        scormanywhere__Transcript__c transcript = new scormanywhere__Transcript__c(scormanywhere__Course__c = Course.Id, scormanywhere__User__c = UserInfo.getUserId());
		insert transcript;
		
		FIMSTrainingExt controller = new FIMSTrainingExt();
			
		List<FIMSTrainingExt.TranscriptWrapper> transcriptions = controller.getTranscriptWrappers();
			
		String title = controller.getUserNamesCoursesTitle();
			
		System.assert( ! String.isEmpty(title));
	}
    
    @isTest
	private static void testEmpty() {
		FIMSTrainingExt controller = new FIMSTrainingExt();
		
		System.assertNotEquals(NULL, controller.currentUser.Id);
	}
    
    @isTest
	private static void testExceptions() {
        PageReference pr = Page.MyFIMSTraining; 
		pr.getParameters().put('userId', 'null');
		Test.setCurrentPageReference(pr);
        
		FIMSTrainingExt controller = new FIMSTrainingExt();
		
	}
    
}