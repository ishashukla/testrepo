// (c) 2016 Appirio, Inc.
//
// Class Name: Instruments_QuickQuoteController.cls - Controller for Instruments_QuickQuote.page
//
// September 29, 2016 - Appirio (Jeff Hulslander)  Original (T-539932)
//
// October   4,  2016 - Appirio (Shreerath Nair)   Updated  (T-542910) - Replaced hard coded data with custom setting.
//
// October   4,  2016 - Appirio (Shreerath Nair)   Updated  (T-542803) - Added error message in Save method if CPQ is not enabled.

public with sharing class Instruments_QuickQuoteController {

    public List<String> myDivisions {get;set;}
    public User myUser {get;set;}
    public Id accountId {get;set;}
    public String selectedDivision{get;set;}
    
    //T-542803(SN) - Boolean variable to render following page if CPQ is enabled.
    public Boolean isCPQEnabled{get;set;}

    public Instruments_QuickQuoteController() {
        //Get the current user's available business units from their user record (division)
        myDivisions = new List<String>();
        this.myUser = [select id, Division from User where id = :UserInfo.getUserId()][0];
        if(String.isNotBlank(myUser.Division)){
            myDivisions = new List<String>(myUser.Division.split(';'));
        }
        accountId = ApexPages.currentPage().getParameters().get('accountId');
        
        //T-542803(SN) - Set default value as true.
        isCPQEnabled= true;
        
    }

    public List<SelectOption> getDivisionsOptions() {
        //Generate a picklist with the available division options
        List<SelectOption> divisionOptions = new List<SelectOption>();
        for(String d : myDivisions){
            divisionOptions.add(new SelectOption(d,d));
        }
 
        return divisionOptions;
    }

    public PageReference autoSave(){
        //If user only has one division, automatically execute the save method
       try{ 
        if(myDivisions.size() == 1){
            selectedDivision = myDivisions[0];
            return save();
        }
        else if(myDivisions.size() == 0){
        	Messages__c msg = Messages__c.getInstance('UserDivisionNotPresent');
        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,msg.Message_Text__c);
            ApexPages.addMessage(myMsg);
            isCPQEnabled=false;
        } 
       }catch(Exception ex){
       		ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'Instruments_QuickQuoteController', true);
            return null;
       }
        return null;
    }

    public PageReference save(){
        try{
            //T-542910(SN) - Replaced hardcoded data with custom setting of New Opportunity creation
            Quick_Quote_Opportunity_Defaults__c customSettingOpp = Quick_Quote_Opportunity_Defaults__c.getOrgDefaults();
            //Create new Opportunity
            Opportunity opportunity = new Opportunity(Name = customSettingOpp.OpportunityName__c + ' ' + system.now().format(),
                            AccountId = accountId,
                            Quick_Quote__c = customSettingOpp.Quick_Quote__c,
                            StageName = customSettingOpp.Stage_Name__c,
                            ForecastCategoryName = customSettingOpp.ForecastCategoryName__c,
                            Type = customSettingOpp.Type__c,
                            Freeze_Forecast_Category__c = customSettingOpp.Freeze_Forecast_Category__c,
                            Business_Unit__c = selectedDivision,
                            CloseDate = date.today());
            insert opportunity;

            //Redirect to CPQ create quote page for new Opportunity
            String urlParams = '?oppId=' + opportunity.Id + '&actId=' + accountId;
            Id onlyActiveSiteId = BigMachines.BigMachinesFunctionLibrary.getOnlyActiveSiteId();
            if (onlyActiveSiteId != null) {
                urlParams += '&siteId=' + onlyActiveSiteId;
            }
            else{
                Messages__c msg = Messages__c.getInstance('CPQInActiveError');
                //T-542803(SN) - If CPQ is not Enabled then show Error message on the same page.
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,msg.Message_Text__c);
                ApexPages.addMessage(myMsg);
                
                //set boolean variable to false so that the page is not rendered
                isCPQEnabled=false;               
                return null;
            }
            PageReference cpqLink = new PageReference('/apex/BigMachines__QuoteEdit' + urlParams);
            return cpqLink;
        }catch(Exception ex){
            //Display and log any errors
            ApexPages.addMessages(ex);
            ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'Instruments_QuickQuoteController', true);
            return null;
        }
    }
}