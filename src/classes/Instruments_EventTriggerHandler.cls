// (c) 2016 Appirio, Inc.
//
// Class Name: Instruments_EventTriggerHandler
// Description: Handler Class for EventTrigger. 
//
// 18 April 2016, Prakarsh Jain  Original 
//
public class Instruments_EventTriggerHandler {
  //Method to restrict users to delete or edit records if they are not the owner
  //Prakarsh Jain 13 April 2016 - UAT Issues sheet shared by Devin
  public static void restrictEditDeleteOfRecords(List<Event> newList, Map<Id, Event> oldMap, Boolean IsDelete){ 
    if(IsDelete){
      for(Event event : oldMap.values()){
        if(event.OwnerId != UserInfo.getUserId() && UserInfo.getProfileId() != Label.Administrator_Profile_ID){
          event.addError(Label.Delete_Permission_Error);
        }
      }
    }
  /*  else{
      for(Event event : newList){
        if(((oldMap.get(event.Id).ownerId == event.OwnerId && event.OwnerId != UserInfo.getUserId()) || (oldMap.get(event.Id).ownerId != event.OwnerId && oldMap.get(event.Id).ownerId != UserInfo.getUserId())) && String.valueOf(UserInfo.getProfileId()).subString(0,15) != Label.Administrator_Profile_ID.subString(0,15)){
          event.addError(Label.Edit_Permission_Error);
        }
      }
    } */
  }
}