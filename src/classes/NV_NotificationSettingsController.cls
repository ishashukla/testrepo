public with sharing class NV_NotificationSettingsController {
	@AuraEnabled
    public static Notification_Settings__c getCurrentUserSettings() {
    	return NV_Utility.getNotificationSettingForUser(UserInfo.getUserId());
    }
    
    @AuraEnabled
    public static void setCurrentUserSettings(Notification_Settings__c settings) {
        update settings;
    }

    public class FollowUser{

    	@AuraEnabled
    	public Id SFId {get;set;}

    	@AuraEnabled
    	public Id FollowingUserId {get;set;}

    	@AuraEnabled
    	public Id RecordId {get;set;}

    	@AuraEnabled
    	public String RecordName {get;set;}

    	@AuraEnabled
    	public String ExternalId {get;set;}

    	@AuraEnabled
    	public Boolean IsFollowed {get;set;}

    	public FollowUser(Id sfId, Id followingUserId, User userRecord, Boolean IsFollowed, String ExternalId){
    		this.SFId = sfId;
    		this.FollowingUserId = followingUserId;
    		this.RecordId = userRecord.Id;
    		this.RecordName = userRecord.Name;
    		this.IsFollowed = IsFollowed;
    		this.ExternalId = ExternalId;
    	}
    }

    @AuraEnabled
    public static List<FollowUser> getCurrentUserFollowUsers(){

    	List<FollowUser> followUsers = new List<FollowUser>();

    	User currentUser = [SELECT Id, Name, UserRoleId,  UserRole.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
		if(currentUser!= null && currentUser.UserRole!= null && currentUser.UserRole.Name!= null 
				&& (currentUser.UserRole.Name.contains('ATM') 
                        || currentUser.UserRole.Name.contains('Regional Manager'))){
			Map<Id,Id> userFollowed = new Map<Id,Id>();
			for(NV_Follow__c record: [SELECT Id, Record_Id__c FROM NV_Follow__c 
											WHERE Following_User__c =: UserInfo.getUserId()
											AND Record_Type__c = 'User']){
				userFollowed.put(Id.valueOf(record.Record_Id__c), record.Id);	
			}
			
			List<User> subordinates = NV_Utility.getSubordinateUsersOfSelectedRole(currentUser.Id, currentUser.UserRoleId, '%Territory Manager%');
			for(User record: subordinates){
				if(userFollowed.containsKey(record.Id)){
					followUsers.add(new FollowUser(userFollowed.get(record.Id), currentUser.Id, record, true, currentUser.Id+'_'+record.Id));
				}
				else{
					followUsers.add(new FollowUser(null, currentUser.Id, record, false, currentUser.Id+'_'+record.Id));
				}
			}
		}
		return followUsers;
    }

    @AuraEnabled
    public static void setCurrentUserFollowUsersJSON(String followUsersString){
    	System.debug(followUsersString);
    	List<FollowUser> followUsers = (List<FollowUser>)JSON.deserialize(followUsersString, List<FollowUser>.class);
    	System.debug(followUsers);
    	setCurrentUserFollowUsers(followUsers);
    }

    @AuraEnabled
    public static void setCurrentUserFollowUsers(List<FollowUser> followUsers){
    	System.debug(followUsers);
		List<NV_Follow__c> followUsersToUpsert = new List<NV_Follow__c>();
		List<NV_Follow__c> followUsersToDelete = new List<NV_Follow__c>();

		NV_Follow__c followRecord = null;
		Integer i = 0;
		for(FollowUser followUser : followUsers){
			System.debug('' + i++ + followUser );
			if(followUser.SFId != null){
				followRecord = new NV_Follow__c(Id = followUser.SFId,
													Following_User__c = followUser.FollowingUserId,
													Record_Id__c = followUser.RecordId,
													External_Id__c = followUser.ExternalId);	
			}
			else{
				followRecord = new NV_Follow__c(Following_User__c = followUser.FollowingUserId,
													Record_Id__c = followUser.RecordId,
													External_Id__c = followUser.ExternalId);	
			}
			

			if(followUser.IsFollowed){
				followUsersToUpsert.add(followRecord);
			}
			else if(followUser.SFId != null){
				followUsersToDelete.add(followRecord);
			}			
		}
		
		System.debug(followUsersToUpsert);
		if(followUsersToUpsert.size()>0){
			upsert followUsersToUpsert External_Id__c;
		}
		
		System.debug(followUsersToDelete);
		if(followUsersToDelete.size()>0){
			delete followUsersToDelete;
		}
    }
}