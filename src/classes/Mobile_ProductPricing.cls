/**================================================================      
* Appirio, Inc
* Name: Mobile_ProductPricing.cls
* Description: Interface for the Product Pricing for
*              the lightning component for INS
* Created Date: 03-AUG-2016
* Created By: Ray Dehler (Appirio)
*
* Date Modified      Modified By      Description of the update
* 03 AUG 2016        Ray Dehler       Created and added support for Instruments S-423983
==================================================================*/
global interface Mobile_ProductPricing {
	List<Account> getMyCustomerAccounts();
	List<NV_Wrapper.ProductLineData> getAllProducts();
	List<NV_Wrapper.ProductData> getProductsForSegment(String segment, String productLine);
	List<NV_Wrapper.ProductLineData> searchForProducts(String searchText);
	List<User> getFollowableUsers();
	List<NV_Wrapper.ModelNPricing> retrievePricing(String customerId, String productIdsCSV);
	Boolean isBigDataMode();
}