@isTest
private  class COMM_OpptyDocumentTriggerTest {

static List<Document__c> opptyDocList = new List<Document__c>();

static testMethod void testBeforeInsert() {
  createData();
  
 }
 
 static void createData() {
  TestUtils.createCommConstantSetting();
  List<User> userList = TestUtils.createUser(1, 'System Administrator', true);
  List<Account> accList = TestUtils.createAccount(1,true);
  List<Opportunity> opptyList = TestUtils.createOpportunity(2, accList.get(0).Id, true); //NB - 11/9 - I-242624
  List<OpportunityTeamMember> opptyTeamMemberList = TestUtils.createOpportunityTeamMember(2,opptyList.get(0).Id,userList.get(0).Id,false);
  opptyTeamMemberList.get(0).TeamMemberRole = 'Project Engineer';
  opptyTeamMemberList.get(1).TeamMemberRole = 'Project Manager'; //NB - 11/9 - I-242624
  opptyTeamMemberList.get(1).OpportunityId = opptyList.get(1).Id; //NB - 11/9 - I-242624
  insert opptyTeamMemberList;
  opptyDocList = TestUtils.createOpportunityDocument(2,accList.get(0).Id, opptyList.get(0).Id,false);
  opptyDocList.get(1).Opportunity__c = opptyList.get(1).Id; //NB - 11/9 - I-242624
  insert opptyDocList;
  System.assertNotEquals(null,userList.get(0).id); //NB - 11/9 - I-242624
 }
}