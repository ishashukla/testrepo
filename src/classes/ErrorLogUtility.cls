/**================================================================      
 * Appirio, Inc
 * Name: ErrorLogUtility
 * Description: Class to create record for Error Log Object whenever an exception occurs(T-538449).
 * Created Date: [09/27/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/
public with sharing class ErrorLogUtility {
//@future
  public static void createErrorRecord(String errMsg, String errType, Integer errLineNo, String errStackTrace, String className, Boolean isCreate) {
		Error_Log__c errLogRecord = new Error_Log__c();
		errLogRecord.Error_Message__c = errMsg.length()>131072 ? errMsg.substring(0,131071) : errMsg;
		errLogRecord.Error_Type__c = errType;
		errLogRecord.Error_Line_Number__c = errLineNo;
		errLogRecord.Error_Stack_Trace__c = errStackTrace.length()>131072 ? errStackTrace.substring(0,131071) : errStackTrace;
		errLogRecord.Apex_Class__c = className;
		errLogRecord.Logged_in_User__c = UserInfo.getName();
		if(isCreate) {
		  insert errLogRecord;
    }
  }
}