/*
Created by Najma Ateeq for Story #S-408891
Purpose - Handler class of UserTrigger 
*/
public with sharing class UserTriggerHandler {
    public static void beforeInsertUpdateHandler(List<User> lstUser, Map<Id,User> oldMap, Boolean isUpdate){ //before insert/update handler method
        showErrorOnInsertUpdate(lstUser,oldMap,isUpdate); // Uncommented by harendra for story S-415269
    //COMMENTED by Rancho . Shall be resumed when Story S-415269 begins 
    }
    
    public static void afterInsertUpdateHandler(List<User> lstUser, Map<Id,User> oldMap, Boolean isUpdate){ //after insert/update handler method
        populateLicenseInUsedField(lstUser,oldMap,isUpdate); // Uncommented by harendra for story S-415269
    //COMMENTED by Rancho . Shall be resumed when Story S-415269 begins
    }

    private static Map<String,License_Management__c> getLicenseManagementData(){
        String projections = getCurrentQuarterYear();
        Map<String,License_Management__c> mp = new Map<String,License_Management__c>();
        for(License_Management__c licMgmt : [Select Id, Business_Unit__c,Lic_Manager_1__c,Lic_Manager_2__c,Lic_In_Use__c from License_Management__c /*where Projections__c =:projections*/ ]){ // Commented by harendra to 'Projections__c' for story #S-415269
            if(licMgmt.Business_Unit__c != null)
            mp.put(licMgmt.Business_Unit__c,licMgmt);
        }
        return mp;
    }

    /*
    Method Name - showErrorOnInsertUpdate
    Purpose - Method to show error to user when no managers are associated to License Management record
    */
    private static void showErrorOnInsertUpdate(List<User> lstUser,Map<Id,User> oldMap, Boolean isUpdate){
        Map<String,License_Management__c> mapOfLicManagement = getLicenseManagementData(); //get data of License Manangement object
        
        //start by harendra for S-415269
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        //end by harendra for S-415269
        
        for(User u: lstUser){
            if((!isUpdate || (isUpdate && u.License_Allocation__c != oldMap.get(u.Id).License_Allocation__c ))){
                String key = null;
                String oldKey = null;
                //start by harendra for S-415269
                /*
                if(u.License_Allocation__c == null || u.License_Allocation__c == '')
                    key = Label.Other_Unknown_Label;
                else */                
                if(u.License_Allocation__c != null || u.License_Allocation__c != '')
                //end by harendra for S-415269
                    key = u.License_Allocation__c;
                       
                if(isUpdate){
                        //start by harendra for S-415269
                        /*if(oldMap.get(u.Id).License_Allocation__c == null || oldMap.get(u.Id).License_Allocation__c == '')
                            oldKey = Label.Other_Unknown_Label;
                        else*/
                        if(oldMap.get(u.Id).License_Allocation__c != null || oldMap.get(u.Id).License_Allocation__c != '')
                        oldKey = oldMap.get(u.Id).License_Allocation__c;
                }
                //Added oldKey != null check by Harendra for Story #S-415269
                if(isUpdate && mapOfLicManagement != null && mapOfLicManagement.size() >0 && oldKey != null && !mapOfLicManagement.containsKey(oldKey))
                    u.addError(Label.Error_Msg_On_UserTrigger_NDE_For_old_LA);
                //Added key != null check by Harendra for Story #S-415269           
                if(mapOfLicManagement != null && mapOfLicManagement.size() >0 && key != null && mapOfLicManagement.containsKey(key)){
                    License_Management__c licMgmt = mapOfLicManagement.get(key);
                     //Profile check in if added by harendra for S-415269
                    if(profileName != Constants.System_Admin_Profile && licMgmt.Lic_Manager_1__c != UserInfo.getUserId() && licMgmt.Lic_Manager_2__c != UserInfo.getUserId() )
                        u.addError(Label.Error_Msg_On_UserTrigger);
                } else {
                    //start by harendra for S-415269
                    if(key != null)
                    u.addError(Label.Error_Msg_On_UserTrigger_NDE);
                }
            }
        }
    }

    /*
    Method Name - showErrorOnInsertUpdate
    Purpose - Method to populate Lic # in used field based on License Allocation field of user.
    */
    private static void populateLicenseInUsedField(List<User> lstUser,Map<Id,User> oldMap, Boolean isUpdate){
        Map<String,License_Management__c> mapOfLicManagement = getLicenseManagementData();//get data of License Manangement object
        Map<Id,License_Management__c> mapToUpdate = new Map<Id,License_Management__c>();
        if(mapOfLicManagement != null && mapOfLicManagement.size() >0){
            for(User u: lstUser){
                if(!isUpdate || (isUpdate && (u.License_Allocation__c != oldMap.get(u.Id).License_Allocation__c || u.IsActive != oldMap.get(u.Id).isActive))) {
                    String key = null;
                    String oldKey = null; 
                      //start by harendra for S-415269                   
                      /*if(u.License_Allocation__c == null || u.License_Allocation__c == '')
                       key = Label.Other_Unknown_Label;
                      else */       
                      //End by harendra for S-415269
                      if(u.License_Allocation__c != null || u.License_Allocation__c != '')         
                      key = u.License_Allocation__c;
                    if(isUpdate){ 
                        //start by harendra for S-415269   
                        /*if(u.License_Allocation__c != oldMap.get(u.Id).License_Allocation__c && (oldMap.get(u.Id).License_Allocation__c == null || oldMap.get(u.Id).License_Allocation__c == ''))
                            oldKey = Label.Other_Unknown_Label;
                        else*/
                        //End - by harendra for S-415269 
                            if(oldMap.get(u.Id).License_Allocation__c != null || oldMap.get(u.Id).License_Allocation__c != '')
                            oldKey = oldMap.get(u.Id).License_Allocation__c;
                    }
                   //Added key != null check by Harendra for Story #S-415269  
                   // Code modified by Vikrant Nathawat for Case-00179201 starts here 
                    if((!isUpdate || (isUpdate && ((u.License_Allocation__c != oldMap.get(u.Id).License_Allocation__c && u.isActive ) || (u.isActive != oldMap.get(u.Id).isActive && u.isActive)))) && mapOfLicManagement.size() >0 && key != null  && mapOfLicManagement.containsKey(key)){
                   // // Code modified by Vikrant Nathawat for Case-00179201 Ends here  
                       system.debug('Entered increment');
                        if(mapOfLicManagement.get(key).Lic_In_Use__c != null && mapOfLicManagement.get(key).Lic_In_Use__c != 0)
                            mapOfLicManagement.get(key).Lic_In_Use__c += 1;
                        else
                            mapOfLicManagement.get(key).Lic_In_Use__c = 1;
                        mapToUpdate.put(mapOfLicManagement.get(key).Id,mapOfLicManagement.get(key));
                        //licMgmt.Lic_In_Use__c += 1;
                    }
                    //Added oldKey != null check by Harendra for Story #S-415269  
                    // Code modified by Vikrant Nathawat for Case-00179201 starts here 
                    if(((isUpdate && u.License_Allocation__c != oldMap.get(u.Id).License_Allocation__c && u.isActive) && oldKey != null && mapOfLicManagement.size() >0 && mapOfLicManagement.containsKey(oldKey)) || ((isUpdate && u.isActive != oldMap.get(u.Id).isActive && !u.isActive) && mapOfLicManagement.size() >0 && mapOfLicManagement.containsKey(key))){
                       // Code modified by Vikrant Nathawat for Case-00179201 Ends here  
                        system.debug('Entered Decrement');
                        if(mapOfLicManagement.get(oldKey).Lic_In_Use__c != null && mapOfLicManagement.get(oldKey).Lic_In_Use__c != 0)
                            mapOfLicManagement.get(oldKey).Lic_In_Use__c -= 1;
                        else
                            mapOfLicManagement.get(oldKey).Lic_In_Use__c = null;
                        mapToUpdate.put(mapOfLicManagement.get(oldKey).Id,mapOfLicManagement.get(oldKey));
                    }
                }
            }
            
            if(mapToUpdate.size() >0 ){
                //update mapToUpdate.values();
                List<String> lst = new List<String>();
                for(License_Management__c l : mapToUpdate.values()){
                    String IdAndVal = l.Id+'~'+l.Lic_In_Use__c;
                    //system.debug(IdAndVal + 'Id And Val------');
                    lst.add(idAndVal);
                }
                updateLicMgmt(lst);
            }
        }// If end
    }//Method End
    
    private static String getCurrentQuarterYear(){
        String quarter = '';
        Date todayDate = Date.today();
        Integer m = todayDate.month();
        if(m >= 1 && m<=3)
        quarter = 'Q1';
        if(m >=4 && m<=6)
        quarter = 'Q2';
        if(m>=7 && m<=9)
        quarter = 'Q3';
        if(m>=10 && m<=12)
        quarter = 'Q4';
        return quarter +' '+ String.valueOf(todayDate.year());
    }
    
    @future 
    public static void updateLicMgmt(List<String> lstToUpdate){
        List<License_Management__c> lstToUpdateOfLicM = new List<License_Management__c>();
        for(String str : lstToUpdate) {
            License_Management__c l = new License_Management__c(Id = Id.valueOf(str.split('~').get(0)));
            //system.debug(Id.valueOf(str.split('~').get(0))+'Id---------');
            if(str.split('~').get(1).equals('null'))
            l.Lic_In_Use__c = null;
            else
            l.Lic_In_Use__c = Integer.valueOf(str.split('~').get(1));
            //system.debug(l + 'l object-----');
            lstToUpdateOfLicM.add(l);
        }
        if(lstToUpdateOfLicM.size() > 0)
           update lstToUpdateOfLicM;
    }
    
    /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - Start ***/
    //Code moved from Endo_UserTriggerHandler to this current class
    public static void onAfterChanges(List<User> lstNewUsers, Map<Id, User> mapOldUsers) {
        alignSalesSupportAgentOnCases(lstNewUsers, mapOldUsers);
        changeAGWithStatus(lstNewUsers, mapOldUsers);
    }
     
    public static void alignSalesSupportAgentOnCases(List<User> lstNewUsers, Map<Id, User> mapOldUsers) {
      /* Below is a suggestion from NLG - need to do regression testing however 
        Boolean hasEndoUser = false;
        for (User u: lstNewUsers) {
            if (u.Division != null && u.Division.equalsIgnoreCase('Endoscopy')) {
                hasEndoUser = true;
                break;
            }
        }
        if (!hasEndoUser) {
            return;
        }*/
        
        List<Case> toBeUpdate = new List<Case>();
         List<Contact> toBeUpdateContact = new List<Contact>();
        System.debug('alignSalesSupportAgentOnCases trigger invoked');
        for(User u:lstNewUsers){
            System.debug('user id of sales support agent ' + u.Id);
            User oldUser=null;
            String oldRegions=null;
            if(mapOldUsers!=null && !mapOldUsers.isEmpty()){
                oldUser=mapOldUsers.get(u.Id);
                oldRegions=oldUser.Endo_Support_Regions__c;
            }
        //User oldUser=mapOldUsers.get(u.Id);
        String newRegions=u.Endo_Support_Regions__c;
        //String oldRegions=oldUser.Endo_Support_Regions__c;
        System.debug('@@@New Region '+ newRegions);
        System.debug('@@@oldRegions '+ oldRegions);
            
         List<String> addedRegions=getAddedRegions(newRegions, oldRegions);
           system.debug('@@@added regions '+ addedRegions);

            for(String addedRegion:addedRegions){
              List<Case> cases=[Select Id,Sales_Support_Agent__c,Top_35_Sales_Rep__c,Samples_Agent__c from Case where region__c= :addedRegion  and closeddate=null and (status='open' or status='new')];  
                for(Case c:cases){
                   system.debug(c.Id);
                    if(u.title!=null && u.title.indexof('Sales')!=-1 && (c.Sales_Support_Agent__c==null || String.valueOf(c.Sales_Support_Agent__c)=='')){
                        if(c.Top_35_Sales_Rep__c){
                            if(u.Top_35_Sales_Agent__c){
                               toBeUpdate.add(new Case(Id = c.Id, Sales_Support_Agent__c = u.Id)) ; 
                            }
                        }else{
                            toBeUpdate.add(new Case(Id = c.Id, Sales_Support_Agent__c = u.Id)) ;
                        }
                   
                    }else if(u.title!=null && u.title.indexof('Sample')!=-1 && (c.Samples_Agent__c==null || String.valueOf(c.Samples_Agent__c)=='')){
                        toBeUpdate.add(new Case(Id = c.Id, Samples_Agent__c = u.Id)) ;
                    }
                }
                
              List<Contact> contacts=[Select Id,Sales_Support_Agent__c,Top_35_Sales_Agent__c,Samples_Agent__c from Contact where ENDO_Region_Name__c= :addedRegion];    
                for(Contact c:contacts){
                   if(u.title!=null && u.title.indexof('Sales')!=-1 && (c.Sales_Support_Agent__c==null || String.valueOf(c.Sales_Support_Agent__c)=='')){
                       if(c.Top_35_Sales_Agent__c){
                            if(u.Top_35_Sales_Agent__c){
                               toBeUpdateContact.add(new Contact(Id = c.Id, Sales_Support_Agent__c = u.Id)) ;
                            }
                        }else{
                            toBeUpdateContact.add(new Contact(Id = c.Id, Sales_Support_Agent__c = u.Id)) ;
                        }
                   
                    }else if(u.title!=null && u.title.indexof('Sample')!=-1 && (c.Samples_Agent__c==null || String.valueOf(c.Samples_Agent__c)=='')){
                        toBeUpdateContact.add(new Contact(Id = c.Id, Samples_Agent__c = u.Id)) ;
                    }
                }
             }
             List<String> removedRegions=getRemovedRegions(newRegions, oldRegions);
            system.debug('Removed Regions ' + removedRegions);
            for(String removedRegion:removedRegions){
                System.debug('removedRegion ' + removedRegion);
               String incluledremovedRegion= '\'' + removedRegion + + '\'';
                if(incluledremovedRegion.length() > 0){
            incluledremovedRegion = '(' + incluledremovedRegion + ')';
            }
          List<User> lstUsers = new List<User>();
          if (String.isBlank(removedRegion) == false){
            String qry = 'SELECT Id ,Name, Title, Endo_Support_Regions__c, Top_35_Sales_Agent__c FROM User WHERE IsActive = true AND (Endo_Support_Regions__c INCLUDES' + incluledremovedRegion + ')';
            lstUsers = Database.query(qry);
          }
                
                String newSalesSupportAgent=null;
                String newSampleSupportAgent=null;
                String top35SalesSupportAgent=null;
                if(!lstUsers.isEmpty()){
                    for(User user:lstUsers){
                        if(!String.isBlank(user.Title) && user.title.indexof('Sales')!=-1 && newSalesSupportAgent==null && user.Top_35_Sales_Agent__c){
                            top35SalesSupportAgent=user.Id;
                            continue;
                        }
                        if(!String.isBlank(user.Title) && user.title.indexof('Sales')!=-1 && newSalesSupportAgent==null && !user.Top_35_Sales_Agent__c){
                            newSalesSupportAgent=user.Id;
                            continue;
                        }
                        if(!String.isBlank(user.Title) && user.title.indexof('Sample')!=-1 && newSampleSupportAgent==null){
                            newSampleSupportAgent=user.Id;
                            continue;
                        }
                    }  
                }
                
                system.debug('@@@lstUsers for removed region ' + lstUsers);
              List<Case> cases=[Select Id,Sales_Support_Agent__c,Top_35_Sales_Rep__c,Samples_Agent__c from Case where region__c= :removedRegion  and closeddate=null and (status='open' or status='new')];  
                for(Case c:cases){
                   system.debug('@@@cases where region matched with removed region ' + c.Id);
                    if(u.title!=null && u.title.indexof('Sales')!=-1){
                        if(c.Sales_Support_Agent__c==u.Id && c.Top_35_Sales_Rep__c && top35SalesSupportAgent!=null ){
                            toBeUpdate.add(new Case(Id = c.Id, Sales_Support_Agent__c = top35SalesSupportAgent));
                        }else if(c.Sales_Support_Agent__c==u.Id && newSalesSupportAgent!=null && !c.Top_35_Sales_Rep__c){
                        toBeUpdate.add(new Case(Id = c.Id, Sales_Support_Agent__c = newSalesSupportAgent));
                    }else{
                        toBeUpdate.add(new Case(Id = c.Id, Sales_Support_Agent__c = null));
                    }
                    }else if(u.title!=null && u.title.indexof('Sample')!=-1){
                      if(c.samples_agent__c==u.Id && newSampleSupportAgent!=null){
                        toBeUpdate.add(new Case(Id = c.Id, samples_agent__c = newSampleSupportAgent));
                    }else{
                        toBeUpdate.add(new Case(Id = c.Id, samples_agent__c = null));
                    }  
                    }
               }
                    
                   
               
                List<Contact> contacts=[Select Id,Sales_Support_Agent__c,Top_35_Sales_Agent__c, samples_agent__c from Contact where ENDO_Region_Name__c= :removedRegion];    
                for(Contact c:contacts){
                   system.debug(c.Id);
                    if(u.title!=null && u.title.indexof('Sales')!=-1){
                     if(c.Sales_Support_Agent__c==u.Id && c.Top_35_Sales_Agent__c && top35SalesSupportAgent!=null ){
                            toBeUpdateContact.add(new Contact(Id = c.Id, Sales_Support_Agent__c = top35SalesSupportAgent));
                        }else    
                    if(c.Sales_Support_Agent__c==u.Id && newSalesSupportAgent!=null && !c.Top_35_Sales_Agent__c){
                        toBeUpdateContact.add(new Contact(Id = c.Id, Sales_Support_Agent__c = newSalesSupportAgent)) ;
                    }else{
                      toBeUpdateContact.add(new Contact(Id = c.Id, Sales_Support_Agent__c = null)) ; 
                    }
                    }else if(u.title!=null && u.title.indexof('Sample')!=-1){
                    if(c.samples_agent__c==u.Id && newSampleSupportAgent!=null){
                        toBeUpdateContact.add(new Contact(Id = c.Id, samples_agent__c = newSampleSupportAgent)) ;
                    }else{
                        toBeUpdateContact.add(new Contact(Id = c.Id, samples_agent__c = null)) ;
                    }
                   }
                }
         }
                     }
        
        
    update toBeUpdate;
    update toBeUpdateContact;    
     }
    
    private static List<String> getAddedRegions(String newRegions,String oldRegions){
        List<String> addedRegions=new List<String>();
        system.debug('In getAddedRegions');
         system.debug('newRegions ' + newRegions);
         system.debug('oldRegions' + oldRegions);
        if(newRegions!=null){
        for(String newRegion :newRegions.split(';')){
            if(oldRegions!=null && oldRegions.indexOf(newRegion)== -1){
                system.debug('newRegion added ' + newRegion);
                addedRegions.add(newRegion);
            }else if(oldRegions==null){
                addedRegions.add(newRegion);
            }
            
        }}
        system.debug('added regions ' + addedRegions);
        return addedRegions;
    }
    
     private static List<String> getRemovedRegions(String newRegions,String oldRegions){
         system.debug('In getRemovedRegions');
         system.debug('newRegions ' + newRegions);
         system.debug('oldRegions' + oldRegions);
        List<String> removedRegions=new List<String>();
         if(oldRegions!=null){
          for(String oldRegion :oldRegions.split(';')){
            if(newRegions!=null && newRegions.indexOf(oldRegion)== -1){
                system.debug('removedRegions added '+ oldRegion);
                removedRegions.add(oldRegion);
            }else if(newRegions==null){
                removedRegions.add(oldRegion);
            }
          }
        }
        return removedRegions;
    }
    
    // Method to be called on AfterInsert event of trigger, to change Assignemnt Group checkbox according to status
    //  @param List of new after insert User, Map of new after insert User
    //  @return void.
    
    public static void changeAGWithStatus(List<User> lstNewUsers, Map<Id, User> mapOldUsers) {
        // Condition to make sure this only runs for Endo users
        Boolean hasEndoUser = false;
        for (User u: lstNewUsers) {
            if (u.Division != null && u.Division.equalsIgnoreCase('Endoscopy')) {
                hasEndoUser = true;
                break;
            }
        }
        if (!hasEndoUser) {
            return;
        }
        
        //I-236931(SN) - Available values of User Presence
        Map<String,Endo_OmniChannel_Status__c> omniChannelStatusMap = Endo_OmniChannel_Status__c.getAll();
        Set<String> onlineStatus= new Set<String> ();
        for(Endo_OmniChannel_Status__c onmiChannel : omniChannelStatusMap.values()){          
            onlineStatus.add(onmichannel.Name);    
        } 
        
        // Getting Assignment Group Members who are representing the users being updated
        List<Assignment_Groups__c> updatedMembers = new List<Assignment_Groups__c>();                                            

        if(!lstNewUsers.isEmpty()) { // && hasEndoUser
            
            List<Assignment_Groups__c> assignmentGroupMembers = [SELECT Id, User__c FROM Assignment_Groups__c WHERE User__c in: lstNewUsers];
            Map<String, List<Assignment_Groups__c>> userAssignmentGroupMembersMap = new Map<String, List<Assignment_Groups__c>>();
            
            // Storing values in map in order to easily retrieve later on
            for (Assignment_Groups__c member: assignmentGroupMembers) {
                if (userAssignmentGroupMembersMap.get(member.User__c) == null) {
                    userAssignmentGroupMembersMap.put(member.User__c, new List<Assignment_Groups__c>());
                }
                userAssignmentGroupMembersMap.get(member.User__c).add(member);
            }

            
            for (User u : lstNewUsers) { //[Select id, Assignment_Group_Active__c, status__c, Profile.Name From User where Id IN :lstNewUsers]){
                //I-236931(SN) - commented u.Assignment_Group_Active__c from the if condition
                if((mapOldUsers == null || u.status__c != mapOldUsers.get(u.id).status__c /* && u.Assignment_Group_Active__c*/)){ //&& u.Profile.Name.contains('Endo') 
                    // Assigning true or false to Group Member based on whether User is available or not (Status__c value)
                    if (!userAssignmentGroupMembersMap.keySet().contains(u.Id)) {
                        continue;
                    }
                    for (Assignment_Groups__c member: userAssignmentGroupMembersMap.get(u.Id)) {
                        if (onlineStatus.contains(u.Status__c)) {
                          member.Active__c = 'True';
                        }
                        else {
                          member.Active__c = 'False';
                        }
                        updatedMembers.add(member);
                    }
                }
            } 
        }
        
        if(!updatedMembers.isEmpty()){
            update updatedMembers;
        }
    }
    /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - End ***/
}