/**
** controller for order detail page button 
**/
public with sharing class NV_OrderItemExt {
    public Order currentOrder {get;set;}
    public NV_OrderItemExt(ApexPages.StandardController controller){
        currentOrder = (Order) controller.getRecord();
    }
    // invoking the service 
    public PageReference TrackOrder(){
        if(currentOrder.Id != null){
            //Logic for S-371704 : Replacing Tracking Number with Tracked Number
            List<OrderItem> orderItemList = [SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c
                                                FROM OrderItem 
                                                WHERE OrderId =: currentOrder.Id
                                                    AND Order_Delivered__c = false 
                                                    AND Tracked_Number__c != NULL ];
            if(orderItemList.size()>0){
                new NV_FedExRequest().TrackFedExRequest(orderItemList);
            }
            return new PageReference('/' + currentOrder.Id); // going back to order 
        }
        else{
            return null;
        }

    }
}