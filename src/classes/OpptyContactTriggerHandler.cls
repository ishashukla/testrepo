/**================================================================      
* Appirio, Inc
* Name:           OpptyContactTriggerHandler
* Description:    To populate customer signer field on project phase with primary contact of Oppty Contact.
* Created Date:   09-05-2016
* Created By:     Shubham Dhuppar(Appirio)
*
* Date Modified      Modified By      Description of the update
* Meghna Vijay      18-05-2016      To populate number of attendees on site visit object(T-502659)
* 
* Date Modified      Modified By      Description of the update
* Shubham Dhupar    10-08-2016       To populate customer signer field on project phase with primary
*                                     contact of Oppty Contact(I-228990)
==================================================================*/

public class OpptyContactTriggerHandler  {
  public static void afterInsert(List<Related_Contact__c> newList) {
    populateCustomerSignerOnplanAtInsert(newList);
    populateNumberOfClientsAtInsert(newList);
  }
  public static void afterUpdate(List<Related_Contact__c> newList, Map<Id,Related_Contact__c> oldMap) {
    populateCustomerSignerOnplanAtUpdate(newList,oldMap);
  }
  public static void afterDelete(List<Related_Contact__c> oldList) {
   populateCustomerSignerOnplanAtDelete(oldList);
   populateNumberOfClientsAtDelete(oldList);
 }
  public static void populateCustomerSignerOnplanAtInsert(List<Related_Contact__c> newList) {
    
    Map<Id,Id> planIdMap = new Map<Id,Id>();
    Map<ID, Schema.RecordTypeInfo> ContactRtMap = 
                      Schema.SObjectType.Related_Contact__c.getRecordTypeInfosById();
    for(Related_Contact__c contactasso : newList){      
      if(ContactRtMap.get(contactasso.RecordtypeID).getName().contains('Project')) {
        if(contactasso.Project_plan__c != NULL && contactasso.Primary__c == TRUE) {
          planIdMap.put(contactasso.Project_plan__c,contactasso.Contact__c);
        }
      }
    }
    List<Project_plan__c> planList = new List<Project_plan__c>();
    for(Project_plan__c plan: [Select Id,Customer_Signer__c,Customer__c
                                  From Project_plan__c
                                  where Id in : planIdMap.keySet()]) {
      if(plan.Customer_Signer__c == NULL) {
        plan.Customer_Signer__c = planIdMap.get(plan.Id);
        plan.Customer__c = plan.Customer_Signer__c;
      }
      planList.add(plan);
    }
    update planList;
  }
  public static void populateCustomerSignerOnplanAtDelete(List<Related_Contact__c> oldList){
    Map<Id,Id> planIdMap = new Map<Id,Id>();
    Map<ID, Schema.RecordTypeInfo> ContactRtMap = 
                      Schema.SObjectType.Related_Contact__c.getRecordTypeInfosById();
    for(Related_Contact__c contactasso : oldList) {      
      if(ContactRtMap.get(contactasso.RecordtypeID).getName().contains('Project')) {
        if(contactasso.Primary__c == True) {
          planIdMap.put(contactasso.Project_plan__c,contactasso.Contact__c);
        }
      }
    }
    UpdateCustomerSignerOnUpdateAndDelete(planIdMap);
  }
  public static void populateCustomerSignerOnplanAtUpdate(List<Related_Contact__c> newList, Map<Id,Related_Contact__c> oldMap){
    Map<Id,Id> planIdMap = new Map<Id,Id>();
    Map<Id,Id> newplanIdMap = new Map<Id,Id>();
    System.debug('abc');
    Map<ID, Schema.RecordTypeInfo> ContactRtMap = 
                      Schema.SObjectType.Related_Contact__c.getRecordTypeInfosById();
    for(Related_Contact__c contactasso : newList) {      
      System.debug('Curr val ' +contactasso.Contact__c + 'Old value' + oldMap.get(contactasso.id).Contact__c);
      if(ContactRtMap.get(contactasso.RecordtypeID).getName().contains('Project')) {
        if((contactasso.Primary__c !=  oldMap.get(contactasso.id).Primary__c) && contactasso.Primary__c == False) {
          planIdMap.put(contactasso.Project_plan__c,contactasso.Contact__c);
           System.debug(' !!!!!!!!!!!!! True 1 ' );
        }else if((contactasso.Primary__c !=  oldMap.get(contactasso.id).Primary__c) && contactasso.Primary__c == True) {
          newplanIdMap.put(contactasso.Project_plan__c,contactasso.Contact__c);
           System.debug(' !!!!!!!!!!!!! True 2 ' );
        }else if((contactasso.Contact__c !=  oldMap.get(contactasso.id).Contact__c) && contactasso.Primary__c == True) {
          System.debug(' !!!!!!!!!!!!! True 3 ' );
          planIdMap.put(contactasso.Project_plan__c,contactasso.Contact__c);
          System.debug(' !!!!!!!!!!!!! ' + planIdMap );
        }
      }
    }
    if(newplanIdMap != NULL) {
      newContactUpdateOnplan(newplanIdMap);
    }
    if(planIdMap != NULL) {
      UpdateCustomerSignerOnUpdateAndDelete(planIdMap);
    }
    
  }
  public static void newContactUpdateOnplan(Map<Id,Id> planIdMap){
    System.debug(' 11111111111111111 ' + planIdMap );
    List<Project_plan__c> planList = new List<Project_plan__c>();
    for(Project_plan__c plan: [Select Id,Customer_Signer__c,Customer__c
                                  From Project_plan__c
                                  where Id in : planIdMap.keySet()]) {
      if(plan.Customer_Signer__c == NULL) {
        plan.Customer_Signer__c = planIdMap.get(plan.id);
        plan.Customer__c = plan.Customer_Signer__c;
        planList.add(plan);
      }
    }
    update planList;
  }  
  public static void UpdateCustomerSignerOnUpdateAndDelete(Map<Id,Id> planIdMap) {
    System.debug(' 2222222222222222 ' + planIdMap );
    List<Project_plan__c> planList = new List<Project_plan__c>();
    for(Project_plan__c plan: [Select Id,Customer_Signer__c 
                                  From Project_plan__c
                                  where Id in : planIdMap.keySet()]) {
      if(plan.Customer_Signer__c == planIdMap.get(plan.Id)) {
        plan.Customer_Signer__c = NULL;
        planList.add(plan);
      }
      else {
        plan.Customer_Signer__c = NULL;
        plan.Customer__c = plan.Customer_Signer__c;
        planList.add(plan);
        
      }
      System.debug( 'NULL HOga' +  plan.Customer_Signer__c );
    }
    update planList;
    
    List<Project_plan__c> newplanList = new List<Project_plan__c>();
    Map<Id,Id> planIdsMap = new Map<Id,Id>();
    for(Related_Contact__c contactAssoc: [Select Id,Contact__c,CreatedDate,Project_plan__c,Primary__c
                                                           From Related_Contact__c
                                                           Where Primary__c = True And Project_plan__c in :planIdMap.keySet()
                                                           ORDER BY CreatedDate ASC NULLS LAST Limit 1]) {
      planIdsMap.put(contactAssoc.Project_plan__c,contactAssoc.Contact__c);         
    }
    for(Project_plan__c plan1: [Select Id,Customer_Signer__c,Customer__c
                                  From Project_plan__c
                                  where Id in : planIdsMap.keySet()]) {
        if(plan1.Customer_Signer__c == NULL) {
          plan1.Customer_Signer__c = planIdsMap.get(plan1.Id);
          plan1.Customer__c = plan1.Customer_Signer__c;
      }
      newplanList.add(plan1);
    }
    update newplanList;
  }
  //===============================================================================================
  //Name             :     populateNumberOfClientsAtInsert Method
  //Description      :     Method created to populate number of attendees at insert on site visit
  //Created by       :     Meghna Vijay T-502659
  //Created Date     :     May 17, 2016
  //===============================================================================================
  public static void populateNumberOfClientsAtInsert(List<Related_Contact__c> newList) {
    Set<String> sVisitId = new Set<String> ();
    Id cOpptyAsscRTID = Schema.SObjectType.Related_Contact__c.getRecordTypeInfosByName().get('Site Visit Attendees').getRecordTypeId();
    for(Related_Contact__c cOpptyAssc :newList) {
      if(cOpptyAssc.RecordtypeID == cOpptyAsscRTID) {
       sVisitId.add(cOpptyAssc.Site_Visit_Attendees__c); 
      }
      
    }
    
    recalculate(sVisitId);
  }
  //Method created to count number of attendees in contact opportunity association
  public static void recalculate(Set<String> sVisitId) {
    List<Site_Visit__c> sVisitList = new List<Site_Visit__c> ();
    Map<String, List<Related_Contact__c>> numberOfClientsMap = new Map<String, List<Related_Contact__c>>();
    for(Site_Visit__c s : [SELECT ID, (SELECT ID FROM Site_Visit_Attendee__r) FROM Site_Visit__c WHERE ID IN : sVisitId]) {
      numberOfClientsMap.put(s.Id,s.Site_Visit_Attendee__r);
    }
    
    for(String sVistId :sVisitId ) {
      Site_Visit__c sVObj = new Site_Visit__c(Id = sVistId);
      sVObj.Number_Of_Clients__c = numberOfClientsMap.get(sVistId).size();
      sVisitList.add(sVObj);
    }
    
    update sVisitList;
  }
  //===============================================================================================
  //Name             :     populateNumberOfClientsAtDelete Method
  //Description      :     Method created to populate number of attendees at delete on site visit
  //Created by       :     Meghna Vijay T-502659
  //Created Date     :     May 18, 2016
  //===============================================================================================
  public static void populateNumberOfClientsAtDelete(List<Related_Contact__c> oldList) {
    Set<String> parentIds = new Set<String>();
    Id cOpptyAsscRTID = Schema.SObjectType.Related_Contact__c.getRecordTypeInfosByName().get('Site Visit Attendees').getRecordTypeId();
    for(Related_Contact__c cOpptyAsscObj : oldList){
      if(cOpptyAsscObj.RecordtypeID == cOpptyAsscRTID ){
        parentIds.add(cOpptyAsscObj.Site_Visit_Attendees__c);
      }
    }
    recalculate(parentIds);
  }
}