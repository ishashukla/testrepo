/**================================================================      
* Appirio, Inc
* Name: OrderLineItemAmountHandler 
* Description: T-492016 - to populate summary fields on Order to sum up cOrderItem fields.
* Created Date: 5 May 2016
* Created By: Mehul Jain (Appirio)
*
* 17th August 2016    Nitish Bansal      Update (T-525896)  //Changed roll up summary logic for project and order fields.
* 31st August 2016    Nitish Bansal      Update (I-232524)  //Changed roll up summary field for project and order fields.
                                                            // (Used quantity * unit price instead of Line_amount__c since Total price can't be used directly)
==================================================================*/
public class OrderLineItemAmountHandler {
  public static void afterInsertupdate(List<OrderItem> newList, Map<Id, OrderItem> oldMap) {
    List<OrderItem> orderitemsUpdated = new List<OrderItem>();
    
    if(trigger.isInsert) {
      populateOrderAndProjectFields(newList) ;
    }
    if(trigger.isUpdate) {
      for(OrderItem temp : newList){
        //if((temp.Quantity * temp.UnitPrice) != (oldMap.get(temp.Id).Quantity * oldMap.get(temp.Id).UnitPrice)){ //NB - 11/11 - I-243413
        if((temp.Sub_Total__c) != (oldMap.get(temp.Id).Sub_Total__c)){   //NB - 11/11 - I-243413
          orderitemsUpdated.add(temp);
        }
      }
      populateOrderAndProjectFields(orderitemsUpdated) ;
    }  
  }
  
  public static void populateOrderAndProjectFields(List<OrderItem> newList) {
    for(OrderItem temp : newList) {
    //NB - T-525896 - 08/17 Start - Commenting original code and using new logic and fields
          
      if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c == '001'){
          //temp.Order.Oris_Amount__c += temp.Quantity * temp.UnitPrice ;Sub_Total__c //NB - 11/11 - I-243413
          temp.Order.Oris_Amount__c += temp.Sub_Total__c; //NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
              //temp.Order.Project_Plan__r.Oris_Amount__c += temp.Quantity * temp.UnitPrice ;     //NB - 11/11 - I-243413
              temp.Order.Project_Plan__r.Oris_Amount__c += temp.Sub_Total__c;     //NB - 11/11 - I-243413
          }
      } else if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c == '002'){
          //temp.Order.Boom_Amount__c += temp.Quantity * temp.UnitPrice ; //NB - 11/11 - I-243413
          temp.Order.Boom_Amount__c += temp.Sub_Total__c; //NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
            //  temp.Order.Project_Plan__r.Boom_Amount__c += temp.Quantity * temp.UnitPrice ; //NB - 11/11 - I-243413
            temp.Order.Project_Plan__r.Boom_Amount__c += temp.Sub_Total__c ; //NB - 11/11 - I-243413
          }    
      } else if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c == '003'){
          temp.Order.Tables_Amount__c += temp.Sub_Total__c ; //NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
              temp.Order.Project_Plan__r.Tables_Amount__c += temp.Sub_Total__c ;   //NB - 11/11 - I-243413    
          }    
      } else if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c == '005'){
          temp.Order.Orreno_Amount__c += temp.Sub_Total__c ; //NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
              temp.Order.Project_Plan__r.Orreno_Amount__c += temp.Sub_Total__c ; //NB - 11/11 - I-243413
          }         
      } else if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c == '006'){
          temp.Order.Lighting_Systems_Amount__c += temp.Sub_Total__c ;//NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
              temp.Order.Project_Plan__r.Lighting_Systems_Amount__c += temp.Sub_Total__c ;//NB - 11/11 - I-243413
          }          
      } else if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c == '009'){
          temp.Order.Integration_Services_Amount__c += temp.Sub_Total__c ;//NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
              temp.Order.Project_Plan__r.Integration_Services_Amount__c += temp.Sub_Total__c ;  //NB - 11/11 - I-243413    
          }    
      } else if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c == '050'){
          temp.Order.Pro_Care_Amount__c += temp.Sub_Total__c ;//NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
              temp.Order.Project_Plan__r.Pro_Care_Amount__c += temp.Sub_Total__c ;    //NB - 11/11 - I-243413 
          }    
      } else if(temp.PricebookEntry.Product2.Product_Class_Code__c != null && temp.PricebookEntry.Product2.Product_Class_Code__c!= ''){
          temp.Order.Other_Amount__c += temp.Sub_Total__c;//NB - 11/11 - I-243413
          if(temp.Order.Project_Plan__c != null){
              temp.Order.Project_Plan__r.Other_Amount__c += temp.Sub_Total__c ;      //NB - 11/11 - I-243413
          }    
      }
    //NB - T-525896 - 08/17  End
    }    
  }
}