/*******************************************************************************
 Author        :  Appirio JDC (Jitendra Kothari)
 Date          :  Mar 19, 2014 
 Purpose       :  This will be used for showing Expiring Pricing Lines of account
*******************************************************************************/
public with sharing class Endo_AccountExpiringPricingLine {
    public Account acc{get;set;}
    public List<ExpiringPricingLineItemWrapper> expiringPricingLineItems{get; set;}
    public boolean showPricingLineItems{get; set;}
    public Integer counter{get; set;}
    public boolean showPricingLineItemCounter{get{
        if(counter > 0 && !showPricingLineItems){
            return true;
        }else{
            return false;
        }
    } set;} 
    
    public Endo_AccountExpiringPricingLine(ApexPages.StandardController stdController){
        if(!Test.isRunningTest()){ // for test classes - addFields are not working with test classes
            stdController.addFields(accountFields());
        }
        acc = (Account) stdController.getRecord();
        if(ApexPages.currentPage().getParameters().containsKey('showPLI')){
            this.showPricingLineItems = true; 
        }else{
            this.showPricingLineItems = false;
        }
        this.counter = 0;
        fetchPricingLineItemsWithCount();
    }
    
    @TestVisible
    private void fetchPricingLineItemsWithCount(){
        PricingLineExpiring__c pce = PricingLineExpiring__c.getInstance();
        Integer expiringDays = 15;
        
        if(pce != null){
            try{
                expiringDays = (Integer) pce.Expiring_Days__c;
            }
            catch(Exception ex){
              expiringDays = 15;
            }
        }
        
        if(expiringDays == null || expiringDays <1){
        	expiringDays = 15;
        }
        
        List<Pricing_Contract_Account__c> pricingContractList = [SELECT Pricing_Contract__c FROM Pricing_Contract_Account__c WHERE Account__c = :acc.Id limit :getRemainingSOQLRows()];
        //Don't proceed if we dont have any pricing contract to Account
        if(pricingContractList.isEmpty()) return;
        
        set<Id> pricingContractIds = new set<Id>();
        for(Pricing_Contract_Account__c pricingContract : pricingContractList){
            pricingContractIds.add(pricingContract.Pricing_Contract__c);
        }
        
        Date exipiringEndDate = System.Today().addDays(expiringDays);
        
        if(showPricingLineItems){
            fetchPricingLineItems(pricingContractIds, exipiringEndDate);
        }else{
            counter = [SELECT count() 
                    FROM Pricing_Contract_Line_Item__c 
                    WHERE Pricing_Contract__c IN :pricingContractIds 
                    AND End_Date__c <= :exipiringEndDate AND End_Date__c >= :System.Today()
                    //AND Excluder__c = false 
                    limit :getRemainingSOQLRows()];
        }
    }
    
    @TestVisible
    private void fetchPricingLineItems(set<Id> pricingContractIds, Date exipiringEndDate){
        this.expiringPricingLineItems = new List<ExpiringPricingLineItemWrapper>();
        List<Pricing_Contract_Line_Item__c> expiringLineItems = [SELECT Id, Name, Pricing_Contract__c, Related_Part_Num__c, Pricing_Line_Id__c, 
                                                                            Product__c, Value__c, End_Date__c, Application_Method__c, Contract_Price__c  
                                                                            FROM Pricing_Contract_Line_Item__c 
                                                                            WHERE Pricing_Contract__c IN :pricingContractIds 
                                                                            AND End_Date__c <= :exipiringEndDate AND End_Date__c >= :System.Today()
                                                                            //AND Excluder__c = false
                                                                            limit :getRemainingSOQLRows()];
        //Don't proceed if we dont have any pricing line items to Account
        if(expiringLineItems.isEmpty()) return;
        /*set<Id> productIds = new set<Id>();
        for(Pricing_Contract_Line_Item__c pricingLine : expiringLineItems){
            if(pricingLine.Application_Method__c != null && pricingLine.Application_Method__c == '%'){
                productIds.add(pricingLine.Product__c);
            }
        }
        
        Map<Id, PricebookEntry> priceBookByProduct = getPriceBookRecords(productIds);*/
        ExpiringPricingLineItemWrapper epliw;
        Decimal discountAmount = 0.0;
        for(Pricing_Contract_Line_Item__c pricingLine : expiringLineItems){
            epliw = new ExpiringPricingLineItemWrapper(pricingLine);
            /*if(pricingLine.Application_Method__c != null){
                if(pricingLine.Application_Method__c == '%' && priceBookByProduct.containsKey(pricingLine.Product__c)){
                    Decimal lstPrice = priceBookByProduct.get(pricingLine.Product__c).UnitPrice;
                    if(pricingLine.Value__c != null){
                        discountAmount = (lstPrice * pricingLine.Value__c) / 100 ;
                    }
                    epliw.contractPrice = lstPrice - discountAmount;
                }else if(pricingLine.Application_Method__c.replace(' ', '').toUpperCase() == 'NEWPRICE'){
                    epliw.contractPrice = pricingLine.Value__c;
                }
            }*/
            this.expiringPricingLineItems.add(epliw);
        }
    }    
    
    // Helper method to get unit price by product
    /*@TestVisible
    private Map<Id, PricebookEntry> getPriceBookRecords(set<Id> productIds){
        List<PricebookEntry> priceBooks = [SELECT UnitPrice, Product2Id 
                                                FROM PricebookEntry 
                                                WHERE Pricebook2.Name = 'Standard Price Book' AND IsActive = true AND Product2Id IN :productIds 
                                                limit :getRemainingSOQLRows()];
        Map<Id, PricebookEntry> priceBookByProduct = new Map<Id, PricebookEntry>();
        for(PricebookEntry pe : priceBooks){
            priceBookByProduct.put(pe.Product2Id, pe);
        }
        return priceBookByProduct;
    }*/
    
    //To get account fields
    @TestVisible
    private List<String> accountFields(){
        List<string> fields = new List<String>();
        SObjectType objToken = Schema.getGlobalDescribe().get('Account');
        Map<String, SObjectField> fieldsDesc = objToken.getDescribe().fields.getMap();
        for(String f : fieldsDesc.keySet()){
            fields.add(f);
        }   
        return fields;
    }
    
    //To get remaining SOQL query rows for the current thread
    @TestVisible
    private Integer getRemainingSOQLRows(){
        return Limits.getLimitQueryRows() - Limits.getQueryRows();
    }
    
    public class ExpiringPricingLineItemWrapper{
        public Pricing_Contract_Line_Item__c pcli{get;set;}
        //public Decimal contractPrice{get; set;}
        public ExpiringPricingLineItemWrapper(Pricing_Contract_Line_Item__c pcli){
            this.pcli = pcli;
            //this.contractPrice = 0;
        }
    }
}