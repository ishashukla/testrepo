/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Dec 12, 2013 
 Purpose       :  Test class for trigger IntelTriggerHandler
 Reference     :  Task T-214509
*******************************************************************************/
@isTest
private class IntelTriggerHandler_Test {

	static testMethod void myUnitTest() {
	
    Test.startTest();
    List<Chatter_Group_Ids__c> csList = TestUtils.createChatterGroupCS(true); 
    Intel__c intel = TestUtils.createIntel(1, true).get(0);
    intel = [SELECT Id, Chatter_Post__c, Mobile_Update__c FROM Intel__c WHERE Id = :intel.Id].get(0);
    System.assertNotEquals(null, intel.Mobile_Update__c);
    System.assertNotEquals(null, intel.Chatter_Post__c);
    System.assertNotEquals(null, IntelTriggerHandler.getChatterGroupId());
    Test.stopTest();
	}
}