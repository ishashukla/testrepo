/****************************************************************
Name  :  RejectLead_Extension
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 11, 2016

Modified: 
*****************************************************************/
public class RejectLead_Extension {

	public Lead leadRecord {get; set;}
	public Boolean customReasonIsRequired {get; set;}
	
	public RejectLead_Extension(ApexPages.StandardController controller) {
		leadRecord = new Lead();
		String idOfLead = ApexPages.currentPage().getParameters().get('leadId');
		leadRecord = [SELECT Id, Name, Status, Rejection_Reason__c, Custom_Reason__c
					FROM Lead
					WHERE Id = :idOfLead];
		customReasonIsRequired = false;
		leadRecord.Status = 'Rejected';
	}

	public PageReference save() {
		update leadRecord;
		return listView(Lead.sObjectType);
	}

	public PageReference cancel() {
		PageReference pageRef = new PageReference('/' + leadRecord.Id);
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference listView(Schema.sObjectType destinationSObject) {
		Schema.DescribeSObjectResult destination = destinationSObject.getDescribe();
		PageReference pageRef = new PageReference('/' + destination.getKeyPrefix() );
		pageRef.setRedirect(true);
		return pageRef;
	}

	public void checkOfCustomReasonisRequired() {
		if(leadRecord.Rejection_Reason__c == 'Other') {
			customReasonIsRequired = true;
		} else {
			customReasonIsRequired = false;
		}
	}	
}