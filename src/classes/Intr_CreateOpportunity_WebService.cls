// (c) 2016 Appirio, Inc.
//
// Class Name: Intr_CreateOpportunity_WebService - Web Service to Create Opportunity and Opportunity Team
//
// March 02 2016, Shreerath Nair  Original (T-517306)
// 
// August 22 2016, Update(I-231399) Shreerath Nair - Added function to check the Profile Id of User
global  class Intr_CreateOpportunity_WebService {
	
	//webservice for creating Quick Opportunity with an Opportunity Team Member
	webService static sObject createOpportunity(String OpportunityName,Id Owner,Id Account){
		
		//create Opportunity
    	Opportunity opportunity = new Opportunity(Name = OpportunityName,
    											  ownerId = Owner,
    											  AccountId= Account,
    											  StageName='In Development',
    											  Sub_Stage__c='Awaiting Renewal Docs',
    											  CloseDate = date.today());
      		
      	insert opportunity;
      	
      	//create Opportunity Team
      	OpportunityTeamMember opportunityTeamMember  = new OpportunityTeamMember(UserId=UserInfo.getUserId(),
      																			 TeamMemberRole='ProCare Rep',
      																			 OpportunityId= opportunity.ID);
        
        insert OpportunityTeamMember;
      		
      	return opportunity;
    	
	}
	
	
	//webservice to check the profile Id of the current User
	webService static sObject checkProfile(Id ProfileId){
		
		
		return ([SELECT Id, Name FROM Profile WHERE Id = :ProfileId])[0];	
		
	
	}
    
}