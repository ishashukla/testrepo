// 
// (c) 2016 Appirio, Inc.
//
// T-500180, This is a test class for Intr_UpdateCaseStatusBatch class
//
// 12 May, 2016  Shreerath Nair Original  

/*
 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
*/

@isTest(seealldata=false)
private class Intr_UpdateCaseStatusBatchTest {
    
  public static List<Case> caseList  = new List<Case>();

  public static testMethod void testUpdateCaseStatus() {
      
    Profile pro = [SELECT Id,Name FROM Profile WHERE Name='Instruments CCKM']; 
    List<User> us = Intr_TestUtils.createUser(1,pro.Name,true);
    System.runAs(us[0]) {
        
        createTestdata();
        Test.startTest();
      
            Intr_UpdateCaseStatusBatch batch = new Intr_UpdateCaseStatusBatch(); // Batch class
            database.executebatch(batch);
            
        Test.stopTest();
        
        list<Case> checkCaseList = [SELECT Id,Status,Close_Notes__c
                                          FROM Case where status = 'closed'];
        System.debug('>>Caselist' +  checkCaseList); 
                             
        system.assertEquals('Automatically Closed Due to Customer Inactivity',checkCaseList[0].Close_Notes__c);
      
      
    
    }
  }
  
  
  public static void createTestData(){
      
      Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
      Account acc = Intr_TestUtils.createAccount('test',RecordTypeId,true);
      List<Contact> lstContact = Intr_TestUtils.createContact(1,acc.id,true);
      caseList = Intr_TestUtils.createCase(1,acc.id,lstContact[0].id,false);
      //caseList[0].RecordTYpeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_NON_PRODUCT).getRecordTypeId();
      //Modified By Chandra Shekhar Sharma to add record types on case Object
	  Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Instruments - Employee Complaint');
	  TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Instruments - Employee Complaint', 'Instruments');
	  caselist[0].recordTypeId = rtByName.getRecordTypeId();
      caselist[0].status = Intr_Constants.INSTRUMENTS_CCKM_CASE_STATUS;
      caselist[0].Last_Status_Change__c = System.now();
      insert caselist;
      insert new Instruments_CCKM_Common_Setting__c(Auto_Case_Closure_Delay_Days__c = -2, Enable_Auto_Case_Closure__c = true);

  }
}