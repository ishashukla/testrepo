/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 15, 2013
 Purpose       :  REST Webservice for saving Intel tags, Chatter reply and likes
 Reference     :  Task T-205160
 Modifications :  Issue I-87335    Dec 20, 2013    Sonal Shrivastava
*******************************************************************************/
@RestResource(urlMapping='/IntelReplyService/*') 
global with sharing class IntelReplyService {
  
  private static final String ERROR = 'Error : ';
  private static final String INVALID_DATA = 'Invalid Data';
  private static final String SAVE_SUCCESS = 'Records saved successfully';
  private static final String NO_RECORDS_FOUND = 'No records found';
  
  //****************************************************************************
  // POST Method for REST service
  //****************************************************************************  
  @HttpPost 
  global static Map<String, String> doPost() {
    RestRequest req = RestContext.request;
    Map<String, String> res = new Map<String,String>();
    String result;    
    try {   
      //parse the requestBody into a string
      if(req.requestBody != null) {                
        Blob reqBody = req.requestBody;   
        res = parseRecords(reqBody.toString());      
      } 
      else{
      	res.put(ERROR, INVALID_DATA);
      	res.put('requestBody', req.requestBody.toString());
      }
    }
    catch(Exception ex){
      res.put('error', ex.getMessage());
      res.put('requestBody', req.requestBody.toString());
      return res;
    }    
    return res;
  }
  
  //****************************************************************************
  // Method for getting Intel and child records from the Request string
  //****************************************************************************
  private static Map<String, String> parseRecords(String reqBody){
  	Map<String, String> mapCommentId_LikeId = new Map<String, String>();
  	String result;
    IntelListWrapper wrap = (IntelListWrapper)JSON.deserialize(reqBody, IntelListWrapper.class);
        
    Map<String, Intel__c> mapIntelId_Intel                = new Map<String, Intel__c>();  
    Map<String, List<Tag__c>> mapIntelId_TagList          = new Map<String, List<Tag__c>>();
    Map<String, List<FeedComment>> mapIntelId_CommentList = new Map<String, List<FeedComment>>();
    Map<String, List<FeedLike>> mapIntelId_LikeList       = new Map<String, List<FeedLike>>();  
    
    try{ 
      if(wrap != null && wrap.intelList != null && wrap.intelList.size() > 0){ 
        for(IntelListWrapper.IntelJson intelwrap : wrap.intelList){
          //Add Intel records in a list
          mapIntelId_Intel.put(intelWrap.id, new Intel__c(Id = intelWrap.id,
                                                          Flagged_As_Inappropriate__c = (intelwrap.flagged == null) ? false : intelwrap.flagged,
                                                          Chatter_Post__c = intelWrap.chatterPost));
          //Add Tag records List in a map 
          if((intelwrap.tags != null) && (intelwrap.tags.size() > 0)){
            Tag__c tag;
            for(IntelListWrapper.TagJson tagWrap : intelwrap.tags){
              tag = new Tag__c(Account__c = tagWrap.Account,
                               Contact__c = tagWrap.Contact,
                               Product__c = tagWrap.Product,
                               User__c    = tagWrap.User,
                               Intel__c   = intelWrap.id );
              if(!mapIntelId_TagList.containsKey(intelWrap.id)){
                mapIntelId_TagList.put(intelWrap.id, new List<Tag__c>());
              }
              mapIntelId_TagList.get(intelWrap.id).add(tag);                                  
            }
          } 
          //Add Feed Comment records List in a map 
          if((intelwrap.postComments != null) && (intelwrap.postComments.size() > 0)){
          	FeedComment postComment;
          	for(IntelListWrapper.CommentJson  comment : intelWrap.postComments){
          		postComment = new FeedComment(CommentBody = comment.body,
		          		                          CreatedById = comment.postedById,
		          		                          CreatedDate = comment.createdDate);
          		if(!mapIntelId_CommentList.containsKey(intelWrap.id)){
                mapIntelId_CommentList.put(intelWrap.id, new List<FeedComment>());
              } 
              mapIntelId_CommentList.get(intelWrap.id).add(postComment);                            
          	}
          }
          //Add Feed Like records List in a map
          if((intelwrap.postLikes != null) && (intelwrap.postLikes.size() > 0)){
            FeedLike postLike;
            for(IntelListWrapper.LikeJson  lik : intelWrap.postLikes){
              postLike = new FeedLike(CreatedById = lik.postedById);
              if(!mapIntelId_LikeList.containsKey(intelWrap.id)){
                mapIntelId_LikeList.put(intelWrap.id, new List<FeedLike>());
              } 
              mapIntelId_LikeList.get(intelWrap.id).add(postLike);                            
            }
          }   
        }
      }
      mapCommentId_LikeId = insertRecords(mapIntelId_Intel, mapIntelId_TagList, mapIntelId_CommentList, mapIntelId_LikeList); 
    }
    catch(Exception ex){
    	mapCommentId_LikeId.put(ERROR, ex.getMessage() + ' on line ' + ex.getLineNumber());
        return mapCommentId_LikeId;
    }
    return mapCommentId_LikeId;
  }
  
  //***************************************************************************/
  // Method for saving Intel and its child records
  //***************************************************************************/
  private static Map<String, String> insertRecords(Map<String, Intel__c> mapIntelId_Intel,  
																	    Map<String, List<Tag__c>> mapIntelId_TagList,
																	    Map<String, List<FeedComment>> mapIntelId_CommentList,
																	    Map<String, List<FeedLike>> mapIntelId_LikeList){
	Map<String, String> mapCommentId_LikeId = new Map<String, String>();																    	
    String result;
    Map<String, String> mapIntelId_TagNames = new Map<String, String>();
    List<Tag__c> tagList = new List<Tag__c>();
    List<FeedComment> commentList = new List<FeedComment>();
    List<FeedLike> likeList = new List<FeedLike>();
    Savepoint sp = Database.setSavepoint();
    try{
    	 //Update Intel records
      if(mapIntelId_Intel.size() > 0){
        update mapIntelId_Intel.values();
        //Insert Tag records
        if(mapIntelId_TagList.size() > 0){
          for(String intelId : mapIntelId_TagList.keySet()){
          	for(Tag__c tag : mapIntelId_TagList.get(intelId)){
          	  tagList.add(tag);	
          	}
          } 
          if(tagList.size() > 0){         
            insert tagList;
            System.debug('tagList --->>> ' + tagList);
          }          
          //Query tags for Tag Name
          for(Tag__c tag : [SELECT Id, Name, Title__c, Intel__c FROM Tag__c WHERE Id IN :tagList order by Name]){
          	if(!mapIntelId_TagNames.containsKey(tag.Intel__c)){
          		mapIntelId_TagNames.put(tag.Intel__c, '\n\n ' + Label.RELATED_TAGS + ' ' + tag.Title__c);
          	}else{
          	  mapIntelId_TagNames.put(tag.Intel__c, mapIntelId_TagNames.get(tag.Intel__c) + ', ' + tag.Title__c);
          	}
          }
        }
        //Insert FeedComment records
        if(mapIntelId_CommentList.size() > 0){
        	for(String intelId : mapIntelId_CommentList.keySet()){
        		if(mapIntelId_Intel.get(intelId).Flagged_As_Inappropriate__c != true){
        			
	        		String relatedTags = mapIntelId_TagNames.containsKey(intelId) ? mapIntelId_TagNames.get(intelId) : '';
	        		String chatterPostId = mapIntelId_Intel.get(intelId).Chatter_Post__c; 
	        		
	        		if(chatterPostId != null && chatterPostId != ''){ 
		            for(FeedComment comment : mapIntelId_CommentList.get(intelId)){              
		              comment.FeedItemId  = chatterPostId;
		              comment.CommentBody = comment.CommentBody + relatedTags; 
		              commentList.add(comment); 
		            }
	        		}
        		}
          } 
          if(commentList.size() > 0){         
            insert commentList;
          }
        }
        //Insert FeedLike records
        if(mapIntelId_LikeList.size() > 0){
          for(String intelId : mapIntelId_LikeList.keySet()){
          	if(mapIntelId_Intel.get(intelId).Flagged_As_Inappropriate__c != true){          		
	          	
	          	String chatterPostId = mapIntelId_Intel.get(intelId).Chatter_Post__c; 
	          	
	          	if(chatterPostId != null && chatterPostId != ''){ 
		            for(FeedLike lik : mapIntelId_LikeList.get(intelId)){   
		            	lik.FeedItemId = chatterPostId;           
		              likeList.add(lik); 
		            }
	          	}
	          } 
	          if(likeList.size() > 0){         
	            insert likeList;
	          }
          }
        }       
        result = SAVE_SUCCESS;
      }
      else{
        result = NO_RECORDS_FOUND;
      }
      mapCommentId_LikeId.put('response', result);
      getNewCommentIds(commentList, likeList, mapCommentId_LikeId);
    }catch(Exception ex){
    	Database.rollback(sp);
    	mapCommentId_LikeId.put(ERROR, ex.getMessage() + ' on line ' + ex.getLineNumber());
      return mapCommentId_LikeId;
    }
    return mapCommentId_LikeId;
  }  
  
  //****************************************************************************
  // Method for fetching chatter post Ids
  //****************************************************************************
  private static void getNewCommentIds(List<FeedComment> commentList, List<FeedLike> likeList, 
                                                       Map<String, String> mapCommentId_LikeId){
    for(FeedComment feedComment : commentList) {
    	mapCommentId_LikeId.put('CommentId', feedComment.Id);
    }
    for(FeedLike feedLike : likeList) {
    	mapCommentId_LikeId.put('LikeId', feedLike.Id);
    }
  }
  
}