@isTest
public class BatchDeleteDataTest{
    static testMethod void deleteAccount(){
        System.debug ('Start');
        
        //Profile p = [SELECT Id FROM Profile WHERE Name='Endo - Tech Support'];
        Profile p = TestUtils.fetchProfile('Endo - Tech Support');
        //User u = [select Id, Name,  IsActive, usertype, UserName, ProfileId from User where IsActive = true and ProfileId = :p.id LIMIT 1];
        User u =TestUtils.createUser(1,p.Name,false).get(0);
        u.IsActive=true;
        insert u;
        
        System.runas(u) {
     
        //Create account
            Account a = new Account(
                Name = 'Test Account',
                OwnerId = u.Id,
                CreatedBy = u,
                Endo_Active__c=True);
                insert a; 
                //System.debug('Here is the account id ' + a.id);
                //System.debug('Created By ' + a.CreatedBy);    
                //System.debug('Owner ' + a.OwnerId);
            
            /*User__c cs = User__c.getValues('Id');
            if(cs == null){
                cs = new User__c(Name='BatchDeleteData');
                cs.Id(u.Id);
                
            insert cs;
            }*/
            
            User__c setting = new User__c();
            setting.Name = 'BatchDeleteData';
            setting.User__c = u.Id; 
            insert setting;   
            
            User__c myCS1 = User__c.getValues('BatchDeleteData');
            String myUserVal = myCS1.User__c;
            
            //System.debug('myUserVal  ' + myUserVal );
            BatchDeleteData bData = new BatchDeleteData();
            bData.query='Select Id From Account Limit 200';
            Database.executeBatch(bData); 
        }
    }
}