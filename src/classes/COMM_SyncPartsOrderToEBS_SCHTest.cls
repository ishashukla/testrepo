// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncPartsOrderToEBS_SCHTest -  Test class 
//
// 2 Nov 2016, Isha Shukla (T-550285) 
@isTest
private class COMM_SyncPartsOrderToEBS_SCHTest {
    static Account acc;
    @isTest(SeeAllData=true)
    static void testSchedulablePartsOrder() {
        createData();
        String CRON_EXP = '0 0 23 * * ?';
        CronTrigger ct;
        Test.startTest();
        COMM_SyncPartsOrderToEBS_SCH obj =new COMM_SyncPartsOrderToEBS_SCH();
        String jobId = System.schedule('ScheduleApexClassTest',CRON_EXP,new COMM_SyncPartsOrderToEBS_SCH());
        ct = [SELECT Id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP,ct.CronExpression);
        Test.stopTest();
    }
    private static void createData() {
        acc = TestUtils.createAccount(1, true).get(0);
        Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM Change Request').getRecordTypeId();
        Map<String, Schema.RecordTypeInfo> partOrderInfo =
            Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
        String PORecordTypeId = partOrderInfo.get('RMA').getRecordTypeId();
        if(!(RTMap__c.getInstance(PORecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = PORecordTypeId,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='RMA');
            insert rtMap;
        }
        if(!(RTMap__c.getInstance(caseRecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = caseRecordTypeId,
                                          Object_API_Name__c='Case',
                                          Division__c = 'COMM',
                                          RT_Name__c='COMM Change Request');
            insert rtMap;
        }
        Case caseRecord = new Case(RecordTypeId = caseRecordTypeId,Accountid = acc.Id);
        insert caseRecord;
        system.debug('>>>>caseacc='+caseRecord.Accountid);
        List<SVMXC__RMA_Shipment_Order__c> partsOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
        for(Integer i = 0; i < 2; i++){
            SVMXC__RMA_Shipment_Order__c stock = new SVMXC__RMA_Shipment_Order__c(
                SVMXC__Case__c = caseRecord.Id,SVMXC__Order_Status__c = 'Closed',
                SVMXC__Company__c = acc.Id,RecordTypeId = PORecordTypeId
            );
            partsOrderList.add(stock);
        }
        insert partsOrderList;    
    }
}