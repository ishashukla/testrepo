/***********************************************************************************
 Author        :  Appirio JDC (Kajal Jalan)
 Date          :  May 05, 2016
 Purpose       :  Constant Class to be used in Intr_CaseTriggerHandlerTest (T-500173)
************************************************************************************/
public class Intr_Constants {
    
    public static final String INSTRUMENTS_ACCOUNT_RECORD_TYPE = 'Instruments Account Record Type'; 
    public static final String INSTRUMENTS_CONTACT_RECORD_TYPE = 'Instruments Contact'; 
    public static final String INSTRUMENTS_CASE_RECORD_TYPE_NON_PRODUCT = 'Instruments - Non Product Issue'; 
    public static final String INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT = 'Instruments - Potential Product Complaint'; 
    public static final String INSTRUMENTS_CASE_RECORD_TYPE_EMPLOYEE_COMPLAINT = 'Instruments - Employee Complaint'; 
    public static final String INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_DEPOT = 'Instruments - Repair (Depot)'; 
    public static final String INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_FIELD_SERVICE = 'Instruments - Repair (Field Service)'; 
    public static final String INSTRUMENTS_Opportunity_RECORD_TYPE = 'Instruments Opportunity Record Type'; 
    public static final string INSTRUMENTS_CCKM_PROFILE = 'Instruments CCKM';
    public static final string GOVERNMENT_USER_PROFILE = 'Government Users';
    public static final string INSTRUMENTS_CCKM_ADMIN_PROFILE = 'Instruments CCKM Admin';
    public static final string SYSTEM_ADMIN_PROFILE = 'System Administrator';
    public static final string INSTRUMENTS_CCKM_CASE_STATUS = 'Follow Up Required (from Customer)';
    public static final string INSTRUMENTS_PROCARE_SALES_PROFILE = 'Instruments ProCare Sales Ops';
    public static final string INSTRUMENTS_SALES_PROFILE = 'Instruments Sales Ops';
    public static final String MEDICAL_ACCOUNT_RECORD_TYPE_MEDICAL_CALL_CENTER = 'Medical Call Center';
    public static final String MEDICAL_ACCOUNT_RECORD_TYPE_MEDICAL_PLACEHOLDER = 'Medical Placeholder Account';
    public static final String MEDICAL_CONTACT_RECORD_TYPE_MEDICAL_CUSTOMER = 'Medical Customer Contact';
    public static final String INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP = 'ProCare Rep';
    public static final String INSTRUMENTS_ACCOUNT_TEAM_ROLE_SURGICAL = 'Surgical';
    public static final String MEDICAL_CASE_RECORD_TYPE_POTENTIAL_PRODUCT = 'Medical - Potential Product Complaint';
    public static final String INSTRUMENTS_OPPORTUNITY_RECORD_TYPE_CLOSED_WON = 'Instruments Closed Won Opportunity';
}