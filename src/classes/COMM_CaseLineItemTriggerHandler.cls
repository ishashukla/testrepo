/**================================================================      
* Appirio, Inc
* Name: COMM_CaseLineItemTriggerHandler
* Description: I-214565 - Populate Price field based on List Price from the related Product object (Part field) based on the COMM Price book List Price.
* Created Date: 21st April 2016
* Created By: Kirti Agarwal (Appirio)
*

* 15th September 2016    Nitish Bansal      Update (I-235331) // Restrict trigger to allow only COMM Price Book products to get associated.
==================================================================*/

public class COMM_CaseLineItemTriggerHandler {

  public static String COMM_PRICE_BOOK = 'COMM Price Book';
  
  //================================================================      
  // Name         : beforeInsertAndUpdate
  // Description  : Used on before insert and update of order record
  // Created Date : 7th April 2016 
  // Created By   : Kirti Agarwal (Appirio)
  // Task         : I-214565
  //==================================================================
  public static void beforeInsertAndUpdate(List < Case_Line_Item__c > orderList, Map < Id, Case_Line_Item__c  > oldMap) {
    Set<Id> productIds = new Set<Id>();
    Id priceBook2IdVal = null;
    Map<Id, Decimal> productAndPriceMap = new Map<Id, Decimal>();    
    
    for(Case_Line_Item__c casLine : orderList) {
      if(casLine.Part__c != null) {
        productIds.add(casLine.Part__c);
      }
    }
    
    //add where condition with "COMM Price Book" record type Id 
    for(PriceBook2 pBook : [SELECT Id FROM PriceBook2 Where Name =: COMM_PRICE_BOOK]) {
       priceBook2IdVal = pBook.id;
    }
    
    //created map for product and its price
    for(PriceBookEntry entry : [SELECT Id, UnitPrice, Product2Id  
                                  FROM PriceBookEntry 
                                 WHERE Product2Id IN :productIds 
                                   AND PriceBook2Id = :priceBook2IdVal
                                   AND IsActive = true]) {   // NB - 09/16 - I-235331 - Added isActive check
      productAndPriceMap.put(entry.Product2Id, entry.UnitPrice);
    }
    
    for(Case_Line_Item__c casLine : orderList) {
      if(casLine.Part__c != null  
          && (oldMap == null || (oldMap.get(casLine.id).Part__c != casLine.Part__c))) {
        if(productAndPriceMap.containsKey(casLine.Part__c)) {
          casLine.Price__c = productAndPriceMap.get(casLine.Part__c);
        } else {
        // NB - 09/15 - I-235331 - Start
          //casLine.Price__c = 0.0;
          casLine.addError(Label.Only_COMM_Price_Book_Products_are_allowed);
        // NB - 09/15 - I-235331 - End  
        }
      }
    } 
  }
}