// (c) 2016 Appirio, Inc.
//
// Class Name: Intr_OpportunityTriggerHandler- Class to create Scheduled Revenue for Closed Won Opportunity
//
// July 26 2016, Shreerath Nair  Original (T-517306)
@isTest
private class Intr_OpportunityTriggerHandler_Test {

   static Account acc;
   static Opportunity opp;
   static List<User> usrList;
    
    static testMethod void testScheduledRevenueForOpportunity() {
    	
    	usrList = TestUtils.createUser(1,'Instruments ProCare Sales Ops',false);
        usrList.get(0).Division = 'ProCare';
        usrList.get(0).Title = 'Instruments';
        insert usrList;
        System.runAs(usrList[0]) {
           
        	acc = Intr_TestUtils.createAccount(1, true).get(0); 
        	Test.startTest(); 
        	
        	    Map<String,Schema.RecordTypeInfo> mapOpportunityInfo = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        	    ID InstrumentRecordTypeId = mapOpportunityInfo.get(Intr_Constants.INSTRUMENTS_Opportunity_RECORD_TYPE).getRecordTypeId();
         		opp = Intr_testUtils.createOpportunity(1,acc.Id,false).get(0);
         		opp.RecordtypeId=InstrumentRecordTypeId;
         		opp.closeDate = date.Today();
         		opp.Business_Unit__c = 'ProCare';
         		opp.OwnerId = usrList.get(0).Id;
         		opp.Contract_Start_Date__c = date.Today();
         		opp.Service_Amount__c=5000;
                opp.Infection_Control_Value__c = 1000;
                opp.IVS_Value__c = 1000;
                opp.Navigation_Value__c = 1000;
                opp.Neptune_Value__c = 500;
                opp.NSE_Value__c = 500;
                opp.Software_Value__c = 500;
                opp.Surgical_Value__c = 500;
         		opp.Term__c=10;
    	 		insert opp; 
    	 		FF_OpportunityTriggerHandler.skipOppTrigger =false;
    			opp.Term__c=5;
    			update opp;
    			
    			Scheduled_Revenue__c[] scheduleList = [select Id from Scheduled_Revenue__c where Opportunity__c =: opp.Id];
    			system.debug('shree>>'+scheduleList);
        		System.assertEquals(35,scheduleList.size());
        	Test.stopTest(); 
        } 
    }
}