/**================================================================      
* Appirio, Inc
* Name: DecomissionAssetExtension
* Description: extension for Custom Button of decomission asset on Installed Product
* Created Date: 30-Sept-2016
* Created By: Varun Vasishtha (Appirio)
*
* Date Modified      Modified By      Description of the update

==================================================================*/
public without sharing class DecomissionAssetExtension {
    public SVMXC__Installed_Product__c assetInstance {get;set;}
    public boolean IsSuccess {get;set;}
    public string ChildAsset{get;set;}
    public DecomissionAssetExtension(ApexPages.StandardController cnt)
    {
         
        assetInstance  = [select Id,SVMXC__Status__c,Inactivate_Child_Assets__c from SVMXC__Installed_Product__c where Id = :cnt.getId()];
        ChildAsset = assetInstance.Inactivate_Child_Assets__c;
    }
    
    public void SaveCustom()
    {
        if(string.isNotBlank(ChildAsset))
        {
            assetInstance.SVMXC__Status__c = 'Terminated';
            assetInstance.Inactivate_Child_Assets__c = ChildAsset;
            update assetInstance;
            IsSuccess = true;
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: You have to select something in Inactive Child Asset option');
            ApexPages.addMessage(myMsg);
            IsSuccess = false;
        }
    }
    
    public List<SelectOption> getItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));
        return options;
    }
}