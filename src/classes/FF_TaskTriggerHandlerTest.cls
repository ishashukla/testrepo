/*******************************************************************************
 Author        :  Appirio (Sunil)
 Date          :  June 20, 2014
 Purpose       :  Test Class for FF_TaskTriggerHandlerTest
*******************************************************************************/
@isTest
private class FF_TaskTriggerHandlerTest{

  public static testmethod void testMethod1(){
    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Type = 'Hospital';
    acc.Legal_Name__c = 'Test';
    acc.Flex_Region__c = 'test';
    insert acc;

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    insert con;
	
    Id flexRTOnlyConventional = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();
    Opportunity opp = new Opportunity();
    opp.StageName = 'Quoting';
    opp.AccountId = acc.Id;
    opp.CloseDate = Date.newinstance(2014,5,29);
    opp.Name = 'test value';
    opp.RecordTypeId = flexRTOnlyConventional;
    insert opp;

    Flex_Contract__c contract = new Flex_Contract__c();
    contract.Account__c = acc.Id;
    contract.Opportunity__c = opp.Id;
    contract.Name = opp.Name;
    contract.Scheduled_Maturity_Date__c = System.today();
    insert contract;

    // insert custom setting
    Flex_Sales_Rep__c salesRep = new Flex_Sales_Rep__c();
    salesRep.RFM_Username__c = UserInfo.getUserName();
    salesRep.Name = 'test';
    insert salesRep;

    Task t = new Task();
    t.OwnerId = UserInfo.getUserId();
    t.Subject ='Test';
    t.Status ='Not Started';
    t.Priority='Normal';
    t.WhatId = contract.Id;
    //t.WhoId = UserInfo.getUserId();
    insert t;

    Test.stopTest();
  }
}