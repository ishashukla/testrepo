/**=====================================================================
 * Appirio, Inc
 * Name: StrategicAccountNoteDeleteController, created for COMM
 * Description: I-215437
 * Created Date: 05/03/2016
 * Created By: Rahul Aeran
 *
=====================================================================*/
public with sharing class StrategicAccountNoteDeleteController {
    String noteId,accountId;
    public StrategicAccountNoteDeleteController(ApexPages.StandardController sc){
        noteId = ApexPages.currentPage().getParameters().get('id');
        accountId = ApexPages.currentPage().getParameters().get('accId'); 
    }
    public PageReference redirectAfterDelete(){
        system.debug('AccountId : ' + accountId +  '::Note ID : ' + noteID);
        if(noteId != null){
          List<Action_Note__c> notes = [SELECT AccountName__c, Id from Action_Note__c WHERE Id = :noteId];
          if(notes != null && notes.size() > 0)
          accountId = notes.get(0).AccountName__c;
          delete notes;
        }
        if(accountId != null){
           return new PageReference('/'+accountId);
        }else{
           return new PageReference('/001/o');
        }
    }
}