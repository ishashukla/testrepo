/**================================================================      
 * Appirio, Inc
 * Name: ValueAddActivitiesTriggerHandlerTest
 * Description: Test class for ValueAddActivitiesTriggerHandler(T-538445), added methods testaddASRUser
 * Created Date: [09/20/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 * Nov 04, 2016       Prakarsh Jain    Added method testPopulateBusinessUnit and testNotPopulateBusinessUnit(I-241684)
 * Nov 10, 2016       Prakarsh Jain    Added method testPopulateAccountOnOpportunity and testPopulateAccountOnContact(T-554454)
 ==================================================================*/
@isTest
public class ValueAddActivitiesTriggerHandlerTest {
  public static Id oppInstrumentsRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type').getRecordTypeId();
  public static Id conInstrumentsRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact').getRecordTypeId();
  public static Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
  public static List<User> salesRepUserList;
  public static List<User> ASRUserList;
  public static List<User> managerUserList;
  static Account account;
  static User adminUser;
  //Method to check Value_Add_Activities__c record is shared with ASR user
  static testMethod void testaddASRUser(){
    Test.startTest();
      insertUser(True,false);
    Test.stopTest();
    //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=ASRUserList[0].Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
    //insert terr;
    System.runAs(adminUser) {
      Mancode__c mancode = TestUtils.createMancode(1, ASRUserList[0].Id, null, false).get(0);
      mancode.Name = '000000';
      mancode.Type__c = 'ASR';
      //mancode.Territory__c = terr.Id;
      insert mancode;
      ASR_Relationship__c asrUser = new ASR_Relationship__c();
      asrUser.Mancode__c = mancode.Id;
      asrUser.ASR_User__c = salesRepUserList[0].Id;
      insert asrUser;
    }
    Value_Add_Activities__c value = new Value_Add_Activities__c();
    value.OwnerId = salesRepUserList[0].Id;
    value.Business_Unit__c = 'NSE';
    system.runAs(salesRepUserList[0]){
          insert value;
    }
    List<Value_Add_Activities__Share> valeAddActivityShareList = [SELECT Id, ParentId FROM Value_Add_Activities__Share WHERE ParentId =: value.Id];
    system.assertEquals(true, valeAddActivityShareList.size()>1);
  }
  
  static testMethod void testaddASRUserUpdate(){
    Test.startTest();
      insertUser(True,false);
    Test.stopTest();
    //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=ASRUserList[0].Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
    //insert terr;
    System.runAs(adminUser) {
      Mancode__c mancode = TestUtils.createMancode(1, ASRUserList[0].Id, null, false).get(0);
      mancode.Name = '000000';
      mancode.Type__c = 'ASR';
      //mancode.Territory__c = terr.Id;
      insert mancode;
      Mancode__c mancodeASR = TestUtils.createMancode(1, ASRUserList[1].Id, null, false).get(0);
      mancodeASR.Name = '000001';
      mancodeASR.Type__c = 'ASR';
      //mancodeASR.Territory__c = terr.Id;
      insert mancodeASR;
      ASR_Relationship__c asrUser = new ASR_Relationship__c();
      asrUser.Mancode__c = mancode.Id;
      asrUser.ASR_User__c = salesRepUserList[0].Id;
      insert asrUser;
      ASR_Relationship__c asrUser2 = new ASR_Relationship__c();
      asrUser2.Mancode__c = mancodeASR.Id;
      asrUser2.ASR_User__c = salesRepUserList[1].Id;
      insert asrUser2;
    }
    Value_Add_Activities__c value = new Value_Add_Activities__c();
    value.OwnerId = salesRepUserList[0].Id;
    value.Business_Unit__c = 'NSE';
    system.runAs(salesRepUserList[0]){
        insert value;
        value.OwnerId = salesRepUserList[1].Id;
        update value;
    
    }
    List<Value_Add_Activities__Share> valeAddActivityShareList = [SELECT Id, ParentId, RowCause FROM Value_Add_Activities__Share 
                                                                  WHERE ParentId =: value.Id AND RowCause =: Schema.Value_Add_Activities__Share.RowCause.Manual];
    system.assertEquals(true, valeAddActivityShareList.size()==1);
  }
  
  //Method to check Business unit is populated on Value Add Activities Record.
  static testMethod void testPopulateBusinessUnit(){
    Test.startTest();
      insertUser(false,false);
    Test.stopTest();
    System.runAs(adminUser) {
      Mancode__c mancode = TestUtils.createMancode(1, salesRepUserList[0].Id, null, false).get(0);
      mancode.Name = '000000';
      mancode.Type__c = 'Sales Rep';
      mancode.Business_Unit__c = 'NSE';
      insert mancode;
    }
    Value_Add_Activities__c value = new Value_Add_Activities__c();
    value.OwnerId = salesRepUserList[0].Id;
    value.Business_Unit__c = 'NSE';
    
    system.runAs(salesRepUserList[0]){
        insert value;
    }
    String bu = [SELECT Id, Name, Business_Unit__c FROM Value_Add_Activities__c WHERE Id =: value.Id].Business_Unit__c;
    system.assertEquals('NSE', bu);
  }
  
  //Method to check Value Add Activities Record is not saved if the user does not belong to a particular BU.
  static testMethod void testNotPopulateBusinessUnit(){
    Test.startTest();
      insertUser(false,true);
    Test.stopTest();
    System.runAs(adminUser) {
      Mancode__c mancode = TestUtils.createMancode(1, salesRepUserList[0].Id, managerUserList[0].Id, false).get(0);
      mancode.Name = '000000';
      mancode.Business_Unit__c = 'NSE';
      mancode.Type__c = 'Sales Rep';
      insert mancode;
      Mancode__c mancodeIVS = TestUtils.createMancode(1, salesRepUserList[0].Id, managerUserList[1].Id, false).get(0);
      mancodeIVS.Name = '000001';
      mancodeIVS.Business_Unit__c = 'IVS';
      mancodeIVS.Type__c = 'Sales Rep';
      insert mancodeIVS;
    }
    Value_Add_Activities__c value = new Value_Add_Activities__c(); 
    value.OwnerId = salesRepUserList[0].Id;
    value.Business_Unit__c = 'Surgical';
    system.runAs(salesRepUserList[0]){
        try{
          insert value;
        }catch(Exception ex){
          system.assertEquals('Please select a correct Business Unit', 'Please select a correct Business Unit');
        }
    
    }
  }
  
  //Test Method to check account is populated on the value add record when created from contact
  static testMethod void testPopulateAccountOnContact(){
    Test.startTest();
      insertUser(false,true);
    Test.stopTest();
    System.runAs(adminUser) {
      account = TestUtils.createAccount(1,false).get(0);
      account.RecordTypeId = rtByName.getRecordTypeId();
      insert account;
      AccountTeamMember atm = TestUtils.createAccountTeamMember(1, account.Id, salesRepUserList[0].Id, true).get(0);
      Mancode__c mancode = TestUtils.createMancode(1, salesRepUserList[0].Id, managerUserList[0].Id, false).get(0);
      mancode.Name = '000000';
      mancode.Business_Unit__c = 'NSE';
      mancode.Type__c = 'Sales Rep';
      insert mancode;
    }
    system.runAs(salesRepUserList[0]){
      List<Contact> contactList = TestUtils.createContact(200, account.Id, false);
	    List<Contact> contactToInsertList = new List<Contact>();
	    for(Contact contact : contactList){
	      contact.RecordTypeId = conInstrumentsRecTypeId;
	      contact.Allow_Create__c = true;
	      contactToInsertList.add(contact);
	    }
	    insert contactToInsertList;
     
		    List<Value_Add_Activities__c> valueList = new List<Value_Add_Activities__c>(); 
		      for(Integer i=0; i<200; i++){
		        Value_Add_Activities__c value = new Value_Add_Activities__c(OwnerId = salesRepUserList[0].Id,Business_Unit__c = 'NSE',
		                                                                    Contact__c = contactToInsertList.get(i).Id);
		        valueList.add(value);                                                            
		      }
		      insert valueList;
		      Set<Id> valueAddActivitiesId = new Set<Id>();
		      for(Value_Add_Activities__c vale : valueList){
		        valueAddActivitiesId.add(vale.Id);
		      }
		    
		    for(Value_Add_Activities__c val : [SELECT Id,Account__c FROM Value_Add_Activities__c WHERE Id IN: valueAddActivitiesId]){
		      system.assertEquals(val.Account__c, account.Id);
		    }
    }
  }
  
  //Test Method to check account is populated on the value add record when created from opportunity
  static testMethod void testPopulateAccountOnOpportunity(){
    Test.startTest();
      insertUser(false,true);
    Test.stopTest();
    System.runAs(adminUser) {
      account = TestUtils.createAccount(1,false).get(0);
      account.RecordTypeId = rtByName.getRecordTypeId();
      insert account;
      AccountTeamMember atm = TestUtils.createAccountTeamMember(1, account.Id, salesRepUserList[0].Id, true).get(0);
      Mancode__c mancode = TestUtils.createMancode(1, salesRepUserList[0].Id, managerUserList[0].Id, false).get(0);
      mancode.Name = '000000';
      mancode.Business_Unit__c = 'NSE';
      mancode.Type__c = 'Sales Rep';
      insert mancode;
    }
    system.runAs(salesRepUserList[0]){
      List<Opportunity> opportunityList = TestUtils.createOpportunity(200, account.Id, false);
	    List<Opportunity> opportunityToInsertList = new List<Opportunity>();
	    for(Opportunity opportunity : opportunityList){
	      opportunity.RecordTypeId = oppInstrumentsRecTypeId;
	      opportunity.Business_Unit__c = 'NSE';
	      opportunity.StageName = 'In Development';
	      opportunity.RecordTypeId = oppInstrumentsRecTypeId;
	      opportunityToInsertList.add(opportunity);
	    }
	    insert opportunityToInsertList;
		    List<Value_Add_Activities__c> valueList = new List<Value_Add_Activities__c>(); 
		      for(Integer i=0; i<200; i++){
		        Value_Add_Activities__c value = new Value_Add_Activities__c(OwnerId = salesRepUserList[0].Id,Business_Unit__c = 'NSE',
		                                                                    Opportunity__c = opportunityToInsertList.get(i).Id);
		        valueList.add(value);                                                            
		      }
		      insert valueList;
		      Set<Id> valueAddActivitiesId = new Set<Id>();
		      for(Value_Add_Activities__c vale : valueList){
		        valueAddActivitiesId.add(vale.Id);
		      }
		    
		    for(Value_Add_Activities__c val : [SELECT Id,Account__c FROM Value_Add_Activities__c WHERE Id IN: valueAddActivitiesId]){
		      system.assertEquals(val.Account__c, account.Id);
		    }
    }
  }
  @future
  public static void InsertUser(Boolean ASR,Boolean Manager) {
    adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
    insert adminUser;
    salesRepUserList = TestUtils.createUser(2, 'Instruments Sales User', false);
    salesRepUserList[0].Division = 'NSE;IVS';
    salesRepUserList[1].Division = 'NSE;IVS';
    salesRepUserList[0].Title = 'Sales Rep';
    salesRepUserList[1].Title = 'Sales Rep';
    insert salesRepUserList;
    if(ASR) {
      ASRUserList = TestUtils.createUser(2, 'Instruments ASR User', false);
      ASRUserList[0].Division = 'NSE';
      ASRUserList[1].Division = 'NSE';
      ASRUserList[0].Title = 'ASR';
      ASRUserList[1].Title = 'ASR';
      insert ASRUserList;
    }
    if(Manager) {
      managerUserList = TestUtils.createUser(2, 'Instruments Sales Manager', false);
      managerUserList[0].Division = 'NSE';
      managerUserList[1].Division = 'IVS';
      managerUserList[0].Title = 'RM';
      managerUserList[1].Title = 'RM';
      insert managerUserList;
    }
  }
}