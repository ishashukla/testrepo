/**================================================================      
* Appirio, Inc
* Name: COMM_CreateOrderExtensionTest
* Description: Test class for COMM_CreateOrderExtension
* Created By: Pragya Awasthi (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
public class Comm_CreateOrderExtensionTest {
    private static List<Account> accounts;
    private static List<Contact> contacts;
    private static Order ord;
    private static Case cs;
    
    static testMethod void testFirst(){
        createTestData(4);
        Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        ord = new Order(AccountId = accounts[0].Id,RecordTypeId = rtOrder, EffectiveDate = date.today(), Status='Entered');
        insert ord;
        PageReference pg = null;
        Test.startTest();
        
        
        PageReference pageRef = Page.Comm_CreateOrder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('order_id',ord.Id);
        ApexPages.currentPage().getParameters().put('RecordType' , ord.RecordTypeId);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        
        
        Comm_CreateOrderExtension orderExtensions = new Comm_CreateOrderExtension(sc);
        pg = orderExtensions.initCurrentOrder();
        
        Test.stopTest();
        system.assertEquals(pg.getUrl().contains(ord.Id),true);
        
    }
    static testMethod void testSecond(){
        createTestData(4);
        
        
        Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        ord = new Order(AccountId = accounts[0].Id,RecordTypeId = rtOrder, EffectiveDate = date.today(), Status='Entered');
        insert ord;
        
        Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        cs = TestUtils.createCase(1,accounts[0].Id , false).get(0);
        cs.RecordTypeId = rtCase;
        insert cs;
        contacts = TestUtils.createContact(1,accounts[0].Id , true);
        PageReference pg = null;
        Test.startTest();
        PageReference pageRef = Page.Comm_CreateOrder;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('contact_id',contacts.get(0).Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
     
        Comm_CreateOrderExtension orderExtensions = new Comm_CreateOrderExtension(sc);
        pg = orderExtensions.initCurrentOrder();
        
        Test.stopTest();
        system.assertEquals(pg.getUrl().contains(contacts.get(0).Id),true);
    }
    
    static testMethod void testThird(){
        createTestData(4);
        
        Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        ord = new Order(AccountId = accounts[0].Id,RecordTypeId = rtOrder, EffectiveDate = date.today(), Status='Entered');
        insert ord;
        
        Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        cs = TestUtils.createCase(1,accounts[0].Id , false).get(0);
        cs.RecordTypeId = rtCase;
        insert cs;
        contacts = TestUtils.createContact(1,accounts[0].Id , true);
        PageReference pg = null;
        Test.startTest();
        PageReference pageRef = Page.Comm_CreateOrder;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('account_id',accounts.get(0).Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        
        
        Comm_CreateOrderExtension orderExtensions = new Comm_CreateOrderExtension(sc);
        pg = orderExtensions.initCurrentOrder();
        Test.stopTest();
         system.assertEquals(pg.getUrl().contains(accounts.get(0).Id),true);
    }
    
    static testMethod void testFourth(){
        createTestData(4);
        
        Id rtOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        ord = new Order(AccountId = accounts[0].Id,RecordTypeId = rtOrder, EffectiveDate = date.today(), Status='Entered');
        insert ord;
        
        Id rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        cs = TestUtils.createCase(1,accounts[0].Id , false).get(0);
        cs.RecordTypeId = rtCase;
        insert cs;
        contacts = TestUtils.createContact(1,accounts[0].Id , true);
        PageReference pg = null;
        Test.startTest();
        
        
        PageReference pageRef = Page.Comm_CreateOrder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('case_id',cs.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        
        
        Comm_CreateOrderExtension orderExtensions = new Comm_CreateOrderExtension(sc);
        pg= orderExtensions.initCurrentOrder();
        Test.stopTest();
         system.assertEquals(pg.getUrl().contains(cs.Id),true);
    }
    private static void createTestData(Integer count){
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        accounts = TestUtils.createAccount(1, false);
        accounts[0].Billing_Type__c ='Bill To Address Only';
        accounts[0].COMM_Shipping_Country__c = 'United States';
        accounts[0].COMM_Shipping_State_Province__c = 'Armed Forces Europe';
        accounts[0].COMM_Billing_Country__c='United States';
        accounts[0].COMM_Billing_State_Province__c='Armed Forces Europe';
        accounts[0].COMM_Billing_City__c='New York';
        accounts[0]. COMM_Billing_Address__c='test';
        accounts[0].COMM_Billing_PostalCode__c='208011';
        accounts[0].COMM_Shipping_Address__c='test';
        accounts[0].COMM_Shipping_City__c='New York';
        accounts[0].COMM_Shipping_PostalCode__c='208011';
        accounts[0].RecordTypeId = accountRTId;
        accounts[0].Endo_Active__c = true;
        insert accounts;
        //  Id OrderrecordTypeId = Order.sObjectType.getDescribe().getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
        COMMBPConfigurations__c comms = new COMMBPConfigurations__c(Ship_To_Location__c='CF00Nc0000001QrPD',
                                                                    Bill_to_Location__c='CF00Nc0000001QrOZ',
                                                                    Case_On_Order__c='CF00Nc0000001QsCt',
                                                                    Contact_On_Order__c='CF00Nc0000001QrP3');
        List<State_and_Country__c> stateandCountryList = new  List<State_and_Country__c>();
        State_and_Country__c sc1= new State_and_Country__c(Name='India',
                                                           SFDC_State__c='',
                                                           SFDC_Country_Code__c='IN' ,
                                                           SFDC_State_Code__c='',
                                                           SFDC_Country__c='India');
        State_and_Country__c sc3= new State_and_Country__c(Name='United States-AE',
                                                           SFDC_State__c='Armed Forces Europe',
                                                           SFDC_Country_Code__c='US' ,
                                                           SFDC_State_Code__c='AE',
                                                           SFDC_Country__c='United States');
        stateandCountryList.add(sc1);
        stateandCountryList.add(sc3);
        insert stateandCountryList;
        string rtCase2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase2,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        
    }
    
}