/**================================================================      
* Appirio, Inc
* Name: COMM_cycleCount_TriggerManagerTest
* Description: Test class for COMM_cycleCount_TriggerManager
* Created Date: 16 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class COMM_cycleCount_TriggerManagerTest {
    static List<Cycle_Count__c> cycleCountList;
    static List<Cycle_Count_Line__c> cycleCountLineList;
    @isTest static void testSubmitForApprovalProcess() {
        createData();
        Test.startTest();
        for(integer i=0;i<3;i++) {
          cycleCountList[i].Submit_for_Approval__c = true;
        }
        update cycleCountList;
        Test.stopTest();
        Cycle_Count__c count = [SELECT CreatedDate FROM Cycle_Count__c WHERE Id = :cycleCountList[0].Id];
        ProcessInstance pi = [SELECT TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :cycleCountList[0].Id];
        System.assertEquals(count.CreatedDate.Date() ,pi.CreatedDate.Date());
    }
    @isTest static void testExceptionInApprovalProcess() {
        Cycle_Count__c cycleCount = new Cycle_Count__c(Submit_for_Approval__c = false);
        insert cycleCount;
        Test.startTest();
        cycleCount.Submit_for_Approval__c = true;
        update cycleCount;
        Test.stopTest();
        System.assertEquals([SELECT CreatedDate FROM ProcessInstance WHERE TargetObjectId = :cycleCount.Id].size(),0);
    }
    @isTest static void testCreate_StockProduct() {
        createData();
        Test.startTest();
        for(integer i=0;i<3;i++) {
          cycleCountList[i].Status__c = 'Approved';
        }
        update cycleCountList;
        Test.stopTest();
        System.assertEquals([SELECT Id FROM SVMXC__Stock_Adjustment__c WHERE Cycle_Count_Line__c = :cycleCountLineList[2].Id].size() > 0, true);
    }
    private static void createData() {
        User userRecord = TestUtils.createUser(1, 'Field Service Technician - COMM US', true).get(0);
        cycleCountList = new List<Cycle_Count__c>();
        for(integer i=0;i<3;i++) {
            Cycle_Count__c cycleCountRecord = new Cycle_Count__c(
                Submit_for_Approval__c = false,
                Status__c = 'New',
                Technician_Salesforce_user__c = userRecord.Id
            );
            cycleCountList.add(cycleCountRecord);
        }
        insert cycleCountList;
        cycleCountLineList = new List<Cycle_Count_Line__c>();
        for(integer i=0;i<3;i++) {
        Cycle_Count_Line__c cycleCountLine = new Cycle_Count_Line__c(
            Cycle_Count__c = cycleCountList[i].Id
        );
          cycleCountLineList.add(cycleCountLine);  
        }
        insert cycleCountLineList;
    }
}