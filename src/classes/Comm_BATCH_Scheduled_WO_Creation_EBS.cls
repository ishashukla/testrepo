/**================================================================      
* Appirio, Inc
* Name: [Comm_BATCH_Scheduled_WO_Creation_EBS]
* Description: [batch Class for Callin EBS ]
* Created Date: [14-09-2016]
* Created By: [Gaurav Sinha] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 14 Sep 2016        Gaurav Sinha      T-509402
==================================================================*/
public class Comm_BATCH_Scheduled_WO_Creation_EBS implements Database.Batchable <SObject>, Database.Stateful, Schedulable {
    public List<Id> WORKDETAIL_ID = new List<Id>();
    public List<Id> WORKORDER_ID = new List<Id>();
    public String query = 'SELECT SVMXC__Service_Order__r.SVMXC__Case__r.Accountid, SVMXC__Billable_Quantity__c, ' +
        'SVMXC__Billable_Line_Price__c,SVMXC__Product__c ' +
        'FROM SVMXC__Service_Order_Line__c ' +
        'WHERE SVMXC__Line_Status__c = \'Completed\' AND recordtype.developername=\'UsageConsumption\' '+
        'AND Order__c = null AND Order_Product__c = null AND SVMXC__Billable_Quantity__c !=0';
    
    /*
    * @method:  Comm_BATCH_Scheduled_WO_Creation_EBS
    * @param : query : to reinitalize the query string
    * @Description: Constructor for reintialze the Query.
    */
    public Comm_BATCH_Scheduled_WO_Creation_EBS(String query) {
        this.query = query;
    }
    
    /*
    * @method:  execute
    * @param : sc : SchedulableContext
    * @Description: Method implemented for the interface Schedulable. It calls the batch class for implementing the desired functionality.
    */
    public void execute(SchedulableContext sc) {
        if (!Test.isRunningTest()) {
            Id batchId = Database.executeBatch(new Comm_BATCH_Scheduled_WO_Creation_EBS(query), 20);
            //System.debug('\n\n==> batchId = ' + batchId);
        }
    }
    
    /*
    * @method:  Comm_BATCH_Scheduled_WO_Creation_EBS
    * @param : query : to reinitalize the query string
    * @Description: Constructor for reintialze the Query.
    */
    public Comm_BATCH_Scheduled_WO_Creation_EBS(){}
    
    /*
    * @method:  finish
    * @param : context : Database.BatchableContext
    * @Description: Method implemented for the interface Schedulable
    */
    public void finish(Database.BatchableContext context) {
        String batchName =  'Comm_BATCH_Scheduled_WS_CALLOUT';
        DateTime dt = system.now().addMinutes(1);
        String sch = dt.second() + ' ' + dt.minute() + ' ' + dt.hour() + 
            ' ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        deleteCompletedJobs(batchName);
        boolean isBatchScheduled = checkIfBatchJobScheduled(batchName);
        if(!isBatchScheduled){          
            system.schedule(batchName + dt, sch,  new Comm_BATCH_Scheduled_WS_CALLOUT(WORKDETAIL_ID,WORKORDER_ID));
        }   
    }
    
    public void deleteCompletedJobs(string batchName){
        list<CronTrigger> jobs = [SELECT State, Id 
                                  FROM CronTrigger
                                  WHERE State = 'DELETED' 
                                  AND CronJobDetail.Name LIKE :batchName + '%'];
        for(CronTrigger ct : jobs){
            system.abortJob(ct.Id);    
        }
    }
    
    public boolean checkIfBatchJobScheduled(string batchName){
        Integer runningBatchCount = [SELECT count() 
                                     FROM CronTrigger
                                     WHERE State != 'DELETED' 
                                     AND CronJobDetail.Name like :batchName + '%' ];                
        if(runningBatchCount > 0){
            return true;
        }
        return false;
    }
    
    /*
    * @method:  start
    * @param : context : Database.BatchableContext
    * @Description: Method implemented for the interface Batchable to get the records on which the batch processing needs to be applied.
    */
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(query);
    }
    
    /*
    * @method:  execute
    * @param : context : Database.BatchableContext : Batch Context
    * @param : objects : List<SVMXC__Warranty__c> : List of obejct returned in the batch context.
    * @Description: 
    */
    public void execute(Database.BatchableContext context, List < SVMXC__Service_Order_Line__c > objects) {
        Map<Id, List <SVMXC__Service_Order_Line__c>> mapWorkOrderToLineItem = new Map<Id, list <SVMXC__Service_Order_Line__c>>();
        
        for (SVMXC__Service_Order_Line__c varloop: objects) {
            if (mapWorkOrderToLineItem.get(varloop.SVMXC__Service_Order__c) == null)
                mapWorkOrderToLineItem.put(varloop.SVMXC__Service_Order__c, new List<SVMXC__Service_Order_Line__c>{varloop});
            else{
                mapWorkOrderToLineItem.get(varloop.SVMXC__Service_Order__c).add(varloop);
            }
        }
        if (!mapWorkOrderToLineItem.isEmpty()){
            createOrder(mapWorkOrderToLineItem);
        }
    }

    Private  void createOrder(Map < Id, List < SVMXC__Service_Order_Line__c >> items) {
        set < id > ordertoBeinserted = new set < id > ();
        //order varorder = new Order();
        pricebook2 pb = [select id from Pricebook2 where name = 'COMM Price Book'];
        order varorder = new Order();
        map < id, Order > liVarOrder = new map < id, Order > ();
        map < id,OrderItem > liVarOrderItem = new map <id, OrderItem > ();
        Set < id > productid = new set < id > ();
        OrderItem varorderItems = new OrderItem();
        for (id var: items.keyset()) {
            for (SVMXC__Service_Order_Line__c varloop: items.get(var)) {
                ordertoBeinserted.add(varloop.id);
                //System.debug('@ID' + varloop.id);
                
                //System.debug('@AccountId' + varloop.SVMXC__Service_Order__r.SVMXC__Case__r.Accountid);
                varorder.Accountid = varloop.SVMXC__Service_Order__r.SVMXC__Case__r.Accountid;
                varorder.EffectiveDate = system.today();
                varorder.Status = 'Entered';
                varOrder.Pricebook2id = pb.id;
                varOrder.recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId();
                productid.add(varloop.SVMXC__Product__c);
            }
            if(varorder.Accountid !=null){
                liVarOrder.put(var, Varorder);
            }
            varOrder = new Order();
        }
           System.debug('==>productid'+productid);
        if (!liVarOrder.isEmpty()) {
            COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA = true;
            System.debug('==>liVarOrder'+liVarOrder);
            insert liVarOrder.values();
            // Retriving the information for the Pricebookentry
            map < string, PricebookEntry > pbe = new map < String, PricebookEntry > ();
            for (PricebookEntry varloop1: [SELECT id, product2id, unitprice
                                           FROM PricebookEntry
                                           WHERE product2id in : productid
                                           AND pricebook2id = : pb.id]) {
                pbe.put(varloop1.product2id, varloop1);
            }
            System.debug('==>pbe.vaslues()'+pbe);
            for (id  var: items.keyset()) {
                integer i=1;
                for (SVMXC__Service_Order_Line__c varloop: items.get(var)) {
                    if (pbe.get(varloop.SVMXC__Product__c) == null || liVarOrder.get(var) == null)
                        continue;
                    varorderItems.Quantity = varloop.SVMXC__Billable_Quantity__c == 0 ? 1 : varloop.SVMXC__Billable_Quantity__c ;
                    varorderItems.pricebookentryid = pbe.get(varloop.SVMXC__Product__c).id;
                    varorderItems.UnitPrice = varloop.SVMXC__Billable_Line_Price__c==0 ? 0 :varloop.SVMXC__Billable_Line_Price__c;
                    varorderItems.orderid = liVarOrder.get(var).id;
                    varorderItems.Status__c = 'Entered';
                    varorderItems.Oracle_Order_Line_Number__c = ''+i;
                    liVarOrderItem.put(varloop.id,varorderItems);
                    varorderItems = new orderItem();
                    i++;
                }
            }
            System.debug('==>liVarOrderItem.values()'+liVarOrderItem);
            System.debug('==>liVarOrderItem.values()'+liVarOrderItem.values());
            if (!liVarOrderItem.isEmpty()) {
                insert liVarOrderItem.values();
            }
            List<SVMXC__Service_Order_Line__c> toupdate = new List<SVMXC__Service_Order_Line__c>();
            for (id  var: items.keyset()) {
                for (SVMXC__Service_Order_Line__c varloop: items.get(var)) {
                    if(liVarOrderItem.get(varLoop.id) == null )
                        continue;
                    varloop.order__c= liVarOrderItem.get(varLoop.id).orderid;
                    varloop.order_product__c= liVarOrderItem.get(varLoop.id).id;
                    toupdate.add(varloop);
                    WORKORDER_ID.add(varloop.SVMXC__Service_Order__c);
                    WORKDETAIL_ID.add(varloop.id);
                }
                
            }
            update toupdate;
        }
    }
}