// (c) 2015 Appirio, Inc.
//
// Class Name: NotifyAccountsBasedOnContractsTest
// Description: Test Class for NotifyAccountsBasedOnContracts   
// April 07, 2016    Meena Shringi    Original 
//
@isTest(SeeAllData = true) 
public class NotifyAccountsBasedOnContractsTest {
    static testMethod void testNotifyAccountsBasedOnContracts(){
        test.startTest();
        Account accountObj = TestUtils.createAccount(1, true).get(0);
        RecordType recType = [Select Id From RecordType  Where SobjectType = 'Contract' and DeveloperName = 'Instruments'];
        Id recordTypeInstrument = recType.Id;
        List<Contract> lstContract = TestUtils.createContracts(10,accountObj.Id,'Service Contract',recordTypeInstrument,true);
        System.debug(lstContract.get(0).EndDate+'**********');
        
        lstContract.addAll(TestUtils.createContracts(10,accountObj.Id,'Local Rep Contract',recordTypeInstrument,true));        
        List<Account> acctList = new List<Account>();   
        Database.QueryLocator QL;
        Database.BatchableContext BC; 
        NotifyAccountsBasedOnContracts notification = new NotifyAccountsBasedOnContracts();
        Database.executeBatch(notification);         
        system.assert(notification.chatterPostList.size()>=0,'chatter must posted for records with record type instrument');
        
        test.stopTest();
    }
}