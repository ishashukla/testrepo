/**================================================================      
* Appirio, Inc
* Name: NV_ProductPricingController.cls
* Description: Controller for the lightning component for product
*              pricing
* Created Date: 03-AUG-2016
* Created By: Ray Dehler (Appirio)
*
* Date Modified      Modified By      Description of the update
* 03 AUG 2016        Ray Dehler       Added header and added support for Instruments S-423983
==================================================================*/
public with sharing class NV_ProductPricingController {
	public static Mobile_ProductPricing productPricing;

	static {
		try {
			String className = Product_Pricing_Settings__c.getInstance().Controller_Class__c;
			// confirm it can be instantiated
			productPricing = (Mobile_ProductPricing)Type.forName(className).newInstance();
		} catch(Exception e) {
			String className = Mobile_Utility.DEFAULT_CONTROLLER_CLASS;
			System.debug('Unable to pull in class from custom setting, going with default of '+className);
			productPricing = (Mobile_ProductPricing)Type.forName(className).newInstance();
		}
		
	}

	@AuraEnabled
	public static Boolean isBigDataMode() {
		return productPricing.isBigDataMode();
	}
	
	@AuraEnabled
	public static List<Account> getMyCustomerAccounts(){
		return productPricing.getMyCustomerAccounts();
	}

	@AuraEnabled
	public static List<NV_Wrapper.ProductLineData> getAllProducts() {
		return productPricing.getAllProducts();
	}

	@AuraEnabled
	public static List<NV_Wrapper.ProductData> getProductsForSegment(String segment, String productLine) {
		return productPricing.getProductsForSegment(segment, productLine);
	}

	@AuraEnabled
	public static List<NV_Wrapper.ProductLineData> searchForProducts(String searchText) {
		return productPricing.searchForProducts(searchText);
	}

	@AuraEnabled
	public static List<NV_Wrapper.ModelNPricing> retrievePricing(String customerId, String productIdsCSV){
		return productPricing.retrievePricing(customerId, productIdsCSV);
	}
    
    @AuraEnabled
	public static List<User> getFollowableUsers(){
		return productPricing.getFollowableUsers();
	}
}