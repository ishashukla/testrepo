@isTest
private class COMM_OrderItemTriggerHandlerTestClass {
    
    private static testMethod void test() {
        
        List<Account> ac= TestUtils.createAccount(1,false);
        insert ac;
        //NB - 11/7 - I-242624 - Start
        list<User> user = TestUtils.createUser(2,'Customer Service - COMM INTL', true);
        Contact con1 = TestUtils.createCOMMContact(ac.get(0).id, 'TestContact', false);
        con1.Email = 'test@nic.in';
        insert con1;
        //NB - 11/7 - I-242624 end
        List<Product2> prdt=TestUtils.createCOMMProduct(8, false); //NB - 11/30 - I-242624
        prdt[0].inventory_item_id__c = '123456'; //NB - 11/7 - I-242624
        prdt[0].Product_Class_Code__c = '001'; //NB - 11/7 - I-242624
        prdt[1].Product_Class_Code__c = '002'; //NB - 11/8 - I-242624
        prdt[2].Product_Class_Code__c = '003'; //NB - 11/8 - I-242624
        prdt[3].Product_Class_Code__c = '005'; //NB - 11/8 - I-242624
        prdt[4].Product_Class_Code__c = '006'; //NB - 11/8 - I-242624
        prdt[5].Product_Class_Code__c = '007'; //NB - 11/8 - I-242624
        prdt[6].Product_Class_Code__c = '009'; //NB - 11/8 - I-242624
        prdt[7].Product_Class_Code__c = '050'; //NB - 11/8 - I-242624
        insert prdt;
        //NB - 11/7 - I-242624 end
        List<Project_Plan__c> ProPlan=TestUtils.createProjectPlan(1,ac[0].Id,false);
        insert ProPlan;
        List<Project_Phase__c> ProPhase=TestUtils.createProjectPhase(2,ProPlan[0].Id,false);
        insert ProPhase;
        Id pricebookId = Test.getStandardPricebookId();
        //NB - 11/8 - I-242624 - start
        PricebookEntry standardPrice = new PricebookEntry();
        List<PricebookEntry> standardPriceList = new List<PricebookEntry>();
        for(Integer i = 0 ; i<8; i++){
            standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prdt[i].Id, UnitPrice = 10000, IsActive = true);
            standardPriceList.add(standardPrice);
        }
        insert standardPriceList;
        standardPrice = standardPriceList[0];
        //NB - 11/8 - I-242624 end
        
        //NB - 11/7 - I-242624 - Start
        List<Pricebook2> pricebooks= TestUtils.createPriceBook(2,'COMM Price Book',false);  //NB - 11/15 - I-243909
        pricebooks[0].name = 'COMM Price Book';
        pricebooks[0].IsActive = true;
        pricebooks[0].currencyisocode = 'USD';
        pricebooks[1].name = 'Endo List Price'; //NB - 11/15 - I-243909
        pricebooks[1].IsActive = true; //NB - 11/15 - I-243909
        pricebooks[1].currencyisocode = 'USD'; //NB - 11/15 - I-243909
        insert pricebooks;
        //NB - 11/15 - I-243909 Start
        
        List<Order> orderlists= TestUtils.createOrder(3,ac[0].Id,'Entered',false); //NB - 11/15 - I-243909
        orderlists[0].RecordTypeId = Constants.getRecordTypeId('COMM Order', 'Order');
        orderlists[0].PriceBook2Id = pricebooks[0].Id;
        orderlists[0].Tracked_in_Install_Base__c = false;
        orderlists[0].currencyisocode = 'USD'; //NB - 11/7 - I-242624
        orderlists[1].RecordTypeId = Constants.getRecordTypeId('COMM Order', 'Order'); //NB - 11/7 - I-242624
        orderlists[1].PriceBook2Id = pricebooks[0].Id; //NB - 11/7 - I-242624
        orderlists[1].Tracked_in_Install_Base__c = false; //NB - 11/7 - I-242624
        orderlists[1].currencyisocode = 'USD'; //NB - 11/7 - I-242624
        orderlists[0].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624
        orderlists[1].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624
        orderlists[2].currencyisocode = 'USD'; //NB - 11/15 - I-243909
        orderlists[2].RecordTypeId = Constants.getRecordTypeId('Endo Orders', 'Order'); //NB - 11/15 - I-243909
        orderlists[2].PriceBook2Id = pricebooks[1].Id; //NB - 11/15 - I-243909
        insert orderlists;
        
        
        List<Pricebookentry> tempPBEList = new List<Pricebookentry>(); 
        Pricebookentry tempPBE = new Pricebookentry();
        tempPBE.Pricebook2id = pricebooks[0].Id ;
        tempPBE.Product2Id = prdt[0].Id;
        tempPBE.CurrencyIsoCode = 'USD';
        tempPBE.UnitPrice = 1.00 ;
        tempPBE.UseStandardPrice = false;//NB - 11/15 - I-243909
        tempPBEList.add(tempPBE);
        tempPBE = new Pricebookentry();
        tempPBE.Pricebook2id = pricebooks[1].Id ;
        tempPBE.Product2Id = prdt[0].Id;
        tempPBE.CurrencyIsoCode = 'USD';
        tempPBE.UnitPrice = 1.00 ;
        tempPBE.UseStandardPrice = false;//NB - 11/15 - I-243909
        tempPBEList.add(tempPBE);
        insert tempPBEList;
        //NB - 11/15 - I-243909 End
        List<OrderItem> orderitems=TestUtils.createOrderItem(8,ProPhase[0].Id,orderlists[0].Id,standardPrice.Id,false);
        orderitems[0].PricebookEntryId=tempPBEList[0].Id;//NB - 11/15 - I-243909
        orderitems[1].PricebookEntryId=tempPBEList[0].Id;//NB - 11/15 - I-243909
        orderitems[2].PricebookEntryId=tempPBEList[0].Id;//NB - 11/15 - I-243909
        orderitems[3].PricebookEntryId=tempPBEList[0].Id;//NB - 11/15 - I-243909
        orderitems[5].PricebookEntryId=tempPBEList[0].Id;//NB - 11/15 - I-243909
        orderitems[6].PricebookEntryId=tempPBEList[0].Id;//NB - 11/15 - I-243909
        orderitems[7].PricebookEntryId=tempPBEList[0].Id;//NB - 11/15 - I-243909
        
        orderitems[0].Status__c='Entered';
        orderitems[1].Status__c='Entered';
        orderitems[2].Status__c='Entered';
        orderitems[3].Status__c='Entered';
        orderitems[4].Status__c='Entered';
        orderitems[5].Status__c='Entered';
        orderitems[6].Status__c='Entered';
        orderitems[7].Status__c='Entered';
        
        orderitems[0].Oracle_Order_Line_Number__c='1.1';
        orderitems[1].Oracle_Order_Line_Number__c='1.1.1';
        orderitems[2].Oracle_Order_Line_Number__c='1.0';
        orderitems[3].Oracle_Order_Line_Number__c='1.21';
        orderitems[4].Oracle_Order_Line_Number__c='21.1';
        orderitems[4].Oracle_Inventory_Item_Id__c = 123456;
        orderitems[4].PricebookEntryId = null;
        //NB - 11/8 - I-242624 - end
        orderitems[0]. Project_Phase__c = ProPhase[1].Id; //NB - 11/7 - I-242624
        
        orderitems[1].Approval_Status__c = 'Requested'; //NB - 11/7 - I-242624
        
        orderitems[4].Approval_Status__c = 'Requested'; //NB - 11/7 - I-242624
        orderitems[3].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624
        orderitems[4].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624   
        //NB - 11/8 - I-242624 Start
        Test.startTest();    
        Database.SaveResult[] results = Database.insert(orderitems, false); 
        if(results != null){
            Integer index = 0;
            for(Database.SaveResult result : results) {
                if(!result.isSuccess()) {
                    system.assert(false, 'RECORD : ' + orderitems.get(index) + 'ERROR::' + result.getErrors());
                }
                index++;
            }
        }
        //NB - 11/8 - I-242624 end              
        //insert orderitems; //NB - 11/15 - I-243909
        //NB - 11/7 - I-242624 - Start
        List <SVMXC__Installed_Product__c> instProdList = TestUtils.createInstalledProducts(1, prdt[0].id, orderlists[0].id, 'QA', false); //NB - 11/7 - I-242624
        instProdList[0].Order_Product__c = orderitems[0].Id;
        insert instProdList;
        //NB - 11/7 - I-242624 - end
        orderitems[2].Oracle_Order_Line_Number__c='2.0';//NB - 11/8 - I-242624
        orderitems[0].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624
        orderitems[1].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624
        orderitems[2].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624
        orderitems[3].Project_plan__c = ProPlan[0].id; //NB - 11/7 - I-242624
        orderitems[0].Project_Phase__c = ProPhase[0].Id; //NB - 11/8 - I-242624
        orderitems[3].Approval_Status__c = 'Requested'; //NB - 11/7 - I-242624
        orderitems[1].Approval_Status__c = 'Reviewed'; //NB - 11/7 - I-242624
        orderitems[2].Requested_Ship_Date__c = System.today().addDays(30); //NB - 11/7 - I-242624
        
        //Database.upsert(orderitems, false); //NB - 11/8 - I-242624
        update orderitems;       //NB - 11/15 - I-243909
        Test.stopTest();
        //NB - 11/8 - I-242624 Start
        List<OrderItem> deleteOrderItemlists = new List<OrderItem>();
        for(OrderItem oli : orderitems){
            if(oli.Id != null){
                deleteOrderItemlists.add(oli);
            }
        }
        delete deleteOrderItemlists;
        //NB - 11/8 - I-242624 end
        Order orderRec = [SELECT Id, Tracked_in_Install_Base__c FROM Order WHERE Id =: orderlists[0].id limit 1];
        
        System.assertEquals(false, orderRec.Tracked_in_Install_Base__c);
    }
    @isTest static void testNotification(){
        Id pricebookId = Test.getStandardPricebookId();
        Product2 prd1 = new Product2(Name = 'Test Product Entry 1', Description = 'Test Product Entry 1', isActive = true);
        insert prd1;
        PricebookEntry pe = new PricebookEntry(UnitPrice = 1, Product2Id = prd1.id, Pricebook2Id = pricebookId, isActive = true);
        insert pe;
        List < Account > accountList = TestUtils.createAccount(1, true);
        TestUtils.createCommConstantSetting();List < Order > orderList = TestUtils.createOrder(100, accountList[0].id, 'Entered', false);
        orderList[0].RecordTypeId = Constants.getRecordTypeId('COMM Order', 'Order');
        orderList[0].PriceBook2Id = pricebookId;
        insert orderList[0];
        Test.startTest();
        OrderItem oi = new OrderItem(OrderId = orderList[0].id, Quantity = decimal.valueof('1'), UnitPrice = 1,PricebookEntryId = pe.id);
        oi.Status__c='Entered';
        oi.Tracking_Numbers__c = '123234';
        insert oi;
        oi.Status__c='Shipped'; 
        oi.Tracking_Numbers__c = '123';
        update oi;
        Test.stopTest();
        System.assertEquals([SELECT Tracking_Numbers__c FROM OrderItem WHERE Id = :oi.Id].Tracking_Numbers__c != null,true);
    }

    @isTest
  static void COMM_OrderItemTriggerHandlerTest() {
    Id pricebookId = Test.getStandardPricebookId();

    Product2 prd1 = new Product2(Name = 'Test Product Entry 1', Description = 'Test Product Entry 1', isActive = true);
    insert prd1;

    PricebookEntry pe = new PricebookEntry(UnitPrice = 1, Product2Id = prd1.id, Pricebook2Id = pricebookId, isActive = true);
    insert pe;

    List < Account > accountList = TestUtils.createAccount(1, true);
    TestUtils.createCommConstantSetting();
    List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
    List < Project_Plan__c > projectPlanList = TestUtils.createProjectPlan(1, accountList[0].id, true);
    List < Order > orderList = TestUtils.createOrder(100, accountList[0].id, 'Entered', false);
    orderList[0].RecordTypeId = Constants.getRecordTypeId('COMM Order', 'Order');
    orderList[0].OpportunityId = oppList[0].id;
    orderList[0].Tracked_in_Install_Base__c = true;
    orderList[0].PriceBook2Id = pricebookId;
    insert orderList[0];
    

    OrderItem oi = new OrderItem(OrderId = orderList[0].id, Quantity = decimal.valueof('1'), UnitPrice = 1,PricebookEntryId = pe.id);
    oi.Status__c='Entered'; //NB - 11/7 - I-242624
    insert oi;
    Order orderRec = [SELECT Id, Tracked_in_Install_Base__c FROM Order WHERE Id =: orderList[0].id limit 1];

    System.assertEquals(false, orderRec.Tracked_in_Install_Base__c);

  }
     
}