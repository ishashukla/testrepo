/**================================================================      
* Appirio, Inc
* Name: COMM_TaskTriggerHandler 
* Description: Trigger handler on Task
* Created Date: 03/25/2016
* Created By: Deepti Maheshwari (T-487405) (Appirio)
// Modified Date            Modified By             Purpose
// 09 Aug,2016              Sahil Batra             (T-505650) Added method populateAccountonTask to populate account Id on task created on Contact.
// 9th Aug,2016             Jagdeep Juneja          (T-508465) updated populateAccountonTask method 
// 08/26/2016               Meghna Vijay            To populate RelatedTo field of Task (Issue Ref:-I-229980)
// 2nd Nov 2016             Nitish Bansal           I-241409 Moved the record type query from each method to single RT query and used the same map across all the functions. Moved RT value to constants class
==================================================================*/
public with sharing class COMM_TaskTriggerHandler {

    Static Map<String, RecordTypeInfo> taskRTMap = Schema.SObjectType.Task.getRecordTypeInfosByName();  //NB - 11/02 - I-241409
  
  //----------------------------------------------------------------------------
  //  Method called on after insert of records
  //----------------------------------------------------------------------------
    public static void afterInsert(List<Task> newTasks) {
      updateDueDatesOnOppty(newTasks,null);
    }
    
    //----------------------------------------------------------------------------
  //  Method called on after Update of records
  //----------------------------------------------------------------------------
    public static void afterUpdate(List<Task> newTasks, Map<ID, Task> oldMap) {
      updateDueDatesOnOppty(newTasks, oldMap);
    }
    
    //Name             :     beforeInsert Method
    //Description      :     Method created when beforeInsert trigger is activated 
    //Created by       :     Meghna Vijay I-215112
    //Created Date     :     May 5, 2016
    public static void beforeInsert(List<Task> newTaskList) {
      flexContractExpiration(newTaskList);
      populateAccountonTask(newTaskList);
      populateRelatedToId(newTaskList);
      syncWhoIdField(newTaskList);
    }
    
    //----------------------------------------------------------------------------
  //  Method to update oppty records with due date on task
  //----------------------------------------------------------------------------
    private static void updateDueDatesOnOppty(List<Task> newTasks, Map<ID, Task> oldMap) {
    //  Map<String, RecordTypeInfo> taskRTMap = Schema.SObjectType.Task.getRecordTypeInfosByName(); //NB - 11/02 - I-241409
      Map<ID, Opportunity> opptyMap = new Map<ID, Opportunity>();
      List<Task> taskList = new List<Task>();
      Set<ID> opptyIDsOfUpdatedTask = new Set<ID>(); 
      for(Task tsk : newTasks){
        if(tsk.RecordTypeID == taskRTMap.get(Constants.COMM_OPPTY_TASK).getRecordTypeId()) {
          
          if(oldMap == null) {
            if(tsk.ActivityDate != null && (tsk.Subject == Constants.GROUND_BREAKING 
            || tsk.Subject == Constants.PRESENTATION
            || tsk.Subject == Constants.FIRST_CASES)) {
                taskList.add(tsk);
                opptyIDsOfUpdatedTask.add(tsk.WhatID);
            }
          }else {
            if((tsk.ActivityDate != null && tsk.ActivityDate != oldMap.get(tsk.id).ActivityDate)
            || (tsk.Subject != oldMap.get(tsk.id).Subject 
            && (tsk.Subject == Constants.GROUND_BREAKING 
          || tsk.Subject == Constants.PRESENTATION
          || tsk.Subject == Constants.FIRST_CASES))) {
            taskList.add(tsk);
            opptyIDsOfUpdatedTask.add(tsk.WhatID);
          }
          }       
        }
      }
      opptyMap = new Map<ID, Opportunity>([SELECT Ground_Breaking__c, First_Cases__c, 
                                           Presentation_Date__c,ID
                                           FROM Opportunity 
                                           WHERE Id in :opptyIDsOfUpdatedTask]);
    if(opptyMap.size() > 0) {
        for(Task tsk : taskList) {
          if(tsk.subject != null && tsk.subject == Constants.GROUND_BREAKING) {
            opptyMap.get(tsk.WhatId).Ground_Breaking__c = tsk.ActivityDate;
          }
          if(tsk.subject != null && tsk.subject == Constants.PRESENTATION) {
            opptyMap.get(tsk.WhatId).Presentation_Date__c = tsk.ActivityDate;
          }  
          if(tsk.subject != null && tsk.subject == Constants.FIRST_CASES) {
            opptyMap.get(tsk.WhatId).First_Cases__c = tsk.ActivityDate;
          } 
        }
        update opptyMap.values();
    }
    }
    
    //Name             :     flexContractExpiration Method
    //Description      :     Method created to set values of task field 
    //Created by       :     Meghna Vijay I-215112
    //Created Date     :     May 4, 2016
    private static void flexContractExpiration(List<Task> newTaskList) {
      for(Task t : newTaskList) {
        if(t.Subject == Constants.FLEX_CONTRACT) {
          t.IsReminderSet = true;
          t.RecordTypeID = Label.Task_Record_Type_Id;
        }
      }
    }
    
    // T-505650 Changes start - Method added to populate whoID with account when task is created on contact and whoid is null
    private static void populateAccountonTask(List<Task> newTaskList){
    //  String COMM_OPPTY_TASK = 'COMM Opportunity Task';
    //  String COMM_PROJECT_TASK = 'COMM Project Daily Report';
    //  String COMM_SITE_TASK = 'COMM Site Readiness';
    //    String COMM_TASK = 'COMM Task';   //NB - 11/02 - I-241409
    //    Map<String, RecordTypeInfo> taskRTMap = Schema.SObjectType.Task.getRecordTypeInfosByName(); //NB - 11/02 - I-241409
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id,Id> contacttoAccountIdMap = new Map<Id,Id>();
        for(Task task : newTaskList){
            if( /*(task.RecordTypeID == taskRTMap.get(COMM_OPPTY_TASK).getRecordTypeId())
                || (task.RecordTypeID == taskRTMap.get(COMM_PROJECT_TASK).getRecordTypeId())
                || (task.RecordTypeID == taskRTMap.get(COMM_SITE_TASK).getRecordTypeId())
                || */ (task.RecordTypeID == taskRTMap.get(Constants.COMM_TASK).getRecordTypeId())){
                    String whoId = task.whoId;
                    if(whoId!=null &&  whoId.substring(0,3) == '003' && task.whatId == null){
                        contactIdSet.add(task.whoId);
                    }
             }
        }
        if(contactIdSet.size() > 0){
            for(Contact con : [SELECT accountId FROM Contact WHERE Id IN :contactIdSet]){
                contacttoAccountIdMap.put(con.Id,con.accountId);
            }
        }
        //system.debug('>>>>>>>>>'+contacttoAccountIdMap);
        for(Task task : newTaskList){
            if( /*(task.RecordTypeID == taskRTMap.get(COMM_OPPTY_TASK).getRecordTypeId())
                || (task.RecordTypeID == taskRTMap.get(COMM_PROJECT_TASK).getRecordTypeId())
                || (task.RecordTypeID == taskRTMap.get(COMM_SITE_TASK).getRecordTypeId())
                || */ (task.RecordTypeID == taskRTMap.get(Constants.COMM_TASK).getRecordTypeId())){
                    String whoId = task.whoId;
                    //system.debug('>>>>>>>>>1');
                    //if(whoId.substring(0,3) == '003' && task.whatId == null){
                    //updated
                    if(task.whatId == null && whoId != null && whoId.substring(0,3) == '003'  ){
                        //system.debug('>>>>>>>>>2');
                        if(contacttoAccountIdMap.containsKey(task.whoId)){
                            //system.debug('>>>>>>>>>3');
                            task.whatId = contacttoAccountIdMap.get(task.whoId);
                        }
                    }
             }
        }
    }
    // T-505650 Changes End
    //==================================================================================================
    //Name             :     populateRelatedToId Method
    //Description      :     Method created to set RelatedTo field of Task
    //Created by       :     Meghna Vijay I-229980
    //Created Date     :     August 26, 2016
    //=================================================================================================
    private static void populateRelatedToId(List<Task> newTaskList) {
      Set<String> projectPhaseNumberSet = new Set<String>();
      Id projectPhaseId;
    //  Map<String, RecordTypeInfo> taskRTMap = Schema.SObjectType.Task.getRecordTypeInfosByName(); //NB - 11/02 - I-241409
      for(Task t : newTaskList) {
        if((t.RecordTypeId == taskRTMap.get(Constants.COMM_SITE_READINESS_RTYPE).getRecordTypeId())||(t.RecordTypeId == taskRTMap.get(Constants.COMM_PROJECT_LOG_RTYPE).getRecordTypeId())) {
          projectPhaseNumberSet.add(t.Phase_Number__c);  
        }
      }
      if(projectPhaseNumberSet.size()>0) {
          for(Project_Phase__c pp : [SELECT Id, Project_Phase_Number__c FROM Project_Phase__c WHERE Project_Phase_Number__c IN:projectPhaseNumberSet]) {
            projectPhaseId=pp.Id;
            
          }
      }
      if(projectPhaseId!=null) {
        for(Task t :newTaskList) {
          t.WhatId = projectPhaseId;
        }
      }
    }
    
  //----------------------------------------------------------------------------------------------
  //  Stryker Europe Field Service: Sync Related To field on Task when Work Order or Case is populated T-516695
  //----------------------------------------------------------------------------------------------
  public static void syncWhoIdField(List<Task> lstNew) { 
    //Id EUrecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('EU Tasks').getRecordTypeId();
    //Id COMMrecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('COMM Task').getRecordTypeId(); //NB - 11/02 - I-241409
    
    for(Task t :lstNew){
      if(t.RecordTypeId == taskRTMap.get(Constants.COMM_TASK).getRecordTypeId()){ //NB - 11/02 - I-241409
        if(t.Work_Order__c != null){
          t.WhatId = t.Work_Order__c;
        }
        else if(t.Case__c != null){
          t.WhatId = t.Case__c;
        }
        //Update 
        if(t.Salesperson_to_Email__c!= null){
          t.WhoId = t.Salesperson_to_Email__c; 
        }
        else if(t.WhoId != null){
          t.Salesperson_to_Email__c = t.WhoId; 
        }
      }
    }
  }
}