/**
** test class for order detail page button 
**/
@isTest
public class NV_OrderItemExtTest {
    static testMethod void TestForOrder(){
        // creating data
        NV_FedExRequestTest.createConfig();
        NV_FedExRequestTest.createOrderItem(); 
        Order selectedOrder = [Select Id FROM Order Limit 1]; // getting the order 
        ApexPages.StandardController con = new ApexPages.StandardController(selectedOrder); 
        Test.startTest();
            NV_OrderItemExt ext = new NV_OrderItemExt(con);
            System.assertNotEquals(null,ext.TrackOrder());
        Test.stopTest();
        
    }
}