/**================================================================      
* Appirio, Inc
* Name: COMM_ProductTriggerHandlerTest
* Description: Test class of COMM_ProductTriggerHandler
* Created Date: 29th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 
@isTest
private class COMM_ProductTriggerHandlerTest
{
    @isTest
    static void itShould()
    {
        List<Account> ac= TestUtils.createAccount(1,false);
        insert ac;
     
        list<User> user = TestUtils.createUser(2,'Customer Service - COMM INTL', true);
        Contact con1 = TestUtils.createCOMMContact(ac.get(0).id, 'TestContact', false);
        con1.Email = 'test@nic.in';
        insert con1;
     
        Id pricebookId = Test.getStandardPricebookId();
        List<Pricebook2> pricebooks= TestUtils.createPriceBook(2,'COMM Price Book',false);  
        pricebooks[0].name = 'COMM Price Book';
        pricebooks[0].IsActive = true;
        pricebooks[0].currencyisocode = 'USD';
        pricebooks[1].name = 'Endo List Price'; 
        pricebooks[1].IsActive = true; 
        pricebooks[1].currencyisocode = 'USD'; 
        insert pricebooks;

        Test.startTest();   
           List<Product2> prdt=TestUtils.createCOMMProduct(8, false);
           prdt[0].Product_Class_Code__c = '001'; 
           prdt[1].Product_Class_Code__c = '002'; 
           prdt[2].Product_Class_Code__c = '003'; 
           prdt[3].Product_Class_Code__c = '005'; 
           prdt[4].Product_Class_Code__c = '006'; 
           prdt[5].Product_Class_Code__c = '007'; 
           prdt[6].Product_Class_Code__c = '009'; 
           prdt[7].Product_Class_Code__c = '050'; 
           for(Integer i = 0; i < 8; i++){
            prdt[i].Item_Owner__c = 'SCC';
            prdt[i].inventory_item_id__c = '123456' + i; 
           }
           insert prdt;
           for(Integer i = 0; i < 8; i++){
            prdt[i].Floor_Price__c = 12;
           }
           update prdt;

        Test.stopTest();   
    }
}