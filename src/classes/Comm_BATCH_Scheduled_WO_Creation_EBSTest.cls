/**================================================================      
* Appirio, Inc
* Name: Comm_BATCH_Scheduled_WO_Creation_EBSTest
* Description: Test class for Comm_BATCH_Scheduled_WO_Creation_EBS
* Created Date: 21 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class Comm_BATCH_Scheduled_WO_Creation_EBSTest {
    static List<SVMXC__Service_Order_Line__c> wolList;
    @isTest static void testBatch() {
        createData();
        Test.startTest();
        Comm_BATCH_Scheduled_WO_Creation_EBS batch = new Comm_BATCH_Scheduled_WO_Creation_EBS();
        Database.executeBatch(batch);
        Test.stopTest();
        System.assertEquals([SELECT Order__c FROM SVMXC__Service_Order_Line__c WHERE Id = :wolList[0].Id].Order__c != null,true);
    }
    private static void createData() {
        Product2 productRecord = TestUtils.createProduct(1, true).get(0);
        Id standardPBId = Test.getStandardPricebookId();
        PriceBook2 pricebook = new PriceBook2(Name='COMM Price Book');
        insert pricebook;
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM US Stryker Field Service Case').getRecordTypeId();
        Map<String, Schema.RecordTypeInfo> WOLRecordTypeInfo =
            Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName();
        String WOLRecordTypeId = WOLRecordTypeInfo.get('Usage/Consumption').getRecordTypeId();
        string rtCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case').getRecordTypeId();
        RTMap__c rtMap = new RTMap__c(Name = rtCase,
                                      Object_API_Name__c='Case',
                                      Division__c = 'COMM',
                                      RT_Name__c='COMM US Stryker Field Service Case');
        insert rtMap;
        Account acc = TestUtils.createAccount(1, true).get(0);
        Case caseRecord = TestUtils.createCase(1, acc.id, false).get(0);
        caseRecord.RecordTypeId = caseRecordTypeId;
        insert caseRecord;
        SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = caseRecord.Id);
        insert workOrder;
        wolList = new List<SVMXC__Service_Order_Line__c>();
        for(integer i=0;i<10;i++) {
            SVMXC__Service_Order_Line__c workOrderLine = new SVMXC__Service_Order_Line__c(
                SVMXC__Service_Order__c = workOrder.Id,SVMXC__Line_Status__c = 'Completed',
                RecordTypeId = WOLRecordTypeId,SVMXC__Product__c = productRecord.Id,SVMXC__Billable_Quantity__c = 10,
                SVMXC__Billable_Line_Price__c = 10
            );
            wolList.add(workOrderLine);
        }
        insert wolList;
    }
}