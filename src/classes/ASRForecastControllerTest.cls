@isTest
private class ASRForecastControllerTest {
    static List<User> userList;
    static List<Mancode__c> mancodeList1;
    static List<Mancode__c> mancodeList2;
    //Added by Shubham for Story S-437706 #Start
    static List<Mancode__c> mancodeList3;
    static List<Mancode__c> mancodeList4;
    //Added by Shubham for Story S-437706 #End
    static User asrUser;
    static User adminUser;
    static List<Forecast_Year__c> lstForcastYear;
    static List<Forecast_Year__c> lstForcastYear2;
    // Testing when Permission access is there
    @isTest static void testFirst() {
        createData();
        System.runAs(adminUser) {
            insert lstForcastYear;
            Id psaId = [SELECT Id FROM PermissionSet WHERE Name = 'Instrument_Forecasts' LIMIT 1].Id;
            //Updated by Shubham for Story S-437706
            PermissionSetAssignment permissionSetAccess = new PermissionSetAssignment(PermissionSetId = psaId , AssigneeId = userList[0].Id);
            insert permissionSetAccess;
        }
        Test.startTest();
        //Updated by Shubham for Story S-437706
        System.runAs(userList[0]) {
            PageReference pageRef = Page.ASRForecast;
            Test.setCurrentPage(pageRef);
            ASRForecastController controller = new ASRForecastController();
            controller.getRoles();
            controller.getAllUsers();
            controller.createURL();
            //Added by Shubham for Story S-437706 #Start
            controller.createURLSF1();
        } 
        System.runAs(asrUser) {
            PageReference pageRef = Page.ASRForecast;
            Test.setCurrentPage(pageRef);
            ASRForecastController controller = new ASRForecastController();
            controller.getRoles();
            controller.getAllUsers();
            controller.createURL();
            controller.ASRselectedUser = userList[0].Id;
            controller.createASRUrlSF1();
            //Added by Shubham for Story S-437706 #End
        }
        Test.stopTest();
    }
    // Testing when permission access is not there and selected role is sales rep
    @isTest static void testSecond() {
        createData();
        
        System.runAs(adminUser) {
            insert lstForcastYear;
            ASR_Relationship__c asrRelationship = new ASR_Relationship__c(ASR_User__c = userList[0].Id , Mancode__c = mancodeList1[0].Id );
            insert asrRelationship;
        }
        Test.startTest();
        System.runAs(userList[1]) {
            PageReference pageRef = Page.ASRForecast;
            Test.setCurrentPage(pageRef);
            ASRForecastController controller = new ASRForecastController();
            //Commented by Shubham for Story S-437706
            //controller.loadASRSelectedBU();
            controller.getRoles();
            controller.selectedRole = 'Sales Rep';
            controller.getAllUsers();
            controller.createURL();
        }
        Test.stopTest();       
    }
    // Testing when asr user is of profile Instruments sales user , selectedBu is Instruments and seleted role is ASR
    @isTest static void testThird() {
        createData();
        
        System.runAs(adminUser) {
            insert lstForcastYear;
            ASR_Relationship__c asrRelationship = new ASR_Relationship__c(ASR_User__c = userList[0].Id , Mancode__c = mancodeList1[0].Id );
            insert asrRelationship;
        }
        Test.startTest();
        System.runAs(userList[1]) {
            PageReference pageRef = Page.ASRForecast;
            Test.setCurrentPage(pageRef);
            ASRForecastController controller = new ASRForecastController();
            //Commented by Shubham for Story S-437706
            //controller.loadASRSelectedBU();
            controller.selectedBu = 'Instruments';
            controller.getRoles();
            controller.selectedRole = 'ASR';
            controller.getAllUsers();
            controller.loadASRSelectedBU();
            controller.createURL();
        }
        Test.stopTest();       
    }
    // Testing when asr user is of profile Instruments sales Manager , selectedBu is IVS and seleted role is ASR
    @isTest static void testFourth() {
        createData();
        
        System.runAs(adminUser) {
            insert lstForcastYear;
            ASR_Relationship__c asrRelationship = new ASR_Relationship__c(ASR_User__c = asrUser.Id , Mancode__c = mancodeList1[0].Id );
            insert asrRelationship;
        }
        Test.startTest();
        System.runAs(userList[1]) {
            PageReference pageRef = Page.ASRForecast;
            Test.setCurrentPage(pageRef);
            ASRForecastController controller = new ASRForecastController();
            //Commented by Shubham for Story S-437706
            //controller.loadASRSelectedBU();
            controller.selectedBu = 'IVS';
            controller.getRoles();
            controller.selectedRole = 'ASR';
            controller.getAllUsers();
            controller.createURL();
            controller.selectedUser = userList[1].id;
            controller.createURL();
            controller.createUrlSF1();
            
        }
        Test.stopTest();       
    }
    // Testing when asr user is of profile Instruments sales Manager , selectedBu is NSE and seleted role is ASR
    @isTest static void testFifth() {
        createData();
        
        System.runAs(adminUser) {
            insert lstForcastYear2;
            ASR_Relationship__c asrRelationship = new ASR_Relationship__c(ASR_User__c = asrUser.Id , Mancode__c = mancodeList1[0].Id );
            insert asrRelationship;
        }
        Test.startTest();
        System.runAs(userList[1]) {
            PageReference pageRef = Page.ASRForecast;
            Test.setCurrentPage(pageRef);
            ASRForecastController controller = new ASRForecastController();
            //Commented by Shubham for Story S-437706
            //controller.loadASRSelectedBU();
            controller.selectedBu = 'NSE';
            controller.getRoles();
            controller.selectedRole = 'ASR';
            controller.getAllUsers();
            controller.createURL();
        }
        Test.stopTest();       
    }
    // creates test data
    public static void createData() {
        Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
        Id recordTypeRep = rtByNameRep.getRecordTypeId();
        Id recordTypeManager = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Manager Forecast').getRecordTypeId();
        Profile pr = TestUtils.fetchProfile('Instruments Sales User');
        userList = TestUtils.createUser(4, pr.Name, false);
        userList[0].Division = 'IVS';
        userList[1].Division = 'IVS';
        userList[2].Division = 'IVS';
        insert userList;
        adminUser = TestUtils.createUser(1,'System Administrator', true).get(0);
        asrUser = TestUtils.createUser(1,'Instruments Sales Manager', true).get(0);
        System.debug('userList-==='+userList);
        System.runAs(adminUser) {
            mancodeList1 = TestUtils.createMancode(1, userList[1].Id, userList[2].Id, false);
            mancodeList1[0].Business_Unit__c = 'IVS';
            mancodeList1[0].Type__c = 'ASR';
            mancodeList1[0].DefaultViewAs__c = userList[0].Id;
            mancodeList1[0].Is_Active__c = true;
            insert mancodeList1;
            mancodeList2 = TestUtils.createMancode(1, userList[1].Id, userList[2].Id, false);
            mancodeList2[0].Business_Unit__c = 'IVS';
            mancodeList2[0].Type__c = 'VPI';
            mancodeList2[0].DefaultViewAs__c = userList[0].Id;
            mancodeList2[0].Is_Active__c = true;
            insert mancodeList2;
            //Added by Shubham for Story S-437706 #Start
            mancodeList3 = TestUtils.createMancode(1, userList[1].Id, userList[2].Id, false);
            mancodeList3[0].Business_Unit__c = 'IVS';
            mancodeList3[0].Type__c = 'ASR';
            mancodeList3[0].DefaultViewAs__c = userList[1].Id;
            mancodeList3[0].Is_Active__c = true;
            insert mancodeList3;
            mancodeList4 = TestUtils.createMancode(1, userList[0].Id, userList[2].Id, false);
            mancodeList4[0].Business_Unit__c = 'IVS';
            mancodeList4[0].Type__c = 'ASR';
            mancodeList4[0].DefaultViewAs__c = userList[0].Id;
            mancodeList4[0].Is_Active__c = true;
            insert mancodeList4;
            //Added by Shubham for Story S-437706 #End
            lstForcastYear = TestUtils.createForcastYear(1,userList[0].Id,recordTypeRep,false);
            lstForcastYear[0].Business_Unit__c = 'IVS';
            lstForcastYear[0].Manager__c = userList[2].Id;
            lstForcastYear[0].Year__c = String.valueOf(System.today().year());
            lstForcastYear2 = TestUtils.createForcastYear(1,userList[1].Id,recordTypeManager,false);
            lstForcastYear2[0].Business_Unit__c = 'IVS';
            lstForcastYear2[0].Manager__c = userList[2].Id;
            lstForcastYear2[0].Year__c = String.valueOf(System.today().year());
        }
    }
}