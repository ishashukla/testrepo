/**================================================================      
* Appirio, Inc
* Name: NoteTriggerHandler
* Description: Handler class for Notes Trigger
* Created Date: Oct 5, 2016
* Created By: Shreerath Nair
* 
* Date Modified    Modified By             Description of the update
* Oct 5, 2016      Shreerath Nair          Class Creation T-542918
==================================================================*/
public class NoteTriggerHandler{

    
    
    /**================================================================      
    * Standard Methods from the NoteTriggerHandlerInterface
    ==================================================================*/
    public static boolean IsActive(){
        return TriggerState.isActive('NotesTrigger');
    }
    
    public void OnBeforeInsert(List<Notes__c> newNotes){}
    public void OnAfterInsert(List<Notes__c> newNotes){
        //T-542919(SN) - function to share the notes record with all related ASR users of salesrep.
        shareNotesRecordWithASR(newNotes);
    }
    
    public void OnBeforeUpdate(List<Notes__c> newNotes, Map<ID, Notes__c> oldNotesMap){}
    public void OnAfterUpdate(List<Notes__c> newNotes, Map<ID, Notes__c> oldNotesMap){}
    
    public void OnBeforeDelete(List<Notes__c> NotesToDelete, Map<ID, Notes__c> NotesMap){}
    /**================================================================      
    * End of Standard Methods from the NoteTriggerHandlerInterface
    ==================================================================*/
    
    /**================================================================      
    * Trigger Call Methods from the NoteTrigger
    ==================================================================*/
    public void beforeInsert(List<Notes__c> newNotes){
        // Org Wide functionality
        OnBeforeInsert(newNotes);
    }
    
    public void afterInsert(List<Notes__c> newNotes){
        // Org Wide functionality
        OnAfterInsert(newNotes);
    }
    
    public void beforeUpdate(List<Notes__c> newNotes, Map<ID, Notes__c> oldNotesMap){
        // Org Wide functionality
        OnBeforeUpdate(newNotes, oldNotesMap);
    }
    
    public void afterUpdate(List<Notes__c> newNotes, Map<ID, Notes__c> oldNotesMap){
        // Org Wide functionality
        OnAfterUpdate(newNotes, oldNotesMap);
    }
    
    public void beforeDelete(List<Notes__c> oldNotes, Map<ID, Notes__c> oldNotesMap){
        // Org Wide functionality
        OnbeforeDelete(oldNotes, oldNotesMap);

    }
    /**================================================================      
    * End of Trigger Call Methods from the NotesTrigger
    ==================================================================*/
    
   
    
    //T-542919(SN) - function to share the notes record with all ASR user related to the Sales Rep  in ASR Relationship .
    public static void shareNotesRecordWithASR(List<Notes__c> newNotes){
        
        try{
            Set<Id> createdByIdSet = new Set<Id>();
            Map<id,List<ID>> ASRUserWithSalesRepMap= new Map<id,List<Id>> ();
            //get the createdbyId of all notes record inserted
            for(Notes__c note : newNotes){
                createdByIdSet.add(note.createdById);
            }
            
            //get all the ASR user related to sales rep from ASR_Relationship object.
            if(createdByIdSet.size()>0){
                for(ASR_Relationship__c asrRel : [SELECT Name, Mancode__c, ASR_User__c, Mancode__r.Sales_Rep__c, Mancode__r.Type__c 
                                                    FROM ASR_Relationship__c 
                                                    WHERE ASR_User__c IN: createdByIdSet
                                                    AND Mancode__r.Type__c = 'ASR'
                                                    AND ASR_User__r.Profile.Name = 'Instruments Sales User']){
                                                        
                    //get the list of ASR user related to salesrep
                    List<Id> ASRUserList = (ASRUserWithSalesRepMap.get(asrRel.ASR_User__c) == null) ? new  List<Id>():ASRUserWithSalesRepMap.get(asrRel.ASR_User__c);
                    //include the new ASR user in the list
                    ASRUserList.add(asrRel.Mancode__r.Sales_Rep__c);
                    //add the uodated list into the map
                    ASRUserWithSalesRepMap.put(asrRel.ASR_User__c,ASRUserList);
                }
            }
            
            List<Notes__Share> noteShareList = new List<Notes__Share>();
            //Loop through all the notes record created by salesrep and create share record for all related ASR users.
            if(!ASRUserWithSalesRepMap.isEmpty()){
                for(Notes__c notes : newNotes){
                    //check if map consist of any record of the salesrep
                    if(ASRUserWithSalesRepMap.containsKey(notes.createdById) && ASRUserWithSalesRepMap.get(notes.createdById)!=null){
                        //loop through the ASR users related to salesrep
                        for(Id userid : ASRUserWithSalesRepMap.get(notes.createdById)){
                            Notes__Share notesShare  = new Notes__Share();
                            notesShare.AccessLevel = 'Edit';
                            notesShare.RowCause = Schema.Notes__Share.RowCause.Manual;
                            notesShare.UserOrGroupId = userid;
                            notesShare.ParentId = notes.Id;
                            noteShareList.add(notesShare);
                        }
                        
                    }
                }
                
            }
            
            if(noteShareList.size()>0){
                insert noteShareList;
            }
            
        }catch(Exception ex){ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'NoteTriggerHandler', true);}
        
        
    }
    
    
    
     
}