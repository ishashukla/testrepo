//
// (c) 2014 Appirio, Inc.
// Class PrivateNotesController - Extension for PrivateNotes
//
// 02 Feb 2016     Sahil Batra       Original(T-470669)
// 13 Feb 2016     Sahil Batra       Modified(T-476187)
// 07 June 2016    Prakarsh Jain     Modified(T-508948)
// 17 June 2016    Prakarsh Jain     Modified(T-512535)
// 28 June 2016    Isha Shukla       Modified(T-515755) 
// 20 Sept 2016    Shreerath Nair    Modified(I-235758) - code merge of Empower with Endo (Changed name ofredirectIfEndoProfileUser to initialAction)
// 13 Oct  2016    Shreerath Nair    Modified(T-542919)
public class PrivateNotesController {// with sharing 
    public Notes__c notes {get;set;}
    public UserRecordAccess ua {get;set;}
    public String pageTitle {get;set;}
    public String relatedToId; 
    public Group grp;
    public Boolean isOwner {get;set;}
    private User currentUser {get;set;}
    public Boolean isNew {get;set;}
    public Boolean isNote {get;set;}
	//T-542919(SN) - boolean variable to check if ASR user Associated with Sales Rep in ASR Relationship
    public boolean isRelatedASRUser{get;set;}
    public String inSF1 {get;set;}
    public String notesId{get;set;}
    public List<ConnectApi.BatchInput> notesPost = new List<ConnectApi.BatchInput>();
    public static Schema.RecordTypeInfo rtByNameNotes = Schema.SObjectType.Notes__c.getRecordTypeInfosByName().get('Notes');
    public static Id recordTypeNotes = rtByNameNotes.getRecordTypeId();
    public static Schema.RecordTypeInfo rtByNameCard = Schema.SObjectType.Notes__c.getRecordTypeInfosByName().get('Preference Cards');
    public static Id recordTypeCards = rtByNameCard.getRecordTypeId();
    public Boolean isParentPost;
	public Boolean isPreferenceCard{get;set;}
    public Boolean isSysAdmin{get;set;}
    public Boolean isOwnerOfInstruments{get;set;}
    public PrivateNotesController(ApexPages.StandardController stdController){
        Id uid = UserInfo.getUserId();
        pageTitle = 'Private Note';
        grp = new Group();
        isOwner = false;
		isRelatedASRUser =false;
        isParentPost = false;
        grp = [Select Type, Name, Id, DeveloperName From Group where DeveloperName='AllInternalUsers'];
        notesId = ApexPages.currentPage().getParameters().get('id');
        relatedToId = ApexPages.currentPage().getParameters().get('relatedId');
        isNote = (ApexPages.currentPage().getParameters().get('isNotes') == 'true') ? true : false;
        // Checks for theme of the user. Used in page to show sharing button if in desktop mode and do not show the button if in SF1
        inSF1 = UserInfo.getUiTheme();
		Profile ProfileName = [SELECT Name FROM profile WHERE id = :UserInfo.getProfileId()];
        if(ProfileName.Name == Constants.System_Admin_Profile ){
          isSysAdmin = true;
        }
        else{
          isSysAdmin = false;
        }
        if(ProfileName.Name.containsIgnoreCase('Instruments')){
          isOwnerOfInstruments = true;
        }
        else{
          isOwnerOfInstruments = false;
        }
		
        if(notesId!=null && notesId!=''){
            notes = [SELECT RecordTypeId, Private__c, Products__c, Procedure__c, Opportunity__c, Name, Id,
                     Description__c, OwnerId, Contact__c, Account__c, Chatter_Timestamp__c 
                     FROM Notes__c WHERE Id =:notesId];
            ua = [SELECT RecordId, HasReadAccess, HasEditAccess, HasTransferAccess, MaxAccessLevel FROM UserRecordAccess WHERE UserId =: uid AND RecordId =: notesId];
            isOwner = (notes.OwnerId == uid) ? true : false;
            if(notes.RecordTypeId == recordTypeNotes){
                isNote = true;
                pageTitle = (ua.HasEditAccess) ? Label.View_Edit_Private_Note : Label.View_Private_Note;
            }else{
                isNote = false;
                pageTitle = (ua.HasEditAccess) ? Label.View_Edit_Preference_Card : Label.View_Preference_Card;
            }
            
            isNew = false;
			
			//T-542919(SN) - get record if current user is ASR User and is associated with  Sales Rep who is the Owner of the Preference Card 
            List<ASR_Relationship__c> asrList = [SELECT Id FROM ASR_Relationship__c 
                                                 WHERE Mancode__r.Sales_Rep__c =: uid
                                                 AND Mancode__r.Type__c = 'ASR'
                                                 AND ASR_User__c =: notes.OwnerId];
            //set isRelatedASRUser to true if record found.                                    
            if(asrList.size()>0){
                isRelatedASRUser = true;    
            }
            //T-542919 End  
			
			
        }else{
            if(isNote){
                pageTitle = Label.New_Private_Note;
				isPreferenceCard = false;
            }else{
                pageTitle = Label.New_Preference_Card;
				isPreferenceCard = true;
            }
            isNew = true;
            notes = new Notes__c();
            notes.Private__c = True; // Isha Shukla Modified(T-515755) when new note is created the private checkbox will be true
            if(isNote){
                notes.recordTypeId = recordTypeNotes;
            }else if(!isNote){
                notes.recordTypeId = recordTypeCards;
            }
            if(relatedToId!=null){
                String relatedId = relatedToId;
                if(relatedId.subString(0,3) == '001'){
                    notes.Account__c = relatedToId;
                }
                else if(relatedId.subString(0,3) == '003'){
                    notes.Contact__c = relatedToId;
                }
                else if(relatedId.subString(0,3) == '006'){
                    notes.Opportunity__c = relatedToId;
                }
            }
            System.debug('>>>>>>'+notes);
        }
        if(relatedToId == null){
            if(notes.Account__c!=null){
                relatedToId = notes.Account__c;
            }
            else if(notes.Contact__c!=null){
                relatedToId = notes.Contact__c;
            }
            else if(notes.Opportunity__c!=null){
                relatedToId = notes.Opportunity__c;
            }
        }
    }
    
    // Method to redirect to standard page of Notes__c - This is specific to Endo Users
    //I-235758 (SN) - Changed name of redirectIfEndoProfileUser to initialAction
    public PageReference initialAction(){
        String notesId = ApexPages.currentPage().getParameters().get('id');
        for (User user : [Select id, profile.Name from User where id = : UserInfo.getUserId()]){
           currentUser = user; 
        }
        if (currentUser.profile.Name.contains('Endo')){
            String url = '/' + notesId;
            if (notesId != null && notesId != ''){
               
                if (ApexPages.currentPage().getUrl().contains('retURL')){
                    for(Notes__c note : [Select id, Account__c from Notes__c where id = : notesId]){
                         url += '/e?nooverride=1&retURL=' + note.Account__c;
                    }                     
                }else{
                    url += '?nooverride=1';                   
                }
                return new PageReference(url);
            } 
        }
       else{
            //I-235758 (SN) call postChatter function if not Endo User(Empower Functionality)
            postChatter();
        }
        return null;
    }
    
    
    
    
    
    
    
    public PageReference save(){
      PageReference ReturnPage;
        
        system.debug('notes.Private__c>>>'+notes.Private__c);
        //Prakarsh Jain   T-512535 changes start
        //if private checkbox is unchecked than we make the record public otherwise private and made private checkbox editable
        if(notes.Private__c == false){
            upsert notes;
            system.debug('notes.Private__c???'+notes.Private__c);
            if(!isParentPost){
              makePublic();
            }
            
            
        }
        else{
          upsert notes;
          PageReference retPage = mkRecordPrivate();
          
        }
        //T-512535 changes end
         
        
          ReturnPage = new PageReference('/'+relatedToId);
        
        return ReturnPage;
    }
    public PageReference cancel(){
        PageReference ReturnPage = new PageReference('/'+relatedToId); 
        return ReturnPage;
    }
    public PageReference makeRecordPublic(){
        makePublic();
        PageReference ReturnPage = new PageReference('/apex/PrivateNotes?Id'+notes.Id); 
        return ReturnPage;
    }
    public PageReference makeRecordPrivate(){
        List<Notes__Share> shareList = new List<Notes__Share>();
        shareList = [Select UserOrGroupId, Id From Notes__Share WHERE UserOrGroupId =:grp.Id AND ParentId =: notes.Id];
        if(shareList.size() > 0){
            delete shareList; 
        }
        notes.Private__c = true;
        update notes;
        PageReference ReturnPage = new PageReference('/apex/PrivateNotes?Id'+notes.Id); 
        return ReturnPage;
    }
    public PageReference mkRecordPrivate(){
      List<Notes__Share> shareList = new List<Notes__Share>();
      shareList = [Select UserOrGroupId, Id, ParentId From Notes__Share WHERE UserOrGroupId =:grp.Id AND ParentId =: notes.Id];
      if(shareList.size() > 0){
        delete shareList; 
      }
      PageReference ReturnPage = new PageReference('/apex/PrivateNotes?Id'+notes.Id); 
      return ReturnPage;
    }
    public void makePublic(){
        Notes__Share notesShare  = new Notes__Share();
        notesShare.AccessLevel = 'Read';
        notesShare.RowCause = Schema.Notes__Share.RowCause.Manual;
        notesShare.UserOrGroupId = grp.Id;
        notesShare.ParentId = notes.Id;
        insert notesShare;
        system.debug('notesShare>>>'+notesShare);
        notes.Private__c = false;
        update notes;
        isParentPost = true;
        postChatterOnParentRecord(notes);
    }
    //T-508948 changes start
  public List<ConnectApi.BatchInput> returnInputFeed(Notes__c notes){
    DateTime timeNow = System.now();
    DateTime X15MinutesAgo = timeNow.addMinutes(-15);
    DateTime chatterTimeStamp = notes.Chatter_Timestamp__c;
    List<Notes__Share> shareList = new List<Notes__Share>();
    List<Notes__Share> ntShareList = new List<Notes__Share>();
    Set<Id> parentIdSet = new Set<Id>();
    Set<Id> insertedById = new Set<Id>();
    Set<String> userNames = new Set<String>();
    Set<String> bodyStringSet = new Set<String>();
    bodyStringSet.add('test');
    Map<Id, Notes__Share> mapIdToNotesShare = new Map<Id, Notes__Share>();
    shareList = [Select UserOrGroupId, RowCause, ParentId, LastModifiedDate, LastModifiedById, IsDeleted, Id, AccessLevel From Notes__Share WHERE ParentId =: notes.Id AND LastModifiedDate >: chatterTimeStamp];
    system.debug('shareList>>>'+shareList);
    for(Notes__Share ntShare : shareList){
      parentIdSet.add(ntShare.ParentId);
      insertedById.add(ntShare.UserOrGroupId);
      mapIdToNotesShare.put(ntShare.UserOrGroupId, ntShare);
    }
    Map<Id, String> mapUserIdToUserName = new Map<Id, String>();
    system.debug('parentIdSet>>>'+parentIdSet);
    system.debug('insertedById>>>'+insertedById);
    List<FeedItem> listFeedItems = [Select Type, Title, SystemModstamp, RelatedRecordId, ParentId, LinkUrl, LikeCount, LastModifiedDate, IsDeleted, InsertedById, Id, HasLink, HasContent, CreatedDate, CreatedById, CommentCount, Body, BestCommentId 
                                    From FeedItem
                                    WHERE ParentId =: notes.Id
                                    ];
    system.debug('listFeedItems>>>'+listFeedItems);
    List<User> listUsers = [SELECT Id, Name FROM User WHERE Id IN: insertedById];
    for(User usr : listUsers){
      userNames.add(usr.Name);
      mapUserIdToUserName.put(usr.Id, usr.Name);
    }
    for(FeedItem fdItem : listFeedItems){
      bodyStringSet.add(fdItem.Body);
    }
    system.debug('bodyStringSet>>>'+bodyStringSet);
    ConnectApi.FeedType feedType = ConnectApi.FeedType.UserProfile;
    ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
    ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
    ConnectApi.TextSegmentInput textSegment;
    ConnectApi.MentionSegmentInput mentionSegment;
    messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
    textSegment = new ConnectApi.TextSegmentInput();
    if(recordTypeNotes == notes.RecordTypeId){
      textSegment.text = Label.Chatter_Body_For_Notes;
    }
    else{
      textSegment.text = Label.Chatter_Body_For_Preference_Cards;
    }
    messageInput.messageSegments.add(textSegment);
    Integer count = 0;
    system.debug('bodyStringSet>>>'+bodyStringSet);
    //changes
    Boolean userFlag = false;
    if(shareList.size()>0){
      system.debug('shareList+++'+shareList);
      for(Notes__Share notesShare : shareList){
        userFlag = false;
        if(String.valueOf(notesShare.UserOrGroupId).startsWith('005')){
          if(bodyStringSet.size()>1 && bodyStringSet.contains('test')){
            bodyStringSet.remove('test');
          }
          system.debug('bodyStringSet???'+bodyStringSet);
          for(String body : bodyStringSet){
            system.debug('body+++'+body);
            system.debug('body.containsIgnoreCase(Label.Chatter_Body_For_Notes)>>>'+body.containsIgnoreCase(Label.Chatter_Body_For_Notes));
            system.debug('body.containsIgnoreCase(Label.Chatter_Body_For_Preference_Cards)>>>>'+body.containsIgnoreCase(Label.Chatter_Body_For_Preference_Cards));
            system.debug('body.containsIgnoreCase(mapUserIdToUserName.get(notesShare.UserOrGroupId))>>>'+body.containsIgnoreCase(mapUserIdToUserName.get(notesShare.UserOrGroupId)));
            system.debug('mapUserIdToUserName.get(notesShare.UserOrGroupId)>>>'+mapUserIdToUserName.get(notesShare.UserOrGroupId));
            /*system.debug();
            system.debug();
            system.debug();*/
            if(body!=null){
            if(!((body.containsIgnoreCase(Label.Chatter_Body_For_Notes) || body.containsIgnoreCase(Label.Chatter_Body_For_Preference_Cards)) && body.containsIgnoreCase(mapUserIdToUserName.get(notesShare.UserOrGroupId))) && notes.OwnerId != notesShare.UserOrGroupId && userFlag == false){
              system.debug('body>>>'+body);
              userFlag = true;
                mentionSegment = new ConnectApi.MentionSegmentInput();
                mentionSegment.id = notesShare.UserOrGroupId;
                messageInput.messageSegments.add(mentionSegment);
                count++;
              }
            }
            }
          }
        }
    }
    if(count>0){
      input.body = messageInput;
      input.subjectId = notes.id;
      input.feedElementType = ConnectApi.FeedElementType.FeedItem;
      system.debug('input>>>'+input);
      ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(input);
      notesPost.add(batchInput);
      return notesPost;
    }
    else{
      return null;
    }
  }
  
  public void postChatter(){
    system.debug('notes>>>'+notes);
    //ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(returnInputFeed(notes));
    //notesPost.add(batchInput); 
    List<ConnectApi.BatchInput> ntsPost = new List<ConnectApi.BatchInput>();
    ntsPost = returnInputFeed(notes);
    system.debug('notesPost>>>'+notesPost);  
    if(!Test.isRunningTest()){
      system.debug('chatterPost!!!');
      if(notesPost.size()>0 && !isParentPost){
        ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), ntsPost);
        notes.Chatter_Timestamp__c = System.now();
        update notes;
      }
    }
  } 
  public void postChatterOnParentRecord(Notes__c notes){
    ConnectApi.FeedType feedType = ConnectApi.FeedType.UserProfile;
    ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
    ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
    messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
    ConnectApi.TextSegmentInput textSegment;
    textSegment = new ConnectApi.TextSegmentInput();
    if(recordTypeNotes == notes.RecordTypeId){
      textSegment.text = Label.Chatter_Post_for_Public_Sharing_Notes;
    }
    else{
      textSegment.text = Label.Chatter_Post_for_Public_Sharing_Pref_Cards;
    }
    messageInput.messageSegments.add(textSegment);
    input.body = messageInput;
    if(notes.Account__c!=null){
      input.subjectId = notes.Account__c;
    }
    if(notes.Contact__c!=null){
      input.subjectId = notes.Contact__c;
    }
    if(notes.Opportunity__c!=null){
      input.subjectId = notes.Opportunity__c;
    }
    input.feedElementType = ConnectApi.FeedElementType.FeedItem;
    system.debug('input>>>'+input);
    ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(input);
    List<ConnectApi.BatchInput> notesPostOnParent = new List<ConnectApi.BatchInput>();
    notesPostOnParent.add(batchInput);
    if(!Test.isRunningTest()){
        ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), notesPostOnParent);
    }
  }
  //T-508948 end
  
   public PageReference deleteRecord(){
    PageReference pgRef;
    if(notesId!= null){
      delete notes;
      pgRef = new PageReference('/'+relatedToId);
      return pgRef;
    }
    return null;
  }
}