//
// (c) 2016 Appirio, Inc.
//
// Class Endo_SendEmailtoOrderCustomers
//
// Aug 30,2016  Parul Gupta (Appirio)  I-232731 (Original)  
global class Endo_SendEmailtoOrderCustomersSchedule implements Schedulable {
   global void execute(SchedulableContext sc) {
      Endo_SendEmailtoOrderCustomers obj = new Endo_SendEmailtoOrderCustomers();
      Id batchInstanceId = Database.executeBatch(obj); 
   }
}