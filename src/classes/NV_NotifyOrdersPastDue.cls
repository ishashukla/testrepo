//
// (c) 2015 Appirio, Inc.
//
// Apex Class Name: NV_NotifyOrdersPastDue
// Description: This is a scheduled apex job class for processing past due orders.
//
// 18th December 2015   Hemendra Singh Bhati   Original (Task # T-458396)
//
global class NV_NotifyOrdersPastDue implements Schedulable {
  /*
  @method      : execute
  @description : This method executes apex scheduler logic.
  @params      : SchedulableContext theSchedulableContext
  @returns     : void
  */
  global void execute(SchedulableContext theSchedulableContext) {
    // Initiating invoice notifications process.
    NV_InvoiceNotifications theInstance = new NV_InvoiceNotifications();
    theInstance.notifyOrdersPastDue();
  }
}