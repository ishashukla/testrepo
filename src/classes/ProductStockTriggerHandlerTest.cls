/**================================================================      
* Appirio, Inc
* Name: ProductStockTriggerHandlerTest
* Description: Test class for ProductStockTriggerHandler
* Created Date: 11 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest(SeeAllData=true)
private class ProductStockTriggerHandlerTest {
    static List<SVMXC__Product_Stock__c> productStockList;
    @isTest static void testTrigger() {
        createData();
        for(integer i = 0 ;i < 4; i++) {
            productStockList[i].Approval_for_New_Inventory__c = 'Requested';
        }
        Test.startTest();
        update productStockList;
        Test.stopTest();
        SVMXC__Product_Stock__c stock = [SELECT CreatedDate FROM SVMXC__Product_Stock__c WHERE Id = :productStockList[0].Id];
        ProcessInstance pi = [SELECT TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :productStockList[0].Id];
        System.assertEquals(stock.CreatedDate.Date() ,pi.CreatedDate.Date());
    }
    @isTest static void testTriggerForApproval() {
        createData();
        for(integer i = 0 ;i < 4; i++) {
            productStockList[i].Approval_for_New_Inventory__c = 'Approved';
        }
        Test.startTest();
        update productStockList;
        Test.stopTest();
        System.assertEquals([SELECT Id FROM SVMXC__RMA_Shipment_Order__c WHERE Product_Stock__c = :productStockList[1].Id].size() > 0,true);
    }
    private static void createData() {
        string rtRMA = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
        if(!(RTMap__c.getInstance(rtRMA).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = rtRMA,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='Shipment');
            insert rtMap;
        }
        SVMXC__Site__c location = new SVMXC__Site__c(Location_ID__c = '1234',SVMXC__Stocking_Location__c = true);
        insert location;
        productStockList = new List<SVMXC__Product_Stock__c>();
        for(integer i = 0 ;i < 4; i++) {
            
            SVMXC__Product_Stock__c productStock  = new SVMXC__Product_Stock__c(SVMXC__Quantity2__c = 2,
                                                                                Maximum_Qty__c = 3,
                                                                                SVMXC__Reorder_Level2__c = 3,
                                                                                Approval_for_New_Inventory__c = 'New',
                                                                                SVMXC__Location__c = location.Id
                                                                               );
            productStockList.add(productStock);
        }
        insert productStockList;
    }
}