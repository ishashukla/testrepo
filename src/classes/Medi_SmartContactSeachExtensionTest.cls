/***********************************************************************
 Class          : Medi_SmartContactSearchExtensionTest
 Author         : Appirio
 Created Date   : November 27, 2015
 Descritption   : Provide test coverage to Smart Contact search for Medical Division
 ************************************************************************/
@isTest
public class Medi_SmartContactSeachExtensionTest {
    static testMethod void testMedi_SmartContactSeachExtension(){
        List<Account> lstAccounts = Medical_TestUtils.createAccount(2, true);
        List<Contact> lstContacts = Medical_TestUtils.createContact(2, lstAccounts.get(0).id,false);
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Customer Contact').getRecordTypeId();

        for(Contact c : lstContacts){
        	c.recordTypeId = recordTypeId;
        }
        insert lstContacts;
        
        Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
        TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
        List<Case> lstCases = Medical_TestUtils.createCase(1,lstAccounts.get(0).id,lstContacts.get(0).id,false);
        lstCases[0].RecordTypeId = MedicalrecordTypeId;
        insert lstCases;

        Case cs = lstCases.get(0);
        Contact con = lstContacts.get(0);
        Account acc = lstAccounts.get(0);
        PageReference pageRef = Page.Medi_SmartContactSearch;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('accId', String.valueOf(acc.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        Medi_SmartContactSeachExtension testSmartSearch = new Medi_SmartContactSeachExtension(sc);
        testSmartSearch.performSearch();
        testSmartSearch.createCase();
        testSmartSearch.showAssociatedAccounts();
        PageReference pageRef1 = testSmartSearch.cancel();
        testSmartSearch.selectedContactId = con.id;
        //testSmartSearch.currentAccount = acc.Id;
        Id MedicalrecordTypeId2 = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Non Product Issue').getRecordTypeId();
        TestUtils.createRTMapCS(MedicalrecordTypeId2, 'Medical - Non Product Issue', 'Medical');
        testSmartSearch.createCase();

        testSmartSearch.contactFirstNameToSearch = '';
        String query1 = 'SELECT Id, Primary__c, Associated_Contact__r.Title, Associated_Contact__r.Phone, Associated_Contact__r.Owner.Name, '+
                        'Associated_Contact__r.Name,Associated_Contact__r.FirstName, Associated_Contact__r.LastName, Associated_Contact__r.Email,  '+
                        'Associated_Account__r.Oracle_Account_Number__c, Associated_Account__r.Name '+
                        'FROM Contact_Acct_Association__c';
        testSmartSearch.findSearchCondition(query1);
        List<Medi_SmartContactSeachExtension.ContactWrapper> conWrap = new List<Medi_SmartContactSeachExtension.ContactWrapper>();
        conWrap = testSmartSearch.getWrapperList();
        testSmartSearch.contactLastNameToSearch = con.LastName;
        testSmartSearch.findSearchCondition(query1);
        testSmartSearch.contactLastNameToSearch = '';
        testSmartSearch.contactFirstNameToSearch = con.FirstName;
        testSmartSearch.findSearchCondition(query1);
        testSmartSearch.findSearchCondition(query1);
        testSmartSearch.contactEmail = con.Email;
        testSmartSearch.findSearchCondition(query1);
        testSmartSearch.contactEmail = '';
        query1+= ' WHERE Id != null';

        testSmartSearch.findSearchCondition(query1);
        testSmartSearch.contactLastNameToSearch = con.LastName;
        testSmartSearch.contactEmail = con.Email;
        testSmartSearch.findSearchCondition(query1);

        testSmartSearch.contactEmail = '';
        testSmartSearch.contactLastNameToSearch = '';
        testSmartSearch.contactFirstNameToSearch = '';
        conWrap = testSmartSearch.getWrapperList();

        //selecting the contact id to be populated to search
        testSmartSearch.selectedContactId = con.Id;
        testSmartSearch.createCase();

        List<Case> lstNewCases = [SELECT Id FROM Case WHERE Id NOT IN: lstCases];
        System.assert(!lstNewCases.isEmpty(),'Case should be created');
        try{
          testSmartSearch.showAssociatedAccounts();
        }catch(Exception e){
        	System.assert(e.getMessage() != null, 'We will be getting duplicate error as there is already an association getting inserted');
        }


    }


}