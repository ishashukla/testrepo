public class CheckPODocument {
  private final SObject parent;
  public static Opportunity opp {get; set;}
  public static boolean POdoc{get;set;}
  public CheckPODocument(ApexPages.StandardController controller) {
    parent = controller.getRecord();
    opp = new Opportunity();
    String oppid = parent.id;
    opp= [Select id,Document_Type_Comm__c,Trigger_Approval_Process__c from Opportunity where id =:oppid];
    POdoc=false;
    if(opp.Document_Type_Comm__c == 0) {
      POdoc = true;
    }
    if(POdoc == false) {
      opp.Trigger_Approval_Process__c =true;
      System.debug(opp.Trigger_Approval_Process__c);
    }
  }
  public static void CheckPODocumentUpdate(){
    if(!Podoc) {
      opp.Trigger_Approval_Process__c =true;
    }
    update opp;
  }
  
}