/**================================================================      
 * Appirio, Inc
 * Name: PopulateBusinessUnitController
 * Description: Controller class for PopulateBusinessUnitIntermediatePage(T-538445), class to populate Business unit on Value Add Activity record.
 * Created Date: [09/27/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/
public class PopulateBusinessUnitController {
  public String ownerId;
  public String accountId;
  public String contactId;
  public String oppId;
  public static String BU;
  public static Id accountValueAddActivityRecordTypeId = Schema.SObjectType.Value_Add_Activities__c.getRecordTypeInfosByName().get('Value Add Activity Account Record Type').getRecordTypeId();
  public static Id contactValueAddActivityRecordTypeId = Schema.SObjectType.Value_Add_Activities__c.getRecordTypeInfosByName().get('Value Add Activity Contact Record Type').getRecordTypeId();
  public static Id opportunityValueAddActivityRecordTypeId = Schema.SObjectType.Value_Add_Activities__c.getRecordTypeInfosByName().get('Value Add Activity Opportunity Record Type').getRecordTypeId();
  public PopulateBusinessUnitController(){
    
  }
  //Method to populate Business Unit field on the Value Add Activities record detail page.
  public PageReference getBUToPopulate(){
    PageReference pgRef;
    Empower_Field_Ids__c empowerFieldIdsSettings = Empower_Field_Ids__c.getOrgDefaults();
    try{
      ownerId = ApexPages.currentPage().getParameters().get('ownerId');
        accountId = ApexPages.currentPage().getParameters().get('accountId');
        contactId = ApexPages.currentPage().getParameters().get('contactId');
        oppId = ApexPages.currentPage().getParameters().get('oppId');
        Map<Id,List<String>> salesRepIdToBUMap = new Map<Id,List<String>>();
        Set<String> BUSet = new Set<String>();
        if(ownerId != null || ownerId != ''){
          for(Mancode__c mancode : [SELECT Id, Sales_Rep__c, Business_Unit__c FROM Mancode__c WHERE Sales_Rep__c =: ownerId]){
            if(salesRepIdToBUMap.containsKey(mancode.Sales_Rep__c)){
              salesRepIdToBUMap.get(mancode.Sales_Rep__c).add(mancode.Business_Unit__c);    
            }
            else{
              salesRepIdToBUMap.put(mancode.Sales_Rep__c,new List<String>{mancode.Business_Unit__c});
            }
          }
        }
        List<String> tempList = new List<String>();
        if(salesRepIdToBUMap.size()>0 && (ownerId != null || ownerId != '')){
          tempList = salesRepIdToBUMap.get(ownerId);
          if(tempList.size()>0){
            for(String str : tempList){
              BUSet.add(str);
            }
          }
        }
        
        List<String> finalList = new List<String>();
        if(BUSet.size()>0){
          for(String str : BUSet){
            finalList.add(str);
          }
        }
        if(finalList.size() == 1){
          BU = finalList.get(0);
        }
        Schema.DescribeSObjectResult obj = Value_Add_Activities__c.sObjectType.getDescribe();
        String keyPrefix = obj.getKeyPrefix();
        
        if((accountId != null && accountId != '')){
          Account account = new Account();
          List<Account> accList = new List<Account>();
          accList = [SELECT Id, Name FROM Account WHERE Id =: accountId];
          if(accList.size() > 0){
             account = accList.get(0);
             //pgRef = new PageReference('/'+keyPrefix+'/e?CF'+empowerFieldIdsSettings.ValueAddActivityAccountId__c+'='+account.Name+'&CF'+empowerFieldIdsSettings.ValueAddActivityAccountId__c+
             //                          '_lkid='+account.Id+'&'+empowerFieldIdsSettings.ValueAddActivityBusinessUnit__c+'='+BU+'&retURL=/'+account.Id);
               pgRef = new PageReference('/'+keyPrefix+'/e?CF'+empowerFieldIdsSettings.ValueAddActivityAccountId__c+'='+account.Name+'&CF'+empowerFieldIdsSettings.ValueAddActivityAccountId__c+
                                         '_lkid='+account.Id+'&retURL=/'+account.Id+'&RecordType='+accountValueAddActivityRecordTypeId+'&'+empowerFieldIdsSettings.ValueAddActivityBusinessUnit__c+'='+BU);
          }
        }
        if((contactId != null && contactId != '')){
          Contact contact;
          List<Contact> contactList = new List<Contact>();
          contactList = [SELECT Id, Name FROM Contact WHERE Id =: contactId];
          if(contactList.size()>0){
            contact = contactList.get(0);
            //pgRef = new PageReference('/'+keyPrefix+'/e?CF'+empowerFieldIdsSettings.ValueAddActivityContactId__c+'='+contact.Name+'&CF'+empowerFieldIdsSettings.ValueAddActivityContactId__c+
            //                          '_lkid='+contact.Id+'&'+empowerFieldIdsSettings.ValueAddActivityBusinessUnit__c+'='+BU+'&retURL=/'+contact.Id);
              pgRef = new PageReference('/'+keyPrefix+'/e?CF'+empowerFieldIdsSettings.ValueAddActivityContactId__c+'='+contact.Name+'&CF'+empowerFieldIdsSettings.ValueAddActivityContactId__c+
                                        '_lkid='+contact.Id+'&retURL=/'+contact.Id+'&RecordType='+contactValueAddActivityRecordTypeId+'&'+empowerFieldIdsSettings.ValueAddActivityBusinessUnit__c+'='+BU);
          }
        }
        if((oppId != null && oppId != '')){
        Opportunity opportunity = new Opportunity();
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [SELECT Id, Name FROM Opportunity WHERE Id =: oppId];
        if(oppList.size() > 0){
           opportunity = oppList.get(0);
           //pgRef = new PageReference('/'+keyPrefix+'/e?CF'+empowerFieldIdsSettings.ValueAddActivityOpportunityId__c+'='+opportunity.Name+'&CF'+empowerFieldIdsSettings.ValueAddActivityOpportunityId__c+
           //                          '_lkid='+opportunity.Id+'&'+empowerFieldIdsSettings.ValueAddActivityBusinessUnit__c+'='+BU+'&retURL=/'+opportunity.Id);
             pgRef = new PageReference('/'+keyPrefix+'/e?CF'+empowerFieldIdsSettings.ValueAddActivityOpportunityId__c+'='+opportunity.Name+'&CF'+empowerFieldIdsSettings.ValueAddActivityOpportunityId__c+
                                       '_lkid='+opportunity.Id+'&retURL=/'+opportunity.Id+'&RecordType='+opportunityValueAddActivityRecordTypeId+'&'+empowerFieldIdsSettings.ValueAddActivityBusinessUnit__c+'='+BU);
        }
      }
    }catch(Exception ex){
      ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'PopulateBusinessUnitController', true);
    }
    return pgRef;
  }
}