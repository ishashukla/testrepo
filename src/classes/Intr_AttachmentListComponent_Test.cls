/*********************************************************************************
Author 		: Appirio JDC (Kajal Jalan)
Date           : May 10, 2016
Descritption 	: Provide test coverage to Intr_AttachmentListComponentHandler.cls
*********************************************************************************/
@isTest
private class Intr_AttachmentListComponent_Test {
    
    static Account account;
    static Opportunity opportunity;
    static List<Opportunity> OpportunityList;
    static List<String> example = new List<String>();
    static User user;
    
    // Method to test search functionality
    static testMethod void testSearchListFunctionality() {
        
        createTestData();
        
        Test.startTest();
        
	        Intr_AttachmentListComponentHandler controller = new Intr_AttachmentListComponentHandler();
	        // Setting attributes for searching
	        controller.fieldsCSV = 'Name,ParentId,Parent.Name,Id,LastModifiedDate,CreatedById';
	        controller.sortDirection = 'asc'; 
	        controller.objectName = 'Attachment';
	        controller.pageSize = 5;
	        controller.pageNumber = 0;
	        controller.searchFieldName = 'ParentId';
	        example.add(OpportunityList[0].Id);
	        controller.searchFieldValue = example;
	        controller.orderByFieldName = 'Name';
	        controller.sortByField	= 'Name';
	        controller.filter = 'CreatedById != null';
	        controller.title = 'OpportunityAttachment';
	        controller.returnUrl = '/003/0';
	        
	        // Calling search method
	        //controller.getRecords();
	        List<sObject> records = controller.getRecords();
	        system.assertEquals(records.size(), 5, 'Search method should return 5 records.');
	        
	        controller.Beginning();
	        controller.Previous();
	        controller.Next();
	        controller.End();
	        controller.getDisablePrevious();
	        controller.getDisableNext();
	        controller.getTotal_size();
	        controller.getPageNumber();
	        controller.getTotalPages();
	        controller.getShowNewButton();
	        controller.sortByFieldAction();
	        controller.showMore();
	        
	        
	        controller.deleteRecordId = controller.getRecords().get(0).id;
	        controller.deleteRecord();
	        //List<sObject> records1 = controller.getRecords();
	        system.assertEquals(records.size(), 5, 'After delete, size of the record list should be 4.');
        
        Test.stopTest();
        
    }
    
    // Method to test validations
    static testMethod void testValidation() {
        
        
        createTestData();
        
        Test.startTest();
        
	        Intr_AttachmentListComponentHandler controller = new Intr_AttachmentListComponentHandler();
	        // Setting attributes for searching
	        controller.objectName = 'Attachment';
	        controller.pageSize = 5;
	        controller.searchFieldName = 'ParentId';
	        example.add(OpportunityList[0].Id);
	        controller.searchFieldValue = example ;
	        controller.orderByFieldName ='Name';
	        controller.sortByField	= 'Name';
	        controller.filter = 'CreatedById != null';
	        controller.title = 'OpportunityAttachment';
	        controller.returnUrl = '/003/0';
	        controller.moreLink = '';
	        controller.returnUrl = account.Id;
	        controller.showAsStandardRelatedList = false;
	        
	        // Calling search method without field list
	        controller.getRecords();
	        System.assert(ApexPages.getMessages().get(0).getSummary().contains('fieldList or fieldsCSV attribute must be defined.') );
	        
	        // Setting field list
	        List<String> fieldList = new List<String>();
	        fieldList.add('Name');
	        //fieldList.add('LastName');
	        controller.fieldsList = fieldList;
	        
	        // Calling search method with incorrect sortDirection
	        controller.sortDirection = 'xyz';
	        controller.getRecords();
	        System.assert(ApexPages.getMessages().get(1).getSummary().contains('sortDirection attribute must have value of "asc" or "desc"') );
	        
	        controller.sortDirection = 'asc';
	        controller.getRecords();
        
        Test.stopTest();
        
        
    }
    
    
    // Method to create test data
    private static void createTestData() {
        
        Profile profile = Intr_TestUtils.fetchInstrumentProfile(Intr_Constants.INSTRUMENTS_CCKM_PROFILE);
        user = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                     LocaleSidKey='en_US', ProfileId = profile.Id, Division = 'IVC',
                     TimeZoneSidKey='America/Los_Angeles', UserName='kaju.jalan@appirio.com');
        
        
        
        OpportunityList = new List<Opportunity>();
        System.runAs(user) {
        account = Intr_TestUtils.createAccount(1, false).get(0);
        account.Name = 'test';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
        insert account;
        
        	
            opportunity = Intr_TestUtils.createOpportunity(1, account.Id,false).get(0);
            opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_Opportunity_RECORD_TYPE ).getRecordTypeId();
            opportunity.Name = 'TestFName';
            opportunity.Type = 'Base';
            opportunity.CloseDate = date.today();
            opportunity.StageName = 'Closed Won';
            opportunity.ForecastCategoryName = 'Commit'; 
            opportunity.Business_Unit__c = 'IVC'; 
            opportunity.AccountId = account.Id;
            OpportunityList.add( opportunity);
            insert OpportunityList;
            
            
            List<Attachment> attach = new List<Attachment>();
            for(Integer i=0;i<10;i++){
                String encodedContentsString = 'This is a test';
                Attachment attachment = new Attachment();
                attachment.Body = Blob.valueOf(encodedContentsString);
                attachment.Name = String.valueOf('test'+i);
                attachment.ParentId = OpportunityList[0].Id;
                attach.add(attachment);
            }
            
            insert attach;
        }
    }
}