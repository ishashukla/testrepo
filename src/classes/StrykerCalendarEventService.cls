/**================================================================      
* Appirio, Inc
* Name: StrykerCalendarEventService
* Description: Used to Show calendar on Home-page using Service Max Event object records

* Date Modified      Modified By      Description of the update
* 07/20/2016         Jagdeep Juneja    T-520405
* 07/22/2016         Jagdeep Juneja    T-520717
* 07/25/2016         Rahul Aeran       T-512027
* 08/11/2016         Nitish Bansal     I-229256 - SVMX events no longer showing on Home page calendar (Modified getServiceMaxEvents method)
==================================================================*/

global class StrykerCalendarEventService {    
    webservice static String createCalendarEvent(String subject, String whoId, String startDate, String startTime, String endDate, String endTime, boolean isAllDay){
        Event newEvent = new Event();
        newEvent.WhoId = whoId;
        newEvent.StartDateTime = Datetime.parse(startDate + ' ' + startTime);
        newEvent.Subject = subject;
        newEvent.EndDateTime = Datetime.parse(endDate + ' ' + endTime);
        newEvent.IsAllDayEvent = isAllDay;
        insert newEvent;
        return newEvent.id;
    }
    
    
    webservice static String updateCalendarEventDateTime(Id eventId, string startDateTime, string endDateTime, boolean isAllDay){
        Event e = [SELECT Id, StartDateTime, EndDateTime FROM Event WHERE Id = :eventId LIMIT 1]; 
        e.StartDateTime = Datetime.valueOf(startDateTime);
        e.EndDateTime = Datetime.valueOf(endDateTime);
        e.IsAllDayEvent = isAllDay;
        System.debug('updateCalendarEventDateTime: ' + e);
        update e;
        return e.id;
    }
    
    webservice static String getMonthlyCalendarEvents(Integer month, Integer year){
        System.debug('Month: ' + month);
        System.debug('Year: ' + year);
        
        DateTime startDate = datetime.newInstance(year, month, 1);
        DateTime endDate = datetime.newInstance(year, month + 1 , 1).addDays(-1);
        string subject;
        string events = '';
        for (Event e : [SELECT Id, IsAllDayEvent, Subject, CreatedBy.Id, CreatedBy.Name, WhoId, Who.Name, Description, StartDateTime, EndDateTime 
                        FROM Event 
                        WHERE StartDateTime >= :startDate 
                            AND StartDateTime <= :endDate 
                        ORDER BY StartDateTime DESC limit 1000]) {  
            if (events.length() > 0){
                events += ',';
            }
            events += '{ "id":"' + e.id + '",';
            events += ' "url":"' + '/' + e.id + '",';
            events += ' "whoId":"' + e.WhoId + '",';
            events += ' "whoName":"' + e.Who.Name + '",';
            events += ' "whoUrl":"' + '/' + e.WhoId + '",';
            events += ' "start":"' + e.StartDateTime.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            events += ' "end":"' + e.EndDateTime.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            events += ' "allDay":' + e.IsAllDayEvent  + ',';
            events += ' "createdById":"' + e.CreatedBy.Id + '",';
            events += ' "createdByName":"' + e.CreatedBy.Name + '",';
            
            subject = e.Subject;
            if (subject == null){
                subject = '';
            }
            
            events += ' "title":"' +  subject   + '"';
            events += '}';
        }
        events = '[' + events + ']'; 
        System.debug('events are :'+events );
        return events;    
    }
    
    // Methods to be executed When page is loaded and search button is clicked on strykerCalender visualforce page.
    // Description- Get all calender events recored and work order custom object records that meet the criteria.
    // @param newRecords - Month,year that show the duration of events on calender and accId,ownerId,state represents the AccountId,OwnerId and 
    // Account shipping state that has been selected through the search criteria.
    // @return Concatenated string result of Calender events records and Work order records in JSON format.
    // Added by Jagdeep Juneja
   webservice static String getMonthlyWorkOrders(Integer month, Integer year, String accId, String ownerId, String state){
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String profileName = PROFILE[0].Name;
        String currentUserId = UserInfo.getUserId();
                
        Boolean IsSchedulingManager = [SELECT Count()
            FROM SetupEntityAccess
            WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'Scheduling_Manager_COMM')
                AND ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId = :currentUserId)] > 0;
        
        System.debug('Month: ' + month);  
        System.debug('Year: ' + year); 
        System.debug('accid is '+accId);
        System.debug('ownerId is '+ownerId);
        System.debug('state is '+state); 
        
        DateTime startDate = datetime.newInstance(year, month, 1);
        if(profileName.equals('Sales Rep - COMM US')){
            Date dToday = Date.today();
            startDate = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
        }
        
        System.debug('startDate'+startDate);
        DateTime endDate = datetime.newInstance(year, month + 1 , 1).addDays(-1);
        System.debug('end date'+endDate);
        string subject;
        string events = '';
        String query = 'SELECT Id,Name,CreatedBy.Id,CreatedBy.Name,SVMXC__Problem_Description__c,SVMXC__Contact__r.Name,SVMXC__Contact__c,'+
                ' SVMXC__Scheduled_Date_Time__c,SVMXC__Preferred_End_Time__c,SVMXC__Company__c,SVMXC__Company__r.Name, OwnerId,SVMXC__Company__r.ShippingState, '+
                ' SVMXC__Group_Member__r.Id,SVMXC__Group_Member__r.Name,Project_Phase__r.Project_Plan__r.Project_Manager__r.Name, Project_Phase__r.Project_Plan__r.Sales_Rep__r.Id '+
                ' FROM SVMXC__Service_Order__c '+
                ' WHERE SVMXC__Scheduled_Date_Time__c >= :startDate ';
                //' AND SVMXC__Scheduled_Date_Time__c <=  :endDate ';
                
                
        if(profileName.equals('Sales Rep - COMM US')){
            query += ' AND SVMXC__Scheduled_Date_Time__c < NEXT_N_MONTHS:6 ';
            query += ' AND SVMXC__Order_Status__c != \'Closed\' AND SVMXC__Order_Status__c != \'Canceled\' AND SVMXC__Order_Status__c != \'Rejected\' ';
            query += ' AND Project_Phase__r.Project_Plan__r.Sales_Rep__r.Id =:currentUserId ';  
        }       
        else{
            query += ' AND SVMXC__Scheduled_Date_Time__c <=  :endDate';
        }
        
        if(profileName.equals('Field Service Technician - COMM US')){
            // query += ' AND SVMXC__Group_Member__r.Id =:currentUserId ';  
            query += ' AND SVMXC__Group_Member__r.SVMXC__Salesforce_User__r.id =:currentUserId';
        }
        
        if(profileName.equals('Service Project Manager - COMM US') && !IsSchedulingManager){
            query += ' AND (SVMXC__Order_Status__c = \'Approved - No Resources Allocated\' OR SVMXC__Order_Status__c = \'Approved - Resources Allocated\')';
            query += 'AND Project_Phase__r.Project_Plan__r.Project_Manager__r.Id =:currentUserId ';  
        }
                 
        if(!String.isBlank(accId) && !(accId.equals('000000000000000')) ){ 
            query += ' AND SVMXC__Company__r.Id=: accId';
        }
        if(!String.isBlank(ownerId)  && !(ownerId.equals('000000000000000')) ){
            List<String> csvList = ownerId.split(',') ;
            query += ' AND SVMXC__Group_Member__c IN :csvList';
        }
        if(!String.isBlank(state) ){
          query += ' AND SVMXC__Company__r.ShippingState=: state';
        }
        
        
        query += ' ORDER BY SVMXC__Scheduled_Date_Time__c DESC limit 1000  ';
        System.debug('Query :'+query);
        for (SVMXC__Service_Order__c sso : Database.query(query)) {  
                System.debug('first value of event is '+events);
            if (events.length() > 0){
              
                events += ',';
            }
            //Edited by Varun Vasishtha to escape double quotes in names
            string whoName;
            if(sso.SVMXC__Contact__r.Name != null)
            {
                whoName = sso.SVMXC__Contact__r.Name.escapeEcmaScript();
            }
            System.debug('second value of event is '+events);
            events += '{ "id":"' + sso.id + '",';
            events += ' "url":"' + '/' + sso.id + '",';
            events += ' "accName":"' +  sso.SVMXC__Company__r.Name  + '",';
            events += ' "techName":"' +  sso.SVMXC__Group_Member__r.Name + '",';
            events += ' "pmName":"' +  sso.Project_Phase__r.Project_Plan__r.Project_Manager__r.Name + '",';
            events += ' "whoId":"' + sso.SVMXC__Contact__c + '",';
            events += ' "whoName":"' + whoName + '",';
            events += ' "whoUrl":"' + '/' + sso.SVMXC__Contact__c + '",';
            if(sso.SVMXC__Scheduled_Date_Time__c!=null){
              events += ' "start":"' + sso.SVMXC__Scheduled_Date_Time__c.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            }
            else{
              //events += ' "start":"' +'null'+ '",';
            }
            
            if(sso.SVMXC__Preferred_End_Time__c!=null){
              events += ' "end":"' + sso.SVMXC__Preferred_End_Time__c.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            }
            else{
             // events += ' "end":"' + 'null'  + '",';
            }
            
            //events += ' "allDay":' + e.IsAllDayEvent  + ',';
            //events += ' "allDay":' + 'true'  + ',';
            events += ' "createdById":"' + sso.SVMXC__Group_Member__r.Id + '",';
            events += ' "createdByName":"' + sso.SVMXC__Group_Member__r.Name + '",';
            subject = 'This is test subject';
            if (subject == null){
                subject = '';
            }
            
            events += ' "title":"' +sso.Name+ '"';
            events += '}';
            
            
        }
        //events = events+',';
        System.debug('juneja events are :'+events );
        
        String result = '';
        for (Event e : [SELECT Id, IsAllDayEvent, Subject, CreatedBy.Id, CreatedBy.Name, WhoId, Who.Name, Description, StartDateTime, EndDateTime FROM Event WHERE StartDateTime >= :startDate AND StartDateTime <=  :endDate 
                        AND OwnerId=: currentUserId ORDER BY StartDateTime DESC limit 1000]) {  
            if (result.length() > 0){
                result += ',';
            }
            string whoName;
            if(e.Who.Name != null)
            {
                whoName = e.Who.Name.escapeEcmaScript();
            }
            result += '{ "id":"' + e.id + '",';
            result += ' "url":"' + '/' + e.id + '",';
            result += ' "whoId":"' + e.WhoId + '",';
            result += ' "whoName":"' + whoName + '",';
            result += ' "whoUrl":"' + '/' + e.WhoId + '",';
            result += ' "start":"' + e.StartDateTime.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            result += ' "end":"' + e.EndDateTime.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            result += ' "allDay":' + e.IsAllDayEvent  + ',';
            result += ' "createdById":"' + e.CreatedBy.Id + '",';
            result += ' "createdByName":"' + e.CreatedBy.Name + '",';
            
            subject = e.Subject;
            if (subject == null){
                subject = '';
            }

            result += ' "title":"' + subject + '"';
            result += '}';
        }
        
        System.debug('result is '+result);
        if(result.length() > 0)
        {
            if(events.length() > 0)
            {
                events = '['+events+',' +result+']';
            }
            else
            {
                events = '['+result+']';
            }
        }
        else
        {
            events = '['+events+']';
        }
        
        System.debug('events is '+events);
        //string substring = ',';
        //integer index = events.lastIndexOf(substring);
        //if (index == -1)
        //{
        //  return events;
       // }
        //else
        //{
        //  return events.substring(0, index) + '' + events.substring(index+substring.length());
        //} 
        return events;
        
      } 
    
    
    webservice static String getMatchingContacts(String prefix){
        prefix += '%';
        string contacts = '';
        for (Contact c : [SELECT Id, Name FROM Contact WHERE Name LIKE :prefix ORDER BY Name ASC LIMIT 100]) {  
            if (contacts.length() > 0){
                contacts += ',';
            }
            contacts += '{ "Id":"' + c.Id + '",';
            contacts += ' "Name":"' + c.Name + '"';
            contacts += '}';
        }
        contacts = '[' + contacts + ']';        
        return contacts;
    }    
    
    //NB - 08/11- I-229256 - Start
    // Methods to be executed for home page components for Project home page layout only
    // Description- Get all service max events for a user
    // @param  Month,year that show the duration of events 
    // @return Concatenated string result of service max events records in JSON format.
    // Added by Rahul Aeran
    webservice static String getServiceMaxEvents(Integer month, Integer year){
        DateTime startDate = datetime.newInstance(year, month, 1);
        DateTime endDate = datetime.newInstance(year, month + 1 , 1).addDays(-1);
        System.debug('Start Date:'+startDate + ',EndDate:'+endDate);
        string subject;
        string events = '';
        for (SVMXC__SVMX_Event__c e : [SELECT Id, Name, CreatedBy.Id, CreatedBy.Name, SVMXC__WhatId__c,SVMXC__IsAllDayEvent__c,SVMXC__Description__c,
                                      SVMXC__WhoId__c,SVMXC__Technician__c,SVMXC__Technician__r.SVMXC__Salesforce_User__c, 
                                      SVMXC__Technician__r.SVMXC__Salesforce_User__r.Name,SVMXC__StartDateTime__c, 
                                      SVMXC__EndDateTime__c ,SVMXC__Service_Order__c,SVMXC__Service_Order__r.Name, 
                                      SVMXC__Service_Order__r.Project_Phase__r.Project_Plan__r.Name, 
                                      SVMXC__Service_Order__r.Project_Phase__r.Project_Plan__r.Account__r.Name
                                      FROM SVMXC__SVMX_Event__c
                                      WHERE
                                      //events can go in months so commenting out  
                                      //((SVMXC__StartDateTime__c >= :startDate  AND SVMXC__EndDateTime__c <=  :endDate)
                                      //OR (SVMXC__StartDateTime__c <= :startDate  AND SVMXC__EndDateTime__c >=  :startDate)) 
                                      //AND 
                                      (SVMXC__Technician__r.SVMXC__Salesforce_User__c =:UserInfo.getUserId())
                                      ORDER BY SVMXC__StartDateTime__c 
                                      DESC limit 1000]) {  
            if (events.length() > 0){ 
                events += ',';
            }
            events += '{ "id":"' + e.id + '",';
            events += ' "url":"' + System.URL.getSalesforceBaseURL().toExternalForm() + '/' + e.id + '",';
            events += ' "whoId":"' + e.SVMXC__Technician__r.SVMXC__Salesforce_User__c + '",';
            events += ' "whoName":"' + e.SVMXC__Technician__r.SVMXC__Salesforce_User__r.Name + '",';
            events += ' "whoUrl":"' + '/' + e.SVMXC__Technician__r.SVMXC__Salesforce_User__c + '",';
            events += ' "start":"' + e.SVMXC__StartDateTime__c.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            events += ' "end":"' + e.SVMXC__EndDateTime__c.format('EEE, d MMM yyyy HH:mm:ss Z')  + '",';
            events += ' "allDay":' + e.SVMXC__IsAllDayEvent__c + ',';
            events += ' "createdById":"' + e.CreatedBy.Id + '",';
            events += ' "createdByName":"' + e.CreatedBy.Name + '",';
            
            subject = e.Name;
            if (subject == null){
                subject = '';
            }
            
            if (e.SVMXC__Technician__r.SVMXC__Salesforce_User__r.Name != null){
                subject += '\\n' + e.SVMXC__Technician__r.SVMXC__Salesforce_User__r.Name;
            }
            if (e.SVMXC__Service_Order__r != null && e.SVMXC__Service_Order__r.Name != null){
                subject += '\\n' + e.SVMXC__Service_Order__r.Name;
            }
            if(e.SVMXC__Service_Order__r != null && e.SVMXC__Service_Order__r.Project_Phase__c != null && 
                e.SVMXC__Service_Order__r.Project_Phase__r.Project_Plan__r.Name != null){
                subject += '\\n' + e.SVMXC__Service_Order__r.Project_Phase__r.Project_Plan__r.Name;
            }
            if(e.SVMXC__Service_Order__r != null && e.SVMXC__Service_Order__r.Project_Phase__c != null && 
                e.SVMXC__Service_Order__r.Project_Phase__r.Project_Plan__r.Account__r.Name != null){
                subject += '\\n' + e.SVMXC__Service_Order__r.Project_Phase__r.Project_Plan__r.Account__r.Name;
            }

            
            events += ' "title":"' +  subject   + '"';
            events += '}'; 
        }
        events = '[' + events + ']'; 
        System.debug('events are :'+events );
        return events;    
    } 
    //NB - 08/11- I-229256 - End
}