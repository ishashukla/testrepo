/*************************************************************************************************
Created By:    Rahul Khanchandani
Date:          13/04/2016
Description  : CaseLineItemTrigger to populated Calculate Total Amount Using Amount From Case Line Item
**************************************************************************************************/
public class CaseLineItemTriggerHandler {
  // Methods to be executed after record is inserted
  // @param cliNewList- List to hold Trigger.new
  // @return Nothing
  public static void afterInsert(List<Case_Line_Item__c> cliNewList) {
        Set<Id> caseIdSet = new Set<Id>();
        for (Case_Line_Item__c cli : cliNewList) {
            caseIdSet.add(cli.Change_Order__c);
        }
        reCalculateGrandTotal(caseIdSet);
    }
    // Methods to be executed after record is updated
    // @param cliNewList- List to hold Trigger.new
    // @param cliOldMap -  List to hold Trigger.oldMap
    // @return Nothing
    public static void afterUpdate(List<Case_Line_Item__c> cliNewList, Map<Id, Case_Line_Item__c> cliOldMap) {
       Set<Id> caseIdSet = new Set<Id>();
       for (Case_Line_Item__c cli : cliNewList) {
            if (cli.Change_Order__c != cliOldMap.get(cli.Id).Change_Order__c) {
                caseIdSet.add(cli.Change_Order__c);
                caseIdSet.add(cliOldMap.get(cli.Id).Change_Order__c);
            }
            else if (cli.Amount__c != cliOldMap.get(cli.Id).Amount__c ) {
                caseIdSet.add(cli.Change_Order__c);
            }
        }
        
        reCalculateGrandTotal(caseIdSet);
    }
    // Methods to be executed after record is deleted
    // @param cliNewList- List to hold Trigger.old
    // @return Nothing
    public static void afterDelete(List<Case_Line_Item__c> cliOldList) {
        Set<Id> caseIdSet = new Set<Id>();
        for (Case_Line_Item__c cli : cliOldList) {
            caseIdSet.add(cli.Change_Order__c);
        }
        reCalculateGrandTotal(caseIdSet);
    }
    // Methods to be executed after record is updated
    // @param cliNewList- List to hold Trigger.new
    // @return Nothing
    public static void afterUndelete(List<Case_Line_Item__c> cliNewList) {
        Set<Id> caseIdSet = new Set<Id>();
        for (Case_Line_Item__c cli : cliNewList) {
            caseIdSet.add(cli.Change_Order__c);
        }
        reCalculateGrandTotal(caseIdSet);
    }
    // Methods to be recalculate Total Amount On Case Using Amount From Case Line Item
    // @param caseIdSet- Set to store Id of case
    // @return Nothing   
    public static void reCalculateGrandTotal(Set<Id> caseIdSet) {
        List<Case> caseToUpdate = new List<Case>();
        for (Case cas : [SELECT Id, Total_amount__c,CreatedDate, (SELECT Amount__c FROM Case_Line_Items__r)
                            FROM Case   
                            WHERE Id in :caseIdSet]) {
                            cas.Total_amount__c = 0;
                            //cas.CreatedDate= System.Today();
                            
        // DM 08/01 Added null check on related list to fix null pointer
        if(cas.Case_Line_Items__r != null && cas.Case_Line_Items__r.size() > 0){
            for (Case_Line_Item__c cli : cas.Case_Line_Items__r){
              cas.Total_amount__c = cas.Total_amount__c + cli.Amount__c;
                
            }
            caseToUpdate.add(cas);
          }
        }
        update caseToUpdate;
    }
}