public with sharing class ChatterUploaderCtrl {

    //TODO: Need remove Title with logo from page UploadCourses when we use ChatterUpload functional
    
    public String uploadId {get{
        if (uploadId!=null) return uploadId;
        if (getPageParam().get('uploadid') != null) {
            return getPageParam().get('uploadid');
        } else {
            return null;
        }
    } set;}
    
    public String getUploadCourseUrl() {
        PageReference pageRef = Page.scormanywhere__UploadCourses;
        pageRef.getParameters().put('inline', '1');
        pageRef.getParameters().put('chatter', '1');
        pageRef.getParameters().put('uploaderstyle', 'chatter');
        pageRef.getParameters().put('height', '300');
        pageRef.getParameters().put('callbackURL', 
            'https://' + ApexPages.currentPage().getHeaders().get('Host') 
            + Page.ChatterUploader.getUrl() + '?uploaderstyle=chatter&chatter=1'
        );
        return pageRef.getUrl();
    }

    public PageReference redirectWithParams() {
        if (uploadId!=null) {
            String courseId = null;
            scormanywhere__Course__c course = new scormanywhere__Course__c();
            if(Test.isRunningTest()){
                course = [SELECT Name, scormanywhere__SCORM_Training_Path__c FROM scormanywhere__Course__c LIMIT 1];
                courseId = course.id;
            } else {
                courseId = scormanywhere.ApiService.createCourse(uploadId);
                course = [SELECT Name, scormanywhere__SCORM_Training_Path__c FROM scormanywhere__Course__c WHERE Id = :courseId];
            }                    
            String courseParentIdentityPath = '%' + course.scormanywhere__SCORM_Training_Path__c.split('/')[0] + '%';
            List<scormanywhere__Course__c> upCourses = [
                SELECT 
                    Id, Name, scormanywhere__SCORM_Training_Path__c 
                FROM 
                    scormanywhere__Course__c 
                WHERE 
                    scormanywhere__SCORM_Training_Path__c LIKE :courseParentIdentityPath
            ];
            List<FeedItem> posts = new List<FeedItem>();
            for (scormanywhere__Course__c upCourse : upCourses) {
                FeedItem post = new FeedItem();
                PageReference upCourseRef = Page.scormanywhere__SCORM_Player;
                upCourseRef.getParameters().put('inline', '1');
                upCourseRef.getParameters().put('courseId', upCourse.Id);
                post.Body = 'Uploaded new Course:';
                post.type = 'LinkPost';
                post.Title = upCourse.Name;
                post.LinkUrl = upCourseRef.getUrl();
                post.parentId = UserInfo.getUserId();
                posts.add(post);
            }
            Database.insert(posts);
            return new PageReference('/_ui/core/chatter/ui/ChatterPage');
        } else {
            return null;
        }
    }
    
    private Map<String, String> getPageParam() {
        Map<String, String> result = new Map<String, String>();
        String fullUrl = ApexPages.currentPage().getURL();
        if (fullUrl.indexOf('?')>=0) {
            List<String> partsOfUrl = multiSplit(fullUrl, new List<String>{'?','%3F'});
            partsOfUrl.remove(0);
            String paramsString = partsOfUrl[0];
            partsOfUrl.remove(0);
            if (partsOfUrl.size()>0) {
                for (String partOfUrl:partsOfUrl) {
                    paramsString += '&'+partOfUrl;
                }
            }
            List<String> paramsList = multiSplit(paramsString, new List<String>{'&','%26'});
            for (String paramSource:paramsList) {
                List<String> paramParts = multiSplit(paramSource, new List<String>{'=','%3D'});
                if (paramParts.size()==2) {
                    result.put(paramParts[0],paramParts[1]);
                }
            }
        }
        //LowerCase for keys of GET parameters fix
        for (String keyParam:result.keySet()) {
            if (!keyParam.equals(keyParam.toLowerCase())) {
                result.put(keyParam.toLowerCase(), result.get(keyParam));
                result.remove(keyParam);
            }
        }
        return result;
    }
    
    private List<String> multiSplit(String src, List<String> splitters) {
        List<String> result = src.split(splitters[0].replace('?','\\?').replace('%','\\%'));
        splitters.remove(0);
        for (String splitter:splitters) {
            String encodeSplitter = splitter.replace('?','\\?').replace('%','\\%');
            for (Integer i = 0; i<result.size(); i++) {
                if (result[i].indexOf(splitter)>=0) {
                    List<String> newSplit = result[i].split(encodeSplitter);
                    result.remove(i--);
                    result.addAll(newSplit);
                }
            }
        }
        return result;
    }
}