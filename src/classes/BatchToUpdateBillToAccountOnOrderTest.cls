/*******************************************************************************
Class           : BatchToUpdateBillToAccountOnOrderTest 
Created by      : Shubham Paboowal
Date            : Sept 15,2016

*******************************************************************************/
@isTest
private class BatchToUpdateBillToAccountOnOrderTest {

    static testMethod void myUnitTest() {
        List<Account> accList = new List<Account>();
        for(Account acc : Medical_TestUtils.createAccount(3, false)){
        	acc.Billing_Account_Name__c = 'Test Billing';
        	acc.Billing_Account_Number__c = '1234';
        	acc.AccountNumber = '123';
        	acc.Name = 'Test Account';
        	acc.Service_Billing_Account_Name__c = 'Test Shipping';
        	acc.Service_Billing_Account_Number__c = '1234';
        	acc.Billing_Type__c = 'Bill To and Ship To Address';
        	accList.add(acc);
        }
        if(accList.size() > 0)
        	insert accList;
        Account accToSearch = new Account();
        accToSearch.Name = 'Test Billing';
        accToSearch.AccountNumber = '1234';
        insert accToSearch;
        Test.startTest();
        BatchToUpdateBillToAccountOnOrder batch = new BatchToUpdateBillToAccountOnOrder();
        Database.executeBatch(batch, 10);
        Test.stopTest();
    }
}