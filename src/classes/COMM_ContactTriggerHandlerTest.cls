// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_ContactTriggerHandler - Trigger Handller for Contact
//
// 02/26/2016, Deepti Maheshwari (T-477658)
//
@isTest
private class COMM_ContactTriggerHandlerTest {
	static Contact con1,con2;
	static list<User> user;
	private static testMethod void test() {
		createTestData();
		Test.startTest();
		insert con1;
		insert con2;
		con1.Private__c = false;
		update con1;
		con2.Private__c = true;
		
		update con2;
		List<ContactShare> contactSharesWhenPrivate = [SELECT ID from ContactShare
		                                              WHERE contactID = :con2.id 
		                                              AND RowCause = :Constants.ROW_CAUSE_MANUAL];
		system.assert(contactSharesWhenPrivate == null || contactSharesWhenPrivate.size() == 0);
		
		ContactShare conShare = new ContactShare();
		conShare.contactID = con2.id;
		conShare.ContactAccessLevel  = Constants.PERM_READ;
		conShare.UserOrGroupID = user.get(0).id;
		insert conShare;		
		con2.OwnerID = user.get(1).id;
		update con2;
		List<ContactShare> contactShares = [SELECT ID 
		                                    FROM ContactShare 
		                                    WHERE contactID = :con2.id 
		                                    AND RowCause = :Constants.ROW_CAUSE_MANUAL];
		system.assert(contactShares.size() > 0);
		Test.stopTest();
	}
	private static void createTestData(){
    List<Account> accList = TestUtils.createAccount(1, true);
    TestUtils.createCommConstantSetting();
    user = TestUtils.createUser(2,'Customer Service - COMM INTL', true);
    AccountTeamMember atm = TestUtils.createTeamMembers(accList.get(0).id, user.get(0).id, 'Service Manager',true);
    con1 = TestUtils.createCOMMContact(accList.get(0).id, 'TestContact', false);
    
    con1.Private__c = true;
    
    con2 = TestUtils.createCOMMContact(accList.get(0).id, 'TestContact2', false);
    //con2.Private_Contact_Account__c = accList.get(0).Id;
    //nsert con2;
    /*ContactShare conShare = new ContactShare();
    conShare.contactID = con2.id;
    conShare.ContactAccessLevel  = Constants.PERM_READ;
    conShare.UserOrGroupID = user.get(0).id;
    insert conShare;*/
	}
}