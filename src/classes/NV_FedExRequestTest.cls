/**
 * This class is for service invoke and parsing test 
 */
/*
  Modified By :   Jessica Schilling
  Date        :   6/02/2016
  Case        :   Case 169518
*/

@isTest
public class NV_FedExRequestTest {
    // creating custom setting 
    public static testMethod FedExServiceConfig__c createConfig() {
        FedExServiceConfig__c config = new FedExServiceConfig__c();
        config.FedEx_AccountNumber__c = '123';
        config.FedEx_IntegratorId__c = '123';
        config.FedEx_Key__c = '123';
        config.FedEx_MeterNumber__c = '123';
        config.FedEx_Password__c = '123';
        config.Interval__c = 15;
        config.Max_Number_allowed__c = 30;
        config.Max_Wait__c = 120;
        config.Package_Identifier__c = '123';
        config.Name = 'Service Call Config';
        insert config;
        return config;
    }
    // creating orders 
    public static testMethod void createOrderItem(){
        Account testAcc = new Account (Name ='Sample Acc');
        insert testAcc;
        
        // creating Contract 
        
        Contract testContract = new Contract (AccountId = testAcc.Id,Status='Draft',StartDate = System.today(), ContractTerm = 1, External_Integration_Id__c = '123123');
        insert  testContract;
        Id standardPriceBookId = Test.getStandardPricebookId(); 
        Order sampleOrder = new Order(AccountId = testAcc.Id , ContractId=testContract.Id, Status='Draft',EffectiveDate = System.today(), Date_Ordered__c = Datetime.now() ,Pricebook2Id = standardPriceBookId);
        insert sampleOrder;
        
        Product2 pd = new Product2(Name='Pord A',isActive=true, Catalog_Number__c = '1');
        pd.IsActive = true;
        insert pd;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert pbe;

        //JSchilling Case 169518 6/02/2016
        //Added Shipped_Date__c to below order
        OrderItem ordPd = new OrderItem(PriceBookEntryId=pbe.Id, OrderId=sampleOrder.Id, Quantity=1, UnitPrice=99,Tracking_Number__c='12345', Shipped_Date__c = Date.today());

        // Putting duplicate Tracking Number .
        OrderItem ordPdOne = new OrderItem(PriceBookEntryId=pbe.Id, OrderId=sampleOrder.Id, Quantity=1, UnitPrice=99,Tracking_Number__c='12345');

        OrderItem ordPdTwo = new OrderItem(PriceBookEntryId=pbe.Id, OrderId=sampleOrder.Id, Quantity=1, UnitPrice=99,Tracking_Number__c='12345', Order_Delivered__c=false);

        insert new List<OrderItem> {
          ordPd,
          ordPdOne,
          ordPdTwo
        };
    }
    // creating success response 
    public static testMethod NV_FedExTrackingService.TrackReply  createSuccessResponse(){
        // creating response
        NV_FedExTrackingService.TrackReply response = new NV_FedExTrackingService.TrackReply();
        // creating success status 
        response.HighestSeverity = 'SUCCESS';
        //instantiate track details 
        response.CompletedTrackDetails = new List<NV_FedExTrackingService.CompletedTrackDetail>();
        //instantiate single row of track detail 
        NV_FedExTrackingService.CompletedTrackDetail completedDetail = new NV_FedExTrackingService.CompletedTrackDetail();
        completedDetail.HighestSeverity = 'SUCCESS';
        //instantiate  track detail 
        completedDetail.TrackDetails = new List<NV_FedExTrackingService.TrackDetail>();
        // creating object 
        NV_FedExTrackingService.TrackDetail trackDetail = new NV_FedExTrackingService.TrackDetail();
        trackDetail.TrackingNumber = '12345';
        
        //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - Start
    	//Assigned ActualDeliveryTimestamp
    	trackDetail.ActualDeliveryTimestamp = Datetime.now();
        //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - End
    	
        //creating delivered status
        NV_FedExTrackingService.TrackStatusDetail StatusDetail = new NV_FedExTrackingService.TrackStatusDetail();
        StatusDetail.Description = 'Delivered';
        //asssign to status 
        trackDetail.StatusDetail = StatusDetail;
        // adding to list of track 
        completedDetail.TrackDetails.add(trackDetail);
        //adding to completed details;
        response.CompletedTrackDetails.add(completedDetail);
        return response;
        
    }
    // creating failure response 
    public static testMethod NV_FedExTrackingService.TrackReply  createFailureResponse(){
        // creating response
        NV_FedExTrackingService.TrackReply response = new NV_FedExTrackingService.TrackReply();
        // creating success status 
        response.HighestSeverity = 'SUCCESS';
        //instantiate track details 
        response.CompletedTrackDetails = new List<NV_FedExTrackingService.CompletedTrackDetail>();
        //instantiate single row of track detail 
        NV_FedExTrackingService.CompletedTrackDetail completedDetail = new NV_FedExTrackingService.CompletedTrackDetail();
        completedDetail.HighestSeverity = 'SUCCESS';
        //instantiate  track detail 
        completedDetail.TrackDetails = new List<NV_FedExTrackingService.TrackDetail>();
        // creating object 
        NV_FedExTrackingService.TrackDetail trackDetail = new NV_FedExTrackingService.TrackDetail();
        trackDetail.TrackingNumber = '9999';
        //creating delivered status
        NV_FedExTrackingService.TrackStatusDetail StatusDetail = new NV_FedExTrackingService.TrackStatusDetail();
        NV_FedExTrackingService.Notification Notification = new NV_FedExTrackingService.Notification();
        Notification.Severity = 'ERROR';
        Notification.Message = 'This tracking number cannot be found. Please check the number or contact the sender.';
        trackDetail.Notification = Notification;
        //asssign to status 
        trackDetail.StatusDetail = StatusDetail;
        // adding to list of track 
        completedDetail.TrackDetails.add(trackDetail);
        //adding to completed details;
        response.CompletedTrackDetails.add(completedDetail);
        return response;
        
    }
    // creating failure response 
    public static testMethod NV_FedExTrackingService.TrackReply  createFailureHighLevelResponse(){
        // creating response
        NV_FedExTrackingService.TrackReply response = new NV_FedExTrackingService.TrackReply();
        // creating success status 
        response.HighestSeverity = 'ERROR';
        NV_FedExTrackingService.Notification Notification = new NV_FedExTrackingService.Notification();
        Notification.Message = 'Authentatication Failed';
        response.Notifications =  new List<NV_FedExTrackingService.Notification>();
        response.Notifications.add(Notification);
        return response;
        
    }
    // testing the Scheduler
    public static testMethod void testScheduler(){
        createConfig();
        createOrderItem();

        Test.startTest();

        SchedulableContext sc = null;
        NV_ScheduledFedExStatus tsc = new NV_ScheduledFedExStatus();
        tsc.execute(sc);

        tsc = new NV_ScheduledFedExStatus(10);
        tsc.execute(sc);

        Database.executeBatch(new NV_ScheduledFedExStatus(), 10);

        Test.stopTest();
    }
    // testing single orderItem
    public static testMethod void testSingleOrder(){
        FedExServiceConfig__c config = createConfig();
        config.Max_Number_allowed__c = 1;
        update config;

        createOrderItem();  
        NV_FedExRequest request = new NV_FedExRequest();
        //Logic for S-371704 : Adding Tracked Number in query
        List<OrderItem> orderItems = [SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c FROM OrderItem WHERE Order_Delivered__c = false AND Tracked_Number__c != NULL];
        request.TrackFedExRequest(orderItems);          
    }
    // Positive test 
    public static testMethod void testParseResponsePositive(){
        createOrderItem();  
        Test.startTest();
            NV_FedExRequest request = new NV_FedExRequest();
            //Logic for S-371704 : Adding Tracked Number in query
            List<OrderItem> orderItemList = [SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c,Order.OwnerId FROM OrderItem WHERE Order_Delivered__c = false AND Tracked_Number__c != NULL];
            request.orderItemMap.put('12345',orderItemList);
            orderItemList = request.parseResponse(createSuccessResponse());
            //orderItemList = [SELECT Id, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c,Order.OwnerId FROM OrderItem ];
            System.assertEquals(orderItemList[0].Order_Delivered__c,true);
        Test.stopTest();
    }
    // negative test tracking number not found 
    public static testMethod void testParseResponseNegative(){
        createOrderItem();  
        Test.startTest();
            NV_FedExRequest request = new NV_FedExRequest();
            //Logic for S-371704 : Adding Tracked Number in query
            List<OrderItem> orderItemList = [SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c,Order.OwnerId FROM OrderItem WHERE Order_Delivered__c = false AND Tracked_Number__c != NULL];
            request.orderItemMap.put('9999',orderItemList);
            request.parseResponse(createFailureResponse());
            //Logic for S-371704 : Adding Tracked Number in query
            orderItemList = [SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c,Order.OwnerId FROM OrderItem];
            System.assertEquals(orderItemList[0].Order_Delivered__c,false);
        Test.stopTest();
    }
    // negative test auth failure 
    public static testMethod void testParseResponseNegativeAuthFailure(){
        createOrderItem();  
        Test.startTest();
            NV_FedExRequest request = new NV_FedExRequest();
            //Logic for S-371704 : Adding Tracked Number in query
            List<OrderItem> orderItemList = [SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c,Order.OwnerId FROM OrderItem WHERE Order_Delivered__c = false AND Tracked_Number__c != NULL];
            request.orderItemMap.put('9999',orderItemList);
            request.parseResponse(createFailureHighLevelResponse());
            //List<FedEx_Service_Error_Log__c> errorLog = [SELECT ID FROM FedEx_Service_Error_Log__c];
            //system.assertNotEquals(errorLog[0].Id,null);
        Test.stopTest();
    }
    
    // this is for service class to increase coverage 
    public static testMethod void serviceClassinitiate(){
        
        NV_FedExTrackingService.StringBarcode  var1     =  new  NV_FedExTrackingService.StringBarcode();
        NV_FedExTrackingService.TrackOtherIdentifierDetail var2     =  new  NV_FedExTrackingService.TrackOtherIdentifierDetail();
        NV_FedExTrackingService.TrackReply var3     =  new  NV_FedExTrackingService.TrackReply();
        NV_FedExTrackingService.NaftaCommodityDetail   var4     =  new  NV_FedExTrackingService.NaftaCommodityDetail();
        NV_FedExTrackingService.TrackNotificationRecipientDetail   var5     =  new  NV_FedExTrackingService.TrackNotificationRecipientDetail();
        NV_FedExTrackingService.Notification   var6     =  new  NV_FedExTrackingService.Notification();
        NV_FedExTrackingService.SendNotificationsRequest   var7     =  new  NV_FedExTrackingService.SendNotificationsRequest();
        NV_FedExTrackingService.Money  var8     =  new  NV_FedExTrackingService.Money();
        NV_FedExTrackingService.DateRange  var9     =  new  NV_FedExTrackingService.DateRange();
        NV_FedExTrackingService.Commodity  var10    =  new  NV_FedExTrackingService.Commodity();
        NV_FedExTrackingService.WebAuthenticationCredential    var11    =  new  NV_FedExTrackingService.WebAuthenticationCredential();
        NV_FedExTrackingService.TransactionDetail  var12    =  new  NV_FedExTrackingService.TransactionDetail();
        NV_FedExTrackingService.Localization   var13    =  new  NV_FedExTrackingService.Localization();
        NV_FedExTrackingService.WebAuthenticationDetail    var14    =  new  NV_FedExTrackingService.WebAuthenticationDetail();
        NV_FedExTrackingService.ClientDetail   var15    =  new  NV_FedExTrackingService.ClientDetail();
        NV_FedExTrackingService.TrackSpecialHandling   var16    =  new  NV_FedExTrackingService.TrackSpecialHandling();
        NV_FedExTrackingService.VersionId  var17    =  new   NV_FedExTrackingService.VersionId();
        NV_FedExTrackingService.SignatureProofOfDeliveryFaxRequest var18    =  new  NV_FedExTrackingService.SignatureProofOfDeliveryFaxRequest();
        NV_FedExTrackingService.TrackReturnDetail  var19    =  new  NV_FedExTrackingService.TrackReturnDetail();
        NV_FedExTrackingService.SignatureProofOfDeliveryLetterReply    var20    =  new  NV_FedExTrackingService.SignatureProofOfDeliveryLetterReply();
        NV_FedExTrackingService.PagingDetail   var21    =  new  NV_FedExTrackingService.PagingDetail();
        NV_FedExTrackingService.SendNotificationsReply var22    =  new  NV_FedExTrackingService.SendNotificationsReply();
        NV_FedExTrackingService.Dimensions var23    =  new  NV_FedExTrackingService.Dimensions();
        NV_FedExTrackingService.PieceCountVerificationDetail   var24    =  new  NV_FedExTrackingService.PieceCountVerificationDetail();
        NV_FedExTrackingService.TrackPackageIdentifier var25    =  new  NV_FedExTrackingService.TrackPackageIdentifier();
        NV_FedExTrackingService.Contact    var26    =  new  NV_FedExTrackingService.Contact();
        NV_FedExTrackingService.CompletedTrackDetail   var27    =  new  NV_FedExTrackingService.CompletedTrackDetail();
        NV_FedExTrackingService.TrackSplitShipmentPart var29    =  new  NV_FedExTrackingService.TrackSplitShipmentPart();
        NV_FedExTrackingService.TrackNotificationPackage   var30    =  new  NV_FedExTrackingService.TrackNotificationPackage();
        NV_FedExTrackingService.CustomerExceptionRequestDetail var31    =  new  NV_FedExTrackingService.CustomerExceptionRequestDetail();
        NV_FedExTrackingService.Address    var32    =  new  NV_FedExTrackingService.Address();
        NV_FedExTrackingService.DeliveryOptionEligibilityDetail    var33    =  new  NV_FedExTrackingService.DeliveryOptionEligibilityDetail();
        NV_FedExTrackingService.ContentRecord  var34    =  new  NV_FedExTrackingService.ContentRecord();
        NV_FedExTrackingService.TrackDetail    var35    =  new  NV_FedExTrackingService.TrackDetail();
        NV_FedExTrackingService.Measure    var36    =  new  NV_FedExTrackingService.Measure();
        NV_FedExTrackingService.SignatureProofOfDeliveryLetterRequest  var37    =  new  NV_FedExTrackingService.SignatureProofOfDeliveryLetterRequest();
        NV_FedExTrackingService.Distance   var38    =  new  NV_FedExTrackingService.Distance();
        NV_FedExTrackingService.EdtExciseCondition var39    =  new  NV_FedExTrackingService.EdtExciseCondition();
        NV_FedExTrackingService.AppointmentTimeDetail  var40    =  new  NV_FedExTrackingService.AppointmentTimeDetail();
        NV_FedExTrackingService.TrackEvent var41    =  new  NV_FedExTrackingService.TrackEvent();
        NV_FedExTrackingService.TrackSpecialInstruction    var42    =  new  NV_FedExTrackingService.TrackSpecialInstruction();
        NV_FedExTrackingService.TrackServiceDescriptionDetail  var43    =  new  NV_FedExTrackingService.TrackServiceDescriptionDetail();
        NV_FedExTrackingService.Weight var44    =  new  NV_FedExTrackingService.Weight();
        NV_FedExTrackingService.LocalTimeRange var45    =  new  NV_FedExTrackingService.LocalTimeRange();
        NV_FedExTrackingService.QualifiedTrackingNumber    var46    =  new  NV_FedExTrackingService.QualifiedTrackingNumber();
        NV_FedExTrackingService.TrackStatusDetail  var47    =  new  NV_FedExTrackingService.TrackStatusDetail();
        NV_FedExTrackingService.TrackAdvanceNotificationDetail var48    =  new  NV_FedExTrackingService.TrackAdvanceNotificationDetail();
        NV_FedExTrackingService.TrackChargeDetail  var49    =  new  NV_FedExTrackingService.TrackChargeDetail();
        NV_FedExTrackingService.EMailNotificationRecipient var50    =  new  NV_FedExTrackingService.EMailNotificationRecipient();
        NV_FedExTrackingService.TrackReconciliation    var51    =  new  NV_FedExTrackingService.TrackReconciliation();
        NV_FedExTrackingService.TrackSelectionDetail   var52    =  new  NV_FedExTrackingService.TrackSelectionDetail();
        NV_FedExTrackingService.NotificationParameter  var53    =  new  NV_FedExTrackingService.NotificationParameter();
        NV_FedExTrackingService.AppointmentDetail  var54    =  new  NV_FedExTrackingService.AppointmentDetail();
        NV_FedExTrackingService.SpecialInstructionStatusDetail var55    =  new  NV_FedExTrackingService.SpecialInstructionStatusDetail();
        NV_FedExTrackingService.TrackRequest   var57    =  new  NV_FedExTrackingService.TrackRequest();
        NV_FedExTrackingService.EMailNotificationDetail    var58    =  new  NV_FedExTrackingService.EMailNotificationDetail();
        NV_FedExTrackingService.CustomsOptionDetail    var59    =  new  NV_FedExTrackingService.CustomsOptionDetail();
        NV_FedExTrackingService.ContactAndAddress  var60    =  new  NV_FedExTrackingService.ContactAndAddress();
        NV_FedExTrackingService.SignatureImageDetail   var61    =  new  NV_FedExTrackingService.SignatureImageDetail();
        NV_FedExTrackingService.SignatureProofOfDeliveryLetterReply    var62    =  new  NV_FedExTrackingService.SignatureProofOfDeliveryLetterReply();
        NV_FedExTrackingService.TrackStatusAncillaryDetail var63    =  new  NV_FedExTrackingService.TrackStatusAncillaryDetail();
        NV_FedExTrackingService.TrackServicePort   var64    =  new  NV_FedExTrackingService.TrackServicePort();
        var64.sendNotifications(null, null, null, null, null, null, null, null, null, null, null, null, null);
        var64.sendSignatureProofOfDeliveryFax(null, null, null, null, null, null, null, null);
        var64.retrieveSignatureProofOfDeliveryLetter(null, null, null, null, null, null, null, null);

        NV_FedExTrackingService.SignatureProofOfDeliveryFaxReply var65 = new NV_FedExTrackingService.SignatureProofOfDeliveryFaxReply();
                
            
    }
}