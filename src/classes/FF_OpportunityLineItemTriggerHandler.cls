/*************************************************************************************************
Created By:    Sunil Gupta
Date:          June 02, 2014
Description:   Handler class for FF_OpportunityLineItemTrigger Trigger

Modified By: Sunil, July 24, 2015, Instead of using OLI.Product.Name we will be using
OLI.Business_Unit__c (S-327105)
Modified By : Deepti Maheshwari, March 25 2014, Updated for task  T-486806
**************************************************************************************************/
public class FF_OpportunityLineItemTriggerHandler {

  public static Set<Id> setOpptyRecordTypeIds;
  //------------------------5-----------------------------------------------------------------------
  //  Copy Location Account field from parent Opportunity
  //-----------------------------------------------------------------------------------------------
  public static void copyLocationAccount(List<OpportunityLineItem> lstOppItems){

    // Eligible Opportunities.
    Set<Id> setOpptyIds = new Set<Id>();
    for(OpportunityLineItem oli :lstOppItems){
      if(oli.OpportunityId != null){
        setOpptyIds.add(oli.OpportunityId);
      }
    }
    System.debug('@@@' + setOpptyIds);

    // SOQL to fetch the parent object fields.
    Set<Id> setRecordTypeIds = new Set<Id>();
    setRecordTypeIds.add(
      Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Constants.RT_FLEX_OPPTY_CONV).getRecordTypeId());

    setRecordTypeIds.add(
      Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Constants.RT_FLEX_OPPTY_RENT).getRecordTypeId());


    // Prepare Map for Opportunity
    Map<Id, Opportunity> mapOpps = new Map<Id, Opportunity>([SELECT Id, Location_Account__c, AccountId
                                              FROM Opportunity
                                              WHERE Id IN :setOpptyIds
                                              AND RecordTypeId IN :setRecordTypeIds]);

    System.debug('@@@' + mapOpps);


    // Update Location Account field on original List of records.
    for(OpportunityLineItem oli :lstOppItems){
     /* Boolean isFlexOp = (mapOpps.containsKey(oli.OpportunityID)) ? isFlexOppty(mapOpps.get(oli.OpportunityID)):true;
      //If Opportunity is not flexed, do not process for functionality and continue
      //to next record to be processed.
      if (!isFlexOp) {
        continue;
      }  */
       // This if block checks if the opportunity associated with OLI is a FLEX oppty
       if(mapOpps.containsKey(oli.OpportunityId) == true){
          Opportunity opp = mapOpps.get(oli.OpportunityId);
          if(opp != null){
            if(opp.Location_Account__c != null){
              oli.Location_Account__c = opp.Location_Account__c;
            }else if(opp.AccountId != null){
              oli.Location_Account__c = opp.AccountId;
            }
          }
          System.debug('@@@' + oli.Location_Account__c);
       }
    }
  }


  //-----------------------------------------------------------------------------------------------
  //  Populate PSR Contact
  //-----------------------------------------------------------------------------------------------
  public static void populatePSRContact(Map<Id, OpportunityLineItem> mapOld, List<OpportunityLineItem> lstNew){
    Set<Id> setOpptyIds = new Set<Id>();
    Set<Id> setAccountIds = new Set<Id>();
    Set<Id> setProductIds = new Set<Id>();
    Set<Id> setTempOpptyIds = new Set<Id>();
    for(OpportunityLineItem oli :lstNew){
      if(oli.OpportunityId != null){
        setTempOpptyIds.add(oli.OpportunityId);
      }
    }
    System.debug('@@@' + setOpptyIds);

    // SOQL to fetch the parent object fields.
    Set<Id> setRecordTypeIds = new Set<ID>{Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Constants.RT_FLEX_OPPTY_CONV).getRecordTypeId(),
                        Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Constants.RT_FLEX_OPPTY_RENT).getRecordTypeId()};
   
    // Prepare Map for Opportunity
    Map<Id, Opportunity> mapOpps = new Map<Id, Opportunity>([SELECT Id, Location_Account__c, AccountId
                                              FROM Opportunity
                                              WHERE Id IN :setTempOpptyIds
                                              AND RecordTypeId IN :setRecordTypeIds]);

    for(OpportunityLineItem oli :lstNew){
      System.debug('~~~debug1.0~~'+oli.Opportunity.RecordTypeId);
      if(mapOpps.containsKey(oli.OpportunityID)){
        if(mapOld == null || (mapOld.get(oli.Id).Location_Account__c != oli.Location_Account__c) || (mapOld.get(oli.Id).Business_Unit__c != oli.Business_Unit__c)){
          setOpptyIds.add(oli.OpportunityId);
    
          if(oli.Location_Account__c != null){
            setAccountIds.add(oli.Location_Account__c);
          }
          if(oli.Product2Id != null){
            setProductIds.add(oli.Product2Id);
          }
        }
      }
    }
    System.debug('@@@' + setOpptyIds);
    System.debug('@@@' + setAccountIds);
    System.debug('@@@xxxx' + setProductIds);

    Id internalContactRT = Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get('Flex Internal Contact').getRecordTypeId();
    System.debug('@@@' + internalContactRT);

    // Fetch all contacts for related Account.
    Map<Id, List<Stryker_Contact__c>> mapContacts = new Map<Id, List<Stryker_Contact__c>>();
    if(setAccountIds.size() > 0){
        for(Stryker_Contact__c con : [SELECT Id, Internal_Contact__c, Internal_Contact__r.Business_Unit_Region__c, Customer_Account__c
                           FROM Stryker_Contact__c
                           WHERE  Customer_Account__c IN :setAccountIds //IsActive__c = TRUE
                           AND Internal_Contact__r.Type__c = 'PSR'
                           AND Internal_Contact__r.RecordTypeId = :internalContactRT]){
          if(mapContacts.containsKey(con.Customer_Account__c) == false){
             mapContacts.put(con.Customer_Account__c, new List<Stryker_Contact__c>());
            }
            mapContacts.get(con.Customer_Account__c).add(con);
        }
    }
    System.debug('@@@' + mapContacts);
    Map<Id, Product2> mapProducts;
    // Fetch bisnuss unit from Product2
    if(setProductIds.size() > 0){
        mapProducts = new Map<Id, Product2>([SELECT Id, Name, Flex_Business_Unit__c FROM Product2 WHERE Id IN :setProductIds]);
    }

    // Get Not Available Contact Id;
    Id notAvailableId;// = '003e000000Igzpz'; // ToDo: replace with custom setting.

    notAvailableId = Flex_Settings__c.getInstance().Not_Avaliable_PSR_Contact__c;

    for(OpportunityLineItem oli :lstNew){
        if(mapOpps.containsKey(oli.OpportunityId)){
            List<Stryker_Contact__c> lstContacts = mapContacts.get(oli.Location_Account__c);
            System.debug('@@@' + lstContacts);

            // Business Unit is Business_Unit__c on Opportunity Product.
            String bizUnit = oli.Business_Unit__c;

            // Start
            //Below code will be obsolete once we deploy CPQ.
            // If OLI.BusinessUnit if null then get value from Product
            // Refer Task# T-440828
            if(String.isBlank(bizUnit) == true){
	            if(mapProducts != null && mapProducts.size() > 0){
	              bizUnit = mapProducts.get(oli.Product2Id).Flex_Business_Unit__c;
	            }
            }
            System.debug('@@@' + bizUnit);
            //End


            List<Contact> lstContactsNew = filterContacts(lstContacts, bizUnit);
            System.debug('@@@' + lstContactsNew);
            if(lstContactsNew.size() == 1){
              System.debug('@@@@@');
              oli.PSR_Contact__c = lstContactsNew.get(0).Id;
            }
            else{
                System.debug('@@@@@');
              oli.PSR_Contact__c = notAvailableId;
            }
        }
        if(oli.PSR_Contact__c == null) {
          System.debug('@@@@@');
          oli.PSR_Contact__c = notAvailableId;
        }
    }
  }


  //-----------------------------------------------------------------------------------------------
  //  Helper method to check the Related Business Units Contacts
  //-----------------------------------------------------------------------------------------------
  private static List<Contact> filterContacts(List<Stryker_Contact__c> lstContacts, String bizUnit){

    List<Contact> lstContactsNew = new List<Contact>();
    if(lstContacts != null && lstContacts.size() > 0){
      for(Stryker_Contact__c con :lstContacts){
        System.debug('@@@' + con.Internal_Contact__r.Business_Unit_Region__c);

        if(String.isBlank(con.Internal_Contact__r.Business_Unit_Region__c) == false){
          List<String> lstBusinessUnits = con.Internal_Contact__r.Business_Unit_Region__c.split(';');
            for(String s :lstBusinessUnits){
              System.debug('@@@' + s);
              Integer firstIndex = s.indexOf('-');

              String productWithoutDash;

              if(firstIndex != -1){
                productWithoutDash = s.subString(0, firstIndex).trim();
              }
              else{
              	productWithoutDash = s.trim();
              }

              System.debug('@@@' + productWithoutDash);
              System.debug('@@@' + bizUnit);

              Boolean isMatched = false;

              if(bizUnit != null && productWithoutDash != null){
                isMatched = comparebizUnit(bizUnit, productWithoutDash);
              }
              System.debug('@@@###' + isMatched);

              if(isMatched){
              	lstContactsNew.add(con.Internal_Contact__r);
              	break;
              }
              //if(productWithoutDash == bizUnit){
              //  lstContactsNew.add(con.Internal_Contact__r);
              //}
            }
          }
        }
    }
    return lstContactsNew;
  }


  //-----------------------------------------------------------------------------------------------
  //  Helper method to check product name and Business Unit product name
  //-----------------------------------------------------------------------------------------------
  private static Boolean comparebizUnit(String bizUnit, String businessUnitName){
    if(bizUnit.equalsIgnoreCase('Endo') && businessUnitName.equalsIgnoreCase('Endoscopy')){
    	return true;
    }
    if(bizUnit.equalsIgnoreCase('NAV') && businessUnitName.equalsIgnoreCase('Navigation')){
      return true;
    }
    if(bizUnit.equalsIgnoreCase('PC') && businessUnitName.equalsIgnoreCase('Patient Care')){
      return true;
    }
    if(bizUnit.equalsIgnoreCase('PH') && businessUnitName.equalsIgnoreCase('Patient Handling')){
      return true;
    }
    if(bizUnit.equalsIgnoreCase('Surg') && businessUnitName.equalsIgnoreCase('Surgical')){
      return true;
    }

    if(bizUnit.equalsIgnoreCase(businessUnitName)){
    	return true;
    }
    return false;

  }
  
  //Method to check Opportunity is Flex Oppty
  private static Boolean isFlexOppty (Opportunity oppty) {
    if (setOpptyRecordTypeIds == null || setOpptyRecordTypeIds.size() <= 0) {
      setOpptyRecordTypeIds = new Set<ID>();
      setOpptyRecordTypeIds.add(
        Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Constants.RT_FLEX_OPPTY_CONV).getRecordTypeId());

      setOpptyRecordTypeIds.add(
        Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Constants.RT_FLEX_OPPTY_RENT).getRecordTypeId());
    }
    
    if (setOpptyRecordTypeIds.contains(oppty.RecordTypeId)) {
      return true;
    } 
    
    return false;
  }
}