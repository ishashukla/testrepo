/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  June 02, 2014
 Purpose       :  Test Class for FF_OpportunityLineItemTriggerHandler
*******************************************************************************/
@isTest(seeAllData = true)
private class FF_OpportunityLineItemTriggerHandlerTest {

  //-----------------------------------------------------------------------------------------------
  // Method for test notifySalesRepIfCaseAlreadyExists method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testMethod1(){
    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Type = 'Hospital';
    //acc.Legal_Name__c = 'Test';
    insert acc;

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    con.Business_Unit_Region__c = 'Gulf Coast'; //HRW 2015-11-25
    insert con;

    Product2 prod = new Product2();
    prod.Name = 'test product';
    prod.Part_Number__c = '12345';
    prod.Flex_Business_Unit__c = 'Surg'; //HRW 2015-11-25
    insert prod;

    Pricebook2 priceBook = [SELECT Name FROM Pricebook2 WHERE isStandard = true FOR UPDATE].get(0);

    /*
    Pricebook2 priceBook = new Pricebook2();
    priceBook.Name = 'test value';
    //priceBook.IsStandard = true;
    priceBook.IsActive = true;
    insert priceBook;*/

    PricebookEntry pbe = new PricebookEntry();
    pbe.Pricebook2Id = priceBook.Id;
    pbe.Product2Id = prod.Id;
    pbe.IsActive = true;
    pbe.UnitPrice = 0.0;
    insert pbe;
    
    Id flexRTOnlyConventional = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();

    // without Location Account field
    Opportunity opp = new Opportunity();
    opp.StageName = 'Quoting';
    opp.AccountId = acc.Id;
    opp.CloseDate = Date.newinstance(2014,5,29);
    opp.Name = 'test value';
    opp.RecordTypeId = flexRTOnlyConventional;
    insert opp;
    
    
    

    // With Location Account field
    Opportunity opp2 = new Opportunity();
    opp2.StageName = 'Quoting';
    opp2.AccountId = acc.Id;
    opp2.Location_Account__c = acc.Id;
    opp2.CloseDate = Date.newinstance(2014,5,29);
    opp2.Name = 'test value';
    opp.RecordTypeId = flexRTOnlyConventional;
    insert opp2;

    // Line Item record of first Opportunity
    OpportunityLineItem oppItem = new OpportunityLineItem();
    oppItem.Quantity = 1.00;
    oppItem.OpportunityId = opp.Id;
    oppItem.TotalPrice = 100;
    oppItem.PricebookEntryId = pbe.Id;
    oppItem.Asset_Type__c = 'Equipment';
    oppItem.Business_Unit__c = 'Surg'; //HRW 2015-11-25
    insert oppItem;

    // Line Item record of Second Opportunity
    OpportunityLineItem oppItem2 = new OpportunityLineItem();
    oppItem2.Quantity = 1.00;
    oppItem2.OpportunityId = opp2.Id;
    oppItem2.TotalPrice = 200;
    oppItem2.PricebookEntryId = pbe.Id;
    oppItem2.Asset_Type__c = 'Service';
    insert oppItem2;

    Test.stopTest();


    // Verifing Results of first Opportunity: Location Account should be parent's Account
    List<OpportunityLineItem> lstItems = [SELECT Id, OpportunityId, Location_Account__c FROM OpportunityLineItem
                                            WHERE OpportunityId = :opp.Id];
    //System.assertEquals(lstItems.get(0).Location_Account__c, opp.AccountId);



    // Verifing Results of second Opportunity: Location Account should be parent's Location Account
    List<OpportunityLineItem> lstItems2 = [SELECT Id, OpportunityId,Location_Account__c FROM OpportunityLineItem
                                            WHERE OpportunityId = :opp2.Id];
    //System.assertEquals(lstItems2.get(0).Location_Account__c, opp2.Location_Account__c);

  }


  //-----------------------------------------------------------------------------------------------
  // Method for test populatePSRContact method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testMethod2(){
    Test.startTest();
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    acc.Type = 'Hospital';
    //acc.Legal_Name__c = 'Test';
    insert acc;

    Id recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Flex Internal Contact'].get(0).Id;
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    con.RecordTypeId = recordTypeId;
    con.Type__c = 'PSR';
    con.Business_Unit_Region__c = 'test product - test';
    insert con;

    Stryker_Contact__c conJunction = new Stryker_Contact__c();
    conJunction.Internal_Contact__c = con.Id;
    conJunction.Customer_Account__c = acc.Id;
    insert conJunction;

    Product2 prod = new Product2();
    prod.Name = 'test product';
    prod.Part_Number__c = '12345';
    prod.Flex_Business_Unit__c = 'Surg'; //HRW 2015-11-25
    insert prod;

    Pricebook2 priceBook = [SELECT Name FROM Pricebook2 WHERE isStandard = true FOR UPDATE].get(0);

    /*
    Pricebook2 priceBook = new Pricebook2();
    priceBook.Name = 'test value';
    //priceBook.IsStandard = true;
    priceBook.IsActive = true;
    insert priceBook;*/

    PricebookEntry pbe = new PricebookEntry();
    pbe.Pricebook2Id = priceBook.Id;
    pbe.Product2Id = prod.Id;
    pbe.IsActive = true;
    pbe.UnitPrice = 0.0;
    insert pbe;
    
     Id flexRTOnlyConventional = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();
    

    // without Location Account field
    Opportunity opp = new Opportunity();
    opp.StageName = 'Quoting';
    opp.AccountId = acc.Id;
    opp.CloseDate = Date.newinstance(2014,5,29);
    opp.Name = 'test value';
    opp.RecordTypeId = flexRTOnlyConventional;
    insert opp;

    // Line Item record of first Opportunity
    OpportunityLineItem oppItem = new OpportunityLineItem();
    oppItem.Quantity = 1.00;
    oppItem.OpportunityId = opp.Id;
    oppItem.TotalPrice = 100;
    oppItem.PricebookEntryId = pbe.Id;
    oppItem.Asset_Type__c = 'Equipment';
    insert oppItem;


    Test.stopTest();


    // Verifing PSR_Contact__c is populating successfully.
    List<OpportunityLineItem> lstItems = [SELECT Id, OpportunityId, PSR_Contact__c, Location_Account__c FROM OpportunityLineItem
                                            WHERE OpportunityId = :opp.Id];
    //System.assertEquals(lstItems.get(0).PSR_Contact__c, con.Id);




  }
}