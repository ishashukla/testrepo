/**================================================================      
* Appirio, Inc
* Name: COMM_CreateReplenishmentPOBatchTest
* Description: Test class for COMM_CreateReplenishmentPOBatch
* Created Date: 11 Nov 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest(SeeAllData=true)
private class COMM_CreateReplenishmentPOBatchTest {
    static List<SVMXC__Product_Stock__c> productStockList;
    @isTest static void testBatch() {
        createData();
        Test.startTest();
        Database.executeBatch(new COMM_CreateReplenishmentPOBatch());
        Test.stopTest();
        System.assertEquals([SELECT Is_Replenished__c FROM SVMXC__Product_Stock__c WHERE Id = :productStockList[0].Id].Is_Replenished__c,true);
    }
    private static void createData() {
        Id rtAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
        Account  acc;
        List<Account> accList = new List<Account>([SELECT Id FROM Account WHERE Name = 'Division VAN' and recordType.name = 'Endo COMM Customer']);
        if(accList.size() == 0) {
            List<User> users = TestUtils.createUser(1, 'DataMigrationIntegration', false );
            users[0].FirstName = 'Integration';
            insert users;

            System.runAs(users[0]){

                acc = TestUtils.createAccount(1, false).get(0);
                acc.Name = 'Division VAN';
                acc.RecordTypeId = rtAccount;
                acc.Endo_Active__c = true;
                insert acc; 
            
            }
        } else {
            acc = accList.get(0);
        }
        User userRecord = TestUtils.createUser(1, 'Field Service Technician - COMM US', true).get(0);
        Product2 productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        SVMXC__Site__c slocation = new SVMXC__Site__c(Location_ID__c = '12345',
                                                      SVMXC__Stocking_Location__c = true,
                                                      SVMXC__Service_Engineer__c = userRecord.Id);
        insert slocation;
        productStockList = new List<SVMXC__Product_Stock__c>();
        for(integer i = 0 ;i < 4; i++) {
            
            SVMXC__Product_Stock__c productStock  = new SVMXC__Product_Stock__c(SVMXC__Quantity2__c = 2,
                                                                                Maximum_Qty__c = 4,
                                                                                Minimum_Qty__c  = 3,
                                                                                SVMXC__Reorder_Level2__c = 3,
                                                                                SVMXC__Location__c = slocation.Id,
                                                                                SVMXC__Product__c = productRecord.Id,
                                                                                Is_Replenished__c = false
                                                                               );
            productStockList.add(productStock);
        }
        insert productStockList;
        Address__c address = new Address__c(Location_ID__c = '1234',Business_Unit__c = '88',Type__c = 'Billing',
                                            Primary_Address_Flag__c = true,Account__c = acc.Id); 
        insert address;
        SVMXC__Site__c dlocation = new SVMXC__Site__c(Location_ID__c = '1234',
                                                      SVMXC__Stocking_Location__c = true);
        insert dlocation;
        SVMXC__Service_Group__c sgroup = new SVMXC__Service_Group__c();
        insert sgroup;
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(SVMXC__Salesforce_User__c = userRecord.Id,
                                                                                         SVMXC__Service_Group__c = sgroup.Id);
        insert technician;        
    }
}