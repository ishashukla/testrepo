public with sharing class ObjectHierarchy_TestUtility {	

	public static List<Account> getSampleHierarchyAccounts(Integer childRecordCount){
		Integer accountNumber = 0;
		List<Account> allAccounts = new List<Account>();
		List<Account> currentAccounts = new List<Account>();
		//Level 1
		currentAccounts.add(new Account(Name='RootAccount', AccountNumber=''+ accountNumber++, NumberOfEmployees=childRecordCount));
		insert currentAccounts;
		allAccounts.addAll(currentAccounts);

		//Level 2
		currentAccounts.clear();
		for(Integer i=0; i<childRecordCount; i++){
			currentAccounts.add(new Account(Name='RootAccount', AccountNumber=''+ accountNumber++,
								 				NumberOfEmployees=childRecordCount,
								 				ParentId=allAccounts[0].Id));
		}
		insert currentAccounts;
		allAccounts.addAll(currentAccounts);
		
		//Level 3
		currentAccounts.clear();
		for(Integer i=0; i<childRecordCount; i++){
			for(Integer j=0; j<childRecordCount; j++){
				currentAccounts.add(new Account(Name='ChildAccount'+ accountNumber, AccountNumber=''+accountNumber++,
									 				NumberOfEmployees=childRecordCount,
									 				ParentId=allAccounts[i+1].Id));
			}
		}
		insert currentAccounts;
		allAccounts.addAll(currentAccounts);
		return allAccounts;
	}
}