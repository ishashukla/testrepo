/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : Test class for OrderProductsTriggerHandler
//
// Oct 08, 2015    Megha Agarwal (Appirio)
//***************************************************************************/
@isTest
private class OrderProductsTriggerHandlerTest {
  static testMethod void testOrderProductsTriggerHandler(){
    Profile pr = [select id from profile where name = 'System Administrator'];
    User usr = new User(alias = '1305200', email='test13052010@appirio.com',
               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
               localesidkey='en_US', profileid = pr.Id, country='United States',IsActive =true,
               timezonesidkey='America/Los_Angeles', username='test13052011@appirio.com');
    Test.startTest();
	    system.runAs(usr){
		    //EmailTemplate e = new EmailTemplate (developerName = 'Scheduled_Order_Notification', TemplateType= 'Visualforce', Name = 'n');
		    //insert e;
		    EmailTemplate validEmailTemplate = new EmailTemplate();
		    validEmailTemplate.isActive = true;
		    validEmailTemplate.Name = 'Scheduled Order Notification';
		    validEmailTemplate.DeveloperName = 'Scheduled_Order_Notification';
	      validEmailTemplate.TemplateType = 'Text';
		    validEmailTemplate.FolderId = UserInfo.getUserId();
		    insert validEmailTemplate;
		    Account acc = Medical_TestUtils.createAccount(10, true).get(0);
		    Contact con = Medical_TestUtils.createContact(10, acc.Id, true).get(0);
		    system.debug('email'+con.Email);
		    
		    Product2 prod = Medical_TestUtils.createProduct(true);
		    Pricebook2 pb = Medical_TestUtils.createPricebook(1,'Test PriceBook', true).get(0);
		    PricebookEntry pbe1 = Medical_TestUtils.createPricebookEntry(prod.Id,pb.Id,false);
		    PricebookEntry pbe2 = Medical_TestUtils.createPricebookEntry(prod.Id,Test.getStandardPricebookId(), false);
		    insert (new List<PricebookEntry>{pbe2,pbe1});
		    Medical_Common_Settings__c setting = Medical_TestUtils.createMedicalCommonSetting(true);
		    Order ord = Medical_TestUtils.createOrder(acc.Id,con.Id, pb.Id,false);
		    ord.Is_Scheduled_Order_Mail_Sent__c = false;
		    insert ord;
		    OrderItem oi1 = Medical_TestUtils.createOrderItem(ord.Id,pbe1.Id ,false);
		    oi1.Line_Number__c = 1;
		    OrderItem oi2 = Medical_TestUtils.createOrderItem(ord.Id,pbe1.Id ,false);
		    oi2.Line_Number__c = 1.1;
		    insert (new List<OrderItem>{oi1,oi2});
		    
		    oi2.Order_Line_Status__c = '531';
		    oi1.Order_Line_Status__c = '530';
		    
		    update (new List<OrderItem>{oi1,oi2});
      }
    Test.stopTest();
  }
}