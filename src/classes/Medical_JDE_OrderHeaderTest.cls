/*************************************************************************************************
Created By:    Rahul Aeran
Date:          April 23, 2016
Description:   Test class for Medical_JDE_OrderHeader for Medical Division
**************************************************************************************************/
@isTest
private class Medical_JDE_OrderHeaderTest {
  static testMethod void testMedical_JDE_OrderHeader(){
    Test.startTest();
      Test.setMock(WebServiceMock.class, new WebServiceMockClass());
      Medical_JDE_OrderHeader objMedicalOrderHeader = new Medical_JDE_OrderHeader();
      Medical_JDE_OrderHeader.OrderHeader_ICPort objOrderHeaderICPort = new Medical_JDE_OrderHeader.OrderHeader_ICPort();
      Medical_JDE_OrderHeaderSchema.OrderHeader objOrderHeader = objOrderHeaderICPort.CreateOrderHeader('123','123','456','Jaipiur','999','Shipping','test','Test1','Test2','1234567890');
      System.assert(objOrderHeader != null , 'Received the response');
    Test.stopTest();
  }
}