public class BigMachinesNewQuoteController {
    
    private class QuoteCreationException extends Exception {}
    
    public Boolean bmUsesSSL;
 
    // for Visualforce pages
    private String bmCreateURL = '';
    private String bmSubtitle = '';
    
    // object IDs
    private ID bmOpportunityId;
    private ID bmAccountId;
    private String bmTransaction;
    private String bmCurrency;

    public BigMachinesNewQuoteController() {
        bmUsesSSL = BigMachinesParameters.usesSSL;
        Opportunity opp = new Opportunity();
        Account act = new Account();
        try {
            bmOpportunityId = ApexPages.currentPage().getParameters().get('oppId');
            if (bmOpportunityId != null) {
                opp = [select AccountId, CurrencyIsoCode from Opportunity where Id = :bmOpportunityId];
            }
        } catch (Exception e) {
            System.debug('BigMachinesNewQuoteController: Parameter \'oppId\' was specified '
                    + 'but it was either invalid or did not belong to an existing opportunity.');
            throw new QuoteCreationException('Could not find specified opportunity '
                    + 'to which quote should be associated.');
        }
        try {
            bmAccountId = ApexPages.currentPage().getParameters().get('actId');
        } catch (Exception e) {
            System.debug('BigMachinesNewQuoteController: Parameter \'actId\' was specified '
                    + 'but it was invalid.');
            throw new QuoteCreationException('Could not find specified account '
                    + 'to which quote should be associated.');
        }
        if (bmAccountId == null) {
            if (opp.AccountId == null) {
                throw new QuoteCreationException('Could not find an account with which to '
                        + 'associate the quote.');
            } else {
                bmAccountId = opp.AccountId;
            }
        }
        if (opp.CurrencyIsoCode == null) {
            throw new QuoteCreationException('Could not find the currency for this Opportunity.');
        }
        else {
            bmCurrency = opp.CurrencyIsoCode;
        }
        try {
            act = [select Id from Account where Id = :bmAccountId];
        } catch (Exception e) {
            System.debug('BigMachinesNewQuoteController: Account Id did not belong to an '
                    + 'existing account.');
            throw new QuoteCreationException('Could not find account to which quote '
                    + 'should be associated.');
        }
        ID quoteId = null;
        try {
            quoteId = ApexPages.currentPage().getParameters().get('cloneId');
        } catch (Exception e) {
            System.debug('BigMachinesNewQuoteController: Parameter \'cloneId\' was specified '
                    + 'but it was invalid.');
            throw new QuoteCreationException('Could not find specified quote to clone.');
        }
        if (quoteId != null) {
            BigMachines__Quote__c cloneQuote = new BigMachines__Quote__c();
            try {
                cloneQuote = [select Name, BigMachines__Transaction_Id__c 
                              from BigMachines__Quote__c where Id = :quoteId];
            } catch (Exception e) {
                System.debug('BigMachinesNewQuoteController: Parameter \'cloneId\' was specified '
                        + 'but it did not belong to an existing quote.');
                throw new QuoteCreationException('Could not find specified quote to clone.');
            }
            bmSubtitle = 'Clone of ' + cloneQuote.Name;
            bmTransaction = cloneQuote.BigMachines__Transaction_Id__c;
        } else {
            bmSubtitle = 'New ' + Schema.SObjectType.BigMachines__Quote__c.Label;
        }   
        buildURL();
    }
    
    public void buildURL() {
        bmCreateURL = 'http' + ((bmUsesSSL) ? 's' : '') + '://'
                    + BigMachinesParameters.bm_site + '.bigmachines.com/commerce/buyside/';
        if (bmTransaction != null) {
            bmCreateURL += 'copy_processing.jsp?bs_id=' + bmTransaction 
                         + '&action_id=' + BigMachinesParameters.action_id_copy
                         + '&bm_cm_process_id=' + BigMachinesParameters.process_id
                         + '&version_id=' + BigMachinesParameters.version_id;
        } else {
            bmCreateURL += 'document.jsp?formaction=create'
                         + '&process=' + BigMachinesParameters.process_var;
        }
        bmCreateURL += '&bm_url_capture_cookie=' + BigMachinesParameters.bm_site
                     + '&_partnerAccountId=' + bmAccountId;
        if (bmOpportunityId != null) {
            bmCreateURL += '&_partnerOpportunityId=' + bmOpportunityId;
        }
        if (bmCurrency != null) {
            bmCreateURL += '&_bm_cm_new_transaction_currency=' + bmCurrency;
            bmCreateURL += '&_bm_session_currency=' + bmCurrency;
        }
    }
    
    public String getCreateURL() {
        return bmCreateURL;
    }
    
    public String getSubtitle() {
        return bmSubtitle;
    }
}