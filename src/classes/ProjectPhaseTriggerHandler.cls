/**================================================================      
* Appirio, Inc
* Name: ProjectPhaseTriggerHandler 
* Description: Rolling up fields which was converted as part of MD to Lookup (T-501829)
* Created Date: 02nd Jun 2016
* Created By: Rahul Aeran (Appirio)
*
* Date Modified      Modified By            Description of the update
* 20th July 2016     Deepti Maheshwari      Update phase sharing for project team (Ref : I-226753)
* 21th July 2016     Pankaj Kumar (Appirio) set sales rep email field on project phase from Project Plan's Sales Rep's Email field(Ref : T-520411)
* 16th Aug 2016      Deepti Maheshwari      Remove Phase sharing as Relationship changed back to MD(I-229890)
* 29th Aug 2016      Nitish Bansal          I-232327 (Exception handling)
* 16th Sept 2016     Nitish Bansal          Update (I-235018) // Added query execution time calculation logic
* 3rd Nov 2016       Nitish Bansal          I-241409 - Proper list checks before SOQL and DML
==================================================================*/

public class ProjectPhaseTriggerHandler {
    Static Datetime qryStart, qryEnd;
    public static void onAfterInsert(List<Project_Phase__c> lstNew){
        //provideAccessToPE(lstNew); //NB - 07/20 - I-224217
        //provideAccessToProjectTeam(lstNew);
        calculateRollUpSummariesonProject(lstNew);
    } 
    public static void onAfterDelete(List<Project_Phase__c> lstOld){
        calculateRollUpSummariesonProject(lstOld);
    }
    public static void onAfterUpdate(List<Project_Phase__c> lstNew,map<id,Project_Phase__c> mapOld){
        calculateRollUpSummariesonProject(lstNew);
        updatePlanOnAllPhaseClose(lstNew,mapOld);
    }
    public static void onBeforeUpdate(List<Project_Phase__c> lstNew,map<id,Project_Phase__c> mapOld){
        setSalesRepEmail_Start(lstNew,mapOld);
    }
    public static void onBeforeInsert(List<Project_Phase__c> lstNew){
        setSalesRepEmail_Start(lstNew,null);
    }
  
    
    private static void updatePlanOnAllPhaseClose(List<Project_Phase__c> newList,map<id,Project_Phase__c> oldMap) {
       Set<Id> projectids = new Set<Id>();
      for(Project_phase__c phase : newList) {
        if(phase.Milestone__c == 'Closed' && phase.Milestone__c !=  oldMap.get(phase.Id).Milestone__c) {
          System.debug('Inside First If');
          projectids.add(phase.Project_Plan__c);
        }
      }
      boolean allPhase = true;
      Set<id> planids  = new Set<Id>();
      qryStart = datetime.now();
      if(projectids.size() > 0){
        for(Project_phase__c phase : [Select id,Milestone__c,Project_Plan__c
                                 FROM Project_phase__c
                                 Where Project_Plan__c in : projectids]) {
          if(phase.Milestone__c != 'Closed') {
            allphase = false;
          }
          planids.add(phase.Project_Plan__c);
        }
      }
      qryEnd = datetime.now();
      //debug number of seconds the query ran
      system.debug('phase loop execution time ' + (qryEnd.getTime() - qryStart.getTime()) /1000);
      if(allphase && planids.size() > 0) {
        System.debug('inside final If');
        List<Project_Plan__c> projectList = new List<project_plan__c> ();
        qryStart = datetime.now();
        for(Project_Plan__c plan : [Select Stage__c,Id
                                      from project_plan__c
                                      where id in: planids]) {
          plan.stage__c = 'Closed';   
          projectList.add(plan);
        }
        qryEnd = datetime.now();
        //debug number of seconds the query ran
        system.debug('phase loop execution time ' + (qryEnd.getTime() - qryStart.getTime()) /1000);
        if(projectList.size() > 0)
          update projectList;
      }
      
    }
    //================================================================      
    // Name: setSalesRepEmail_Start setSalesRepEmail
    // Description: set sales rep email field on project phase from Project Plan's Sales Rep's Email field
    // Created Date: [21/7/2016] Ref : T-520411
    // Created By: Pankaj Kumar (Appirio)
    //==================================================================
    private static void setSalesRepEmail_Start(List<Project_Phase__c> newList,map<id,Project_Phase__c> mapOld){
      
        Set<Id> projectPlanIds = new Set<Id>();
        for(Project_Phase__c aProjectPahse : newList){
            if(aProjectPahse.Project_Plan__c != null && (mapOld == null || (mapOld.get(aProjectPahse.Id).Project_Plan__c != aProjectPahse.Project_Plan__c))){
                projectPlanIds.add(aProjectPahse.Project_Plan__c); 
            }
        }
        if(projectPlanIds.size() > 0){
            setSalesRepEmail(projectPlanIds,newList);       
        }
    }
    
    private static void setSalesRepEmail(Set<Id> projectPlanIds, List<Project_Phase__c> newList){
      
        Map<Id,String> projectPlanIdSalesRepEmailMap = new map<Id,String>();
        Map<Id,Project_Plan__c> planIdMap = new Map<Id,Project_Plan__c>();
        qryStart = datetime.now();
        for(Project_Plan__c aProjectPlan : [select Id,Sales_Rep__r.Email,Account__c,Project_PO_Numbers__c from Project_Plan__c where Id in :projectPlanIds]){
            projectPlanIdSalesRepEmailMap.put(aProjectPlan.Id,aProjectPlan.Sales_Rep__r.Email);
            planIdMap.put(aProjectPlan.Id,aProjectPlan);
        }
        qryEnd = datetime.now();
        //debug number of seconds the query ran
        system.debug('BP loop execution time ' + (qryEnd.getTime() - qryStart.getTime()) /1000);
        for(Project_Phase__c aProjectPahse : newList){
            if(projectPlanIdSalesRepEmailMap.get(aProjectPahse.Project_Plan__c) != null){
                aProjectPahse.Sales_Rep_Email__c = projectPlanIdSalesRepEmailMap.get(aProjectPahse.Project_Plan__c); 
            }
            if(planIdMap.containsKey(aProjectPahse.Project_Plan__c)){  
                aProjectPahse.Account__c = planIdMap.get(aProjectPahse.Project_Plan__c).Account__c;
                aProjectPahse.Project_Plan_PO_Numbers__c = planIdMap.get(aProjectPahse.Project_Plan__c).Project_PO_Numbers__c;
            }
        }
    }
    
    
    //================================================================      
    // Name: provideAccessToProjectTeam
    // Description: Used to provide access to project teams on phase when phase 
    //              is created after some team members have been added to project team
    // Created Date: [20/7/2017] Ref : I-226753
    // Created By: Deepti Maheshwari (Appirio)
    //==================================================================
    /*private static void provideAccessToProjectTeam(List<Project_Phase__c> newList){ 
        Map<Id, List<Project_Phase__c>> projectIdMap = new Map<Id, List<Project_Phase__c>>();
        List<Project_Phase__Share> phaseSharesList = new List<Project_Phase__Share>();
        for(Project_Phase__c phase : newList){
            if(!projectIdMap.containsKey(phase.Project_Plan__c)){
                projectIdMap.put(phase.Project_Plan__c, new List<Project_Phase__c>{phase});
            }
            else{
                projectIdMap.get(phase.Project_Plan__c).add(phase);
            }
        }
        String PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
        List<Custom_Account_Team__c> projectTeams = [SELECT ID, Project__r.id, User__c,Project_Access_Level__c 
                                                     FROM Custom_Account_Team__c
                                                     WHERE Project__r.id in :projectIdMap.keyset()
                                                     AND RecordTypeID = :PROJECT_TEAM_RECORDTYPE_ID];
        for(Custom_Account_Team__c teamMember : projectTeams){
            if(projectIdMap.containsKey(teamMember.Project__r.id)){
                for(Project_Phase__c phase : projectIdMap.get(teamMember.Project__r.id)){
                    phaseSharesList.add(new Project_Phase__Share(UserOrGroupId = teamMember.User__c, 
                                                                 ParentId  = phase.Id ,
                                                                 AccessLevel  = teamMember.Project_Access_Level__c,
                                                                 RowCause = Schema.Project_Phase__Share.RowCause.Project_Team_Member_Phase_Access__c));
                }
            }
        }
        if(phaseSharesList.size() > 0){
            Database.insert(phaseSharesList, false);
        }
    }*/
    
    //NB - 07/20 - I-224217 - Start
    // Method to provide edit access to PE on phases
    // @param lstPhases - List of inserted phase records 
    // @return Nothing 
    /*private static void provideAccessToPE(List<Project_Phase__c> lstPhases){
        Map<Id, List<Project_Phase__c>> mapPPtophases = new Map<Id, List<Project_Phase__c>>();
        List<Project_Phase__c> tempPhaseList = new List<Project_Phase__c>();
        List<Project_Phase__Share> lstSharingRecordsToInsert = new List<Project_Phase__Share>();
        
        for(Project_Phase__c phase : lstPhases){
            tempPhaseList = new List<Project_Phase__c>();
            if(mapPPtophases.containsKey(phase.Project_Plan__c)){
                tempPhaseList.addAll(mapPPtophases.get(phase.Project_Plan__c));
                tempPhaseList.add(phase);
                mapPPtophases.put(phase.Project_Plan__c, tempPhaseList);
            } 
            else {
                tempPhaseList.add(phase);
                mapPPtophases.put(phase.Project_Plan__c, tempPhaseList);
            }
        }
        
        String PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();
        
        for(Custom_Account_Team__c projTeam : [Select User__c, Project__c from Custom_Account_Team__c
                                               Where RecordTypeID = :PROJECT_TEAM_RECORDTYPE_ID 
                                               and Team_Member_Role__c = : Constants.PROJECT_ENGINEER
                                               and Project__c In : mapPPtophases.keyset()]){
            for(Project_Phase__c pPhase : mapPPtophases.get(projTeam.Project__c)){
                lstSharingRecordsToInsert.add(new Project_Phase__Share(AccessLevel = Constants.PERM_EDIT, 
                                                                       ParentId = pPhase.Id, UserOrGroupId = projTeam.User__c));
            }
        }
        
        //Providing access on project phase
        if(lstSharingRecordsToInsert.size() > 0){
            try {
                Database.SaveResult[] results = Database.insert(lstSharingRecordsToInsert,false);
                if(results != null){
                    Integer index = 0;
                    for(Database.SaveResult result : results) {
                        if(!result.isSuccess()) {
                            system.debug('RECORD : ' + lstSharingRecordsToInsert.get(index) + 'ERROR::' + result.getErrors());
                        }
                        index++;
                    }
                }
            } 
            catch (Exception e) {
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }                  
        }                                            
        
    }*/
    //NB - 07/20 - I-224217 - End
    
    // RA - 06/02 - T-501829
    // Methods to calculate roll up summary fields on project plan which were converted as part of MD --> Lookup conversion
    // @param lstPhases - List of inserted/deleted/updated phase records 
    // @return Nothing  
    private static void calculateRollUpSummariesonProject(List<Project_Phase__c> lstPhases){   
        List<Project_Plan__c> listOfSobj = new List<Project_Plan__c>();
        List<COMM_RollUpSummaryUtility.fieldDefinition> fieldDefinitionCompletedPhases = new List<COMM_RollUpSummaryUtility.fieldDefinition> {
            new COMM_RollUpSummaryUtility.fieldDefinition (Constants.ROLL_UP_COUNT, Constants.ROLL_UP_ID,Constants.COMPLETED_PHASES, null)
        };
        qryStart = datetime.now();
        listOfSobj.addAll((List<Project_Plan__c>)COMM_RollUpSummaryUtility.rollUpTrigger(fieldDefinitionCompletedPhases, 
                                                                                         lstPhases, 
                                                                                         Constants.SOBJECT_PHASE, 
                                                                                         Constants.SOBJECT_PROJECT_PLAN, 
                                                                                         Constants.SOBJECT_PROJECT_PLAN, 
                                                                                         ' AND ( ' + Constants.PHASE_MILESTONE_FIELD + ' = \'' +  'Installation Complete' + '\'' + ' OR '+ Constants.PHASE_MILESTONE_FIELD + ' = \'' +  Constants.PHASE_ROLL_UP_SIGN_OFF_FILTER + '\'' + ' )', 
                                                                                         false)); //NB - 11/15 - I-243837 - Added OR clause
        
        qryEnd = datetime.now();
        //debug number of seconds the query ran
        system.debug('fieldDefinitionCompletedPhases roll up execution time ' + (qryEnd.getTime() - qryStart.getTime()) /1000);
        List<COMM_RollUpSummaryUtility.fieldDefinition> fieldDefinitionSignedPhases = new List<COMM_RollUpSummaryUtility.fieldDefinition> {
            new COMM_RollUpSummaryUtility.fieldDefinition (Constants.ROLL_UP_COUNT, Constants.ROLL_UP_ID,Constants.SIGNED_PHASES, null)      
        };
        qryStart = datetime.now();
        listOfSobj.addAll((List<Project_Plan__c>)COMM_RollUpSummaryUtility.rollUpTrigger(fieldDefinitionSignedPhases, 
                                                                                         lstPhases, 
                                                                                         Constants.SOBJECT_PHASE , 
                                                                                         Constants.SOBJECT_PROJECT_PLAN, 
                                                                                         Constants.SOBJECT_PROJECT_PLAN, 
                                                                                         ' AND ' + Constants.PHASE_MILESTONE_FIELD + ' = \'' +  Constants.PHASE_ROLL_UP_SIGN_OFF_FILTER + '\'', 
                                                                                         false));  
        qryEnd = datetime.now();
        //debug number of seconds the query ran
        system.debug('fieldDefinitionSignedPhases roll up execution time ' + (qryEnd.getTime() - qryStart.getTime()) /1000);
        List<COMM_RollUpSummaryUtility.fieldDefinition> fieldTotalPhases = new List<COMM_RollUpSummaryUtility.fieldDefinition> {
            new COMM_RollUpSummaryUtility.fieldDefinition (Constants.ROLL_UP_COUNT, Constants.ROLL_UP_ID,Constants.TOTAL_PHASES, null)      
        };
        qryStart = datetime.now();
        listOfSobj.addAll((List<Project_Plan__c>)COMM_RollUpSummaryUtility.rollUpTrigger(fieldTotalPhases, 
                                                                                         lstPhases, 
                                                                                         Constants.SOBJECT_PHASE , 
                                                                                         Constants.SOBJECT_PROJECT_PLAN, 
                                                                                         Constants.SOBJECT_PROJECT_PLAN, 
                                                                                         '' , 
                                                                                         false)); 
        qryEnd = datetime.now();

        
         
        //debug number of seconds the query ran
        system.debug('fieldTotalPhases roll up execution time ' + (qryEnd.getTime() - qryStart.getTime()) /1000);
        map<Id,Project_Plan__c> mapProjectsToUpdate = new map<Id,Project_Plan__c>();
        Map<String, Object> m;
        
        for(Project_Plan__c pp : listOfSobj){
            m = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(pp));
            if(!mapProjectsToUpdate.containsKey(pp.Id)){
                mapProjectsToUpdate.put(pp.Id,pp); 
            }
            else{
                //Validate which field was queried using map
                if (pp.get(String.valueOf(Constants.TOTAL_PHASES)) != null) {
                    mapProjectsToUpdate.get(pp.Id).put(Constants.TOTAL_PHASES, pp.get(Constants.TOTAL_PHASES));
                }
                if (pp.get(String.valueOf(Constants.SIGNED_PHASES)) != null) {
                    mapProjectsToUpdate.get(pp.Id).put(Constants.SIGNED_PHASES, pp.get(Constants.SIGNED_PHASES));
                }
            }
        }
        //system.assert(false, mapProjectsToUpdate.values());
        if(!mapProjectsToUpdate.keySet().isEmpty()){
        //  NB - 08/29 - I-232327 - Start
            try{

                update mapProjectsToUpdate.values();
            } catch(DmlException e){
                for(Project_Phase__c phase : lstPhases){
                    for ( Integer i = 0; i < e.getNumDml(); i++ ){
                       phase.addError(e.getDmlMessage( i ));
                    }
                }
            }
        //  NB - 08/29 - I-232327 - End
        }        
    }
}