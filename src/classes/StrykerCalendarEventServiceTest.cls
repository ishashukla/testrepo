/**================================================================      
* Appirio, Inc
* Name: StrykerCalendarEventServiceTest
* Description: Test Class for apex class StrykerCalendarEventService
* Created Date: 15-Aug-2016
* Created By: Raghu Rankawat (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class StrykerCalendarEventServiceTest {
    @isTest
    private static void strykerCalendarEventServiceTestMethod(){
		
        
       
        Account acc = TestUtils.createAccount(1, false).get(0);
        acc.ShippingState ='Alaska';
        insert acc;
        Contact con = TestUtils.createContact(2, acc.id, false).get(0);
        insert con;
        string contacts = StrykerCalendarEventService.getMatchingContacts('TestFName');
        //Create Event
        Id eventId1 = StrykerCalendarEventService.createCalendarEvent('Test Subject', con.id, '11/15/2016', '08:00 AM', '11/25/2016', '11:00 PM', false);
        Id eventId2 = StrykerCalendarEventService.createCalendarEvent('', con.id, '08/20/2016', '08:00 AM', '08/30/2016', '08:00 PM', false);
        
        
        //Update Event 
        eventId1 = StrykerCalendarEventService.updateCalendarEventDateTime(eventId1, '2016-08-15 08:00:00', '2016-08-20 20:00:00', false);
        
        
        //Monthly Calendar Events
        String events = StrykerCalendarEventService.getMonthlyCalendarEvents(8, 2016);
        
        //Monthly Work Order
        Id recTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        
        Id changeRequestCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.COMM_CASE_RTYPE).getRecordTypeId();
        TestUtils.createRTMapCS(changeRequestCaseRT, Constants.COMM_CASE_RTYPE, 'COMM');
        Case caseObj = TestUtils.createCase(1, acc.Id, false).get(0);
        caseObj.RecordTypeId = changeRequestCaseRT;
        insert caseObj;

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile sp = TestUtils.fetchProfile('Sales Rep - COMM US');
        Profile pm = TestUtils.fetchProfile('Service Project Manager - COMM US');
        Profile fst = TestUtils.fetchProfile('Field Service Technician - COMM US');
        User u = new User(Alias = 'standt', Email='standarduser@testappirio.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testappirio.com');
                          
         User spu = new User(Alias = 'salesTes', Email='salesTest@testappirio.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = sp.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='salesTest@testappirio.com');
                          
        User pmu = new User(Alias = 'pmTest', Email='pmTest@testappirio.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = pm.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='pmTest@testappirio.com');
        
        User fstu = new User(Alias = 'fstTest', Email='fstTest@testappirio.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = fst.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='fstTest@testappirio.com');
        Test.startTest();
        SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(Name='Test Service Team');
        insert team;
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(Name='Test Technician',SVMXC__Email__c='trst@example.com',
        SVMXC__Salesforce_User__c=u.Id,SVMXC__Service_Group__c=team.Id);
        insert technician;
        SVMXC__SVMX_Event__c sEvent = new SVMXC__SVMX_Event__c(SVMXC__Technician__c = technician.Id,Name='Test Evetns',SVMXC__StartDateTime__c=System.Today(),
        SVMXC__EndDateTime__c = System.Today()+1);
        insert sEvent;
        System.runAs(u) {            
            SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c();
            workOrder.RecordTypeId = recTypeId;
            workOrder.SVMXC__Case__c = caseObj.Id;
            workOrder.SVMXC__Company__c = acc.Id;
            workOrder.SVMXC__Scheduled_Date_Time__c = System.Today();
            insert workOrder;
            Event e = new Event(Id = eventId1);
            e.OwnerId = u.Id;
            e.StartDateTime = System.Today();
            update e;
            String monthlyWorkOrders = StrykerCalendarEventService.getMonthlyWorkOrders(11, 2016, '', '', '');
            string sMaxevents = StrykerCalendarEventService.getServiceMaxEvents(11,2016);
        }
         
        System.runAs(spu) {            
            String monthlyWorkOrders = StrykerCalendarEventService.getMonthlyWorkOrders(11, 2016, acc.Id, spu.Id, 'Alaska');
            string sMaxevents = StrykerCalendarEventService.getServiceMaxEvents(8,2016);
        }
        System.runAs(pmu) {            
            String monthlyWorkOrders = StrykerCalendarEventService.getMonthlyWorkOrders(8, 2016, acc.Id, pmu.Id, 'Alaska');
            string sMaxevents = StrykerCalendarEventService.getServiceMaxEvents(11,2016);
        }
        System.runAs(fstu) {            
            String monthlyWorkOrders = StrykerCalendarEventService.getMonthlyWorkOrders(11, 2016, acc.Id, fstu.Id, 'Alaska');
            string sMaxevents = StrykerCalendarEventService.getServiceMaxEvents(11,2016);
        }
        Test.stopTest();
        
    }
}