/**================================================================      
/**================================================================      
* Appirio, Inc
* Name: SyncStandardAccountTeamMember 
* Description: Batch Class to sync standard accountteammembers with custom accountteam
* Created Date: [DD-mmm-YYYY] 
* Created By: [FirstName LastName] (Appirio)
*
* March 02 2016, Prakarsh Jain  Original (T-479750)
* March 07 2016, Prakarsh Jain  Creation of Map to remove inner loop and use that map
* March 11 2016, Deepti Maheshwari Updation of contacts shares (Ref : T-477658)
* March 14 2016, Deepti Maheshwari Updated query to include null check for user (Ref : T-484559)
* March 15 2016, Deepti Maheshwari Updated to sync custom ATM with Stryker contact object(Ref :T-484874)
* March 18 2016, Meghna Vijay Updated for Opportunity Share Records T-484598 , S-371029
* March 23 2016, Meghna Vijay Updated to insert more COMM  Opportunity Record Types T-484346, S-375210
* May 14 2016, Ralf Hoffmann updated to replace Styker_Contact__c references with new native AccountContatRelation object (Ref S-414651)

* June 19,2016   Sahil Batra   Modified - Added logic to create accountShare record for Edit access(I-222958)
* June 28,2016   Shreerath Nair   Modified - Added logic to add Procare Rep into Account Team(T-514970)
* June 30, 2016  Prakarsh Jain   Modified - Added logic to delete custom account team member records when isDeleted is checked(T-516154)
* July 07, 2016  Prakarsh Jain   Modified - Updated the logic to delete custom account team member records(I-224971)

* 5th Sep 2016   Nitish Bansal  Update (I-233136)  //merged class from production sync in Dev and resolved merged errors
* 10th Oct 2016  Kanika Mathur  Modified - Updated query to include a check as part of Prod sync
*  4th Nov 2016	 Deepti Maheshwari (Modified) Commented some code as part of prod sync
* 6th Dec 2016	 Deepti Maheshwari (Modified) Updated the code to reset sharing for comm accounts (I-246418)
* 8th Dec 2016	 Deepti Maheshwari (Modified) Empower Hot fixes Prod Sync
==================================================================*/
global class SyncStandardAccountTeamMember implements Database.Batchable<sObject> , System.Schedulable {
  
  /***********************************************************************************
  Start Method of the batch class
  ************************************************************************************/
  global Database.QueryLocator start(Database.BatchableContext BC) {
    DateTime dt = System.Now().addDays(-1);
    String query = '';
    query = 'SELECT User__c, Team_Member_Role__c,Contact__c,ResourceID__c, Name, User__r.Procare_Region_Rep__c, '; 
    query += ' IsDeleted__c, LastModifiedDate, Id, External_Integration_Id__c,';
    query += ' Effective_From_Date__c, Division__c, Account__c, Account_Access_Level__c';
    //Modified below query as part of Prod syn -KM 10/10/2016
    query += ' FROM Custom_Account_Team__c WHERE LastModifiedDate > :dt AND Team_Member_Role__c != null';
    query += ' AND ( Account__c != null OR Account_Access_Level__c != null )';
    system.debug('---query----'+query);
    return Database.getQueryLocator(query);
  }
  
  /***********************************************************************************
  Execute Method of the batch class
  ************************************************************************************/
  
  
  global void execute(Database.BatchableContext BC,List<Sobject> scope){
	//DM 12/8 (Modified) Empower Hot fixes Prod Sync
	try{
    system.debug('---scope----'+scope);
    List<AccountTeamMember> accountMemberList = new List<AccountTeamMember>();
    //update ref S-414651 starts
    List<AccountContactRelation> strykerContactList = new List<AccountContactRelation>();
    //update ref S-414651 ends
    Set<Id> userIdsSet = new Set<Id>();
    Set<Id> accountIdsSet = new Set<Id>();
    Set<Id> contactIDSet = new Set<Id>();
    //changes start   T-516154 
    Map<String, Custom_Account_Team__c> mapAccIdUserIdToCustomAccTeam = new Map<String, Custom_Account_Team__c>();
    List<Custom_Account_Team__c> customAccTeamListToBeDeleted = new List<Custom_Account_Team__c>();
    //changes end   T-516154
    String picklistforEditAccess = Label.AccountTeamSyncEditAccessPicklistValues;
    List<AccountTeamMember> accountMemberListToBeInserted = new List<AccountTeamMember>();
    List<AccountTeamMember> accountMemberListToBeUpdated = new List<AccountTeamMember>();
    List<AccountTeamMember> accountMemberListToBeDeleted = new List<AccountTeamMember>();
    List<AccountShare> accountShareList = new List<AccountShare>();
    List<AccountShare> accountShareListtoUpdate = new List<AccountShare>();
    List<AccountShare> accountShareListtoInsert = new List<AccountShare>();
    List<AccountShare> accountShareListtoDelete = new List<AccountShare>();
    //DM 11/4 : Commented as part of Prod sync    
    //T-514970 (SN)
    //Map<ID,Custom_Account_Team__c> UserIdWithCustomAccountMemberMap =  new Map<Id,Custom_Account_Team__c>();
	//DM 12/8 (Modified) Empower Hot fixes Prod Sync
    List<Custom_Account_Team__c> customAccountTeamToDeleteList = new List<Custom_Account_Team__c>();    //update ref S-414651 starts
    List<AccountContactRelation> strykerContactsToBeInserted = new List<AccountContactRelation>();
    List<AccountContactRelation> strykerContactsToBeDeleted = new List<AccountContactRelation>();    
    
    //update ref S-414651 ends
    Id commContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().
                                get(Constants.COMM_CONTACT_RTYPE).getRecordTypeId();
    
    for(Sobject s : scope){
      Custom_Account_Team__c customAccountTeam = (Custom_Account_Team__c)s;
      if(customAccountTeam.user__c != null){
        userIdsSet.add(customAccountTeam.User__c);        
      }
      accountIdsSet.add(customAccountTeam.Account__c);
      contactIDSet.add(customAccountTeam.contact__c);
    }
        
    Map<String,AccountShare> AccUserRoletoShareRecord = new Map<String,AccountShare>();
    Map<String,AccountTeamMember> AccUserRoletoAccountTeam = new Map<String,AccountTeamMember>();
    for(AccountShare accShare : [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel 
                                 FROM AccountShare
                                 WHERE UserOrGroupId IN:userIdsSet
                                 AND AccountId IN: accountIdsSet
                                 AND RowCause =:Schema.AccountShare.RowCause.Manual]){
      AccUserRoletoShareRecord.put(accShare.AccountId+'+'+accShare.UserOrGroupId,accShare);
    }

    Map<ID, Set<ID>> accountIDtoUserID = new Map<ID, Set<ID>>();
    Map<ID, Set<ID>> accountIDToContactID = new Map<ID, Set<ID>>();
    Map<String,AccountTeamMember> userAccIdToaccTeamMember = new Map<String,AccountTeamMember>();//Map is created to remove inner loop from the code
    Map<String,AccountContactRelation> contactAccountIDRelationship = new Map<String,AccountContactRelation>();
    Map<String, COMM_Profiles__c> commProfiles = COMM_Profiles__c.getAll();
    Set<String> commProfilesSet = new Set<String>();
    for(COMM_Profiles__c commProfileName : commProfiles.values()){
        commProfilesSet.add(commProfileName.Profile_Name__c);
    } 

    Map<Id, String> userProfileNameMap = new Map<Id, String>();
    for(User usr : [SELECT Id, Profile.Name FROM User WHERE Id in :userIdsSet]){
        userProfileNameMap.put(usr.id, usr.Profile.Name);
    }

    if(userIdsSet != null && userIdsSet.size() > 0) {
        for(AccountTeamMember accMember : [SELECT UserId, TeamMemberRole, Id, AccountId, AccountAccessLevel 
                           FROM AccountTeamMember 
                           WHERE UserId IN: userIdsSet 
                           AND AccountId IN: accountIdsSet]){
          accountMemberList.add(accMember);                    
          AccUserRoletoAccountTeam.put(accMember.AccountId+'+'+accMember.UserId+'+'+accMember.TeamMemberRole,accMember);
          if(accountIDtoUserID.containsKey(accMember.AccountId)) {
            accountIDtoUserID.get(accMember.AccountId).add(accMember.UserId);
          }else {
            accountIDtoUserID.put(accMember.AccountId, new Set<Id>{accMember.UserId});
          }
          if(!userAccIdToaccTeamMember.containsKey(accMember.UserId+'+'+accMember.AccountId)) {
            userAccIdToaccTeamMember.put(accMember.UserId+'+'+accMember.AccountId, accMember);
          }
          
        }
    }
  
    //Update ref T-484874 starts 
    if(contactIDSet != null && contactIDSet.size() > 0) {
    //Update ref S-414651 starts
      strykerContactList = [SELECT AccountId, ContactId, Stryker_Contact_ID__c
                           FROM AccountContactRelation
                           WHERE AccountId in :accountIdsSet
                           AND ContactId in :contactIDSet];
    }
    //update ref S-414651 ends 
    
    for(AccountContactRelation strykerCon : strykerContactList) {    
      if(!contactAccountIDRelationship.containsKey(strykerCon.ContactId+'+'+strykerCon.AccountId)) {
          contactAccountIDRelationship.put(strykerCon.ContactId+'+'+strykerCon.AccountId,strykerCon);
      }
    }
    
    //update ref S-414651 ends  
    //Update ref T-484874 ends
    Map<ID,Set<ID>> accountIDUserMap = new Map<ID,Set<ID>>();
    Set<ID> accountIDSet = new Set<ID>();
    List<ContactShare> contactSharesToInsert = new List<ContactShare>();
  
    for(Sobject s : scope){
      Custom_Account_Team__c customAccountTeam = (Custom_Account_Team__c)s;
      //Changes start I-224971
      if(customAccountTeam.IsDeleted__c){
        customAccTeamListToBeDeleted.add(customAccountTeam);
      }
      //Changes End I-224971
      String accTeamkey = customAccountTeam.Account__c+'+'+customAccountTeam.User__c+'+'+customAccountTeam.Team_Member_Role__c;
      String accSharekey = customAccountTeam.Account__c+'+'+customAccountTeam.User__c;
      //changes start   T-516154
      mapAccIdUserIdToCustomAccTeam.put(accTeamkey, customAccountTeam);
      //changes end   T-516154
      // Insert Standard Account team Records
      System.debug('>>>>1'+accTeamkey);
      System.debug('>>>>2'+AccUserRoletoAccountTeam.containsKey(accTeamkey));
      if(AccUserRoletoAccountTeam.containsKey(accTeamkey)){       
        //T-514970 (SN) check if team role is Procare Rep        
        System.debug('>>>>3'+customAccountTeam.IsDeleted__c);
        if(customAccountTeam.IsDeleted__c){
          accountMemberListToBeDeleted.add(AccUserRoletoAccountTeam.get(accTeamkey));
        }
        else{
          AccUserRoletoAccountTeam.get(accTeamkey).TeamMemberRole = customAccountTeam.Team_Member_Role__c;
          accountMemberListToBeUpdated.add(AccUserRoletoAccountTeam.get(accTeamkey));
        }
      }else{
        if(!customAccountTeam.IsDeleted__c){
           accountMemberListToBeInserted.add(new AccountTeamMember(UserId = customAccountTeam.User__c,
                                        AccountId = customAccountTeam.Account__c,
                                        TeamMemberRole = customAccountTeam.Team_Member_Role__c));
                    

            // Update ref - T-484559 start
            if(accountIDUserMap.containsKey(customAccountTeam.Account__c)){
                accountIDUserMap.get(customAccountTeam.Account__c).add(customAccountTeam.User__c);
              }else{
                accountIDUserMap.put(customAccountTeam.Account__c,new Set<ID>{customAccountTeam.User__c});
            }           
         
          }
      }
    
      if(customAccountTeam.contact__c != null){
        if(contactAccountIDRelationship !=null 
            && contactAccountIDRelationship.containsKey(customAccountTeam.Contact__c+'+'+customAccountTeam.Account__c)){
            if(customAccountTeam.IsDeleted__c){           
              strykerContactsToBeDeleted.add(contactAccountIDRelationship.get(customAccountTeam.Contact__c+'+'+customAccountTeam.Account__c));               
            }
        }else{
            //update ref S-414651 starts    
            strykerContactsToBeInserted.add(new AccountContactRelation(
            ContactId = customAccountTeam.Contact__c,
            AccountId = customAccountTeam.Account__c,
            //AccountAccessLevel = customAccountTeam.Account_Access_Level__c, Not Writeable
            //IsActive = true,
            Stryker_Contact_ID__c = customAccountTeam.ResourceID__c
            ));        
              
            //update ref S-414651 ends
        }
      }
      //Update ref T-484874 ends
      //I-246418 Fix
      if(customAccountTeam.user__c != null 
      && userProfileNameMap.containsKey(customAccountTeam.user__c)
      && !commProfilesSet.contains(userProfileNameMap.get(customAccountTeam.user__c))){

          // Insert sharing records to provide Edit access to account team members and managers in Roles and Hierarchy
          if(AccUserRoletoShareRecord.containsKey(accSharekey)){
            if(customAccountTeam.IsDeleted__c){
              accountShareListtoDelete.add(AccUserRoletoShareRecord.get(accSharekey));
            } else{
              AccountShare accShare = AccUserRoletoShareRecord.get(accSharekey);
              if(picklistforEditAccess != null && customAccountTeam.Account_Access_Level__c != null 
              && picklistforEditAccess.containsIgnoreCase(customAccountTeam.Account_Access_Level__c)){
                system.debug('Deepti::: Updating access in if ' + accShare.AccountAccessLevel);
                if(accShare.AccountAccessLevel!='Edit'){
                  accShare.AccountAccessLevel = 'Edit';
                  accountShareListtoUpdate.add(accShare);
                }
              } else{
                system.debug('Deepti::: Updating access ' + accShare.AccountAccessLevel);
                if(accShare.AccountAccessLevel=='Edit'){
                  accShare.AccountAccessLevel = 'Read';
                  accountShareListtoUpdate.add(accShare);
                }
              }
            }
          } else{
			 //DM 12/8 (Modified) Empower Hot fixes Prod Sync
              if(!customAccountTeam.IsDeleted__c){
                  //T-514970 (SN) function to create account share record
	            AccountShare accShare = createAccountShareRecord(customAccountTeam);
	            accountShareListtoInsert.add(accShare);
              }
          }           
      }
    }
    // Update ref - T-484559 start
    if(accountIDUserMap != null && accountIDUserMap.size() > 0){
        for(Contact contacts : [SELECT Private__c, AccountID,RecordTypeID, ID 
                               FROM Contact 
                               WHERE AccountId in :accountIDUserMap.keyset()
                               AND private__c = false 
                               AND recordTypeID = :commContactRecordTypeId]){
            for(ID userID : accountIDUserMap.get(contacts.AccountID)){
              ContactShare conShare = new ContactShare();
              conShare.UserOrGroupId = userID;
              conShare.ContactAccessLevel = Constants.PERM_READ;
              conShare.contactID = contacts.ID;
              contactSharesToInsert.add(conShare);
            }
        }
        Map<Id, Contact> contactsOfAccount = new Map<ID, Contact>([SELECT ID 
                                          FROM Contact 
                                          WHERE AccountID in :accountIDSet 
                                          AND Private__c = false]);
        List<ContactShare> contactShares = [SELECT ID 
                                        FROM ContactShare 
                                        WHERE ContactId in :contactsOfAccount.keyset() 
                                        AND UserOrGroupId in :userIdsSet
                                        AND RowCause = :Constants.ROW_CAUSE_MANUAL];
        if(contactShares != null && contactShares.size() > 0){
            Map<Id, ContactShare> mapSObj = new Map<Id, ContactShare>();
            mapSObj.putAll(contactShares);
            Database.delete(mapSObj.values(), false);
        }
    }
    // Update ref - T-484559 end
    // Update ref - T-484559 end
    if(contactSharesToInsert.size() > 0){
        Map<Id, ContactShare> mapSObj = new Map<Id, ContactShare>();
        mapSObj.putAll(contactSharesToInsert);
        Database.insert(mapSObj.values(),false);
      }
      
      //Update ref T-484874 ends
      if(strykerContactsToBeDeleted.size() > 0){
        Map<Id, AccountContactRelation> mapSObj = new Map<Id, AccountContactRelation>();
        mapSObj.putAll(strykerContactsToBeDeleted);
        Database.Delete(mapSObj.values(),false);
      }
      if(strykerContactsToBeInserted.size() > 0){
        Map<Id, AccountContactRelation> mapSObj = new Map<Id, AccountContactRelation>();
        mapSObj.putAll(strykerContactsToBeInserted);
        Database.insert(mapSObj.values(),false); 
      }
       
    if(accountMemberListToBeInserted.size()>0){
      system.debug('accountMemberListToBeInserted' + accountMemberListToBeInserted);
      List<Database.UpsertResult> sr = Database.upsert(accountMemberListToBeInserted, false);
      Map<ID, User> usersMap = new Map<ID, User>([SELECT ProfileID, ID, Profile.Name 
                                                FROM User 
                                                WHERE ID in :userIdsSet]);
    
      List<AccountShare> accountSharesForTeam = new List<AccountShare>(); 
      Integer i = 0;
      for(Database.UpsertResult res : sr){
        if(res.isSuccess()){
          if(usersMap.containsKey(accountMemberListToBeInserted[i].UserID) 
          && usersMap.get(accountMemberListToBeInserted[i].UserID).Profile.Name == Constants.STRATEGIC_SALES_PROFILE){
            accountSharesForTeam.add(new AccountShare(AccountID = accountMemberListToBeInserted[i].AccountID,
            UserOrGroupId = accountMemberListToBeInserted[i].UserId, AccountAccessLevel = Constants.PERM_EDIT,
            OpportunityAccessLevel = Constants.PERM_READ, CaseAccessLevel = Constants.PERM_READ));
          }  
        }
        i++;
      }
    
      if(accountSharesForTeam.size() > 0)
      accountShareListtoInsert.addall(accountSharesForTeam);
    }
    

    if(accountShareListtoInsert.size() > 0){
      Map<Id, AccountShare> mapSObj = new Map<Id, AccountShare>();
      mapSObj.putAll(accountShareListtoInsert);
      Database.insert(mapSObj.values(),false);
    }
    if(accountShareListtoUpdate.size() > 0){
      Map<Id, AccountShare> mapSObj = new Map<Id, AccountShare>();
      mapSObj.putAll(accountShareListtoUpdate);
      Database.update(mapSObj.values(),false);
    }
    if(accountShareListtoDelete.size() > 0){
      Map<Id, AccountShare> mapSObj = new Map<Id, AccountShare>();
      mapSObj.putAll(accountShareListtoDelete);
      Database.delete(mapSObj.values(),false);
    }

    if(accountMemberListToBeUpdated.size()>0){
      Map<Id, AccountTeamMember> mapSObj = new Map<Id, AccountTeamMember>();
      mapSObj.putAll(accountMemberListToBeUpdated);
      Database.update(mapSObj.values(),false);
    }
    if(accountMemberListToBeDeleted.size() > 0){
      Map<Id, AccountTeamMember> mapSObj = new Map<Id, AccountTeamMember>();
      mapSObj.putAll(accountMemberListToBeDeleted);
      Database.delete(mapSObj.values(),false);
    }
    //changes start   T-516154
    if(customAccTeamListToBeDeleted.size()>0){
      Map<Id, Custom_Account_Team__c> mapSObj = new Map<Id, Custom_Account_Team__c>();
      mapSObj.putAll(customAccTeamListToBeDeleted);
      Database.delete(mapSObj.values(),false);
    }
    //changes end   T-516154
    
    opptyShareRecordInsert(accountIDUserMap);
    //DM 12/8 (Modified) Empower Hot fixes Prod Sync
	}catch(Exception ex){
	    ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'SyncStandardAccountTeamMember', true);
	    
	}
        
  }
  
  
  //**********************************************************************COMM********************************************//
  //Name            :     opptyShareRecordInsert Method
  //Description     :     To insert Opportunity Share Records
  //Created By      :     Meghna Vijay T-484598 S-371029
  //Created Date    :     March 18, 2016
  //@param Map of Id for accountIDUserMap*/
    private void opptyShareRecordInsert(Map < ID, Set < ID >> accountIDUserMap) {
      List < OpportunityShare > opportunitySharesToInsert = new List < OpportunityShare > ();
    Map<ID, Schema.RecordTypeInfo> opptyRtMap = 
                      Schema.SObjectType.Opportunity.getRecordTypeInfosById();                                 
      if (accountIDUserMap != null && accountIDUserMap.size() > 0) {
        // Loop to populate Account Id and Record Type Id of Oppty Object
        for (Opportunity opportunities: [SELECT AccountId, 
                                                RecordTypeId,OwnerID
                                         FROM Opportunity
                                         WHERE AccountId in : accountIDUserMap.keyset()]) {
          if(opptyRTMap.get(opportunities.RecordtypeID).getName().contains(Constants.COMM)){
          for (ID userID: accountIDUserMap.get(opportunities.AccountID)) {
            system.debug('Deepti::: Updating oppty share access ' + opportunities);
            if(userID != opportunities.OwnerID){
                OpportunityShare opShare = new OpportunityShare();
                opShare.UserOrGroupId = userID;
                opShare.OpportunityAccessLevel = Constants.PERM_READ;
                opShare.OpportunityId = opportunities.ID;
                opportunitySharesToInsert.add(opShare);
            }
          }
          }
        }
        // Insertion of Oppty Share records if list is not empty
        if (!opportunitySharesToInsert.isEmpty()) {
          Map<Id, OpportunityShare> mapSObj = new Map<Id, OpportunityShare>();
          mapSObj.putAll(opportunitySharesToInsert);
          List<Database.UpsertResult> sr = Database.upsert(mapSObj.values(),false);
        }
      }
    }
    // Update ref - T-484598 end
   //*********************************************************************COMM***********************************************// 
  /***********************************************************************************
  Finish Method of the batch class
  ************************************************************************************/
  global void finish(Database.BatchableContext BC) {
  
  }
  
  /***********************************************************************************
  Schedular for batch
  ************************************************************************************/
  
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
      Id batchId = Database.executeBatch(new SyncStandardAccountTeamMember(), 200);
      system.debug('SyncStandardAccountTeamMember @@@batchId = ' + batchId);
      //batchId = Database.executeBatch(new NotifyAccountTeamBatch());
      //system.debug('NotifyAccountTeamBatch @@@batchId = ' + batchId);
    }
  }

  //DM 12/8 (Modified) Empower Hot fixes Prod Sync
  //T-514970 (SN) function to create accountShare record
  public static AccountShare createAccountShareRecord(Custom_Account_Team__c customAccountTeam){
    
    String picklistforEditAccess = Label.AccountTeamSyncEditAccessPicklistValues;
    AccountShare accShare = new AccountShare();
    accShare.AccountId = customAccountTeam.Account__c;
    accShare.UserOrGroupId = customAccountTeam.User__c;
    accShare.RowCause = Schema.AccountShare.RowCause.Manual;
    accShare.OpportunityAccessLevel = 'None';
    accShare.ContactAccessLevel = 'None';
    accShare.CaseAccessLevel = 'None';
    if(customAccountTeam.Account_Access_Level__c!=null && picklistforEditAccess.containsIgnoreCase(customAccountTeam.Account_Access_Level__c)){
       accShare.AccountAccessLevel = 'Edit';
    }else{
       accShare.AccountAccessLevel = 'Read';
     }
     
     return accShare;
    
  }
}