/**================================================================      
* Appirio, Inc
* Name: CheckPODocumentTest
* Description: Test class of CheckPODocument
* Created Date: 29th Nov 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 
@isTest
private class CheckPODocumentTest
{
	@isTest
	static void itShould()
	{
		Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Endo COMM Customer').getRecordTypeId();
		Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('COMM Service Opportunity').getRecordTypeId();
        
        Account acc = TestUtils.createAccount(1, false).get(0);
        acc.RecordTypeId = accRT;
        acc.Endo_Active__c = true;
        acc.COMM_Shipping_Country__c = 'United States';
        acc.COMM_Shipping_Address__c = 'test';
        acc.COMM_Shipping_State_Province__c  = 'Arizona';
        acc.COMM_Shipping_PostalCode__c = '123';
        acc.COMM_Shipping_City__c = 'testcity';
        insert acc;

        Pricebook2 pricebook = new Pricebook2(Name = 'COMM Price Book');
        insert pricebook;

        List<Opportunity> oppList = TestUtils.createOpportunity(1, acc.Id, false);
        oppList[0].Document_Type_Comm__c = 0;
        oppList[0].RecordTypeId = rtOpp;
        insert oppList;

        Test.startTest();
	        ApexPages.StandardController cntrl = new ApexPages.StandardController(oppList[0]);
	        CheckPODocument com = new CheckPODocument(cntrl);
	        oppList = [Select id, Document_Type_Comm__c from Opportunity];
	        oppList[0].Document_Type_Comm__c = 1;
	        update oppList;
	        cntrl = new ApexPages.StandardController(oppList[0]);
	        com = new CheckPODocument(cntrl);
	        CheckPODocument.CheckPODocumentUpdate();
        Test.stopTest();
	}
}