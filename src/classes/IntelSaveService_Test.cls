/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 11, 2013
 Purpose       :  Test Class for IntelSaveService.cls
 Reference     :  Task T-205140
 Modifications :  Task T-207120    Dec 23, 2013    Sonal Shrivastava
*******************************************************************************/
@isTest 
private class IntelSaveService_Test {
  static testMethod void myUnitTest() {
    
    //Create test records
    User usr1 = TestUtils.createUser(1, null, true).get(0);
    List<Intel__c> intelList = TestUtils.createIntel(1, true);
    List<Tag__c> tagList = TestUtils.createTag(1, intelList.get(0).Id, true);
    
    intelList = [SELECT Id, OwnerId, Owner.Name, CreatedDate, Details__c, Flagged_As_Inappropriate__c, LastModifiedDate, Chatter_Post__c, Mobile_Update__c, Intel_Flags__c,
	                (SELECT Id, Name, Title__c, Account__c, Contact__c, Product__c, User__c, CreatedDate FROM Tags__r)
	               FROM Intel__c WHERE Id = :intelList.get(0).Id];
    
    List<IntelListWrapper.IntelJson> intelJsonList = new List<IntelListWrapper.IntelJson>();
    List<IntelListWrapper.TagJson> tagJsonList = new List<IntelListWrapper.TagJson>();
    List<IntelListWrapper.CommentJson> commentJsonList = new List<IntelListWrapper.CommentJson>();
    List<IntelListWrapper.LikeJson> likeJsonList = new List<IntelListWrapper.LikeJson>();
    List<IntelListWrapper.AttachmentJson> attJsonList  = new List<IntelListWrapper.AttachmentJson>();
     
    tagJsonList.add(new IntelListWrapper.TagJson(intelList.get(0).Tags__r.get(0)));
    intelJsonList.add(new IntelListWrapper.IntelJson(intelList.get(0), usr1, tagJsonList, commentJsonList, likeJsonList, attJsonList));
    
    IntelListWrapper wrap = new IntelListWrapper();
    wrap.intelList = intelJsonList;
    
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    
    req.requestURI = '/services/apexrest/IntelSaveService/*';
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueOf(JSON.serialize(wrap)); 
    
    RestContext.request = req;
    RestContext.response = res;
    
    test.startTest();
    Map<String, String> result = IntelSaveService.doPost();    
    System.assertEquals(1, [SELECT Id FROM Intel__c WHERE Id NOT IN :intelList].size());
    System.assertEquals(1, [SELECT Id FROM Tag__c WHERE Id NOT IN :tagList].size());    
    test.stopTest();
  }
}