/*******************************************************************************
Created By:    Sunil Gupta
Date:          Sep 15, 2014
Description  : Controller class for Flex_AccountHierarchy
This page is a clone functionality of standard Account Hierarchy Page.

Change Log:
05/24/2016     Jeff Hulslander (Appirio)      Added check for instruments record type to re-use page as inline VF in our layouts
*******************************************************************************/
public with sharing class Flex_AccountHierarchyController {
    
    // Variables
  public Account currentAccount{get;set;}
    public List<AccountWrapper> lstWrapper;
  public Boolean isInstruments {get;set;}
  private Map<String, AccountWrapper> mapAccountWrapper;
    
    //----------------------------------------------------------------------------
  // Constructor 
  //----------------------------------------------------------------------------
  public Flex_AccountHierarchyController(ApexPages.StandardController controller) {
    this.currentAccount = (Account)controller.getRecord();
    isInstruments = false;

    if(this.currentAccount == null){
        return;
    }
    
    Id instRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type').getRecordTypeId();
    if(this.currentAccount.RecordTypeId == instRT){
      isInstruments = true;
    }
    this.lstWrapper = new List<AccountWrapper>{};
    this.mapAccountWrapper = new Map<String, AccountWrapper>{};
  }
    
    
    //----------------------------------------------------------------------------
  // Get List of Wrapper Object used to bind page block table. 
  //----------------------------------------------------------------------------
  public List<AccountWrapper> getObjectStructure(){
    this.lstWrapper.clear();
    this.lstWrapper = buildHierarchy();
    
    
    for(AccountWrapper obj :lstWrapper){
        Integer countDots = obj.nodeId.replace('.', '').trim().length();
        String s = '';
        for(Integer i=0; i< countDots; i++){
            s = s + '&nbsp;';
        }
        obj.accountName = s;
        //obj.accountName = obj.nodeId; 
        //obj.accountName = obj.nodeId.replace('.', '&nbsp;').replace('0', '&nbsp;'); 
    }
    return this.lstWrapper;
  }
  
  //----------------------------------------------------------------------------
  // Helper method to check if Element is the top node 
  //----------------------------------------------------------------------------
  private String getTopElement(){
    Boolean top = false;
    Id tempId = currentAccount.Id;
    while(!top){
        Account a = [SELECT Id, ParentId FROM Account WHERE Id = :tempId LIMIT 1];
        if(a.ParentID != null){
            tempId = a.ParentID;
      }
      else {
        top = true;
      }
    }
    return tempId;
  }
  
  
  //----------------------------------------------------------------------------
  // Helper method to Query Accounts from top down to build the AccountWrapper 
  //----------------------------------------------------------------------------
  private List<AccountWrapper> buildHierarchy(){
    List<AccountWrapper> lstTemp = new List<AccountWrapper>{};
    mapAccountWrapper.clear();
    
    List<Account> lstAccounts = new List<Account>{};
    List<Id> currentParent = new List<ID>{};
    Map<Id, String> nodeList = new Map<ID, String>{};
    List<String> nodeSortList = new List<String>{};
    String nodeId = '0';
    Integer count = 0;
    Integer level = 0;
    Boolean endOfStructure = false;
    
    //Find highest level obejct in the structure
    currentParent.add(getTopElement());

    // Loop though all children
    while (!endOfStructure){
        if(level == 0){
            lstAccounts = [SELECT Id, AccountNumber, Name, Type, ShippingStreet, Industry, ParentId, BillingCity, BillingState, ShippingCity, ShippingState, OwnerId ,Owner.Name 
                           FROM Account WHERE Id IN :currentParent ORDER BY Name];
      }
      else {
        if(isInstruments){
            lstAccounts = [SELECT Id, AccountNumber, Name, Type, ShippingStreet, Industry, ParentId, BillingCity, BillingState, ShippingCity, ShippingState, OwnerId,Owner.Name, Account_Number_Ship_To__c
                       FROM Account a WHERE ParentID IN :currentParent ORDER BY ShippingState,ShippingCity,Account_Number_Ship_To__c,Name];
        }else{
            lstAccounts = [SELECT Id, AccountNumber, Name, Type, ShippingStreet, Industry, ParentId, BillingCity, BillingState, ShippingCity, ShippingState, OwnerId,Owner.Name, Account_Number_Ship_To__c
                       FROM Account a WHERE ParentID IN :currentParent ORDER BY Name];
        }
      }
      if(lstAccounts.size() == 0){
        endOfStructure = true;
      }
      else{
        currentParent.clear();
        for (Integer i = 0 ; i < lstAccounts.size(); i++ ){
          Account a = lstAccounts[i];
          nodeId = (level > 0) ? nodeList.get(a.ParentId) + '.' + String.valueOf(i) : String.valueOf(i);
          mapAccountWrapper.put(nodeId, new AccountWrapper(nodeID, false, false, a));
          currentParent.add(a.id );
          nodeList.put(a.id, nodeId);
          nodeSortList.add(nodeId);
        }
        level++;
      }
    }
    
    //Account structure must now be formatted
    nodeSortList.sort();
    for( Integer i = 0; i < nodeSortList.size(); i++ ){
      List<String> pnl = new List<String> {};
      List<String> cnl = new List<String> {};
      List<String> nnl = new List<String> {};
            
      if ( i > 0 ){
        String pn   = nodeSortList[i-1];
        pnl         = pn.split( '\\.', -1 );
      }
      
      String cn   = nodeSortList[i];
      cnl         = cn.split( '\\.', -1 );

      if( i < nodeSortList.size()-1 ){
        String nn = nodeSortList[i+1];
        nnl = nn.split( '\\.', -1 );
      }
            
      AccountWrapper tasm = mapAccountWrapper.get( cn );
      if ( cnl.size() < nnl.size() ){
        //Parent
        //tasm.nodeType = ( isLastNode( cnl ) ) ? 'parent_end' : 'parent';
      }
      else if( cnl.size() > nnl.size() ){
        //tasm.nodeType   = 'child_end';
        //tasm.closeFlag  = setcloseFlag( cnl, nnl, tasm.nodeType );
      }
      else{
        //tasm.nodeType = 'child';
      }
            
      //tasm.levelFlag = setlevelFlag( cnl, tasm.nodeType ); 
            
      //Change below
      if ( tasm.account.id == currentAccount.Id ) {
        tasm.currentNode = true;
      }
      lstTemp.add( tasm );
    }
        
    //asm[0].nodeType             = 'start';
    //asm[asm.size()-1].nodeType  = 'end';
    return lstTemp;
  }
  
  //----------------------------------------------------------------------------
  // Wrapper class
  //----------------------------------------------------------------------------
  public without sharing class AccountWrapper{
    public String nodeId {get;set;}
    public Boolean currentNode {get;set;}
    public Account account {get;set;}
    public String accountName {get;set;}
    public AccountWrapper(String nodeId, Boolean lastNode, Boolean currentNode, Account a){
      this.nodeId = nodeId;
      this.currentNode = currentNode;
      this.account = a;
    }
  }
}