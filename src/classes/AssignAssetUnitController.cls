/*
Name             AssignAssetUnitController 
Purpose          T-497433 (To replace asset and it's fields via Installed Product)
Modified Date    04/27/2016
Modified By      Nitish Bansal
*/


public with sharing class AssignAssetUnitController {

  transient Case c;
  private id caseId;
  
  public List<AssetObject> assetObjectList {get;set;}
  
  public List<Integer> unitCount {get;set;}
  
  private Map<Id, Map<Integer, AssetUnit__c>> existingAssetUnits;
  
  private static final Map<String, Map<Integer, String>> fieldNameDefinitions  = new Map<String, Map<Integer, String>>{
                                                    'Serial' => new Map<Integer,String> {
                                                                      1 => 'Serial_Number_1__c',
                                                                      2 => 'Serial_2__c',
                                                                      3 => 'Serial_3__c',
                                                                      4 => 'Serial_Number_4__c',
                                                                      5 => 'Serial_Number_5_Unit_5__c',
                                                                      6 => 'Serial_Number_6_Unit_6__c'
                                                                      },
                                                    'Unit' => new Map<Integer,String> {
                                                                      1 => 'Unit1__c',
                                                                      2 => 'Unit2__c',
                                                                      3 => 'Unit3__c',
                                                                      4 => 'Unit4__c',
                                                                      5 => 'Unit_5__c',
                                                                      6 => 'Unit_6__c'
                                                                      }
                                                    };

  public AssignAssetUnitController (Apexpages.Standardcontroller ctrl) {
    c          = getCase(ctrl.getId());
    caseId        = c.id;
    unitCount      = new List<Integer>{1,2,3,4,5,6};
    existingAssetUnits  = getAssetUnits();
    
    assetObjectList    = new List<AssetObject>();
    createPageObjects();
  }

  public void createPageObjects() {
    List<SVMXC__Installed_Product__c> installedProducts  = getInstalledProducts();
    
    for (SVMXC__Installed_Product__c installedProduct : installedProducts) {
      assetObjectList.add(new AssetObject(installedProduct, c, this.existingAssetUnits, this.unitCount.size()));
    }
  }

  private Case getCase (Id caseId) {
    return [SELECT Id, AccountId FROM Case WHERE Id = :caseId];
  }
    
 /* private List<Asset> getAssets () {
    List<Asset> assets  = new List<Asset>();
    for (Asset asset : [SELECT Id, Name, Room__c, Serial_Number_1__c, Serial_2__c, Serial_3__c, Serial_Number_4__c, Serial_Number_5_Unit_5__c, Serial_Number_6_Unit_6__c, Unit1__c, Unit2__c, Unit3__c, Unit4__c, Unit_5__c, Unit_6__c, Warranty_Start_Date__c, Warranty_End_Date__c, Service_Agreement__c, Agreement_Type__c FROM Asset WHERE AccountId = :c.AccountId]) {
      assets.add(asset);
    }
    return assets;
  }*/

  private List<SVMXC__Installed_Product__c> getInstalledProducts () {
    List<SVMXC__Installed_Product__c> installedProducts  = new List<SVMXC__Installed_Product__c>();
    for (SVMXC__Installed_Product__c installedProduct : [SELECT Id, Name, Room__c, Serial_Number_1__c, Serial_2__c, Serial_3__c, 
                                                Serial_Number_4__c, Serial_Number_5_Unit_5__c, Serial_Number_6_Unit_6__c, Unit1__c, Unit2__c, Unit3__c,
                                                Unit4__c, Unit_5__c, Unit_6__c, SVMXC__Warranty_Start_Date__c, SVMXC__Warranty_End_Date__c, 
                                                Service_Agreement__c, Agreement_Type__c FROM SVMXC__Installed_Product__c 
                                                WHERE SVMXC__Company__c = :c.AccountId]) {
      installedProducts.add(installedProduct);
    } 
    return installedProducts;
  }
  
  private Map<Id, Map<Integer, AssetUnit__c>> getAssetUnits () {
    Map<Id, Map<Integer, AssetUnit__c>> existingAssetUnit  = new Map<Id, Map<Integer, AssetUnit__c>>();
    for (AssetUnit__c assetUnit : [SELECT Id, Name, AssetUnit__c, Asset__c, SerialNumber__c, Case__c, Installed_Product__c,
                                          Installed_Product__r.Serial_Number_1__c, Installed_Product__r.Serial_2__c,
                                          Installed_Product__r.Serial_Number_4__c, Installed_Product__r.Serial_3__c,
                                          Installed_Product__r.Serial_Number_5_Unit_5__c, Installed_Product__r.Serial_Number_6_Unit_6__c 
                                          FROM AssetUnit__c WHERE Case__c = :c.Id]) {
      if (assetUnit.Installed_Product__c != null) {
        
        for (Integer fieldNo : fieldNameDefinitions.get('Serial').keySet()) {
          if (assetUnit.SerialNumber__c == assetUnit.Installed_Product__r.get(fieldNameDefinitions.get('Serial').get(fieldno))) {
            if (!existingAssetUnit.containsKey(assetUnit.Installed_Product__c)) {
              existingAssetUnit.put(assetUnit.Installed_Product__c, new Map<Integer, AssetUnit__c>());
            }
            existingAssetUnit.get(assetUnit.Installed_Product__c).put(fieldNo, assetUnit);
          }
        }
      }
    }
    return existingAssetUnit;
  }

  public class AssetUnitObject {
    public Boolean isMarked {get;set;}
    public AssetUnit__c assetUnit {get;set;}
    
    public AssetUnitObject (AssetUnit__c assetUnit) {
      this.isMarked  = false;
      this.assetUnit  = assetUnit;
    }
  }
  
  public class AssetObject {
    public Boolean isMarked {get;set;}
    public Map<Integer, AssetUnitObject> assetUnitsMap {get;set;}
    
    public String agreementType {get;set;}
    public String serviceAgreement {get; set;}

    public String warrantyStartDate;
    public String warrantyEndDate;
      
    public String room {get;set;}
    public String name {get; set;}
    
    public AssetObject (SVMXC__Installed_Product__c a, Case c, Map<Id, Map<Integer, AssetUnit__c>> existingAssetUnits, Integer unitCount) {
      
      this.isMarked    = false;
      this.agreementType  = a.Agreement_Type__c;
      this.serviceAgreement = a.Service_Agreement__c;
      setWarrantyStartDate(a.SVMXC__Warranty_Start_Date__c);
      setWarrantyEndDate(a.SVMXC__Warranty_End_Date__c);
      this.room = a.Room__c;
      this.name = a.Name;
      
      this.assetUnitsMap  = new Map<Integer, AssetUnitObject>();
      
      Map<Integer, AssetUnit__c> existingUnits  = new Map<Integer, AssetUnit__c>();
      
      system.debug(existingAssetUnits);
      
      if (existingAssetUnits.containsKey(a.id)) {
        existingUnits  = existingAssetUnits.get(a.id);
        
        system.debug(existingUnits);
      }
      
      for (Integer i=1; i<=unitCount; i++) {
        String fieldNo  = String.valueOf(i);
        
        Boolean existingAU  = false;
        
        AssetUnit__c assetUnit  = new AssetUnit__c(
                              Name      = String.valueOf(a.get(fieldNameDefinitions.get('Unit').get(i))),
                              Installed_Product__c    = a.Id,
                              Case__c      = c.Id,
                              SerialNumber__c  = String.valueOf(a.get(fieldNameDefinitions.get('Serial').get(i))),
                              AssetUnit__c  = String.valueOf(a.get(fieldNameDefinitions.get('Unit').get(i)))
                              );
    
        if (existingUnits.containsKey(i)) {
          assetUnit  = existingUnits.get(i);
          existingAU  = true;
        }
        AssetUnitObject auo  = new AssetUnitObject(assetUnit);
        auo.isMarked    = existingAU;

        this.assetUnitsMap.put(i, auo);
      }
    }
    
    public void setWarrantyStartDate(Date d){
      if(d == null){
        this.warrantyStartDate = '';
      }else{
        this.warrantyStartDate = d.format();
      }
    }
    public String getWarrantyEndDate(){
      return this.warrantyEndDate;
    }
    
    public void setWarrantyEndDate(Date d){
      if(d == null){
        this.warrantyEndDate = '';
      }else{
        this.warrantyEndDate = d.format();
      }
    }
    
    public String getWarrantyStartDate(){
      return this.warrantyStartDate;
    }
  }
  
  public PageReference saveChanges () {
    List<AssetUnit__c> assetUnitUpdates  = new List<AssetUnit__c>();
    List<AssetUnit__c> assetUnitDeletes  = new List<AssetUnit__c>();
  
    for (AssetObject assetObject : assetObjectList) {

      for (AssetUnitObject assetUnitObject : assetObject.assetUnitsMap.values()) {
        if ((assetObject.isMarked || assetUnitObject.isMarked) && assetUnitObject.assetUnit.SerialNumber__c != null) {              
          assetUnitUpdates.add(assetUnitObject.assetUnit);
        }
        else if (assetUnitObject.assetUnit.Id != null) {                
          assetUnitDeletes.add(new AssetUnit__c(Id=assetUnitObject.assetUnit.Id));
          assetUnitObject.assetUnit.Id  = null;
        }
      }
    }
    
    if (!assetUnitUpdates.isEmpty()) {
      upsert assetUnitUpdates;
    }
    if (!assetUnitDeletes.isEmpty()) {
      delete assetUnitDeletes;
    }
    return new PageReference('/' + caseId);
  }
}