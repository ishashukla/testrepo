/*
*   This class is responsible for placing the webserice call and updating records along with wdoing a chatter post

* 14 Sept 2015   Durgesh Dhoot   Updated - added logic to update order Item Status to 'Delivered'
  
  Modified By :   Jessica Schilling
  Date        :   5/23/2016
  Case        :   Case 169599

  Modified By :   Jessica Schilling
  Date        :   6/02/2016
  Case        :   Case 169518
*/
public class NV_FedExRequest {
    
    Map <String,String> TrackingMapForDelivered = new Map < String, String > (); // this will hold the tracking number which have status Delivered
    Map <String,String> TrackingMapForOther = new Map < String, String > (); // This will hold the status for other status than Delivered 
    Set <String> deliveredPackages = new Set <String> (); // This the set will be used to know what are the tracking number is delivered and then will be
    
    //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - Start
    //Map to hold Tracking number with TrackDetail instance of NV_FedExTrackingService
    Map<String, NV_FedExTrackingService.TrackDetail> mapFedExTrackDetails = new Map<String, NV_FedExTrackingService.TrackDetail>(); 
    //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - End
         
    // This map hold the mapping between tracking number and order item, One tracking number may have multiple item so string,list                                                   // iterated for orderItem Update
    //public static Map<String,List<OrderItem>> orderItemMap {get;set;} {orderItemMap = new Map<String,List<OrderItem>> (); } 
    public Map<String,List<OrderItem>> orderItemMap {get;set;}
    //JSchilling Case 169599 5/23/2016 start
    //Changed variable to custom label to make it easier to change in the future
    public Integer CALLOUT_MAX = (Label.Max_Callout != '' ? Integer.valueof(Label.Max_Callout) : 30); //Errors out past this.
    // ============== All values will come from custom setting ===================
    String FEDEXKEY; // This the service key to invoke 
    String FEDEXPASSWORD; // This the service password to invoke 
    String FEDEXACCOUNTNUMBER; // This the service account number to invoke 
    String FEDEXMETERNUMBER; // This the service meternumber to invoke 
    Integer FEDEXLIMIT;  //  This the service limit to invoke track service as per documentation it is 30 .
    String FEDEXINTEGRATORID; // This the service integratorId to invoke 
    String FEDEXTRACKTYPE;  //// This the track type to invoke used "TRACKING_NUMBER_OR_DOORTAG"
    Integer  MAX_TIME; // this is the time to wait after invocation of service 
    FedExServiceConfig__c config; // Custom setting inatance 
    
    //
    // constructor of the class will be used for initialization of system variables 
    //
    public NV_FedExRequest(){
        orderItemMap = new Map<String,List<OrderItem>> ();
        // getting the values from config
        config = FedExServiceConfig__c.getInstance('Service Call Config');
        if(config <> NULL){
            FEDEXKEY =config.FedEx_Key__c ;
            FEDEXPASSWORD =config.FedEx_Password__c;
            FEDEXACCOUNTNUMBER = config.FedEx_AccountNumber__c;
            FEDEXMETERNUMBER =config.FedEx_MeterNumber__c;
            FEDEXLIMIT = Integer.valueOf(config.Max_Number_allowed__c) ;
            FEDEXINTEGRATORID =config.FedEx_IntegratorId__c ;
            FEDEXTRACKTYPE =config.Package_Identifier__c ; 
            MAX_TIME =  Integer.valueOf(config.Max_Wait__c);

        } 
    }
    
    //
    // This method will prepare the request body and invoke the service 
    //
    public pagereference TrackFedExRequest(List <OrderItem> orderItemList) {
        try{
            // Preparing the request 
            NV_FedExTrackingService.WebAuthenticationDetail WebAuthenticationDetail = new NV_FedExTrackingService.WebAuthenticationDetail();
            NV_FedExTrackingService.WebAuthenticationCredential UserCredential = new NV_FedExTrackingService.WebAuthenticationCredential();
            // setting credentials 
            UserCredential.Key = FEDEXKEY;
            UserCredential.Password = FEDEXPASSWORD;
            WebAuthenticationDetail.UserCredential = UserCredential;
            NV_FedExTrackingService.ClientDetail ClientDetail = new NV_FedExTrackingService.ClientDetail();
            ClientDetail.AccountNumber = FEDEXACCOUNTNUMBER;
            ClientDetail.MeterNumber = FEDEXMETERNUMBER;
            ClientDetail.IntegratorId = FEDEXINTEGRATORID;
            NV_FedExTrackingService.VersionId Version = new NV_FedExTrackingService.VersionId();
            Version.ServiceId = 'trck';
            Version.Major = 10;
            Version.Intermediate = 0;
            Version.Minor = 0;
            List < NV_FedExTrackingService.TrackSelectionDetail > SelectionDetails = new List < NV_FedExTrackingService.TrackSelectionDetail > ();

            /*
            List <OrderItem> orderItemList; // will hold the orderitem 
            // invoked from scheduler 
            if (orderList == NULL || orderList.size() == 0) {
                orderItemList = [SELECT Id, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c,Order.OwnerId FROM OrderItem WHERE Order_Delivered__c = false AND Tracking_Number__c != NULL];
            } 
            // invoked from order page or elsewhere 
            else {
                List <String> orderIds = new List <String>();
                for (Order currentOrder: orderList) {
                    orderIds.add(currentOrder.Id);  // collecting the Id's         
                }
                // getting the data 
                orderItemList = [SELECT Id, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c,Order.OwnerId FROM OrderItem WHERE Order_Delivered__c = false AND Tracking_Number__c != NULL AND OrderId IN: orderIds];
            }
            */
            Integer j = 0;
            Integer i = 0;

            Map<Id, OrderItem> orderItemsToBeUpdated = new Map<Id, OrderItem>();
			Integer calloutCount = 0;
            // If any value exists 
            System.debug(LoggingLevel.WARN, 'orderItemList Size  : '+ orderItemList.size());
            if (orderItemList != NULL && orderItemList.size() > 0) {
                
                Set < String > uniqueTrackingNumbers = new Set < String > (); // this will ensure one tracking number invoked only once 
                for (OrderItem tempOrderItem: orderItemList) {

                    System.debug(LoggingLevel.INFO, 'OrderItem : '+ j++);

                    // Creating the map with trackingnumber to order item will be used during updation of record 
                    //Logic for S-371704 : Replacing Tracking Number with Tracked Number
                    if(orderItemMap.get(tempOrderItem.Tracked_Number__c) == NULL){
                        List<OrderItem> itemList = new List<OrderItem>();
                        itemList.add(tempOrderItem);
                        orderItemMap.put(tempOrderItem.Tracked_Number__c,itemList);
                    }
                    else{
                        List<OrderItem> itemList = orderItemMap.get(tempOrderItem.Tracked_Number__c);
                        itemList.add(tempOrderItem);
                        orderItemMap.put(tempOrderItem.Tracked_Number__c,itemList);
                    }
                    // end of map creation 
                    // as we are looping for each 30 tracking item checking if callout limit exists if not returning from method 
                    if(calloutCount >= CALLOUT_MAX){
                        break;
                    }
                    // removing duplication of tracking number 
                    if (!uniqueTrackingNumbers.contains(tempOrderItem.Tracked_Number__c)) {
                        // preparing request 
                        NV_FedExTrackingService.TrackSelectionDetail senctionDetail = new NV_FedExTrackingService.TrackSelectionDetail();
                        NV_FedExTrackingService.TrackPackageIdentifier PackageIdentifier = new NV_FedExTrackingService.TrackPackageIdentifier();
                        PackageIdentifier.Type_X = FEDEXTRACKTYPE;
                        PackageIdentifier.Value = tempOrderItem.Tracked_Number__c;
                        senctionDetail.PackageIdentifier = PackageIdentifier;
                        // in case of reached the fedex limit for per call tracking number calling the service also checking the limit of salesforce 
                        if(SelectionDetails.size() == FEDEXLIMIT && Limits.getCallouts() != Limits.getLimitCallouts()){
                            System.debug(LoggingLevel.WARN,'[DD] : In Condition - SelectionDetails Size - ['+ i++ +'] - ' + SelectionDetails.size());
                            NV_FedExTrackingService.TransactionDetail TransactionDetail = new NV_FedExTrackingService.TransactionDetail();
                            NV_FedExTrackingService.TrackServicePort service = new NV_FedExTrackingService.TrackServicePort();
                            List < String > process = new List < String > ();
                            // invoking the service and sending it for processing 
                            calloutCount++;
                            System.debug('Callout #: ' + calloutCount);
                            List<OrderItem> toAdd = parseResponse(service.track(WebAuthenticationDetail, ClientDetail, TransactionDetail, Version, SelectionDetails, MAX_TIME, process));
                            for(OrderItem o : toAdd){
                                if(!orderItemsToBeUpdated.containsKey(o.Id)){
                                    if(orderItemsToBeUpdated.values().size() < Limits.getLimitDMLRows() - Limits.getDMLRows()){
                                        orderItemsToBeUpdated.put(o.Id, o);
                                    }
                                }
                            }
                            SelectionDetails = new List < NV_FedExTrackingService.TrackSelectionDetail > (); // clearing the list 
                            continue; // skipping other part of the loop.
                        }
                        SelectionDetails.add(senctionDetail);
                    }
    
                }
            }
            System.debug(LoggingLevel.WARN,'[DD] : Outside Loop - SelectionDetails Size - ['+ i++ +'] - ' + SelectionDetails.size());
            // in case of less or greater than fedex limit this will be invoked to get data for rest of tracking numbers 
            NV_FedExTrackingService.TransactionDetail TransactionDetail = new NV_FedExTrackingService.TransactionDetail();
            NV_FedExTrackingService.TrackServicePort service = new NV_FedExTrackingService.TrackServicePort();
            List < String > process = new List < String > ();
            List<OrderItem> toAdd = parseResponse(service.track(WebAuthenticationDetail, ClientDetail, TransactionDetail, Version, SelectionDetails, MAX_TIME, process));
            for(OrderItem o : toAdd){
                if(!orderItemsToBeUpdated.containsKey(o.Id)){
                    if(orderItemsToBeUpdated.values().size() < Limits.getLimitDMLRows() - Limits.getDMLRows()){
                        orderItemsToBeUpdated.put(o.Id, o);
                    }
                }
            }
        	System.debug('Number of Order Items to be updated: ' + orderItemsToBeUpdated.values().size());
        	System.debug('Number of Callouts Total: ' + calloutCount);
            if(orderItemsToBeUpdated.values().size() > 0){
                update orderItemsToBeUpdated.values();
            }
        }
        catch(Exception e){
            // in case of any error adding a record to exception object 
            FedEx_Service_Error_Log__c exceptionLog = new FedEx_Service_Error_Log__c(Error_Text__c = e.getMessage());
            insert exceptionLog;
        }
        
        return null;
    }
    //
    // This method will parse the data 
    //
    public List<OrderItem> parseResponse(NV_FedExTrackingService.TrackReply response) {
        System.debug('Order Items:');
        System.debug(orderItemMap);
        
        List<OrderItem> toBeUpdated = new List<OrderItem>();
        // in case of success response 
        if (response != NULL && response.HighestSeverity == 'SUCCESS') {
            for (NV_FedExTrackingService.CompletedTrackDetail completedDetail: response.CompletedTrackDetails) {
                // status of tracking number 
                if (completedDetail.HighestSeverity == 'SUCCESS') {
                    Boolean isDelivered = false;
                    for (NV_FedExTrackingService.TrackDetail trackDetail: completedDetail.TrackDetails) {
                        NV_FedExTrackingService.Notification Notification = trackDetail.Notification;
                        NV_FedExTrackingService.TrackStatusDetail StatusDetail = trackDetail.StatusDetail;
                        // invalid tarcking number 
                        if(Notification <> NULL && Notification.Severity != 'SUCCESS'){
                            TrackingMapForOther.put(trackDetail.TrackingNumber, Notification.Message);                          
                        }
                        // delivered package 
                        if (StatusDetail != null && StatusDetail.Description == 'Delivered') {
                            deliveredPackages.add(trackDetail.TrackingNumber);
                            
                            //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - Start
    						//Check for already contained Key
                            if(!mapFedExTrackDetails.containsKey(trackDetail.TrackingNumber)) {
                            	
                            	//populate map
                            	mapFedExTrackDetails.put(trackDetail.TrackingNumber, trackDetail);
                            }
                            //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - End
    
                            isDelivered = true;
                            break; 
                        }
                    }
                    // in case of delivered takeing the topmost response to get the status 
                    if (!isDelivered 
                            && completedDetail != NULL && completedDetail.TrackDetails != NULL 
                            && completedDetail.TrackDetails.size() > 0 && completedDetail.TrackDetails[0].StatusDetail != NULL 
                            && completedDetail.TrackDetails[0].StatusDetail.Description!= NULL){ 
                            TrackingMapForOther.put(completedDetail.TrackDetails[0].TrackingNumber, 
                                completedDetail.TrackDetails[0].StatusDetail.Description);
                        
                    }
                }
            }
           
            // Updating records 
            for(String trackingNumber : orderItemMap.keySet()){
                List<OrderItem> tempItems = orderItemMap.get(trackingNumber); // getting the items 
                if(deliveredPackages.contains(trackingNumber)){
                    // assigning values 
                    for(OrderItem oItem : tempItems){
                        oItem.Order_Delivered__c = true;
                        //[DD] Adding condition to update status to Delivered
                        oItem.Status__c = 'Delivered';
                        oItem.Tracking_Information__c = 'Delivered';
                        
                        //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - Start
    					//Check for already contained key
                        if(mapFedExTrackDetails.containsKey(trackingNumber) && mapFedExTrackDetails.get(trackingNumber).ActualDeliveryTimestamp != null) {
                        	
                        	//Assign value to Delivered_Date__c field with ActualDeilveryTimestamp
                        	oItem.Delivered_Date__c = mapFedExTrackDetails.get(trackingNumber).ActualDeliveryTimestamp.date();
                        }
                        //Code modified - Padmesh Soni (06/16/2016 Appirio Offshore) - S-421189 - End
    
                        toBeUpdated.add(oItem);
                    }
                    
                }
                // failure 
                else if(TrackingMapForOther.get(trackingNumber) <> NULL){
                    for(OrderItem oItem : tempItems){
                        oItem.Tracking_Information__c = TrackingMapForOther.get(trackingNumber);
                        toBeUpdated.add(oItem);
                    }
                }
            }
            
            /*
            if(toBeUpdated.size() > 0){
                update toBeUpdated; // updating records 
                //
                //Set<String> trackPosted = new Set<String>();
                //// posting only once for a tracking number 
                //for(OrderItem oItem : toBeUpdated){
                //  if(!trackPosted.contains('oItem.Tracking_Information__c') && !Test.isRunningTest()){
                //      NV_ChatterUtils.mentionTextPost(userInfo.getUserId(),oItem.Order.OwnerId, ' Status for tracking number '+ oItem.Tracking_Number__c + ' is ' + oItem.Tracking_Information__c);
                //  }
                //}
                //
            }
            */
        }
        return toBeUpdated;
        
        /*
        else{
            // in case of any error adding a record to exception object 
                if(response != NULL && response.Notifications !=NULL && response.Notifications.size() > 0){
                    List<NV_FedExTrackingService.Notification> Notification = response.Notifications;
                    FedEx_Service_Error_Log__c exceptionLog = new FedEx_Service_Error_Log__c(Error_Text__c = response.Notifications[0].Message);
                    insert exceptionLog; // catch block at top method will handle any exception     
                }
        }
        */
    }
    

    //Method for Scehdular (As you can't call Callout From Schedlar Method)
    @future(callout=true)
    public static void processOrderItemsForFedEx(){   
        
         Integer limitValue = Limits.getLimitQueryRows() - Limits.getQueryRows();
         if(limitValue > 10000)
            limitValue = 10000;
         //START JSchilling Case 169518 6/02/2016
         //Added code below to allow the query to only look at records so many days in the past, which is driven by the Label
         Integer days = Integer.valueOf(Label.Last_n_days_for_FedEx_Job);
         Date dateForQuery = System.today() - days;
         //END JSchilling Case 169518 6/02/2016

         //JSchilling Case 169518 6/02/2016
         //Added Shipped_Date__c condition to below query
         //Logic for S-371704 : Replacing Tracking Number with Tracked Number
         new NV_FedExRequest().TrackFedExRequest([SELECT Id, Tracked_Number__c, Tracking_Number__c, Order_Delivered__c, Tracking_Information__c, Order.EffectiveDate 
												  FROM OrderItem 
												  WHERE Order_Delivered__c = false 
                                                  AND Tracked_Number__c != NULL 
                                                  AND Shipped_Date__c >= :dateForQuery
                                                  AND (NOT Tracking_Information__c LIKE '%Invalid%') 
                                                  //AND (NOT Tracking_Information__c LIKE '%cannot be found%')
                                                  ORDER BY Tracking_Information__c NULLS FIRST, Order.EffectiveDate DESC
                                                  LIMIT :limitValue]);
    }
    
}