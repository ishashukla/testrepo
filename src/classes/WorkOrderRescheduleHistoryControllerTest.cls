/**================================================================      
* Appirio, Inc
* Name: WorkOrderRescheduleHistoryControllerTest
* Description: [Test class for WorkOrderRescheduleHistoryController]
* Created Date: [23-11-2016]
* Created By: [Meha Simlote] (Appirio)
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest()
private class WorkOrderRescheduleHistoryControllerTest {
    @isTest static void testone(){
        Test.startTest();
        WorkOrderRescheduleHistoryController con = new WorkOrderRescheduleHistoryController();
        List<SVMXC__Service_Order__History> WoHistory= con.WoHistory;
        system.assertEquals(WoHistory!=null, true);
        Test.stopTest();
    }
}