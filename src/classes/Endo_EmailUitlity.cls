/*******************************************************************************
* Author        :  Appirio JDC
* Date          :  27 June 2016
* Description   :  Pratibha Chimppa   Original (T-497178)
* 02 Aug 2016    Pratibha Chhimpa   T-523890

* 02 Aug 2016     Pratibha Chhimpa     T-523890
* 05 Aug 2016     Parul Gupta          I-227221 Reviewed and refactored code
* 17 Sept 2016    Nathalie Le Guay     Remove references to Serial_Numbers__c, which is no longer planned
* 18 Sept 2016    Parul Gupta          I-235763 Updated getEmailBodyForConsumer() to refer quantity and product Name 
                                       instead of Quantity Shipped and Description
* 06 Oct  2016    Parul Gupta          T-542929 | Refer new field "Tracking_Numbers__c"              
 ==================================================================*/
public class Endo_EmailUitlity {
    // Constructor  of Email Utility 
    public Endo_EmailUitlity(){}
    
    //  Return HTML body required to display Email when Item is not returned yet
    //  @param String Name of Contact, Selected OrderItems to show in table
    //  @return a String of HTML Body.    
    public String getEmailBodyForConsumer(String imageUrl, String Name, List<OrderItem> orderItem){
                
        // Creating HTML Body
        String body = '<html>'+
                                '<head> <style> table { width: 100%; border-collapse: collapse;border: 1px solid black}' +
                                'th, td {  text-align: left;  padding: 8px;border: 1px solid black;}' +
                                'tr:nth-child(even){background-color: #f2f2f2;}' +
                                'th { background-color: #717671; color: white; } </style> </head>' +
                                
                                // Displaying Email Logo and Header of Email
                                '<body>'+
                                '<img src= "'+ imageURL + '" alt="Mountain View" style="width:216px;height:58px;;">' +
                                '<p>' + Label.Dear + ' ' +  Name + ',' + '</p>'+
                                '<p>' +  Label.Email_Template_Customer + '</p>' +
                                
                                // Starting Table and HardCoding Table's Headers
                                '<table> <tr>'+
                                '<th>Account Name</th>' +
                                '<th>Account Number</th>' +
                                '<th>Order Number</th>' +
                                '<th>Order Type</th>' +
                                '<th>PO Number</th>' +            
                                '<th>Part Number</th>' +
                                '<th>Description</th>' +
                                // PG commenting out as serial number is no longer planned
                                //'<th>Serial Number</th>' +
                                '<th>Quantity</th></tr>';
            
            
        // Displaying Table Row's with retrieved OrderItems
        // Number of Column are equal to orderItem.size()
        for(OrderItem oliFirst : orderItem){
            body +='<tr> <td>'+ (oliFirst.Order.Account.Name == null ? '' : oliFirst.Order.Account.Name) +'</td>'+ 
                        '<td>' +(oliFirst.Order.Account.AccountNumber == null ? '' : oliFirst.Order.Account.AccountNumber) +'</td>'+
                        '<td>' +(oliFirst.Order.OrderNumber == null ? '' : oliFirst.Order.OrderNumber) +'</td>'+
                        '<td>' +(oliFirst.Order.Type == null ? '' : oliFirst.Order.Type) +'</td>'+
                        '<td>' + (oliFirst.Order.PoNumber == null ? '' : oliFirst.Order.PoNumber) +'</td>'+
                         '<td>' + (oliFirst.PartNo__c == null ? '' : oliFirst.PartNo__c)+ '</td>'+
                         // I-235763 PG (Appirio) replaced Description and Quantity Shipped
                         // with Product name and Quantity                        
                         '<td>' + (oliFirst.PriceBookEntryId == null ? '' : oliFirst.PriceBookEntry.Name) +'</td>'+
                         '<td>' + (oliFirst.Quantity == null ? 0 : oliFirst.Quantity) +'</td>'
                         +'</tr>';
        }
        body +=' </table>';
        
        // Displaying Table Footer 
        body += '<p>' +  Label.Email_Bottom_Template_Customer + '</p>';
        body += '<p>' +  Label.Email_Bottom_Template_Address+ '</p>';
        body +=' </table></body></html>';
        
        system.debug('Endo_SendEmailtoOrderContact - Email Body ::' + body); 
        
        if(body != null){
            return body; 
        }
        return 'No Order Items are pending ' ;
    }

    //  Return HTML body required to display Email when Item is Shipped
    //  @param String Name of Contact, Selected OrderItems to show in table
    //  @return a String of HTML Body.  

    public String getEmailBody(String imageURL, String Name, List<OrderItem> orderItem){
        
        // Creating HTML Body
        String body = '<html>'+
                '<head> <style> table { border: 1px solid black; width: 100%;border-collapse: collapse; }' +
                'th, td {  text-align: left;  padding: 8px; border: 1px solid black;}' +
                'tr:nth-child(even){background-color: #f2f2f2}' +
                'th { background-color: #717671; color: white; } </style> </head>' +
                '<body>'+
                
                // Displaying Email Logo and Header of Email
                '<img src= "'+ imageURL + '" style="width:216px;height:58px;">' +
                '<p>' + Label.Dear + ' ' +  Name + '</p>'+
                '<p>' +  Label.Email_Template_Contact + '</p>' +
                //'<p>' +  Label.Template_Body + '</p>' +
                // Removed second paragraph because of email template change - D.Saldana
                
                // Starting Table and HardCoding Table's Headers
                '<table> <tr>'+
                '<th>Account Name</th>' +
                '<th>Account Number</th>' +
                '<th>Order Number</th>' +
                '<th>Part Number</th>' +
                '<th>Description</th>' +
                '<th>QTY Shipped</th>' +
                '<th>QTY Ordered</th>' +
                '<th>Unit of Mearsurment</th>' +
                '<th>Shipping Method</th> ' +
                '<th>Tracking #</th>' +
                '<th>PO Number</th></tr>';
                 
                
        // Displaying Table Row's with retrieved OrderItems
        // Number of Rows are equal to orderItem.size()  
        for(OrderItem oliFirst : orderItem){
        //T-523890 Pratibha|Appirio
            body +='<tr> <td>'+ (oliFirst.Order.Account.Name == null ? '' : oliFirst.Order.Account.Name) +'</td>'+ 
                        '<td>' +(oliFirst.Order.Account.AccountNumber == null ? '' : oliFirst.Order.Account.AccountNumber) +'</td>'+
                        '<td>' +(oliFirst.Order.OrderNumber == null ? '' : oliFirst.Order.OrderNumber) +'</td>'+
                        '<td>'+ (oliFirst.PartNo__c == null ? ' ' : oliFirst.PartNo__c) +'</td>'+ 
                         '<td>' + (oliFirst.PriceBookEntry.Product2.Name == null ? '' : oliFirst.PriceBookEntry.Product2.Name) +'</td>'+
                         '<td>' + (oliFirst.Quantity_Shipped__c == null ? 0 : oliFirst.Quantity_Shipped__c) +'</td>'+
                         '<td>' + (oliFirst.Quantity == null ? 0 : oliFirst.Quantity) +'</td>'+
                         '<td>' + (oliFirst.Unit_of_Measure__c == null ? '' : oliFirst.Unit_of_Measure__c) + '</td>'+
                         '<td>' + (oliFirst.Shipping_Method__c == null ? '' : oliFirst.Shipping_Method__c) + '</td>'+
                         '<td>' + (oliFirst.Tracking_Numbers__c== null ? '' : oliFirst.Tracking_Numbers__c) + '</td>'+
                         '<td>' + (oliFirst.Order.PoNumber == null ? '' : oliFirst.Order.PoNumber) + '</td>'+
                         '</tr>';
        }
        body +=' </table>';
         // Displaying Table Footer 
         
        body += '<p>' +  Label.Email_Address_Contact + '</p>';
        body += '<p>' + '</p>';
        //body += '<p>' +  Label.Email_Bottom_Template_Contact+ '</p>';
        // Removed second paragraph because of email template change - D.Saldana
        body +=' </table></body></html>';
            
        system.debug('Endo_SendEmailtoOrderContact - Email body' + body); 
        if(body != null){
            return body; 
        }
        return 'No Order Items are shipped' ;
    }
    
    // 
    //  Returns endo email logo
    //  @param 
    //  @return a String of url.  
    public String getEndoEmailLogo(){
            String imageURL = '';
            if(!System.Test.isRunningTest()){
                    for(Document sr : [SELECT Id, NamespacePrefix, SystemModstamp 
                                            FROM Document 
                                            WHERE DeveloperName =:Label.Endo_Email_Logo LIMIT 1]){
                            imageURL = System.URL.getSalesforceBaseUrl().toExternalForm() 
                                            + '/servlet/servlet.ImageServer?id=' 
                                            + sr.id 
                                            + '&oid=' 
                                            + UserInfo.getOrganizationId() 
                                            + '&lastMod=' 
                                            + sr.SystemModstamp.getTime() ;                 
                    }
            }       
        return imageURL;
    }
    
     // 
    //  Returns email message
    //  @param Email Subject, Email Body, Target Object Id, List of to email addresses
    //  @return Messaging.SingleEmailMessage 
    public Messaging.SingleEmailMessage createEmailMessage(String subject, String emailBody, String htmlEmailBody, 
                                                                            List<String> toEmailAddresses, String targetObjId,
                                                                            boolean setSaveAsActivity){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSaveAsActivity(setSaveAsActivity);
            mail.setTargetObjectId(targetObjId);
            mail.setToAddresses(toEmailAddresses);  
            mail.setSubject(subject);
            mail.setBccSender(false);
            mail.setUseSignature(false);            
            if (emailBody != null){
                mail.setPlainTextBody(emailBody);
            }
            if (htmlEmailBody != null){
                mail.setHtmlBody(htmlEmailBody);
            }
            return mail;
    }
}