/**================================================================      
* Appirio, Inc
* Name: ProjectPlanTriggerHandler
* Description: T-490786 (To create project share records)
* Created Date: 26-Apr-2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By         Description of the update
*                   Deepti Maheshwari   (Send project records to EBS on creation Ref : T-502086)
*                   Meghna Vijay        (Send email notification to project team on completed stage Ref: T-516054)
* 19th Aug 2016     ralf hoffmann       (changed start_date__c to Project_start_date__c)
* 22nd Aug 2016     Shubham Dhupar      (Used to update project phase milestone to Closed when Project stage 
*                                        is closed only if all phases were Complete or Installation Complete Ref: I-230042)
* 16th September 2016    Nitish Bansal      Update (I-235018) // Added query execution time calculation logic
* 13th October 2016     Deepti Maheshwari   (Fix for I-237877, physical address on plan to be combination of all comm shipping
*                                           address Fields)
==================================================================*/

public class ProjectPlanTriggerHandler {
    Static Datetime qryStart, qryEnd;
    public static void beforeInsert(List<Project_Plan__c> newList) {
      updatePhysicalAddress(newList,null);
    }
    public static void beforeUpdate(List<Project_Plan__c> newList,Map<Id,Project_Plan__c> oldMap) {
      updatePhysicalAddress(newList,oldMap);
    }
    //================================================================      
    // Name: afterInsert
    // Description: afterInsert Method ProjectPlan( T-502086)
    // Created By: Deepti Maheshwari (Appirio)
    //==================================================================
    public static void afterInsert(List<Project_Plan__c> newProjectList) {
      createProjectsInEBS(newProjectList);
      
    }
   
    //================================================================      
    // Name: afterInsert
    // Description: afterUpdate method of ProjectPlanTrigger( T-502086)
    // Created By: Deepti Maheshwari (Appirio)
    //==================================================================
    public static void afterUpdate(List<Project_Plan__c> newProjectList, Map<Id,Project_Plan__c> oldMap){
      updateProjectsInEBS(newProjectList, oldMap);
      sendEmailNotification(newProjectList,oldMap);
      populateCustomerSignerOnPhase(newProjectList,oldMap);
      closePhaseOnProjectClose(newProjectList,oldMap); // SD 8/22: [I-230042]
     
    }
    private static void updatePhysicalAddress(List<Project_Plan__c> newList, Map<Id,Project_Plan__c> oldMap){
      System.debug('Entry ' + newList.get(0).Account__r.Name);
      Set<Id> accountIdSet = new Set<Id> ();
      Boolean isUpdate = oldMap != null ? true : false;  
      for(Project_Plan__c plan : newList) {
        if(!isUpdate ||plan.Account__c !=  oldMap.get(plan.Id).Account__c) {
          if(plan.Account__c != NULL) {
            accountIdSet.add(plan.Account__c);
          }
          else {
            plan.Physical_Address__c = NULL;
          }
        }
      }
      Map<Id,String> addressAccountMap = new Map<Id,String>();
      String accountAddress = '';
      if(!accountIdSet.isEmpty()) {
        //DM Fix for I-237877 - Physical Address to be a combination of all shipping address fields
        for(Account acc : [SELECT id,COMM_Shipping_Address__c,COMM_Shipping_City__c,
                           COMM_Shipping_Country__c, COMM_Shipping_PostalCode__c, 
                           COMM_Shipping_State_Province__c
                           FROM Account 
                           WHERE id in:accountIdSet]) {
          accountAddress = '';
          if(acc.COMM_Shipping_Address__c != null){
            accountAddress = acc.COMM_Shipping_Address__c;
          }
          if(acc.COMM_Shipping_City__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_City__c;
          }
          if(acc.COMM_Shipping_State_Province__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_State_Province__c;
          }
          if(acc.COMM_Shipping_PostalCode__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_PostalCode__c; 
          }
          if(acc.COMM_Shipping_Country__c != null){
            accountAddress += ' ' + acc.COMM_Shipping_Country__c; 
          }
          addressAccountMap.put(acc.id,accountAddress);                                           
        }
      }
      if(!addressAccountMap.isEmpty()) {
        for(Project_Plan__c plan : newList) {
          plan.Physical_Address__c = addressAccountMap.get(plan.Account__c);
        }
      }
    }
    //==================================================================================================    
    // Name:        closePhaseOnProjectClose
    // Description: Used to update project phase milestone to Closed when Project stage 
    //              is closed only if all phases were Complete or Installation Complete     (I-230042)
    // Created By:  Shubham Dhupar (Appirio)
    //===================================================================================================
    private static void closePhaseOnProjectClose(List<Project_Plan__c> newList, Map<Id,Project_Plan__c> oldMap){
      Set<Id> projectids = new Set<Id>();
      for(Project_plan__c plan : newList) {
        if(plan.Stage__c == 'Closed' && plan.Stage__c !=  oldMap.get(plan.Id).Stage__c)
        projectids.add(plan.id);
      }
      List<Project_Phase__c> phaseList = new List<Project_Phase__c>();
      Boolean allPhase = true;
      if(projectids.size() > 0){
        qryStart = datetime.now();  
        for(Project_Phase__c phase: [Select id,Milestone__c,Project_Plan__c 
                                      From Project_phase__c
                                      Where Project_Plan__c in:projectids]) {
          
          if(phase.Milestone__c != 'Installation Complete') {
            allPhase = false;
          }
          else {
              phase.Milestone__c = 'Closed';
              phaseList.add(phase);
          }
        }
        qryEnd = datetime.now();
        //debug number of seconds the query ran
      }
      
      if(allPhase = true && phaseList.size() > 0) {
        update phaseList;
      }  
    }
    
    //================================================================      
    // Name: createProjectsInEBS
    // Description: Used to create projects in EBS( T-502086)
    // Created By: Deepti Maheshwari (Appirio)
    //==================================================================
    private static void createProjectsInEBS(List<Project_Plan__c> newProjectList){
        List<Project_Plan__c> ppsToSync = new List<Project_Plan__c>();
      for(Project_Plan__c pp : newProjectList){
        if(pp.Oracle_Project_Status__c  == Constants.CONST_APPROVED){
          ppsToSync.add(pp);
        }
      }
      if(ppsToSync.size() > 0){
        syncProjectsToEBS(ppsToSync);
      }     
    }
   
    //================================================================      
    // Name:          populateCustomerSignerOnPhase
    // Description:   populateCustomerSignerOnPhase( I-228995)
    // Created By:    Shubham Dhupar(Appirio)
    //==================================================================
    
    private static void populateCustomerSignerOnPhase(List<Project_Plan__c> newList, Map<ID, Project_Plan__c> oldMap) {
      Boolean isUpdate = oldMap != null ? true : false;  
      Map<id,id> projectIds = new Map<id,String>();                                                     
      for(Project_Plan__c plan : newList) {
        if(!isUpdate || plan.Customer_Signer__c !=  oldMap.get(plan.Id).Customer_Signer__c) {
          projectIds.put(plan.id,plan.Customer_Signer__c);
          System.debug('1. Project Plan        '+ plan.id + '      Customer_Signer              '+ plan.Customer_Signer__c );
        }
      }
      List<Project_Phase__c>phaseList = new List<Project_phase__c>();
      if(projectIds.size() > 0){
         qryStart = datetime.now();
        for(Project_Phase__c phase : [Select id,project_plan__c,Customer_Signer__c
                                        From Project_Phase__c
                                        where Project_Plan__c in : projectIds.keyset()]) {
          System.debug('2.phase plan           '+ phase.Project_plan__c);
    
            phase.Customer_Signer__c = projectIds.get(phase.project_plan__c);  
          
          System.debug('3. phase Signer           '+ phase.Customer_Signer__c);
          phaseList.add(phase);
        }
        qryEnd = datetime.now();
        //debug number of seconds the query ran
      }
     
      if(phaseList.size() > 0){
        update phaseList;
      }
    }
    //================================================================      
    // Name: updateProjectsInEBS
    // Description: Used to create projects in EBS( T-502086)
    // Created By: Deepti Maheshwari (Appirio)
    //==================================================================
    private static void updateProjectsInEBS(List<Project_Plan__c> newProjectList, Map<ID, Project_Plan__c> oldMap){
        List<Project_Plan__c> newProjectListTosync = new List<Project_Plan__c>();
      for(Project_Plan__c project : newProjectList){
        if(project.Oracle_Project_Status__c != Constants.CONST_NOTAPPROVED){
          if(project.Project_Start_Date__c != oldMap.get(project.id).Project_Start_Date__c
          || project.Completion_Date__c != oldMap.get(project.id).Completion_Date__c
          || project.Project_Manager__c != oldMap.get(project.id).Project_Manager__c
          || project.Long_Name__c != oldMap.get(project.id).Long_Name__c
          || project.Oracle_Project_Status__c != oldMap.get(project.id).Oracle_Project_Status__c
          || project.Name != oldMap.get(project.id).Name){
            newProjectListTosync.add(project);
          }
        }
      }
      if(newProjectList.size() > 0){
        syncProjectsToEBS(newProjectListTosync);
      }
    }
    
    //================================================================      
    // Name: syncProjectsToEBS
    // Description: Used to create projects in EBS( T-502086)
    // Created By: Deepti Maheshwari (Appirio)
    //==================================================================
    private static void syncProjectsToEBS(List<Project_Plan__c> projectsToSync){
      //SchemasStrykerCOMMproject.ProjectsType projectsList = new SchemasStrykerCOMMproject.ProjectsType();
      //NB - 05/23 - Skipping call out for test class
      Set<Id> projectPlanIds = new Set<ID>();
      if(!test.isrunningtest()) {
        //callout
        for(Project_Plan__c project : projectsToSync){
          projectPlanIds.add(project.id);
        }
        if(projectPlanIds.size() > 0){
          WebServiceHelper.asyncExecuteProjectCreationService(projectPlanIds);
        }
      }
    }
    
    //================================================================      
    // Name: shareProjectRecords
    // Description: Used to share Projects wih the account owner and acount team
    // Created Date: [04/26/2016]; T-490786
    // Created By: Nitish Bansal (Appirio)
    //==================================================================
    public static void shareProjectRecords(List<Project_Plan__c> newList , Map<Id,Project_Plan__c> oldMap) {
        List<String> newRecord = new List<String>();
        List<String> deleteRecord = new List<String>();
        Set<Id> accId = new Set<Id>();
        Set<Id> oldAccId = new Set<Id>();
        for(Project_Plan__c project : newList) {
            //insert scenario
            if(oldMap == null && project.Account__c != null) {
                newRecord.add(project.Account__c + '+' + project.Id);
                accId.add(project.Account__c);
            } 
            //update scenario
            else if (oldMap != null) {
                if(project.Account__c != oldMap.get(project.Id).Account__c && project.Account__c != null){
                    newRecord.add(project.Account__c + '+' + project.Id);
                    deleteRecord.add(oldMap.get(project.Id).Account__c + '+' + project.Id);
                    accId.add(project.Account__c);
                    oldAccId.add(oldMap.get(project.Id).Account__c);
                }
            }
        }
        List<Account> accountList = new List<Account>();
        Map<Id,Id> accountToOwnerMap = new Map<Id,Id>();
        Map<Id,Set<Id>> accountToCustomAccountTeamMember = new Map<Id,Set<Id>>();
        List<Custom_Account_Team__c> customAccontTeamMemberList = new List<Custom_Account_Team__c>();
        if(accId.size() > 0){
          accountList = [SELECT OwnerId FROM Account WHERE Id IN :accId];
          //Querying PM role account team members
          qryStart = datetime.now();
          customAccontTeamMemberList = [Select User__c, Account__c From Custom_Account_Team__c 
                                          WHERE (Team_Member_Role__c = :Constants.ROLE_PM_SERVICES OR Team_Member_Role__c = :Constants.ROLE_PM) 
                                          AND Account__c IN:accId
                                          AND User__r.isActive = true]; //Checking for active users
          qryEnd = datetime.now();
          //debug number of seconds the query ran
          system.debug('customAccontTeamMemberList execution time ' + (qryEnd.getTime() - qryStart.getTime()) /1000);                                
        }
        for(Account acc : accountList) {
            accountToOwnerMap.put(acc.Id, acc.OwnerId);
        }
        
        for(Custom_Account_Team__c member : customAccontTeamMemberList){
            if(!accountToCustomAccountTeamMember.containsKey(member.Account__c)){
                accountToCustomAccountTeamMember.put(member.Account__c, new Set<Id>());
            }
            accountToCustomAccountTeamMember.get(member.Account__c).add(member.User__c);
        }
        System.debug('>>>>>>'+accountToCustomAccountTeamMember);

        //inserting new  share records
        if(newRecord.size() > 0) {
            List<Project_Plan__Share> projectShareList = new List<Project_Plan__Share>();
            for(String accKey : newRecord) {
                List<String> tempList = new List<String>();
                tempList = accKey.split('\\+');
                Id accountOwner = accountToOwnerMap.get(templist.get(0));
                //sharing with account owner
                if(accountOwner != UserInfo.getUserId()){
                    projectShareList.add(createProjectShare(accountOwner, templist.get(1)));
                }
                Set<Id> teamMembers = new Set<Id>();
                if(accountToCustomAccountTeamMember.containsKey(templist.get(0))){
                    teamMembers = accountToCustomAccountTeamMember.get(templist.get(0));
                    //sharing with account team members
                    for(Id user : teamMembers){
                        if(user != accountOwner && user != UserInfo.getUserId()){   
                            projectShareList.add(createProjectShare(user,templist.get(1)));
                        }
                    }
                }
            }
            Database.insert(projectShareList,false);
        }

        //deleting old sharing records
        Set<Id> accOwnerIdToDelSet = new Set<Id>();
        Set<Id> userIdToDelSet = new Set<Id>();
        if(deleteRecord.size() > 0) {
          if(oldAccId.size() > 0){
            accountList = new List<Account>([SELECT OwnerId FROM Account WHERE Id IN :oldAccId]);
          }
            
            accountToOwnerMap = new Map<Id,Id>();
            for(Account acc : accountList) {
                accountToOwnerMap.put(acc.Id, acc.OwnerId);
            }
            for(String delRecord : deleteRecord) {
                List<String> tempList = new List<String>();
                tempList = delRecord.split('\\+');
                accOwnerIdToDelSet.add(accountToOwnerMap.get(templist.get(0)));
                userIdToDelSet.add(tempList.get(1));
            }
        }
        List<Project_Plan__Share> projectShareDeleteList = new List<Project_Plan__Share>();
        if(accOwnerIdToDelSet.size() > 0 && userIdToDelSet.size() > 0) {
            qryStart = datetime.now();
            projectShareDeleteList =[SELECT Id, ParentId, UserOrGroupId, RowCause 
                                    FROM Project_Plan__Share 
                                    WHERE RowCause = :Constants.ROW_CAUSE_MANUAL
                                    AND ParentId IN :userIdToDelSet 
                                    AND UserOrGroupId IN :accOwnerIdToDelSet];
            qryEnd = datetime.now();
            //debug number of seconds the query ran
        }
        if(projectShareDeleteList.size() > 0) {
            Database.delete(projectShareDeleteList, false);
        } 
    }

    //================================================================      
    // Name: createProjectShare
    // Description: Used to create Project share record instance
    // Created Date: [04/26/2016]; T-490786
    // Created By: Nitish Bansal (Appirio)
    //==================================================================
    public static Project_Plan__Share createProjectShare(Id userId, Id projId) {
        Project_Plan__Share projShare = new Project_Plan__Share();
        projShare.UserOrGroupId = userId;
        projShare.ParentId = projId;
        projShare.AccessLevel = 'Edit';
        projShare.RowCause = Constants.ROW_CAUSE_MANUAL;
        return projShare;
    }
    
    //================================================================      
    // Name: sendEmailNotification
    // Description: Used to send email notification to project team member
    // Created Date: [06/29/2016]; T-516054
    // Created By: Meghna Vijay (Appirio)
    //==================================================================
    private static void sendEmailNotification(List<Project_Plan__c> newProjectList, Map<Id, Project_Plan__c> oldMap) {
      Set<Id> projectPlanIdSet = new Set<Id>();
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
      //String baseURL = System.url.getSalesforceBaseUrl().gethost();
      //String body='';
      if(oldMap!=Null) {
        for(Project_Plan__c p : newProjectList) {
          if(p.Number_of_Phases__c != oldMap.get(p.Id).Number_of_Phases__c || p.Stage__c != oldMap.get(p.Id).Stage__c) {
            if(p.Number_of_Phases__c == 0 && p.Stage__c == 'Customer Sign Off Complete') {
              projectPlanIdSet.add(p.Id);
            }
          }
        }
        System.debug('projectPlanIdSet'+projectPlanIdSet);
      }
      if(projectPlanIdSet.size()>0) {
        Map<Id, List<String>> projectTeamMap = new Map<Id, List<String>>();
        List<String> userEmailSet = new List<String>();
        List<String> Addresses = new List<String>();
        qryStart = datetime.now();
        for(Custom_Account_Team__c c : [SELECT Project__c, User__r.email FROM Custom_Account_Team__c WHERE Project__c IN : projectPlanIdSet]) {
          userEmailSet.add(c.User__r.email);
          //projectTeamMap.put(c.Project__c,c.User__r.email);
        }
        qryEnd = datetime.now();
        //debug number of seconds the query ran
        qryStart = datetime.now();
        for(Custom_Account_Team__c c : [SELECT Project__c FROM Custom_Account_Team__c WHERE Project__c IN : projectPlanIdSet]) {
          projectTeamMap.put(c.Project__c,userEmailSet);
        }
        qryEnd = datetime.now();
        //debug number of seconds the query ran
        //System.debug('projectTeamMap'+projectTeamMap);
        if(!projectTeamMap.keyset().isEmpty()) {
           List<Contact> cnt = [SELECT ID, Email From Contact WHERE Email != Null LIMIT 1];
           List<EmailTemplate> e = [SELECT id FROM EmailTemplate WHERE DeveloperName=:Constants.PROJECT_PLAN_COMPLETED_TEMPLATE LIMIT 1];
          for(Id pId : projectTeamMap.keyset()) {
            mail = new Messaging.SingleEmailMessage();
            //body+=' <a href="' + baseURL + '/' + pId +  '">' +'</a> \n';
            mail.setTemplateId(e[0].id);
            mail.setWhatId(pId);
            mail.setTargetObjectId(cnt.get(0).Id);
            //system.debug('whatId'+pId);
            Addresses.addAll(projectTeamMap.get(pId));
            mail.setToAddresses(Addresses);
            mails.add(mail);
          }
         //System.debug('mails'+mails);
        }
        //System.debug('&&&&&&&&'+'Addresses.size'+Addresses.size());
        if(Addresses.size()>0) {
          Savepoint sp = Database.setSavepoint();
          Messaging.sendEmail(mails); // Dummy email send
          Database.rollback(sp); // Email will not send as it is rolled Back
          List<Messaging.SingleEmailMessage> msgListToBeSend = new List<Messaging.SingleEmailMessage>();
          for (Messaging.SingleEmailMessage email : mails) {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getToAddresses());
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());          
            emailToSend.setSubject(email.getSubject());
            msgListToBeSend.add(emailToSend);
          }
          //System.debug('msgListToBeSend'+msgListToBeSend);
          Messaging.sendEmail(msgListToBeSend);
        }
      }
    }
  }