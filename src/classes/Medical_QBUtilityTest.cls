/*
//
// (c) 2015 Appirio, Inc.
//
// Medical_CreateOrderExtensionsTest
//
// April 26, 2016, Sunil (Appirio JDC)
//
// Test class for Medical_CreateOrderExtensions
*/
@isTest
private class Medical_QBUtilityTest {
  static testMethod void testQBUtility(){
    Test.startTest();
      String xmlresponse = '<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">'+
                              '<?xml version="1.0" encoding="UTF-8"?><e2wSSOResponse><UniqueReqId>0163f9c6-9467-4509-ab9b-66d597874da1'+
                              '</UniqueReqId><UserId>troland</UserId><ErrorCode>0</ErrorCode><ErrorDescription></ErrorDescription></e2wSSOResponse></string>';
      Medical_QBUtility.QBWrapper wrapper = Medical_QBUtility.getFormattedResult(xmlresponse);
      System.assert(wrapper != null,'Wrapper has been created from response');
    Test.stopTest();
  }
}