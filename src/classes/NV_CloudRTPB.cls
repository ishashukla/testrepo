//Methods Included: RealTimePriceMultiRequest
// Primary Port Class Name: RealTimePriceMultiPort	
public class NV_CloudRTPB {
	public class ModelNProductRealTimePriceLookupICServicePort {
		public String endpoint_x = 'https://ps1w2.rt.informaticacloud.com:443/active-bpel/services/0010ES/ModelNProductRealTimePriceLookupICService';
		//public String endpoint_x = 'http://requestb.in/oanhnjoa';
		public Map<String,String> inputHttpHeaders_x;
		public Map<String,String> outputHttpHeaders_x;
		public String clientCertName_x;
		public String clientCert_x;
		public String clientCertPasswd_x;
		public Integer timeout_x;
		private String[] ns_map_type_info = new String[]{'http://www.stryker.com/ModelN/RTPM','NV_RTPM'};

		public ModelNProductRealTimePriceLookupICServicePort(){
			inputHttpHeaders_x = new  Map<String,String>();
			Map<String, NV_Product_Pricing_Settings__c> pricingSettings = NV_Product_Pricing_Settings__c.getAll();
			
            if(pricingSettings.get('Endpoint') != null && pricingSettings.get('Endpoint').Value__c != null){ 
                endpoint_x = pricingSettings.get('Endpoint').Value__c;
            }
            
			System.debug('[DD] Username: '+ pricingSettings.get('Username'));
			System.debug('[DD] Password: '+ pricingSettings.get('Password'));
			System.debug('[DD] Base64 Code: '+ EncodingUtil.base64Encode(Blob.valueOf(pricingSettings.get('Username')+':'+pricingSettings.get('Password'))));

			
			if(pricingSettings.containsKey('Username') && pricingSettings.containsKey('Password')
					&& pricingSettings.get('Username').Value__c != null && pricingSettings.get('Password').Value__c !=null ){

				String token = EncodingUtil.base64Encode(Blob.valueOf(pricingSettings.get('Username').Value__c+':'+pricingSettings.get('Password').Value__c));
				inputHttpHeaders_x.put('Authorization', 'Basic ' + token);
			}
			this.timeout_x = 120000;
		}

		public NV_RTPM.RealTimePriceMultiResponseType ModelNProductRealTimePriceLookup(NV_RTPM.RealTimePriceMultiRequestType req) {
			NV_RTPM.RealTimePriceMultiRequestType request_x = req;
			System.debug('[DD] Request : '+ req);
			NV_RTPM.RealTimePriceMultiResponseType response_x;
			Map<String, NV_RTPM.RealTimePriceMultiResponseType> response_map_x = new Map<String, NV_RTPM.RealTimePriceMultiResponseType>();
			response_map_x.put('response_x', response_x);
			if(!Test.isRunningTest()) {
  			WebServiceCallout.invoke(this, request_x, response_map_x, new String[] { endpoint_x, '', 'http://www.stryker.com/ModelN/RTPM', 'RealTimePriceMultiRequest', 'http://www.stryker.com/ModelN/RTPM', 'RealTimePriceMultiResponse', 'NV_RTPM.RealTimePriceMultiResponseType' });
			}
			response_x = response_map_x.get('response_x');
			return response_x;
		}
	}
}