@isTest
public without sharing class Instruments_QuickQuoteControllerTest {
	static Account acc;
	static List<User> usrList;
	static User adminUser;
	public static testMethod void testQuickCreate() {
        Test.startTest();
          insertUser(true,false);
        Test.stopTest();
        System.runAs(adminUser) {
          Quick_Quote_Opportunity_Defaults__c rec = new Quick_Quote_Opportunity_Defaults__c();
          rec.ForecastCategoryName__c = 'Omitted';
          rec.Freeze_Forecast_Category__c = true;
          rec.OpportunityName__c = 'Quick Quote';
          rec.Quick_Quote__c = true;
          rec.Stage_Name__c = 'In Development';
          rec.Type__c = 'Standard';
          insert rec;
        }
        System.runAs(usrList[0]) {
        	createTestData();
  		    PageReference pageRef = Page.Instruments_QuickQuote;
    			Test.setCurrentPage(pageRef);
    			ApexPages.currentPage().getParameters().put('accountId',acc.Id);
    			Instruments_QuickQuoteController iqqc = new Instruments_QuickQuoteController();
    			iqqc.selectedDivision = 'IVS';
    			iqqc.getDivisionsOptions();
    			iqqc.autosave();
    			iqqc.save();
    			List<Opportunity> myOpps = new List<Opportunity>([select id from Opportunity where AccountId =:acc.Id]);
    			System.assertEquals(1,myOpps.size());
		}
	}
	
	public static testMethod void testQuickCreate1() {
  	  Test.startTest();
        insertUser(false,true);
      Test.stopTest();
      System.RunAs(adminUser) {
        Quick_Quote_Opportunity_Defaults__c rec = new Quick_Quote_Opportunity_Defaults__c();
        rec.ForecastCategoryName__c = 'Omitted';
        rec.Freeze_Forecast_Category__c = true;
        rec.OpportunityName__c = 'Quick Quote';
        rec.Quick_Quote__c = true;
        rec.Stage_Name__c = 'In Development';
        rec.Type__c = 'Standard';
        insert rec;
      }
      System.runAs(usrList[0]) {
      	createTestData();
		    
			
		    PageReference pageRef = Page.Instruments_QuickQuote;
  			Test.setCurrentPage(pageRef);
  			ApexPages.currentPage().getParameters().put('accountId',acc.Id);
  			Instruments_QuickQuoteController iqqc = new Instruments_QuickQuoteController();
  			iqqc.selectedDivision = 'IVS';
  			iqqc.getDivisionsOptions();
  			iqqc.autosave();
  			iqqc.save();
      }	
      System.runAs(adminUser) {
  		  usrList.get(0).Division = 'IVS';
  		  update usrlist;
      }
		System.runAs(usrList[0]) {
		    Instruments_QuickQuoteController iqqc = new Instruments_QuickQuoteController();
			iqqc.autosave();

			List<Opportunity> myOpps = new List<Opportunity>([select id from Opportunity where AccountId =:acc.Id]);
			//System.assertEquals(1,myOpps.size());
		}
	}
    
    public static void createTestData() {
    	acc = TestUtils.createAccount(1, true).get(0);
    }
    @future
    public static void insertUser(Boolean method1, Boolean method2) {
      adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
      insert adminUser;
      if(method1) {
        usrList = TestUtils.createUser(1,'Instruments Sales User',false);
        usrList.get(0).Division = 'IVS;Navigation';
        usrList.get(0).Title = 'Instruments';
        insert usrList;
      }
      if(method2) {
        usrList = TestUtils.createUser(1,'Instruments Sales User',false);
        usrList.get(0).Division = '';
        usrList.get(0).Title = 'Instruments';
        insert usrList;
      }
    }
}