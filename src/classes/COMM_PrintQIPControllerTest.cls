@isTest
private class COMM_PrintQIPControllerTest {
//NB - 11/14 I-242624- Start
    private static testMethod void test() {
      List<Account> acc = TestUtils.createAccount(1, true);
    Opportunity opp = TestUtils.createOpportunity(1, acc[0].Id, true).get(0);
    List<Order> ordList = TestUtils.createOrder(1, acc[0].Id, 'Open', false);
    ordList[0].OpportunityId = opp.Id;
    insert ordList;
    List<Product2> prod = TestUtils.createCOMMProduct(1, true);
    List<SVMXC__Installed_Product__c> assetList = TestUtils.createInstalledProducts(1, prod[0].Id, ordList[0].Id, 'test', false);
    assetList[0].QIP_ID__c = 'QIP Visum Single LT';
    assetList[0].SVMXC__Date_Installed__c = System.today(); //NB - 11/14 - I-242624
    assetList[0].QIP_Completion_Date__c = System.today().addDays(30); //NB - 11/14 - I-242624
    insert assetList;
    List<ChecklistCUST__c> checkListCustomList = TestUtils.createCustomChecklist(1, 'QIP SINGLE VISIU', 1,true); //NB - 11/14 - I-242624
    List<Checklist_Group__c> checkListGroupList = TestUtils.createChecklistGroup(1, 'Test Name',checkListCustomList[0].id, 1,true); //NB - 11/14 - I-242624
    Checklist_Header__c checklist = new checklist_Header__c(status__c='New',Asset__c = assetList[0].id);
    insert checklist;
    CheckList_Detail__c woChecklist = new CheckList_Detail__c(Name = 'test1', Checklist_Header__c =checklist.id,question__c ='abcd',result__c='passed', Checklist_Group__c = checkListGroupList[0].Id); //NB - 11/14 - I-242624
    insert woChecklist;
    Test.setCurrentPageReference(new PageReference('Page.comm_printQIP'));
    System.currentPageReference().getParameters().put('headid',  checklist.id);
    ContentVersion conv=new ContentVersion();
    ApexPages.StandardController cntrl = new ApexPages.StandardController(conv) ; 
    COMM_PrintQIPController print = new COMM_PrintQIPController(cntrl);
    }
//NB - 11/14 I-242624- End
}