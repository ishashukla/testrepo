/**================================================================      
* Appirio, Inc
* Name: StrykerCalendarController 
* Description: Controller for StrykerCalendar
* Created Date: 20 July 2016
* Created By: Jagdeep Juneja (Appirio)
*
* Date Modified      Modified By      Description of the update
==================================================================*/
public class StrykerCalendarController {
  public Account acc {get;set;}
  public SVMXC__Service_Order__c wo {get;set;}

    
  public List<Account> lstAcc{get;set;}
  public List<SVMXC__Service_Group_Members__c> lstTech{get;set;}
  
  
  public StrykerCalendarController(){
    lstAcc = new List<Account>{new Account()};
    lstTech = new List<SVMXC__Service_Group_Members__c>{new SVMXC__Service_Group_Members__c()};
    acc = new Account();
  }
}