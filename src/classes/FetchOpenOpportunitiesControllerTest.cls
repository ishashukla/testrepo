//
// (c) 2012 Appirio, Inc.
//
//  Test class of  WebserviceAmendOpportunity
//
// 19 Aug 2015     Naresh K Shiwani      Original
//
@isTest
private class FetchOpenOpportunitiesControllerTest {
  static testmethod void testMethodSendEmail(){
    
    Test.startTest();
    TestUtils.createCommConstantSetting();
    Account acnt                      = TestUtils.createAccount(1, true).get(0);
    List<Contact> contactList         = TestUtils.createContact(2, acnt.id, false);
    Contact RSMContact                = contactList[0];
    RSMContact.title                  = 'RM';
    insert RSMContact;
    System.debug('RSMContact===='+RSMContact);
    System.assertNotEquals(RSMContact, null);
    
    Id recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Flex Internal Contact'].get(0).Id;
    
    Contact RSMContactTeamMember      = contactList[1];
    RSMContactTeamMember.ReportsToId  = RSMContact.Id;
    RSMContactTeamMember.Title= 'Sales Rep';
    RSMContactTeamMember.RecordTypeId = recordTypeId;
    RSMContactTeamMember.Type__c = 'PSR';
    RSMContactTeamMember.Business_Unit_Region__c = 'test product - test';
    insert RSMContactTeamMember;
    
    Stryker_Contact__c conJunction = new Stryker_Contact__c();
    conJunction.Internal_Contact__c = RSMContactTeamMember.Id;
    conJunction.Customer_Account__c = acnt.Id;
    insert conJunction;

    Product2 prodNew = new Product2();
    prodNew.Name = 'test product';
    prodNew.Part_Number__c = '12345';
    insert prodNew;
    
    System.debug('RSMContactTeamMember===='+RSMContactTeamMember);
    System.assertNotEquals(RSMContactTeamMember, null);
    Id flexRTOnlyConventional         = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();
    Opportunity opp                   = TestUtils.createOpportunity(1, acnt.id, false).get(0);
    opp.StageName                     = 'Legal Review';
    opp.RecordTypeId 				  = flexRTOnlyConventional;
    insert opp;
    System.assertNotEquals(opp, null);
    Product2 prod                     = TestUtils.createProduct(1, true).get(0);
      
	  PricebookEntry pbe2               = new PricebookEntry(unitprice=1,Product2Id=prod.Id,
	                                       Pricebook2Id=Test.getStandardPricebookId(),
	                                       isActive=true,UseStandardPrice = false);
	  insert pbe2;
	  
	  OpportunityLineItem oppItem       = new OpportunityLineItem();
	  oppItem.Quantity                  = 1.00;
	  oppItem.OpportunityId             = opp.Id;
	  oppItem.TotalPrice                = 100;
	  oppItem.Asset_Type__c             = 'Equipment';
	  oppItem.PriceBookEntryId          = pbe2.Id;
	  oppItem.PSR_Contact__c            = RSMContactTeamMember.id;
	  insert oppItem;
	  System.debug('oppItem===='+oppItem);
	  System.assertNotEquals(oppItem, null);
	  OpportunityLineItem oppLineItem = [Select OpportunityId, PSR_Contact__c, PriceBookEntryId  From OpportunityLineItem];
	  System.debug('oppLineItem===='+oppLineItem);
	  
/*	  Id recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Flex Internal Contact'].get(0).Id;
    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.Title= 'Sales Rep';
    con.RecordTypeId = recordTypeId;
    con.Type__c = 'PSR';
    con.Business_Unit_Region__c = 'test product - test';
    insert con;
*/
    
	  FetchOpenOpportunitiesController oppSendEmailContObj = new FetchOpenOpportunitiesController(new ApexPages.StandardController(RSMContact));
	  Test.stopTest();
  }
}