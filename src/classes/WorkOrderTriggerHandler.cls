/**================================================================      
* Appirio, Inc
* Name: [WorkOrderTriggerHandler]
* Description: [Trigger Handler for Work Order object]
* Created Date: [20-Jul-2016]
* Created By: [Varun Vasishtha] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 11 Aug 2016        Varun Vasishtha        T-505396
* 9  Sept 2016       Garuav Sinha           T-533438
* 23  Sept 2016      Garuav Sinha           T-536639, adding createCheckList
* 03  Oct  2016      Isha Shukla            I-237942, Added method checkOpenActivitiesWhenWOcloses
* 12 Oct 2016        Varun Vasishtha        I-239290 Populated Case type when create from work order
* 17 Oct 2016        Varun Vasishtha        I-240154 Commented PopulateManagerEmail method to solve too many SOQL issue, this has been covered in WF
* 18 Oct 2016        Varun Vasishtha        I-240249 Added a condition that checks if work order has type of pfa then auto created case is assigned to PFA Queue
* 4 Nov 2016         Varun Vasishtha        Populate Work Order's site and address by associated case (I-239029 Varun Vasishtha)
* 11 Nov 2016        Varun Vasishtha        Added a condition in validateFSIFromWorkOrder() to allow work order to be closed when issue is not fixed in FSI.(I-243393) 
* 14 Nov 2016        Isha Shukla            Changed value of recordtype_Admin to Installation (I-243738)
* 18 Nov 2016        Varun Vasishtha        Created PopulateWoOwner() method to populate wo owner according to Technician's salesforce user id. (I-241061) 
* 23 Nov 2016        Varun Vasishtha        Created SetClosedOnFSI() method to Check the Flag on related FSI if Work Order is closed. (T-556457)
* 5  Dec 2016        Pankaj Kumar           I-244809, Populate Comm Sales Rep with Account.Comm Sales Rep
* 6  Dec 2016        Isha Shukla            I-241551, added method populateServiceTeam
==================================================================*/
public class WorkOrderTriggerHandler {    
    Static public string recordtype_Admin;
    static {
        for(recordtype varrloop : [select id from recordtype where developername = 'Installation']){
            recordtype_Admin = varrloop.id;
        }
    }
    //Handles On Before UpDate event for this object.
    public static void onBeforeUpdate(List<SVMXC__Service_Order__c> orderList,Map<Id,SVMXC__Service_Order__c> oldMap){
        SubmitforApproval(orderList); 
        checkOpenActivitiesWhenWOcloses(orderList,oldMap);
        validateFSIFromWorkOrder(orderList, oldMap);
        //PopulateWOLocation(orderList,oldMap);
        PopulateWoOwner(orderList,oldMap);
        populateCommSalesRep(orderList,oldMap);
        populateServiceTeam(orderList,oldMap);
    }
    
    // Handles Bfore Insert event for this object.
    public static void onBeforeInsert(List<SVMXC__Service_Order__c> orderList){
        //T-538797 - Populate 'Case Owner' when 'Preventive Maintenance' work order is created
        createCaseForPMWO(orderList);
        //PopulateWOLocation(orderList,null);
        PopulateWoOwner(orderList,null);
        populateCommSalesRep(orderList,null);
        populateServiceTeam(orderList,null);
    }
    
    // Handles After Insert event for this object.
    public static void onAfterUpdate(List<SVMXC__Service_Order__c> orderList,Map<Id,SVMXC__Service_Order__c> oldMap){
        PopulateWorkOrderHistory(orderList, oldMap); 
        SetClosedOnFSI(orderList,oldMap);
        // T-505396 - Create email template for email alert
        //PopulateManagerEmail(orderList,oldMap);
    }
    
    public static void onAfterInsert(List<SVMXC__Service_Order__c> orderList){
        PopulateWorkOrderHistory(orderList, null); 
        // T-505396 - Create email template for email alert
        //PopulateManagerEmail(orderList,null);
    }
    
    private static void SetClosedOnFSI(List<SVMXC__Service_Order__c> orderList, Map<Id,SVMXC__Service_Order__c> oldMap){
        Set<Id> Wos = new Set<Id>();
        List<Field_Service_Investigation__c> investigationToUpdate = new List<Field_Service_Investigation__c>();
        for(SVMXC__Service_Order__c item : orderList){
            SVMXC__Service_Order__c oldItem;
            if(oldMap != null){
                oldItem = oldMap.get(item.Id);
            }
            
            if((item.SVMXC__Order_Status__c == 'Closed' || item.SVMXC__Order_Status__c == 'Canceled') 
               && (oldItem == null || item.SVMXC__Order_Status__c != oldItem.SVMXC__Order_Status__c)){
                   Wos.add(item.Id);
               }
        }
        
        for(Field_Service_Investigation__c item : [SELECT Id, WorkOrderClosed__c
                                                   FROM Field_Service_Investigation__c 
                                                   WHERE Work_Order__c in:Wos AND WorkOrderClosed__c=false]){
                                                       item.WorkOrderClosed__c = true;
                                                       investigationToUpdate.add(item);
                                                   }
        
        if(investigationToUpdate.size() > 0){
            update investigationToUpdate;
        }
    }
    
    private static void PopulateWoOwner(List<SVMXC__Service_Order__c> orderList,Map<Id,SVMXC__Service_Order__c> oldMap){
        Set<Id> technicians = new Set<Id>();
        Map<string,string> techWithUserId = new Map<string,string>();
        for(SVMXC__Service_Order__c item : orderList){
            SVMXC__Service_Order__c oldItem;
            if(oldMap != null){
                oldItem = oldMap.get(item.Id);
            }
            if(item.SVMXC__Group_Member__c != null && (oldItem == null || oldItem.SVMXC__Group_Member__c != item.SVMXC__Group_Member__c)){
                technicians.add(item.SVMXC__Group_Member__c);
            }
        }
        if(technicians.size() > 0)
        {
            System.debug('vas1 '+technicians);
            for(SVMXC__Service_Group_Members__c item : [Select Id,SVMXC__Salesforce_User__c from SVMXC__Service_Group_Members__c where Id in:technicians]){
                if(!techWithUserId.containsKey(item.Id)){
                    techWithUserId.put(item.Id,item.SVMXC__Salesforce_User__c);
                }
            }
            System.debug('vas2 '+techWithUserId);
            for(SVMXC__Service_Order__c item: orderList){
                if(techWithUserId.containsKey(item.SVMXC__Group_Member__c)){
                    string userId = techWithUserId.get(item.SVMXC__Group_Member__c);
                    if(string.isNotBlank(userId)){
                        item.OwnerId = userId;
                    }
                }
            }
        }
    }
    
    //5  Dec 2016        Pankaj Kumar           I-244809, Populate Comm Sales Rep with Account.Comm Sales Rep
    private static void populateCommSalesRep(List<SVMXC__Service_Order__c> orderList,Map<Id,SVMXC__Service_Order__c> oldMap)
    {
        Set<Id> accountIds = new Set<Id>();
        for(SVMXC__Service_Order__c workOrder : orderList){
            if(workOrder.SVMXC__Company__c != null && (oldMap == null || oldMap.get(workOrder.Id).SVMXC__Company__c != workOrder.SVMXC__Company__c )){
                accountIds.add(workOrder.SVMXC__Company__c);
            }
        }
        
        if(accountIds.size() > 0)
        {
            Map<Id,Sobject> accMap = new Map<Id,Sobject>();
            for(Account aAccount : [select Id,COMM_Sales_Rep__c from Account where Id in:accountIds])
            {
                accMap.put(aAccount.Id,aAccount);
            }
            
            for(SVMXC__Service_Order__c workOrder : orderList)
            {
                if(accMap.containsKey(workOrder.SVMXC__Company__c))
                {
                    Account aAccount = (Account)accMap.get(workOrder.SVMXC__Company__c);
                    workOrder.COMM_Sales_Rep__c = aAccount.COMM_Sales_Rep__c;
                }
            }
        }
    }
    
    //Populate Work Order's site and address by associated case (I-239029 Varun Vasishtha)
    /* commented based on issue I-239668
private static void PopulateWOLocation(List<SVMXC__Service_Order__c> orderList,Map<Id,SVMXC__Service_Order__c> oldMap){
Set<Id> caseIds = new Set<Id>();

for(SVMXC__Service_Order__c item : orderList){
if(item.SVMXC__Case__c != null){
caseIds.add(item.SVMXC__Case__c);
}
}
if(caseIds.size() > 0)
{
Map<Id,Case> cases = new Map<Id,Case>([SELECT 
id,
SVMXC__Site__c,
SVMXC__Site__r.SVMXC__Street__c,
SVMXC__Site__r.SVMXC__Zip__c,
SVMXC__Site__r.SVMXC__State__c,
SVMXC__Site__r.SVMXC__Country__c,
SVMXC__Site__r.SVMXC__City__c
FROM Case 
WHERE Id IN :caseIds]);

for(SVMXC__Service_Order__c item : orderList){
SVMXC__Service_Order__c oldItem;
if(oldMap != null){
oldItem = oldMap.get(item.Id);
}

if(item.SVMXC__Case__c != null){
Case locationDetail = cases.get(item.SVMXC__Case__c);

if(locationDetail.SVMXC__Site__c != null && (oldItem == null || oldItem.SVMXC__Site__c != locationDetail.SVMXC__Site__c)){
item.SVMXC__Site__c = locationDetail.SVMXC__Site__c;
item.SVMXC__Zip__c = locationDetail.SVMXC__Site__r.SVMXC__Zip__c;
item.SVMXC__Street__c = locationDetail.SVMXC__Site__r.SVMXC__Street__c;
item.SVMXC__State__c = locationDetail.SVMXC__Site__r.SVMXC__State__c;
item.SVMXC__Country__c = locationDetail.SVMXC__Site__r.SVMXC__Country__c;
item.SVMXC__City__c = locationDetail.SVMXC__Site__r.SVMXC__City__c;
}                
}
}
}
}
*/
    
    //Populate 'Case Owner' when 'Preventive Maintenance' work order is created
    private static void createCaseForPMWO(List<SVMXC__Service_Order__c> lstWO){
        //List<SVMXC__Service_Order__c> lstPMWO = new List<SVMXC__Service_Order__c>();
        
        //Id PMRecTypeId = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Preventive Maintenance').getRecordTypeId();
        
        //for (SVMXC__Service_Order__c wo : lstPMWO) {
        //if (wo.recordTypeId == PMRecTypeId) {
        //lstPMWO.add(wo);
        //}
        //}
        List<SVMXC__Service_Order__c> lstPMWO = lstWO;
        if (!lstPMWO.isEmpty()) {
            Id techSupportQueueId;
            Id pfaQueueId;
            for (Group grp : [Select id from Group Where type = 'Queue' and developerName='Tech_Support_queue']) {
                techSupportQueueId = grp.Id;
            }
            
            for (Group grp : [Select id from Group Where type = 'Queue' and developerName='TS_PFA_Queue']) {
                pfaQueueId = grp.Id;
            }
            
            List<Case> lstCase = new List<Case>();
            Map<SVMXC__Service_Order__c, Case> mapWOCase = new Map<SVMXC__Service_Order__c, Case>();
            for (SVMXC__Service_Order__c wo : lstPMWO) {
                if (wo.SVMXC__Case__c == null) {
                    Case cs  = new Case();
                    cs.AccountId = wo.SVMXC__Company__c;
                    cs.ContactId = wo.SVMXC__Contact__c;
                    cs.Description = wo.SVMXC__Problem_Description__c;
                    cs.Origin = 'Work Order';
                    // if work order has type of pfa then auto created case is assigned to PFA Queue(I-240249 Varun Vasishtha)
                    if(wo.SVMXC__Order_Type__c == 'PFA' && pfaQueueId != null){
                        cs.OwnerId = pfaQueueId;
                    }
                    else{
                        if (/*wo.recordTypeId == PMRecTypeId && */techSupportQueueId != null) {
                            cs.OwnerId = techSupportQueueId;
                        }
                    }
                    if(!string.isEmpty(wo.Coverage_Type__c)){
                        cs.Type = wo.Coverage_Type__c; //Varun Vasishtha for I-239290
                    }
                    cs.Priority = wo.SVMXC__Priority__c;
                    cs.RecordtypeId = Label.Case_RecordType_Id;
                    cs.Subject = Date.today().format() + ' - ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                    lstCase.add(cs);
                    mapWOCase.put(wo, cs);
                }
            }
            if (!lstCase.isEmpty()) {
                insert lstCase;
            }
            for (SVMXC__Service_Order__c wo : mapWOCase.keySet()) {
                wo.SVMXC__Case__c = mapWOCase.get(wo).Id;
            }
        }
    } 
    
    // Add the validation on WorkOrder where FSI does not have any resolved issues
    public static void validateFSIFromWorkOrder(List<SVMXC__Service_Order__c> orderList, map<id,SVMXC__Service_Order__c> oldordermap){
        Map<id,SVMXC__Service_Order__c> validWorkOrder = new map<id,SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c varloop: orderList){
            if(varloop.recordtypeid != recordtype_Admin && varloop.SVMXC__Order_Status__c =='Closed' 
               && ( varloop.SVMXC__Order_Status__c!= oldordermap.get(varloop.id).SVMXC__Order_Status__c)){
                   validWorkOrder.put(varloop.id,varloop);
               }
        }
        boolean check = false;
        if(!validWorkOrder.isEmpty()){
            Map<string,Field_Service_Investigation__c> liFLS = new map<string,Field_Service_Investigation__c>();
            for(Field_Service_Investigation__c varloop : [select id,B_Repair_Fix_the_Reported_Issue__c,Work_Order__c
                                                          from Field_Service_Investigation__c 
                                                          where Work_Order__c in : validWorkOrder.keySet()
                                                          and   (B_Repair_Fix_the_Reported_Issue__c ='Y' or B_Repair_Fix_the_Reported_Issue__c ='N')]){
                                                              liFLS.put(varloop.Work_Order__c,varloop);
                                                          }
            if(liFLS.isEmpty()){
                orderList[0].adderror(System.label.A_Field_Service_Investigation_must_be_completed_before_closing_this_Work_Order);
            }
            else{
                for(SVMXC__Service_Order__c varloop:validWorkOrder.values() ){
                    if(liFLS.get(varloop.id)==null){
                        check =true;
                        break;
                    }
                }
            }
            if(check){
                orderList[0].adderror(System.label.A_Field_Service_Investigation_must_be_completed_before_closing_this_Work_Order);
            }
        }
    }
    // Checks if Submit for Approval is clicked then it change the Milestone of Project Phase
    private static void SubmitforApproval(List<SVMXC__Service_Order__c> orderList){
        list<Project_Phase__c> phaselist = new list<Project_Phase__c>();
        for(SVMXC__Service_Order__c workOrder : orderList){
            if(workOrder.IsSubmitforApproval__c){
                if(workOrder.Project_Phase__c != null){
                    Project_Phase__c phase = new Project_Phase__c( id = workOrder.Project_Phase__c);
                    phase.Milestone__c = 'Schedule Requested';
                    phaselist.add(phase);
                }
                workOrder.IsSubmitforApproval__c = false;
            }
        }
        if (!phaselist.isEmpty()) {
            update phaselist;
        }
    }
    
    // Check if there is an technician assigned on the record and if assigned then populate the manager email field from the corresponding manager of technician.
    // T-505396 - Create email template for email alert
    /* private static void PopulateManagerEmail(List<SVMXC__Service_Order__c> orderList, Map<id,SVMXC__Service_Order__c> oldItems){
List<SVMXC__Service_Order__c> requiredOrders = new List<SVMXC__Service_Order__c>();
List<SVMXC__Service_Order__c> recordtoUpdate = new List<SVMXC__Service_Order__c>();
SVMXC__Service_Order__c oldEntity;
for(SVMXC__Service_Order__c workOrder : orderList){
if(oldItems != null){
oldEntity = oldItems.get(workOrder.Id);
}
if(workOrder.SVMXC__Group_Member__c != null &&(oldEntity == null || (string.isBlank(oldEntity.MemberEmailManager__c) || oldEntity.SVMXC__Group_Member__c != workOrder.SVMXC__Group_Member__c))){
requiredOrders.add(workOrder);
}
}

Map<Id,SVMXC__Service_Order__c> technicianDetail = new Map<Id,SVMXC__Service_Order__c>([select Id,SVMXC__Group_Member__r.SVMXC__Salesforce_User__r.Manager.Email 
from SVMXC__Service_Order__c 
where Id in :requiredOrders]);                                                                                      
for(SVMXC__Service_Order__c workOrder : requiredOrders){
SVMXC__Service_Order__c tempRecord = technicianDetail.get(workOrder.Id);
if(tempRecord != null && tempRecord.SVMXC__Group_Member__r != null && 
tempRecord.SVMXC__Group_Member__r.SVMXC__Salesforce_User__r != null &&
tempRecord.SVMXC__Group_Member__r.SVMXC__Salesforce_User__r.ManagerId != null){
string email = tempRecord.SVMXC__Group_Member__r.SVMXC__Salesforce_User__r.Manager.Email;
SVMXC__Service_Order__c newOrder = new SVMXC__Service_Order__c(Id=workOrder.id);
newOrder.MemberEmailManager__c = email;
recordtoUpdate.add(newOrder);
}
}
update recordtoUpdate;
}*/
    
    //Populate WorkOrderHistory where Work Order Status is changed
    public static void PopulateWorkOrderHistory(List<SVMXC__Service_Order__c> newWorkOrders,Map<Id,SVMXC__Service_Order__c> oldWorkOrders){
        List<Work_Order_History__c> listWorkOrderHistory = new List<Work_Order_History__c>();
        Work_Order_History__c workOrderHistory;
        Work_Order_History__c oldWorkOrderHistory;
        Set<Id> setWOChangeId = new Set<Id>();
        
        /* Creating WO History */
        for(SVMXC__Service_Order__c c : newWorkOrders){      
            if(oldWorkOrders == null || c.SVMXC__Order_Status__c != oldWorkOrders.get(c.Id).SVMXC__Order_Status__c){
                workOrderHistory = new Work_Order_History__c(WorkOrderID__c = c.Id, Status__c = c.SVMXC__Order_Status__c, Start_time__c = DateTime.now());
                listWorkOrderHistory.add(workOrderHistory);
                if(oldWorkOrders != null){
                    setWOChangeId.add(c.Id);
                }
            }
        }
        
        if(!setWOChangeId.isEmpty()){
            for(SVMXC__Service_Order__c c : [SELECT Id, SVMXC__Order_Status__c, (Select Id, End_time__c, Start_Time__c from Work_Order_History__r order by Createddate desc limit 1) FROM SVMXC__Service_Order__c WHERE Id IN :setWOChangeId]){
                if(c.Work_Order_History__r != null && !c.Work_Order_History__r.isEmpty()){
                    oldWorkOrderHistory = c.Work_Order_History__r.get(0);
                    workOrderHistory = new Work_Order_History__c(Id = oldWorkOrderHistory.Id, End_time__c = DateTime.now());
                    //caseHistory.Status_in_Mins__c = getDifferenceInMinutes(caseHistory.End_time__c, oldCaseHistory.Start_Time__c);
                    listWorkOrderHistory.add(workOrderHistory);               
                }
            }
        }
        
        /* Upserting Work Order History */
        if(!listWorkOrderHistory.isEmpty()){
            upsert listWorkOrderHistory;
        }
        
        // Create the Check List Header
        // Called only on insert
        if(oldWorkOrders==null){
            createCheckList(newWorkOrders);
        }
    }
    
    // Creating check list Header
    public static void createCheckList(List<SVMXC__Service_Order__c> newWorkOrders){
        map<String,string> mapWOPM = new Map<String,string>();
        map<String,List<string>> mapPMWO = new Map<String,list<string>>();
        for(SVMXC__Service_Order__c install : newWorkOrders) {
            if(install.PM_Checklist_ID__c != NULL) {
                mapWOPM.put(install.Id,install.PM_Checklist_ID__c);
                if(mapPMWO.get(install.PM_Checklist_ID__c)==null){
                    mapPMWO.put(install.PM_Checklist_ID__c,new list<string>{install.Id});
                }
                else{
                    mapPMWO.get(install.PM_Checklist_ID__c).add(install.id);
                }                
            }
        }
        
        Set<Id> checklistIdSet = new Set<Id>();
        Map<String,String> mapPMCheckList = new Map<String,string>();
        for(ChecklistCUST__c checkCust : [Select id,Name 
                                          FROM ChecklistCUST__c
                                          WHERE Name IN :mapPMWO.keySet()]){
                                              checklistIdSet.add(checkCust.id);
                                              mapPMCheckList.put(checkCust.name,checkCust.id);
                                          }
        
        List <Checklist_Header__c> checkHeadList = new List<Checklist_Header__c>();
        for(SVMXC__Service_Order__c install : newWorkOrders) {
            Checklist_Header__c checkHeader = new Checklist_Header__c (Asset__c = install.SVMXC__Component__c,
                                                                       Checklist_Type__c = install.PM_Checklist_ID__c, 
                                                                       Status__c = 'New',
                                                                       Initial_Product__c = install.SVMXC__Product__c,
                                                                       Work_Order__c = install.id,
                                                                       checklist__c = mapPMCheckList.get( install.PM_Checklist_ID__c)); // SD:9/19[Auto populate Product and checklist lookup on Creation,Ref: T-536950]
            
            checkHeadList.add(checkHeader);
        }
        
        if(!checkHeadList.isEmpty()){
            insert checkHeadList;  
        }
        
        List<CheckList_Item__c> checkItemList = new List<CheckList_Item__c>();
        if (checklistIdSet != null && checklistIdSet.size() > 0) {
            checkItemList = new List<CheckList_Item__c>([Select id,Name,
                                                         Type__c,Order__c,Checklist__c,Checklist_Group__c,Maximum__c,Minimum__c,
                                                         Question__c,Question_ID__c,Active__c,Result_Option__c
                                                         FROM CheckList_Item__c
                                                         WHERE Checklist__c IN :checklistIdSet]) ;
        }
        List<CheckList_Detail__c> checkDetailList = new List<CheckList_Detail__c>();
        for(CheckList_Item__c itemCheck : checkItemList) {
            if(itemCheck.Active__c) { 
                for(Checklist_Header__c headCheck :checkHeadList) { 
                    CheckList_Detail__c detailCheck = new CheckList_Detail__c(name = itemCheck.Name + ' 1',Checklist__c = itemcheck.Checklist__c,Checklist_Group__c =itemCheck.Checklist_Group__c,
                                                                              Maximum__c = itemCheck.Maximum__c,Minimum__c = itemCheck.Minimum__c,Question__c = itemCheck.Question__c,
                                                                              Question_ID__c = itemCheck.Question_ID__c,CheckList_Item__c = itemCheck.id, Checklist_Header__c = headCheck.id,
                                                                              Result__c = itemCheck.Result_Option__c,Type__c = itemCheck.Type__c,Order__c=itemCheck.Order__c,Asset__c  = headCheck.Asset__c );
                    checkDetailList.add(detailCheck);
                }
            }
        }
        if (checkDetailList.size() > 0) {
            Insert checkDetailList;
        }
    }
    
    // This method will check if there are any open activities then work order cannot be closed.
    public static void checkOpenActivitiesWhenWOcloses(List<SVMXC__Service_Order__c> newList,Map<Id,SVMXC__Service_Order__c> oldMap) {
        Set<Id> workOrdersToBeClosed = new Set<Id>();
        Set<Id> workOrderIdsForError = new Set<Id>();
        for(SVMXC__Service_Order__c workOrder : newList) {
            if( workOrder.SVMXC__Order_Status__c.containsIgnoreCase('Closed') 
               && workOrder.SVMXC__Order_Status__c != oldMap.get(workOrder.id).SVMXC__Order_Status__c ) {
                   workOrdersToBeClosed.add(workOrder.id);
               }
        }
        if(!workOrdersToBeClosed.isEmpty()) {
            for(SVMXC__Service_Order__c workOrder : [SELECT id , (SELECT id  FROM OpenActivities limit 1) 
                                                     FROM SVMXC__Service_Order__c 
                                                     WHERE id IN : workOrdersToBeClosed]) {
                                                         // check Open Activity
                                                         if(!workOrder.OpenActivities.isEmpty()) {
                                                             workOrderIdsForError.add(workOrder.id);
                                                         }
                                                     }
        }
        if(!workOrderIdsForError.isEmpty()) {
            for(SVMXC__Service_Order__c workOrder : newList) {
                System.debug('errorWorkOrder=='+workOrderIdsForError);
                if(workOrderIdsForError.contains(workOrder.id) ){
                    workOrder.addError(System.Label.Cannot_Close_WO_Open_Activities);
                }
            }
        }
    }
    // Populate service team same as of Technician's Service Team
    public static void populateServiceTeam(List<SVMXC__Service_Order__c> newList,Map<Id,SVMXC__Service_Order__c> oldMap) {
        Map<Id,List<SVMXC__Service_Order__c>> technicianToWorkOrderMap = new Map<Id,List<SVMXC__Service_Order__c>>();
        for(SVMXC__Service_Order__c workOrder : newList) {
            if((oldMap == null || workOrder.SVMXC__Group_Member__c != oldMap.get(workOrder.id).SVMXC__Group_Member__c) && workOrder.SVMXC__Group_Member__c != null) {
                if(!technicianToWorkOrderMap.containsKey(workOrder.SVMXC__Group_Member__c)) {
                    technicianToWorkOrderMap.put(workOrder.SVMXC__Group_Member__c,new List<SVMXC__Service_Order__c>{workOrder});
                } else { 
                    technicianToWorkOrderMap.get(workOrder.SVMXC__Group_Member__c).add(workOrder);
                }
            }
        }
        for(SVMXC__Service_Group_Members__c technician : [SELECT Id,SVMXC__Service_Group__c 
                                                          FROM SVMXC__Service_Group_Members__c 
                                                          WHERE Id IN :technicianToWorkOrderMap.keySet()]) {
                                                              for(SVMXC__Service_Order__c workOrderRecord : technicianToWorkOrderMap.get(technician.Id)) {
                                                                  workOrderRecord.SVMXC__Service_Group__c = technician.SVMXC__Service_Group__c;                                
                                                              }                                                                                                                                               
                                                              
                                                          }
    }
}