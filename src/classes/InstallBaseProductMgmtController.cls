/**================================================================      
 * Appirio, Inc
 * Name: InstallBaseProductMgmtController
 * Description: Controller class for InstallBaseMobilePage Page(Original(T-541293)) - Used to display products on SF1 when user clicks on Add 
                Products Button
 * Created Date: [07/10/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 ==================================================================*/
public class InstallBaseProductMgmtController {
  public String ibId;
  public String businessUnit{get;set;}
  public String assetType{get;set;}
  public List<Install_Base__c> ib;
  public String val{get;set;}
  public List<InstalledProduct__c> installedProductList;
  public Map<Integer,List<InstalledProduct__c>> mapIntegerToInstallProduct{get;set;}
  public Map<Id,String> prodIdtoDescriptionMap{get;set;}
  public Map<String,String> apiToLabel{get;set;}
  public List<String> selectedFields{get;set;}
  public String editableField{get;set;}
  public String sltProducts{get;set;}
  public Integer keyVal{get;set;}
  public Integer keyValDel{get;set;}
  public List<InstalledProduct__c> delList = new List<InstalledProduct__c>();
  public Boolean displayPopup{get;set;}
  public List<InstalledProduct__c> tempIPList{get;set;}
  public Id selectedProduct{get;set;}
  public InstalledProduct__c installedProduct{get;set;}
  public Boolean showProduct{get;set;}
  public Boolean showButtons{get;set;}
  public Map<Integer,List<InstalledProduct__c>> mapIntegerToIPListTemp; 
  public Boolean displayIntermediatePopup{get;set;}
  public String dupQuantity{get;set;}
  
  public List<Schema.FieldSetMember> getFields() {
    return SObjectType.InstalledProduct__c.FieldSets.Installed_Product_Columns.getFields();
  }
  public InstallBaseProductMgmtController(){
    ib = new List<Install_Base__c>();
    ibId = ApexPages.currentPage().getParameters().get('ibId');
    businessUnit = ApexPages.currentPage().getParameters().get('BU');
    assetType = ApexPages.currentPage().getParameters().get('type');
    val = '';
    installedProductList = new List<InstalledProduct__c>();
    mapIntegerToInstallProduct = new Map<Integer,List<InstalledProduct__c>>();
    prodIdtoDescriptionMap = new Map<Id,String>();
    selectedFields = new List<String>();
    apiToLabel = new Map<String,String>();
    mapIntegerToIPListTemp = new Map<Integer,List<InstalledProduct__c>>();
    editableField = 'Custom_Name__c,Serial__c,Quantity__c,Expiration_Date__c';
    for(Schema.FieldSetMember f : this.getFields()){ 
      selectedFields.add(f.getFieldPath());
      if(!apiToLabel.containsKey(f.getFieldPath())) {
        if(f.getFieldPath().containsIgnoreCase('Delete')) {
          apiToLabel.put(f.getFieldPath(),'Action');
        } 
        else{
          apiToLabel.put(f.getFieldPath(),f.getLabel());
        }
      }
    }
    if(ibId != null && ibId != ''){
      ib = [SELECT Type__c, System__c, System_Type__c, System_Name__c, Purchase_Origin__c, Purchase_Date__c, Price__c, Number_of_Systems__c, 
            Name, Instrument_Status__c, Installed_Date__c, Id, GPO_Name__c, Description__c, Contract_Status__c, Business_Unit__c, Account__c 
            FROM Install_Base__c WHERE Id =: ibId];
    }
    else{
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please save the Install Base Record first.'));
    }
    if(installedProductList.size() == 0){
      Set<Id> prodIds = new Set<Id>();
	    String query = 'SELECT Id ';
	    for(Schema.FieldSetMember f : this.getFields()){
	      query += ' '+','+f.getFieldPath()+' ';
	    }
	    query += 'FROM InstalledProduct__c WHERE Install_Base__c = :ibId';
	    installedProductList = Database.query(query);
	    Map<Id,List<InstalledProduct__c>> tempMap =new Map<Id,List<InstalledProduct__c>>();
			for(InstalledProduct__c installedProduct : installedProductList){
				if(!tempMap.containsKey(installedProduct.Product__c)){    
				 tempMap.put(installedProduct.Product__c,new List<InstalledProduct__c>());    
				}   
			  tempMap.get(installedProduct.Product__c).add(installedProduct);
			  prodIds.add(installedProduct.Product__c);
			}
			if(prodIds.size() > 0){
			  prodIdtoDescriptionMap.putAll(returnDescription(prodIds));
			}
			if(tempMap.size() > 0){   
			  Integer index = 1;    
			  for(Id key : tempMap.keySet()){   
				  mapIntegerToInstallProduct.put(index,tempMap.get(key));   
				  index++;    
			  }   
			}
    }
    if(mapIntegerToInstallProduct.size()>0){
      showButtons = true;
    }
    else{
      showButtons = false;
    }
  }
  
  public static Map<Id,String> returnDescription(Set<Id> prodId){
    Map<Id,String> idToDesc = new Map<Id,String>();
    if(prodId.size() > 0){
	    List<Product2> prodList = new List<Product2>();
	    prodList = [SELECT Id,Description FROM Product2 WHERE Id =:prodId];
	    if(prodList.size() > 0){
	      for(Product2 prod : prodList){
	        idToDesc.put(prod.Id,prod.Description);
	      }
	    }
    }
    return idToDesc;
  }
  
  public void createNewProduct(){
		installedProduct = new InstalledProduct__c();
		if(selectedProduct!=null){
		  installedProduct.Product__c = selectedProduct;
		  installedProductList.add(installedProduct);
		}
		Map<Id,List<InstalledProduct__c>> tempMap =new Map<Id,List<InstalledProduct__c>>();
		for(InstalledProduct__c installedProduct : installedProductList){
		  if(!tempMap.containsKey(installedProduct.Product__c)){    
		    tempMap.put(installedProduct.Product__c,new List<InstalledProduct__c>());    
		  }   
		  tempMap.get(installedProduct.Product__c).add(installedProduct);
		}
		if(tempMap.size() > 0){   
		  Integer index = mapIntegerToInstallProduct.size()+1; 
		  for(Id key : tempMap.keySet()){   
		    mapIntegerToInstallProduct.put(index,tempMap.get(key));   
		  }   
		}
		showProduct = false;
		val = '';
		Set<Id> tempIdSet = new Set<Id>();
		tempIdSet.add(selectedProduct);
		prodIdtoDescriptionMap.putAll(returnDescription(tempIdSet));
	}
	
  public void delProduct(){
    installedProductList = new List<InstalledProduct__c>();
    List<InstalledProduct__c> temp = new List<InstalledProduct__c>();
    List<InstalledProduct__c> finalList = new List<InstalledProduct__c>();
    if(keyVal != null){
      temp = mapIntegerToInstallProduct.get(keyVal);
      finalList = mapIntegerToInstallProduct.get(keyVal);
    }
    if(temp.size()>0){
      delList.add(temp.get(keyValDel-1));
      finalList.remove(keyValDel-1);
    }
    if(finalList.size()>0){
      mapIntegerToIPListTemp.put(keyVal,finalList);
      mapIntegerToInstallProduct.put(keyVal,finalList);
    }
  } 
  
  public void deleteProduct(){
    installedProductList = new List<InstalledProduct__c>();
    for(Integer index : mapIntegerToInstallProduct.keySet()){
      List<InstalledProduct__c> temp = new List<InstalledProduct__c>();
      List<InstalledProduct__c> finalList = new List<InstalledProduct__c>();
      temp = mapIntegerToInstallProduct.get(index);
      for(InstalledProduct__c bp : temp){
        if((bp.Delete__c && bp.Id !=null) || (bp.Enable_Duplicate_Product__c && bp.Id != null)){
          delList.add(bp);
        }
        else if(!bp.Delete__c || !bp.Enable_Duplicate_Product__c){
          finalList.add(bp);   
        }
      }
      installedProductList.addAll(finalList);
      mapIntegerToInstallProduct.put(index,finalList);
    }
  }
  
  public PageReference save(){
    List<InstalledProduct__c> listIP = new List<InstalledProduct__c>();
    if(mapIntegerToInstallProduct.size()>0){    
      for(Integer index : mapIntegerToInstallProduct.keySet()){   
        listIP.addAll(mapIntegerToInstallProduct.get(index));   
      }   
    } 
    if(listIP.size()>0){    
      for(InstalledProduct__c iprod : listIP){   
        iprod.Install_Base__c = ibId;   
        iprod.Enable_Duplicate_Product__c = false;
        iprod.Duplicate_Quantity__c = null;
      }   
      upsert listIP;    
    }
    List<InstalledProduct__c> delIPList = new List<InstalledProduct__c>();
    if(delList.size () > 0){
      for(InstalledProduct__c ipr : delList){
        if(ipr.Id != null){
          delIPList.add(ipr);
        }
      }
      if(delIPList.size()>0){
        delete delIPList;
      }
    }
    PageReference pg = new PageReference('/apex/apex/InstallBase?Id='+ibId);
    return pg;
  }
  
  public void cancelPopUp(){
	  displayPopup = false;
	  displayIntermediatePopup = false;
	}
	
	public void showPopup(){  
	  if(dupQuantity.isNumeric()){
	    displayIntermediatePopup = false;
	    List<String> keyList = new List<String>();
	    if(sltProducts != null && sltProducts != ''){
	      keyList = sltProducts.split(';');
	    } 
	    Set<Integer> keyNumberSet = new Set<Integer>();
	    if(keyList.size()>0){
	      for(String keys : keyList){
	        keyNumberSet.add(Integer.valueOf(keys));
	      }
	    }
	    List<InstalledProduct__c> iproduct = new List<InstalledProduct__c>();
	    List<InstalledProduct__c> iproductFinal = new List<InstalledProduct__c>();
	    List<InstalledProduct__c> tempIprod = new List<InstalledProduct__c>();
	    mapIntegerToIPListTemp = new Map<Integer,List<InstalledProduct__c>>();
	    for(Integer keyNumber : keyNumberSet){
	      List<InstalledProduct__c> tempIprodList = new List<InstalledProduct__c>();
	      iproduct = mapIntegerToInstallProduct.get(keyNumber);
	      Integer counter = 0;
	      for(InstalledProduct__c ipo : iproduct){
	        if(ipo.Enable_Duplicate_Product__c){
	          if(!mapIntegerToIPListTemp.containsKey(keyNumber)){
	            tempIprod.add(ipo);
	          }
	          String customName = ipo.Custom_Name__c;
	          counter = counter + 1;
	          for(Integer i = 1 ; i < Integer.valueOf(dupQuantity);  i++){
	            Integer lastCustomName;
	            InstalledProduct__c ipTemp = new InstalledProduct__c();
	            ipTemp.Prod_Description__c = prodIdtoDescriptionMap.get(ipo.Product__c);
	            ipTemp.Product__c = ipo.Product__c;
	            if(ipo.Custom_Name__c != null && ipo.Custom_Name__c != ''){
	              Integer lengthCustomName = ipo.Custom_Name__c.length();
	              String reversedCustomName = customName.reverse();
	              if(reversedCustomName.startsWith('1') || reversedCustomName.startsWith('2') || reversedCustomName.startsWith('3') ||
	                 reversedCustomName.startsWith('4') || reversedCustomName.startsWith('5') || reversedCustomName.startsWith('6') ||
	                 reversedCustomName.startsWith('7') || reversedCustomName.startsWith('8') || reversedCustomName.startsWith('9') ||
	                 reversedCustomName.startsWith('0')){
	                   String tempNumberString;
	                   Pattern pat = Pattern.compile('([0-9]+)');
	                   Matcher matcher = pat.matcher(reversedCustomName);
	                   Boolean matches = matcher.find();
	                   tempNumberString = matcher.group(1);
	                   tempNumberString = tempNumberString.reverse();
	                   lastCustomName = Integer.valueOf(tempNumberString) + 1;
	                   customName = ipo.Custom_Name__c.replaceAll('([0-9]+)', String.valueOf(lastCustomName));
	                   ipTemp.Custom_Name__c = customName;
	              }
	              else{
	                ipTemp.Custom_Name__c = ipo.Custom_Name__c;
	              }
	            }
	            
	            ipTemp.Quantity__c = 1;
	            if(counter == 1){
	              tempIprod.add(ipTemp);
	              tempIprodList.add(ipTemp);
	            }
	          }
	          if(mapIntegerToIPListTemp.containsKey(keyNumber)){
              mapIntegerToIPListTemp.get(keyNumber).addAll(tempIprodList);
            }
            else{
              mapIntegerToIPListTemp.put(keyNumber,tempIprodList);
            }
	        }         
	      }
	    }
	    tempIPList = new List<InstalledProduct__c>();
	    tempIPList.addAll(tempIprod);
	    displayPopup = true;   
		}
    else{
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This is not a valid quantity.'));
    }  
	}
	
	public void saveAndContinue(){
	  for(Integer tempInt : mapIntegerToIPListTemp.keySet()){
      mapIntegerToInstallProduct.get(tempInt).addAll(mapIntegerToIPListTemp.get(tempInt));
    } 
    displayPopup = false; 
  }
  
  public void getSelectedRecord(){
    
  }
  
  public void showIntermediatePopup(){
      displayIntermediatePopup = true;
  }
}