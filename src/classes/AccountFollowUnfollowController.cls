global class AccountFollowUnfollowController{
    public Account objAcc {get;set;}
    public boolean isFollow{get;set;}
    public Id accountId{get;set;}
    
    public AccountFollowUnfollowController(ApexPages.StandardController stdController){
        isFollow = false;  
        Id userId =  UserInfo.getUserId();
        objAcc = (Account)stdController.getRecord();
        if(objAcc != null)
            accountId = objAcc.Id;
        else
            accountId = null;
                
        String externalId  = userId+'_'+objAcc.Id;
        for(NV_Follow__c nvfollow: [SELECT Id From NV_Follow__c Where External_Id__c =: externalId]){
            isFollow = true;
        }  
        system.debug('****accountId ****'+accountId ); 
        system.debug('****UserId****'+UserInfo.getUserId() );     
        
    }   
    
    @RemoteAction
    global static boolean followAccountAndOrder(String accountID) {     
        boolean isfollowing;   
        List<NV_Follow__c > followRecords = new List<NV_Follow__c>();
        if(accountID != null){
            NV_Follow__c  follow = new NV_Follow__c();
            follow.Record_Id__c = accountID;
            follow.Following_User__c = UserInfo.getUserId();
            follow.Record_Type__c = 'Account';
            follow.External_Id__c = UserInfo.getUserId()+ '_'+accountID;
            followRecords.add(follow );
            
            for(Order order: [Select Id, Status,AccountId From Order where AccountId =: accountID AND Status NOT IN ('Cancelled', 'Shipped', 'Closed', 'Delivered')]){
                NV_Follow__c  followOrder = new NV_Follow__c();
                followOrder.Record_Id__c = order.Id;
                followOrder.Following_User__c = UserInfo.getUserId();
                followOrder.Record_Type__c = 'Order';
                followOrder.External_Id__c = UserInfo.getUserId()+ '_'+order.Id;
                followRecords.add(followOrder);
            }
            
        }
        if(!followRecords.isEmpty()){
            try{
                insert followRecords;
            } 
            catch(Exception e){
            
            }
            isfollowing=  true;
        }
         
        
        return isfollowing;
        
    } 
    
    @RemoteAction
    global static boolean unFollowAccountAndOrder(String accountID) {
        boolean isfollowing;
        Set<Id> orderIds = new Set<Id>();
        List<NV_Follow__c > followRecords = new List<NV_Follow__c>();
        if(accountId != null){
            for(Order order: [Select Id, AccountId From Order where AccountId =: accountID]){
                orderIds.add(order.Id);
            }
            
            for(NV_Follow__c followRec: [SELECT Id,Record_Id__c,Record_Type__c FROM NV_Follow__c WHERE Following_User__c = :UserInfo.getUserId() AND (Record_Id__c in:  orderIds or Record_Id__c = :accountID)]){
                followRecords.add(followRec);
            }
            
            try {       
              delete followRecords;
            }
            catch(Exception e){
            
            }
            isfollowing=  false;
        }   
         
        
        return isfollowing;    
    } 
      
}