/**================================================================      
* Appirio, Inc
* Name: CommPartsOrderTriggerHandler
* Description: T-518090
* Created Date: 07/08/2016
* Created By: Jagdeep Juneja (Appirio)
* 
* Date Modified        Modified By        Description of the update
* 
==================================================================*/
public virtual class RmaShipmentOrderTriggerHandler Implements RMAShipmentTriggerHandlerInterface{
    private List<SVMXC__RMA_Shipment_Order__c> newCommPartsOrders {get; set;}
    
    /**================================================================      
    * Standard Methods from the RMAShipmentTriggerHandlerInterface
    ==================================================================*/
    public virtual boolean IsActive(){
        return true;
    }
    
    public virtual void OnBeforeInsert(List<SVMXC__RMA_Shipment_Order__c> newRecords){}
    public virtual void OnAfterInsert(List<SVMXC__RMA_Shipment_Order__c> newRecords){}
    public virtual void OnAfterInsertAsync(Set<ID> newRecordIDs){}
    
    public virtual void OnBeforeUpdate(List<SVMXC__RMA_Shipment_Order__c> newRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> oldRecordMap){}
    public virtual void OnAfterUpdate(List<SVMXC__RMA_Shipment_Order__c> newRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> oldRecordMap){}
    public virtual void OnAfterUpdateAsync(Set<ID> updatedRecordIDs){}
        
    public virtual void OnBeforeDelete(List<SVMXC__RMA_Shipment_Order__c> RecordsToDelete, Map<ID, SVMXC__RMA_Shipment_Order__c> RecordMap){}
    public virtual void OnAfterDelete(List<SVMXC__RMA_Shipment_Order__c> deletedRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> RecordMap){}
    public virtual void OnAfterDeleteAsync(Set<ID> deletedRecordIDs){}
    
    public virtual void OnUndelete(List<SVMXC__RMA_Shipment_Order__c> restoredRecords){}
    /**================================================================      
    * End of Standard Methods from the RMAShipmentTriggerHandlerInterface
    ==================================================================*/
    
    /**================================================================      
    * Trigger Call Methods from the RMAShipmentOrderTrigger
    ==================================================================*/
    public void beforeInsert(List<SVMXC__RMA_Shipment_Order__c> newRecords){
        // Org Wide functionality
        //OnBeforeInsert(newRecords);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newRecords);
        
        // Division specific functionality
        if(newCommPartsOrders.size() > 0) COMM_RmaShipmentOrderTriggerHandler.OnBeforeInsert(newCommPartsOrders);
    }
    public void afterInsert(List<SVMXC__RMA_Shipment_Order__c> newRecords){
        // Org Wide functionality
        //OnAfterInsert(newRecords);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newRecords);
        
        // Division specific functionality 
        if(newCommPartsOrders.size() > 0) COMM_RmaShipmentOrderTriggerHandler.OnAfterInsert(newCommPartsOrders);
    }
    public void afterInsertAsync(Set<ID> newRecordIDs){
    }
    
    public void beforeUpdate(List<SVMXC__RMA_Shipment_Order__c> newRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> oldRecordMap){
        // Org Wide functionality
        //OnBeforeUpdate(newRecords, oldRecordMap);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newRecords);
        
        // Division specific functionality
        if(newCommPartsOrders.size() > 0) COMM_RmaShipmentOrderTriggerHandler.OnBeforeUpdate(newCommPartsOrders, oldRecordMap);
    }
    public void afterUpdate(List<SVMXC__RMA_Shipment_Order__c> newRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> oldRecordMap){
        // Org Wide functionality
        //OnAfterUpdate(newRecords, oldRecordMap);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newRecords);
        
        // Division specific functionality
        if(newCommPartsOrders.size() > 0) COMM_RmaShipmentOrderTriggerHandler.OnAfterUpdate(newCommPartsOrders, oldRecordMap);
    }
    public void afterUpdateAsync(Set<ID> updatedRecordIDs){
    }
        
    public void beforeDelete(List<SVMXC__RMA_Shipment_Order__c> RecordsToDelete, Map<ID, SVMXC__RMA_Shipment_Order__c> RecordMap){
    }
    public void afterDelete(List<SVMXC__RMA_Shipment_Order__c> deletedRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> RecordMap){
    }
    public void afterDeleteAsync(Set<ID> deletedRecordIDs){
    }
    
    public void afterUndelete(List<SVMXC__RMA_Shipment_Order__c> restoredRecords){
    }
    /**================================================================      
    * Trigger Call Methods from the RMAShipmentOrderTrigger
    ==================================================================*/

    private void setFilteredDataSets(List<SVMXC__RMA_Shipment_Order__c> newRecords){
        newCommPartsOrders = new List<SVMXC__RMA_Shipment_Order__c>();
        
        for(SVMXC__RMA_Shipment_Order__c a : newRecords){
            if(COMM_RmaShipmentOrderTriggerHandler.IsActive() && 
                    RTMap__c.getInstance(a.RecordTypeId).Division__c.containsIgnoreCase('COMM')){
                newCommPartsOrders.add(a);
            }
        }
    }
}