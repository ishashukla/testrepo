/*************************************************************************************************
Created By:    Surabhi Sharma/Megha Agrawal (Appirio)
Date:          Oct 06, 2015
Description  : Controller class for Visualforce page Medical_ChangeCaseOwner

March 14, 2016; sunil; Resolve I-203808
Modified By : Kajal Jalan  (T-506845) 03 May 2016 , To display ICCKM User
Modified By : Shreerath Nair  (I-222400) 14 June 2016 , To display Active ICCKM User
Modified By : Shreerath Nair  (T-529211) 23 Aug  2016, To show validation rule  error message in correct format
**************************************************************************************************/
public without sharing class Medical_ChangeCaseOwnerExtension {
  // Visualforce page variables.
  public String selectedUserId {get;set;}
  public List<UserWrapper> UserWrapperList{get; set;}
  public Boolean sendMailToOwner{get;set;}
  public String selectedOwner{get;set;}
  public String selectedQueueId{get;set;}
  public Id myProfileId{get;set;}
  
  //I-227281(SN) start
  public String InstrumentProcareId{get;set;}
  //I-227281(SN) end
  // private variables.
  //private PageReference retUrl;
  private List<Case> selectedCases;
  private Id myRecordTypeId;



  //-----------------------------------------------------------------------------------------------
  // Constructor
  //-----------------------------------------------------------------------------------------------
  public Medical_ChangeCaseOwnerExtension(ApexPages.StandardSetController stdctrl){
  	this.selectedCases = (List<Case>)stdctrl.getSelected();

  	//retUrl = stdctrl.cancel();
  	sendMailToOwner = false;
    selectedOwner = 'User';

    String caseId = ApexPages.currentPage().getParameters().get('Id');
    if(caseId != null){
      Case myCase = [SELECT ID, OwnerId, RecordTypeId FROM Case WHERE Id =: caseId];
    	this.selectedCases.add(myCase);
      myRecordTypeId = myCase.RecordTypeId;
    }

    fillAvailableUsersList();
  }

  //-----------------------------------------------------------------------------------------------
  // Fill List of all Available Users
  //-----------------------------------------------------------------------------------------------
  private void fillAvailableUsersList(){
    Set<Id> setOwnerId =new Set<Id>();
    UserWrapperList = new List<UserWrapper>();
    
    //I-227281(SN) start 
    List<String> profileNameSearch = new List<String>{'Medical - Call Center'};
    //String profileNameSearch = 'Medical - Call Center';
	//I-227281(SN) End
	
    Set<Id> instRecordTypeIds = new Set<Id>();
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Modified By : Kajal Jalan  (T-506845)
    instRecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_EMPLOYEE_COMPLAINT ).getRecordTypeId());
    instRecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_NON_PRODUCT).getRecordTypeId());
    instRecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_POTENTIAL_PRODUCT).getRecordTypeId());
    instRecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_DEPOT).getRecordTypeId());
    instRecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_REPAIR_FIELD_SERVICE).getRecordTypeId());
    
    If(instRecordTypeIds.contains(myRecordTypeId)){
      //I-227281(SN) start 	
      profileNameSearch.clear();  
      profileNameSearch.add(Intr_Constants.INSTRUMENTS_CCKM_PROFILE);
      profileNameSearch.add(Intr_Constants.INSTRUMENTS_PROCARE_SALES_PROFILE);
      profileNameSearch.add(Intr_Constants.INSTRUMENTS_CCKM_ADMIN_PROFILE);
      //I-227281(SN) end
        
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------
    for(Event e :[SELECT Id, OwnerId FROM Event WHERE ShowAs = 'OutOfOffice' AND EndDateTime >= :DateTime.Now()]) {
    	System.debug('@@@@###' + e.OwnerId);
      setOwnerId.add(e.OwnerId);
    }

    System.debug('setOwnerId+++++++++++++++++' +setOwnerId);
    //I-227281(SN) start 
    List<ID> pf =new List<Id>(new Map<id,Profile>([SELECT Id, Name FROM Profile WHERE Name IN: profileNameSearch]).keySet());
    myProfileId = pf[0];
    If(instRecordTypeIds.contains(myRecordTypeId)){
    	InstrumentProcareID = String.join(pf,',');
    }else{
    	InstrumentProcareID = String.join(pf,',');
    }
    //I-227281(SN) end
    /******   I-222400 (SN) *******************/
    //I-227281(SN) start 
    for(User u : [SELECT Id, Name FROM User Where ProfileId IN: pf AND isActive = true]){
    //I-227281(SN) end	
      UserWrapper userWrapr;
      if(setOwnerId.contains(u.Id)){
        userWrapr = new UserWrapper(u, false );
      }
      else{
        userWrapr = new UserWrapper(u, true);
      }
      UserWrapperList.add(userWrapr);
    }
  }


  //-----------------------------------------------------------------------------------------------
  // saveOwner
  //-----------------------------------------------------------------------------------------------
  public PageReference saveOwner(){
  	List<Case> listCaseToUpdate = new List<Case>();
  	String error  = '';
    Database.DMLOptions dmlOpts = new Database.DMLOptions();
    dmlOpts.EmailHeader.triggerAutoResponseEmail = false;
    dmlOpts.EmailHeader.triggerOtherEmail = false;
    dmlOpts.EmailHeader.triggerUserEmail = true;

    if ((selectedUserId == '' || selectedUserId == null) && (selectedQueueId == null || selectedQueueId ==  '')) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Please click the lookup icon and select respective owner.'));
      return null;
    }

    for(Case c : selectedCases){
    	if(selectedOwner == 'User') {
    		c.OwnerId = selectedUserId;
      }
    	else if(selectedOwner == 'Queue') {
    		c.OwnerId = selectedQueueId;
      }

    	if(sendMailToOwner){
    		c.setOptions(dmlOpts);
      }
      listCaseToUpdate.add(c);
    }

    try{
    	update listCaseToUpdate;
    	PageReference pg = new PageReference('/' + selectedCases.get(0).Id);
    	pg.setRedirect(true);
      return pg;
    }
    catch(Exception ex){
    	//T-529211 (SN)
    	//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
    	ApexPages.addMessages(ex);
    	return null;
    }
  }





  //-----------------------------------------------------------------------------------------------
  // Wrapper class used to on visualforce page
  //-----------------------------------------------------------------------------------------------
  public class UserWrapper{
  	public Boolean isAvailable{get;set;}
    public User usr{get; set;}

    public UserWrapper(User usr, Boolean isAvailable){
    	this.usr = usr;
      this.isAvailable = isAvailable;
    }
  }
}