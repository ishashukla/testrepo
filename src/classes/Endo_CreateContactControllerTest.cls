/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  March 20, 2014 
 Purpose       :  Test Class for Case Endo_createContactController
*******************************************************************************/
@isTest
private class Endo_CreateContactControllerTest {
  
  //-----------------------------------------------------------------------------------------------
  // Method for test create contact functionality
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testmethod1(){
    Account acc = Endo_TestUtils.createAccount(1, true).get(0);
    
    Test.startTest();
    ApexPages.currentPage().getParameters().put('accId', acc.Id);
    ApexPages.currentPage().getParameters().put('name_firstcon2', 'test');
    ApexPages.currentPage().getParameters().put('name_lastcon2', 'testlast');
    ApexPages.currentPage().getParameters().put('con15', 'test@test.com');
    Endo_createContactController objController = new Endo_createContactController(new ApexPages.StandardController(acc));
    
    PageReference pr1 = objController.saveAccount();
    
    Test.stopTest();
    
    //verify results.
    System.assert(pr1 != null);
  }
  
   //-----------------------------------------------------------------------------------------------
  // Method for test create contact functionality
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testmethod2(){
    Account acc = Endo_TestUtils.createAccount(1, true).get(0);
    
    Test.startTest();
    ApexPages.currentPage().getParameters().put('accId', acc.Id);
    Endo_createContactController objController = new Endo_createContactController(new ApexPages.StandardController(acc));
    objController.newContact.FirstName = 'test';
    objController.newContact.LastName = 'test';
    objController.newContact.Email = 'test@test.com';
    PageReference pr2 = objController.saveCase();
    
    Test.stopTest();
    
    //verify results.
    System.assert(pr2 != null);
  }
  
}