// (c) 2016 Appirio, Inc.
//
// Class Name: COMM_AssetPFARelTriggerHandler - Creating WO from PFA Relationship
//
// 08/09/2016, Gaurav Sinha      (T-507420) Trigger for auto-assigning tech/WO creation
public class COMM_AssetPFARelTriggerHandler{
    public static final String CONST_RT_Service = 'Service';
    public static final String CONST_Sobejct = 'SVMXC__Service_Order__c';
    public static void afterInsert(List<Asset_PFA_Relationship__c> newList ){
        createWorkOrder(newList);
    }
    
    // Method to populate the Default Technican Name
    public static void beforeinsert(List<Asset_PFA_Relationship__c> newList){
       /* system.debug('newList>>'+newList);
        for(Asset_PFA_Relationship__c varloop: newList){
            if(varloop.Technician_Name__c==null){
                varloop.Technician_Name__c = 'Automated';
            }
        }
        AssignTechandAsset(newList); */
    }
    
    // Assign Technician and Asset based on Passed Asset Serial Number and Technician Name(Varun Vasishtha I-240236)
    /*private static void AssignTechandAsset(List<Asset_PFA_Relationship__c> newList){
        map<String,string> mapTechnicianName = new map<String,String>();
        map<String,string> mapAssetName = new map<String,String>();
        for(Asset_PFA_Relationship__c item : newList){
            System.debug('item>>'+item);
            if(item.Technician__c == null && string.isNotBlank(item.Technician_Name__c)){
                mapTechnicianName.put(item.Technician_Name__c,null);
            }
            if(item.Asset__c == null && string.isNotBlank(item.AssetId__c)){
                mapAssetName.put(item.AssetId__c,null);
            }
        }
        
        for(SVMXC__Service_Group_Members__c varloop: [SELECT Id, Name 
                                                      FROM SVMXC__Service_Group_Members__c
                                                      WHERE Name in :mapTechnicianName.keySet()]){
                                                          system.debug('varlooptech>>'+varloop);
            mapTechnicianName.put(varloop.name,varloop.id);                                             
        }
         system.debug('1asset>>');   
        for(SVMXC__Installed_Product__c varloop: [SELECT id, SVMXC__Serial_Lot_Number__c 
                                                  FROM SVMXC__Installed_Product__c
                                                  WHERE SVMXC__Serial_Lot_Number__c in :mapAssetName.keySet()]){
                                                      system.debug('varloopasset>>'+varloop);
            mapAssetName.put(varloop.SVMXC__Serial_Lot_Number__c,varloop.id);                                             
        }
         system.debug('2asset>>');  
        for(Asset_PFA_Relationship__c item : newList){
            if(item.Technician__c == null && string.isNotBlank(item.Technician_Name__c)){
               item.Technician__c = mapTechnicianName.get(item.Technician_Name__c);
            }
            if(item.Asset__c == null && string.isNotBlank(item.AssetId__c)){
                 item.Asset__c = mapAssetName.get(item.AssetId__c);
            }
        }        
    } */
    
    // Method to create the Work Order from the obejct
    public static void createWorkOrder(List<Asset_PFA_Relationship__c> newList ){
        List<SVMXC__Service_Order__c> linewWorkOrder = new List<SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c tempVar = new SVMXC__Service_Order__c();
        Recordtype rt = [select id from Recordtype where sobjecttype =: CONST_Sobejct and developername =:CONST_RT_Service];
        if(rt!=null){
            for(Asset_PFA_Relationship__c varLoop : newList){
                if(varloop.Asset__c!=null){
                    tempVar = new SVMXC__Service_Order__c();
                    if(varloop.Technician__c != null){
                        tempvar.SVMXC__Group_Member__c = varloop.Technician__c;
                    }
                    tempVar.Created_from_PFA__c = varloop.Id;
                    tempvar.SVMXC__Order_Type__c = 'PFA';
                    tempVar.SVMXC__Component__c = varloop.Asset__c;
                    linewWorkOrder.add(tempVar);
                }
            }
            if(!linewWorkOrder.isEmpty()){
                insert linewWorkOrder;
            }
        }
    }
}