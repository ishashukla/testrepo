/*******************************************************************************
 Author        :  Appirio JDC (Megha Agarwal)
 Date          :  Oct 5th, 2015
 Description   :  Test class for RegulatoryQuestionAnswerTriggerTest
*******************************************************************************/
@isTest
private class RegulatoryQuestionAnswerTriggerTest {
  private static List<Account>  accounts;
  private static List<Contact> contacts;
  private static List<Case>  cases;
  private static List<Asset> assets;
  private static void createTestData(){
    accounts = Medical_TestUtils.createAccount(1, true);
    contacts =  Medical_TestUtils.createContact(1, accounts.get(0).id,true);
    assets = Medical_TestUtils.createAsset(1, accounts.get(0).Id, true);
    Id MedicalrecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Medical - Potential Product Complaint').getRecordTypeId();
    TestUtils.createRTMapCS(MedicalrecordTypeId, 'Medical - Potential Product Complaint', 'Medical');
    cases = Medical_TestUtils.createCase(1, accounts.get(0).id, contacts.get(0).id, false);
    cases[0].RecordTypeId = MedicalrecordTypeId;
    cases.get(0).AssetId = assets.get(0).Id;
    insert cases;

  }
  
  static testMethod void testRegulatoryQuestionAnswerTrigger(){
    createTestData();
    
    Case cs = cases.get(0);
    cs.Status = 'Closed';
    //cs.AssetId = assets.Id;
    update cs;
    Medical_TestUtils.createRegulatoryQuesAnswer(1,cases.get(0).Id,true);
    Test.startTest();
      try{
      	Medical_TestUtils.createRegulatoryQuesAnswer(1,cases.get(0).Id,true);
      }catch(Exception e){
      	System.assert(e.getMessage() != null , 'Duplicate regulatory answer error');
      }
    Test.stopTest();
  }
}