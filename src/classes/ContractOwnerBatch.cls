// (c) 2015 Appirio, Inc.
//
// Class Name: ContractOwnerBatch
// Description: Batch Class To change contract owner as account team member as per the account on contract
//
// April 28 2016, Prakarsh Jain  Original (I-215394)
//
global class ContractOwnerBatch implements Database.Batchable<sObject> { 
  global Database.QueryLocator start(Database.BatchableContext BC){
    User user = [SELECT Id, Name FROM User WHERE Name =: Label.Integration_User].get(0);
    String userId = user.Id;
    String query = '';
    //Uncomment line 14 and 15 to check the batch runs fine for only one record. in hardcodedId give id of contract whose owner is Integration user
    //String hardcodedId = '800c0000000Uv9C'; //used hardcoded id for checking the functionality of the batch
    //query = 'SELECT Id, Name, OwnerId, AccountId FROM Contract WHERE OwnerId = :userId AND Id = :hardcodedId'; //query to check the functionality using just one id.
    query = 'SELECT Id, Name, OwnerId, AccountId FROM Contract WHERE OwnerId = :userId';
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext BC,List<Sobject> scope){
    Set<Id> accountIdSet = new Set<Id>();
    List<String> listContractRoles = new List<String>();
    List<AccountTeamMember> listAccountTeamMember = new List<AccountTeamMember>();
    Map<Id, Id> mapAccountIdToAccountTeamMemberId = new Map<Id, Id>();
    Map<Id, Id> mapAccountTeamMemberIdToUserId = new Map<Id, Id>();
    List<Contract_Batch_Role__c> contractRolesList = Contract_Batch_Role__c.getall().values();
    List<Contract> listContract = new List<Contract>();
    for(Sobject s : scope){
      Contract contract = (Contract)s;
      accountIdSet.add(contract.AccountId);
    }
    for(Contract_Batch_Role__c contractRoles : contractRolesList){
      listContractRoles.add(contractRoles.Name);
    }
    listAccountTeamMember = [SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId IN :accountIdSet AND TeamMemberRole IN :listContractRoles];
    for(AccountTeamMember accountTeamMember : listAccountTeamMember){
      if(!mapAccountIdToAccountTeamMemberId.containsKey(accountTeamMember.AccountId)){
        mapAccountIdToAccountTeamMemberId.put(accountTeamMember.AccountId, accountTeamMember.Id);
        mapAccountTeamMemberIdToUserId.put(accountTeamMember.Id, accountTeamMember.UserId);
      }
    }
    for(Sobject s : scope){
      Contract contract = (Contract)s;
      if(mapAccountIdToAccountTeamMemberId.containsKey(contract.AccountId)){
        contract.OwnerId = mapAccountTeamMemberIdToUserId.get(mapAccountIdToAccountTeamMemberId.get(contract.AccountId));
        listContract.add(contract);
      }
    }
    if(listContract.size()>0){
      update listContract;
    }
  }
  global void finish(Database.BatchableContext BC){
  
  }
}