/*************************************************************************************************
Created By:     Prakarsh Jain
Date:           Sept 20, 2016
Description:    Test class for ViewAsRelationshipTriggerHandler(T-538445), added method testshareASRUserRecords
**************************************************************************************************/
@isTest
public class ViewAsRelationshipTriggerHandlerTest {
  //Method to test value add activity record is shared with the asr user when view as relationship record is inserted and sharing is removed
  //when view as relationship record is deleted
  public static List<User> salesRepUserList;
  public static List<User> ASRUserList;
  static User adminUser;
  static Mancode__c mancode;
  static testMethod void testshareASRUserRecords(){
    Test.startTest();
      insertUser();
    Test.stopTest();
    //Territory__c terr = new Territory__c(Name='test',Territory_Manager__c=ASRUserList[0].Id,Business_Unit__c = 'IVS',Mancode__c = '1234',Super_Territory__c=true,Territory_Mancode__c = '1234');
    //insert terr;
    System.runAs(adminUser) {
        mancode = TestUtils.createMancode(1, ASRUserList[0].Id, null, false).get(0);
        mancode.Name = '000000';
        mancode.Type__c = 'ASR';
        //mancode.Territory__c = terr.Id;
        insert mancode;
    
        Value_Add_Activities__c value = new Value_Add_Activities__c();
        value.OwnerId = salesRepUserList[0].Id;
        value.Business_Unit__c = 'NSE';
        system.runAs(salesRepUserList[0]){
          insert value;
        }
        ASR_Relationship__c asrUser = new ASR_Relationship__c();
        asrUser.Mancode__c = mancode.Id;
        asrUser.ASR_User__c = salesRepUserList[0].Id;
        insert asrUser;
        List<Value_Add_Activities__Share> valeAddActivityShareList = [SELECT Id, ParentId,UserOrGroupId FROM Value_Add_Activities__Share WHERE ParentId =: value.Id];
        system.assertEquals(true, valeAddActivityShareList.size()>1);
        delete asrUser;
        List<Value_Add_Activities__Share> valeAddActivityShareDeleteList = [SELECT Id, ParentId,UserOrGroupId FROM Value_Add_Activities__Share WHERE ParentId =: value.Id];
        system.assertEquals(true, valeAddActivityShareDeleteList.size()==1);
    }
  }
  @future
  public static void InsertUser() {
    adminUser = TestUtils.createUser(2,'System Administrator', false).get(0);
    insert adminUser;
    salesRepUserList = TestUtils.createUser(2, 'Instruments Sales User', false);
    salesRepUserList[0].Division = 'NSE';
    salesRepUserList[1].Division = 'NSE';
    salesRepUserList[0].Title = 'Sales Rep';
    salesRepUserList[1].Title = 'Sales Rep';
    insert salesRepUserList;
    
    ASRUserList = TestUtils.createUser(2, 'Instruments ASR User', false);
    ASRUserList[0].Division = 'NSE';
    ASRUserList[1].Division = 'NSE';
    ASRUserList[0].Title = 'ASR';
    ASRUserList[1].Title = 'ASR';
    insert ASRUserList;
  }
}