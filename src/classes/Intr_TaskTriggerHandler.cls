/*************************************************************************************************
Created By:    Kajal Jalan
Date:          June 06, 2016
Description:   Handler class for Task Trigger(T-508997)

Update     :   Aug 05, 2016 (I-227752) Shreerath Nair - Added function to check if attachment
Update     :   Sept 02, 2016 (I-233217) Manish Soni - Added check for record type
**************************************************************************************************/
public without sharing class Intr_TaskTriggerHandler {
    
    //I-227752(SN) static variable to stop recursion of trigger
    public static Boolean isFirstTime = true;
    
    // To update call reason field from call disposition field when a new task is created.
    public static void UpdateCallReason(list<Task> listNewTask,map<Id,task> MapOldTask){
        
        Schema.RecordTypeInfo rtByName = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments CCKM Task Record');
        Id recordTypeId = rtByName.getRecordTypeId();
    
        for(Task task : listNewTask){
            // I-233217 - Added check for record type 
            if(task.RecordTypeId == recordTypeId && ( MapOldTask == null ||  MapOldTask.get(task.Id).CallDisposition != task.CallDisposition)){
                task.Call_Reason__c  = task.CallDisposition;
                
            }
        }
        
    }
    
    //I-227752(SN) function to check if attachment exist on Task
     public static void checkAttachmentOnTask(map<Id,Task> mapNewTask){
        
        Map<id,Task> taskToUpdateMap = new Map<id,Task>();
        Task task;
        //check if trigger fired for first time 
        if(Intr_TaskTriggerHandler.isFirstTime){
            
            //set static variable to false to avoid recursion of trigger
            Intr_TaskTriggerHandler.isFirstTime = false;
            
            //Check all the attachments related to tasks
            for(Attachment attach: [SELECT id,parentId
                                FROM  Attachment
                                WHERE ParentId IN : mapNewTask.KeySet()]){
                
                //check if task existed in Map to avoid duplicate records update
                if(!taskToUpdateMap.containsKey(attach.parentId)){
                    
                    //add task to map for updation
                    task = new Task(id=attach.ParentId,Has_Attachment__c = true);
                    TaskToUpdateMap.put(attach.parentId,task);
                }                   
            }
            
            //Update the Tasks 
            if(taskToUpdateMap.size()>0){
                update taskToUpdateMap.values();
            }
            
        }
        
     }
     
}