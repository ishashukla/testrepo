global class OpportunityTeamMultiRepManagerBatch implements Database.Batchable<sObject>,Database.Stateful{
	global Database.QueryLocator start(Database.BatchableContext BC){
		String query='';
		String timeduration = Label.Opportunity_Team_Batch_Sync;
		DateTime dt = system.now();
		Integer minutes = Integer.valueOf(timeduration);
		DateTime currentDate = dt.addMinutes(-minutes);
		query = 'Select UserId, TeamMemberRole,OpportunityId,OpportunityAccessLevel,LastModifiedDate,CreatedDate FROM OpportunityTeamMember WHERE CreatedDate >=:currentDate ';
		System.debug('>>>>>query'+query);
		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC,List<Sobject> scope){
		Set<String> newRecord = new Set<String>();
		Set<String> oldRecord = new Set<String>();
		Set<String> BUSet = new Set<String>();
  		Set<String> allUsers = new Set<String>();
  		system.debug('>>>>>>>>1'+scope);
		for(Sobject s : scope){
			OpportunityTeamMember  member = (OpportunityTeamMember)s;
			newRecord.add(member.OpportunityId+'+'+member.UserId+'+'+member.TeamMemberRole+'+'+member.OpportunityAccessLevel);
		}
		system.debug('>>>>>>>>1'+newRecord);
		Map<String,Id> userBUtoManager = new Map<String,Id>();
		List<OpportunityShare> recordToInsert = new List<OpportunityShare>();
		if(newRecord.size() > 0){
  			for(String record : newRecord){
  			  List<String> tempList = new List<String>();
  			  tempList = record.split('\\+');
  			  BUSet.add(tempList.get(2));
  			  allUsers.add(tempList.get(1));
  			}
  		}
  		if(BUSet.size() > 0 && allUsers.size() > 0){
  		  List<Multi_BU_Assignment__c> assignmentList = new List<Multi_BU_Assignment__c>();
  		  assignmentList = [SELECT Id,Business_Unit__c,Rep__c,Manager__c 
						    FROM Multi_BU_Assignment__c 
						    WHERE Business_Unit__c IN:BUSet 
						    AND Rep__c IN:allUsers];
		  if(assignmentList.size() > 0){
		  	for(Multi_BU_Assignment__c multiRepUser : assignmentList){
		  		userBUtoManager.put(multiRepUser.Rep__c+'+'+multiRepUser.Business_Unit__c,multiRepUser.Manager__c);
		  	}
		  }
  		}
  		if(userBUtoManager.size() > 0){
  			for(String record : newRecord){
  			 List<String> tempList = new List<String>();
  			 tempList = record.split('\\+');
  			 if(userBUtoManager.containsKey(tempList.get(1)+'+'+tempList.get(2))){
  			 	OpportunityShare oppShare = new OpportunityShare ();
  				oppShare.userOrGroupId = userBUtoManager.get(tempList.get(1)+'+'+tempList.get(2));
  				if(tempList.get(3).equalsIgnoreCase('Edit') || tempList.get(3).equalsIgnoreCase('Private') || tempList.get(3).equalsIgnoreCase('Read Only')){
  					oppShare.OpportunityAccessLevel = 'Read';
  				}else{
  					oppShare.OpportunityAccessLevel = 'Edit';
  				}
  				oppShare.RowCause = Schema.OpportunityShare.RowCause.Manual;
  				oppShare.OpportunityId = tempList.get(0);
  				recordToInsert.add(oppShare);
  			 }
  			}
  		}
  		if(recordToInsert.size() > 0){
  			insert recordToInsert;
  		}
	}
	global void finish(Database.BatchableContext BC){
	}
}