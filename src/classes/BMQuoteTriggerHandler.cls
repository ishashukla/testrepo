//
// (c) 2016 Appirio, Inc.
//
//  Migrated From COMM Src
// 
//  13 April 2016,        Meghna Vijay
//  6th May, 2016         Deepti Maheshwari (Updated Ref : I-216824)
// 18th Oct 2016          Nitish Bansal   (I-240579 - Resolve - MISSING_ARGUMENT, Id not specified in an update call)
public with sharing class BMQuoteTriggerHandler extends TriggerController{

    /** 
     * Constructor.
     * @see TriggerController   The base class of trigger controller.
     */
    public BMQuoteTriggerHandler(Boolean isAfter, Boolean isBefore,
            Boolean isDelete, Boolean isInsert, Boolean isUndelete, Boolean isUpdate,
            List<sObject> lstNewItems, Map<Id, sObject> mapNewItems, List<sObject> lstOldItems, Map<Id, sObject> mapOldItems)
    {
        super(isAfter, isBefore, isDelete, isInsert, isUndelete, isUpdate, lstNewItems, mapNewItems, lstOldItems, mapOldItems);
    }
    
    public BMQuoteTriggerHandler (){} //NB - 06/27 - I-224056
    
     /**
     * Override method to perform action after an insert operation was fired.
     * @see TriggerController#runAfterInsert
     */
    protected override void runAfterInsert() {
        setQuoteOrderNumberOpp((List<BigMachines__Quote__c>)lstNewItems, null);
    }

     /**
     * Override method to perform action after an update operation was fired.
     * @see TriggerController#runAfterInsert
     */    
    protected override void runAfterUpdate() {
    
    setQuoteOrderNumberOpp((List<BigMachines__Quote__c>)lstNewItems, (Map<Id, BigMachines__Quote__c>) mapOldItems);
    // Removed updation of opportunity ID on quote document, when the quote is changed primary to non primary
    //updateOpportunityIDOnDocument((List<BigMachines__Quote__c>)lstNewItems, (Map<Id, BigMachines__Quote__c>) mapOldItems);
    }

    /**
     * Override method to perform action after a delete operation was fired.
     * @see TriggerController#runAfterDelete
     */
    protected override void runAfterDelete() {
    setQuoteOrderNumberOpp((List<BigMachines__Quote__c>)lstOldItems, null);
    }

    /**
     * Override method to perform action after an undelete operation was fired.
     * @see TriggerController#runAfterUndelete
     */
    protected override void runAfterUndelete() {
    setQuoteOrderNumberOpp((List<BigMachines__Quote__c>)lstNewItems, null);
    }
    
    public void setQuoteOrderNumberOpp(List<BigMachines__Quote__c> lstNewBigMachinesQuotes, Map<Id, BigMachines__Quote__c> mapOldBigMachinesQuotes) {//NB - 06/27 - I-224056
  
        /*
        Übernimmt die Quote Number aus dem Primary BigMachines Quote in die Opportunity
        */
        Map<Id, Opportunity> oppByQuoteId  = new Map<Id, Opportunity>();

        for (BigMachines__Quote__c pq : lstNewBigMachinesQuotes) {
            if (!isUpdate ||
                 pq.BigMachines__Is_Primary__c != mapOldBigMachinesQuotes.get(pq.Id).BigMachines__Is_Primary__c ||
                 (pq.Name != mapOldBigMachinesQuotes.get(pq.Id).Name && pq.BigMachines__Is_Primary__c) || pq.Revision_Number__c != mapOldBigMachinesQuotes.get(pq.Id).Revision_Number__c
               ) {
              if(pq.BigMachines__Opportunity__c != null){ //NB - 10/18 - I-240579-  MISSING_ARGUMENT, Id not specified in an update call resolution
                oppByQuoteId.put(pq.BigMachines__Opportunity__c, new Opportunity(Id=pq.BigMachines__Opportunity__c, Quote_Number_Big_Machines__c=null, SAP_Order_Number__c=null, Primary_Quote_Number__c = null)); //NB - 06/27 - I-224056
              }
            }
        }

        if(!oppByQuoteId.isEmpty() && oppByQuoteId.size() > 0) { //NB - 10/18 - I-240579 - MISSING_ARGUMENT, Id not specified in an update call resolution
          for (BigMachines__Quote__c quote : [SELECT Id, BigMachines__Opportunity__c, Name, BigMachines__Is_Primary__c, Revision_Number__c, SAP_Order_Number__c FROM BigMachines__Quote__c WHERE BigMachines__Opportunity__c IN :oppByQuoteId.keySet() AND BigMachines__Is_Primary__c = true ORDER BY BigMachines__Opportunity__c]) {
            
            //oppByQuoteId.get(quote.BigMachines__Opportunity__c).Quote_Number_Big_Machines__c  = quote.Name;
            //edited by aMind
            oppByQuoteId.get(quote.BigMachines__Opportunity__c).Quote_Number_Big_Machines__c  = quote.Name + '_' + quote.Revision_Number__c + '-';
            oppByQuoteId.get(quote.BigMachines__Opportunity__c).SAP_Order_Number__c = quote.SAP_Order_Number__c;
            if(quote.BigMachines__Is_Primary__c){
                oppByQuoteId.get(quote.BigMachines__Opportunity__c).Primary_Quote_Number__c = quote.Name; //NB - 06/27 - I-224056
            }
                
          }
          
          update oppByQuoteId.values();
        }
    }
    
    /*private void updateOpportunityIDOnDocument(List<BigMachines__Quote__c> lstNewBigMachinesQuotes, 
                                            Map<Id, BigMachines__Quote__c> mapOldBigMachinesQuotes) {
      Map<Id,BigMachines__Quote__c> primaryQuotes = new Map<Id,BigMachines__Quote__c>();
      Map<Id,BigMachines__Quote__c> noMorePrimaryQuotes = new Map<Id,BigMachines__Quote__c>();
      for(BigMachines__Quote__c updatedQuote : lstNewBigMachinesQuotes) {
         if(mapOldBigMachinesQuotes.get(updatedQuote.id).BigMachines__Is_Primary__c 
                != updatedQuote.BigMachines__Is_Primary__c){
            if(updatedQuote.BigMachines__Is_Primary__c) {
              primaryQuotes.put(updatedQuote.id, updatedQuote);
            }else{
              noMorePrimaryQuotes.put(updatedQuote.id, updatedQuote);
            }
         }
      }
      if(primaryQuotes.size() > 0){
        updateOpportunityID(primaryQuotes);
      }
      if(noMorePrimaryQuotes.size() > 0){
        removeOpportunityID(noMorePrimaryQuotes);
      }
    }
    
    private void updateOpportunityID(Map<Id,BigMachines__Quote__c> primaryQuotes){
      List<Document__c> docsToUpdate = new List<Document__c>();
      Set<ID> OpportunityIDSet = new Set<ID>();
      for(Document__c docs : [SELECT Opportunity__c, Id , Oracle_Quote__r.id, Project_Plan__c
                              FROM Document__c 
                              WHERE Oracle_Quote__r.id in :primaryQuotes.keyset()]){
         docs.Opportunity__c = primaryQuotes.get(docs.Oracle_Quote__r.id).BigMachines__Opportunity__c;
         OpportunityIDSet.add(docs.Opportunity__c);
         docsToUpdate.add(docs);
      }
      Map<Id,Opportunity> opportunityMap = new Map<ID, Opportunity>([SELECT Project_Plan__c, Id 
                                            FROM Opportunity 
                                            WHERE ID in :OpportunityIDSet]);
      for(Document__c docs : docsToUpdate){
        if(opportunityMap.containsKey(docs.Opportunity__c)){
          docs.Project_Plan__c = opportunityMap.get(docs.Opportunity__c).Project_Plan__c;
        }
      }                                            
      update docsToUpdate;
    }
    
    private void removeOpportunityID(Map<Id,BigMachines__Quote__c> noMorePrimaryQuotes){
      List<Document__c> docsToUpdate = new List<Document__c>();
      for(Document__c docs : [SELECT Opportunity__c, Id , Oracle_Quote__r.id, Project_Plan__c
                              FROM Document__c 
                              WHERE Oracle_Quote__r.id in :noMorePrimaryQuotes.keyset()]){
         docs.Opportunity__c = null;
         docs.Project_Plan__c = null;
         docsToUpdate.add(docs);
      }
      update docsToUpdate;
    }*/
}