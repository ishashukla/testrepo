/**================================================================      
 * Appirio, Inc
 * Name: Endo_SendEmailtoOrderContactTest
 * Description: Test Class for Endo_SendEmailtoOrderCustomers  
 * Created Date: 22/07/2016
 * Created By: Pratibha Chhimpa (Appirio)
 * 
 
 Date Modified      Modified By         Description of the update
 27 July 2016       Nathalie Le Guay    Fix test class
 05 Aug  2016       Parul Gupta         I-226406 After refactoring fix text class
 19 Sep 2016        Shreerath            T-538447 - Replaced Days_Delinquent__c with Delinquent_Days__c in query
*******************************************************************************/
@isTest(SeeAllData=false)
private class Endo_SendEmailtoOrderCustomersTest{
    
    // Test method to test functionality for Sending Email to Order Contact
    static TestMethod void testorderCustomerBatch() {
    
        // Fetch orders
        List<Order> ordList = [SELECT Id,recordtypeId, Type, Contact__c, RMA_Number__c, Account.AccountNumber, 
                            (SELECT Shipped_Date__c, Part_Number__c, Part_Number__r.Name, Description, 
                                    Quantity_Shipped__c, OrderId, Order.PoNumber, Oracle_Ordered_Item__c, 
                                    Status__c, Order.OrderNumber, Order.Type, Order.Account.AccountNumber, Delinquent_Days__c,
                                    Order.Account.Name 
                                    From OrderItems where Status__c = : Label.Endo_Order_Item_Awaiting_Return AND Delinquent_Days__c = 7) 
                             FROM Order 
                             WHERE Order.Type = 'SJC Credit Return' AND Contact__c != null ];
    
        System.assert(ordList.size() > 0);  
        Test.startTest();
            Endo_SendEmailtoOrderCustomers batch = new Endo_SendEmailtoOrderCustomers();
            Database.executeBatch(batch);
        Test.stopTest();
    }
    
      // Set up test data
        @testSetup static void createTestData(){
        List<Account> accountList = new List<Account>();
        List<Contact> contactList = new List<Contact>();
        List<Product2> prod = new List<Product2>();
       
        //Retrieve the describe result for the desired object
        DescribeSObjectResult result = Schema.getGlobalDescribe().get('Order').getDescribe();
    
        //Generate a map of tokens for all the Record Types for the desired object
        Map<String, Schema.RecordTypeInfo>  recordTypeInfo = result.getRecordTypeInfosByName();
    
        //Retrieve the record type id by name
        Id recordTypeId = recordTypeInfo.get('Endo Complete Orders').getRecordTypeId();
        
        EndoGeneralSettings__c cs = new EndoGeneralSettings__c();
        cs.OrderEndoCompleteOrders__c = recordTypeId;
        insert cs;
        
        // Creating one Account
        accountList = Endo_TestUtils.createAccount(1,true);
        
        // Creating one Contact
        contactList = TestUtils.createContact(1,accountList[0].id, false);    
        contactList[0].Email = 'test@test.com';
        insert contactList;
        
        // Getting Product, pricebook and pricebookEntry for Order and OrderItems
        prod = Endo_TestUtils.createProduct(1, true);
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = Endo_TestUtils.getTestPricebookEntry(pricebookId,prod[0].Id);
        
        // Getting Order 
        Order ord = Endo_TestUtils.createStandardOrder(contactList[0].id, accountList[0].id, false);
        ord.RecordTypeId = recordTypeId; 
        ord.Type = 'SJC Credit Return'; // For testing
        ord.Pricebook2Id = pricebookId;
        ord.Status = 'Backordered';
        insert ord;
        
        // Getting Order Product 
        OrderItem ordItems = Endo_TestUtils.createOrderItem(prod[0].id, ord.id, false);
        ordItems.Status__c = Label.Endo_Order_Item_Awaiting_Return;
        ordItems.PricebookEntryId = standardPrice.Id ;
 	    //Added by Shubham for Story S-447620
        ordItems.Loaner_Delinquency_Date__c = Date.today().addDays(-7);
        insert ordItems;
    
    }          
}