/**================================================================      
* Appirio, Inc
* Name: COMM_cycleCount_TriggerManager
* Description: Create Trigger on Cycle count to call approval process from SFM T-542521
* Created Date: 3 - Oct - 2016
* Created By: Gaurav Sinha (Appirio)
*
* Date Modified      Modified By      Description of the update
* 9 nov 2016         Isha Shukla      (I-243239)Set Integration status to new when stock adjustment record is created in method Create_StockProduct
==================================================================*/
public class COMM_cycleCount_TriggerManager {
     public static void submitForApprovalProcess(List<Cycle_Count__c > spaList, Map<Id, Cycle_Count__c > oldMapContractors) {
        for(Cycle_Count__c  spaRec : spaList) {
            if(  spaRec.Submit_for_Approval__c  && 
                (oldMapContractors == null || (oldMapContractors != null && spaRec.Submit_for_Approval__c != oldMapContractors.get(spaRec.Id).Submit_for_Approval__c))) {
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submit record in Approval Process.');
                req1.setObjectID(spaRec.id);
                
                try {
                    // Submit the approval request for the account
                    Approval.ProcessResult result = Approval.process(req1);    
                } 
                catch(Exception e) {
                    System.debug(e.getMessage());
                }
            }
        }
    }
    
    public static void afterUpdate(List<Cycle_Count__c > spaList, Map<Id, Cycle_Count__c > oldMapContractors){
        submitForApprovalProcess(spaList,oldMapContractors);
        Create_StockProduct(spaList,oldMapContractors);
    }
    
    public static void Create_StockProduct(List<Cycle_Count__c > spaList, Map<Id, Cycle_Count__c > oldMapContractors){
        Set<id> setofcycleCount = new set<id>();
        for(Cycle_Count__c  spaRec : spaList) {
            if(spaRec.status__c=='Approved' && spaRec.status__c != oldMapContractors.get(spaRec.Id).status__c) {
                setofcycleCount.add(spaRec.id);
            }
        }
        List<SVMXC__Stock_Adjustment__c> liStockedProduct = new List<SVMXC__Stock_Adjustment__c>();
        if(!setofcycleCount.isEmpty()){
            for(cycle_count_line__c varLoop: [SELECT id,product__c,variance__c,cycle_count__R.location__C 
                                            FROM cycle_count_line__c 
                                            WHERE cycle_count__c IN :setofcycleCount]){
                liStockedProduct.add(
                    new SVMXC__Stock_Adjustment__c(SVMXC__Product__c = varloop.product__c,
                            SVMXC__Location__c = varloop.cycle_count__R.location__c,
                            SVMXC__New_Quantity2__c = varloop.variance__c,
                            SVMXC__Adjustment_Type__c='Missing',
                            cycle_count_line__c = varloop.id,
                            Integration_Status__c = 'New'
                    )
                );                                        
            }
            
            if(!liStockedProduct.isEmpty()){
                insert liStockedProduct;
            }
        }
    }
}