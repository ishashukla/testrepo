/**================================================================      
* Appirio, Inc
* Name: ProductComponentController
* Description: Controller class for ProductComponent Component(Original(T-481991))
* Created Date: [01/03/2016]
* Created By: [Sahil Batra] (Appirio)
* Date Modified      Modified By      Description of the update
  21 June 2016       Isha Shukla      Modified(T-512565)
  30 Sept 2016       Prakarsh Jain    Modified(T-541248) Added functionality to duplicate installed products, added methods saveAndContinue
                                      showPopup, cancelPopUp, delProduct  
==================================================================*/
public class ProductComponentController {
    public Map<Id,String> prodIdtoDescriptionMap{get;set;}
    public boolean showProductSection{get;set;}
    public Id recordId;
    public Id baseId;
    public Id selectedProduct{get;set;}
    public List<InstalledProduct__c> installedProductList{get;set;}
    public InstalledProduct__c installedProduct{get;set;}
    public boolean showProduct{get;set;} 
    public String val{get;set;}
    public boolean isEditable{get;set;}
    public Install_Base__c asset{get;set;}
    public List<InstalledProduct__c> delList = new List<InstalledProduct__c>();
    public boolean validated{get;set;}
    public String editableField{get;set;}
    public boolean showInput{get;set;}
    public String selectedId{get;set;}
    public String businessUnit{get;set;}
    public String systemName{get;set;}
    public Map<String,String> apiToLabel{get;set;}
    public String fieldQuery;
    public List<String> selectedFields{get;set;}
    public boolean displayPopup {get; set;}   
    public Integer quantity;    
    public List<InstalledProduct__c> tempIPList{get;set;}    
    public Map<Integer,List<InstalledProduct__c>> mapIntegerToInstallProduct{get;set;}
    public Map<String,String> mapNameToMsg;
    public String sltProducts{get;set;}
    public Integer keyVal{get;set;}
    public Integer keyValDel{get;set;}
    public Boolean showButtons{get;set;}
    public Map<Integer,List<InstalledProduct__c>> mapIntegerToIPListTemp;
    public Map<Integer,List<InstalledProduct__c>> mapIntegerToIPListTempCancel;
    public Boolean displayIntermediatePopup{get;set;}
    public String dupQuantity{get;set;}
    
    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.InstalledProduct__c.FieldSets.Installed_Product_Columns.getFields();
    }
    public id getrecordId(){
        return recordId;
    }
    public void setrecordId(id s){
        if(s!=null){
            recordId = s;
            populateAccountorOpportunity();
        }
    }
    public ProductComponentController(){
        quantity = 0;
        prodIdtoDescriptionMap = new Map<Id,String>();
        fieldQuery = '';
        selectedFields = new List<String>();
        apiToLabel = new Map<String,String>();
        showProductSection = false;
        businessUnit = '';
        mapIntegerToInstallProduct = new Map<Integer,List<InstalledProduct__c>>();
        mapNameToMsg = new Map<String,String>();
        mapIntegerToIPListTemp = new Map<Integer,List<InstalledProduct__c>>();
        User userDetails = [SELECT Division 
                            FROM User
                            WHERE Id = :UserInfo.getUserId()];
        if(userDetails != null && !String.isBlank(userDetails.Division)) {
            businessUnit = userDetails.Division.replaceAll(';','*');
        }
        selectedId = '';
        systemName='';
        validated = true;
        showInput = true;
        editableField = 'Custom_Name__c,Serial__c,Quantity__c,Expiration_Date__c';
        asset = new Install_Base__c();
        asset.ownerId = UserInfo.getUserId();
        String assetId = String.valueOf(asset.Id);
        if(installedProductList == null){
            installedProductList = new List<InstalledProduct__c>();
        }
        showProduct = false;
        val = '';
        isEditable = true;
        
        for(Schema.FieldSetMember f : this.getFields()){ 
            selectedFields.add(f.getFieldPath());
            if(!apiToLabel.containsKey(f.getFieldPath())) {
                if(f.getFieldPath().containsIgnoreCase('Delete')) {
                    apiToLabel.put(f.getFieldPath(),'Action');
                } else {
                    apiToLabel.put(f.getFieldPath(),f.getLabel());
                }
            }
        }
    }
    public void populateAccountorOpportunity(){
        Schema.DescribeSObjectResult r = Install_Base__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();
        if(recordId!=null && String.valueOf(recordId).subString(0,3) == '001'){
            asset.Account__c = recordId;
        }
        else if(recordId!=null && String.valueOf(recordId).subString(0,3) == keyPrefix && asset.Id == null){ 
            baseId = recordId;
            List<Install_Base__c> ibList = [SELECT Type__c,Account__c,  Installed_Date__c,OwnerId,Description__c, Number_of_Systems__c,  Purchase_Origin__c, Purchase_Date__c,Instrument_Status__c,System__c,System_Type__c,Contract_Status__c,Price__c FROM Install_Base__c WHERE Id =:baseId];
            if(ibList.size() > 0){
                asset = ibList.get(0);
                if(asset.Type__c == 'Stryker'){
                    showProductSection = true;
                    showInput = false;
                    System__c s = new System__c();
                    if(asset.System__c!=null){
                        s = [Select Name from System__c WHERE Id =: asset.System__c];
                        systemName = s.Name;
                        showInput = false;
                    }
                    else{
                        showInput = true;
                    }
                }
                if(installedProductList.size() == 0){
                    Set<Id> prodIds = new Set<Id>();
                    
                    String query = 'SELECT Id ';
                    for(Schema.FieldSetMember f : this.getFields()){
                        query += ' '+','+f.getFieldPath()+' '; 
                    }
                    String assetId = String.valueOf(asset.Id);
                    query += ',Product__r.Name FROM InstalledProduct__c WHERE Install_Base__c = :assetId';
                    installedProductList = Database.query(query);
                    Map<Id,List<InstalledProduct__c>> tempMap =new Map<Id,List<InstalledProduct__c>>();
                    for(InstalledProduct__c installedProduct : installedProductList){
                        if(!tempMap.containsKey(installedProduct.Product__c)){    
                            tempMap.put(installedProduct.Product__c,new List<InstalledProduct__c>());    
                        }   
                        tempMap.get(installedProduct.Product__c).add(installedProduct);
                        prodIds.add(installedProduct.Product__c);
                    }
                    if(prodIds.size() > 0){
                        prodIdtoDescriptionMap.putAll(returnDescription(prodIds));
                    }
                    if(tempMap.size() > 0){   
                        Integer index = 1;    
                        for(Id key : tempMap.keySet()){   
                            mapIntegerToInstallProduct.put(index,tempMap.get(key));   
                            index++;    
                        }   
                    }
                }
            }
        } 
        if(mapIntegerToInstallProduct.size()>0){
            showButtons = true;
        }
        else{
            showButtons = false;
        }
    }
    public void createNewProduct(){
        installedProduct = new InstalledProduct__c();
        if(selectedProduct!=null){
            installedProduct.Product__c = selectedProduct;
            installedProductList.add(installedProduct);
        }
        Map<Id,List<InstalledProduct__c>> tempMap =new Map<Id,List<InstalledProduct__c>>();
        for(InstalledProduct__c installedProduct : installedProductList){
            if(!tempMap.containsKey(installedProduct.Product__c)){    
                tempMap.put(installedProduct.Product__c,new List<InstalledProduct__c>());    
            }   
            tempMap.get(installedProduct.Product__c).add(installedProduct);
            
        }
        if(tempMap.size() > 0){   
            Integer index = mapIntegerToInstallProduct.size()+1;    
            for(Id key : tempMap.keySet()){   
                mapIntegerToInstallProduct.put(index,tempMap.get(key));   
            }   
        } 
        showProduct = false;
        val = '';
        Set<Id> tempIdSet = new Set<Id>();
        tempIdSet.add(selectedProduct);
        prodIdtoDescriptionMap.putAll(returnDescription(tempIdSet));
    }
    public PageReference save(){
        Schema.RecordTypeInfo recordTypebyNameStryker = Schema.SObjectType.Install_Base__c.getRecordTypeInfosByName().get('Stryker');
        Id recordTypeStryker = recordTypebyNameStryker.getRecordTypeId();
        Schema.RecordTypeInfo recordTypebyNameCompetitor = Schema.SObjectType.Install_Base__c.getRecordTypeInfosByName().get('Competitor');
        Id recordTypeCompetitor = recordTypebyNameCompetitor.getRecordTypeId();
        String message = '';
        boolean hasError = false;
        if(asset.System__c == null){
            message = 'Please select system'; 
            hasError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
        }
        if(!hasError){
            if(asset.Type__c == 'Stryker'){
                asset.Price__c = null;
                asset.System_Type__c = null;
                asset.Contract_Status__c = null;
                asset.recordTypeId = recordTypeStryker;
            }else{
                asset.recordTypeId = recordTypeCompetitor;
            }
            List<System__c> systemList = new List<System__c>();
            systemList = [SELECT Name FROM System__c WHERE Id =:asset.System__c];
            if(systemList.size() > 0){
                asset.Name = systemList.get(0).Name+'-IB';
            }
            upsert asset;
            List<InstalledProduct__c> listIP = new List<InstalledProduct__c>();
            if(asset.Type__c == 'Stryker'){
                if(mapIntegerToInstallProduct.size()>0){    
                    for(Integer index : mapIntegerToInstallProduct.keySet()){      
                        listIP.addAll(mapIntegerToInstallProduct.get(index));   
                    }   
                }   
                if(listIP.size()>0){    
                    for(InstalledProduct__c iprod : listIP){   
                        iprod.Install_Base__c = asset.Id;   
                        iprod.Enable_Duplicate_Product__c = false;
                        iprod.Duplicate_Quantity__c = null;
                    }   
                    upsert listIP;    
                }
            }
            List<InstalledProduct__c> delIPList = new List<InstalledProduct__c>();
            if(delList.size () > 0){
              for(InstalledProduct__c ipr : delList){
                if(ipr.Id != null){
                  delIPList.add(ipr);
                }
              }
              if(delIPList.size()>0){
                delete delIPList;
              }
            }
            PageReference pg = new PageReference('/'+recordId);
            return pg;
        }
        else{
            PageReference pg = apexpages.Currentpage();
            return pg;
        }
    }
    public PageReference cancel(){
        PageReference pg = new PageReference('/'+recordId);
        return pg;
    }
    public PageReference addProductSF1(){
        val='';
        PageReference pgRef;
        if(asset.System__c ==  null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select required system to add products'));
            return null;
        }
        else{
            if(asset.Id!=null){
                pgRef = new PageReference('/apex/InstallBaseMobilePage?ibId='+asset.Id+'&type='+asset.Type__c+'&BU='+businessUnit);
                pgRef.setRedirect(false);
                return pgRef;
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please save the Install Base Record first.'));
                return null;
            }
        }
        
    }
    public void addProduct(){
        val='';
        if(asset.System__c ==  null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select required system to add products'));
        }
        else{
            showProduct = true;
        }
    }
    public void refreshProdList(){
        installedProductList = new List<InstalledProduct__c>();
    }
    public void deleteProduct(){
        installedProductList = new List<InstalledProduct__c>();
        for(Integer index : mapIntegerToInstallProduct.keySet()){
            List<InstalledProduct__c> temp = new List<InstalledProduct__c>();
            List<InstalledProduct__c> finalList = new List<InstalledProduct__c>();
            temp = mapIntegerToInstallProduct.get(index);
            for(InstalledProduct__c bp : temp){
                if((bp.Delete__c && bp.Id !=null) || (bp.Enable_Duplicate_Product__c && bp.Id != null)){
                    delList.add(bp);
                }
                else if(!bp.Delete__c || !bp.Enable_Duplicate_Product__c){
                    finalList.add(bp);   
                }
            }
            installedProductList.addAll(finalList);
            mapIntegerToInstallProduct.put(index,finalList);
        }
    }
    public void delProduct(){
        installedProductList = new List<InstalledProduct__c>();
        List<InstalledProduct__c> temp = new List<InstalledProduct__c>();
        List<InstalledProduct__c> finalList = new List<InstalledProduct__c>();
        if(keyVal != null){
            temp = mapIntegerToInstallProduct.get(keyVal);
            finalList = mapIntegerToInstallProduct.get(keyVal);
        }
        if(temp.size()>0){
            delList.add(temp.get(keyValDel-1));
            finalList.remove(keyValDel-1);
        }
        if(finalList.size()>0){
            mapIntegerToIPListTemp.put(keyVal,finalList);
            mapIntegerToInstallProduct.put(keyVal,finalList);
        }
    }
    public void validateValues(){
        if(asset.Type__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill the type before selecting system'));
            validated = false;
        }
        else{
            validated = true;
        }
    }
    public void changeType(){
        asset.System__c = null;
        showInput = true;
        systemName='';
        if(asset.Type__c == 'Stryker'){
            showProductSection = true;
        }
        else{
            showProductSection = false;
        }
    }
    public void getSelectedRecord(){
        asset.System__c = selectedId;
        System__c s = new System__c();
        s = [Select Name from System__c WHERE Id =: selectedId];
        systemName = s.Name;
        showInput = false;
    }
    public static Map<Id,String> returnDescription(Set<Id> prodId){
        Map<Id,String> idToDesc = new Map<Id,String>();
        if(prodId.size() > 0){
            List<Product2> prodList = new List<Product2>();
            prodList = [SELECT Id,Description FROM Product2 WHERE Id =:prodId];
            if(prodList.size() > 0){
                for(Product2 prod : prodList){
                    idToDesc.put(prod.Id,prod.Description);
                }
            }
        }
        return idToDesc;
    } 
    public void changeInput(){
        showInput = true;
    }
    public void refreshPage(){
        
    }
    public void cancelPopUp(){
        displayPopup = false;
        displayIntermediatePopup = false;
    }
    
    public void showIntermediatePopup(){
      displayIntermediatePopup = true;
    }
    
    public void showPopup(){  
        if(dupQuantity.isNumeric()){
          displayIntermediatePopup = false;
	        List<String> keyList = new List<String>();
	        if(sltProducts != null && sltProducts != ''){
	            keyList = sltProducts.split(';');
	        }
	        Set<Integer> keyNumberSet = new Set<Integer>();
	        if(keyList.size()>0){
	            for(String keys : keyList){
	                keyNumberSet.add(Integer.valueOf(keys));
	            }
	        }
	        List<InstalledProduct__c> iproduct = new List<InstalledProduct__c>();
	        List<InstalledProduct__c> iproductFinal = new List<InstalledProduct__c>();
	        List<InstalledProduct__c> tempIprod = new List<InstalledProduct__c>();
	        mapIntegerToIPListTemp = new Map<Integer,List<InstalledProduct__c>>();
	        for(Integer keyNumber : keyNumberSet){
	            List<InstalledProduct__c> tempIprodList = new List<InstalledProduct__c>();
	            iproduct = mapIntegerToInstallProduct.get(keyNumber);
	            Integer counter = 0;
	            for(InstalledProduct__c ipo : iproduct){
	                if(ipo.Enable_Duplicate_Product__c){
	                    if(!mapIntegerToIPListTemp.containsKey(keyNumber)){
	                        tempIprod.add(ipo);
	                    }
	                    String customName = ipo.Custom_Name__c;
	                    counter = counter + 1;
	                    for(Integer i = 1 ; i < Integer.valueOf(dupQuantity);  i++){
	                        Integer lastCustomName;
	                        InstalledProduct__c ipTemp = new InstalledProduct__c();
	                        ipTemp.Prod_Description__c = prodIdtoDescriptionMap.get(ipo.Product__c);
	                        ipTemp.Product__c = ipo.Product__c;
	                        if(ipo.Custom_Name__c != null && ipo.Custom_Name__c != ''){
	                          Integer lengthCustomName = ipo.Custom_Name__c.length();
	                          String reversedCustomName = customName.reverse();
	                          if(reversedCustomName.startsWith('1') || reversedCustomName.startsWith('2') || reversedCustomName.startsWith('3') || reversedCustomName.startsWith('4') || reversedCustomName.startsWith('5') || reversedCustomName.startsWith('6') || reversedCustomName.startsWith('7') || reversedCustomName.startsWith('8') || reversedCustomName.startsWith('9') || reversedCustomName.startsWith('0')){
	                               String tempNumberString;
	                               Pattern pat = Pattern.compile('([0-9]+)');
	                               Matcher matcher = pat.matcher(reversedCustomName);
	                               Boolean matches = matcher.find();
	                               tempNumberString = matcher.group(1);
	                               tempNumberString = tempNumberString.reverse();
	                               lastCustomName = Integer.valueOf(tempNumberString) + 1;
	                               customName = ipo.Custom_Name__c.replaceAll('([0-9]+)', String.valueOf(lastCustomName));
	                               ipTemp.Custom_Name__c = customName;
	                          }
	                          else{
	                            ipTemp.Custom_Name__c = ipo.Custom_Name__c;
	                          }
	                        }
	                        ipTemp.Quantity__c = 1;
	                        if(counter == 1){
	                            tempIprod.add(ipTemp);
	                            tempIprodList.add(ipTemp);
	                        }
	                    } 
	                    if(mapIntegerToIPListTemp.containsKey(keyNumber)){
	                      mapIntegerToIPListTemp.get(keyNumber).addAll(tempIprodList);
	                    }
	                    else{
	                      mapIntegerToIPListTemp.put(keyNumber,tempIprodList);
	                    }
	                }         
	            }
	        }
	        tempIPList = new List<InstalledProduct__c>();
	        tempIPList.addAll(tempIprod);
	        displayPopup = true;
        }
        else{
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This is not a valid quantity.'));
        }
           
    }
    
    public void saveAndContinue(){
        for(Integer tempInt : mapIntegerToIPListTemp.keySet()){
            mapIntegerToInstallProduct.get(tempInt).addAll(mapIntegerToIPListTemp.get(tempInt));
        } 
        displayPopup = false; 
    }      
}