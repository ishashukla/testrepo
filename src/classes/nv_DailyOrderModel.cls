public class nv_DailyOrderModel {
    //@AuraEnabled
    //public String ContactRecordTypeId = Schema.getGlobalDescribe().get('Contact').getDescribe().getRecordTypeInfosByName().get('NV_Contact').getRecordTypeId();
    
    public class FilterOption{
        @AuraEnabled
        public String label;
        
        @AuraEnabled
        public Boolean isEnabled;
    }

    /*
    public class OrderData{
        @AuraEnabled
        public List<OrderWrapper> ordersBookedPrior {get;set;} 

        @AuraEnabled
        public List<OrderWrapper> ordersBookedToday {get;set;} 

        @AuraEnabled
        public List<OrderWrapper> ordersBookedAll {get;set;} 

        public OrderData(){
            ordersBookedPrior = new List<OrderWrapper>();
            ordersBookedToday = new List<OrderWrapper>();
            ordersBookedAll = new List<OrderWrapper>();
        }

        public void addOrderWrapperByDate(OrderWrapper wrapper, Date startDate, Date endDate){
            if(NV_Utility.isInRange(wrapper.OrderData.EffectiveDate, startDate, endDate)){
                ordersBookedToday.add(wrapper);
            }
            else{
                ordersBookedPrior.add(wrapper);
            }
        }

        public void addOrderWrapper(OrderWrapper wrapper){
            ordersBookedAll.add(wrapper);            
        }
    }

    public class OrderWrapper{
        @AuraEnabled
        public Order OrderData {get;set;}
        @AuraEnabled
        public Decimal BookedTotal {get;set;}
        @AuraEnabled
        public Decimal ShippedTotal {get;set;}
        @AuraEnabled
        public Date startDate, endDate;
        @AuraEnabled
        public Boolean OnHold {get;set;}
        @AuraEnabled
        public Boolean Expedited {get;set;}

        public OrderWrapper(Order record){
            this.OrderData = record;
            BookedTotal = 0;
            ShippedTotal = 0;
            this.OnHold = false;
        }
        public OrderWrapper(Order record, Date startDate, Date endDate){
            this(record);
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
    */

    @AuraEnabled
    public static List<NV_Wrapper.UserOrders> getTodayOrders(){
        Date currentDate = Date.today();
        //Date currentDate = Date.newInstance(2015,10,14);
        String todaysDay = NV_Utility.getDayOfWeek(currentDate);

      if(Test.isRunningTest()) {
        todaysDay = ControlApexExecutionFlow.theCurrentDay;
      }

        Date startDate, endDate;

        if(todaysDay == 'Mon'){
            startDate = currentDate.addDays(-2);
            endDate = currentDate.addDays(1);
        }
        else if(todaysDay == 'Sun'){
            startDate = currentDate.addDays(-1);
            endDate = currentDate.addDays(2);
        }
        else if(todaysDay == 'Sat'){
            startDate = currentDate;
            endDate = currentDate.addDays(3);
        }
        else{
            startDate = currentDate;
            endDate = currentDate.addDays(1);
        }

        return getOrdersByDate(startDate, endDate);
    }

    
    @AuraEnabled
    public static List<NV_Wrapper.UserOrders> getYesterdayOrders(){
        Date currentDate = Date.today();
        //Date currentDate = Date.newInstance(2015,10,14);
        String todaysDay = NV_Utility.getDayOfWeek(currentDate);

      if(Test.isRunningTest()) {
        todaysDay = ControlApexExecutionFlow.theCurrentDay;
      }

        Date startDate, endDate;
        if(todaysDay == 'Tue'){
            startDate = currentDate.addDays(-3);
            endDate = currentDate;
        }
        else if(todaysDay == 'Mon'){
            startDate = currentDate.addDays(-3);
            endDate = currentDate.addDays(-2);
        }
        else if(todaysDay == 'Sun'){
            startDate = currentDate.addDays(-2);
            endDate = currentDate.addDays(-1);
        }
        else{
            startDate = currentDate.addDays(-1);
            endDate = currentDate;
        }
        return getOrdersByDate(startDate, endDate);
    }
    
    @AuraEnabled
    public static String getContactRecordTypeId(){
        /*String recordTypeId = Schema.getGlobalDescribe().get('Contact').getDescribe().getRecordTypeInfosByName().get('NV_Contact').getRecordTypeId();
        if(!String.IsBlank(recordTypeId)){
            return recordTypeId;
        } else {
            return '';
        }*/
        //String recordTypeId = Schema.SObjectType.Contact.RecordTypeInfosByName.get('NV_Contact').RecordTypeId;
        String recordTypeId = [Select Id,SobjectType,DeveloperName From RecordType WHERE DeveloperName ='NV_Contact'
                                                 and SobjectType ='Contact'  
                                                 limit 1].Id;  
        if(!String.IsBlank(recordTypeId)){
            return recordTypeId;
        } else {
            return '';
        }
    }

    @AuraEnabled
    public static List<NV_Wrapper.UserOrders> getTwoDayOldOrders(){
        Date currentDate = Date.today();
        //Date currentDate = Date.newInstance(2015,10,14);
        String todaysDay = NV_Utility.getDayOfWeek(currentDate);

      if(Test.isRunningTest()) {
        todaysDay = ControlApexExecutionFlow.theCurrentDay;
      }

        Date startDate, endDate;
        if(todaysDay == 'Wed'){
            startDate = currentDate.addDays(-4);
            endDate = currentDate.addDays(-1);
        }
        else if(todaysDay == 'Tue' || todaysDay == 'Mon'){
            startDate = currentDate.addDays(-4);
            endDate = currentDate.addDays(-3);
        }
        else if(todaysDay == 'Sun'){
            startDate = currentDate.addDays(-3);
            endDate = currentDate.addDays(-2);
        }
        else{
            startDate = currentDate.addDays(-2);
            endDate = currentDate.addDays(-1);
        }
        return getOrdersByDate(startDate, endDate);
    }
    
    @AuraEnabled
    public static List<NV_Wrapper.UserOrders> getActiveOrders(){
        
        //Set Of User Ids
        Set<Id> ownersToConsider = new Set<Id>();
        User currentUser = [SELECT Id, Name, UserRole.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        Boolean isAnATMOrRM = false;
        if(currentUser!= null && currentUser.UserRole != null && currentUser.UserRole.Name != null && 
            (currentUser.UserRole.Name.contains('ATM') || currentUser.UserRole.Name.contains('Regional Manager'))){
            isAnATMOrRM = true;
            for(NV_Follow__c record: [SELECT Record_Id__c FROM NV_Follow__c 
                                            WHERE Following_User__c =: UserInfo.getUserId()
                                            AND Record_Type__c = 'User']){
                ownersToConsider.add(Id.valueOf(record.Record_Id__c));
            }
        }
        ownersToConsider.add(UserInfo.getUserId());
        system.debug('TRACE: nv_DailyOrderModel - getActiveOrders - ownersToConsider - ' + ownersToConsider);

        // Order statuses to be ignored for open pending orders.
        Set<String> statusToSkipForOpenPendingOrders = new Set<String> {
          'Cancelled'
        };

        // Extracting order records ids of followed users.
        Set<Id> ordersOwnedByFollowedUsers = new Set<Id>();
        Set<String> invoiceRelatedSalesOrderData = new Set<String>();
        Map<Id, Order> theOrdersToProcess = new Map<Id, Order>([
                                              SELECT
                                                Id, Account.Name, Status, Type, Invoice_Related_Sales_Order__c, EffectiveDate,
                                                TotalAmount, Date_Ordered__c, Order_Total__c, Hold_Type__c, Customer_PO__c,
                                                RecordType.DeveloperName, LastModifiedDate, Closed_Date__c, On_Hold__c, Invoice_Subtotal__c,
                                                Account.OwnerId, Account.Owner.FirstName, Account.Owner.LastName
                                             FROM
                                              Order
                                             WHERE
                                               Account.OwnerId IN :ownersToConsider AND
                                               Status NOT IN :statusToSkipForOpenPendingOrders AND
                                               Date_Ordered__c <= TODAY AND
                                               (Type != 'LTC Transfer' OR (Type = 'LTC Transfer' AND EffectiveDate = LAST_N_DAYS:3))]
                                            );
        for(Order theRecord : theOrdersToProcess.values()) {
          ordersOwnedByFollowedUsers.add(theRecord.Id);
          if(
            
            theRecord.RecordType.DeveloperName.equalsIgnoreCase('NV_Credit') &&
            String.isNotBlank(theRecord.Invoice_Related_Sales_Order__c)
          ) {
            invoiceRelatedSalesOrderData.add(theRecord.Invoice_Related_Sales_Order__c);
          }
        }
        system.debug('TRACE: nv_DailyOrderModel - getActiveOrders - ordersOwnedByFollowedUsers - ' + ordersOwnedByFollowedUsers);
        system.debug('TRACE: nv_DailyOrderModel - getActiveOrders - invoiceRelatedSalesOrderData - ' + invoiceRelatedSalesOrderData);

        // Determining open orders.
        Map<Id, List<Order_Invoice__c>> orderIdAndItsOpenInvoicesMapping = new Map<Id, List<Order_Invoice__c>>();
        for(Order_Invoice__c theRecord : [SELECT Id, Order__c, Invoice_Due_Date__c, Order__r.Account.OwnerId FROM Order_Invoice__c WHERE
                                          Order__c IN :ordersOwnedByFollowedUsers AND Transaction_Status__c = 'Open']) {
          if(!orderIdAndItsOpenInvoicesMapping.containsKey(theRecord.Order__c)) {
            orderIdAndItsOpenInvoicesMapping.put(theRecord.Order__c, new List<Order_Invoice__c>());
          }
          orderIdAndItsOpenInvoicesMapping.get(theRecord.Order__c).add(theRecord);
        }
        system.debug('TRACE: nv_DailyOrderModel - getActiveOrders - orderIdAndItsOpenInvoicesMapping - ' + orderIdAndItsOpenInvoicesMapping);

        // Extracting external orders if any.
        Map<String, Order> theExternalOrderMapping = new Map<String, Order>();
        for(Order theRecord : [SELECT Id, Customer_PO__c, Oracle_Order_Number__c FROM Order
                               WHERE Oracle_Order_Number__c IN :invoiceRelatedSalesOrderData]) {
          theExternalOrderMapping.put(
            theRecord.Oracle_Order_Number__c,
            theRecord
          );
        }

        // Order statuses to be ignored.
        Set<String> statusToSkip = new Set<String> {
          'Delivered',
          'Shipped',
          'Closed',
          'Cancelled',
          'Credit Applied'
        };

        // New Code Changes.
        Order theExternalOrder = null;
        Map<Id, NV_Wrapper.OrderWrapper> orderMap = new Map<Id, NV_Wrapper.OrderWrapper>();
        Set<String> recordTypeToConsiderForClosed = new Set<String> {
          'NV_Bill_Only',
          'NV_RMA'
        };
        Set<String> expeditedShippingMethods = new Set<String> {
          'FDX-Parcel-Next Day 8AM',
          'FDX-Parcel-P1 Overnight 10.30',
          'FDX-Parcel-Saturday/8AM'
        };

        // Processing order items.  
        //Start By Jai/Nishank for S-412528
        Map<String,Decimal> OrderLineStatusTotalMapLocal = new Map<String,Decimal>();
        Id oldOrderId = null ;
        integer count = 0 ;
        //End By Jai/Nishank for S-412528
        //Added Order.PartialBackOrder__c field in Query for Story S-389737 by Jyoti
        for(OrderItem record: [SELECT
                                    Id, OrderId, Sub_Total__c, Line_Amount__c, Status__c,UnitPrice,Quantity, // Added fields UnitPrice, Quantity in query by Harendra for story S-412528   
                                    Order.Id, Order.Account.Name, Order.Status,Order.PartialBackOrder__c, Order.Type, Order.Invoice_Class__c, Order.Invoice_Related_Sales_Order__c,
                                    Order.EffectiveDate, Order.TotalAmount, Order.Date_Ordered__c, Order.Order_Total__c, Order.Invoice_Subtotal__c,
                                    Order.Hold_Type__c, Order.Customer_PO__c, Order.RecordType.DeveloperName, Order.LastModifiedDate,
                                    Order.Closed_Date__c, Order.On_Hold__c, On_Hold__c, Shipped_Date__c, Hold_Description__c, Shipping_Method__c,
                                    Order.Account.OwnerId, Order.Account.Owner.FirstName, Order.Account.Owner.LastName
                                FROM
                                    OrderItem
                                WHERE
                                    Order.Account.OwnerId in : ownersToConsider
                                    AND Order.RecordType.DeveloperName <> 'NV_Credit'
                                    AND Order.Status NOT in : statusToSkip
                                    AND Order.Date_Ordered__c <= TODAY
                                    AND (Order.Type != 'LTC Transfer' 
                                         OR (Order.Type = 'LTC Transfer' 
                                             AND Order.EffectiveDate = LAST_N_DAYS:3))
                                ORDER BY 
                                   OrderID, Order.Date_Ordered__c DESC]) {
                                   system.debug(count + '---------' + record.OrderId);
                                   count++ ;
          Decimal totalPrice = 0.0;//Harendra for story S-412528                 
          if(!orderMap.containsKey(record.OrderId)) {
            orderMap.put(record.OrderId, new NV_Wrapper.OrderWrapper(record.Order));

	    /*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - Start ***/
            orderMap.get(record.OrderId).orderItemWrap = new List<NV_Wrapper.OrderItemWrapper>();
            /*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - End ***/
            
            if(
              record.Order.Status != 'Cancelled' &&
              record.Order.Status != 'Entered' &&
              record.Order.Status != 'Open'
            ) {
                orderMap.get(record.OrderId).BookedTotal += record.Order.Order_Total__c;
            }
             //Start by Harendra for story S-412528
            /* system.debug('-----------'+oldOrderId );
             system.debug('-----------'+record.OrderId);
             if(oldOrderId != null && record.OrderId != oldOrderId) {
             
             system.debug('-------1----'+OrderLineStatusTotalMapLocal );             
                OrderLineStatusTotalMapLocal = new Map<String,Decimal>();
             system.debug('------2-----'+OrderLineStatusTotalMapLocal );
            }
            oldOrderId = record.orderId;
            system.debug('----record.Order.Status ----'+record.Order.Status );
            if(
              record.Order.Status != 'Cancelled' &&
              record.Order.Status != 'Entered' &&
              record.Order.Status != 'Open' 
            ) {
                totalPrice = record.UnitPrice*record.Quantity;
                //orderMap.get(record.OrderId).TotalOpen += totalPrice; 
               system.debug('-----status-----'+record.Status__c);
                if(OrderLineStatusTotalMapLocal!=null && !OrderLineStatusTotalMapLocal.ContainsKey(record.Status__c)){
                OrderLineStatusTotalMapLocal.put(record.Status__c,totalPrice);
                }
                else{
                Decimal NetTotal = 0;
                NetTotal += OrderLineStatusTotalMapLocal.get(record.Status__c);
                NetTotal += totalPrice;
                OrderLineStatusTotalMapLocal.put(record.Status__c,NetTotal);

                }
                               system.debug('-----Mapppp-----'+OrderLineStatusTotalMapLocal);

            }*/
            //End Story S-412528
          }
          //NJ and Jai
           //Start By Jai/Nishank for S-412528
             if(oldOrderId != null && record.OrderId != oldOrderId) {
             
             system.debug('-------1----'+OrderLineStatusTotalMapLocal );             
                OrderLineStatusTotalMapLocal = new Map<String,Decimal>();
             system.debug('------2-----'+OrderLineStatusTotalMapLocal );
            }
            oldOrderId = record.orderId;
            system.debug('----record.Order.Status ----'+record.Order.Status );
          if(
              record.Order.Status != 'Cancelled' &&
              record.Order.Status != 'Entered' &&
              record.Order.Status != 'Open' 
            ) {
                totalPrice = record.UnitPrice*record.Quantity;

		/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - Start ***/
                orderMap.get(record.OrderId).orderItemWrap.add(new NV_Wrapper.OrderItemWrapper(record));
		/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - End ***/
		
                //orderMap.get(record.OrderId).TotalOpen += totalPrice; 
               system.debug('-----status-----'+record.Status__c);
                if(OrderLineStatusTotalMapLocal!=null && !OrderLineStatusTotalMapLocal.ContainsKey(record.Status__c)){
                	
                OrderLineStatusTotalMapLocal.put(record.Status__c,totalPrice);
                }
                else{
                Decimal NetTotal = 0;
                NetTotal += OrderLineStatusTotalMapLocal.get(record.Status__c);
                NetTotal += totalPrice;
                	
                OrderLineStatusTotalMapLocal.put(record.Status__c,NetTotal);
                
                }

            }
          
          
          //NJ and Jai
          
          system.debug('------OrderLineStatusTotalMapLocal-------'+OrderLineStatusTotalMapLocal);
        orderMap.get(record.OrderId).OrderLineStatusTotalMap = new Map<String,Decimal>(OrderLineStatusTotalMapLocal );
        //End By Jai/Nishank for S-412528
          System.debug('********12345'+orderMap.get(record.OrderId).OrderLineStatusTotalMap);
          if(orderIdAndItsOpenInvoicesMapping.containsKey(record.OrderId)) {
            for(Order_Invoice__c theRecord : orderIdAndItsOpenInvoicesMapping.get(record.OrderId)) {
              if(theRecord.Invoice_Due_Date__c != null && theRecord.Invoice_Due_Date__c < Date.today()) {
                orderMap.get(record.OrderId).pastDue = true;
                break;
              }
            }
          }

          // Shipped:
          // For each Order Product that has a Shipped, the SubTotal of that Order Product record should be added to the Order's Shipped Total contribution.
          // For RMA and Bill Only Orders, if the Order has a Closed Date, the Order Products on that order should added to teh Order's Shipped Total contribution. In the case of RMAs, this can be negative values
          // Per I-85593, we will only calculate on Closed status. Delivered status will also be included in the case that closed is overriden.
          if(record.Status__c == 'Shipped' || record.Status__c == 'Closed' || record.Status__c == 'Delivered') {
            // orderMap.get(record.OrderId).ShippedTotal += record.Sub_Total__c;
            if(
              record.Status__c.equalsIgnoreCase('Closed') &&
              record.Order.RecordType.DeveloperName.equalsIgnoreCase('NV_Credit') &&
              record.Line_Amount__c != null
            ) {
              orderMap.get(record.OrderId).ShippedTotal += record.Line_Amount__c;
            }
            else {
              if(record.Order.RecordType.DeveloperName != 'NV_RMA') {
                orderMap.get(record.OrderId).ShippedTotal += record.Sub_Total__c; 
              }
              else {
                orderMap.get(record.OrderId).ShippedTotal -= record.Sub_Total__c;
              }
            }
          }

          // External Order.
          if(
            String.isNotBlank(record.Order.Invoice_Related_Sales_Order__c) &&
            theExternalOrderMapping.containsKey(record.Order.Invoice_Related_Sales_Order__c)
          ) {
            theExternalOrder = theExternalOrderMapping.get(record.Order.Invoice_Related_Sales_Order__c);
            orderMap.get(record.OrderId).OrderData.Customer_PO__c = theExternalOrder.Customer_PO__c;
          }

          // On Hold.
          if(record.On_Hold__c || record.Order.On_Hold__c) {
            orderMap.get(record.OrderId).OnHold = true;
          }
          if(!String.isBlank(record.Shipping_Method__c) && expeditedShippingMethods.contains(record.Shipping_Method__c)) {
            orderMap.get(record.OrderId).Expedited = true;
          }
        }

        // Determining orders with open invoice status if any.
        if(orderIdAndItsOpenInvoicesMapping.size() > 0) {
          for(Id theOrderId : orderIdAndItsOpenInvoicesMapping.keySet()) {
            if(theOrdersToProcess.containsKey(theOrderId)) {
              orderMap.put(
                theOrderId,
                new NV_Wrapper.OrderWrapper(theOrdersToProcess.get(theOrderId))
              );

              orderMap.get(theOrderId).openOrderInvoices = orderIdAndItsOpenInvoicesMapping.get(theOrderId);
              for(Order_Invoice__c theRecord : orderIdAndItsOpenInvoicesMapping.get(theOrderId)) {
                if(theRecord.Invoice_Due_Date__c != null && theRecord.Invoice_Due_Date__c < Date.today()) {
                  orderMap.get(theOrderId).pastDue = true;
                  break;
                }
              }
            }
          }
        }

        system.debug('TRACE: nv_DailyOrderModel - getActiveOrders - orderMap - ' + orderMap);
        //Added ownersToConsider for Story S-389741 by Jyoti
        return mapUserWiseOrders(orderMap.values(), isAnATMOrRM, ownersToConsider );        
        // return orderMap.values();
    }

    private static List<NV_Wrapper.UserOrders> getOrdersByDate(Date startDate, Date endDate){
        
        Id currentUserId = UserInfo.getUserId();

        System.debug('[DD] startDate : ' + startDate);
        System.debug('[DD] endDate : ' + endDate);
        //Set Of User Ids
        Set<Id> ownersToConsider = new Set<Id>();
        User currentUser = [SELECT Id, Name, UserRole.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        Boolean isAnATMOrRM = false;
        if(currentUser!= null && currentUser.UserRole != null && currentUser.UserRole.Name !=  null && 
            (currentUser.UserRole.Name.contains('ATM') || currentUser.UserRole.Name.contains('Regional Manager'))){
            isAnATMOrRM = true;
            for(NV_Follow__c record: [SELECT Record_Id__c FROM NV_Follow__c 
                                            WHERE Following_User__c =: UserInfo.getUserId()
                                            AND Record_Type__c = 'User']){
                ownersToConsider.add(Id.valueOf(record.Record_Id__c));
            }
        }
        ownersToConsider.add(currentUserId);

        // Extracting invoice related sales order data.
        Set<String> invoiceRelatedSalesOrderData = new Set<String>();
        for(Order theRecord : [SELECT
                                Id, Invoice_Related_Sales_Order__c
                               FROM
                                Order
                               WHERE
                                 Account.OwnerId IN :ownersToConsider AND
                                 RecordType.DeveloperName = 'NV_Credit' AND
                                 Invoice_Related_Sales_Order__c != ''
        ]) {
          invoiceRelatedSalesOrderData.add(theRecord.Invoice_Related_Sales_Order__c);
        }
        system.debug('TRACE: nv_DailyOrderModel - getOrdersByDate - invoiceRelatedSalesOrderData - ' + invoiceRelatedSalesOrderData);

        // Extracting external orders if any.
        Map<String, Order> theExternalOrderMapping = new Map<String, Order>();
        for(Order theRecord : [SELECT Id, Customer_PO__c, Oracle_Order_Number__c FROM Order
                               WHERE Oracle_Order_Number__c IN :invoiceRelatedSalesOrderData]) {
          theExternalOrderMapping.put(
            theRecord.Oracle_Order_Number__c,
            theRecord
          );
        }

        // New Code Changes.
        Map<Id, NV_Wrapper.OrderWrapper> orderMap = new Map<Id, NV_Wrapper.OrderWrapper>();
        Set<String> recordTypeToConsiderForClosed = new Set<String>{'NV_Bill_Only','NV_RMA'};
        Set<String> expeditedShippingMethods = new Set<String>{'FDX-Parcel-Next Day 8AM','FDX-Parcel-P1 Overnight 10.30', 'FDX-Parcel-Saturday/8AM'};
        Order theExternalOrder = null;
        //Start By Jai/Nishank for S-412528
        Map<String,Decimal> OrderLineStatusTotalMapLocal = new Map<String,Decimal>();
        Id oldOrderId = null ;
        //End By Jai/Nishank for S-412528
        //Added Order.PartialBackOrder__c field in Query for Story S-389737
        for(OrderItem record: [SELECT
                                    Id, OrderId, Sub_Total__c, Line_Amount__c, Status__c,UnitPrice,Quantity, // Added fields UnitPrice,Quantity in query by Harendra for story S-412528   
                                    Order.Id, Order.Account.Name, Order.Status, Order.PartialBackOrder__c, Order.Type, Order.Invoice_Class__c, Order.Invoice_Related_Sales_Order__c,
                                    Order.EffectiveDate, Order.TotalAmount, Order.Date_Ordered__c, Order.Order_Total__c, Order.Invoice_Subtotal__c,
                                    Order.Hold_Type__c, Order.Customer_PO__c, Order.RecordType.DeveloperName, Order.LastModifiedDate,
                                    Order.Closed_Date__c, Order.On_Hold__c, Order.Line_Hold_Count__c, On_Hold__c, Shipped_Date__c,
                                                      Hold_Description__c, Shipping_Method__c,
                                    Order.Account.OwnerId, Order.Account.Owner.FirstName, Order.Account.Owner.LastName
                                FROM 
                                    OrderItem
                                WHERE 
                                    Order.Account.OwnerId in : ownersToConsider AND
                                    ((Order.EffectiveDate >= :startDate AND Order.EffectiveDate < :endDate)
                                        OR (Shipped_Date__c >= :startDate AND Shipped_Date__c < :endDate))
                                ORDER BY 
                                    OrderID, Order.LastModifiedDate DESC]) {
            Decimal totalPrice = 0.0;//Added by Harendra for story S-412528                          
            if(!orderMap.containsKey(record.OrderId)){
              orderMap.put(record.OrderId, new NV_Wrapper.OrderWrapper(record.Order, startDate, endDate));

		/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - Start ***/
		orderMap.get(record.OrderId).orderItemWrap = new List<NV_Wrapper.OrderItemWrapper>();
		/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - End ***/

              // Booked
              // An Order should contribute the Total Amount to the Book Total if its Order Start Date is the date of that tab.
              if(
                record.Order.Status != 'Cancelled' &&
                record.Order.Status != 'Entered' &&
                record.Order.Status != 'Open' &&
                isInRange(record.Order.EffectiveDate, startDate, endDate)
              ){
                orderMap.get(record.OrderId).BookedTotal += record.Order.Order_Total__c;
              }
               //Start by Harendra for story S-412528
              /* if(oldOrderId != null && record.OrderId == oldOrderId) {
                           
               
                OrderLineStatusTotalMapLocal = new Map<String,Decimal>();
            }
            oldOrderId = record.orderId;
              if(
                record.Order.Status != 'Cancelled' &&
                record.Order.Status != 'Entered' &&
                record.Order.Status != 'Open' &&
                isInRange(record.Order.EffectiveDate, startDate, endDate)           
              ){
                totalPrice = record.UnitPrice*record.Quantity;
                //orderMap.get(record.OrderId).TotalOpen += totalPrice; 
                if(OrderLineStatusTotalMapLocal!=null && !OrderLineStatusTotalMapLocal.ContainsKey(record.Status__c)){
                OrderLineStatusTotalMapLocal.put(record.Status__c,totalPrice);
                }
                else{
                Decimal NetTotal = 0;
                NetTotal += OrderLineStatusTotalMapLocal.get(record.Status__c);
                NetTotal += totalPrice;
                OrderLineStatusTotalMapLocal.put(record.Status__c,NetTotal);

                }
              }
              orderMap.get(record.OrderId).OrderLineStatusTotalMap = new Map<String,Decimal>(OrderLineStatusTotalMapLocal );
              System.debug('********1234567788'+orderMap.get(record.OrderId).OrderLineStatusTotalMap);*/
              //End Story S-412528
            }
            if(oldOrderId != null && record.OrderId == oldOrderId) {
                           
               
                OrderLineStatusTotalMapLocal = new Map<String,Decimal>();
            }
            oldOrderId = record.orderId;
              if(
                record.Order.Status != 'Cancelled' &&
                record.Order.Status != 'Entered' &&
                record.Order.Status != 'Open' &&
                isInRange(record.Order.EffectiveDate, startDate, endDate)           
              ){

		/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - Start ***/
              	orderMap.get(record.OrderId).orderItemWrap.add(new NV_Wrapper.OrderItemWrapper(record));
		/*** Code modified - Padmesh Soni (11/10/2016 Appirio Offshore) - Case #: 00173911 - End ***/
		
                totalPrice = record.UnitPrice*record.Quantity;
                //orderMap.get(record.OrderId).TotalOpen += totalPrice; 
                if(OrderLineStatusTotalMapLocal!=null && !OrderLineStatusTotalMapLocal.ContainsKey(record.Status__c)){
                OrderLineStatusTotalMapLocal.put(record.Status__c,totalPrice);
                }
                else{
                Decimal NetTotal = 0;
                NetTotal += OrderLineStatusTotalMapLocal.get(record.Status__c);
                NetTotal += totalPrice;
                	
                OrderLineStatusTotalMapLocal.put(record.Status__c,NetTotal);
                
                }
              }
              orderMap.get(record.OrderId).OrderLineStatusTotalMap = new Map<String,Decimal>(OrderLineStatusTotalMapLocal );
            Set<String> statusToConsider = new Set<String>{'Delivered', 'Shipped', 'Closed'};
            
            //Shipped
            //For each Order Product that has a Shipped Date that matches the Tab Date, the SubTotal of that Order Product record should be added to the Order's Shipped Total contribution.
            ////Per Issue I-185593 - all orders should be calculated on Closed status.
            if(
              record.Status__c != 'Cancelled' &&
              isInRange(record.Shipped_Date__c, startDate, endDate) &&
              statusToConsider.contains(record.Status__c)
            ) {
                if(record.Order.RecordType.DeveloperName != 'NV_RMA') {
                  orderMap.get(record.OrderId).ShippedTotal += record.Sub_Total__c;
                }
                else {
                  orderMap.get(record.OrderId).ShippedTotal -= record.Sub_Total__c;
                }
            } else {
                if(record.Order.RecordType.DeveloperName.equalsIgnoreCase('NV_Credit')) {
                    orderMap.get(record.OrderId).ShippedTotal += record.Line_Amount__c;
                }
            }

                    // External Order.
            if(
              String.isNotBlank(record.Order.Invoice_Related_Sales_Order__c) &&
              theExternalOrderMapping.containsKey(record.Order.Invoice_Related_Sales_Order__c)
            ) {
              theExternalOrder = theExternalOrderMapping.get(record.Order.Invoice_Related_Sales_Order__c);
              orderMap.get(record.OrderId).OrderData.Customer_PO__c = theExternalOrder.Customer_PO__c;
            }

            //For RMA and Bill Only Orders, if the Order has a Closed Date of the Tab Date, the Order Products on that order should added to teh Order's Shipped Total contribution. In the case of RMAs, this can be negative values
            //Per Issue I-185593 - all orders should be calculated on Closed status.
            /*if( record.Status__c != 'Cancelled' 
                && isInRange(record.Order.Closed_Date__c, startDate, endDate)){
                orderMap.get(record.OrderId).ShippedTotal += record.Sub_Total__c;
            }*/

            //On Hold
            if(record.Order.On_Hold__c){
                orderMap.get(record.OrderId).OnHold = true;
            } else if(record.Order.Line_Hold_Count__c > 0){
                orderMap.get(record.OrderId).OnHold = true;
            }
                                        
            if(!String.isBlank(record.Shipping_Method__c) && expeditedShippingMethods.contains(record.Shipping_Method__c)){
                orderMap.get(record.OrderId).Expedited = true;
            }
           
        }

        //Added ownersToConsider for Story S-389741 by Jyoti
        return mapUserWiseOrders(orderMap.values(), isAnATMOrRM, ownersToConsider );

        //return orderMap.values();
    }

    private static Boolean isInRange(Datetime dateToCompare, Date startDate, Date endDate){
        if(dateToCompare>= startDate && dateToCompare < endDate){
            return true;
        }
        else{
            return false;
        }
    }
    
    //Added Set<Id> for Story S-389741 by Jyoti 
    private static List<NV_Wrapper.UserOrders> mapUserWiseOrders(List<NV_Wrapper.OrderWrapper> orderData, Boolean isAnATMOrRM ,Set<Id> ownersToConsider ){
        //Start - S-389741 - Added by Jyoti
        String TM = 'Territory Manager%';
        Id theActiveUserId = null;
        Id currentUserId = UserInfo.getUserId();
        String currentUserName = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();

        Map<Id, NV_Wrapper.UserOrders> userOrders = new Map<Id, NV_Wrapper.UserOrders>();
        userOrders.put(currentUserId, new NV_Wrapper.UserOrders(currentUserId, currentUserName));

        String userName = currentUserName;
        for(NV_Wrapper.OrderWrapper wrapper : orderData) {
          if(wrapper.openOrderInvoices.size() > 0) {
            theActiveUserId = isAnATMOrRM ? wrapper.OrderData.Account.OwnerId : currentUserId;
            if(!userOrders.containsKey(theActiveUserId)) {
              userOrders.put(theActiveUserId, new NV_Wrapper.UserOrders(theActiveUserId, userName));
            }
            userOrders.get(theActiveUserId).pendingInvoices.add(wrapper);
          }
          else {
            if(isAnATMOrRM) {
              Id userId = wrapper.OrderData.Account.OwnerId;
              userName = wrapper.OrderData.Account.Owner.FirstName + ' ' + wrapper.OrderData.Account.Owner.LastName;
  
              if(userOrders.containsKey(userId)) {
                userOrders.get(userId).Orders.add(wrapper);
              }
              else {
                userOrders.put(userId, new NV_Wrapper.UserOrders(userId, userName));
                userOrders.get(userId).Orders.add(wrapper);
              }
            }
            else {
              userOrders.get(currentUserId).Orders.add(wrapper);
            }
          }
        }
        
        //Start - S-389741 - Added by Jyoti 
        if(isAnATMOrRM){            
            List<User> subordinates = NV_Utility.getSubordinateUsersOfSelectedRole(currentUserId, UserInfo.getUserRoleId(), TM);
            for(User user: subordinates){
                //Added ownersToConsider check in below if for Case 00166081
                if(userOrders != null && !userOrders.containsKey(user.Id) && ownersToConsider != null && ownersToConsider.contains(user.Id)){
                    userOrders.put(user.Id, new NV_Wrapper.UserOrders(user.Id, user.Name));
                }
            }  
        }
        //End - S-389741
        
        system.debug('TRACE: nv_DailyOrderModel - mapUserWiseOrders - userOrders - ' + userOrders);
        return userOrders.values();

        /*
            Map<Id, OrderData> orderDataByUser = new Map<Id, OrderData>();
            orderDataByUser.put(currentUserId, new OrderData());
            for(OrderWrapper wrapper : orderMap.values()){
                if(isAnATMOrRM){
                    Id userId = wrapper.OrderData.Account.OwnerId;
                    if(orderDataByUser.containsKey(userId)){
                        orderDataByUser.get(userId).addOrderWrapperByDate(wrapper);    
                    }
                    else{
                        orderDataByUser.put(userId, new OrderData());
                        orderDataByUser.get(userId).addOrderWrapperByDate(wrapper);  
                    }   
                }
                else{
                    orderDataByUser.get(currentUserId).addOrderWrapperByDate(wrapper);
                }
            }
        */
    }
     
}