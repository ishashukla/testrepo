/*************************************************************************************************
Created By:    Megha Agarwal 
Date:          October 15, 2015
Description:   Handler class for Contact Acct Association Trigger for Medical Division
11 May 2016    Partibha Chhimpa(Appirio)    Added updateContact method
**************************************************************************************************/
public class ContactAcctAssociationTriggerHandler {
        
    public static void beforeDelete(List<Contact_Acct_Association__c> oldContactAcctList){
        for(Contact_Acct_Association__c caa : oldContactAcctList){
            if(caa.Primary__c){
                caa.addError(System.Label.Contact_Acct_Association_Primary_Delete_Message);
            }
        }
        
    }
    public static void afterInsert(List<Contact_Acct_Association__c> newTriggerList) {
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Instruments Contact');
        Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        List<EntitySubscription> entityList = new List<EntitySubscription>();
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> contactIdEntitySet = new Set<Id>();
        updateContact(newTriggerList);
        for(Contact_Acct_Association__c tnList : newTriggerList) {
            contactIdSet.add(tnList.Associated_Contact__c);
        }
        List<Contact> contactList = new List<Contact>([SELECT Id FROM Contact WHERE Id IN :contactIdSet AND RecordTypeId = :recordTypeInstrument]);
        List<EntitySubscription> entitySubscriptionList = [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE ParentId IN: contactIdSet AND SubscriberId =: UserInfo.getUserId()];
        for(EntitySubscription entity : entitySubscriptionList){
            contactIdEntitySet.add(entity.ParentId);
        }
        if(contactList.size() > 0) {
            for(Contact conList : contactList) {
                //EntitySubscription entity = new EntitySubscription(ParentId=conList.Id,SubscriberId=UserInfo.getUserId());
                if(!contactIdEntitySet.contains(conList.Id)){
                    EntitySubscription entity = new EntitySubscription(ParentId=conList.Id,SubscriberId=UserInfo.getUserId());
                    entityList.add(entity);
                }
                
            }
        }
        if(entityList.size() > 0){
            insert entityList;
        }
    }
    /*public static void onDelete(List<Contact_Acct_Association__c> oldTriggerList) {
        Set<Id> contactId = new Set<Id>();
        for(Contact_Acct_Association__c toList : oldTriggerList) {
            contactId.add(toList.Associated_Contact__c);
        }
        if(contactId.size() > 0){
        List<EntitySubscription> entityList = [SELECT ParentId FROM EntitySubscription WHERE ParentId IN :contactId];
            if(entityList!=null && entityList.size() >  0){
                delete entityList;
            }
        }
    }*/
//  Update Contact from Contact Acct Association
        //  argument.
        //  @param List of contact Account association 
        //  @return void

        public static void updateContact(List<Contact_Acct_Association__c> ContactAcctList){
            Set<Id> setContactA = new Set<Id>();
            Set<Id> setContact = new Set<Id>();
            List<Id> listContact = new List<Id>();
            List<contact> contactUpdate = new List<contact>();
            
            Boolean flag = false;
            
            for(Contact_Acct_Association__c caa : ContactAcctList) {
                listContact.add(caa.Associated_Contact__c);
            }
            for(Contact_Acct_Association__c conAssociation : [Select id, Associated_Contact__c 
                                                              from Contact_Acct_Association__c 
                                                              where Associated_Contact__c IN :listContact]) {
                if(setContactA.contains(conAssociation.Associated_Contact__c)) {
                    setContact.add(conAssociation.Associated_Contact__c);
                }
                setContactA.add(conAssociation.Associated_Contact__c);
                                
            }
            for(Contact con : [Select id, Assoc_w_Multiple_Accounts__c from Contact where Id IN :setContact]){
               
                if(!con.Assoc_w_Multiple_Accounts__c){
                    con.Assoc_w_Multiple_Accounts__c = True;
                    contactUpdate.add(con); 
                }         
            }
            if(!contactUpdate.isEmpty()){
                update contactUpdate;
            }
        }
}