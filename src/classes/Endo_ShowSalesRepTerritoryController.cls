/*************************************************************************************************
Created By:    Used Asset(Show Filtered Accounts on a Map) modified by Sunil Gupta
Date:          Feb 14, 2014
Description  : Controller class for Endo_showSalesRepTerritory
Javascript remoting to get the Account Records with their Call Reports for a given Date Range
**************************************************************************************************/

global with sharing class Endo_ShowSalesRepTerritoryController {
    // Public Varibales.
  public static Contact currentContact{get;set;}
  //public List<Account> crAccounts {get;set;}
  
  //-----------------------------------------------------------------------------------------------
  // Constructor
  //-----------------------------------------------------------------------------------------------
  public Endo_ShowSalesRepTerritoryController(ApexPages.StandardController sc){
    currentContact = (Contact)sc.getRecord();
    
    
    //Datetime startdt = Datetime.valueOf(startDate + ' 00:00:00'); // Convert from Dates to DateTimes
    //Datetime enddt   = Datetime.valueof(endDate + ' 23:59:00'); // Dates to DateTimes
    
    //crAccounts = [SELECT Id, Name, SheepingStreet, BillingCity,BillingCountry,BillingState,BillingPostalCode,
    //(Select Id, Name, Status__c, Date_Completed__c from Call_Reports__r where CreatedDate > :startdt 
    //AND CreatedDate < :enddt) from Account order by Id];
    //system.debug('@@@' + crAccounts);
  }
  
  
  @RemoteAction
  global static List<Account> returnAccountsforDateRange(String contactId){
    //System.debug('@@@' + ApexPages.currentPage().getParameters().get('Id'));
    //System.debug('@@@' + currentContact);
    List<Account> crAccounts = new List<Account>();
    
    //Datetime startdt = Datetime.valueOf(startDate + ' 00:00:00'); // Convert from Dates to DateTimes
    //Datetime enddt   = Datetime.valueof(endDate + ' 23:59:00'); // Dates to DateTimes
    
    //crAccounts = [SELECT Id, Name, SheepingStreet, BillingCity,BillingCountry,BillingState,BillingPostalCode,
    //(Select Id, Name, Status__c, Date_Completed__c from Call_Reports__r where CreatedDate > :startdt 
    //AND CreatedDate < :enddt) from Account order by Id];
    //system.debug('@@@' + currentContact);
    crAccounts = [SELECT Id, Name, ShippingStreet, ShippingStateCode, ShippingState, ShippingPostalCode, 
                    ShippingCountryCode, ShippingCountry, ShippingCity FROM Account 
                    WHERE Id IN (SELECT Customer_Account__c FROM Stryker_Contact__c 
                                  WHERE Internal_Contact__c = :contactId)];
    
    system.debug('@@@' + crAccounts);
    return crAccounts;
  }


}