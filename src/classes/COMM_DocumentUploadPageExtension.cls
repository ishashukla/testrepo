//
// (c) 2016 Appirio, Inc.
//
//  Migrated From COMM Src
// 
//  29 February 2016,        Meghna Vijay

public class COMM_DocumentUploadPageExtension {

  public String description {
    get;
    set;
  }
  public String documenttype {
    get;
    set;
  }
  public Boolean linkToAccount {
    get;
    set;
  }
  private Opportunity opp {
    get;
    set;
  }
  public String fileName {
    get;
    set;
  }
  public Blob fileBody {
    get;
    set;
  }
  public COMM_DocumentUploadPageExtension(ApexPages.StandardController controller) {
    this.opp = (Opportunity) controller.getRecord();
  }

  // creates a new Document__c record
  private Database.SaveResult saveCustomDocument() {
    Document__c obj = new Document__c();
    obj.Opportunity__c = opp.Id;
    obj.Description__c = description;
    obj.Document_type__c = documenttype;
    obj.Link_to_Account__c = linkToAccount;
    obj.Account__c = linkToAccount == true ? opp.AccountId : null;
    // fill out custom obj fields
    return Database.insert(obj);
  }

  // create an actual Document record with the Document__c as parent
  private Database.SaveResult saveStandardDocument(Id parentId) {
    Database.SaveResult result;
    Attachment doc = new Attachment();
    doc.body = this.fileBody;
    doc.name = this.fileName;
    doc.parentId = parentId;
    // insert the document
    result = Database.insert(doc);
    // reset the file for the view state
    fileBody = Blob.valueOf(' ');
    return result;
  }

  /**
   * Upload process is:
   *  1. Insert new Document__c record
   *  2. Insert new Attachment with the new Document__c record as parent
   *  3. Update the Document__c record with the ID of the new Attachment
   **/
   
   
  public PageReference processUpload() {
    System.Savepoint sp = Database.setSavepoint();
    try {
      Database.SaveResult customDocumentResult = saveCustomDocument();
      if (customDocumentResult == null || !customDocumentResult.isSuccess()) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
          'Could not save document.'));
        return null;
      }
      Database.SaveResult documentResult = saveStandardDocument(customDocumentResult.getId());
      if (documentResult == null || !documentResult.isSuccess()) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
          'Could not save document.'));
        return null;
      } else {
        // update the custom document record with some document info
        Document__c customDocument = [select id from Document__c where id = : customDocumentResult.getId()];
        customDocument.name = this.fileName;
        customDocument.DocumentId__c = documentResult.getId();
        update customDocument;
      }

    } catch (Exception e) {
      Database.rollback(sp);
      ApexPages.AddMessages(e);
      return null;
    }

    return new PageReference('/' + opp.Id);
  }

  public PageReference back() {
    return new PageReference('/' + opp.Id);
  }
  
}