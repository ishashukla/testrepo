/**================================================================      
 * Appirio, Inc
 * Name: CreateAccountMSARecordBatchTest
 * Description: Created as per T-547784. Test class CreateAccountMSARecordBatch
 * Created Date: [10/18/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 * 1st December       Nitish Bansal    I-242624
 ==================================================================*/
@isTest
public class CreateAccountMSARecordBatchTest {
  //Method to check whether Account MSA object records are created.
  static testMethod void testCreateAccountMSARecord(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
    Id oppFlexConventionalRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();
    List<Account> accountList = TestUtils.createAccount(20, false);
    List<Account> updatedAccountList = new List<Account>();
    for(Integer i = 0 ; i<20; i++){
      Account acc = accountList.get(i);
      acc.RecordTypeId = rtByName.getRecordTypeId();
      Integer temp = Integer.valueOf('123') + i;
      acc.AccountNumber = String.valueOf(temp)+'-0';
      updatedAccountList.add(acc);
    }
    insert updatedAccountList;
    updatedAccountList[0].Instruments_Division__c = '123,124,125,126,127';
    update updatedAccountList;
    List<Master_Agreement__c> masterAgreementList = new List<Master_Agreement__c>();
    for(Account account : updatedAccountList){
      Master_Agreement__c masterAgreement = new Master_Agreement__c(Account__c = account.Id);
      masterAgreementList.add(masterAgreement);
    }
    insert masterAgreementList;
    CreateAccountMSARecordBatch createRecord = new CreateAccountMSARecordBatch();
    Test.startTest();
      database.executeBatch(createRecord); 
    Test.stopTest();
    createRecord.accountMSAList = [SELECT Id, Account__c, MSA__c, Account__r.Active_MSA__c FROM Account_MSA__c];
    system.assertEquals(createRecord.accountMSAList.size(), 5);
    for(Account_MSA__c accountMSA : createRecord.accountMSAList){
      system.assertEquals(accountMSA.Account__r.Active_MSA__c, true);
    }
  }
  //Method to check already existed Account MSA record is not created again with the same Account and Master Agreement.
  static testMethod void testDoNotCreateAccountMSARecord(){
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
    List<Account> accountList = TestUtils.createAccount(20, false);
    List<Account> updatedAccountList = new List<Account>();
    for(Integer i = 0 ; i<20; i++){
      Account acc = accountList.get(i);
      acc.RecordTypeId = rtByName.getRecordTypeId();
      Integer temp = Integer.valueOf('123') + i;
      acc.AccountNumber = String.valueOf(temp)+'-0';
      updatedAccountList.add(acc);
    }
    insert updatedAccountList;
    updatedAccountList[0].Instruments_Division__c = '123';
    update updatedAccountList;
    List<Master_Agreement__c> masterAgreementList = new List<Master_Agreement__c>();
    for(Account account : updatedAccountList){
      Master_Agreement__c masterAgreement = new Master_Agreement__c(Account__c = account.Id);
      masterAgreementList.add(masterAgreement);
    }
    insert masterAgreementList;
    List<Account_MSA__c> accountMSAList = new List<Account_MSA__c>();
    for(Integer i=0; i<20; i++){
      Account_MSA__c accountMSA = new Account_MSA__c(Account__c = updatedAccountList.get(i).Id, MSA__c = masterAgreementList.get(i).Id);
      accountMSAList.add(accountMSA);
    }
    insert accountMSAList;
    CreateAccountMSARecordBatch createRecord = new CreateAccountMSARecordBatch();
    Test.startTest();
      database.executeBatch(createRecord); 
    Test.stopTest();
    createRecord.accountMSAList = [SELECT Id, Account__c, MSA__c FROM Account_MSA__c];
    system.assertEquals(createRecord.accountMSAList.size(), 20);
  }
}