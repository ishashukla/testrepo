public class COMM_CheckListItemTriggerHandler {
  public static void beforeInsert(List<Checklist_item__c> newList) {
    populateChecklistAndSequence(newList);
  }
  public static void afterupdate(List<Checklist_item__c> newList,Map<Id,Checklist_item__c> oldMap) {
    updateSequence(newList,oldMap);
  }
  public static void populateChecklistAndSequence(List<Checklist_item__c> newList) {
    Set<Id> groupid = new Set<Id>();
    Set<Decimal> sequenceSet = new Set<Decimal>();
    for(Checklist_item__c checkitem: newList) {
      ////system.debug('Checklist   ' + checkitem.checklist__C + 'Sequence   ' + checkitem.sequence__c);
      groupid.add(checkitem.Checklist_Group__c);
      sequenceSet.add(checkitem.Sequence__c);
    }
    Map<id,id> groupChecklistMap = new Map<id,id>();
    for(Checklist_Group__c checkgroup : [Select id,name,Checklist__c 
                                          from checklist_group__c
                                          where id in: groupid]) {
      groupChecklistMap.put(checkgroup.id,checkgroup.Checklist__c);                         
    }
    Map<id,Decimal> intidMap = new Map<id,Decimal>();
    Decimal sequence;
    boolean flag = false;
    List<Checklist_item__c> sequenceListupdate = new List<Checklist_item__c>();
    for(Checklist_item__c items : [Select id,Checklist_Group__c,sequence__c
                                    from Checklist_item__c
                                    where Checklist_Group__c in: groupid Order by sequence__c DESC]) {
      if(!(intidMap.containsKey(items.Checklist_Group__c))) {
        ////system.debug('Inside if   ' + items.Checklist_Group__c);
        sequence = items.sequence__c;
        sequence++;
        intidMap.put(items.Checklist_Group__c,sequence);
      }
    }
    for(Checklist_item__c items : [Select id,Checklist_Group__c,sequence__c
                                     from Checklist_item__c
                                     where Checklist_Group__c in: groupid Order by sequence__c ASC]) {
      if((sequenceSet.Contains(items.sequence__c) || flag == true) && (items.sequence__c !=NULL)){
        ////system.debug('Second if');
        flag = true;
        items.sequence__c++;
        sequenceListupdate.add(items);
      }
    }
    ////system.debug('List getting updated ' +sequenceListupdate);
    if(sequenceListupdate != NULL) {
      update sequenceListupdate;
    }
    for(Checklist_item__c checkItem : newList) {
      checkItem.Checklist__c = groupChecklistMap.get(checkItem.Checklist_Group__c);
      if(checkItem.sequence__c == NULL) {
        ////system.debug('Inside final if');
        checkItem.sequence__c = intidMap.get(checkItem.Checklist_Group__c);
      }
    }
  }
  public static void updateSequence(List<Checklist_item__c> newList,Map<Id,Checklist_item__c> oldMap){
    System.debug('Entered here');
    Map<Id,Decimal> groupSequenceMap = new Map<Id,Decimal>();
    Set<Id> groupid = new Set<Id>();  
    Set<Id> checklistId = new Set<Id>();
    for(Checklist_item__c checkItem : newList) {
      if((checkItem.Sequence__c !=  oldMap.get(checkItem.Id).Sequence__c)) {
        groupSequenceMap.put(checkItem.Checklist_Group__c,checkItem.Sequence__c);
        //system.debug('group seuence map   '+ groupSequenceMap);
        groupid.add(checkItem.Checklist_Group__c);
        checklistId.add(checkItem.id);
        //System.debug('Current Id:   ' + checklistId);
      }
    }
    Decimal n = 1;
    List<Checklist_item__c> sequenceListupdate = new List<Checklist_item__c>();
     for(Checklist_item__c items : [Select id,Checklist_Group__c,sequence__c
                                    from Checklist_item__c
                                    where Checklist_Group__c in: groupid Order by sequence__c ASC]) {
        if(items.sequence__c == groupSequenceMap.get(items.Checklist_Group__c) && !checklistId.contains(items.id)) {
          system.debug('Inside if   ' + items.id);
          items.sequence__c =  groupSequenceMap.get(items.Checklist_Group__c) +n;
          n++;
          sequenceListupdate.add(items);
        }                               
      }
      update sequenceListupdate;
  }
}