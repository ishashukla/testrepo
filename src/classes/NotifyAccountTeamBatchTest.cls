// (c) 2015 Appirio, Inc.
//
// Class Name: NotifyAccountTeamBatchTest 
// Description: Test Class for NotifyAccountTeamBatch class.
// 
// April 13 2016, Isha Shukla  Original 
//
@isTest
private class NotifyAccountTeamBatchTest {
    static List<AccountTeamMember> accountTeamMemberList;
    static List<Account> accountList;
    @isTest static void testNotifyAccountTeamBatch() {
        Test.startTest();
        createData();
        NotifyAccountTeamBatch obj = new NotifyAccountTeamBatch();
        Database.executeBatch(obj,1);
        Test.stopTest();
    }
    public static void createData() {
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
        Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
        accountList = TestUtils.createAccount(1, False);
        accountList[0].RecordTypeId = recordTypeInstrument;
        insert accountList;
        accountTeamMemberList = createAccountTeamMember(1, accountList[0].Id,UserInfo.getUserId(),True);
       
    }
    public static List<AccountTeamMember> createAccountTeamMember(Integer atmCount,Id accountId ,Id userId,Boolean isInsert) {
        accountTeamMemberList = new List<AccountTeamMember>();
        for(Integer i = 0; i < atmCount; i++) {
            AccountTeamMember accountTeamMemberRecord = new AccountTeamMember(UserId = userId, AccountId = accountId);
            accountTeamMemberList.add(accountTeamMemberRecord);
        }
        if(isInsert) {
            insert accountTeamMemberList;
        } 
        return accountTeamMemberList;
    }
}