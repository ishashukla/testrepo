public with sharing class MyStrategicAccountsController {
    private final Account acct;
    public Strategic_Account__c sa { set; get; }
    public List<Strategic_Account__c> listStrategicAccount{set;get;}
    public String url {get;set;}
    public Boolean hasURL{get;set;}
    public MyStrategicAccountsController(ApexPages.StandardController stdController) {
        sa = new Strategic_Account__c();
        listStrategicAccount = new List<Strategic_Account__c>();
        String accId = '';
        hasURL = false;
        url = '';
        String userId = '';
        this.acct = (Account)stdController.getRecord();
        accId = acct.Id;
        userId = UserInfo.getUserId();
        listStrategicAccount = [SELECT Id, Name, Account__c, User__c FROM Strategic_Account__c WHERE Account__c =: accId AND User__c =: userId];
        if(listStrategicAccount.size()>0){
            sa = listStrategicAccount.get(0);
            url = '/' + sa.id;
            hasURL = true;
        }
    }
}