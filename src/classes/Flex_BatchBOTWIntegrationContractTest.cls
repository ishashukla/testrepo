//
// (c) 2015 Appirio, Inc.
//
//  Test class for Flex_BatchBOTWIntegrationContract
//             and Flex_BatchBOTWIntegrationContractLine
//
// 25 Aug 2015     Naresh K Shiwani      Original
// 25 Aug 2015     Herb Whitacre         Add testing for method "calculateBillingCycleString".
// 10 Feb 2016     Herb Whitacre         Specify recordtypes for test data.
//
@isTest
private class Flex_BatchBOTWIntegrationContractTest {
  static testmethod void testExecuteFlexContractBatch(){
    // Get the recordtypes we need
    Recordtype rtFlexCustomer = [select Id from RecordType where SobjectType = 'Account' and DeveloperName = 'Flex_Customer'];
    RecordType rtFlexOppConv = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Flex_Opportunity_Conventional'];

    // Create Account, Oppty, and 1 Contract with 1 Contract Line
  	Account acnt                      = TestUtils.createAccount(1, false).get(0);
    acnt.RecordTypeId                 = rtFlexCustomer.Id;
    insert acnt;
    System.assertNotEquals(acnt.Id, null);
    TestUtils.createCommConstantSetting();
    Opportunity opp                   = TestUtils.createOpportunity(1, acnt.id, false).get(0);
    opp.RecordTypeId                  = rtFlexOppConv.Id;
    insert opp;
    System.assertNotEquals(opp, null);
    System.assertNotEquals(opp.Id, null);
  	
    // First contract covers most conditions
    // and uses constructor with no params
  	Flex_Contract__c contract             = new Flex_Contract__c();
    contract.Account__c                   = acnt.Id;
    contract.Opportunity__c               = opp.Id;
    contract.Name                         = opp.Name;
    contract.Scheduled_Maturity_Date__c   = System.today();
    contract.Status__c                    = 'Active';
    contract.Lender_Name__c               = 'DLL';
    contract.Deal_Type__c                 = 'Lease';
    contract.Lease_Type__c                = 'OL';
    contract.BOTW_Tax_Code__c             = '99'; //code for UT
    contract.Term__c                      = 48;
    contract.Payment_Frequency__c         = 'Monthly';
    contract.First_Payment_Due__c         = 'In Advance';
    contract.End_Of_Term__c               = '$1 out';
    contract.Commence_Date__c             = System.today();
    contract.First_Payment_Due_Date__c    = System.today().addDays(30);
    contract.Misc_Billable_Amount__c      = 1.00;
    contract.Service_by_BOTW__c           = true;
    contract.BOTW_Integration_Status__c   = null;
    insert contract;
    System.assertNotEquals(contract, null);
    System.assertNotEquals(contract.id, null);
    
    Flex_Misc_Invoiced_Item__c flexMisc   = new Flex_Misc_Invoiced_Item__c();
    flexMisc.Flex_Contract__c             = contract.id;
    insert flexMisc;
    System.assertNotEquals(flexMisc, null);

    Flex_Misc_Invoiced_Item__c flexMisc1  = new Flex_Misc_Invoiced_Item__c();
    flexMisc1.Flex_Contract__c            = contract.id;
    flexMisc1.Miscellaneous_Invoiced_Type__c     = 'Other';
    flexMisc1.Other_Misc_Inv_Type_Description__c = 'Freight Not Financed';
    insert flexMisc1;
    System.assertNotEquals(flexMisc1, null);

    Flex_Contract_Line__c flexConLine     = new Flex_Contract_Line__c();
    flexConLine.Flex_Contract__c          = contract.id;
    flexConLine.Opportunity__c            = opp.id;
    flexConLine.Location_Account__c       = acnt.id;
    flexConLine.State_Tax_Amt__c           = 1.00;
    flexConLine.State_Tax_Pct__c           = 1.00;
    insert flexConLine;
    System.assertNotEquals(flexConLine, null);
    
    // Test the main execution
    Test.startTest();
    Database.executeBatch(new Flex_BatchBOTWIntegrationContract(),1);
      
    
    // Second contract covers special conditions not covered by first
    // and uses constructor with list of id's as param
    Flex_Contract__c contract1             = new Flex_Contract__c();
    contract1.Account__c                   = acnt.Id;
    contract1.Opportunity__c               = opp.Id;
    contract1.Name                         = opp.Name;
    contract1.Term__c                      = 60;
    contract1.Payment_Frequency__c         = 'Irregular';
    contract1.First_Payment_Due__c         = 'Net 30';
    contract1.BOTW_Tax_Code__c             = '150';
    contract1.End_Of_Term__c               = 'FMV';
    insert contract1;
    
    Flex_Contract_Line__c flexConLine1    = new Flex_Contract_Line__c();
    flexConLine1.Flex_Contract__c         = contract1.id;
    flexConLine1.Opportunity__c           = opp.id;
    flexConLine1.Location_Account__c      = acnt.id;
    insert flexConLine1;
    System.assertNotEquals(flexConLine1, null);

    List<String> lst1 = new List<String>();
		lst1.add(contract1.id);
    Database.executeBatch(new Flex_BatchBOTWIntegrationContract(lst1),1);

    // Test ad-hoc file Attachment method
    Flex_BatchBOTWIntegrationContract fbb = new Flex_BatchBOTWIntegrationContract();
    Integer qty = fbb.createFileAttachments(new List<string>{contract.Id});
    System.assertEquals(1, qty);
          
    Test.stopTest();
  }
    
  static testmethod void testCalculateBillingCycleString(){
    // Provide Test Coverage for various combinations of payment month & frequency.
    // And asserting the resulting pattern is accurate.
    // Test method added by H.Whitacre 2015-08-25.
    Test.startTest();
    
    Flex_BatchBOTWIntegrationContract cls = new Flex_BatchBOTWIntegrationContract();
    String resp = '';
    // signature:  cls.calculateBillingCycleString(firstPaymentDueDate, paymentFrequency);
    // paymentFreqs: {'Monthly', 'Quarterly', 'Semi-annually', 'Annually', 'Irregular'};
    
    // Monthly
    resp = cls.calculateBillingCycleString(Date.newInstance(2015, 1, 1), 'Monthly');
    System.assert(resp == '1|1|1|1|1|1|1|1|1|1|1|1');

    // Quarterly
	resp = cls.calculateBillingCycleString(Date.newInstance(2015, 2, 1), 'Quarterly');
    System.assert(resp == '0|1|0|0|1|0|0|1|0|0|1|0');
	resp = cls.calculateBillingCycleString(Date.newInstance(2015, 7, 1), 'Quarterly');
    System.assert(resp == '1|0|0|1|0|0|1|0|0|1|0|0');
      
    // Semi-annually
	resp = cls.calculateBillingCycleString(Date.newInstance(2015, 3, 1), 'Semi-annually');
    System.assert(resp == '0|0|1|0|0|0|0|0|1|0|0|0');
	resp = cls.calculateBillingCycleString(Date.newInstance(2015, 12, 1), 'Semi-annually');
    System.assert(resp == '0|0|0|0|0|1|0|0|0|0|0|1');
      
    // Annually
	resp = cls.calculateBillingCycleString(Date.newInstance(2015, 4, 1), 'Annually');
    System.assert(resp == '0|0|0|1|0|0|0|0|0|0|0|0');

    // Irregular
	resp = cls.calculateBillingCycleString(Date.newInstance(2015, 1, 1), 'junk value');
    System.assert(resp == '');

    Test.stopTest();
  }
    
  static testmethod void testSchedulableInterface() {
    // Provide test coverage for the Schedulable interface
    Test.startTest();

    String CRON_EXP = '0 0 0 15 3 ? 2025';
    String jobId = System.schedule('Test_Cron_BatchBOTWIntegration',
                CRON_EXP, new Flex_BatchBOTWIntegrationContract());
      
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                      FROM CronTrigger WHERE id = :jobId];

    System.assertEquals(CRON_EXP, ct.CronExpression);
    System.assertEquals(0, ct.TimesTriggered);
    System.assertEquals('2025-03-15 00:00:00', String.valueOf(ct.NextFireTime));

    Test.stopTest();
  }
  
}