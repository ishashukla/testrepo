// (c) 2016 Appirio, Inc.
//
// Class Name: Intr_CreateOpportunity_WebService - Web Service to Create Opportunity and Opportunity Team
//
// July 05 2016, Shreerath Nair  Original (T-517306)
@isTest
private class Intr_CreateOpportunity_WebService_Test {
	
	static Account acc;
    static Opportunity opp;
    static List<User> usrList;
    
    static testMethod void testCreateOpportunityWebService() {
    
    	usrList = TestUtils.createUser(1,'Instruments ProCare Sales Ops',false);
        usrList.get(0).Division = 'ProCare';
        insert usrList;
        System.runAs(usrList[0]) {
           
        	acc = Intr_TestUtils.createAccount(1, true).get(0); 
        	Test.startTest();
        		Profile prof = (Profile)Intr_CreateOpportunity_WebService.checkProfile(usrList[0].profileId); 
        		Schema.DescribeSObjectResult opptyRecordType = Schema.SObjectType.Opportunity; 
            Map<String,Schema.RecordTypeInfo> opptyRecordTypeMap = opptyRecordType.getRecordTypeInfosByName(); 
            Id rtId = opptyRecordTypeMap.get('Instruments Opportunity Record Type').getRecordTypeId();
            //String rtDevelopName = [SELECT DeveloperName FROM RecordType WHERE Id=:rtId].DeveloperName;
         		opp = Intr_testUtils.createOpportunity(1,acc.Id,false).get(0);
         		opp.closeDate = date.Today();
         		opp.Business_Unit__c = 'ProCare';
         		opp.OwnerId = usrList.get(0).Id;
         		opp.RecordTypeId = rtId;
         		//System.debug('<<<<<<<<<<<'+opp.RecordTypeId+'>>>>>>>>>>>>>>>'+rtId);
    	 		insert opp;   
    		
    			Opportunity opp2 = (Opportunity)Intr_CreateOpportunity_WebService.createOpportunity(opp.Name,Opp.OwnerId,opp.AccountId);
    	    
    			Opportunity[] oppList = [select Id from Opportunity where Id =: opp2.Id];
        		System.assertEquals(oppList.size(),1);
        	Test.stopTest(); 
        } 
    }
    
    
}