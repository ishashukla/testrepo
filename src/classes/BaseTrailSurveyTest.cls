// (c) 2015 Appirio, Inc.
//
// Class Name: BaseTrailSurveyTest 
// Description: Test Class for BaseTrailSurveyController class.
// 
// April 6 2016, Isha Shukla  Original 
// July 14 2016, Isha Shukla  Modified(I-225980)
@isTest
public class BaseTrailSurveyTest {
    // Testing when base trail record count is greater than zero
    @isTest static void testBasTrailSurveyController() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                List<Base_Trail_Survey__c> btsRecord = TestUtils.createBaseTrailSurvey(1,opp[0].Id,'description',true);
                List<Product2> product = TestUtils.createProduct(1, false);
                product[0].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Instruments Product Record type').getRecordTypeId();
                insert product;
                BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
                BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc[0].Id, opp[0].Id, site, True);
                List<Base_Product__c> bpRecord = TestUtils.createBaseProduct(2,btsRecord[0].Id,1000,product[0].Id ,2,false);
                bpRecord[0].External_Id__c = '987';
                bpRecord[1].External_Id__c = '9876';
                bpRecord[1].Product__c = Null;
                bpRecord[0].Oracle_Quote__c = quote.Id;
                bpRecord[1].Oracle_Quote__c = quote.Id;
                insert bpRecord;
                PageReference pageRef = Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController = new BaseTrailSurveyController();
                baseTrailController.bps = bpRecord;
                baseTrailController.bts = btsRecord[0];
                baseTrailController.priceBook = opp[0].Pricebook2Id;
                baseTrailController.selectedProduct = product[0].Id;
                baseTrailController.addBaseProduct();
                baseTrailController.populateDescription();
                System.assertEquals(btsRecord.size()>0, True);
                System.assertEquals(true,baseTrailController.oppId != Null);
            Test.stopTest();
        }
    }
    // Testing when Base trail survey records are not created 
    @isTest static void testBaseTrailSurveyControllerWhenNoBTSRecordisCreated() {  
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                PageReference pageRef1= Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef1);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController1 = new BaseTrailSurveyController();
            Test.stopTest();
        }
    }
    // Testing method UpdateTrailingImpact when Price and Quantity is not null
    @isTest static void checkUpdateTrailingImpactWhenPriceQuantityNotNull() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                List<Base_Trail_Survey__c> btsRecord = TestUtils.createBaseTrailSurvey(1,opp[0].Id,'description',true);
                List<Product2> product = TestUtils.createProduct(1, false);
                product[0].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Instruments Product Record type').getRecordTypeId();
                insert product;
                BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
                BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc[0].Id, opp[0].Id, site, True);
                List<Base_Product__c> bpRecord = TestUtils.createBaseProduct(2,btsRecord[0].Id,1000,product[0].Id ,2,false);
                bpRecord[0].External_Id__c = '987';
                bpRecord[1].External_Id__c = '9876';
                bpRecord[0].Oracle_Quote__c = quote.Id;
                bpRecord[1].Oracle_Quote__c = quote.Id;
                insert bpRecord;
                PageReference pageRef1= Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef1);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController1 = new BaseTrailSurveyController();
                baseTrailController1.updateTrailingImpact();
            Test.stopTest();
        }
        
    }
    // Testing method UpdateTrailingImpact when Price and Quantity is null
    @isTest static void checkUpdateTrailingImpactWhenPriceQuantityNull() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                List<Base_Trail_Survey__c> btsRecord = TestUtils.createBaseTrailSurvey(1,opp[0].Id,'description',true);
                List<Product2> product = TestUtils.createProduct(1, false);
                product[0].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Instruments Product Record type').getRecordTypeId();
                insert product;
                BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
                BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc[0].Id, opp[0].Id, site, True);
                List<Base_Product__c> bpRecord = TestUtils.createBaseProduct(2,btsRecord[0].Id,null,product[0].Id ,null,false);
                bpRecord[0].External_Id__c = '987';
                bpRecord[1].External_Id__c = '9876';
                bpRecord[0].Oracle_Quote__c = quote.Id;
                bpRecord[1].Oracle_Quote__c = quote.Id;
                insert bpRecord;
                PageReference pageRef1= Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef1);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController1 = new BaseTrailSurveyController();
                baseTrailController1.bps = bpRecord;
                baseTrailController1.bts = btsRecord[0];
                baseTrailController1.priceBook = opp[0].Pricebook2Id;
                baseTrailController1.updateTrailingImpact();
            Test.stopTest();
        }
        
    }
    // Testing save method call
    @isTest static void checkSave() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                List<Base_Trail_Survey__c> btsRecord = TestUtils.createBaseTrailSurvey(1,opp[0].Id,'description',true);
                List<Product2> product = TestUtils.createProduct(1, false);
                product[0].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Instruments Product Record type').getRecordTypeId();
                insert product;
                BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
                BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc[0].Id, opp[0].Id, site, True);
                List<Base_Product__c> bpRecord = TestUtils.createBaseProduct(2,btsRecord[0].Id,1000,product[0].Id ,2,false);
                bpRecord[0].External_Id__c = '987';
                bpRecord[1].External_Id__c = '9876';
                bpRecord[0].Oracle_Quote__c = quote.Id;
                bpRecord[1].Oracle_Quote__c = quote.Id;
                bpRecord[1].Product__c = Null;
                insert bpRecord;
                PageReference pageRef = Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController = new BaseTrailSurveyController();
                baseTrailController.validateProduct(bpRecord[1]);
                baseTrailController.save();
                System.assertEquals(True,baseTrailController.save() == Null);
            Test.stopTest();
        }
        
    }
    // Testing method cancel first page
    @isTest static void checkCancelfirstPage() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                List<Base_Trail_Survey__c> btsRecord = TestUtils.createBaseTrailSurvey(1,opp[0].Id,'description',true);
                List<Product2> product = TestUtils.createProduct(1, false);
                product[0].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Instruments Product Record type').getRecordTypeId();
                insert product;
                BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
                BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc[0].Id, opp[0].Id, site, True);
                List<Base_Product__c> bpRecord = TestUtils.createBaseProduct(2,btsRecord[0].Id,1000,product[0].Id ,2,false);
                bpRecord[0].External_Id__c = '987';
                bpRecord[1].External_Id__c = '9876';
                bpRecord[0].Oracle_Quote__c = quote.Id;
                bpRecord[1].Oracle_Quote__c = quote.Id;
                insert bpRecord;
                PageReference pageRef = Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController = new BaseTrailSurveyController();
                baseTrailController.cancel();
            Test.stopTest();
        }
        
    }
    // Testing method cancel second page 
    @isTest static void checkCancelSecondPage() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;        
            Test.startTest();
                PageReference pageRef = Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController = new BaseTrailSurveyController();
                baseTrailController.cancel();
            Test.stopTest();
        }
        
    }
    // Testing method deleteBaseProduct call when delete is false
    @isTest static void checkDeleteBaseProductWhenDeleteFalse() {
        
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                List<Base_Trail_Survey__c> btsRecord = TestUtils.createBaseTrailSurvey(1,opp[0].Id,'description',true);
                List<Product2> product = TestUtils.createProduct(1, false);
                product[0].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Instruments Product Record type').getRecordTypeId();
                insert product;
                BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
                BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc[0].Id, opp[0].Id, site, True);
                List<Base_Product__c> bpRecord = TestUtils.createBaseProduct(2,btsRecord[0].Id,1000,product[0].Id ,2,false);
                bpRecord[0].External_Id__c = '987';
                bpRecord[1].External_Id__c = '9876';
                bpRecord[0].Oracle_Quote__c = quote.Id;
                bpRecord[1].Oracle_Quote__c = quote.Id;
                insert bpRecord;
                PageReference pageRef = Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController = new BaseTrailSurveyController();
                baseTrailController.deleteBaseProduct();
            Test.stopTest();
        }
        
    }
    // Testing method deleteBaseProduct call when delete is true
    @isTest static void checkDeleteBaseProductWhenDeleteTrue() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            List<Account> acc = TestUtils.createAccount(1, false);
            acc[0].RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            List<Opportunity> opp = TestUtils.createOpportunity(1,acc[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            Test.startTest();
                List<Base_Trail_Survey__c> btsRecord = TestUtils.createBaseTrailSurvey(1,opp[0].Id,'description',true);
                List<Product2> product = TestUtils.createProduct(1, false);
                product[0].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Instruments Product Record type').getRecordTypeId();
                insert product;
                BigMachines__Configuration_Record__c site = TestUtils.createSiteRecord(true);
                BigMachines__Quote__c quote = TestUtils.createQuoteRecord(acc[0].Id, opp[0].Id, site, True);
                List<Base_Product__c> bpRecord = TestUtils.createBaseProduct(2,btsRecord[0].Id,1000,product[0].Id ,2,false);
                bpRecord[0].External_Id__c = '987';
                bpRecord[1].External_Id__c = '9876';
                bpRecord[0].Oracle_Quote__c = quote.Id;
                bpRecord[1].Oracle_Quote__c = quote.Id;
                bpRecord[0].Delete_Record__c = True;
                insert bpRecord;
                PageReference pageRef = Page.BaseTrailSurvey;
                Test.setCurrentPage(pageRef);
                System.currentPageReference().getParameters().put('id',String.valueOf(opp[0].Id));
                BaseTrailSurveyController baseTrailController = new BaseTrailSurveyController();
                baseTrailController.deleteBaseProduct();
            Test.stopTest();
        }
    }
    
    /*public static List<Base_Trail_Survey__c> createBaseTrailSurvey(Integer btsCount,Id oppId ,String description ,Boolean isInsert) {
        List<Base_Trail_Survey__c> btsList = new List<Base_Trail_Survey__c>();
        for(Integer i = 0; i < btsCount; i++) {
            Base_Trail_Survey__c btsRecord = new Base_Trail_Survey__c(Opportunity__c = oppId , Description__c = description);
            btsList.add(btsRecord);
        }
        if(isInsert) {
            insert btsList;
        } 
        return btsList;
    }
    
    public static List<Base_Product__c> createBaseProduct(Integer bpCount,Id btsId ,Decimal price,Id productId ,Integer quantity,Boolean isInsert) {
        List<Base_Product__c> bpList = new List<Base_Product__c>();
       
        for(Integer i = 0; i < bpCount; i++) {
            Base_Product__c bpRecord = new Base_Product__c(Base_Trail_Survey__c = btsId, Product__c = productId , Price__c = price,Quantity__c=quantity);
            bpList.add(bpRecord);
        }
        if(isInsert) {
            insert bpList;
        } 
        return bpList;
    }*/
}