/**================================================================      
* Appirio, Inc
* Name: [COMM_ShowRelatedTrialsOnAccountExtTest]
* Description: [Test class for COMM_ShowRelatedTrialsOnAccountExtension]
* Created Date: [22-11-2016]
* Created By: [Meha Simlote] (Appirio)
* Date Modified      Modified By      Description of the update
==================================================================*/
@isTest
private class COMM_ShowRelatedTrialsOnAccountExtTest {
    static Account acc;
    @isTest static void testFirst(){
        createData();
        Test.startTest();
        PageReference pageRef = Page.COMM_ShowRelatedTrialsOnAccount;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        COMM_ShowRelatedTrialsOnAccountExtension con = new COMM_ShowRelatedTrialsOnAccountExtension(sc);
        con.previous();
        con.next();
        con.sortData();
        con.first();
        con.last();
        Boolean bool = con.hasNext;
        Boolean bool2 = con.hasPrevious;        
        Test.stopTest();
    } 
    private static void createData(){
        acc = TestUtils.createAccount(1, true).get(0);
        Opportunity opportunityRecord = new Opportunity(Name='Test',StageName='test',CloseDate=System.today(),AccountId=acc.id);
        insert opportunityRecord;
        Trial__c trail = new Trial__c(Name='Test',Opportunity__c=opportunityRecord.id);
        insert trail;
    }
}