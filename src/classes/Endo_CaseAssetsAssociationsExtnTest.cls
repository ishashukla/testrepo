/*******************************************************************************
* (c) Appirio, Inc.
*
* Description: Test class for Endo_CaseAssetsAssociationsExtensions
*              Endo does not use the OOTB Asset object that Case_Asset__c supports
*              This is duplicate functionality which should be removed as soon as Endo migrates 
*              to the OOTB Asset object
*
* 17 September 2016     Nathalie Le Guay
*******************************************************************************/

@isTest
private class Endo_CaseAssetsAssociationsExtnTest {

  static testMethod void testAddAssetRow(){
    Schema.DescribeSObjectResult caseRT = Schema.SObjectType.Case; 
    Map<String,Schema.RecordTypeInfo> caseRTMap = caseRT.getRecordTypeInfosByName(); 
    Id caseRTId = caseRTMap.get('Sales Order').getRecordTypeId();
    TestUtils.createRTMapCS(caseRTId,'Sales Order','Endo');
    Account objAccount = Endo_TestUtils.createAccount(1, true).get(0);

    Asset__c objAsset = Endo_TestUtils.createAsset(1,objAccount.Id,true).get(0);

    Contact objContact = Endo_TestUtils.createContact(1, objAccount.id, true).get(0);
    
    List<Case> lstCases = Endo_TestUtils.createCase(1,objAccount.Id, objContact.Id,false);
    Case objCase = lstCases.get(0);
    objCase.RecordTypeId = caseRTId;
    insert objCase;
    List<Case_Asset__c> lstCaseAssets = new List<Case_Asset__c>();
    Case_Asset__c objJunction1 = Endo_TestUtils.createCaseAssetJunction(1, objCase.Id, objAsset.Id, true).get(0);
    lstCaseAssets.add(objJunction1);

    System.currentPageReference().getParameters().put('Id', objCase.Id);

    Test.startTest();
      ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(lstCaseAssets);
      ApexPages.currentPage().getParameters().put('Id' , objCase.Id);
      Endo_CaseAssetsAssociationsExtension ext = new Endo_CaseAssetsAssociationsExtension(ssc);
      PageReference pageRef = Page.Endo_CaseAssetsAssociations;        
      Test.setCurrentPage(pageRef);
      ext.caseObj = objCase;
      ext.rowIndex = 1;
      ext.getSelectedRecord();
      ext.addRow();
      ext.populateSerialNumber();
      ext.populateAccount();
      ext.addRow();
      ext.saveCaseAndForm();
      ext.removeRow();
      //setting caseAssets as 1
      ext.caseAssets = new List<Case_Asset__c>();
      ext.caseAssets.add(new Case_Asset__c());
      ext.rowIndex = 0;
      ext.removeRow();
      ext.csAssetId = objJunction1.Id;
      ext.populateSerialNumber();
      Case_Asset__c objQueriedJunction = [SELECT Id,Name FROM Case_Asset__c LIMIT 1];
      ext.csAssetId = null; 
      ext.caSerialNumber  = objQueriedJunction.Name;
      ext.populateSerialNumber();
      
      //setting account as null for the case
      objCase.AccountId = null;
      update objCase;
      ext.caseObj = objCase;
      ext.populateAccount();
      
    Test.stopTest();
    PageReference pg = ext.saveCaseAndForm();
    System.assert(pg != null);



  }
}