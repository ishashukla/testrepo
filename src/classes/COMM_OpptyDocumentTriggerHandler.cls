/**================================================================      
* Appirio, Inc
* Name: OpptyDocumentTriggerHandler
* Description: Trigger for Opportunity Document 
* Created Date: 25-Feb-2016 
* Created By: Nitish Bansal (Appirio, Ref:T-459652 )
*
* Date Modified      Modified By      Description of the update
* 30-Mar-2016      Deepti Maheshwari       T-487784
* 18-Apr-2016       Kirti Agarwal     I-212882 - populate the PE user on Oppty Document
* 20-Apr-2016       Nitish Bansal     I-214012 - updated the function calculateRollUpSummariesOnOppty
* 20-Apr-2016       Rahul Aeran       I-213997 - updated the function to stop updating/uploading document on closed opp
* 21-Apr-2016       Kirti Agarwal     T-496412 - populate the PE user on Oppty Document
* 09-Sep-2016       Meghna Vijay      I-233099 - Updated the sharing documents with project team
* 22-Sep-2016       Meghna Vijay      I-236425 - populate the PM user on Oppty Document COMM
* 21st Oct 2016     Nitish Bansal     I-240941 // TOO Many SOQL resolution (Proper list checks before SOQL and DML in most methods, single RT query)
* 25-Oct-2016      Shubham Dhupar     I-241339 - To populate PM and PE on Oppty Doc 
==================================================================*/

public class COMM_OpptyDocumentTriggerHandler {

  private static String PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId(); //NB - 05/09 - T-500928
  
  // Methods to be executed before record is inserted
  // @param newRecords - List to hold Trigger.new
  // @return Nothing
  public static void beforeRecordInsert(List<Document__c> newRecords){
     Set<Id> opportunityIdSet = new Set<Id>();
     Map<Id, Id> oppAndOpptyTeamMemberIdMap = new Map<Id, Id>(); 
     Map<Id, Id> projectPlanOpptyMap = new Map<Id,Id>();
     Map<Id, Id> oppAndCustomTeamMemberIdMap = new Map<Id, Id>(); 
     Set<Id> projectPlanIdSet = new Set<Id>();
     Map<Id, Id> oppAndOpptyPMIdMap = new Map<Id, Id>(); 
     for(Document__c doc : newRecords) {
       if(doc.Opportunity__c != null) {
         opportunityIdSet.add(doc.Opportunity__c);
       }
     }  
     if(opportunityIdSet.size() > 0){ 
       //SD 10/25: Added TeamMemberRole =: Constants.ROLE_PM clause
      for(OpportunityTeamMember atm : [SELECT Id , UserId, OpportunityId ,TeamMemberRole
                                  FROM OpportunityTeamMember 
                                  WHERE OpportunityId IN :opportunityIdSet
                                  AND (TeamMemberRole = :Constants.PROJECT_ENGINEER OR TeamMemberRole =: Constants.ROLE_PM)]) {
        if(atm.TeamMemberRole == Constants.PROJECT_ENGINEER) {
          oppAndOpptyTeamMemberIdMap.put(atm.OpportunityId, atm.UserId);
        }
        if(atm.TeamMemberRole == Constants.ROLE_PM) {
          oppAndOpptyPMIdMap.put(atm.OpportunityId, atm.UserId);
        }
      }
     }
     
     // MV - 09/22 - COMM I-236425 Start 
     if(opportunityIdSet.size()>0) {
       for(Opportunity opp : [SELECT Project_Plan__c FROM Opportunity WHERE Id IN: opportunityIdSet]) {
        
        projectPlanOpptyMap.put(opp.Project_Plan__c,opp.Id);
      }
     }
     if(!projectPlanOpptyMap.keyset().IsEmpty() && projectPlanOpptyMap.size() > 0) {
       for(Custom_Account_Team__c custmAcctTeam : [SELECT Id, Project__c, Team_Member_Role__c, User__r.Id
                                                   FROM Custom_Account_Team__c WHERE Project__c IN: projectPlanOpptyMap.keyset() 
                                                   AND Team_Member_Role__c =:Constants.ROLE_PM AND RecordTypeId =:PROJECT_TEAM_RECORDTYPE_ID]) {
            oppAndCustomTeamMemberIdMap.put(projectPlanOpptyMap.get(custmAcctTeam.Project__c),custmAcctTeam.User__r.Id);
      }  
     }
     ////system.debug('<<<<<<<<<<<<<'+oppAndCustomTeamMemberIdMap);
     //populate the PE user on Oppty Document
     //populate the PM user on Oppty Document - MV - 09/22 - COMM - I-236425
     if(!oppAndCustomTeamMemberIdMap.keyset().IsEmpty() || !oppAndOpptyTeamMemberIdMap.keyset().IsEmpty()) {
       for(Document__c doc : newRecords) {
       if(doc.Opportunity__c != null && (oppAndOpptyTeamMemberIdMap.containsKey(doc.Opportunity__c)||oppAndCustomTeamMemberIdMap.containsKey(doc.Opportunity__c))) {
         doc.Project_Engineer_User__c = oppAndOpptyTeamMemberIdMap.get(doc.Opportunity__c);
         doc.Project_Manager__c = oppAndCustomTeamMemberIdMap.get(doc.Opportunity__c);
       }
      }  
     } 
          // MV - 09/22 - COMM I-236425 End
          //SD 10/25 -COMM I-241339 Start
      if(!oppAndOpptyTeamMemberIdMap.keyset().IsEmpty() || !oppAndOpptyPMIdMap.keyset().IsEmpty()) {
       for(Document__c doc : newRecords) {
         if(doc.Opportunity__c != null && (oppAndOpptyTeamMemberIdMap.containsKey(doc.Opportunity__c)||oppAndOpptyPMIdMap.containsKey(doc.Opportunity__c))) {
           if(doc.Project_Engineer_User__c == NULL) {
             doc.Project_Engineer_User__c = oppAndOpptyTeamMemberIdMap.get(doc.Opportunity__c);
           }
           if(doc.Project_Manager__c == NULL) {
             doc.Project_Manager__c = oppAndOpptyPMIdMap.get(doc.Opportunity__c);
           }
         }
       }  
     }
        // SD - 10/25 - COMM I-241339 End
   preventUserToUploadDocumentOnClosedOpportunity(newRecords,null);
                         
  }
  
  // Methods to be executed before record is Deleted
  // @param oldRecords - List to hold Trigger.old 
  // @return Nothing
  public static void beforeRecordDelete(List<Document__c> oldRecords){
     preventUserToUploadDocumentOnClosedOpportunity(oldRecords,null);
     preventRecordDelete(oldRecords);
  }
  // Methods to be executed before record is updated
  // @param newRecords - List to hold Trigger.new
  // @param oldMap - map to hold Trigger.oldMap
  // @return Nothing
  public static void beforeRecordUpdate(List<Document__c> newRecords,map<Id,Document__c> oldMap){
     preventUserToUploadDocumentOnClosedOpportunity(newRecords,oldMap);
     populatePEAndPMOnOpptyDoc(newRecords); //SD 10/25:  To populate PM and PE on Oppty Doc(Ref,I-241339) 
  }
    //================================================================      
    // Name         : populatePEAndPMOnOpptyDoc
    // Description  : To populate PM and PE on Oppty Doc on Update
    // Created Date : 25th Oct 2016 
    // Created By   : Shubham Dhupar (Appirio)
    // Issue        : I-241339
    //==================================================================
  public static void populatePEAndPMOnOpptyDoc(List<Document__c> newRecords) {
    Set<Id> opportunityIdSet = new Set<Id>();
    Map<Id, Id> oppAndOpptyTeamMemberIdMap = new Map<Id, Id>(); 
    Map<Id, Id> oppAndOpptyPMIdMap = new Map<Id, Id>();
    for(Document__c doc : newRecords) {
       if(doc.Opportunity__c != null &&(doc.Project_Engineer_User__c == NULL || doc.Project_Manager__c == NULL)) {
         opportunityIdSet.add(doc.Opportunity__c);
       }
     }
     if(opportunityIdSet.size() > 0){
       for(OpportunityTeamMember atm : [SELECT Id , UserId, OpportunityId ,TeamMemberRole
                                         FROM OpportunityTeamMember 
                                         WHERE OpportunityId IN :opportunityIdSet
                                         AND (TeamMemberRole = :Constants.PROJECT_ENGINEER OR TeamMemberRole =: Constants.ROLE_PM)]) {
          if(atm.TeamMemberRole == Constants.PROJECT_ENGINEER) {
            oppAndOpptyTeamMemberIdMap.put(atm.OpportunityId, atm.UserId);
          }
          if(atm.TeamMemberRole == Constants.ROLE_PM) {
            oppAndOpptyPMIdMap.put(atm.OpportunityId, atm.UserId);
          }
        }
     }
     if(!oppAndOpptyTeamMemberIdMap.keyset().IsEmpty() || !oppAndOpptyPMIdMap.keyset().IsEmpty()) {
       for(Document__c doc : newRecords) {
         if(doc.Opportunity__c != null && (oppAndOpptyTeamMemberIdMap.containsKey(doc.Opportunity__c)||oppAndOpptyPMIdMap.containsKey(doc.Opportunity__c))) {
           if(doc.Project_Engineer_User__c == NULL) {
             doc.Project_Engineer_User__c = oppAndOpptyTeamMemberIdMap.get(doc.Opportunity__c);
           }
           if(doc.Project_Manager__c == NULL) {
             doc.Project_Manager__c = oppAndOpptyPMIdMap.get(doc.Opportunity__c);
           }
         }
       }  
     }
  }
  // Methods to be executed after record is created (Reference : T-487784)
  // @param oldRecords - List to hold Trigger.new 
  // @return Nothing 
  public static void afterInsert(List<Document__c> newRecords){
     calculateRollUpSummariesOnOppty(newRecords,null, true); // NB - 04/20 - I-214012
     removeSharingForDocuments(newRecords); //NB - 04/29- T-497457
     updateSharingForDocuments(newRecords);
  } 
  
  // Methods to be executed after record is Update (Reference : T-487784)
  // @param oldRecords - List to hold Trigger.new 
  // @return Nothing
  public static void afterUpdate(List<Document__c> newRecords, 
                                Map<ID, Document__c> oldMap) {

     calculateRollUpSummariesOnOppty(newRecords,oldMap, false);  // NB - 04/20 - I-214012

  } 
  
  // Methods to be executed before record is Deleted (Reference : T-487784)
  // @param oldRecords - List to hold Trigger.old 
  // @return Nothing
  public static void afterDelete(List<Document__c> oldRecords) {
     calculateRollUpSummariesOnOppty(oldRecords,null, false);  // NB - 04/20 - I-214012
  } 
  
  // NB - 04/20 - I-214012 Start
  // Methods to calculate roll up summary fields on opportunity deleted as part of T-487784
  // to make oppty look up relation on oppty documents  
  // @param documents - List of inserted/deleted document records 
  // @return Nothing  
  private static void calculateRollUpSummariesOnOppty(List<Document__c> documents, Map<Id, Document__c> oldMap, Boolean isInsert){   
   // if(!Constants.isTriggerExecuted){
      List<Opportunity> listOfSobj = new List<Opportunity>();
      Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>();
      List<Document__c> poDocList = new List<Document__c>();
      List<Document__c> phoenixProposalList = new List<Document__c>();
      for(Document__c doc : documents){
        if(oldMap == null){
          if(doc.Document_type__c == Constants.PO_CONST){
            poDocList.add(doc);
          }else if(doc.Document_type__c == Constants.PHOENIX_PROPOSAL_CONST){
            phoenixProposalList.add(doc);
          }
        }else{
          if(doc.Document_type__c != oldMap.get(doc.Id).Document_Type__c){
            if(doc.Document_type__c == Constants.PO_CONST
            || oldMap.get(doc.Id).Document_Type__c == Constants.PO_CONST){
               poDocList.add(doc);
            }else if(doc.Document_type__c == Constants.PHOENIX_PROPOSAL_CONST
            || oldMap.get(doc.Id).Document_Type__c == Constants.PHOENIX_PROPOSAL_CONST){
               phoenixProposalList.add(doc);
            }
          }
        }
      }
      if(phoenixProposalList.size() > 0){
          List<COMM_RollUpSummaryUtility.fieldDefinition> fieldDefinitions = new List<COMM_RollUpSummaryUtility.fieldDefinition> {
              new COMM_RollUpSummaryUtility.fieldDefinition ('COUNT', 'ID',Constants.PHEONIX_PROPOSAL_ROLLUP, 
                'PO_Approval_Status__c, Revised_PO__c, Trigger_Approval_Process__c')        
          };
          listOfSobj.addAll((List<Opportunity>)COMM_RollUpSummaryUtility.rollUpTrigger(fieldDefinitions, phoenixProposalList, Constants.DOCUMENT_OBJECT_NAME, 
                      'Opportunity__c', 'Opportunity', ' AND ' + Constants.FIELDNAME_DOCUMENT_TYPE + ' = \'' +  Constants.PHOENIX_PROPOSAL_CONST + '\'', false)); 
      }
      if(poDocList.size () > 0){     
          List<COMM_RollUpSummaryUtility.fieldDefinition> fieldDefinition2 = new List<COMM_RollUpSummaryUtility.fieldDefinition> {
              new COMM_RollUpSummaryUtility.fieldDefinition ('COUNT', 'ID', Constants.PO_DOCTYPE_ROLLUP, 
                'PO_Approval_Status__c, Revised_PO__c, Trigger_Approval_Process__c')        
          };
          listOfSobj = new List<Opportunity>();
          listOfSobj.addAll((List<Opportunity>)COMM_RollUpSummaryUtility.rollUpTrigger(fieldDefinition2, poDocList, Constants.DOCUMENT_OBJECT_NAME , 
                  'Opportunity__c', 'Opportunity', ' AND ' + Constants.FIELDNAME_DOCUMENT_TYPE + ' = \'' +  Constants.PO_CONST + '\'', false));
      }
       
      /*COMM_RollUpSummaryUtility.rollUpTrigger(fieldDefinition2, documents, Constants.DOCUMENT_OBJECT_NAME , 'Opportunity__c',
                       'Opportunity', ' AND ' + Constants.FIELDNAME_DOCUMENT_TYPE 
                      + ' = \'' +  Constants.PO_CONST + '\'', false);  */   

      //If a Purchase Order type revised opprotunity document is inserted then updating Approval Process fields for parent opportunity
      try{
          if(listOfSobj.size() > 0){
              if(isInsert){
                Opportunity tempOpportunity;
                for(Opportunity updateOpp : listOfSobj){
                  mapOpportunity.put(updateOpp.Id, updateOpp);
                }
                for(Document__c newOppDoc : documents){
                  if(newOppDoc.Opportunity__c != null && newOppDoc.Document_type__c == 'Purchase Order' && newOppDoc.Revised_Document__c){
                    if(mapOpportunity.containsKey(newOppDoc.Opportunity__c)){
                      tempOpportunity = new Opportunity();
                      tempOpportunity = mapOpportunity.get(newOppDoc.Opportunity__c);
                      tempOpportunity.Revised_PO__c = true;
                      tempOpportunity.PO_Approval_Status__c = 'Not Submitted';
                      tempOpportunity.Trigger_Approval_Process__c = true;
                      mapOpportunity.put(newOppDoc.Opportunity__c, tempOpportunity);
                    }
                  }
                }
                if(mapOpportunity.size() > 0){
                  update mapOpportunity.values();
                }
                
              } else {
                if(listOfSobj.size() > 0)
                  update listOfSobj;
              }
          }
      }Catch(DMLException ex){
        //system.debug(ex.getCause()  + 'Message : ' + ex.getMessage());
      }
   //   Constants.isTriggerExecuted = true;
   // }
  }
  // NB - 04/20 - I-214012 End

  // Methods to update sharing of opportunity/Quote documents (Reference : T-487784)
  // @param documentRecords - List of document__c records 
  // @return Nothing
  private static void updateSharingForDocuments(List<Document__c> documentRecords) {
    //PROJECT_TEAM_RECORDTYPE_ID = Schema.SObjectType.Custom_Account_Team__c.getRecordTypeInfosByName().get('Project Team').getRecordTypeId();//NB - 05/09 - T-500928
    Set<ID> opportunityIds = new Set<ID>();
    Map<ID, List<Document__c>> projectDocuments = new Map<ID, List<Document__c>>();
    Map<ID, List<Document__c>> opptyDocuments = new Map<ID, List<Document__c>>();
    Map<ID, List<Document__c>> bmQuotes = new Map<ID, List<Document__c>>();
    Map<String, Document__Share> docShareMapToInsert = new Map<String, Document__Share>();
    //Id projectPlanId;
    for(Document__c doc : documentRecords) {
      // Create a map with oppty ID as Key and List of docs associated with oppty as values
      if(doc.Opportunity__c != null) {
        if(!opptyDocuments.containsKey(doc.Opportunity__c)) {
          opptyDocuments.put(doc.Opportunity__c, new List<Document__c>());
        }
        opptyDocuments.get(doc.Opportunity__c).add(doc);
      }
      //system.debug('doc.Oracle_Quote__c'+doc.Oracle_Quote__c);
      // Create a map with quote ID as Key and List of docs associated with quote as values
      if(doc.Oracle_Quote__c != null){
       
        if(!bmQuotes.containsKey(doc.Oracle_Quote__c)) {
          bmQuotes.put(doc.Oracle_Quote__c, new List<Document__c>());
        }
        bmQuotes.get(doc.Oracle_Quote__c).add(doc);
      }
      
    }
    //system.debug('bmQuotes.size()'+bmQuotes.size());
    if(bmQuotes.size() > 0){
      for(BigMachines__Quote__c bmQuote : [SELECT OwnerID,ID,BigMachines__Opportunity__c, Owner.isActive
                                          FROM BigMachines__Quote__c 
                                          WHERE ID in :bmQuotes.keyset()]) {
        //Added opportunities associated with BMQuotes to the opportunity document
        //Map so that opportunity owners and team members will get access to
        //quote document
        if(bmQuote.BigMachines__Opportunity__c != null) {
          if(!opptyDocuments.containsKey(bmQuote.BigMachines__Opportunity__c)) {
           //system.debug('I am here in if block');
            opptyDocuments.put(bmQuote.BigMachines__Opportunity__c, bmQuotes.get(bmQuote.id));
          }else{
           //system.debug('I am here in else block');
            opptyDocuments.get(bmQuote.BigMachines__Opportunity__c).addAll(bmQuotes.get(bmQuote.id));
          }
          //system.debug('opptyDocuments'+opptyDocuments);
        }  
        
        //Provide Edit Access To Quote owners    
        for(Document__c  doc : bmQuotes.get(bmQuote.id)){
          if(!docShareMapToInsert.containsKey(doc.id + '_' + bmQuote.OwnerID) && bmQuote.Owner.isActive == true) {
            docShareMapToInsert.put(doc.id + '_' + bmQuote.OwnerID, new Document__Share(
            ParentID = doc.id, UserOrGroupID = bmQuote.OwnerID, AccessLevel = Constants.PERM_EDIT,
            rowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Quote_Document__c));
          }
        }
        //system.debug('docShareMapToInsert'+docShareMapToInsert);                             
      } 
    }
    
    if(opptyDocuments.size() > 0) {
      //Provide Edit Access To opportunity owners
      for(Opportunity oppty : [SELECT OwnerID, ID ,Owner.isActive
                                 FROM Opportunity 
                                 WHERE ID in :opptyDocuments.keyset()]) {
         if(opptyDocuments.containsKey(oppty.ID)) {
          for(Document__c  doc : opptyDocuments.get(oppty.ID)){
            if(!docShareMapToInsert.containsKey(doc.id + '_' + oppty.OwnerID) && oppty.Owner.isActive == true) {
              docShareMapToInsert.put(doc.id + '_' + oppty.OwnerID, new Document__Share(
              ParentID = doc.id, UserOrGroupID = oppty.OwnerID, AccessLevel = Constants.PERM_EDIT,
              rowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Quote_Document__c));
            }
          }
        }                            
      }
      
      //Provide Edit Access To opportunity teams                     
      for(OpportunityTeamMember team : [SELECT UserID, ID, OpportunityID,user.isActive
                                 FROM OpportunityTeamMember 
                                 WHERE OpportunityID in :opptyDocuments.keyset()]) {
        if(opptyDocuments.containsKey(team.OpportunityID)) {
          for(Document__c  doc : opptyDocuments.get(team.OpportunityID)){
            if(!docShareMapToInsert.containsKey(doc.id + '_' + team.userID) && team.user.isActive == true ) {
              docShareMapToInsert.put(doc.id + '_' + team.userID, new Document__Share(
              ParentID = doc.id, UserOrGroupID = team.UserID, AccessLevel = Constants.PERM_READ,
              rowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Quote_Document__c));
            }
          }
        }
      }
    }
    // MV - 09/12 -I-233099 Start
  if(!opptyDocuments.keyset().IsEmpty()) {
    for(Opportunity opp : [SELECT Id, Project_Plan__c FROM Opportunity WHERE Id =: opptyDocuments.keyset()]) {
      projectDocuments.put(opp.Project_Plan__c, opptyDocuments.get(opp.Id));
    }
    ////system.debug('projectDocuments'+projectDocuments);
    for(Custom_Account_Team__c projTeam : [SELECT Id, Project__c, Team_Member_Role__c, User__c ,user__r.isActive FROM Custom_Account_Team__c
                                           WHERE Project__c =:projectDocuments.keyset() AND RecordTypeId =:PROJECT_TEAM_RECORDTYPE_ID]) {
      if(projectDocuments.containsKey(projTeam.Project__c)) {
        for(Document__c  doc : projectDocuments.get(projTeam.Project__c)){
          if(!docShareMapToInsert.containsKey(doc.id + '_' + projTeam.User__c) && projTeam.user__r.isActive) {
            if(projTeam.Team_Member_Role__c!='Integration Specialist (3rd Party)'|| projTeam.Team_Member_Role__c!='Integration Specialist (Stryker)') {
              docShareMapToInsert.put(doc.id + '_' + projTeam.User__c, new Document__Share(
              ParentID = doc.id, UserOrGroupID = projTeam.User__c, AccessLevel = Constants.PERM_EDIT,
              rowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c));
            }
            else if (projTeam.Team_Member_Role__c=='Integration Specialist (3rd Party)'|| projTeam.Team_Member_Role__c=='Integration Specialist (Stryker)'
                      && doc.Document_Type__c!='Purchase Order') {
              docShareMapToInsert.put(doc.id + '_' + projTeam.User__c, new Document__Share(
              ParentID = doc.id, UserOrGroupID = projTeam.User__c, AccessLevel = Constants.PERM_EDIT,
              rowCause = Schema.Document__Share.RowCause.COMM_Opportunity_Doc_Project_Team__c));
            }
          }
        }
        ////system.debug('Sharing Record created');
        ////system.debug('docShareMapToInsert' + docShareMapToInsert);
      }                                      
    }
  }
    // MV - 09/12 -I-233099 End
    
    ////system.debug('docShareMapToInsert.size()'+docShareMapToInsert.size());
  if(docShareMapToInsert.size() > 0) {
    ////system.debug('Insertion has begin');
     Database.insert(docShareMapToInsert.values(), false);
  }                                           

}
  
  // Prevents a user from deleting combined proposal if it doesn't falls in RolesCustomSetting__c custom setting
  // @param oldRecords - List to hold Trigger.old 
  // @return Nothing
  private static void preventRecordDelete(List<Document__c> oldRecords){
    //Delete allowed if user role is in RolesCustomSetting__c 
    Boolean deleteAllowed = CombinedProposalUtil.isAllowedDeleteCP();
    Map<Id,Id> opptyIdSet = new Map<Id,Id>();
    if(!deleteAllowed){
      for(Document__c oppDoc : oldRecords){
        if(oppDoc.Document_type__c == Constants.RESTRICTED_DOCUMENT_TYPE){
          oppDoc.addError(System.Label.Please_contact_an_Administrator_to_delete_Combined_Proposals);
        }
      }
    }
    
  }
  
   // Prevents a user from uploading a document on a closed opportunity
  // @param oldRecords - List to hold Trigger.new for update/insert and Trigger.old for delete
  // @param oldMap - map to hold Trigger.oldMap
  // @return Nothing
  private static void preventUserToUploadDocumentOnClosedOpportunity(List<Document__c> newRecord, map<Id,Document__c> oldMap){ 
      Boolean isUpdate = oldMap != null ? true : false;
      map<Id,Document__c> mapOpportunityIdsToDocument = new map<Id,Document__c>();
      //This loop will itereate the new records in case of insert and update and ititerate old records for delete
      for(Document__c doc : newRecord){
         if(doc.Opportunity__c != null){
             if((doc.Oracle_Quote__c != null && doc.Document_type__c == Constants.PO_CONST)){
                if(!isUpdate || doc.Opportunity__c != oldMap.get(doc.Id).Opportunity__c || doc.Document_type__c != oldMap.get(doc.Id).Document_type__c){
                    mapOpportunityIdsToDocument.put(doc.Opportunity__c,doc);
                }   
             }else if(doc.Oracle_Quote__c == null){
                 if(!isUpdate || doc.Opportunity__c != oldMap.get(doc.Id).Opportunity__c || doc.Document_type__c != oldMap.get(doc.Id).Document_type__c){
                    mapOpportunityIdsToDocument.put(doc.Opportunity__c,doc);
                 } 
             }
         }
      }
      
      //First we need to check if the opportunity is locked or not
      if(!mapOpportunityIdsToDocument.keySet().isEmpty()){
        for(Id oppId : mapOpportunityIdsToDocument.keySet()){
            
            if(Approval.isLocked(oppId)){
                mapOpportunityIdsToDocument.get(oppId).addError(Label.Record_Locked_Error);
                mapOpportunityIdsToDocument.remove(oppId);
            }
        }  
      }
      
      
      
  }
  
  //================================================================      
    // Name: removeSharingForDocuments
    // Description: Used to remove case access for non 'Sales Operations Support' cases from 'Sales Operations Support' users
    // @param documentRecords - List to hold Trigger.new 
    // @return Nothing
    // Created Date: [04/29/2016]; T-497457
    // Created By: Nitish Bansal (Appirio)
  //==================================================================
  private static void removeSharingForDocuments(List<Document__c> documentRecords) {
    Set<Id> purchaseOrderDocIds = new Set<Id>();
    Set<Id> userIdToDelSet = new Set<Id>();

    List<Profile> intProfile = [SELECT Id, Name FROM Profile WHERE Name =: Constants.COMM_INT_SPL_PROFILE];

    if(intProfile.size() > 0){
      for(Document__c newRecord : documentRecords){
        //storing all document id for those records whose type is 'Purchase Order'
        if(newRecord.Document_type__c != null && newRecord.Document_type__c == Constants.PO_CONST){
          purchaseOrderDocIds.add(newRecord.Id);     //Set to store ID of record
        }
      }

      //Fetching all users of 'Integration Specialist - COMM' profile
      for(User intUser : [Select Id from User Where ProfileId =:intProfile.get(0).Id]){
        userIdToDelSet.add(intUser.Id);   // Set to store ID of users
      }
    }
    
    //delete oppty document share records
    if(purchaseOrderDocIds.size() > 0 && userIdToDelSet.size() > 0){
      List<Document__Share> deleteShareRecords = [SELECT Id, ParentId, UserOrGroupId, RowCause 
                                              FROM Document__Share 
                                              WHERE ParentId IN :purchaseOrderDocIds 
                                              AND UserOrGroupId IN :userIdToDelSet];
      if(deleteShareRecords.size() > 0){
        delete deleteShareRecords;
      } 
    }
  }
  
}