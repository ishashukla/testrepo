/**
Created by Najma Ateeq for Story #S-408891
Purpose - Handler class of UserTrigger

* 22 Nov   2016      Nitish Bansal     I-244792 - Empower prod sync
 */
@isTest
private class UserTriggerHandler_Test {
    Static Schema.DescribeSObjectResult caseRT = Schema.SObjectType.Case; 
    Static Map<String,Schema.RecordTypeInfo> caseRecordTypeMap = caseRT.getRecordTypeInfosByName(); 
    Static Id caseRTId = caseRecordTypeMap.get('Sales Order').getRecordTypeId();
    static{
        createLicenseManagement();
        
    }
    static testMethod void myUnitTest() {
        List<Account> accList = TestUtils.createAccount(1,true);
        List<Contact> conList = TestUtils.createContact(2,accList.get(0).id,false);
        conlist.get(0).Resource_ID__c = '21345';
        conlist.get(1).Resource_ID__c = '21355';
        Insert conList;
        User u = TestUtils.createUser(1, null, false).get(0);
        u.license_Allocation__c = 'Flex';
        try{
        insert u;
        }catch(Exception e){
            System.assert(e.getMessage().contains('The licence managers on License Management record for'));
        }
        User u1 = TestUtils.createUser(1, null, false).get(0);
        u1.license_Allocation__c = 'Endo';
        u1.Sales_Rep_ID__c = '21345';
        insert u1;
        Custom_Account_Team__c actList = TestUtils.createCustomAccountTeam(accList.get(0).id,conList.get(0).id, u1.id,'Sales Rep',false);
        actList.ResourceID__c = '21355';
        Insert actList;
        u1.Sales_Rep_ID__c = '21355';
        u1.License_Allocation__c = null;
       
        update u1;
        Test.Starttest();
        u1.Sales_Rep_ID__c = '';
        update u1;
        Test.Stoptest();
        License_Management__c li = [Select Lic_In_Use__c from License_Management__c where Business_Unit__c = 'IS' limit 1];
        system.assertEquals(li.Lic_In_Use__c,null);
    }
    static testMethod void myUnitTestValidationRule() {
       // createLicenseManagement();
        User u = TestUtils.createUser(1, 'Business Admin - NV', false).get(0);
        u.license_Allocation__c = 'Flex';
        insert u;
        Execute_User_Validation_Rule_for_Profile__c custS = Execute_User_Validation_Rule_for_Profile__c.getInstance(u.ProfileId);
        custS.Run_BA_Validation_Rule__c = false;
        custS.Run_Required_Validation_Rule__c = false;
        insert custS;
        System.runAs(u){
        User u1 = TestUtils.createUser(1, 'Business Admin - NV', false).get(0);
        u1.license_Allocation__c = 'Endo';
        try{
            insert u1;
            }catch(Exception e){
                //System.assert(e.getMessage().contains('Please select valid License Allocation field'));
            }
        }
      
    }
    private static void createLicenseManagement(){
        String pId = UserInfo.getProfileId();
        Execute_User_Validation_Rule_for_Profile__c custS = Execute_User_Validation_Rule_for_Profile__c.getInstance();
        custS.Run_BA_Validation_Rule__c = false;
        custS.Run_Required_Validation_Rule__c = false;
        insert custS;
        License_Management__c li = new License_Management__c();
        li.Business_Unit__c = 'Flex';
        insert li;
        
        License_Management__c li1 = new License_Management__c();
        li1.Business_Unit__c = 'Endo';
        li1.Lic_Manager_1__c = UserInfo.getUserId();
        insert li1;
        
        License_Management__c li2 = new License_Management__c();
        li2.Business_Unit__c = 'IS';
        li2.Lic_Manager_1__c = UserInfo.getUserId();
        insert li2;
        
        
    }
   
    /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - Start ***/
    //Code moved from Endo_UserTriggerHandlerTest to this current class
    public static testmethod void alignSalesSupportAgent(){
        TestUtils.createRTMapCS(caseRTId,'Sales Order','ENDO');
        List<Case> toBeUpdateCase=new LIST<Case>();
        List<Account> toBeUpdateAccount=new LIST<Account>();
        
        List<User> testUsers=Endo_TestUtils.createUser(1, 'Endo - Sales/Samples Support', false);
        
        List<Account> testAccounts=Endo_TestUtils.createAccount(1, true);
        
        
        toBeUpdateAccount.add(new Account(Id=testAccounts.get(0).Id,Endo_Region_Name__c='region1'));
        update toBeUpdateAccount;
        
        List<Case> testCases=Endo_TestUtils.createCase(1, testAccounts.get(0).Id, null, false);
        testCases.get(0).status='new';
        testCases.get(0).recordtypeid = caseRTId;
        Test.StartTest();
        insert testCases;
        List<Contact> testContacts=Endo_TestUtils.createContact(1, testAccounts.get(0).Id, false);
        testContacts.get(0).ENDO_Region_Name__c='region1';
        insert testContacts;
        
        testUsers.get(0).title='Sales Support Agent';
        testUsers.get(0).Endo_Support_Regions__c='region1';
        
        insert testUsers;
        Test.StopTest();
        List<Case> cases=[Select Id,Sales_Support_Agent__c,Top_35_Sales_Rep__c,Samples_Agent__c from Case where region__c= 'region1']; 
        List<Contact> contacts=[Select Id,Sales_Support_Agent__c,Samples_Agent__c from Contact where ENDO_Region_Name__c= 'region1']; 
        
        System.assert(cases.get(0).Sales_Support_Agent__c==testUsers.get(0).Id);
        System.assert(contacts.get(0).Sales_Support_Agent__c==testUsers.get(0).Id);
        
        User updatedUser=new User(Id=testUsers.get(0).Id,Endo_Support_Regions__c='region2');
        
        update updatedUser;
        String caseID=cases.get(0).Id;
        String contactID=contacts.get(0).Id;
        cases= Database.query('Select Id,Sales_Support_Agent__c,Top_35_Sales_Rep__c,Samples_Agent__c from Case where Id= :caseID');
        contacts=Database.query('Select Id,Sales_Support_Agent__c,Samples_Agent__c from Contact where Id= :contactID');
        System.assert(cases.get(0).Sales_Support_Agent__c==null);
        System.assert(contacts.get(0).Sales_Support_Agent__c==null);
        
        
    }
    
    
    public static testmethod void alignSampleSupportAgent(){
        TestUtils.createRTMapCS(caseRTId,'Sales Order','ENDO');
        List<Case> toBeUpdateCase=new LIST<Case>();
        List<Account> toBeUpdateAccount=new LIST<Account>();
        
        List<User> testUsers=Endo_TestUtils.createUser(1, 'Endo - Sales/Samples Support', false);
        
        List<Account> testAccounts=Endo_TestUtils.createAccount(1, true);
        
        toBeUpdateAccount.add(new Account(Id=testAccounts.get(0).Id,Endo_Region_Name__c='region1'));
        update toBeUpdateAccount;
        
        List<Case> testCases=Endo_TestUtils.createCase(1, testAccounts.get(0).Id, null, false);
         testCases.get(0).status='new';
         testCases.get(0).recordtypeid = caseRTId;
        Test.StartTest();
        insert testCases;
        List<Contact> testContacts=Endo_TestUtils.createContact(1, testAccounts.get(0).Id, false);
        testContacts.get(0).ENDO_Region_Name__c='region1';
        insert testContacts;
        
        testUsers.get(0).title='Sample Support Agent';
        testUsers.get(0).Endo_Support_Regions__c='region1';
        
        insert testUsers;
        test.StopTest();
        List<Case> cases=[Select Id,Sales_Support_Agent__c,Top_35_Sales_Rep__c,Samples_Agent__c from Case where region__c= 'region1']; 
        List<Contact> contacts=[Select Id,Sales_Support_Agent__c,Samples_Agent__c from Contact where ENDO_Region_Name__c= 'region1'];
        
       // System.assert(cases.get(0).Samples_Agent__c==testUsers.get(0).Id);
        //System.assert(contacts.get(0).Samples_Agent__c==testUsers.get(0).Id);
        
        User updatedUser=new User(Id=testUsers.get(0).Id,Endo_Support_Regions__c='region2');
        
        update updatedUser;
        
        String caseID=cases.get(0).Id;
        String contactID=contacts.get(0).Id;
        cases= Database.query('Select Id,Sales_Support_Agent__c,Top_35_Sales_Rep__c,Samples_Agent__c from Case where Id= :caseID');
        contacts=Database.query('Select Id,Sales_Support_Agent__c,Samples_Agent__c from Contact where Id= :contactID');
        
       // System.assert(cases.get(0).Samples_Agent__c==null);
       // System.assert(contacts.get(0).Samples_Agent__c==null);

    
    }
    
    public static testmethod void changeAGWithStatusTest(){
        
        List<User> testUsersUpdate = new List<User>();
        List<User> testUsers = Endo_TestUtils.createUser(1, 'Endo - Sales/Samples Support', false);
        testUsers.get(0).Status__c = 'Ready';
        testUsers.get(0).Division = 'Endoscopy';
        insert testUsers;
        
        List<Assignment_Group_Name__c> groups = Endo_TestUtils.createAssignmentGroup(1, true);
        Endo_TestUtils.createAssignmentGroupMember(testUsers.get(0).Id, groups.get(0).Id, true);
        
        
        List <Assignment_Groups__c> testMembersUpdate = [Select Id, Active__c FROM Assignment_Groups__c  where User__c =:testUsers.get(0).id];
        
        system.assertequals(testMembersUpdate.get(0).Active__c, 'True');
        testUsers.get(0).status__c='Logged Off';
        update testUsers;
        
        testMembersUpdate = [Select Id, Active__c FROM Assignment_Groups__c  where User__c =:testUsers.get(0).id];
        system.assertequals(testMembersUpdate.get(0).Active__c, 'False');
        
    }
    /*** Code modified - Padmesh Soni (10/03/2016 Appirio Offshore) - S-441565 - End ***/
}