/*************************************************************************************************
Created By:    Pratibha
Date:          July 7, 2016
Description  : Handler class for Button Email_Follow_Up_for_Sales on Task
July 7, 2016      Pratibha|Appirio    T-497272| Original
8 August,2016     Pratibha|Appirio    I-227723| Added Contact,Call Type and Account Names
17 Sept,2016      Pankaj|Appirio      I-234589| Updated sendEmail method, Added Endo_Customer_Care_Team Label in Html Body
**************************************************************************************************/
global class SendEmailtoTaskContact{
  //--------------------------------------------------
  // Mthod to Send Email to Task's Internal Contact
  //---------------------------------------------------
  webservice static void sendEmail(Id taskId){
  
      String body;
      String subjectLine;
      Date dueDate;
      
      // Getting Basics with taskId, which  is passed from the button for current task
      Task task = [Select id, ActivityDate, Description, Subject, Salesperson_to_Email__c, WhoId, Who.Name, WhatId,
                   What.type, What.Name, Salesperson_to_Email__r.Name, Salesperson_to_Email__r.Email, CallType from
                   Task where id =: taskId];
  
      if(task.Salesperson_to_Email__c != null){      
          Document sr; 
          String imageURL;  
       // Getting Upper Left Image for Email Format
       if(!System.Test.isRunningTest()){
          sr = [SELECT Id, NamespacePrefix, SystemModstamp FROM Document WHERE 
                         DeveloperName =:Label.Endo_Task_Email_Logo LIMIT 1];
          imageURL = System.URL.getSalesforceBaseUrl().toExternalForm() 
                          + '/servlet/servlet.ImageServer?id=' 
                          + sr.id + '&oid=' 
                          + UserInfo.getOrganizationId() 
                          + '&lastMod=' + sr.SystemModstamp.getTime() ; 
            }
      // Intialzeing Email Instance
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] emailAdd = new String[]{};
           emailAdd.add(task.Salesperson_to_Email__r.Email);
           mail.setToAddresses(emailAdd);  
           subjectLine = Label.Endo_Task_Contact_Subject + task.Salesperson_to_Email__r.Name ; 
           mail.setSubject(subjectLine);
       
     //  Getting Due Date from Date/Time Acitvity Date
           if (task.ActivityDate != null) {
                DateTime dT = task.ActivityDate;
                dueDate = date.newinstance(dT.year(), dT.month(), dT.day());
           }
      
      // Creating HTML Body
       body = '<html>'+
                // Displaying Email Logo and Header of Email
                '<body>'+
                '<img src= "'+ imageURL + '" alt="Mountain View" style="width:104px;height:50px;">' +
                '<p>' + Label.Endo_Hello + ' ' +  task.Salesperson_to_Email__r.Name + ',' + '</p>'+
                '<p>' +  Label.Endo_Task_Email_Template + '</p>' +   
                
                '<p>' +  Label.Endo_Task_Subject + ' ' +      (task.Subject == null ? '' : task.Subject) + '</p>' +   
                '<p>' +  Label.Endo_Task_Activity_Date + ' ' + dueDate  + '</p>' +  
                '<p>' +  Label.Endo_Call_Type+ ' ' +         (task.CallType == null ? '' : task.CallType)+ '</p>' + 
                '<p>' +  Label.Endo_Task_Contact_Name + ' ' + task.Who.Name + '</p>' + 
                '<p>' +  Label.Endo_Task_Account_Name + ' ' + (task.What.type == 'Account'? task.What.Name : '') + '</p>' +   
                '<p>' +  Label.Endo_Comments+ '</p>' +
                '<p>' +  (task.Description == null ? '' : task.Description)+ '</p>' +
                '<p>' +  Label.Endo_Task_Email_Thanks + '</p>' +
                '<p>' +  Label.Endo_Customer_Care_Team + '</p>' +
                
                '</body></html>';  
                 
       // Sending Email to Contact with HTML Body         
       mail.setHtmlBody(body);   
       mail.setUseSignature(false);
       mail.setSaveAsActivity(true);
      try{
       Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail}); 
      }
      catch(Exception e){
        System.debug(e);
      }
     }
 }
}