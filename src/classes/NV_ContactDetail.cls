public class NV_ContactDetail{
        @AuraEnabled
        public Id ContactId {get;set;}

        @AuraEnabled
        public Id AccountId {get;set;}

        @AuraEnabled
        public String Name {get;set;}

        @AuraEnabled
        public String Type {get;set;}

        @AuraEnabled
        public String Title {get;set;}

        @AuraEnabled
        public String Email {get;set;}

        @AuraEnabled
        public String OfficeNumber {get;set;}

        @AuraEnabled
        public String MobileNumber {get;set;}

        @AuraEnabled
        public Boolean IsPrimary {get;set;}

        public NV_ContactDetail(Contact contactRecord){
            ContactId = contactRecord.Id;
            AccountId = contactRecord.AccountId;
            Name = contactRecord.Name;
            Title = contactRecord.Title__c;
            Type = contactRecord.Contact_Type__c;
            Email = contactRecord.Email;
            OfficeNumber = contactRecord.Phone;
            MobileNumber = contactRecord.MobilePhone;
            IsPrimary = contactRecord.Is_Primary__c;
        }

        @AuraEnabled
        public static void setPrimary(Id cId, Boolean isPrimary){
            Contact contactRecord = new Contact();
            contactRecord.Id = cId;
            contactRecord.Is_Primary__c = isPrimary;
            update contactRecord;
        }

        @AuraEnabled
        public static void removeContact(Id cId){
        	Contact contactRecord = new Contact();
            contactRecord.Id = cId;
            contactRecord.Is_Visible_In_Detail__c = false;
            update contactRecord;
        }

    }