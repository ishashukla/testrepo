//
// (c) 2016 Appirio, Inc.
// Class UpdateHubTabPermissionSetBatch
// Description : Batch to provide access for The Hub tab to Instruments profile users with business unit equals to IVS
//
// 29 June 2016     Isha Shukla		Original(T-511436)
global with sharing class UpdateHubTabPermissionSetBatch implements Database.Batchable<sObject>,System.Schedulable {
	Set<Id> InstrumentsProfileIdSet = new Set<Id>();
    DateTime dt = System.Now().addDays(-1);
	global UpdateHubTabPermissionSetBatch() {
		for(Profile profileId : [SELECT Id FROM Profile WHERE Name LIKE 'Instruments%']) {
			InstrumentsProfileIdSet.add(profileId.Id); 
		}
	}
	global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id,Division FROM User WHERE IsActive = True AND ProfileId IN :InstrumentsProfileIdSet AND LastModifiedDate > :dt';
        System.debug(query);
        return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext bc, List<User> records) {
        processData(records);
 
    }
    global void finish(Database.BatchableContext info) {
		
	}
    // Schedular for batch.
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
      Id batchId = Database.executeBatch(new UpdateHubTabPermissionSetBatch(), 200);
      system.debug('@@@batchId = ' + batchId);
    }
  }
  global void processData(List<User> records){
            Set<Id> userWithIVS = new Set<Id>(); // set of users having business unit IVS
         Set<Id> NonIVSUser = new Set<Id>(); // set of users not having business unit IVS
         Set<Id> allUser = new Set<Id>(); // all Instruments users set
         List<PermissionSetAssignment> newPermissionSetAccess = new List<PermissionSetAssignment>(); //list for new permission sets
         Map<Id,PermissionSetAssignment> usersHavingPermission = new Map<Id,PermissionSetAssignment>();
         for(User usr : records) {
             if(usr.Division != null && usr.Division.containsIgnoreCase('IVS')) {
                 userWithIVS.add(usr.Id);
             } else {
                 NonIVSUser.add(usr.Id);
             }
         }
         allUser.addAll(userWithIVS);
         allUser.addAll(NonIVSUser);
         System.debug('allUser='+allUser);
         Id psaId = [SELECT Id FROM PermissionSet WHERE Name = 'Access_To_The_Hub_Tab_For_Instruments_User_with_IVS_BU' LIMIT 1].Id; //Id of the permission set we want Users to be assigned
         for(PermissionSetAssignment psa : [SELECT Id,AssigneeId FROM PermissionSetAssignment 
                                            WHERE PermissionSetId = :psaId 
                                            AND AssigneeId IN :allUSer]) {
                                                if(!usersHavingPermission.containsKey(psa.AssigneeId)) {
                                                    usersHavingPermission.put(psa.AssigneeId,psa); // map of users having permission set assigned already
                                                }
         }
         List<PermissionSetAssignment> userAssignmentToDelete = new List<PermissionSetAssignment>();
         if(usersHavingPermission != null) {
             for(Id userid : usersHavingPermission.keySet()) {
                 if(NonIVSUser.contains(userid)) {
                     userAssignmentToDelete.add(usersHavingPermission.get(userid)); // list of assignment needs to be deleted for users who have changed their Business unit from IVS to some other
                 }
             }
             
             for(Id userid : userWithIVS) {
                 if(!usersHavingPermission.containsKey(userid)) {
                    PermissionSetAssignment newPSA = new PermissionSetAssignment();
                    newPSA.PermissionSetId = psaId; //set the permission set Id
                    newPSA.AssigneeId = userid; //set the User Id
                    newPermissionSetAccess.add(newPSA); //add the record to our list
                 }
             }
         }
         
        System.debug(newPermissionSetAccess+'<===');
        if(!newPermissionSetAccess.isEmpty()) { 
            insert newPermissionSetAccess;
        }
        System.debug('===>>'+userAssignmentToDelete);
        if(!userAssignmentToDelete.isEmpty()) {
            delete userAssignmentToDelete;
        }
  }
    
    
}