// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_SyncPartsOrderQueueTest - Test class for COMM_SyncPartsOrderQueue
//
// 3 Nov 2016, Isha Shukla (T-550285) 
// 
@isTest(SeeAllData=true)
private class COMM_SyncPartsOrderQueueTest {
    static SVMXC__RMA_Shipment_Order__c partOrderRecord;
    static Id orderRecordId;
    static Id orderLineId;
    static Account acc;
    static Case caseRecord;
    static Product2 productRecord;
    //Create case
    @isTest static void testCOMM_SyncPartsOrderQueue() {
        acc = TestUtils.createAccount(1, true).get(0);
        Address__c addressRecord = new Address__c(
          Type__c = 'Shipping',Primary_Address_Flag__c =true,Account__c = acc.Id,
            Location_ID__c = '1234',Business_Unit__c = '88'
        );
         insert addressRecord;
        productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM Change Request').getRecordTypeId();
        if(!(RTMap__c.getInstance(caseRecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = caseRecordTypeId,
                                          Object_API_Name__c='Case',
                                          Division__c = 'COMM',
                                          RT_Name__c='COMM Change Request');
            insert rtMap;
        }
        caseRecord = new Case(RecordTypeId = caseRecordTypeId,Accountid = acc.Id,Type  = 'Billable repair');
        insert caseRecord;
        SVMXC__Installed_Product__c asset = new SVMXC__Installed_Product__c();
        insert asset;
        SVMXC__Warranty__c warranty = new SVMXC__Warranty__c(SVMXC__End_Date__c = date.today() + 1,SVMXC__Installed_Product__c = asset.Id);
        insert warranty;
        SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Warranty__c = warranty.Id);
        insert workOrder;
        Map<String, Schema.RecordTypeInfo> partOrderInfo =
            Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
        String PORecordTypeId = partOrderInfo.get('RMA').getRecordTypeId();
        if(!(RTMap__c.getInstance(PORecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = PORecordTypeId,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='RMA');
            insert rtMap;
        }
        partOrderRecord = new SVMXC__RMA_Shipment_Order__c(
            SVMXC__Case__c = caseRecord.Id,SVMXC__Order_Status__c = 'Closed',
            SVMXC__Company__c = acc.Id,RecordTypeId = PORecordTypeId,Reason_Code__c='Out of Box Failure',
            SVMXC__Service_Order__c = workOrder.Id
        );
        insert partOrderRecord;
        SVMXC__RMA_Shipment_Line__c line = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__Product__c = productRecord.Id,
            SVMXC__RMA_Shipment_Order__c = partOrderRecord.Id,
            SVMXC__Expected_Quantity2__c = 1
        );
        insert line;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pb = [SELECT Id from Pricebook2 where Name = 'COMM Price Book'];
        if(pb == null) {
            pb = new Pricebook2(name = 'COMM Price Book',isActive = true);
            insert pb; 
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Order orderRecord = new Order(
            AccountId = acc.Id,EffectiveDate = system.today(),Status= 'Entered',Pricebook2id = pb.id,
            RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId()
        );
        COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA=true;
        insert orderRecord;
        orderRecordId = orderRecord.Id;
        Orderitem orderLines = new Orderitem(
            OrderId = orderRecord.Id,PriceBookEntryId = pbe.Id,Status__c = 'Entered',
            Requested_Ship_Date__c = Date.today(),Quantity = 1,UnitPrice  = 0,Description = line.Id
        );
        insert orderLines;
        orderLineId = orderLines.Id;
        Test.startTest();
        System.enqueueJob(new COMM_SyncPartsOrderQueue(partOrderRecord,orderRecordId,new List<Id>{orderLineId},'Create',null));
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Order WHERE Id = :orderRecordId].size(), 0);
    }
    // Update Case
    @isTest static void testWhenPOLineUpdates() {
        acc = TestUtils.createAccount(1, true).get(0);
        productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
        Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM Change Request').getRecordTypeId();
        caseRecord = new Case(RecordTypeId = caseRecordTypeId,Accountid = acc.Id,Type  = '120 Day Repair Warranty(Service)');
        insert caseRecord;
        if(!(RTMap__c.getInstance(caseRecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = caseRecordTypeId,
                                          Object_API_Name__c='Case',
                                          Division__c = 'COMM',
                                          RT_Name__c='COMM Change Request');
            insert rtMap;
        }
        Map<String, Schema.RecordTypeInfo> partOrderInfo =
            Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
        String PORecordTypeId = partOrderInfo.get('RMA').getRecordTypeId();
        if(!(RTMap__c.getInstance(PORecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = PORecordTypeId,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='RMA');
            insert rtMap;
        }
        partOrderRecord = new SVMXC__RMA_Shipment_Order__c(
            SVMXC__Case__c = caseRecord.Id,SVMXC__Order_Status__c = 'Closed',
            SVMXC__Company__c = acc.Id,RecordTypeId = PORecordTypeId,Reason_Code__c='Order Entry Error'
        );
        insert partOrderRecord;
        SVMXC__RMA_Shipment_Line__c line = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__Product__c = productRecord.Id,
            SVMXC__RMA_Shipment_Order__c = partOrderRecord.Id,
            SVMXC__Expected_Quantity2__c = 1
        );
        insert line;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pb = [SELECT Id from Pricebook2 where Name = 'COMM Price Book'];
        if(pb == null) {
            pb = new Pricebook2(name = 'COMM Price Book',isActive = true);
            insert pb; 
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Order orderRecord = new Order(
            AccountId = acc.Id,EffectiveDate = system.today(),Status= 'Entered',Pricebook2id = pb.id,
            RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId()
        );
        COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA=true;
        insert orderRecord;
        orderRecordId = orderRecord.Id;
        Orderitem orderLines = new Orderitem(
            OrderId = orderRecord.Id,PriceBookEntryId = pbe.Id,Status__c = 'Entered',
            Requested_Ship_Date__c = Date.today(),Quantity = 1,UnitPrice  = 0,Description = line.Id
        );
        insert orderLines;
        Address__c addressRecord = new Address__c(
          Type__c = 'Shipping',Primary_Address_Flag__c =true,Account__c = acc.Id,
            Location_ID__c = '1234',Business_Unit__c = '88'
        );
         insert addressRecord;
        Test.startTest();
        System.enqueueJob(new COMM_SyncPartsOrderQueue(partOrderRecord,orderRecordId,null,'Update',new List<Id>{line.Id}));
        Test.stopTest();
        System.assertEquals([SELECT Callout_Result__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id = :partOrderRecord.Id].Callout_Result__c,'Batch id 59 is processed sucessfully.');
    }
     @isTest static void testCOMM_SyncPartsOrderQueueWithAddress() {
        acc = TestUtils.createAccount(1, true).get(0);
        productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
         Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM Change Request').getRecordTypeId();
         if(!(RTMap__c.getInstance(caseRecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = caseRecordTypeId,
                                          Object_API_Name__c='Case',
                                          Division__c = 'COMM',
                                          RT_Name__c='COMM Change Request');
            insert rtMap;
        }
        caseRecord = new Case(RecordTypeId = caseRecordTypeId,Accountid = acc.Id,Type  = 'Factory Warranty Period');
        insert caseRecord;
         SVMXC__Installed_Product__c asset = new SVMXC__Installed_Product__c();
        insert asset;
        SVMXC__Warranty__c warranty = new SVMXC__Warranty__c(SVMXC__End_Date__c = date.today() + 1,SVMXC__Installed_Product__c = asset.Id,
                                                            SVMXC__Start_Date__c = date.today() - 2);
        insert warranty;
        SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Warranty__c = warranty.Id);
        insert workOrder;
         Map<String, Schema.RecordTypeInfo> partOrderInfo =
            Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
        String PORecordTypeId = partOrderInfo.get('RMA').getRecordTypeId();
         if(!(RTMap__c.getInstance(PORecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = PORecordTypeId,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='RMA');
            insert rtMap;
        }
         partOrderRecord = new SVMXC__RMA_Shipment_Order__c(
            SVMXC__Case__c = caseRecord.Id,SVMXC__Order_Status__c = 'Closed',
            SVMXC__Company__c = acc.Id,RecordTypeId = PORecordTypeId,Reason_Code__c='Wrong Part Ordered',
             SVMXC__Service_Order__c = workOrder.Id
        );
        insert partOrderRecord;
        SVMXC__RMA_Shipment_Line__c line = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__Product__c = productRecord.Id,
            SVMXC__RMA_Shipment_Order__c = partOrderRecord.Id,
            SVMXC__Expected_Quantity2__c = 1
        );
        insert line;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pb = [SELECT Id from Pricebook2 where Name = 'COMM Price Book'];
        if(pb == null) {
            pb = new Pricebook2(name = 'COMM Price Book',isActive = true);
            insert pb; 
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Order orderRecord = new Order(
            AccountId = acc.Id,EffectiveDate = system.today(),Status= 'Entered',Pricebook2id = pb.id,
            RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId()
        );
        COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA=true;
        insert orderRecord;
        orderRecordId = orderRecord.Id;
        Orderitem orderLines = new Orderitem(
            OrderId = orderRecord.Id,PriceBookEntryId = pbe.Id,Status__c = 'Entered',
            Requested_Ship_Date__c = Date.today(),Quantity = 1,UnitPrice  = 0,Description = line.Id
        );
        insert orderLines;
        orderLineId = orderLines.Id;
        Address__c addressRecord = new Address__c(
          Type__c = 'Billing',Primary_Address_Flag__c =true,Account__c = acc.Id,
            Location_ID__c = '1234',Business_Unit__c = '88'
        );
         insert addressRecord;
        Test.startTest();
        System.enqueueJob(new COMM_SyncPartsOrderQueue(partOrderRecord,orderRecordId,new List<Id>{orderLineId},'Create',null));
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Order WHERE Id = :orderRecordId].size(), 0);
    } 
    @isTest static void testotherCaseType() {
        acc = TestUtils.createAccount(1, true).get(0);
        productRecord = TestUtils.createProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;
        insert productRecord;
         Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM Change Request').getRecordTypeId();
         if(!(RTMap__c.getInstance(caseRecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = caseRecordTypeId,
                                          Object_API_Name__c='Case',
                                          Division__c = 'COMM',
                                          RT_Name__c='COMM Change Request');
            insert rtMap;
        }
        caseRecord = new Case(RecordTypeId = caseRecordTypeId,Accountid = acc.Id,Type  = 'POS Ext Warranty Concessions');
        insert caseRecord;
         Map<String, Schema.RecordTypeInfo> partOrderInfo =
            Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
        String PORecordTypeId = partOrderInfo.get('RMA').getRecordTypeId();
        if(!(RTMap__c.getInstance(PORecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = PORecordTypeId,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='RMA');
            insert rtMap;
        }
         partOrderRecord = new SVMXC__RMA_Shipment_Order__c(
            SVMXC__Case__c = caseRecord.Id,SVMXC__Order_Status__c = 'Closed',
            SVMXC__Company__c = acc.Id,RecordTypeId = PORecordTypeId,Reason_Code__c='Return as part of upgrade incentive program'
        );
        insert partOrderRecord;
        SVMXC__RMA_Shipment_Line__c line = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__Product__c = productRecord.Id,
            SVMXC__RMA_Shipment_Order__c = partOrderRecord.Id,
            SVMXC__Expected_Quantity2__c = 1
        );
        insert line;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pb = [SELECT Id from Pricebook2 where Name = 'COMM Price Book'];
        if(pb == null) {
            pb = new Pricebook2(name = 'COMM Price Book',isActive = true);
            insert pb; 
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
        Order orderRecord = new Order(
            AccountId = acc.Id,EffectiveDate = system.today(),Status= 'Entered',Pricebook2id = pb.id,
            RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('COMM Order').getRecordTypeId()
        );
        COMM_OrderTriggerHandler.donotExecuteFrom_WO_RMA=true;
        insert orderRecord;
        orderRecordId = orderRecord.Id;
        Orderitem orderLines = new Orderitem(
            OrderId = orderRecord.Id,PriceBookEntryId = pbe.Id,Status__c = 'Entered',
            Requested_Ship_Date__c = Date.today(),Quantity = 1,UnitPrice  = 0,Description = line.Id
        );
        insert orderLines;
        orderLineId = orderLines.Id;
        Address__c addressRecord = new Address__c(
          Type__c = 'Shipping',Primary_Address_Flag__c =true,Account__c = acc.Id,
            Location_ID__c = '1234',Business_Unit__c = '88'
        );
         insert addressRecord;
        Test.startTest();
        System.enqueueJob(new COMM_SyncPartsOrderQueue(partOrderRecord,orderRecordId,new List<Id>{orderLineId},'Create',null));
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Order WHERE Id = :orderRecordId].size(), 0);
    }
}