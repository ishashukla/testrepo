/*
* Created By : Sunil
* Purpose : Set IsExpired flag on Service Contract
*/
global class Endo_SetExpireFlagServiceContract implements Database.Batchable<sObject> {
   global Database.QueryLocator start(Database.BatchableContext BC){
   	  //Date objDate = System.today().addDays(30);
   	  //NEXT_N_DAYS:30
      //return Database.getQueryLocator('SELECT Is_Expiring__c, End_Date__c FROM Service_Contract__c WHERE End_date__c <= :objDate AND Is_Expiring__c = FALSE');
      return Database.getQueryLocator('SELECT Is_Expiring__c, End_Date__c FROM Service_Contract__c');
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
     list<Service_Contract__c> lstContracts = (list<Service_Contract__c>)scope;
     
     list<Service_Contract__c> lstToUpdate = new list<Service_Contract__c>();
     
     Date next30DaysDate = System.today().addDays(30);
     for(Service_Contract__c sc :lstContracts){
     	 if(sc.End_Date__c >= System.today() && sc.End_Date__c < next30DaysDate && sc.Is_Expiring__c == false){
         sc.Is_Expiring__c = true;
         lstToUpdate.add(sc);
       }
       if((sc.End_Date__c < System.today() || sc.End_Date__c >=  next30DaysDate) &&  sc.Is_Expiring__c == true){
         sc.Is_Expiring__c = false;
         lstToUpdate.add(sc);
       }
     }
     System.debug('@@@' + lstToUpdate);
     update lstToUpdate;
   }

   global void finish(Database.BatchableContext BC){
     
   }
 }