/**================================================================      
* Appirio, Inc
* Name: ForecastYearTrigger_Handler 
* Description: Handler class for ForecastYearTrigger.
* Created Date: 10 Dec 2015
* Created By: Tom Muse (Appirio)
*
* Date Modified      Modified By            Description of the update
* 2 June 2016        Isha Shukla            (T-507897) Commented updateManagerforForecastYear method
* 7 Sept 2016        Ralf Hoffmann          (I-234079) added external ID to the generated monthly record.
* Nov 21 2016        Nitish Bansal          I-244792 - Empower prod sync
==================================================================*/
public with sharing class ForecastYearTrigger_Handler {
    public static Id recordTypeRep = null;
    static{
         Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
         recordTypeRep = rtByNameRep.getRecordTypeId();
    }
    public static void afterInsert(Map<Id, Forecast_Year__c> newMap){
        List<String> monthStrings = new List<String>{'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'};
        // rh 9/7/2016 added yearly external id to select
        List<Forecast_Year__c> fyList = [Select id, User2__c, User2__r.Email, Yearly_Base__c, Historical_Service__c, Yearly_Quota__c, Year__c, Year_External_Id__c from Forecast_Year__c where id in : newMap.keyset()];//NB - 11/21 - I-244792
        List<Monthly_Forecast__c> lstToInsert = new List<Monthly_Forecast__c>();
        Integer mInt = 1;
        for(Forecast_Year__c fy : fyList ){
            Decimal monthlyQuota = (fy.Yearly_Quota__c!=null && fy.Yearly_Quota__c > 0) ? (fy.Yearly_Quota__c / 12) : 0;
            Decimal anualBase = (fy.Yearly_Base__c!=null && fy.Yearly_Base__c > 0) ? (fy.Yearly_Base__c / 12) : 0; //NB - 11/21 - I-244792
            Decimal historicalService = (fy.Historical_Service__c!=null && fy.Historical_Service__c > 0) ? (fy.Historical_Service__c / 12) : 0;//NB - 11/21 - I-244792
            mInt = 1;
            String monthExtId = Null;
            for(String month : monthStrings){
                Date eom = getEndOfMonthDate(mInt, fy.Year__c);
                DateTime dt = DateTime.newInstance(eom.year(),eom.month(),eom.day(),16,45,00);
                // rh 9/7/2016 added monlthy external id to insert 
                monthExtId = (fy.Year_External_Id__c!=Null) ? (fy.Year_External_Id__c+'_'+mInt) : Null;
                lstToInsert.add(new Monthly_Forecast__c(OwnersEmail__c = fy.User2__r.Email, Base_Forecast__c=anualBase, Quota__c=monthlyQuota, Forecast_Year__c=fy.id, Month__c=month, Month_End__c = eom,Month_End_Reminder__c =dt, Month_External_Id__c=monthExtId, Historical_Service__c = historicalService));//NB - 11/21 - I-244792
                mInt++;
            }
        }
        if(lstToInsert.size() > 0)
      insert lstToInsert;
    }
    // updateManagerforForecastYear
    private static Date getEndOfMonthDate(Integer month, String year){
        Date eom = date.newInstance(Integer.valueOf(year), (month) , 1);
        eom = eom.addMonths(1).addDays(-1);
        //Datetime eom2 = Datetime.newInstance(eom, Time.newInstance(0, 0, 0, 0));
        return eom;
    }
}