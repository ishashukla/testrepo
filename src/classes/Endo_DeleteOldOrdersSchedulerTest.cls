/*******************************************************************************
 Author        :  Appirio JDC (Sunil)
 Date          :  Feb 12, 2014 
 Purpose       :  Test Class for Case Endo_DeleteOldOrdersScheduler
*******************************************************************************/
@isTest
private class Endo_DeleteOldOrdersSchedulerTest {
  
  //-----------------------------------------------------------------------------------------------
  // Method for test delete order schedular method
  //-----------------------------------------------------------------------------------------------
  public static testmethod void testMethod1(){
    Test.startTest(); 
    // Schedule the test job
    String sch = '0 0 0 * * ?';
    String jobId = System.schedule('Endo_DeleteOldOrdersScheduler', sch, new Endo_DeleteOldOrdersScheduler());
    // Get the information from the CronTrigger API object
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    
    // Verify the expressions are the same  
    System.assertEquals(sch, ct.CronExpression);
    
    // Verify the job has not run
    System.assertEquals(0, ct.TimesTriggered);
    Test.stopTest();
  }
}