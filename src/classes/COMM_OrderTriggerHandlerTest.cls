// 
// (c) 2016 Appirio, Inc.
//
// Name : COMM_OrderTriggerHandlerTest 
// Used to Test trigger handler class "COMM_OrderTriggerHandler"
//
// 8th April 2016     Kirti Agarwal      Original(T-489842)
//
@isTest
private class COMM_OrderTriggerHandlerTest {
    @isTest
    static void comm_OrderTriggerHandlerTest () {
        List < Account > accountList = TestUtils.createAccount(1, true);
        //TestUtils.createCommConstantSetting();
        List<Contact> contactList = TestUtils.createContact(1,accountList.get(0).Id,false);
        contactList.get(0).Email = 'xyz@test.com';
        insert contactList;
        Pricebook2 priceBook = TestUtils.createPricebook(1,'test',false).get(0);
        priceBook.Name = 'COMM Price Book';
        insert priceBook;
        List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
        List < Project_Plan__c > projectPlanList = TestUtils.createProjectPlan(1, accountList[0].id, true);
        List<Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
        //Id recTypeId = Schema.sObjectType.Order.getRecordTypeInfosByName().get('COMM_ORDER_RECORD_TYPE_NAME').getRecordTypeId();
        for(Order order : orderList ) {
            order.OpportunityId =  oppList[0].id; 
            order.PONumber = '50';
            //order.recordTypeId=recTypeId;
        }   
        insert orderList;   
        
        for(Order order : orderList ) {
            order.Status = 'Booked';
        }  
        update orderList;
        
        User usr = [SELECT Email 
                    FROM User 
                    WHERE id = :UserInfo.getUserId() limit 1 ];
        
        for(Order o :  [SELECT Sales_Rep_Email__c 
                        FROM Order 
                        WHERE Id IN :orderList]) {
                            System.assertEquals(o.Sales_Rep_Email__c , usr.Email);
                        }
    }
    
    //================================================================      
    // Name         : projectPlanTest
    // Description  : Used on before insert and update of order record
    // Created Date : 7th April 2016 
    // Created By   : Kirti Agarwal (Appirio)
    // Task         : T-492006
    //==================================================================
    @isTest
    static void projectPlanTest() {
        List < Account > accountList = TestUtils.createAccount(1, true);
        TestUtils.createCommConstantSetting();
        Pricebook2 priceBook = TestUtils.createPricebook(1,'test',false).get(0);
        priceBook.Name = 'COMM Price Book';
        insert priceBook;
        List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
        List < Project_Plan__c > projectPlanList = TestUtils.createProjectPlan(1, accountList[0].id, false);
        projectPlanList[0].Stage__c = 'Cancelled'; 
        insert projectPlanList;
        
        List<Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
        for(Order orderRec : orderList) {
            orderRec.Project_Plan__c = projectPlanList[0].id;
            orderRec.Oracle_Order_Number__c='12';
            orderRec.PONumber = '122';
            orderRec.Order_Number_Oracle__c='11';
        }
        insert orderList;
        List<User> userList = TestUtils.createUser(1,'System Administrator', true);
        //order.Project_plan__c != (null or blank), If the Project.stage = "Cancelled" then change to "Initiation".
        for(Project_Plan__c plan : [SELECT Id, Stage__c FROM Project_Plan__c WHERE Id =: projectPlanList[0].id]) {
            System.assertEquals(plan.Stage__c, 'Initiation');
        }
        
        //If order.project_plan__c = null/blank 
        //and Order.opportunity.stage != "Cancelled" then populate Order.Project_Plan__c = order.Opportunity.Project_Plan__c
        oppList[0].Project_Plan__c = projectPlanList[0].id;
        update oppList[0];
        List<OpportunityTeamMember> otmList = TestUtils.createOpportunityTeamMember(1,oppList.get(0).id,userList.get(0).id,true);
        orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
        for(Order orderRec: orderList ) {
            orderRec.OpportunityId = oppList[0].id;
            orderRec.Oracle_Order_Number__c='123675';
        }
        insert orderList ;
        for(Order orderRec : [SELECT Id, Project_Plan__c FROM Order WHERE Id IN: orderList  ]) {
            System.assertEquals(orderRec.Project_Plan__c, projectPlanList[0].id);
        }      
        OrderList.get(0).Tracked_in_Install_Base__c = true;
        OrderList.get(0).Status = 'Awaiting Shipping';
        update orderList;
    } 
    @isTest static void projectPlanTestDelete() {
        List < Account > accountList = TestUtils.createAccount(1, true);
        TestUtils.createCommConstantSetting();
        Pricebook2 priceBook = TestUtils.createPricebook(1,'test',false).get(0);
        priceBook.Name = 'COMM Price Book';
        insert priceBook;
        List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
        List < Project_Plan__c > projectPlanList = TestUtils.createProjectPlan(1, accountList[0].id, false);
        projectPlanList[0].Stage__c = 'Cancelled'; 
        insert projectPlanList;
        
        List<Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
        for(Order orderRec : orderList) {
            orderRec.Project_Plan__c = projectPlanList[0].id;
            orderRec.Oracle_Order_Number__c='12';
            orderRec.PONumber = '122';
            orderRec.Order_Number_Oracle__c='11';
        }
        insert orderList;
        //order.Project_plan__c != (null or blank), If the Project.stage = "Cancelled" then change to "Initiation".
        for(Project_Plan__c plan : [SELECT Id, Stage__c FROM Project_Plan__c WHERE Id =: projectPlanList[0].id]) {
            System.assertEquals(plan.Stage__c, 'Initiation');
        }
        
        //If order.project_plan__c = null/blank 
        //and Order.opportunity.stage != "Cancelled" then populate Order.Project_Plan__c = order.Opportunity.Project_Plan__c
        oppList[0].Project_Plan__c = projectPlanList[0].id;
        update oppList[0];
        orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false);
        for(Order orderRec: orderList ) {
            orderRec.OpportunityId = oppList[0].id;
            orderRec.Oracle_Order_Number__c='123675';
        }
        insert orderList ;
        delete orderList;
        for(Order orderRec : [SELECT Id, Project_Plan__c FROM Order WHERE Id IN: orderList  ]) {
            System.assertEquals(orderRec.Project_Plan__c, projectPlanList[0].id);
        }
        
    }    
    
}