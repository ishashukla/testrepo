/**================================================================      
* Appirio, Inc
* Name: COMM_RmaShipmentOrderTriggerHandler
* Description: T-518090
* Created Date: 07/08/2016
* Created By: Jagdeep Juneja (Appirio)
* 
* Date Modified        Modified By        Description of the update
* 07/29/2016           Kanika Mathur      added function to populate no of RMA on Case T-522190
* 09/06/2016           Kanika Mathur      removed the populateNoOfRma() method as changes reverted
* 09/29/2016           Varun Vasishtha    Added function to call approval process by SFM T-541918
* 10/04/2016           Varun Vasishtha    Added functions to Populate Address in RMA and Case I-237921
==================================================================*/
public class COMM_RmaShipmentOrderTriggerHandler Implements RMAShipmentTriggerHandlerInterface{
    public static boolean futureProofCaseCreation = false;
    Static map<string,string> recordtypeDetails = new Map<String,string>();
    
    static{
        Set<String> setDevRTname =new set<String>{'RMA','Shipment'};
        for(Recordtype varloop: [select id,developername 
                                from recordtype 
                                where developername in :setDevRTname
                                and sobjecttype = 'SVMXC__RMA_Shipment_Order__c']){
            recordtypeDetails.put(varloop.id,varloop.developername);                        
        }
    }
    /**================================================================      
    * Standard Methods from the RMAShipmentTriggerHandlerInterface
    ==================================================================*/
    public static boolean IsActive(){
        return true;
    }
    
    public static void OnBeforeInsert(List<SVMXC__RMA_Shipment_Order__c> newRecords){
        PopulateRMAAddress(newRecords);
        populateOpportunityFromOrder(newRecords, null);
    }
    public static void OnAfterInsert(List<SVMXC__RMA_Shipment_Order__c> newRecords){
        //createCasesFromOrder(newRecords);
    }
    public static void OnAfterInsertAsync(Set<ID> newRecordIDs){}
    
    public static void OnBeforeUpdate(List<SVMXC__RMA_Shipment_Order__c> newRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> oldRecordMap){
        populateOpportunityFromOrder(newRecords, oldRecordMap);
    }
    
    public static void OnAfterUpdate(List<SVMXC__RMA_Shipment_Order__c> newRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> oldRecordMap){
        System.debug('==>OnAfterUpdate GS');
        //
        //Commented out by Jason Carlson 28Oct2016. Approval process moved to process builder
        //
        //CallApprovalProcess(newRecords);
        
    }    
    
    public static void OnAfterUpdateAsync(Set<ID> updatedRecordIDs){}
        
    public static void OnBeforeDelete(List<SVMXC__RMA_Shipment_Order__c> RecordsToDelete, Map<ID, SVMXC__RMA_Shipment_Order__c> RecordMap){}
    public static void OnAfterDelete(List<SVMXC__RMA_Shipment_Order__c> deletedRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> RecordMap){}
    public static void OnAfterDeleteAsync(Set<ID> deletedRecordIDs){}
    
    public static void OnUndelete(List<SVMXC__RMA_Shipment_Order__c> restoredRecords){}
    /**================================================================      
    * End of Standard Methods from the RMAShipmentTriggerHandlerInterface
    ==================================================================*/
    
    // Creating cases if no case has been added to shipment
    /*
    public static void createCasesFromOrder(List<SVMXC__RMA_Shipment_Order__c> newRecords){
        // Controlling Recoursion
        if(futureProofCaseCreation)
            return;
        Map<String,Case> liCase = new map<string,Case>();
        Case varCase = new Case();
        map<String,string> mapAccountName = new map<String,string>();
        map<String,string> mapAssetName = new map<String,string>();
        map<String,string> mapAccountAsset = new map<String,string>();
        // Capturing information for the account and Asset
        for(SVMXC__RMA_Shipment_Order__c varloop: newRecords){
            if(varloop.SVMXC__Case__c==null){
                if(varloop.SVMX_Asset__c!=null)
                    mapAssetName.put(varloop.SVMX_Asset__c,null);                                             
            }
        }

        for(SVMXC__Installed_Product__c var : [select name,SVMXC__Company__r.name,SVMXC__Company__c from SVMXC__Installed_Product__c where id in : mapAssetName.keyset()]){
            mapAssetName.put(var.id,var.name);
            mapAccountName.put(var.SVMXC__Company__c,var.SVMXC__Company__r.name);
            mapAccountAsset.put(var.id,var.SVMXC__Company__c);
        }        
        Recordtype rt = [select id,name from recordtype where developername = 'COMM_US_Tech_Support_Field_Services_Case'];
        for(SVMXC__RMA_Shipment_Order__c varloop: newRecords){
            if(varloop.SVMXC__Case__c==null){
                varcase = new Case();
                varcase.RMA_on_Case__c = varloop.id;
                varCase.Recordtypeid = rt.id;
                VarCase.subject = 'RMA ';
                VarCase.subject+= mapAccountName.get(mapAccountAsset.get(varloop.SVMX_Asset__c)) ==null?' ':mapAccountName.get(mapAccountAsset.get(varloop.SVMX_Asset__c))+ ' ';
                VarCase.subject+= mapAssetName.get(varloop.SVMX_Asset__c) ==null?' ':mapAssetName.get(varloop.SVMX_Asset__c);
                varcase.accountid = mapAccountAsset.get(varloop.SVMX_Asset__c);
                varcase.contactid = varLoop.SVMXC__Contact__c; 
                varcase.Asset_Installed_Product__c = varloop.SVMX_Asset__c;
                varcase.Description = 'Auto Created Case';
                //Code Update by Varun Vasishtha to populate Address ib newly created case.
                string address;
                if(string.isNotBlank(varloop.SVMXC__Source_Street__c)){
                    address = varloop.SVMXC__Source_Street__c+','; 
                }
                if(string.isNotBlank(varloop.SVMXC__Source_City__c)){
                    address += varloop.SVMXC__Source_City__c+','; 
                }
                if(string.isNotBlank(varloop.SVMXC__Source_State__c)){
                    address += varloop.SVMXC__Source_State__c+','; 
                }
                if(string.isNotBlank(varloop.SVMXC__Source_Country__c)){
                    address += varloop.SVMXC__Source_Country__c+','; 
                }
                if(string.isNotBlank(varloop.SVMXC__Source_Zip__c)){
                    address += varloop.SVMXC__Source_Zip__c; 
                }
                if(string.isNotBlank(address)){
                    varcase.Ship_To_Address_for_pickup__c = address;
                }
                liCase.put(varloop.id,Varcase);
            }
        }
        if(!liCase.isEmpty()){
            insert liCase.values();
            List<SVMXC__RMA_Shipment_Order__c> liNewUpdate = new List<SVMXC__RMA_Shipment_Order__c>();
            for(String s : liCase.keyset()){
                liNewUpdate.add(new SVMXC__RMA_Shipment_Order__c(id=s,SVMXC__Case__c=liCase.get(s).id));
            }
            
            if(!liNewUpdate.isEmpty()){
                futureProofCaseCreation = true;
                update liNewUpdate;
            }
        }
    }
    */
    // Method to Make the Rest Call and creation of Order and Order product.
    
    // Methods to be executed before record is inserted and updated.
    // Description-Populate Opportunity from Order when RMA shipment order record is created when Order field has value.
    // @param newRecords - List to hold Trigger.new, and Map to hold Trigger.oldMap
    // @return Nothing
    // Added by Jagdeep Juneja 
    private static void populateOpportunityFromOrder(List<SVMXC__RMA_Shipment_Order__c> partsOrderList,Map<Id,SVMXC__RMA_Shipment_Order__c> oldPartsOrderMap){
        Boolean isInsert = oldPartsOrderMap == null ? true:false;
        Set<Id> OrderIdSet = new Set<Id>();
        for(SVMXC__RMA_Shipment_Order__c partOrder: partsOrderList){
            if(isInsert || partOrder.SVMX_Order__c != oldPartsOrderMap.get(partOrder.Id).SVMX_Order__c){
                OrderIdSet.add(partOrder.SVMX_Order__c); 
                System.debug('<<<<<<'+'in populate opportunity');
            }            
        }
        
        if(OrderIdSet.isEmpty()){
            return;
        }
        
        map<Id,Order> mapOrderIdToOrder = new map<Id,Order>([SELECT Id,OpportunityId 
                                                             FROM Order 
                                                             WHERE ID IN:OrderIdSet
                                                             AND OpportunityId != null]);
        
        for(SVMXC__RMA_Shipment_Order__c partOrder: partsOrderList){
            if(isInsert || partOrder.SVMX_Order__c != oldPartsOrderMap.get(partOrder.Id).SVMX_Order__c){
                if(partOrder.SVMX_Order__c != null && mapOrderIdToOrder.containsKey(partOrder.SVMX_Order__c)){
                    partOrder.SVMX_Opportunity__c = mapOrderIdToOrder.get(partOrder.SVMX_Order__c).OpportunityId;
                }
                else{
                    partOrder.SVMX_Opportunity__c =null;
                }
            }
        }
    }
    
     // Call approval process when create by SFM.T-541918 (Varun Vasishtha)
    private static void CallApprovalProcess(List<SVMXC__RMA_Shipment_Order__c> items){
        
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        List<SVMXC__RMA_Shipment_Order__c> itemtoUpdate = new List<SVMXC__RMA_Shipment_Order__c>();
        for(SVMXC__RMA_Shipment_Order__c item : items){
            if(item.SubmitForApproval__c){
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval for SFM.');
                req1.setObjectId(item.Id);
                
                // Submit on behalf of a specific submitter
                req1.setSubmitterId(item.CreatedById); 
                
                // Submit the record to specific process and skip the criteria evaluation
                req1.setProcessDefinitionNameOrId('RMA_Approval_Process');
                req1.setSkipEntryCriteria(false);
                requests.add(req1);
                SVMXC__RMA_Shipment_Order__c updateItem = new SVMXC__RMA_Shipment_Order__c(Id = item.Id);
                updateItem.SubmitForApproval__c = false;
                itemtoUpdate.add(updateItem);
            }
        }
        // Submit the approval request for the account
        if(requests.size() > 0){
            Approval.process(requests);
            update itemtoUpdate;
        }
    }
    //Code to populate RMA with Address Associated with Account(Varun Vasishtha I-237921)
    private static void PopulateRMAAddress(List<SVMXC__RMA_Shipment_Order__c> items){
        Map<Id,List<SVMXC__RMA_Shipment_Order__c>> mapAccountwithRMA = new Map<Id,List<SVMXC__RMA_Shipment_Order__c>>();
        for(SVMXC__RMA_Shipment_Order__c item :items){
            string recordType = recordtypeDetails.get(item.Recordtypeid);
            if(item.SVMXC__Company__c != null && recordType == 'RMA'){
                if(!mapAccountwithRMA.Containskey(item.SVMXC__Company__c)){
                    List<SVMXC__RMA_Shipment_Order__c> rmaList = new List<SVMXC__RMA_Shipment_Order__c>();
                    rmaList.add(item);
                    mapAccountwithRMA.put(item.SVMXC__Company__c,rmaList);
                }
                else{
                     mapAccountwithRMA.get(item.SVMXC__Company__c).add(item);
                }
            }
        }
        if(mapAccountwithRMA.size() > 0){
            PopulateAddressRMAbyAccount(mapAccountwithRMA);
        }
    }
    
    //Code to populate RMA with Address Associated with Account(Varun Vasishtha I-237921)
    private static void PopulateAddressRMAbyAccount(Map<Id,List<SVMXC__RMA_Shipment_Order__c>> mapAccountwithRMA){
        for(Account accountAdd :[SELECT Id,COMM_Shipping_Address__c,COMM_Shipping_City__c,COMM_Shipping_Country__c,COMM_Shipping_State_Province__c,
        COMM_Shipping_PostalCode__c
        from Account where Id in :mapAccountwithRMA.keySet()]){
            List<SVMXC__RMA_Shipment_Order__c> rmas = mapAccountwithRMA.get(accountAdd.Id);
            for(SVMXC__RMA_Shipment_Order__c rma :rmas){
                if(!string.isBlank(accountAdd.COMM_Shipping_Address__c)){
                    rma.SVMXC__Source_Street__c = accountAdd.COMM_Shipping_Address__c;
                }
                if(!string.isBlank(accountAdd.COMM_Shipping_City__c)){
                    rma.SVMXC__Source_City__c = accountAdd.COMM_Shipping_City__c;
                }
                if(!string.isBlank(accountAdd.COMM_Shipping_State_Province__c)){
                    rma.SVMXC__Source_State__c = accountAdd.COMM_Shipping_State_Province__c;
                }
                if(!string.isBlank(accountAdd.COMM_Shipping_Country__c)){
                    rma.SVMXC__Source_Country__c = accountAdd.COMM_Shipping_Country__c;
                }
                if(!string.isBlank(accountAdd.COMM_Shipping_PostalCode__c)){
                    rma.SVMXC__Source_Zip__c = accountAdd.COMM_Shipping_PostalCode__c;
                }
            }
        }
    }
}