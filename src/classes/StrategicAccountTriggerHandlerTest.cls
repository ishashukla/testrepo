// (c) 2016 Appirio, Inc.
//
// Class Name: StrategicAccountTriggerHandlerTest
// Description: Test Class for StrategicAccountTriggerHandler.
// 
// June 23 2016, Isha Shukla  Original (T-513125)
//
@isTest
private class StrategicAccountTriggerHandlerTest {
    // Bulk Test
    // Method to test when Strategic_Account__c records insert User__c field is populated with id of currently logged in user
    @isTest static void testBulk() {
        List<Account> accountList = TestUtils.createAccount(1, True);
        List<Strategic_Account__c> strategicAccountList = new List<Strategic_Account__c>();
        for(Integer i = 0; i < 50; i++){
            Strategic_Account__c stAcc = new Strategic_Account__c(Account__c = accountList[0].Id);
            strategicAccountList.add(stAcc);
    	}
        Test.startTest();
        insert strategicAccountList;
        Test.stopTest();
        System.assertEquals([SELECT User__c FROM Strategic_Account__c WHERE User__c = :UserInfo.getUserId() AND Account__c = :accountList[0].Id].size(),50);
    }
    // Positive Test
    @isTest static void testPositive() {
        List<Account> accountList = TestUtils.createAccount(1, True);
        Strategic_Account__c strategicAccount = new Strategic_Account__c(Account__c = accountList[0].Id);
        Test.startTest();
        insert strategicAccount;
        Test.stopTest();
        System.assertEquals([SELECT User__c FROM Strategic_Account__c WHERE Account__c = :accountList[0].Id].User__c,UserInfo.getUserId());
    }
}