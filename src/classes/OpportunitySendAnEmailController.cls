/************************************************************************
Name  : OpportunitySendAnEmailController 
Author: Naresh K Shiwani(Appirio)
Date  : 21 July, 2015
Description: Controller of OpportunitySendAnEmail page.
Updates:
Naresh K Shiwani  04-08-2015  I-174439  Added Opp.Account.AccountOwner(Finance Rep) mail id.
Naresh K Shiwani  21-08-2015  I-177106  Added Limit on Account query.
*************************************************************************/
public with sharing class OpportunitySendAnEmailController {
  
  public Opportunity currentOpportunity{get;set;}
  public String regionalFinanceManagerMailId,
                oppAccountOwnerId,
                financeRepresentativeMailId,
                recepientMailsIds;
  public Set<String> productSalesRepresentativeSet  = new Set<String>();
  public Set<String> recipientMailIdsSet;
  public OpportunitySendAnEmailController(ApexPages.StandardController stdController){
    currentOpportunity = (Opportunity)stdController.getRecord();
    currentOpportunity = [ SELECT Id, OwnerId, Owner.Email, Account.OwnerId
                           FROM Opportunity 
                           WHERE Id = :currentOpportunity.Id 
                           limit 1];
    regionalFinanceManagerMailId  = currentOpportunity.Owner.Email;
    oppAccountOwnerId             = currentOpportunity.Account.OwnerId;
    
    for(Account oppAccount : [ Select Owner.Email, OwnerId 
                               From Account
                               Where OwnerId =: oppAccountOwnerId
                               limit 1]){
      financeRepresentativeMailId = oppAccount.Owner.Email;   
    }
    
    for(OpportunityLineItem oppLineItem : [ Select PSR_Contact__r.Email, PSR_Contact__c 
                                            From OpportunityLineItem
                                            Where OpportunityId =: currentOpportunity.Id]){
      if(oppLineItem.PSR_Contact__r.Email != null || oppLineItem.PSR_Contact__r.Email != '')
        productSalesRepresentativeSet.add(oppLineItem.PSR_Contact__r.Email);
    }
    recipientMailIdsSet = new Set<String>(productSalesRepresentativeSet);
    recipientMailIdsSet.add(financeRepresentativeMailId);
    recipientMailIdsSet.add(regionalFinanceManagerMailId);
    recepientMailsIds = '';
    for(String str:recipientMailIdsSet){
      if(String.isNotBlank(str))
        recepientMailsIds += str + ',';
    }    
    if(recepientMailsIds.length() > 0){
      recepientMailsIds = recepientMailsIds.substring(0,recepientMailsIds.length()-1);
    }
  }
  
  // This method being called from page action to Send Email Page.
  public pageReference redirectPage(){				   
  	PageReference pg = new PageReference('/_ui/core/email/author/EmailAuthor?rtype=003');
    for (String paramName : ApexPages.currentPage().getParameters().keySet()) {
      pg.getParameters().put(paramName, ApexPages.currentPage().getParameters().get(paramName));
    }    
    pg.getParameters().put('p3_lkid', currentOpportunity.id);
    pg.getParameters().put('retURL', currentOpportunity.id);
    pg.getParameters().put('p24', recepientMailsIds);
    pg.getParameters().put('nooverride', '1');
    pg.getParameters().remove('save_new');
    return pg;
  }
}