@isTest
public class Intr_AttachmentListCtrlOnOppty_Test {
    
    static Account account;
    static opportunity opportunity;
    static List<Opportunity> OpportunityList;
    
    //Test Validation
    public static testMethod void TestValidation() {
    	
        createTestDate();
        Test.startTest();
        Intr_AttachmentListControllerOnOppty Intr = new Intr_AttachmentListControllerOnOppty(new ApexPages.StandardController(opportunity));
        system.assertEquals(OpportunityList[0].AccountId,account.Id);
        Test.stopTest();
        
    }
    
    //Creating Sample Data
    public static void createTestDate() {
    	
        Profile profile = Intr_TestUtils.fetchInstrumentProfile(Intr_Constants.INSTRUMENTS_CCKM_PROFILE);
        User user = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profile.Id, Division = 'IVC',
                          TimeZoneSidKey='America/Los_Angeles', UserName='kaju.jalan@appirio.com');
        
        
        
        OpportunityList = new List<Opportunity>();
        
        System.runAs(user) {
            account = Intr_TestUtils.createAccount(1, false).get(0);
            account.Name = 'test';
            account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
            insert account;
            Account childaccount = Intr_TestUtils.createAccount(1, false).get(0);
            childaccount.Name = 'childtest';
            childaccount.ParentId = account.Id;
            childaccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
            insert childaccount;
        
            opportunity = Intr_TestUtils.createOpportunity(1, account.Id,false).get(0);
            opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_Opportunity_RECORD_TYPE ).getRecordTypeId();
            opportunity.Name = 'TestFName';
            opportunity.Type = 'Base';
            opportunity.CloseDate = date.today();
            opportunity.StageName = 'Closed Won';
            opportunity.ForecastCategoryName = 'Commit'; 
            opportunity.Business_Unit__c = 'IVC'; 
            opportunity.AccountId = account.Id;
            OpportunityList.add( opportunity);
            insert OpportunityList;
            
            
        }  
        
    }

}