/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : Test class for ScheduledOrderProductsController
//
// Nov 26, 2015    Megha Agarwal (Appirio)
//***************************************************************************/
@isTest
public class ScheduledOrderProductsControllerTest {
  static testMethod void testScheduledOrderProductsController(){
  	Test.startTest();
	    ScheduledOrderProductsController testSch = new ScheduledOrderProductsController();
	    Account acc = Medical_TestUtils.createAccount(1,true).get(0);
	    Contact con = Medical_TestUtils.createContact(1, acc.Id,true).get(0);
		  Order ord = new Order();
	    ord.Bill_To_Account__c = acc.Id;
	    ord.Ship_To_Account__c = acc.Id;
	 		ord.PoNumber = '1212';
	    ord.Type = 'CO Credit';
	    ord.Order_Origin__c = 'Fax';
	    ord.EffectiveDate = System.today();
	    ord.AccountId = acc.Id;
	    ord.Status = 'Draft';
	    insert ord;
	    testSch.receivingContact = null;
	    String str1 = [Select Id from Contact where Id =: con.Id].Id;
	    testSch.receivingConId = str1;
	    Contact con1 = testSch.receivingContact;
	    Contact con2 = con1;
	    testSch.receivingConId = null;
	    testSch.receivingContact = null;
	    Contact con3 = testSch.receivingContact;
	    Contact con4 = con3;
	    testSch.receivingConId = null;
	    testSch.orderObj = null;
	    String str = [Select Id from Order where Id =: ord.Id].Id;
	    testSch.orderId = str;
	    Order order1 = testSch.orderObj;
	    Order ord1 = order1;
	    testSch.orderId = null;
	  Test.stopTest();
  }

  static testMethod void testScheduledOrderProductsController1(){
    // PageReference pageRef = Page.TestOrderProducts;
		//Test.setCurrentPage(pageRef);
    Test.startTest();
	    ScheduledOrderProductsController testSch = new ScheduledOrderProductsController();
	    Account acc = Medical_TestUtils.createAccount(1,true).get(0);
	    Contact con = Medical_TestUtils.createContact(1, acc.Id,true).get(0);
	    Contact cont = Medical_TestUtils.createContact(1, acc.Id,false).get(0);
	  	cont.FirstName = 'testFirstName1';
	   	cont.LastName = 'testLastName1';
	    cont.AccountId = acc.id;
	    insert cont;
	
	    testSch.receivingContact = null;
	    String str2 = [Select Id from Contact where Id =: con.Id].Id;
	    testSch.receivingConId = str2;
	    Contact con3 = testSch.receivingContact;
	  Test.stopTest();
  }
  
  
   static testMethod void testWithWrongContactIds(){
    // PageReference pageRef = Page.TestOrderProducts;
    //Test.setCurrentPage(pageRef);
    Test.startTest();
      ScheduledOrderProductsController testSch = new ScheduledOrderProductsController();
      testSch.receivingConId = '123';
      System.assert(testSch.receivingContact != null, 'New object created');
      System.assert(testSch.receivingContact.Id == null, 'Id is blank');
    Test.stopTest();
  }
  
   static testMethod void testWithWrongOrderId(){
    // PageReference pageRef = Page.TestOrderProducts;
    //Test.setCurrentPage(pageRef);
    Test.startTest();
      ScheduledOrderProductsController testSch = new ScheduledOrderProductsController();
      testSch.orderId = '123';
      System.assert(testSch.orderObj != null, 'New object created');
      System.assert(testSch.orderObj.Id == null, 'Id is blank');
      testSch.orderId = null;
      System.assert(testSch.orderObj == null, 'object is null');
    Test.stopTest();
  }
}