@istest
public class AccountFollowUnfollowControllerTest{
    @testSetup static void setup() {        
        Map<String, Id> accountRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');
        Account testAccount = NV_TestUtility.createAccount('TestAccount',accountRecordTypes.get('NV_Customer'), true);
        
        Contract testContract = NV_TestUtility.createContract(testAccount.Id, true);
        testContract.Status = 'Activated';
        update testContract;
        
        Map<String, Id> productRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Product2');
        List<Product2> productData = new List<Product2>();
        for(Integer i=1; i<=9;i++){
            productData.add(NV_TestUtility.createProduct('NVProduct'+i, '00000'+i, 
                                                            productRecordTypes.get('Stryker_Neurovascular') , false));
        }
        insert productData;
        
        Pricebook2 testPriceBook = NV_TestUtility.createPricebook('NVPriceBook1', true, true);

        Map<String, Id> orderRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');
        Order testOrder1 = NV_TestUtility.createOrder(testAccount.Id, testContract.Id, 
                                                        orderRecordTypes.get('NV_Standard'), false);
        testOrder1.Customer_PO__c = 'PO00001';
        testOrder1.EffectiveDate = Date.today();
        testOrder1.Date_Ordered__c = Datetime.now();
        testOrder1.Status = 'Booked';
        testOrder1.Pricebook2Id = testPriceBook.Id;
        insert testOrder1;

        
        Test.startTest();
        PricebookEntry pbEntryForP1 = NV_TestUtility.addPricebookEntries(testPriceBook.Id, productData[0].Id, 1000, true, true);
        OrderItem testOrderItem1 = new OrderItem();
        testOrderItem1.OrderId = testOrder1.Id;
        testOrderItem1.PricebookEntryId = pbEntryForP1.Id;
        testOrderItem1.Quantity = 3;
        testOrderItem1.UnitPrice = pbEntryForP1.UnitPrice;
        insert testOrderItem1;
        
        testOrderItem1.Status__c = 'Shipped';
        update testOrderItem1;
        Test.stopTest();  
        
        OrderItem testOrderItem2 = new OrderItem();
        testOrderItem2.OrderId = testOrder1.Id;
        testOrderItem2.PricebookEntryId = pbEntryForP1.Id;
        testOrderItem2.Quantity = 3;
        testOrderItem2.UnitPrice = pbEntryForP1.UnitPrice;
        insert testOrderItem2;
        testOrderItem2.Status__c = 'Backordered';
        update testOrderItem2;        
        
    }
    
    @isTest static void testOrderItemTriggerBeforeInsertUpdate () { 
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
        PageReference pageRef = Page.AccountFollowUnfollow;
        Test.setCurrentPage(pageRef);
              
        ApexPages.StandardController sc = new ApexPages.standardController(testAccount);
        AccountFollowUnfollowController ext= new  AccountFollowUnfollowController(sc);
        Test.startTest(); 
        AccountFollowUnfollowController.followAccountAndOrder(testAccount.Id); 
        
        System.assertEquals(2, [SELECT count() FROM NV_Follow__c]);
        AccountFollowUnfollowController.unFollowAccountAndOrder(testAccount.Id);
        System.assertEquals(0, [SELECT count() FROM NV_Follow__c]);
        Test.stopTest();
        
    }
       
}