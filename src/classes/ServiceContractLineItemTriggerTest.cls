/**================================================================      
* Appirio, Inc
* Name: ServiceContractLineItemTriggerTest
* Description: Test class of ServiceContractLineItemTriggerHandler
* Created Date: 14th Dec 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/

@isTest
private class ServiceContractLineItemTriggerTest
{
    @isTest
    static void itShouldUpdateEndDate()
    {
        List<Account> accountList = TestUtils.createAccount(1,false);
        insert accountList;

        List<Product2> productList = TestUtils.createCOMMProduct(1, false);
        productList[0].Product_Class_Code__c = '001'; 
        insert productList;

        List <Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false); 
        orderList[0].PriceBook2Id = Test.getStandardPricebookId();
        insert orderList;

        List <SVMXC__Installed_Product__c> instProdList = TestUtils.createInstalledProducts(1, productList[0].id, orderList[0].id, 'QA', true);

        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c();
        testServiceContract.RecordTypeId = Constants.getRecordTypeId(Constants.RT_COMM_SERVICE_CONTRACT , Constants.SERVICE_CONTRACT_OBJECT);
        testServiceContract.SVMXC__Start_Date__c = system.today();
        testServiceContract.SVMXC__End_Date__c = system.today().addDays(30);
        testServiceContract.SVMXC__Company__c = accountList[0].Id;
        testServiceContract.SVMXC__Active__c = true;
        testServiceContract.Name = 'test service contract'; 
        insert testServiceContract;

        Test.startTest();
            SVMXC__Service_Contract_Products__c testServiceContractProduct = new SVMXC__Service_Contract_Products__c();
            testServiceContractProduct.SVMXC__Service_Contract__c = testServiceContract.Id;
            testServiceContractProduct.SVMXC__Product__c = productList[0].Id;
            testServiceContractProduct.SVMXC__Installed_Product__c = instProdList[0].Id;
            insert testServiceContractProduct;
        Test.stopTest();

        for(SVMXC__Installed_Product__c installedProd : [Select Id, SVMXC__Service_Contract_End_Date__c from SVMXC__Installed_Product__c]){
            system.assertNotEquals(null, installedProd.SVMXC__Service_Contract_End_Date__c);
        }
    }
}