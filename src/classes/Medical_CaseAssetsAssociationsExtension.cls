//
// (c) 2012 Appirio, Inc.
//
//  Extention class for visualforce page Medical_CaseAssetsAssociations
//
// 22 Feb 2016     Sunil, Aashita
//

public class Medical_CaseAssetsAssociationsExtension {
	public Case caseObj {get;set;}
  public String selectedRecordName {get;set;}
  public List<Case_Asset_Junction__c> caseAssets {get;set;}
  public Integer rowIndex {get;set;}
  public string caSerialNumber{get;set;}
  public String csAssetId {get;set;}
  public Integer index{get;set;}
  public Boolean caseSaved{get;set;}
  public String caseObjId {get;set;}


  //-----------------------------------------------------------------------------------------------
  //  Cosntructor
  //-----------------------------------------------------------------------------------------------
  public Medical_CaseAssetsAssociationsExtension(ApexPages.StandardSetController stdController) {
    index = 1;
    caseObj = new Case();

    //String assetId = ApexPages.currentPage().getParameters().get('AssetId');
    caseObjId = ApexPages.currentPage().getParameters().get('Id');
    caseAssets = new List<Case_Asset_Junction__c>();
    caseAssets.add(new Case_Asset_Junction__c());

    /* if(assetId != null && assetId != ''){
	      caseAssets.get(0).Asset__c = assetId;
	      for(Asset a : [select id , Ship_To_Account__c, serialNumber from Asset where id = : assetId]){
	              if(a.Ship_To_Account__c != null){
	                  caseObj.AccountId = a.Ship_To_Account__c;
	              }
	              caseAssets.get(0).Asset__r = a;
	      }
	  }*/

	  if(caseObjId != null && caseObjId != '') {
	  	caseAssets.get(0).Case__c = caseObjId;
      caseObj.ID = caseObjId;
      for(Case c : [SELECT id, CaseNumber From Case Where Id =: caseObjId]) {
        caseAssets.get(0).Case__r = c;
      }
    }
  }

  //-----------------------------------------------------------------------------------------------
  //  Method to get selected record
  //-----------------------------------------------------------------------------------------------
  public PageReference getSelectedRecord(){
    if(caseObj.AccountId != null){
      String accId = caseObj.AccountId;
      List<Sobject> sobjectList = Database.query('SELECT Id, Name  FROM Account WHERE ID = : accId');
      if(!sobjectList.isEmpty()){
        caseObj.AccountId = sobjectList[0].id;
        selectedRecordName = (String) sobjectList[0].get('Name');
      }
    }
    return null;
  }

  //-----------------------------------------------------------------------------------------------
  //  Method to add more rows to select asset
  //-----------------------------------------------------------------------------------------------
  public PageReference addRow(){
    caseAssets.add(new Case_Asset_Junction__c());
    return null;
  }


  //-----------------------------------------------------------------------------------------------
  //  Method to remove rows
  //-----------------------------------------------------------------------------------------------
  public PageReference removeRow(){
    system.debug('@@rowIndex' + rowIndex);
    system.debug('@@caseAssets.size()' + caseAssets.size());

    // When remove button click on 0th row then do not remove row just empty values.
    if(rowIndex == 0 && caseAssets.size() == 1) {

    	system.debug('@@caseAssets.size()' + caseAssets[0]);
      caseAssets[0].Asset__c = null;
      caseAssets[0].Asset__r = null;
      return null;
    }

    // When remove button click on any rows.
    if(rowIndex != null && rowIndex < caseAssets.size()){
      caseAssets.remove(rowIndex);
    }
    return null;
  }


  //-----------------------------------------------------------------------------------------------
  //  Method to populate serial number
  //-----------------------------------------------------------------------------------------------
  public PageReference populateSerialNumber(){
    if(index != null){
        String soqlQuery = 'SELECT id, Ship_To_Account__c , serialNumber, Asset_Description__c FROM Asset';
        String serialComparison = caSerialNumber != null && caSerialNumber != '' ? caSerialNumber.trim() + '%' : '';
        if(csAssetId != null  && csAssetId != '' && csAssetId != '000000000000000'){
            soqlQuery += ' WHERE id = : csAssetId';
        }
        else if(caSerialNumber != null && caSerialNumber != ''){
             soqlQuery += ' WHERE (Name like :serialComparison OR serialNumber like : serialComparison) ';
        }
        System.debug('::soqlQuery'+soqlQuery);
        List<Asset> resultAssets = Database.query(soqlQuery);
        System.debug('::resultAssets'+resultAssets);
        if(resultAssets.size() == 1){
            caseAssets.get(index).asset__r = resultAssets.get(0);
             caseAssets.get(index).asset__c = resultAssets.get(0).id;
        }
    }
    return null;
  }

  //-----------------------------------------------------------------------------------------------
  //  Method to populate account
  //-----------------------------------------------------------------------------------------------
  public PageReference populateAccount(){
    if(caseObj.contactId != null && caseObj.AccountId == null){
        for(Contact con : [select id, AccountId from Contact where id = : caseObj.contactId limit 1]){
            caseObj.AccountId = con.AccountId;
        }
    }
    return null;
  }

  //-----------------------------------------------------------------------------------------------
  //  Method to Save the records
  //-----------------------------------------------------------------------------------------------
  public PageReference saveCaseAndForm(){
    Savepoint sp = Database.setSavepoint();
    PageReference page;
    //Boolean isSuccess;
    try{
    	upsert caseObj;
	    List<Case_Asset_Junction__c> caseAssetsToBeInsert = new List<Case_Asset_Junction__c>();
	    system.debug('@@caseAssets'+caseAssets);
	    for(Case_Asset_Junction__c csAsset : caseAssets){
        if(csAsset.Asset__c != null){
            csAsset.Case__c = caseObj.id;
            caseAssetsToBeInsert.add(csAsset);
        }
	    }
	    system.debug('@@caseAssets>>>'+caseAssetsToBeInsert);
	    if(caseAssetsToBeInsert.size() > 0){
	      upsert caseAssetsToBeInsert;
	    }
	    page = new PageReference('/' + caseObj.id);
	    page.setRedirect(true);
	    return page;
	    //isSuccess = true;
    }
    catch(Exception ex){
      ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error , ex.getMessage()));
      Database.rollback(sp);
      return page;
      /*isSuccess = false;
      caseSaved = false;*/
    }
    return null;
  }
}