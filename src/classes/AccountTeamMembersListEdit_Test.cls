/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMembersListEdit_Test
 * Description: Test class for AccountTeamMembersListEdit
 * Created Date: 
 * Created By: Deepti Maheshwari (Appirio)
 *
 * Date Modified            Modified By           Description of the update
 * 
  =====================================================================*/
@isTest
private class AccountTeamMembersListEdit_Test {

    // Method to verify the functionality of AccountTeamMembersList controller
  static testMethod void basicTest() {
    createTestData();
    //fetch the created Account and User
    Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account 0'];
    User usr = [SELECT Id FROM User WHERE Email = 'user0@test.com' limit 1];

      // Set the standard controller and call the constructor.
    ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(acc);
    AccountTeamMembersListEdit editController = new AccountTeamMembersListEdit(std);

    // Call the methods and create new team member
    editController.addTeamMembers();
    editController.newAccountTeamMembers[0].member.UserId = usr.Id;
    editController.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
    editController.newAccountTeamMembers[0].accountAccess = 'Edit';
    editController.newAccountTeamMembers[0].opportunityAccess = 'Read';
    editController.newAccountTeamMembers[0].caseAccess = 'Read';

    // Cancel (removes ATM from the list)
    editController.doCancel();

    List<AccountTeamMember> atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
    System.assertEquals(0, atm.size());

    // Try again, and then save
    editController.addTeamMembers();
    editController.newAccountTeamMembers[0].member.UserId = usr.Id;
    editController.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
    editController.newAccountTeamMembers[0].accountAccess = 'Edit';
    editController.newAccountTeamMembers[0].opportunityAccess = 'Read';
    editController.newAccountTeamMembers[0].caseAccess = 'Read';

    editController.saveAndMore();

    // Verify that the new team member is created and has proper role as given by user
    atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
    System.assertEquals(1, atm.size());
    System.assertEquals(atm.get(0).TeamMemberRole, 'Sales Manager');

  }
  
  // Method to verify the functionality of AccountTeamMembersList controller
  static testMethod void basicTest2() {
    createTestData();
    //fetch the created Account and User
    Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account 0'];
    User usr = [SELECT Id FROM User WHERE Email = 'user0@test.com' limit 1];

      // Set the standard controller and call the constructor.
    ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(acc);
    AccountTeamMembersListEdit controller = new AccountTeamMembersListEdit(std);

    // Call the methods and create new team member
    controller.addTeamMembers();
    controller.newAccountTeamMembers[0].member.UserId = usr.Id;
    controller.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
    controller.newAccountTeamMembers[0].accountAccess = 'Edit';
    controller.newAccountTeamMembers[0].opportunityAccess = 'Read';
    controller.newAccountTeamMembers[0].caseAccess = 'Read';

    // Cancel (removes ATM from the list)
    controller.doCancel();

    List<AccountTeamMember> atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
    System.assertEquals(0, atm.size());

    // Try again, and then save
    controller.addTeamMembers();
    controller.newAccountTeamMembers[0].member.UserId = usr.Id;
    controller.newAccountTeamMembers[0].member.TeamMemberRole = 'Sales Manager';
    controller.newAccountTeamMembers[0].accountAccess = 'Edit';
    controller.newAccountTeamMembers[0].opportunityAccess = 'Read';
    controller.newAccountTeamMembers[0].caseAccess = 'Read';

    controller.saveAndMore();

    // Verify that the new team member is created and has proper role as given by user
    atm = [SELECT Id,TeamMemberRole FROM AccountTeamMember WHERE UserId = :usr.Id];
    System.assertEquals(1, atm.size());
    System.assertEquals(atm.get(0).TeamMemberRole, 'Sales Manager');
  }

  //=======================================================================
  // Create Test Data
  //=======================================================================
  
  public static void createTestData() {
    
    List<User> testUser = TestUtils.createUser(1,'System Administrator', true);
    List<Account> accList = TestUtils.createAccount(1, true);
  }

}