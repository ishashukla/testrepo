/******************************************************************************************
 *  Purpose : Test Class for Sched_BatchToUpdateBillToAccountOnOrder
 *  Author  : Shubham Paboowal
 *  Story   : S-436452
*******************************************************************************************/  
@isTest
private class Sched_BatchToUpdateBillToAccountOnODTest {

    static testMethod void testScheduler() {
      String CRON_EXP = '0 0 0 * * ? *';
      Test.startTest();
      Id jobId = System.schedule('Update Bill To Account', CRON_EXP, new Sched_BatchToUpdateBillToAccountOnOrder());
      Test.stopTest();
      System.assert(jobId != null);  // ensure the class was scheduled.
    }
}