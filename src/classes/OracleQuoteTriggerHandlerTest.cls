/**================================================================      
* Appirio, Inc
* Name: OracleQuoteTriggerHandlerTest 
* Description: Test Class for OracleQuoteTriggerHandler class.
* Created Date: April 13 2016
* Created By: Isha Shukla (Appirio)
*
* Date Modified      Modified By            Description of the update
* July  2  2016      Isha Shukla            Modified(T-516161)
* August 8 2016      Kanika Mathur          Modified method testPrimaryQuote() to fix test failures
* August 18, 2016    Prakarsh Jain          Modified(T-531095)
* Oct 5, 2016        Shreerath Nair         Modified(T-542918)
* Oct 20, 2016       Shreerath Nair         Modified(T-549050)
* Nov 21 2016        Nitish Bansal          I-244792 - Empower prod sync
==================================================================*/
@isTest(SeeAllData=true)
public class OracleQuoteTriggerHandlerTest {
    static BigMachines__Quote__c quote;
    static List<Account> accountList;
    static List<User> user;
    static List<Opportunity> opp;
    static BigMachines__Configuration_Record__c site;
    // Testing when quote changes status to Quoting (Approved) 
    static testMethod void testOracleQuoteTrigger(){ 
        createData();
        system.runAs(user[0]){
            Test.startTest();
            insert quote;
            OracleQuoteTriggerHandler obj = new OracleQuoteTriggerHandler();
            quote.BigMachines__Status__c='Quoting (Approved)';
            quote.Financing_Requested__c = True;
            
            update quote;
            Test.stopTest();  
            System.assertEquals('Quoting (Approved)', quote.BigMachines__Status__c);
            System.assertEquals(True, quote != Null);
        }
    }
    // Testing when quote changes status to Flex Quoting
    static testMethod void testOracleQuoteTriggerWithStatusFlex(){ 
        createData();
        system.runAs(user[0]){
            Test.startTest();
            insert quote;
            OracleQuoteTriggerHandler obj = new OracleQuoteTriggerHandler();
            quote.BigMachines__Status__c='Flex Quoting';
            quote.Financing_Requested__c = True;
            
            update quote;
            Test.stopTest(); 
            System.assertEquals('Flex Quoting', quote.BigMachines__Status__c);
            System.assertEquals(True, quote != Null);
        }
    }
    static testMethod void testUpdatedStatus(){ 
        createData();
        system.runAs(user[0]){
            Test.startTest();
            quote.BigMachines__Status__c='Flex Quoting';
            quote.Financing_Requested__c = True;
            insert quote;
            OracleQuoteTriggerHandler obj = new OracleQuoteTriggerHandler();
            quote.BigMachines__Status__c='Quoting (Approved)';
            update quote;
            Test.stopTest(); 
            System.assertEquals('Quoting (Approved)', quote.BigMachines__Status__c);
            System.assertEquals(True, quote != Null);
        }
    }
    // Testing when quote updates to become primary
    //NB - 11/21 - I-244792 Start
    static testMethod void testPrimaryQuote(){ 
        createData();
        system.runAs(user[0]){
            Test.startTest();
            BigMachines__Quote__c quote1 = new BigMachines__Quote__c(BigMachines__Opportunity__c = opp[0].Id,BigMachines__Is_Primary__c = false,BigMachines__Site__c =site.Id);
            insert quote1;
            System.assertEquals(false, quote1.BigMachines__Is_Primary__c);
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
            Base_Trail_Survey__c survey2 = TestUtils.createBaseTrailSurvey(1, opp[0].Id, true).get(0);
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(1, product.Id,null,'908',quote1.Id,True);
            quote1.BigMachines__Is_Primary__c = false;
            update quote1;
            quote1.BigMachines__Is_Primary__c = true;
            update quote1;
            Test.stopTest(); 
            System.assertEquals(true, quote1.BigMachines__Is_Primary__c);
            
            System.assertEquals(True, quote1 != Null);
        }
    }
    //NB - 11/21 - I-244792 End
    // Testing when quote updates to become not primary
    static testMethod void testNotPrimaryQuote(){ 
        
        createData();
        system.runAs(user[0]){
            //Test.startTest();
            //BigMachines__Quote__c quote1 = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, site, false);
            quote.BigMachines__Is_Primary__c = true;
            quote.OwnerId = user[0].Id;
            insert quote;
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
            Base_Trail_Survey__c survey2 = TestUtils.createBaseTrailSurvey(1, opp[0].Id, false).get(0);
            survey2.Owner__c = user[0].Id;
            insert survey2;
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(1, product.Id,null,'9081',quote.Id,true);
            OracleQuoteTriggerHandler obj = new OracleQuoteTriggerHandler();
            quote.BigMachines__Is_Primary__c = false;
            Test.startTest();
            update quote;
            Test.stopTest();
            BigMachines__Quote__c q = [SELECT Id, BigMachines__Is_Primary__c FROM BigMachines__Quote__c WHERE Id =: quote.Id];
            System.assertEquals(false, q.BigMachines__Is_Primary__c);
            System.assertEquals(True, quote != Null);
        }
    }
    // Testing when primary quote deletes
    static testMethod void testDeletePrimaryQuote(){ 
        createData();
        system.runAs(user[0]){
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
            quote.BigMachines__Is_Primary__c = true;
            insert quote;
            Base_Trail_Survey__c survey2 = TestUtils.createBaseTrailSurvey(1, opp[0].Id, true).get(0);
            List<Base_Product__c> baseProdlst = TestUtils.createBaseProduct(1, product.Id,null,'9081',quote.Id,True);
            OracleQuoteTriggerHandler obj = new OracleQuoteTriggerHandler();
            Test.startTest();
            delete quote;
            Test.stopTest();
        }
    }
    
    //31 Aug, 2016  Prakarsh Jain   T-531095
    //Method to check opportunity record is shared with the users whose mail ids are present in the approver's field
    static testMethod void testupdateSharingOnOpportunityRecord(){
        createData();
        user[1].Email = 'pqr@stu.com';
        update user;
        system.runAs(user[0]){
            Test.startTest();
            update opp;
            quote.BigMachines__Status__c = 'Flex';
            quote.Approvers__c = 'abc@xyz.com;pqr@stu.com';
            insert quote;
            quote.BigMachines__Status__c = 'Quoting (Pending Approval)';
            update quote;
            Test.stopTest();
            List<OpportunityShare> listOppShare = new List<OpportunityShare>();
            List<BigMachines__Quote__Share> listQuoteShare = new List<BigMachines__Quote__Share>();
            listOppShare = [SELECT Id, userOrGroupId, OpportunityAccessLevel, RowCause, OpportunityId FROM OpportunityShare WHERE OpportunityId =: opp[0].Id];
            listQuoteShare = [SELECT Id, userOrGroupId, AccessLevel, ParentId FROM BigMachines__Quote__Share WHERE ParentId =: quote.Id];
            system.assertEquals(listOppShare[0].userOrGroupId, user[0].Id);
            system.assertEquals(listQuoteShare[0].userOrGroupId, user[0].Id);
        }    
    }

    /**********************NB - 11/21 - I-244792 Start*********************************/

    //5 Oct, 2016  Shreerath Nair   T-542918
     //Method to positive test to check if quote is primary or not to restrict the deletion
     static testMethod void testRestrictDeletionOfQuotePositive(){ 
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        system.runAs(usr){
           
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            opp[0].stageName='Closed Won';
            update opp;
            
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            
            Product2 product = TestUtils.createProduct(1,false).get(0);
            product.Description = 'test product';
            insert product;
            
            quote.BigMachines__Is_Primary__c = true;
            insert quote;
            
            Test.startTest();
            try{
                delete quote;
            }catch(Exception e){
                System.AssertEquals(e.getMessage().contains('Primary quotes cannot be deleted. Users can delete the opportunity or create a new quote as primary.'), true);  
            }
            Test.stopTest();
        }
    }
    
    
     //5 Oct, 2016  Shreerath Nair   T-542918
     //Method to negative test to check if quote is primary or not to restrict the deletion
     static testMethod void testRestrictDeletionOfQuoteNegative(){ 
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        system.runAs(usr){
           
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            opp[0].stageName='Closed Won';
            update opp;
            
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            
            BigMachines__Quote__c quote1 = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            quote1.BigMachines__Is_Primary__c = true;
            insert quote1;
            
            quote.BigMachines__Is_Primary__c = false;
            insert quote;
            
            Test.startTest();
            
            delete quote;
            List<BigMachines__Quote__c> tempQuoteList = [SELECT Id FROM BigMachines__Quote__c WHERE ID =: quote.Id];
            System.AssertEquals(tempQuoteList.size(),0);  

            Test.stopTest();
        }
    }
    
     //5 Oct, 2016  Shreerath Nair   T-542918
     //Method to check if quote is primary of not to restrict the bulk deletion
     static testMethod void testRestrictDeletionOfQuoteBulk(){ 
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        system.runAs(usr){
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            opp[0].stageName='Closed Won';
            update opp;
            
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            quote.BigMachines__Is_Primary__c = true;
            insert quote;
            
            List<BigMachines__Quote__c> quoteList = new List<BigMachines__Quote__c>();
            
            for(Integer i=0 ; i<200 ; i++){
                BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
                quoteList.add(quotes);
                
            }
            
            insert quoteList;
            
            Test.startTest();
            try{
                delete quoteList;
            }catch(Exception e){
                 System.AssertEquals(e.getMessage().contains('Primary quotes cannot be deleted. Users can delete the opportunity or create a new quote as primary.'), true);  
            }
            Test.stopTest();
        }
    }
    
    //20 Oct, 2016  Shreerath Nair   T-549050
     //Positive test Method to set Opportunity.CPQ_Quote_Created__c to true if quote is inserted on an Opportunity
    static testmethod void testSetCPQCreatedOnInsertPositive(){       
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        
        system.runAs(usr){
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            Test.StartTest();
                insert quote;               
            Test.StopTest();                    
        }
        Opportunity oppt = [SELECT id,CPQ_Quote_Created__c FROM Opportunity WHERE Id =: quote.BigMachines__Opportunity__c][0];
        System.assertEquals(oppt.CPQ_Quote_Created__c,true);      
    }
    
    
    //20 Oct, 2016  Shreerath Nair   T-549050
    //Negative test Method to set Opportunity.CPQ_Quote_Created__c to true if quote is inserted on an Opportunity
    static testmethod void testSetCPQCreatedOnInsertNegative(){       
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        
        system.runAs(usr){
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(false);
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            Test.StartTest();
            try{
                insert quote; 
            }catch(Exception ex){
                Opportunity oppt = [SELECT id,CPQ_Quote_Created__c FROM Opportunity WHERE Id =: quote.BigMachines__Opportunity__c][0];
                System.assertEquals(oppt.CPQ_Quote_Created__c,false);
                
            }
            Test.StopTest();                    
        }
              
    }
    
    //20 Oct, 2016  Shreerath Nair   T-549050
    //bulk test Method to set Opportunity.CPQ_Quote_Created__c to true if quote is inserted on an Opportunity
    static testmethod void testSetCPQCreatedOnInsertBulk(){
        
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        List<BigMachines__Quote__c> quoteList = new List<BigMachines__Quote__c>();
        system.runAs(usr){           
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);            
            for(Integer i=0 ; i<200 ; i++){
                BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
                quoteList.add(quotes);
                
            }            
            Test.StartTest();
                insert quoteList;               
            Test.StopTest();                  
        }        
        Opportunity oppt = [SELECT id,CPQ_Quote_Created__c FROM Opportunity WHERE Id =: quoteList[0].BigMachines__Opportunity__c][0];
        System.assertEquals(oppt.CPQ_Quote_Created__c,true);       
    }
    
    //20 Oct, 2016  Shreerath Nair   T-549050
    //positive test Method to set Opportunity.CPQ_Quote_Created__c to false if all quote is deleted from the Opportunity
    static testmethod void testSetCPQCreatedOnDeletePositive(){
                
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        
        system.runAs(usr){
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            insert quote;
            Test.StartTest();               
                delete quote;              
            Test.StopTest();                       
        }        
        Opportunity oppt = [SELECT id,CPQ_Quote_Created__c FROM Opportunity WHERE Id =: quote.BigMachines__Opportunity__c][0];
        System.assertEquals(oppt.CPQ_Quote_Created__c,false);       
    }
    
    //20 Oct, 2016  Shreerath Nair   T-549050
    //Negative test Method to set Opportunity.CPQ_Quote_Created__c to false if all quote is deleted from the Opportunity
    static testmethod void testSetCPQCreatedOnDeleteNegative(){              
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr;       
        system.runAs(usr){
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            insert quote;
            BigMachines__Quote__c quote1 = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
            insert quote1;
            Test.StartTest();            
                delete quote;
            Test.StopTest(); 
        }            
        Opportunity oppt = [SELECT id,CPQ_Quote_Created__c FROM Opportunity WHERE Id =: quote.BigMachines__Opportunity__c][0];
        System.assertEquals(oppt.CPQ_Quote_Created__c,true);        
    }
    
    //20 Oct, 2016  Shreerath Nair   T-549050
    //Bulk test Method to set Opportunity.CPQ_Quote_Created__c to false if all quote is deleted from the Opportunity
    static testmethod void testSetCPQCreatedOnDeleteBulk(){       
        User usr = TestUtils.createUser(1, 'Instruments Sales User', false ).get(0);
        usr.Division = 'IVS;NSE';
        insert usr; 
        List<BigMachines__Quote__c> quoteList = new List<BigMachines__Quote__c>();
        system.runAs(usr){            
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            opp[0].OwnerId = usr.Id; 
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            BigMachines__Configuration_Record__c sites = TestUtils.createSiteRecord(true);
            
            for(Integer i=0 ; i<200 ; i++){
                BigMachines__Quote__c quotes = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, sites, false);
                quoteList.add(quotes);               
            }
            insert quoteList;            
            Test.StartTest();
                delete quoteList;        
            Test.StopTest();    
        }        
        Opportunity oppt = [SELECT id,CPQ_Quote_Created__c,(SELECT Id from BigMachines__BigMachines_Quotes__r) FROM Opportunity WHERE Id =: quoteList[0].BigMachines__Opportunity__c][0];
        System.assertEquals(oppt.CPQ_Quote_Created__c,false);       
    }


    /*********************NB - 11/21 - I-244792 End*********************************/
    
    public static void createData() {
        user = TestUtils.createUser(2, 'System Administrator', false );
        user[0].Division = 'NSE';
        user[0].Email = 'abc@xyz.com';
        insert user;
        system.runAs(user[0]){
            accountList = TestUtils.createAccount(1,True);
            Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
            Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
            opp = TestUtils.createOpportunity(1,accountList[0].Id,false);
            opp[0].Business_Unit__c = 'NSE';
            //opp[0].Opportunity_Number__c = '50001';
            opp[0].OwnerId = user[0].Id;
            opp[0].recordtypeId =  recordTypeInstrument;
            insert opp;
            List<OpportunityTeamMember> memberList = TestUtils.createOpportunityTeamMember(1,opp[0].Id,user[0].Id,False);
            memberList[0].OpportunityAccessLevel = 'Read';
            insert memberList;
            site = new BigMachines__Configuration_Record__c(BigMachines__action_id_copy__c = '18793335',
                                                            BigMachines__action_id_open__c = '0393544',
                                                            BigMachines__bm_site__c = '4345453',
                                                            BigMachines__document_id__c = '5334322',
                                                            BigMachines__process__c = '90293233',
                                                            BigMachines__process_id__c = '483212949',
                                                            BigMachines__version_id__c = '73421844',
                                                            BigMachines__Is_Active__c = true                                                                                           
                                                           );
            insert site;
            /*quote = new BigMachines__Quote__c(BigMachines__Account__c = accountList[0].Id,BigMachines__Status__c='status1',BigMachines__Opportunity__c=opp[0].Id,BigMachines__Is_Primary__c=True,BigMachines__Site__c=site.Id);*/
            quote = TestUtils.createQuoteRecord(accountList.get(0).Id, opp.get(0).Id, site, false);
            
        }
    }
}