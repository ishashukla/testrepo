/**================================================================      
 * Appirio, Inc
 * Name: RMAShipmentTriggerHandlerInterface
 * Description: Top level interface describing the methods available to all Stryker or division specific functionality
 * Created Date: 10 Aug 2016
 * Created By: Nick Whitney (Appirio)
 * 
 * Date Modified      Modified By      Description of the update
 * 
 ==================================================================*/

public interface RMAShipmentTriggerHandlerInterface{
    boolean IsActive();
    
    void OnBeforeInsert(List<SVMXC__RMA_Shipment_Order__c> NewRecords);
    void OnAfterInsert(List<SVMXC__RMA_Shipment_Order__c> NewRecords);
    void OnAfterInsertAsync(Set<ID> NewRecordIds);
    
    void OnBeforeUpdate(List<SVMXC__RMA_Shipment_Order__c> NewRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> OldRecordMap);
    void OnAfterUpdate(List<SVMXC__RMA_Shipment_Order__c> NewRecords, Map<ID, SVMXC__RMA_Shipment_Order__c> OldRecordMap);
    void OnAfterUpdateAsync(Set<ID> UpdatedRecordIDs);
        
    void OnBeforeDelete(List<SVMXC__RMA_Shipment_Order__c> RecordsToDelete, Map<ID, SVMXC__RMA_Shipment_Order__c> OldRecordMap);
    void OnAfterDelete(List<SVMXC__RMA_Shipment_Order__c> RecordsToDelete, Map<ID, SVMXC__RMA_Shipment_Order__c> OldRecordMap);
    void OnAfterDeleteAsync(Set<ID> DeletedRecordIDs);
    
    void OnUndelete(List<SVMXC__RMA_Shipment_Order__c> RestoredRecords);
}