/**================================================================      
 * Appirio, Inc
 * Name: COMM_IntegrationErrorBatchTest
 * Description: To test COMM_IntegrationErrorProcessingBatch
 * Created Date: 09/28/2016
 * Created By: Meghna Vijay (Appirio)
 * 
  ==================================================================*/
@isTest
public class COMM_IntegrationErrorBatchTest {
   Static List<Order> orderList;
   Static OrderItem oi;
  static List<COMM_Callout_Error__c> callOutErrList = new List<COMM_Callout_Error__c>();
	static String AUTHORIZATION = 'Authorization';
  static testMethod void testCreateBatch() {
    createTestData();
    String query = 'SELECT Id, Retry_Counter__c, TargetObjectID__c,Operation__c FROM COMM_Callout_Error__c';
    query += ' WHERE Retry_Counter__c < 3';
    callOutErrList.get(0).Operation__c = 'Create';
    callOutErrList.get(0).Retry_Counter__c = 2;
    insert callOutErrList;
    COMM_IntegrationErrorProcessingBatch errorBatch = new COMM_IntegrationErrorProcessingBatch();
    Database.executeBatch(errorBatch);
  }
  static testMethod void testOrderItemToSOBatch() {
    createTestData();
    String query = 'SELECT Id, Retry_Counter__c, TargetObjectID__c,Operation__c FROM COMM_Callout_Error__c';
    query += ' WHERE Retry_Counter__c < 3';
    callOutErrList.get(0).TargetObjectID__c = oi.id;
    callOutErrList.get(0).Operation__c = 'OrderItem';
    callOutErrList.get(0).Retry_Counter__c = 2;
    insert callOutErrList;
    //String[] orderPPArray = new String[2];
    COMM_IntegrationErrorProcessingBatch errorBatch = new COMM_IntegrationErrorProcessingBatch();
    Database.executeBatch(errorBatch);
  }
  static testMethod void testAssignToSOBatch() {
    createTestData();
    String query = 'SELECT Id, Retry_Counter__c, TargetObjectID__c,Operation__c FROM COMM_Callout_Error__c';
    query += ' WHERE Retry_Counter__c < 3';
    callOutErrList.get(0).Operation__c = 'AssignmentToSO';
    callOutErrList.get(0).Retry_Counter__c = 2;
    insert callOutErrList;
    //String[] orderPPArray = new String[2];
    COMM_IntegrationErrorProcessingBatch errorBatch = new COMM_IntegrationErrorProcessingBatch();
    Database.executeBatch(errorBatch);
  }
  static void createTestData() {
    TestUtils.createCommBPConfigConstantSetting();
     Id pricebookId = Test.getStandardPricebookId();
    Product2 prd1 = new Product2(Name = 'Test Product Entry 1', Description = 'Test Product Entry 1', isActive = true);
    insert prd1;

    PricebookEntry pe = new PricebookEntry(UnitPrice = 1, Product2Id = prd1.id, Pricebook2Id = pricebookId, isActive = true);
    insert pe;
  	User u = TestUtils.createUser(1,'System Administrator', true).get(0);
  	List<Account> accList = TestUtils.createAccount(1,true);
  	List<Opportunity> opptyList = TestUtils.createOpportunity(1, accList.get(0).Id, true);
  	List<Project_Plan__c> projectPlanList = TestUtils.createProjectPlan(1,accList.get(0).Id,true);
  	orderList = TestUtils.createOrder(1,accList.get(0).Id,'Entered',false);
  	orderList.get(0).OpportunityId = opptyList.get(0).Id;
  	orderList.get(0).Project_Plan__c = projectPlanList.get(0).Id;
  	orderList.get(0).Oracle_Order_Number__c = '12';
  	orderList[0].PriceBook2Id = pricebookId;
  	insert orderList;
  	oi = new OrderItem(OrderId = orderList[0].id, Quantity = decimal.valueof('1'), UnitPrice = 1,PricebookEntryId = pe.id);
    oi.Status__c='Entered'; //NB - 11/7 - I-242624
    oi.Requested_Ship_Date__c = System.Today();
    insert oi;
  	callOutErrList = TestUtils.createCallOutError(1,projectPlanList.get(0).Project_Plan_Number__c+':'+orderList.get(0).Id,'',false);
  }
}