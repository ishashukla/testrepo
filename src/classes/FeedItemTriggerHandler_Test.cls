/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Jan 23, 2014  
 Purpose       :  Test class for trigger FeedItemTriggerHandler
 Reference     :  Task T-241113
*******************************************************************************/
@isTest 
private class FeedItemTriggerHandler_Test {

  static testMethod void myUnitTest() {
  
    Test.startTest();
    
    //Create Custom Setting
    List<Chatter_Group_Ids__c> csList = TestUtils.createChatterGroupCS(false); 
    csList.get(0).Group_Id__c = null;
    insert csList;
    
    //Create Intel 
    Intel__c intel = TestUtils.createIntel(1, true).get(0);
    intel = [SELECT Id, Chatter_Post__c FROM Intel__c WHERE Id = :intel.Id].get(0);
    System.assertNotEquals(null, intel.Chatter_Post__c);
	    
	  //Delete FeedItem (Chatter Post) on Intel Record
	  Delete [Select Id FROM FeedItem WHERE Id = :intel.Chatter_Post__c];
	  
	  //Query Intel Record
	  intel = [SELECT Id, Chatter_Post__c, Intel_Inactive__c FROM Intel__c WHERE Id = :intel.Id].get(0);
    System.assertEquals(null, intel.Chatter_Post__c);
    System.assertEquals(true, intel.Intel_Inactive__c);
     
    Test.stopTest();
  }
}