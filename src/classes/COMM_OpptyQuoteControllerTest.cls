/**================================================================      
* Appirio, Inc
* Name: COMM_OpptyQuoteControllerTest
* Description: Test class of COMM_OpptyQuoteController
* Created Date: 1st Dec 2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 
==================================================================*/ 
@isTest
private class COMM_OpptyQuoteControllerTest {

	static List<Opportunity> opptyList = new List<Opportunity>();
    
    static List<BigMachines__Quote__c> bigMachineQuoteList;
    static void createTestData() {
        TestUtils.createCommBPConfigConstantSetting();
        TestUtils.createCommConstantSetting();
        List<Account> accList = TestUtils.createAccount(1,true);
        opptyList = TestUtils.createOpportunity(20,accList.get(0).Id,true);
        
        List<BigMachines__Configuration_Record__c> bigMachineConfigList = TestUtils.createBigMachineConfig(1,true);
        bigMachineQuoteList = TestUtils.createBigMachinesQuote(20,accList.get(0).Id, opptyList.get(0).Id,false);
        bigMachineQuoteList.get(0).BigMachines__Site__c = bigMachineConfigList.get(0).Id;
        for(Integer i = 0; i < 20; i++){
            bigMachineQuoteList.get(i).BigMachines__Site__c = bigMachineConfigList.get(0).Id;
            if(i < 10){
                bigMachineQuoteList.get(i).BigMachines__Is_Primary__c = true;
            }
        }
        insert bigMachineQuoteList;
    }

    @isTest
    static void itShouldTestController()
    {
        createTestData();
        Test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(opptyList.get(0));
            COMM_OpptyQuoteController controller = new COMM_OpptyQuoteController(std);
            System.assertEquals(controller.quoteList.size()>0,true);
            System.assertEquals(false,controller.hasPrevious);
            System.assertEquals(true,controller.hasNext);
            controller.next();
            System.assertEquals(true,controller.hasPrevious);
            controller.previous();
            controller.first();
            System.assertEquals(true,controller.hasNext);
            System.assertEquals(false,controller.hasPrevious);
            controller.last();
            System.assertEquals(true,controller.hasPrevious);
            System.assertEquals(false,controller.hasNext);
            controller.sortList();
            controller.sortField = 'LastModifiedBy';
            controller.sortList();
            controller.selectedQuote = controller.quoteList.get(0);
            controller.selectedId = controller.quoteList.get(0).Id;
            controller.doDelete();
            System.assertEquals(false,controller.hasPrevious);
            System.assertEquals(true,controller.hasNext);
            System.assertEquals(true, controller.quoteList.size()>0);
            controller.first();
            controller.processSelected();
            controller.setQuote();
            System.assertEquals(true,controller.hasNext);
            controller.next();
            System.assertEquals(true,controller.hasPrevious);
            controller.previous();
            controller.quoteWrapperList[0].quote = controller.quoteList.get(0);
            controller.quoteWrapperList[0].isSelected = true;
            controller.processSelected();
            controller.setQuote();
            System.assertEquals(true, controller.quoteList.size()>0);
        Test.stopTest();    
    }
}