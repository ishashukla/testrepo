/**================================================================      
* Appirio, Inc
* Name: COMM_SFDCProjectWSDLtoApexController  
* Description: T-492006 (WSDL Generated class for project interface)
* Created Date: 05/15/2016
* Created By: Deepti Maheshwari (Appirio)
*
* Date Modified      Modified By      Description of the update
 
==================================================================*/

public class COMM_SFDCProjectWSDLtoApexController {
    public class SFDCProject_ICPort {
        public String endpoint_x = Label.Project_Create_EP;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://SFDCProjectBPELProcess_IC/sfdcprojectbpelprocess_ic/public', 'COMM_SFDCProjectWSDLtoApexController', 'http://schemas.stryker.com/LocalProject/V1', 'SchemasStrykerCOMMproject'};
        public SchemasStrykerCOMMproject.ProjectResponseType ProcessSFDCProject_IC(SchemasStrykerCOMMproject.IntegrationHeaderType IntegrationHeader,SchemasStrykerCOMMproject.ProjectsType Projects) {
            SchemasStrykerCOMMproject.ProjectRequestType request_x = new SchemasStrykerCOMMproject.ProjectRequestType();
            request_x.IntegrationHeader = IntegrationHeader;
            request_x.Projects = Projects;
            SchemasStrykerCOMMproject.ProjectResponseType response_x;
            Map<String, SchemasStrykerCOMMproject.ProjectResponseType> response_map_x = new Map<String, SchemasStrykerCOMMproject.ProjectResponseType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://schemas.stryker.com/LocalProject/V1',
              'ProjectRequest',
              'http://schemas.stryker.com/LocalProject/V1',
              'ProjectResponse',
              'SchemasStrykerCOMMproject.ProjectResponseType'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}