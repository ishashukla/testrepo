/******************************************************************************************
 *  Purpose : Test Class for CustomLookupController
 *  Author  : Shubham Paboowal
 *  Date    : September 13, 2016
 *  Story	: S-437706
*******************************************************************************************/  
@isTest
private class CustomLookupControllerTest {

    static testMethod void myUnitTest() {
    	List<Account> accList = Medical_TestUtils.createAccount(1, true);
    	List<Contact> contList = Medical_TestUtils.createContact(1, accList[0].Id, true);
    	ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(contList[0]);
        CustomLookupController con = new CustomLookupController(controller);
        String query = 'SELECT Id FROM Contact LIMIT 1';
        List<sObject> conList = CustomLookupController.find(query);
        
    }
}