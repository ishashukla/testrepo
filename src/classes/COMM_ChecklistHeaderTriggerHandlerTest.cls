/**================================================================      
* Appirio, Inc
* Name: COMM_ChecklistHeaderTriggerHandlerTest
* Description: Test class for COMM_ChecklistHeaderTriggerHandler
* Created Date: 13-Sep-2016 
* Created By: Kanika Mathur (Appirio)
*
* Date Modified      Modified By      Description of the update
* 22 Nov 2016       Shubham Dhupar    Updated the parameters of createCheckListItem 
*                                     Method which was causing Test Failure.
==================================================================*/
@isTest
public class COMM_ChecklistHeaderTriggerHandlerTest {
    public static List<SVMXC__Installed_Product__c> assetList;
    
  @isTest
  static void valueOnClone() {
    testData();
    Schema.DescribeSObjectResult checkListHeader = Schema.SObjectType.Checklist_Header__c; 
    Map<String,Schema.RecordTypeInfo> checkListHeaderRTMap = checkListHeader.getRecordTypeInfosByName(); 
    Id checkListHeaderRtId = checkListHeaderRTMap.get('COMM Checklist Header').getRecordTypeId();
    Test.startTest();
      List<Checklist_Header__c> checkListHeaderList = TestUtils.createChecklistHeader(1, assetList[0].Id, false);
      checkListHeaderList.get(0).RecordTypeId = checkListHeaderRtId ;
      insert checkListHeaderList;
    Test.stopTest();
    Checklist_Header__c Newcheck = checkListHeaderList[0].clone(false, true);
    checkListHeaderList[0].IsLatest__c =false;
    update checkListHeaderList[0];
    System.assertEquals(checkListHeaderList[0].isLatest__c, false);
    System.assertEquals(Newcheck.isLatest__c, false);

  }
  @isTest
  static void testEvalauteResult() {
    testData();
     assetList.get(0).QIP_ID__c = 'QIP Visum LT-FP';
        update assetList;
    Schema.DescribeSObjectResult checkListHeader = Schema.SObjectType.Checklist_Header__c; 
    Map<String,Schema.RecordTypeInfo> checkListHeaderRTMap = checkListHeader.getRecordTypeInfosByName(); 
    Id checkListHeaderRtId = checkListHeaderRTMap.get('COMM Checklist Header').getRecordTypeId();
    List<Checklist_Header__c> checkListHeaderList = TestUtils.createChecklistHeader(3, assetList[0].Id, false);
    checkListHeaderList.get(0).RecordTypeId = checkListHeaderRtId ;
    insert checkListHeaderList;
    List<ChecklistCUST__c> checkListCustomList = TestUtils.createCustomChecklist(1, 'QIP SINGLE VISIU', 1,true);
    List<Checklist_Group__c> checkListGroupList = TestUtils.createChecklistGroup(1, 'Test Name',checkListCustomList[0].id, 1,true);
    List<Checklist_Item__c> checkListItemList = TestUtils.createCheckListItem(1, 'TestItem', checkListCustomList[0].id, checkListGroupList[0].id, true, true); // SD 11/22 [Updated the method Signatures]
    List<CheckList_Detail__c> checkListDetailList = TestUtils.createChecklistDetail(2,'Test',assetList[0].Id,checkListHeaderList[0].id,checkListCustomList[0].id,checkListGroupList[0].id,false);
   // List<CheckList_Detail__c> secondCheckListDetailList = TestUtils.createChecklistDetail(1,'Test',assetList[0].Id,checkListHeaderList[1].id,checkListCustomList[0].id,checkListGroupList[0].id,false);
    //List<CheckList_Detail__c> thirdCheckListDetailList = TestUtils.createChecklistDetail(1,'Test',assetList[0].Id,checkListHeaderList[2].id,checkListCustomList[0].id,checkListGroupList[0].id,false);
    checkListDetailList[0].Type__c = 'Picklist';
    checkListDetailList[0].CheckList_Item__c = checkListItemList[0].id;
    insert checkListDetailList[0];
    checkListDetailList[0].Result__c = 'Passed';
    update checkListDetailList[0];
    checkListHeaderList[0].Status__c = 'In Progress';
    update checkListHeaderList[0];
    checkListDetailList[1].Type__c = 'Comment';
    checkListDetailList[1].CheckList_Item__c = checkListItemList[0].id;
    insert checkListDetailList[1];
    checkListDetailList[1].Comment__c = 'Pass';
    update checkListDetailList[1];
    checkListHeaderList[0].Status__c = 'In Progress';
    update checkListHeaderList[0];
    checkListDetailList[0].Result__c = 'Failed';
    update checkListDetailList[0];
    checkListHeaderList[0].Status__c = 'In Progress';
    update checkListHeaderList[0];
    checkListDetailList[0].Result__c = null;
    update checkListDetailList[0];
    checkListHeaderList[0].Status__c = 'In Progress';
    update checkListHeaderList[0];
    checkListDetailList[1].Comment__c = null;
    update checkListDetailList[1];

  }
  
/*  @isTest
    static void updateQIPStatusToInProgressTest() {
        testData();
        Test.startTest();
            List<Checklist_Header__c> checkListHeaderList = TestUtils.createChecklistHeader(1, assetList[0].Id, true);
        Test.stopTest();
        List<SVMXC__Installed_Product__c> assetUpdatedList = [SELECT Id, QIP_Status__c FROM SVMXC__Installed_Product__c WHERE Id =: assetList[0].Id];
        System.assertEquals(assetUpdatedList[0].QIP_Status__c, 'In Progress');
    }*/
    
    @isTest
    static void populateCompletedByAndOnFieldsTest() {
        testData();
        Schema.DescribeSObjectResult checkListHeader = Schema.SObjectType.Checklist_Header__c; 
        Map<String,Schema.RecordTypeInfo> checkListHeaderRTMap = checkListHeader.getRecordTypeInfosByName(); 
        Id checkListHeaderRtId = checkListHeaderRTMap.get('COMM Checklist Header').getRecordTypeId();
        List<Checklist_Header__c> checkListHeaderList = TestUtils.createChecklistHeader(1, assetList[0].Id, false);
        checkListHeaderList.get(0).RecordTypeId = checkListHeaderRtId ;
        insert checkListHeaderList;
        Test.startTest();
            checkListHeaderList[0].Status__c = 'Fail';
            update checkListHeaderList;
        Test.stopTest();
        List<Checklist_Header__c> checklistHeaderUpdatedList = [SELECT Id, Completed_By__c, Completed_On__c FROM Checklist_Header__c WHERE Id =: checkListHeaderList[0].Id];
        System.assert(checklistHeaderUpdatedList[0].Completed_By__c != null);
        System.assert(checklistHeaderUpdatedList[0].Completed_On__c != null);
    }
    
    @isTest
    static void updateQIPFieldsOnAssetTest() {
        testData();
        assetList.get(0).QIP_ID__c = 'QIP Visum LT-FP';
        update assetList;
        Schema.DescribeSObjectResult checkListHeader = Schema.SObjectType.Checklist_Header__c; 
        Map<String,Schema.RecordTypeInfo> checkListHeaderRTMap = checkListHeader.getRecordTypeInfosByName(); 
        Id checkListHeaderRtId = checkListHeaderRTMap.get('COMM Checklist Header').getRecordTypeId();
        List<Checklist_Header__c> checkListHeaderList = TestUtils.createChecklistHeader(1, assetList[0].Id, false);
        checkListHeaderList.get(0).RecordTypeId = checkListHeaderRtId ;
        insert checkListHeaderList;
        Test.startTest();
            checkListHeaderList[0].Status__c = 'Pass';
        update checkListHeaderList;
        Test.stopTest();
        List<Checklist_Header__c> checklistHeaderUpdatedList = [SELECT Id, Completed_By__c, Completed_On__c FROM Checklist_Header__c WHERE Id =: checkListHeaderList[0].Id];
        List<SVMXC__Installed_Product__c> assetUpdatedList = [SELECT Id, QIP_Status__c, QIP_Completion_Date__c, Case_Owner__c FROM SVMXC__Installed_Product__c WHERE Id =: assetList[0].Id];
        System.assertEquals(assetUpdatedList[0].QIP_Status__c, Constants.COMPLETED);
        System.assertEquals(assetUpdatedList[0].QIP_Completion_Date__c, checklistHeaderUpdatedList[0].Completed_On__c);
        System.assertEquals(assetUpdatedList[0].Case_Owner__c, checklistHeaderUpdatedList[0].Completed_By__c);
    }
    @isTest
    static void testData() {
        Account acc = TestUtils.createAccount(1, true).get(0);
        Opportunity opp = TestUtils.createOpportunity(1, acc.Id, true).get(0);
        Order ordr = TestUtils.createOrder(1, acc.Id, 'Open', false).get(0);
        ordr.OpportunityId = opp.Id;
        insert ordr;
        Product2 prod = TestUtils.createProduct(1, false).get(0);
        prod.RecordTypeId = [select id from recordtype where SobjectType = 'Product2' and developername = 'Master'].id;
        assetList = TestUtils.createInstalledProducts(1, prod.Id, ordr.Id, 'test', false);
        assetList.get(0).RecordTypeID = Schema.SObjectType.SVMXC__Installed_Product__c.getRecordTypeInfosByName().get('COMM Installed Product').getRecordTypeID();
        insert assetList;
        //assetList.get(0).QIP_Status__c = Constants.STATUS_INPROGRESS;
        //assetList.get(0).QIP_ID__c = 'QIP Blade 10387.01 Dual LT';
        //assetList.get(0).Create_Checklist__c = true;
        //insert assetList;
    }
}