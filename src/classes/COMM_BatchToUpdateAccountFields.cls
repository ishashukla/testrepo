/**================================================================      
* Appirio, Inc
* Name: COMM_BatchToUpdateAccountFields  
* Description: T-526438 (To populate converted fields on Account records)
* Created Date: 08/26/2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
 
==================================================================*/
global class COMM_BatchToUpdateAccountFields implements Database.Batchable<sObject> {
	
	String query;
	
	global COMM_BatchToUpdateAccountFields() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'Select Id, COMM_Sales_Rep__c, Owner_First_Name__c, Owner_Last_Name__c, Owner_Manager_Id__c, Owner_Manager_Name__c, OwnerId, RecordTypeId from Account' ;
        
        system.debug('**query**'+query);
        return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Account> accountList) {
   		Set<Id> userIdsSalesRep = new Set<Id>();
    	Set<Id> userIdsOwner = new Set<Id>();
    	Map<Id, User> userMap = new Map<Id, User>();
    	List<Account> listUpdatedAccounts = new List<Account>();
	
      	for(Account a : accountList) {
        	if(getRecordTypeName(a.RecordTypeId, 'Account').contains('COMM')) {
            	if(getRecordTypeName(a.RecordTypeId, 'Account').contains('Endo')
                	&& a.COMM_Sales_Rep__c == null) {
                    	userIdsOwner.add(a.OwnerId); 
            	}
            	if(a.COMM_Sales_Rep__c != null) {
                	userIdsSalesRep.add(a.COMM_Sales_Rep__c);
            	}
        	} else {
            	userIdsOwner.add(a.OwnerId);
            }
      	}

      	if(userIdsOwner.size() > 0 || userIdsSalesRep.size() > 0) {
        	for(User u : [SELECT Id, FirstName, LastName, ManagerId, Manager.Name
                      FROM User 
                      WHERE Id IN : userIdsSalesRep 
                      OR Id IN : userIdsOwner]) {
          		userMap.put(u.Id, u);
        	}
      	}

      	for(Account a : accountList) {
        	if(userIdsSalesRep.contains(a.COMM_Sales_Rep__c)) {
          		if(userMap.containsKey(a.COMM_Sales_Rep__c)) {
            		a.Owner_First_Name__c = userMap.get(a.COMM_Sales_Rep__c).FirstName;
            		a.Owner_Last_Name__c = userMap.get(a.COMM_Sales_Rep__c).LastName;
            		a.Owner_Manager_Id__c = userMap.get(a.COMM_Sales_Rep__c).ManagerId;
            		a.Owner_Manager_Name__c = userMap.get(a.COMM_Sales_Rep__c).Manager.Name;
					listUpdatedAccounts.add(a);
          		}
        	} else if(userIdsOwner.contains(a.OwnerId)) {
            	if(userMap.containsKey(a.OwnerId)) {
              		a.Owner_First_Name__c = userMap.get(a.OwnerId).FirstName;
              		a.Owner_Last_Name__c = userMap.get(a.OwnerId).LastName;
              		a.Owner_Manager_Id__c = userMap.get(a.OwnerId).ManagerId;
        		    a.Owner_Manager_Name__c = userMap.get(a.OwnerId).Manager.Name;
			  		listUpdatedAccounts.add(a);
            	}
          	}
      	}
	  
	  	if(listUpdatedAccounts.size() > 0){
			update listUpdatedAccounts;
	  	}
    }
	
	global void finish(Database.BatchableContext BC) {
		
	}

	global static String getRecordTypeName(Id recordType, String objectType) {
	    Map < Id, Schema.RecordTypeInfo > rtMapById = null;
	    
	    if (rtMapById == null) {
	    	Map < String, Schema.SObjectType > gd = Schema.getGlobalDescribe();
	      	Schema.SObjectType ctype = gd.get(objectType);
	      	Schema.DescribeSObjectResult d1 = ctype.getDescribe();
	      	rtMapById = d1.getRecordTypeInfosById();
		}
	    
	    Schema.RecordTypeInfo recordTypeDetail = rtMapById.get(recordType);
	    if (recordTypeDetail != null) {
	      	return recordTypeDetail.getName();
	    } else {
	      	return null;
	    }
  	}
}