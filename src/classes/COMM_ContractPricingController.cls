//StrykerBlueprintCOMMBP-----similar to NV_ProductPricing T-530873

public  class COMM_ContractPricingController {
	
	//
	public string searchstring {get;set;}
    @AuraEnabled
	public static List<Account> getMyCustomerAccounts(){
		Set<Id> ownersToConsider = new Set<Id>();
        User currentUser = [SELECT Id, Name, UserRole.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        Boolean isAnATMOrRM = false;
        if(currentUser!= null && currentUser.UserRole != null && currentUser.UserRole.Name != null && 
            (currentUser.UserRole.Name.contains('ATM') || currentUser.UserRole.Name.contains('Regional Manager'))){
            isAnATMOrRM = true;
            for(NV_Follow__c record: [SELECT Record_Id__c FROM NV_Follow__c 
                                            WHERE Following_User__c =: UserInfo.getUserId()
                                            AND Record_Type__c = 'User']){
                ownersToConsider.add(Id.valueOf(record.Record_Id__c));
            }
        }
        ownersToConsider.add(UserInfo.getUserId());

        List<Account> accountData = [SELECT Id, Name, AccountNumber, ShippingCity, ShippingState, Nickname__c, OwnerId, Owner.Name 
        								FROM Account 
        								WHERE Purpose__c = 'Main'
        								AND OwnerId in: ownersToConsider
                                    	ORDER BY Name];
        return accountData;
	}

	@AuraEnabled
	public static List<NV_Wrapper.ProductLineData> getAllProducts(){
		Id nv_pricebookID = [SELECT Id FROM PriceBook2 WHERE Name = 'NV Price Book' AND IsActive = true LIMIT 1][0].Id;
		Map<String, NV_Wrapper.ProductLineData> productLineData = new Map<String, NV_Wrapper.ProductLineData>();
		for(Product2 p : [SELECT Id, Name, ProductCode, Part_Number__c, Segment__c, Description, Long_Description__c,
							ODP_Category__c, Catalog_Number__c, GTIN__c, 
							(SELECT Id, UnitPrice FROM PricebookEntries 
								WHERE PriceBook2Id = :nv_pricebookID AND IsActive = true LIMIT 1)
							FROM Product2
							WHERE Product_Division__c = 'NV'
                            AND IsActive = true
                            AND Life_Cycle_Code__c != ''
							ORDER BY ODP_Category__c]){
            if(!String.isBlank(p.ODP_Category__c)){
                if(!productLineData.containsKey(p.ODP_Category__c.toLowerCase())){
                    productLineData.put(p.ODP_Category__c.toLowerCase(), new NV_Wrapper.ProductLineData(p.ODP_Category__c));
                }	
                productLineData.get(p.ODP_Category__c.toLowerCase()).addNewProduct(p);
            }
		}
        List<NV_Wrapper.ProductLineData> finalData = productLineData.values();
        for(integer i = 0; i < finalData.size(); i++){
            NV_Wrapper.ProductLineData pld = finalData[i];
            pld.finalize();
        }
		return finalData;

	}

	@AuraEnabled
	public static List<NV_Wrapper.ModelNPricing> retrievePricing(String customerId, String productIdsCSV){
		System.debug('[DD] CustomerID : '+customerId);
		System.debug('[DD] productIdsCSV : '+productIdsCSV);
		Map<String, NV_Product_Pricing_Settings__c> pricingSettings = NV_Product_Pricing_Settings__c.getAll();
		List<NV_Wrapper.ModelNPricing> pricingData;
		try{
			Datetime today = Datetime.now();
			NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort obj = new NV_CloudRTPB.ModelNProductRealTimePriceLookupICServicePort();
	        NV_RTPM.RealTimePriceMultiRequestType req = new NV_RTPM.RealTimePriceMultiRequestType();
	        req.ConfigName = pricingSettings.get('ConfigName').Value__c==null?'Global':pricingSettings.get('ConfigName').Value__c;
	        req.CustomerID = customerId;
	        req.EffectiveDate = today;
	        req.ModelDate = today;
	        req.CurrencyCode = pricingSettings.get('CurrencyCode').Value__c==null?'USD':pricingSettings.get('CurrencyCode').Value__c;
	        req.OrgUnitID = pricingSettings.get('OrgUnitID').Value__c==null?'NV_US':pricingSettings.get('OrgUnitID').Value__c;
	        req.BusSegCode = pricingSettings.get('BusSegCode').Value__c==null?'':pricingSettings.get('BusSegCode').Value__c;
	        req.ERPCode = pricingSettings.get('ERPCode').Value__c==null?'EMPR':pricingSettings.get('ERPCode').Value__c;
	        NV_RTPM.ProductIDs pIds = new NV_RTPM.ProductIDs();
	        pIds.ProductID = productIdsCSV.split(',');
	        req.ProductIDs = pIds;
	        NV_RTPM.RealTimePriceMultiResponseType response = obj.ModelNProductRealTimePriceLookup(req);
	        System.debug('[DD] 1. ResolvedPrices: '+ response.ResolvedPrices);
	        System.debug('[DD] 2. ResolvedPrices.ResolvedPrice: '+ response.ResolvedPrices.ResolvedPrice);
	        if(response.ResolvedPrices != null && 
	        		response.ResolvedPrices.ResolvedPrice != null && 
	        		response.ResolvedPrices.ResolvedPrice.size()>0){
            	pricingData = new List<NV_Wrapper.ModelNPricing>();
            	for(NV_RTPM.ResolvedPriceType record: response.ResolvedPrices.ResolvedPrice){
            		pricingData.add(new NV_Wrapper.ModelNPricing(record));
            	}
	        }
		}
		catch(Exception e){
			System.debug(e.getStackTraceString());
		}
		return pricingData;
	}
    
    @AuraEnabled
	public static List<User> getFollowableUsers(){
		List<User> followableUsers = new List<User>();
		Set<Id> ownersToConsider = new Set<Id>();
        User currentUser = [SELECT Id, Name, UserRole.Name FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

        List<User> subordinates = NV_Utility.getSubordinateUsersOfSelectedRole(currentUser.Id, currentUser.UserRoleId, '%Territory Manager%');
        if(subordinates.size() > 0){
            subordinates.add(currentUser);
        }
        return subordinates;
	}
}