// (c) 2016 Appirio, Inc.
//
// Class Name: UpdateRepManagerController
// Description: Contoller Class for UpdateRepManager Page. Class to run batch to update manager
//
// June 3 2016, Isha Shukla  Original (T-508005)
//
public with sharing class UpdateRepManagerController {
    public Mancode__c oldManager{get;set;}
    public Mancode__c newManager{get;set;}
    public UpdateRepManagerController() {
        oldManager = new Mancode__c();
        newManager = new Mancode__c();
    }
    public void RunBatchUpdate() {
        try {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,Label.BatchUpdateConfirmation));
            Database.executeBatch(new UpdateManagerBatch(oldManager.Manager__c,newManager.Manager__c));
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
        }   
    }
}