/*************************************************************************************************
Created By:    Brandon Jones
Date:          Nov 03, 2015
Description  : Handler class for Contact Trigger.
**************************************************************************************************/
public class NV_ContactTriggerHandler {
	/*
		Looks at NV Contacts, and then checks to see if they are assigned to a Child Account. It then
		assigns them to the Parent Account. This will need to be rewritten if there is ever a shift to
		multi-level hierarchies in the future.
	*/    
    public static void updateParentAccount(List<Contact> lstNewContacts, Map<Id, Contact> mapOldContacts) {
        Schema.RecordTypeInfo rtByNameCC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('NV Contact');
        Id recordTypeCC = rtByNameCC.getRecordTypeId();
        Map<Id, List<Contact>>nvContactsByAccount = new Map<Id, List<Contact>>();
        for(Contact c : lstNewContacts){
            if(c.RecordTypeId == recordTypeCC){
                if(nvContactsByAccount.containsKey(c.AccountId) == false){
                    nvContactsByAccount.put(c.AccountId, new List<Contact>());
                }
                nvContactsByAccount.get(c.AccountId).add(c);
            } 
        }
        for(Account a : [SELECT Id, ParentId FROM Account WHERE Id IN :nvContactsByAccount.keySet()]){
            if(String.isNotEmpty(a.ParentId)){
                List<Contact> cons = nvContactsByAccount.get(a.Id);
                for(Contact c : cons){
                    c.AccountId = a.ParentId; 
                }
            }
        }
    }
}