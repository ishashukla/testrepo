/*************************************************************************************************
Created By:     Sahil Batra
Date:           Jan 18, 2016
Description:    Handler class for OracleQuoteTrigger Trigger
Sahil Batra     Mar 07,2016 Modified (T-483180) - Added method notifyonQuoteClose
Sahil Batra     May 17,2016 Modified (I-218726) - Added record type check in method updateOpportunityStage and notifyonQuoteClose in query.
Sahil Batra     May 31,2016 Modified (I-I-220516) - Updated value of Constants.quoteStageUpdated
Pratz Joshi     June 18,2016 Modified (S-420435) - Update the SOQL Queries to incorporate the new 
                Instruments Closed Won Opportunity Record Type in the WHERE Clause
Isha Shukla     July 1,2016 Modified(T-516161) Added method updateOnPrimaryQuoteChange
Isha Shukla     July 7,2016 Modified(I-225271)
Prakarsh Jain   August 31, 2016 Modified(T-531095)  Added method updateSharingOnOpportunityRecord
Shreerath Nair  October 5, 2016 Modified(T-542918)  Added method restrictDeletionOfQuote 
Shreerath Nair  Oct 19,2016 Modified(T-549050) - Added function setCPQQuoteCreatedOnInsert & setCPQQuoteCreatedOnDelete
**************************************************************************************************/
public class OracleQuoteTriggerHandler {
  //public static Map<String,String> OppQuoteToOpportunityStage = new Map<String,String>{'Quoting (Start)' => 'Prospecting','Quoting (Pending)' => 'Prospecting','Quoting (Pending Approval)' => 'Pending Approval','Quoting (Approved)' => 'Final Quote','Quoting (Ordered)' => 'Closed Won'}; 
  public static Map<String,String> OppQuoteToOpportunityStage;
  public static final String FLEXFIN = 'Flex Financial';
  public static final String messageString = ', Flex has finished working on your Quote, and it is ready for your review.';
  public static List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
  public static Id recordTypeInstrument = null;
  
  //Pratz Joshi Jun 18th, 2016 Story # - S-420435
  public static Id rt_OppInstrumentClosedWon_Id = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Closed Won Opportunity').getRecordTypeId();
  
  static{
        Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
        recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
    }
  public OracleQuoteTriggerHandler(){
    
  }
  public static void updateOpportunityStage(Map<Id, BigMachines__Quote__c> oldMap,List<BigMachines__Quote__c> newList){
   try{      
    OppQuoteToOpportunityStage = new Map<String,String>();
    Map<String,String> quoteToOpportunityIdMap = new Map<String,String>();
    Map<String,Opportunity> quoteToOpportunityMap = new Map<String,Opportunity>();
    Map<String,Opportunity> opportunityMaptoUpdate = new Map<String,Opportunity>();
    Set<String> OpportunityIds = new Set<String>();
    List<OpportunityTeamMember> lstOpportunityTeamMembers = new List<OpportunityTeamMember>();
    Map<String,AccountTeamMember> accIdToAccountTeamMemberMap = new Map<String,AccountTeamMember>();
    List<CPQStatusToOppStageMapping__c> cpqStatusList = CPQStatusToOppStageMapping__c.getall().values();
    if(cpqStatusList.size()>0){
      for(CPQStatusToOppStageMapping__c customSetting : cpqStatusList){
        if(!OppQuoteToOpportunityStage.containsKey(customSetting.Name)){
          OppQuoteToOpportunityStage.put(customSetting.Name,customSetting.Opportunity_Stage__c);
        }
      }
    }
    for(BigMachines__Quote__c quote : newList){
      if(oldMap == null || (oldMap.get(quote.Id).BigMachines__Status__c != quote.BigMachines__Status__c)){
        if(quote.BigMachines__Is_Primary__c){
            OpportunityIds.add(quote.BigMachines__Opportunity__c);
            quoteToOpportunityIdMap.put(quote.Id,quote.BigMachines__Opportunity__c);
        }
      }
    }
    
    Map<String,Opportunity> idToOpportunityMap = new Map<String,Opportunity>();
    if(OpportunityIds.size() > 0){
      List<Opportunity> opportunityList = new List<Opportunity>();
      
      //Pratz Joshi Jun 18th, 2016 Story # S-420435
      //Update the SOQL Query to incorporate the new 
      //Instruments Closed Won Opportunity Record Type in the WHERE Clause
      opportunityList = [SELECT Id,StageName,AccountId,RecordTypeId 
                         FROM Opportunity WHERE Id IN :OpportunityIds 
                         AND (RecordTypeId =:recordTypeInstrument OR
                             RecordTypeId =:rt_OppInstrumentClosedWon_Id)];
      for(Opportunity opp : opportunityList){
        idToOpportunityMap.put(opp.Id,opp);
      }
    for(String quoteId : quoteToOpportunityIdMap.keySet()){
      String OppId = quoteToOpportunityIdMap.get(quoteId);
      if(idToOpportunityMap.containsKey(OppId)){
        quoteToOpportunityMap.put(quoteId,idToOpportunityMap.get(OppId));
      }
    }
    for(BigMachines__Quote__c quote : newList){
      if(quote.BigMachines__Status__c!=null && OppQuoteToOpportunityStage.containsKey(quote.BigMachines__Status__c) && quoteToOpportunityMap.containsKey(quote.Id)){
        Opportunity opp = quoteToOpportunityMap.get(quote.Id);
        if(OppQuoteToOpportunityStage.containsKey(quote.BigMachines__Status__c)){
            opp.StageName = OppQuoteToOpportunityStage.get(quote.BigMachines__Status__c);
         /*   if(opp.StageName != 'Submitted to Flex'){
              opp.Other_Status_Date__c = Date.Today();
            }
            else{
              opp.Flex_Status_Date__c = Date.Today().addDays(4);
            }*/
            opportunityMaptoUpdate.put(opp.Id,opp);
        }
      }
      else if(quote.BigMachines__Status__c!=null && quote.BigMachines__Status__c.containsIgnoreCase('Flex') && quoteToOpportunityMap.containsKey(quote.Id)){
        Opportunity opp = quoteToOpportunityMap.get(quote.Id);
        opp.StageName = 'Submitted to Flex';
        opportunityMaptoUpdate.put(opp.Id,opp);
      }
    }
    if(opportunityMaptoUpdate.size() > 0){
      Constants.quoteStageUpdated = true;
      update opportunityMaptoUpdate.values();
    }
    }
    system.debug('opportunityMaptoUpdate.values()>>>'+opportunityMaptoUpdate.values());
   }
    catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
  }
  
  public static void notifyonQuoteClose(Map<Id, BigMachines__Quote__c> oldMap,List<BigMachines__Quote__c> newList){
   try{      
    Set<Id> OpportunityIdSet = new Set<Id>();
    for(BigMachines__Quote__c quote : newList){
        if(oldMap.get(quote.Id).BigMachines__Status__c != quote.BigMachines__Status__c){
            if(quote.Financing_Requested__c && oldMap.get(quote.Id).BigMachines__Status__c != null && quote.BigMachines__Status__c!=null && oldMap.get(quote.Id).BigMachines__Status__c.containsIgnoreCase('Flex') && quote.BigMachines__Status__c.containsIgnoreCase('Quoting (Approved)')){
              if(quote.BigMachines__Is_Primary__c){ 
                OpportunityIdSet.add(quote.BigMachines__Opportunity__c);
              }
            }
        }
    }
    if(OpportunityIdSet.size() > 0){
        List<Opportunity> opportunityList = new List<Opportunity>();
        
        //Pratz Joshi Jun 18th, 2016 Story # S-420435
        //Update the SOQL Query to incorporate the new 
        //Instruments Closed Won Opportunity Record Type in the WHERE Clause
        opportunityList = [SELECT Id,OwnerID,RecordTypeId
                           FROM Opportunity 
                           WHERE Id IN :OpportunityIdSet 
                           AND (RecordTypeId =:recordTypeInstrument OR
                                RecordTypeId =:rt_OppInstrumentClosedWon_Id)];
                                
        for(Opportunity opportunity : opportunityList){
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(Instruments_OpportunityTriggerHandler.returnInputFeed(opportunity.Id,new Set<Id>{opportunity.ownerId},messageString,false));
            batchInputs.add(batchInput);
        }
        if(batchInputs.size() > 0){
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
        }        
    }
   }
   catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
  }
 // Method used to associate base product to base trail survey of opportunity related with primary quote  and disassociate when changes to not primary
  public static void updateOnPrimaryQuoteChange(Map<Id, BigMachines__Quote__c> oldMap,List<BigMachines__Quote__c> newList) {
   try{      
    Set<Id> oppIds = new Set<Id>();
    Map<Id,Id> primaryQuoteToOppMap = new Map<Id,Id>();
    Map<Id,Id> oppToPrimaryQuoteMap = new Map<Id,Id>();
    Set<Id> notPrimaryQuotesId = new Set<Id>();
    List<Base_Trail_Survey__c> baseTrailListToUpdate = new List<Base_Trail_Survey__c>();
    List<Base_Trail_Survey__c> baseTrailListToUpdateForNotPrimaryQuote = new List<Base_Trail_Survey__c>();
    
    List<Base_Product__c> disassociateBaseProduct = new List<Base_Product__c>();
    //Boolean isPrimary = false;
    System.debug('newList='+newList);
     System.debug('oldmap=='+oldMap);
    for(BigMachines__Quote__c quote : newList) {
        if(oldMap != null && oldMap.get(quote.Id).BigMachines__Is_Primary__c != quote.BigMachines__Is_Primary__c) {
            System.debug('quote.BigMachines__Is_Primary__c='+quote.BigMachines__Is_Primary__c);
            if(quote.BigMachines__Is_Primary__c == True) {
                //isPrimary = true; when quote updates to primary 
                if(!primaryQuoteToOppMap.containsKey(quote.Id)) {
                    primaryQuoteToOppMap.put(quote.Id,quote.BigMachines__Opportunity__c);
                }   
                if(!oppToPrimaryQuoteMap.containsKey(quote.BigMachines__Opportunity__c)) {
                    oppToPrimaryQuoteMap.put(quote.BigMachines__Opportunity__c,quote.Id);
                }
            } else if(quote.BigMachines__Is_Primary__c == False) {
                //isPrimary = false; // when quote updates to become not primary
                notPrimaryQuotesId.add(quote.Id);
            }
                    
        }
    }
    
        
    
    Map<Id,Id> oppOfPrimaryQuoteToBaseTrailMap = new Map<Id,Id>();
    Map<Id,Id> baseTrailToOppOfPrimaryQuoteMap = new Map<Id,Id>();
    if(primaryQuoteToOppMap.size() > 0){
    for(Base_Trail_Survey__c bts : [SELECT Id,Opportunity__c FROM Base_Trail_Survey__c WHERE Opportunity__c IN :primaryQuoteToOppMap.values()]) {                           
        if(!oppOfPrimaryQuoteToBaseTrailMap.containsKey(bts.Opportunity__c)) {
            oppOfPrimaryQuoteToBaseTrailMap.put(bts.Opportunity__c,bts.Id);
        }
    }}
    List<Base_Product__c> associateBaseProduct = new List<Base_Product__c>();
    
    if(primaryQuoteToOppMap.size() > 0){
    for(Base_Product__c bp : [SELECT Base_Trail_Survey__c,Oracle_Quote__c FROM Base_Product__c WHERE Oracle_Quote__c IN :primaryQuoteToOppMap.keySet()]) {
        
            bp.Base_Trail_Survey__c = oppOfPrimaryQuoteToBaseTrailMap.get(primaryQuoteToOppMap.get(bp.Oracle_Quote__c));
            associateBaseProduct.add(bp); // assoiciate all base product to base trail of opportunity
            
    }}   
    System.debug('associateBaseProduct=='+associateBaseProduct);
    if(associateBaseProduct.size() > 0 ) {
        update associateBaseProduct;
    }
    system.debug('associateBaseProduct==='+associateBaseProduct);
    
    //System.debug('disassociateBaseProduct=='+disassociateBaseProduct);
   //  if(disassociateBaseProduct.size() > 0) {
   //   update disassociateBaseProduct;
  //   }
     if(oppOfPrimaryQuoteToBaseTrailMap.size() > 0){
     for(Base_Trail_Survey__c bts :[SELECT Oracle_Quote__c,Opportunity__c,Roll_Up_Total__c FROM Base_Trail_Survey__c WHERE Id IN :oppOfPrimaryQuoteToBaseTrailMap.values()]) {
        
            bts.Oracle_Quote__c = oppToPrimaryQuoteMap.get(bts.Opportunity__c);
            //if(mapBaseTrailToTotal.containsKey(bts.Id)) {
            //  bts.Roll_Up_Total__c = mapBaseTrailToTotal.get(bts.Id);
            //}
            baseTrailListToUpdate.add(bts); // assoicate base trail to primary quote
        
        // else {
            //bts.Oracle_Quote__c = null;  // disassociate base trail from quote which is not primary
        //  baseTrailListToUpdate.add(bts); 
        //}
     }}
     if(baseTrailListToUpdate.size() > 0 ) {
      update baseTrailListToUpdate;
     }
    	if(notPrimaryQuotesId.size() > 0){
        for(Base_Product__c bp : [SELECT Base_Trail_Survey__c,Oracle_Quote__c FROM Base_Product__c WHERE Oracle_Quote__c IN :notPrimaryQuotesId]) {
            bp.Base_Trail_Survey__c = null;
            disassociateBaseProduct.add(bp); // disassociate all base product of quote from base trail of opportunity
        }}
        if(disassociateBaseProduct.size() > 0) {
            update disassociateBaseProduct;
        }
        if(notPrimaryQuotesId.size() > 0){
        for(Base_Trail_Survey__c bts : [SELECT Oracle_Quote__c FROM Base_Trail_Survey__c WHERE Oracle_Quote__c IN :notPrimaryQuotesId]) {
            bts.Oracle_Quote__c = null;  // disassociate base trail from quote which is not primary
            baseTrailListToUpdateForNotPrimaryQuote.add(bts);
        }}
    
     
     if(baseTrailListToUpdateForNotPrimaryQuote.size() > 0) {
        update baseTrailListToUpdateForNotPrimaryQuote;
     }
     
     System.debug('baseTrailListToUpdate=='+baseTrailListToUpdate);
     Map<Id,decimal> mapBaseTrailToTotal = new Map<Id,decimal>();
    Set<Id> setofBaseTrailSurveyidforPrimary = new Set<Id>(); 
    for(Base_Trail_Survey__c bts : baseTrailListToUpdate) {
        setofBaseTrailSurveyidforPrimary.add(bts.Id);
    }
    Set<Id> setofBaseTrailSurveyidforNotPrimary = new Set<Id>(); 
    for(Base_Trail_Survey__c bts : baseTrailListToUpdateForNotPrimaryQuote) {
        setofBaseTrailSurveyidforNotPrimary.add(bts.Id);
    }
     decimal temp = 0;
  	if(setofBaseTrailSurveyidforPrimary.size() > 0){
     for(Base_Product__c baseProduct : [SELECT Price__c,Quantity__c,Base_Trail_Survey__c FROM  Base_Product__c WHERE Base_Trail_Survey__c IN :setofBaseTrailSurveyidforPrimary ]) {
      if(!mapBaseTrailToTotal.containsKey(baseProduct.Base_Trail_Survey__c)) {
        mapBaseTrailToTotal.put( baseProduct.Base_Trail_Survey__c , 0 );
      } 
      if(mapBaseTrailToTotal.containsKey(baseProduct.Base_Trail_Survey__c) && baseProduct.Price__c != null && baseProduct.Quantity__c != null) {
         temp = (baseProduct.Price__c * baseProduct.Quantity__c);
         mapBaseTrailToTotal.put(baseProduct.Base_Trail_Survey__c,mapBaseTrailToTotal.get(baseProduct.Base_Trail_Survey__c) + temp);
        }
     }}
     List<Base_Trail_Survey__c> baseTrailListToUpdateTotal = new List<Base_Trail_Survey__c>();
     for(Id btsIDs : setofBaseTrailSurveyidforPrimary) {
        Decimal calculatedValue = 0;
        if(mapBaseTrailToTotal.ContainsKey(btsIds)){
            calculatedValue = mapBaseTrailToTotal.get(btsIds);
        }
        Base_Trail_Survey__c btsRecord = new Base_Trail_Survey__c(Id = btsIds , Roll_Up_Total__c = calculatedValue);
        baseTrailListToUpdateTotal.add(btsRecord);
     }
     System.debug('baseTrailListToUpdateTotal=='+baseTrailListToUpdateTotal);
     if( baseTrailListToUpdateTotal.size() > 0 ) {
        update baseTrailListToUpdateTotal;
     }
     // for non primary
     if(setofBaseTrailSurveyidforNotPrimary.size() > 0){
     for(Base_Product__c baseProduct : [SELECT Price__c,Quantity__c,Base_Trail_Survey__c FROM  Base_Product__c WHERE Base_Trail_Survey__c IN :setofBaseTrailSurveyidforNotPrimary ]) {
      if(!mapBaseTrailToTotal.containsKey(baseProduct.Base_Trail_Survey__c)) {
        mapBaseTrailToTotal.put( baseProduct.Base_Trail_Survey__c , 0 );
      } 
      if(mapBaseTrailToTotal.containsKey(baseProduct.Base_Trail_Survey__c) && baseProduct.Price__c != null && baseProduct.Quantity__c != null) {
         temp = (baseProduct.Price__c * baseProduct.Quantity__c);
         mapBaseTrailToTotal.put(baseProduct.Base_Trail_Survey__c,mapBaseTrailToTotal.get(baseProduct.Base_Trail_Survey__c) + temp);
        }
     }}
     List<Base_Trail_Survey__c> baseTrailListToUpdateRollupTotal = new List<Base_Trail_Survey__c>();
     for(Id btsIDs : setofBaseTrailSurveyidforNotPrimary) {
        Decimal calculatedValue = 0;
        if(mapBaseTrailToTotal.ContainsKey(btsIds)){
            calculatedValue = mapBaseTrailToTotal.get(btsIds);
        }
        Base_Trail_Survey__c btsRecord = new Base_Trail_Survey__c(Id = btsIds , Roll_Up_Total__c = calculatedValue);
        baseTrailListToUpdateRollupTotal.add(btsRecord);
     }
     System.debug('baseTrailListToUpdateTotal=='+baseTrailListToUpdateTotal);
     if( baseTrailListToUpdateRollupTotal.size() > 0 ) {
        update baseTrailListToUpdateRollupTotal;
     }
     
   }
   catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
  }
  // When Primary quote deletes opportunity fields will get null and base trail monthly total get null
  public static void OnPrimaryQuoteBeforeDelete(List<BigMachines__Quote__c> oldList) {
   try{      
    Set<Id> setOfOppId = new Set<Id>();
    Set<Id> setOfQuoteId = new Set<Id>();
    List<Opportunity> oppListToUpdate = new List<Opportunity>();
    for(BigMachines__Quote__c quote : oldList) {
        if(quote.BigMachines__Is_Primary__c == True) {
            setOfOppId.add(quote.BigMachines__Opportunity__c);
            setOfQuoteId.add(quote.Id);
        }
    }
    if(setOfOppId.size() > 0){
        for(Id oppId : setOfOppId){
                Opportunity opp = new Opportunity();
                opp.Id = oppId ;
                opp.Capital_Amount__c = 0;
                opp.Service_Amount__c = 0;
                opp.Service_Upfront__c = 0;
                opp.Service_Monthly__c = 0;
                opp.Total_Trailing_Impact__c = 0;
                oppListToUpdate.add(opp);
        }
    }
    if(oppListToUpdate.size() > 0){
        update oppListToUpdate;
    }
    List<Base_Trail_Survey__c> baseTrailtoUpdate = new List<Base_Trail_Survey__c>();
    if(setOfQuoteId.size() > 0){
    for(Base_Trail_Survey__c bts : [SELECT Id,Roll_Up_Total__c FROM Base_Trail_Survey__c WHERE Oracle_Quote__c IN :setOfQuoteId]) {
        bts.Roll_Up_Total__c = 0;
        baseTrailtoUpdate.add(bts);
    }}
    if(baseTrailtoUpdate.size() > 0 ) {
        update baseTrailtoUpdate;
    }
   }
   catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
  }
  
  //T-531095
  //Method to add users present in the approver's field on Oracle Quote to Opportunity Share record and Oracle Quote Share record when Big Machine's quote status is changed to Quoting (Pending Approval)
  public static void updateSharingOnOpportunityRecord(List<BigMachines__Quote__c> newList, Map<Id, BigMachines__Quote__c> oldMap){
   try{      
    List<BigMachines__Quote__c> listToConsider = new List<BigMachines__Quote__c>();
    List<Opportunity> listOfOpportunities = new List<Opportunity>();
    Set<Id> quoteIdSet = new Set<Id>();
    Set<Id> opportunityIdSet = new Set<Id>();
    Map<Id,List<String>> mapOppIdToEmail = new Map<Id,List<String>>();
    Map<Id, Set<String>> mapOppIdToEmailId = new Map<Id, Set<String>>();
    Map<Id, List<String>> mapQuoteIdToEmail = new Map<Id, List<String>>();
    Map<Id, Set<String>> mapQuoteIdToEmailId = new Map<Id, Set<String>>();
    List<String> listEmailId = new List<String>();
      Set<Id> setUserId = new Set<Id>();
      List<User> listUser = new List<User>();
      Set<String> setEmailIds = new Set<String>(); 
      Map<String, Id> mapEmailToUserId = new Map<String, Id>();
      Map<Id,Id> mapQuoteIdToOppId = new Map<Id,Id>();
      Map<Id,Id> mapOppIdToQuoteId = new Map<Id,Id>();
      List<OpportunityShare> listOppShare = new List<OpportunityShare>();
      List<BigMachines__Quote__Share> listQuoteShare = new List<BigMachines__Quote__Share>();
      Map<Id,Id> OpportunityToRecordTypeIdMap = new Map<Id,Id>();
      Set<Id> allOpprtunityIdSet = new Set<Id>();
       for(BigMachines__Quote__c quote : newList){
         if((oldMap == null && quote.BigMachines__Status__c == 'Quoting (Pending Approval)') || (quote.BigMachines__Status__c == 'Quoting (Pending Approval)' && oldMap.get(quote.Id).BigMachines__Status__c != 'Quoting (Pending Approval)')){
             allOpprtunityIdSet.add(quote.BigMachines__Opportunity__c);
         }
      }
      if(allOpprtunityIdSet.size() > 0){
        for(Opportunity opp : [Select Id,recordTypeId FROM Opportunity WHERE Id IN:allOpprtunityIdSet]){
            OpportunityToRecordTypeIdMap.put(opp.Id,opp.recordTypeId);
        }
      }
      
    for(BigMachines__Quote__c quote : newList){
       Id currentOpportunityRecordTypeId = null;
       if(quote.BigMachines__Opportunity__c !=null && OpportunityToRecordTypeIdMap.containsKey(quote.BigMachines__Opportunity__c)){
            currentOpportunityRecordTypeId = OpportunityToRecordTypeIdMap.get(quote.BigMachines__Opportunity__c);
          }
       if(currentOpportunityRecordTypeId != null && (currentOpportunityRecordTypeId == rt_OppInstrumentClosedWon_Id || currentOpportunityRecordTypeId == recordTypeInstrument)){
        if((oldMap == null && quote.BigMachines__Status__c == 'Quoting (Pending Approval)') || (quote.BigMachines__Status__c == 'Quoting (Pending Approval)' && oldMap.get(quote.Id).BigMachines__Status__c != 'Quoting (Pending Approval)')){
            listToConsider.add(quote);
            quoteIdSet.add(quote.Id);
            opportunityIdSet.add(quote.BigMachines__Opportunity__c);
            mapQuoteIdToOppId.put(quote.Id, quote.BigMachines__Opportunity__c);
            mapOppIdToQuoteId.put(quote.BigMachines__Opportunity__c, quote.Id);
          }
      }
    }
    if(opportunityIdSet.size()>0){
        listOfOpportunities = [SELECT Id, Name, Approvers__c FROM Opportunity WHERE Id IN: opportunityIdSet];
    }
    if(listToConsider.size()>0){
      for(BigMachines__Quote__c quote : listToConsider){
        if(quote.Approvers__c != null || quote.Approvers__c != ''){
          List<String> listEmails = new List<String>();
          Set<String> setEmails = new Set<String>();
          listEmails = quote.Approvers__c.split(';'); 
          mapQuoteIdToEmail.put(quote.Id, listEmails);
          setEmails.addAll(listEmails);
          mapQuoteIdToEmailId.put(quote.Id, setEmails);
        }
      }
    }
    if(mapQuoteIdToEmail.size()>0){
      for(Id quoteId : mapQuoteIdToEmail.keySet()){
        List<String> listOfEmail = mapQuoteIdToEmail.get(quoteId);
        for(String mails : listOfEmail){
          setEmailIds.add(mails);
        }
      }
    }
    if(setEmailIds.size()>0){
        listUser = [SELECT Id, Email FROM User WHERE Email IN: setEmailIds];
    }
    if(listUser.size()>0){
      for(User user : listUser){
        mapEmailToUserId.put(user.Email, user.Id);
      }
    }
      if(listToConsider.size()>0){
      for(BigMachines__Quote__c quote : listToConsider){
        if(quote.Approvers__c != null || quote.Approvers__c != ''){
          for(String emailIds : mapQuoteIdToEmailId.get(quote.Id)){
              BigMachines__Quote__Share quoteShare = new BigMachines__Quote__Share();
              quoteShare.UserOrGroupId = mapEmailToUserId.get(emailIds);
              quoteShare.AccessLevel = 'Edit';
              quoteShare.RowCause = Schema.BigMachines__Quote__Share.RowCause.Manual;
              quoteShare.ParentId = quote.Id;
              listQuoteShare.add(quoteShare);
          }
        }
      }
    }
    if(listOfOpportunities.size()>0){
      for(Opportunity opp : listOfOpportunities){
        Set<String> setString = new Set<String>();
        setString = mapQuoteIdToEmailId.get(mapOppIdToQuoteId.get(opp.Id));
        for(String emailIds : setString){
          OpportunityShare oppShare = new OpportunityShare();
          oppShare.userOrGroupId = mapEmailToUserId.get(emailIds);
          oppShare.OpportunityAccessLevel = 'Edit';
          oppShare.RowCause = Schema.OpportunityShare.RowCause.Manual;
          oppShare.OpportunityId = opp.Id;
          listOppShare.add(oppShare);
        }
      }
    }
      if(listOppShare.size()>0){
        Database.insert(listOppShare,false);
      }
      if(listQuoteShare.size()>0){
        Database.insert(listQuoteShare,false);
      }
    }
    catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
  }
  
  
  //T-542918 (SN) - Function to restrict deletion of quote if its 'Primary' or related to Closed Opportunity.
  public static void restrictDeletionOfQuote(List<BigMachines__Quote__c> oldList){
  
      try{
          
          SET<id> primaryQuoteSet = new SET<id>();
          
          //Get all the primary quotes
          for(BigMachines__Quote__c quote : oldList){
              
              if(quote.BigMachines__Is_Primary__c == TRUE){
                  
                  primaryQuoteSet.add(quote.Id);
              }
          }
          
          if(primaryQuoteSet.size()>0){
            //Query the Quotes which are primary and whose Opportunities are closed.
            Map<Id,BigMachines__Quote__c> quoteMap = new Map<Id,BigMachines__Quote__c>([SELECT Id 
                                                                                      FROM BigMachines__Quote__c 
                                                                                      WHERE ID IN : primaryQuoteSet
                                                                                      AND BigMachines__Opportunity__r.StageName IN ('Closed Won','Closed Lost')
                                                                                      AND BigMachines__Opportunity__r.Recordtype.DeveloperName IN ('Instruments_Opportunity_Record_Type','Instruments_Closed_Won_Opportunity')]);   
            //loop through all quotes
            for(BigMachines__Quote__c quote : oldList) {
                //check if record exist in the map to prevent deletion
                if(quoteMap.containsKey(quote.Id)){                   
                    quote.addError(Messages__c.getInstance('RestrictQuoteDeletion').Message_Text__c);   
                }       
            }
          }    
      }
      catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
  }
  
  
  //T-549050(SN)  function to update the CPQ_Quote_Created field of Opportunity if Quote is associated with it.
  public static void setCPQQuoteCreatedOnInsert(Map<id,BigMachines__Quote__c> newQuoteMap){
      
      try{
          
          //get all the bigmachine quote record if the Opportunity associated with Quote is Instrument type and CPQ_Quote_Created is false.
          Set<Id> opportunityIdSet = new Set<id>();
          if(newQuoteMap.size() > 0){
          for(BigMachines__Quote__c quote : [SELECT BigMachines__Opportunity__c FROM BigMachines__Quote__c WHERE ID IN:newQuoteMap.KeySet() AND BigMachines__Opportunity__r.CPQ_Quote_Created__c = false AND   BigMachines__Opportunity__r.Recordtype.DeveloperName IN ('Instruments_Opportunity_Record_Type','Instruments_Closed_Won_Opportunity')]){
            
            if(!opportunityIdSet.contains(quote.BigMachines__Opportunity__c)){
                opportunityIdSet.add(quote.BigMachines__Opportunity__c);
            }    
          }}
          
          List<Opportunity> oppListToUpdate = new List<Opportunity>();
          
          //if any opportunity exist then update the CPQ_Quote_Created__c field of Opportunity to true.
          if(opportunityIdSet.size() > 0){
              
            for(Id oppId : opportunityIdSet){
                Opportunity opp = new Opportunity();
                opp.Id = oppId ;
                opp.CPQ_Quote_Created__c = true;
                oppListToUpdate.add(opp);
            }
        }
        
        if(oppListToUpdate.size() > 0){
            update oppListToUpdate;
        }
          
      }catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
  }
  
  
  //T-549050(SN) function to check if no quote is associated with opportunity then set its CPQ_Quote_Created__c to false
  public static void setCPQQuoteCreatedOnDelete(Map<id,BigMachines__Quote__c> oldQuoteMap){
      
      try{
      
            //get all the bigmachine quote record if the Opportunity associated with Quote is Instrument type and CPQ_Quote_Created is false.
            Set<Id> opportunityIdSet = new Set<id>();
            if(oldQuoteMap.size() > 0){
            for(BigMachines__Quote__c quote : [SELECT BigMachines__Opportunity__c FROM BigMachines__Quote__c WHERE ID IN:oldQuoteMap.KeySet() AND BigMachines__Opportunity__r.CPQ_Quote_Created__c = true AND   BigMachines__Opportunity__r.Recordtype.DeveloperName IN ('Instruments_Opportunity_Record_Type','Instruments_Closed_Won_Opportunity')]){
       
                if(!opportunityIdSet.contains(quote.BigMachines__Opportunity__c)){
                    opportunityIdSet.add(quote.BigMachines__Opportunity__c);
                }
            }}
            
            Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>();
            
            //get all the quote child records of the opportunities in opportunityIdSet
            if(opportunityIdSet.size()>0){
                for(Opportunity opp: [SELECT Id,(SELECT Id from BigMachines__BigMachines_Quotes__r) FROM Opportunity WHERE Id IN : opportunityIdSet]){
                    opportunityMap.put(opp.id,opp);
                }
            } 
            
            List<Opportunity> oppListToUpdate = new List<Opportunity>();
            //flag to be used to check if any quote still exist on Opportunity(true if exist or else false)
            Boolean flag = false;
            
            //check if any of the quote associated with the opportunity is not existed in the oldMap then dont set CPQ_Quote_Created field to false.  
            if(!opportunityMap.isEmpty()){
                for(Id oppId: opportunityIdSet){
                    flag = false;
                    if(opportunityMap.containskey(oppId)){
                        
                        for(BigMachines__Quote__c quote : opportunityMap.get(oppId).BigMachines__BigMachines_Quotes__r){
                            //if any quote record doesnt exist in oldMap then set flag to true to skip updation of this opportunity record
                            if(!oldQuoteMap.containskey(quote.Id)){
                                flag=true;
                                break;
                            }

                        }
                        //if flag is false then update the Opportunity as no other quote is associated with this Opportunitu
                        if(flag==false){
                            Opportunity opp = new Opportunity();
                            opp.Id = oppId ;
                            opp.CPQ_Quote_Created__c = false;
                            oppListToUpdate.add(opp);  
                        }
                    } 
              
                }
            }
            
            //update opportunity
            if(oppListToUpdate.size() > 0){
                update oppListToUpdate;
            }
            
      }catch(Exception ex){ ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'OracleQuoteTriggerHandler', true);}
      
      
  }
  
  
  
    
}