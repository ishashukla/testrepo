// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_TaskTriggerHandler
// Description: Handler Class for TaskTrigger. 
//
// February 17 2016, Prakarsh Jain  Original (T-477366)
//
public class  Instruments_TaskTriggerHandler {
  public static List<ConnectApi.BatchInput> accountPost = new List<ConnectApi.BatchInput>();
  public static List<ConnectApi.BatchInput> contactPost = new List<ConnectApi.BatchInput>();
  public static List<ConnectApi.BatchInput> opportunityPost = new List<ConnectApi.BatchInput>();
  public static List<ConnectApi.BatchInput> chatterPostList = new List<ConnectApi.BatchInput>();
  public static Map<Id,Id> ownerMap;
  public static Map<String,Boolean> customSettingMap;
  public static Map<Id,Id> taskIdToObjectMap;
  
  static{
    customSettingMap = new Map<String,Boolean>();
    List<TriggerRunSetting__c> triggerRunList = TriggerRunSetting__c.getall().values();
    for(TriggerRunSetting__c triggerValue : triggerRunList){
      if(!customSettingMap.containsKey(triggerValue.Name)){
        customSettingMap.put(triggerValue.Name, triggerValue.Trigger_Run__c);
      }
    }
  }
  
  public static void onInsert(List<Task> newTaskList){
    if(customSettingMap.size()>0 && customSettingMap.containsKey('MarketingActivityTaskTrigger') && customSettingMap.get('MarketingActivityTaskTrigger')){
      postChatter(newTaskList);
    }
  }
  
  public static void onUpdateDelete(List<Task> newTaskList, Map<Id, Task> oldMap, Boolean IsDelete){
    //if(customSettingMap.size()>0 && customSettingMap.containsKey('MarketingActivityTaskTrigger') && customSettingMap.get('MarketingActivityTaskTrigger')){
      restrictEditDeleteOfRecords(newTaskList, oldMap, IsDelete);
    //}
  }
  
  public static void postChatter(List<Task> newTaskList){
    ownerMap = new Map<Id,Id>(); 
    taskIdToObjectMap = new Map<Id,Id>();
    List<Account> lstAccount = new List<Account>();
    List<Contact> lstContact = new List<Contact>();
    List<Opportunity> lstOpportunity = new List<Opportunity>();
    Set<Id> setAccountId = new Set<Id>();
    Set<Id> setContactId = new Set<Id>();
    Set<Id> setOpportunityId = new Set<Id>();
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Marketing Task Record');
    Id recordTypeId = rtByName.getRecordTypeId();
    for(Task tsk : newTaskList){
      if(tsk.RecordTypeId == recordTypeId){
        if(tsk.WhoId!=null && String.valueOf(tsk.WhoId).startsWith('003')){
          setContactId.add(tsk.WhoId);
          ownerMap.put(tsk.WhoId, tsk.OwnerId);
          taskIdToObjectMap.put(tsk.WhoId,tsk.Id);
        }
        else if(tsk.WhatId != null && String.valueOf(tsk.WhatId).startsWith('006')){
          setOpportunityId.add(tsk.WhatId);
          ownerMap.put(tsk.WhatId, tsk.OwnerId);
          taskIdToObjectMap.put(tsk.WhatId,tsk.Id);
        }
        else if(tsk.WhatId != null && String.valueOf(tsk.WhatId).startsWith('001')){
          setAccountId.add(tsk.WhatId);
          ownerMap.put(tsk.WhatId, tsk.OwnerId);
          taskIdToObjectMap.put(tsk.WhatId,tsk.Id);
        }
      }
    }
    if(setAccountId.size()>0){
      lstAccount = [SELECT Id, Name FROM Account WHERE Id IN: setAccountId];
    }
    if(setContactId.size()>0){
      lstContact = [SELECT Id, Name FROM Contact WHERE Id IN: setContactId];
    }
    if(setOpportunityId.size()>0){
      lstOpportunity = [SELECT Id, Name FROM Opportunity WHERE Id IN: setOpportunityId];
    }
    if(lstAccount.size() > 0){
      for(Account acc : lstAccount){
        ConnectApi.BatchInput accPost = new ConnectApi.BatchInput(returnInputFeed(acc,null,null));
        accountPost.add(accPost);
      }
    }
    if(lstContact.size() > 0){
      for(Contact con : lstContact){
        ConnectApi.BatchInput conPost = new ConnectApi.BatchInput(returnInputFeed(null,con,null));
        contactPost.add(conPost);
      }
    }
    if(lstOpportunity.size() > 0){
      for(Opportunity opp : lstOpportunity){
        ConnectApi.BatchInput oppPost = new ConnectApi.BatchInput(returnInputFeed(null,null,opp));
        opportunityPost.add(oppPost);
      }
    }
    
    if(accountPost.size() > 0){
       chatterPostList.addAll(accountPost);
    }
    if(contactPost.size() > 0){
        chatterPostList.addAll(contactPost);
    }
    if(opportunityPost.size() > 0){
       chatterPostList.addAll(opportunityPost);
    }
      if(!Test.isRunningTest()){
     if(chatterPostList.size() > 0){
        ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), chatterPostList);
    }
      }
  }
  
  //Method to post chatter on Account/Contact/Opportunity when a task is created.
  public static ConnectApi.FeedItemInput returnInputFeed(Account acc, Contact con, Opportunity opp){
    String communityId = null;
    ConnectApi.FeedType feedType = ConnectApi.FeedType.UserProfile;
    ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
    ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
    ConnectApi.TextSegmentInput textSegment;
    ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
    ConnectApi.LinkSegmentInput linksegment = new ConnectApi.LinkSegmentInput();
    messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
    textSegment = new ConnectApi.TextSegmentInput();
    String baseUrl = String.valueOf(URL.getSalesforceBaseUrl().toExternalForm());
    if(acc!=null){
      textSegment.text = Label.Marketing_Chatter_Notification + ' ' +acc.Name;
    }
    if(con!=null){
      textSegment.text =  Label.Marketing_Chatter_Notification + ' ' +con.Name;
    }
    if(opp!=null){
      textSegment.text =  Label.Marketing_Chatter_Notification + ' ' +opp.Name;
    }
    messageInput.messageSegments.add(textSegment);
    if(acc!=null){
      if(ownerMap.containsKey(acc.Id)){
         mentionSegment.id = ownerMap.get(acc.Id);
         linksegment.url = baseUrl + '/' + taskIdToObjectMap.get(acc.Id);
         system.debug('linksegment.url>>>'+linksegment.url);
      }
    }
    if(con!=null){
       if(ownerMap.containsKey(con.Id)){
         mentionSegment.id = ownerMap.get(con.Id);
         linksegment.url = baseUrl+'/'+taskIdToObjectMap.get(con.Id);
      }
    }
    if(opp!=null){
       if(ownerMap.containsKey(opp.Id)){
         mentionSegment.id = ownerMap.get(opp.Id);
         linksegment.url = baseUrl+'/'+taskIdToObjectMap.get(opp.Id);
      }
    }
   // messageInput.messageSegments.add(mentionSegment);
    textSegment = new ConnectApi.TextSegmentInput();
    textSegment.text = ' \n ';
    messageInput.messageSegments.add(textSegment);
    messageInput.messageSegments.add(linksegment);
    input.body = messageInput;
    if(acc!=null){
      input.subjectId = acc.id;  
    } 
    if(con!=null){
      input.subjectId = con.id;
    }
    if(opp!=null){ 
      input.subjectId = opp.id;
    } 
    input.feedElementType = ConnectApi.FeedElementType.FeedItem;
    return input;
  }
  
  //Method to restrict users to delete or edit records if they are not the owner
  //Prakarsh Jain 13 April 2016 - UAT Issues sheet shared by Devin
  public static void restrictEditDeleteOfRecords(List<Task> newTaskList, Map<Id, Task> oldMap, Boolean IsDelete){ 
    Set<Id> taskIdSet = new Set<Id>();
    Schema.RecordTypeInfo rtByNameInstrumentMarketing = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Marketing Task Record');
    Id recordTypeMarketingInstrument = rtByNameInstrumentMarketing.getRecordTypeId();
    Schema.RecordTypeInfo rtByNameInstrumentTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Instruments Task Record');
    Id recordTypeTaskInstrument = rtByNameInstrumentTask.getRecordTypeId();
    if(IsDelete){
      for(Task task : oldMap.values()){
        if(task.OwnerId != UserInfo.getUserId() && UserInfo.getProfileId() != Label.Administrator_Profile_ID && (task.RecordTypeId == recordTypeMarketingInstrument || task.RecordTypeId == recordTypeTaskInstrument)){
          task.addError(Label.Delete_Permission_Error);
        }
      }
    }
  /*  else{
      for(Task task : newTaskList){
       // if(task.OwnerId != UserInfo.getUserId() && UserInfo.getProfileId() != Label.Administrator_Profile_ID && (task.RecordTypeId == recordTypeMarketingInstrument || task.RecordTypeId == recordTypeTaskInstrument)){
        if(((oldMap.get(task.Id).ownerId == task.OwnerId && task.OwnerId != UserInfo.getUserId()) || (oldMap.get(task.Id).ownerId != task.OwnerId && oldMap.get(task.Id).ownerId != UserInfo.getUserId())) && String.valueOf(UserInfo.getProfileId()).subString(0,15) != Label.Administrator_Profile_ID.subString(0,15) && (task.RecordTypeId == recordTypeMarketingInstrument || task.RecordTypeId == recordTypeTaskInstrument)){
          task.addError(Label.Edit_Permission_Error);
        }
      }
    } */
  }
}