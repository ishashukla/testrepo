// (c) 2015 Appirio, Inc.
//
// Class Name: InstallBaseController_Test 
// Description: Test Class for InstallBaseController class.
// 
// April 21 2016, Isha Shukla  Original 
//
@isTest
public class InstallBaseController_Test {
    Static List<Account> acc;
    Static List<Install_Base__c> ib ;
    @isTest static void checkRecIdwithid() {
        createData();
        Test.startTest();
            PageReference pageRef1 = Page.InstallBase;
            Test.setCurrentPage(pageRef1);
            System.currentPageReference().getParameters().put('id', String.valueOf(acc[0].Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(acc[0]);
            InstallBaseController ibc = new InstallBaseController(sc);
            System.assertEquals(String.valueOf(acc[0].Id),(ibc.recId));
        Test.stopTest();
    }
    @isTest static void checkRecIdwithaccid() {
        createData();
        Test.startTest();
            PageReference pageRef2 = Page.InstallBase;
            Test.setCurrentPage(pageRef2);
            System.currentPageReference().getParameters().put('accid', String.valueOf(acc[0].Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(acc[0]);
            InstallBaseController ibc = new InstallBaseController(sc);
            System.assertEquals(String.valueOf(acc[0].Id),(ibc.recId));
        Test.stopTest();
    }
    
    public static void createData() {
        List<User> user = TestUtils.createUser(1, 'System Administrator', false );
        user[0].Division = 'NSE';
        insert user;
        system.runAs(user[0]){
            acc = TestUtils.createAccount(1, True);
            List<System__c> sys = TestUtils.createSystem(1,'System1' ,'NSE',True);
            ib = TestUtils.createInstallBase(1,acc[0].Id, user[0].Id, sys[0].Id,True);
        }
    }
}