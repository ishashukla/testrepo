// (c) 2016 Appirio, Inc.
//
// Description Controller for COMM_OpptyQuoteDocumentsListPage
//
// Created On 04/13/2016   Deepti Maheshwari Reference (T-492209) 
// Modified on 25th april 2016          Kirti Agarwal T-496412 
// updated query so that It should only show those oppty doc record of the same opportunity and/or quote
// Modified on 25th April 2016   Deepti Maheshwari (Ref : T-496412)       
//
public with sharing class COMM_DocumentVersionDetailPage {
  public Document__c quoteDoc{get;set;} 
  public List<Document__c> documentList{get;set;}
  public Id selectedID{get;set;}
  public List<Document__c> documentListToShow{get;set;}
  public Integer listSize {get;set;}
  public Integer pageSize{get;set;}
  public Integer pageNumber{get;set;}
  public COMM_DocumentVersionDetailPage(ApexPages.StandardController std){
    quoteDoc = (Document__c)std.getRecord();      
    //documentListToShow = new List<Document__c>();
    documentList = getDocumentList();
    pageSize = (Integer)COMM_Constant_Setting__c.getOrgDefaults().Quote_Document_Pagesize__c;
    pageNumber = 1;
    getDocumentListToShow();
    isAsc = true;
    previousSortField = sortField = 'Name';
  }
  
  public List<Document__c> getDocumentList(){
    documentList = new List<Document__c>();
    documentListToShow = new List<Document__c>();
   if(quoteDoc == null || quoteDoc.id == null){
      listSize = 0;
      //returning a blank list for escaping no rows assignment error
      return new List<Document__c>();
      
    }
    
    Document__c quoteDocTemp = [SELECT Oracle_Quote__c,Opportunity__c,Document_Type__c, ID
                                FROM Document__c WHERE id =: quoteDoc.id];
    //T-496412 - updated query so that It should only show those oppty doc record of the same opportunity and/or quote 
    String query = 'SELECT CreatedDate,ID,Name,Approval_Status__c,'
                    + ' Document_type__c, LastModifiedBy.Name, Opportunity__r.Name,'
                    + ' Oracle_Quote__r.Name,View__c, LastModifiedDate, Document_Link__c'
                    + ' FROM Document__c '
                    + ' WHERE ' 
                    + ' Document_Type__c =  \'' + quoteDocTemp.Document_Type__c+ '\''
                    + ' AND Id !=\'' + quoteDocTemp.id+ '\'';
                    
                    
    if(quoteDocTemp.Opportunity__c != null && quoteDocTemp.Oracle_Quote__c != null) {
      query += 'AND (Opportunity__c = \'' + quoteDocTemp.Opportunity__c + '\''
                + 'OR Oracle_Quote__c = \'' + quoteDocTemp.Oracle_Quote__c + '\')';
    }
    
    else if(quoteDocTemp.Opportunity__c != null && quoteDocTemp.Oracle_Quote__c == null) {
      query += 'AND Opportunity__c = \'' + quoteDocTemp.Opportunity__c + '\'';
    }
    
    else if(quoteDocTemp.Opportunity__c == null && quoteDocTemp.Oracle_Quote__c != null) {
      query += 'AND Oracle_Quote__c = \'' + quoteDocTemp.Oracle_Quote__c + '\'';
    }
    
    query += ' ORDER BY CreatedDate DESC';
    //end- T-496412
    List<Document__c> doclist = Database.query(query);
    if(doclist != null){
      listSize = docList.size();
    }
    return doclist;
  }
  
  //----------------------------------------------------------------------------
  // Name : doDelete() Method to delete selected document
  // Params : None
  // Returns : list of documents
  //----------------------------------------------------------------------------
  public void doDelete(){
    try{
      List<Document__c> docList = [SELECT Id 
                                  FROM Document__c 
                                  WHERE ID = :selectedID];
      if(docList != null && docList.size() > 0){
          delete docList;
      }
      documentList = getDocumentList();
      /*if( listSize <= 5) {
        reachedMax = true;
      }else{
        reachedMax = false;
      }*/
      pageNumber = 1;
      getDocumentListToShow();
    }Catch(DmlException ex){
      //ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Unable to Delete this record!'));
    }
  }
  
  //Pagination methods
  
  public Boolean hasPrevious{get{
      if(PageNumber == 1){
          return false;
      }else{
          return true;
      }
  }set;}
  
  public Boolean hasNext{get{
      if(PageNumber == 1){
          if((listSize / pageSize  > 1)
          || (((listSize / pageSize ) == 1)
          && (math.mod(listSize, pageSize) != 0))){
              system.debug('Return FROM Here');
              return true;
          }else{
              system.debug('Return FROM tHere');
              return false;
          }
      }else{
          if((pageNumber > (listSize / pageSize))
          || (pageNumber == (listSize / pageSize)
          && (math.mod(listSize, pageSize) == 0))){
              system.debug('Return FROM Hereee');
              return false;
          }else{
              system.debug('Return FROM Here1');
              return true;
          }
      }
  }set;}
  
  public void next() 
  {
      pageNumber++;
      getDocumentListToShow();
  }
  
  public void previous() 
  {
      pageNumber--;
      getDocumentListToShow();
  }
  
  public void first() 
  {
      pageNumber = 1;
      getDocumentListToShow();
  }
  
  
  public void Last() 
  {
    if(Math.mod(listSize,pageSize) == 0){
      pageNumber = listSize / pageSize;
    }else{
      if(listSize / pageSize >= 1){
        pageNumber = listSize / pageSize + 1;
      }else{
        pageNumber = 1;
      }
    }
    getDocumentListToShow();
  }
  
  public List<Document__c> getDocumentListToShow() {
    documentListToShow = new List<Document__c>();
    if(documentList != null && documentList.size() > 0){
      if(pageNumber == 1){
        if(documentList.size() < pagesize){
          for(Integer i=0; i<documentList.size();i++){
              documentListToShow.add(documentList.get(i));
          }
        }else{
          for(Integer i=0; i<pagesize;i++){
              documentListToShow.add(documentList.get(i));
          }
        }
        system.debug('in if');
      }else{
        if((pageNumber-1) == (documentList.size() / pageSize)){
          system.debug('I am in second If');
          for(Integer i= ((pageNumber * pageSize) - pageSize); i < documentList.size(); i++){
                  documentListToShow.add(documentList.get(i));
          }
        }else{
          for(Integer i= ((pageNumber * pageSize) - pageSize); i<(pageNumber * pageSize);i++){
              documentListToShow.add(documentList.get(i));
          }
        }
      }
    }
    return documentListToShow;
  }
  
  public String sortField{get;set;}
  public String order{get;set;}
  public Boolean isAsc{get;set;}
  String previousSortField{get;set;}
  public void sortList(){
    system.debug('Sort Field : ' + sortField + ':::order::' + order + 'previousSortField' + previousSortField);
    
    isAsc = previousSortField.equals(sortField)? !isAsc : true; 
    previousSortField = sortField;
    order = isAsc ? 'ASC' : 'DESC';
    List<Document__c> resultList = new List<Document__c>();
    //Create a map that can be used for sorting 
    Map<object, List<Document__c>> objectMap = new Map<object, List<Document__c>>();
    for(Document__c ob : documentListToShow){
      if(sortField == 'LastModifiedBy'){
        if(objectMap.get(ob.LastModifiedBy.Name) == null){
          objectMap.put(ob.LastModifiedBy.Name, new List<Sobject>()); 
        }
        objectMap.get(ob.LastModifiedBy.Name).add(ob);
      }else{
        if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
            objectMap.put(ob.get(sortField), new List<Sobject>()); 
        }
        objectMap.get(ob.get(sortField)).add(ob);
      }
    }       
    //Sort the keys
    List<object> keys = new List<object>(objectMap.keySet());
    keys.sort();
    for(object key : keys){ 
      resultList.addAll(objectMap.get(key)); 
    }
    //Apply the sorted values to the source list
    documentListToShow.clear();
    if(order.toLowerCase() == 'asc'){
      documentListToShow.addAll(resultList);
    }else if(order.toLowerCase() == 'desc'){
      for(integer i = resultList.size()-1; i >= 0; i--){
        documentListToShow.add(resultList[i]);  
      }
    }
  }
}