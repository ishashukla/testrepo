// (c) 2015 Appirio, Inc.
//
// Class Name: FlexContractTriggerHandlerTest
// Description: Test Class for FlexContractTriggerHandler class.
// 
// April 6 2016, Meena Shringi  Original 
//
@isTest
private class FlexContractTriggerHandlerTest
{
    @isTest
    static void itShould()
    {
        List<Account> acc = TestUtils.createAccount(2, true);
        // Modified and Added by Nishank for Story S-422734
                test.startTest();
        Id flexRTOnlyConventional = Schema.sObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional').getRecordTypeId();  
        Opportunity opp = new Opportunity();
        opp.StageName = 'Quoting';
        opp.AccountId = acc.get(0).Id;
        opp.CloseDate = Date.newinstance(2014,5,29);
        opp.Name = 'test value';
        opp.End_Of_Term__c = 'FMV';
        opp.RecordTypeId = flexRTOnlyConventional;
        insert opp;
    
        Flex_Credit__c fc = new Flex_Credit__c();
        fc.Credit_Amount__c = 10;
        fc.Credit_Approval_Duration__c = 1;
        fc.Credit_Status__c = 'New';
        fc.Opportunity__c = opp.Id;
        fc.total_Sales_Tax_Rate__c = 2.0000;
        fc.EOT_Sales_Tax_Treatment__c = 'Test';
        fc.EOT_Sales_Tax__c = 'Test2';
        insert fc;
    
        Flex_Contract__c flexContract = TestUtils.createFlexContracts(1,acc.get(0).Id,false).get(0); // Changed by Nishank for Story 
        flexContract.Opportunity__c = opp.Id;
        Insert flexContract;
                  
        opp.End_Of_Term__c = '$1 Out';
        update opp;
               
        Flex_Contract__c flexContract2 = TestUtils.createFlexContracts(1,acc.get(0).Id,false).get(0); // Changed by Nishank for Story 
        flexContract2.Opportunity__c = opp.Id;
        Insert flexContract2;
         //End of changes Modified and Added by Nishank for Story S-422734

        flexContract.Account__c = acc.get(1).Id;
        update flexContract;   
        flexContract.Account__c = acc.get(1).Id;
        update flexContract;  
        delete flexContract;
        Account accountObj = [select Id, Active_MRA__c from account LIMIT 1];
        
        System.assertEquals(false,accountObj.Active_MRA__c);
        
        test.stopTest();
    }
}