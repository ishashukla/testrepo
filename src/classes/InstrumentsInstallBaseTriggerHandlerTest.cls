// (c) 2015 Appirio, Inc.
//
// Class Name: InstrumentsInstallBaseTriggerHandlerTest 
// Description: Test Class for InstrumentsInstallBaseTriggerHandler class.
// 
// April 18 2016, Isha Shukla  Original 
//
// Modified Reason: Test Class Failure fix
// Modified By: Kanika Mathur
// Modified Date: 31 Aug, 2016
@isTest
private class InstrumentsInstallBaseTriggerHandlerTest {
    static List<Account> acc;
    static List<Install_Base__c> ib;
    static List<Install_Base__c> installbase;
    static List<User> user;
    @isTest static void testDelete() {
        createData();
        ib[0].Instrument_Status__c = 'Purchased';
        Test.startTest();
        System.runAs(user[0]) {
                try {
                    delete ib[0];
                } catch(Exception e) {
                    e.setMessage('Deletion failed');
                }
        }
        Test.stopTest();
        System.assertEquals(2, ib.size());
    }
    @isTest static void testRestrictEditDeleteOfRecords() {
        createData();
        Test.startTest();
        System.runAs(user[0]) {
                try {
                    delete ib[0];
                } catch(Exception e) {
                    e.setMessage('Deletion failed');
                }
        }
        Test.stopTest();
        System.assertEquals(2, ib.size());
    }
    @isTest static void testEdit() {
        createData();
        Test.startTest();
        System.runAs(user[0]) {
                try {
                    update ib[0];
                } catch(Exception e) {
                    e.setMessage('updation failed');
                }
        }
        Test.stopTest();
        
        System.assertEquals(2, ib.size());
    }
    @isTest static void testShare() {
        createData();
        Test.startTest();
       
                try {
                    insert installbase[0];
                } catch(Exception e) {
                    e.setMessage('insert failed');
                }
        
        Test.stopTest();
        
        System.assertEquals(2, installbase.size());
    }
     @isTest static void testInstallBaseShare() {
        createData(); 
        Test.startTest();
         System.runAs(user[0]) {
                try {
                    insert installbase[0];
                } catch(Exception e) {
                    e.setMessage('insert failed');
                }
         }
         Test.stopTest();
        System.assertEquals(2, installbase.size());
    }
     public static void createData() {
        Profile profile = TestUtils.fetchProfile('Standard User');
        user = TestUtils.createUser(2, profile.Name, false );
        user[0].Division = 'NSE';
        insert user;
        System.runAs(user[0]) {
            acc = TestUtils.createAccount(1, false);
            //Added by Kanika Mathur on 31 Aug, 2016 to add record type
            acc[0].RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get(Label.Instruments_Account_RT_Label).getRecordTypeId();
            insert acc;
            List<System__c> sys = TestUtils.createSystem(1,'System1' ,'NSE',True);
            ib = TestUtils.createInstallBase(2,acc[0].Id, user[0].Id, sys[0].Id,True);
            installbase = TestUtils.createInstallBase(2,acc[0].Id, user[0].Id, sys[0].Id,false);
            AccountTeamMember member = new AccountTeamMember(AccountId = acc[0].Id,UserId=user[1].Id);
            insert member; 
        }
    }
    
    /*public static List<Install_Base__c> createInstallBase(Integer ibCount,Id accId , Id userId, Id sysId,Boolean isInsert) {
        List<Install_Base__c> ibList = new List<Install_Base__c>();
       
        for(Integer i = 0; i < ibCount; i++) {
            Install_Base__c ibRecord = new Install_Base__c(Account__c = accId , System__c = sysId , ownerId = userId);
            ibList.add(ibRecord);
        }
        if(isInsert) {
            insert ibList;
        } 
        return ibList;
    }
    public static List<System__c> createSystem(Integer sysCount,String sysName ,String businessunit ,Boolean isInsert) {
        List<System__c> sysList = new List<System__c>();
       
        for(Integer i = 0; i < sysCount; i++) {
            System__c sysRecord = new System__c(Name = sysName , Business_Unit__c = businessunit);
            sysList.add(sysRecord);
        }
        if(isInsert) {
            insert sysList;
        } 
        return sysList;
    }*/
}