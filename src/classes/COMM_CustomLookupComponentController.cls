// 
// (c) 2016 Appirio, Inc.
//
// Name : COMM_CustomLookupComponentController 
// Used to deplay the custom UI for lookup and also lookup window 
//
// 15th March 2016     Kirti Agarwal      Original(T-483770)
//
// Modified Date         Modified By      Purpose
//23rg August 2016       Meghna Vijay     To display Address on Site Visit Address LookUp Page
//                                        during edit(I-227379)       
public with sharing class COMM_CustomLookupComponentController {
  
  public String fieldName {get;set;}   
  public String objName {get;set;}
  public String selectedId {get;set;}
  public SObject currentRecord{get;set;}
  //public String address{get;set;}
  //private String addressId;
  public string addr;
  //public String selectedRecordName {get;set;}
  
  public String selectedRecordName {
    get {
               if(selectedRecordName==null) {
                //if(address!=null) {
                  //selectedRecordName=[SELECT Address__r.Full_Address__c, Address__c 
                                       //FROM Site_Visit__c WHERE Address__c=:address].get(0).Address__r.Full_Address__c;
                  
                //}
               } 
               return selectedRecordName;
        }
    set {
        selectedRecordName = value;
        String breakLine = '<br>';
        if(selectedRecordName.indexOf(breakLine)!=-1) {
         selectedRecordName = selectedRecordName.replace('<br>','\n');
        }
        
    }
  }
  
  
  public String fieldPopulatingId {get;set;}
  public String associatedFieldToPopulate {get;set;}
  public String fieldSetNameAPI {get;set;}
  public String selectedRecordPhone {get;set;}
  public String opportunityId {get;set;}
  
  
  //constructor              
  public COMM_CustomLookupComponentController() {
    fieldName = '';
    objName = '';
    currentRecord = null;
          
  }

  //================================================================      
  // Name: getSelectedRecord
  // Description: Used to get record name from the selected record id
  // for sales manager profiles
  // Created Date: 15th March 2016 
  // Created By: Kirti Agarwal (Appirio)
  //==================================================================
  public PageReference getSelectedRecord() {
    if(selectedId != null && selectedId != '' && opportunityId != NULL) {
       //System.assert(false,this.fieldPopulatingId);
       string currentRecordId = this.selectedId;
         if(currentRecordId!=null) {
         List < Sobject > sobjectList = Database.query('SELECT Id, ' + this.fieldName + ' FROM ' 
           + this.objName + ' WHERE ID = :currentRecordId');
         //System.assert(false,sobjectList);
       
      if (!sobjectList.isEmpty()) {
          this.currentRecord = sobjectList[0];
          //selectedRecordName = (String) sobjectList[0].get('Full_Address__c');
          //System.assert(false,selectedRecordName);
        }   
      }
    }
    else if (selectedId != null && selectedId != '') {
      
      string currentRecordId = this.selectedId;
      if (currentRecordId != null) {
        List < Sobject > sobjectList = Database.query('SELECT Id, ' + this.fieldName + ' FROM ' 
           + this.objName + ' WHERE ID =:currentRecordId');

       if (!sobjectList.isEmpty()) {
          this.currentRecord = sobjectList[0];
          selectedRecordName = (String) sobjectList[0].get('Name');
          //System.assert(false,selectedRecordName);
         }
      }
    }
    return null;
  }
}