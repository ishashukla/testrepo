//
// (c) 2012 Appirio, Inc.
//
//  Test class of  FlexContractMatureBatch
//
// August 28, 2015 Sunil Original
//
@isTest
private class FlexContractMatureBatchTest {

    // Test method to verify that the process can be scheduled.
  static testMethod void testMethod1() {

    Account accnt = new Account();
    accnt.Name            = 'AccntName';
    accnt.Type            = 'Hospital';
    accnt.Legal_Name__c   = 'Test';
    accnt.Flex_Region__c  = 'test';
    insert accnt;
    TestUtils.createCommConstantSetting();
    Schema.RecordTypeInfo rtByNameFlex = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Flex Opportunity - Conventional');
    Id recordTypeFlex = rtByNameFlex.getRecordTypeId();
    Opportunity opp                 = new Opportunity();
    opp.StageName                   = 'Quoting';
    opp.AccountId                   = accnt.Id;
    opp.CloseDate                   = System.today();
    opp.Name                        = 'test value';
    opp.StageName                   = 'Closed Won';
    opp.Contract_Sign_Date__c       = System.today();
    opp.Opportunity_Number__c       = '101';
    opp.recordtypeid                = recordTypeFlex;
    insert opp;

    Flex_Contract__c contract = new Flex_Contract__c();
    contract.Account__c = accnt.Id;
    contract.Opportunity__c = opp.Id;
    contract.Name = opp.Name;
    contract.Scheduled_Maturity_Date__c = System.today().addDays(50);
    insert contract;
    
    //added for the story #S-404578 #START
    
    Contact con = new Contact();
    con.firstName = 'Mr';
    con.lastName =  'testCon';
    con.accountID = accnt.id;
    insert con;
    
    Flex_Contract_Line__c flexConLine     = new Flex_Contract_Line__c();
    flexConLine.Flex_Contract__c          = contract.id;
    flexConLine.Opportunity__c            = opp.id;
    flexConLine.Location_Account__c       = accnt.id;
    flexConLine.State_Tax_Amt__c           = 1.00;
    flexConLine.State_Tax_Pct__c           = 1.00;
    flexConLine.PSR_Contact__c           = con.id;
    flexConLine.Flex_Sales_Rep__c           = UserInfo.getUserId();
    insert flexConLine;
    //added for the story #S-404578 #END

    Test.startTest();

    Id batchprocessid = Database.executeBatch(new FlexContractMatureBatch(), 3);
    AsyncApexJob aaj;
    try{
      aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob
             WHERE ID =: batchprocessid];
    }
    catch(Exception e){
      // DO Nothing
    }
    Test.stopTest();


    // verify Asserts.
    Flex_Contract__c updatedContract = [SELECT Is_Batch_Processed__c FROM Flex_Contract__c WHERE Id = :contract.Id].get(0);
    System.assertEquals(updatedContract.Is_Batch_Processed__c, true);

  }

    // Test method to verify that the process can be scheduled.
  static testMethod void testApexScheduleSetupTemplate() {
    Test.startTest();
    try{
      //FlexContractMatureBatch.Start();
    }
    catch(Exception e){
      System.debug(e.getMessage());
    }
    Test.stopTest();
  }
}