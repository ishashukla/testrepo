/*******************************************************************************
Class           : BatchToUpdateBillToAccountOnOrder 
Created by      : Shubham Paboowal
Date            : Sept 15,2016

*******************************************************************************/
global class BatchToUpdateBillToAccountOnOrder implements Database.Batchable<sObject>{
    
    public String query = 'SELECT Id, Billing_Account_Name__c, Billing_Account_Number__c, Bill_To_Account__c, AccountNumber FROM Account WHERE Billing_Account_Name__c != NULL AND Billing_Account_Number__c != NULL AND Bill_To_Account__c = null';
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope){
    Map<String, String> accNoToNameMap = new Map<String, String>(); 
    //Commented by Shubham for Case 00180520
    //List<Account> accsToUpdate = new List<Account>();
    //Added by Shubham for Case 00180520
    Map<Id, Account> accsToUpdateMap = new Map<Id, Account>();
    Map<String,List<Id>> billAccAndListOfAccToUpdate = new Map<String,List<Id>>();
    for(Account a : scope){
      if(a.AccountNumber != a.Billing_Account_Number__c && a.Billing_Account_Name__c != NULL && a.Billing_Account_Number__c != NULL){
        String mapKey = a.Billing_Account_Number__c+'~'+a.Billing_Account_Name__c;
        accNoToNameMap.put(a.Billing_Account_Number__c, a.Billing_Account_Name__c);
        if(!billAccAndListOfAccToUpdate.containsKey(mapKey)) {
          billAccAndListOfAccToUpdate.put(mapKey,new List<Id>());
        }
        billAccAndListOfAccToUpdate.get(mapKey).add(a.Id);
      }
    }
    for(Account billToAccount : [SELECT Id,Name,AccountNumber FROM Account WHERE AccountNumber IN :accNoToNameMap.keySet() AND Name IN: accNoToNameMap.values()]){
      String nameNumberKey = billToAccount.AccountNumber+'~'+billToAccount.Name;
      if(billAccAndListOfAccToUpdate.containsKey(nameNumberKey)) {
        for(Id accToUpdateId : billAccAndListOfAccToUpdate.get(nameNumberKey)) {
          //Commented by Shubham for Case 00180520
          //accsToUpdate.add(new Account(Id=accToUpdateId,Bill_To_Account__c=billToAccount.Id));
          //Added by Shubham for Case 00180520
          accsToUpdateMap.put(accToUpdateId, new Account(Id=accToUpdateId,Bill_To_Account__c=billToAccount.Id));
        }
      }
    }
    //Updated by Shubham for Case 00180520 #Start
    if(accsToUpdateMap.size() > 0) {
      update accsToUpdateMap.values();
    }
    //Updated by Shubham for Case 00180520 #End
    }
    
    global void finish(Database.BatchableContext BC){             
    }
}