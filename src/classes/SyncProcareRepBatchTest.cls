/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class SyncProcareRepBatchTest {
/**
 *  Purpose         :       This class is used to cover functionality SyncProcareRepBatch.
 *
 *  Created By      :       Padmesh Soni (Appirio Offshore)
 *
 *  Created Date    :       10/06/2016
 *
 *  Current Version :       V_1.0
 *
 *  Revision Log    :       V_1.0 - Created - S-441563
 *
 *	Code Coverage	:		V_1.0 - 83%
 **/
 
    // Method to test inset of Custom Account Team
	static testMethod void testSyncProcareRepBatch(){
        
		User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
		User usr1 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
		User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
        
        System.runAs(usr) {
        	
            Test.startTest();
            
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            
            List<Custom_Account_Team__c> recList = new List<Custom_Account_Team__c>();
            recList.add(new Custom_Account_Team__c(Account__c = acc.Id, User__c = usr1.Id, Team_Member_Role__c = 'Navigation', 
														Account_Access_Level__c ='Read Only'));
            recList.add(new Custom_Account_Team__c(Account__c = acc.Id, User__c = usr2.Id, Team_Member_Role__c = 'Surgical', 
														Account_Access_Level__c ='Read/Write'));
            insert recList;
            
            SyncProcareRepBatch sync = new SyncProcareRepBatch();
            Database.executeBatch(sync);
            
            Test.stopTest();
        }
    }
	
	// Method to test delete of Custom Account Team
    static testMethod void testSyncProcareRepBatch1(){
        
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User usr1 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
		User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
		
		System.runAs(usr) {
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            
            Test.startTest();
            
            AccountTeamMember atm = new AccountTeamMember(AccountId = acc.Id, TeamMemberRole = 'Navigation', UserId = usr1.Id);
            insert atm;
            
            AccountShare accShare = new AccountShare(AccountId = acc.Id, UserOrGroupId = usr1.Id, RowCause = Schema.AccountShare.RowCause.Manual, 
            											OpportunityAccessLevel = 'None', ContactAccessLevel = 'None', CaseAccessLevel = 'None',
            											AccountAccessLevel = 'Edit');
			insert accShare;
            List<Custom_Account_Team__c> recList = new List<Custom_Account_Team__c>();
            Custom_Account_Team__c cat = new Custom_Account_Team__c(Account__c = acc.Id, User__c = usr1.Id, Team_Member_Role__c = 'Navigation', 
            															Account_Access_Level__c ='Private', IsDeleted__c = true);
            insert cat;
            
            SyncProcareRepBatch sync = new SyncProcareRepBatch();
            database.executeBatch(sync);
            
            Test.stopTest();
            
            List<AccountShare> shareList= [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:usr1.Id 
            									AND AccountId =:acc.Id AND RowCause =:Schema.AccountShare.RowCause.Manual];
                        						
			system.assertEquals(shareList.size(),1);
        }
    }
     
	// Method to test update of Access Level in Custom Account Team
	static testMethod void testSyncProcareRepBatch2(){
		
        User usr = TestUtils.createUser(1, 'System Administrator', true).get(0);
        User usr1 = TestUtils.createUser(1, 'Instruments Sales User', false).get(0);
        usr1.ProCare_Region_Rep__c = usr.Id;
        insert usr1;
        
		User usr2 = TestUtils.createUser(1, 'Instruments Sales User', true).get(0);
		
        System.runAs(usr) {
        	
            Account acc = TestUtils.createAccount(1, false).get(0);
            Schema.RecordTypeInfo rtByName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Instruments Account Record Type');
            acc.RecordTypeId = rtByName.getRecordTypeId();
            insert acc;
            
            Test.startTest();
            
            AccountTeamMember atm = new AccountTeamMember(AccountId = acc.Id, TeamMemberRole = Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP, 
            												UserId = usr1.Id);
            insert atm;
            
            AccountShare accShare = new AccountShare(AccountId = acc.Id, UserOrGroupId = usr1.Id, RowCause = Schema.AccountShare.RowCause.Manual, 
            											OpportunityAccessLevel = 'None', ContactAccessLevel = 'None', CaseAccessLevel = 'None', 
            											AccountAccessLevel = 'Edit');
			insert accShare;
			
            List<Custom_Account_Team__c> recList = new List<Custom_Account_Team__c>();
            Custom_Account_Team__c cat = new Custom_Account_Team__c(Account__c = acc.Id, User__c = usr1.Id, Team_Member_Role__c = 'Navigation', 
            															Account_Access_Level__c ='Private');
            insert cat;
            
            SyncProcareRepBatch sync = new SyncProcareRepBatch();
            Database.executeBatch(sync);
            
            Test.stopTest();
            
            
            List<AccountShare> shareList= [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel  FROM AccountShare WHERE UserOrGroupId =:usr1.Id
                        						AND AccountId =:acc.Id AND RowCause =:Schema.AccountShare.RowCause.Manual];
            System.assertEquals(shareList.size(),1);
        }
    }
}