/*******************************************************************************
* Author        :  Appirio JDC
* Date          :  27 June 2016
* Description   :  Pratibha Chimppa   Original (T-513058)
*                  SendEmailtoOrderCustomers 
* 19 July 2016    Parul Gupta          Modified  I-226406
* 03 Aug 2016     Nathalie Le Guay     Added failure notification email in finish()
* 05 Aug 2016     Parul Gupta          I-226406 Reviewed and refactored code
* 17 Sept 2016    Nathalie Le Guay     Remove references to Serial_Numbers__c, which is no longer planned
* 19 Sept 2016    Shreerath            T-538447 - Replaced Days_Delinquent__c with Delinquent_Days__c in query
 ==================================================================*/

global class Endo_SendEmailtoOrderCustomers implements Database.Batchable<sObject>, Database.Stateful{

  global final String eventLogQuery;
  global final Decimal sevenDays = 7;
  global final Decimal fourteenDays = 14;
  global final String awaitingStatus = Label.Endo_Order_Item_Awaiting_Return;
  global List<String> orderType;
  global Map<Id, List<OrderItem>> mapofOrderItem;
  global Map<Id, Order> mapofOrder;
  global List<Id> recordId ;

  /****************************************************************************
    // Constructor
  /***************************************************************************/
  global Endo_SendEmailtoOrderCustomers(){
     
      // Defining That Query should only be fore endo orders
      recordId = new List<Id>();
      EndoGeneralSettings__c mc = EndoGeneralSettings__c.getOrgDefaults();
      if (mc.OrderEndoCompleteOrders__c != null){
      	recordId.add(mc.OrderEndoCompleteOrders__c);
      }
      if (mc.Order_EndoOrdersRT__c != null){
      	recordId.add(mc.Order_EndoOrdersRT__c);  
      }      
      mapofOrderItem = new Map<Id, List<OrderItem>>();
      mapofOrder = new Map<Id, Order>();
      
       // Specific SJC order types
      orderType = new List<String> { 'SJC Credit Return', 'SJC Credit and Replacement', 'SJC CPT Return', 'SJC Sample Return'}; 
      
      // Query on Order to get all Order Products related to it
      eventLogQuery = 'Select Id, Type, Contact__c, RMA_Number__c, Account.AccountNumber, '+
                      '(Select Shipped_Date__c, PartNo__c, Description, Quantity_Shipped__c, OrderId, Order.PoNumber, Delinquent_Days__c,'+
                      ' Oracle_Ordered_Item__c, Status__c, Order.OrderNumber, Order.Type, Order.Account.AccountNumber, Order.Account.Name,' +
                      ' PriceBookEntryId, PriceBookEntry.Name, Quantity' + 
                      ' From OrderItems where Status__c =:AwaitingStatus AND (Delinquent_Days__c =:sevenDays  OR Delinquent_Days__c =:fourteenDays) )'+
                      ' From Order Where Order.Type IN :orderType AND Contact__c != null AND RecordTypeId IN :recordId';
  }

  /****************************************************************************
    // Start the batch
  /***************************************************************************/
  global Database.QueryLocator start(Database.BatchableContext BC){
    system.debug('Endo_SendEmailtoOrderCustomers - Query ::' + Database.getQueryLocator(eventLogQuery));
    return Database.getQueryLocator(eventLogQuery);
  }
  
  /****************************************************************************
    // Execute the batch
  /***************************************************************************/
  global void execute(Database.BatchableContext BC, List<Order> scope){
    
  	// Iterate through orders
   	for (Order ord: scope){
    	if (!ord.OrderItems.isEmpty()){
        if (mapofOrderItem.get(ord.Contact__c) == null ){
            mapofOrderItem.put(ord.Contact__c, new List<OrderItem>());
        }     
        mapofOrder.put(ord.Contact__c, ord);
				mapofOrderItem.get(ord.Contact__c).addAll(ord.OrderItems);            
    	}  
		}
		system.debug('Endo_SendEmailtoOrderCustomers - mapofOrderItem :: ' + mapofOrderItem);     
  }

  /****************************************************************************
    // Finish the batch
  /***************************************************************************/
  global void finish(Database.BatchableContext BC){    
    Endo_EmailUitlity emailUtility = new Endo_EmailUitlity(); 
    
    // Send email to contact and additional contact
    if (!mapofOrderItem.keySet().isEmpty()){
    	sendMailToContact(emailUtility);
    }   
     
    // NLG 3 Aug 2016     
    List<String> toAddresses = new List<String>();
    AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                      FROM AsyncApexJob WHERE Id =: BC.getJobId()];
    if(a.NumberOfErrors > 0) { 
        System.debug('\n[BatchAccountType: finish]: [The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.]]');
        
        for (Division_Admin__c divAdmin : Division_Admin__c.getAll().values()){
          if(divAdmin.Endo__c == true) {
            toAddresses.add(divAdmin.Email__c);
          }
        }
        String emailBody = 'The Batch Endo_SendEmailtoOrderContact job processed ' + a.JobItemsProcessed + ' batches out of ' + 
                            a.TotalJobItems + ' and generated ' + a.NumberOfErrors;
                                   
        String userId = UserInfo.getUserId();
        Messaging.SingleEmailMessage mail = emailUtility.createEmailMessage('SFDC Batch Error Alert: Endo_SendEmailtoOrderCustomers failed', 
        																		emailBody, null, toAddresses, userId, false);
       
        // Send email to admins for batch failure
        if (!Test.isRunningTest()) {        	
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            return;
        }       
    }
  }
  
  /****************************************************************************
    // Send email to contacts and additional contact of Order 
    // which includes all order item details
  /***************************************************************************/
	private void sendMailToContact(Endo_EmailUitlity emailUtility){
		Map<Id, Contact> emailAddresses = new Map<Id, Contact>();    	
		List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage mail;   
		String body; 
		String subject;
		List<String> emailAdd;
	
		// Fetch contacts with email address
    for (Contact con : [Select Email, id, Name 
    									from contact 
    									where id IN :mapofOrderItem.keySet()
    									and email != null]){
        emailAddresses.put(con.id, con);
    }
        
    // Get Email Logo From Document
    String imageURL = emailUtility.getEndoEmailLogo();
        
    for (Id contactid : mapofOrderItem.keySet()){
	       
	    // Add email addressess
	    emailAdd = new List<String>();
	    if (emailAddresses.containsKey(contactid)){
	        emailAdd.add(emailAddresses.get(contactid).Email);
	        system.debug('Endo_SendEmailtoOrderCustomers - To Email Addresses :: ' + emailAdd);
	    }
	    
	    // Create subject
	    subject = Label.Endo_Subject_for_return ; 
	    if (mapofOrder.get(contactid).RMA_Number__c != null){
	        subject += ' ' + mapofOrder.get(contactid).RMA_Number__c + ' and';
	    }
	    subject += ' ' + mapofOrder.get(contactid).Account.AccountNumber;
	       
    	body = emailUtility.getEmailBodyForConsumer(imageURL, emailAddresses.get(contactid).Name,  mapofOrderItem.get(contactid));           
        
      // Get Email Message and add in list 	    
	  	emailMessages.add(emailUtility.createEmailMessage(subject, null, body, emailAdd, contactid, true));
	  }
	  
	  // Send email messages to contact
	  if (!Test.isRunningTest()) {	 
	  	Messaging.sendEmail(emailMessages);
	  }
    
	}
}