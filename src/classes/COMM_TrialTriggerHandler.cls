/**================================================================      
* Appirio, Inc
* Name: COMM_TrialTriggerHandler 
* Description: Created this class to handle TrialTrigger
* Created Date: 
* Created By: 
*
* Date Modified      Modified By            Description of the update
* 05/31/2016        Rahul Aeran             T-506835, Set Opportunity to Null if Self relationship is populated on the inserted/updated object
* 06/03/2016        Meghna Vijay            T-504206,Populate Total of Trials On Oppty Object at after insert/delete
* 3rd Nov 2016      Nitish Bansal           I-241409 - Added Proper null & list checks before SOQL and DML; Moved RT query from all methods to single query
==================================================================*/

public class COMM_TrialTriggerHandler{

  static Id trialRtId = Schema.SObjectType.Trial__c.getRecordTypeInfosByName().get(Constants.TRIAL_RT_TRIAL).getRecordTypeId(); //NB - 11/03 - I-241409
  static Id rtRequest = Schema.SObjectType.Trial__c.getRecordTypeInfosByName().get(Constants.TRIAL_RT_REQUEST).getRecordTypeId(); //NB - 11/03 - I-241409

  public static void onBeforeInsert(List<Trial__c> lstNew){
    setOpportunityToBlank(lstNew,null);
  }
  public static void onAfterInsert(List<Trial__c> lstNew) {
    populateTotalTrialsAtInsert(lstNew);
  }
  public static void onBeforeUpdate(List<Trial__c> lstNew, map<Id,Trial__c> mapOld){
    setOpportunityToBlank(lstNew,mapOld);
  }
  public static void onAfterDelete(List<Trial__c> oldList) {
    populateTotalTrialsAtDelete(oldList);
  }
  
  public static void setOpportunityToBlank(List<Trial__c> lstNew, map<Id,Trial__c> mapOld){
    Boolean isUpdate = mapOld != null ? true : false;
    //Trial Object and its record types
    //String rtTrial = Constants.getRecordTypeId(Constants.TRIAL_RT_TRIAL,Constants.SOBJECT_TRIAL);   //NB - 11/03 - I-241409
    //String rtRequest = Constants.getRecordTypeId(Constants.TRIAL_RT_REQUEST,Constants.SOBJECT_TRIAL); //NB - 11/03 - I-241409
    // We don't need to check the record type of the trial__c field as there is already a filter added to it
    for(Trial__c trial : lstNew){
      if((!isUpdate || trial.Trial__c != mapOld.get(trial.Id).Trial__c 
          || trial.RecordTypeId != mapOld.get(trial.Id).RecordTypeId ) && trial.Trial__c != null && trial.RecordTypeId == rtRequest){
        //If the field is not null that indicates that it is of Trial record type, now we just need to check
        // for the Request record type of the inserted object
        trial.Opportunity__c = null;
      }
    }
  }
  //=================================================================================================
  //Name             :     populateTotalTrialsAtInsert Method
    //Description      :     Method created to populate total of trials sfter delete at oppty object
    //Created by       :     Meghna Vijay T-504206    
    //Created Date     :     June 03, 2016
  //=================================================================================================
  public static void populateTotalTrialsAtInsert(List<Trial__c> lstNew) {
    Set<String> opptyId = new Set<String> ();
    //Id trialRtId = Schema.SObjectType.Trial__c.getRecordTypeInfosByName().get(Constants.TRIAL_RT_TRIAL).getRecordTypeId();//NB - 11/03 - I-241409
    for(Trial__c tr :lstNew) {
      if(tr.RecordtypeID == trialRtId && tr.Opportunity__c != null) {//NB - 11/03 - I-241409
        opptyId.add(tr.Opportunity__c); 
      }
    }
    if(opptyId.size()>0) {
      recalculate(opptyId,lstNew);
    }
  }
  //=================================================================================================
  //Name             :     populateTotalTrialsAtDelete Method
    //Description      :     Method created to populate total of trials after delete at oppty object
    //Created by       :     Meghna Vijay T-504206    
    //Created Date     :     June 03, 2016
  //=================================================================================================
  public static void populateTotalTrialsAtDelete(List<Trial__c> oldList) {
    Set<String> opptyIds = new Set<String>();
    //Id trialRtId = Schema.SObjectType.Trial__c.getRecordTypeInfosByName().get(Constants.TRIAL_RT_TRIAL).getRecordTypeId();//NB - 11/03 - I-241409
    for(Trial__c trialObj : oldList) {
      if(trialObj.RecordtypeID == trialRtId && trialObj.Opportunity__c != null) {//NB - 11/03 - I-241409
          opptyIds.add(trialObj.Opportunity__c);
        }
      }
    if(opptyIds.size()>0) {
      recalculate(opptyIds,oldList);
    }

  }

  //=================================================================================================
  //Name             :     recalculate Method
    //Description      :     Method created to calculate total of trials after delete/insert at oppty object
    //Created by       :     Meghna Vijay T-504206    
    //Created Date     :     June 03, 2016
  //=================================================================================================
  public static void recalculate(Set<String> opptyId, List<Trial__c> trialList) {
    List<Opportunity> opptyList = new List<Opportunity> ();
    Map<String, Integer> totalTrialMap = new Map<String, Integer>();
    //Id trialRtId = Schema.SObjectType.Trial__c.getRecordTypeInfosByName().get(Constants.TRIAL_RT_TRIAL).getRecordTypeId();//NB - 11/03 - I-241409
    Integer trialSize=0;
    for(Opportunity opp : [SELECT ID, Total_of_Trials__c, (SELECT ID, RecordtypeID FROM Trials__r Where Opportunity__c IN : opptyId) 
                            FROM Opportunity WHERE ID IN : opptyId]) { //NB - 11/03 - I-241409
      if(opp.Trials__r.size()>0) {
        for(Trial__c tr : opp.Trials__r) {
          if(tr.RecordTypeId==trialRtId) {
            trialSize+=1;
            totalTrialMap.put(opp.Id,trialSize);
          }
          else {
            totalTrialMap.put(opp.Id,trialSize);
          }
          
        }
        opp.Total_of_Trials__c = totalTrialMap.get(opp.Id);
        opptyList.add(opp);
      }
      else {
        opp.Total_of_Trials__c =0;
        opptyList.add(opp);
      }
    }
    try {
      if(opptyList.size() > 0) //NB - 11/03 - I-241409
        update opptyList;
    }
    catch(Exception e) {
      for(Trial__c tr: trialList) {
        tr.addError(Label.COMM_Oppty_Upate_Records_Error_Message);
      }
    }
  }
}