// (c) 2015 Appirio, Inc.
//
// Class Name: ProductToPriceBookControllerTest
// Description: Test Class for ProductToPriceBookController class.
// 
// April 22 2016, Isha Shukla  Original 
//
@isTest
private class ProductToPriceBookControllerTest
{
    @isTest
    static void testProductPriceBookRelatedList()
    {	List<Product2> prodL = GlobalTestClassUtility.genProducts(1);
        PageReference pageRef = Page.PriceBookInlineView;
        Test.setCurrentPage(pageRef);
        User usr = TestUtils.createUser(1,'',false).get(0);
    	usr.Division = 'IVS';
        insert usr;
        User usr2 = TestUtils.createUser(1,'',false).get(0);
    	usr2.Division = 'IVS';
        insert usr2;
        System.runAs(usr2) {
        BusinessUnitAndPricebookName__c bup = new BusinessUnitAndPricebookName__c(Name='IVS',Price_Book_Name__c='IVS Price Book',System_Type__c='Stryker');
        insert bup;
        prodL = GlobalTestClassUtility.genProducts(1);
        insert prodL;
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prodL[0].Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        System.assertEquals(True,standardPrice != Null);
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='IVS Price Book', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prodL[0].Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        System.assertEquals(True,customPrice != Null);
        }
        System.runAs(usr) {
        	Test.startTest();
        	ProductToPriceBookController controller = new ProductToPriceBookController(new ApexPages.StandardController(prodL[0]));
        	Test.stopTest();
        }
    }
}