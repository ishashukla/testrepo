public class COMM_ProjectPlanQuickActionCntrlr{
    public Project_Plan__c projectPlan{get;set;}
    
    public COMM_ProjectPlanQuickActionCntrlr(ApexPages.StandardController controller) {
      projectPlan = [SELECT Id, Stage__c FROM Project_Plan__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }
    
    public PageReference cancelProject(){
      projectPlan.Stage__c  = 'Cancelled';
      update projectPlan;
      PageReference page = new PageReference('/' + projectPlan.id);
      page.setRedirect(true);
      return page;
    }
    
    public PageReference mergeProject(){
      PageReference page = new PageReference( '/apex/COMM_MergeRecords?sObject=Project_Plan__c&sObjectId=' + projectPlan.Id + '&sObjectField=Name');
      page.setRedirect(true);
      return page;
    }    
}