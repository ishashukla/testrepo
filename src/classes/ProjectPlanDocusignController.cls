/*
Name            ProjectPlanDocusignController 
Created Date    06/23/2016
Created By      Rahul Aeran
Purpose         T-509669, controller for the page for adding Quick Actions in LEX for Docusign
*/
public class ProjectPlanDocusignController{
  public String projectId;
  public ProjectPlanDocusignController(ApexPages.StandardController std){
    projectId = std.getRecord().id; 

  }
  public PageReference redirect(){
    String returnUrl = DocusignUtility.insertDocusignDocuments(projectId);
    return new PageReference(returnUrl);
  }
  
 /* public PageReference redirectToBeneficialForm(){
    String returnUrl = DocusignUtility.insertBeneficialProjectDocument(projectId);
    return new PageReference(returnUrl);
  }*/
  public PageReference redirectToStandardDocusignPage(){
    String returnUrl = DocusignUtility.getStandardDocusignRedirectUrl(projectId);
    return new PageReference(returnUrl);
  }
}