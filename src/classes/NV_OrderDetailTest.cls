@isTest
private class NV_OrderDetailTest {
  @testSetup static void setup() {
    Map<String, Id> accountRecordTypes1 = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Account');

    // Inserting Test Accounts.
    Account theTestAccount = null;
    List<Account> accountList  = new List<Account>();

    Account theParentAccount = NV_TestUtility.createAccount('Test Parent Account', accountRecordTypes1.get('NV_Customer'), true);

    theTestAccount = NV_TestUtility.createAccount('Test Account1', accountRecordTypes1.get('NV_Customer'), false);
    theTestAccount.AccountNumber = '9876543210';
    theTestAccount.ParentId = theParentAccount.Id;
    accountList.add(theTestAccount);
    
    theTestAccount = NV_TestUtility.createAccount('Test Account2', accountRecordTypes1.get('NV_Customer'), false);
    theTestAccount.AccountNumber = '9876543210';
    theTestAccount.ParentId = theParentAccount.Id;
    accountList.add(theTestAccount);

    insert accountList;
    Test.startTest();
    // Inserting Test Contacts.
    Contact theTestContact = null;
    Id theContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('NV Contact').getRecordTypeId();

    List<Contact> contactList  = new List<Contact>();

    theTestContact = NV_TestUtility.createContact('test1', 'TestContact1', accountList[0].Id, false);
    theTestContact.RecordTypeId = theContactRecordTypeId;
    contactList.add(theTestContact);

    theTestContact = NV_TestUtility.createContact('test2', 'TestContact2', accountList[1].Id, false);
    theTestContact.RecordTypeId = theContactRecordTypeId;
    contactList.add(theTestContact);

    insert contactList;

    Contact_Acct_Association__c associationRecord = new Contact_Acct_Association__c(
      Associated_Account__c = accountList[0].Id,
      Associated_Contact__c = contactList[1].Id
    );
    insert associationRecord;

    Contract testContract1 = NV_TestUtility.createContract(accountList[0].Id, true);
    testContract1.Status = 'Activated';
    update testContract1;
    Test.stopTest();
    Pricebook2 testPriceBook = NV_TestUtility.createPricebook('NVPriceBook1', true, true);

    Map<String, Id> orderRecordTypes1 = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order');

    Order testOrder1 = NV_TestUtility.createOrder(
      accountList[0].Id,
      testContract1.Id, 
      orderRecordTypes1.get('NV_RMA'),
      false
    );
    testOrder1.Customer_PO__c = 'PO00001';
    testOrder1.EffectiveDate = Date.today();
    testOrder1.Date_Ordered__c = Datetime.now();
    testOrder1.Status = 'Partial Ship';
    testOrder1.Pricebook2Id = testPricebook.Id;
    testOrder1.Invoice_Related_Sales_Order__c = '1234567890';
    testOrder1.On_Hold__c = true;

    Order testOrder2 = NV_TestUtility.createOrder(
      accountList[0].Id,
      testContract1.Id,
      orderRecordTypes1.get('NV_Credit'),
      false
    );
    testOrder2.Customer_PO__c = 'PO00002';
    testOrder2.EffectiveDate = Date.today();
    testOrder2.Date_Ordered__c = Datetime.now();
    testOrder2.Status = 'Backordered';
    testOrder2.Pricebook2Id = testPricebook.Id;
    testOrder2.Oracle_Order_Number__c = '1234567890';

    Order testOrder3 = NV_TestUtility.createOrder(
      accountList[0].Id,
      testContract1.Id,
      orderRecordTypes1.get('NV_Bill_Only'),
      false
    );
    testOrder3.Customer_PO__c = 'PO00003';
    testOrder3.EffectiveDate = Date.today();
    testOrder3.Date_Ordered__c = null;
    testOrder3.Status = 'Booked';
    testOrder3.Pricebook2Id = testPricebook.Id;

    insert new List<Order> {
      testOrder1,
      testOrder2,
      testOrder3
    };

    Map<String, Id> productRecordTypes = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Product2');

    List<Product2> productData = new List<Product2>();
    for(Integer i = 1;i <= 9;i++) {
      productData.add(NV_TestUtility.createProduct(
        'NVProduct' + i,
        '00000' + i, 
        productRecordTypes.get('Stryker_Neurovascular'),
        false
      ));
    }
    insert productData;
  }

  @isTest static void testNV_OrderDetail() {
    // Query Order Ids.
    List<Order> testOrders = [SELECT Id FROM Order WHERE Customer_PO__c LIKE '%PO%'];

    // Updating Order Data.
    Date currentDay = Date.today();
    Date invoiceDate = currentDay.addDays(5);
    for(Order order: testOrders) {
      order.Invoice_Due_Date__c = invoiceDate;
    }
    testOrders[0].Status = 'Shipped';
    testOrders[1].Status = 'Partial Ship';
    testOrders[2].Status = 'On Hold';
    testOrders[0].Tracking_Number__c = '1,2,3,4';
    update testOrders;

    // Create Order Items.
    Product2 testProduct1 = [SELECT Id FROM Product2 WHERE Name = 'NVProduct1' LIMIT 1];
    Pricebook2 testPricebook1 = [SELECT Id From Pricebook2 WHERE Name = 'NVPriceBook1' LIMIT 1];
    PricebookEntry pbEntryForP1 = NV_TestUtility.addPricebookEntries(
      testPricebook1.Id,
      testProduct1.Id,
      1000,
      true,
      true
    );
    List<OrderItem> testOrderItems = new List<OrderItem>();
    for(Integer i = 0;i < 3;i++) {
      for(Integer j = 0;j < 5;j++) {
        OrderItem testOrderItem = new OrderItem();
        testOrderItem.OrderId = testOrders[i].Id;

        if((j == 0) || (j == 1)) {
          testOrderItem.Tracking_Number__c = '100ABC';
        }
        else if((j == 2) || (j == 3)) {
          testOrderItem.Tracking_Number__c = '110ABC';
        }
        else {
          testOrderItem.Tracking_Number__c = null;
        }

        testOrderItem.Status__c = (i == 0) ? 'Shipped' :
                                  (i == 1) ? 'Delivered' :
                                  (i == 2) ? 'Awaiting Shipping' : '';
        testOrderItem.OrderId = testOrders[i].Id;
        testOrderItem.PricebookEntryId = pbEntryForP1.Id;
        testOrderItem.Quantity = 3;
        testOrderItem.UnitPrice = pbEntryForP1.UnitPrice;
        testOrderItem.Shipping_Method__c = 'FDX-Parcel-Next Day 8AM';
        testOrderItem.Line_Type__c = 'US NV Complaint Ship Only L';
        testOrderItems.add(testOrderItem);
      }
    }
    insert testOrderItems;

    // Inserting Test Order Invoices.
    List<Order_Invoice__c> theTestOrderInvoices = new List<Order_Invoice__c>();
    for(Integer i = 0;i < 5;i++) {
      theTestOrderInvoices.add(new Order_Invoice__c(
        Order__c = testOrders[0].Id,
        Order_Item__c = testOrderItems[i].Id,
        PO_Number__c = 'PO00003' + i,
        Sales_Order_Number__c = '1234567890' + i,
        Oracle_Invoice_Number__c = '1234567890' + i,
        Salesrep_Name__c = 'Testing',
        Sub_Total__c = testOrderItems[i].UnitPrice * testOrderItems[i].Quantity,
        Transaction_Status__c = 'Open'
      ));
    }
    insert theTestOrderInvoices;

    List<NV_Follow__c> followRecords = new List<NV_Follow__c>();
    for(Integer i = 0;i <= 1;i++) {
      followRecords.add(new NV_Follow__c(
        Record_Id__c = testOrders[i].Id,
        Following_User__c = UserInfo.getUserId()
      ));
    }
    insert followRecords;

    // Query First Order having OrderItems.
    NV_OrderDetail orderDetail1 = NV_OrderDetail.getOrderDetailByOrderId(testOrders[0].Id);
    NV_OrderDetail orderDetail2 = NV_OrderDetail.getOrderDetailByOrderId(testOrders[1].Id);
    NV_OrderDetail orderDetail3 = NV_OrderDetail.getOrderDetailByOrderId(testOrders[2].Id);
    NV_OrderDetail.changeFollowRecord(testOrders[0].Id,true);
    NV_OrderDetail.changeFollowRecord(testOrders[1].Id,false);

    List<Contact> theTestContacts = [SELECT Id, Name, AccountId, Title__c, Contact_Type__c, Email, Phone, MobilePhone, Is_Primary__c
                                     FROM Contact LIMIT 1];
    if(theTestContacts.size() > 0) {
      NV_ContactDetail theContactDetail = new NV_ContactDetail(theTestContacts[0]);
      NV_ContactDetail.setPrimary(theTestContacts[0].Id, true);
      NV_ContactDetail.removeContact(theTestContacts[0].Id);
    }
  }
  
  /*********************NB - 11/21 - I-244774 Start*********************************/ 
  /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - Start **/
  @isTest 
  private static void testNV_OrderDetailRMA() {
      
      // Query Order Ids.
      List<Order> testOrders = [SELECT Id FROM Order WHERE Customer_PO__c LIKE '%PO%'];
  
      // Updating Order Data.
      Date currentDay = Date.today();
      Date invoiceDate = currentDay.addDays(5);
      for(Order order: testOrders) {
        order.Invoice_Due_Date__c = invoiceDate;
      }
      testOrders[0].Status = 'Shipped';
      testOrders[0].Type = 'NV Recall';
      testOrders[1].RecordTypeId = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order').get('NV_RMA');
      testOrders[1].Type = 'NV Recall';
      testOrders[2].RecordTypeId = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds('Order').get('NV_RMA');
      testOrders[0].Tracking_Number__c = '1,2,3,4';
      update testOrders;
  
      // Create Order Items.
      Product2 testProduct1 = [SELECT Id FROM Product2 WHERE Name = 'NVProduct1' LIMIT 1];
      Pricebook2 testPricebook1 = [SELECT Id From Pricebook2 WHERE Name = 'NVPriceBook1' LIMIT 1];
      PricebookEntry pbEntryForP1 = NV_TestUtility.addPricebookEntries(
        testPricebook1.Id,
        testProduct1.Id,
        1000,
        true,
        true
      );
      List<OrderItem> testOrderItems = new List<OrderItem>();
      for(Integer i = 0;i < 3;i++) {
        for(Integer j = 0;j < 5;j++) {
          OrderItem testOrderItem = new OrderItem();
          testOrderItem.OrderId = testOrders[i].Id;
  
          if((j == 0) || (j == 1)) {
            testOrderItem.Tracking_Number__c = '100ABC';
          }
          else if((j == 2) || (j == 3)) {
            testOrderItem.Tracking_Number__c = '110ABC';
          }
          else {
            testOrderItem.Tracking_Number__c = null;
          }
  
          testOrderItem.Status__c = (i == 0) ? 'Booked' :
                                    (i == 1) ? 'Returned' :
                                    (i == 2) ? 'Awaiting Return' : '';
          testOrderItem.OrderId = testOrders[i].Id;
          testOrderItem.PricebookEntryId = pbEntryForP1.Id;
          testOrderItem.Quantity = 3;
          testOrderItem.On_Hold__c = (i == 0) ? true : false;
          testOrderItem.UnitPrice = pbEntryForP1.UnitPrice;
          testOrderItem.Shipping_Method__c = 'FDX-Parcel-Next Day 8AM';
          testOrderItem.Line_Type__c = 'US NV Complaint Ship Only L';
          testOrderItems.add(testOrderItem);
        }
      }
      insert testOrderItems;
  
      // Inserting Test Order Invoices.
      List<Order_Invoice__c> theTestOrderInvoices = new List<Order_Invoice__c>();
      for(Integer i = 0;i < 5;i++) {
        theTestOrderInvoices.add(new Order_Invoice__c(
          Order__c = testOrders[0].Id,
          Order_Item__c = testOrderItems[i].Id,
          PO_Number__c = 'PO00003' + i,
          Sales_Order_Number__c = '1234567890' + i,
          Oracle_Invoice_Number__c = '1234567890' + i,
          Salesrep_Name__c = 'Testing',
          Sub_Total__c = testOrderItems[i].UnitPrice * testOrderItems[i].Quantity,
          Transaction_Status__c = 'Open'
        ));
      }
      insert theTestOrderInvoices;
  
      List<NV_Follow__c> followRecords = new List<NV_Follow__c>();
      for(Integer i = 0;i <= 1;i++) {
        followRecords.add(new NV_Follow__c(
          Record_Id__c = testOrders[i].Id,
          Following_User__c = UserInfo.getUserId()
        ));
      }
      insert followRecords;
  
      // Query First Order having OrderItems.
      NV_OrderDetail orderDetail1 = NV_OrderDetail.getOrderDetailByOrderId(testOrders[0].Id);
      NV_OrderDetail orderDetail2 = NV_OrderDetail.getOrderDetailByOrderId(testOrders[1].Id);
      NV_OrderDetail orderDetail3 = NV_OrderDetail.getOrderDetailByOrderId(testOrders[2].Id);
      NV_OrderDetail.changeFollowRecord(testOrders[0].Id,true);
      NV_OrderDetail.changeFollowRecord(testOrders[1].Id,false);
  
      List<Contact> theTestContacts = [SELECT Id, Name, AccountId, Title__c, Contact_Type__c, Email, Phone, MobilePhone, Is_Primary__c
                                       FROM Contact LIMIT 1];
      if(theTestContacts.size() > 0) {
        NV_ContactDetail theContactDetail = new NV_ContactDetail(theTestContacts[0]);
        NV_ContactDetail.setPrimary(theTestContacts[0].Id, true);
        NV_ContactDetail.removeContact(theTestContacts[0].Id);
      }
  }
  /** Code modified - Padmesh Soni (10/07/2016 Appirio Offshore) - S-443533 - End **/
  /*********************NB - 11/21 - I-244774 End*********************************/
}