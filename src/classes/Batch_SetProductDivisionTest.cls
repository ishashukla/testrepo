// (c) 2016 Appirio, Inc. 
//
// Class Name: Batch_SetProductDivisionTest - Test class for Batch_SetProductDivision
//
// 8 Nov 2016, Isha Shukla (T-553847) 
//
@isTest
private class Batch_SetProductDivisionTest {
    static List<Product2> productRecordList;
    @isTest static void testBatch_SetProductDivision() {
        createData();
        Test.startTest();
        Database.executeBatch(new Batch_SetProductDivision());
        Test.stopTest();
        System.assertEquals( [SELECT COMM_Product__c FROM Product2 WHERE Id = :productRecordList[0].Id].COMM_Product__c, true);
    }
    private static void createData() {
        Division_Admin__c cs = new Division_Admin__c(COMM__c = true,Name='COMM',Email__c = 'test@est.com');
        insert cs;
        productRecordList = TestUtils.createProduct(4, false);
        productRecordList[0].CurrencyIsoCode = 'USD';
        productRecordList[1].CurrencyIsoCode = 'USD';
        productRecordList[2].CurrencyIsoCode = 'USD';
        productRecordList[3].CurrencyIsoCode = 'USD';
        productRecordList[0].QIP_ID__c = null;
        productRecordList[1].QIP_ID__c = null;
        productRecordList[2].QIP_ID__c = null;
        productRecordList[3].QIP_ID__c = null;
        insert productRecordList;
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pbCOMM = new Pricebook2(name = 'COMM Price Book',isActive = true);
        insert pbCOMM;
        Pricebook2 pbEndo = new Pricebook2(name = 'Endo List Price',isActive = true);
        insert pbEndo;
        Pricebook2 pbMed = new Pricebook2(name = 'Medical',isActive = true);
        insert pbMed;
        Pricebook2 pbSPS = new Pricebook2(name = 'SPS Products',isActive = true);
        insert pbSPS;   
        PricebookEntry standardPBE1 = new PricebookEntry(currencyISOCode = 'USD' ,Pricebook2Id = standardPBId, 
                                                         Product2Id = productRecordList[0].Id, 
                                                         UnitPrice = 1000, 
                                                         IsActive = true);
        insert standardPBE1;
        PricebookEntry standardPBE2 = new PricebookEntry(currencyISOCode = 'USD',Pricebook2Id = standardPBId, 
                                                         Product2Id = productRecordList[1].Id, 
                                                         UnitPrice = 1000, 
                                                         IsActive = true);
        insert standardPBE2;
        PricebookEntry standardPBE3 = new PricebookEntry(currencyISOCode = 'USD',Pricebook2Id = standardPBId, 
                                                         Product2Id = productRecordList[2].Id, 
                                                         UnitPrice = 1000, 
                                                         IsActive = true);
        insert standardPBE3;
        PricebookEntry standardPBE4 = new PricebookEntry(currencyISOCode = 'USD',Pricebook2Id = standardPBId, 
                                                         Product2Id = productRecordList[3].Id, 
                                                         UnitPrice = 1000, 
                                                         IsActive = true);
        insert standardPBE4; 
        PricebookEntry pbe1 = new PricebookEntry(currencyISOCode = 'USD',Pricebook2Id = pbCOMM.Id, Product2Id = productRecordList[0].Id, UnitPrice = 1000, IsActive = true);
        insert pbe1;
        PricebookEntry pbe2 = new PricebookEntry(currencyISOCode = 'USD',Pricebook2Id = pbEndo.Id, Product2Id = productRecordList[0].Id, UnitPrice = 1000, IsActive = true);
        insert pbe2;
        PricebookEntry pbe3 = new PricebookEntry(currencyISOCode = 'USD',Pricebook2Id = pbCOMM.Id, Product2Id = productRecordList[1].Id, UnitPrice = 1000, IsActive = true);
        insert pbe3;
        PricebookEntry pbe4 = new PricebookEntry(currencyISOCode = 'USD',Pricebook2Id = pbMed.Id, Product2Id = productRecordList[1].Id, UnitPrice = 1000, IsActive = true);
        insert pbe4;
        PricebookEntry pbe5 = new PricebookEntry(Pricebook2Id = pbCOMM.Id, Product2Id = productRecordList[2].Id, UnitPrice = 1000, IsActive = true);
        insert pbe5;
        PricebookEntry pbe6 = new PricebookEntry(Pricebook2Id = pbSPS.Id, Product2Id = productRecordList[2].Id, UnitPrice = 1000, IsActive = true);
        insert pbe6;
        PricebookEntry pbe7 = new PricebookEntry(Pricebook2Id = pbCOMM.Id, Product2Id = productRecordList[3].Id, UnitPrice = 1000, IsActive = true);
        insert pbe7;
    }
}