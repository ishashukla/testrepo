/*************************************************************************************************
Created By:    Prakarsh Jain
Date:          Sept 28, 2015
Description  : Contact Trigger Handler Class
**************************************************************************************************/
@isTest
private class ContactTriggerHandlerTest {
    
    static Account objAccount;
    static List<Contact> objContact;
    static Contact_Acct_Association__c objAssociationObject;
    static final String PROFILE_MEDICAL_SYS_ADMIN = 'Medical - System Admin';
    static final String RT_MEDICAL_EMPLOYEE_CONTACT = 'Medical Employee Contact';
    static Id recordTypeCC = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Medical Customer Contact').getRecordTypeId();

    static testMethod void testAssociationObjectCreation(){
        objAccount = Medical_TestUtils.createAccount(1, true).get(0);
        Test.startTest();
        objContact = Medical_TestUtils.createContact(1, objAccount.id,false);
        objContact[0].RecordTypeId = recordTypeCC;
        insert objContact;
        for(Contact_Acct_Association__c objAssociation :  [SELECT Id
                                                           FROM Contact_Acct_Association__c
                                                           WHERE Associated_Account__c = :objAccount.id AND Associated_Contact__c = : objContact[0].id]) {
        System.assert(objAssociation != null, 'Association object has been created');
                                                        
        } 
        Test.stopTest();
    }
    static testMethod void testpreventDeleteOfMedEmployee(){
        User objMedicalProfileUser = Medical_TestUtils.createUser(1,PROFILE_MEDICAL_SYS_ADMIN,true).get(0);
        System.runAs(objMedicalProfileUser){
            objAccount = Medical_TestUtils.createAccount(1, true).get(0);
            objContact = Medical_TestUtils.createContact(1, objAccount.id,false);
            Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(RT_MEDICAL_EMPLOYEE_CONTACT).getRecordTypeId();
            objContact[0].recordTypeId = recordTypeId;
            insert objContact;
            Test.startTest();
            try{
                delete objContact;
            }catch(Exception ex){
                System.assert(ex.getMessage().contains('You do not have sufficient privileges to delete the record') , 'Actual Error : ' + ex.getMessage());
            }
            Test.stopTest();
        }
    }
    
}