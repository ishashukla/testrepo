// 
// (c) 2016 Appirio, Inc.
//
// Test class of COMM_CustomLookupComponentController
//
// 22 Mar 2016     Kirti Agarwal   Original(T-483770)
//
@isTest
private class COMM_CustomLookupComponentControllerTest {

  @isTest
  static void COMM_CustomLookupComponentControllerTest() {
    List < Account > accountList = TestUtils.createAccount(1, true);
    COMM_CustomLookupComponentController objCustomLookupCtrl = new COMM_CustomLookupComponentController();
    objCustomLookupCtrl.objName = 'Account';
    objCustomLookupCtrl.fieldName = 'Name';
    objCustomLookupCtrl.selectedId = accountList[0].id;
    objCustomLookupCtrl.getSelectedRecord();
    System.assertEquals(objCustomLookupCtrl.selectedRecordName, accountList[0].Name );
  }
}