/**================================================================      
* Appirio, Inc
* Name: CaseTriggerHandler
* Description: 
* Created Date: 28 Feb 2016
* Created By: Nitish Bansal (Appirio) - T-459650
* 
* Date Modified    Modified By             Description of the update
* 10 Aug 2016      Rahul Khanchandani      T-489156
* 09 Aug 2016      Jagdeep Juneja          I-227842
* 10 Aug 2016      Nick Whitney            
==================================================================*/

public virtual class CaseTriggerHandler implements CaseTriggerHandlerInterface{ 
    private List<Case> newCommCase {get; set;}
    private List<Case> newEndoCase {get; set;}
    private List<Case> newMedCase {get; set;}
    private List<Case> newInstCase {get; set;}
    
    /**================================================================      
    * Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/
    public static boolean IsActive(){
        return TriggerState.isActive(Constants.CASE_TRIGGER);
    }
    
    public void OnBeforeInsert(List<Case> newCases){}
    public void OnAfterInsert(List<Case> newCases){}
    public void OnAfterInsertAsync(Set<ID> newCaseIDs){}
    
    public void OnBeforeUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){}
    public void OnAfterUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){}
    public void OnAfterUpdateAsync(Set<ID> updatedCaseIDs){}
    
    public void OnBeforeDelete(List<Case> CasesToDelete, Map<ID, Case> caseMap){}
    public void OnAfterDelete(List<Case> deletedCases, Map<ID, Case> caseMap){}
    public void OnAfterDeleteAsync(Set<ID> deletedCaseIDs){}
    
    public void OnUndelete(List<Case> restoredCases){}
    /**================================================================      
    * End of Standard Methods from the CaseTriggerHandlerInterface
    ==================================================================*/
    
    /**================================================================      
    * Trigger Call Methods from the CaseTrigger
    ==================================================================*/
    public void beforeInsert(List<Case> newCases){
        // Org Wide functionality
        OnBeforeInsert(newCases);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newCases);
        
        // Division specific functionality
        if(newEndoCase.size() > 0) Endo_CaseTriggerHandler.OnBeforeInsert(newEndoCase);
        if(newCommCase.size() > 0) COMM_CaseTriggerHandler.OnBeforeInsert(newCommCase);
        if(newMedCase.size() > 0) Medical_CaseTriggerHandler.OnBeforeInsert(newMedCase);
        if(newInstCase.size() > 0) Intr_CaseTriggerHandler.OnBeforeInsert(newInstCase);
    }
    
    public void afterInsert(List<Case> newCases){
        // Org Wide functionality
        OnAfterInsert(newCases);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newCases);
        
        // Division specific functionality 
        if(newEndoCase.size() > 0) Endo_CaseTriggerHandler.OnAfterInsert(newEndoCase);
        if(newCommCase.size() > 0) COMM_CaseTriggerHandler.OnAfterInsert(newCommCase);
        if(newMedCase.size() > 0) Medical_CaseTriggerHandler.OnAfterInsert(newMedCase);
        if(newInstCase.size() > 0) Intr_CaseTriggerHandler.OnAfterInsert(newInstCase);
    }
    
    public void beforeUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){
        // Org Wide functionality
        OnBeforeUpdate(newCases, oldCaseMap);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newCases);
        
        // Division specific functionality
        if(newEndoCase.size() > 0) Endo_CaseTriggerHandler.OnBeforeUpdate(newEndoCase, oldCaseMap);
        if(newCommCase.size() > 0) COMM_CaseTriggerHandler.OnBeforeUpdate(newCommCase, oldCaseMap);
        if(newMedCase.size() > 0) Medical_CaseTriggerHandler.OnBeforeUpdate(newMedCase, oldCaseMap);
        if(newInstCase.size() > 0) Intr_CaseTriggerHandler.OnBeforeUpdate(newInstCase, oldCaseMap);
    }
    
    public void afterUpdate(List<Case> newCases, Map<ID, Case> oldCaseMap){
        // Org Wide functionality
        OnAfterUpdate(newCases, oldCaseMap);
        
        // Set Filtered Datasets for each Division 
        setFilteredDataSets(newCases);
        
        // Division specific functionality
        if(newEndoCase.size() > 0) Endo_CaseTriggerHandler.OnAfterUpdate(newEndoCase, oldCaseMap);
        if(newCommCase.size() > 0) COMM_CaseTriggerHandler.OnAfterUpdate(newCommCase, oldCaseMap);
        if(newMedCase.size() > 0) Medical_CaseTriggerHandler.OnAfterUpdate(newMedCase, oldCaseMap);
        if(newInstCase.size() > 0) Intr_CaseTriggerHandler.OnAfterUpdate(newInstCase, oldCaseMap);
    }
    /**================================================================      
    * End of Trigger Call Methods from the CaseTrigger
    ==================================================================*/
    
    private void setFilteredDataSets(List<Case> newCases){
        newCommCase = new List<Case>();
        newEndoCase = new List<Case>();
        newMedCase = new List<Case>();
        newInstCase = new List<Case>();
        
        for(Case c : newCases){
          //System.debug('Record Type ===========     '+   RTMap__c.getInstance(c.RecordTypeId).Division__c.containsIgnoreCase('COMM'));
            if(COMM_CaseTriggerHandler.IsActive() && 
                    RTMap__c.getInstance(c.RecordTypeId).Division__c.containsIgnoreCase('COMM')){
                      System.debug('Record Type ===========     '+ c.RecordTypeId);
                newCommCase.add(c);
            }
            if(Endo_CaseTriggerHandler.IsActive() && 
                    RTMap__c.getInstance(c.RecordTypeId).Division__c.containsIgnoreCase('ENDO')){
                newEndoCase.add(c);
            }
            if(Medical_CaseTriggerHandler.IsActive() && 
                    RTMap__c.getInstance(c.RecordTypeId).Division__c.containsIgnoreCase('Medical')){
                newMedCase.add(c);
            }
            if(Intr_CaseTriggerHandler.IsActive() && 
                    RTMap__c.getInstance(c.RecordTypeId).Division__c.containsIgnoreCase('Instruments')){
                newInstCase.add(c);
            }
        }
    }
}