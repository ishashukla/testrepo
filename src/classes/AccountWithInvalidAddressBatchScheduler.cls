/*******************************************************************************
Name        : AccountWithInvalidAddressBatchScheduler
Updated By  : Rahul Aeran (Appirio)
Date        : 04/15/2016
Purpose     : Scheduler class which will run weekly to send a report of invalid primary address
              to the system admin users
*******************************************************************************/
global class AccountWithInvalidAddressBatchScheduler implements Database.Batchable<Sobject>,Database.Stateful,Schedulable{ 
  
  String strURL = URL.getSalesforceBaseUrl().toExternalForm(); 
  //Constructor
  public AccountWithInvalidAddressBatchScheduler(){

  }
  /**************************************************************
   SCHEDULABLE METHODS
  *************************************************************/
  global void execute(SchedulableContext sc){
    if(!Test.isRunningTest()){
          id batchId = Database.executeBatch(new AccountWithInvalidAddressBatchScheduler(), 200);        
          System.debug('\n\n==> batchId = ' + batchId);
        } 
  }
  
  /**************************************************************
   BATCHABLE METHODS
  *************************************************************/
  global Database.Querylocator start(Database.BatchableContext context){
    
    //putting limit 1 as we just need to send one email if the failures exist
    Database.Querylocator query = Database.getQueryLocator([SELECT Id
                                  FROM Account 
                                  WHERE Has_Wrong_Primary_Checked__c = true
                                  LIMIT 1]);
    return query;
    
  }
  
  
  global void execute(Database.BatchableContext context, List<Sobject> objects){
    List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage>(); 
    if(!objects.isEmpty()){ 
        List<Report> reports = [SELECT Id FROM Report 
                               WHERE DeveloperName = : Constants.REPORT_ACCOUNTS_WITH_INVALID_ADDRESS] ;  
        if(!reports.isEmpty()){
            List<User> lstUsers = [SELECT Id,Email FROM User 
                                   WHERE Profile.Name=:Constants.ADMIN_PROFILE
                                   AND UserRole.Name =:Constants.ROLE_STRYKER_COMM];
                                     
            if(!lstUsers.isEmpty()){
               String reportUrl = strURL + '/' + reports.get(0).Id;
               for (User u : lstUsers) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                string body = '<html>'+Label.COMM_Email_Body_Account_With_Invalid_Primary_Address+'<br><a href="'+reportUrl+'">'+Label.COMM_Email_Subject_Account_With_Invalid_Primary_Address+'</a></html>';
                mail.setSubject(Label.COMM_Email_Subject_Account_With_Invalid_Primary_Address);
                mail.setTargetObjectId(u.Id); 
                mail.setSaveAsActivity(false);
                mail.setHtmlBody(body); 
                lstEmails.add(mail);
        
              }
            }
        }

    }
    if(!lstEmails.isEmpty()){
        Messaging.sendEmail(lstEmails);
    }
        
    
    
  }
  
  global void finish(Database.BatchableContext context){
    //Send email in finish method
    
  }
}