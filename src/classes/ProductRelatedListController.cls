public class ProductRelatedListController {

        public String parentField {get;set;}
        public String parentId{get;set;}
        public String OrderBy {get;set;}
        public ApexPages.StandardSetController stdSetContrl {get; set;}
        public String objectName {get;set;}
        public String fieldSetName {get;set;}
        public String startDateField {get;set;}
        public String endDateField {get;set;}
        public Map<String, String> referenceNameFields {get;set;}
        private static Integer PAGE_SIZE = 10;




        public Boolean  isRecordFound{
            get{
    			if(stdSetContrl == null){
    				initializeStandardSetController();
    			}
    			return stdSetContrl.getResultSize() > 0;
            } private set;
		}


		public List<Sobject> getRecords(){
			if(stdSetContrl == null){
				initializeStandardSetController();
			}
			return stdSetContrl.getRecords();
		}

		private void initializeStandardSetController(){
			String soqlQuery = 'Select Id,';
			referenceNameFields = new Map<String, String>();

      if(fieldSetName != null && fieldSetName != ''){
      	Schema.FieldSet  fieldSet = Schema.getGlobalDescribe().get(objectName).getDescribe().fieldSets.getMap().get(fieldSetName);

				if(fieldSet != null){
					for(Schema.FieldSetMember fsm : fieldSet.getFields()){
					  if(fsm.getType() == SCHEMA.DisplayType.REFERENCE) {
					  	String idField = fsm.getFieldPath();
					    String nameField = (idField.endsWith('__c') ? idField.replace('__c' , '__r')  : idField.replace('Id' , '')) + '.Name ';
					    soqlQuery += idField + ', ' + nameField +' , ';
					    //referenceNameFields.put(idField, nameField);
					  }
					  else{
					  	soqlQuery += fsm.getFieldPath() + ', '; 
					  }
					}

					referenceNameFields.put('Alternate_Product__c', 'Alternate_Product__r.Name');
					referenceNameFields.put('Part__c', 'Part__r.Name');
					referenceNameFields.put('Product__c', 'Product__r.Name');

          if(!soqlQuery.contains('Alternate_Product__r.Name')){
            soqlQuery += 'Alternate_Product__r.Name, ';
          }
          if(!soqlQuery.contains('Part__r.Name')){
            soqlQuery += 'Part__r.Name, ';
          }
          if(!soqlQuery.contains('Product__r.Name')){
            soqlQuery += 'Product__r.Name, ';
          }


					if(!soqlQuery.contains('Name')){
						soqlQuery += 'Name, ';
					}
					if(!soqlQuery.contains(parentField)){
						soqlQuery += parentField;
					}
					if(soqlQuery.trim().endsWith(',')){
						soqlQuery = soqlQuery.subString(0, soqlQuery.trim().lastIndexOf(','));
					}
				}else{
					soqlQuery += 'Name ';
				}
			}else{
				soqlQuery += 'Name ';
			}
			soqlQuery += ' From ' + objectName +  ' WHERE ';

			System.debug('@@@::parentField' + parentField);

			if(parentField == 'Alternate_Product__c'){
			  soqlQuery += 'Product__c = : parentId   AND Alternate_Product__c != null AND ' + startDateField + ' <= TODAY AND ' + endDateField + ' >=  TODAY ' + ( OrderBy != null && OrderBy != '' ? 'order by ' + OrderBy : '');
			}
			else if(parentField == 'Part__c'){
			  soqlQuery += parentField + ' = : parentId   AND Product__c != null AND ' + startDateField + ' <= TODAY AND ' + endDateField + ' >=  TODAY ' + ( OrderBy != null && OrderBy != '' ? 'order by ' + OrderBy : '');
			}
			else{
        soqlQuery += parentField + ' = : parentId   AND Part__c != null AND ' + startDateField + ' <= TODAY AND ' + endDateField + ' >=  TODAY ' + ( OrderBy != null && OrderBy != '' ? 'order by ' + OrderBy : '');
      }
			System.debug('@@@::soqlQuery' + soqlQuery);

			List<sObject> lst = Database.query(soqlQuery);
			System.debug('@@@###' + lst);
			stdSetContrl = new ApexPages.StandardSetController(lst);
			System.debug('@@@###');
			// sets the number of records in each page set
			stdSetContrl.setPageSize(PAGE_SIZE);
        }
}