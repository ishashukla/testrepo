/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = true)
private class SPS_OpportunityTriggerHandlerTest {

    static testMethod void myUnitTest() {
    	Test.startTest();
    	
    	Account acc = new Account(Name = 'Parent Test Account',
                                BillingStreet = 'Test Street', ShippingStreet = 'Test Street',
                                BillingCity = 'Test City', ShippingCity = 'Test City',
                                BillingState = 'California', ShippingState = 'California',
                                BillingCountry = 'United States', ShippingCountry = 'United States');
        acc.Type = 'Hospital';
        insert acc;
        Id recTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('SPS Products Record type').getRecordTypeId();
	    Product2 prod = new Product2();
	    prod.Name = 'test product';
	    prod.RecordTypeId = recTypeId;
	    insert prod;
	
	    Pricebook2 priceBook = [SELECT Name FROM Pricebook2 WHERE isStandard = true].get(0);
	    
	    PricebookEntry pbe = new PricebookEntry();
	    pbe.Pricebook2Id = priceBook.Id;
	    pbe.Product2Id = prod.Id;
	    pbe.IsActive = true;
	    pbe.UnitPrice = 10000.0;
	    insert pbe;

	    Opportunity opp = new Opportunity();
	    opp.StageName = 'Initial Meeting';
	    opp.AccountId = acc.Id;
	    opp.CloseDate = Date.newinstance(2014,12,30);
	    opp.Name = 'test value';
	    insert opp;
	    
	    OpportunityLineItem oppItem = new OpportunityLineItem();
	    oppItem.Quantity = 1.00;
	    oppItem.OpportunityId = opp.Id;
	    oppItem.TotalPrice = 10000;
	    oppItem.PricebookEntryId = pbe.Id;
	    insert oppItem;
	    
	    opp.StageName = 'Closed Won';
	    opp.Contract_Sign_Date__c = Date.newinstance(2014,7,29);
    	update opp;
    	
	    opp.Type = 'New Client';
    	update opp;
    	
    	SPS_Program__c program = new SPS_Program__c();
    	program.Account__c = acc.Id;
    	program.Opportunity__c=opp.Id;
    	insert program;
    	Test.stopTest();
    	
	    // Verifing Results.
	    List<SPS_Program__c> TestPrograms = [SELECT Id, Opportunity__c, Name FROM SPS_Program__c
	                                            WHERE Opportunity__c = :opp.Id ];
	
	    System.assertEquals(TestPrograms.size(), 1);
    
    }
}