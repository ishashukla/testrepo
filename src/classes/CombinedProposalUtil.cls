/*
Name 					- CombinedProposalUtil
Created By 				- Nitish Bansal
Created Date 			- 02/25/2016
Purpose 				- T-459652 ; Utility for Combined Proposal document type
*/
public class CombinedProposalUtil {
	    
    // Method to check if a user is allowed to delete a combined proposal.
	// @param docId - String to hold document Id 
	// @return a boolean value 
    public static boolean isAllowedDeleteCP(){
        final String userRoleName = [select UserRole.name from User Where Id=:Userinfo.getUserId()].UserRole.Name;
        if(null != userRoleName && userRoleName.length() > 0){
            for(RolesCustomSetting__c role: [Select Name from RolesCustomSetting__c]){
                if(role.Name == userRoleName){
                    return true;
                }
            }
        }
        return false;
    }
}