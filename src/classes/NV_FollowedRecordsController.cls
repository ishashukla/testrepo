//
// (c) 2015 Appirio, Inc.
//
// Apex Class Name: NV_FollowedRecordsController
// Description: This apex class is used to extract all accounts and orders followed by the logged-in user.
//              Also, provides functionality to unfollow all or selected records.
//
// 18th January 2015    Hemendra Singh Bhati    Original (Task # T-467212)
//
public with sharing class NV_FollowedRecordsController {
  // Private Data Members.
  // Public Data Members.

  // Inner Class - FollowedAccounts
  public class FollowedAccounts {
    // Public Data Members.
    @AuraEnabled
    public NV_Follow__c theNVFollowRecord;

    @AuraEnabled
    public Account theAssociatedAccountRecord;
    
   

    // The Constructor.
    public FollowedAccounts(NV_Follow__c nvFollowRecord, Account theAccountRecord) {
      theNVFollowRecord = nvFollowRecord;
      theAssociatedAccountRecord = theAccountRecord;
    }
  }

  // Inner Class - FollowedOrders
  public class FollowedOrders implements Comparable{
    // Public Data Members.
    @AuraEnabled
    public NV_Follow__c theNVFollowRecord;

    @AuraEnabled
    public Order theAssociatedOrderRecord;

    // The Constructor.
    public FollowedOrders(NV_Follow__c nvFollowRecord, Order theOrderRecord) {
      theNVFollowRecord = nvFollowRecord;
      theAssociatedOrderRecord = theOrderRecord;
    }
      
    // Compare FollowedOrders based on the followRecords date.
    public Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        FollowedOrders compareToFollow = (FollowedOrders)compareTo;
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (theNVFollowRecord.CreatedDate < compareToFollow.theNVFollowRecord.CreatedDate) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (theNVFollowRecord.CreatedDate > compareToFollow.theNVFollowRecord.CreatedDate) {
            // Set return value to a negative value.
            returnValue = -1;
        }

        return returnValue;      
    }

  }

  // The Constructor.
  public NV_FollowedRecordsController() {
  }

  /*
  @method      : getFollowedAccounts
  @description : This method is used to extract all accounts followed by the logged-in user. Return ordered alphabetically.
  @params      : void
  @returns     : List<FollowedAccounts> theFollowedAccounts
  */
  @AuraEnabled
  public static List<FollowedAccounts> getFollowedAccounts() {
    try {
      // Extracting Followed Account Ids.
      Id theRecordId = null;
      String theRecordType = null;
      Map<Id, NV_Follow__c> theAccountIdAndNVFollowMapping = new Map<Id, NV_Follow__c>();
      for(NV_Follow__c theRecord : [SELECT Id, Record_Id__c, Following_User__c FROM NV_Follow__c WHERE Record_Id__c != null AND
                                    Following_User__c = :UserInfo.getUserId() AND Record_Type__c = 'Account']) {
        theRecordId = Id.valueOf(theRecord.Record_Id__c);
        theRecordType = theRecordId.getSObjectType().getDescribe().getName();
        if(theRecordType.equalsIgnoreCase('Account')) {
          theAccountIdAndNVFollowMapping.put(
            theRecordId,
            theRecord
          );
        }
      }

      // Extracting Followed Accounts.
      List<FollowedAccounts> theFollowedAccounts = null;
      if(theAccountIdAndNVFollowMapping.size() > 0) {
        theFollowedAccounts = new List<FollowedAccounts>();
        for(Account theRecord : [SELECT Id, Name, AccountNumber, Type, ShippingStreet, ShippingCity, ShippingState, ShippingCountry,
                                 ShippingPostalCode FROM Account WHERE Id IN :theAccountIdAndNVFollowMapping.keySet() ORDER BY Name]) {
          theFollowedAccounts.add(new FollowedAccounts(
            theAccountIdAndNVFollowMapping.get(theRecord.Id),
            theRecord
          ));
        }
      }

      return theFollowedAccounts;
    }
    catch(Exception e) {
      system.debug('TRACE: NV_FollowedRecordsController - getFollowedAccounts - Exception Occurred - ' + e.getMessage());
      system.debug('TRACE: NV_FollowedRecordsController - getFollowedAccounts - Exception Occurred - ' + e.getStackTraceString());
    }
    return null;
  }

  /*
  @method      : getFollowedOrders
  @description : This method is used to extract all orders followed by the logged-in user.
  @params      : void
  @returns     : List<FollowedOrders> theFollowedOrders
  */
  @AuraEnabled
  public static List<FollowedOrders> getFollowedOrders() {
    try {
      // Extracting Followed Order Ids.
      Id theRecordId = null;
      String theRecordType = null;
      Map<Id, NV_Follow__c> theOrderIdAndNVFollowMapping = new Map<Id, NV_Follow__c>();
      for(NV_Follow__c theRecord : [SELECT Id, Record_Id__c, Following_User__c, CreatedDate FROM NV_Follow__c WHERE Record_Id__c != null AND
                                    Following_User__c = :UserInfo.getUserId() AND Record_Type__c = 'Order']) {
        theRecordId = Id.valueOf(theRecord.Record_Id__c);
        theRecordType = theRecordId.getSObjectType().getDescribe().getName();
        if(theRecordType.equalsIgnoreCase('Order')) {
          theOrderIdAndNVFollowMapping.put(
            theRecordId,
            theRecord
          );
        }
      }

      // Extracting Followed Orders.
      List<FollowedOrders> theFollowedOrders = null;
      if(theOrderIdAndNVFollowMapping.size() > 0) {
        theFollowedOrders = new List<FollowedOrders>();
        for(Order theRecord : [SELECT Id, Account.Name, Account.AccountNumber, Oracle_Order_Number__c, EffectiveDate,recordType.name,
                               Order_Total__c, Status FROM Order WHERE Id IN :theOrderIdAndNVFollowMapping.keySet()
                              
                              ]) {
           //code Started  Added By jyotirmaya on 4/19/2016 , for 00166082                    
          if( theRecord.recordType.name == Constants.ORDER_NV_CREDIT){
             // do nothing
          }
          else{
              theFollowedOrders.add(new FollowedOrders(theOrderIdAndNVFollowMapping.get(theRecord.Id),theRecord));
          }       
           //  code added By jyotirmaya on 4/19/2016 , for 00166082        
          //theFollowedOrders.add(new FollowedOrders(theOrderIdAndNVFollowMapping.get(theRecord.Id),theRecord)); //code commented By jyotirmaya on 4/19/2016 , for 00166082  
        } 
      }
        
      theFollowedOrders.sort();

      return theFollowedOrders;
    }
    catch(Exception e) {
      system.debug('TRACE: NV_FollowedRecordsController - getFollowedOrders - Exception Occurred - ' + e.getMessage());
      system.debug('TRACE: NV_FollowedRecordsController - getFollowedOrders - Exception Occurred - ' + e.getStackTraceString());
    }
    return null;
  }

  /*
  @method      : unfollowAll
  @description : This method is used to unfollow all records except the 'User' record.
  @params      : void
  @returns     : Boolean theStatus
  */
  @AuraEnabled
  public static Boolean unfollowAll(String recordType) {
    Boolean theStatus = true;
    System.debug('TRACE: NV_FollowedRecordsController - unfollowAll - Beginning');
    try {
        //commented for Story S-389745
        //if(recordType == 'Account' || recordType == 'Order'){
        //Added for Story S-389745
        if(recordType == 'Order'){
          delete [SELECT Id FROM NV_Follow__c WHERE Following_User__c = :UserInfo.getUserId() AND Record_Type__c =: recordType];
        }        
        //Start - Added for Story S-389745
        if(recordType == 'Account'){
            delete [SELECT Id FROM NV_Follow__c WHERE Following_User__c = :UserInfo.getUserId() AND (Record_Type__c =: recordType OR Record_Type__c = 'Order')];
        }
        //End Story S-389745
    }
    catch(Exception e) {
      theStatus = false;

      system.debug('TRACE: NV_FollowedRecordsController - unfollowAll - Exception Occurred - ' + e.getMessage());
      system.debug('TRACE: NV_FollowedRecordsController - unfollowAll - Exception Occurred - ' + e.getStackTraceString());
    }
    return theStatus;
  }

  /*
  @method      : unfollowRecord
  @description : This method is used to unfollow the selected record.
  @params      : Id theRecordId
  @returns     : Boolean theStatus
  */
  @AuraEnabled
  public static Boolean unfollowRecord(String theRecordId) {
    Boolean theStatus = true;
    
    //Commented by Jyoti for Story S-389745
    /* 
    try {
      delete [SELECT Id FROM NV_Follow__c WHERE Following_User__c = :UserInfo.getUserId() AND Id = :Id.valueOf(theRecordId)];
    }*/
    
    //Start - Story S-389745
    String PREFIX = Account.sObjectType.getDescribe().getKeyPrefix();
    Set<String> accountID = new Set<String>();
    Set<Id> orderId = new Set<Id>();
    List<NV_Follow__c> followRecords = new List<NV_Follow__c>();
    followRecords = [SELECT Id,Record_Id__c,Record_Type__c FROM NV_Follow__c WHERE Following_User__c = :UserInfo.getUserId() AND Id = :Id.valueOf(theRecordId)];
    
    for(NV_Follow__c followRec: followRecords){
        if(followRec.Record_Id__c.startsWith(PREFIX)){
            accountID.add(followRec.Record_Id__c);
        }       
    }   
    for(Order order: [Select Id, AccountId From Order where AccountId in : accountID]){
        orderId.add(order.Id);
    }
    
    for(NV_Follow__c followRec: [SELECT Id,Record_Id__c,Record_Type__c FROM NV_Follow__c WHERE Following_User__c = :UserInfo.getUserId() AND Record_Id__c in:  orderId]){
        followRecords.add(followRec);
    }
    
    try {       
      delete followRecords;
    }
    //End - Story S-389745
    
    catch(Exception e) {
      theStatus = false;

      system.debug('TRACE: NV_FollowedRecordsController - unfollowRecord - Exception Occurred - ' + e.getMessage());
      system.debug('TRACE: NV_FollowedRecordsController - unfollowRecord - Exception Occurred - ' + e.getStackTraceString());
    }
    return theStatus;
  }
}