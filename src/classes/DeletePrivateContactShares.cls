//
// (c) 2014 Appirio, Inc.
// Class DeletePrivateContactShares
// Class to make contact records private
// 3/3/2106     Created By : Deepti Maheshwari   Reference - T-477658
global class DeletePrivateContactShares implements Database.Batchable<sObject>, System.Schedulable{
    //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query='';
        query = 'SELECT Id FROM Contact WHERE Private__c = true AND RecordType.Name = \'COMM Contact\'';
        System.debug('>>>>>query'+query);
        return Database.getQueryLocator(query);
    }
    
    //Execute Method
    global void execute(Database.BatchableContext BC,List<Sobject> scope){
        Set<ID> contactIDs = new Set<ID>();
        for(Contact con : (List<Contact>)scope){
            contactIDs.add(con.ID);
        }
        List<ContactShare> contactShares = [SELECT Id FROM ContactShare WHERE ContactID in :ContactIDs AND RowCause = 'Manual' LIMIT 10000];
        if(contactShares != null && contactShares.size() > 0){
            delete contactShares;
        }
        
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC){
    }
    
    // Schedular for batch.
    global void execute(SchedulableContext sc){
        if(!Test.isRunningTest()){
          Id batchId = Database.executeBatch(new DeletePrivateContactShares(), 200);
          system.debug('@@@batchId = ' + batchId);
        }
   }
}