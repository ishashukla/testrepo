/******************************************************************************************
 *  Purpose : Schedular class to Delete Archieve Orders
 *  Author  : Sunil Gupta
 *  Date    : Feb 12, 2014
*******************************************************************************************/  
global class Endo_DeleteOldOrdersScheduler implements Schedulable {
  
  // Call a batch class
  global void execute(SchedulableContext sc) {
    List<Order__c> orderRecords = [SELECT Id FROM Order__c WHERE Order_Entry_Date__c <= :System.today().addDays(-365) LIMIT 9999];
    System.debug('@@@' + orderRecords); 
    Database.delete(orderRecords, false); 
  }
}