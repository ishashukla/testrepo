// (c) 2015 Appirio, Inc.
//
// Class Name: Instruments_ContractTriggerHandler
// Description: Handler Class for ContractTrigger 
//
// February 25 2016, Prakarsh Jain  Original (T-479823)
//
public class Instruments_ContractTriggerHandler {
  //Method to update Reminder Date field on Contract Object.
  public static void updateReminderDateField(Map<Id, Contract> oldMap, List<Contract> newList){
    //Set<Id> setContract = new Set<Id>();
    Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Instruments');
  	Id recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
    List<Contract> updatedContractsNew = new List<Contract>();
    List<Contract> updatedContracts = new List<Contract>();
    List<Days_Left_For_Reminder__c> daysLeft = Days_Left_For_Reminder__c.getAll().values();
    Integer countTemp;
    if(daysLeft.size()>0){
      countTemp = Integer.valueOf(daysLeft.get(0).Initial_Notification_Days__c)/Integer.valueOf(daysLeft.get(0).Next_Notification_Days__c);
    }
    Integer innerCount = 0;
    system.debug('daysLeft+++'+daysLeft);
    system.debug('countTemp>>>'+countTemp);
    Set<Id> contractIdsSet = new Set<Id>();
    for(Contract contract : newList){
      if(oldMap == null ||(oldMap.get(contract.Id).EndDate != contract.EndDate)) {
      	if(contract.RecordTypeId == recordTypeInstrument){
      		contractIdsSet.add(contract.Id);
      	}
      }
    }
    system.debug('contractIdsSet>>>>'+contractIdsSet);
    updatedContracts = [SELECT Id, Name, EndDate, Reminder_Date__c FROM Contract WHERE Id IN: contractIdsSet];
    
    for(Contract contract : updatedContracts){
      while(countTemp > 0){
          system.debug('contract.EndDate.daysBetween(Date.Today())>>>'+Date.Today().daysBetween(contract.EndDate));
          system.debug('????'+Integer.valueOf(daysLeft.get(0).Initial_Notification_Days__c - (innerCount * daysLeft.get(0).Next_Notification_Days__c)));
          if(contract.EndDate > Date.Today() && Date.Today().daysBetween(contract.EndDate) >= Integer.valueOf(daysLeft.get(0).Initial_Notification_Days__c - (innerCount * daysLeft.get(0).Next_Notification_Days__c))){
            system.debug('INSIDEIF');
            contract.Reminder_Date__c = contract.EndDate.addDays((-1) * Integer.valueOf(daysLeft.get(0).Initial_Notification_Days__c - (innerCount * daysLeft.get(0).Next_Notification_Days__c)));
            system.debug('contract.Reminder_Date__c>>>'+contract.Reminder_Date__c);
            updatedContractsNew.add(contract);
            break;
          }
          innerCount++;
          countTemp--;
        }
    }
    
    system.debug('updatedContractsNew>>>'+updatedContractsNew);
    update updatedContractsNew;
  }
  
  /*public static void updateCheckBoxes(Map<Id, Contract> oldMap, List<Contract> newList, Boolean isDel){
    Set<Id> accountsSetId = new Set<Id>();
    List<Contract> lstContracts = new List<Contract>();
    Map<String,Integer> stringToIntegerMap = new Map<String,Integer>();
    List<Account> lstAccounts = new List<Account>();
    List<Account> accountsToBeUpdated = new List<Account>();
    system.debug('newList>>>'+newList);
    for(Contract contract : newList){
      system.debug('contract>>>'+contract);
      if(oldMap == null || (oldMap.get(contract.Id).AccountId != contract.AccountId)){
        accountsSetId.add(contract.AccountId);
        system.debug('accountsSetId???'+accountsSetId);
        if(oldMap!=null && oldMap.get(contract.Id).AccountId != contract.AccountId){
          system.debug('oldMap>>>'+oldMap);
          accountsSetId.add(oldMap.get(contract.Id).AccountId);
          stringToIntegerMap.put(oldMap.get(contract.Id).AccountId + '+' + 'Master Financial Agreement',0);
          stringToIntegerMap.put(oldMap.get(contract.Id).AccountId + '+' + 'Master Lease Agreement',0);
          stringToIntegerMap.put(oldMap.get(contract.Id).AccountId + '+' + 'Master Rental Agreement',0);
          stringToIntegerMap.put(oldMap.get(contract.Id).AccountId + '+' + 'Master Service Agreement',0);
        }
      }
      else if(isDel){
        accountsSetId.add(contract.AccountId);
        stringToIntegerMap.put(contract.AccountId + '+' + 'Master Financial Agreement', 0);
        stringToIntegerMap.put(contract.AccountId + '+' + 'Master Lease Agreement', 0);
        stringToIntegerMap.put(contract.AccountId + '+' + 'Master Rental Agreement', 0);
        stringToIntegerMap.put(contract.AccountId + '+' + 'Master Service Agreement', 0);
      }
    }
    system.debug('accountsSetId>>>'+accountsSetId);
    lstContracts = [SELECT Id, Name, Contract_Type__c, AccountId FROM Contract WHERE AccountId IN: accountsSetId];
    system.debug('lstContracts>>>'+lstContracts);
    for(Contract con : lstContracts){
      String key = con.AccountId + '+' + con.Contract_Type__c;
      if(!stringToIntegerMap.containsKey(key)){
        stringToIntegerMap.put(key,0);
      }
      if(stringToIntegerMap.containsKey(key)){
        Integer tempKey = stringToIntegerMap.get(key);
        tempKey = tempKey + 1;
        stringToIntegerMap.put(key,tempKey);
      }
      
    } 
    system.debug('stringToIntegerMap>>>'+stringToIntegerMap);
    lstAccounts = [SELECT Id, Name, Master_Financial_Agreement_MFA__c, Master_Lease_Agreement_MLA__c, Master_Rental_Agreement_MRA__c, Master_Service_Agreement_MSA__c FROM Account WHERE Id IN: accountsSetId];
    system.debug('lstAccounts>>>'+lstAccounts);
    for(Account account : lstAccounts){
      if(stringToIntegerMap.containsKey(account.Id + '+' + 'Master Financial Agreement')){
        system.debug('stringToIntegerMapFIN'+stringToIntegerMap);
        system.debug('<<<<'+(stringToIntegerMap.get(account.Id + '+' + 'Master Financial Agreement') == 0));
        if(stringToIntegerMap.get(account.Id + '+' + 'Master Financial Agreement') == 0){
          account.Master_Financial_Agreement_MFA__c = false;
        }
        else{
          account.Master_Financial_Agreement_MFA__c = true;
        }
      }
      if(stringToIntegerMap.containsKey(account.Id + '+' + 'Master Lease Agreement')){
        system.debug('stringToIntegerMapLEASE'+stringToIntegerMap);
        if(stringToIntegerMap.get(account.Id + '+' + 'Master Lease Agreement') == 0){
          account.Master_Lease_Agreement_MLA__c = false;
        }
        else{
          account.Master_Lease_Agreement_MLA__c = true;
        }
      }
      if(stringToIntegerMap.containsKey(account.Id + '+' + 'Master Rental Agreement')){
        system.debug('stringToIntegerMapREN'+stringToIntegerMap);
        if(stringToIntegerMap.get(account.Id + '+' + 'Master Rental Agreement') == 0){
          account.Master_Rental_Agreement_MRA__c = false;
        }
        else{
          account.Master_Rental_Agreement_MRA__c = true; 
        }
      }
      if(stringToIntegerMap.containsKey(account.Id + '+' + 'Master Service Agreement')){
        system.debug('stringToIntegerMapSER'+stringToIntegerMap);
        if(stringToIntegerMap.get(account.Id + '+' + 'Master Service Agreement') == 0){
          account.Master_Service_Agreement_MSA__c = false;
        }
        else{
          account.Master_Service_Agreement_MSA__c = true;
        }
      }
      system.debug('stringToIntegerMap???'+stringToIntegerMap);
      accountsToBeUpdated.add(account);
      system.debug('accountsToBeUpdated>>>'+accountsToBeUpdated);
    }
    if(accountsToBeUpdated.size() > 0){
      update accountsToBeUpdated;
    }
  }*/
}