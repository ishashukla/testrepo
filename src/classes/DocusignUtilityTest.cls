@isTest
private class DocusignUtilityTest {

    private static testMethod void test() {

     
       Map<String, Schema.RecordTypeInfo> accountRTMAp = Schema.SObjectType.Account.getRecordTypeInfosByName();
       
       List<Account> ac= TestUtils.createAccount(1,false);
       ac.get(0).RecordTypeID = accountRTMAp.get('COMM Corporate Accounts').getRecordTypeId();
       insert ac;
       List<Contact> con=TestUtils.createContact(1,ac[0].Id, true);
       
       List<User> u = TestUtils.createUser(4,'System Administrator',true);
       //String usrName = [SELECT LastName, Name FROM User WHERE LastName =:u.get(0).lastname].Name;
       List<Product2> prdt=TestUtils.createProduct(1, false);
       insert prdt;
       List<Project_Plan__c> ProPlan=TestUtils.createProjectPlan(1,ac[0].Id,false);
       ProPlan.get(0).Sales_rep__c = u[0].Id;
       ProPlan.get(0).Project_Manager__c = u[1].Id;
       ProPlan.get(0).Customer__c=con[0].Id; 
       Proplan.get(0).Finance__c=u[2].Id; 
       ProPlan.get(0).OwnerId = u[3].Id;
       //String usName = [SELECT Name FROM User WHERE Id=:ProPlan.get(0).OwnerId].Name;
       //ProPlan.get(0).Owner.Name=usName;
       insert ProPlan;
       List<Project_Phase__c> ProPhase=TestUtils.createProjectPhase(2,ProPlan[0].Id,false);
       insert ProPhase;
       
       Id changeRequestCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.COMM_CASE_RTYPE).getRecordTypeId();
      TestUtils.createRTMapCS(changeRequestCaseRT, Constants.COMM_CASE_RTYPE, 'COMM');

       Test.startTest();
       List<Case> cas=TestUtils.createCase(1,ac[0].Id,false);
       cas[0].RecordTypeId = changeRequestCaseRT;
       insert cas;
       Test.stopTest();
       
   //     String CustomerSignOffDoc=DocusignUtility.customersignoffdocument(ProPlan[0].Id);
   //     String InsertBeneficialProjectDoc=DocusignUtility.insertBeneficialProjectDocument(ProPlan[0].Id);
          String InsertDocusignDoc= DocusignUtility.insertDocusignDocuments(ProPlan[0].Id);
   //     String InsertBeneficialPhaseDoc=DocusignUtility.insertBeneficialPhaseDocument(ProPhase[0].Id);
          String InsertDocusignDocsFromPhaseobj=DocusignUtility.insertDocusignDocsFromPhase(ProPhase[0].Id);
   //       String RedirectToCODoc=DocusignUtility.redirectToCODocusign(cas[0].Id);
   //     String GetStandardDocusignRedirectUrlobj=DocusignUtility.getStandardDocusignRedirectUrl(String sourceObjectId) ;
      


    }
    

}