// (c) 2016 Appirio, Inc.
//
// Class Name: Intr_UserTriggerHandler - Handler Class to sync standard AccountTeam Members with custom AccountTeam
//
// June 27 2016, Shreerath Nair  Original (T-514970)
//
public class Intr_UserTriggerHandler {
	
	//Function to check and add Procare Rep as Account Team Member
	public static void addProcareRep(List<User> newList, Map<Id, User> oldMap){
		
		List<AccountShare> newAccountSharetoInsertList = new List<AccountShare>();
		List<AccountShare> accountShareToDeleteList = new List<AccountShare>();
		Map<id,Id> UsertoAccountMapForAccountShareMap = new Map<Id,Id>();
		Map<Id,Id> proCareRepMap = new Map<ID,ID>();
		//Check if Procare Rep field is updated
		for(User user : newList){
      		if((oldMap == null && user.ProCare_Region_Rep__c!=null) ||
      		   (oldMap != null && user.ProCare_Region_Rep__c != oldMap.get(user.Id).ProCare_Region_Rep__c && user.ProCare_Region_Rep__c!=null)){ 
        		proCareRepMap.put(user.Id,user.ProCare_Region_Rep__c);   		  		
        		
      		}    		
    	}
    	
    	Map<Id,AccountTeamMember> accTeamMap = new Map<Id,AccountTeamMember>();
        
        Map<id, SET<Id> > accountIdToUser = new    Map<id,SET<Id>> ();
        //Get all AccountTeam Member of the Users and sort according to Accounts 
   		for(AccountTeamMember accountTeam : [SELECT AccountId,userId,TeamMemberRole
    							     FROM AccountTeamMember
    								 WHERE UserId IN :proCareRepMap.keySet()]){
    								 	
   			SET<Id> accountTeamMembers = (accountIdToUser.get(accountTeam.Accountid) == null) ?
                                                       new  SET<ID>() :
                                                       accountIdToUser.get(accountTeam.Accountid);	
   			accountTeamMembers.add(accountTeam.UserId);
   			accountIdToUser.put(accountTeam.Accountid,accountTeamMembers);			
   		}
   		
   		Map<Id,Id> addRepToAccountMap = new Map<Id,Id>();
   		Map<Id,Id> deleteAccountMemberMap  = new Map<Id,Id>();
   		
   		//Check for All Account
   		for(Id accId: accountIdToUser.keySet()){
   				
   			SET<Id> temp = accountIdToUser.get(accId);
   			//check for All account team member of a particular account
   			for(ID userId : accountIdToUser.get(accId)){
   				if(proCareRepMap.containsKey(userId)){
   					//If Procare rep does not exist in the Account as Account Team then add 
 					if(!temp.contains(proCareRepMap.get(userId))){
 						addRepToAccountMap.put(accId,proCareRepMap.get(userId));
 															
 					}
 					else{
 						//If exist then update the record of With its ROle as Procare Rep
 						deleteAccountMemberMap.put(accId,proCareRepMap.get(userId));
 						UsertoAccountMapForAccountShareMap.put(accId,proCareRepMap.get(userId));
 						addRepToAccountMap.put(accId,proCareRepMap.get(userId));
 						
 					}
 					//Create sharing record of the ProCare rep Added.
 					AccountShare accShare = new AccountShare();
          			accShare.AccountId = accId;
          			accShare.UserOrGroupId = proCareRepMap.get(userId);
          			accShare.RowCause = Schema.AccountShare.RowCause.Manual;
          			accShare.OpportunityAccessLevel = 'None';
          			accShare.ContactAccessLevel = 'None';
          			accShare.CaseAccessLevel = 'None';
  					accShare.AccountAccessLevel = 'Edit';
  					newAccountSharetoInsertList.add(accShare);
 							
   				}	
   			}		
   		}
   		
		//check if Account team Member with Procare rep already existing on Account then delete.
   		for(AccountTeamMember ac:[SELECT userId,TeamMemberRole,accountId
   								  FROM AccountTeamMember
   								  WHERE AccountID IN: addRepToAccountMap.KeySet() 
   								  AND TeamMemberRole = : Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP] ){
   			
   			if(addRepToAccountMap.containsKey(ac.accountId)){
   				deleteAccountMemberMap.put(ac.accountId,ac.Id); 
   				UsertoAccountMapForAccountShareMap.put(ac.accountId,ac.UserId);				
   			}	
   		} 
   		//function to Update the share Record of the Account
   		processAccountShareRecords(UsertoAccountMapForAccountShareMap);
   		//Function to create and delete the Account team 
   		processPrcareRep(addRepToAccountMap,deleteAccountMemberMap);
   		
   		//Insert AccountShare records
   		if(newAccountSharetoInsertList.size()>0){
			database.insert(newAccountSharetoInsertList);
		}	
		
	}
	
	//Function to Process the share records of the User added or removed from Account Team
	public static void processAccountShareRecords(Map<Id,ID> UsertoAccountMapForAccountShareMap){
		
		List<AccountShare> deleteAccountShareList = new List<AccountShare>();
		for(AccountShare accShare: [SELECT ID 
							FROM AccountShare
							WHERE UserOrGroupId IN : UsertoAccountMapForAccountShareMap.Values()
							AND AccountId IN : UsertoAccountMapForAccountShareMap.KeySet()]){
		
			deleteAccountShareList.add(accShare);						
		}
		System.debug('@@@2shreee' + UsertoAccountMapForAccountShareMap);
		if(deleteAccountShareList.size()>0){
			database.delete(deleteAccountShareList,false);
		}
		
		
	}
	//Funciton to insert and delete the Users from the Account team.
	public static void processPrcareRep(Map<Id,Id> addRepToAccountMap,Map<Id,Id> deleteAccountMemberMap){
		
		List<Custom_Account_Team__c> customAccountTeamList = new List<Custom_Account_Team__c>();
		List<AccountTeamMember> AccountTeamList = new List<AccountTeamMember>();
		//Delete any perviously existing Procare Rep on Account
		for(Custom_Account_Team__c customAccountTeam : [SELECT Id ,Account__c
											 FROM Custom_Account_Team__c
											 WHERE Account__c IN: deleteAccountMemberMap.KeySet() 
											 AND Team_Member_Role__c =: Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP]){
			if(addRepToAccountMap.containsKey(customAccountTeam.Account__c)){
   				Custom_Account_Team__c	 custAcc = new Custom_Account_Team__c(id = customAccountTeam.Id);
				customAccountTeamList.add(custAcc);				
   			}
											 	
		}
		
		if(customAccountTeamList.size()>0){
			Database.delete(customAccountTeamList,false);
		}
		
		customAccountTeamList.clear();
		
		//Delete from Standrd Account Team as well.	
		for(Id id: deleteAccountMemberMap.keySet()){
					
			 AccountTeamMember accTeamMem = new AccountTeamMember(Id = deleteAccountMemberMap.get(id));
			 AccountTeamList.add(accTeamMem);    															
		}
		
		if(AccountTeamList.size()>0){
			database.delete(AccountTeamList,false);
		}
		
		AccountTeamList.clear();
		//Create Procare Rep as Account Team Member of a Particular Account for both Custom and Standard Account Team
		for(Id id : addRepToAccountMap.KeySet()){
			Custom_Account_Team__c	 custAcc = new Custom_Account_Team__c(User__c = addRepToAccountMap.get(id),
																		  Team_Member_Role__c = Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP,
																		  Account__c = id,
																		  Account_Access_Level__c = 'Read/Write');
		    customAccountTeamList.add(custAcc);
		    
		    AccountTeamMember accTeamMem = new AccountTeamMember(UserId = addRepToAccountMap.get(id),
		    													 TeamMemberRole = Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP,
		    													 Accountid = id);
		    
		    AccountTeamList.add(accTeamMem);
		}
		
		if(customAccountTeamList.size()>0){
			database.insert(customAccountTeamList,false);
		}
		
		if(AccountTeamList.size()>0){
			database.insert(AccountTeamList,false);
		}	
		
	}
	
	
	
	
    
}