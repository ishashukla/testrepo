/**
 *  Purpose         :       This class is consist of CCKM code from SyncStandardAccountTeamMember class marked with //T-514970 (SN).
 *
 *  Created By      :       Padmesh Soni (Appirio Offshore)
 *
 *  Created Date    :       10/06/2016
 *
 *  Current Version :       V_1.0
 *
 *  Revision Log    :       V_1.0 - Created - S-441563
 **/
global class SyncProcareRepBatch implements Database.Batchable<sObject>{
    
    //T-514970 (SN)   
	public List<AccountTeamMember> memberToAdd = new List<AccountTeamMember>();
	public List<AccountTeamMember> memberToDelete = new List<AccountTeamMember>();
	public List<AccountTeamMember> memberToUpdate = new List<AccountTeamMember>();
	public List<AccountShare> newAccountSharetoInsertList = new List<AccountShare>();
	public List<AccountShare> accountShareToDeleteList = new List<AccountShare>();
	
	global Database.QueryLocator start(Database.BatchableContext BC){
	
		DateTime dt = System.Now().addDays(-1);
		String query = 'SELECT User__c,User__r.Procare_Region_Rep__c ,Team_Member_Role__c, Name, IsDeleted__c, LastModifiedDate, Id, External_Integration_Id__c, '
							+'Effective_From_Date__c, Division__c, Account__c, Account_Access_Level__c FROM Custom_Account_Team__c WHERE LastModifiedDate > :dt '
							+'AND Team_Member_Role__c != null AND Account_Access_Level__c != null';
							
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC,List<Sobject> scope){
		
		Set<Id> userIdsSet = new Set<Id>();
	    Set<Id> accountIdsSet = new Set<Id>();
	    
		List<AccountTeamMember> accountMemberListToBeInserted = new List<AccountTeamMember>();
	    List<AccountTeamMember> accountMemberListToBeUpdated = new List<AccountTeamMember>();
	    List<AccountTeamMember> accountMemberListToBeDeleted = new List<AccountTeamMember>();
	    List<AccountShare> accountShareList = new List<AccountShare>();
	    List<AccountShare> accountShareListtoUpdate = new List<AccountShare>();
	    List<AccountShare> accountShareListtoInsert = new List<AccountShare>();
	    List<AccountShare> accountShareListtoDelete = new List<AccountShare>();
	    
		//T-514970 (SN)
		Map<ID,Custom_Account_Team__c> UserIdWithCustomAccountMemberMap =  new Map<Id,Custom_Account_Team__c>();
		List<Custom_Account_Team__c> customAccountTeamToDeleteList = new List<Custom_Account_Team__c>();
			
	    for(Sobject s : scope){
		    Custom_Account_Team__c customAccountTeam = (Custom_Account_Team__c)s;
		    userIdsSet.add(customAccountTeam.User__c);
		    accountIdsSet.add(customAccountTeam.Account__c);
	    }
	    
	    Map<String,AccountShare> AccUserRoletoShareRecord = new Map<String,AccountShare>();
	    Map<String,AccountTeamMember> AccUserRoletoAccountTeam = new Map<String,AccountTeamMember>();
	    for(AccountShare accShare : [SELECT UserOrGroupId, RowCause, AccountId, AccountAccessLevel  FROM AccountShare WHERE UserOrGroupId IN:userIdsSet
	                                 AND AccountId IN: accountIdsSet AND RowCause =:Schema.AccountShare.RowCause.Manual]){
	                                 	
			AccUserRoletoShareRecord.put(accShare.AccountId+'+'+accShare.UserOrGroupId,accShare);
	    }
	    
	    for(AccountTeamMember accMember : [SELECT UserId, TeamMemberRole, Id, AccountId, AccountAccessLevel FROM AccountTeamMember WHERE UserId IN: userIdsSet 
												AND AccountId IN: accountIdsSet]){
			AccUserRoletoAccountTeam.put(accMember.AccountId+'+'+accMember.UserId+'+'+accMember.TeamMemberRole,accMember);
	    }
	    
		for(Sobject s : scope) {
			
			Custom_Account_Team__c customAccountTeam = (Custom_Account_Team__c)s;
			
			String accTeamkey = customAccountTeam.Account__c+'+'+customAccountTeam.User__c+'+'+customAccountTeam.Team_Member_Role__c;
			String accSharekey = customAccountTeam.Account__c+'+'+customAccountTeam.User__c;
			
			// Insert Standard Account team Records
			System.debug('>>>>1'+accTeamkey);
			System.debug('>>>>2'+AccUserRoletoAccountTeam.containsKey(accTeamkey));
			
			if(AccUserRoletoAccountTeam.containsKey(accTeamkey)){
		
				//T-514970 (SN) check if team role is Procare Rep
				If(customAccountTeam.Team_Member_Role__c == Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP){
					UserIdWithCustomAccountMemberMap.put(customAccountTeam.User__c,customAccountTeam);
				}
				
				System.debug('>>>>3'+customAccountTeam.IsDeleted__c);
			} else{
				
				if(!customAccountTeam.IsDeleted__c){
			
					//T-514970 (SN) User Record to check for Procare Rep
					UserIdWithCustomAccountMemberMap.put(customAccountTeam.User__c,customAccountTeam);
				}
			}
			// Insert sharing records to povide Edit access to account team members and managers in Roles and Hierarchy
			if(!AccUserRoletoShareRecord.containsKey(accSharekey)){
		
				if(!customAccountTeam.IsDeleted__c){
				
					//T-514970 (SN) function to create account share record
					AccountShare accShare = createAccountShareRecord(customAccountTeam);
					accountShareListtoInsert.add(accShare);
				}
			}
		}
		
		//T-514970 (SN) Function to check for Procare Rep
	    getAllProcareRep(UserIdWithCustomAccountMemberMap);
	    
	    //T-514970 (SN) include procare users into insert list
	    if(memberToAdd.size()>0){
	        accountMemberListToBeInserted.addall(memberToAdd);
	    }
	    //T-514970 (SN) include procare users into update list
	     if(memberToUpdate.size()>0){
	        accountMemberListToBeUpdated.addall(memberToUpdate);
	    }
	    //T-514970 (SN) include previous procare users into delete list
	    if(memberToDelete.size()>0){
	        accountMemberListToBeDeleted.addall(memberToDelete);
	    }
	    //T-514970 (SN) include procare user's share records into insert list
	    if(newAccountSharetoInsertList.size()>0){
	        accountShareListtoInsert.addall(newAccountSharetoInsertList);
	    }
	    //T-514970 (SN) include procare user's share records into delete list
	    if(accountShareToDeleteList.size()>0){
	        accountShareListtoDelete.addall(accountShareToDeleteList);
	    }
	    
	    if(accountShareListtoInsert.size() > 0){
	      Database.insert(accountShareListtoInsert,false);
	    }
	    if(accountShareListtoUpdate.size() > 0){
	      Database.update(accountShareListtoUpdate,false);
	    }
	    if(accountShareListtoDelete.size() > 0){
	      Database.delete(accountShareListtoDelete,false);
	    }
	    if(accountMemberListToBeInserted.size()>0){
	      Database.insert(accountMemberListToBeInserted,false);
	    }
	    if(accountMemberListToBeUpdated.size()>0){
	      Database.update(accountMemberListToBeUpdated,false);
	    }
	    if(accountMemberListToBeDeleted.size() > 0){
	      Database.delete(accountMemberListToBeDeleted,false);
	    }
	    
	    //T-514970 (SN) check if the Account team member's Role is 'Surgical'
    	checkForSurgicalUsers(UserIdWithCustomAccountMemberMap);
	}
	
	global void finish(Database.BatchableContext BC){
		
	}
	
	//T-514970 (SN) function to create accountShare record
	public static AccountShare createAccountShareRecord(Custom_Account_Team__c customAccountTeam){
	
		String picklistforEditAccess = Label.AccountTeamSyncEditAccessPicklistValues;
		AccountShare accShare = new AccountShare();
		accShare.AccountId = customAccountTeam.Account__c;
		accShare.UserOrGroupId = customAccountTeam.User__c;
		accShare.RowCause = Schema.AccountShare.RowCause.Manual;
		accShare.OpportunityAccessLevel = 'None';
		accShare.ContactAccessLevel = 'None';
		accShare.CaseAccessLevel = 'None';
		
		if(customAccountTeam.Account_Access_Level__c!=null && picklistforEditAccess.containsIgnoreCase(customAccountTeam.Account_Access_Level__c)){
			accShare.AccountAccessLevel = 'Edit';
		} else{
			accShare.AccountAccessLevel = 'Read';
		}
		
		return accShare;
		
	}
	
	
	//T-514970 (SN) Function to check for all Account Team Member's Procare Rep
	public void getAllProcareRep(Map<Id,Custom_Account_Team__c> UserIdWithCustomAccountMemberMap){
	
		Map<ID,Id> accountWithProcareRep = new Map<ID,Id>();
		List<Custom_Account_Team__c> customAccountTeamToInsert  = new List<Custom_Account_Team__c>();
		List<Custom_Account_Team__c> customAccountTeamToUpdate  = new List<Custom_Account_Team__c>();
		Map<ID,ID> UserWithAccountToUpdateMap = new Map<Id,Id>();
		
		Map<id,Id> UsertoAccountMapForAccountShareMap = new Map<Id,Id>();
		
		//Check for all procare rep of user
		for(Id id :  UserIdWithCustomAccountMemberMap.KeySet()){
			
			User user = UserIdWithCustomAccountMemberMap.get(id).User__r;
			Custom_Account_Team__c customAccountTeamofUser = UserIdWithCustomAccountMemberMap.get(id);
			
			if(user.ProCare_Region_Rep__c!=null){
				
				Custom_Account_Team__c  customAccountTeamofProcareRep = UserIdWithCustomAccountMemberMap.get(user.ProCare_Region_Rep__c);
				
				//if procare rep is already added in account team then update the role with 'Procare Rep'
				if(UserIdWithCustomAccountMemberMap.containsKey(user.ProCare_Region_Rep__c) 
					&& customAccountTeamofUser.Account__c == customAccountTeamofProcareRep.Account__c) {
					
					UserWithAccountToUpdateMap.put(user.ProCare_Region_Rep__c,customAccountTeamofProcareRep.Account__c);
					accountWithProcareRep.put(user.ProCare_Region_Rep__c,customAccountTeamofUser.Account__c);
					
				} //If user is not procare rep then Create User's Procare rep Account team & Account Share Records.
				else if(customAccountTeamofUser.Team_Member_Role__c!= Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP){
					
					AccountTeamMember accountTeamMember = new AccountTeamMember(userID=user.ProCare_Region_Rep__c , 
																					TeamMemberRole = Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP, 
																					AccountId = customAccountTeamofUser.Account__c);
					memberToAdd.add(accountTeamMember)  ;
					accountWithProcareRep.put(user.ProCare_Region_Rep__c,customAccountTeamofUser.Account__c);
					
					Custom_Account_Team__c customAccountTeam = new Custom_Account_Team__c(User__c = user.ProCare_Region_Rep__c, 
																							Team_Member_Role__c = Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP,
																							Account_Access_Level__c = customAccountTeamofUser.Account_Access_Level__c, 
																							Account__c = customAccountTeamofUser.Account__c);
					customAccountTeamToInsert.add(customAccountTeam);
					
					AccountShare accShare = createAccountShareRecord(customAccountTeam);
					newAccountSharetoInsertList.add(accShare);
				} else{
					
					//Map to check if any other procare user is existing in the Account
					accountWithProcareRep.put(user.ProCare_Region_Rep__c,customAccountTeamofUser.Account__c);
				}
			}               
		}
		
		List <Custom_Account_Team__c> customAccountTeamToDelete = new List<Custom_Account_Team__c>();
		
		SET<Id> UserWithAccountToUpdateSet  = new SET<Id>();
		SET<Id> accountWithProcareRepSet = new SET<Id>();
		accountWithProcareRepSet.addall(accountWithProcareRep.Values());
		UserWithAccountToUpdateSet.addAll(UserWithAccountToUpdateMap.Values());
		
		//Delete Perviously existing Procare Rep or Update Role if Pervious exited in Custom Account Team of an Account
		for(Custom_Account_Team__c customAccountTeam: [SELECT ID,Account__c,User__c,Team_Member_Role__c FROM Custom_Account_Team__c 
															WHERE (User__c NOT IN : accountWithProcareRep.KeySet() 
															AND Account__c IN : accountWithProcareRepSet 
															AND Team_Member_Role__c =: Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP) 
															OR (User__c IN : UserWithAccountToUpdateMap.keySet() AND Account__c IN : UserWithAccountToUpdateSet )]){
			
			if(!accountWithProcareRep.containsKey(customAccountTeam.User__c)
				&& accountWithProcareRepSet.contains(customAccountTeam.Account__c)
				&& customAccountTeam.Team_Member_Role__c == Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP){
			
				customAccountTeamToDelete.add(customAccountTeam);
				UsertoAccountMapForAccountShareMap.put(customAccountTeam.User__c,customAccountTeam.Account__c); 
			}
			
			if(UserWithAccountToUpdateMap.containsKey(customAccountTeam.User__c) && UserWithAccountToUpdateSet.contains(customAccountTeam.Account__c)){
			
				customAccountTeam.Team_Member_Role__c = Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP;
				customAccountTeamToUpdate.add(customAccountTeam);   
			}
		}
		
		//Delete Perviously existing Procare Rep in Account share record of an Account
		for(AccountShare accShare : [SELECT ID FROM AccountShare  WHERE UserOrGroupId IN :UsertoAccountMapForAccountShareMap.KeySet() 
										AND AccountId IN : UsertoAccountMapForAccountShareMap.Values()]){
		
			accountShareToDeleteList.add(accShare);                 
		}
		
		if(customAccountTeamToDelete.size()>0){
			database.delete(customAccountTeamToDelete,false);
		}
		
		//Delete Perviously existing Procare Rep or Update existing role in Standard Account Team record of an Account
		for(AccountTeamMember accountTeamMember : [SELECT Id,TeamMemberRole,UserID,AccountId FROM AccountTeamMember WHERE (AccountId IN : accountWithProcareRepSet
														AND  USERID NOT IN : accountWithProcareRep.KeySet() 
														AND TeamMemberRole = : Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP) 
														OR (UserID IN : UserWithAccountToUpdateMap.keySet() AND AccountId IN : UserWithAccountToUpdateSet)]){
			
			if(!accountWithProcareRep.containsKey(accountTeamMember.UserId) && accountWithProcareRepSet.contains(accountTeamMember.AccountId)
				&& accountTeamMember.TeamMemberRole == Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP){    
			
				memberToDelete.add(accountTeamMember);
			}
			
			if(UserWithAccountToUpdateMap.containsKey(accountTeamMember.UserId) && UserWithAccountToUpdateSet.contains(accountTeamMember.AccountId)){
			
				accountTeamMember.TeamMemberRole = Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_PROCARE_REP;
				memberToUpdate.add(accountTeamMember);
			}                           
		}
		
		//Insert new CustomAccountTeam  
		If(customAccountTeamToInsert.size()>0){
			database.insert(customAccountTeamToInsert,false);
		}
		
		//update the Custom Account team Role of the Procare Rep User if previously existing in Account
		if(customAccountTeamToUpdate.size()>0){
			database.update(customAccountTeamToUpdate);
		}
	}
	
	//T-514970 (SN) Check if the Account team member Role is 'Surgical', then add the User's Role in Account's Procare Region Field
	public void checkForSurgicalUsers(Map<Id,Custom_Account_Team__c> AccountWithUserIdMap){
		
		List<Account> accountToUpdateList = new List<Account>();
		
		for(AccountTeamMember accountTeam : [SELECT User.UserRole.Name,AccountId FROM AccountTeamMember  WHERE UserId IN : AccountWithUserIdMap.keySet()
												AND TeamMemberRole = : Intr_Constants.INSTRUMENTS_ACCOUNT_TEAM_ROLE_SURGICAL AND User.UserRole.Name LIKE '%Surg%']){
		
			Account account = new Account(ID = accountTeam.AccountId, Empower_Surgical_Region__c = accountTeam.USER.UserRole.Name);
			accountToUpdateList.add(account);
		}
		
		if(accountToUpdateList.size()>0){
			database.update(accountToUpdateList);
		}
	}	
}