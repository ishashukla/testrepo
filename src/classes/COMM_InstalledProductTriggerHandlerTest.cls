@isTest
public class COMM_InstalledProductTriggerHandlerTest {
   @isTest
  static void COMM_AssetTriggerHandlerTest() {
    //NB - 11/14 I-242624- Start

    List <Product2> productList = TestUtils.createCOMMProduct(1, false);
    productList[0].Product_Class_Code__c = '001';
    insert productList;
    List <Account> accountList = TestUtils.createAccount(1, true);
    List <Project_Plan__c> projectList = TestUtils.createProjectPlan(1,accountList[0].id,true);
    List<Project_Phase__c> lstPhases1 = TestUtils.createProjectPhase(1,projectList.get(0).Id,true);
    List <Order> orderList = TestUtils.createOrder(1, accountList[0].id, 'Entered',  false); 

    PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productList[0].Id, UnitPrice = 10000, IsActive = true);
    insert standardPrice;

    for(Order orderRec : orderList) {
      orderRec.Project_Plan__c = projectList[0].id;
      orderRec.Oracle_Order_Number__c = '12';
      orderRec.PriceBook2Id = Test.getStandardPricebookId();
    }
    insert orderList;
    
    List<OrderItem> orderitems=TestUtils.createOrderItem(2,lstPhases1[0].Id,orderList[0].Id,standardPrice.Id,false);
    orderitems[0].Status__c='Entered';
    orderitems[1].Status__c='Entered';
    orderitems[0].Requested_Ship_Date__c = System.today().addDays(10);
    insert orderitems;

    List <SVMXC__Installed_Product__c> instProdList = TestUtils.createInstalledProducts(2, productList[0].id, orderList[0].id, 'QA', true);
    SVMXC__Installed_Product__c instProd = [SELECT Project_Plan__c, Name, RecordTypeId, Create_Checklist__c, QIP_ID__c
                    FROM SVMXC__Installed_Product__c
                    Where Id =: instProdList.get(0).Id];
      System.assertEquals(orderList[0].Project_Plan__c, instProd.Project_Plan__c);

    List<ChecklistCUST__c> checkListCustomList = TestUtils.createCustomChecklist(1, 'QIP SINGLE VISIU', 1,false); 
    checkListCustomList[0].Name = instProd.QIP_ID__c;
    insert checkListCustomList;

    Checklist_Header__c checklist = new checklist_Header__c(status__c='New',Asset__c = instProd.id, isLatest__c = true);
    insert checklist;
    List<Checklist_Group__c> checkListGroupList = TestUtils.createChecklistGroup(1, 'Test Name',checkListCustomList[0].id, 1,true);
    Checklist_Item__c checkListItemList = new CheckList_Item__c (
               Active__c = true,
               //Name = 'TestItem',
               Checklist__c = checkListCustomList[0].id,
               Checklist_Group__c = checkListGroupList[0].id,
               Required__c = true);
    insert checkListItemList;
     
    instProdList.get(1).SVMXC__Parent__c = instProd.Id;

    instProdList.get(0).RecordTypeId = Constants.getRecordTypeId(Constants.RT_COMM_INSTALL_PRODUCT,Constants.INSTALL_PRODUCT_SOBJECT_ASSET);
    instProdList.get(0).Create_Checklist__c = true;  
    instProdList.get(0).SVMXC__status__c =  'Terminated';
    instProdList.get(0).Inactivate_Child_Assets__c = 'Yes';
    instProdList.get(0).Project_Phase__c = lstPhases1.get(0).Id;
    instProdList[0].Order_Product__c = orderitems[0].Id;
    instProdList[0].SVMXC__Warranty_Start_Date__c = system.today();
    update instProdList;

    //NB - 11/14 I-242624 - End
  }
}