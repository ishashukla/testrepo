// (c) 2015 Appirio, Inc.
//
// Class Name: AccountScheduleController
// Description: Contoller Class for AccountSchedule Page. Class to show calendar for blocked schedule on the contact related list.
//
// February 5 2016, Prakarsh Jain  Original (T-455039)
//
public class AccountScheduleController {
  public List<BlockScheduleWrapper> lstBlockScheduleWrapper;
  public String contactTimezone {get;set;}
  public Contact con;
  public Contact cont;
  public String contactName{get;set;}
  public String accId;
  public List<Block_Schedule__c> lstBlckSch;
  public Block_Schedule__c blckSchedule{get;set;}
  public BlockScheduleWrapper wrapperBlck;
  public String jSONString{get;set;}
  public User usr{get;set;}
  public String timeZoneString{get;set;}
  public map<String, String> mapMonths; 
  public String dayOfWeek{get;set;} 
  public map<String, String> mapDays;
  public String saveDt{get;set;}
  public String strtDate{get;set;}
  public String endDt{get;set;}
  public String titl{get;set;}
  public String blckRecId{get;set;}
  public List<Contact_Acct_Association__c> lstConAccAsso{get;set;}
  public String selectedContactOrAccount{get;set;}
  public String insertContactOrAccount{get;set;}
  public String currentDt{get;set;}
  public String startDt{get;set;}
  public Map<String,String> dateToDayMap{get;set;}
  public String assignToContactOrAccount{get;set;}
  public list<String> lstCon;
  public Account acct;
  public Date datetoday {get;set;}
  public Boolean isPrintView {get;set;}
  public Boolean inSF1 {get;set;}
  public Map<Id,String> contactToTimezoneMap{get;set;}
  public Map<Id,String> contactToRoleMap{get;set;}
  
  //Constructor
  public AccountScheduleController(ApexPages.StandardController sc){
    jSONString = '';
    isPrintView = false;
    lstConAccAsso = new List<Contact_Acct_Association__c>();
    acct = new Account();
    mapMonths = new map<String, String>();
    mapDays = new map<String, String>();
    dateToDayMap =new map<String, String>();
    lstBlckSch = new List<Block_Schedule__c>();
    usr = new User();
    lstCon = new list<String>();
    titl = '';
    contactToTimezoneMap = new Map<Id,String>();
    contactToRoleMap = new Map<Id,String>();
    inSF1 = false;
    if(UserInfo.getUiTheme() == 'Theme4t'){
      inSF1 = true;
    }
    blckSchedule = new Block_Schedule__c(notes__c = '', title__c = '');
    mapDays = currentDateMap();
    lstBlockScheduleWrapper = new List<BlockScheduleWrapper>();
    mapMonths.put('Jan', '01');
    mapMonths.put('Feb', '02');
    mapMonths.put('Mar', '03');
    mapMonths.put('Apr', '04');
    mapMonths.put('May', '05');
    mapMonths.put('Jun', '06');
    mapMonths.put('Jul', '07');
    mapMonths.put('Aug', '08');
    mapMonths.put('Sep', '09');
    mapMonths.put('Oct', '10');
    mapMonths.put('Nov', '11');
    mapMonths.put('Dec', '12');
    isPrintView = (ApexPages.currentPage().getParameters().get('isPrintView') == 'true') ? true : false;
    selectedContactOrAccount = (ApexPages.currentPage().getParameters().get('prevContact') != null && ApexPages.currentPage().getParameters().get('prevContact') != '' ) ? ApexPages.currentPage().getParameters().get('prevContact') : 'All';
    insertContactOrAccount = 'All';
    assignToContactOrAccount = selectedContactOrAccount;
    accId = ApexPages.currentPage().getParameters().get('id');
    if(ApexPages.currentPage().getParameters().get('selectedContactOrAccount')!=null && ApexPages.currentPage().getParameters().get('selectedContactOrAccount')!=''){
      selectedContactOrAccount = ApexPages.currentPage().getParameters().get('selectedContactOrAccount');
      showSchAsPerAccounts(selectedContactOrAccount, accId);
    }
    else{
      selectedContactOrAccount = 'All';
      accountScheduleCon(accId);
    }
    //setInitialDates();
   }
   
   // Helper method to get the list of all Contacts related to the account and show the blocked calendar on Account page.
   public void accountScheduleCon(String accId){
     usr = [SELECT Id, Name, TimeZoneSidKey
           FROM User 
           WHERE Id =: UserInfo.getUserId()];
    lstConAccAsso = [SELECT Associated_Account__c, Associated_Contact__c, Associated_Account__r.Name, Associated_Contact__r.Name
                     FROM Contact_Acct_Association__c
                     WHERE Associated_Account__c =: accId];
    if(accId != null && accId != ''){
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id, 
                    Contact__r.Name, Contact__r.Id
                    FROM Block_Schedule__c 
                    WHERE Account__c =: accId];
    }
    if(lstBlckSch.size() == 0){
      Block_Schedule__c blck = new Block_Schedule__c();
      blck = createBlockScheduleData(blck);
      lstBlckSch.add(blck);
    }
    convertToJSON(lstBlckSch);
   }
   
   public void showSchAsPerAccounts(String selectedContactOrAccount, String accId){
    usr = [SELECT Id, Name, TimeZoneSidKey
           FROM User 
           WHERE Id =: UserInfo.getUserId()];
    lstConAccAsso = [SELECT Associated_Account__c, Associated_Contact__c, Associated_Account__r.Name, Associated_Contact__r.Name
                     FROM Contact_Acct_Association__c
                     WHERE Associated_Account__c =: accId];
    if(accId != null && accId != ''){
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id,
                    Contact__r.Name, Contact__r.Id
                    FROM Block_Schedule__c 
                    WHERE Contact__c =: selectedContactOrAccount AND Account__c =: accId];
    }
    if(lstBlckSch.size() == 0){
      Block_Schedule__c blck = new Block_Schedule__c();
      blck = createBlockScheduleData(blck);
      lstBlckSch.add(blck);
    }
    if(selectedContactOrAccount!='All'){
      cont = [SELECT Id, Name FROM Contact WHERE Id =: selectedContactOrAccount];
      contactName = cont.Name;
    }
    convertToJSON(lstBlckSch);
  }
  
  //Method to convert the list of Block Schedules to JSON.
  public void convertToJSON(List<Block_Schedule__c> lstBlckSch){
   Set<id> conIdBlck = new Set<Id>();
   Map<Id,String> contactIdToRoleBlckMap = new Map<Id,String>();
   if(lstConAccAsso.size() > 0){
     for(Contact_Acct_Association__c conAcc : lstConAccAsso){
       conIdBlck.add(conAcc.Associated_Contact__c);
     }
   }
   List<Contact>  conListBlck = new List<Contact>();
   conListBlck = [SELECT Id, Timezone__c, Personnel__c FROM Contact WHERE Id IN :conIdBlck];
   for(Contact c : conListBlck){
     contactIdToRoleBlckMap.put(c.Id,c.Personnel__c);
   }
   lstBlockScheduleWrapper = new List<BlockScheduleWrapper>();
   if(lstBlckSch.size()>0){
     for(Block_Schedule__c blck : lstBlckSch){
     wrapperBlck = new BlockScheduleWrapper();
     wrapperBlck.contact = blck.Contact__r.Name;
     wrapperBlck.contactid = blck.Contact__r.Id;
     wrapperBlck.id = blck.Id;
     wrapperBlck.start = convertDateTime(blck.starting_Time__c,blck.dow__c);
     wrapperBlck.endDate = convertDateTime(blck.Ending_Time__c,blck.dow__c);
     //TM Checked this and its not causing the account picklist bug
     if(wrapperBlck.endDate.contains('T00:00:00.000')){
       wrapperBlck.endDate = wrapperBlck.endDate.replace('T00:00:00.000', 'T24:00:00.000');
     }
     if((wrapperBlck.contact != null || wrapperBlck.contact != '') && (contactIdToRoleBlckMap.containsKey(wrapperBlck.contactid) && contactIdToRoleBlckMap.get(wrapperBlck.contactid) != null) || (contactIdToRoleBlckMap.containsKey(wrapperBlck.contactid) && contactIdToRoleBlckMap.get(wrapperBlck.contactid) != '')){
      wrapperBlck.title = (blck.title__c != null) ? blck.title__c : '';
      //wrapperBlck.title += '</br>Surgeon:' + wrapperBlck.contact;
      String contactRole =  contactIdToRoleBlckMap.get(wrapperBlck.contactid)!=null ? contactIdToRoleBlckMap.get(wrapperBlck.contactid) + ': ': '';
      wrapperBlck.title += '</br>' + contactRole + wrapperBlck.contact;
     }
     else{
      wrapperBlck.title = (blck.title__c != null) ? blck.title__c : '';
     } 
     lstBlockScheduleWrapper.add(wrapperBlck);
    }
    jSONString = JSON.serialize(lstBlockScheduleWrapper); 
    jSONString = jSONString.replaceAll('endDate', 'end');
   }
   else{
    
   }
   
  }
  
  //Method to convert the date in String format to use it in JSON
  public String convertDateTime(String dateTimeStartEnd,String dayofWeek){
    system.debug('dateTimeStartEnd>>>'+dateTimeStartEnd);
    system.debug('dayofWeek>>>'+dayofWeek);
    String dtStrtEnd = dateTimeStartEnd;
    if(dateTimeStartEnd.contains('PM') && !dateTimeStartEnd.contains('12')){
      dateTimeStartEnd = dateTimeStartEnd.substring(0,2);
      dtStrtEnd = dtStrtEnd.substring(3,5);
      Integer i = integer.valueOf(dateTimeStartEnd);
      i += 12;
      dateTimeStartEnd = String.valueOf(i);
    }
    else if(dateTimeStartEnd.contains('AM') && dateTimeStartEnd.contains('12')){
      dateTimeStartEnd = '00';
      dtStrtEnd = dtStrtEnd.substring(3,5);
    }
    else{
     dateTimeStartEnd = dateTimeStartEnd.substring(0,2);
     dtStrtEnd = dtStrtEnd.substring(3,5); 
    }
    DateTime currentDate = System.Now();
    String customerTimeZoneSidId = usr.TimeZoneSidKey;
    TimeZone customerTimeZone = TimeZone.getTimeZone(customerTimeZoneSidId);
    Integer offsetToCustomersTimeZone = customerTimeZone.getOffset(currentDate);
    Integer correctTimeInMin = offsetToCustomersTimeZone / (1000 * 60);
    Integer correctTimeInMin1 = offsetToCustomersTimeZone / (1000 * 60);
    if(correctTimeInMin1 < 0){
      correctTimeInMin =correctTimeInMin * -1;
    }
    Integer correctTimeInMinNew = math.mod(correctTimeInMin, 60);
    Integer correctTimeInHrNew = correctTimeInMin/60; 
    String correctTimeInMinNewStr = String.valueOf(correctTimeInMinNew);
    if(correctTimeInMinNew < 10){
      correctTimeInMinNewStr = '0'+correctTimeInMinNewStr;
    }
    String correctTimeInHrNewStr = String.valueOf(correctTimeInHrNew);
    String yr = String.valueOf(currentDate.year());
    String mth = String.valueOf(currentDate.month());
    if(mth.length() == 1){
      mth = '0' + mth;
    }
    String dt = String.valueOf(currentDate.day());
    String hr = dateTimeStartEnd;
    String convertedTime = '';
    if(integer.valueOf(correctTimeInHrNewStr) < 10){
      if(correctTimeInMin1 < 0){
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
      }
      else{
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        
      }
    }
    else{
      if(correctTimeInMin1 < 0){
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
      }
      else{
        if(dtStrtEnd.contains('30')){
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':30' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
        else{
          convertedTime = mapDays.get(dayofWeek)+'T'+dateTimeStartEnd + ':00' + ':00.000-0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
          timeZoneString = '+0' + correctTimeInHrNewStr + ':' + correctTimeInMinNewStr;
        }
      }
    }
    return convertedTime;
  }
  
  //Action Function method to save or update the records.
  public PageReference saveSchedule(){
    PageReference pgRef;
    String saveDtMon = saveDt.subString(1,4);
    String saveDtdt = saveDt.subString(5,7);
    String saveDtYr = saveDt.subString(8,12);
    DateTime convertedDateFromString;
    if(mapMonths.containsKey(saveDtMon)){
      saveDt = saveDtYr+'-'+mapMonths.get(saveDtMon)+'-'+saveDtdt;
      convertedDateFromString = Date.valueOf(saveDt);
      dayOfWeek = dateToDayMap.get(saveDt);
    }
    
    blckSchedule.dow__c = dayOfWeek;
    blckSchedule.starting_Time__c = strtDate;
    blckSchedule.Ending_Time__c = endDt;
    blckSchedule.title__c = (titl != null && titl != 'null' && titl != '') ? titl : '';
    blckSchedule.Account__c = accId;
    //system.debug('lstCon.get(0)>>>'+lstCon.get(0));
    if(lstCon.size()>0 && (assignToContactOrAccount == 'All' ||(assignToContactOrAccount == '' && assignToContactOrAccount == null))){
      blckSchedule.Contact__c = lstCon.get(0);
    }
    else{
      blckSchedule.Contact__c = assignToContactOrAccount;
    }
    try{
      if(blckRecId != null && blckRecId != ''){
        blckSchedule.Id = blckRecId;
        update blckSchedule;
      }
      else{
        insert blckSchedule;
      }
    }catch(Exception e){
    }
    if(assignToContactOrAccount == 'All' ||(assignToContactOrAccount == '' && assignToContactOrAccount == null)){
     //pgRef  = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact='+assignToContactOrAccount+'&selectedContactOrAccount='+lstCon.get(0));
      selectedContactOrAccount = lstCon.get(0);
      pgRef  = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact='+assignToContactOrAccount);
    }
    else if(selectedContactOrAccount != assignToContactOrAccount){
      selectedContactOrAccount=assignToContactOrAccount;
      pgRef = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact=All');
    }
    else{
      pgRef = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact='+assignToContactOrAccount+'&selectedContactOrAccount='+assignToContactOrAccount);
    }
    pgRef.setRedirect(true);
    return pgRef;
  }
  
  //Action Function method to delete records
  public PageReference deleteSchedule(){
    try{
      delete[SELECT Id 
             FROM Block_Schedule__c 
             WHERE Id =: blckRecId];
    }catch(Exception e){
    }
    PageReference pgRef = new PageReference('/apex/AccountSchedule?id='+accId);
    pgRef.setRedirect(true);
    return pgRef;
  }
  
  public map<String,String> currentDateMap(){
    Date dt = Date.Today();
    if(dt.day() <= 7){
        dt = dt.addDays(7);
    }else if(dt.day() > 20){
        dt = dt.addDays(-7);
    }
    datetoday = dt;
    Date strtWeek = dt.toStartOfWeek();
    Date strtWeekMon = strtWeek.addDays(1);
    Date strtWeekTue = strtWeek.addDays(2);
    Date strtWeekWed = strtWeek.addDays(3);
    Date strtWeekThur = strtWeek.addDays(4);
    Date strtWeekFri = strtWeek.addDays(5);
    Date endWeek = strtWeek.addDays(6);
    String strtWeekString = String.valueOf(strtWeek);
    String strtWeekStringMon = String.valueOf(strtWeekMon);
    String strtWeekStringTue = String.valueOf(strtWeekTue);
    String strtWeekStringWed = String.valueOf(strtWeekWed);
    String strtWeekStringThur = String.valueOf(strtWeekThur);
    String strtWeekStringFri = String.valueOf(strtWeekFri);
    String endWeekString = String.valueOf(endWeek);
    mapDays.put('Sunday', strtWeekString);
    mapDays.put('Monday', strtWeekStringMon);
    mapDays.put('Tuesday', strtWeekStringTue);
    mapDays.put('Wednesday', strtWeekStringWed);
    mapDays.put('Thursday', strtWeekStringThur);
    mapDays.put('Friday', strtWeekStringFri);
    mapDays.put('Saturday', endWeekString);
    dateToDayMap.put(strtWeekString,'Sunday');
    dateToDayMap.put(strtWeekStringMon,'Monday');
    dateToDayMap.put(strtWeekStringTue,'Tuesday');
    dateToDayMap.put(strtWeekStringWed,'Wednesday');
    dateToDayMap.put(strtWeekStringThur,'Thursday');
    dateToDayMap.put(strtWeekStringFri,'Friday');
    dateToDayMap.put(endWeekString,'Saturday');
    return mapDays;
  }

  public list<selectOption> getLstAssoContacts(){
    //create list of checkboxes
    list<selectOption> myOptions = new list<selectOption>();
    myOptions.add(new selectOption('All','All'));
    if(lstConAccAsso.size() > 0){
      for(Contact_Acct_Association__c conAcc : lstConAccAsso){
        myOptions.add(new selectOption(conAcc.Associated_Contact__c,conAcc.Associated_Contact__r.Name));
        lstCon.add(conAcc.Associated_Contact__c);
        system.debug('lstCon>>>'+lstCon);
      }
    }
    return myOptions;
  }
  
  public list<selectOption> getLstContacts(){
    //create list of checkboxes
    Set<id> conId = new Set<Id>();
    list<selectOption> myOptions = new list<selectOption>();
    if(lstConAccAsso.size() > 0){
      for(Contact_Acct_Association__c conAcc : lstConAccAsso){
        myOptions.add(new selectOption(conAcc.Associated_Contact__c,conAcc.Associated_Contact__r.Name));
        conId.add(conAcc.Associated_Contact__c);
      }
    }
    system.debug('myOptions>>>'+myOptions);
    system.debug('myOptions>>>'+myOptions.size());
    List<Contact>  conList = new List<Contact>();
    conList = [SELECT Id, Timezone__c, Personnel__c FROM Contact WHERE Id IN :conId];
    for(Contact c : conList){
      contactToTimezoneMap.put(c.Id,c.Timezone__c);
      contactToRoleMap.put(c.Id,c.Personnel__c);
    }
    //setInitialDates();
    system.debug('contactToRoleMap>>>'+contactToRoleMap);
    return myOptions;
  }
  public String mapParentChildrenJSON {get{return JSON.serialize(contactToTimezoneMap);}}
  public String mapContactRoleJSON {get{return JSON.serialize(contactToRoleMap);}}
  //Method to show Contacts Scheduled for a Account on Calendar
  public PageReference showSchAccounts(){
    /*
    if(selectedContactOrAccount!='All'){
      cont = [SELECT Id, Name FROM Contact WHERE Id =: selectedContactOrAccount];
      contactName = cont.Name;
    }
    assignToContactOrAccount = selectedContactOrAccount;
    if(selectedContactOrAccount == 'All'){
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id,
                    Contact__r.Name, Contact__r.Id
                    FROM Block_Schedule__c 
                    WHERE Account__c =: accId];
      if(lstBlckSch.size() == 0){
        Block_Schedule__c blck = new Block_Schedule__c();
        blck = createBlockScheduleData(blck);
        lstBlckSch.add(blck);
      }
      convertToJSON(lstBlckSch);
    }
    else{
      lstBlckSch = [SELECT Id, Contact__c, Ending_Time__c, starting_Time__c, title__c, dow__c, Account__r.Name, Account__c, Account__r.Id,
                    Contact__r.Name, Contact__r.Id
                    FROM Block_Schedule__c 
                    WHERE Account__c =: accId AND Contact__c =: selectedContactOrAccount];
      if(lstBlckSch.size() == 0){
        Block_Schedule__c blck = new Block_Schedule__c();
        blck = createBlockScheduleData(blck);
        lstBlckSch.add(blck);
      }
      convertToJSON(lstBlckSch);
    } */
   PageReference pgRef; // ||(assignToContactOrAccount == '' && assignToContactOrAccount == null)
   if(selectedContactOrAccount == 'All'){
     //pgRef  = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact='+assignToContactOrAccount+'&selectedContactOrAccount='+lstCon.get(0));
      selectedContactOrAccount = lstCon.get(0);
      pgRef  = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact='+selectedContactOrAccount);
    }
  /*  else if(selectedContactOrAccount != assignToContactOrAccount){
      selectedContactOrAccount=assignToContactOrAccount;
      pgRef = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact=All');
    } */
    else{
      pgRef = new PageReference('/apex/AccountSchedule?id='+accId+'&prevContact='+selectedContactOrAccount+'&selectedContactOrAccount='+selectedContactOrAccount);
    }
    pgRef.setRedirect(true);
    return pgRef;
  }
  
  //Blank Method to get Selected Contact Value
  public void setInitialDates(){
    
  }
  
  //Method to create dummy data if a new account is created
  public Block_Schedule__c createBlockScheduleData(Block_Schedule__c blck){
    blck.starting_Time__c = '02:00PM';
    blck.Ending_Time__c = '01:00PM';
    blck.title__c = 'test';
    blck.dow__c = 'Sunday';
    return blck;
  }
  
  //Wrapper Class to create JSON
  public class BlockScheduleWrapper{
  public String contact;
  public String contactid;
  public String id;
  public String start;
  public String endDate;
  public String title;
 }
}