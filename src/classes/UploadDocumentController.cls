// (c) 2016 Appirio, Inc.
//
// Description Controller for UploadDocumentPage (Migrated from Comm Source)
//
// Created On 04/13/2016   Deepti Maheshwari Reference (T-485167) 
// Updated On 04/18/2016   Deepti Maheshwari Reference (Ref : Chatter changes T-485167) 
// Updated on 05/06/2016   Deepti Maheshwari Reference (Ref : I-216824)
//
public class UploadDocumentController { 

  public Boolean linkToAccount {get;set;}
  private Opportunity opp {get;set;}
  private BigMachines__Quote__c quoteObj {get;set;}
  public String fileName {get;set;}
  transient public Blob fileBody {get;set;}
  
  
  public Document__c documentObj{get;set;}
  public UploadDocumentController(ApexPages.StandardController controller) { 
    //if(controller.getRecord().ID.getSobjectType().getDescribe().getName()
    //  == 'Opportunity'){
    if(controller.getRecord() instanceof Opportunity) {
      this.opp = (Opportunity)controller.getRecord();
    }else {
      this.quoteObj = (BigMachines__Quote__c)controller.getRecord();
    }
    Map<String,Schema.RecordTypeInfo> docTypeRTMap = Schema.SObjectType.Document__c.getRecordtypeInfosByName();
    documentObj = new Document__c();
    documentObj.RecordTypeId = docTypeRTMap.get(Constants.OPPTY_DOCUMENT_RT).getRecordTypeId();
    documentObj.Link_to_Account__c = true;
  }
  
  // creates a new Document__c record
  private Database.SaveResult saveCustomDocument() {
    //Document__c obj = new Document__c();
    if(opp != null){
      documentObj.Opportunity__c = opp.Id; 
      documentObj.Account__c = documentObj.Link_to_Account__c == true ? opp.AccountId : null;
    }else{
      documentObj.Oracle_Quote__c = quoteObj.Id;
      //Ref I-216824 Removed association of quote documents with opportunity
      /*List<BigMachines__Quote__c> quotes = [SELECT BigMachines__Is_Primary__c,
                                          BigMachines__Opportunity__c, ID 
                                          FROM BigMachines__Quote__c
                                          WHERE id = :quoteObj.Id];
      if(quotes != null && quotes.size() > 0){
        if(quotes.get(0).BigMachines__Is_Primary__c){
          documentObj.Opportunity__c = quotes.get(0).BigMachines__Opportunity__c;
        }
      }*/
    }
    return Database.insert(documentObj);
  }
  
  // create an actual Document record with the Document__c as parent
  private Database.SaveResult saveStandardDocument(Id parentId) {
    Database.SaveResult result;
    try{
    
    Attachment doc = new Attachment();
    
    doc.body = this.fileBody;
    doc.name = this.fileName;
    doc.parentId = parentId;
    // insert the document
    result = Database.insert(doc);
    // reset the file for the view state
    fileBody = Blob.valueOf(' ');
    }catch(exception ex){
       ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
          Constants.Upload_Doc)); 
    }
    return result;
  }
  
  /**
  * Upload process is:
  *  1. Insert new Document__c record
  *  2. Insert new Attachment with the new Document__c record as parent
  *  3. Update the Document__c record with the ID of the new Attachment
  **/
  public PageReference processUpload() {
    Map<String,Schema.RecordTypeInfo> docTypeRTMap = Schema.SObjectType.Document__c.getRecordtypeInfosByName();
    System.Savepoint sp = Database.setSavepoint();

    try {
      Database.SaveResult customDocumentResult = saveCustomDocument();
      system.debug('Hello'  + customDocumentResult);
      
      if (customDocumentResult == null || !customDocumentResult.isSuccess()) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
          'Could not save document.'));
        Database.rollback( sp );
        documentObj = new Document__c();
        documentObj.RecordTypeId = docTypeRTMap.get(Constants.OPPTY_DOCUMENT_RT).getRecordTypeId();
        documentObj.Link_to_Account__c = true;
        return null;
      }
      system.debug('Heeee');
      Database.SaveResult documentResult = saveStandardDocument(customDocumentResult.getId());
      system.debug('HeeeeIIII' + documentResult);
      if (documentResult == null || !documentResult.isSuccess()) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
          'Could not save document.')); 
        Database.rollback( sp );
        documentObj = new Document__c();
        documentObj.RecordTypeId = docTypeRTMap.get(Constants.OPPTY_DOCUMENT_RT).getRecordTypeId();
        documentObj.Link_to_Account__c = true;
        return null;
      } else {
        
      // update the custom document record with some document info
      Document__c customDocument = [select id,name from Document__c where id = :customDocumentResult.getId()];
      //COMM - Changed the name as part of T-485167
      if(customDocument.name == null){
        customDocument.name = this.fileName;
      }
      customDocument.DocumentId__c = documentResult.getId();
      update customDocument;
      }
    
    } catch (Exception e) {
      Database.rollback(sp);
        //Indicates that the record is locked for approval process and
        if(opp!=  null && opp.Id != null && Approval.isLocked(opp.Id)){
        	ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.Record_Locked_Error));
        }else{
          ApexPages.AddMessages(e);
        }
        return null;
    }  
    if(opp != null){
        return new PageReference('/'+opp.Id);
    }else{
        return new PageReference('/'+quoteObj.Id);
    }
  }
  
  public PageReference back() {
    if(opp != null){
        return new PageReference('/'+opp.Id);
    }else{
        return new PageReference('/'+quoteObj.Id);
    }
  }   
}