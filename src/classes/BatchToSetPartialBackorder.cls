/*
Author - Jyoti Singh (JDC)
Story -  S-389737
Usage - To set PartialBackorder field of order if any orderitem is Backordered
*/
global class BatchToSetPartialBackorder implements Database.Batchable<sObject>{
    global String query;
    global static final String BACKORDERED    = 'Backordered';
    global BatchToSetPartialBackorder(){        
        
    }
    
    global BatchToSetPartialBackorder(String q){        
        query = q;
    }
    
    global Database.QueryLocator start(Database.batchableContext bc){  
        if(query != '')
            query = 'Select Id, Status__c, OrderId, Order.Status,Order.PartialBackOrder__c From OrderItem where Status__c =: '+BACKORDERED+' and Order.PartialBackOrder__c !=: '+BACKORDERED+'  order by OrderId';
        system.debug('**query**'+query); 
        return Database.getQueryLocator(query);                                                                                   
    }

    global void execute(Database.BatchableContext bc, List<OrderItem> orderItemList){
        Set<Id> orderId = new Set<Id>();
        List<Order> updateOrderList = new List<Order>();
        for(OrderItem oi: orderItemList){
            orderId.add(oi.OrderId);
        }
        
        for(Order o: [Select Id,PartialBackOrder__c from Order where Id in : orderId]){
            o.PartialBackOrder__c = BACKORDERED;
            updateOrderList.add(o);
        }
        if(!updateOrderList.isEmpty()){
            update updateOrderList;
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}