/**================================================================      
* Appirio, Inc
* Name: COMM_SendRQAToTrackwise
* Description: This class is used to call web service and set update case.
* Created Date: 21/09/2016
* Created By: Isha Shukla
* 
* 21 Sep 2016        Isha Shukla         Class creation. T-539444
==================================================================*/
global class COMM_SendRQAToTrackwise {
    webservice static void callWebService(Id caseId, String twStatus) {
        List<Regulatory_Question_Answer__c> lstRQA = [SELECT Id FROM Regulatory_Question_Answer__c WHERE Case__c = :caseId];
        
        if(lstRQA.size() > 0){
            Case objCase = new Case(Id = caseId);
            objCase.Trackwise_Status__c = 'Sent for Trackwise';
            update objCase;
            callWebserviceTrackwise(lstRQA.get(0).Id);         
        }    
    }
    
    @Future(callout=true)
    private static void callWebserviceTrackwise(Id rqaId){
        Medical_TW_Utility.invokeTrackWiseWebService(new Set<Id>{rqaId});
    }
}