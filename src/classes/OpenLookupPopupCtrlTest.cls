/*
//
// (c) 2015 Appirio, Inc.
//
// OpenLookupPopupCtrlTest
//
// 02 Oct 2015 , Megha (Appirio)
// This is test class for OpenLookupPopupCtrl
*/
@isTest
private class OpenLookupPopupCtrlTest {

	private static List<Account> accounts;
	private static List<Contact> contacts;
	@isTest
	private static void testLookupResult() {
    createTestData();
    ApexPages.currentPage().getParameters().put('Object' , 'Contact_Acct_Association__c');
    ApexPages.currentPage().getParameters().put('fieldName' , 'Associated_Contact__c');
    ApexPages.currentPage().getParameters().put('fieldSetName' , 'Account_Contact_Association_Search');
    ApexPages.currentPage().getParameters().put('pField' , 'Associated_Account__c');
    ApexPages.currentPage().getParameters().put('isEqualSearch' , 'true');
    ApexPages.currentPage().getParameters().put('equalText' , contacts.get(0).id);
    ApexPages.currentPage().getParameters().put('currentContactId' , contacts.get(0).id);
    OpenLookupPopupCtrl openLookupCtrl = new OpenLookupPopupCtrl();
    //System.assert(openLookupCtrl.listRecords.size() > 0 , 'Actual Size : ' + openLookupCtrl.listRecords.size() );
    
    openLookupCtrl.searchText = 'Test';
    openLookupCtrl.runSearch();
  }
  static testMethod void testLookupResult1() {
    createTestData();
    ApexPages.currentPage().getParameters().put('Object' , 'Account');
    ApexPages.currentPage().getParameters().put('fieldName' , 'Name');
    //ApexPages.currentPage().getParameters().put('fieldSetName' , 'Account_Contact_Association_Search');
    //ApexPages.currentPage().getParameters().put('pField' , 'Account');
    ApexPages.currentPage().getParameters().put('isEqualSearch' , 'false');
    ApexPages.currentPage().getParameters().put('currentContactId' , contacts.get(0).id);
    ApexPages.currentPage().getParameters().put('showAllAccounts' , 'yes');
    
    //ApexPages.currentPage().getParameters().put('equalText' , contacts.get(0).id);
    OpenLookupPopupCtrl openLookupCtrl = new OpenLookupPopupCtrl();
    //System.assert(openLookupCtrl.listRecords.size() > 0 , 'Actual Size : ' + openLookupCtrl.listRecords.size() );
    openLookupCtrl.selectedObject = 'Order';
    openLookupCtrl.searchText = '*T';
    try{
      openLookupCtrl.runSearch();
    }catch(Exception e){
    	System.assert(e.getMessage() != null , 'Wrong Field error');
    }
  }

  private static void createTestData(){
    accounts = Medical_TestUtils.createAccount(4, true);
    contacts = Medical_TestUtils.createContact(2, accounts.get(0).id , true);
    
  }
}