/**================================================================      
* Appirio, Inc
* Name: [COMM_FSITriggerHandler]
* Description: [Trigger handler for Field Investingation object]
* Created Date: [22-Sept-2016]
* Created By: [Gaurav Sinha] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 22  Sept 2016      Gaurav Sinha     T-539147
* 07  Nov  2016      Varun Vasishtha  I-239570 PopulateMultipleVisitRequired() populates Multiple_Visits_Required__c on Workorder
==================================================================*/
public with sharing class COMM_FSITriggerHandler {
    static map<String,List<String>> mapProfileToFieldEdit = new map<String,List<String>>();
    static{
        List<String> localFields = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.Field_Service_Investigation__c.FieldSets.Un_Editable_status_on_Reopened_status_FS.getFields()) {
            localFields.add(f.getFieldPath());
        }

        mapProfileToFieldEdit.put('Field Service Technician - COMM US',localFields);
        mapProfileToFieldEdit.put('Field Service Technician Manager - COMM US',localFields);
        localFields = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.Field_Service_Investigation__c.FieldSets.Un_Editable_field_on_Reopened_Status.getFields()) {
            localFields.add(f.getFieldPath());
        }
        mapProfileToFieldEdit.put('Technical Field Service - COMM Intl',localFields);
        mapProfileToFieldEdit.put('Technical Support - COMM',localFields);
        mapProfileToFieldEdit.put('Customer Service - COMM US',localFields);
        mapProfileToFieldEdit.put('Customer Service - COMM INT',localFields);
        mapProfileToFieldEdit.put('System Administrator',localFields);        
    }
    
    public static void onBeforeUpdate(List<Field_Service_Investigation__c> newList,Map<id,Field_Service_Investigation__c> oldMap){
        validateFieldEditingCapability(newList,oldMap);
    }
    
    public static void onAfterInsert(List<Field_Service_Investigation__c> newList){
        PopulateMultipleVisitRequired(newList,null);
    }
    
    public static void onAfterUpdate(List<Field_Service_Investigation__c> newList,Map<id,Field_Service_Investigation__c> oldMap){
        PopulateMultipleVisitRequired(newList,oldMap);
    }
    
    private static void validateFieldEditingCapability(List<Field_Service_Investigation__c> newList,Map<id,Field_Service_Investigation__c> oldMap){
        Set<id> workOrderID = new Set<Id>();
        for(Field_Service_Investigation__c varLoop: newList){
            workOrderid.add(varloop.Work_Order__c);
        }
        // Validate the Reopened Work order only
        map<id,SVMXC__Service_Order__c> mapWO = new Map<Id,SVMXC__Service_Order__c>([SELECT Id
                                                                                    FROM SVMXC__Service_Order__c
                                                                                    WHERE Id IN : workorderid
                                                                                        AND SVMXC__Order_Status__c ='Reopened']);
        System.debug('==>'+mapWO);
        String varProfileName = [select name from profile where id=: userinfo.getprofileId()][0].name;
        System.debug('==>'+mapProfileToFieldEdit.get(varProfileName));
        if(mapProfileToFieldEdit.get(varProfileName)!=null){

            for(Field_Service_Investigation__c varLoop: newList){
                System.debug('==>'+mapWO.containsKey(varloop.Work_Order__c));
                if(mapWO.containsKey(varloop.Work_Order__c)){
                    for(String s: mapProfileToFieldEdit.get(varProfileName)){
                         System.debug('========>fieldname==>'+s);
                        System.debug('========>'+varloop.get(s));
                        System.debug('OLD ========>'+oldMap.get(varloop.id).get(s));
                        if(varloop.get(s) != oldMap.get(varloop.id).get(s)){
                            varloop.addError('Cannot change the Field in Reopened work order status');
                        }
                    }
                }
            }
        }
    }
    
    // I-239570 PopulateMultipleVisitRequired() populates Multiple_Visits_Required__c on Workorder(Varun Vasishtha)
    private static void PopulateMultipleVisitRequired(List<Field_Service_Investigation__c> newList,Map<id,Field_Service_Investigation__c> oldMap){
        List<SVMXC__Service_Order__c> listToUpdate = new List<SVMXC__Service_Order__c>();
        for(Field_Service_Investigation__c item : newList){
            Field_Service_Investigation__c oldItem;
            if(oldMap != null){
                oldItem = oldMap.get(item.Id);
            }
            if(item.Work_Order__c != null 
                    && string.isNotEmpty(item.Return_Visit_Required__c) 
                    && (oldItem == null || oldItem.Return_Visit_Required__c != item.Return_Visit_Required__c)){
                SVMXC__Service_Order__c updateItem = new SVMXC__Service_Order__c(id = item.Work_Order__c);
                updateItem.Multiple_Visits_Required__c = item.Return_Visit_Required__c;
                listToUpdate.add(updateItem);
            }
        }
        if(listToUpdate.size() > 0){
            update listToUpdate;
        }
    }
}