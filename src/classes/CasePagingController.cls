public with sharing class CasePagingController{
    
    public Id nextCaseId {get;set;}
    public Id prevCaseId {get;set;}
    public Boolean hasNext {get;set;}
    public Boolean hasPrev {get;set;}  
    
    public CasePagingController(ApexPages.StandardController controller){
        
        hasNext = False;
        hasPrev = False;
        Case currentCaseRec = (Case)controller.getRecord();
        String currentNumber = currentCaseRec.CaseNumber;
        Id ownerId =  UserInfo.getUserId();
        List<Case> isOpenCase = [SELECT Id, CaseNumber, OwnerId FROM Case WHERE CaseNumber = :currentNumber  AND    OwnerId = :ownerId  AND (NOT Status LIKE 'Closed%')];
        List<Case> myNextCaseList = [SELECT Id, CaseNumber, OwnerId FROM Case WHERE CaseNumber > :currentNumber  AND    OwnerId = :ownerId  AND (NOT Status LIKE 'Closed%') ORDER BY CaseNumber ASC LIMIT 1];
        List<Case> myPrevCaseList = [SELECT Id, CaseNumber, OwnerId FROM Case WHERE CaseNumber < :currentNumber  AND    OwnerId = :ownerId  AND (NOT Status LIKE 'Closed%') ORDER BY CaseNumber DESC LIMIT 1];
        
        if (!myNextCaseList.Isempty()) {
            nextCaseId = myNextCaseList.get(0).Id;
            if(!isOpenCase.Isempty()){
                hasNext = True;
            }
        }
        if (!myPrevCaseList.Isempty()) {
            prevCaseId = myPrevCaseList.get(0).Id;
            if(!isOpenCase.Isempty()){
                hasPrev = True;
            }
        }
        
        //this.currentCaseRe
    }
    
   // public Page
   // 
    
}