/*
Name                    - AttachmentTriggerHandlerTest
Created By              - Nitish Bansal
Created Date            - 02/26/2016
Purpose                 - T-459651
*/

@isTest
private class COMM_AttachmentTriggerHandlerTest
{
    @testSetup
    static void setupTestData(){
        //Creating a System Admin User
        List<User> userList = TestUtils.createUser(1, 'System Administrator', true);
        List<Document__c> oppDocList = new List<Document__c>() ;
        Document__c oppDoc = new Document__c();
        
        //Creating an account, opprtunity and multile documents as the Admin user
        system.runAs(userList.get(0)){
            List<Account> accountList = TestUtils.createAccount(1, true);
            TestUtils.createCommConstantSetting();
            List<Opportunity> opportunityList = TestUtils.createOpportunity(1, accountList.get(0).Id, true);
            for(Integer i = 0; i <= 5; i++){
                oppDoc = new Document__c();
                oppDoc.Name = 'Test Opportunity Document' + String.valueOf(i);
                oppDoc.Opportunity__c = opportunityList.get(0).Id;
                oppDoc.Account__c = accountList.get(0).Id;
                oppDoc.Document_type__c = Constants.RESTRICTED_DOCUMENT_TYPE;
                oppDocList.add(oppDoc);
            }
            insert oppDocList;
        }   
    }

    //Method to test parent restricted Document__c is updated
    @isTest
    static void itShouldUpdateParentDocs()
    {
        setupTestData();
        User testUser = [select id from User limit 1];
        List<Attachment> attchmntList = new List<Attachment>() ;
        
        system.runAs(testUser){
            List<Document__c> oppDocList = [Select Id, Name, Opportunity__c, Account__c, Document_type__c, DocumentId__c from Document__c];
            for(Document__c opptyDoc : oppDocList){
                system.assertEquals(null, opptyDoc.DocumentId__c);
            }
            for(Document__c opptyDoc : oppDocList){
                attchmntList.add(TestUtils.addAttachmentToParent(opptyDoc.Id));
            }
            insert attchmntList;
            
            for(Document__c opptyDoc : [Select Id, Name, Opportunity__c, Account__c, Document_type__c, DocumentId__c from Document__c]){
                system.assertNotEquals(null, opptyDoc.DocumentId__c);
            }
        }   

    }

    //Method to test parent attachment is not deleted.
    @isTest
    static void itShouldPreventDeletion()
    {
        setupTestData();
        User testUser = [select id from User limit 1];
        List<Attachment> attchmntList = new List<Attachment>() ;
        //TestUtils.createCommConstantSetting();
        system.runAs(testUser){
            List<Document__c> oppDocList = [Select Id, Name, Opportunity__c, Account__c, Document_type__c, DocumentId__c from Document__c];
            for(Document__c opptyDoc : oppDocList){
                attchmntList.add(TestUtils.addAttachmentToParent(opptyDoc.Id));
            }
            insert attchmntList;

            //deleting multiple records (logged-in user has no role)
            try{
                delete attchmntList;
            } catch(Exception e){
                Boolean messagefound = false; 
                //Checking that user gets excepted error message
                if(e.getMessage().contains(System.Label.Please_contact_an_Administrator_to_delete_Combined_Proposals)){
                    messagefound = true;
                }
                system.assertEquals(true, messagefound);
            }
        }   
    }
}