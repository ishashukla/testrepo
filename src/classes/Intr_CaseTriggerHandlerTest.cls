/*******************************************************************************
 Author        :  Appirio JDC (Kajal Jalan)
 Date          :  May 05, 2016
 Purpose       :  Test Class For Intr_CaseTriggerHandler (T-500173)
*******************************************************************************/
@istest

public class Intr_CaseTriggerHandlerTest {
	
	static list<Case> CaseList;
	
	//To notify the value of LastStatus Change
    public static testMethod void notifyLastStatusChange(){
    	
    	//Inserting test data
    	createTestData();
    	
        Test.startTest();
        	
        	insert CaseList;
	        
	    Test.stopTest();
        
        CaseList = [SELECT Id, Last_Status_Change__c FROM Case WHERE Id IN :CaseList];
        // Verify Last Status Change is populating
        System.assertEquals((CaseList[0].Last_Status_Change__c).Date(), (system.now()).date());
        
        
        
    }
    
    //To create sample data for case
    public static void createTestData(){
    	    
    	    // Inserting test account
			Account account = Intr_TestUtils.createAccount(1, false).get(0);
			account.Name = 'test';
			account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_ACCOUNT_RECORD_TYPE).getRecordTypeId();
			insert account;
			
			//Inserting test contact
			Contact contact = Intr_TestUtils.createContact(1, account.Id, false).get(0);
			contact.RecordTypeId =   Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CONTACT_RECORD_TYPE).getRecordTypeId();
			contact.FirstName = 'test';
			contact.LastName = 'Jalan';
        	contact.Timezone__c = '(GMT–04:00) Eastern Daylight Time (America/New_York)';
			insert contact;
			
			Intr_TestUtils.createContactJunction(1, account.Id, contact.Id, true).get(0);
			Id caseNonProductRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Intr_Constants.INSTRUMENTS_CASE_RECORD_TYPE_NON_PRODUCT).getRecordTypeId();
			TestUtils.createRTMapCS(caseNonProductRT, 'Instruments - Non Product Issue', 'Instruments');
			CaseList = new list<Case>();
			
			// Creating list of cases for testing bulk insert
			for(integer i=0;i<2;i++){
				
				Case objCase = Intr_TestUtils.createCase(1, account.Id, contact.Id, false).get(0);
				objCase.RecordTypeId = caseNonProductRT;
				objCase.Origin = 'Phone'+i;
        		objCase.Sub_Type__c = 'other'+i;
        		objCase.Description = 'test'+i;
        		objCase.Status = 'new'+i;
				
				CaseList.add(objCase);
				
			}
    	
    }
}