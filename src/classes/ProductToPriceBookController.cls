public with sharing class ProductToPriceBookController {
	private final Product2 product;
	public List<PricebookEntry> pbe {get;set;}
	public String noValue = '';
	public ProductToPriceBookController(ApexPages.StandardController sc) {
		this.product = (Product2)sc.getRecord();

		//Get user's divisions
		Map<Id,String> userDivi = DoubleBaggerUtility.getUsersBusinessUnits( new Set<Id>{UserInfo.getUserId()} );
		List<String> divisions = new List<String>();
		List<String> users = new List<String>();
		for(String u : userDivi.keySet()){
		    users.add(u);
		}
		divisions = userDivi.get(users[0]).split(';');
		Set<String> divSet = new Set<String>();
		for(String d : divisions){
		    divSet.add(d);
		}
		//Get user's divisions

		//Get pricebook map
		Map<String, String> mapOfBUAndPriceBookName = new Map<String, String> ();
    for(BusinessUnitAndPricebookName__c custSet : [SELECT Id, Name, Price_Book_Name__c
                                                    FROM BusinessUnitAndPricebookName__c]) {
        mapOfBUAndPriceBookName.put(custSet.Price_Book_Name__c ,custSet.Name);
    }
    //Get pricebook map

		pbe = [SELECT Id, Name, Pricebook2.Id, Pricebook2.Name, isPriceVisible__c, Division__c, Pricebook2Id, Product2Id, UnitPrice, IsActive, 
					UseStandardPrice, ProductCode, IsDeleted, Data_Source__c from PricebookEntry where 
					Product2Id =: product.id AND IsDeleted = false AND IsActive = true AND Pricebook2.Name != 'Standard Price Book'];
		for(PricebookEntry p : pbe){
			p.Division__c = mapOfBUAndPriceBookName.get(p.Pricebook2.Name);
			if( divSet.contains( mapOfBUAndPriceBookName.get(p.Pricebook2.Name) ) ){
				p.isPriceVisible__c = true;
			}
		}
	}
}