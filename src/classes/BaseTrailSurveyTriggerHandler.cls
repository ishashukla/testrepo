/*************************************************************************************************
Created By:     Sahil Batra
Date:           Jan 18, 2016
Description:    Handler class for BaseTrailSurvey Trigger
Sahil Batra 	Feb 15,2016 Modified (T-476380)
Isha Shukla     July 1,2016 Modified (T-516161) 
**************************************************************************************************/ 
public class BaseTrailSurveyTriggerHandler {
	public static Id recordTypeInstrument = null;
	static{
	  Schema.RecordTypeInfo rtByNameInstrument = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type');
      recordTypeInstrument = rtByNameInstrument.getRecordTypeId();
	}
	public static List<String> monthString = new List<String>{'','January','February','March','April','May','June','July','August','September','October','November','December'};
	public static Map<String, Integer> monthMap = new Map<String, Integer>{'January' => 12, 'February' => 11, 'March' => 10, 'April' => 9, 'May' => 8, 'June' => 7, 'July' => 6, 'August' => 5, 'September' => 4, 'October' => 3, 'November' => 2, 'December' => 1};
/*	public static void updateMonthlyForecast(Map<Id, Base_Trail_Survey__c> mapOld,List<Base_Trail_Survey__c> newList) {
		Set<Id> opportunityIds = new Set<Id>();
		for(Base_Trail_Survey__c survey : newList){
			if(mapOld == null ||(mapOld.get(survey.Id).Roll_Up_Total__c != survey.Roll_Up_Total__c 
								  || mapOld.get(survey.Id).Months_Remaining__c != survey.Months_Remaining__c 
								  || mapOld.get(survey.Id).Trail_Total__c != survey.Trail_Total__c 
								  || mapOld.get(survey.Id).Opportunity__c != survey.Opportunity__c 
								 // || mapOld.get(survey.Id).Owner__c != survey.Owner__c
								  || mapOld.get(survey.Id).Business_Unit__c != survey.Business_Unit__c)){
				opportunityIds.add(survey.Opportunity__c);
			}
		}
			if(opportunityIds.size() > 0){
		//		getOpportunities(opportunityIds);
			}
	}
	public static void getOpportunities(Set<Id> opportunityIds){
		Set<String> userIds = new Set<String>();
        Set<String> oppDateSet = new Set<String>();
        Set<String> BUSet = new Set<String>();
		List<Opportunity> oppList = new List<Opportunity>();
    	oppList = [SELECT Name, StageName,Business_Unit__c,ForecastCategoryName, OwnerId, CloseDate, Id, Close_Date_Month_Year__c, Amount 
            	   FROM Opportunity
                   WHERE Id IN :opportunityIds];
             		if(oppList.size() > 0){
             			for(Opportunity opp : oppList){
             				for(String mon : monthString){
             					if(mon!=''){
             						oppDateSet.add(mon+'-'+opp.CloseDate.year());
             					}
             				}
             				userIds.add(opp.OwnerId);
             				BUSet.add(opp.Business_Unit__c);
             			}
             		}
             if(oppDateSet.size() > 0 && userIds.size() > 0){
			 	updateMonthlyForecast(userIds,oppDateSet,BUSet);
      		}
	}
	public static void updateMonthlyForecast(Set<String> userIds, Set<String> oppDateSet,Set<String> BUSet){
			 Schema.RecordTypeInfo rtByNameRep = Schema.SObjectType.Forecast_Year__c.getRecordTypeInfosByName().get('Rep Forecast');
			 Id recordTypeRep = rtByNameRep.getRecordTypeId();
			 Set<String> finalOpportunityId = new Set<String>();
		     List<Opportunity> finaloppList = new List<Opportunity>();
             Set<String> yearSet = new Set<String>();
             Set<String> userSet = new Set<String>();
             finaloppList = [SELECT Name, StageName,Business_Unit__c, ForecastCategoryName, OwnerId, CloseDate, Id, Close_Date_Month_Year__c, Amount 
                   			 FROM Opportunity
                   			 WHERE OwnerId IN:userIds 
				   			 AND Close_Date_Month_Year__c IN:oppDateSet
				   			 AND Business_Unit__c IN:BUSet];
             for(String yearString : oppDateSet){
             	yearSet.add(yearString.substring(yearString.length()-4));
             }
             for(Opportunity opp : finaloppList){
             	finalOpportunityId.add(opp.Id);
             }
             System.debug('>>>>>>>>>>2>>>>>>>'+yearSet);
             System.debug('>>>>>>>>>>3>>>>>>>'+userSet);
             System.debug('>>>>>>>>>>4>>>>>>>'+oppDateSet);
             System.debug('>>>>>>>>>>5>>>>>>>'+userSet);
             System.debug('>>>>>>>>>>6>>>>>>>'+finalOpportunityId);
             List<Base_Trail_Survey__c> surveyList = new List<Base_Trail_Survey__c>();
             surveyList = [SELECT Trail_Total__c, Roll_Up_Total__c, Opportunity__r.Close_Date_Month_Year__c, 
             			   Opportunity__r.CloseDate, Opportunity__c,Opportunity__r.OwnerId,Opportunity__r.ForecastCategoryName,
             			   Opportunity__r.StageName, Months_Remaining__c, Id,Business_Unit__c,Opportunity__r.Service_Monthly__c  
             			   FROM Base_Trail_Survey__c 
             			   WHERE Opportunity__c IN :finalOpportunityId];
            	Map<String,List<Base_Trail_Survey__c>> UserYeartoBaseTrail = new Map<String,List<Base_Trail_Survey__c>>();
            	List<Monthly_Forecast__c> monthForecastListToUpdate = new List<Monthly_Forecast__c>();
       if(surveyList.size() > 0){      
             for(Base_Trail_Survey__c survey : surveyList){
             		String key = survey.Opportunity__r.OwnerId+'-'+survey.Opportunity__r.CloseDate.year()+'-'+survey.Business_Unit__c;
             		if(!UserYeartoBaseTrail.containsKey(key)){
             			UserYeartoBaseTrail.put(key,new List<Base_Trail_Survey__c>());
             		}
             		UserYeartoBaseTrail.get(key).add(survey);
             }
             System.debug('>>>>>>>>>>7>>>>>>>'+UserYeartoBaseTrail);
             List<Monthly_Forecast__c> monthForecastList = new List<Monthly_Forecast__c>();
			 Map<String,List<Monthly_Forecast__c>> YearUsettoMontlyForecastList = new Map<String,List<Monthly_Forecast__c>>();
			 monthForecastList = [Select Month__c, Id,Trail_Forecast_Total__c, Forecast_Year__r.User2__c, Forecast_Year__r.Year__c,
								 Forecast_Year__c, Capital_Forecast__c ,Business_Unit__c
								 FROM Monthly_Forecast__c
								 WHERE Forecast_Year__r.User2__c IN:userIds
								 AND Forecast_Year__r.Year__c IN:yearSet
								 AND Forecast_Record_Type_Id__c =:recordTypeRep
								 AND Business_Unit__c IN:BUSet];//T-476380 changes -Added record type check
			for(Monthly_Forecast__c mf : monthForecastList){
				String key = mf.Forecast_Year__r.User2__c+'-'+mf.Forecast_Year__r.Year__c+'-'+mf.Business_Unit__c;
				if(!YearUsettoMontlyForecastList.containsKey(key)){
					YearUsettoMontlyForecastList.put(key,new List<Monthly_Forecast__c>());
				}
				YearUsettoMontlyForecastList.get(key).add(mf);
			}
			System.debug('>>>>>>>>>>8>>>>>>>'+UserYeartoBaseTrail);
			for(String monthYearForecast : YearUsettoMontlyForecastList.keySet()){
				List<Monthly_Forecast__c> tempMf = YearUsettoMontlyForecastList.get(monthYearForecast);
				List<Base_Trail_Survey__c> tempBaseTrail = new List<Base_Trail_Survey__c>();
				if(UserYeartoBaseTrail.containsKey(monthYearForecast)){
					tempBaseTrail = UserYeartoBaseTrail.get(monthYearForecast);
				}
					for(Monthly_Forecast__c mf : tempMf){
						mf.Trail_Forecast_Total__c = 0;
						if(tempBaseTrail.size() > 0){
						boolean resetVal = false;
						Integer numOfMonths = monthMap.get(mf.Month__c);
						for(Base_Trail_Survey__c bs : tempBaseTrail){
							if(bs.Opportunity__r.ForecastCategoryName != 'Omitted' && bs.Opportunity__r.StageName!='Closed Lost'){
							  if(bs.Months_Remaining__c >= numOfMonths){
								mf.Trail_Forecast_Total__c = mf.Trail_Forecast_Total__c + bs.Roll_Up_Total__c;
								if(bs.Opportunity__r.Service_Monthly__c != null){
									mf.Trail_Forecast_Total__c = mf.Trail_Forecast_Total__c + bs.Opportunity__r.Service_Monthly__c;
								}
							  }
						  }
						}
					   }
					   System.debug('>>>>>>>>>>9>>>>>>>'+mf);
					   monthForecastListToUpdate.add(mf);
				   }
			}
	}
			if(monthForecastListToUpdate.size() > 0){
				update monthForecastListToUpdate;
			}
	} */
	public static void updateTrailingImpact(Map<Id, Base_Trail_Survey__c> mapOld,List<Base_Trail_Survey__c> newList){
		Map<Id,Double> opportunityIdtoTotalImpact = new Map<Id,Double>();
		Set<Id> opportunityIdSet = new Set<Id>();
		for(Base_Trail_Survey__c survey : newList){
			if(mapOld == null || (mapOld.get(survey.Id).Roll_Up_Total__c != survey.Roll_Up_Total__c)){	
				opportunityIdSet.add(survey.Opportunity__c);
				if(survey.Roll_Up_Total__c!=null && survey.Roll_Up_Total__c >= 0.00){
					opportunityIdtoTotalImpact.put(survey.Opportunity__c,survey.Roll_Up_Total__c);
				}
				else{
					opportunityIdtoTotalImpact.put(survey.Opportunity__c,0.00);
				}	  	
			}
		}
		List<Opportunity> oppListToUpdate = new List<Opportunity>();
		if(opportunityIdSet.size() > 0){
			for(Id oppId : opportunityIdSet){
				Opportunity opp = new Opportunity();
				opp.Id = oppId;
				if(opportunityIdtoTotalImpact.containsKey(oppId)){
					Double tempVal = opportunityIdtoTotalImpact.get(oppId);
					if(tempVal == 0.00){
						opp.Total_Trailing_Impact__c = 0;
					}else{
						opp.Total_Trailing_Impact__c = tempVal;
					}
					oppListToUpdate.add(opp);
				}
			}
		}
		if(oppListToUpdate.size() > 0){
			update oppListToUpdate;
		} 
	}
	public static void updateTrailingImpactbeforeDelete(Map<Id, Base_Trail_Survey__c> mapOld){
		Set<Id> opportunityIds = new Set<Id>();
		Set<String> userIds = new Set<String>();
        Set<String> oppDateSet = new Set<String>();
        Set<String> BUSet = new Set<String>();
		List<Opportunity> oppListToUpdate = new List<Opportunity>();
		for(Base_Trail_Survey__c survey : mapOld.values()){
				opportunityIds.add(survey.Opportunity__c);
		}
		if(opportunityIds.size() > 0){
			for(Id oppId : opportunityIds){
				Opportunity opp = new Opportunity();
				opp.Id = oppId ;
				opp.Total_Trailing_Impact__c = 0;
				oppListToUpdate.add(opp);
			}
		}
		if(oppListToUpdate.size() > 0){
			update oppListToUpdate;
		}
	}
	// Method to delete all child base products when base trail deletes 
	public static void deleteBaseProducts(List<Base_Trail_Survey__c> oldlist) {
		Set<Id> baseTrailids = new Set<Id>();
        for(Base_Trail_Survey__c bts : oldlist) {
            baseTrailids.add(bts.Id);
        }
         List<Base_Product__c> baseProducts = [SELECT Base_Trail_Survey__c FROM Base_Product__c WHERE Base_Trail_Survey__c IN :baseTrailids];
    	 delete baseProducts;
	}
}