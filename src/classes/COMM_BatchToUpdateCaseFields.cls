/**================================================================      
* Appirio, Inc
* Name: COMM_BatchToUpdateCaseFields  
* Description: T-526438 (To populate converted fields on COMM Case records)
* Created Date: 08/24/2016
* Created By: Nitish Bansal (Appirio)
*
* Date Modified      Modified By      Description of the update
* 29th Nov           Nitish Bansal    I-242624 
==================================================================*/

global class COMM_BatchToUpdateCaseFields implements Database.Batchable<sObject>{
    global String query;
    global Map<ID,RecordType> caseRtMap;
    global Database.QueryLocator start(Database.batchableContext bc){  
        //Querying COMM RT for Cases
        caseRtMap = New Map<ID,RecordType>([Select ID, DeveloperName From RecordType Where sObjectType = 'Case' and DeveloperName Like '%COMM%']);
        List<Id> caseRTIds = new List<Id>(); //NB - 11/29 - I-242624
        caseRTIds.addAll(caseRtMap.keySet()); //NB - 11/29 - I-242624
        //Querying COMM Cases
        query = 'Select Id, Creator_Email__c, Owner_First_Name__c, Owner_Last_Name__c, RecordTypeName__c, Sales_Rep_Top_35_Sales_Agent__c, OwnerId, RecordTypeId, Sales_Rep__c, Sales_Rep__r.Top_35_Sales_Agent__c from Case where RecordTypeId In :caseRTIds' ;//NB - 11/29 - I-242624
        
        //system.debug('**query**'+query);
        return Database.getQueryLocator(query);                                                                                   
    }

    global void execute(Database.BatchableContext bc, List<Case> cases){
        List<Case> caseListUpdate = new List<Case>();       
        List<Id> contactIds = new List<Id>();
        Map<Id, String> mapIdToRT = new Map<Id, String>();
        Map<Id, Boolean> mapIdToAgent = new Map<Id, Boolean>();
        Set<Id> setUserIds = new Set<Id>();
        List<Case> lstCasesToUpdate = new List<Case>();
        caseRtMap = New Map<ID,RecordType>([Select ID, DeveloperName From RecordType Where sObjectType = 'Case' and DeveloperName Like '%COMM%']);//NB - 11/29 - I-242624
        for(Case cs : cases) {
            if(caseRtMap.get(cs.RecordTypeId).DeveloperName.contains('COMM')){
                
                setUserIds.add(cs.OwnerId); 
                mapIdToRT.put(cs.Id,caseRtMap.get(cs.RecordTypeId).DeveloperName);
                if(cs.Sales_Rep__c == null || cs.Sales_Rep__r.Top_35_Sales_Agent__c == null){
                    mapIdToAgent.put(cs.Id,false);
                }
                else{
                    mapIdToAgent.put(cs.Id,cs.Sales_Rep__r.Top_35_Sales_Agent__c);               
                }
                caseListUpdate.add(cs); 
            } 
        }
        
        if(!caseListUpdate.isEmpty() && caseListUpdate.size() > 0){
            Map<id,User> mapIdToUSer = new Map<id,User>();
            for(User us : [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE Id IN:setUserids]){
                mapIdToUser.put(us.Id,us);
            }
            
            
            for(Case cs: caseListUpdate){
                lstCasesToUpdate.add(new Case(Id = cs.Id,
                                                  Owner_First_Name__c = mapIdToUser.get(cs.OwnerId).FirstName,
                                                  Owner_Last_Name__c = mapIdToUser.get(cs.OwnerId).LastName,
                                                  RecordTypeName__c = mapIdToRT.get(cs.Id),
                                                  Creator_Email__c = UserInfo.getUserEmail(),
                                                  Sales_Rep_Top_35_Sales_Agent__c = mapIdToAgent.get(cs.Id)
                                                 )
                                        );   
            }
        }       
        if(!lstCasesToUpdate.isEmpty() && lstCasesToUpdate.size() > 0){
            update lstCasesToUpdate;
        }
    }

    global void finish(Database.BatchableContext BC){
    }
}