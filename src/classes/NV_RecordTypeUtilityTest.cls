@isTest
private class NV_RecordTypeUtilityTest {

	@isTest static void testGetRecordTypeNames(){

	String objectName = 'Order';
	Map<Id,String> getRecordTypes =  NV_RecordTypeUtility.getRecordTypeNames(objectName);
	System.assertEquals(getRecordTypes.size()>0,true);

	}

	@isTest static void testGetDeveloperNameRecordTypeIds(){

		String objectName = 'Order';
		Map<String, Id> getRecordId = NV_RecordTypeUtility.getDeveloperNameRecordTypeIds(objectName);
		Id getId = [SELECT Id from RecordType where DeveloperName = 'NV_Bill_Only' AND SobjectType =:objectName LIMIT 1].Id;
		System.assertEquals(getId,getRecordId.get('NV_Bill_Only'));
	}


	@isTest static void testGetRecordTypeIds(){
		String objectName = 'Order';
		Map<String, Id> getRecordId = NV_RecordTypeUtility.getRecordTypeIds(objectName);
		Id getId = [SELECT Id from RecordType where Name = 'NV Bill Only' AND SobjectType =:objectName LIMIT 1 ].Id;
		System.assertEquals(getId,getRecordId.get('NV Bill Only'));
	}

	@isTest static void testGetName(){
		String objectName = 'Order';
		String recordName = 'NV Bill Only';
		String recordtypeId = [SELECT Id from RecordType WHERE Name =:recordName AND SobjectType =:objectName LIMIT 1].Id;
		String getName = NV_RecordTypeUtility.getName(objectName,recordtypeId);
		System.assertEquals(recordName,getName);
	}


	@isTest static void testGetId(){
		String objectName = 'Order';
		String recordName = 'NV Bill Only';
		String recordtypeId = [SELECT Id from RecordType WHERE Name =:recordName AND SobjectType =:objectName LIMIT 1].Id;
		Id getId = NV_RecordTypeUtility.getId(objectName,recordName);
		System.assertEquals(recordtypeId,getId);

	}

	@isTest static void testGetIdDN(){

		String objectName = 'Order';
		String recordName = 'NV_Bill_Only';
		String recordtypeId = [SELECT Id from RecordType WHERE DeveloperName =:recordName AND SobjectType =:objectName LIMIT 1].Id;
		Id getId = NV_RecordTypeUtility.getIdDN(objectName,recordName);
		System.assertEquals(recordtypeId,getId);

	}





}