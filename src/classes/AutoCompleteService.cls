/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Nov 21, 2013
 Purpose       :  REST Webservice for returning Tag records based on a search.
 Reference     :  Task T-206113
 Modifications :  Task T-218154    Dec 05, 2013   Sonal Shrivastava
               :  Task T-217387    Dec 12, 2013   Sonal Shrivastava
               :  Task T-220117    Dec 16, 2013   Pitamber Sharma
               :  Task T-215773    Dec 16, 2013   Pitamber Sharma
               :  Issue I-94416    Jan 09, 2014   Sonal Shrivastava
               :  Issue I-94422    Jan 15, 2014   Sonal Shrivastava
*******************************************************************************/
@RestResource(urlMapping='/AutoCompleteService/*') 
global with sharing class AutoCompleteService {
    
    private static String PRODUCT_STRYKER_NV_RECTYPEID;
  
  // ***************************************************************************
  // Get Method for REST service
  // ***************************************************************************  
  @HttpGet 
  global static ObjectListWrapper doGet() {
    try {
      RestRequest req = RestContext.request;      
      ObjectListWrapper objListWrapper = new ObjectListWrapper(req);                    
      return objListWrapper;
    } 
    catch(Exception ex) {
      return new ObjectListWrapper(); 
    }
    return null;
  } 
  
  // ***************************************************************************
  // Static block
  // ***************************************************************************  
  static {
    String prodNVRecTypeName = Label.Product_RecordType_Stryker_NV;
    PRODUCT_STRYKER_NV_RECTYPEID = Schema.SObjectType.Product2.RecordTypeInfosByName.get(prodNVRecTypeName).RecordTypeId;   
  } 
 
  // ***************************************************************************
  // Wrapper class for List of Tag records
  // ***************************************************************************  
  global class ObjectListWrapper{
    public List<ObjectJson> objectList;     
    
    //-----------------------------------
    // Default Constructor
    //-----------------------------------
    public ObjectListWrapper(){}
    
    //-----------------------------------
    // Parameterized Constructor
    //-----------------------------------
    public ObjectListWrapper(RestRequest req){
      String searchTerm = null;
      Boolean excludeUsers = false;
      if(req.params.containsKey('searchTerm')){
          searchTerm = req.params.get('searchTerm');
        }
        if(req.params.containsKey('excludeUsers')){
        excludeUsers = req.params.get('excludeUsers') == 'true' ? true : false;
      }
      this.objectList = fetchRecords(searchTerm, excludeUsers);
    }
    
    //-----------------------------------
    // Method to fetch records
    //-----------------------------------
    public List<ObjectJson> fetchRecords(String searchTerm, Boolean excludeUsers){
        List<ObjectJson> objList = new List<ObjectJson>(); 
        if(searchTerm != null && searchTerm != ''){
        
        String searchStr = searchTerm.trim().replace('*','');     
        String soslQuery = ' FIND {' + searchStr + '*} IN ALL FIELDS '
                       + ' RETURNING Account (id, Name, Nickname__c, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode WHERE (Name LIKE \'%' + searchStr + '%\' OR Nickname__c LIKE \'%' + searchStr + '%\')) '
                       + ', Contact (Id, Name, Account.Name WHERE Name LIKE \'%' + searchStr + '%\' ) '
                       + ', Product2 (Id, Name, Nickname__c, Manufacturer__c, RecordTypeId WHERE Taggable__c = true AND IsActive = true AND (Name LIKE \'%' + searchStr + '%\' OR Nickname__c LIKE \'%' + searchStr + '%\')) ';
            soslQuery = excludeUsers ? soslQuery : (soslQuery + ', User (Id, Name WHERE isActive = true AND Name LIKE \'%' + searchStr + '%\') '); 
            soslQuery += ' LIMIT 20 ';
        
        System.debug('<<<<< ' + soslQuery);
                    
        for(List<sObject> lstObj : Search.query(soslQuery)){
          if(lstObj.size() > 0){
            String objType = getObjectType(lstObj.get(0));
            
            for(sObject obj : lstObj){
              objList.add(new ObjectJson(obj, objType));
            } 
          }                     
        }
        }
      return objList;
    }
    
    //--------------------------------------------------------------------------
    // Method for getting object name
    //-------------------------------------------------------------------------- 
      private String getObjectType (sObject obj){
        return obj.getsObjectType() == Account.getsObjectType() ? Constants.ACCOUNT_TYPE
             : obj.getsObjectType() == Contact.getsObjectType() ? Constants.CONTACT_TYPE 
             : obj.getsObjectType() == Product2.getsObjectType() ? Constants.PRODUCT_TYPE
             : obj.getsObjectType() == User.getsObjectType() ? Constants.USER_TYPE
             : '';
      }     
  }
  
  // ***************************************************************************
  // Wrapper class for object records
  // ***************************************************************************
  public class ObjectJson{
    public String Title;
    public String obj;
    public String Subtitle;
    public String Id;
    
    //-----------------------------------
    // Default Constructor
    //-----------------------------------
    public ObjectJson(){}
    
    //-----------------------------------
    // Parameterized Constructor
    //----------------------------------- 
    public ObjectJson(sObject obj, String objType){      
      this.Id = obj.Id;
      this.obj = objType;
            
      if(objType == Constants.ACCOUNT_TYPE){
        Account acc = (Account)obj;         
        this.Title = (acc.Nickname__c != null && !acc.Nickname__c.equals('')) ? acc.Nickname__c : acc.Name; 
        String address = null;
        address = ((acc.ShippingStreet != null) ? acc.ShippingStreet : address );
        address = ((acc.ShippingCity != null) ? (address != null ? (address + ', ' + acc.ShippingCity) : acc.ShippingCity) : address );
        address = ((acc.ShippingState != null) ? (address != null ? (address + ', ' + acc.ShippingState) : acc.ShippingState) : address );
        address = ((acc.ShippingCountry != null) ? (address != null ? (address + ', ' + acc.ShippingCountry) : acc.ShippingCountry) : address );
        address = ((acc.ShippingPostalCode != null) ? (address != null ? (address + ', ' + acc.ShippingPostalCode) : acc.ShippingPostalCode) : address );
        this.Subtitle = address;
      }
      else if(objType == Constants.CONTACT_TYPE){
        Contact con = (Contact)obj;       
        this.Title = con.Name;
        this.Subtitle = con.Account.Name;
      }
      else if(objType == Constants.PRODUCT_TYPE){
        Product2 prod = (Product2)obj;       
        this.Title = (prod.Nickname__c != null && !prod.Nickname__c.equals('')) ? prod.Nickname__c : prod.Name;
        //Use Product Name if it is a Stryker product, otherwise use Manufacturer
        this.Subtitle = (prod.RecordTypeId == PRODUCT_STRYKER_NV_RECTYPEID) ? prod.Name : (prod.Manufacturer__c == null ? '' : prod.Manufacturer__c);
      }
      else if(objType == Constants.USER_TYPE){
        User usr = (User)obj;
        this.Title = usr.Name;
        this.Subtitle = Constants.USER_TYPE;
      }
    }
  }
}