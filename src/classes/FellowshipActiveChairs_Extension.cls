/****************************************************************
Name  :  FellowshipActiveChairs
Author:  Appirio India (Gaurav Nawal)
Date  :  Feb 10, 2016

Modified: 
****************************************************************/
public class FellowshipActiveChairs_Extension {
	public List<Fellowship_Member__c> lstFellowshipActiveMembers {get;set;}
	public List<Schema.FieldSetMember> fellowshipMembersFields{get;set;}
	public List<Fellowship_Member__c> tempList{get;set;}
	public List<String> selectedFields{get;set;}
	public Boolean renderTable {get; set;}
	public Integer noOfRecords{get;set;}
	public Integer size{get;set;}
	public ApexPages.StandardSetController setCon{get;set;}
	public Map<String,String> fellowshipMemberFieldToLabelMap{get;set;}
	public String sortedField{get;set;}
	public String ascDesc;
	public boolean isAsc{get;set;}
	String fellowshipId;
    public Boolean hasNext {
      get {
        return setCon.getHasNext();
      }
        set;
    }
    public Boolean hasPrevious {
      get {
        return setCon.getHasPrevious();
      }
      set;
    }
    public Integer pageNumber {
      get {
        return setCon.getPageNumber();
      }
        set;
    }
    public void getAllFellowshipRecords(){
    lstFellowshipActiveMembers = new List<Fellowship_Member__c>();
    for(Fellowship_Member__c fm : (List<Fellowship_Member__c>)setCon.getRecords()){
      lstFellowshipActiveMembers.add(fm);
    }
  }
	public FellowshipActiveChairs_Extension(ApexPages.StandardController controller) {
		renderTable = false;
		sortedField = '';
		ascDesc='';
		lstFellowshipActiveMembers = new List<Fellowship_Member__c> ();
		tempList = new List<Fellowship_Member__c> ();
		fellowshipMemberFieldToLabelMap = new Map<String,String>();
		selectedFields = new List<String>();
		fellowshipMembersFields = SObjectType.Fellowship_Member__c.FieldSets.ActiveChairsFieldSet.getFields();
		fellowshipId = ApexPages.currentPage().getParameters().get('Id');
		createQuery();
	}
	
	public void first() {
        setCon.first();
        getAllFellowshipRecords();
    }
  
    public void last() {
        setCon.last();
        getAllFellowshipRecords();
    }
  
    public void previous() {
        setCon.previous();
        getAllFellowshipRecords();
    }
  
    public void next() {
        setCon.next();
        getAllFellowshipRecords();
    }
   public void sortData(){
	if(sortedField!=''){
	   if(ascDesc == ' desc '){
          ascDesc = ' asc ';
          isAsc = true;
       }
       else{
    	   ascDesc = ' desc ';
           isAsc = false;
       }
	}
	createQuery();
	}
	public void createQuery(){
		selectedFields.clear();
		fellowshipMemberFieldToLabelMap.clear();
	 for(Schema.FieldSetMember f : fellowshipMembersFields) {
      if(f.getFieldPath().containsIgnoreCase('Contact__c')){
        fellowshipMemberFieldToLabelMap.put('Contact__r.Name', 'Contact');
        selectedFields.add('Contact__r.Name');
      }
      else{
        fellowshipMemberFieldToLabelMap.put(f.getFieldPath(), f.getLabel());
        selectedFields.add(f.getFieldPath());
      }
     }
     String query =  'SELECT Id, ';
    for(String fieldSets : fellowshipMemberFieldToLabelMap.keySet()){
      query += fieldSets + ', ';
      if(fieldSets.equalsIgnoreCase('Contact__c')){
        system.debug('IF');
        selectedFields.add('Contact__r.Name');
      }
    }
    system.debug('selectedFields>>>'+selectedFields);
    query += ' Contact__c FROM Fellowship_Member__c WHERE Fellowship__c =: fellowshipId AND Status__c =\'Active\'';
    if(sortedField!=''){ //order by '+ sortField + ' ' + ascDesc +' nulls last
			query += ' order by '+ sortedField + ' ' + ascDesc +' nulls last';
	}
	lstFellowshipActiveMembers = new List<Fellowship_Member__c> ();
	tempList = new List<Fellowship_Member__c> ();
	tempList = Database.query(query);
		if(!tempList.isEmpty()) {
			renderTable = true;
		} 
	setCon = new ApexPages.StandardSetController(tempList);
	size = Integer.valueOf(Label.Fellowship_Record_Size);
    setCon.setPageSize(size);
    noOfRecords = setCon.getResultSize();
    setCon.setPageNumber(1);
    getAllFellowshipRecords();
	}
}