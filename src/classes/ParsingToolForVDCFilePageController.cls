/************************************************
Author  : Appirio India [Hitesh - Nov 11, 2016]
Date    : Nov 11, 2016 (Original)
Purpose : S-444360
************************************************/
public with sharing class ParsingToolForVDCFilePageController {
    public blob inputFileContent {get;set;}
    public boolean displayTableBlock {get;set;}
    public string inputContentType {get;set;}
    public string inputFileName {get;set;}
    public string content {get;set;}
    public List<VelocityItem> velocityItemsWrapper {get;set;}
    public customerWrapper customerWrapper {get;set;}
    public boolean notValidFormat {get;set;}
    public boolean errorFlag {get;set;}
    public boolean noFileFound{get;set;}
    public decimal total_listprice {get;set;}
    public decimal total_equipTotal {get;set;}
    public decimal total_discount {get;set;}
    public decimal total_sellPrice {get;set;}
    
    public ParsingToolForVDCFilePageController(){
        string processParameter = ApexPages.currentPage().getParameters().get('process');        
        this.displayTableBlock  = false;
        this.errorFlag = false;
        this.noFileFound = false;
        this.total_listprice = 0.00;
        this.total_equipTotal = 0.00;
        this.total_discount = 0.00;
        this.total_sellPrice = 0.00;
    }
    
    public pageReference processInputFile(){
        velocityItemsWrapper = new List<VelocityItem>();               
        displayTableBlock = true;
        PageReference ref = new PageReference('/apex/ParsingToolForVDCFilePage?process=1');
        ref.setRedirect(false);
        try{
            if(inputFileName == null){
            	noFileFound = true;
            	displayTableBlock = false;
            }else if(!inputFileName.containsIgnoreCase('.vdc')){
                notValidFormat = true;
                displayTableBlock = false;
                noFileFound = false;
            }else{
            	notValidFormat = false;
            	noFileFound = false;
                content = inputFileContent.toString();
            
            XmlStreamReader xmlReader= new XmlStreamReader(inputFileContent.toString());            
            while(xmlReader.hasNext()){
                 if(xmlReader.getEventType() == xmlTag.START_ELEMENT){
                    
                     //system.debug('**** Element: ' + xmlReader.getLocalName()+' ***** prefix '+xmlReader.getPrefix());
                     
                     //system.debug('**** att count = '+xmlReader.getAttributeCount()+'**** attribute: '+xmlReader.getAttributeValue(null,xmlReader.getLocalName()));
                     
                     if(xmlReader.getPrefix().equalsIgnoreCase('velocity') && xmlReader.getLocalName().equalsIgnoreCase('customer')){
                         customerWrapper = new customerWrapper();
                         //inside customer tag
                         if(xmlReader.getAttributeCount() > 0){
                             for(integer i=0; i<xmlReader.getAttributeCount(); i++){
                                 if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('custnumber')){
                                     customerWrapper.custnumber = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('shipnumber')){
                                     customerWrapper.shipnumber = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('name')){
                                     customerWrapper.name = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('address1')){
                                     customerWrapper.address1 = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('address2')){
                                     customerWrapper.address2 = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('city')){
                                     customerWrapper.city = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('state')){
                                     customerWrapper.state = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('zip')){
                                     customerWrapper.zip = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('phone')){
                                     customerWrapper.phone = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('fax')){
                                     customerWrapper.fax = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('gponumber')){
                                     customerWrapper.gponumber = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('gponame')){
                                     customerWrapper.gponame = xmlReader.getAttributeValueAt(i);
                                 }else
                                 continue;
                             }
                         }
                     }
                     
                     if(xmlReader.getPrefix().equalsIgnoreCase('velocity') && xmlReader.getLocalName().equalsIgnoreCase('effectiveDays')){
                     	xmlReader.next();
                     	system.debug('*****inside eff days'+xmlReader.gettext());
                     	//if(customerWrapper != null && xmlReader.getEventType() == xmlTag.CHARACTERS){
                     		customerWrapper.effectiveDays = Integer.valueOf(xmlReader.gettext());
                     		xmlReader.next();
                     		//xmlReader.next();
                     	//}
                     }
                     system.debug('**** out of eff days');
                     
                     if(xmlReader.getPrefix().equalsIgnoreCase('velocity') && xmlReader.getLocalName().equalsIgnoreCase('proposalDate')){
                     	xmlReader.next();
                     	system.debug('*****inside proposaldate');
                     	//if(customerWrapper != null && xmlReader.getEventType() == xmlTag.CHARACTERS){
                     		string d_string = xmlReader.gettext();
                     		if(d_string != null && d_string != '' && d_string.contains('-')){                     			
                     			//Date pDate = Date.newInstance(Integer.valueOf(d_string.split('-')[0]), Integer.valueOf(d_string.split('-')[1]), Integer.valueOf(d_string.split('-')[2]));
                     			customerWrapper.proposalDate_dd = d_string.split('-')[2];
                     			customerWrapper.proposalDate_mm= d_string.split('-')[1];
                     			customerWrapper.proposalDate_yy = d_string.split('-')[0];
                     		}                     		
                     	//}
                     	xmlReader.next();
                     }
                                          
                     
                     if(xmlReader.getPrefix().equalsIgnoreCase('velocity') && xmlReader.getLocalName().equalsIgnoreCase('item')){
                         VelocityItem itemWrapper = new VelocityItem();
                         //inside item tag
                         if(xmlReader.getAttributeCount() > 0){
                             for(integer i=0; i<xmlReader.getAttributeCount(); i++){
                                 system.debug('******Item Attributes START******');
                                 system.debug('++Att Name  = '+xmlReader.getAttributeLocalName(i)+'  ++ value is = '+xmlReader.getAttributeValueAt(i));
                                 system.debug('******Item Attributes END******');
                                 if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('type')){
                                     itemWrapper.i_type = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('description')){
                                     itemWrapper.i_description = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('uom')){
                                     itemWrapper.i_uom = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('packageQuantity')){
                                     itemWrapper.i_pkgQuantity = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('discount')){
                                     itemWrapper.i_discount = (Decimal.valueOf(xmlReader.getAttributeValueAt(i))*100).setScale(2);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('listPrice')){
                                     itemWrapper.i_listPrice = Decimal.valueOf(xmlReader.getAttributeValueAt(i));
                                     itemWrapper.i_sellPrice = itemWrapper.i_listPrice - (itemWrapper.i_listPrice * (itemWrapper.i_discount/100));
                                     total_listprice += itemWrapper.i_listPrice;                                     
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('priceType')){
                                     itemWrapper.i_priceType = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('quantity')){
                                     itemWrapper.i_quantity = Integer.valueOf(xmlReader.getAttributeValueAt(i));
                                 }else
                                 continue;
                             }
                         }
                         
                         //jump to items part
                         xmlReader.next();xmlReader.next();
                         
                         if(xmlReader.getAttributeCount() > 0){
                             for(integer i=0; i<xmlReader.getAttributeCount(); i++){
                                 system.debug('******part Attributes START******');
                                 system.debug('++Att Name  = '+xmlReader.getAttributeLocalName(i)+'  ++ value is = '+xmlReader.getAttributeValueAt(i));
                                 system.debug('******part Attributes END******');
                                 
                                 if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('company')){
                                     itemWrapper.p_company = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('pn')){
                                     string p_pn = xmlReader.getAttributeValueAt(i);
                                     itemWrapper.p_pn = p_pn.subString(0,4)+ '-' + p_pn.subString(3,7)+ '-' + p_pn.subString(7);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('partType')){
                                     itemWrapper.p_partType = xmlReader.getAttributeValueAt(i);
                                 }else if(xmlReader.getAttributeLocalName(i).equalsIgnoreCase('quantity')){
                                     itemWrapper.p_quantity= Integer.valueOf(xmlReader.getAttributeValueAt(i));
                                     itemWrapper.i_total = itemWrapper.p_quantity*itemWrapper.i_sellPrice;
                                     total_sellPrice += itemWrapper.i_total;
                                 }else
                                 continue;
                             }
                         }
                         
                         velocityItemsWrapper.add(itemWrapper);
                     }
                     
                                      
                 }else if(xmlReader.getEventType() == xmlTag.CHARACTERS){
                     if(!xmlReader.isWhiteSpace())
                         system.debug('****Text: ' + xmlReader.getText());                                         
                     system.debug('*** loal name '+xmlReader.getLocalName());
                     system.debug('*** prefix '+xmlReader.getPrefix());
                 }
                 system.debug('***=======****');
                 xmlReader.next();
             }
             total_sellprice = total_sellprice.setScale(2);
             total_listprice = total_listprice.setScale(2);
             total_discount = total_sellprice < total_listprice ? (((total_listprice - total_sellprice)/total_listprice)*100).setScale(2) : 0;
            }
        }catch(Exception ex){
            errorFlag = true;
            displayTableBlock = false;            
            return ref;
        }                                          
        return ref;
    }
    
    public class VelocityItem{
        public string i_type {get;set;}
        public string i_description {get;set;}
        public string i_uom {get;set;}
        public string i_pkgQuantity {get;set;}
        public decimal i_discount {get;set;}
        public decimal i_listPrice {get;set;}
        public string i_priceType {get;set;}
        public integer i_quantity {get;set;}
        public decimal i_sellPrice {get;set;}
        public decimal i_total {get;set;}
        
        public string p_company {get;set;}
        public string p_pn {get;set;}
        public string p_partType {get;set;}
        public integer p_quantity {get;set;}        
        
        /*public VelocityItem(string i_type, string i_description, string i_uom, string i_pkgQuantity, string i_discount, string i_listPrice, string i_priceType,
                                string i_quantity, string p_company, string p_pn, string p_partType, string p_quantity){
            this.i_type = i_type ;
            this.i_description = i_description ;
            this.i_uom  = i_uom ;
            this.i_pkgQuantity = i_pkgQuantity ;
            this.i_discount = i_discount ;
            this.i_listPrice = i_listPrice ;
            this.i_priceType = i_priceType ;
            this.i_quantity = i_quantity ;
            this.p_company = p_company ;
            this.p_pn = p_pn ;
            this.p_partType = p_partType ;
            this.p_quantity = p_quantity ;
        }*/
    }
    
    public class customerWrapper{
        public string custnumber{get;set;}
        public string shipnumber{get;set;}
        public string name{get;set;}
        public string address1{get;set;}
        public string address2{get;set;}
        public string city{get;set;}
        public string state{get;set;}
        public string zip{get;set;}
        public string phone{get;set;}
        public string fax{get;set;}
        public string gponumber{get;set;}
        public string gponame{get;set;}
        public string proposalDate_dd{get;set;}
        public string proposalDate_mm{get;set;}
        public string proposalDate_yy{get;set;}
        public integer effectiveDays{get;set;}
    }
}