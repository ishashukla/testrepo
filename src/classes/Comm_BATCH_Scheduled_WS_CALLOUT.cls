/**================================================================      
* Appirio, Inc
* Name: [Comm_BATCH_Scheduled_WS_CALLOUT]
* Description: [batch Class for Callin EBS]
* Created Date: [14-09-2016]
* Created By: [Gaurav Sinha] (Appirio)
*
* Date Modified      Modified By      Description of the update
* 14 Sep 2016        Gaurav Sinha      T-509402
==================================================================*/
public class Comm_BATCH_Scheduled_WS_CALLOUT implements Database.Batchable < SObject > ,Database.AllowsCallouts, Database.Stateful, Schedulable {
    public List<ID> WORKDETAIL_ID = new List<Id>();
    public list<id> WORKORDER_ID = new List<Id>();
    public String query = 'SELECT Id,name,Coverage_Type__c,EU_PO__c, ' +
        'SVMXC__Service_Contract__r.SVMXC__Start_Date__c,SVMXC__Case__r.Account.AccountNumber, ' +
        'SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Site__r.Location_ID__c, ' +
        'SVMXC__Group_Member__r.Name,SVMXC__Case__r.Accountid, ' +
        'SVMXC__Order_Type__c, SVMXC__Warranty__r.SVMXC__End_Date__c,( ' +
        '    SELECT Id,name,SVMXC__Line_Type__c, SVMXC__Actual_Quantity2__c, ' +
        '       SVMXC__Product__r.ProductCode,SVMXC__Product__r.Inventory_Item_ID__c, ' +
        '       Installation_Date__c,SVMXC__Billable_Quantity__c, SVMXC__Product__c, ' +
        '       From_Van_Stock__c, SVMXC__Billable_Line_Price__c,Order_Product__c,order__C ' +
        '    FROM SVMXC__Service_Order_Line__r ' + 
        '    WHERE Id IN : WORKDETAIL_ID ' +
        ') ' + 
        'FROM SVMXC__Service_Order__c WHERE id IN : WORKORDER_ID';
    /*
    * @method:  Comm_BATCH_Scheduled_WS_CALLOUT
    * @param : query : to reinitalize the query string
    * @Description: Constructor for reintialze the Query.
    */
    public Comm_BATCH_Scheduled_WS_CALLOUT(String query,List<id> WORKDETAIL_ID,List<Id> WORKORDER_ID) {
        this.query = query;
        this.WORKDETAIL_ID = WORKDETAIL_ID;
        this.WORKORDER_ID = WORKORDER_ID;
    }
    
    /*
    * @method:  execute
    * @param : sc : SchedulableContext
    * @Description: Method implemented for the interface Schedulable. It calls the batch class for implementing the desired functionality.
    */
    public void execute(SchedulableContext sc) {
        if (!Test.isRunningTest()) {
            Id batchId = Database.executeBatch(new Comm_BATCH_Scheduled_WS_CALLOUT(query,WORKDETAIL_ID,WORKORDER_ID), 1);
        }
    }
    
    /*
    * @method:  Comm_BATCH_Scheduled_WS_CALLOUT
    * @param : query : to reinitalize the query string
    * @Description: Constructor for reintialze the Query.
    */
    public Comm_BATCH_Scheduled_WS_CALLOUT(List<id> WORKDETAIL_ID,List<Id>WORKORDER_ID) {
        this.WORKDETAIL_ID = WORKDETAIL_ID;
        this.WORKORDER_ID = WORKORDER_ID;
    }
    
    /*
    * @method:  finish
    * @param : context : Database.BatchableContext
    * @Description: Method implemented for the interface Schedulable
    */
    public void finish(Database.BatchableContext context) {}
    
    /*
    * @method:  start
    * @param : context : Database.BatchableContext
    * @Description: Method implemented for the interface Batchable to get the records on which the batch processing needs to be applied.
    */
    public Database.QueryLocator start(Database.BatchableContext context) {
        system.debug('query '+query);
        system.debug('WORKORDER_ID '+ WORKORDER_ID);
        system.debug('WORKDETAIL_ID '+WORKDETAIL_ID);
        return Database.getQueryLocator(query);
    }
    
    /*
    * @method:  execute
    * @param : context : Database.BatchableContext : Batch Context
    * @param : objects : List<SVMXC__Warranty__c> : List of obejct returned in the batch context.
    * @Description: 
    */
    public void execute(Database.BatchableContext context, List < SVMXC__Service_Order__c > objects) {
        system.debug('Inside the Execute method');
        callOrderCreateANDWS(objects[0]);
    }

    public  void callOrderCreateANDWS( SVMXC__Service_Order__c  objects) {
        System.debug(objects.SVMXC__Service_Order_Line__r+'  ==>data');
        system.debug(WORKORDER_ID);
        system.debug(WORKDETAIL_ID);
        List<sobject> todelete = new List<sobject>();
        try {
            List<SVMXC__Service_Order_Line__c> listOrderLineItem = new List<SVMXC__Service_Order_Line__c>();
            Map<Id, String> accountmapping = new Map<Id, String>();
            Map<Id, String> accountmappingInvoice = new Map<Id, String>();
            for(SVMXC__Service_Order_Line__c varloop:  [select id,name,SVMXC__Line_Type__c, 
                                                        SVMXC__Actual_Quantity2__c, SVMXC__Product__r.ProductCode,
                                                        SVMXC__Product__r.Inventory_Item_ID__c, Installation_Date__c,
                                                        SVMXC__Billable_Quantity__c, SVMXC__Product__c,From_Van_Stock__c, 
                                                        SVMXC__Billable_Line_Price__c,Order_Product__c,order__c, Order_Product__r.Oracle_Order_Line_Number__c 
                                                        from SVMXC__Service_Order_Line__c 
                                                        where id in : WORKDETAIL_ID
                                                        and SVMXC__Service_Order__c =: objects.id]){
                listOrderLineItem.add(varloop);
                todelete.add(new OrderItem(id=varloop.Order_Product__c));
                todelete.add(new Order(id=varloop.Order__c));
            }
/*            for(SVMXC__Service_Order_Line__c varloop: objects.SVMXC__Service_Order_Line__r) {
                //mapOrderItems.put(varloop.id,varorderItems);
                listOrderLineItem.add(varloop);
                todelete.add(new OrderItem(id=varloop.Order_Product__c));
                todelete.add(new Order(id=varloop.Order__c));
            }*/
            System.debug(listOrderLineItem+'==>listOrderLineItem');
            
            accountmapping.put(objects.SVMXC__Case__r.Accountid, null);
            accountmappingInvoice.put(objects.SVMXC__Case__r.Accountid, null);
            
            for (Address__c varloop: [SELECT id, account__c, type__C, Location_ID__c 
                                      FROM address__c 
                                      WHERE account__c in : accountMapping.keySet()
                                          AND Business_Unit_Name__c = 'COMM'
                                          AND Primary_Address_Flag__c = true
                                          AND(Type__c = 'Shipping' OR Type__c = 'Billing')]) {
                if (Varloop.type__c == 'Shipping'){
                    accountmapping.put(varloop.account__c, varloop.Location_ID__c);
                }
                if (Varloop.type__c == 'Billing'){
                    accountmappingInvoice.put(varloop.Account__c, varloop.Location_ID__c);
                }
            }
            
            
            // Starting web Service callout
            //  for (SVMXC__Service_Order_Line__c varloop: listOrderLineItem) {
            COMM_CreateEBSSalesOrder_REST.informaticaRequest request = new COMM_CreateEBSSalesOrder_REST.informaticaRequest();
            COMM_CreateEBSSalesOrder_REST.salesOrderRequest salesOrderRequest = new COMM_CreateEBSSalesOrder_REST.salesOrderRequest();
            COMM_CreateEBSSalesOrder_REST.lineItems lineItems = new COMM_CreateEBSSalesOrder_REST.lineItems();
            salesOrderRequest.IntegrationHeader = COMM_CreateEBSSalesOrder_REST.createIntegrationHeader(Label.ORDER_SFDC, Label.order_commerp);
            System.debug('Integration header');
            salesOrderRequest.orderHeader = createOrderHeader(objects, accountmapping, accountmappingInvoice, listOrderLineItem[0].order__C);
            System.debug('Order header');
            lineItems.lineItem = createOrderItems(listOrderLineItem);
            System.debug('Order Line Items');
            salesOrderRequest.lineItems = lineItems;
            request.salesOrderRequest = salesOrderRequest;
            System.debug('SOAP request '+request);
            // Call Rest utility to create order on oracle SOA
            String response = COMM_CreateEBSSalesOrder_REST.createOrderRestCallout(request);
            System.debug('SOAP response '+response);
            if(!String.isEmpty(response)) {
                listOrderLineItem[0].Callout_Result__c = response;
                update listOrderLineItem;
                delete todelete;
            }
        } 
        catch (Exception e) {
            System.debug(e.getMessage());
            // database.rollback(sp);
        }
    }
    
    // Method to create order Line items
    public static List < COMM_CreateEBSSalesOrder_REST.lineItem > createOrderItems(List<SVMXC__Service_Order_Line__c> order) {
        COMM_CreateEBSSalesOrder_REST.lineItem orderitem = new COMM_CreateEBSSalesOrder_REST.lineItem();
        List<COMM_CreateEBSSalesOrder_REST.lineItem> liorderitem = new List<COMM_CreateEBSSalesOrder_REST.lineItem>();
        for(SVMXC__Service_Order_Line__c varloop : order){
            orderitem = new COMM_CreateEBSSalesOrder_REST.lineItem();
            //orderitem.linetype = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Line_Type__c);
            //orderitem.LineNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.name);
            orderitem.LineNumber = new COMM_CreateEBSSalesOrder_REST.TagName(varloop.Order_Product__r.Oracle_Order_Line_Number__c);    
            orderitem.orderedItem = new COMM_CreateEBSSalesOrder_REST.TagName(varloop.SVMXC__Product__r.ProductCode);
            if (varloop.Installation_Date__c != null) {
                orderitem.requestDate = new COMM_CreateEBSSalesOrder_REST.TagName(varloop.Installation_Date__c.format());
            } 
            else {
                orderitem.requestDate = new COMM_CreateEBSSalesOrder_REST.TagName(Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'));
            }
            orderitem.orderedQuantity = new COMM_CreateEBSSalesOrder_REST.TagName(String.valueOf(varloop.SVMXC__Billable_Quantity__c));
            orderitem.unitSellingPrice = new COMM_CreateEBSSalesOrder_REST.TagName(String.valueOf(varloop.SVMXC__Billable_Line_Price__c));
            orderitem.origSysLineRef = new COMM_CreateEBSSalesOrder_REST.TagName(varloop.Order_Product__c);
            orderitem.lineCategoryCode = new COMM_CreateEBSSalesOrder_REST.TagName('Regular');
            orderitem.operation = new COMM_CreateEBSSalesOrder_REST.TagName(Label.Order_Create);
            orderitem.orgId = new COMM_CreateEBSSalesOrder_REST.TagName(label.COMM_Order_BU);
            orderitem.orderQuantityUom = new COMM_CreateEBSSalesOrder_REST.TagName('EA');
            orderitem.shipFromVan = new COMM_CreateEBSSalesOrder_REST.TagName(String.valueOf(varloop.From_Van_Stock__c));
            liorderitem.add(orderitem);
        }
        //orderitem.unitListPrice = new COMM_CreateEBSSalesOrder_REST.TagName('0.0');
        //orderitem.extendedLinePrice = new COMM_CreateEBSSalesOrder_REST.TagName('0.0');
        //orderitem.lockFlag = new COMM_CreateEBSSalesOrder_REST.TagName('false');            
        
        return liorderitem;
    }
    
    // Method to create Order Request
    public static COMM_CreateEBSSalesOrder_REST.orderHeader createOrderHeader(SVMXC__Service_Order__c order, Map < id, string > accountmapping, Map < id, string > accountmappingInvoice, Id orderId) {
        COMM_CreateEBSSalesOrder_REST.orderHeader orderHeader = new COMM_CreateEBSSalesOrder_REST.orderHeader();
        orderHeader.operation = new COMM_CreateEBSSalesOrder_REST.TagName(Label.Order_Create);
        orderHeader.businessUnit = new COMM_CreateEBSSalesOrder_REST.TagName(label.COMM_Order_BU);
        
        if (order.SVMXC__Warranty__r.SVMXC__End_Date__c != null 
            && order.SVMXC__Warranty__r.SVMXC__End_Date__c >= Datetime.now()) {
            orderHeader.custPoNumber = new COMM_CreateEBSSalesOrder_REST.TagName('WARRANTY');
        }
        if (order.SVMXC__Service_Contract__r.SVMXC__Start_Date__c != null 
            && order.SVMXC__Service_Contract__r.SVMXC__End_Date__c != null) {
            if (order.SVMXC__Service_Contract__r.SVMXC__Start_Date__c <= Datetime.now() 
                && order.SVMXC__Service_Contract__r.SVMXC__End_Date__c >= Datetime.now()) {
                orderHeader.custPoNumber = new COMM_CreateEBSSalesOrder_REST.TagName('SERVICE CONTRACT');
            }
        }
        
        If(order.Coverage_Type__c != null) {
            if (order.Coverage_Type__c == '120 Day Repair Warranty(Service)') {
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Standard Warranty');
            } 
            else if (order.Coverage_Type__c == 'Billable repair') {
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Service Purchase Order');
                orderHeader.custPoNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.EU_PO__c);
            } 
            else if (order.Coverage_Type__c == 'Factory Warranty Period') {
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Standard Warranty');
            } 
            else if (order.Coverage_Type__c == 'POS Ext Warranty Concessions') {
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Standard Warranty');
            } 
            else if (order.Coverage_Type__c == 'Service Contract') {
                orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName('COM Standard Service Contract');
            }
        }
        //orderHeader.orderType = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Service_Order__r.SVMXC__Order_Type__c );
        orderHeader.orderSource = new COMM_CreateEBSSalesOrder_REST.TagName(Label.ORDER_SFDC);
        orderHeader.priceList = new COMM_CreateEBSSalesOrder_REST.TagName(Label.Order_PriceList);
        orderHeader.soldFromOrgId = new COMM_CreateEBSSalesOrder_REST.TagName('0');
        orderHeader.soldToOrgId = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Case__r.Account.AccountNumber);
        
        if (accountmapping.get(order.SVMXC__Case__r.Accountid) != null) {
            orderHeader.shipToOrgId = new COMM_CreateEBSSalesOrder_REST.TagName(accountmapping.get(order.SVMXC__Case__r.Accountid));
        }
        if (accountmappingInvoice.get(order.SVMXC__Case__r.Accountid) != null) {
            orderHeader.invoiceToOrgId = new COMM_CreateEBSSalesOrder_REST.TagName(accountmappingInvoice.get(order.SVMXC__Case__r.Accountid));
        }
        
        orderHeader.salesrepId = new COMM_CreateEBSSalesOrder_REST.TagName('Communications Sales');
        //orderHeader.paymentTerm = new COMM_CreateEBSSalesOrder_REST.TagName(order.Payment_Terms__c);
        orderHeader.paymentTerm = new COMM_CreateEBSSalesOrder_REST.TagName('Net 20'); //Hardcoded for Testing -Mohan        
        orderHeader.origSysDocumentRef = new COMM_CreateEBSSalesOrder_REST.TagName(orderId);
        orderHeader.projectManager = new COMM_CreateEBSSalesOrder_REST.TagName(order.SVMXC__Group_Member__r.Name);
        orderHeader.projectNumber = new COMM_CreateEBSSalesOrder_REST.TagName(order.name);
        return orderHeader;
    }
}