// 
// (c) 2016 Appirio, Inc.
//
// Name : COMM_RMAShipmentOrderLineTriggerHandler 
// 2 Nov 2016   Mohan Panneerselvam 
// 
//
public class COMM_RMAShipmentOrderLineTriggerHandler {
    public static void processRMAItemsAfterUpdate(Map<Id, SVMXC__RMA_Shipment_Line__c> oldRecordMap, Map<Id, SVMXC__RMA_Shipment_Line__c> newRecordMap){        

		for(Profile migIntegProfile : [Select id from Profile where Name = :Label.DataMigrationIntegration]){
      		if(migIntegProfile.Id != UserInfo.getProfileId()){
                Map<Id, SVMXC__RMA_Shipment_Line__c> newRefinedRecordMap = new Map<Id, SVMXC__RMA_Shipment_Line__c>();
                for(Id lineId: newRecordMap.keySet()){
                    if(oldRecordMap.get(lineId).SVMXC__Expected_Quantity2__c != newRecordMap.get(lineId).SVMXC__Expected_Quantity2__c){
                        newRefinedRecordMap.put(lineId, newRecordMap.get(lineId));
                    }
                }
		        syncOrderLineUpdatetoEBS(newRefinedRecordMap);
            }
		}
    }
    
    public static void syncOrderLineUpdatetoEBS(Map<Id, SVMXC__RMA_Shipment_Line__c> newRecordMap){
        List<SVMXC__RMA_Shipment_Line__c>rmaShipLineList = [select id, SVMXC__RMA_Shipment_Order__c, SVMXC__RMA_Shipment_Order__r.SVMX_Order__c 
                                                            from SVMXC__RMA_Shipment_Line__c where id in: newRecordMap.keySet()];
        
        String rmaHeaderId;
        Id orderHeaderId;
        List<id> rmaLineIdList = new List<id>();
        for(SVMXC__RMA_Shipment_Line__c tempLine: rmaShipLineList){
            if(rmaHeaderId==null || rmaHeaderId == tempLine.SVMXC__RMA_Shipment_Order__c){
                rmaHeaderId = tempLine.SVMXC__RMA_Shipment_Order__c;
                orderHeaderId = tempLine.SVMXC__RMA_Shipment_Order__r.SVMX_Order__c;
                rmaLineIdList.add(tempLine.id);                
            }else{								
                System.enqueueJob(new COMM_SyncPartsOrderQueue(new SVMXC__RMA_Shipment_Order__c(id=rmaHeaderId),orderHeaderId,null,'Update', rmaLineIdList));                
                rmaLineIdList.clear();
                rmaHeaderId = tempLine.SVMXC__RMA_Shipment_Order__c;
                orderHeaderId = tempLine.SVMXC__RMA_Shipment_Order__r.SVMX_Order__c;                
                rmaLineIdList.add(tempLine.id);
            }
        }
        if(rmaLineIdList.size() > 0){
            System.enqueueJob(new COMM_SyncPartsOrderQueue(new SVMXC__RMA_Shipment_Order__c(id=rmaHeaderId),orderHeaderId,null,'Update', rmaLineIdList));
        }

    }
}