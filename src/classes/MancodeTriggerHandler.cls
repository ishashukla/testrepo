/**================================================================      
 * Appirio, Inc
 * Name: MancodeTriggerHandler 
 * Description: Handler Class for MancodeTrigger
 * Created Date: 18 July,2016
 * Created By: Prakarsh Jain (Appirio)
 * 
 * Date Modified      Modified By      Description of the update
 * 18 July 2016       Prakarsh Jain         Class creation.
 * 24 Aug  2016      Isha Shukla      Modified (T-525938) Added method updateOppSharing to update the sharing and populate manager once the mancode record is created or updated.
 *October 23,2016	 Sahil Batra		T-549184 - Added code to populate Mancode record Name - populateMancodeName
 ==================================================================*/
public class MancodeTriggerHandler {
  public static Id oppInstrumentsRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Opportunity Record Type').getRecordTypeId();
    public static Id oppInstrumentsClosedWonRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Instruments Closed Won Opportunity').getRecordTypeId();
  //Method to restrict user to create mancode record if business unit and sales rep are same and manager is different for newly created records.
 /* public static void restrictDuplicateMancodeRecord(List<Mancode__c> newList, Map<Id, Mancode__c> oldMap){
    List<Mancode__c> listMancode = new List<Mancode__c>();
    Map<String, Mancode__c> mapIdToMancode = new Map<String, Mancode__c>();
    Set<String> setBU = new Set<String>();
    Set<String> setSalesRep = new Set<String>();
    for(Mancode__c mancode : newList){
      setBU.add(mancode.Business_Unit__c);
      setSalesRep.add(mancode.Sales_Rep__c);
    }
    listMancode = [SELECT Id, Business_Unit__c, Manager__c, Sales_Rep__c FROM Mancode__c WHERE Business_Unit__c IN: setBU AND Sales_Rep__c IN: setSalesRep];
    if(listMancode.size()>0){
      for(Mancode__c mancd : listMancode){
        mapIdToMancode.put(mancd.Business_Unit__c + '+' + mancd.Sales_Rep__c, mancd);
      }
    }
    for(Mancode__c mancode : newList){
        System.debug('mapIdToMancode>>>'+mapIdToMancode);
      if(mapIdToMancode.containsKey(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c)){
          System.debug('mancode.Manager__c>>'+mancode.Manager__c);
          System.debug('mapIdToMancode.get(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c).Manager__c>>'+mapIdToMancode.get(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c).Manager__c);
          System.debug('mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c' + mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c);
        if(mancode.Manager__c != mapIdToMancode.get(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c).Manager__c){
          mancode.addError(Label.Mancode_Trigger_Error);
        }
      }
    }
    
  }
  //Method to pupulate Mancode name if Territory is populated.
  public static void populateMancodeName(List<Mancode__c> newList, Map<Id, Mancode__c> oldMap){
  	try{	
  		Set<Id> territoryIdSet = new Set<Id>();
  		Map<Id,String> territoryIdtoMancodeMap = new Map<Id,String>();
  		for(Mancode__c mancode : newList){
  			if(oldMap == null || (mancode.Territory__c != oldMap.get(mancode.Id).Territory__c)){
  				if(mancode.Territory__c != null){
  					territoryIdSet.add(mancode.Territory__c);
  				}
  			}
  		}
  		if(territoryIdSet.size() > 0){
  			for(Territory__c terr : [SELECT Id,Mancode__c FROM Territory__c WHERE Id IN :territoryIdSet AND Mancode__c != null]){
  				territoryIdtoMancodeMap.put(terr.Id,terr.Mancode__c);
  			}
  		}
  		for(Mancode__c mancode : newList){
  			if(oldMap == null || (mancode.Territory__c != oldMap.get(mancode.Id).Territory__c)){
  				if(mancode.Territory__c != null && territoryIdtoMancodeMap.containsKey(mancode.Territory__c)){
  					mancode.Name = territoryIdtoMancodeMap.get(mancode.Territory__c);
  				}
  			}
  		}
  	}catch(Exception ex){
       ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(),'MancodeTriggerHandler', true);
	}
  } */
  //Method to update Opportunity BU Manager field and Share Opportunity on insert and update of Mancode records.
  public static void updateOppSharing(List<Mancode__c> newList, Map<Id, Mancode__c> oldMap) {
    Map<String,Id> userBUToNewManagerId = new Map<String,Id>();
    Map<String,Id> userBUToOldManagerId = new Map<String,Id>();
    Set<Id> userId = new Set<Id>();
    for(Mancode__c mancode : newList) {
      if(oldMap != null && mancode.Manager__c != oldMap.get(mancode.Id).Manager__c) {
        userId.add(mancode.Sales_Rep__c);
        if(!userBUToNewManagerId.containsKey(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c)) {
          userBUToNewManagerId.put(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c,mancode.Manager__c);  
        }
        if(!userBUToOldManagerId.containsKey(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c)) {
          userBUToOldManagerId.put(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c,oldMap.get(mancode.Id).Manager__c);  
        }
      } else if(oldMap == null) {
        userId.add(mancode.Sales_Rep__c);
        if(!userBUToNewManagerId.containsKey(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c)) {
          userBUToNewManagerId.put(mancode.Business_Unit__c + '+' + mancode.Sales_Rep__c,mancode.Manager__c);  
        }
      }
    }
    List<OpportunityShare> sharingRecordToInsert = new List<OpportunityShare>();
    List<OpportunityShare> sharingRecordToDelete = new List<OpportunityShare>();
    List<Opportunity> listToUpdateBUManager = new List<Opportunity>();
    Set<Id> oppId = new Set<Id>();
    for(Opportunity opp : [SELECT Id,BU_Manager__c, OwnerId,Business_Unit__c 
                 FROM Opportunity 
                 WHERE OwnerId 
                 IN :userId 
                 AND (RecordTypeId = :oppInstrumentsRecTypeId OR RecordTypeId = :oppInstrumentsClosedWonRecTypeId)]) {
      oppId.add(opp.Id);
      if(userBUToNewManagerId.containsKey(opp.Business_Unit__c + '+' + opp.OwnerId)){
        OpportunityShare oppShare = new OpportunityShare();
          oppShare.userOrGroupId = userBUToNewManagerId.get(opp.Business_Unit__c + '+' + opp.OwnerId);
          oppShare.OpportunityAccessLevel = 'Edit';
          oppShare.RowCause = Schema.OpportunityShare.RowCause.Manual;
          oppShare.OpportunityId = opp.Id;
          sharingRecordToInsert.add(oppShare);
          opp.BU_Manager__c = userBUToNewManagerId.get(opp.Business_Unit__c + '+' + opp.OwnerId);
          listToUpdateBUManager.add(opp);
      }
      
    }
    if(userBUToOldManagerId.size() > 0) {
     for(OpportunityShare oppShare : [SELECT id,userOrGroupId, OpportunityId 
                       FROM OpportunityShare 
                       WHERE userOrGroupId 
                       IN:userBUToOldManagerId.values() 
                       AND OpportunityId IN: oppId]) {
      sharingRecordToDelete.add(oppShare);
     }
    }
    if(sharingRecordToDelete.size() > 0) {
      Database.delete(sharingRecordToDelete,false);
    }
    if(sharingRecordToInsert.size() > 0) {
      Database.insert(sharingRecordToInsert,false);
    }
    if(listToUpdateBUManager.size() > 0) {
      update listToUpdateBUManager;
    }
  }
 
}