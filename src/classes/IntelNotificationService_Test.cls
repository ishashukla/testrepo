/*******************************************************************************
 Author        :  Appirio JDC (Sonal Shrivastava)
 Date          :  Dec 19, 2013
 Purpose       :  Test Class for IntelNotificationService.cls
 Reference     :  Task T-205163
*******************************************************************************/
@isTest
private class IntelNotificationService_Test {
    static List<Intel__c> intelList;
  static testMethod void myUnitTest() {
    //Create test records
    List<User> userList = TestUtils.createUser(2, null, true);
    Intel__c intel1;
    Intel__c intel2;
    
    
    test.startTest();
    
    //List<Intel__c> intelList = TestUtils.createIntel(1, true);
   // intelList = [SELECT Id, Chatter_Post__c, CreatedDate, Mobile_Update__c FROM Intel__c WHERE Id IN :intelList];
  //  intel1 = intelList.get(0);      
   // List<FeedComment> commentList = TestUtils.createComment(1, intelList.get(0).Chatter_Post__c, true);
      
    System.runAs(userList.get(0)){
      intelList = TestUtils.createIntel(1, true);
      intelList = [SELECT Id, Chatter_Post__c, CreatedDate, Mobile_Update__c FROM Intel__c WHERE Id IN :intelList];
      intel1 = intelList.get(0);
      List<FeedComment>commentList = TestUtils.createComment(1, intelList.get(0).Chatter_Post__c, true);
    }
      System.runAs(userList.get(1)){
    intel2 = intelList.get(0);     
    List<FeedComment> commentList = TestUtils.createComment(1, intel2.Chatter_Post__c, true);
    intelList = [SELECT Id, Chatter_Post__c, CreatedDate, Mobile_Update__c FROM Intel__c WHERE Id IN :intelList];
    intel2 = intelList.get(0); 
    
      commentList = TestUtils.createComment(1, intel2.Chatter_Post__c, true);
    }
       
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    
    req.requestURI = '/services/apexrest/IntelNotificationService/*';
    req.httpMethod = 'GET';
      
    String[] arr = String.valueOf(intel1.CreatedDate).split(' ');
    String dt = arr[0] + 'T' + arr[1] + '.000Z';
    req.addParameter('timeStamp', dt); 
      
    RestContext.request = req;
    RestContext.response = res;
     
    IntelNotificationService.IntelNotificationWrapper result1 = IntelNotificationService.doGet();
    //System.assertNotEquals(null, result1.intelList);
    //System.assertNotEquals(0, result1.intelList.size());    
    
    test.stopTest();
  }
}