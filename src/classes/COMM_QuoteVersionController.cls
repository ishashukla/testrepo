/*
Name            COMM_QuoteVersionController
Created Date    05/05/2016
Created By      Nitish Bansal
Purpose         I-216827 (To show different revisions of the quote with same number in an inline VF page)
*/
public class COMM_QuoteVersionController {

    public BigMachines__Quote__c quote{get;set;} 
    public Id selectedID{get;set;}
    public String newDocumentURL{get;set;}
    public COMM_Conga_Setting__c congaSetting{get;set;}
    public List<BigMachines__Quote__c> quoteListToShow{get;set;}
    public List<BigMachines__Quote__c> quoteList{get;set;}

    public String sortField{get;set;}
    public String order{get;set;}
    public Boolean isAsc{get;set;}
    String previousSortField{get;set;}

    public Integer listSize {get;set;}
    public Integer pageSize{get;set;}
    public Integer pageNumber{get;set;}

    public COMM_QuoteVersionController(ApexPages.StandardController std) {
        quote = (BigMachines__Quote__c)std.getRecord();      
        //documentListToShow = new List<Document__c>();
        quoteList = getQuoteList();
        pageSize = (Integer)COMM_Constant_Setting__c.getOrgDefaults().Quote_Document_Pagesize__c;
        pageNumber = 1;
        getQuoteListToShow();
        isAsc = true;
        previousSortField = sortField = 'Name';
    }

    public List<BigMachines__Quote__c> getQuoteList(){
        quoteList = new List<BigMachines__Quote__c>();
        quoteListToShow = new List<BigMachines__Quote__c>();
       
        BigMachines__Quote__c quoteTemp = [SELECT BigMachines__Opportunity__c, Id, Name, Revision_Number__c, BigMachines__Description__c, 
                                            BigMachines__Status__c, BigMachines__Total__c, BigMachines__Is_Primary__c, Budgetary_Quote__c, 
                                            Unverified__c, SAP_Order_Number__c
                                            FROM BigMachines__Quote__c WHERE id =: quote.id];
        
        String query = 'SELECT BigMachines__Opportunity__c, Id, Name, Revision_Number__c, BigMachines__Description__c,'
                        + ' BigMachines__Status__c, BigMachines__Total__c, BigMachines__Is_Primary__c, Budgetary_Quote__c, '
                        + ' Unverified__c, SAP_Order_Number__c'
                        + ' FROM BigMachines__Quote__c '
                        + ' WHERE '
                        + ' Name =  \'' + quoteTemp.Name + '\''
                        + ' AND Id != \'' + quote.Id + '\''; 
                        
                        
        if(quoteTemp.BigMachines__Opportunity__c != null) {
          query += ' AND BigMachines__Opportunity__c = \'' + quoteTemp.BigMachines__Opportunity__c + '\'';
        }
        
        query += ' ORDER BY CreatedDate DESC';
        
        List<BigMachines__Quote__c> doclist = Database.query(query);
        if(doclist != null){
          listSize = docList.size();
        }
        return doclist;
    }

    //----------------------------------------------------------------------------
      // Name : doDelete() Method to delete selected quote
      // Params : None
      // Returns : list of quotes
      //----------------------------------------------------------------------------
      public void doDelete(){
        try{
          List<BigMachines__Quote__c> docList = [SELECT Id 
                                      FROM BigMachines__Quote__c 
                                      WHERE ID = :selectedID];
          if(docList != null && docList.size() > 0){
              delete docList;
          }
          quoteList = getQuoteList();
          pageNumber = 1;
          getQuoteListToShow();
        }Catch(DmlException ex){
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Unable to Delete this record!'));
        }
      }

    //Pagination methods
  
      public Boolean hasPrevious{get{
          if(PageNumber == 1){
              return false;
          }else{
              return true;
          }
      }set;}
      
      public Boolean hasNext{get{
          if(PageNumber == 1){
              if((listSize / pageSize  > 1)
              || (((listSize / pageSize ) == 1)
              && (math.mod(listSize, pageSize) != 0))){
                  system.debug('Return FROM Here');
                  return true;
              }else{
                  system.debug('Return FROM tHere');
                  return false;
              }
          }else{
              if((pageNumber > (listSize / pageSize))
              || (pageNumber == (listSize / pageSize)
              && (math.mod(listSize, pageSize) == 0))){
                  system.debug('Return FROM Hereee');
                  return false;
              }else{
                  system.debug('Return FROM Here1');
                  return true;
              }
          }
      }set;}
      
      public void next() 
      {
          pageNumber++;
          getQuoteListToShow();
      }
      
      public void previous() 
      {
          pageNumber--;
          getQuoteListToShow();
      }
      
      public void first() 
      {
          pageNumber = 1;
          getQuoteListToShow();
      }
      
      
      public void Last() 
      {
        if(Math.mod(listSize,pageSize) == 0){
          pageNumber = listSize / pageSize;
        }else{
          if(listSize / pageSize >= 1){
            pageNumber = listSize / pageSize + 1;
          }else{
            pageNumber = 1;
          }
        }
        getQuoteListToShow();
      }  

    public List<BigMachines__Quote__c> getQuoteListToShow() {
        quoteListToShow = new List<BigMachines__Quote__c>();
        if(quoteList != null && quoteList.size() > 0){
          if(pageNumber == 1){
            if(quoteList.size() < pagesize){
              for(Integer i=0; i<quoteList.size();i++){
                  quoteListToShow.add(quoteList.get(i));
              }
            }else{
              for(Integer i=0; i<pagesize;i++){
                  quoteListToShow.add(quoteList.get(i));
              }
            }
            system.debug('in if');
          }else{
            if((pageNumber-1) == (quoteList.size() / pageSize)){
              system.debug('I am in second If');
              for(Integer i= ((pageNumber * pageSize) - pageSize); i < quoteList.size(); i++){
                      quoteListToShow.add(quoteList.get(i));
              }
            }else{
              for(Integer i= ((pageNumber * pageSize) - pageSize); i<(pageNumber * pageSize);i++){
                  quoteListToShow.add(quoteList.get(i));
              }
            }
          }
        }
        return quoteListToShow;
    }  

    public void sortList(){
        system.debug('Sort Field : ' + sortField + ':::order::' + order + 'previousSortField' + previousSortField);
        
        isAsc = previousSortField.equals(sortField)? !isAsc : true; 
        previousSortField = sortField;
        order = isAsc ? 'ASC' : 'DESC';
        List<BigMachines__Quote__c> resultList = new List<BigMachines__Quote__c>();
        //Create a map that can be used for sorting 
        Map<object, List<BigMachines__Quote__c>> objectMap = new Map<object, List<BigMachines__Quote__c>>();
        for(BigMachines__Quote__c ob : quoteListToShow){
          if(sortField == 'LastModifiedBy'){
            if(objectMap.get(ob.LastModifiedBy.Name) == null){
              objectMap.put(ob.LastModifiedBy.Name, new List<Sobject>()); 
            }
            objectMap.get(ob.LastModifiedBy.Name).add(ob);
          }else{
            if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
                objectMap.put(ob.get(sortField), new List<Sobject>()); 
            }
            objectMap.get(ob.get(sortField)).add(ob);
          }
        }       
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();
        for(object key : keys){ 
          resultList.addAll(objectMap.get(key)); 
        }
        //Apply the sorted values to the source list
        quoteListToShow.clear();
        if(order.toLowerCase() == 'asc'){
          quoteListToShow.addAll(resultList);
        }else if(order.toLowerCase() == 'desc'){
          for(integer i = resultList.size()-1; i >= 0; i--){
            quoteListToShow.add(resultList[i]);  
          }
        }
    }
}