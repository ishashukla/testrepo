/**================================================================      
* Appirio, Inc
* Name: COMM_CheckListDetailTriggerHandler
* Description: Update Header status to In Progress after first question is answered
* Created Date: 21-Sep-2016(T-536950)
* Created By: Shubham Dhupar (Appirio)
*

* 26th Oct 2016     Nitish Bansal  I-241409 // Added proper null & list size checks before SOQL and DML statements
==================================================================*/
public class COMM_CheckListDetailTriggerHandler {
  public static void afterUpdate(List<Checklist_detail__c> newList) {
    updateHeaderStatus(newList);
  }
  //============================================================================================     
  // Name         : updateHeaderStatus
  // Description  : To update header status to in Progress after first question is Answered
  // Created Date : 26th Sep 2016 
  // Created By   : Shubham Dhupar (Appirio)
  // Task         : T-536950
  //============================================================================================
  public static void updateHeaderStatus(List<Checklist_detail__c> newList) {
    Set<Id> heaaderIds = new Set<Id>();
    for(Checklist_detail__c detail: newList) {
      if((detail.Comment__c != NULL || detail.Result__c != NULL) && detail.Checklist_Header__c != null) { //NB - 10/26 - I-241409 -  Added Checklist_Header__c null check
        heaaderIds.add(detail.Checklist_Header__c);
      }
    }
    if(heaaderIds.size() > 0){ //NB - 10/26 - I-241409
      Map<ID, Checklist_Header__c> headerMapToUpdate = new Map<Id, Checklist_Header__c>();
      for(Checklist_Header__c header : [Select id,Status__c, Evaluate_Result__c
                                          From Checklist_Header__c
                                          where id in:heaaderIds]) {
        if(header.Status__c =='New') {
          //system.debug('Inside first If');
          header.Status__c ='In Progress';
          headerMapToUpdate.put(header.id, header);
          //system.debug('Status   ' +  header.Status__c);
        }
        if(!header.Evaluate_Result__c){
          if(headerMapToUpdate.containsKey(header.id)){
            headerMapToUpdate.get(header.id).Evaluate_Result__c = true;
          }else{
            header.Evaluate_Result__c = true;
            headerMapToUpdate.put(header.id, header);
          }
          break;
        }
      }
      if(headerMapToUpdate.size() > 0){ //NB - 10/26 - I-241409
        update headerMapToUpdate.values();
      }
    }
  }
}