//
// (c) 2016 Appirio, Inc.
//
// Name : COMM_CaseLineItemTriggerHandlerTest 
// Used to Test trigger handler class "COMM_CaseLineItemTriggerHandler"
//
// 20th April 2016     Kirti Agarwal      Original(I-214565)
//

/*
 Modified Date			Modified By				Purpose
 06-09-2016				Chandra Shekhar			To Fix Test Class
*/

@isTest
private class COMM_CaseLineItemTriggerHandlerTest {

  @isTest
  static void COMM_CaseLineItemTriggerHandlerTest() {
  
    List<Case_Line_Item__c > lineItemList = new List<Case_Line_Item__c >();
    Case_Line_Item__c casItem;
    
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM Change Request');
    Id recordTypeId = rtByName.getRecordTypeId(); 
    
    Id pricebookId = Test.getStandardPricebookId();
    
    Product2 prd1 = new Product2(Name = 'Test Product Entry 1', Description = 'Test Product Entry 1', isActive = true);
    insert prd1;

    PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prd1.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
    Pricebook2 customPB = new Pricebook2(Name='COMM Price Book', isActive=true);
    insert customPB;
    
    PricebookEntry pe = new PricebookEntry(UnitPrice = 1, Product2Id = prd1.id, Pricebook2Id = customPB.id, isActive = true);
    insert pe;
    
    
    Case objCase = new Case();
    objCase.Symptom_Type__c = 'Physical Damage'; 
    objCase.recordTypeId = recordTypeId;
    objCase.CreatedDate = date.today();
    
    //Modified By Chandra Shekhar Sharma to add record types on case Object
	Schema.RecordTypeInfo rtByName1 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('COMM US Stryker Field Service Case');
	TestUtils.createRTMapCS(rtByName1.getRecordTypeId(), 'COMM US Stryker Field Service Case', 'COMM');
	objCase.recordTypeId = rtByName1.getRecordTypeId();
	
    insert objCase;
    
    //COMM_CaseLineItemTriggerHandler.COMM_PRICE_BOOK = 'Standard Price Book';
    
    
    for(Integer i = 0; i < 100; i++) {
      casItem = new Case_Line_Item__c (Change_Order__c = objCase.id, Part__c = prd1.id, Quantity__c = 2);
      lineItemList.add(casItem);
    }
    
    insert lineItemList;
    for(Case_Line_Item__c  casIt : [SELECT Id, Price__c FROM Case_Line_Item__c  WHERE Id IN :lineItemList]) {
      System.assertEquals(1, casIt.Price__c);
    }
  }
  
}