/**================================================================      
* Appirio, Inc
* Name: COMM_HomePageAuraComponentController
* Description: I-236389 - Controller class of COMM_HomePageAuraComponent
* Created Date: 7th Oct 2016
* Created By: Kanika Mathur (Appirio)
*
* Date Modified      Modified By      Description of the update
* 04+08 Nov 2016       Ralf HOffmann   I-242838 - updated aggregate result to get the yearly quota from the yearly forecast object. 
* 10 Nov 2016         Nitish Bansal    I-243430 - Show Stryker Pulse URL
* 24 Nov 2016         Nitish Bansal    I-245226 - Quota value inaccurate
==================================================================*/ 
public class COMM_HomePageAuraComponentController {
    //static AggregateResult[] groupedResults= [SELECT SUM(Total_Sales__c)sales, SUM(Quota__c)quotas FROM Monthly_Forecast__c WHERE Is_User_And_Current_Year_Data__c = 1];
    @AuraEnabled
    public static List<String> getTotalSales() {
        List<String> stringList = new List<String>();
        String temp, show='false';
        //AggregateResult[] groupedResults= [SELECT SUM(Total_Sales__c)sales, SUM(Quota__c)quotas FROM Monthly_Forecast__c WHERE Is_User_And_Current_Year_Data__c = 1];
        //RH 11/4+8/2016 ; I-242838; changed to get the quota from the yearly quota instead of the sum of monthly
        AggregateResult[] groupedResults= [SELECT SUM(Total_Sales__c)sales, MAX(Forecast_Year__r.YTD_Quota__c)quotas FROM Monthly_Forecast__c WHERE Is_User_And_Current_Year_Data__c = 1]; //NB - 11/24 - I-245226
        if(groupedResults != null && groupedResults.size() > 0) {
            if(groupedResults[0].get('quotas') != null && groupedResults[0].get('quotas') != 0) {
                Integer str = Integer.valueOf(groupedResults[0].get('quotas'));
                stringList.add(String.valueOf(str.format()));
            }
            if(groupedResults[0].get('sales') != null && groupedResults[0].get('sales') != 0) {
                Integer str = Integer.valueOf(groupedResults[0].get('sales'));
                //String str1 = String.valueOf(str.format());
                stringList.add(String.valueOf(str.format()));
            }
            if(groupedResults[0].get('quotas') != null && groupedResults[0].get('quotas') != 0) {
                temp = ((Decimal.valueOf(Integer.valueOf(groupedResults[0].get('sales')))/Decimal.valueOf(Integer.valueOf(groupedResults[0].get('quotas'))))*100).setScale(2) +'';
                stringList.add(temp); 
            }
            if((groupedResults[0].get('quotas') != null && groupedResults[0].get('quotas') != 0) || (groupedResults[0].get('sales') != null && groupedResults[0].get('sales') != 0)) {
                show = 'true';
                stringList.add(show);
            }
            
        } 
        if(stringList.size() == 0){
            stringList.add('');
            stringList.add('');
            stringList.add('');
            stringList.add('');
        }
        return stringList;
    }
    
    //NB - 11/10- I-243430 - Show Stryker Pulse URL
    @AuraEnabled
    public static String getStrykerPulseiUrl() {
        return (System.Label.StrykerPulseiFrame);
    }    
}