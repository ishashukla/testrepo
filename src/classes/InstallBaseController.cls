//
// (c) 2014 Appirio, Inc.
// Class InstallBaseController
// Controller class for InstallBase Page
//
// 01 Mar 2016	   Sahil Batra		Original(T-481991)
public with sharing class InstallBaseController {
	public Id recId{get;set;}
	public InstallBaseController(ApexPages.StandardController stdController){
		if(ApexPages.currentPage().getParameters().get('id') != null){
			recId = ApexPages.currentPage().getParameters().get('id');
		}
		else if(ApexPages.currentPage().getParameters().get('accid') != null){
			recId = ApexPages.currentPage().getParameters().get('accid');
		}
	}

}