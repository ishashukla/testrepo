// 
// (c) 2016 Appirio, Inc.
//
// Test class of COMM_ContactOpptyCreationController apex class
//
// 22 Mar 2016     Kirti Agarwal   Original(T-483770)
//
@isTest
private class COMM_ContactOpptyCreationControllerTest {

  @isTest
  static void COMM_ContactOpptyCreationControllerTest() {
    List < Account > accountList = TestUtils.createAccount(1, true);
    List < Contact > contactList = TestUtils.createContact(1, accountList[0].id, false);
    List < Pricebook2 > priceBookList = TestUtils.createpricebook(1, ' ',false);
    priceBookList.get(0).Name = 'COMM Price Book';
    insert priceBookList;
    List < Opportunity > oppList = TestUtils.createOpportunity(1, accountList[0].id, true);
    Id recordTypeId = Schema.SObjectType.Related_Contact__c.getRecordTypeInfosByName().get(Constants.ContactAssocRT).getRecordTypeId();
    PageReference curentPage= Page.COMM_ContactOpptyCreationPage;
    Test.setCurrentPage(curentPage);
    ApexPages.currentPage().getParameters().put('retUrl', '/' + oppList[0].id);
    ApexPages.currentPage().getParameters().put('RecordType',recordTypeId);
    system.assert(oppList[0].id != null);
    TestUtils.createCommConstantSetting();
    ApexPages.StandardController cntrl = new ApexPages.StandardController(new Related_Contact__c());
    COMM_ContactOpptyCreationController obj = new COMM_ContactOpptyCreationController(cntrl);
    obj.saveJunctionObject();
    obj.cancel();
    System.assert(obj.junctionContactOpp != null);
    obj.oppId = '';
    obj.cancel();
    obj.contactName = contactList[0].id;
    obj.junctionContactOpp.Opportunity__c = oppList[0].id;
    obj.saveJunctionObject();
    obj.cancel();
    System.assert(obj.junctionContactOpp.id != null);
  }
}