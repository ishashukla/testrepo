// (c) 2016 Appirio, Inc. 
//
// Class Name: COMM_CreateStdOrderForPOQueueTest -  Test class 
//
// 2 Nov 2016, Isha Shukla (T-550285) 
@isTest
private class COMM_CreateStdOrderForPOQueueTest {
    static Account acc;
    static List<SVMXC__RMA_Shipment_Order__c> partsOrderList;
    @isTest(SeeAllData=true)
    static void testSchedulablePartsOrder() {
        createData();
        Test.startTest();
        System.enqueueJob(new COMM_CreateStdOrderForPartsOrderQueue(partsOrderList[0])); 
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Order WHERE AccountId = :acc.Id].size() > 0, true);
    }
    private static void createData() {
        acc = TestUtils.createAccount(1, true).get(0);
        Map<String, Schema.RecordTypeInfo> caseRecordTypeInfo =
            Schema.SObjectType.Case.getRecordTypeInfosByName();
        String caseRecordTypeId = caseRecordTypeInfo.get('COMM Change Request').getRecordTypeId();
        Map<String, Schema.RecordTypeInfo> partOrderInfo =
            Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
        String PORecordTypeId = partOrderInfo.get('RMA').getRecordTypeId();
        if(!(RTMap__c.getInstance(PORecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = PORecordTypeId,
                                          Object_API_Name__c='SVMXC__RMA_Shipment_Order__c',
                                          Division__c = 'COMM',
                                          RT_Name__c='RMA');
            insert rtMap;
        }
        if(!(RTMap__c.getInstance(caseRecordTypeId).Division__c.containsIgnoreCase('COMM'))) {
            RTMap__c rtMap = new RTMap__c(Name = caseRecordTypeId,
                                          Object_API_Name__c='Case',
                                          Division__c = 'COMM',
                                          RT_Name__c='COMM Change Request');
            insert rtMap;
        }
        Case caseRecord = new Case(RecordTypeId = caseRecordTypeId,Accountid = acc.Id);
        insert caseRecord;
        system.debug('>>>>caseacc='+caseRecord.Accountid);
        // Contact cnct = TestUtils.createContact(1, acc.Id, true).get(0);
        Product2 productRecord = TestUtils.createCOMMProduct(1, false).get(0);
        productRecord.QIP_ID__c = null;		
        insert productRecord;
        SVMXC__Site__c DestiLocation = new SVMXC__Site__c(Location_ID__c = '9845');
        insert DestiLocation;
        partsOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
        for(Integer i = 0; i < 2; i++){
            SVMXC__RMA_Shipment_Order__c stock = new SVMXC__RMA_Shipment_Order__c(
                SVMXC__Case__c = caseRecord.Id,SVMXC__Order_Status__c = 'Closed',
                SVMXC__Company__c = acc.Id,RecordTypeId = PORecordTypeId,SVMXC__Destination_Location__c = DestiLocation.Id
            );
            partsOrderList.add(stock);
        }
        insert partsOrderList;
        system.debug('debugpartorder?>>>'+partsOrderList);
        system.debug('queried>>'+[select SVMXC__Case__r.Accountid from SVMXC__RMA_Shipment_Order__c where id=:partsOrderList[0].Id].SVMXC__Case__r.Accountid);
        List<SVMXC__RMA_Shipment_Line__c> lineList = new List<SVMXC__RMA_Shipment_Line__c>();
        for(Integer i = 0; i < 2; i++){
            SVMXC__RMA_Shipment_Line__c line = new SVMXC__RMA_Shipment_Line__c(
                SVMXC__Product__c = productRecord.Id,
                SVMXC__RMA_Shipment_Order__c = partsOrderList[i].Id,
                SVMXC__Expected_Quantity2__c = 1
            );
            lineList.add(line);
        }
        insert lineList;
        
        Address__c addr= new Address__c(account__c=acc.Id,
                                        type__C='Billing',
                                        Location_ID__c='568',
                                        Business_Unit__c='88',
                                        Primary_Address_Flag__c =true);
        insert addr;
        Address__c addr2= new Address__c(account__c=acc.Id,
                                         type__C='Shipping',
                                         Location_ID__c='9845',
                                         Business_Unit__c='88');
        insert addr2;
        
        Id standardPBId = Test.getStandardPricebookId();
        Pricebook2 pb = [SELECT Id from Pricebook2 where Name = 'COMM Price Book'];
        if(pb == null) {
            pb = new Pricebook2(name = 'COMM Price Book',isActive = true);
            insert pb; 
        }
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = productRecord.Id, UnitPrice = 1000, IsActive = true);
        insert pbe;
        
    }
}