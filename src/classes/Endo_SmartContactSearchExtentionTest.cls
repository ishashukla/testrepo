/*******************************************************************
Name  : Endo_SmartContactSearchExtentionTest
Author: Sunil Gupta
Date  : March 20, 2014

 Modified Date			Modified By				Purpose
 05-09-2016				Chandra Shekhar			To Fix Test Class
*************************************************************************/
@isTest
private class Endo_SmartContactSearchExtentionTest {
  static testMethod void myUnitTest() {
    Account acc = Endo_TestUtils.createAccount(1, false).get(0);
    insert acc;

    Contact con = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con.FirstName = 'test';
    con.LastName = 'test';
    con.Email = 'test@test.com';
    insert con;

    Contact con2 = Endo_TestUtils.createContact(1, acc.Id, false).get(0);
    con2.FirstName = 'test';
    con2.LastName = 'test';
    con2.Email = 'test@test.com';
    insert con2;

    Contact_Acct_Association__c ca = new Contact_Acct_Association__c();
    ca.Associated_Account__c = acc.Id;
    ca.Associated_Contact__c = con.Id;
    ca.Primary__c = true;
    insert ca;


    ApexPages.currentPage().getParameters().put('accId', acc.Id);
    Endo_SmartContactSearchExtention controller = new Endo_SmartContactSearchExtention(new ApexPages.StandardController(con));
    controller.contactFirstNameToSearch = 'test';
    controller.contactLastNameToSearch = 'test';
    controller.contactEmail = 'test@test.com';


    //System.assertEquals(1,controller.showingFrom);
    //System.assertEquals(20,controller.showingTo);
    //System.assertEquals(3,controller.totalPage);
    //System.assertEquals(true,controller.hasNext);
    //System.assertEquals(false,controller.hasPrevious);
    //controller.nextContactPage();

    //System.assertEquals(21,controller.showingFrom);
    //System.assertEquals(40,controller.showingTo);
    //System.assertEquals(3,controller.totalPage);
    //System.assertEquals(true,controller.hasNext);
    //System.assertEquals(true,controller.hasPrevious);
    //controller.previousContactPage();

    //System.assertEquals(41,controller.showingFrom);
    //System.assertEquals(55,controller.showingTo);
    //System.assertEquals(3,controller.totalPage);

    // Verify Cancel action
    PageReference prCancel = controller.cancel();
    System.assert(prCancel == null);

    // Verify Account association with Contact
    controller.selectedContactId = con2.Id;
    controller.showAssociatedAccounts();

    controller.performSearch();

    List<Contact_Acct_Association__c> lstAssociation = [SELECT Id FROM Contact_Acct_Association__c
                                                        WHERE Associated_Contact__c = :con.Id];
    System.assert(lstAssociation.size() > 0);

    //Modified By Chandra Shekhar Sharma to add record types on case Object
    Schema.RecordTypeInfo rtByName = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Inquiry & Requests');
	TestUtils.createRTMapCS(rtByName.getRecordTypeId(), 'Customer Inquiry & Requests', 'Endo');
	
	// Verify Case Action
    controller.createCase();
    System.assert([SELECT Id FROM Case WHERE AccountId = :acc.Id AND ContactId = :con2.Id].size() > 0);


    List<Endo_SmartContactSearchExtention.ContactWrapper> lst = controller.getWrapperList();

    //Added for S-397689
    ApexPages.Message[] pageMessages = ApexPages.getMessages();
	System.assertNotEquals(0, pageMessages.size());

	// Check that the error message you are expecting is in pageMessages
	Boolean messageFound = false;

	for(ApexPages.Message message : pageMessages) {
	    if(message.getSummary() == Constants.DEACTIVE_ACCOUNT_ERROR
	        && message.getSeverity() == ApexPages.Severity.ERROR) {
	        messageFound = true;
	    }
	}

	System.assert(messageFound);
	//end S-397689


  }

}