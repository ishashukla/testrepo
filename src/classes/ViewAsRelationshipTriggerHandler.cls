/**================================================================      
 * Appirio, Inc
 * Name: ViewAsRelationshipTriggerHandler
 * Description: Handler class for ViewAsRelationshipTrigger(T-538445), added Method shareASRUserRecords() and deleteASRUserRecords
 * Created Date: [09/20/2016]
 * Created By: [Prakarsh Jain] (Appirio)
 * Date Modified      Modified By      Description of the update
 * Oct 12, 2016       Shreerath Nair   Implemented new Trigger framework(T-542899)
 ==================================================================*/
public class ViewAsRelationshipTriggerHandler{


  /**================================================================      
  * Standard Methods from the ViewAsRelationshipTriggerHandler
  ==================================================================*/  
  
  public static boolean IsActive(){
      return TriggerState.isActive('ViewAsRelationshipTrigger');
      
  }
  
  public void OnBeforeInsert(List<ASR_Relationship__c> newList){}
  public void OnAfterInsert(List<ASR_Relationship__c> newList){
  
      //Method to add ASR User to Value Add Activities Records when View As Relationship record in inserted.
      shareASRUserRecords(newList);    
  }
    
  public void OnBeforeUpdate(List<ASR_Relationship__c> newList, Map<ID, ASR_Relationship__c> oldMap){}
  public void OnAfterUpdate(List<ASR_Relationship__c> newList, Map<ID, ASR_Relationship__c> oldMap){}
    
  public void OnBeforeDelete(List<ASR_Relationship__c> oldList, Map<ID, ASR_Relationship__c> oldMap){
  
      //Method to delete ASR User to Value Add Activities Records when View As Relationship record in delete.
      deleteASRUserRecords(oldList);
  }
  /**================================================================      
  * End of Standard Methods from the ViewAsRelationshipTriggerHandler
  ==================================================================*/
  
  /**================================================================      
  * Trigger Call Methods from the ViewAsRelationshipTrigger 
  ==================================================================*/
  
  public void beforeInsert(List<ASR_Relationship__c> newList){
      // Org Wide functionality
      OnBeforeInsert(newList);
  }
    
  public void afterInsert(List<ASR_Relationship__c> newList){
      // Org Wide functionality
      OnAfterInsert(newList);
  }
    
  public void beforeUpdate(List<ASR_Relationship__c> newList, Map<ID, ASR_Relationship__c> oldMap){
      // Org Wide functionality
      OnBeforeUpdate(newList, oldMap);
  }
    
  public void afterUpdate(List<ASR_Relationship__c> newList, Map<ID, ASR_Relationship__c> oldMap){
      // Org Wide functionality
      OnAfterUpdate(newList, oldMap);
  }
    
  public void beforeDelete(List<ASR_Relationship__c> oldList, Map<ID, ASR_Relationship__c> oldMap){
      // Org Wide functionality
      OnbeforeDelete(oldList, oldMap);

  }
  /**================================================================      
  * End of Trigger Call Methods from the ViewAsRelationshipTrigger 
  ==================================================================*/


  //Method to add ASR User to Value Add Activities Records when View As Relationship record in inserted.
  public static void shareASRUserRecords(List<ASR_Relationship__c> newList){
    try{
      Map<Id,Id> salesRepIdToASRUserId = new Map<Id,Id>();
        Set<Id> salesRepIdsSet = new Set<Id>();
        List<Value_Add_Activities__Share> valueAddActivitiesShareList = new List<Value_Add_Activities__Share>();
        List<Value_Add_Activities__c> valueAddActivityList = new List<Value_Add_Activities__c>();
        for(ASR_Relationship__c viewAs : newList){
          salesRepIdsSet.add(viewAs.ASR_User__c);
        }
        if(salesRepIdsSet.size()>0){
          for(ASR_Relationship__c viewAsUser : [SELECT Id, Name, Mancode__c, ASR_User__c, Mancode__r.Sales_Rep__c, Mancode__r.Type__c 
                                                FROM ASR_Relationship__c 
                                                WHERE ASR_User__c IN: salesRepIdsSet AND Mancode__r.Type__c = 'ASR']){
            if(!salesRepIdToASRUserId.containsKey(viewAsUser.ASR_User__c)){
              salesRepIdToASRUserId.put(viewAsUser.ASR_User__c, viewAsUser.Mancode__r.Sales_Rep__c);
            }                                        
          }
        }
        for(Value_Add_Activities__c value : [SELECT Id, Name, OwnerId FROM Value_Add_Activities__c WHERE OwnerId IN: salesRepIdsSet]){
          if(salesRepIdToASRUserId.size()>0){
            if(salesRepIdToASRUserId.containsKey(value.OwnerId)){
              Value_Add_Activities__Share valShare = new Value_Add_Activities__Share(UserOrGroupId = salesRepIdToASRUserId.get(value.OwnerId), 
                                                                                     RowCause = Schema.Value_Add_Activities__Share.RowCause.Manual,
                                                                                     ParentId = value.Id, AccessLevel = 'Edit');
              valueAddActivitiesShareList.add(valShare);
            }
          }
        }
        if(valueAddActivitiesShareList.size()>0){
          insert valueAddActivitiesShareList; 
        }
    }catch(Exception ex){ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'ViewAsRelationshipTriggerHandler', true);}
  }
  
  //Method to delete ASR User to Value Add Activities Records when View As Relationship record in delete.
  public static void deleteASRUserRecords(List<ASR_Relationship__c> oldList){ 
    try{
      Map<Id,Id> salesRepIdToASRUserId = new Map<Id,Id>();
        Set<Id> salesRepIdsSet = new Set<Id>();
        Set<Id> ASRUserIdsSet = new Set<Id>();
        List<Value_Add_Activities__Share> valueAddActivitiesShareList = new List<Value_Add_Activities__Share>();
        for(ASR_Relationship__c viewAs : oldList){
          salesRepIdsSet.add(viewAs.ASR_User__c);
        }
        if(salesRepIdsSet.size()>0){
          for(ASR_Relationship__c viewAsUser : [SELECT Id, Name, Mancode__c, ASR_User__c, Mancode__r.Sales_Rep__c, Mancode__r.Type__c 
                                                FROM ASR_Relationship__c 
                                                WHERE ASR_User__c IN: salesRepIdsSet AND Mancode__r.Type__c = 'ASR']){
            if(!salesRepIdToASRUserId.containsKey(viewAsUser.ASR_User__c)){
              salesRepIdToASRUserId.put(viewAsUser.ASR_User__c, viewAsUser.Mancode__r.Sales_Rep__c);
              ASRUserIdsSet.add(viewAsUser.Mancode__r.Sales_Rep__c);
            }                                        
          }
        }
        if(salesRepIdsSet.size()>0){
          for(Value_Add_Activities__Share valShare : [SELECT UserOrGroupId, RowCause FROM Value_Add_Activities__Share 
                                                      WHERE RowCause =: Schema.Value_Add_Activities__Share.RowCause.Manual
                                                      AND UserOrGroupId IN: ASRUserIdsSet]){
            valueAddActivitiesShareList.add(valShare);
          }
        }
        if(valueAddActivitiesShareList.size()>0){
            delete valueAddActivitiesShareList;
        }
    }catch(Exception ex){ErrorLogUtility.createErrorRecord(ex.getMessage(), ex.getTypeName(), ex.getLineNumber(), ex.getStackTraceString(), 'ViewAsRelationshipTriggerHandler', true);}
  }
}