/*******************************************************************************
Created By:    Sunil Gupta
Date:          March 18, 2014
Description  : Controller class for endo_createContact
*******************************************************************************/

public with sharing class Endo_createContactController {
  
  // Public Varibales.
  public Contact newContact{get;set;} 
  
  //-----------------------------------------------------------------------------------------------
  // Constructor
  //-----------------------------------------------------------------------------------------------
  public Endo_createContactController(ApexPages.StandardController sc){
    //newContact = (Contact)sc.getRecord();
    newContact = new Contact();
    String accountId = ApexPages.currentPage().getParameters().get('accId');
    String firstName = ApexPages.currentPage().getParameters().get('name_firstcon2');
    String lastName = ApexPages.currentPage().getParameters().get('name_lastcon2');
    String email = ApexPages.currentPage().getParameters().get('con15');
    Account currentAccount;
    if(String.isBlank(accountId) == false){
        currentAccount = [SELECT Preferred_Shipping_Method__c, Preferred_Repair_Shipping_Method__c, ShippingStreet, ShippingStateCode, ShippingState, ShippingPostalCode, ShippingCountryCode, ShippingCountry, ShippingCity 
                           FROM Account WHERE Id = :accountId].get(0); 
    }
    
    IF(String.isBlank(accountId) == false){
      newContact.AccountId = accountId;    
    }
    IF(String.isBlank(firstName) == false){
      newContact.FirstName = firstName;    
    }
    IF(String.isBlank(lastName) == false){
      newContact.LastName = lastName;    
    }
    IF(String.isBlank(email) == false){
      newContact.Email = email;
      newContact.Order_Notification_Email__c = email;    
    }
    newContact.MailingStreet = currentAccount.ShippingStreet;
    newContact.MailingStateCode = currentAccount.ShippingStateCode;
    newContact.MailingState = currentAccount.ShippingState;
    newContact.MailingPostalCode = currentAccount.ShippingPostalCode;
    newContact.MailingCountryCode = currentAccount.ShippingCountryCode;
    newContact.MailingCountry = currentAccount.ShippingCountry;
    newContact.MailingCity = currentAccount.ShippingCity;
    
    newContact.Preferred_Shipping_Method__c = currentAccount.Preferred_Shipping_Method__c;
    newContact.Preferred_Repair_Shipping_Method__c = currentAccount.Preferred_Repair_Shipping_Method__c;
    //newContact = [SELECT Id, Name, ODP_Category__c FROM Product2 WHERE Id = :currentProduct.Id].get(0);
    
    
    Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Customer Contact').getRecordTypeId();
    newContact.RecordTypeId = recordTypeId;
    
    
  }
  
  //-----------------------------------------------------------------------------------------------
  // Action method for save and return to Account
  //-----------------------------------------------------------------------------------------------
  public pageReference saveAccount(){
    Boolean success = saveContact();
    if(success){
      return new PageReference('/' + newContact.AccountId);
    }
    return null;
  }
  
  //-----------------------------------------------------------------------------------------------
  // Action method for save and create new case
  //-----------------------------------------------------------------------------------------------
  public pageReference saveCase(){
    Boolean success = saveContact();
    if(success){
      String url = '/setup/ui/recordtypeselect.jsp?ent=Case&save_new_url=%2F500%2Fe%3FretURL%3D%252F500%252Fo&cas3_lkid=' + newContact.Id +
      '&cas4_lkid=' + newContact.AccountId + '&retURL=/apex/SmartcontactSearch?accid=' + newContact.AccountId;
      return new PageReference(url);
    }
    return null;
  }
  
  //-----------------------------------------------------------------------------------------------
  // Helper method to save contact
  //-----------------------------------------------------------------------------------------------
  private Boolean saveContact(){
    Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Endo Customer Contact').getRecordTypeId();
    newContact.RecordTypeId = recordTypeId;
    system.debug('@@@' + newContact);
    try{
      insert newContact;
      return true;
    }
    catch(Exception ex){
        Apexpages.addMessages(ex);
        return false;
    }
  }
  
}