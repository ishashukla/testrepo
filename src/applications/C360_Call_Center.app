<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Set of functionality for the Endoscopy Call Center.</description>
    <formFactors>Large</formFactors>
    <label>C360 Call Center</label>
    <logo>EndoSharedDocuments/C360StrykerLogoTransparent_png.png</logo>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Knowledge</tab>
    <tab>standard-KnowledgePublishing</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Chatter</tab>
    <tab>Asset__c</tab>
    <tab>Order__c</tab>
    <tab>Pricing_Contract__c</tab>
    <tab>Pricing_Contract_Line_Item__c</tab>
    <tab>Service_Contract__c</tab>
    <tab>Service_Line_Item__c</tab>
    <tab>Non_Conformance_Report__c</tab>
</CustomApplication>
