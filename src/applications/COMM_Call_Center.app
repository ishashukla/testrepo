<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>COMM T-459516,T-515702, I-235328</description>
    <label>COMM Call Center</label>
    <logo>Logo/Stryker_Logo_PNG_File.png</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Document</tab>
    <tab>standard-KnowledgePublishing</tab>
    <tab>StrykerPulse</tab>
    <tab>Trial__c</tab>
    <tab>SVMXC__Service_Contract__c</tab>
    <tab>SVMXC__Installed_Product__c</tab>
    <tab>Document__c</tab>
    <tab>SVMXC__Timesheet__c</tab>
    <tab>SVMXC__Warranty__c</tab>
    <tab>Address__c</tab>
    <tab>Activities</tab>
    <tab>Non_Conformance_Report__c</tab>
</CustomApplication>
