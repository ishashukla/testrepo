<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>A default branded App for Flex Financial Project</description>
    <formFactors>Large</formFactors>
    <label>Flex Financial Sales</label>
    <logo>Public_Folder/Flex_Financial_Sales_Logo.png</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>Flex_Contract__c</tab>
    <tab>Flex_Contract_Line__c</tab>
    <tab>Flex_Contingency__c</tab>
    <tab>Flex_Credit__c</tab>
    <tab>standard-Document</tab>
    <tab>standard-Lead</tab>
</CustomApplication>
