<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>COMM_Account_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Account</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Lead</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Opportunity_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Oppty_Doc_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Document__c</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#1589EE</headerColor>
        <logo>servlet2_1</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <formFactors>Large</formFactors>
    <label>COMM Admin Navigation</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Account</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Task</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Feed</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-ContentNote</tab>
    <tab>standard-report</tab>
    <tab>Document__c</tab>
    <tab>Checklist_Header__c</tab>
    <tab>Checklist_Group__c</tab>
    <tab>CheckList_Detail__c</tab>
    <tab>ChecklistCUST__c</tab>
    <tab>CheckList_Item__c</tab>
    <tab>SVMXC__Product_Stock__c</tab>
    <tab>Cycle_Count__c</tab>
    <tab>Non_Conformance_Report__c</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
